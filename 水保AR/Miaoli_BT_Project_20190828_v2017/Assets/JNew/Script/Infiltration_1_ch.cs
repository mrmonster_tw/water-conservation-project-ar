﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Infiltration_1_ch : MonoBehaviour {

	public GameObject Infiltration_1_ball;
	public GameObject finger;
	public GameObject touchfinger;

	public GameObject s_1;
	public GameObject s_2;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
	private void OnMouseDown()
	{
		colliderfal (false);
		Infiltration_1_ball.SetActive (true);
		this.gameObject.SetActive (false);
		touchfinger.SetActive (false);
		finger.SetActive (true);
	}
	public void colliderfal(bool sw)
	{
		s_1.GetComponent<BoxCollider> ().enabled = sw;
		s_2.GetComponent<BoxCollider> ().enabled = sw;
	}
}
