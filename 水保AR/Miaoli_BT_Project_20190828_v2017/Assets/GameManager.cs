﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class GameManager : MonoBehaviour
{   
   public static GameManager Gmanager;
    public GameObject Bt;
    public Text bt_text;
    public enum device_state
    {
        Phone, NoPhone
    }
    public device_state _device_state = device_state.Phone;

    public enum GameState
    {
        Main,
        Level_01,
        Level_02,
        Level_03,
        Level_04,
        Level_05,
        notMobile_Main
    }
    public GameState _state = GameState.Main;

    public bool L01_workDown = false;
    public bool L02_workDown = false;
    public bool L03_workDown = false;
    public bool L04_workDown = false;
    public bool L05_workDown = false;

    public GameObject Image_UI_down;
    public GameObject Image_UI_down_nophone;
    public GameObject Win_Panal;
    public GameObject Lose_Panel;
	public GameObject Win_Panal2;
	public GameObject Lose_Panel2;
   private void Awake()
   {
      Gmanager = this;
      if (_device_state == device_state.Phone)
      {
         bt_text.enabled = false;
         Bt.SetActive(false);
         Image_UI_down.SetActive(true);
         Image_UI_down_nophone.SetActive(false);
      }
      if (_device_state == device_state.NoPhone)
      {
         bt_text.enabled = true;
         Bt.SetActive(true);
         Image_UI_down.SetActive(false);
         Image_UI_down_nophone.SetActive(true);
      }

   }
   //Use this for initialization
   void Start()
    {
      //bool focusModeSet = CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_INFINITY);
      Drag_Controller.nowPicCount = 0;

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.RightControl) && Input.GetKeyDown(KeyCode.F12)) { UnityEngine.SceneManagement.SceneManager.LoadScene(2); }

    }

    public void Set_Game_State_01()
    { _state = GameState.Level_01; }
    public void Set_Game_State_02()
    { _state = GameState.Level_02; }
    public void Set_Game_State_03()
    { _state = GameState.Level_03; }
    public void Set_Game_State_04()
    { _state = GameState.Level_04; }
    public void Set_Game_State_05()
    { _state = GameState.Level_05; }
    public void Set_Game_State_Main()
    {
      _state = (_device_state == device_state.Phone) ? GameState.Main : GameState.notMobile_Main;
   }

    public void Level_01_Mission_Success()
    {L01_workDown = true;}
    public void Level_02_Mission_Success()
    {L02_workDown = true;}
    public void Level_03_Mission_Success()
    {L03_workDown = true;}
    public void Level_04_Mission_Success()
    {L04_workDown = true;}
    public void Level_05_Mission_Success()
    {L05_workDown = true;}
    public void WinLose()
    {
        if (L01_workDown && L02_workDown && L03_workDown && L04_workDown && L05_workDown)
        {
            Win_Panal.SetActive(true);
        }
        else
        {
            Lose_Panel.SetActive(true);
        }
	}
	public void WinLose_Each(){
		if (L01_workDown && L02_workDown && L03_workDown && L04_workDown && L05_workDown)
		{
			Win_Panal2.SetActive(true);
			Lose_Panel2.SetActive(false);
		}
		else
		{
			Lose_Panel2.SetActive(true);
		}
	
	}
    public void URL()
    {
        Application.OpenURL("https://www.facebook.com/swcb02/");
    }
}
