﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class bt_sound : MonoBehaviour {

    public AudioClip bt_s;
    public void play_bt_sound()
    {
        gameObject.GetComponent<AudioSource>().Stop();
        gameObject.GetComponent<AudioSource>().clip = bt_s;
        gameObject.GetComponent<AudioSource>().Play();
    }
}
