﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Sprite_Changer : MonoBehaviour {
    //private Text 請點擊任意處以繼續遊戲;
    public GameObject nextImage;
    public int Ask_Numbers = 2;
    private void Awake()
    {
        //請點擊任意處以繼續遊戲 = FindObjectsOfType<Text>().ToList().Find(t => t.gameObject.name == "請點擊任意處以繼續遊戲");
    }
    private void Start()
    {
    }
    private void OnEnable()
    {
        //if (請點擊任意處以繼續遊戲)
          //  請點擊任意處以繼續遊戲.enabled = true;
    }
    public void next_page()
    {
        //if (請點擊任意處以繼續遊戲)
          //  請點擊任意處以繼續遊戲.enabled = true;

        //若是選擇按鈕
        if (Ask_Numbers == transform.parent.childCount)
        {
            gameObject.transform.parent.gameObject.SetActive(false);
            nextImage.SetActive(true);
            return;
        }

        //若切到最後一張
        if (nextImage == null)
        {
            //gameObject.SetActive(false);
            gameObject.transform.parent.gameObject.GetComponent<Level_Controller>().unShowImage();
            gameObject.transform.parent.gameObject.GetComponent<Level_Controller>().isPostLevel = true;
            //if (請點擊任意處以繼續遊戲)
              //  請點擊任意處以繼續遊戲.enabled = false;
            return;
        }

        //若是一般切換介面
        nextImage.SetActive(true);
        
        gameObject.SetActive(false);
        return;
    }
}
