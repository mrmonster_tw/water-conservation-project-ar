﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lottery_Controller : MonoBehaviour
{
    public GameObject Mission_Get;
    public GameObject Mission_Lose;
    public GameObject Confirm_Lottery;
    public GameObject Cancel_lottery;
    public GameObject lottery_Win;
    public GameObject lottery_Lose;

    public GameObject[] star_Images;
    public GameObject Parent_obj;
    public GameObject Shinning;

    public GameObject[] star_ImagesWIN;
    public GameObject Parent_objWIN;
    public GameObject ShinningWIN;

    public float Shinning_speed = 50;

    void OnEnable()
    {
        GameObject.Find("Tips").GetComponent<Tips_Controller>().closeAll();
        Mission_Get.SetActive(false);
        Mission_Lose.SetActive(false);
        Confirm_Lottery.SetActive(false);
        Cancel_lottery.SetActive(false);
        lottery_Win.SetActive(false);
        lottery_Lose.SetActive(false);
        //若一到五關都破了
        if (GameObject.Find("ApplicationGameMaker").GetComponent<GameManager>().L01_workDown &&
            GameObject.Find("ApplicationGameMaker").GetComponent<GameManager>().L02_workDown &&
            GameObject.Find("ApplicationGameMaker").GetComponent<GameManager>().L03_workDown &&
            GameObject.Find("ApplicationGameMaker").GetComponent<GameManager>().L04_workDown &&
            GameObject.Find("ApplicationGameMaker").GetComponent<GameManager>().L05_workDown)
        {
            Mission_Get.SetActive(true);
            Parent_obj.transform.localScale = Vector3.zero;
            foreach (GameObject i in star_Images)
                i.transform.localScale = Vector3.zero;
            StartCoroutine(StartImage());
        }
        //若有其中一關沒破
        else
        {
            Mission_Lose.SetActive(true);
        }
    }
    void OnDisable()
    {
        iTween.Stop();
    }
    void Start()
    {
        Close();
    }
    void Update()
    {
        if (Shinning)
            Shinning.transform.Rotate(0, 0, Shinning_speed * Time.deltaTime);
        if (ShinningWIN)
            ShinningWIN.transform.Rotate(0, 0, Shinning_speed * Time.deltaTime);
    }

    IEnumerator StartImage()
    {
        iTween.ScaleTo(Parent_obj, iTween.Hash("scale", new Vector3(0.04f, 0.04f, 0.04f), "time", 0.75f, "easeType", iTween.EaseType.easeOutBounce));
        yield return new WaitForSeconds(1);
        for (int i = 0; i < star_Images.Length; i++)
        {
            iTween.ScaleTo(star_Images[i], iTween.Hash("scale", Vector3.one, "time", 0.5f, "easeType", iTween.EaseType.easeOutBack));
            yield return new WaitForSeconds(0.5f);
        }
    }
    IEnumerator StartImage_WIN()
    {
        iTween.ScaleTo(Parent_objWIN, iTween.Hash("scale", new Vector3(0.04f, 0.04f, 0.04f), "time", 0.75f, "easeType", iTween.EaseType.easeOutBounce));
        yield return new WaitForSeconds(1);
        for (int i = 0; i < star_ImagesWIN.Length; i++)
        {
            iTween.ScaleTo(star_ImagesWIN[i], iTween.Hash("scale", Vector3.one, "time", 0.5f, "easeType", iTween.EaseType.easeOutBack));
            yield return new WaitForSeconds(0.5f);
        }
    }
    public void Open()
    {
        gameObject.SetActive(true);
    }
    public void Close()
    {
        gameObject.SetActive(false);
    }
    public void Go_lottery()
    {
        Mission_Get.SetActive(false);
        Mission_Lose.SetActive(false);
        Confirm_Lottery.SetActive(true);
        Cancel_lottery.SetActive(false);
        lottery_Win.SetActive(false);
        lottery_Lose.SetActive(false);
        GameObject.Find("ApplicationGameMaker").GetComponent<GameManager>().L01_workDown = false;
        GameObject.Find("ApplicationGameMaker").GetComponent<GameManager>().L02_workDown = false;
        GameObject.Find("ApplicationGameMaker").GetComponent<GameManager>().L03_workDown = false;
        GameObject.Find("ApplicationGameMaker").GetComponent<GameManager>().L04_workDown = false;
        GameObject.Find("ApplicationGameMaker").GetComponent<GameManager>().L05_workDown = false;
    }
    public void Exit_lottery()
    {
        Mission_Get.SetActive(false);
        Mission_Lose.SetActive(false);
        Confirm_Lottery.SetActive(false);
        Cancel_lottery.SetActive(true);
        lottery_Win.SetActive(false);
        lottery_Lose.SetActive(false);
        GameObject.Find("ApplicationGameMaker").GetComponent<GameManager>().L01_workDown = false;
        GameObject.Find("ApplicationGameMaker").GetComponent<GameManager>().L02_workDown = false;
        GameObject.Find("ApplicationGameMaker").GetComponent<GameManager>().L03_workDown = false;
        GameObject.Find("ApplicationGameMaker").GetComponent<GameManager>().L04_workDown = false;
        GameObject.Find("ApplicationGameMaker").GetComponent<GameManager>().L05_workDown = false;
    }
    public void Send_Data()
    {
        int chance = 0;
        chance = Random.Range(1, 6);

        if (chance == 1)
        {
            Mission_Get.SetActive(false);
            Mission_Lose.SetActive(false);
            Confirm_Lottery.SetActive(false);
            Cancel_lottery.SetActive(false);
            lottery_Win.SetActive(true);
            lottery_Lose.SetActive(false);

            Parent_objWIN.transform.localScale = Vector3.zero;
            foreach (GameObject i in star_ImagesWIN)
                i.transform.localScale = Vector3.zero;
            StartCoroutine(StartImage_WIN());
        }
        else
        {
            Mission_Get.SetActive(false);
            Mission_Lose.SetActive(false);
            Confirm_Lottery.SetActive(false);
            Cancel_lottery.SetActive(false);
            lottery_Win.SetActive(false);
            lottery_Lose.SetActive(true);
        }
    }

    public void reload()
    {
        Application.LoadLevel("1_MainMenu");
    }
}
