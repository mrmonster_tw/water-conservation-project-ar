﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class fade_InOut : MonoBehaviour {
	public Image Black;
	public bool Run;
	public float Speed;
	public float Max;
	public Color temp;
	// Use this for initialization
	void Start () {
		 temp = Black.color;
	}
	
	// Update is called once per frame
	void Update ()
	{	
		if(!Run){
				if (Max == 0) {
					temp.a += Speed * Time.deltaTime;
				} else {
					if (temp.a < Max) {
						temp.a += Speed * Time.deltaTime;
//					print (temp.a);
					} else {
						temp.a = Max;
//						print ("2");
					}
			
				}
		}else{
			temp.a -= Speed * Time.deltaTime;
		}
		Black.color = temp;
		if (temp.a <= 0) {
			this.gameObject.SetActive (false);
		}
	}
}
