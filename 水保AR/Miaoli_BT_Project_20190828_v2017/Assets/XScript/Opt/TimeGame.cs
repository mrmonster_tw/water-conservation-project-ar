﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
public class TimeGame : MonoBehaviour {
	public float Left_time = 60;
	public float Left_timeMax = 60;
	public GameObject checkObj;
	public Image im;
	public Text timeText;
	public GameObject Clock;
	public GameObject[] OpenIt;
	public UnityEvent Events;

	// Use this for initialization
	public GameObject ReplayTips;
	//public GameObject RockAnim_Clone;
	public GameObject RC_Pos;

	public bool Rock_Once;

    private Level_Controller Level_05;

    private void Awake()
    {
        Level_05 = System.Array.Find(FindObjectsOfType<Level_Controller>(), l => l.name == "Level_05");
    }
    void Start () {
		//im = GetComponent<Image> ();	
		//timeText = GetComponentInChildren<Text> ();

	}
	
	// Update is called once per frame
	void Update () {
        if (!Level_05) return;
        if (!Level_05.isPostLevel) return;

        if (!this.gameObject.GetComponent<Level_05_Controller> ().do_once) {
			Destroy (this);
			Destroy(Clock);

		}
		if (checkObj.gameObject.activeSelf&&!this.gameObject.GetComponent<Level_05_Controller> ().win_panel.activeSelf) {
			im.fillAmount = Left_time / Left_timeMax;
			if (Left_time > 0.4f) {
				Left_time -= Time.unscaledDeltaTime;
				//timeText.text = Left_time.ToString("##");
				string tempt;
				tempt = Left_time.ToString("##");
				if(Left_time<9.5f){
					timeText.text = "0" + Left_time.ToString("##");
					}else{
								timeText.text = tempt ;
					}
			}else if (Left_time < 0.4f) {
				timeText.text = "00";
                TimesUp();
                if (!Rock_Once) {
					Rock_Once = true;
                    StartCoroutine(RockStart());
                }
            }
		}

	}

	public IEnumerator RockStart(){
        if (GetComponent<Level_05_Controller>())
            GetComponent<Level_05_Controller>().completeParticle.Play();
        yield return new WaitForSeconds (5.0f);
		ReplayTips.SetActive (true);       
    }
    public void Resets()
    {
        StartCoroutine(Reset());
    }
	public IEnumerator Reset(){
		yield return new WaitForSeconds (0.1f);
        if (GetComponent<Level_05_Controller>())
            GetComponent<Level_05_Controller>().completeParticle.Stop();
        Left_time = Left_timeMax;
		Rock_Once = false;           
    }
	public void TimesUp(){
		Left_time = 0;
		timeText.text = Left_time.ToString();
        Events.Invoke ();
      foreach (var pointController in GetComponentsInChildren<Point_Controller>())
         pointController.OnMouseUp();
      foreach (var leve4To2Ds in GetComponentsInChildren<leve4To2D>())
         leve4To2Ds.OnMouseUp();
   }
    public void OpenAll(){
        foreach (GameObject i in OpenIt)
		{
			i.SetActive(true);
			i.GetComponentInChildren<SpriteRenderer> ().enabled = true;
		}
	}
}
