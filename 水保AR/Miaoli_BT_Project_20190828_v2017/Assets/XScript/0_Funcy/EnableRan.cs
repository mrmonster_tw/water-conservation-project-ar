﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableRan : MonoBehaviour {
    public CameraFilterPack_Atmosphere_Rain_Pro rain;

    private void OnDisable()
    {
      if (rain)
         rain.enabled = false;
    }
	
	void Start ()
    {
		
	}
    bool OnMeshRenderOpen = false;
	void Update ()
    {
        if (OnMeshRenderOpen != GetComponentInChildren<MeshRenderer>().enabled)
        {
            OnMeshRenderOpen = GetComponentInChildren<MeshRenderer>().enabled;
            rain.enabled = OnMeshRenderOpen;

        }
        
    }
}
