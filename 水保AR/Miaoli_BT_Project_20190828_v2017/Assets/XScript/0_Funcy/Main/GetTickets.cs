﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RestFulsClass;

[ExecuteInEditMode]
public class GetTickets : RestFulClass
{
    public enum IPAddress
    {
        _192o168o1o174,
        _118o163o191o154,
            _118o163o191o153
    }
    public IPAddress address;
    string GetAddress(IPAddress addres)
    {
        return addres.ToString().Substring(1).Replace('o', '.');
    }
    private string GetTypeForSceneMode(string mode)
    {
        string s = "";
        switch (mode)
        {
            case "3": s = "3D VR攝影棚「水土保持守護戰記」"; break;
            case "4": s = "4D 劇院「從土石流災害看見的事」"; break;
            case "5": s = "3D 電影「水保小尖兵－埤塘風雲」"; break;
            case "6": s = "神祕小禮抽獎"; break;
        }
        return s;
    }
    public static GetTickets This;

    #region Initialize Group
        #region Variables And Componets
        public static string Mode, PlayerUUID;
        private int hour, minute;

        public PlayerSetting playerSetting;
        [Serializable] public struct PlayerSetting { 
        public string myAddress, mySVC, FuncNormal;
        }
        public ComponetFuncScript componetFuncScript;
        [Serializable] public struct ComponetFuncScript {
        public ExpiredComponets    expiredComponets   ;
        public QuestionedComponets questionedComponets;
        }        
    #endregion
        #region Initialize3DSceneList()
        private void Initialize3DScene()
        {
        #region 3D場次陣列
        DateTime[] dt = new DateTime[d3D.Length];
        hour = 10; minute = 20;

        for (int i = 0; i < dt.Length; i++)
        {
            dt[i] = dt[i].AddYears(DateTime.Now.Year - 1);
            dt[i] = dt[i].AddMonths(DateTime.Now.Month - 1);
            dt[i] = dt[i].AddDays(DateTime.Now.Day - 1);
        }
        for (int i = 0; i <= 9; i++)
        {
            dt[i] = dt[i].AddHours(10 + ((i <= 1) ? 0 : (i <= 3) ? 1 : (i <= 5) ? 3 : (i <= 7) ? 4 : 5));
            dt[i] = dt[i].AddMinutes(20 + ((i % 2 == 1) ? 20 : 0));
        }
        d3D = dt;
        #endregion
        }
    #endregion
        #region Initialize4DSceneList()
        private void Initialize4DScene()
        {
            #region 4D場次陣列
            DateTime[] dt2 = new DateTime[d4D.Length];
            minute = 10; minute = 10;
            for (int i = 0; i < dt2.Length; i++)
            {
                dt2[i] = dt2[i].AddYears(DateTime.Now.Year - 1);
                dt2[i] = dt2[i].AddMonths(DateTime.Now.Month - 1);
                dt2[i] = dt2[i].AddDays(DateTime.Now.Day - 1);
            }

            for (int i = 0; i <= 14; i++)
            {
                dt2[i] = dt2[i].AddHours(10 + ((i <= 2) ? 0 : (i <= 5) ? 1 : (i <= 8) ? 3 : (i <= 11) ? 4 : 5));
                dt2[i] = dt2[i].AddMinutes(10 + ((i % 3 == 0) ? 0 : (i % 3 == 1) ? 10 : 20));
            }
            d4D = dt2;
            #endregion
        }
        #endregion
        #region Initialize()
        public DateTime[] d3D = new DateTime[10], d4D = new DateTime[15],dnoD=new DateTime[0];
        
        [HideInInspector]public Animator Connecting;
        private void Initialize()
    {
        PlayerUUID = SystemInfo.deviceUniqueIdentifier;
        This = this; RestFul.This = This; ListString();
        Connecting = Array.Find(FindObjectsOfType<Animator>(), a => a.name == "Connecting");
        Initialize3DScene(); Initialize4DScene();
    }
        public void ListString()
    {
        RestFul.myAddress  = playerSetting.myAddress ;
        RestFul.mySVC      = playerSetting.mySVC     ;
        RestFul.FuncNormal = playerSetting.FuncNormal;
        RestFul.PlayerUUID = PlayerUUID;
    }
        #endregion
    #endregion

    void Awake()
    {
        if (!Application.isPlaying) return; Initialize();
    }
    void Start()
    {
        if (!Application.isPlaying) return;
        GC.Collect();
        Resources.UnloadUnusedAssets();
        #region Setting Server Info
            playerSetting.myAddress = GetAddress(address); 
            playerSetting.mySVC      = "/SAWC/wcf/SAWCService.svc";
            playerSetting.FuncNormal =                   "RestFul";
        #endregion   
        RestFul.myAddress = playerSetting.myAddress;
    }

    #region Communication with RestFul
    private void GetData(string s) { 
        
        string[] sArrat = s.Split(','); string t = sArrat[0], sceneMode = sArrat[1];
        GetFuncTypes T = ConvertType.stringToGetFuncTypes(t); GetSceneMode S = ConvertType.stringToGetSceneMode(sceneMode);
        StartCoroutine(RestFul.Datas.Getdata(T, S));
    }
    private void SetData(string s) { 
        
        string[] sArrat = s.Split(','); string t = sArrat[0], sceneMode = sArrat[1];
        InsertTypes T = ConvertType.stringToInsertTypes(t); GetSceneMode S = ConvertType.stringToGetSceneMode(sceneMode);
        StartCoroutine(RestFul.Datas.Setdata(T, S));
    }
    #endregion
    #region WWW Convert Tool
    public struct ConvertTool
    {
        public static bool boolToString(string s) { return ((s == "true") ? true : false); }        
        public static IEnumerator checkQuestion(string type)
        {
            checkingAny[0] = true;
            RestFul.CheckQuestion(PlayerUUID, type);
            while (RestFul.Datas.RestFulString.Count <= 0) { yield return null; print("Waitting!!"); }   //等待 RestFul.Datas 取得資料                
            checkingAny[0] = false;
        }
        public static IEnumerator checkExpired()
        {
            checkingAny[1] = true;
            #region 先看看你有沒有已經預約的票種
            RestFul.GetUserExpired(PlayerUUID);

            UnityEngine.UI.Text message = FindObjectsOfType<UnityEngine.UI.Text>().ToList().Find(t => t.gameObject.name == "目前非系統預約開放時間");
            #region 等待 RestFul.Datas 取得資料
            {
                float nowTime = Time.realtimeSinceStartup;
                while (true)
                {
                    yield return null;
                    if (RestFul.Datas.RestFulString.Count > 2) break;

                    if (Time.realtimeSinceStartup - nowTime > 10f)
                    {
                        message.enabled = true;
                        RestFul.SetConnectState(false);
                        while (true)
                        {
                            if (Input.touchCount > 0)
                            {
                                if (Input.GetTouch(0).phase == TouchPhase.Began)
                                {
                                    message.enabled = false;
                                    This.StopCoroutine(RestFul.Datas.Getdata(GetFuncTypes.GetUserNumber, GetSceneMode._0, PlayerUUID));
                                    This.StartCoroutine(RestFul.Datas.Getdata(GetFuncTypes.GetUserNumber, GetSceneMode._0, PlayerUUID));
                                    nowTime = Time.realtimeSinceStartup;
                                    RestFul.SetConnectState(true);
                                    break;
                                }
                            }
                            else if (Input.GetMouseButtonDown(0))
                            {
                                message.enabled = false;
                                This.StopCoroutine(RestFul.Datas.Getdata(GetFuncTypes.GetUserNumber, GetSceneMode._0, PlayerUUID));
                                This.StartCoroutine(RestFul.Datas.Getdata(GetFuncTypes.GetUserNumber, GetSceneMode._0, PlayerUUID));
                                nowTime = Time.realtimeSinceStartup;
                                RestFul.SetConnectState(true);
                                break;
                            }
                            yield return null;
                        }


                    }
                }
            }
            #endregion 等待 RestFul.Datas 取得資料


            List<string> RestFulString = RestFul.Datas.RestFulString;
            RestFulString[0] = RestFulString[0].Replace("Expired", ""); RestFulString[0] = RestFulString[0].Substring(2);
            RestFulString[1] = RestFulString[1].Replace("Number", ""); RestFulString[1] = RestFulString[1].Substring(2);
            RestFulString[2] = RestFulString[2].Replace("Type", "");

            string elseType = (RestFulString.Count <= 3) ? "" : GetedElseType(RestFulString);
            RestFulString[2] += elseType;
            char specialChar = '"';
            RestFulString[2] = RestFulString[2].Replace(specialChar.ToString(), "");
            //print(RestFulString[2]);

            RestFul.Datas.RestFulString = RestFulString;
            #endregion
            checkingAny[1] = false;
        }
        public static bool TryParhase(string s)
        {
            int tryParse;
            return int.TryParse(RestFul.Datas.RestFulString[0], out tryParse);
        }
        static string GetedElseType(List<string> restFulString)
        {
            string s = "";
            for (int i = 3; i < restFulString.Count; i++)
            {
                s += "," + ((i == restFulString.Count - 1) ? restFulString[i].Substring(0, 1) : restFulString[i]);
            }
            return s;
        }
    }
    #endregion

    #region CheckSceneAPI
    public void CheckScene(string url) { StartCoroutine(checkSceneMode(url)); } //請掛在UnityEvent上
    List<string> checkExpiredresult = new List<string>();
    public static bool[] checkingAny = new bool[2];
   public IEnumerator checkSceneMode(string url)
   {
      int getScenesMode = 0, Number = 0;
      string[] temps = url.Split(','); //(功能,票種)

      RestFul.SetConnectState(true);

      #region 看看你是不是有待櫃台確認的獎品
      yield return StartCoroutine(ConvertTool.checkExpired());

      while (RestFul.Datas.RestFulString.Count <= 0) { yield return null; }   //等待 RestFul.Datas 取得資料


      bool Expired = ConvertTool.boolToString(RestFul.Datas.RestFulString[0]);
      Debug.Log(RestFul.Datas.RestFulString[1]);
      int
      number = int.Parse(RestFul.Datas.RestFulString[1]);
      string[] getTypesStr = RestFul.Datas.RestFulString[2].Split(',');
      List<int> getunExpiredTypes = new List<int>();
      for (int i = 0; i < getTypesStr.Length; i++)
      {
         getunExpiredTypes.Add(int.Parse(getTypesStr[i]));
      }
      RestFul.Datas.RestFulString.Clear();


      print("票種:" + temps[1] + "," + Expired + "," + number + ",已持有票種數:" + getunExpiredTypes.Count);
      #endregion

      int findNumber = 0;


      RestFul.Datas.RestFulString.Clear();
      RestFul.GetMaxNumber(temps[0], temps[1]);
      while (RestFul.Datas.RestFulString.Count <= 0)
      {
         yield return null;
      }
      //等待 RestFul.Datas 取得資料        
      for (int i = 0; i < temps.Length; i++)                                  //嘗試解析字串，如果格式錯誤請報錯
         while (!ConvertTool.TryParhase(temps[i])) { Debug.LogError("你嘗試Parhase的字串不是數字格式!!!"); yield return null; }


      int nowPlayerCount = int.Parse(RestFul.Datas.RestFulString[0]); int sceneMode = int.Parse(temps[1]);
      Number = nowPlayerCount + 1;

      getScenesMode = int.Parse(temps[1]);

      int nowHour = DateTime.Now.Hour; int nowMinute = DateTime.Now.Minute;

      List<DateTime> selectDateTime = new List<DateTime>();
      selectDateTime.AddRange(
      ((sceneMode == 3) ? d3D : (sceneMode == 4) ? d4D : dnoD)
      );


      selectDateTime.RemoveAll(d => d.Hour < nowHour);
      selectDateTime.RemoveAll(d => d.Hour == nowHour && d.Minute < nowMinute);



      nowPlayerCount /= getScenesMode;


      #region 如果是需要找時間的場次 就執行篩選模式
      if (getScenesMode == 3 || getScenesMode == 4)//is 3D or 4D
      {
         if (selectDateTime.Count <= 0)
         {
            Debug.Log("你太晚預約了！今天已經沒有任何場次摟!");
            componetFuncScript.expiredComponets.normalList.buttonList.gameObject.SetActive(true);
            componetFuncScript.expiredComponets.normalList.msg.gameObject.SetActive(true);
         }
         else
         {
            bool fix = false;
            findNumber = 0;
            while (true)
            {
               bool b = false;
               try
               { b = selectDateTime[findNumber + nowPlayerCount].Hour >= nowHour; }
               catch { fix = true; break; }
               if (b)
               {
                  break;
               }
               findNumber++; yield return null;
            }
            if (findNumber >= 0)
            {
               while (true)
               {
                  bool b = false;
                  try
                  { b = selectDateTime[findNumber + nowPlayerCount].Hour == nowHour && selectDateTime[findNumber + nowPlayerCount].Minute < nowMinute; }
                  catch { fix = true; break; }
                  if (b)
                     findNumber++;
                  else break;
                  yield return null;
               }
               if (findNumber >= 0 && !fix) 
               {
                  hour = selectDateTime[findNumber + nowPlayerCount].Hour; minute = selectDateTime[findNumber + nowPlayerCount].Minute;
                  print(hour + ":" + minute);
                  componetFuncScript.expiredComponets.SetSure(sceneMode, true, Number, hour, minute);
               }
               if(fix)
               {
                  Debug.Log("你太晚預約了！今天已經沒有任何場次摟!");
                  componetFuncScript.expiredComponets.normalList.buttonList.gameObject.SetActive(true);
                  componetFuncScript.expiredComponets.normalList.msg.gameObject.SetActive(true);
               }
            }
         }
      }
      else if (getScenesMode == 5 || getScenesMode == 6)//電影票 or ㄙㄣˊ秘小禮物
      {
         componetFuncScript.expiredComponets.SetSure(sceneMode, true, Number, hour, minute);
      }

      #endregion


      RestFul.Datas.RestFulString.Clear();
      RestFul.SetConnectState(false);

      if (getScenesMode == 0 || Number == 0 || findNumber < -1) yield break;
      while (!sureInsert && componetFuncScript.expiredComponets.sureList.sureListAnimator[getScenesMode - 3].GetBool("Scale"))//等待使用者按下按鈕
         yield return null;

      if (sureInsert) { StartCoroutine(GetTicket(getScenesMode.ToString(), Number, hour, minute)); }
   }
    IEnumerator GetTicket(string scenemode, int Number, int nextHour, int nextMinute)
    {
        yield return null;

        RestFul.SetConnectState(true);
        RestFul.InsertNewNumber(PlayerUUID, scenemode);
        while (RestFul.Datas.RestFulString.Count <= 0) { yield return null; }                                                   //等待 RestFul.Datas 取得資料        
        int newNomber = int.Parse(RestFul.Datas.RestFulString[0]);
        print("場次:" + scenemode + ",號碼:" + newNomber);
        componetFuncScript.expiredComponets.SetResult(int.Parse(scenemode), newNomber, nextHour, nextMinute, false);
        PlayerPrefs.SetInt("問券填寫", int.Parse(scenemode));
        RestFul.SetConnectState(false);
        yield return new WaitForSeconds(0.8f);

        CaptureScreenshot();
    }
    IEnumerator checkQuestion(string url, int number, int mode)
    {
        string[] temps = url.Split(',');

        #region 看看你是不是有跳過沒填的問券
        StartCoroutine(ConvertTool.checkQuestion(temps[1]));
        while (!checkingAny[0]) yield return null; while (checkingAny[0]) yield return null;//先確定已經進入執行續,再開始等待執行續結束
        bool Questioned = ConvertTool.boolToString(RestFul.Datas.RestFulString[0]);
        RestFul.Datas.RestFulString.Clear();
        yourUnQuestionedMode = mode.ToString();
        yourUnQuestionedNumber = number;
        print("票種:" + yourUnQuestionedMode + "," + "Questioned:" + !Questioned + "未回答問券組:" + PlayerPrefs.GetInt("問券填寫"));

        if (Questioned || PlayerPrefs.GetInt("問券填寫") != 0)
            componetFuncScript.questionedComponets.gameObject.SetActive(true);

        if (PlayerPrefs.GetInt("問券填寫") == 0)
        {
            componetFuncScript.expiredComponets.normalList.buttonList.gameObject.SetActive(true);
            componetFuncScript.expiredComponets.normalList.msg.gameObject.SetActive(true);
        }
            

        #endregion
    }
    public GameObject thanks;
    private string yourUnQuestionedMode = "";int yourUnQuestionedNumber = 0;
    public void SendQuestionData() { StartCoroutine(sendQuestionData()); }
    IEnumerator sendQuestionData()
    {
        RestFul.Datas.RestFulString.Clear();
        RestFul.SetConnectState(true);
        yield return null;
        string[] questionResult = componetFuncScript.questionedComponets.PlayerSelectInfo.Split(',');

        //將問券資料寫到Serv
        RestFul.InsertQuestion(yourUnQuestionedMode, yourUnQuestionedNumber.ToString(),
            string.IsNullOrEmpty(questionResult[2]) ? "" : questionResult[2],
            string.IsNullOrEmpty(questionResult[1]) ? "" : questionResult[1],
            string.IsNullOrEmpty(questionResult[0]) ? "" : questionResult[0],
            string.IsNullOrEmpty(questionResult[3]) ? "" : questionResult[3]
            );

        UnityEngine.UI.Text message = FindObjectsOfType<UnityEngine.UI.Text>().ToList().Find(t => t.gameObject.name == "目前非系統預約開放時間");
        #region 等待 RestFul.Datas 取得資料
        {
            float nowTime = Time.realtimeSinceStartup;
            while (true)
            {
                yield return null;
                if (RestFul.Datas.RestFulString.Count > 0) break;

                if (Time.realtimeSinceStartup - nowTime > 10f)
                {
                    message.enabled = true;
                    RestFul.SetConnectState(false);
                    while (true)
                    {
                        if (Input.touchCount > 0)
                        {
                            if (Input.GetTouch(0).phase == TouchPhase.Began)
                            {
                                message.enabled = false;
                                InsertTypes type = InsertTypes.InsertQuestion;
                                GetSceneMode mode = ConvertType.stringToGetSceneMode(yourUnQuestionedMode);

                                This.StopCoroutine(
                                    RestFul.Datas.Setdata(type, mode, "0", string.IsNullOrEmpty(questionResult[2]) ? "" : questionResult[2],
            string.IsNullOrEmpty(questionResult[1]) ? "" : questionResult[1],
            string.IsNullOrEmpty(questionResult[0]) ? "" : questionResult[0],
            string.IsNullOrEmpty(questionResult[3]) ? "" : questionResult[3])
);
                                This.StartCoroutine(
                                    RestFul.Datas.Setdata(type, mode, "0", string.IsNullOrEmpty(questionResult[2]) ? "" : questionResult[2],
            string.IsNullOrEmpty(questionResult[1]) ? "" : questionResult[1],
            string.IsNullOrEmpty(questionResult[0]) ? "" : questionResult[0],
            string.IsNullOrEmpty(questionResult[3]) ? "" : questionResult[3])
                                    );

                                nowTime = Time.realtimeSinceStartup;
                                RestFul.SetConnectState(true);
                                break;
                            }
                        }
                        else if (Input.GetMouseButtonDown(0))
                        {
                            message.enabled = false;
                            InsertTypes type = InsertTypes.InsertQuestion;
                            GetSceneMode mode = ConvertType.stringToGetSceneMode(yourUnQuestionedMode);

                            This.StopCoroutine(
    RestFul.Datas.Setdata(type, mode, "0", string.IsNullOrEmpty(questionResult[2]) ? "" : questionResult[2],
string.IsNullOrEmpty(questionResult[1]) ? "" : questionResult[1],
string.IsNullOrEmpty(questionResult[0]) ? "" : questionResult[0],
string.IsNullOrEmpty(questionResult[3]) ? "" : questionResult[3])
);
                            This.StartCoroutine(
                                RestFul.Datas.Setdata(type, mode, "0", string.IsNullOrEmpty(questionResult[2]) ? "" : questionResult[2],
        string.IsNullOrEmpty(questionResult[1]) ? "" : questionResult[1],
        string.IsNullOrEmpty(questionResult[0]) ? "" : questionResult[0],
        string.IsNullOrEmpty(questionResult[3]) ? "" : questionResult[3])
                                );

                            nowTime = Time.realtimeSinceStartup;
                            RestFul.SetConnectState(true);
                            break;
                        }
                        yield return null;
                    }


                }
            }
        }
        #endregion 等待 RestFul.Datas 取得資料

        thanks.SetActive(true);
        Debug.Log("填寫成功!!");
        PlayerPrefs.SetInt("問券填寫", 0);
        RestFul.Datas.RestFulString.Clear();
        RestFul.SetConnectState(false);
    }
    private bool sureInsert = false;
    public void SureInsert() {
        sureInsert = true;
    }
    #endregion

    #region waitScendAndAnimationTime
    [HideInInspector] public float waitScendAndAnimationTime = 0.0f;
    public IEnumerator waitScendAndAnimation()
    {
        while (This.waitScendAndAnimationTime < 40f / 60f)
        {
            This.waitScendAndAnimationTime += Time.unscaledDeltaTime;
            yield return null;
        }
        This.waitScendAndAnimationTime = 0;
    }
    #endregion

    void Update()
    {
        runEditingMode(); runPlayingMode();

    }

    #region UpdateInEditor
    private void runEditingMode()
    {
        if (Application.isPlaying) return;

    }
    #endregion

    #region UpdateInPlayerMode
    private void runPlayingMode()
    {
        if (!Application.isPlaying) return;


    }
    #endregion

    #region Applocation isQuitting Bug Fix
        public bool isquitting = false;
        private void OnDestroy()
        {
            if (isquitting) this.StopAllCoroutines();
        }
        private void OnApplicationQuit()
        {
            isquitting = true;
        }
    #endregion

    bool Shot = false;
    void CaptureScreenshot()
    {
        Shot = true;
        StartCoroutine(waittexGetting());
    }
    IEnumerator waittexGetting()
    {
        tex = null;
        while (!tex) yield return null;
        SaveTextureToFile(tex, "ARTicket" + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".png");
    }
    [HideInInspector]public Texture2D tex;
    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (Shot)
        {
            Shot = false;             
            tex = new Texture2D(source.width, source.height);
            tex.ReadPixels(new Rect(0, 0, source.width, source.height), 0, 0);
            tex.Apply();            
        }

        Graphics.Blit(source, destination);
    }
    public void SaveTextureToFile(Texture2D texture, string filename)
    {
        string path = device() + filename;
        System.IO.Directory.CreateDirectory(device());
        byte[] bytes;
        bytes = texture.EncodeToPNG();
        System.IO.File.WriteAllBytes(path, bytes);
    }
    string device()
    {
        string s = "";
        if(Application.isEditor)
        {
            s = Application.dataPath.Replace("/Assets", "") + "/";
        }
        else
        {
            s = @"/mnt/sdcard/Pictures/Screenshots/";
        }
        return s;
    }
}
