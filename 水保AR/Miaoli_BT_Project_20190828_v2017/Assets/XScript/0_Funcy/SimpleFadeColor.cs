﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleFadeColor : MonoBehaviour {
   public static SimpleFadeColor instance;
    [SerializeField]
    Color 
        color1 = Color.white, 
        color2 = Color.white;
   private void Awake()
   {
      instance = this;
   }
   void Start () {
        nowColor = color1;
    }

    private Color nowColor = Color.white;
	// Update is called once per frame
	void Update ()
    {        
        Color[] colors = new Color[] { color1, color2 };
        nowColor = Color.LerpUnclamped(nowColor, colors[changeColor], Time.unscaledDeltaTime * 5f);
        GetComponent<Renderer>().material.color = nowColor;
    }

   private int _changeColors = 0;
   public int changeColor = 0;    
}
