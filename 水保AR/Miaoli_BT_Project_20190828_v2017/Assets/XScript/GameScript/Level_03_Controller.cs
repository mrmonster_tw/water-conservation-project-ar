﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_03_Controller : MonoBehaviour {
    public Level_Controller lvManager;

    public GameObject[] all_bad_car;
    private bool do_once = true;
    public GameObject win_panel;
    // Use this for initialization
    void Start () {
		
	}
    private bool check(GameObject[] cah)
    {
        for (int i = 0; i < cah.Length; i++)
        {
            if (!cah[i].GetComponent<Point_Controller>().already_did)
            {
                return false;
            }
        }
        return true;
    }
    // Update is called once per frame
    void Update () {
        if (check(all_bad_car) && do_once)
        {
            do_once = false;
            Invoke("WIN", 1);
        }
    }
    void WIN()
    {
        win_panel.GetComponent<Win_Panel_Controller>().Index = 2;
        win_panel.SetActive(true);
        lvManager.setComplete(true);
    }
}
