﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
public class Point_Controller : MonoBehaviour {
	public bool IS_level03;
    public bool already_did = false;
    public UnityEvent Events;
	public TimeGame tg;
    // Use this for initialization
	public GameObject level5Finger;
    void Start () {
        if (Events == null)
            Events = new UnityEvent();
    }
	
	// Update is called once per frame
	void Update () {
		if (tg != null) {
			if (tg.Left_time <= 0.4f) {
				already_did = false;
			}
		}
		if (already_did && level5Finger != null) {
			Destroy (level5Finger);
		}
	}
    private void OnMouseDown()
    {
        if (gameObject.tag == "tag_click")
        {
            gameObject.GetComponent<AudioSource>().Play();
        }
    }
    public void OnMouseUp()
    {
        if (gameObject.tag == "tag_click")
        {
			if(IS_level03)already_did = true;
            Events.Invoke();
        }
    }
    public void close()
    {
        gameObject.SetActive(false);
    }
	public void TurnToTrue(){
		already_did = true;
	}
}
