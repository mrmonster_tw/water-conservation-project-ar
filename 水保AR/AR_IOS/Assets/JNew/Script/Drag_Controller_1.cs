﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using HighlightingSystem;
using UnityEngine.Networking;


public class Drag_Controller_1 : MonoBehaviour {
   public bool LOCKING = false;
   bool complete = false;
    public bool useClick = false;
    public bool dontFixPosition = false;
    public bool already_did = false;
    public UnityEvent Events;
	private Vector3 reset_pos;

    public BoxCollider target;
    private Vector3 screenPoint;
    private Vector3 offset;
	private bool do_Once = true;
	public Highlighter _highlighter;
	public GameObject highlight_ob;

	public SpriteRenderer SR;
	public Sprite[] MySprite;

   public MeshRenderer level4background;
   public Texture[] level4backgroundSpriteArray;

    public GameObject LV_1;
    public GameObject LV_2;

    private void Awake()
    {
//        if (useClick) myCount = int.Parse(gameObject.name.Split('_')[0]) - 1;
    }
    private void Start()
	{ 	reset_pos = gameObject.transform.localPosition;
        if (Events == null)
            Events = new UnityEvent();
        rigi = GetComponent<Rigidbody>();

		ss = highlight_ob.GetComponent<MeshRenderer> ().materials [0].color;
    }
   public Sprite level5TrueImage, level5FalseImage;
	public bool fired;
    private void Update()
    {
		if (fired)
		{
			rotat ();
		}

		if (complete && gameObject.activeInHierarchy)
		{
			already_did = true; 
			gameObject.SetActive(false);
		}

		if (do_Once && sw)
		{
			already_did = true;
			Events.Invoke();
			Show_Glow(false);
			do_Once = false;
		}

        if (LV_1.GetComponent<Level_Controller>().leveling == false && LV_2.GetComponent<Level_Controller>().leveling == false)
        {
            arrow.SetActive(false);
            finger.SetActive(false);
            rigi.useGravity = false;
            rigi.velocity = new Vector3(0, 0, 0);
            gameObject.transform.localPosition = reset_pos;
            mark.SetActive(true);
            Show_Glow(false);
            fired = false;
            transform.rotation = new Quaternion(0, 0, 0, 0);
            gameObject.SetActive(false);
            hintshow();
        }
    }
   
    public bool sw = false;
	public GameObject arrow;

    private void OnMouseDown()
    {
        if (fired == false)
        {
            Debug.Log("Down");
            finger.SetActive(true);
            arrow.SetActive(true);
            Show_Glow(true);
            reset_pos = gameObject.transform.localPosition;
            screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
            offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
        }
    }
   bool picking = false;
	public float drag_dis;
	private Vector3 cursorPosition;
    
    private void OnMouseDrag()
    {
        if (fired == false)
        {
            Vector3 cursorPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
            cursorPosition = Camera.main.ScreenToWorldPoint(cursorPoint) + offset;
            transform.position = cursorPosition;
            if (transform.localPosition.y > -67f)
            {
                transform.localPosition = new Vector3(transform.localPosition.x, -67f, transform.localPosition.z);
            }
            if (transform.localPosition.y <= -67f)
            {
                transform.localPosition = new Vector3(0, transform.localPosition.y, transform.localPosition.z);
            }
        }

//		if (useClick) return;
//		Vector3 cursorPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
//		Vector3 cursorPosition = Camera.main.ScreenToWorldPoint(cursorPoint) + offset;
//		transform.position = cursorPosition;
//		if (!dontFixPosition)
//			transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z);
    }

	public float dis;
	Rigidbody rigi;
	public GameObject finger;

    private void OnMouseUp()
    {
        if (fired == false)
        {
            collidertr(true);
            Debug.Log("Up");
            if (transform.localPosition.y <= -67f)
            {
                finger.SetActive(false);
                arrow.SetActive(false);
                dis = transform.localPosition.y - (-67f);
                rigi = GetComponent<Rigidbody>();
                rigi.useGravity = true;
                rigi.AddRelativeForce(Vector3.up * Mathf.Abs(dis) * 200f);
                rigi.AddRelativeForce(Vector3.forward * Mathf.Abs(dis) * 200f);
                StartCoroutine(WaitAndsetfalse(3f));
                fired = true;
            }
        }
    }

	public GameObject mark;
	private IEnumerator WaitAndsetfalse(float waitTime)
	{
		yield return new WaitForSeconds (waitTime);
		gameObject.transform.localPosition = reset_pos;
		rigi.useGravity = false;
		mark.SetActive (true);
		rigi.velocity = new Vector3(0,0,0);
		Show_Glow(false);
		fired = false;
		transform.rotation = new Quaternion (0,0,0,0);
		gameObject.SetActive(false);
		hintshow ();

    }

    public void close()
    {
        if(gameObject.name == "03_Soultion")
        {
            SimpleFadeColor.instance.changeColor++;
        }
        gameObject.SetActive(false);
    }
    
    private void OnTriggerEnter(Collider other)
    {
		if (other == target && fired)
        {
			hintshow ();
            sw = true;
			if(SR!=null)SR.sprite = MySprite[1];
        }
    }
	private void OnTriggerExit(Collider other)
	{
		if (other == target)
		{
			sw = false;
            if (SR != null) SR.sprite = MySprite[0];            
        }
	}
	Color ss;
    private void Show_Glow(bool sw)
    {
		
		if (sw) 
		{
			StartCoroutine (hh_fh());
		}
		else if (!sw) 
		{
			highlight_ob.GetComponent<MeshRenderer> ().materials [0].color = new Color (ss.r, ss.g, ss.b);
		}


        //if (_highlighter == null)
        //    return;
        //else
        //{
            //必須為3D物件才有光暈
        //    if (sw)
        //        _highlighter.ConstantOnImmediate(Color.yellow);
        //    else if (!sw)
        //        _highlighter.Off();
        //}
    }
	private IEnumerator hh_fh()
	{
		highlight_ob.GetComponent<MeshRenderer> ().materials [0].color = new Color (1f, ss.g, ss.b);
		yield return new WaitForSeconds (0.5f);
		highlight_ob.GetComponent<MeshRenderer> ().materials [0].color = new Color (ss.r, ss.g, ss.b);
		yield return new WaitForSeconds (0.5f);
		StartCoroutine (hh_fh());
	}

	public void ResetDrag(){

		if(SR!=null)SR.sprite = MySprite[0];
		gameObject.transform.localPosition = reset_pos;
		sw = false;
		do_Once = true;
	}
	private void rotat()
	{
		transform.Rotate(Vector3.right*50f,Space.Self);
	}
	public GameObject s_1;
	public GameObject s_2;
	public GameObject s_3;
	public GameObject s_4;
	public GameObject s_5;
	public GameObject s_6;
	public GameObject s_7;
	public GameObject s_8;

	public void collidertr(bool sw)
	{
		s_1.GetComponent<BoxCollider> ().enabled = sw;
		s_2.GetComponent<BoxCollider> ().enabled = sw;
		s_3.GetComponent<BoxCollider> ().enabled = sw;
		s_4.GetComponent<BoxCollider> ().enabled = sw;
		s_5.GetComponent<BoxCollider> ().enabled = sw;
		s_6.GetComponent<BoxCollider> ().enabled = sw;
		s_7.GetComponent<BoxCollider> ().enabled = sw;
		s_8.GetComponent<BoxCollider> ().enabled = sw;
	}
	public GameObject hint;
	public void hintshow()
	{
		hint.SetActive (true);
	}
}
