﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_05_Controller : MonoBehaviour {
    public Level_Controller lvManager;

    public ParticleSystem completeParticle;

    public GameObject[] all_image;
	public bool do_once = true;
    public GameObject win_panel;
    // Use this for initialization
    void Start()
    {

    }
    private bool check(GameObject[] cah)
    {
        for (int i = 0; i < cah.Length; i++)
        {
            if (!cah[i].GetComponent<Point_Controller>().already_did)
            {
                return false;
            }
        }
        return true;
    }
    // Update is called once per frame
    void Update()
    {
        if (check(all_image) && do_once)
        {
            do_once = false;
            StartCoroutine(doWinEffect());
        }
    }
    IEnumerator doWinEffect()
    {
        yield return new WaitForSeconds(1.0f);
        Destroy(completeParticle.gameObject);
        Invoke("WIN", 0);
    }
    void WIN()
    {
        win_panel.GetComponent<Win_Panel_Controller>().Index = 4;
        win_panel.SetActive(true);
        lvManager.setComplete(true);
    }
}
