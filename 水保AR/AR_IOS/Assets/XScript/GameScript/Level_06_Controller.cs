﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Level_06_Controller : MonoBehaviour {

    
public GameObject[] star_Images; public GameObject Parent_obj; public GameObject Shinning; public float Shinning_speed = 50;
	public Image black;
	private AsyncOperation op;
    // Use this for initialization
    void Start () {
		op =  Application.LoadLevelAsync ("3_Booking");
		op.allowSceneActivation = false;
	}
    void OnEnable()
    {
        Parent_obj.transform.localScale = Vector3.zero;
        foreach (GameObject i in star_Images)
        {
            i.transform.localScale = Vector3.zero;
        }
        StartCoroutine(StartImage());
    }
    IEnumerator StartImage()
    {
        //yield return new WaitForSeconds(0);
        iTween.ScaleTo(Parent_obj, iTween.Hash("scale", new Vector3(0.6f, 0.6f, 0.6f), "time", 0.75f, "easeType", iTween.EaseType.easeOutBounce));
        yield return new WaitForSeconds(1);
        for (int i = 0; i < star_Images.Length; i++)
        {

            iTween.ScaleTo(star_Images[i], iTween.Hash("scale", Vector3.one, "time", 0.5f, "easeType", iTween.EaseType.easeOutBack));
            yield return new WaitForSeconds(0.5f);
        }
    }
    // Update is called once per frame
    void Update () {
        Shinning.transform.Rotate(0, 0, Shinning_speed * Time.deltaTime);
    }
	public void GoToBooking(){
		
		black.gameObject.SetActive (true);
		StartCoroutine(FadeToBooking());
	}
	public IEnumerator FadeToBooking(){
		yield return new WaitForSeconds (0.5f);
		op.allowSceneActivation = true;  
		yield break;
	}
}
