﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Level_03_Controller_ICON : MonoBehaviour {



    public Image[] cars;
	// Use this for initialization
	void Start () {


        foreach (Image i in cars)
        {
            i.enabled = false;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void Close()
    {
        gameObject.SetActive(false);
    }
    public void Open()
    {
        gameObject.SetActive(true);
    }
    public void able_Image(int num)
    {
        cars[num].enabled = true;
    }
}
