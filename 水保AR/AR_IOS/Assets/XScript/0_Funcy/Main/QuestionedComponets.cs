﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class QuestionedComponets : MonoBehaviour {
    public ExpiredComponets ex;
    public Imgs imgs;
    [Serializable]public struct Imgs
    {
        public Sprite[] BackgroundImgArray_1;
        public Sprite[] BackgroundImgArray_2;
    }        
    public Questions questions;
    [Serializable]public struct Questions
    {
        public RectTransform satisfaction, Sex_chose, Age_b, Opinion;

    }
    private void OnEnable()
    {
        if (string.IsNullOrEmpty(PlayerSelectInfo))
            PlayerSelectInfo = "未填寫,未填寫,未填寫,未填寫";
    }
    public string PlayerSelectInfo = "";
    private string[] resultsaver = new string[4];
    public void updatePlayerSelectInfo(GameObject me)
    {
        
        if      (me.name.Contains("顆星"))    //星等
        {
            resultsaver[0] = me.name.Substring(0, 1);
        }
        else if (me.name.Contains("man"))    //性別
        {
            resultsaver[1] = me.name.Substring(me.name.Length - 1);
            resultsaver[1] = (resultsaver[1] == "女") ? "F" : "M";
        }
        else if (me.name.Contains("歲" ))    //年齡範圍
        {
            resultsaver[2] = me.name;
        }
        if (me.GetComponent<InputField>())
            resultsaver[3] = me.GetComponent<InputField>().text;
        resultsaver[3] = (string.IsNullOrEmpty(resultsaver[3])) ? "未填寫" : resultsaver[3];

        string result = compareResultsaver(resultsaver);
        PlayerSelectInfo = result;
    }
    string compareResultsaver(string[] ss)
    {
        string tmp = "";
        for (int i = 0; i < ss.Length; i++) tmp += ((string.IsNullOrEmpty(ss[i])) ? "未填寫" : ss[i]) + ((i == ss.Length - 1) ? "" : ",");
        return tmp;
    }
    public void reloadScene(UnityEngine.Object scene)
    {
        try
        {
            SceneManager.LoadScene(scene.name);
        }
        catch
        {
            Debug.LogError("The Object Type is not Scene.");
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
