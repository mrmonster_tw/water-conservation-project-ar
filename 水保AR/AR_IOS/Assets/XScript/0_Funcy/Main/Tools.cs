﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[ExecuteInEditMode]
public class Tools : MonoBehaviour {
    public static Tools This;
    public bool resetMemory = false;
    private void Awake()
    {
        This = this;
    }
    // Use this for initialization
    void Start () {
        if (MainMenu_Controller.inputCount > 4) { 
            MainMenu_Controller.inputCount = 0;
            SceneManager.LoadScene(0);
        }

    }
	
	// Update is called once per frame
	void Update () {
		if(resetMemory)
        {
            resetMemory = false;ResetMemory();
        }
	}
    
    public void ResetMemory()
    {
        PlayerPrefs.DeleteAll();
        Debug.Log("Memory Reseted");
    }
    public void ReloadScene(int id)
    {
        SceneManager.LoadScene(id);
    }
}
