﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SchoolToWordPos : MonoBehaviour {
    [SerializeField]
    Transform SchoolFwTarget;
    public float BaseScale = 50f;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector2 screenPos = Camera.main.WorldToScreenPoint(SchoolFwTarget.position);
        transform.position = screenPos;

        float dis = Vector3.Distance(SchoolFwTarget.position, Camera.main.transform.position);
        transform.localScale = Vector3.one * (1 / dis) * BaseScale;

    }

}
