﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu_Controller : GetTickets
{
    public bool NetworkingCheck = false;
	public GameObject[] BeforeStart;
    public float persent;
	
    // private RectTransform bar_rt;

    Image Image_Bar_front; Text Text_Bar_Persent;
    TweenScale Image_BFS;
    // Use this for initialization
    void Awake()
    {
		PlayerUUID = SystemInfo.deviceUniqueIdentifier;
        Image_Bar_front  = Array.Find(FindObjectsOfType<Image>(), im => im.name == "Image_Bar_front");
        Text_Bar_Persent = Array.Find(FindObjectsOfType<Text >(), tx => tx.name == "Text_Bar_Persent");
        Image_BFS = Array.Find(FindObjectsOfType<TweenScale>(), tx => tx.name == "Image_BFS");
    }
    void Start()
    {
        if (PlayerPrefs.GetInt("尚未重製") == 0)
            StartCoroutine(Mstart());
        else
            SceneManager.LoadScene("3_Booking");
    }

    const int
        Main      = 0,
        GameScene = 1,
        Booking   = 2;

    public static int inputCount = 0;
    public IEnumerator Mstart(){
        yield return null;
        StartCoroutine(Loading());
    }

    private bool userClick = false;

   IEnumerator Loading()
   {
      if (!Application.isPlaying) yield break;
      Scene unLoadScene = SceneManager.GetActiveScene();
      print(unLoadScene.name);

      AsyncOperation LoadTarget = SceneManager.LoadSceneAsync(GameScene, LoadSceneMode.Additive);
      Image 請稍後bg = FindObjectsOfType<Image>().ToList().Find(b => b.gameObject.name == "請稍後bg");


      LoadTarget.allowSceneActivation = false;
      while (true)
      {
         Image_Bar_front.fillAmount = Mathf.LerpUnclamped(Image_Bar_front.fillAmount, (LoadTarget.progress + 0.1f), Time.fixedDeltaTime);
         Text_Bar_Persent.text = (Image_Bar_front.fillAmount * 100).ToString("0.0") + "%";
         if (Text_Bar_Persent.text == "100.0%") break;
         yield return new WaitForEndOfFrame();
      }
      Image_BFS.enabled = true;
      while (請稍後bg.color.a < 0.65f)
      {
         請稍後bg.color = Color.Lerp(請稍後bg.color, new Color(請稍後bg.color.r, 請稍後bg.color.g, 請稍後bg.color.b, 0.66f), 0.1f);
         yield return null;
      }
      yield return new WaitForSeconds(1.0f);
      float time = Time.realtimeSinceStartup;
      FindObjectsOfType<Text>().ToList().Find(t => t.gameObject.name == "請稍後").enabled = true;
      while (Time.realtimeSinceStartup - time < 4.0f)
         yield return null;


      foreach (GameObject go in FindObjectsOfType<GameObject>())
         Destroy(go);

      LoadTarget.allowSceneActivation = true;
   }
    // Update is called once per frame
    void Update()
    {
       
		
    }
    public void PointDown()
    {
        inputCount++;print(inputCount);
    }
	void ShowTips(){
		BeforeStart[1].SetActive (true);
		BeforeStart[0].SetActive (true);
	}
    public void Jump_level()
    {
        userClick = true;
    }    
}
