﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Play_Level_Music : MonoBehaviour {

    public AudioClip[] audio;
    // Use this for initialization
    void Start() {

    }

    public void stop_music()
    {
        gameObject.GetComponent<AudioSource>().Stop();
    }
    public void play_music(int num)
    {
        gameObject.GetComponent<AudioSource>().Stop();
        gameObject.GetComponent<AudioSource>().clip = audio[num];
        gameObject.GetComponent<AudioSource>().Play();
    }
	// Update is called once per frame
	void Update () {
		
	}
}
