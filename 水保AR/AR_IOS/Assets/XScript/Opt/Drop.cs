﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drop : MonoBehaviour
{
    public GameObject Image;
    public GameObject Cube;
    Vector3 Position;
    Camera ThisCamera;
    public LayerMask Targetlayer;
    // Use this for initialization
    void Start()
    {
        ThisCamera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Position = Input.mousePosition;

            Ray ray = ThisCamera.ScreenPointToRay(Position);
            RaycastHit rayhit;
            Physics.Raycast(ray, out rayhit, Mathf.Infinity, Targetlayer);
            Vector3 cameraPos = ThisCamera.transform.position;
            Vector3 mousePos = new Vector3(rayhit.point.x - cameraPos.x, rayhit.point.y - cameraPos.y, rayhit.point.z - cameraPos.z);
            Debug.DrawRay(ThisCamera.transform.position, mousePos, Color.red);

            if (Physics.Raycast(ray, out rayhit, Mathf.Infinity, Targetlayer))
            {
                if (rayhit.collider.gameObject == Cube)
                {
                    Cube.SetActive(false);
                    Image.SetActive(true);

                    
                }
            }
            if (Image.activeSelf)
            {
                Image.GetComponent<RectTransform>().anchoredPosition = Input.mousePosition;
            }
        }
    }
}
