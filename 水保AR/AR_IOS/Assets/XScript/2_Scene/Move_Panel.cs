﻿using UnityEngine;
using System.Collections;

public class Move_Panel : MonoBehaviour {

    public Transform pos_show;//出現位置
    public Transform pos_unshow;//消失位置
    public float _Time;//花費時間

    private void Start()
    {
        gameObject.transform.position = pos_unshow.position;
        MoveUp();
    }
    public void MoveUp()
    {
            iTween.MoveTo(gameObject, iTween.Hash("position", pos_show.position, "time", _Time, "easeType", iTween.EaseType.easeOutExpo));
    }

    public void MoveDown()
    {
            iTween.MoveTo(gameObject, iTween.Hash("position", pos_unshow.position, "time", _Time, "easeType", iTween.EaseType.easeOutExpo));
    }

}
