﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Close_Object : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Close_All();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OpenURL(string url)
    {
        Application.OpenURL(url);
    }

    public void Close_All()
    {
        gameObject.SetActive(false);
        Reset.ResetText.ResetAllText(gameObject);
    }
	public void QuitAPP(){
		Application.Quit ();
	}

}
public class Reset : Close_Object
{
    public struct ResetText
    {
        public static void ResetAllText(GameObject g) { foreach (Text t in g.GetComponentsInChildren<Text>())t.text = ""; }
    }
}
