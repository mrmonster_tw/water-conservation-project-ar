﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow_Script : MonoBehaviour {
    public float speed = 50;
    private Vector3 org;
	// Use this for initialization
	void Start () {
        org = gameObject.transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
        gameObject.transform.localPosition = new Vector3(
            gameObject.transform.localPosition.x,
            gameObject.transform.localPosition.y - speed * Time.deltaTime,
            gameObject.transform.localPosition.z);


        if (gameObject.transform.localPosition.y < org.y - 30)
            gameObject.transform.localPosition = org;
    }
}
