﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scroller_Image_Controller : MonoBehaviour {

    public GameObject Panel_True;
    public GameObject Panel_False;
	// Use this for initialization
	void Start () {
		
	}

    public void open_Panel_01()
    {
        if (GameObject.Find("ApplicationGameMaker").GetComponent<GameManager>().L01_workDown)
        {
            Panel_True.SetActive(true);
        }
        else
        {
            Panel_False.SetActive(true);
        }
    }
    public void open_Panel_02()
    {
        if (GameObject.Find("ApplicationGameMaker").GetComponent<GameManager>().L02_workDown)
        {
            Panel_True.SetActive(true);
        }
        else
        {
            Panel_False.SetActive(true);
        }
    }
    public void open_Panel_03()
    {
        if (GameObject.Find("ApplicationGameMaker").GetComponent<GameManager>().L03_workDown)
        {
            Panel_True.SetActive(true);
        }
        else
        {
            Panel_False.SetActive(true);
        }
    }
    public void open_Panel_04()
    {
        if (GameObject.Find("ApplicationGameMaker").GetComponent<GameManager>().L04_workDown)
        {
            Panel_True.SetActive(true);
        }
        else
        {
            Panel_False.SetActive(true);
        }
    }
    public void open_Panel_05()
    {
        if (GameObject.Find("ApplicationGameMaker").GetComponent<GameManager>().L05_workDown)
        {
            Panel_True.SetActive(true);
        }
        else
        {
            Panel_False.SetActive(true);
        }
    }
}
