﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetOnEnable : MonoBehaviour {
    public Object setObj;
    private void OnEnable()
    {
        if (((GameObject)setObj).GetComponent<TimeLevel4>()) 
        {
            TimeLevel4 t4 = ((GameObject)setObj).GetComponent<TimeLevel4>();
            t4.Left_time = 60;
            t4.timeText.text = "60";
            t4.im.fillAmount = 1;
        }
        if (((GameObject)setObj).GetComponent <TimeGame>())
        {
            TimeGame Tg = ((GameObject)setObj).GetComponent<TimeGame>();
            Tg.Left_time = 30;
            Tg.timeText.text = "30";
            Tg.im.fillAmount = 1;
        }
    }
}
