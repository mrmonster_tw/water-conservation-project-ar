﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class TimeLevel4 : MonoBehaviour {
	
	public float Left_time = 60;
	public float Left_timeMax = 60;
	public GameObject checkObj;
	public Image im;
	public Text timeText;
	public GameObject Clock;

	public GameObject[] OpenIt;

    public Sprite[] boxCut;
    public SpriteRenderer[] boxAll;
    public Drag_Controller[] DC_all;

	public UnityEvent Events;
	// Use this for initialization
	public GameObject ReplayTips;
	private bool once;
    private Level_Controller Level_04;
    private void Awake()
    {
        Level_04 = System.Array.Find(FindObjectsOfType<Level_Controller>(), l => l.name == "Level_04");
    }
    void Start () {
		
	}
    private void OnEnable()
    {

        boxAll[0].sprite = boxCut[1];
    }
    public void OnClickTrue()
    {

    }
    void Update () {

        if (!Level_04) return;
        if (!Level_04.isPostLevel) return;


        if (!this.gameObject.GetComponent<Level_04_Controller> ().do_once) {
			Destroy (this);
			Destroy(Clock);
		}
		if (checkObj.gameObject.activeSelf&&!this.gameObject.GetComponent<Level_04_Controller> ().win_panel.activeSelf) {

			im.fillAmount = Left_time / Left_timeMax;
			if (Left_time > 0.4f) {
				Left_time -= Time.unscaledDeltaTime;
				string tempt;
				tempt = Left_time.ToString("##");
				if(Left_time<9.5f){
					timeText.text = "0" + Left_time.ToString("##");
				}else{
					timeText.text = tempt ;
				}
			}else if (Left_time < 0.4f) {
            if (ReplayTips.activeInHierarchy)
            timeText.text = "00";
            ReplayTips.SetActive(true);
            
            //TimesUp ();
         }
		}
      SetAllLocking(ReplayTips.activeInHierarchy);
   }
   public IEnumerator Reset(){
      yield return new WaitForSeconds (0.1f);
		Left_time = Left_timeMax;
		ReplayTips.SetActive (false);
		yield break;
	}
	public void TimesUp(){
		Left_time = 0;

      
      timeText.text = Left_time.ToString();
		Events.Invoke ();

		StartCoroutine (Reset ());
	}
	public void Restlevel(){
		foreach(Drag_Controller i in DC_all){	
			i.already_did = false;
			i.ResetDrag ();
		
		}
	}
   public void SetAllLocking(bool locking)
   {
      foreach (var dg in GetComponentsInChildren<Drag_Controller>())
         dg.LOCKING = locking;
   }
}
