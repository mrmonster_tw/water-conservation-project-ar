﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class leve4To2D : MonoBehaviour {
	public bool NowChange;
	public GameObject bloodUI;
	 public SpriteRenderer SR;
	public GameObject MC;
	public bool once;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void FixedUpdate () {
		if (!this.gameObject.GetComponent<Point_Controller> ().already_did) {
			if (NowChange) {
				bloodUI.SetActive (true);
				if (GeometryUtility.TestPlanesAABB (GeometryUtility.CalculateFrustumPlanes (MC.GetComponent<Camera> ()), this.gameObject.GetComponent<Collider> ().bounds)) {
					if (!once) {
						once = true;
						Vector3 pt = Camera.main.WorldToScreenPoint (new Vector3 (this.transform.position.x,
							            this.transform.position.y, this.transform.position.z));
						bloodUI.transform.position = new Vector3 (pt.x, pt.y, 1);
					} else {
						bloodUI.transform.position = Input.mousePosition;
					}
				}
			} else {
				bloodUI.SetActive (false);
			}
		} else {
			SR.enabled = false;
			bloodUI.SetActive (false);
		}
	}
	public void OnMouseUp(){
		if (!this.gameObject.GetComponent<Point_Controller> ().already_did) {
			NowChange = false;
			SR.enabled = true;
			once = false;
		}
	}
	void OnMouseDown(){
		if (!this.gameObject.GetComponent<Point_Controller> ().already_did) {
			NowChange = true;
			SR.enabled = false;
		}

	}
	public void OnceBack(){
		once = false;

	}
}
