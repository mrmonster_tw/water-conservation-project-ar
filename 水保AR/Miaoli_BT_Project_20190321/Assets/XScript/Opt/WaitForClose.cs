﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitForClose : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	public void GoWait(){
		StartCoroutine (waitClose ());
	}
	public IEnumerator waitClose(){
		yield return new WaitForSecondsRealtime (1.5f);
		this.gameObject.SetActive (false);
		yield break;

	}
	// Update is called once per frame
	void Update () {
		
	}
}
