﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_04_Controller : MonoBehaviour {
    public Level_Controller lvManager;

    public GameObject[] all_word;
    public GameObject[] all_line;
    // Use this for initialization
	public bool do_once = true;
    public GameObject win_panel;

    // Update is called once per frame
    void Update()
    {
        if (check(all_word) && do_once)
        {
            do_once = false;

            Invoke("WIN", 1);
        }
    }
    void WIN()
    {
        win_panel.GetComponent<Win_Panel_Controller>().Index = 3;
        win_panel.SetActive(true);
        lvManager.setComplete(true);
    }

    private bool check(GameObject[] cah)
    {
        for (int i = 0; i < cah.Length; i++)
        {
            if (!cah[i].GetComponent<Drag_Controller>().already_did)
            {
                return false;
            }
        }
        return true;
    }


    public void close_word_and_line(int num)
    {
        all_word[num].SetActive(false);
        all_line[num].SetActive(false);
    }
	public void open_word_and_line(int num){
		all_word[num].SetActive(true);
		all_line[num].SetActive(true);

	}
}
