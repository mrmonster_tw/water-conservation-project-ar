﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_01_Controller : MonoBehaviour {
    public Level_Controller lvManager;

    public GameObject[] all_unShow_Objs;
    public GameObject[] all_Objs;

    public GameObject[] chapter_01;
    public GameObject[] chapter_02;
	public GameObject[] LV1tips;
    public GameObject win_panel;

	public GameObject good_road;
	public GameObject good_water;

	public GameObject Evaporation;
	public GameObject Evaportranspiration;
	public GameObject Infiltration;

	public GameObject tip_1;
	public GameObject tip_2;
    // Use this for initialization
    void Start () {
        foreach (GameObject i in all_unShow_Objs)
        {
            i.SetActive(false);
        }
	}
    private bool do_once = true;
    private bool do_once2 = true;
    // Update is called once per frame
    void Update () 
	{
		if (good_road.activeSelf && good_water.activeSelf && do_once)
		{
			do_once = false;

			Invoke("obj_show", 0.5f);
		}
		if (Evaporation.activeSelf && Evaportranspiration.activeSelf && Infiltration.activeSelf && do_once2)
		{
			do_once2 = false;
			Invoke("WIN", 1);
			Destroy (tip_2);
		}

//        if (check(chapter_01) && do_once)
//        {
//            do_once = false;
//
//            Invoke("obj_show", 0.5f);
//
//        }
//        if (check(chapter_02) && do_once2)
//        {
//            do_once2 = false;
//            Invoke("WIN", 1);
//
//        }

    }
    void obj_show()
    {
		Destroy (tip_1);
        all_Objs[5].SetActive(true);
        all_Objs[6].SetActive(true);
        all_Objs[10].SetActive(true);
        all_Objs[11].SetActive(true);
        all_Objs[12].SetActive(true);
		all_Objs[13].SetActive(true);
		LV1tips [0].SetActive (false);
		LV1tips [1].SetActive (true);
//		LV1tips [2].SetActive (false);
//		LV1tips [3].SetActive (true);
    }
    void WIN()
    {
        win_panel.GetComponent<Win_Panel_Controller>().Index = 0;
        win_panel.SetActive(true);
        lvManager.setComplete(true);
    }
//    private bool check(GameObject[] cah)
//    {
//        for (int i = 0; i < cah.Length; i++)
//        {
//            if (!cah[i].GetComponent<Drag_Controller>().already_did)
//            {
//                return false;
//            }
//        }
//        return true;
//    }
    
    public void Open_Object(int index)
    {
        all_Objs[index].SetActive(true);
    }
    public void Close_Object(int index)
    {
        all_Objs[index].SetActive(false);
    }
}
