﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Level_05_Controller_ICON : MonoBehaviour {
    public Image[] image;
    // Use this for initialization
    void Start()
    {


        foreach (Image i in image)
        {
            i.enabled = false;
        }
    }
    // Update is called once per frame
    void Update () {
		
	}
    public void Close()
    {
        gameObject.SetActive(false);
    }
    public void Open()
    {
        gameObject.SetActive(true);
    }
    public void able_Image(int num)
    {
        image[num].enabled = true;
    }
	public void fail_Image(){

		foreach (Image i in image)
		{
			i.enabled = false;
		}

	}
}
