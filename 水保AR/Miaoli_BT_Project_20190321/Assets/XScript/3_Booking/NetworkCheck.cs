﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class NetworkCheck : MonoBehaviour {
    public UnityEvent Offline__Event;
    public UnityEvent Online_Event;
    // Use this for initialization

    static public bool is_connet = false;
    public void Start()
    {
		StartCoroutine (CheckNetPerSecond ());
        
    }
	IEnumerator CheckNetPerSecond(){
		Check_Network();
		yield return new WaitForSeconds(2.0f);
		StartCoroutine (CheckNetPerSecond ());
		yield break;
	}
    public void Check_Network()
	{	if (Application.internetReachability == NetworkReachability.NotReachable)
		{
			is_connet = false;
			Offline__Event.Invoke();
			Debug.Log("Error. Check internet connection!");
		}
		else {
			is_connet = true;
			Online_Event.Invoke();
			//Debug.Log("OK,You are Online!");
		}
       
           
    }
}
