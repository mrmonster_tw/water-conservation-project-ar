﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using HighlightingSystem;
using UnityEngine.Networking;
public class Drag_Controller : MonoBehaviour {
   public bool LOCKING = false;
   bool complete = false;
    public bool useClick = false;
    public bool dontFixPosition = false;
	public GameObject NeedToDestroy;
    public bool already_did = false;
    public UnityEvent Events;
	private Vector3 reset_pos;

    public BoxCollider target;
    private Vector3 screenPoint;
    private Vector3 offset;
	private bool do_Once = true;
    public Highlighter _highlighter;

	public SpriteRenderer SR;
	public Sprite[] MySprite;

   public MeshRenderer level4background;
   public Texture[] level4backgroundSpriteArray;
    private void Awake()
    {
        if (useClick) myCount = int.Parse(gameObject.name.Split('_')[0]) - 1;
    }
    private void Start()
	{ 	reset_pos = gameObject.transform.localPosition;
        if (Events == null)
            Events = new UnityEvent();
    }
   public Sprite level5TrueImage, level5FalseImage;
    private void Update()
    {
      if (complete && gameObject.activeInHierarchy)
      { already_did = true; gameObject.SetActive(false); }
    }
   
    public bool sw = false;
    public int myCount = 0;
    public static int nowPicCount = 0;
    private void OnMouseDown()
    {
      if (LOCKING) return;
        if (useClick && !picking)
        {
            if(myCount == nowPicCount)
            {
                StartCoroutine(點對了());
            }
            else
            {
                StartCoroutine(點錯了());
            }
            return;
        }
        
        Show_Glow(true);
		reset_pos = gameObject.transform.localPosition;
        screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
    }
   bool picking = false;
   IEnumerator 點對了()
   {
      picking = true;
      GetComponentInChildren<SpriteRenderer>().sprite = level5TrueImage;      
      FindObjectsOfType<UnityEngine.UI.Image>().ToList().Find(t => t.gameObject.name == "厲害").enabled = true;
      yield return new WaitForSeconds(1.0f);
      FindObjectsOfType<UnityEngine.UI.Image>().ToList().Find(t => t.gameObject.name == "厲害").enabled = false;
      sw = true;
      OnMouseUp();
      nowPicCount++;
      try
      {
         FindObjectsOfType<Drag_Controller>().ToList().Find(d => d.myCount == nowPicCount).SR.sprite = MySprite[1];
         level4background.material.mainTexture = level4backgroundSpriteArray[nowPicCount];

      }
      catch
      { }
      complete = true;
      picking = false ;
   }
    IEnumerator 點錯了()
    {
      picking = true;
      Sprite s = GetComponentInChildren<SpriteRenderer>().sprite;
      GetComponentInChildren<SpriteRenderer>().sprite = level5FalseImage;      
      FindObjectsOfType<UnityEngine.UI.Image>().ToList().Find(t => t.gameObject.name == "喔喔，你填錯了，再想想吧").enabled = true;
        yield return new WaitForSeconds(1.0f);
        FindObjectsOfType<UnityEngine.UI.Image>().ToList().Find(t => t.gameObject.name == "喔喔，你填錯了，再想想吧").enabled = false;
      GetComponentInChildren<SpriteRenderer>().sprite = s;
      picking = false;
   }

    private void OnMouseDrag()
    {
        if (useClick) return;
        Vector3 cursorPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        Vector3 cursorPosition = Camera.main.ScreenToWorldPoint(cursorPoint) + offset;
        transform.position = cursorPosition;
        if (!dontFixPosition)
            transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -1);
    }
	
    private void OnMouseUp()
    {
        Show_Glow(false);
        if (do_Once && sw)
        {
            already_did = true;
            do_Once = false;
            Events.Invoke();
            
            if (NeedToDestroy != null)
				NeedToDestroy.SetActive (false);
            
        }
        else
        {
			gameObject.transform.localPosition = reset_pos;
            
        }
    }
    
    public void close()
    {
        if(gameObject.name == "03_Soultion")
        {
            SimpleFadeColor.instance.changeColor++;
        }
        gameObject.SetActive(false);
    }
    
    private void OnTriggerStay(Collider other)
    {
        if (other == target)
        {
            sw = true;
			if(SR!=null)SR.sprite = MySprite[1];
        }
    }
	private void OnTriggerExit(Collider other)
	{
		if (other == target)
		{
			sw = false;
            if (SR != null) SR.sprite = MySprite[0];            
        }
	}
    private void Show_Glow(bool sw)
    {
        if (_highlighter == null)
            return;
        else
        {
            //必須為3D物件才有光暈
            if (sw)
                _highlighter.ConstantOnImmediate(Color.yellow);
            else if (!sw)
                _highlighter.Off();
        }
    }
	public void ResetDrag(){

		if(SR!=null)SR.sprite = MySprite[0];
		gameObject.transform.localPosition = reset_pos;
		sw = false;
		do_Once = true;
	}
   private void OnDisable()
   {
      //if (useClick) Destroy(gameObject);
   }
}
