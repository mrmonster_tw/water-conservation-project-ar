﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SendCompleteInfo : MonoBehaviour {
    public UnityEngine.UI.Image Info;
    public bool complete = false;private bool checkimg = false;
    private Level_Controller MyCheckController;
    private void Awake()
    {
        string mylevel = gameObject.name.Split('_')[1];
        MyCheckController = Array.Find(FindObjectsOfType<Level_Controller>(), lvc => lvc.gameObject.name.Contains(mylevel));
    }
    private void OnEnable()
    {
        complete = MyCheckController.completed;

        if (complete != checkimg)
        {
            checkimg = complete;
            ObjectPuter com = GetComponent<ObjectPuter>();

            Texture2D tex = com.putObjects[0] as Texture2D;
            Sprite sp = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.zero);

            GetComponent<UnityEngine.UI.Image>().sprite = sp;
        }


    }
    public void Sendcomplete()
    {
        ObjectPuter puter = Info.GetComponent<ObjectPuter>();
        Sprite[] completeInfoSprite = new Sprite[2];
        for (int i = 0; i < puter.putObjects.Count; i++)
        {
            Texture2D tex = puter.putObjects[i] as Texture2D;
            completeInfoSprite[i] = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.zero);
        }            

        Info.sprite = (complete) ? completeInfoSprite[1] : completeInfoSprite[0];
    }

}
