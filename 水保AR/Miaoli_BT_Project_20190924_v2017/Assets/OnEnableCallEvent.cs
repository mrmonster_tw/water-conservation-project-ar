﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class OnEnableCallEvent : MonoBehaviour {
   public  Button b;

   bool enb = false;
   private void Start()
   {
      b= FindObjectsOfType<UnityEngine.UI.Button>().ToList().Find(b => b.gameObject.name ==
 "bt_schedule"
);
   }
   void Update()
   {
      if(GetComponent<Text>().enabled!= enb)
      {
         enb = GetComponent<Text>().enabled;
         if (enb)
            b.onClick.Invoke();
      }
   }
}
