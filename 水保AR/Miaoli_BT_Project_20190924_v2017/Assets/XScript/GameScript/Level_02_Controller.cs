﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_02_Controller : MonoBehaviour {
    public Level_Controller lvManager;

    public GameObject[] all_unShow_Objs;
    public GameObject[] all_Objs;
    public GameObject[] chapter_01;
    public GameObject win_panel;

	public GameObject good_1;
	public GameObject good_2;
	public GameObject good_3;

	public GameObject tip_1;
    // Use this for initialization
    void Start () {
        foreach (GameObject i in all_unShow_Objs)
        {
            i.SetActive(false);
        }
    }
    private bool do_once = true;
    // Update is called once per frame
    void Update () 
	{
		if (good_1.activeSelf && good_2.activeSelf && good_3.activeSelf && do_once)
		{
			Destroy (tip_1);
			do_once = false;
			Invoke("WIN", 1);
		}
//        if (check(chapter_01) && do_once)
//        {
//            do_once = false;
//
//            Invoke("WIN", 1);
//        }
    }
    void WIN()
    {
        win_panel.GetComponent<Win_Panel_Controller>().Index = 1;
        win_panel.SetActive(true);
        lvManager.setComplete(true);
    }
//    private bool check(GameObject[] cah)
//    {
//        for (int i = 0; i < cah.Length; i++)
//        {
//            if (!cah[i].GetComponent<Drag_Controller>().already_did)
//            {
//                return false;
//            }
//        }
//        return true;
//    }
    public void Open_Object(int index)
    {
        all_Objs[index].SetActive(true);
    }
    public void Close_Object(int index)
    {
        all_Objs[index].SetActive(false);
    }
}
