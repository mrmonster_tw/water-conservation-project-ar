﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace RestFulsClass
{
    public class RestFulClass : MonoBehaviour
    {
        public enum GetFuncTypes { none, GetSystemTime, GetMaxNumber, GetUserNumber, CheckQuestion }
        public enum InsertTypes { none, InsertNewNumber, InsertQuestion }
        public enum GetSceneMode { _0, _3, _4, _5, _6 }

        public class ConvertType
        {
            #region ConvertType
            public static GetFuncTypes stringToGetFuncTypes(string s)
            {
                GetFuncTypes g = GetFuncTypes.none;
                switch (s)
                {
                    case "GetMaxNumber": g = GetFuncTypes.GetMaxNumber; break;
                    case "GetUserNumber": g = GetFuncTypes.GetUserNumber; break;
                    case "CheckQuestion": g = GetFuncTypes.CheckQuestion; break;
                }
                return g;
            }
            public static InsertTypes stringToInsertTypes(string s)
            {
                InsertTypes g = InsertTypes.none;
                switch (s)
                {
                    case "InsertNewNumber": g = InsertTypes.InsertNewNumber; break;
                    case "InsertQuestion": g = InsertTypes.InsertQuestion; break;
                }
                return g;
            }
            public static GetSceneMode stringToGetSceneMode(string s)
            {
                GetSceneMode g = GetSceneMode._0;
                switch (s)
                {
                    case "3": g = GetSceneMode._3; break;
                    case "4": g = GetSceneMode._4; break;
                    case "5": g = GetSceneMode._5; break;
                    case "6": g = GetSceneMode._6; break;
                }
                return g;
            }

            #endregion
        }
        #region RestFulClass    
        public class RestFul
        {
            #region Initialize()
            public static GetTickets This;
            public static RestFul restFul;
            public static string myAddress, mySVC, FuncNormal, PlayerUUID;
            #endregion
            public static void SetConnectState(bool s)
            {
                try
                { GetTickets.This.Connecting.SetTrigger("Trigger"); GetTickets.This.Connecting.SetBool("Connecting", s); }
                catch { }
            }

            //這是櫃台人員的操作介面 http://192.168.1.64/SAWC/backendsetting.aspx
            //Server系統時間 http://192.168.1.64/SAWC/wcf/SAWCService.svc/GetSystemTime

            #region ReturnAPIs
            public struct WaitResults
            {
                public static bool waitResult;
                public static IEnumerator waitEesult() { while (Datas.RestFulString.Count == 0) yield return null; }
            }

            public static void GetSystemTime()
            {
                GetFuncTypes type = GetFuncTypes.GetSystemTime;
                GetSceneMode mode = GetSceneMode._0;
                try
                { GetTickets.This.StartCoroutine(Datas.Getdata(type, mode)); }
                catch { }
            }

            public static void GetMaxNumber(string EventName, string SceneMode)
            {                 //EX: http://192.168.1.64/SAWC/wcf/SAWCService.svc/RestFulGetMaxNumber/3                                //Return:  {"RestFulGetMaxNumberResult":1}

                GetFuncTypes type = ConvertType.stringToGetFuncTypes(EventName);
                GetSceneMode mode = ConvertType.stringToGetSceneMode(SceneMode);
                try
                { GetTickets.This.StartCoroutine(Datas.Getdata(type, mode)); }
                catch { }
            }
            public static void InsertNewNumber(string UUID, string Type)
            {                          //EX: http://192.168.1.64/SAWC/wcf/SAWCService.svc/RestFulInsertNewNumber/784533/3                      //Return:  {"RestFulInsertNewNumberResult":1}
                InsertTypes type = InsertTypes.InsertNewNumber;
                GetSceneMode mode = ConvertType.stringToGetSceneMode(Type);
                try
                { GetTickets.This.StartCoroutine(Datas.Setdata(type, mode)); }
                catch { }
            }
            public static void GetUserExpired(string UUID)
            {                                        //EX: http://192.168.1.64/SAWC/wcf/SAWCService.svc/RestFulGetUserNumber/784533                          //Return:  {"RestFulGetUserNumberResult":{"Expired":false,"Number":1,"Type":"3"}}

                GetFuncTypes type = GetFuncTypes.GetUserNumber;
                GetSceneMode mode = GetSceneMode._0;
                try
                { GetTickets.This.StartCoroutine(Datas.Getdata(type, mode, UUID)); }
                catch { }

            }
            public static void InsertQuestion                                                      //EX: http://192.168.1.64/SAWC/wcf/SAWCService.svc/RestFulInsertQuestion/784533/3/1/51歲以上/F/5/123    //Return:  {"RestFulInsertQuestionResult":1}
            (string Type, string Number, string Age, string Sex, string Score, string Opinion)
            {

                InsertTypes type = InsertTypes.InsertQuestion;
                GetSceneMode mode = ConvertType.stringToGetSceneMode(Type);
                try { GetTickets.This.StartCoroutine(Datas.Setdata(type, mode, "0", Age, Sex, Score, Opinion)); }
                catch { }
            }

            //你還沒填問券嗎???
            public static void CheckQuestion(string ID, string Type)
            {                              //EX: http://192.168.1.64/SAWC/wcf/SAWCService.svc/RestFulCheckQuestion/784533/3                        //Return:  {"RestFulCheckQuestionResult":false}    

                GetFuncTypes type = GetFuncTypes.CheckQuestion;
                GetSceneMode mode = ConvertType.stringToGetSceneMode(Type);
                try { GetTickets.This.StartCoroutine(Datas.Getdata(type, mode)); }
                catch { }
            }
            #endregion
            public struct Datas
            {
                #region 取得資料
                public static List<string> RestFulString = new List<string>();
                public static IEnumerator Getdata(GetFuncTypes typemode, GetSceneMode sceneMode, params string[] elseStr)
                {
                    string Func = typemode + (
                        (typemode == GetFuncTypes.GetUserNumber) ?
                        ""
                        :
                        (typemode == GetFuncTypes.CheckQuestion) ?
                        ("/" + PlayerUUID + "/" + sceneMode.ToString().Substring(sceneMode.ToString().Length - 1))
                        :
                        ("/" + sceneMode.ToString().Substring(sceneMode.ToString().Length - 1))

                        );
                    string requstUrl = "http://" + myAddress + mySVC + "/" + ((typemode == GetFuncTypes.GetSystemTime) ? "" : FuncNormal) + Func + ((elseStr.Length <= 0) ? "" : "/" + elseStr[0]);
                    requstUrl = (typemode == GetFuncTypes.GetSystemTime) ? requstUrl.Substring(0, requstUrl.Length - 2) : requstUrl;
                    yield return null;
                    This.StartCoroutine(waitGetdataSend(requstUrl));
                }
                static IEnumerator waitGetdataSend(string requstUrl)
                {
                    using (UnityWebRequest www = UnityWebRequest.Get(requstUrl))
                    {
                        while (true)
                        {
                            This.StartCoroutine(GetTickets.This.waitScendAndAnimation());
                            while (This.waitScendAndAnimationTime < 40f / 60f) yield return null;//如果在0.8秒內連到的話,就等待動畫結束

                            yield return www.SendWebRequest();

                            if (!www.isDone)
                            {
                                Debug.Log("The WWW Out of time!! so Reconnecting." + "Send is done:" + www.SendWebRequest().isDone);
                                This.StartCoroutine(waitGetdataSend(requstUrl));
                                yield break;
                            }
                            else
                                print("Send Success!!");

                            if (www.isNetworkError)
                            {
                                Debug.Log("The WWW Connect error!! so system is reconnecting.");
                                //This.StartCoroutine(waitGetdataSend(requstUrl));
                                yield break;
                            }
                            else
                            {                                
                                RestFulString = Decode_WWW_Return(www.downloadHandler.text);
                                break;
                            }
                        }
                    }
                }
                #endregion

                #region 新增資料
                public static IEnumerator Setdata(InsertTypes insertTypes, GetSceneMode sceneMode, params string[] elseStr)
                {
                    string Func = insertTypes + "/";
                    string requstUrl = "http://" + myAddress + mySVC + "/" + FuncNormal + Func + PlayerUUID + "/" + sceneMode.ToString().Substring(1);
                    Debug.Log(requstUrl);
                    string elseData = "";
                    if (elseStr.Length > 0)
                        foreach (string s in elseStr)
                            elseData += "/" + s;

                    requstUrl = requstUrl + elseData;

                    yield return null;
                    This.StartCoroutine(waitGetdataSend(requstUrl));
                }
                static IEnumerator waitSetdataSend(string requstUrl)
                {
                    using (UnityWebRequest www = UnityWebRequest.Get(requstUrl))
                    {
                        while (true)
                        {
                            This.StartCoroutine(GetTickets.This.waitScendAndAnimation());
                            while (This.waitScendAndAnimationTime < 40f / 60f) yield return null;//如果在0.8秒內連到的話,就等待動畫結束

                            yield return www.SendWebRequest();

                            if (!www.isDone)
                            {
                                Debug.LogError("The WWW Out of time!! so Reconnecting." + "Send is done:" + www.SendWebRequest().isDone);
                                This.StartCoroutine(waitGetdataSend(requstUrl));
                                yield break;
                            }
                            else
                                print("Send Success!!");

                            if (www.isNetworkError)
                            {
                                Debug.LogError("The WWW Connect error!! so system is reconnecting.");
                                This.StartCoroutine(waitGetdataSend(requstUrl));
                                yield break;
                            }
                            else
                            {
                                RestFulString = Decode_WWW_Return(www.downloadHandler.text);
                                print(RestFulString[0]);
                                break;
                            }

                        }
                    }

                }
                #endregion
            }
            #region Decode_WWW_Return
            public static List<string> Decode_WWW_Return(string www)
            {
                List<string> wwwText = new List<string>();
                string[] temps; string temp = "";

                temp = www.Substring(1, www.Length - 2);    //去前後括號
                temps = temp.Split(':');                    //尋找中斷點
                temp = "";
                for (int i = 1; i < temps.Length; i++) temp += temps[i];                            //取後者
                if (!temp.Contains("{"))                                                //資料只有一筆，安心輸出
                    wwwText.Add(temp);
                else                                                                    //很遺憾資料不只一筆，再解開一次斷行
                {
                    temp = temp.Substring(1, temp.Length - 2);    //去前後括號
                    temps = temp.Split(',');
                    for (int i = 0; i < temps.Length; i++) wwwText.Add(temps[i]);
                }
                return wwwText;
            }
            #endregion
        }
        #endregion
    }

}
