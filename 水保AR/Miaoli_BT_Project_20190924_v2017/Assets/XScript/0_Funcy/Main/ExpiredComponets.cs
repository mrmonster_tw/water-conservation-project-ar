﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExpiredComponets : MonoBehaviour {
    public static ExpiredComponets This;
    public NormalList normalList;
    [Serializable]public struct NormalList
    {
        public RectTransform girl,buttonList,msg;
    }
    public SureList sureList;
    [Serializable]public struct SureList
    {
        public RectTransform  sureListRoot;
        public RectTransform sureResult;
        public RectTransform message;
        public Animator[] sureListAnimator;
        public Sprite[] resultSprite;
    }
    private void Initialize()
    {
        sureList.sureListAnimator = sureList.sureListRoot.GetComponentsInChildren<Animator>();
    }
    void Awake() { This = this; Initialize(); InitializeClick(); }
    // Use this for initialization
    void InitializeClick()
    {
        
    }
    void Start ()
    {
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    const int 
        _3DMode  =  3,
        _4DMode  =  4,
        _3DMovie =  5,
        _Gift    =  6;

    public void SetSure(int mode, bool active, int Number, int nextHour, int nextMinute)
    {
        SetAnim(mode, active);
        SetConstance(mode, Number, nextHour, nextMinute);
        switch (mode)
        {
            case _3DMode:

                break;
            case _4DMode:

                break;
            case _3DMovie:

                break;
            case _Gift:

                break;
            default:
                Debug.LogError("沒有這個票種!!!");
                break;
        }

    }
    private void SetConstance(int Mode, int Number, int nextHour, int nextMinute)
    {
        Transform t = sureList.sureListAnimator[Mode - 3].transform;
        if (Mode <= 4)
        {
            Text
            all_Number = t.FindChild("all_Number").GetComponent<Text>(),
            Next_time = t.FindChild("Next_time").GetComponent<Text>();
            all_Number.text = Number.ToString("00");
            Next_time.text = nextHour.ToString("00") + ":" + nextMinute.ToString("00");
        }
        else
        {
            t.FindChild("all_Number").GetComponent<Text>().text = Number.ToString("00");
        }
    }
    private void SetAnim(int Mode, bool active)
    {
        Animator a = sureList.sureListAnimator[Mode - 3];
        a.SetBool("Scale", active);
        a.SetTrigger("Trigger");
    }
    public void CloseAnim(Transform me)
    {
        Animator a = me.parent.GetComponent<Animator>();
        a.SetBool("Scale", false);
        a.SetTrigger("Trigger");
    }
    public Text all_Number;
    public void SetResult(int Mode, int Number, int nextHour, int nextMinute, bool isrepeat)
    {
        Transform resultGroup = sureList.sureResult;
        Image sure_booking_white = resultGroup.FindChild("sure_booking_white").GetComponent<Image>();
        Text NowNumber = resultGroup.FindChild("NowNumber").GetComponent<Text>();
        Text estimate  = resultGroup.FindChild("estimate" ).GetComponent<Text>();

        sure_booking_white.sprite = sureList.resultSprite[Mode - 3];


        NowNumber.text = (Mode <= 4) ? Number.ToString("00") : "";
        all_Number.text = (Mode > 4) ? Number.ToString("00") : "";
        estimate.text = (Mode <= 4) ? (nextHour.ToString("00") + ":" + nextMinute.ToString("00")) : "";


        resultGroup.gameObject.SetActive(true);
        SetAnim(Mode, false);

            PlayerPrefs.SetInt("預定Mode" , Mode);
            PlayerPrefs.SetInt("預定Hour" , nextHour);
            PlayerPrefs.SetInt("預定Minute", nextMinute);
            PlayerPrefs.SetInt("預定Number", Number);
            PlayerPrefs.SetInt("尚未重製", 1);

        if (isrepeat != sureList.message.gameObject.activeInHierarchy)
            sureList.message.gameObject.SetActive(isrepeat);
    }
    public void SetResultRepeat(int Mode, int Number, int nextHour, int nextMinute, bool isrepeat)
    {
        Transform resultGroup = sureList.sureResult;
        Image sure_booking_white = resultGroup.FindChild("sure_booking_white").GetComponent<Image>();
        Text NowNumber = resultGroup.FindChild("NowNumber").GetComponent<Text>();
        Text estimate = resultGroup.FindChild("estimate").GetComponent<Text>();

        sure_booking_white.sprite = sureList.resultSprite[Mode - 3];


        NowNumber.text = (Mode <= 4) ? Number.ToString("00") : "";
        all_Number.text = (Mode > 4) ? Number.ToString("00") : "";
        estimate.text = (Mode <= 4) ? (nextHour.ToString("00") + ":" + nextMinute.ToString("00")) : "";


        resultGroup.gameObject.SetActive(true);
        SetAnim(Mode, false);

        if (isrepeat != sureList.message.gameObject.activeInHierarchy)
            sureList.message.gameObject.SetActive(isrepeat);
    }
}
