﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class DragAction : MonoBehaviour
{
    public RectTransform item;
    public Vector3 StartPos = new Vector3(2, -90, 0);
    public Vector2 clampRange = new Vector2(-90, 155);
    Vector3 nowPos = Vector3.zero;
    private void Awake()
    {
        nowPos = StartPos;
    }
    public void OnDrag(BaseEventData data)
    {
        PointerEventData p = (PointerEventData)data;
        nowPos += new Vector3(0, p.delta.y, 0);
        nowPos.y = Mathf.Clamp(nowPos.y, clampRange.x, clampRange.y);
        item.localPosition = nowPos;
    }

}
