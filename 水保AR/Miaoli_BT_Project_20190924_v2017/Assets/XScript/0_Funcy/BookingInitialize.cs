﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookingInitialize : MonoBehaviour {
    [SerializeField] List<GameObject> openObject = new List<GameObject>();
    [SerializeField] List<GameObject> closeObject = new List<GameObject>();
	// Use this for initialization
	void Start ()
    {
        if (PlayerPrefs.GetInt("尚未重製") == 0)
            return;
        else
        {
            foreach (GameObject g in openObject)  g.SetActive(true);
            foreach (GameObject g in closeObject) g.SetActive(false);
            ExpiredComponets.This.SetResultRepeat(
                PlayerPrefs.GetInt("預定Mode"),
                PlayerPrefs.GetInt("預定Number"),
                PlayerPrefs.GetInt("預定Hour"),
                PlayerPrefs.GetInt("預定Minute"),
                false
                );            
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
