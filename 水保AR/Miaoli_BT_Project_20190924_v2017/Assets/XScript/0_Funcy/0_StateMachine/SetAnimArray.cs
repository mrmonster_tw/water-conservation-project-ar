﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
public class SetAnimArray : StateMachineBehaviour
{
    [Serializable]
    public struct SetAnimInfo
    {
        public AnimatorControllerParameterType type;
        public float latetime;
        public string ParameterName;
        public bool use判斷, bool參數;
        public float Float參數;
        public int Int參數;
    }
    public List<SetAnimInfo> setAnimInfo = new List<SetAnimInfo>();

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Coroutine mono = animator.GetComponent<Coroutine>();
        mono.StartCoroutine(late(animator));
    }
    IEnumerator late(Animator animator)
    {
        foreach (SetAnimInfo si in setAnimInfo)
        {
            if (si.use判斷)
            {
                bool returnfalse = false;
                switch (si.type)
                {
                    case AnimatorControllerParameterType.Bool: returnfalse = animator.GetBool(si.ParameterName) != si.bool參數; break;
                    case AnimatorControllerParameterType.Float: returnfalse = animator.GetFloat(si.ParameterName) != si.Float參數; break;
                    case AnimatorControllerParameterType.Int: returnfalse = animator.GetInteger(si.ParameterName) != si.Int參數; break;
                }

                if (returnfalse) yield break;
            }
            if (si.latetime > 0)
                yield return new WaitForSeconds(si.latetime);

            switch (si.type)
            {
                case AnimatorControllerParameterType.Bool: animator.SetBool(si.ParameterName, si.bool參數); break;
                case AnimatorControllerParameterType.Float: animator.SetFloat(si.ParameterName, si.Float參數); break;
                case AnimatorControllerParameterType.Int: animator.SetInteger(si.ParameterName, si.Int參數); break;
                case AnimatorControllerParameterType.Trigger: animator.SetTrigger(si.ParameterName); break;
            }

        }
    }
}
