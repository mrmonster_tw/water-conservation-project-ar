﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
public class Scrollbar_Controller : MonoBehaviour
{
    public RectTransform[] target;
    public Image[] dots;
    public GameObject grid;
    private float[] distance;
    public AudioClip swipe_sound;
    // Use this for initialization
    void Start()
    {
        gameObject.SetActive(false);
       // GameObject.Find("bt_schedule").GetComponent<Change_Sprite>().Sprite_change(0);
        distance = new float[target.Length];
    }

    // Update is called once per frame
    void Update()
    {
        //放開手指時
        if (Input.GetMouseButtonUp(0))
        {
            //計算自己與各座標的距離
            for (int i = 0; i < target.Length; i++)
            {
                distance[i] = Vector3.Distance(grid.GetComponent<RectTransform>().position, target[i].GetComponent<RectTransform>().position);
            }

            //計算最小距離
            float min = distance[0];
            int final_index = 0;
            for (int k = 0; k < target.Length; k++)
            {
                //所有的點點全部為灰色
                dots[k].gameObject.GetComponent<Change_Sprite>().Sprite_change(0);
                if (distance[k] < min)
                {
                    min = distance[k];
                    final_index = k;
                }
            }
            //將整個scroll panel移動到指定位置
            iTween.MoveTo(grid, iTween.Hash("position", target[final_index].GetComponent<RectTransform>().position, "time", 0.5f, "easeType", iTween.EaseType.easeOutExpo));
            gameObject.GetComponent<AudioSource>().Stop();
            gameObject.GetComponent<AudioSource>().clip = swipe_sound;
            gameObject.GetComponent<AudioSource>().Play();

            //將其點點設定為白色
            dots[final_index].gameObject.GetComponent<Change_Sprite>().Sprite_change(1);
        }
    }

    public void Set_Active_True()
    {
        gameObject.SetActive(true);
    }
    public void Set_Active_False()
    {
        gameObject.SetActive(false);
        //GameObject.Find("bt_schedule").GetComponent<Change_Sprite>().Sprite_change(0);
    }
}
