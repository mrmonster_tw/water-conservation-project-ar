﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Level_Controller : MonoBehaviour {
    public static Level_Controller This;
    [HideInInspector]public Text 請點擊任意處以繼續遊戲;
    
    public void setPostBack(bool b)
    {
        isPostLevel = b;
    }
    public void setComplete(bool b)
    {
        請點擊任意處以繼續遊戲.enabled = true;
        completed = b;
    }
    public bool isPostLevel, completed, leveling = false;
    Button QUIT_Button;
    //對話框1
    public GameObject[] All_Images;
    public GameObject gray;
    public GameObject bt_close;
    private Vuforia.ImageTargetBehaviour levelMeshsTarget;
    private void Awake()
    {
        This = this;
        請點擊任意處以繼續遊戲 = FindObjectsOfType<Text>().ToList().Find(t => t.gameObject.name == "請點擊任意處以繼續遊戲");

        string findStr = gameObject.name.Split('_')[1];
        levelMeshsTarget = System.Array.Find(FindObjectsOfType<Vuforia.ImageTargetBehaviour>(), i => i.gameObject.name.Contains(findStr));

        QUIT_Button = System.Array.Find(FindObjectsOfType<Button>(), b => b.name == "QUIT_Button");
    }
    private void Start()
    {
      unShowImage();
      //yield return new WaitForSeconds(0.5f);
      //請點擊任意處以繼續遊戲.enabled = true;
   }
    private void Update()
    {
        if (All_Images[0].activeInHierarchy && isPostLevel) isPostLevel = false;
        if (levelMeshsTarget.GetComponentInChildren<MeshRenderer>())
        {
            leveling = levelMeshsTarget.GetComponentInChildren<MeshRenderer>().enabled;
        }
            
    }
    private void FixedUpdate()
    {
        if (!(gameObject.name == "Level_01")) return;
        bool waitsearch = true;
        foreach (Level_Controller lc in FindObjectsOfType<Level_Controller>())
            if (lc.leveling)
                waitsearch = false;

        QUIT_Button.GetComponent<Image>().enabled = waitsearch;
    }
    // Use this for initialization
    public void ShowImage()
    {
        if (completed)
        {
            FindObjectsOfType<Text>().ToList().Find(t => t.gameObject.name == "挑戰成功").enabled = true;
            return;
        }
            
        請點擊任意處以繼續遊戲.enabled = true;
        if (GameObject.Find("ScrollRect"))
            GameObject.Find("ScrollRect").GetComponent<Scrollbar_Controller>().Set_Active_False();
        if (GameObject.Find("Tips"))
            GameObject.Find("Tips").GetComponent<Tips_Controller>().closeAll();

        foreach (GameObject i in All_Images)
        {
            i.SetActive(false);
        }
        All_Images[0].SetActive(true);
        gray.SetActive(true);
        bt_close.SetActive(true);
    }
    public void unShowImage()
    {
        foreach (GameObject i in All_Images)
        {
            i.SetActive(false);
        }
        gray.SetActive(false);
        bt_close.SetActive(false);
    }
}
