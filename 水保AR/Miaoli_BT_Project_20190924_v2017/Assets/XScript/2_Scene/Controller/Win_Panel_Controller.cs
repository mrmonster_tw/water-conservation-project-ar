﻿using UnityEngine.UI;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Win_Panel_Controller : MonoBehaviour
{
   public int Index = 0;
   public GameObject[] CharacterImage;
   public GameObject[] star_Images;
   public GameObject[] thanks;
   public GameObject Parent_obj;
   public GameObject Shinning;
   public float Shinning_speed = 50;

   public Transform didntfind;
   // Use this for initialization
   void Start()
   {
      bt_schedule = FindObjectsOfType<UnityEngine.UI.Button>().ToList().Find(b => b.gameObject.name ==
"bt_schedule"
);
      請點擊任意處以繼續遊戲 = FindObjectsOfType<Text>().ToList().Find(t => t.gameObject.name == "請點擊任意處以繼續遊戲");

      Close();
   }
   [SerializeField]UnityEngine.UI.Button bt_schedule;
   Text 請點擊任意處以繼續遊戲;
   private void Awake()
   {

   }
   
   void OnEnable()
   {
      didntfind.localScale = Vector3.zero;
      for (int k = 0; k < gameObject.transform.GetChildCount(); k++)
      {
         gameObject.transform.GetChild(k).gameObject.SetActive(true);
      }
      foreach (GameObject i in thanks)
      {
         i.SetActive(false);
      }
      foreach (GameObject i in CharacterImage)
      {
         i.SetActive(false);
      }
      CharacterImage[Index].SetActive(true);

      GameObject.Find("Tips").GetComponent<Tips_Controller>().closeAll();

      Parent_obj.transform.localScale = Vector3.zero;
      foreach (GameObject i in star_Images)
      {
         i.transform.localScale = Vector3.zero;
      }
      StartCoroutine(StartImage());
   }
   void OnDisable()
   {
      iTween.Stop();
   }
   // Update is called once per frame
   void Update()
   {
      Shinning.transform.Rotate(0, 0, Shinning_speed * Time.deltaTime);
   }
   IEnumerator StartImage()
   {
      yield return new WaitForSeconds(0.05f);
      請點擊任意處以繼續遊戲.enabled = false;
      //yield return new WaitForSeconds(0);
      iTween.ScaleTo(Parent_obj, iTween.Hash("scale", new Vector3(0.6f, 0.6f, 0.6f), "time", 0.75f, "easeType", iTween.EaseType.easeOutBounce));
      yield return new WaitForSeconds(1);
      for (int i = 0; i < star_Images.Length; i++)
      {
         star_Images[i].GetComponent<UnityEngine.UI.Image>().enabled = true;
         iTween.ScaleTo(star_Images[i], iTween.Hash("scale", Vector3.one, "time", 0.5f, "easeType", iTween.EaseType.easeOutBack));
         yield return new WaitForSeconds(0.5f);
      }
      while (didntfind.localScale.x < 1.66f)
      {
         didntfind.localScale = Vector3.Lerp(didntfind.localScale, Vector3.one * 1.67f, 0.1f);
         yield return null;
      }
      請點擊任意處以繼續遊戲.enabled = true;
   }

   public void close_Part()
   {
      for (int k = 0; k < gameObject.transform.GetChildCount(); k++)
      {
         gameObject.transform.GetChild(k).gameObject.SetActive(false);
      }
      thanks[Index].SetActive(true);
   }
   public void Close()
   {
      gameObject.SetActive(false);
   }
   public void OnWinAndTalkEnded()
   {
      if (GameManager.Gmanager._device_state == GameManager.device_state.Phone)
         bt_schedule.onClick.Invoke();
   }
}
