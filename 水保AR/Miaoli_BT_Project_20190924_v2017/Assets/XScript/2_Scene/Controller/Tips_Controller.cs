﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Tips_Controller : MonoBehaviour
{
   public GameObject existGameBtn;
   public GameObject[] Level_Tips;
   public GameObject[] Level_bt;
   public GameObject[] Level_bg;
   //對話框1
   public GameObject[] All_Images;
   public GameObject[] All_Images_notMobile;
   public GameObject bt_skip;
   private Text 請點擊任意處以繼續遊戲;
   private void Awake()
   {
      請點擊任意處以繼續遊戲 = FindObjectsOfType<Text>().ToList().Find(t => t.gameObject.name == "請點擊任意處以繼續遊戲");
   }
   private void Start()
   {
      reset();
      //yield return new WaitForSeconds(0.1f);
      //請點擊任意處以繼續遊戲.enabled = true;
   }

   private void Update()
   {

   }
   // Use this for initialization
   public void reset()
   {      

      if (GameObject.Find("ApplicationGameMaker").GetComponent<GameManager>()._state == GameManager.GameState.Level_01)
      {
         Level_Tips[0].SetActive(true);
         Level_bt[0].SetActive(true);
         Level_bg[0].SetActive(true);
      }
      if (GameObject.Find("ApplicationGameMaker").GetComponent<GameManager>()._state == GameManager.GameState.Level_02)
      {
         Level_Tips[1].SetActive(true);
         Level_bt[1].SetActive(true);
         Level_bg[1].SetActive(true);
      }
      if (GameObject.Find("ApplicationGameMaker").GetComponent<GameManager>()._state == GameManager.GameState.Level_03)
      {
         Level_Tips[2].SetActive(true);
         Level_bt[2].SetActive(true);
         Level_bg[2].SetActive(true);
      }
      if (GameObject.Find("ApplicationGameMaker").GetComponent<GameManager>()._state == GameManager.GameState.Level_04)
      {
         Level_Tips[3].SetActive(true);
         Level_bt[3].SetActive(true);
         Level_bg[3].SetActive(true);
      }
      if (GameObject.Find("ApplicationGameMaker").GetComponent<GameManager>()._state == GameManager.GameState.Level_05)
      {
         Level_Tips[4].SetActive(true);
         Level_bt[4].SetActive(true);
         Level_bg[4].SetActive(true);
      }

      existGameBtn.SetActive(false);

      if (GameObject.Find("ApplicationGameMaker").GetComponent<GameManager>()._state == GameManager.GameState.Main)
      {
         foreach (GameObject i in All_Images)
         {
            i.SetActive(false);
         }
         foreach (GameObject i in All_Images_notMobile)
         {
            i.SetActive(false);
         }

         All_Images[0].SetActive(true);
         bt_skip.SetActive(true);
      }
      if (GameObject.Find("ApplicationGameMaker").GetComponent<GameManager>()._state == GameManager.GameState.notMobile_Main)
      {
         foreach (GameObject i in All_Images_notMobile)
         {
            i.SetActive(false);
         }
         All_Images_notMobile[0].SetActive(true);
         bt_skip.SetActive(true);
      }
      請點擊任意處以繼續遊戲.enabled = false;
   }
   public void closeAll()
   {
      if (請點擊任意處以繼續遊戲)
         請點擊任意處以繼續遊戲.enabled = false;
      if (GameObject.Find("ApplicationGameMaker").GetComponent<GameManager>()._state == GameManager.GameState.Main)
         foreach (GameObject i in All_Images)
         {
            i.SetActive(false);
         }
      else
         foreach (GameObject i in All_Images_notMobile)
         {
            i.SetActive(false);
         }

      existGameBtn.SetActive(true);
      bt_skip.SetActive(false);
   }
}

