﻿using System.Linq;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
[System.Serializable]
public class BT_Mic
{
    public bool SW;
    public string MIC; 
}
public class Send_Receive_ArduinoData_by_BT : MonoBehaviour
{
    public BT_Mic[] _data;
    //是否顯示連線狀態
    public bool b_debug = false;
    public Text BT_state_text;

    private void Awake()
    {
        for (int i = 0; i < _data.Length; i++)
        {
            if (_data[i].SW)
            {
                //step:1 制定藍芽編碼
                BtConnector.moduleMAC(_data[i].MIC);//藍芽位址
            }
        }

        //step:2 關閉藍芽<確保每次開啟APP重置訊號>
        BtConnector.close();

        //step:3 若藍芽有開啟則連接藍芽
        if (!BtConnector.isBluetoothEnabled())
            BtConnector.askEnableBluetooth();
        else
            BtConnector.connect();
    }
   char a = ' ';
    void Update()
    {
        //若b_debug為真,則顯示Debug模式
        if (b_debug)
            BT_state_text.text = "State: " + BtConnector.readControlData();
        else
            BT_state_text.text = "";

      if (Input.GetKeyDown(KeyCode.F11))
         StartCoroutine(onScaned());


   }   
    //傳送字元
    public void Send_Char_Date()
    {
        print("Send_BT");        
            StartCoroutine(onScaned());        
    }
   string readLine = "null";
   IEnumerator onScaned()
   {
      Text t = FindObjectsOfType<Text>().ToList().Find(tx => tx.gameObject.name == "恭喜挑戰成功");
      int nowSce = 60;
      t.transform.parent.GetComponent<Image>().enabled = true;
      t.transform.GetComponentInChildren<Image>().enabled = true;
      if (BtConnector.isConnected())
      {
         BtConnector.sendChar('Y');
      }
      while (true)
      {
         string contannt =
         "恭喜挑戰成功!!" + "\n" +
         "請將手環靠近感應器紀錄" + "\n\n\n\n\n\n\n\n\n" +

         "注意！ 程式將在 " + nowSce + " 秒後重新開始 "
          ;
         t.text = contannt;

         yield return StartCoroutine(wait(1));
         nowSce--;
         if (nowSce < 0) break;

      }
      load_level();
   }
    IEnumerator wait(float time)
    {
        float nowTime = Time.realtimeSinceStartup;
        while (Time.realtimeSinceStartup - nowTime < time) yield return null;        
    }
    void load_level()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("1_MainMenu");
    }
}
