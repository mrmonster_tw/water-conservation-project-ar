﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class notMobile_MapController : MonoBehaviour {
   [SerializeField] Image map_m;
   [SerializeField] Sprite[] spriteIndex;
   private void OnEnable()
   {
      int index = (int)GameManager.Gmanager._state;
         if (index > spriteIndex.Length - 1) index = 0;
      map_m.sprite = spriteIndex[index];
   }
   // Use this for initialization
   void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
