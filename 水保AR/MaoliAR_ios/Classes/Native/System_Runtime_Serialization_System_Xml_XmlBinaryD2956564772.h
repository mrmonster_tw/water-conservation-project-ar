﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Xml_System_Xml_XmlNodeType1672767151.h"

// System.String
struct String_t;
// System.Xml.XmlDictionaryString
struct XmlDictionaryString_t3504120266;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlBinaryDictionaryReader/NodeInfo
struct  NodeInfo_t2956564772  : public Il2CppObject
{
public:
	// System.Boolean System.Xml.XmlBinaryDictionaryReader/NodeInfo::IsAttributeValue
	bool ___IsAttributeValue_0;
	// System.Int32 System.Xml.XmlBinaryDictionaryReader/NodeInfo::Position
	int32_t ___Position_1;
	// System.String System.Xml.XmlBinaryDictionaryReader/NodeInfo::Prefix
	String_t* ___Prefix_2;
	// System.Xml.XmlDictionaryString System.Xml.XmlBinaryDictionaryReader/NodeInfo::DictLocalName
	XmlDictionaryString_t3504120266 * ___DictLocalName_3;
	// System.Xml.XmlDictionaryString System.Xml.XmlBinaryDictionaryReader/NodeInfo::DictNS
	XmlDictionaryString_t3504120266 * ___DictNS_4;
	// System.Xml.XmlDictionaryString System.Xml.XmlBinaryDictionaryReader/NodeInfo::DictValue
	XmlDictionaryString_t3504120266 * ___DictValue_5;
	// System.Xml.XmlNodeType System.Xml.XmlBinaryDictionaryReader/NodeInfo::NodeType
	int32_t ___NodeType_6;
	// System.Object System.Xml.XmlBinaryDictionaryReader/NodeInfo::TypedValue
	Il2CppObject * ___TypedValue_7;
	// System.Byte System.Xml.XmlBinaryDictionaryReader/NodeInfo::ValueType
	uint8_t ___ValueType_8;
	// System.Int32 System.Xml.XmlBinaryDictionaryReader/NodeInfo::NSSlot
	int32_t ___NSSlot_9;
	// System.String System.Xml.XmlBinaryDictionaryReader/NodeInfo::name
	String_t* ___name_10;
	// System.String System.Xml.XmlBinaryDictionaryReader/NodeInfo::local_name
	String_t* ___local_name_11;
	// System.String System.Xml.XmlBinaryDictionaryReader/NodeInfo::ns
	String_t* ___ns_12;
	// System.String System.Xml.XmlBinaryDictionaryReader/NodeInfo::value
	String_t* ___value_13;

public:
	inline static int32_t get_offset_of_IsAttributeValue_0() { return static_cast<int32_t>(offsetof(NodeInfo_t2956564772, ___IsAttributeValue_0)); }
	inline bool get_IsAttributeValue_0() const { return ___IsAttributeValue_0; }
	inline bool* get_address_of_IsAttributeValue_0() { return &___IsAttributeValue_0; }
	inline void set_IsAttributeValue_0(bool value)
	{
		___IsAttributeValue_0 = value;
	}

	inline static int32_t get_offset_of_Position_1() { return static_cast<int32_t>(offsetof(NodeInfo_t2956564772, ___Position_1)); }
	inline int32_t get_Position_1() const { return ___Position_1; }
	inline int32_t* get_address_of_Position_1() { return &___Position_1; }
	inline void set_Position_1(int32_t value)
	{
		___Position_1 = value;
	}

	inline static int32_t get_offset_of_Prefix_2() { return static_cast<int32_t>(offsetof(NodeInfo_t2956564772, ___Prefix_2)); }
	inline String_t* get_Prefix_2() const { return ___Prefix_2; }
	inline String_t** get_address_of_Prefix_2() { return &___Prefix_2; }
	inline void set_Prefix_2(String_t* value)
	{
		___Prefix_2 = value;
		Il2CppCodeGenWriteBarrier(&___Prefix_2, value);
	}

	inline static int32_t get_offset_of_DictLocalName_3() { return static_cast<int32_t>(offsetof(NodeInfo_t2956564772, ___DictLocalName_3)); }
	inline XmlDictionaryString_t3504120266 * get_DictLocalName_3() const { return ___DictLocalName_3; }
	inline XmlDictionaryString_t3504120266 ** get_address_of_DictLocalName_3() { return &___DictLocalName_3; }
	inline void set_DictLocalName_3(XmlDictionaryString_t3504120266 * value)
	{
		___DictLocalName_3 = value;
		Il2CppCodeGenWriteBarrier(&___DictLocalName_3, value);
	}

	inline static int32_t get_offset_of_DictNS_4() { return static_cast<int32_t>(offsetof(NodeInfo_t2956564772, ___DictNS_4)); }
	inline XmlDictionaryString_t3504120266 * get_DictNS_4() const { return ___DictNS_4; }
	inline XmlDictionaryString_t3504120266 ** get_address_of_DictNS_4() { return &___DictNS_4; }
	inline void set_DictNS_4(XmlDictionaryString_t3504120266 * value)
	{
		___DictNS_4 = value;
		Il2CppCodeGenWriteBarrier(&___DictNS_4, value);
	}

	inline static int32_t get_offset_of_DictValue_5() { return static_cast<int32_t>(offsetof(NodeInfo_t2956564772, ___DictValue_5)); }
	inline XmlDictionaryString_t3504120266 * get_DictValue_5() const { return ___DictValue_5; }
	inline XmlDictionaryString_t3504120266 ** get_address_of_DictValue_5() { return &___DictValue_5; }
	inline void set_DictValue_5(XmlDictionaryString_t3504120266 * value)
	{
		___DictValue_5 = value;
		Il2CppCodeGenWriteBarrier(&___DictValue_5, value);
	}

	inline static int32_t get_offset_of_NodeType_6() { return static_cast<int32_t>(offsetof(NodeInfo_t2956564772, ___NodeType_6)); }
	inline int32_t get_NodeType_6() const { return ___NodeType_6; }
	inline int32_t* get_address_of_NodeType_6() { return &___NodeType_6; }
	inline void set_NodeType_6(int32_t value)
	{
		___NodeType_6 = value;
	}

	inline static int32_t get_offset_of_TypedValue_7() { return static_cast<int32_t>(offsetof(NodeInfo_t2956564772, ___TypedValue_7)); }
	inline Il2CppObject * get_TypedValue_7() const { return ___TypedValue_7; }
	inline Il2CppObject ** get_address_of_TypedValue_7() { return &___TypedValue_7; }
	inline void set_TypedValue_7(Il2CppObject * value)
	{
		___TypedValue_7 = value;
		Il2CppCodeGenWriteBarrier(&___TypedValue_7, value);
	}

	inline static int32_t get_offset_of_ValueType_8() { return static_cast<int32_t>(offsetof(NodeInfo_t2956564772, ___ValueType_8)); }
	inline uint8_t get_ValueType_8() const { return ___ValueType_8; }
	inline uint8_t* get_address_of_ValueType_8() { return &___ValueType_8; }
	inline void set_ValueType_8(uint8_t value)
	{
		___ValueType_8 = value;
	}

	inline static int32_t get_offset_of_NSSlot_9() { return static_cast<int32_t>(offsetof(NodeInfo_t2956564772, ___NSSlot_9)); }
	inline int32_t get_NSSlot_9() const { return ___NSSlot_9; }
	inline int32_t* get_address_of_NSSlot_9() { return &___NSSlot_9; }
	inline void set_NSSlot_9(int32_t value)
	{
		___NSSlot_9 = value;
	}

	inline static int32_t get_offset_of_name_10() { return static_cast<int32_t>(offsetof(NodeInfo_t2956564772, ___name_10)); }
	inline String_t* get_name_10() const { return ___name_10; }
	inline String_t** get_address_of_name_10() { return &___name_10; }
	inline void set_name_10(String_t* value)
	{
		___name_10 = value;
		Il2CppCodeGenWriteBarrier(&___name_10, value);
	}

	inline static int32_t get_offset_of_local_name_11() { return static_cast<int32_t>(offsetof(NodeInfo_t2956564772, ___local_name_11)); }
	inline String_t* get_local_name_11() const { return ___local_name_11; }
	inline String_t** get_address_of_local_name_11() { return &___local_name_11; }
	inline void set_local_name_11(String_t* value)
	{
		___local_name_11 = value;
		Il2CppCodeGenWriteBarrier(&___local_name_11, value);
	}

	inline static int32_t get_offset_of_ns_12() { return static_cast<int32_t>(offsetof(NodeInfo_t2956564772, ___ns_12)); }
	inline String_t* get_ns_12() const { return ___ns_12; }
	inline String_t** get_address_of_ns_12() { return &___ns_12; }
	inline void set_ns_12(String_t* value)
	{
		___ns_12 = value;
		Il2CppCodeGenWriteBarrier(&___ns_12, value);
	}

	inline static int32_t get_offset_of_value_13() { return static_cast<int32_t>(offsetof(NodeInfo_t2956564772, ___value_13)); }
	inline String_t* get_value_13() const { return ___value_13; }
	inline String_t** get_address_of_value_13() { return &___value_13; }
	inline void set_value_13(String_t* value)
	{
		___value_13 = value;
		Il2CppCodeGenWriteBarrier(&___value_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
