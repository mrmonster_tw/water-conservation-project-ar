﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.InstanceContext
struct InstanceContext_t3593205954;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.CallbackInstanceContextProvider
struct  CallbackInstanceContextProvider_t1043138406  : public Il2CppObject
{
public:
	// System.ServiceModel.InstanceContext System.ServiceModel.Dispatcher.CallbackInstanceContextProvider::ctx
	InstanceContext_t3593205954 * ___ctx_0;

public:
	inline static int32_t get_offset_of_ctx_0() { return static_cast<int32_t>(offsetof(CallbackInstanceContextProvider_t1043138406, ___ctx_0)); }
	inline InstanceContext_t3593205954 * get_ctx_0() const { return ___ctx_0; }
	inline InstanceContext_t3593205954 ** get_address_of_ctx_0() { return &___ctx_0; }
	inline void set_ctx_0(InstanceContext_t3593205954 * value)
	{
		___ctx_0 = value;
		Il2CppCodeGenWriteBarrier(&___ctx_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
