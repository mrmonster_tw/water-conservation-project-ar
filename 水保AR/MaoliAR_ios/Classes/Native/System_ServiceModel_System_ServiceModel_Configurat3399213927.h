﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Configurati313369665.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.StandardBindingElementCollection`1<System.ServiceModel.Configuration.NetTcpBindingElement>
struct  StandardBindingElementCollection_1_t3399213927  : public ServiceModelEnhancedConfigurationElementCollection_1_t313369665
{
public:

public:
};

struct StandardBindingElementCollection_1_t3399213927_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.StandardBindingElementCollection`1::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_23;

public:
	inline static int32_t get_offset_of_properties_23() { return static_cast<int32_t>(offsetof(StandardBindingElementCollection_1_t3399213927_StaticFields, ___properties_23)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_23() const { return ___properties_23; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_23() { return &___properties_23; }
	inline void set_properties_23(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_23 = value;
		Il2CppCodeGenWriteBarrier(&___properties_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
