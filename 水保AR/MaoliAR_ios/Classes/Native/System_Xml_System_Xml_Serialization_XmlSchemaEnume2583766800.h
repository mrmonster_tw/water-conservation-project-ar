﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.IEnumerator
struct IEnumerator_t1853284238;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSchemaEnumerator
struct  XmlSchemaEnumerator_t2583766800  : public Il2CppObject
{
public:
	// System.Collections.IEnumerator System.Xml.Serialization.XmlSchemaEnumerator::e
	Il2CppObject * ___e_0;

public:
	inline static int32_t get_offset_of_e_0() { return static_cast<int32_t>(offsetof(XmlSchemaEnumerator_t2583766800, ___e_0)); }
	inline Il2CppObject * get_e_0() const { return ___e_0; }
	inline Il2CppObject ** get_address_of_e_0() { return &___e_0; }
	inline void set_e_0(Il2CppObject * value)
	{
		___e_0 = value;
		Il2CppCodeGenWriteBarrier(&___e_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
