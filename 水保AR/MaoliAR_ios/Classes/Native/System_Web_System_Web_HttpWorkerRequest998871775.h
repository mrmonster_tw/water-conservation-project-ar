﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpWorkerRequest
struct  HttpWorkerRequest_t998871775  : public Il2CppObject
{
public:
	// System.Boolean System.Web.HttpWorkerRequest::started_internally
	bool ___started_internally_2;

public:
	inline static int32_t get_offset_of_started_internally_2() { return static_cast<int32_t>(offsetof(HttpWorkerRequest_t998871775, ___started_internally_2)); }
	inline bool get_started_internally_2() const { return ___started_internally_2; }
	inline bool* get_address_of_started_internally_2() { return &___started_internally_2; }
	inline void set_started_internally_2(bool value)
	{
		___started_internally_2 = value;
	}
};

struct HttpWorkerRequest_t998871775_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Web.HttpWorkerRequest::RequestHeaderIndexer
	Dictionary_2_t2736202052 * ___RequestHeaderIndexer_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Web.HttpWorkerRequest::ResponseHeaderIndexer
	Dictionary_2_t2736202052 * ___ResponseHeaderIndexer_1;

public:
	inline static int32_t get_offset_of_RequestHeaderIndexer_0() { return static_cast<int32_t>(offsetof(HttpWorkerRequest_t998871775_StaticFields, ___RequestHeaderIndexer_0)); }
	inline Dictionary_2_t2736202052 * get_RequestHeaderIndexer_0() const { return ___RequestHeaderIndexer_0; }
	inline Dictionary_2_t2736202052 ** get_address_of_RequestHeaderIndexer_0() { return &___RequestHeaderIndexer_0; }
	inline void set_RequestHeaderIndexer_0(Dictionary_2_t2736202052 * value)
	{
		___RequestHeaderIndexer_0 = value;
		Il2CppCodeGenWriteBarrier(&___RequestHeaderIndexer_0, value);
	}

	inline static int32_t get_offset_of_ResponseHeaderIndexer_1() { return static_cast<int32_t>(offsetof(HttpWorkerRequest_t998871775_StaticFields, ___ResponseHeaderIndexer_1)); }
	inline Dictionary_2_t2736202052 * get_ResponseHeaderIndexer_1() const { return ___ResponseHeaderIndexer_1; }
	inline Dictionary_2_t2736202052 ** get_address_of_ResponseHeaderIndexer_1() { return &___ResponseHeaderIndexer_1; }
	inline void set_ResponseHeaderIndexer_1(Dictionary_2_t2736202052 * value)
	{
		___ResponseHeaderIndexer_1 = value;
		Il2CppCodeGenWriteBarrier(&___ResponseHeaderIndexer_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
