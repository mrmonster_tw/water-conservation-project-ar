﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.IEnumerable
struct IEnumerable_t1941168011;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.HtmlControls.HtmlSelect/<GetData>c__AnonStorey7
struct  U3CGetDataU3Ec__AnonStorey7_t1645920000  : public Il2CppObject
{
public:
	// System.Collections.IEnumerable System.Web.UI.HtmlControls.HtmlSelect/<GetData>c__AnonStorey7::result
	Il2CppObject * ___result_0;

public:
	inline static int32_t get_offset_of_result_0() { return static_cast<int32_t>(offsetof(U3CGetDataU3Ec__AnonStorey7_t1645920000, ___result_0)); }
	inline Il2CppObject * get_result_0() const { return ___result_0; }
	inline Il2CppObject ** get_address_of_result_0() { return &___result_0; }
	inline void set_result_0(Il2CppObject * value)
	{
		___result_0 = value;
		Il2CppCodeGenWriteBarrier(&___result_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
