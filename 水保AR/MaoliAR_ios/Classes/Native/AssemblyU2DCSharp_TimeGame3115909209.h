﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;
// Level_Controller
struct Level_Controller_t1433348427;
// System.Predicate`1<Level_Controller>
struct Predicate_1_t2258642551;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimeGame
struct  TimeGame_t3115909209  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TimeGame::Left_time
	float ___Left_time_2;
	// System.Single TimeGame::Left_timeMax
	float ___Left_timeMax_3;
	// UnityEngine.GameObject TimeGame::checkObj
	GameObject_t1113636619 * ___checkObj_4;
	// UnityEngine.UI.Image TimeGame::im
	Image_t2670269651 * ___im_5;
	// UnityEngine.UI.Text TimeGame::timeText
	Text_t1901882714 * ___timeText_6;
	// UnityEngine.GameObject TimeGame::Clock
	GameObject_t1113636619 * ___Clock_7;
	// UnityEngine.GameObject[] TimeGame::OpenIt
	GameObjectU5BU5D_t3328599146* ___OpenIt_8;
	// UnityEngine.Events.UnityEvent TimeGame::Events
	UnityEvent_t2581268647 * ___Events_9;
	// UnityEngine.GameObject TimeGame::ReplayTips
	GameObject_t1113636619 * ___ReplayTips_10;
	// UnityEngine.GameObject TimeGame::RC_Pos
	GameObject_t1113636619 * ___RC_Pos_11;
	// System.Boolean TimeGame::Rock_Once
	bool ___Rock_Once_12;
	// Level_Controller TimeGame::Level_05
	Level_Controller_t1433348427 * ___Level_05_13;

public:
	inline static int32_t get_offset_of_Left_time_2() { return static_cast<int32_t>(offsetof(TimeGame_t3115909209, ___Left_time_2)); }
	inline float get_Left_time_2() const { return ___Left_time_2; }
	inline float* get_address_of_Left_time_2() { return &___Left_time_2; }
	inline void set_Left_time_2(float value)
	{
		___Left_time_2 = value;
	}

	inline static int32_t get_offset_of_Left_timeMax_3() { return static_cast<int32_t>(offsetof(TimeGame_t3115909209, ___Left_timeMax_3)); }
	inline float get_Left_timeMax_3() const { return ___Left_timeMax_3; }
	inline float* get_address_of_Left_timeMax_3() { return &___Left_timeMax_3; }
	inline void set_Left_timeMax_3(float value)
	{
		___Left_timeMax_3 = value;
	}

	inline static int32_t get_offset_of_checkObj_4() { return static_cast<int32_t>(offsetof(TimeGame_t3115909209, ___checkObj_4)); }
	inline GameObject_t1113636619 * get_checkObj_4() const { return ___checkObj_4; }
	inline GameObject_t1113636619 ** get_address_of_checkObj_4() { return &___checkObj_4; }
	inline void set_checkObj_4(GameObject_t1113636619 * value)
	{
		___checkObj_4 = value;
		Il2CppCodeGenWriteBarrier(&___checkObj_4, value);
	}

	inline static int32_t get_offset_of_im_5() { return static_cast<int32_t>(offsetof(TimeGame_t3115909209, ___im_5)); }
	inline Image_t2670269651 * get_im_5() const { return ___im_5; }
	inline Image_t2670269651 ** get_address_of_im_5() { return &___im_5; }
	inline void set_im_5(Image_t2670269651 * value)
	{
		___im_5 = value;
		Il2CppCodeGenWriteBarrier(&___im_5, value);
	}

	inline static int32_t get_offset_of_timeText_6() { return static_cast<int32_t>(offsetof(TimeGame_t3115909209, ___timeText_6)); }
	inline Text_t1901882714 * get_timeText_6() const { return ___timeText_6; }
	inline Text_t1901882714 ** get_address_of_timeText_6() { return &___timeText_6; }
	inline void set_timeText_6(Text_t1901882714 * value)
	{
		___timeText_6 = value;
		Il2CppCodeGenWriteBarrier(&___timeText_6, value);
	}

	inline static int32_t get_offset_of_Clock_7() { return static_cast<int32_t>(offsetof(TimeGame_t3115909209, ___Clock_7)); }
	inline GameObject_t1113636619 * get_Clock_7() const { return ___Clock_7; }
	inline GameObject_t1113636619 ** get_address_of_Clock_7() { return &___Clock_7; }
	inline void set_Clock_7(GameObject_t1113636619 * value)
	{
		___Clock_7 = value;
		Il2CppCodeGenWriteBarrier(&___Clock_7, value);
	}

	inline static int32_t get_offset_of_OpenIt_8() { return static_cast<int32_t>(offsetof(TimeGame_t3115909209, ___OpenIt_8)); }
	inline GameObjectU5BU5D_t3328599146* get_OpenIt_8() const { return ___OpenIt_8; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_OpenIt_8() { return &___OpenIt_8; }
	inline void set_OpenIt_8(GameObjectU5BU5D_t3328599146* value)
	{
		___OpenIt_8 = value;
		Il2CppCodeGenWriteBarrier(&___OpenIt_8, value);
	}

	inline static int32_t get_offset_of_Events_9() { return static_cast<int32_t>(offsetof(TimeGame_t3115909209, ___Events_9)); }
	inline UnityEvent_t2581268647 * get_Events_9() const { return ___Events_9; }
	inline UnityEvent_t2581268647 ** get_address_of_Events_9() { return &___Events_9; }
	inline void set_Events_9(UnityEvent_t2581268647 * value)
	{
		___Events_9 = value;
		Il2CppCodeGenWriteBarrier(&___Events_9, value);
	}

	inline static int32_t get_offset_of_ReplayTips_10() { return static_cast<int32_t>(offsetof(TimeGame_t3115909209, ___ReplayTips_10)); }
	inline GameObject_t1113636619 * get_ReplayTips_10() const { return ___ReplayTips_10; }
	inline GameObject_t1113636619 ** get_address_of_ReplayTips_10() { return &___ReplayTips_10; }
	inline void set_ReplayTips_10(GameObject_t1113636619 * value)
	{
		___ReplayTips_10 = value;
		Il2CppCodeGenWriteBarrier(&___ReplayTips_10, value);
	}

	inline static int32_t get_offset_of_RC_Pos_11() { return static_cast<int32_t>(offsetof(TimeGame_t3115909209, ___RC_Pos_11)); }
	inline GameObject_t1113636619 * get_RC_Pos_11() const { return ___RC_Pos_11; }
	inline GameObject_t1113636619 ** get_address_of_RC_Pos_11() { return &___RC_Pos_11; }
	inline void set_RC_Pos_11(GameObject_t1113636619 * value)
	{
		___RC_Pos_11 = value;
		Il2CppCodeGenWriteBarrier(&___RC_Pos_11, value);
	}

	inline static int32_t get_offset_of_Rock_Once_12() { return static_cast<int32_t>(offsetof(TimeGame_t3115909209, ___Rock_Once_12)); }
	inline bool get_Rock_Once_12() const { return ___Rock_Once_12; }
	inline bool* get_address_of_Rock_Once_12() { return &___Rock_Once_12; }
	inline void set_Rock_Once_12(bool value)
	{
		___Rock_Once_12 = value;
	}

	inline static int32_t get_offset_of_Level_05_13() { return static_cast<int32_t>(offsetof(TimeGame_t3115909209, ___Level_05_13)); }
	inline Level_Controller_t1433348427 * get_Level_05_13() const { return ___Level_05_13; }
	inline Level_Controller_t1433348427 ** get_address_of_Level_05_13() { return &___Level_05_13; }
	inline void set_Level_05_13(Level_Controller_t1433348427 * value)
	{
		___Level_05_13 = value;
		Il2CppCodeGenWriteBarrier(&___Level_05_13, value);
	}
};

struct TimeGame_t3115909209_StaticFields
{
public:
	// System.Predicate`1<Level_Controller> TimeGame::<>f__am$cache0
	Predicate_1_t2258642551 * ___U3CU3Ef__amU24cache0_14;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_14() { return static_cast<int32_t>(offsetof(TimeGame_t3115909209_StaticFields, ___U3CU3Ef__amU24cache0_14)); }
	inline Predicate_1_t2258642551 * get_U3CU3Ef__amU24cache0_14() const { return ___U3CU3Ef__amU24cache0_14; }
	inline Predicate_1_t2258642551 ** get_address_of_U3CU3Ef__amU24cache0_14() { return &___U3CU3Ef__amU24cache0_14; }
	inline void set_U3CU3Ef__amU24cache0_14(Predicate_1_t2258642551 * value)
	{
		___U3CU3Ef__amU24cache0_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
