﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.OperationContext
struct OperationContext_t2829644788;
// System.ServiceModel.Channels.RequestContext
struct RequestContext_t1473296135;
// System.ServiceModel.Channels.Message
struct Message_t869514973;
// System.ServiceModel.InstanceContext
struct InstanceContext_t3593205954;
// System.Exception
struct Exception_t1436737249;
// System.ServiceModel.Dispatcher.DispatchOperation
struct DispatchOperation_t1810306617;
// System.ServiceModel.Dispatcher.UserEventsHandler
struct UserEventsHandler_t1403801399;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.MessageProcessingContext
struct  MessageProcessingContext_t1414617556  : public Il2CppObject
{
public:
	// System.ServiceModel.OperationContext System.ServiceModel.Dispatcher.MessageProcessingContext::operation_context
	OperationContext_t2829644788 * ___operation_context_0;
	// System.ServiceModel.Channels.RequestContext System.ServiceModel.Dispatcher.MessageProcessingContext::request_context
	RequestContext_t1473296135 * ___request_context_1;
	// System.ServiceModel.Channels.Message System.ServiceModel.Dispatcher.MessageProcessingContext::incoming_message
	Message_t869514973 * ___incoming_message_2;
	// System.ServiceModel.Channels.Message System.ServiceModel.Dispatcher.MessageProcessingContext::reply_message
	Message_t869514973 * ___reply_message_3;
	// System.ServiceModel.InstanceContext System.ServiceModel.Dispatcher.MessageProcessingContext::instance_context
	InstanceContext_t3593205954 * ___instance_context_4;
	// System.Exception System.ServiceModel.Dispatcher.MessageProcessingContext::processingException
	Exception_t1436737249 * ___processingException_5;
	// System.ServiceModel.Dispatcher.DispatchOperation System.ServiceModel.Dispatcher.MessageProcessingContext::operation
	DispatchOperation_t1810306617 * ___operation_6;
	// System.ServiceModel.Dispatcher.UserEventsHandler System.ServiceModel.Dispatcher.MessageProcessingContext::user_events_handler
	UserEventsHandler_t1403801399 * ___user_events_handler_7;

public:
	inline static int32_t get_offset_of_operation_context_0() { return static_cast<int32_t>(offsetof(MessageProcessingContext_t1414617556, ___operation_context_0)); }
	inline OperationContext_t2829644788 * get_operation_context_0() const { return ___operation_context_0; }
	inline OperationContext_t2829644788 ** get_address_of_operation_context_0() { return &___operation_context_0; }
	inline void set_operation_context_0(OperationContext_t2829644788 * value)
	{
		___operation_context_0 = value;
		Il2CppCodeGenWriteBarrier(&___operation_context_0, value);
	}

	inline static int32_t get_offset_of_request_context_1() { return static_cast<int32_t>(offsetof(MessageProcessingContext_t1414617556, ___request_context_1)); }
	inline RequestContext_t1473296135 * get_request_context_1() const { return ___request_context_1; }
	inline RequestContext_t1473296135 ** get_address_of_request_context_1() { return &___request_context_1; }
	inline void set_request_context_1(RequestContext_t1473296135 * value)
	{
		___request_context_1 = value;
		Il2CppCodeGenWriteBarrier(&___request_context_1, value);
	}

	inline static int32_t get_offset_of_incoming_message_2() { return static_cast<int32_t>(offsetof(MessageProcessingContext_t1414617556, ___incoming_message_2)); }
	inline Message_t869514973 * get_incoming_message_2() const { return ___incoming_message_2; }
	inline Message_t869514973 ** get_address_of_incoming_message_2() { return &___incoming_message_2; }
	inline void set_incoming_message_2(Message_t869514973 * value)
	{
		___incoming_message_2 = value;
		Il2CppCodeGenWriteBarrier(&___incoming_message_2, value);
	}

	inline static int32_t get_offset_of_reply_message_3() { return static_cast<int32_t>(offsetof(MessageProcessingContext_t1414617556, ___reply_message_3)); }
	inline Message_t869514973 * get_reply_message_3() const { return ___reply_message_3; }
	inline Message_t869514973 ** get_address_of_reply_message_3() { return &___reply_message_3; }
	inline void set_reply_message_3(Message_t869514973 * value)
	{
		___reply_message_3 = value;
		Il2CppCodeGenWriteBarrier(&___reply_message_3, value);
	}

	inline static int32_t get_offset_of_instance_context_4() { return static_cast<int32_t>(offsetof(MessageProcessingContext_t1414617556, ___instance_context_4)); }
	inline InstanceContext_t3593205954 * get_instance_context_4() const { return ___instance_context_4; }
	inline InstanceContext_t3593205954 ** get_address_of_instance_context_4() { return &___instance_context_4; }
	inline void set_instance_context_4(InstanceContext_t3593205954 * value)
	{
		___instance_context_4 = value;
		Il2CppCodeGenWriteBarrier(&___instance_context_4, value);
	}

	inline static int32_t get_offset_of_processingException_5() { return static_cast<int32_t>(offsetof(MessageProcessingContext_t1414617556, ___processingException_5)); }
	inline Exception_t1436737249 * get_processingException_5() const { return ___processingException_5; }
	inline Exception_t1436737249 ** get_address_of_processingException_5() { return &___processingException_5; }
	inline void set_processingException_5(Exception_t1436737249 * value)
	{
		___processingException_5 = value;
		Il2CppCodeGenWriteBarrier(&___processingException_5, value);
	}

	inline static int32_t get_offset_of_operation_6() { return static_cast<int32_t>(offsetof(MessageProcessingContext_t1414617556, ___operation_6)); }
	inline DispatchOperation_t1810306617 * get_operation_6() const { return ___operation_6; }
	inline DispatchOperation_t1810306617 ** get_address_of_operation_6() { return &___operation_6; }
	inline void set_operation_6(DispatchOperation_t1810306617 * value)
	{
		___operation_6 = value;
		Il2CppCodeGenWriteBarrier(&___operation_6, value);
	}

	inline static int32_t get_offset_of_user_events_handler_7() { return static_cast<int32_t>(offsetof(MessageProcessingContext_t1414617556, ___user_events_handler_7)); }
	inline UserEventsHandler_t1403801399 * get_user_events_handler_7() const { return ___user_events_handler_7; }
	inline UserEventsHandler_t1403801399 ** get_address_of_user_events_handler_7() { return &___user_events_handler_7; }
	inline void set_user_events_handler_7(UserEventsHandler_t1403801399 * value)
	{
		___user_events_handler_7 = value;
		Il2CppCodeGenWriteBarrier(&___user_events_handler_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
