﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// Level_Controller
struct Level_Controller_t1433348427;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1800779281;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.GameObject
struct GameObject_t1113636619;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Level_05_Controller
struct  Level_05_Controller_t1454764318  : public MonoBehaviour_t3962482529
{
public:
	// Level_Controller Level_05_Controller::lvManager
	Level_Controller_t1433348427 * ___lvManager_2;
	// UnityEngine.ParticleSystem Level_05_Controller::completeParticle
	ParticleSystem_t1800779281 * ___completeParticle_3;
	// UnityEngine.GameObject[] Level_05_Controller::all_image
	GameObjectU5BU5D_t3328599146* ___all_image_4;
	// System.Boolean Level_05_Controller::do_once
	bool ___do_once_5;
	// UnityEngine.GameObject Level_05_Controller::win_panel
	GameObject_t1113636619 * ___win_panel_6;

public:
	inline static int32_t get_offset_of_lvManager_2() { return static_cast<int32_t>(offsetof(Level_05_Controller_t1454764318, ___lvManager_2)); }
	inline Level_Controller_t1433348427 * get_lvManager_2() const { return ___lvManager_2; }
	inline Level_Controller_t1433348427 ** get_address_of_lvManager_2() { return &___lvManager_2; }
	inline void set_lvManager_2(Level_Controller_t1433348427 * value)
	{
		___lvManager_2 = value;
		Il2CppCodeGenWriteBarrier(&___lvManager_2, value);
	}

	inline static int32_t get_offset_of_completeParticle_3() { return static_cast<int32_t>(offsetof(Level_05_Controller_t1454764318, ___completeParticle_3)); }
	inline ParticleSystem_t1800779281 * get_completeParticle_3() const { return ___completeParticle_3; }
	inline ParticleSystem_t1800779281 ** get_address_of_completeParticle_3() { return &___completeParticle_3; }
	inline void set_completeParticle_3(ParticleSystem_t1800779281 * value)
	{
		___completeParticle_3 = value;
		Il2CppCodeGenWriteBarrier(&___completeParticle_3, value);
	}

	inline static int32_t get_offset_of_all_image_4() { return static_cast<int32_t>(offsetof(Level_05_Controller_t1454764318, ___all_image_4)); }
	inline GameObjectU5BU5D_t3328599146* get_all_image_4() const { return ___all_image_4; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_all_image_4() { return &___all_image_4; }
	inline void set_all_image_4(GameObjectU5BU5D_t3328599146* value)
	{
		___all_image_4 = value;
		Il2CppCodeGenWriteBarrier(&___all_image_4, value);
	}

	inline static int32_t get_offset_of_do_once_5() { return static_cast<int32_t>(offsetof(Level_05_Controller_t1454764318, ___do_once_5)); }
	inline bool get_do_once_5() const { return ___do_once_5; }
	inline bool* get_address_of_do_once_5() { return &___do_once_5; }
	inline void set_do_once_5(bool value)
	{
		___do_once_5 = value;
	}

	inline static int32_t get_offset_of_win_panel_6() { return static_cast<int32_t>(offsetof(Level_05_Controller_t1454764318, ___win_panel_6)); }
	inline GameObject_t1113636619 * get_win_panel_6() const { return ___win_panel_6; }
	inline GameObject_t1113636619 ** get_address_of_win_panel_6() { return &___win_panel_6; }
	inline void set_win_panel_6(GameObject_t1113636619 * value)
	{
		___win_panel_6 = value;
		Il2CppCodeGenWriteBarrier(&___win_panel_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
