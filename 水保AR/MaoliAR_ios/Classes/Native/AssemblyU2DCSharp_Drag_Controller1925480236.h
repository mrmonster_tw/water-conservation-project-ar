﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Vector33722313464.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;
// UnityEngine.BoxCollider
struct BoxCollider_t1640800422;
// HighlightingSystem.Highlighter
struct Highlighter_t672210414;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// UnityEngine.Texture[]
struct TextureU5BU5D_t908295702;
// UnityEngine.Sprite
struct Sprite_t280657092;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Drag_Controller
struct  Drag_Controller_t1925480236  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Drag_Controller::LOCKING
	bool ___LOCKING_2;
	// System.Boolean Drag_Controller::complete
	bool ___complete_3;
	// System.Boolean Drag_Controller::useClick
	bool ___useClick_4;
	// System.Boolean Drag_Controller::dontFixPosition
	bool ___dontFixPosition_5;
	// UnityEngine.GameObject Drag_Controller::NeedToDestroy
	GameObject_t1113636619 * ___NeedToDestroy_6;
	// System.Boolean Drag_Controller::already_did
	bool ___already_did_7;
	// UnityEngine.Events.UnityEvent Drag_Controller::Events
	UnityEvent_t2581268647 * ___Events_8;
	// UnityEngine.Vector3 Drag_Controller::reset_pos
	Vector3_t3722313464  ___reset_pos_9;
	// UnityEngine.BoxCollider Drag_Controller::target
	BoxCollider_t1640800422 * ___target_10;
	// UnityEngine.Vector3 Drag_Controller::screenPoint
	Vector3_t3722313464  ___screenPoint_11;
	// UnityEngine.Vector3 Drag_Controller::offset
	Vector3_t3722313464  ___offset_12;
	// System.Boolean Drag_Controller::do_Once
	bool ___do_Once_13;
	// HighlightingSystem.Highlighter Drag_Controller::_highlighter
	Highlighter_t672210414 * ____highlighter_14;
	// UnityEngine.SpriteRenderer Drag_Controller::SR
	SpriteRenderer_t3235626157 * ___SR_15;
	// UnityEngine.Sprite[] Drag_Controller::MySprite
	SpriteU5BU5D_t2581906349* ___MySprite_16;
	// UnityEngine.MeshRenderer Drag_Controller::level4background
	MeshRenderer_t587009260 * ___level4background_17;
	// UnityEngine.Texture[] Drag_Controller::level4backgroundSpriteArray
	TextureU5BU5D_t908295702* ___level4backgroundSpriteArray_18;
	// UnityEngine.Sprite Drag_Controller::level5TrueImage
	Sprite_t280657092 * ___level5TrueImage_19;
	// UnityEngine.Sprite Drag_Controller::level5FalseImage
	Sprite_t280657092 * ___level5FalseImage_20;
	// System.Boolean Drag_Controller::sw
	bool ___sw_21;
	// System.Int32 Drag_Controller::myCount
	int32_t ___myCount_22;
	// System.Boolean Drag_Controller::picking
	bool ___picking_24;

public:
	inline static int32_t get_offset_of_LOCKING_2() { return static_cast<int32_t>(offsetof(Drag_Controller_t1925480236, ___LOCKING_2)); }
	inline bool get_LOCKING_2() const { return ___LOCKING_2; }
	inline bool* get_address_of_LOCKING_2() { return &___LOCKING_2; }
	inline void set_LOCKING_2(bool value)
	{
		___LOCKING_2 = value;
	}

	inline static int32_t get_offset_of_complete_3() { return static_cast<int32_t>(offsetof(Drag_Controller_t1925480236, ___complete_3)); }
	inline bool get_complete_3() const { return ___complete_3; }
	inline bool* get_address_of_complete_3() { return &___complete_3; }
	inline void set_complete_3(bool value)
	{
		___complete_3 = value;
	}

	inline static int32_t get_offset_of_useClick_4() { return static_cast<int32_t>(offsetof(Drag_Controller_t1925480236, ___useClick_4)); }
	inline bool get_useClick_4() const { return ___useClick_4; }
	inline bool* get_address_of_useClick_4() { return &___useClick_4; }
	inline void set_useClick_4(bool value)
	{
		___useClick_4 = value;
	}

	inline static int32_t get_offset_of_dontFixPosition_5() { return static_cast<int32_t>(offsetof(Drag_Controller_t1925480236, ___dontFixPosition_5)); }
	inline bool get_dontFixPosition_5() const { return ___dontFixPosition_5; }
	inline bool* get_address_of_dontFixPosition_5() { return &___dontFixPosition_5; }
	inline void set_dontFixPosition_5(bool value)
	{
		___dontFixPosition_5 = value;
	}

	inline static int32_t get_offset_of_NeedToDestroy_6() { return static_cast<int32_t>(offsetof(Drag_Controller_t1925480236, ___NeedToDestroy_6)); }
	inline GameObject_t1113636619 * get_NeedToDestroy_6() const { return ___NeedToDestroy_6; }
	inline GameObject_t1113636619 ** get_address_of_NeedToDestroy_6() { return &___NeedToDestroy_6; }
	inline void set_NeedToDestroy_6(GameObject_t1113636619 * value)
	{
		___NeedToDestroy_6 = value;
		Il2CppCodeGenWriteBarrier(&___NeedToDestroy_6, value);
	}

	inline static int32_t get_offset_of_already_did_7() { return static_cast<int32_t>(offsetof(Drag_Controller_t1925480236, ___already_did_7)); }
	inline bool get_already_did_7() const { return ___already_did_7; }
	inline bool* get_address_of_already_did_7() { return &___already_did_7; }
	inline void set_already_did_7(bool value)
	{
		___already_did_7 = value;
	}

	inline static int32_t get_offset_of_Events_8() { return static_cast<int32_t>(offsetof(Drag_Controller_t1925480236, ___Events_8)); }
	inline UnityEvent_t2581268647 * get_Events_8() const { return ___Events_8; }
	inline UnityEvent_t2581268647 ** get_address_of_Events_8() { return &___Events_8; }
	inline void set_Events_8(UnityEvent_t2581268647 * value)
	{
		___Events_8 = value;
		Il2CppCodeGenWriteBarrier(&___Events_8, value);
	}

	inline static int32_t get_offset_of_reset_pos_9() { return static_cast<int32_t>(offsetof(Drag_Controller_t1925480236, ___reset_pos_9)); }
	inline Vector3_t3722313464  get_reset_pos_9() const { return ___reset_pos_9; }
	inline Vector3_t3722313464 * get_address_of_reset_pos_9() { return &___reset_pos_9; }
	inline void set_reset_pos_9(Vector3_t3722313464  value)
	{
		___reset_pos_9 = value;
	}

	inline static int32_t get_offset_of_target_10() { return static_cast<int32_t>(offsetof(Drag_Controller_t1925480236, ___target_10)); }
	inline BoxCollider_t1640800422 * get_target_10() const { return ___target_10; }
	inline BoxCollider_t1640800422 ** get_address_of_target_10() { return &___target_10; }
	inline void set_target_10(BoxCollider_t1640800422 * value)
	{
		___target_10 = value;
		Il2CppCodeGenWriteBarrier(&___target_10, value);
	}

	inline static int32_t get_offset_of_screenPoint_11() { return static_cast<int32_t>(offsetof(Drag_Controller_t1925480236, ___screenPoint_11)); }
	inline Vector3_t3722313464  get_screenPoint_11() const { return ___screenPoint_11; }
	inline Vector3_t3722313464 * get_address_of_screenPoint_11() { return &___screenPoint_11; }
	inline void set_screenPoint_11(Vector3_t3722313464  value)
	{
		___screenPoint_11 = value;
	}

	inline static int32_t get_offset_of_offset_12() { return static_cast<int32_t>(offsetof(Drag_Controller_t1925480236, ___offset_12)); }
	inline Vector3_t3722313464  get_offset_12() const { return ___offset_12; }
	inline Vector3_t3722313464 * get_address_of_offset_12() { return &___offset_12; }
	inline void set_offset_12(Vector3_t3722313464  value)
	{
		___offset_12 = value;
	}

	inline static int32_t get_offset_of_do_Once_13() { return static_cast<int32_t>(offsetof(Drag_Controller_t1925480236, ___do_Once_13)); }
	inline bool get_do_Once_13() const { return ___do_Once_13; }
	inline bool* get_address_of_do_Once_13() { return &___do_Once_13; }
	inline void set_do_Once_13(bool value)
	{
		___do_Once_13 = value;
	}

	inline static int32_t get_offset_of__highlighter_14() { return static_cast<int32_t>(offsetof(Drag_Controller_t1925480236, ____highlighter_14)); }
	inline Highlighter_t672210414 * get__highlighter_14() const { return ____highlighter_14; }
	inline Highlighter_t672210414 ** get_address_of__highlighter_14() { return &____highlighter_14; }
	inline void set__highlighter_14(Highlighter_t672210414 * value)
	{
		____highlighter_14 = value;
		Il2CppCodeGenWriteBarrier(&____highlighter_14, value);
	}

	inline static int32_t get_offset_of_SR_15() { return static_cast<int32_t>(offsetof(Drag_Controller_t1925480236, ___SR_15)); }
	inline SpriteRenderer_t3235626157 * get_SR_15() const { return ___SR_15; }
	inline SpriteRenderer_t3235626157 ** get_address_of_SR_15() { return &___SR_15; }
	inline void set_SR_15(SpriteRenderer_t3235626157 * value)
	{
		___SR_15 = value;
		Il2CppCodeGenWriteBarrier(&___SR_15, value);
	}

	inline static int32_t get_offset_of_MySprite_16() { return static_cast<int32_t>(offsetof(Drag_Controller_t1925480236, ___MySprite_16)); }
	inline SpriteU5BU5D_t2581906349* get_MySprite_16() const { return ___MySprite_16; }
	inline SpriteU5BU5D_t2581906349** get_address_of_MySprite_16() { return &___MySprite_16; }
	inline void set_MySprite_16(SpriteU5BU5D_t2581906349* value)
	{
		___MySprite_16 = value;
		Il2CppCodeGenWriteBarrier(&___MySprite_16, value);
	}

	inline static int32_t get_offset_of_level4background_17() { return static_cast<int32_t>(offsetof(Drag_Controller_t1925480236, ___level4background_17)); }
	inline MeshRenderer_t587009260 * get_level4background_17() const { return ___level4background_17; }
	inline MeshRenderer_t587009260 ** get_address_of_level4background_17() { return &___level4background_17; }
	inline void set_level4background_17(MeshRenderer_t587009260 * value)
	{
		___level4background_17 = value;
		Il2CppCodeGenWriteBarrier(&___level4background_17, value);
	}

	inline static int32_t get_offset_of_level4backgroundSpriteArray_18() { return static_cast<int32_t>(offsetof(Drag_Controller_t1925480236, ___level4backgroundSpriteArray_18)); }
	inline TextureU5BU5D_t908295702* get_level4backgroundSpriteArray_18() const { return ___level4backgroundSpriteArray_18; }
	inline TextureU5BU5D_t908295702** get_address_of_level4backgroundSpriteArray_18() { return &___level4backgroundSpriteArray_18; }
	inline void set_level4backgroundSpriteArray_18(TextureU5BU5D_t908295702* value)
	{
		___level4backgroundSpriteArray_18 = value;
		Il2CppCodeGenWriteBarrier(&___level4backgroundSpriteArray_18, value);
	}

	inline static int32_t get_offset_of_level5TrueImage_19() { return static_cast<int32_t>(offsetof(Drag_Controller_t1925480236, ___level5TrueImage_19)); }
	inline Sprite_t280657092 * get_level5TrueImage_19() const { return ___level5TrueImage_19; }
	inline Sprite_t280657092 ** get_address_of_level5TrueImage_19() { return &___level5TrueImage_19; }
	inline void set_level5TrueImage_19(Sprite_t280657092 * value)
	{
		___level5TrueImage_19 = value;
		Il2CppCodeGenWriteBarrier(&___level5TrueImage_19, value);
	}

	inline static int32_t get_offset_of_level5FalseImage_20() { return static_cast<int32_t>(offsetof(Drag_Controller_t1925480236, ___level5FalseImage_20)); }
	inline Sprite_t280657092 * get_level5FalseImage_20() const { return ___level5FalseImage_20; }
	inline Sprite_t280657092 ** get_address_of_level5FalseImage_20() { return &___level5FalseImage_20; }
	inline void set_level5FalseImage_20(Sprite_t280657092 * value)
	{
		___level5FalseImage_20 = value;
		Il2CppCodeGenWriteBarrier(&___level5FalseImage_20, value);
	}

	inline static int32_t get_offset_of_sw_21() { return static_cast<int32_t>(offsetof(Drag_Controller_t1925480236, ___sw_21)); }
	inline bool get_sw_21() const { return ___sw_21; }
	inline bool* get_address_of_sw_21() { return &___sw_21; }
	inline void set_sw_21(bool value)
	{
		___sw_21 = value;
	}

	inline static int32_t get_offset_of_myCount_22() { return static_cast<int32_t>(offsetof(Drag_Controller_t1925480236, ___myCount_22)); }
	inline int32_t get_myCount_22() const { return ___myCount_22; }
	inline int32_t* get_address_of_myCount_22() { return &___myCount_22; }
	inline void set_myCount_22(int32_t value)
	{
		___myCount_22 = value;
	}

	inline static int32_t get_offset_of_picking_24() { return static_cast<int32_t>(offsetof(Drag_Controller_t1925480236, ___picking_24)); }
	inline bool get_picking_24() const { return ___picking_24; }
	inline bool* get_address_of_picking_24() { return &___picking_24; }
	inline void set_picking_24(bool value)
	{
		___picking_24 = value;
	}
};

struct Drag_Controller_t1925480236_StaticFields
{
public:
	// System.Int32 Drag_Controller::nowPicCount
	int32_t ___nowPicCount_23;

public:
	inline static int32_t get_offset_of_nowPicCount_23() { return static_cast<int32_t>(offsetof(Drag_Controller_t1925480236_StaticFields, ___nowPicCount_23)); }
	inline int32_t get_nowPicCount_23() const { return ___nowPicCount_23; }
	inline int32_t* get_address_of_nowPicCount_23() { return &___nowPicCount_23; }
	inline void set_nowPicCount_23(int32_t value)
	{
		___nowPicCount_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
