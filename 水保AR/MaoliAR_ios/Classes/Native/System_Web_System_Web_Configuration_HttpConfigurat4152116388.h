﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.HttpConfigurationContext
struct  HttpConfigurationContext_t4152116388  : public Il2CppObject
{
public:
	// System.String System.Web.Configuration.HttpConfigurationContext::virtualPath
	String_t* ___virtualPath_0;

public:
	inline static int32_t get_offset_of_virtualPath_0() { return static_cast<int32_t>(offsetof(HttpConfigurationContext_t4152116388, ___virtualPath_0)); }
	inline String_t* get_virtualPath_0() const { return ___virtualPath_0; }
	inline String_t** get_address_of_virtualPath_0() { return &___virtualPath_0; }
	inline void set_virtualPath_0(String_t* value)
	{
		___virtualPath_0 = value;
		Il2CppCodeGenWriteBarrier(&___virtualPath_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
