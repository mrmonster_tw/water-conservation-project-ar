﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Security_Mono_Xml_XmlCanonicalizer_XmlCanoni745999092.h"

// System.String
struct String_t;
// System.Xml.XmlNodeList
struct XmlNodeList_t2551693786;
// System.Text.StringBuilder
struct StringBuilder_t1712802186;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Collections.Hashtable
struct Hashtable_t1853889766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.XmlCanonicalizer
struct  XmlCanonicalizer_t3076776375  : public Il2CppObject
{
public:
	// System.Boolean Mono.Xml.XmlCanonicalizer::comments
	bool ___comments_0;
	// System.Boolean Mono.Xml.XmlCanonicalizer::exclusive
	bool ___exclusive_1;
	// System.String Mono.Xml.XmlCanonicalizer::inclusiveNamespacesPrefixList
	String_t* ___inclusiveNamespacesPrefixList_2;
	// System.Xml.XmlNodeList Mono.Xml.XmlCanonicalizer::xnl
	XmlNodeList_t2551693786 * ___xnl_3;
	// System.Text.StringBuilder Mono.Xml.XmlCanonicalizer::res
	StringBuilder_t1712802186 * ___res_4;
	// Mono.Xml.XmlCanonicalizer/XmlCanonicalizerState Mono.Xml.XmlCanonicalizer::state
	int32_t ___state_5;
	// System.Collections.ArrayList Mono.Xml.XmlCanonicalizer::visibleNamespaces
	ArrayList_t2718874744 * ___visibleNamespaces_6;
	// System.Int32 Mono.Xml.XmlCanonicalizer::prevVisibleNamespacesStart
	int32_t ___prevVisibleNamespacesStart_7;
	// System.Int32 Mono.Xml.XmlCanonicalizer::prevVisibleNamespacesEnd
	int32_t ___prevVisibleNamespacesEnd_8;
	// System.Collections.Hashtable Mono.Xml.XmlCanonicalizer::propagatedNss
	Hashtable_t1853889766 * ___propagatedNss_9;

public:
	inline static int32_t get_offset_of_comments_0() { return static_cast<int32_t>(offsetof(XmlCanonicalizer_t3076776375, ___comments_0)); }
	inline bool get_comments_0() const { return ___comments_0; }
	inline bool* get_address_of_comments_0() { return &___comments_0; }
	inline void set_comments_0(bool value)
	{
		___comments_0 = value;
	}

	inline static int32_t get_offset_of_exclusive_1() { return static_cast<int32_t>(offsetof(XmlCanonicalizer_t3076776375, ___exclusive_1)); }
	inline bool get_exclusive_1() const { return ___exclusive_1; }
	inline bool* get_address_of_exclusive_1() { return &___exclusive_1; }
	inline void set_exclusive_1(bool value)
	{
		___exclusive_1 = value;
	}

	inline static int32_t get_offset_of_inclusiveNamespacesPrefixList_2() { return static_cast<int32_t>(offsetof(XmlCanonicalizer_t3076776375, ___inclusiveNamespacesPrefixList_2)); }
	inline String_t* get_inclusiveNamespacesPrefixList_2() const { return ___inclusiveNamespacesPrefixList_2; }
	inline String_t** get_address_of_inclusiveNamespacesPrefixList_2() { return &___inclusiveNamespacesPrefixList_2; }
	inline void set_inclusiveNamespacesPrefixList_2(String_t* value)
	{
		___inclusiveNamespacesPrefixList_2 = value;
		Il2CppCodeGenWriteBarrier(&___inclusiveNamespacesPrefixList_2, value);
	}

	inline static int32_t get_offset_of_xnl_3() { return static_cast<int32_t>(offsetof(XmlCanonicalizer_t3076776375, ___xnl_3)); }
	inline XmlNodeList_t2551693786 * get_xnl_3() const { return ___xnl_3; }
	inline XmlNodeList_t2551693786 ** get_address_of_xnl_3() { return &___xnl_3; }
	inline void set_xnl_3(XmlNodeList_t2551693786 * value)
	{
		___xnl_3 = value;
		Il2CppCodeGenWriteBarrier(&___xnl_3, value);
	}

	inline static int32_t get_offset_of_res_4() { return static_cast<int32_t>(offsetof(XmlCanonicalizer_t3076776375, ___res_4)); }
	inline StringBuilder_t1712802186 * get_res_4() const { return ___res_4; }
	inline StringBuilder_t1712802186 ** get_address_of_res_4() { return &___res_4; }
	inline void set_res_4(StringBuilder_t1712802186 * value)
	{
		___res_4 = value;
		Il2CppCodeGenWriteBarrier(&___res_4, value);
	}

	inline static int32_t get_offset_of_state_5() { return static_cast<int32_t>(offsetof(XmlCanonicalizer_t3076776375, ___state_5)); }
	inline int32_t get_state_5() const { return ___state_5; }
	inline int32_t* get_address_of_state_5() { return &___state_5; }
	inline void set_state_5(int32_t value)
	{
		___state_5 = value;
	}

	inline static int32_t get_offset_of_visibleNamespaces_6() { return static_cast<int32_t>(offsetof(XmlCanonicalizer_t3076776375, ___visibleNamespaces_6)); }
	inline ArrayList_t2718874744 * get_visibleNamespaces_6() const { return ___visibleNamespaces_6; }
	inline ArrayList_t2718874744 ** get_address_of_visibleNamespaces_6() { return &___visibleNamespaces_6; }
	inline void set_visibleNamespaces_6(ArrayList_t2718874744 * value)
	{
		___visibleNamespaces_6 = value;
		Il2CppCodeGenWriteBarrier(&___visibleNamespaces_6, value);
	}

	inline static int32_t get_offset_of_prevVisibleNamespacesStart_7() { return static_cast<int32_t>(offsetof(XmlCanonicalizer_t3076776375, ___prevVisibleNamespacesStart_7)); }
	inline int32_t get_prevVisibleNamespacesStart_7() const { return ___prevVisibleNamespacesStart_7; }
	inline int32_t* get_address_of_prevVisibleNamespacesStart_7() { return &___prevVisibleNamespacesStart_7; }
	inline void set_prevVisibleNamespacesStart_7(int32_t value)
	{
		___prevVisibleNamespacesStart_7 = value;
	}

	inline static int32_t get_offset_of_prevVisibleNamespacesEnd_8() { return static_cast<int32_t>(offsetof(XmlCanonicalizer_t3076776375, ___prevVisibleNamespacesEnd_8)); }
	inline int32_t get_prevVisibleNamespacesEnd_8() const { return ___prevVisibleNamespacesEnd_8; }
	inline int32_t* get_address_of_prevVisibleNamespacesEnd_8() { return &___prevVisibleNamespacesEnd_8; }
	inline void set_prevVisibleNamespacesEnd_8(int32_t value)
	{
		___prevVisibleNamespacesEnd_8 = value;
	}

	inline static int32_t get_offset_of_propagatedNss_9() { return static_cast<int32_t>(offsetof(XmlCanonicalizer_t3076776375, ___propagatedNss_9)); }
	inline Hashtable_t1853889766 * get_propagatedNss_9() const { return ___propagatedNss_9; }
	inline Hashtable_t1853889766 ** get_address_of_propagatedNss_9() { return &___propagatedNss_9; }
	inline void set_propagatedNss_9(Hashtable_t1853889766 * value)
	{
		___propagatedNss_9 = value;
		Il2CppCodeGenWriteBarrier(&___propagatedNss_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
