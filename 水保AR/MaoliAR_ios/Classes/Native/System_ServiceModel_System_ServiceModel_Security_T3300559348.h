﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Security_T2868958784.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_Se114151047.h"

// System.ServiceModel.Channels.Binding
struct Binding_t859993683;
// System.ServiceModel.EndpointAddress
struct EndpointAddress_t3119842923;
// System.String
struct String_t;
// System.Collections.ObjectModel.Collection`1<System.ServiceModel.Security.Tokens.ClaimTypeRequirement>
struct Collection_1_t674479886;
// System.Collections.ObjectModel.Collection`1<System.Xml.XmlElement>
struct Collection_1_t3800926332;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.IssuedSecurityTokenParameters
struct  IssuedSecurityTokenParameters_t3300559348  : public SecurityTokenParameters_t2868958784
{
public:
	// System.ServiceModel.Channels.Binding System.ServiceModel.Security.Tokens.IssuedSecurityTokenParameters::binding
	Binding_t859993683 * ___binding_4;
	// System.ServiceModel.EndpointAddress System.ServiceModel.Security.Tokens.IssuedSecurityTokenParameters::issuer_address
	EndpointAddress_t3119842923 * ___issuer_address_5;
	// System.ServiceModel.EndpointAddress System.ServiceModel.Security.Tokens.IssuedSecurityTokenParameters::issuer_meta_address
	EndpointAddress_t3119842923 * ___issuer_meta_address_6;
	// System.Int32 System.ServiceModel.Security.Tokens.IssuedSecurityTokenParameters::key_size
	int32_t ___key_size_7;
	// System.IdentityModel.Tokens.SecurityKeyType System.ServiceModel.Security.Tokens.IssuedSecurityTokenParameters::key_type
	int32_t ___key_type_8;
	// System.String System.ServiceModel.Security.Tokens.IssuedSecurityTokenParameters::token_type
	String_t* ___token_type_9;
	// System.Collections.ObjectModel.Collection`1<System.ServiceModel.Security.Tokens.ClaimTypeRequirement> System.ServiceModel.Security.Tokens.IssuedSecurityTokenParameters::reqs
	Collection_1_t674479886 * ___reqs_10;
	// System.Collections.ObjectModel.Collection`1<System.Xml.XmlElement> System.ServiceModel.Security.Tokens.IssuedSecurityTokenParameters::additional_reqs
	Collection_1_t3800926332 * ___additional_reqs_11;

public:
	inline static int32_t get_offset_of_binding_4() { return static_cast<int32_t>(offsetof(IssuedSecurityTokenParameters_t3300559348, ___binding_4)); }
	inline Binding_t859993683 * get_binding_4() const { return ___binding_4; }
	inline Binding_t859993683 ** get_address_of_binding_4() { return &___binding_4; }
	inline void set_binding_4(Binding_t859993683 * value)
	{
		___binding_4 = value;
		Il2CppCodeGenWriteBarrier(&___binding_4, value);
	}

	inline static int32_t get_offset_of_issuer_address_5() { return static_cast<int32_t>(offsetof(IssuedSecurityTokenParameters_t3300559348, ___issuer_address_5)); }
	inline EndpointAddress_t3119842923 * get_issuer_address_5() const { return ___issuer_address_5; }
	inline EndpointAddress_t3119842923 ** get_address_of_issuer_address_5() { return &___issuer_address_5; }
	inline void set_issuer_address_5(EndpointAddress_t3119842923 * value)
	{
		___issuer_address_5 = value;
		Il2CppCodeGenWriteBarrier(&___issuer_address_5, value);
	}

	inline static int32_t get_offset_of_issuer_meta_address_6() { return static_cast<int32_t>(offsetof(IssuedSecurityTokenParameters_t3300559348, ___issuer_meta_address_6)); }
	inline EndpointAddress_t3119842923 * get_issuer_meta_address_6() const { return ___issuer_meta_address_6; }
	inline EndpointAddress_t3119842923 ** get_address_of_issuer_meta_address_6() { return &___issuer_meta_address_6; }
	inline void set_issuer_meta_address_6(EndpointAddress_t3119842923 * value)
	{
		___issuer_meta_address_6 = value;
		Il2CppCodeGenWriteBarrier(&___issuer_meta_address_6, value);
	}

	inline static int32_t get_offset_of_key_size_7() { return static_cast<int32_t>(offsetof(IssuedSecurityTokenParameters_t3300559348, ___key_size_7)); }
	inline int32_t get_key_size_7() const { return ___key_size_7; }
	inline int32_t* get_address_of_key_size_7() { return &___key_size_7; }
	inline void set_key_size_7(int32_t value)
	{
		___key_size_7 = value;
	}

	inline static int32_t get_offset_of_key_type_8() { return static_cast<int32_t>(offsetof(IssuedSecurityTokenParameters_t3300559348, ___key_type_8)); }
	inline int32_t get_key_type_8() const { return ___key_type_8; }
	inline int32_t* get_address_of_key_type_8() { return &___key_type_8; }
	inline void set_key_type_8(int32_t value)
	{
		___key_type_8 = value;
	}

	inline static int32_t get_offset_of_token_type_9() { return static_cast<int32_t>(offsetof(IssuedSecurityTokenParameters_t3300559348, ___token_type_9)); }
	inline String_t* get_token_type_9() const { return ___token_type_9; }
	inline String_t** get_address_of_token_type_9() { return &___token_type_9; }
	inline void set_token_type_9(String_t* value)
	{
		___token_type_9 = value;
		Il2CppCodeGenWriteBarrier(&___token_type_9, value);
	}

	inline static int32_t get_offset_of_reqs_10() { return static_cast<int32_t>(offsetof(IssuedSecurityTokenParameters_t3300559348, ___reqs_10)); }
	inline Collection_1_t674479886 * get_reqs_10() const { return ___reqs_10; }
	inline Collection_1_t674479886 ** get_address_of_reqs_10() { return &___reqs_10; }
	inline void set_reqs_10(Collection_1_t674479886 * value)
	{
		___reqs_10 = value;
		Il2CppCodeGenWriteBarrier(&___reqs_10, value);
	}

	inline static int32_t get_offset_of_additional_reqs_11() { return static_cast<int32_t>(offsetof(IssuedSecurityTokenParameters_t3300559348, ___additional_reqs_11)); }
	inline Collection_1_t3800926332 * get_additional_reqs_11() const { return ___additional_reqs_11; }
	inline Collection_1_t3800926332 ** get_address_of_additional_reqs_11() { return &___additional_reqs_11; }
	inline void set_additional_reqs_11(Collection_1_t3800926332 * value)
	{
		___additional_reqs_11 = value;
		Il2CppCodeGenWriteBarrier(&___additional_reqs_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
