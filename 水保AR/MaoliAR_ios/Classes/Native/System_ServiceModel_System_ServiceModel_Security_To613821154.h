﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.ServiceModel.Security.Tokens.TlsServerSession
struct TlsServerSession_t3338798525;
// System.IO.MemoryStream
struct MemoryStream_t94973147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.SslAuthenticatorCommunicationObject/TlsServerSessionInfo
struct  TlsServerSessionInfo_t613821154  : public Il2CppObject
{
public:
	// System.String System.ServiceModel.Security.Tokens.SslAuthenticatorCommunicationObject/TlsServerSessionInfo::ContextId
	String_t* ___ContextId_0;
	// System.ServiceModel.Security.Tokens.TlsServerSession System.ServiceModel.Security.Tokens.SslAuthenticatorCommunicationObject/TlsServerSessionInfo::Tls
	TlsServerSession_t3338798525 * ___Tls_1;
	// System.IO.MemoryStream System.ServiceModel.Security.Tokens.SslAuthenticatorCommunicationObject/TlsServerSessionInfo::Messages
	MemoryStream_t94973147 * ___Messages_2;

public:
	inline static int32_t get_offset_of_ContextId_0() { return static_cast<int32_t>(offsetof(TlsServerSessionInfo_t613821154, ___ContextId_0)); }
	inline String_t* get_ContextId_0() const { return ___ContextId_0; }
	inline String_t** get_address_of_ContextId_0() { return &___ContextId_0; }
	inline void set_ContextId_0(String_t* value)
	{
		___ContextId_0 = value;
		Il2CppCodeGenWriteBarrier(&___ContextId_0, value);
	}

	inline static int32_t get_offset_of_Tls_1() { return static_cast<int32_t>(offsetof(TlsServerSessionInfo_t613821154, ___Tls_1)); }
	inline TlsServerSession_t3338798525 * get_Tls_1() const { return ___Tls_1; }
	inline TlsServerSession_t3338798525 ** get_address_of_Tls_1() { return &___Tls_1; }
	inline void set_Tls_1(TlsServerSession_t3338798525 * value)
	{
		___Tls_1 = value;
		Il2CppCodeGenWriteBarrier(&___Tls_1, value);
	}

	inline static int32_t get_offset_of_Messages_2() { return static_cast<int32_t>(offsetof(TlsServerSessionInfo_t613821154, ___Messages_2)); }
	inline MemoryStream_t94973147 * get_Messages_2() const { return ___Messages_2; }
	inline MemoryStream_t94973147 ** get_address_of_Messages_2() { return &___Messages_2; }
	inline void set_Messages_2(MemoryStream_t94973147 * value)
	{
		___Messages_2 = value;
		Il2CppCodeGenWriteBarrier(&___Messages_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
