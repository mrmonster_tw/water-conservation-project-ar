﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.timespec
struct  timespec_t893749660 
{
public:
	// System.Int32 System.IO.timespec::tv_sec
	int32_t ___tv_sec_0;
	// System.Int32 System.IO.timespec::tv_usec
	int32_t ___tv_usec_1;

public:
	inline static int32_t get_offset_of_tv_sec_0() { return static_cast<int32_t>(offsetof(timespec_t893749660, ___tv_sec_0)); }
	inline int32_t get_tv_sec_0() const { return ___tv_sec_0; }
	inline int32_t* get_address_of_tv_sec_0() { return &___tv_sec_0; }
	inline void set_tv_sec_0(int32_t value)
	{
		___tv_sec_0 = value;
	}

	inline static int32_t get_offset_of_tv_usec_1() { return static_cast<int32_t>(offsetof(timespec_t893749660, ___tv_usec_1)); }
	inline int32_t get_tv_usec_1() const { return ___tv_usec_1; }
	inline int32_t* get_address_of_tv_usec_1() { return &___tv_usec_1; }
	inline void set_tv_usec_1(int32_t value)
	{
		___tv_usec_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
