﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Dispatcher.DispatchRuntime
struct DispatchRuntime_t796075230;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.InstanceBehavior
struct  InstanceBehavior_t2611969614  : public Il2CppObject
{
public:
	// System.ServiceModel.Dispatcher.DispatchRuntime System.ServiceModel.Dispatcher.InstanceBehavior::dispatch_runtime
	DispatchRuntime_t796075230 * ___dispatch_runtime_0;

public:
	inline static int32_t get_offset_of_dispatch_runtime_0() { return static_cast<int32_t>(offsetof(InstanceBehavior_t2611969614, ___dispatch_runtime_0)); }
	inline DispatchRuntime_t796075230 * get_dispatch_runtime_0() const { return ___dispatch_runtime_0; }
	inline DispatchRuntime_t796075230 ** get_address_of_dispatch_runtime_0() { return &___dispatch_runtime_0; }
	inline void set_dispatch_runtime_0(DispatchRuntime_t796075230 * value)
	{
		___dispatch_runtime_0 = value;
		Il2CppCodeGenWriteBarrier(&___dispatch_runtime_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
