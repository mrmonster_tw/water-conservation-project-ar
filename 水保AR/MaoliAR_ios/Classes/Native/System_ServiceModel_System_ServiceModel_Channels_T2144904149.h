﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_B3643137745.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.TransportBindingElement
struct  TransportBindingElement_t2144904149  : public BindingElement_t3643137745
{
public:
	// System.Boolean System.ServiceModel.Channels.TransportBindingElement::manual_addressing
	bool ___manual_addressing_0;
	// System.Int64 System.ServiceModel.Channels.TransportBindingElement::max_buffer_pool_size
	int64_t ___max_buffer_pool_size_1;
	// System.Int64 System.ServiceModel.Channels.TransportBindingElement::max_recv_message_size
	int64_t ___max_recv_message_size_2;

public:
	inline static int32_t get_offset_of_manual_addressing_0() { return static_cast<int32_t>(offsetof(TransportBindingElement_t2144904149, ___manual_addressing_0)); }
	inline bool get_manual_addressing_0() const { return ___manual_addressing_0; }
	inline bool* get_address_of_manual_addressing_0() { return &___manual_addressing_0; }
	inline void set_manual_addressing_0(bool value)
	{
		___manual_addressing_0 = value;
	}

	inline static int32_t get_offset_of_max_buffer_pool_size_1() { return static_cast<int32_t>(offsetof(TransportBindingElement_t2144904149, ___max_buffer_pool_size_1)); }
	inline int64_t get_max_buffer_pool_size_1() const { return ___max_buffer_pool_size_1; }
	inline int64_t* get_address_of_max_buffer_pool_size_1() { return &___max_buffer_pool_size_1; }
	inline void set_max_buffer_pool_size_1(int64_t value)
	{
		___max_buffer_pool_size_1 = value;
	}

	inline static int32_t get_offset_of_max_recv_message_size_2() { return static_cast<int32_t>(offsetof(TransportBindingElement_t2144904149, ___max_recv_message_size_2)); }
	inline int64_t get_max_recv_message_size_2() const { return ___max_recv_message_size_2; }
	inline int64_t* get_address_of_max_recv_message_size_2() { return &___max_recv_message_size_2; }
	inline void set_max_recv_message_size_2(int64_t value)
	{
		___max_recv_message_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
