﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_Services_System_Web_Services_Descriptio2466402210.h"

// System.Web.Services.Description.OperationBinding
struct OperationBinding_t314325322;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.MessageBinding
struct  MessageBinding_t3764124827  : public NamedItem_t2466402210
{
public:
	// System.Web.Services.Description.OperationBinding System.Web.Services.Description.MessageBinding::operationBinding
	OperationBinding_t314325322 * ___operationBinding_4;

public:
	inline static int32_t get_offset_of_operationBinding_4() { return static_cast<int32_t>(offsetof(MessageBinding_t3764124827, ___operationBinding_4)); }
	inline OperationBinding_t314325322 * get_operationBinding_4() const { return ___operationBinding_4; }
	inline OperationBinding_t314325322 ** get_address_of_operationBinding_4() { return &___operationBinding_4; }
	inline void set_operationBinding_4(OperationBinding_t314325322 * value)
	{
		___operationBinding_4 = value;
		Il2CppCodeGenWriteBarrier(&___operationBinding_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
