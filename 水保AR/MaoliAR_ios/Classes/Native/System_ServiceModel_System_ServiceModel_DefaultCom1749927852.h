﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_TimeSpan881159249.h"

// System.ServiceModel.DefaultCommunicationTimeouts
struct DefaultCommunicationTimeouts_t1749927852;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.DefaultCommunicationTimeouts
struct  DefaultCommunicationTimeouts_t1749927852  : public Il2CppObject
{
public:
	// System.TimeSpan System.ServiceModel.DefaultCommunicationTimeouts::close
	TimeSpan_t881159249  ___close_1;
	// System.TimeSpan System.ServiceModel.DefaultCommunicationTimeouts::open
	TimeSpan_t881159249  ___open_2;
	// System.TimeSpan System.ServiceModel.DefaultCommunicationTimeouts::receive
	TimeSpan_t881159249  ___receive_3;
	// System.TimeSpan System.ServiceModel.DefaultCommunicationTimeouts::send
	TimeSpan_t881159249  ___send_4;

public:
	inline static int32_t get_offset_of_close_1() { return static_cast<int32_t>(offsetof(DefaultCommunicationTimeouts_t1749927852, ___close_1)); }
	inline TimeSpan_t881159249  get_close_1() const { return ___close_1; }
	inline TimeSpan_t881159249 * get_address_of_close_1() { return &___close_1; }
	inline void set_close_1(TimeSpan_t881159249  value)
	{
		___close_1 = value;
	}

	inline static int32_t get_offset_of_open_2() { return static_cast<int32_t>(offsetof(DefaultCommunicationTimeouts_t1749927852, ___open_2)); }
	inline TimeSpan_t881159249  get_open_2() const { return ___open_2; }
	inline TimeSpan_t881159249 * get_address_of_open_2() { return &___open_2; }
	inline void set_open_2(TimeSpan_t881159249  value)
	{
		___open_2 = value;
	}

	inline static int32_t get_offset_of_receive_3() { return static_cast<int32_t>(offsetof(DefaultCommunicationTimeouts_t1749927852, ___receive_3)); }
	inline TimeSpan_t881159249  get_receive_3() const { return ___receive_3; }
	inline TimeSpan_t881159249 * get_address_of_receive_3() { return &___receive_3; }
	inline void set_receive_3(TimeSpan_t881159249  value)
	{
		___receive_3 = value;
	}

	inline static int32_t get_offset_of_send_4() { return static_cast<int32_t>(offsetof(DefaultCommunicationTimeouts_t1749927852, ___send_4)); }
	inline TimeSpan_t881159249  get_send_4() const { return ___send_4; }
	inline TimeSpan_t881159249 * get_address_of_send_4() { return &___send_4; }
	inline void set_send_4(TimeSpan_t881159249  value)
	{
		___send_4 = value;
	}
};

struct DefaultCommunicationTimeouts_t1749927852_StaticFields
{
public:
	// System.ServiceModel.DefaultCommunicationTimeouts System.ServiceModel.DefaultCommunicationTimeouts::Instance
	DefaultCommunicationTimeouts_t1749927852 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(DefaultCommunicationTimeouts_t1749927852_StaticFields, ___Instance_0)); }
	inline DefaultCommunicationTimeouts_t1749927852 * get_Instance_0() const { return ___Instance_0; }
	inline DefaultCommunicationTimeouts_t1749927852 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(DefaultCommunicationTimeouts_t1749927852 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
