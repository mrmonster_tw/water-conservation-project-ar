﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.TagMapInfo
struct  TagMapInfo_t470982543  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct TagMapInfo_t470982543_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Web.Configuration.TagMapInfo::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.TagMapInfo::mappedTagTypeProp
	ConfigurationProperty_t3590861854 * ___mappedTagTypeProp_14;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.TagMapInfo::tagTypeProp
	ConfigurationProperty_t3590861854 * ___tagTypeProp_15;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(TagMapInfo_t470982543_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier(&___properties_13, value);
	}

	inline static int32_t get_offset_of_mappedTagTypeProp_14() { return static_cast<int32_t>(offsetof(TagMapInfo_t470982543_StaticFields, ___mappedTagTypeProp_14)); }
	inline ConfigurationProperty_t3590861854 * get_mappedTagTypeProp_14() const { return ___mappedTagTypeProp_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_mappedTagTypeProp_14() { return &___mappedTagTypeProp_14; }
	inline void set_mappedTagTypeProp_14(ConfigurationProperty_t3590861854 * value)
	{
		___mappedTagTypeProp_14 = value;
		Il2CppCodeGenWriteBarrier(&___mappedTagTypeProp_14, value);
	}

	inline static int32_t get_offset_of_tagTypeProp_15() { return static_cast<int32_t>(offsetof(TagMapInfo_t470982543_StaticFields, ___tagTypeProp_15)); }
	inline ConfigurationProperty_t3590861854 * get_tagTypeProp_15() const { return ___tagTypeProp_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_tagTypeProp_15() { return &___tagTypeProp_15; }
	inline void set_tagTypeProp_15(ConfigurationProperty_t3590861854 * value)
	{
		___tagTypeProp_15 = value;
		Il2CppCodeGenWriteBarrier(&___tagTypeProp_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
