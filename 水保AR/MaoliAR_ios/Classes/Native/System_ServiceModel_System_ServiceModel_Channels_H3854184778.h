﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_I1191478253.h"

// System.ServiceModel.Channels.HttpChannelListenerBase`1<System.ServiceModel.Channels.IReplyChannel>
struct HttpChannelListenerBase_1_t2430035905;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.HttpReplyChannel
struct  HttpReplyChannel_t3854184778  : public InternalReplyChannelBase_t1191478253
{
public:
	// System.ServiceModel.Channels.HttpChannelListenerBase`1<System.ServiceModel.Channels.IReplyChannel> System.ServiceModel.Channels.HttpReplyChannel::source
	HttpChannelListenerBase_1_t2430035905 * ___source_15;

public:
	inline static int32_t get_offset_of_source_15() { return static_cast<int32_t>(offsetof(HttpReplyChannel_t3854184778, ___source_15)); }
	inline HttpChannelListenerBase_1_t2430035905 * get_source_15() const { return ___source_15; }
	inline HttpChannelListenerBase_1_t2430035905 ** get_address_of_source_15() { return &___source_15; }
	inline void set_source_15(HttpChannelListenerBase_1_t2430035905 * value)
	{
		___source_15 = value;
		Il2CppCodeGenWriteBarrier(&___source_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
