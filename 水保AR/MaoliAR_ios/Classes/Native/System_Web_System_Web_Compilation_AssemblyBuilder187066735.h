﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.CodeDom.Compiler.CodeDomProvider
struct CodeDomProvider_t110349836;
// System.CodeDom.Compiler.CompilerParameters
struct CompilerParameters_t2325000967;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t4177511560;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Web.Compilation.CompileUnitPartialType>>
struct Dictionary_2_t500510178;
// System.Collections.Generic.Dictionary`2<System.String,System.Web.Compilation.BuildProvider>
struct Dictionary_2_t3521637304;
// System.Collections.Generic.List`1<System.Web.Compilation.AssemblyBuilder/CodeUnit>
struct List_1_t2992048114;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<System.Reflection.Assembly>
struct List_1_t1279540245;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.CodeDom.Compiler.TempFileCollection
struct TempFileCollection_t2060973068;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.AssemblyBuilder
struct  AssemblyBuilder_t187066735  : public Il2CppObject
{
public:
	// System.CodeDom.Compiler.CodeDomProvider System.Web.Compilation.AssemblyBuilder::provider
	CodeDomProvider_t110349836 * ___provider_1;
	// System.CodeDom.Compiler.CompilerParameters System.Web.Compilation.AssemblyBuilder::parameters
	CompilerParameters_t2325000967 * ___parameters_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Boolean> System.Web.Compilation.AssemblyBuilder::code_files
	Dictionary_2_t4177511560 * ___code_files_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Web.Compilation.CompileUnitPartialType>> System.Web.Compilation.AssemblyBuilder::partial_types
	Dictionary_2_t500510178 * ___partial_types_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Web.Compilation.BuildProvider> System.Web.Compilation.AssemblyBuilder::path_to_buildprovider
	Dictionary_2_t3521637304 * ___path_to_buildprovider_5;
	// System.Collections.Generic.List`1<System.Web.Compilation.AssemblyBuilder/CodeUnit> System.Web.Compilation.AssemblyBuilder::units
	List_1_t2992048114 * ___units_6;
	// System.Collections.Generic.List`1<System.String> System.Web.Compilation.AssemblyBuilder::source_files
	List_1_t3319525431 * ___source_files_7;
	// System.Collections.Generic.List`1<System.Reflection.Assembly> System.Web.Compilation.AssemblyBuilder::referenced_assemblies
	List_1_t1279540245 * ___referenced_assemblies_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> System.Web.Compilation.AssemblyBuilder::resource_files
	Dictionary_2_t1632706988 * ___resource_files_9;
	// System.CodeDom.Compiler.TempFileCollection System.Web.Compilation.AssemblyBuilder::temp_files
	TempFileCollection_t2060973068 * ___temp_files_10;
	// System.String System.Web.Compilation.AssemblyBuilder::outputFilesPrefix
	String_t* ___outputFilesPrefix_11;
	// System.String System.Web.Compilation.AssemblyBuilder::outputAssemblyPrefix
	String_t* ___outputAssemblyPrefix_12;
	// System.String System.Web.Compilation.AssemblyBuilder::outputAssemblyName
	String_t* ___outputAssemblyName_13;

public:
	inline static int32_t get_offset_of_provider_1() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t187066735, ___provider_1)); }
	inline CodeDomProvider_t110349836 * get_provider_1() const { return ___provider_1; }
	inline CodeDomProvider_t110349836 ** get_address_of_provider_1() { return &___provider_1; }
	inline void set_provider_1(CodeDomProvider_t110349836 * value)
	{
		___provider_1 = value;
		Il2CppCodeGenWriteBarrier(&___provider_1, value);
	}

	inline static int32_t get_offset_of_parameters_2() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t187066735, ___parameters_2)); }
	inline CompilerParameters_t2325000967 * get_parameters_2() const { return ___parameters_2; }
	inline CompilerParameters_t2325000967 ** get_address_of_parameters_2() { return &___parameters_2; }
	inline void set_parameters_2(CompilerParameters_t2325000967 * value)
	{
		___parameters_2 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_2, value);
	}

	inline static int32_t get_offset_of_code_files_3() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t187066735, ___code_files_3)); }
	inline Dictionary_2_t4177511560 * get_code_files_3() const { return ___code_files_3; }
	inline Dictionary_2_t4177511560 ** get_address_of_code_files_3() { return &___code_files_3; }
	inline void set_code_files_3(Dictionary_2_t4177511560 * value)
	{
		___code_files_3 = value;
		Il2CppCodeGenWriteBarrier(&___code_files_3, value);
	}

	inline static int32_t get_offset_of_partial_types_4() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t187066735, ___partial_types_4)); }
	inline Dictionary_2_t500510178 * get_partial_types_4() const { return ___partial_types_4; }
	inline Dictionary_2_t500510178 ** get_address_of_partial_types_4() { return &___partial_types_4; }
	inline void set_partial_types_4(Dictionary_2_t500510178 * value)
	{
		___partial_types_4 = value;
		Il2CppCodeGenWriteBarrier(&___partial_types_4, value);
	}

	inline static int32_t get_offset_of_path_to_buildprovider_5() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t187066735, ___path_to_buildprovider_5)); }
	inline Dictionary_2_t3521637304 * get_path_to_buildprovider_5() const { return ___path_to_buildprovider_5; }
	inline Dictionary_2_t3521637304 ** get_address_of_path_to_buildprovider_5() { return &___path_to_buildprovider_5; }
	inline void set_path_to_buildprovider_5(Dictionary_2_t3521637304 * value)
	{
		___path_to_buildprovider_5 = value;
		Il2CppCodeGenWriteBarrier(&___path_to_buildprovider_5, value);
	}

	inline static int32_t get_offset_of_units_6() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t187066735, ___units_6)); }
	inline List_1_t2992048114 * get_units_6() const { return ___units_6; }
	inline List_1_t2992048114 ** get_address_of_units_6() { return &___units_6; }
	inline void set_units_6(List_1_t2992048114 * value)
	{
		___units_6 = value;
		Il2CppCodeGenWriteBarrier(&___units_6, value);
	}

	inline static int32_t get_offset_of_source_files_7() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t187066735, ___source_files_7)); }
	inline List_1_t3319525431 * get_source_files_7() const { return ___source_files_7; }
	inline List_1_t3319525431 ** get_address_of_source_files_7() { return &___source_files_7; }
	inline void set_source_files_7(List_1_t3319525431 * value)
	{
		___source_files_7 = value;
		Il2CppCodeGenWriteBarrier(&___source_files_7, value);
	}

	inline static int32_t get_offset_of_referenced_assemblies_8() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t187066735, ___referenced_assemblies_8)); }
	inline List_1_t1279540245 * get_referenced_assemblies_8() const { return ___referenced_assemblies_8; }
	inline List_1_t1279540245 ** get_address_of_referenced_assemblies_8() { return &___referenced_assemblies_8; }
	inline void set_referenced_assemblies_8(List_1_t1279540245 * value)
	{
		___referenced_assemblies_8 = value;
		Il2CppCodeGenWriteBarrier(&___referenced_assemblies_8, value);
	}

	inline static int32_t get_offset_of_resource_files_9() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t187066735, ___resource_files_9)); }
	inline Dictionary_2_t1632706988 * get_resource_files_9() const { return ___resource_files_9; }
	inline Dictionary_2_t1632706988 ** get_address_of_resource_files_9() { return &___resource_files_9; }
	inline void set_resource_files_9(Dictionary_2_t1632706988 * value)
	{
		___resource_files_9 = value;
		Il2CppCodeGenWriteBarrier(&___resource_files_9, value);
	}

	inline static int32_t get_offset_of_temp_files_10() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t187066735, ___temp_files_10)); }
	inline TempFileCollection_t2060973068 * get_temp_files_10() const { return ___temp_files_10; }
	inline TempFileCollection_t2060973068 ** get_address_of_temp_files_10() { return &___temp_files_10; }
	inline void set_temp_files_10(TempFileCollection_t2060973068 * value)
	{
		___temp_files_10 = value;
		Il2CppCodeGenWriteBarrier(&___temp_files_10, value);
	}

	inline static int32_t get_offset_of_outputFilesPrefix_11() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t187066735, ___outputFilesPrefix_11)); }
	inline String_t* get_outputFilesPrefix_11() const { return ___outputFilesPrefix_11; }
	inline String_t** get_address_of_outputFilesPrefix_11() { return &___outputFilesPrefix_11; }
	inline void set_outputFilesPrefix_11(String_t* value)
	{
		___outputFilesPrefix_11 = value;
		Il2CppCodeGenWriteBarrier(&___outputFilesPrefix_11, value);
	}

	inline static int32_t get_offset_of_outputAssemblyPrefix_12() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t187066735, ___outputAssemblyPrefix_12)); }
	inline String_t* get_outputAssemblyPrefix_12() const { return ___outputAssemblyPrefix_12; }
	inline String_t** get_address_of_outputAssemblyPrefix_12() { return &___outputAssemblyPrefix_12; }
	inline void set_outputAssemblyPrefix_12(String_t* value)
	{
		___outputAssemblyPrefix_12 = value;
		Il2CppCodeGenWriteBarrier(&___outputAssemblyPrefix_12, value);
	}

	inline static int32_t get_offset_of_outputAssemblyName_13() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t187066735, ___outputAssemblyName_13)); }
	inline String_t* get_outputAssemblyName_13() const { return ___outputAssemblyName_13; }
	inline String_t** get_address_of_outputAssemblyName_13() { return &___outputAssemblyName_13; }
	inline void set_outputAssemblyName_13(String_t* value)
	{
		___outputAssemblyName_13 = value;
		Il2CppCodeGenWriteBarrier(&___outputAssemblyName_13, value);
	}
};

struct AssemblyBuilder_t187066735_StaticFields
{
public:
	// System.Boolean System.Web.Compilation.AssemblyBuilder::KeepFiles
	bool ___KeepFiles_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Web.Compilation.AssemblyBuilder::<>f__switch$map6
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map6_14;

public:
	inline static int32_t get_offset_of_KeepFiles_0() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t187066735_StaticFields, ___KeepFiles_0)); }
	inline bool get_KeepFiles_0() const { return ___KeepFiles_0; }
	inline bool* get_address_of_KeepFiles_0() { return &___KeepFiles_0; }
	inline void set_KeepFiles_0(bool value)
	{
		___KeepFiles_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map6_14() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t187066735_StaticFields, ___U3CU3Ef__switchU24map6_14)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map6_14() const { return ___U3CU3Ef__switchU24map6_14; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map6_14() { return &___U3CU3Ef__switchU24map6_14; }
	inline void set_U3CU3Ef__switchU24map6_14(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map6_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map6_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
