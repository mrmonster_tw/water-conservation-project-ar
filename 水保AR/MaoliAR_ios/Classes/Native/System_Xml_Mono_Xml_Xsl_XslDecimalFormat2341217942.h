﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t435877138;
// System.String
struct String_t;
// Mono.Xml.Xsl.XslDecimalFormat
struct XslDecimalFormat_t2341217942;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.XslDecimalFormat
struct  XslDecimalFormat_t2341217942  : public Il2CppObject
{
public:
	// System.Globalization.NumberFormatInfo Mono.Xml.Xsl.XslDecimalFormat::info
	NumberFormatInfo_t435877138 * ___info_0;
	// System.Char Mono.Xml.Xsl.XslDecimalFormat::digit
	Il2CppChar ___digit_1;
	// System.Char Mono.Xml.Xsl.XslDecimalFormat::zeroDigit
	Il2CppChar ___zeroDigit_2;
	// System.Char Mono.Xml.Xsl.XslDecimalFormat::patternSeparator
	Il2CppChar ___patternSeparator_3;
	// System.String Mono.Xml.Xsl.XslDecimalFormat::baseUri
	String_t* ___baseUri_4;
	// System.Int32 Mono.Xml.Xsl.XslDecimalFormat::lineNumber
	int32_t ___lineNumber_5;
	// System.Int32 Mono.Xml.Xsl.XslDecimalFormat::linePosition
	int32_t ___linePosition_6;

public:
	inline static int32_t get_offset_of_info_0() { return static_cast<int32_t>(offsetof(XslDecimalFormat_t2341217942, ___info_0)); }
	inline NumberFormatInfo_t435877138 * get_info_0() const { return ___info_0; }
	inline NumberFormatInfo_t435877138 ** get_address_of_info_0() { return &___info_0; }
	inline void set_info_0(NumberFormatInfo_t435877138 * value)
	{
		___info_0 = value;
		Il2CppCodeGenWriteBarrier(&___info_0, value);
	}

	inline static int32_t get_offset_of_digit_1() { return static_cast<int32_t>(offsetof(XslDecimalFormat_t2341217942, ___digit_1)); }
	inline Il2CppChar get_digit_1() const { return ___digit_1; }
	inline Il2CppChar* get_address_of_digit_1() { return &___digit_1; }
	inline void set_digit_1(Il2CppChar value)
	{
		___digit_1 = value;
	}

	inline static int32_t get_offset_of_zeroDigit_2() { return static_cast<int32_t>(offsetof(XslDecimalFormat_t2341217942, ___zeroDigit_2)); }
	inline Il2CppChar get_zeroDigit_2() const { return ___zeroDigit_2; }
	inline Il2CppChar* get_address_of_zeroDigit_2() { return &___zeroDigit_2; }
	inline void set_zeroDigit_2(Il2CppChar value)
	{
		___zeroDigit_2 = value;
	}

	inline static int32_t get_offset_of_patternSeparator_3() { return static_cast<int32_t>(offsetof(XslDecimalFormat_t2341217942, ___patternSeparator_3)); }
	inline Il2CppChar get_patternSeparator_3() const { return ___patternSeparator_3; }
	inline Il2CppChar* get_address_of_patternSeparator_3() { return &___patternSeparator_3; }
	inline void set_patternSeparator_3(Il2CppChar value)
	{
		___patternSeparator_3 = value;
	}

	inline static int32_t get_offset_of_baseUri_4() { return static_cast<int32_t>(offsetof(XslDecimalFormat_t2341217942, ___baseUri_4)); }
	inline String_t* get_baseUri_4() const { return ___baseUri_4; }
	inline String_t** get_address_of_baseUri_4() { return &___baseUri_4; }
	inline void set_baseUri_4(String_t* value)
	{
		___baseUri_4 = value;
		Il2CppCodeGenWriteBarrier(&___baseUri_4, value);
	}

	inline static int32_t get_offset_of_lineNumber_5() { return static_cast<int32_t>(offsetof(XslDecimalFormat_t2341217942, ___lineNumber_5)); }
	inline int32_t get_lineNumber_5() const { return ___lineNumber_5; }
	inline int32_t* get_address_of_lineNumber_5() { return &___lineNumber_5; }
	inline void set_lineNumber_5(int32_t value)
	{
		___lineNumber_5 = value;
	}

	inline static int32_t get_offset_of_linePosition_6() { return static_cast<int32_t>(offsetof(XslDecimalFormat_t2341217942, ___linePosition_6)); }
	inline int32_t get_linePosition_6() const { return ___linePosition_6; }
	inline int32_t* get_address_of_linePosition_6() { return &___linePosition_6; }
	inline void set_linePosition_6(int32_t value)
	{
		___linePosition_6 = value;
	}
};

struct XslDecimalFormat_t2341217942_StaticFields
{
public:
	// Mono.Xml.Xsl.XslDecimalFormat Mono.Xml.Xsl.XslDecimalFormat::Default
	XslDecimalFormat_t2341217942 * ___Default_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.XslDecimalFormat::<>f__switch$map1B
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map1B_8;

public:
	inline static int32_t get_offset_of_Default_7() { return static_cast<int32_t>(offsetof(XslDecimalFormat_t2341217942_StaticFields, ___Default_7)); }
	inline XslDecimalFormat_t2341217942 * get_Default_7() const { return ___Default_7; }
	inline XslDecimalFormat_t2341217942 ** get_address_of_Default_7() { return &___Default_7; }
	inline void set_Default_7(XslDecimalFormat_t2341217942 * value)
	{
		___Default_7 = value;
		Il2CppCodeGenWriteBarrier(&___Default_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1B_8() { return static_cast<int32_t>(offsetof(XslDecimalFormat_t2341217942_StaticFields, ___U3CU3Ef__switchU24map1B_8)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map1B_8() const { return ___U3CU3Ef__switchU24map1B_8; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map1B_8() { return &___U3CU3Ef__switchU24map1B_8; }
	inline void set_U3CU3Ef__switchU24map1B_8(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map1B_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1B_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
