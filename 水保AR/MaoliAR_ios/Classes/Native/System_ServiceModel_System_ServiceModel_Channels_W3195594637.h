﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Channels.WSSecurityMessageHeader
struct WSSecurityMessageHeader_t97321761;
// System.IdentityModel.Selectors.SecurityTokenSerializer
struct SecurityTokenSerializer_t972969391;
// System.IdentityModel.Selectors.SecurityTokenResolver
struct SecurityTokenResolver_t3905425829;
// System.Xml.XmlDocument
struct XmlDocument_t2837193595;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t418790500;
// System.Collections.Generic.List`1<System.IdentityModel.Tokens.SecurityToken>
struct List_1_t2743948282;
// System.Collections.Generic.Dictionary`2<System.String,System.Security.Cryptography.Xml.EncryptedData>
struct Dictionary_2_t2915132046;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.WSSecurityMessageHeaderReader
struct  WSSecurityMessageHeaderReader_t3195594637  : public Il2CppObject
{
public:
	// System.ServiceModel.Channels.WSSecurityMessageHeader System.ServiceModel.Channels.WSSecurityMessageHeaderReader::header
	WSSecurityMessageHeader_t97321761 * ___header_0;
	// System.IdentityModel.Selectors.SecurityTokenSerializer System.ServiceModel.Channels.WSSecurityMessageHeaderReader::serializer
	SecurityTokenSerializer_t972969391 * ___serializer_1;
	// System.IdentityModel.Selectors.SecurityTokenResolver System.ServiceModel.Channels.WSSecurityMessageHeaderReader::resolver
	SecurityTokenResolver_t3905425829 * ___resolver_2;
	// System.Xml.XmlDocument System.ServiceModel.Channels.WSSecurityMessageHeaderReader::doc
	XmlDocument_t2837193595 * ___doc_3;
	// System.Xml.XmlNamespaceManager System.ServiceModel.Channels.WSSecurityMessageHeaderReader::nsmgr
	XmlNamespaceManager_t418790500 * ___nsmgr_4;
	// System.Collections.Generic.List`1<System.IdentityModel.Tokens.SecurityToken> System.ServiceModel.Channels.WSSecurityMessageHeaderReader::tokens
	List_1_t2743948282 * ___tokens_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Security.Cryptography.Xml.EncryptedData> System.ServiceModel.Channels.WSSecurityMessageHeaderReader::encryptedDataList
	Dictionary_2_t2915132046 * ___encryptedDataList_6;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(WSSecurityMessageHeaderReader_t3195594637, ___header_0)); }
	inline WSSecurityMessageHeader_t97321761 * get_header_0() const { return ___header_0; }
	inline WSSecurityMessageHeader_t97321761 ** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(WSSecurityMessageHeader_t97321761 * value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier(&___header_0, value);
	}

	inline static int32_t get_offset_of_serializer_1() { return static_cast<int32_t>(offsetof(WSSecurityMessageHeaderReader_t3195594637, ___serializer_1)); }
	inline SecurityTokenSerializer_t972969391 * get_serializer_1() const { return ___serializer_1; }
	inline SecurityTokenSerializer_t972969391 ** get_address_of_serializer_1() { return &___serializer_1; }
	inline void set_serializer_1(SecurityTokenSerializer_t972969391 * value)
	{
		___serializer_1 = value;
		Il2CppCodeGenWriteBarrier(&___serializer_1, value);
	}

	inline static int32_t get_offset_of_resolver_2() { return static_cast<int32_t>(offsetof(WSSecurityMessageHeaderReader_t3195594637, ___resolver_2)); }
	inline SecurityTokenResolver_t3905425829 * get_resolver_2() const { return ___resolver_2; }
	inline SecurityTokenResolver_t3905425829 ** get_address_of_resolver_2() { return &___resolver_2; }
	inline void set_resolver_2(SecurityTokenResolver_t3905425829 * value)
	{
		___resolver_2 = value;
		Il2CppCodeGenWriteBarrier(&___resolver_2, value);
	}

	inline static int32_t get_offset_of_doc_3() { return static_cast<int32_t>(offsetof(WSSecurityMessageHeaderReader_t3195594637, ___doc_3)); }
	inline XmlDocument_t2837193595 * get_doc_3() const { return ___doc_3; }
	inline XmlDocument_t2837193595 ** get_address_of_doc_3() { return &___doc_3; }
	inline void set_doc_3(XmlDocument_t2837193595 * value)
	{
		___doc_3 = value;
		Il2CppCodeGenWriteBarrier(&___doc_3, value);
	}

	inline static int32_t get_offset_of_nsmgr_4() { return static_cast<int32_t>(offsetof(WSSecurityMessageHeaderReader_t3195594637, ___nsmgr_4)); }
	inline XmlNamespaceManager_t418790500 * get_nsmgr_4() const { return ___nsmgr_4; }
	inline XmlNamespaceManager_t418790500 ** get_address_of_nsmgr_4() { return &___nsmgr_4; }
	inline void set_nsmgr_4(XmlNamespaceManager_t418790500 * value)
	{
		___nsmgr_4 = value;
		Il2CppCodeGenWriteBarrier(&___nsmgr_4, value);
	}

	inline static int32_t get_offset_of_tokens_5() { return static_cast<int32_t>(offsetof(WSSecurityMessageHeaderReader_t3195594637, ___tokens_5)); }
	inline List_1_t2743948282 * get_tokens_5() const { return ___tokens_5; }
	inline List_1_t2743948282 ** get_address_of_tokens_5() { return &___tokens_5; }
	inline void set_tokens_5(List_1_t2743948282 * value)
	{
		___tokens_5 = value;
		Il2CppCodeGenWriteBarrier(&___tokens_5, value);
	}

	inline static int32_t get_offset_of_encryptedDataList_6() { return static_cast<int32_t>(offsetof(WSSecurityMessageHeaderReader_t3195594637, ___encryptedDataList_6)); }
	inline Dictionary_2_t2915132046 * get_encryptedDataList_6() const { return ___encryptedDataList_6; }
	inline Dictionary_2_t2915132046 ** get_address_of_encryptedDataList_6() { return &___encryptedDataList_6; }
	inline void set_encryptedDataList_6(Dictionary_2_t2915132046 * value)
	{
		___encryptedDataList_6 = value;
		Il2CppCodeGenWriteBarrier(&___encryptedDataList_6, value);
	}
};

struct WSSecurityMessageHeaderReader_t3195594637_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Channels.WSSecurityMessageHeaderReader::<>f__switch$map8
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map8_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Channels.WSSecurityMessageHeaderReader::<>f__switch$map9
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map9_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Channels.WSSecurityMessageHeaderReader::<>f__switch$mapA
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24mapA_9;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Channels.WSSecurityMessageHeaderReader::<>f__switch$mapB
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24mapB_10;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Channels.WSSecurityMessageHeaderReader::<>f__switch$mapC
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24mapC_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Channels.WSSecurityMessageHeaderReader::<>f__switch$mapD
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24mapD_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Channels.WSSecurityMessageHeaderReader::<>f__switch$mapE
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24mapE_13;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map8_7() { return static_cast<int32_t>(offsetof(WSSecurityMessageHeaderReader_t3195594637_StaticFields, ___U3CU3Ef__switchU24map8_7)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map8_7() const { return ___U3CU3Ef__switchU24map8_7; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map8_7() { return &___U3CU3Ef__switchU24map8_7; }
	inline void set_U3CU3Ef__switchU24map8_7(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map8_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map8_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map9_8() { return static_cast<int32_t>(offsetof(WSSecurityMessageHeaderReader_t3195594637_StaticFields, ___U3CU3Ef__switchU24map9_8)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map9_8() const { return ___U3CU3Ef__switchU24map9_8; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map9_8() { return &___U3CU3Ef__switchU24map9_8; }
	inline void set_U3CU3Ef__switchU24map9_8(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map9_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map9_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapA_9() { return static_cast<int32_t>(offsetof(WSSecurityMessageHeaderReader_t3195594637_StaticFields, ___U3CU3Ef__switchU24mapA_9)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24mapA_9() const { return ___U3CU3Ef__switchU24mapA_9; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24mapA_9() { return &___U3CU3Ef__switchU24mapA_9; }
	inline void set_U3CU3Ef__switchU24mapA_9(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24mapA_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapA_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapB_10() { return static_cast<int32_t>(offsetof(WSSecurityMessageHeaderReader_t3195594637_StaticFields, ___U3CU3Ef__switchU24mapB_10)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24mapB_10() const { return ___U3CU3Ef__switchU24mapB_10; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24mapB_10() { return &___U3CU3Ef__switchU24mapB_10; }
	inline void set_U3CU3Ef__switchU24mapB_10(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24mapB_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapB_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapC_11() { return static_cast<int32_t>(offsetof(WSSecurityMessageHeaderReader_t3195594637_StaticFields, ___U3CU3Ef__switchU24mapC_11)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24mapC_11() const { return ___U3CU3Ef__switchU24mapC_11; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24mapC_11() { return &___U3CU3Ef__switchU24mapC_11; }
	inline void set_U3CU3Ef__switchU24mapC_11(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24mapC_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapC_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapD_12() { return static_cast<int32_t>(offsetof(WSSecurityMessageHeaderReader_t3195594637_StaticFields, ___U3CU3Ef__switchU24mapD_12)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24mapD_12() const { return ___U3CU3Ef__switchU24mapD_12; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24mapD_12() { return &___U3CU3Ef__switchU24mapD_12; }
	inline void set_U3CU3Ef__switchU24mapD_12(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24mapD_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapD_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapE_13() { return static_cast<int32_t>(offsetof(WSSecurityMessageHeaderReader_t3195594637_StaticFields, ___U3CU3Ef__switchU24mapE_13)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24mapE_13() const { return ___U3CU3Ef__switchU24mapE_13; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24mapE_13() { return &___U3CU3Ef__switchU24mapE_13; }
	inline void set_U3CU3Ef__switchU24mapE_13(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24mapE_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapE_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
