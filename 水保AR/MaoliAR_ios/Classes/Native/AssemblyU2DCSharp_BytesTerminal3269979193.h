﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Vector22156229523.h"
#include "UnityEngine_UnityEngine_Rect2360479859.h"

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BytesTerminal
struct  BytesTerminal_t3269979193  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector2 BytesTerminal::scrollPosition
	Vector2_t2156229523  ___scrollPosition_2;
	// System.Boolean BytesTerminal::connected
	bool ___connected_3;
	// System.String BytesTerminal::messageToMC
	String_t* ___messageToMC_4;
	// System.Byte[] BytesTerminal::messageFromMC
	ByteU5BU5D_t4116647657* ___messageFromMC_5;
	// System.String BytesTerminal::controlData
	String_t* ___controlData_6;
	// System.Collections.Generic.List`1<System.String> BytesTerminal::messages
	List_1_t3319525431 * ___messages_7;
	// System.Int32 BytesTerminal::labelHeight
	int32_t ___labelHeight_8;
	// System.Int32 BytesTerminal::height
	int32_t ___height_9;
	// UnityEngine.Rect BytesTerminal::gRect
	Rect_t2360479859  ___gRect_10;

public:
	inline static int32_t get_offset_of_scrollPosition_2() { return static_cast<int32_t>(offsetof(BytesTerminal_t3269979193, ___scrollPosition_2)); }
	inline Vector2_t2156229523  get_scrollPosition_2() const { return ___scrollPosition_2; }
	inline Vector2_t2156229523 * get_address_of_scrollPosition_2() { return &___scrollPosition_2; }
	inline void set_scrollPosition_2(Vector2_t2156229523  value)
	{
		___scrollPosition_2 = value;
	}

	inline static int32_t get_offset_of_connected_3() { return static_cast<int32_t>(offsetof(BytesTerminal_t3269979193, ___connected_3)); }
	inline bool get_connected_3() const { return ___connected_3; }
	inline bool* get_address_of_connected_3() { return &___connected_3; }
	inline void set_connected_3(bool value)
	{
		___connected_3 = value;
	}

	inline static int32_t get_offset_of_messageToMC_4() { return static_cast<int32_t>(offsetof(BytesTerminal_t3269979193, ___messageToMC_4)); }
	inline String_t* get_messageToMC_4() const { return ___messageToMC_4; }
	inline String_t** get_address_of_messageToMC_4() { return &___messageToMC_4; }
	inline void set_messageToMC_4(String_t* value)
	{
		___messageToMC_4 = value;
		Il2CppCodeGenWriteBarrier(&___messageToMC_4, value);
	}

	inline static int32_t get_offset_of_messageFromMC_5() { return static_cast<int32_t>(offsetof(BytesTerminal_t3269979193, ___messageFromMC_5)); }
	inline ByteU5BU5D_t4116647657* get_messageFromMC_5() const { return ___messageFromMC_5; }
	inline ByteU5BU5D_t4116647657** get_address_of_messageFromMC_5() { return &___messageFromMC_5; }
	inline void set_messageFromMC_5(ByteU5BU5D_t4116647657* value)
	{
		___messageFromMC_5 = value;
		Il2CppCodeGenWriteBarrier(&___messageFromMC_5, value);
	}

	inline static int32_t get_offset_of_controlData_6() { return static_cast<int32_t>(offsetof(BytesTerminal_t3269979193, ___controlData_6)); }
	inline String_t* get_controlData_6() const { return ___controlData_6; }
	inline String_t** get_address_of_controlData_6() { return &___controlData_6; }
	inline void set_controlData_6(String_t* value)
	{
		___controlData_6 = value;
		Il2CppCodeGenWriteBarrier(&___controlData_6, value);
	}

	inline static int32_t get_offset_of_messages_7() { return static_cast<int32_t>(offsetof(BytesTerminal_t3269979193, ___messages_7)); }
	inline List_1_t3319525431 * get_messages_7() const { return ___messages_7; }
	inline List_1_t3319525431 ** get_address_of_messages_7() { return &___messages_7; }
	inline void set_messages_7(List_1_t3319525431 * value)
	{
		___messages_7 = value;
		Il2CppCodeGenWriteBarrier(&___messages_7, value);
	}

	inline static int32_t get_offset_of_labelHeight_8() { return static_cast<int32_t>(offsetof(BytesTerminal_t3269979193, ___labelHeight_8)); }
	inline int32_t get_labelHeight_8() const { return ___labelHeight_8; }
	inline int32_t* get_address_of_labelHeight_8() { return &___labelHeight_8; }
	inline void set_labelHeight_8(int32_t value)
	{
		___labelHeight_8 = value;
	}

	inline static int32_t get_offset_of_height_9() { return static_cast<int32_t>(offsetof(BytesTerminal_t3269979193, ___height_9)); }
	inline int32_t get_height_9() const { return ___height_9; }
	inline int32_t* get_address_of_height_9() { return &___height_9; }
	inline void set_height_9(int32_t value)
	{
		___height_9 = value;
	}

	inline static int32_t get_offset_of_gRect_10() { return static_cast<int32_t>(offsetof(BytesTerminal_t3269979193, ___gRect_10)); }
	inline Rect_t2360479859  get_gRect_10() const { return ___gRect_10; }
	inline Rect_t2360479859 * get_address_of_gRect_10() { return &___gRect_10; }
	inline void set_gRect_10(Rect_t2360479859  value)
	{
		___gRect_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
