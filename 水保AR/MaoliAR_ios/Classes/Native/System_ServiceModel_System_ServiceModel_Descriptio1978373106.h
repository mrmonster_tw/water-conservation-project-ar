﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_B3441673553.h"

// System.String
struct String_t;
// System.ServiceModel.Description.WspAppliesTo
struct WspAppliesTo_t2332734730;
// System.IdentityModel.Tokens.SecurityToken
struct SecurityToken_t1271873540;
// System.ServiceModel.Description.WstBinaryExchange
struct WstBinaryExchange_t134690608;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.WstRequestSecurityTokenBase
struct  WstRequestSecurityTokenBase_t1978373106  : public BodyWriter_t3441673553
{
public:
	// System.String System.ServiceModel.Description.WstRequestSecurityTokenBase::Context
	String_t* ___Context_1;
	// System.String System.ServiceModel.Description.WstRequestSecurityTokenBase::RequestType
	String_t* ___RequestType_2;
	// System.ServiceModel.Description.WspAppliesTo System.ServiceModel.Description.WstRequestSecurityTokenBase::AppliesTo
	WspAppliesTo_t2332734730 * ___AppliesTo_3;
	// System.IdentityModel.Tokens.SecurityToken System.ServiceModel.Description.WstRequestSecurityTokenBase::Entropy
	SecurityToken_t1271873540 * ___Entropy_4;
	// System.Int32 System.ServiceModel.Description.WstRequestSecurityTokenBase::KeySize
	int32_t ___KeySize_5;
	// System.String System.ServiceModel.Description.WstRequestSecurityTokenBase::KeyType
	String_t* ___KeyType_6;
	// System.String System.ServiceModel.Description.WstRequestSecurityTokenBase::ComputedKeyAlgorithm
	String_t* ___ComputedKeyAlgorithm_7;
	// System.ServiceModel.Description.WstBinaryExchange System.ServiceModel.Description.WstRequestSecurityTokenBase::BinaryExchange
	WstBinaryExchange_t134690608 * ___BinaryExchange_8;

public:
	inline static int32_t get_offset_of_Context_1() { return static_cast<int32_t>(offsetof(WstRequestSecurityTokenBase_t1978373106, ___Context_1)); }
	inline String_t* get_Context_1() const { return ___Context_1; }
	inline String_t** get_address_of_Context_1() { return &___Context_1; }
	inline void set_Context_1(String_t* value)
	{
		___Context_1 = value;
		Il2CppCodeGenWriteBarrier(&___Context_1, value);
	}

	inline static int32_t get_offset_of_RequestType_2() { return static_cast<int32_t>(offsetof(WstRequestSecurityTokenBase_t1978373106, ___RequestType_2)); }
	inline String_t* get_RequestType_2() const { return ___RequestType_2; }
	inline String_t** get_address_of_RequestType_2() { return &___RequestType_2; }
	inline void set_RequestType_2(String_t* value)
	{
		___RequestType_2 = value;
		Il2CppCodeGenWriteBarrier(&___RequestType_2, value);
	}

	inline static int32_t get_offset_of_AppliesTo_3() { return static_cast<int32_t>(offsetof(WstRequestSecurityTokenBase_t1978373106, ___AppliesTo_3)); }
	inline WspAppliesTo_t2332734730 * get_AppliesTo_3() const { return ___AppliesTo_3; }
	inline WspAppliesTo_t2332734730 ** get_address_of_AppliesTo_3() { return &___AppliesTo_3; }
	inline void set_AppliesTo_3(WspAppliesTo_t2332734730 * value)
	{
		___AppliesTo_3 = value;
		Il2CppCodeGenWriteBarrier(&___AppliesTo_3, value);
	}

	inline static int32_t get_offset_of_Entropy_4() { return static_cast<int32_t>(offsetof(WstRequestSecurityTokenBase_t1978373106, ___Entropy_4)); }
	inline SecurityToken_t1271873540 * get_Entropy_4() const { return ___Entropy_4; }
	inline SecurityToken_t1271873540 ** get_address_of_Entropy_4() { return &___Entropy_4; }
	inline void set_Entropy_4(SecurityToken_t1271873540 * value)
	{
		___Entropy_4 = value;
		Il2CppCodeGenWriteBarrier(&___Entropy_4, value);
	}

	inline static int32_t get_offset_of_KeySize_5() { return static_cast<int32_t>(offsetof(WstRequestSecurityTokenBase_t1978373106, ___KeySize_5)); }
	inline int32_t get_KeySize_5() const { return ___KeySize_5; }
	inline int32_t* get_address_of_KeySize_5() { return &___KeySize_5; }
	inline void set_KeySize_5(int32_t value)
	{
		___KeySize_5 = value;
	}

	inline static int32_t get_offset_of_KeyType_6() { return static_cast<int32_t>(offsetof(WstRequestSecurityTokenBase_t1978373106, ___KeyType_6)); }
	inline String_t* get_KeyType_6() const { return ___KeyType_6; }
	inline String_t** get_address_of_KeyType_6() { return &___KeyType_6; }
	inline void set_KeyType_6(String_t* value)
	{
		___KeyType_6 = value;
		Il2CppCodeGenWriteBarrier(&___KeyType_6, value);
	}

	inline static int32_t get_offset_of_ComputedKeyAlgorithm_7() { return static_cast<int32_t>(offsetof(WstRequestSecurityTokenBase_t1978373106, ___ComputedKeyAlgorithm_7)); }
	inline String_t* get_ComputedKeyAlgorithm_7() const { return ___ComputedKeyAlgorithm_7; }
	inline String_t** get_address_of_ComputedKeyAlgorithm_7() { return &___ComputedKeyAlgorithm_7; }
	inline void set_ComputedKeyAlgorithm_7(String_t* value)
	{
		___ComputedKeyAlgorithm_7 = value;
		Il2CppCodeGenWriteBarrier(&___ComputedKeyAlgorithm_7, value);
	}

	inline static int32_t get_offset_of_BinaryExchange_8() { return static_cast<int32_t>(offsetof(WstRequestSecurityTokenBase_t1978373106, ___BinaryExchange_8)); }
	inline WstBinaryExchange_t134690608 * get_BinaryExchange_8() const { return ___BinaryExchange_8; }
	inline WstBinaryExchange_t134690608 ** get_address_of_BinaryExchange_8() { return &___BinaryExchange_8; }
	inline void set_BinaryExchange_8(WstBinaryExchange_t134690608 * value)
	{
		___BinaryExchange_8 = value;
		Il2CppCodeGenWriteBarrier(&___BinaryExchange_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
