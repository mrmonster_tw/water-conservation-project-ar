﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.Configuration.SessionStateSection
struct SessionStateSection_t500006340;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.SessionState.SessionIDManager
struct  SessionIDManager_t3564083178  : public Il2CppObject
{
public:
	// System.Web.Configuration.SessionStateSection System.Web.SessionState.SessionIDManager::config
	SessionStateSection_t500006340 * ___config_0;

public:
	inline static int32_t get_offset_of_config_0() { return static_cast<int32_t>(offsetof(SessionIDManager_t3564083178, ___config_0)); }
	inline SessionStateSection_t500006340 * get_config_0() const { return ___config_0; }
	inline SessionStateSection_t500006340 ** get_address_of_config_0() { return &___config_0; }
	inline void set_config_0(SessionStateSection_t500006340 * value)
	{
		___config_0 = value;
		Il2CppCodeGenWriteBarrier(&___config_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
