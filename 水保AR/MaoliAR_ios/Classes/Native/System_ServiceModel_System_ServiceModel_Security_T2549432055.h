﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Security_To589097440.h"

// System.ServiceModel.Security.ServiceCredentialsSecurityTokenManager
struct ServiceCredentialsSecurityTokenManager_t557944679;
// System.ServiceModel.Security.Tokens.SslAuthenticatorCommunicationObject
struct SslAuthenticatorCommunicationObject_t1333352365;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.SslSecurityTokenAuthenticator
struct  SslSecurityTokenAuthenticator_t2549432055  : public CommunicationSecurityTokenAuthenticator_t589097440
{
public:
	// System.ServiceModel.Security.ServiceCredentialsSecurityTokenManager System.ServiceModel.Security.Tokens.SslSecurityTokenAuthenticator::manager
	ServiceCredentialsSecurityTokenManager_t557944679 * ___manager_1;
	// System.ServiceModel.Security.Tokens.SslAuthenticatorCommunicationObject System.ServiceModel.Security.Tokens.SslSecurityTokenAuthenticator::comm
	SslAuthenticatorCommunicationObject_t1333352365 * ___comm_2;
	// System.Boolean System.ServiceModel.Security.Tokens.SslSecurityTokenAuthenticator::mutual
	bool ___mutual_3;

public:
	inline static int32_t get_offset_of_manager_1() { return static_cast<int32_t>(offsetof(SslSecurityTokenAuthenticator_t2549432055, ___manager_1)); }
	inline ServiceCredentialsSecurityTokenManager_t557944679 * get_manager_1() const { return ___manager_1; }
	inline ServiceCredentialsSecurityTokenManager_t557944679 ** get_address_of_manager_1() { return &___manager_1; }
	inline void set_manager_1(ServiceCredentialsSecurityTokenManager_t557944679 * value)
	{
		___manager_1 = value;
		Il2CppCodeGenWriteBarrier(&___manager_1, value);
	}

	inline static int32_t get_offset_of_comm_2() { return static_cast<int32_t>(offsetof(SslSecurityTokenAuthenticator_t2549432055, ___comm_2)); }
	inline SslAuthenticatorCommunicationObject_t1333352365 * get_comm_2() const { return ___comm_2; }
	inline SslAuthenticatorCommunicationObject_t1333352365 ** get_address_of_comm_2() { return &___comm_2; }
	inline void set_comm_2(SslAuthenticatorCommunicationObject_t1333352365 * value)
	{
		___comm_2 = value;
		Il2CppCodeGenWriteBarrier(&___comm_2, value);
	}

	inline static int32_t get_offset_of_mutual_3() { return static_cast<int32_t>(offsetof(SslSecurityTokenAuthenticator_t2549432055, ___mutual_3)); }
	inline bool get_mutual_3() const { return ___mutual_3; }
	inline bool* get_address_of_mutual_3() { return &___mutual_3; }
	inline void set_mutual_3(bool value)
	{
		___mutual_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
