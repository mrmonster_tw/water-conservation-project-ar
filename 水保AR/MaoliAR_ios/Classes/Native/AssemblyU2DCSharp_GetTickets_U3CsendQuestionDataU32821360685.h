﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String[]
struct StringU5BU5D_t1281789340;
// UnityEngine.UI.Text
struct Text_t1901882714;
// GetTickets
struct GetTickets_t911946028;
// System.Object
struct Il2CppObject;
// System.Predicate`1<UnityEngine.UI.Text>
struct Predicate_1_t2727176838;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetTickets/<sendQuestionData>c__Iterator3
struct  U3CsendQuestionDataU3Ec__Iterator3_t2821360685  : public Il2CppObject
{
public:
	// System.String[] GetTickets/<sendQuestionData>c__Iterator3::<questionResult>__0
	StringU5BU5D_t1281789340* ___U3CquestionResultU3E__0_0;
	// UnityEngine.UI.Text GetTickets/<sendQuestionData>c__Iterator3::<message>__0
	Text_t1901882714 * ___U3CmessageU3E__0_1;
	// System.Single GetTickets/<sendQuestionData>c__Iterator3::<nowTime>__1
	float ___U3CnowTimeU3E__1_2;
	// GetTickets GetTickets/<sendQuestionData>c__Iterator3::$this
	GetTickets_t911946028 * ___U24this_3;
	// System.Object GetTickets/<sendQuestionData>c__Iterator3::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean GetTickets/<sendQuestionData>c__Iterator3::$disposing
	bool ___U24disposing_5;
	// System.Int32 GetTickets/<sendQuestionData>c__Iterator3::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CquestionResultU3E__0_0() { return static_cast<int32_t>(offsetof(U3CsendQuestionDataU3Ec__Iterator3_t2821360685, ___U3CquestionResultU3E__0_0)); }
	inline StringU5BU5D_t1281789340* get_U3CquestionResultU3E__0_0() const { return ___U3CquestionResultU3E__0_0; }
	inline StringU5BU5D_t1281789340** get_address_of_U3CquestionResultU3E__0_0() { return &___U3CquestionResultU3E__0_0; }
	inline void set_U3CquestionResultU3E__0_0(StringU5BU5D_t1281789340* value)
	{
		___U3CquestionResultU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CquestionResultU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CmessageU3E__0_1() { return static_cast<int32_t>(offsetof(U3CsendQuestionDataU3Ec__Iterator3_t2821360685, ___U3CmessageU3E__0_1)); }
	inline Text_t1901882714 * get_U3CmessageU3E__0_1() const { return ___U3CmessageU3E__0_1; }
	inline Text_t1901882714 ** get_address_of_U3CmessageU3E__0_1() { return &___U3CmessageU3E__0_1; }
	inline void set_U3CmessageU3E__0_1(Text_t1901882714 * value)
	{
		___U3CmessageU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmessageU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CnowTimeU3E__1_2() { return static_cast<int32_t>(offsetof(U3CsendQuestionDataU3Ec__Iterator3_t2821360685, ___U3CnowTimeU3E__1_2)); }
	inline float get_U3CnowTimeU3E__1_2() const { return ___U3CnowTimeU3E__1_2; }
	inline float* get_address_of_U3CnowTimeU3E__1_2() { return &___U3CnowTimeU3E__1_2; }
	inline void set_U3CnowTimeU3E__1_2(float value)
	{
		___U3CnowTimeU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CsendQuestionDataU3Ec__Iterator3_t2821360685, ___U24this_3)); }
	inline GetTickets_t911946028 * get_U24this_3() const { return ___U24this_3; }
	inline GetTickets_t911946028 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(GetTickets_t911946028 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CsendQuestionDataU3Ec__Iterator3_t2821360685, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CsendQuestionDataU3Ec__Iterator3_t2821360685, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CsendQuestionDataU3Ec__Iterator3_t2821360685, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

struct U3CsendQuestionDataU3Ec__Iterator3_t2821360685_StaticFields
{
public:
	// System.Predicate`1<UnityEngine.UI.Text> GetTickets/<sendQuestionData>c__Iterator3::<>f__am$cache0
	Predicate_1_t2727176838 * ___U3CU3Ef__amU24cache0_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(U3CsendQuestionDataU3Ec__Iterator3_t2821360685_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Predicate_1_t2727176838 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Predicate_1_t2727176838 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Predicate_1_t2727176838 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
