﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// UnityEngine.Texture
struct Texture_t3661962703;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenPixel
struct  ScreenPixel_t1488455970  : public Il2CppObject
{
public:
	// UnityEngine.Texture ScreenPixel::tex
	Texture_t3661962703 * ___tex_0;

public:
	inline static int32_t get_offset_of_tex_0() { return static_cast<int32_t>(offsetof(ScreenPixel_t1488455970, ___tex_0)); }
	inline Texture_t3661962703 * get_tex_0() const { return ___tex_0; }
	inline Texture_t3661962703 ** get_address_of_tex_0() { return &___tex_0; }
	inline void set_tex_0(Texture_t3661962703 * value)
	{
		___tex_0 = value;
		Il2CppCodeGenWriteBarrier(&___tex_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
