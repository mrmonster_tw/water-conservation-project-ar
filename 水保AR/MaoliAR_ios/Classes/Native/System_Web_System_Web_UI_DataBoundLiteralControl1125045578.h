﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_Control3006474639.h"

// System.String[]
struct StringU5BU5D_t1281789340;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.DataBoundLiteralControl
struct  DataBoundLiteralControl_t1125045578  : public Control_t3006474639
{
public:
	// System.String[] System.Web.UI.DataBoundLiteralControl::staticLiterals
	StringU5BU5D_t1281789340* ___staticLiterals_28;
	// System.String[] System.Web.UI.DataBoundLiteralControl::dataBoundLiterals
	StringU5BU5D_t1281789340* ___dataBoundLiterals_29;

public:
	inline static int32_t get_offset_of_staticLiterals_28() { return static_cast<int32_t>(offsetof(DataBoundLiteralControl_t1125045578, ___staticLiterals_28)); }
	inline StringU5BU5D_t1281789340* get_staticLiterals_28() const { return ___staticLiterals_28; }
	inline StringU5BU5D_t1281789340** get_address_of_staticLiterals_28() { return &___staticLiterals_28; }
	inline void set_staticLiterals_28(StringU5BU5D_t1281789340* value)
	{
		___staticLiterals_28 = value;
		Il2CppCodeGenWriteBarrier(&___staticLiterals_28, value);
	}

	inline static int32_t get_offset_of_dataBoundLiterals_29() { return static_cast<int32_t>(offsetof(DataBoundLiteralControl_t1125045578, ___dataBoundLiterals_29)); }
	inline StringU5BU5D_t1281789340* get_dataBoundLiterals_29() const { return ___dataBoundLiterals_29; }
	inline StringU5BU5D_t1281789340** get_address_of_dataBoundLiterals_29() { return &___dataBoundLiterals_29; }
	inline void set_dataBoundLiterals_29(StringU5BU5D_t1281789340* value)
	{
		___dataBoundLiterals_29 = value;
		Il2CppCodeGenWriteBarrier(&___dataBoundLiterals_29, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
