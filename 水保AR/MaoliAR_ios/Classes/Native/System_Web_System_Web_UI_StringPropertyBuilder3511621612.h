﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_ControlBuilder2523018631.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.StringPropertyBuilder
struct  StringPropertyBuilder_t3511621612  : public ControlBuilder_t2523018631
{
public:
	// System.String System.Web.UI.StringPropertyBuilder::prop_name
	String_t* ___prop_name_28;

public:
	inline static int32_t get_offset_of_prop_name_28() { return static_cast<int32_t>(offsetof(StringPropertyBuilder_t3511621612, ___prop_name_28)); }
	inline String_t* get_prop_name_28() const { return ___prop_name_28; }
	inline String_t** get_address_of_prop_name_28() { return &___prop_name_28; }
	inline void set_prop_name_28(String_t* value)
	{
		___prop_name_28 = value;
		Il2CppCodeGenWriteBarrier(&___prop_name_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
