﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_Control3006474639.h"
#include "System_Web_System_Web_UI_HtmlTextWriterTag4267392773.h"

// System.Web.UI.WebControls.Style
struct Style_t3589053173;
// System.String
struct String_t;
// System.Web.UI.AttributeCollection
struct AttributeCollection_t3488369622;
// System.Web.UI.StateBag
struct StateBag_t282928164;
// System.Char[]
struct CharU5BU5D_t3528271667;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.WebControls.WebControl
struct  WebControl_t2705811671  : public Control_t3006474639
{
public:
	// System.Web.UI.WebControls.Style System.Web.UI.WebControls.WebControl::style
	Style_t3589053173 * ___style_28;
	// System.Web.UI.HtmlTextWriterTag System.Web.UI.WebControls.WebControl::tag
	int32_t ___tag_29;
	// System.String System.Web.UI.WebControls.WebControl::tag_name
	String_t* ___tag_name_30;
	// System.Web.UI.AttributeCollection System.Web.UI.WebControls.WebControl::attributes
	AttributeCollection_t3488369622 * ___attributes_31;
	// System.Web.UI.StateBag System.Web.UI.WebControls.WebControl::attribute_state
	StateBag_t282928164 * ___attribute_state_32;
	// System.Boolean System.Web.UI.WebControls.WebControl::enabled
	bool ___enabled_33;
	// System.Boolean System.Web.UI.WebControls.WebControl::track_enabled_state
	bool ___track_enabled_state_34;

public:
	inline static int32_t get_offset_of_style_28() { return static_cast<int32_t>(offsetof(WebControl_t2705811671, ___style_28)); }
	inline Style_t3589053173 * get_style_28() const { return ___style_28; }
	inline Style_t3589053173 ** get_address_of_style_28() { return &___style_28; }
	inline void set_style_28(Style_t3589053173 * value)
	{
		___style_28 = value;
		Il2CppCodeGenWriteBarrier(&___style_28, value);
	}

	inline static int32_t get_offset_of_tag_29() { return static_cast<int32_t>(offsetof(WebControl_t2705811671, ___tag_29)); }
	inline int32_t get_tag_29() const { return ___tag_29; }
	inline int32_t* get_address_of_tag_29() { return &___tag_29; }
	inline void set_tag_29(int32_t value)
	{
		___tag_29 = value;
	}

	inline static int32_t get_offset_of_tag_name_30() { return static_cast<int32_t>(offsetof(WebControl_t2705811671, ___tag_name_30)); }
	inline String_t* get_tag_name_30() const { return ___tag_name_30; }
	inline String_t** get_address_of_tag_name_30() { return &___tag_name_30; }
	inline void set_tag_name_30(String_t* value)
	{
		___tag_name_30 = value;
		Il2CppCodeGenWriteBarrier(&___tag_name_30, value);
	}

	inline static int32_t get_offset_of_attributes_31() { return static_cast<int32_t>(offsetof(WebControl_t2705811671, ___attributes_31)); }
	inline AttributeCollection_t3488369622 * get_attributes_31() const { return ___attributes_31; }
	inline AttributeCollection_t3488369622 ** get_address_of_attributes_31() { return &___attributes_31; }
	inline void set_attributes_31(AttributeCollection_t3488369622 * value)
	{
		___attributes_31 = value;
		Il2CppCodeGenWriteBarrier(&___attributes_31, value);
	}

	inline static int32_t get_offset_of_attribute_state_32() { return static_cast<int32_t>(offsetof(WebControl_t2705811671, ___attribute_state_32)); }
	inline StateBag_t282928164 * get_attribute_state_32() const { return ___attribute_state_32; }
	inline StateBag_t282928164 ** get_address_of_attribute_state_32() { return &___attribute_state_32; }
	inline void set_attribute_state_32(StateBag_t282928164 * value)
	{
		___attribute_state_32 = value;
		Il2CppCodeGenWriteBarrier(&___attribute_state_32, value);
	}

	inline static int32_t get_offset_of_enabled_33() { return static_cast<int32_t>(offsetof(WebControl_t2705811671, ___enabled_33)); }
	inline bool get_enabled_33() const { return ___enabled_33; }
	inline bool* get_address_of_enabled_33() { return &___enabled_33; }
	inline void set_enabled_33(bool value)
	{
		___enabled_33 = value;
	}

	inline static int32_t get_offset_of_track_enabled_state_34() { return static_cast<int32_t>(offsetof(WebControl_t2705811671, ___track_enabled_state_34)); }
	inline bool get_track_enabled_state_34() const { return ___track_enabled_state_34; }
	inline bool* get_address_of_track_enabled_state_34() { return &___track_enabled_state_34; }
	inline void set_track_enabled_state_34(bool value)
	{
		___track_enabled_state_34 = value;
	}
};

struct WebControl_t2705811671_StaticFields
{
public:
	// System.Char[] System.Web.UI.WebControls.WebControl::_script_trim_chars
	CharU5BU5D_t3528271667* ____script_trim_chars_35;

public:
	inline static int32_t get_offset_of__script_trim_chars_35() { return static_cast<int32_t>(offsetof(WebControl_t2705811671_StaticFields, ____script_trim_chars_35)); }
	inline CharU5BU5D_t3528271667* get__script_trim_chars_35() const { return ____script_trim_chars_35; }
	inline CharU5BU5D_t3528271667** get_address_of__script_trim_chars_35() { return &____script_trim_chars_35; }
	inline void set__script_trim_chars_35(CharU5BU5D_t3528271667* value)
	{
		____script_trim_chars_35 = value;
		Il2CppCodeGenWriteBarrier(&____script_trim_chars_35, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
