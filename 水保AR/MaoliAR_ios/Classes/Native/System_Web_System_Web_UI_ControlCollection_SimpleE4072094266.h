﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.UI.ControlCollection
struct ControlCollection_t4212191938;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.ControlCollection/SimpleEnumerator
struct  SimpleEnumerator_t4072094266  : public Il2CppObject
{
public:
	// System.Web.UI.ControlCollection System.Web.UI.ControlCollection/SimpleEnumerator::coll
	ControlCollection_t4212191938 * ___coll_0;
	// System.Int32 System.Web.UI.ControlCollection/SimpleEnumerator::index
	int32_t ___index_1;
	// System.Int32 System.Web.UI.ControlCollection/SimpleEnumerator::version
	int32_t ___version_2;
	// System.Object System.Web.UI.ControlCollection/SimpleEnumerator::currentElement
	Il2CppObject * ___currentElement_3;

public:
	inline static int32_t get_offset_of_coll_0() { return static_cast<int32_t>(offsetof(SimpleEnumerator_t4072094266, ___coll_0)); }
	inline ControlCollection_t4212191938 * get_coll_0() const { return ___coll_0; }
	inline ControlCollection_t4212191938 ** get_address_of_coll_0() { return &___coll_0; }
	inline void set_coll_0(ControlCollection_t4212191938 * value)
	{
		___coll_0 = value;
		Il2CppCodeGenWriteBarrier(&___coll_0, value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(SimpleEnumerator_t4072094266, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(SimpleEnumerator_t4072094266, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_currentElement_3() { return static_cast<int32_t>(offsetof(SimpleEnumerator_t4072094266, ___currentElement_3)); }
	inline Il2CppObject * get_currentElement_3() const { return ___currentElement_3; }
	inline Il2CppObject ** get_address_of_currentElement_3() { return &___currentElement_3; }
	inline void set_currentElement_3(Il2CppObject * value)
	{
		___currentElement_3 = value;
		Il2CppCodeGenWriteBarrier(&___currentElement_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
