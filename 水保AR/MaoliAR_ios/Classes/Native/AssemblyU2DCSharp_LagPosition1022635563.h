﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Vector33722313464.h"

// UnityEngine.Transform
struct Transform_t3600365921;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LagPosition
struct  LagPosition_t1022635563  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 LagPosition::speed
	Vector3_t3722313464  ___speed_2;
	// System.Boolean LagPosition::ignoreTimeScale
	bool ___ignoreTimeScale_3;
	// UnityEngine.Transform LagPosition::mTrans
	Transform_t3600365921 * ___mTrans_4;
	// UnityEngine.Vector3 LagPosition::mRelative
	Vector3_t3722313464  ___mRelative_5;
	// UnityEngine.Vector3 LagPosition::mAbsolute
	Vector3_t3722313464  ___mAbsolute_6;
	// System.Boolean LagPosition::mStarted
	bool ___mStarted_7;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(LagPosition_t1022635563, ___speed_2)); }
	inline Vector3_t3722313464  get_speed_2() const { return ___speed_2; }
	inline Vector3_t3722313464 * get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(Vector3_t3722313464  value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_ignoreTimeScale_3() { return static_cast<int32_t>(offsetof(LagPosition_t1022635563, ___ignoreTimeScale_3)); }
	inline bool get_ignoreTimeScale_3() const { return ___ignoreTimeScale_3; }
	inline bool* get_address_of_ignoreTimeScale_3() { return &___ignoreTimeScale_3; }
	inline void set_ignoreTimeScale_3(bool value)
	{
		___ignoreTimeScale_3 = value;
	}

	inline static int32_t get_offset_of_mTrans_4() { return static_cast<int32_t>(offsetof(LagPosition_t1022635563, ___mTrans_4)); }
	inline Transform_t3600365921 * get_mTrans_4() const { return ___mTrans_4; }
	inline Transform_t3600365921 ** get_address_of_mTrans_4() { return &___mTrans_4; }
	inline void set_mTrans_4(Transform_t3600365921 * value)
	{
		___mTrans_4 = value;
		Il2CppCodeGenWriteBarrier(&___mTrans_4, value);
	}

	inline static int32_t get_offset_of_mRelative_5() { return static_cast<int32_t>(offsetof(LagPosition_t1022635563, ___mRelative_5)); }
	inline Vector3_t3722313464  get_mRelative_5() const { return ___mRelative_5; }
	inline Vector3_t3722313464 * get_address_of_mRelative_5() { return &___mRelative_5; }
	inline void set_mRelative_5(Vector3_t3722313464  value)
	{
		___mRelative_5 = value;
	}

	inline static int32_t get_offset_of_mAbsolute_6() { return static_cast<int32_t>(offsetof(LagPosition_t1022635563, ___mAbsolute_6)); }
	inline Vector3_t3722313464  get_mAbsolute_6() const { return ___mAbsolute_6; }
	inline Vector3_t3722313464 * get_address_of_mAbsolute_6() { return &___mAbsolute_6; }
	inline void set_mAbsolute_6(Vector3_t3722313464  value)
	{
		___mAbsolute_6 = value;
	}

	inline static int32_t get_offset_of_mStarted_7() { return static_cast<int32_t>(offsetof(LagPosition_t1022635563, ___mStarted_7)); }
	inline bool get_mStarted_7() const { return ___mStarted_7; }
	inline bool* get_address_of_mStarted_7() { return &___mStarted_7; }
	inline void set_mStarted_7(bool value)
	{
		___mStarted_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
