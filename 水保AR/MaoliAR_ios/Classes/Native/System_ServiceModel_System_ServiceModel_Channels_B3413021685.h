﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_Collections_Generic_Key1429017627.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.BindingParameterCollection
struct  BindingParameterCollection_t3413021685  : public KeyedByTypeCollection_1_t1429017627
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
