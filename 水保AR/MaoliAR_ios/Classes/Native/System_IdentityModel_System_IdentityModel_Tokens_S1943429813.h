﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4116647657;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Tokens.SecurityKeyIdentifierClause
struct  SecurityKeyIdentifierClause_t1943429813  : public Il2CppObject
{
public:
	// System.String System.IdentityModel.Tokens.SecurityKeyIdentifierClause::clause_type
	String_t* ___clause_type_0;
	// System.Byte[] System.IdentityModel.Tokens.SecurityKeyIdentifierClause::nonce
	ByteU5BU5D_t4116647657* ___nonce_1;
	// System.Int32 System.IdentityModel.Tokens.SecurityKeyIdentifierClause::deriv_length
	int32_t ___deriv_length_2;

public:
	inline static int32_t get_offset_of_clause_type_0() { return static_cast<int32_t>(offsetof(SecurityKeyIdentifierClause_t1943429813, ___clause_type_0)); }
	inline String_t* get_clause_type_0() const { return ___clause_type_0; }
	inline String_t** get_address_of_clause_type_0() { return &___clause_type_0; }
	inline void set_clause_type_0(String_t* value)
	{
		___clause_type_0 = value;
		Il2CppCodeGenWriteBarrier(&___clause_type_0, value);
	}

	inline static int32_t get_offset_of_nonce_1() { return static_cast<int32_t>(offsetof(SecurityKeyIdentifierClause_t1943429813, ___nonce_1)); }
	inline ByteU5BU5D_t4116647657* get_nonce_1() const { return ___nonce_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_nonce_1() { return &___nonce_1; }
	inline void set_nonce_1(ByteU5BU5D_t4116647657* value)
	{
		___nonce_1 = value;
		Il2CppCodeGenWriteBarrier(&___nonce_1, value);
	}

	inline static int32_t get_offset_of_deriv_length_2() { return static_cast<int32_t>(offsetof(SecurityKeyIdentifierClause_t1943429813, ___deriv_length_2)); }
	inline int32_t get_deriv_length_2() const { return ___deriv_length_2; }
	inline int32_t* get_address_of_deriv_length_2() { return &___deriv_length_2; }
	inline void set_deriv_length_2(int32_t value)
	{
		___deriv_length_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
