﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslNumber/XslNumberFormatter/FormatItem
struct  FormatItem_t2815695937  : public Il2CppObject
{
public:
	// System.String Mono.Xml.Xsl.Operations.XslNumber/XslNumberFormatter/FormatItem::sep
	String_t* ___sep_0;

public:
	inline static int32_t get_offset_of_sep_0() { return static_cast<int32_t>(offsetof(FormatItem_t2815695937, ___sep_0)); }
	inline String_t* get_sep_0() const { return ___sep_0; }
	inline String_t** get_address_of_sep_0() { return &___sep_0; }
	inline void set_sep_0(String_t* value)
	{
		___sep_0 = value;
		Il2CppCodeGenWriteBarrier(&___sep_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
