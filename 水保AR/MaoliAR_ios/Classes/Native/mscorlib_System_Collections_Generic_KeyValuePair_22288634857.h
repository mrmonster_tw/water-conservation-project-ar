﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"

// System.Uri
struct Uri_t100236324;
// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t714049126;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Uri,System.Security.Cryptography.X509Certificates.X509Certificate2>
struct  KeyValuePair_2_t2288634857 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	Uri_t100236324 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	X509Certificate2_t714049126 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2288634857, ___key_0)); }
	inline Uri_t100236324 * get_key_0() const { return ___key_0; }
	inline Uri_t100236324 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(Uri_t100236324 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier(&___key_0, value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2288634857, ___value_1)); }
	inline X509Certificate2_t714049126 * get_value_1() const { return ___value_1; }
	inline X509Certificate2_t714049126 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(X509Certificate2_t714049126 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
