﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeExpression2166265795.h"
#include "System_System_CodeDom_CodeBinaryOperatorType1562930934.h"

// System.CodeDom.CodeExpression
struct CodeExpression_t2166265795;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeBinaryOperatorExpression
struct  CodeBinaryOperatorExpression_t4171161493  : public CodeExpression_t2166265795
{
public:
	// System.CodeDom.CodeExpression System.CodeDom.CodeBinaryOperatorExpression::left
	CodeExpression_t2166265795 * ___left_1;
	// System.CodeDom.CodeExpression System.CodeDom.CodeBinaryOperatorExpression::right
	CodeExpression_t2166265795 * ___right_2;
	// System.CodeDom.CodeBinaryOperatorType System.CodeDom.CodeBinaryOperatorExpression::op
	int32_t ___op_3;

public:
	inline static int32_t get_offset_of_left_1() { return static_cast<int32_t>(offsetof(CodeBinaryOperatorExpression_t4171161493, ___left_1)); }
	inline CodeExpression_t2166265795 * get_left_1() const { return ___left_1; }
	inline CodeExpression_t2166265795 ** get_address_of_left_1() { return &___left_1; }
	inline void set_left_1(CodeExpression_t2166265795 * value)
	{
		___left_1 = value;
		Il2CppCodeGenWriteBarrier(&___left_1, value);
	}

	inline static int32_t get_offset_of_right_2() { return static_cast<int32_t>(offsetof(CodeBinaryOperatorExpression_t4171161493, ___right_2)); }
	inline CodeExpression_t2166265795 * get_right_2() const { return ___right_2; }
	inline CodeExpression_t2166265795 ** get_address_of_right_2() { return &___right_2; }
	inline void set_right_2(CodeExpression_t2166265795 * value)
	{
		___right_2 = value;
		Il2CppCodeGenWriteBarrier(&___right_2, value);
	}

	inline static int32_t get_offset_of_op_3() { return static_cast<int32_t>(offsetof(CodeBinaryOperatorExpression_t4171161493, ___op_3)); }
	inline int32_t get_op_3() const { return ___op_3; }
	inline int32_t* get_address_of_op_3() { return &___op_3; }
	inline void set_op_3(int32_t value)
	{
		___op_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
