﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Object
struct Il2CppObject;
// System.Char[]
struct CharU5BU5D_t3528271667;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpUtility
struct  HttpUtility_t1255642059  : public Il2CppObject
{
public:

public:
};

struct HttpUtility_t1255642059_StaticFields
{
public:
	// System.Collections.Hashtable System.Web.HttpUtility::entities
	Hashtable_t1853889766 * ___entities_0;
	// System.Object System.Web.HttpUtility::lock_
	Il2CppObject * ___lock__1;
	// System.Char[] System.Web.HttpUtility::hexChars
	CharU5BU5D_t3528271667* ___hexChars_2;

public:
	inline static int32_t get_offset_of_entities_0() { return static_cast<int32_t>(offsetof(HttpUtility_t1255642059_StaticFields, ___entities_0)); }
	inline Hashtable_t1853889766 * get_entities_0() const { return ___entities_0; }
	inline Hashtable_t1853889766 ** get_address_of_entities_0() { return &___entities_0; }
	inline void set_entities_0(Hashtable_t1853889766 * value)
	{
		___entities_0 = value;
		Il2CppCodeGenWriteBarrier(&___entities_0, value);
	}

	inline static int32_t get_offset_of_lock__1() { return static_cast<int32_t>(offsetof(HttpUtility_t1255642059_StaticFields, ___lock__1)); }
	inline Il2CppObject * get_lock__1() const { return ___lock__1; }
	inline Il2CppObject ** get_address_of_lock__1() { return &___lock__1; }
	inline void set_lock__1(Il2CppObject * value)
	{
		___lock__1 = value;
		Il2CppCodeGenWriteBarrier(&___lock__1, value);
	}

	inline static int32_t get_offset_of_hexChars_2() { return static_cast<int32_t>(offsetof(HttpUtility_t1255642059_StaticFields, ___hexChars_2)); }
	inline CharU5BU5D_t3528271667* get_hexChars_2() const { return ___hexChars_2; }
	inline CharU5BU5D_t3528271667** get_address_of_hexChars_2() { return &___hexChars_2; }
	inline void set_hexChars_2(CharU5BU5D_t3528271667* value)
	{
		___hexChars_2 = value;
		Il2CppCodeGenWriteBarrier(&___hexChars_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
