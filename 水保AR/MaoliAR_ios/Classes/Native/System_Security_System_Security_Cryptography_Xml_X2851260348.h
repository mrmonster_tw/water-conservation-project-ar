﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Security_System_Security_Cryptography_Xml_T1105379765.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.Xml.XmlDsigEnvelopedSignatureTransform
struct  XmlDsigEnvelopedSignatureTransform_t2851260348  : public Transform_t1105379765
{
public:
	// System.Boolean System.Security.Cryptography.Xml.XmlDsigEnvelopedSignatureTransform::comments
	bool ___comments_3;
	// System.Object System.Security.Cryptography.Xml.XmlDsigEnvelopedSignatureTransform::inputObj
	Il2CppObject * ___inputObj_4;

public:
	inline static int32_t get_offset_of_comments_3() { return static_cast<int32_t>(offsetof(XmlDsigEnvelopedSignatureTransform_t2851260348, ___comments_3)); }
	inline bool get_comments_3() const { return ___comments_3; }
	inline bool* get_address_of_comments_3() { return &___comments_3; }
	inline void set_comments_3(bool value)
	{
		___comments_3 = value;
	}

	inline static int32_t get_offset_of_inputObj_4() { return static_cast<int32_t>(offsetof(XmlDsigEnvelopedSignatureTransform_t2851260348, ___inputObj_4)); }
	inline Il2CppObject * get_inputObj_4() const { return ___inputObj_4; }
	inline Il2CppObject ** get_address_of_inputObj_4() { return &___inputObj_4; }
	inline void set_inputObj_4(Il2CppObject * value)
	{
		___inputObj_4 = value;
		Il2CppCodeGenWriteBarrier(&___inputObj_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
