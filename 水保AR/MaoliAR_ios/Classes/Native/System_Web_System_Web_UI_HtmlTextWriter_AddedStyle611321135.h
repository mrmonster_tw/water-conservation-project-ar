﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"
#include "System_Web_System_Web_UI_HtmlTextWriterStyle3329380222.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.HtmlTextWriter/AddedStyle
struct  AddedStyle_t611321135 
{
public:
	// System.String System.Web.UI.HtmlTextWriter/AddedStyle::name
	String_t* ___name_0;
	// System.Web.UI.HtmlTextWriterStyle System.Web.UI.HtmlTextWriter/AddedStyle::key
	int32_t ___key_1;
	// System.String System.Web.UI.HtmlTextWriter/AddedStyle::value
	String_t* ___value_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(AddedStyle_t611321135, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_key_1() { return static_cast<int32_t>(offsetof(AddedStyle_t611321135, ___key_1)); }
	inline int32_t get_key_1() const { return ___key_1; }
	inline int32_t* get_address_of_key_1() { return &___key_1; }
	inline void set_key_1(int32_t value)
	{
		___key_1 = value;
	}

	inline static int32_t get_offset_of_value_2() { return static_cast<int32_t>(offsetof(AddedStyle_t611321135, ___value_2)); }
	inline String_t* get_value_2() const { return ___value_2; }
	inline String_t** get_address_of_value_2() { return &___value_2; }
	inline void set_value_2(String_t* value)
	{
		___value_2 = value;
		Il2CppCodeGenWriteBarrier(&___value_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Web.UI.HtmlTextWriter/AddedStyle
struct AddedStyle_t611321135_marshaled_pinvoke
{
	char* ___name_0;
	int32_t ___key_1;
	char* ___value_2;
};
// Native definition for COM marshalling of System.Web.UI.HtmlTextWriter/AddedStyle
struct AddedStyle_t611321135_marshaled_com
{
	Il2CppChar* ___name_0;
	int32_t ___key_1;
	Il2CppChar* ___value_2;
};
