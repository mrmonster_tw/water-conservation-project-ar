﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"

// System.Web.UI.ToolboxDataAttribute
struct ToolboxDataAttribute_t3280476760;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.ToolboxDataAttribute
struct  ToolboxDataAttribute_t3280476760  : public Attribute_t861562559
{
public:
	// System.String System.Web.UI.ToolboxDataAttribute::data
	String_t* ___data_1;

public:
	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(ToolboxDataAttribute_t3280476760, ___data_1)); }
	inline String_t* get_data_1() const { return ___data_1; }
	inline String_t** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(String_t* value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier(&___data_1, value);
	}
};

struct ToolboxDataAttribute_t3280476760_StaticFields
{
public:
	// System.Web.UI.ToolboxDataAttribute System.Web.UI.ToolboxDataAttribute::Default
	ToolboxDataAttribute_t3280476760 * ___Default_0;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(ToolboxDataAttribute_t3280476760_StaticFields, ___Default_0)); }
	inline ToolboxDataAttribute_t3280476760 * get_Default_0() const { return ___Default_0; }
	inline ToolboxDataAttribute_t3280476760 ** get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(ToolboxDataAttribute_t3280476760 * value)
	{
		___Default_0 = value;
		Il2CppCodeGenWriteBarrier(&___Default_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
