﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeTypeMember1555525554.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeSnippetTypeMember
struct  CodeSnippetTypeMember_t4221488240  : public CodeTypeMember_t1555525554
{
public:
	// System.String System.CodeDom.CodeSnippetTypeMember::text
	String_t* ___text_8;

public:
	inline static int32_t get_offset_of_text_8() { return static_cast<int32_t>(offsetof(CodeSnippetTypeMember_t4221488240, ___text_8)); }
	inline String_t* get_text_8() const { return ___text_8; }
	inline String_t** get_address_of_text_8() { return &___text_8; }
	inline void set_text_8(String_t* value)
	{
		___text_8 = value;
		Il2CppCodeGenWriteBarrier(&___text_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
