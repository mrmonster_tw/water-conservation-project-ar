﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_Collections_Generic_Syn3023046804.h"

// System.ServiceModel.Dispatcher.ChannelDispatcher
struct ChannelDispatcher_t2781106991;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.ChannelDispatcher/EndpointDispatcherCollection
struct  EndpointDispatcherCollection_t3232467337  : public SynchronizedCollection_1_t3023046804
{
public:
	// System.ServiceModel.Dispatcher.ChannelDispatcher System.ServiceModel.Dispatcher.ChannelDispatcher/EndpointDispatcherCollection::owner
	ChannelDispatcher_t2781106991 * ___owner_2;

public:
	inline static int32_t get_offset_of_owner_2() { return static_cast<int32_t>(offsetof(EndpointDispatcherCollection_t3232467337, ___owner_2)); }
	inline ChannelDispatcher_t2781106991 * get_owner_2() const { return ___owner_2; }
	inline ChannelDispatcher_t2781106991 ** get_address_of_owner_2() { return &___owner_2; }
	inline void set_owner_2(ChannelDispatcher_t2781106991 * value)
	{
		___owner_2 = value;
		Il2CppCodeGenWriteBarrier(&___owner_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
