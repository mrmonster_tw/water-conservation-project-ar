﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SmartTerrainInitializationInfo
struct  SmartTerrainInitializationInfo_t1789741059 
{
public:
	union
	{
		struct
		{
		};
		uint8_t SmartTerrainInitializationInfo_t1789741059__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
