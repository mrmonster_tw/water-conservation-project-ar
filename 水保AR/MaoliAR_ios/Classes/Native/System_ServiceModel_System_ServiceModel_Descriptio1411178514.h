﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_System_Net_Security_ProtectionLevel2559578148.h"
#include "System_ServiceModel_System_ServiceModel_SessionMode513474935.h"

// System.ServiceModel.Description.OperationDescriptionCollection
struct OperationDescriptionCollection_t4198923421;
// System.Collections.Generic.KeyedByTypeCollection`1<System.ServiceModel.Description.IContractBehavior>
struct KeyedByTypeCollection_1_t3033242326;
// System.Type
struct Type_t;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.ContractDescription
struct  ContractDescription_t1411178514  : public Il2CppObject
{
public:
	// System.ServiceModel.Description.OperationDescriptionCollection System.ServiceModel.Description.ContractDescription::operations
	OperationDescriptionCollection_t4198923421 * ___operations_0;
	// System.Collections.Generic.KeyedByTypeCollection`1<System.ServiceModel.Description.IContractBehavior> System.ServiceModel.Description.ContractDescription::behaviors
	KeyedByTypeCollection_1_t3033242326 * ___behaviors_1;
	// System.Type System.ServiceModel.Description.ContractDescription::callback_contract_type
	Type_t * ___callback_contract_type_2;
	// System.Type System.ServiceModel.Description.ContractDescription::contract_type
	Type_t * ___contract_type_3;
	// System.String System.ServiceModel.Description.ContractDescription::name
	String_t* ___name_4;
	// System.String System.ServiceModel.Description.ContractDescription::ns
	String_t* ___ns_5;
	// System.String System.ServiceModel.Description.ContractDescription::config_name
	String_t* ___config_name_6;
	// System.Net.Security.ProtectionLevel System.ServiceModel.Description.ContractDescription::protection_level
	int32_t ___protection_level_7;
	// System.Boolean System.ServiceModel.Description.ContractDescription::has_protection_level
	bool ___has_protection_level_8;
	// System.ServiceModel.SessionMode System.ServiceModel.Description.ContractDescription::session
	int32_t ___session_9;

public:
	inline static int32_t get_offset_of_operations_0() { return static_cast<int32_t>(offsetof(ContractDescription_t1411178514, ___operations_0)); }
	inline OperationDescriptionCollection_t4198923421 * get_operations_0() const { return ___operations_0; }
	inline OperationDescriptionCollection_t4198923421 ** get_address_of_operations_0() { return &___operations_0; }
	inline void set_operations_0(OperationDescriptionCollection_t4198923421 * value)
	{
		___operations_0 = value;
		Il2CppCodeGenWriteBarrier(&___operations_0, value);
	}

	inline static int32_t get_offset_of_behaviors_1() { return static_cast<int32_t>(offsetof(ContractDescription_t1411178514, ___behaviors_1)); }
	inline KeyedByTypeCollection_1_t3033242326 * get_behaviors_1() const { return ___behaviors_1; }
	inline KeyedByTypeCollection_1_t3033242326 ** get_address_of_behaviors_1() { return &___behaviors_1; }
	inline void set_behaviors_1(KeyedByTypeCollection_1_t3033242326 * value)
	{
		___behaviors_1 = value;
		Il2CppCodeGenWriteBarrier(&___behaviors_1, value);
	}

	inline static int32_t get_offset_of_callback_contract_type_2() { return static_cast<int32_t>(offsetof(ContractDescription_t1411178514, ___callback_contract_type_2)); }
	inline Type_t * get_callback_contract_type_2() const { return ___callback_contract_type_2; }
	inline Type_t ** get_address_of_callback_contract_type_2() { return &___callback_contract_type_2; }
	inline void set_callback_contract_type_2(Type_t * value)
	{
		___callback_contract_type_2 = value;
		Il2CppCodeGenWriteBarrier(&___callback_contract_type_2, value);
	}

	inline static int32_t get_offset_of_contract_type_3() { return static_cast<int32_t>(offsetof(ContractDescription_t1411178514, ___contract_type_3)); }
	inline Type_t * get_contract_type_3() const { return ___contract_type_3; }
	inline Type_t ** get_address_of_contract_type_3() { return &___contract_type_3; }
	inline void set_contract_type_3(Type_t * value)
	{
		___contract_type_3 = value;
		Il2CppCodeGenWriteBarrier(&___contract_type_3, value);
	}

	inline static int32_t get_offset_of_name_4() { return static_cast<int32_t>(offsetof(ContractDescription_t1411178514, ___name_4)); }
	inline String_t* get_name_4() const { return ___name_4; }
	inline String_t** get_address_of_name_4() { return &___name_4; }
	inline void set_name_4(String_t* value)
	{
		___name_4 = value;
		Il2CppCodeGenWriteBarrier(&___name_4, value);
	}

	inline static int32_t get_offset_of_ns_5() { return static_cast<int32_t>(offsetof(ContractDescription_t1411178514, ___ns_5)); }
	inline String_t* get_ns_5() const { return ___ns_5; }
	inline String_t** get_address_of_ns_5() { return &___ns_5; }
	inline void set_ns_5(String_t* value)
	{
		___ns_5 = value;
		Il2CppCodeGenWriteBarrier(&___ns_5, value);
	}

	inline static int32_t get_offset_of_config_name_6() { return static_cast<int32_t>(offsetof(ContractDescription_t1411178514, ___config_name_6)); }
	inline String_t* get_config_name_6() const { return ___config_name_6; }
	inline String_t** get_address_of_config_name_6() { return &___config_name_6; }
	inline void set_config_name_6(String_t* value)
	{
		___config_name_6 = value;
		Il2CppCodeGenWriteBarrier(&___config_name_6, value);
	}

	inline static int32_t get_offset_of_protection_level_7() { return static_cast<int32_t>(offsetof(ContractDescription_t1411178514, ___protection_level_7)); }
	inline int32_t get_protection_level_7() const { return ___protection_level_7; }
	inline int32_t* get_address_of_protection_level_7() { return &___protection_level_7; }
	inline void set_protection_level_7(int32_t value)
	{
		___protection_level_7 = value;
	}

	inline static int32_t get_offset_of_has_protection_level_8() { return static_cast<int32_t>(offsetof(ContractDescription_t1411178514, ___has_protection_level_8)); }
	inline bool get_has_protection_level_8() const { return ___has_protection_level_8; }
	inline bool* get_address_of_has_protection_level_8() { return &___has_protection_level_8; }
	inline void set_has_protection_level_8(bool value)
	{
		___has_protection_level_8 = value;
	}

	inline static int32_t get_offset_of_session_9() { return static_cast<int32_t>(offsetof(ContractDescription_t1411178514, ___session_9)); }
	inline int32_t get_session_9() const { return ___session_9; }
	inline int32_t* get_address_of_session_9() { return &___session_9; }
	inline void set_session_9(int32_t value)
	{
		___session_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
