﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_R1473296135.h"

// System.ServiceModel.Channels.RecipientMessageSecurityBindingSupport
struct RecipientMessageSecurityBindingSupport_t1155974079;
// System.ServiceModel.Channels.SecurityReplyChannel
struct SecurityReplyChannel_t2188340137;
// System.ServiceModel.Channels.RequestContext
struct RequestContext_t1473296135;
// System.ServiceModel.Channels.Message
struct Message_t869514973;
// System.ServiceModel.Channels.MessageBuffer
struct MessageBuffer_t778472114;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.SecurityRequestContext
struct  SecurityRequestContext_t3267761371  : public RequestContext_t1473296135
{
public:
	// System.ServiceModel.Channels.RecipientMessageSecurityBindingSupport System.ServiceModel.Channels.SecurityRequestContext::security
	RecipientMessageSecurityBindingSupport_t1155974079 * ___security_0;
	// System.ServiceModel.Channels.SecurityReplyChannel System.ServiceModel.Channels.SecurityRequestContext::channel
	SecurityReplyChannel_t2188340137 * ___channel_1;
	// System.ServiceModel.Channels.RequestContext System.ServiceModel.Channels.SecurityRequestContext::source
	RequestContext_t1473296135 * ___source_2;
	// System.ServiceModel.Channels.Message System.ServiceModel.Channels.SecurityRequestContext::msg
	Message_t869514973 * ___msg_3;
	// System.ServiceModel.Channels.MessageBuffer System.ServiceModel.Channels.SecurityRequestContext::source_request
	MessageBuffer_t778472114 * ___source_request_4;

public:
	inline static int32_t get_offset_of_security_0() { return static_cast<int32_t>(offsetof(SecurityRequestContext_t3267761371, ___security_0)); }
	inline RecipientMessageSecurityBindingSupport_t1155974079 * get_security_0() const { return ___security_0; }
	inline RecipientMessageSecurityBindingSupport_t1155974079 ** get_address_of_security_0() { return &___security_0; }
	inline void set_security_0(RecipientMessageSecurityBindingSupport_t1155974079 * value)
	{
		___security_0 = value;
		Il2CppCodeGenWriteBarrier(&___security_0, value);
	}

	inline static int32_t get_offset_of_channel_1() { return static_cast<int32_t>(offsetof(SecurityRequestContext_t3267761371, ___channel_1)); }
	inline SecurityReplyChannel_t2188340137 * get_channel_1() const { return ___channel_1; }
	inline SecurityReplyChannel_t2188340137 ** get_address_of_channel_1() { return &___channel_1; }
	inline void set_channel_1(SecurityReplyChannel_t2188340137 * value)
	{
		___channel_1 = value;
		Il2CppCodeGenWriteBarrier(&___channel_1, value);
	}

	inline static int32_t get_offset_of_source_2() { return static_cast<int32_t>(offsetof(SecurityRequestContext_t3267761371, ___source_2)); }
	inline RequestContext_t1473296135 * get_source_2() const { return ___source_2; }
	inline RequestContext_t1473296135 ** get_address_of_source_2() { return &___source_2; }
	inline void set_source_2(RequestContext_t1473296135 * value)
	{
		___source_2 = value;
		Il2CppCodeGenWriteBarrier(&___source_2, value);
	}

	inline static int32_t get_offset_of_msg_3() { return static_cast<int32_t>(offsetof(SecurityRequestContext_t3267761371, ___msg_3)); }
	inline Message_t869514973 * get_msg_3() const { return ___msg_3; }
	inline Message_t869514973 ** get_address_of_msg_3() { return &___msg_3; }
	inline void set_msg_3(Message_t869514973 * value)
	{
		___msg_3 = value;
		Il2CppCodeGenWriteBarrier(&___msg_3, value);
	}

	inline static int32_t get_offset_of_source_request_4() { return static_cast<int32_t>(offsetof(SecurityRequestContext_t3267761371, ___source_request_4)); }
	inline MessageBuffer_t778472114 * get_source_request_4() const { return ___source_request_4; }
	inline MessageBuffer_t778472114 ** get_address_of_source_request_4() { return &___source_request_4; }
	inline void set_source_request_4(MessageBuffer_t778472114 * value)
	{
		___source_request_4 = value;
		Il2CppCodeGenWriteBarrier(&___source_request_4, value);
	}
};

struct SecurityRequestContext_t3267761371_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Channels.SecurityRequestContext::<>f__switch$map7
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map7_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map7_5() { return static_cast<int32_t>(offsetof(SecurityRequestContext_t3267761371_StaticFields, ___U3CU3Ef__switchU24map7_5)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map7_5() const { return ___U3CU3Ef__switchU24map7_5; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map7_5() { return &___U3CU3Ef__switchU24map7_5; }
	inline void set_U3CU3Ef__switchU24map7_5(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map7_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map7_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
