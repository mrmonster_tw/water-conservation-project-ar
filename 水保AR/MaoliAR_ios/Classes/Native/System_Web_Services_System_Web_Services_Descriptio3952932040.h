﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_Services_System_Web_Services_Descriptio2466402210.h"

// System.Web.Services.Description.ServiceDescriptionFormatExtensionCollection
struct ServiceDescriptionFormatExtensionCollection_t2630153888;
// System.Web.Services.Description.OperationBindingCollection
struct OperationBindingCollection_t1644124013;
// System.Web.Services.Description.ServiceDescription
struct ServiceDescription_t3693704363;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2760654312;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.Binding
struct  Binding_t3952932040  : public NamedItem_t2466402210
{
public:
	// System.Web.Services.Description.ServiceDescriptionFormatExtensionCollection System.Web.Services.Description.Binding::extensions
	ServiceDescriptionFormatExtensionCollection_t2630153888 * ___extensions_4;
	// System.Web.Services.Description.OperationBindingCollection System.Web.Services.Description.Binding::operations
	OperationBindingCollection_t1644124013 * ___operations_5;
	// System.Web.Services.Description.ServiceDescription System.Web.Services.Description.Binding::serviceDescription
	ServiceDescription_t3693704363 * ___serviceDescription_6;
	// System.Xml.XmlQualifiedName System.Web.Services.Description.Binding::type
	XmlQualifiedName_t2760654312 * ___type_7;

public:
	inline static int32_t get_offset_of_extensions_4() { return static_cast<int32_t>(offsetof(Binding_t3952932040, ___extensions_4)); }
	inline ServiceDescriptionFormatExtensionCollection_t2630153888 * get_extensions_4() const { return ___extensions_4; }
	inline ServiceDescriptionFormatExtensionCollection_t2630153888 ** get_address_of_extensions_4() { return &___extensions_4; }
	inline void set_extensions_4(ServiceDescriptionFormatExtensionCollection_t2630153888 * value)
	{
		___extensions_4 = value;
		Il2CppCodeGenWriteBarrier(&___extensions_4, value);
	}

	inline static int32_t get_offset_of_operations_5() { return static_cast<int32_t>(offsetof(Binding_t3952932040, ___operations_5)); }
	inline OperationBindingCollection_t1644124013 * get_operations_5() const { return ___operations_5; }
	inline OperationBindingCollection_t1644124013 ** get_address_of_operations_5() { return &___operations_5; }
	inline void set_operations_5(OperationBindingCollection_t1644124013 * value)
	{
		___operations_5 = value;
		Il2CppCodeGenWriteBarrier(&___operations_5, value);
	}

	inline static int32_t get_offset_of_serviceDescription_6() { return static_cast<int32_t>(offsetof(Binding_t3952932040, ___serviceDescription_6)); }
	inline ServiceDescription_t3693704363 * get_serviceDescription_6() const { return ___serviceDescription_6; }
	inline ServiceDescription_t3693704363 ** get_address_of_serviceDescription_6() { return &___serviceDescription_6; }
	inline void set_serviceDescription_6(ServiceDescription_t3693704363 * value)
	{
		___serviceDescription_6 = value;
		Il2CppCodeGenWriteBarrier(&___serviceDescription_6, value);
	}

	inline static int32_t get_offset_of_type_7() { return static_cast<int32_t>(offsetof(Binding_t3952932040, ___type_7)); }
	inline XmlQualifiedName_t2760654312 * get_type_7() const { return ___type_7; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_type_7() { return &___type_7; }
	inline void set_type_7(XmlQualifiedName_t2760654312 * value)
	{
		___type_7 = value;
		Il2CppCodeGenWriteBarrier(&___type_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
