﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Security_T2685676710.h"

// Mono.Security.Protocol.Tls.SslClientStream
struct SslClientStream_t3914624662;
// System.IO.MemoryStream
struct MemoryStream_t94973147;
// Mono.Security.Protocol.Tls.CertificateSelectionCallback
struct CertificateSelectionCallback_t3743405225;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.TlsClientSession
struct  TlsClientSession_t151923929  : public TlsSession_t2685676710
{
public:
	// Mono.Security.Protocol.Tls.SslClientStream System.ServiceModel.Security.Tokens.TlsClientSession::ssl
	SslClientStream_t3914624662 * ___ssl_0;
	// System.IO.MemoryStream System.ServiceModel.Security.Tokens.TlsClientSession::stream
	MemoryStream_t94973147 * ___stream_1;
	// System.Boolean System.ServiceModel.Security.Tokens.TlsClientSession::mutual
	bool ___mutual_2;

public:
	inline static int32_t get_offset_of_ssl_0() { return static_cast<int32_t>(offsetof(TlsClientSession_t151923929, ___ssl_0)); }
	inline SslClientStream_t3914624662 * get_ssl_0() const { return ___ssl_0; }
	inline SslClientStream_t3914624662 ** get_address_of_ssl_0() { return &___ssl_0; }
	inline void set_ssl_0(SslClientStream_t3914624662 * value)
	{
		___ssl_0 = value;
		Il2CppCodeGenWriteBarrier(&___ssl_0, value);
	}

	inline static int32_t get_offset_of_stream_1() { return static_cast<int32_t>(offsetof(TlsClientSession_t151923929, ___stream_1)); }
	inline MemoryStream_t94973147 * get_stream_1() const { return ___stream_1; }
	inline MemoryStream_t94973147 ** get_address_of_stream_1() { return &___stream_1; }
	inline void set_stream_1(MemoryStream_t94973147 * value)
	{
		___stream_1 = value;
		Il2CppCodeGenWriteBarrier(&___stream_1, value);
	}

	inline static int32_t get_offset_of_mutual_2() { return static_cast<int32_t>(offsetof(TlsClientSession_t151923929, ___mutual_2)); }
	inline bool get_mutual_2() const { return ___mutual_2; }
	inline bool* get_address_of_mutual_2() { return &___mutual_2; }
	inline void set_mutual_2(bool value)
	{
		___mutual_2 = value;
	}
};

struct TlsClientSession_t151923929_StaticFields
{
public:
	// Mono.Security.Protocol.Tls.CertificateSelectionCallback System.ServiceModel.Security.Tokens.TlsClientSession::<>f__am$cache3
	CertificateSelectionCallback_t3743405225 * ___U3CU3Ef__amU24cache3_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(TlsClientSession_t151923929_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline CertificateSelectionCallback_t3743405225 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline CertificateSelectionCallback_t3743405225 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(CertificateSelectionCallback_t3743405225 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
