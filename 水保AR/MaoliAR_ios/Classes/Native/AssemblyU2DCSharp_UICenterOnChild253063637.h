﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// SpringPanel/OnFinished
struct OnFinished_t3778785451;
// UICenterOnChild/OnCenterCallback
struct OnCenterCallback_t2077531662;
// UIScrollView
struct UIScrollView_t1973404950;
// UnityEngine.GameObject
struct GameObject_t1113636619;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICenterOnChild
struct  UICenterOnChild_t253063637  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UICenterOnChild::springStrength
	float ___springStrength_2;
	// System.Single UICenterOnChild::nextPageThreshold
	float ___nextPageThreshold_3;
	// SpringPanel/OnFinished UICenterOnChild::onFinished
	OnFinished_t3778785451 * ___onFinished_4;
	// UICenterOnChild/OnCenterCallback UICenterOnChild::onCenter
	OnCenterCallback_t2077531662 * ___onCenter_5;
	// UIScrollView UICenterOnChild::mScrollView
	UIScrollView_t1973404950 * ___mScrollView_6;
	// UnityEngine.GameObject UICenterOnChild::mCenteredObject
	GameObject_t1113636619 * ___mCenteredObject_7;

public:
	inline static int32_t get_offset_of_springStrength_2() { return static_cast<int32_t>(offsetof(UICenterOnChild_t253063637, ___springStrength_2)); }
	inline float get_springStrength_2() const { return ___springStrength_2; }
	inline float* get_address_of_springStrength_2() { return &___springStrength_2; }
	inline void set_springStrength_2(float value)
	{
		___springStrength_2 = value;
	}

	inline static int32_t get_offset_of_nextPageThreshold_3() { return static_cast<int32_t>(offsetof(UICenterOnChild_t253063637, ___nextPageThreshold_3)); }
	inline float get_nextPageThreshold_3() const { return ___nextPageThreshold_3; }
	inline float* get_address_of_nextPageThreshold_3() { return &___nextPageThreshold_3; }
	inline void set_nextPageThreshold_3(float value)
	{
		___nextPageThreshold_3 = value;
	}

	inline static int32_t get_offset_of_onFinished_4() { return static_cast<int32_t>(offsetof(UICenterOnChild_t253063637, ___onFinished_4)); }
	inline OnFinished_t3778785451 * get_onFinished_4() const { return ___onFinished_4; }
	inline OnFinished_t3778785451 ** get_address_of_onFinished_4() { return &___onFinished_4; }
	inline void set_onFinished_4(OnFinished_t3778785451 * value)
	{
		___onFinished_4 = value;
		Il2CppCodeGenWriteBarrier(&___onFinished_4, value);
	}

	inline static int32_t get_offset_of_onCenter_5() { return static_cast<int32_t>(offsetof(UICenterOnChild_t253063637, ___onCenter_5)); }
	inline OnCenterCallback_t2077531662 * get_onCenter_5() const { return ___onCenter_5; }
	inline OnCenterCallback_t2077531662 ** get_address_of_onCenter_5() { return &___onCenter_5; }
	inline void set_onCenter_5(OnCenterCallback_t2077531662 * value)
	{
		___onCenter_5 = value;
		Il2CppCodeGenWriteBarrier(&___onCenter_5, value);
	}

	inline static int32_t get_offset_of_mScrollView_6() { return static_cast<int32_t>(offsetof(UICenterOnChild_t253063637, ___mScrollView_6)); }
	inline UIScrollView_t1973404950 * get_mScrollView_6() const { return ___mScrollView_6; }
	inline UIScrollView_t1973404950 ** get_address_of_mScrollView_6() { return &___mScrollView_6; }
	inline void set_mScrollView_6(UIScrollView_t1973404950 * value)
	{
		___mScrollView_6 = value;
		Il2CppCodeGenWriteBarrier(&___mScrollView_6, value);
	}

	inline static int32_t get_offset_of_mCenteredObject_7() { return static_cast<int32_t>(offsetof(UICenterOnChild_t253063637, ___mCenteredObject_7)); }
	inline GameObject_t1113636619 * get_mCenteredObject_7() const { return ___mCenteredObject_7; }
	inline GameObject_t1113636619 ** get_address_of_mCenteredObject_7() { return &___mCenteredObject_7; }
	inline void set_mCenteredObject_7(GameObject_t1113636619 * value)
	{
		___mCenteredObject_7 = value;
		Il2CppCodeGenWriteBarrier(&___mCenteredObject_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
