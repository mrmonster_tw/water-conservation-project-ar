﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Dispatcher.ClientRuntime
struct ClientRuntime_t3616476419;
// System.String
struct String_t;
// System.Collections.Generic.SynchronizedCollection`1<System.ServiceModel.Dispatcher.IParameterInspector>
struct SynchronizedCollection_1_t1032157755;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.ClientOperation
struct  ClientOperation_t2430985984  : public Il2CppObject
{
public:
	// System.ServiceModel.Dispatcher.ClientRuntime System.ServiceModel.Dispatcher.ClientOperation::parent
	ClientRuntime_t3616476419 * ___parent_0;
	// System.String System.ServiceModel.Dispatcher.ClientOperation::name
	String_t* ___name_1;
	// System.String System.ServiceModel.Dispatcher.ClientOperation::action
	String_t* ___action_2;
	// System.String System.ServiceModel.Dispatcher.ClientOperation::reply_action
	String_t* ___reply_action_3;
	// System.Boolean System.ServiceModel.Dispatcher.ClientOperation::deserialize_reply
	bool ___deserialize_reply_4;
	// System.Boolean System.ServiceModel.Dispatcher.ClientOperation::serialize_request
	bool ___serialize_request_5;
	// System.Collections.Generic.SynchronizedCollection`1<System.ServiceModel.Dispatcher.IParameterInspector> System.ServiceModel.Dispatcher.ClientOperation::inspectors
	SynchronizedCollection_1_t1032157755 * ___inspectors_6;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(ClientOperation_t2430985984, ___parent_0)); }
	inline ClientRuntime_t3616476419 * get_parent_0() const { return ___parent_0; }
	inline ClientRuntime_t3616476419 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(ClientRuntime_t3616476419 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___parent_0, value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(ClientOperation_t2430985984, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_action_2() { return static_cast<int32_t>(offsetof(ClientOperation_t2430985984, ___action_2)); }
	inline String_t* get_action_2() const { return ___action_2; }
	inline String_t** get_address_of_action_2() { return &___action_2; }
	inline void set_action_2(String_t* value)
	{
		___action_2 = value;
		Il2CppCodeGenWriteBarrier(&___action_2, value);
	}

	inline static int32_t get_offset_of_reply_action_3() { return static_cast<int32_t>(offsetof(ClientOperation_t2430985984, ___reply_action_3)); }
	inline String_t* get_reply_action_3() const { return ___reply_action_3; }
	inline String_t** get_address_of_reply_action_3() { return &___reply_action_3; }
	inline void set_reply_action_3(String_t* value)
	{
		___reply_action_3 = value;
		Il2CppCodeGenWriteBarrier(&___reply_action_3, value);
	}

	inline static int32_t get_offset_of_deserialize_reply_4() { return static_cast<int32_t>(offsetof(ClientOperation_t2430985984, ___deserialize_reply_4)); }
	inline bool get_deserialize_reply_4() const { return ___deserialize_reply_4; }
	inline bool* get_address_of_deserialize_reply_4() { return &___deserialize_reply_4; }
	inline void set_deserialize_reply_4(bool value)
	{
		___deserialize_reply_4 = value;
	}

	inline static int32_t get_offset_of_serialize_request_5() { return static_cast<int32_t>(offsetof(ClientOperation_t2430985984, ___serialize_request_5)); }
	inline bool get_serialize_request_5() const { return ___serialize_request_5; }
	inline bool* get_address_of_serialize_request_5() { return &___serialize_request_5; }
	inline void set_serialize_request_5(bool value)
	{
		___serialize_request_5 = value;
	}

	inline static int32_t get_offset_of_inspectors_6() { return static_cast<int32_t>(offsetof(ClientOperation_t2430985984, ___inspectors_6)); }
	inline SynchronizedCollection_1_t1032157755 * get_inspectors_6() const { return ___inspectors_6; }
	inline SynchronizedCollection_1_t1032157755 ** get_address_of_inspectors_6() { return &___inspectors_6; }
	inline void set_inspectors_6(SynchronizedCollection_1_t1032157755 * value)
	{
		___inspectors_6 = value;
		Il2CppCodeGenWriteBarrier(&___inspectors_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
