﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Configurat2874801746.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.ClaimTypeElementCollection
struct  ClaimTypeElementCollection_t2856322314  : public ServiceModelConfigurationElementCollection_1_t2874801746
{
public:

public:
};

struct ClaimTypeElementCollection_t2856322314_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.ClaimTypeElementCollection::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_23;

public:
	inline static int32_t get_offset_of_properties_23() { return static_cast<int32_t>(offsetof(ClaimTypeElementCollection_t2856322314_StaticFields, ___properties_23)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_23() const { return ___properties_23; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_23() { return &___properties_23; }
	inline void set_properties_23(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_23 = value;
		Il2CppCodeGenWriteBarrier(&___properties_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
