﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_RootBuilder1594119744.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.FileLevelPageControlBuilder
struct  FileLevelPageControlBuilder_t3675283787  : public RootBuilder_t1594119744
{
public:
	// System.Boolean System.Web.UI.FileLevelPageControlBuilder::hasContentControls
	bool ___hasContentControls_35;
	// System.Boolean System.Web.UI.FileLevelPageControlBuilder::hasLiteralControls
	bool ___hasLiteralControls_36;
	// System.Boolean System.Web.UI.FileLevelPageControlBuilder::hasOtherControls
	bool ___hasOtherControls_37;

public:
	inline static int32_t get_offset_of_hasContentControls_35() { return static_cast<int32_t>(offsetof(FileLevelPageControlBuilder_t3675283787, ___hasContentControls_35)); }
	inline bool get_hasContentControls_35() const { return ___hasContentControls_35; }
	inline bool* get_address_of_hasContentControls_35() { return &___hasContentControls_35; }
	inline void set_hasContentControls_35(bool value)
	{
		___hasContentControls_35 = value;
	}

	inline static int32_t get_offset_of_hasLiteralControls_36() { return static_cast<int32_t>(offsetof(FileLevelPageControlBuilder_t3675283787, ___hasLiteralControls_36)); }
	inline bool get_hasLiteralControls_36() const { return ___hasLiteralControls_36; }
	inline bool* get_address_of_hasLiteralControls_36() { return &___hasLiteralControls_36; }
	inline void set_hasLiteralControls_36(bool value)
	{
		___hasLiteralControls_36 = value;
	}

	inline static int32_t get_offset_of_hasOtherControls_37() { return static_cast<int32_t>(offsetof(FileLevelPageControlBuilder_t3675283787, ___hasOtherControls_37)); }
	inline bool get_hasOtherControls_37() const { return ___hasOtherControls_37; }
	inline bool* get_address_of_hasOtherControls_37() { return &___hasOtherControls_37; }
	inline void set_hasOtherControls_37(bool value)
	{
		___hasOtherControls_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
