﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.Compiler.CompilerInfo
struct  CompilerInfo_t947794800  : public Il2CppObject
{
public:
	// System.String System.CodeDom.Compiler.CompilerInfo::Languages
	String_t* ___Languages_0;
	// System.String System.CodeDom.Compiler.CompilerInfo::Extensions
	String_t* ___Extensions_1;
	// System.String System.CodeDom.Compiler.CompilerInfo::TypeName
	String_t* ___TypeName_2;
	// System.Int32 System.CodeDom.Compiler.CompilerInfo::WarningLevel
	int32_t ___WarningLevel_3;
	// System.String System.CodeDom.Compiler.CompilerInfo::CompilerOptions
	String_t* ___CompilerOptions_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> System.CodeDom.Compiler.CompilerInfo::ProviderOptions
	Dictionary_2_t1632706988 * ___ProviderOptions_5;
	// System.Boolean System.CodeDom.Compiler.CompilerInfo::inited
	bool ___inited_6;
	// System.Type System.CodeDom.Compiler.CompilerInfo::type
	Type_t * ___type_7;

public:
	inline static int32_t get_offset_of_Languages_0() { return static_cast<int32_t>(offsetof(CompilerInfo_t947794800, ___Languages_0)); }
	inline String_t* get_Languages_0() const { return ___Languages_0; }
	inline String_t** get_address_of_Languages_0() { return &___Languages_0; }
	inline void set_Languages_0(String_t* value)
	{
		___Languages_0 = value;
		Il2CppCodeGenWriteBarrier(&___Languages_0, value);
	}

	inline static int32_t get_offset_of_Extensions_1() { return static_cast<int32_t>(offsetof(CompilerInfo_t947794800, ___Extensions_1)); }
	inline String_t* get_Extensions_1() const { return ___Extensions_1; }
	inline String_t** get_address_of_Extensions_1() { return &___Extensions_1; }
	inline void set_Extensions_1(String_t* value)
	{
		___Extensions_1 = value;
		Il2CppCodeGenWriteBarrier(&___Extensions_1, value);
	}

	inline static int32_t get_offset_of_TypeName_2() { return static_cast<int32_t>(offsetof(CompilerInfo_t947794800, ___TypeName_2)); }
	inline String_t* get_TypeName_2() const { return ___TypeName_2; }
	inline String_t** get_address_of_TypeName_2() { return &___TypeName_2; }
	inline void set_TypeName_2(String_t* value)
	{
		___TypeName_2 = value;
		Il2CppCodeGenWriteBarrier(&___TypeName_2, value);
	}

	inline static int32_t get_offset_of_WarningLevel_3() { return static_cast<int32_t>(offsetof(CompilerInfo_t947794800, ___WarningLevel_3)); }
	inline int32_t get_WarningLevel_3() const { return ___WarningLevel_3; }
	inline int32_t* get_address_of_WarningLevel_3() { return &___WarningLevel_3; }
	inline void set_WarningLevel_3(int32_t value)
	{
		___WarningLevel_3 = value;
	}

	inline static int32_t get_offset_of_CompilerOptions_4() { return static_cast<int32_t>(offsetof(CompilerInfo_t947794800, ___CompilerOptions_4)); }
	inline String_t* get_CompilerOptions_4() const { return ___CompilerOptions_4; }
	inline String_t** get_address_of_CompilerOptions_4() { return &___CompilerOptions_4; }
	inline void set_CompilerOptions_4(String_t* value)
	{
		___CompilerOptions_4 = value;
		Il2CppCodeGenWriteBarrier(&___CompilerOptions_4, value);
	}

	inline static int32_t get_offset_of_ProviderOptions_5() { return static_cast<int32_t>(offsetof(CompilerInfo_t947794800, ___ProviderOptions_5)); }
	inline Dictionary_2_t1632706988 * get_ProviderOptions_5() const { return ___ProviderOptions_5; }
	inline Dictionary_2_t1632706988 ** get_address_of_ProviderOptions_5() { return &___ProviderOptions_5; }
	inline void set_ProviderOptions_5(Dictionary_2_t1632706988 * value)
	{
		___ProviderOptions_5 = value;
		Il2CppCodeGenWriteBarrier(&___ProviderOptions_5, value);
	}

	inline static int32_t get_offset_of_inited_6() { return static_cast<int32_t>(offsetof(CompilerInfo_t947794800, ___inited_6)); }
	inline bool get_inited_6() const { return ___inited_6; }
	inline bool* get_address_of_inited_6() { return &___inited_6; }
	inline void set_inited_6(bool value)
	{
		___inited_6 = value;
	}

	inline static int32_t get_offset_of_type_7() { return static_cast<int32_t>(offsetof(CompilerInfo_t947794800, ___type_7)); }
	inline Type_t * get_type_7() const { return ___type_7; }
	inline Type_t ** get_address_of_type_7() { return &___type_7; }
	inline void set_type_7(Type_t * value)
	{
		___type_7 = value;
		Il2CppCodeGenWriteBarrier(&___type_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
