﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1958507577.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Ci76530769.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_C736195574.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.FXAA/Preset
struct  Preset_t736195574 
{
public:
	// UnityStandardAssets.CinematicEffects.FXAA/QualitySettings UnityStandardAssets.CinematicEffects.FXAA/Preset::qualitySettings
	QualitySettings_t1958507577  ___qualitySettings_0;
	// UnityStandardAssets.CinematicEffects.FXAA/ConsoleSettings UnityStandardAssets.CinematicEffects.FXAA/Preset::consoleSettings
	ConsoleSettings_t76530769  ___consoleSettings_1;

public:
	inline static int32_t get_offset_of_qualitySettings_0() { return static_cast<int32_t>(offsetof(Preset_t736195574, ___qualitySettings_0)); }
	inline QualitySettings_t1958507577  get_qualitySettings_0() const { return ___qualitySettings_0; }
	inline QualitySettings_t1958507577 * get_address_of_qualitySettings_0() { return &___qualitySettings_0; }
	inline void set_qualitySettings_0(QualitySettings_t1958507577  value)
	{
		___qualitySettings_0 = value;
	}

	inline static int32_t get_offset_of_consoleSettings_1() { return static_cast<int32_t>(offsetof(Preset_t736195574, ___consoleSettings_1)); }
	inline ConsoleSettings_t76530769  get_consoleSettings_1() const { return ___consoleSettings_1; }
	inline ConsoleSettings_t76530769 * get_address_of_consoleSettings_1() { return &___consoleSettings_1; }
	inline void set_consoleSettings_1(ConsoleSettings_t76530769  value)
	{
		___consoleSettings_1 = value;
	}
};

struct Preset_t736195574_StaticFields
{
public:
	// UnityStandardAssets.CinematicEffects.FXAA/Preset UnityStandardAssets.CinematicEffects.FXAA/Preset::s_ExtremePerformance
	Preset_t736195574  ___s_ExtremePerformance_2;
	// UnityStandardAssets.CinematicEffects.FXAA/Preset UnityStandardAssets.CinematicEffects.FXAA/Preset::s_Performance
	Preset_t736195574  ___s_Performance_3;
	// UnityStandardAssets.CinematicEffects.FXAA/Preset UnityStandardAssets.CinematicEffects.FXAA/Preset::s_Default
	Preset_t736195574  ___s_Default_4;
	// UnityStandardAssets.CinematicEffects.FXAA/Preset UnityStandardAssets.CinematicEffects.FXAA/Preset::s_Quality
	Preset_t736195574  ___s_Quality_5;
	// UnityStandardAssets.CinematicEffects.FXAA/Preset UnityStandardAssets.CinematicEffects.FXAA/Preset::s_ExtremeQuality
	Preset_t736195574  ___s_ExtremeQuality_6;

public:
	inline static int32_t get_offset_of_s_ExtremePerformance_2() { return static_cast<int32_t>(offsetof(Preset_t736195574_StaticFields, ___s_ExtremePerformance_2)); }
	inline Preset_t736195574  get_s_ExtremePerformance_2() const { return ___s_ExtremePerformance_2; }
	inline Preset_t736195574 * get_address_of_s_ExtremePerformance_2() { return &___s_ExtremePerformance_2; }
	inline void set_s_ExtremePerformance_2(Preset_t736195574  value)
	{
		___s_ExtremePerformance_2 = value;
	}

	inline static int32_t get_offset_of_s_Performance_3() { return static_cast<int32_t>(offsetof(Preset_t736195574_StaticFields, ___s_Performance_3)); }
	inline Preset_t736195574  get_s_Performance_3() const { return ___s_Performance_3; }
	inline Preset_t736195574 * get_address_of_s_Performance_3() { return &___s_Performance_3; }
	inline void set_s_Performance_3(Preset_t736195574  value)
	{
		___s_Performance_3 = value;
	}

	inline static int32_t get_offset_of_s_Default_4() { return static_cast<int32_t>(offsetof(Preset_t736195574_StaticFields, ___s_Default_4)); }
	inline Preset_t736195574  get_s_Default_4() const { return ___s_Default_4; }
	inline Preset_t736195574 * get_address_of_s_Default_4() { return &___s_Default_4; }
	inline void set_s_Default_4(Preset_t736195574  value)
	{
		___s_Default_4 = value;
	}

	inline static int32_t get_offset_of_s_Quality_5() { return static_cast<int32_t>(offsetof(Preset_t736195574_StaticFields, ___s_Quality_5)); }
	inline Preset_t736195574  get_s_Quality_5() const { return ___s_Quality_5; }
	inline Preset_t736195574 * get_address_of_s_Quality_5() { return &___s_Quality_5; }
	inline void set_s_Quality_5(Preset_t736195574  value)
	{
		___s_Quality_5 = value;
	}

	inline static int32_t get_offset_of_s_ExtremeQuality_6() { return static_cast<int32_t>(offsetof(Preset_t736195574_StaticFields, ___s_ExtremeQuality_6)); }
	inline Preset_t736195574  get_s_ExtremeQuality_6() const { return ___s_ExtremeQuality_6; }
	inline Preset_t736195574 * get_address_of_s_ExtremeQuality_6() { return &___s_ExtremeQuality_6; }
	inline void set_s_ExtremeQuality_6(Preset_t736195574  value)
	{
		___s_ExtremeQuality_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
