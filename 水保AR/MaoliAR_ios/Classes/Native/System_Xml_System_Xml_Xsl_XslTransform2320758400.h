﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.IO.TextWriter
struct TextWriter_t3478189236;
// System.Object
struct Il2CppObject;
// Mono.Xml.Xsl.CompiledStylesheet
struct CompiledStylesheet_t759457236;
// System.Xml.XmlResolver
struct XmlResolver_t626023767;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Xsl.XslTransform
struct  XslTransform_t2320758400  : public Il2CppObject
{
public:
	// System.Object System.Xml.Xsl.XslTransform::debugger
	Il2CppObject * ___debugger_2;
	// Mono.Xml.Xsl.CompiledStylesheet System.Xml.Xsl.XslTransform::s
	CompiledStylesheet_t759457236 * ___s_3;
	// System.Xml.XmlResolver System.Xml.Xsl.XslTransform::xmlResolver
	XmlResolver_t626023767 * ___xmlResolver_4;

public:
	inline static int32_t get_offset_of_debugger_2() { return static_cast<int32_t>(offsetof(XslTransform_t2320758400, ___debugger_2)); }
	inline Il2CppObject * get_debugger_2() const { return ___debugger_2; }
	inline Il2CppObject ** get_address_of_debugger_2() { return &___debugger_2; }
	inline void set_debugger_2(Il2CppObject * value)
	{
		___debugger_2 = value;
		Il2CppCodeGenWriteBarrier(&___debugger_2, value);
	}

	inline static int32_t get_offset_of_s_3() { return static_cast<int32_t>(offsetof(XslTransform_t2320758400, ___s_3)); }
	inline CompiledStylesheet_t759457236 * get_s_3() const { return ___s_3; }
	inline CompiledStylesheet_t759457236 ** get_address_of_s_3() { return &___s_3; }
	inline void set_s_3(CompiledStylesheet_t759457236 * value)
	{
		___s_3 = value;
		Il2CppCodeGenWriteBarrier(&___s_3, value);
	}

	inline static int32_t get_offset_of_xmlResolver_4() { return static_cast<int32_t>(offsetof(XslTransform_t2320758400, ___xmlResolver_4)); }
	inline XmlResolver_t626023767 * get_xmlResolver_4() const { return ___xmlResolver_4; }
	inline XmlResolver_t626023767 ** get_address_of_xmlResolver_4() { return &___xmlResolver_4; }
	inline void set_xmlResolver_4(XmlResolver_t626023767 * value)
	{
		___xmlResolver_4 = value;
		Il2CppCodeGenWriteBarrier(&___xmlResolver_4, value);
	}
};

struct XslTransform_t2320758400_StaticFields
{
public:
	// System.Boolean System.Xml.Xsl.XslTransform::TemplateStackFrameError
	bool ___TemplateStackFrameError_0;
	// System.IO.TextWriter System.Xml.Xsl.XslTransform::TemplateStackFrameOutput
	TextWriter_t3478189236 * ___TemplateStackFrameOutput_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Xsl.XslTransform::<>f__switch$map3D
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map3D_5;

public:
	inline static int32_t get_offset_of_TemplateStackFrameError_0() { return static_cast<int32_t>(offsetof(XslTransform_t2320758400_StaticFields, ___TemplateStackFrameError_0)); }
	inline bool get_TemplateStackFrameError_0() const { return ___TemplateStackFrameError_0; }
	inline bool* get_address_of_TemplateStackFrameError_0() { return &___TemplateStackFrameError_0; }
	inline void set_TemplateStackFrameError_0(bool value)
	{
		___TemplateStackFrameError_0 = value;
	}

	inline static int32_t get_offset_of_TemplateStackFrameOutput_1() { return static_cast<int32_t>(offsetof(XslTransform_t2320758400_StaticFields, ___TemplateStackFrameOutput_1)); }
	inline TextWriter_t3478189236 * get_TemplateStackFrameOutput_1() const { return ___TemplateStackFrameOutput_1; }
	inline TextWriter_t3478189236 ** get_address_of_TemplateStackFrameOutput_1() { return &___TemplateStackFrameOutput_1; }
	inline void set_TemplateStackFrameOutput_1(TextWriter_t3478189236 * value)
	{
		___TemplateStackFrameOutput_1 = value;
		Il2CppCodeGenWriteBarrier(&___TemplateStackFrameOutput_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map3D_5() { return static_cast<int32_t>(offsetof(XslTransform_t2320758400_StaticFields, ___U3CU3Ef__switchU24map3D_5)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map3D_5() const { return ___U3CU3Ef__switchU24map3D_5; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map3D_5() { return &___U3CU3Ef__switchU24map3D_5; }
	inline void set_U3CU3Ef__switchU24map3D_5(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map3D_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map3D_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
