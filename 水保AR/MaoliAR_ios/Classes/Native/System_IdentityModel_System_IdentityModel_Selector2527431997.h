﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Selector2079378378.h"

// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t714049126;
// System.Security.Cryptography.X509Certificates.X509Store
struct X509Store_t2922691911;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Selectors.X509SecurityTokenProvider
struct  X509SecurityTokenProvider_t2527431997  : public SecurityTokenProvider_t2079378378
{
public:
	// System.Security.Cryptography.X509Certificates.X509Certificate2 System.IdentityModel.Selectors.X509SecurityTokenProvider::cert
	X509Certificate2_t714049126 * ___cert_0;
	// System.Security.Cryptography.X509Certificates.X509Store System.IdentityModel.Selectors.X509SecurityTokenProvider::store
	X509Store_t2922691911 * ___store_1;

public:
	inline static int32_t get_offset_of_cert_0() { return static_cast<int32_t>(offsetof(X509SecurityTokenProvider_t2527431997, ___cert_0)); }
	inline X509Certificate2_t714049126 * get_cert_0() const { return ___cert_0; }
	inline X509Certificate2_t714049126 ** get_address_of_cert_0() { return &___cert_0; }
	inline void set_cert_0(X509Certificate2_t714049126 * value)
	{
		___cert_0 = value;
		Il2CppCodeGenWriteBarrier(&___cert_0, value);
	}

	inline static int32_t get_offset_of_store_1() { return static_cast<int32_t>(offsetof(X509SecurityTokenProvider_t2527431997, ___store_1)); }
	inline X509Store_t2922691911 * get_store_1() const { return ___store_1; }
	inline X509Store_t2922691911 ** get_address_of_store_1() { return &___store_1; }
	inline void set_store_1(X509Store_t2922691911 * value)
	{
		___store_1 = value;
		Il2CppCodeGenWriteBarrier(&___store_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
