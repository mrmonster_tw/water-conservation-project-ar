﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_HtmlizedException2999156009.h"

// System.String
struct String_t;
// System.CodeDom.Compiler.CompilerErrorCollection
struct CompilerErrorCollection_t2383307460;
// System.CodeDom.Compiler.CompilerResults
struct CompilerResults_t1736487784;
// System.Int32[]
struct Int32U5BU5D_t385246372;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.CompilationException
struct  CompilationException_t3125132074  : public HtmlizedException_t2999156009
{
public:
	// System.String System.Web.Compilation.CompilationException::filename
	String_t* ___filename_14;
	// System.CodeDom.Compiler.CompilerErrorCollection System.Web.Compilation.CompilationException::errors
	CompilerErrorCollection_t2383307460 * ___errors_15;
	// System.CodeDom.Compiler.CompilerResults System.Web.Compilation.CompilationException::results
	CompilerResults_t1736487784 * ___results_16;
	// System.String System.Web.Compilation.CompilationException::fileText
	String_t* ___fileText_17;
	// System.String System.Web.Compilation.CompilationException::errmsg
	String_t* ___errmsg_18;
	// System.Int32[] System.Web.Compilation.CompilationException::errorLines
	Int32U5BU5D_t385246372* ___errorLines_19;

public:
	inline static int32_t get_offset_of_filename_14() { return static_cast<int32_t>(offsetof(CompilationException_t3125132074, ___filename_14)); }
	inline String_t* get_filename_14() const { return ___filename_14; }
	inline String_t** get_address_of_filename_14() { return &___filename_14; }
	inline void set_filename_14(String_t* value)
	{
		___filename_14 = value;
		Il2CppCodeGenWriteBarrier(&___filename_14, value);
	}

	inline static int32_t get_offset_of_errors_15() { return static_cast<int32_t>(offsetof(CompilationException_t3125132074, ___errors_15)); }
	inline CompilerErrorCollection_t2383307460 * get_errors_15() const { return ___errors_15; }
	inline CompilerErrorCollection_t2383307460 ** get_address_of_errors_15() { return &___errors_15; }
	inline void set_errors_15(CompilerErrorCollection_t2383307460 * value)
	{
		___errors_15 = value;
		Il2CppCodeGenWriteBarrier(&___errors_15, value);
	}

	inline static int32_t get_offset_of_results_16() { return static_cast<int32_t>(offsetof(CompilationException_t3125132074, ___results_16)); }
	inline CompilerResults_t1736487784 * get_results_16() const { return ___results_16; }
	inline CompilerResults_t1736487784 ** get_address_of_results_16() { return &___results_16; }
	inline void set_results_16(CompilerResults_t1736487784 * value)
	{
		___results_16 = value;
		Il2CppCodeGenWriteBarrier(&___results_16, value);
	}

	inline static int32_t get_offset_of_fileText_17() { return static_cast<int32_t>(offsetof(CompilationException_t3125132074, ___fileText_17)); }
	inline String_t* get_fileText_17() const { return ___fileText_17; }
	inline String_t** get_address_of_fileText_17() { return &___fileText_17; }
	inline void set_fileText_17(String_t* value)
	{
		___fileText_17 = value;
		Il2CppCodeGenWriteBarrier(&___fileText_17, value);
	}

	inline static int32_t get_offset_of_errmsg_18() { return static_cast<int32_t>(offsetof(CompilationException_t3125132074, ___errmsg_18)); }
	inline String_t* get_errmsg_18() const { return ___errmsg_18; }
	inline String_t** get_address_of_errmsg_18() { return &___errmsg_18; }
	inline void set_errmsg_18(String_t* value)
	{
		___errmsg_18 = value;
		Il2CppCodeGenWriteBarrier(&___errmsg_18, value);
	}

	inline static int32_t get_offset_of_errorLines_19() { return static_cast<int32_t>(offsetof(CompilationException_t3125132074, ___errorLines_19)); }
	inline Int32U5BU5D_t385246372* get_errorLines_19() const { return ___errorLines_19; }
	inline Int32U5BU5D_t385246372** get_address_of_errorLines_19() { return &___errorLines_19; }
	inline void set_errorLines_19(Int32U5BU5D_t385246372* value)
	{
		___errorLines_19 = value;
		Il2CppCodeGenWriteBarrier(&___errorLines_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
