﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3549286319.h"
#include "Mono_Posix_U3CModuleU3E692745525.h"
#include "Mono_Posix_MapAttribute440548554.h"
#include "mscorlib_System_Void1185182177.h"
#include "mscorlib_System_Attribute861562559.h"
#include "mscorlib_System_String1847450689.h"
#include "Mono_Posix_Mono_Unix_Native_FileNameMarshaler4011664002.h"
#include "mscorlib_System_Object3080106164.h"
#include "Mono_Posix_Mono_Unix_Native_FilePermissions1971995193.h"
#include "Mono_Posix_Mono_Unix_Native_SignalHandler1590986791.h"
#include "mscorlib_System_IntPtr840150181.h"
#include "mscorlib_System_Int322950945753.h"
#include "mscorlib_System_AsyncCallback3962456242.h"
#include "Mono_Posix_Mono_Unix_Native_Signum4209417157.h"
#include "Mono_Posix_Mono_Unix_Native_Stdlib3308777473.h"
#include "mscorlib_System_RuntimeTypeHandle3027515415.h"
#include "mscorlib_System_Type2483944760.h"
#include "mscorlib_System_IO_TextWriter3478189236.h"
#include "Mono_Posix_Mono_Unix_Native_Syscall2623000133.h"
#include "mscorlib_System_UInt644134040092.h"

// MapAttribute
struct MapAttribute_t440548554;
// System.Attribute
struct Attribute_t861562559;
// System.String
struct String_t;
// Mono.Unix.Native.FileNameMarshaler
struct FileNameMarshaler_t4011664002;
// System.Object
struct Il2CppObject;
// Mono.Unix.Native.SignalHandler
struct SignalHandler_t1590986791;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Type
struct Type_t;
// System.Array
struct Il2CppArray;
// System.IO.TextWriter
struct TextWriter_t3478189236;
extern Il2CppClass* FileNameMarshaler_t4011664002_il2cpp_TypeInfo_var;
extern const uint32_t FileNameMarshaler__cctor_m385830040_MetadataUsageId;
extern Il2CppClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern const uint32_t SignalHandler_BeginInvoke_m1218707962_MetadataUsageId;
extern const Il2CppType* Signum_t4209417157_0_0_0_var;
extern Il2CppClass* Stdlib_t3308777473_il2cpp_TypeInfo_var;
extern Il2CppClass* SignalHandler_t1590986791_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t4135868527_il2cpp_TypeInfo_var;
extern Il2CppClass* SignalHandlerU5BU5D_t3624871774_il2cpp_TypeInfo_var;
extern const MethodInfo* Stdlib__DefaultHandler_m2028008363_MethodInfo_var;
extern const MethodInfo* Stdlib__ErrorHandler_m1157198641_MethodInfo_var;
extern const MethodInfo* Stdlib__IgnoreHandler_m4246007571_MethodInfo_var;
extern const uint32_t Stdlib__cctor_m1233279594_MetadataUsageId;
extern Il2CppClass* Console_t3208230065_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3081411587;
extern Il2CppCodeGenString* _stringLiteral3506992033;
extern const uint32_t Stdlib__ErrorHandler_m1157198641_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1804036214;
extern const uint32_t Stdlib__DefaultHandler_m2028008363_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1117258614;
extern const uint32_t Stdlib__IgnoreHandler_m4246007571_MetadataUsageId;
extern Il2CppClass* Syscall_t2623000133_il2cpp_TypeInfo_var;
extern const uint32_t Syscall__cctor_m3858266335_MetadataUsageId;

// Mono.Unix.Native.SignalHandler[]
struct SignalHandlerU5BU5D_t3624871774  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SignalHandler_t1590986791 * m_Items[1];

public:
	inline SignalHandler_t1590986791 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SignalHandler_t1590986791 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SignalHandler_t1590986791 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline SignalHandler_t1590986791 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SignalHandler_t1590986791 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SignalHandler_t1590986791 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};



// System.Void System.Attribute::.ctor()
extern "C"  void Attribute__ctor_m1529526131 (Attribute_t861562559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Unix.Native.FileNameMarshaler::.ctor()
extern "C"  void FileNameMarshaler__ctor_m1660406898 (FileNameMarshaler_t4011664002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Unix.Native.SignalHandler::Invoke(System.Int32)
extern "C"  void SignalHandler_Invoke_m1706898168 (SignalHandler_t1590986791 * __this, int32_t ___signal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Unix.Native.Stdlib::GetDefaultSignal()
extern "C"  IntPtr_t Stdlib_GetDefaultSignal_m244206146 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Unix.Native.Stdlib::GetErrorSignal()
extern "C"  IntPtr_t Stdlib_GetErrorSignal_m1976333349 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Unix.Native.Stdlib::GetIgnoreSignal()
extern "C"  IntPtr_t Stdlib_GetIgnoreSignal_m1284219770 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Unix.Native.SignalHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void SignalHandler__ctor_m149715264 (SignalHandler_t1590986791 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Unix.Native.Stdlib::GetFullyBuffered()
extern "C"  int32_t Stdlib_GetFullyBuffered_m957807578 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Unix.Native.Stdlib::GetLineBuffered()
extern "C"  int32_t Stdlib_GetLineBuffered_m2749366463 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Unix.Native.Stdlib::GetNonBuffered()
extern "C"  int32_t Stdlib_GetNonBuffered_m2367686862 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Unix.Native.Stdlib::GetBufferSize()
extern "C"  int32_t Stdlib_GetBufferSize_m844208237 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Unix.Native.Stdlib::GetEOF()
extern "C"  int32_t Stdlib_GetEOF_m1009044156 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Unix.Native.Stdlib::GetFopenMax()
extern "C"  int32_t Stdlib_GetFopenMax_m17051811 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Unix.Native.Stdlib::GetFilenameMax()
extern "C"  int32_t Stdlib_GetFilenameMax_m3375712948 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Unix.Native.Stdlib::GetTmpnamLength()
extern "C"  int32_t Stdlib_GetTmpnamLength_m1266675616 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Unix.Native.Stdlib::GetStandardError()
extern "C"  IntPtr_t Stdlib_GetStandardError_m3660521923 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Unix.Native.Stdlib::GetStandardInput()
extern "C"  IntPtr_t Stdlib_GetStandardInput_m3930908709 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Unix.Native.Stdlib::GetStandardOutput()
extern "C"  IntPtr_t Stdlib_GetStandardOutput_m2021102671 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Unix.Native.Stdlib::GetTmpMax()
extern "C"  int32_t Stdlib_GetTmpMax_m1870318599 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Unix.Native.Stdlib::GetExitFailure()
extern "C"  int32_t Stdlib_GetExitFailure_m3631902857 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Unix.Native.Stdlib::GetExitSuccess()
extern "C"  int32_t Stdlib_GetExitSuccess_m2518085135 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Unix.Native.Stdlib::GetMbCurMax()
extern "C"  int32_t Stdlib_GetMbCurMax_m1131594865 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Unix.Native.Stdlib::GetRandMax()
extern "C"  int32_t Stdlib_GetRandMax_m1945737296 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m1620074514 (Il2CppObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Array System.Enum::GetValues(System.Type)
extern "C"  Il2CppArray * Enum_GetValues_m4192343468 (Il2CppObject * __this /* static, unused */, Type_t * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::get_Length()
extern "C"  int32_t Array_get_Length_m21610649 (Il2CppArray * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array::GetValue(System.Int32)
extern "C"  Il2CppObject * Array_GetValue_m2528546681 (Il2CppArray * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.TextWriter System.Console::get_Error()
extern "C"  TextWriter_t3478189236 * Console_get_Error_m1839879495 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object,System.Object)
extern "C"  String_t* String_Concat_m1715369213 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, Il2CppObject * p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Unix.Native.Syscall::_L_ctermid()
extern "C"  int32_t Syscall__L_ctermid_m3613822146 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Unix.Native.Syscall::_L_cuserid()
extern "C"  int32_t Syscall__L_cuserid_m2335993338 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.IntPtr::op_Explicit(System.Int32)
extern "C"  IntPtr_t IntPtr_op_Explicit_m1593216315 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MapAttribute::.ctor()
extern "C"  void MapAttribute__ctor_m3482542387 (MapAttribute_t440548554 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MapAttribute::set_SuppressFlags(System.String)
extern "C"  void MapAttribute_set_SuppressFlags_m2492530130 (MapAttribute_t440548554 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_suppressFlags_0(L_0);
		return;
	}
}
// System.Void Mono.Unix.Native.FileNameMarshaler::.ctor()
extern "C"  void FileNameMarshaler__ctor_m1660406898 (FileNameMarshaler_t4011664002 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Unix.Native.FileNameMarshaler::.cctor()
extern "C"  void FileNameMarshaler__cctor_m385830040 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FileNameMarshaler__cctor_m385830040_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FileNameMarshaler_t4011664002 * L_0 = (FileNameMarshaler_t4011664002 *)il2cpp_codegen_object_new(FileNameMarshaler_t4011664002_il2cpp_TypeInfo_var);
		FileNameMarshaler__ctor_m1660406898(L_0, /*hidden argument*/NULL);
		((FileNameMarshaler_t4011664002_StaticFields*)FileNameMarshaler_t4011664002_il2cpp_TypeInfo_var->static_fields)->set_Instance_0(L_0);
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_SignalHandler_t1590986791 (SignalHandler_t1590986791 * __this, int32_t ___signal0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___signal0);

}
// System.Void Mono.Unix.Native.SignalHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void SignalHandler__ctor_m149715264 (SignalHandler_t1590986791 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Mono.Unix.Native.SignalHandler::Invoke(System.Int32)
extern "C"  void SignalHandler_Invoke_m1706898168 (SignalHandler_t1590986791 * __this, int32_t ___signal0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		SignalHandler_Invoke_m1706898168((SignalHandler_t1590986791 *)__this->get_prev_9(),___signal0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___signal0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___signal0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___signal0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___signal0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult Mono.Unix.Native.SignalHandler::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SignalHandler_BeginInvoke_m1218707962 (SignalHandler_t1590986791 * __this, int32_t ___signal0, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SignalHandler_BeginInvoke_m1218707962_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &___signal0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void Mono.Unix.Native.SignalHandler::EndInvoke(System.IAsyncResult)
extern "C"  void SignalHandler_EndInvoke_m361464107 (SignalHandler_t1590986791 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Mono.Unix.Native.Stdlib::.cctor()
extern "C"  void Stdlib__cctor_m1233279594 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Stdlib__cctor_m1233279594_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppArray * V_0 = NULL;
	{
		IntPtr_t L_0 = Stdlib_GetDefaultSignal_m244206146(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Stdlib_t3308777473_StaticFields*)Stdlib_t3308777473_il2cpp_TypeInfo_var->static_fields)->set__SIG_DFL_0(L_0);
		IntPtr_t L_1 = Stdlib_GetErrorSignal_m1976333349(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Stdlib_t3308777473_StaticFields*)Stdlib_t3308777473_il2cpp_TypeInfo_var->static_fields)->set__SIG_ERR_1(L_1);
		IntPtr_t L_2 = Stdlib_GetIgnoreSignal_m1284219770(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Stdlib_t3308777473_StaticFields*)Stdlib_t3308777473_il2cpp_TypeInfo_var->static_fields)->set__SIG_IGN_2(L_2);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)Stdlib__DefaultHandler_m2028008363_MethodInfo_var);
		SignalHandler_t1590986791 * L_4 = (SignalHandler_t1590986791 *)il2cpp_codegen_object_new(SignalHandler_t1590986791_il2cpp_TypeInfo_var);
		SignalHandler__ctor_m149715264(L_4, NULL, L_3, /*hidden argument*/NULL);
		((Stdlib_t3308777473_StaticFields*)Stdlib_t3308777473_il2cpp_TypeInfo_var->static_fields)->set_SIG_DFL_3(L_4);
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)Stdlib__ErrorHandler_m1157198641_MethodInfo_var);
		SignalHandler_t1590986791 * L_6 = (SignalHandler_t1590986791 *)il2cpp_codegen_object_new(SignalHandler_t1590986791_il2cpp_TypeInfo_var);
		SignalHandler__ctor_m149715264(L_6, NULL, L_5, /*hidden argument*/NULL);
		((Stdlib_t3308777473_StaticFields*)Stdlib_t3308777473_il2cpp_TypeInfo_var->static_fields)->set_SIG_ERR_4(L_6);
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)Stdlib__IgnoreHandler_m4246007571_MethodInfo_var);
		SignalHandler_t1590986791 * L_8 = (SignalHandler_t1590986791 *)il2cpp_codegen_object_new(SignalHandler_t1590986791_il2cpp_TypeInfo_var);
		SignalHandler__ctor_m149715264(L_8, NULL, L_7, /*hidden argument*/NULL);
		((Stdlib_t3308777473_StaticFields*)Stdlib_t3308777473_il2cpp_TypeInfo_var->static_fields)->set_SIG_IGN_5(L_8);
		int32_t L_9 = Stdlib_GetFullyBuffered_m957807578(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Stdlib_t3308777473_StaticFields*)Stdlib_t3308777473_il2cpp_TypeInfo_var->static_fields)->set__IOFBF_7(L_9);
		int32_t L_10 = Stdlib_GetLineBuffered_m2749366463(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Stdlib_t3308777473_StaticFields*)Stdlib_t3308777473_il2cpp_TypeInfo_var->static_fields)->set__IOLBF_8(L_10);
		int32_t L_11 = Stdlib_GetNonBuffered_m2367686862(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Stdlib_t3308777473_StaticFields*)Stdlib_t3308777473_il2cpp_TypeInfo_var->static_fields)->set__IONBF_9(L_11);
		int32_t L_12 = Stdlib_GetBufferSize_m844208237(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Stdlib_t3308777473_StaticFields*)Stdlib_t3308777473_il2cpp_TypeInfo_var->static_fields)->set_BUFSIZ_10(L_12);
		int32_t L_13 = Stdlib_GetEOF_m1009044156(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Stdlib_t3308777473_StaticFields*)Stdlib_t3308777473_il2cpp_TypeInfo_var->static_fields)->set_EOF_11(L_13);
		int32_t L_14 = Stdlib_GetFopenMax_m17051811(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Stdlib_t3308777473_StaticFields*)Stdlib_t3308777473_il2cpp_TypeInfo_var->static_fields)->set_FOPEN_MAX_12(L_14);
		int32_t L_15 = Stdlib_GetFilenameMax_m3375712948(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Stdlib_t3308777473_StaticFields*)Stdlib_t3308777473_il2cpp_TypeInfo_var->static_fields)->set_FILENAME_MAX_13(L_15);
		int32_t L_16 = Stdlib_GetTmpnamLength_m1266675616(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Stdlib_t3308777473_StaticFields*)Stdlib_t3308777473_il2cpp_TypeInfo_var->static_fields)->set_L_tmpnam_14(L_16);
		IntPtr_t L_17 = Stdlib_GetStandardError_m3660521923(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Stdlib_t3308777473_StaticFields*)Stdlib_t3308777473_il2cpp_TypeInfo_var->static_fields)->set_stderr_15(L_17);
		IntPtr_t L_18 = Stdlib_GetStandardInput_m3930908709(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Stdlib_t3308777473_StaticFields*)Stdlib_t3308777473_il2cpp_TypeInfo_var->static_fields)->set_stdin_16(L_18);
		IntPtr_t L_19 = Stdlib_GetStandardOutput_m2021102671(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Stdlib_t3308777473_StaticFields*)Stdlib_t3308777473_il2cpp_TypeInfo_var->static_fields)->set_stdout_17(L_19);
		int32_t L_20 = Stdlib_GetTmpMax_m1870318599(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Stdlib_t3308777473_StaticFields*)Stdlib_t3308777473_il2cpp_TypeInfo_var->static_fields)->set_TMP_MAX_18(L_20);
		Il2CppObject * L_21 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m297566312(L_21, /*hidden argument*/NULL);
		((Stdlib_t3308777473_StaticFields*)Stdlib_t3308777473_il2cpp_TypeInfo_var->static_fields)->set_tmpnam_lock_19(L_21);
		int32_t L_22 = Stdlib_GetExitFailure_m3631902857(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Stdlib_t3308777473_StaticFields*)Stdlib_t3308777473_il2cpp_TypeInfo_var->static_fields)->set_EXIT_FAILURE_20(L_22);
		int32_t L_23 = Stdlib_GetExitSuccess_m2518085135(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Stdlib_t3308777473_StaticFields*)Stdlib_t3308777473_il2cpp_TypeInfo_var->static_fields)->set_EXIT_SUCCESS_21(L_23);
		int32_t L_24 = Stdlib_GetMbCurMax_m1131594865(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Stdlib_t3308777473_StaticFields*)Stdlib_t3308777473_il2cpp_TypeInfo_var->static_fields)->set_MB_CUR_MAX_22(L_24);
		int32_t L_25 = Stdlib_GetRandMax_m1945737296(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Stdlib_t3308777473_StaticFields*)Stdlib_t3308777473_il2cpp_TypeInfo_var->static_fields)->set_RAND_MAX_23(L_25);
		Il2CppObject * L_26 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m297566312(L_26, /*hidden argument*/NULL);
		((Stdlib_t3308777473_StaticFields*)Stdlib_t3308777473_il2cpp_TypeInfo_var->static_fields)->set_strerror_lock_24(L_26);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Signum_t4209417157_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t4135868527_il2cpp_TypeInfo_var);
		Il2CppArray * L_28 = Enum_GetValues_m4192343468(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		V_0 = L_28;
		Il2CppArray * L_29 = V_0;
		Il2CppArray * L_30 = V_0;
		NullCheck(L_30);
		int32_t L_31 = Array_get_Length_m21610649(L_30, /*hidden argument*/NULL);
		NullCheck(L_29);
		Il2CppObject * L_32 = Array_GetValue_m2528546681(L_29, ((int32_t)((int32_t)L_31-(int32_t)1)), /*hidden argument*/NULL);
		((Stdlib_t3308777473_StaticFields*)Stdlib_t3308777473_il2cpp_TypeInfo_var->static_fields)->set_registered_signals_6(((SignalHandlerU5BU5D_t3624871774*)SZArrayNew(SignalHandlerU5BU5D_t3624871774_il2cpp_TypeInfo_var, (uint32_t)((*(int32_t*)((int32_t*)UnBox(L_32, Int32_t2950945753_il2cpp_TypeInfo_var)))))));
		return;
	}
}
// System.IntPtr Mono.Unix.Native.Stdlib::GetDefaultSignal()
extern "C"  IntPtr_t Stdlib_GetDefaultSignal_m244206146 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef intptr_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MonoPosixHelper"), "Mono_Posix_Stdlib_SIG_DFL", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GetDefaultSignal'"));
		}
	}

	// Native function invocation
	intptr_t returnValue = il2cppPInvokeFunc();

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
// System.IntPtr Mono.Unix.Native.Stdlib::GetErrorSignal()
extern "C"  IntPtr_t Stdlib_GetErrorSignal_m1976333349 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef intptr_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MonoPosixHelper"), "Mono_Posix_Stdlib_SIG_ERR", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GetErrorSignal'"));
		}
	}

	// Native function invocation
	intptr_t returnValue = il2cppPInvokeFunc();

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
// System.IntPtr Mono.Unix.Native.Stdlib::GetIgnoreSignal()
extern "C"  IntPtr_t Stdlib_GetIgnoreSignal_m1284219770 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef intptr_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MonoPosixHelper"), "Mono_Posix_Stdlib_SIG_IGN", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GetIgnoreSignal'"));
		}
	}

	// Native function invocation
	intptr_t returnValue = il2cppPInvokeFunc();

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
// System.Void Mono.Unix.Native.Stdlib::_ErrorHandler(System.Int32)
extern "C"  void Stdlib__ErrorHandler_m1157198641 (Il2CppObject * __this /* static, unused */, int32_t ___signum0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Stdlib__ErrorHandler_m1157198641_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t3208230065_il2cpp_TypeInfo_var);
		TextWriter_t3478189236 * L_0 = Console_get_Error_m1839879495(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___signum0;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1715369213(NULL /*static, unused*/, _stringLiteral3081411587, L_3, _stringLiteral3506992033, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(25 /* System.Void System.IO.TextWriter::WriteLine(System.String) */, L_0, L_4);
		return;
	}
}
// System.Void Mono.Unix.Native.Stdlib::_DefaultHandler(System.Int32)
extern "C"  void Stdlib__DefaultHandler_m2028008363 (Il2CppObject * __this /* static, unused */, int32_t ___signum0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Stdlib__DefaultHandler_m2028008363_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t3208230065_il2cpp_TypeInfo_var);
		TextWriter_t3478189236 * L_0 = Console_get_Error_m1839879495(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___signum0;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1715369213(NULL /*static, unused*/, _stringLiteral1804036214, L_3, _stringLiteral3506992033, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(25 /* System.Void System.IO.TextWriter::WriteLine(System.String) */, L_0, L_4);
		return;
	}
}
// System.Void Mono.Unix.Native.Stdlib::_IgnoreHandler(System.Int32)
extern "C"  void Stdlib__IgnoreHandler_m4246007571 (Il2CppObject * __this /* static, unused */, int32_t ___signum0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Stdlib__IgnoreHandler_m4246007571_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t3208230065_il2cpp_TypeInfo_var);
		TextWriter_t3478189236 * L_0 = Console_get_Error_m1839879495(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___signum0;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1715369213(NULL /*static, unused*/, _stringLiteral1117258614, L_3, _stringLiteral3506992033, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(25 /* System.Void System.IO.TextWriter::WriteLine(System.String) */, L_0, L_4);
		return;
	}
}
// System.Int32 Mono.Unix.Native.Stdlib::GetFullyBuffered()
extern "C"  int32_t Stdlib_GetFullyBuffered_m957807578 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MonoPosixHelper"), "Mono_Posix_Stdlib__IOFBF", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GetFullyBuffered'"));
		}
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc();

	return returnValue;
}
// System.Int32 Mono.Unix.Native.Stdlib::GetLineBuffered()
extern "C"  int32_t Stdlib_GetLineBuffered_m2749366463 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MonoPosixHelper"), "Mono_Posix_Stdlib__IOLBF", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GetLineBuffered'"));
		}
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc();

	return returnValue;
}
// System.Int32 Mono.Unix.Native.Stdlib::GetNonBuffered()
extern "C"  int32_t Stdlib_GetNonBuffered_m2367686862 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MonoPosixHelper"), "Mono_Posix_Stdlib__IONBF", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GetNonBuffered'"));
		}
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc();

	return returnValue;
}
// System.Int32 Mono.Unix.Native.Stdlib::GetBufferSize()
extern "C"  int32_t Stdlib_GetBufferSize_m844208237 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MonoPosixHelper"), "Mono_Posix_Stdlib_BUFSIZ", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GetBufferSize'"));
		}
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc();

	return returnValue;
}
// System.Int32 Mono.Unix.Native.Stdlib::GetEOF()
extern "C"  int32_t Stdlib_GetEOF_m1009044156 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MonoPosixHelper"), "Mono_Posix_Stdlib_EOF", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GetEOF'"));
		}
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc();

	return returnValue;
}
// System.Int32 Mono.Unix.Native.Stdlib::GetFilenameMax()
extern "C"  int32_t Stdlib_GetFilenameMax_m3375712948 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MonoPosixHelper"), "Mono_Posix_Stdlib_FILENAME_MAX", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GetFilenameMax'"));
		}
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc();

	return returnValue;
}
// System.Int32 Mono.Unix.Native.Stdlib::GetFopenMax()
extern "C"  int32_t Stdlib_GetFopenMax_m17051811 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MonoPosixHelper"), "Mono_Posix_Stdlib_FOPEN_MAX", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GetFopenMax'"));
		}
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc();

	return returnValue;
}
// System.Int32 Mono.Unix.Native.Stdlib::GetTmpnamLength()
extern "C"  int32_t Stdlib_GetTmpnamLength_m1266675616 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MonoPosixHelper"), "Mono_Posix_Stdlib_L_tmpnam", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GetTmpnamLength'"));
		}
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc();

	return returnValue;
}
// System.IntPtr Mono.Unix.Native.Stdlib::GetStandardInput()
extern "C"  IntPtr_t Stdlib_GetStandardInput_m3930908709 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef intptr_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MonoPosixHelper"), "Mono_Posix_Stdlib_stdin", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GetStandardInput'"));
		}
	}

	// Native function invocation
	intptr_t returnValue = il2cppPInvokeFunc();

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
// System.IntPtr Mono.Unix.Native.Stdlib::GetStandardOutput()
extern "C"  IntPtr_t Stdlib_GetStandardOutput_m2021102671 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef intptr_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MonoPosixHelper"), "Mono_Posix_Stdlib_stdout", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GetStandardOutput'"));
		}
	}

	// Native function invocation
	intptr_t returnValue = il2cppPInvokeFunc();

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
// System.IntPtr Mono.Unix.Native.Stdlib::GetStandardError()
extern "C"  IntPtr_t Stdlib_GetStandardError_m3660521923 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef intptr_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MonoPosixHelper"), "Mono_Posix_Stdlib_stderr", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GetStandardError'"));
		}
	}

	// Native function invocation
	intptr_t returnValue = il2cppPInvokeFunc();

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
// System.Int32 Mono.Unix.Native.Stdlib::GetTmpMax()
extern "C"  int32_t Stdlib_GetTmpMax_m1870318599 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MonoPosixHelper"), "Mono_Posix_Stdlib_TMP_MAX", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GetTmpMax'"));
		}
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc();

	return returnValue;
}
// System.Int32 Mono.Unix.Native.Stdlib::GetExitFailure()
extern "C"  int32_t Stdlib_GetExitFailure_m3631902857 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MonoPosixHelper"), "Mono_Posix_Stdlib_EXIT_FAILURE", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GetExitFailure'"));
		}
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc();

	return returnValue;
}
// System.Int32 Mono.Unix.Native.Stdlib::GetExitSuccess()
extern "C"  int32_t Stdlib_GetExitSuccess_m2518085135 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MonoPosixHelper"), "Mono_Posix_Stdlib_EXIT_SUCCESS", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GetExitSuccess'"));
		}
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc();

	return returnValue;
}
// System.Int32 Mono.Unix.Native.Stdlib::GetMbCurMax()
extern "C"  int32_t Stdlib_GetMbCurMax_m1131594865 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MonoPosixHelper"), "Mono_Posix_Stdlib_MB_CUR_MAX", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GetMbCurMax'"));
		}
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc();

	return returnValue;
}
// System.Int32 Mono.Unix.Native.Stdlib::GetRandMax()
extern "C"  int32_t Stdlib_GetRandMax_m1945737296 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (CDECL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MonoPosixHelper"), "Mono_Posix_Stdlib_RAND_MAX", IL2CPP_CALL_C, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GetRandMax'"));
		}
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc();

	return returnValue;
}
// System.Void Mono.Unix.Native.Syscall::.cctor()
extern "C"  void Syscall__cctor_m3858266335 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Syscall__cctor_m3858266335_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m297566312(L_0, /*hidden argument*/NULL);
		((Syscall_t2623000133_StaticFields*)Syscall_t2623000133_il2cpp_TypeInfo_var->static_fields)->set_readdir_lock_25(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m297566312(L_1, /*hidden argument*/NULL);
		((Syscall_t2623000133_StaticFields*)Syscall_t2623000133_il2cpp_TypeInfo_var->static_fields)->set_fstab_lock_26(L_1);
		Il2CppObject * L_2 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m297566312(L_2, /*hidden argument*/NULL);
		((Syscall_t2623000133_StaticFields*)Syscall_t2623000133_il2cpp_TypeInfo_var->static_fields)->set_grp_lock_27(L_2);
		Il2CppObject * L_3 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m297566312(L_3, /*hidden argument*/NULL);
		((Syscall_t2623000133_StaticFields*)Syscall_t2623000133_il2cpp_TypeInfo_var->static_fields)->set_pwd_lock_28(L_3);
		Il2CppObject * L_4 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m297566312(L_4, /*hidden argument*/NULL);
		((Syscall_t2623000133_StaticFields*)Syscall_t2623000133_il2cpp_TypeInfo_var->static_fields)->set_signal_lock_29(L_4);
		int32_t L_5 = Syscall__L_ctermid_m3613822146(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Syscall_t2623000133_StaticFields*)Syscall_t2623000133_il2cpp_TypeInfo_var->static_fields)->set_L_ctermid_30(L_5);
		int32_t L_6 = Syscall__L_cuserid_m2335993338(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Syscall_t2623000133_StaticFields*)Syscall_t2623000133_il2cpp_TypeInfo_var->static_fields)->set_L_cuserid_31(L_6);
		Il2CppObject * L_7 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m297566312(L_7, /*hidden argument*/NULL);
		((Syscall_t2623000133_StaticFields*)Syscall_t2623000133_il2cpp_TypeInfo_var->static_fields)->set_getlogin_lock_32(L_7);
		IntPtr_t L_8 = IntPtr_op_Explicit_m1593216315(NULL /*static, unused*/, (-1), /*hidden argument*/NULL);
		((Syscall_t2623000133_StaticFields*)Syscall_t2623000133_il2cpp_TypeInfo_var->static_fields)->set_MAP_FAILED_33(L_8);
		Il2CppObject * L_9 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m297566312(L_9, /*hidden argument*/NULL);
		((Syscall_t2623000133_StaticFields*)Syscall_t2623000133_il2cpp_TypeInfo_var->static_fields)->set_tty_lock_34(L_9);
		Il2CppObject * L_10 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m297566312(L_10, /*hidden argument*/NULL);
		((Syscall_t2623000133_StaticFields*)Syscall_t2623000133_il2cpp_TypeInfo_var->static_fields)->set_usershell_lock_35(L_10);
		return;
	}
}
// System.Int32 Mono.Unix.Native.Syscall::_L_ctermid()
extern "C"  int32_t Syscall__L_ctermid_m3613822146 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MonoPosixHelper"), "Mono_Posix_Syscall_L_ctermid", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: '_L_ctermid'"));
		}
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc();

	return returnValue;
}
// System.Int32 Mono.Unix.Native.Syscall::_L_cuserid()
extern "C"  int32_t Syscall__L_cuserid_m2335993338 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MonoPosixHelper"), "Mono_Posix_Syscall_L_cuserid", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: '_L_cuserid'"));
		}
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc();

	return returnValue;
}
// System.Int32 Mono.Unix.Native.Syscall::mknod(System.String,Mono.Unix.Native.FilePermissions,System.UInt64)
extern "C"  int32_t Syscall_mknod_m3463737514 (Il2CppObject * __this /* static, unused */, String_t* ___pathname0, uint32_t ___mode1, uint64_t ___dev2, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*, uint32_t, uint64_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(char*) + sizeof(uint32_t) + sizeof(uint64_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MonoPosixHelper"), "Mono_Posix_Syscall_mknod", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'mknod'"));
		}
	}

	// Marshaling of parameter '___pathname0' to native representation
	char* ____pathname0_marshaled = NULL;
	____pathname0_marshaled = il2cpp_codegen_marshal_string(___pathname0);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(____pathname0_marshaled, ___mode1, ___dev2);
	il2cpp_codegen_marshal_store_last_error();

	// Marshaling cleanup of parameter '___pathname0' native representation
	il2cpp_codegen_marshal_free(____pathname0_marshaled);
	____pathname0_marshaled = NULL;

	return returnValue;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
