﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Serialization_XmlSerializatio982275218.h"

// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSerializationWriter
struct  XmlSchemaSerializationWriter_t52073008  : public XmlSerializationWriter_t982275218
{
public:

public:
};

struct XmlSchemaSerializationWriter_t52073008_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaSerializationWriter::<>f__switch$map49
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map49_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map49_8() { return static_cast<int32_t>(offsetof(XmlSchemaSerializationWriter_t52073008_StaticFields, ___U3CU3Ef__switchU24map49_8)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map49_8() const { return ___U3CU3Ef__switchU24map49_8; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map49_8() { return &___U3CU3Ef__switchU24map49_8; }
	inline void set_U3CU3Ef__switchU24map49_8(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map49_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map49_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
