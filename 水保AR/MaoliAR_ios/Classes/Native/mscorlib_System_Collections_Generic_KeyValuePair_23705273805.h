﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"

// System.ServiceModel.Description.MessageBodyDescription
struct MessageBodyDescription_t2541387169;
// System.Xml.Serialization.XmlSerializer
struct XmlSerializer_t1117804635;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.ServiceModel.Description.MessageBodyDescription,System.Xml.Serialization.XmlSerializer>
struct  KeyValuePair_2_t3705273805 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	MessageBodyDescription_t2541387169 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	XmlSerializer_t1117804635 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3705273805, ___key_0)); }
	inline MessageBodyDescription_t2541387169 * get_key_0() const { return ___key_0; }
	inline MessageBodyDescription_t2541387169 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(MessageBodyDescription_t2541387169 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier(&___key_0, value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3705273805, ___value_1)); }
	inline XmlSerializer_t1117804635 * get_value_1() const { return ___value_1; }
	inline XmlSerializer_t1117804635 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(XmlSerializer_t1117804635 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
