﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.StandardBindingReliableSessionElement
struct  StandardBindingReliableSessionElement_t1899919908  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct StandardBindingReliableSessionElement_t1899919908_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.StandardBindingReliableSessionElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.StandardBindingReliableSessionElement::inactivity_timeout
	ConfigurationProperty_t3590861854 * ___inactivity_timeout_14;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.StandardBindingReliableSessionElement::ordered
	ConfigurationProperty_t3590861854 * ___ordered_15;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(StandardBindingReliableSessionElement_t1899919908_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier(&___properties_13, value);
	}

	inline static int32_t get_offset_of_inactivity_timeout_14() { return static_cast<int32_t>(offsetof(StandardBindingReliableSessionElement_t1899919908_StaticFields, ___inactivity_timeout_14)); }
	inline ConfigurationProperty_t3590861854 * get_inactivity_timeout_14() const { return ___inactivity_timeout_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_inactivity_timeout_14() { return &___inactivity_timeout_14; }
	inline void set_inactivity_timeout_14(ConfigurationProperty_t3590861854 * value)
	{
		___inactivity_timeout_14 = value;
		Il2CppCodeGenWriteBarrier(&___inactivity_timeout_14, value);
	}

	inline static int32_t get_offset_of_ordered_15() { return static_cast<int32_t>(offsetof(StandardBindingReliableSessionElement_t1899919908_StaticFields, ___ordered_15)); }
	inline ConfigurationProperty_t3590861854 * get_ordered_15() const { return ___ordered_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_ordered_15() { return &___ordered_15; }
	inline void set_ordered_15(ConfigurationProperty_t3590861854 * value)
	{
		___ordered_15 = value;
		Il2CppCodeGenWriteBarrier(&___ordered_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
