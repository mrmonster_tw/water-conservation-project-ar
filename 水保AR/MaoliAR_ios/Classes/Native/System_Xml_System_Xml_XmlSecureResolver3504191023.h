﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XmlResolver626023767.h"

// System.Xml.XmlResolver
struct XmlResolver_t626023767;
// System.Security.PermissionSet
struct PermissionSet_t223948603;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlSecureResolver
struct  XmlSecureResolver_t3504191023  : public XmlResolver_t626023767
{
public:
	// System.Xml.XmlResolver System.Xml.XmlSecureResolver::resolver
	XmlResolver_t626023767 * ___resolver_0;
	// System.Security.PermissionSet System.Xml.XmlSecureResolver::permissionSet
	PermissionSet_t223948603 * ___permissionSet_1;

public:
	inline static int32_t get_offset_of_resolver_0() { return static_cast<int32_t>(offsetof(XmlSecureResolver_t3504191023, ___resolver_0)); }
	inline XmlResolver_t626023767 * get_resolver_0() const { return ___resolver_0; }
	inline XmlResolver_t626023767 ** get_address_of_resolver_0() { return &___resolver_0; }
	inline void set_resolver_0(XmlResolver_t626023767 * value)
	{
		___resolver_0 = value;
		Il2CppCodeGenWriteBarrier(&___resolver_0, value);
	}

	inline static int32_t get_offset_of_permissionSet_1() { return static_cast<int32_t>(offsetof(XmlSecureResolver_t3504191023, ___permissionSet_1)); }
	inline PermissionSet_t223948603 * get_permissionSet_1() const { return ___permissionSet_1; }
	inline PermissionSet_t223948603 ** get_address_of_permissionSet_1() { return &___permissionSet_1; }
	inline void set_permissionSet_1(PermissionSet_t223948603 * value)
	{
		___permissionSet_1 = value;
		Il2CppCodeGenWriteBarrier(&___permissionSet_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
