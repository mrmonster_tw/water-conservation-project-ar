﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.String
struct String_t;
// System.Xml.XmlElement
struct XmlElement_t561603118;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.Xml.SignedInfo
struct  SignedInfo_t1687583442  : public Il2CppObject
{
public:
	// System.Collections.ArrayList System.Security.Cryptography.Xml.SignedInfo::references
	ArrayList_t2718874744 * ___references_0;
	// System.String System.Security.Cryptography.Xml.SignedInfo::c14nMethod
	String_t* ___c14nMethod_1;
	// System.String System.Security.Cryptography.Xml.SignedInfo::id
	String_t* ___id_2;
	// System.String System.Security.Cryptography.Xml.SignedInfo::signatureMethod
	String_t* ___signatureMethod_3;
	// System.String System.Security.Cryptography.Xml.SignedInfo::signatureLength
	String_t* ___signatureLength_4;
	// System.Xml.XmlElement System.Security.Cryptography.Xml.SignedInfo::element
	XmlElement_t561603118 * ___element_5;

public:
	inline static int32_t get_offset_of_references_0() { return static_cast<int32_t>(offsetof(SignedInfo_t1687583442, ___references_0)); }
	inline ArrayList_t2718874744 * get_references_0() const { return ___references_0; }
	inline ArrayList_t2718874744 ** get_address_of_references_0() { return &___references_0; }
	inline void set_references_0(ArrayList_t2718874744 * value)
	{
		___references_0 = value;
		Il2CppCodeGenWriteBarrier(&___references_0, value);
	}

	inline static int32_t get_offset_of_c14nMethod_1() { return static_cast<int32_t>(offsetof(SignedInfo_t1687583442, ___c14nMethod_1)); }
	inline String_t* get_c14nMethod_1() const { return ___c14nMethod_1; }
	inline String_t** get_address_of_c14nMethod_1() { return &___c14nMethod_1; }
	inline void set_c14nMethod_1(String_t* value)
	{
		___c14nMethod_1 = value;
		Il2CppCodeGenWriteBarrier(&___c14nMethod_1, value);
	}

	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(SignedInfo_t1687583442, ___id_2)); }
	inline String_t* get_id_2() const { return ___id_2; }
	inline String_t** get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(String_t* value)
	{
		___id_2 = value;
		Il2CppCodeGenWriteBarrier(&___id_2, value);
	}

	inline static int32_t get_offset_of_signatureMethod_3() { return static_cast<int32_t>(offsetof(SignedInfo_t1687583442, ___signatureMethod_3)); }
	inline String_t* get_signatureMethod_3() const { return ___signatureMethod_3; }
	inline String_t** get_address_of_signatureMethod_3() { return &___signatureMethod_3; }
	inline void set_signatureMethod_3(String_t* value)
	{
		___signatureMethod_3 = value;
		Il2CppCodeGenWriteBarrier(&___signatureMethod_3, value);
	}

	inline static int32_t get_offset_of_signatureLength_4() { return static_cast<int32_t>(offsetof(SignedInfo_t1687583442, ___signatureLength_4)); }
	inline String_t* get_signatureLength_4() const { return ___signatureLength_4; }
	inline String_t** get_address_of_signatureLength_4() { return &___signatureLength_4; }
	inline void set_signatureLength_4(String_t* value)
	{
		___signatureLength_4 = value;
		Il2CppCodeGenWriteBarrier(&___signatureLength_4, value);
	}

	inline static int32_t get_offset_of_element_5() { return static_cast<int32_t>(offsetof(SignedInfo_t1687583442, ___element_5)); }
	inline XmlElement_t561603118 * get_element_5() const { return ___element_5; }
	inline XmlElement_t561603118 ** get_address_of_element_5() { return &___element_5; }
	inline void set_element_5(XmlElement_t561603118 * value)
	{
		___element_5 = value;
		Il2CppCodeGenWriteBarrier(&___element_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
