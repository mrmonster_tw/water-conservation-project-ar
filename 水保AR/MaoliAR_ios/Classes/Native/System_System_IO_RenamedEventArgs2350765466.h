﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_IO_FileSystemEventArgs1603777841.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.RenamedEventArgs
struct  RenamedEventArgs_t2350765466  : public FileSystemEventArgs_t1603777841
{
public:
	// System.String System.IO.RenamedEventArgs::oldName
	String_t* ___oldName_4;
	// System.String System.IO.RenamedEventArgs::oldFullPath
	String_t* ___oldFullPath_5;

public:
	inline static int32_t get_offset_of_oldName_4() { return static_cast<int32_t>(offsetof(RenamedEventArgs_t2350765466, ___oldName_4)); }
	inline String_t* get_oldName_4() const { return ___oldName_4; }
	inline String_t** get_address_of_oldName_4() { return &___oldName_4; }
	inline void set_oldName_4(String_t* value)
	{
		___oldName_4 = value;
		Il2CppCodeGenWriteBarrier(&___oldName_4, value);
	}

	inline static int32_t get_offset_of_oldFullPath_5() { return static_cast<int32_t>(offsetof(RenamedEventArgs_t2350765466, ___oldFullPath_5)); }
	inline String_t* get_oldFullPath_5() const { return ___oldFullPath_5; }
	inline String_t** get_address_of_oldFullPath_5() { return &___oldFullPath_5; }
	inline void set_oldFullPath_5(String_t* value)
	{
		___oldFullPath_5 = value;
		Il2CppCodeGenWriteBarrier(&___oldFullPath_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
