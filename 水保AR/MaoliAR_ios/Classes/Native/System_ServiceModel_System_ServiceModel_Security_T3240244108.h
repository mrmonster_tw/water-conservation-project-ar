﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Co518829156.h"

// System.ServiceModel.Channels.Binding
struct Binding_t859993683;
// System.ServiceModel.EndpointAddress
struct EndpointAddress_t3119842923;
// System.Uri
struct Uri_t100236324;
// System.Collections.Generic.KeyedByTypeCollection`1<System.ServiceModel.Description.IEndpointBehavior>
struct KeyedByTypeCollection_1_t4222441378;
// System.IdentityModel.Selectors.SecurityTokenSerializer
struct SecurityTokenSerializer_t972969391;
// System.ServiceModel.Security.SecurityAlgorithmSuite
struct SecurityAlgorithmSuite_t2980594242;
// System.ServiceModel.Channels.SecurityBindingElement
struct SecurityBindingElement_t1737011943;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.AuthenticatorCommunicationObject
struct  AuthenticatorCommunicationObject_t3240244108  : public CommunicationObject_t518829156
{
public:
	// System.ServiceModel.Channels.Binding System.ServiceModel.Security.Tokens.AuthenticatorCommunicationObject::issuer_binding
	Binding_t859993683 * ___issuer_binding_9;
	// System.ServiceModel.EndpointAddress System.ServiceModel.Security.Tokens.AuthenticatorCommunicationObject::issuer_address
	EndpointAddress_t3119842923 * ___issuer_address_10;
	// System.Uri System.ServiceModel.Security.Tokens.AuthenticatorCommunicationObject::listen_uri
	Uri_t100236324 * ___listen_uri_11;
	// System.Collections.Generic.KeyedByTypeCollection`1<System.ServiceModel.Description.IEndpointBehavior> System.ServiceModel.Security.Tokens.AuthenticatorCommunicationObject::behaviors
	KeyedByTypeCollection_1_t4222441378 * ___behaviors_12;
	// System.IdentityModel.Selectors.SecurityTokenSerializer System.ServiceModel.Security.Tokens.AuthenticatorCommunicationObject::serializer
	SecurityTokenSerializer_t972969391 * ___serializer_13;
	// System.ServiceModel.Security.SecurityAlgorithmSuite System.ServiceModel.Security.Tokens.AuthenticatorCommunicationObject::algorithm
	SecurityAlgorithmSuite_t2980594242 * ___algorithm_14;
	// System.ServiceModel.Channels.SecurityBindingElement System.ServiceModel.Security.Tokens.AuthenticatorCommunicationObject::element
	SecurityBindingElement_t1737011943 * ___element_15;

public:
	inline static int32_t get_offset_of_issuer_binding_9() { return static_cast<int32_t>(offsetof(AuthenticatorCommunicationObject_t3240244108, ___issuer_binding_9)); }
	inline Binding_t859993683 * get_issuer_binding_9() const { return ___issuer_binding_9; }
	inline Binding_t859993683 ** get_address_of_issuer_binding_9() { return &___issuer_binding_9; }
	inline void set_issuer_binding_9(Binding_t859993683 * value)
	{
		___issuer_binding_9 = value;
		Il2CppCodeGenWriteBarrier(&___issuer_binding_9, value);
	}

	inline static int32_t get_offset_of_issuer_address_10() { return static_cast<int32_t>(offsetof(AuthenticatorCommunicationObject_t3240244108, ___issuer_address_10)); }
	inline EndpointAddress_t3119842923 * get_issuer_address_10() const { return ___issuer_address_10; }
	inline EndpointAddress_t3119842923 ** get_address_of_issuer_address_10() { return &___issuer_address_10; }
	inline void set_issuer_address_10(EndpointAddress_t3119842923 * value)
	{
		___issuer_address_10 = value;
		Il2CppCodeGenWriteBarrier(&___issuer_address_10, value);
	}

	inline static int32_t get_offset_of_listen_uri_11() { return static_cast<int32_t>(offsetof(AuthenticatorCommunicationObject_t3240244108, ___listen_uri_11)); }
	inline Uri_t100236324 * get_listen_uri_11() const { return ___listen_uri_11; }
	inline Uri_t100236324 ** get_address_of_listen_uri_11() { return &___listen_uri_11; }
	inline void set_listen_uri_11(Uri_t100236324 * value)
	{
		___listen_uri_11 = value;
		Il2CppCodeGenWriteBarrier(&___listen_uri_11, value);
	}

	inline static int32_t get_offset_of_behaviors_12() { return static_cast<int32_t>(offsetof(AuthenticatorCommunicationObject_t3240244108, ___behaviors_12)); }
	inline KeyedByTypeCollection_1_t4222441378 * get_behaviors_12() const { return ___behaviors_12; }
	inline KeyedByTypeCollection_1_t4222441378 ** get_address_of_behaviors_12() { return &___behaviors_12; }
	inline void set_behaviors_12(KeyedByTypeCollection_1_t4222441378 * value)
	{
		___behaviors_12 = value;
		Il2CppCodeGenWriteBarrier(&___behaviors_12, value);
	}

	inline static int32_t get_offset_of_serializer_13() { return static_cast<int32_t>(offsetof(AuthenticatorCommunicationObject_t3240244108, ___serializer_13)); }
	inline SecurityTokenSerializer_t972969391 * get_serializer_13() const { return ___serializer_13; }
	inline SecurityTokenSerializer_t972969391 ** get_address_of_serializer_13() { return &___serializer_13; }
	inline void set_serializer_13(SecurityTokenSerializer_t972969391 * value)
	{
		___serializer_13 = value;
		Il2CppCodeGenWriteBarrier(&___serializer_13, value);
	}

	inline static int32_t get_offset_of_algorithm_14() { return static_cast<int32_t>(offsetof(AuthenticatorCommunicationObject_t3240244108, ___algorithm_14)); }
	inline SecurityAlgorithmSuite_t2980594242 * get_algorithm_14() const { return ___algorithm_14; }
	inline SecurityAlgorithmSuite_t2980594242 ** get_address_of_algorithm_14() { return &___algorithm_14; }
	inline void set_algorithm_14(SecurityAlgorithmSuite_t2980594242 * value)
	{
		___algorithm_14 = value;
		Il2CppCodeGenWriteBarrier(&___algorithm_14, value);
	}

	inline static int32_t get_offset_of_element_15() { return static_cast<int32_t>(offsetof(AuthenticatorCommunicationObject_t3240244108, ___element_15)); }
	inline SecurityBindingElement_t1737011943 * get_element_15() const { return ___element_15; }
	inline SecurityBindingElement_t1737011943 ** get_address_of_element_15() { return &___element_15; }
	inline void set_element_15(SecurityBindingElement_t1737011943 * value)
	{
		___element_15 = value;
		Il2CppCodeGenWriteBarrier(&___element_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
