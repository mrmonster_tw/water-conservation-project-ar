﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.TransactionProtocol
struct TransactionProtocol_t3972232485;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.TransactionProtocol
struct  TransactionProtocol_t3972232485  : public Il2CppObject
{
public:

public:
};

struct TransactionProtocol_t3972232485_StaticFields
{
public:
	// System.ServiceModel.TransactionProtocol System.ServiceModel.TransactionProtocol::wsat
	TransactionProtocol_t3972232485 * ___wsat_0;
	// System.ServiceModel.TransactionProtocol System.ServiceModel.TransactionProtocol::oletx
	TransactionProtocol_t3972232485 * ___oletx_1;

public:
	inline static int32_t get_offset_of_wsat_0() { return static_cast<int32_t>(offsetof(TransactionProtocol_t3972232485_StaticFields, ___wsat_0)); }
	inline TransactionProtocol_t3972232485 * get_wsat_0() const { return ___wsat_0; }
	inline TransactionProtocol_t3972232485 ** get_address_of_wsat_0() { return &___wsat_0; }
	inline void set_wsat_0(TransactionProtocol_t3972232485 * value)
	{
		___wsat_0 = value;
		Il2CppCodeGenWriteBarrier(&___wsat_0, value);
	}

	inline static int32_t get_offset_of_oletx_1() { return static_cast<int32_t>(offsetof(TransactionProtocol_t3972232485_StaticFields, ___oletx_1)); }
	inline TransactionProtocol_t3972232485 * get_oletx_1() const { return ___oletx_1; }
	inline TransactionProtocol_t3972232485 ** get_address_of_oletx_1() { return &___oletx_1; }
	inline void set_oletx_1(TransactionProtocol_t3972232485 * value)
	{
		___oletx_1 = value;
		Il2CppCodeGenWriteBarrier(&___oletx_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
