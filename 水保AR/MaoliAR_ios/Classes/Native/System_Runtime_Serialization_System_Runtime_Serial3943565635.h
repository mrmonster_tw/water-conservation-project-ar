﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Runtime_Serialization_System_Runtime_Serial4088073813.h"

// System.Collections.Generic.List`1<System.Runtime.Serialization.EnumMemberInfo>
struct List_1_t249403600;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.EnumMap
struct  EnumMap_t3943565635  : public SerializationMap_t4088073813
{
public:
	// System.Collections.Generic.List`1<System.Runtime.Serialization.EnumMemberInfo> System.Runtime.Serialization.EnumMap::enum_members
	List_1_t249403600 * ___enum_members_6;
	// System.Boolean System.Runtime.Serialization.EnumMap::flag_attr
	bool ___flag_attr_7;

public:
	inline static int32_t get_offset_of_enum_members_6() { return static_cast<int32_t>(offsetof(EnumMap_t3943565635, ___enum_members_6)); }
	inline List_1_t249403600 * get_enum_members_6() const { return ___enum_members_6; }
	inline List_1_t249403600 ** get_address_of_enum_members_6() { return &___enum_members_6; }
	inline void set_enum_members_6(List_1_t249403600 * value)
	{
		___enum_members_6 = value;
		Il2CppCodeGenWriteBarrier(&___enum_members_6, value);
	}

	inline static int32_t get_offset_of_flag_attr_7() { return static_cast<int32_t>(offsetof(EnumMap_t3943565635, ___flag_attr_7)); }
	inline bool get_flag_attr_7() const { return ___flag_attr_7; }
	inline bool* get_address_of_flag_attr_7() { return &___flag_attr_7; }
	inline void set_flag_attr_7(bool value)
	{
		___flag_attr_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
