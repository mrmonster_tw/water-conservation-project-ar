﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Configurat2544877501.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.NamedServiceModelExtensionCollectionElement`1<System.Object>
struct  NamedServiceModelExtensionCollectionElement_1_t274909112  : public ServiceModelExtensionCollectionElement_1_t2544877501
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.NamedServiceModelExtensionCollectionElement`1::_properties
	ConfigurationPropertyCollection_t2852175726 * ____properties_16;

public:
	inline static int32_t get_offset_of__properties_16() { return static_cast<int32_t>(offsetof(NamedServiceModelExtensionCollectionElement_1_t274909112, ____properties_16)); }
	inline ConfigurationPropertyCollection_t2852175726 * get__properties_16() const { return ____properties_16; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of__properties_16() { return &____properties_16; }
	inline void set__properties_16(ConfigurationPropertyCollection_t2852175726 * value)
	{
		____properties_16 = value;
		Il2CppCodeGenWriteBarrier(&____properties_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
