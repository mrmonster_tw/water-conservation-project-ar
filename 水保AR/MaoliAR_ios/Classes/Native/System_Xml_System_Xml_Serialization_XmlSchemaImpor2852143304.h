﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Serialization_SchemaImporter2185830201.h"
#include "System_Xml_System_Xml_Serialization_CodeGeneration2228734478.h"

// System.Xml.Serialization.XmlSchemas
struct XmlSchemas_t3283371924;
// System.Xml.Serialization.CodeIdentifiers
struct CodeIdentifiers_t4095039290;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Collections.Queue
struct Queue_t3637523393;
// System.Xml.Serialization.XmlReflectionImporter
struct XmlReflectionImporter_t1777580912;
// System.Xml.Serialization.SoapReflectionImporter
struct SoapReflectionImporter_t749884629;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2760654312;
// System.Xml.Schema.XmlSchemaElement
struct XmlSchemaElement_t427880856;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSchemaImporter
struct  XmlSchemaImporter_t2852143304  : public SchemaImporter_t2185830201
{
public:
	// System.Xml.Serialization.XmlSchemas System.Xml.Serialization.XmlSchemaImporter::schemas
	XmlSchemas_t3283371924 * ___schemas_0;
	// System.Xml.Serialization.CodeIdentifiers System.Xml.Serialization.XmlSchemaImporter::typeIdentifiers
	CodeIdentifiers_t4095039290 * ___typeIdentifiers_1;
	// System.Xml.Serialization.CodeIdentifiers System.Xml.Serialization.XmlSchemaImporter::elemIdentifiers
	CodeIdentifiers_t4095039290 * ___elemIdentifiers_2;
	// System.Collections.Hashtable System.Xml.Serialization.XmlSchemaImporter::mappedTypes
	Hashtable_t1853889766 * ___mappedTypes_3;
	// System.Collections.Hashtable System.Xml.Serialization.XmlSchemaImporter::primitiveDerivedMappedTypes
	Hashtable_t1853889766 * ___primitiveDerivedMappedTypes_4;
	// System.Collections.Hashtable System.Xml.Serialization.XmlSchemaImporter::dataMappedTypes
	Hashtable_t1853889766 * ___dataMappedTypes_5;
	// System.Collections.Queue System.Xml.Serialization.XmlSchemaImporter::pendingMaps
	Queue_t3637523393 * ___pendingMaps_6;
	// System.Collections.Hashtable System.Xml.Serialization.XmlSchemaImporter::sharedAnonymousTypes
	Hashtable_t1853889766 * ___sharedAnonymousTypes_7;
	// System.Boolean System.Xml.Serialization.XmlSchemaImporter::encodedFormat
	bool ___encodedFormat_8;
	// System.Xml.Serialization.XmlReflectionImporter System.Xml.Serialization.XmlSchemaImporter::auxXmlRefImporter
	XmlReflectionImporter_t1777580912 * ___auxXmlRefImporter_9;
	// System.Xml.Serialization.SoapReflectionImporter System.Xml.Serialization.XmlSchemaImporter::auxSoapRefImporter
	SoapReflectionImporter_t749884629 * ___auxSoapRefImporter_10;
	// System.Boolean System.Xml.Serialization.XmlSchemaImporter::anyTypeImported
	bool ___anyTypeImported_11;
	// System.Xml.Serialization.CodeGenerationOptions System.Xml.Serialization.XmlSchemaImporter::options
	int32_t ___options_12;
	// System.Xml.Schema.XmlSchemaElement System.Xml.Serialization.XmlSchemaImporter::anyElement
	XmlSchemaElement_t427880856 * ___anyElement_16;

public:
	inline static int32_t get_offset_of_schemas_0() { return static_cast<int32_t>(offsetof(XmlSchemaImporter_t2852143304, ___schemas_0)); }
	inline XmlSchemas_t3283371924 * get_schemas_0() const { return ___schemas_0; }
	inline XmlSchemas_t3283371924 ** get_address_of_schemas_0() { return &___schemas_0; }
	inline void set_schemas_0(XmlSchemas_t3283371924 * value)
	{
		___schemas_0 = value;
		Il2CppCodeGenWriteBarrier(&___schemas_0, value);
	}

	inline static int32_t get_offset_of_typeIdentifiers_1() { return static_cast<int32_t>(offsetof(XmlSchemaImporter_t2852143304, ___typeIdentifiers_1)); }
	inline CodeIdentifiers_t4095039290 * get_typeIdentifiers_1() const { return ___typeIdentifiers_1; }
	inline CodeIdentifiers_t4095039290 ** get_address_of_typeIdentifiers_1() { return &___typeIdentifiers_1; }
	inline void set_typeIdentifiers_1(CodeIdentifiers_t4095039290 * value)
	{
		___typeIdentifiers_1 = value;
		Il2CppCodeGenWriteBarrier(&___typeIdentifiers_1, value);
	}

	inline static int32_t get_offset_of_elemIdentifiers_2() { return static_cast<int32_t>(offsetof(XmlSchemaImporter_t2852143304, ___elemIdentifiers_2)); }
	inline CodeIdentifiers_t4095039290 * get_elemIdentifiers_2() const { return ___elemIdentifiers_2; }
	inline CodeIdentifiers_t4095039290 ** get_address_of_elemIdentifiers_2() { return &___elemIdentifiers_2; }
	inline void set_elemIdentifiers_2(CodeIdentifiers_t4095039290 * value)
	{
		___elemIdentifiers_2 = value;
		Il2CppCodeGenWriteBarrier(&___elemIdentifiers_2, value);
	}

	inline static int32_t get_offset_of_mappedTypes_3() { return static_cast<int32_t>(offsetof(XmlSchemaImporter_t2852143304, ___mappedTypes_3)); }
	inline Hashtable_t1853889766 * get_mappedTypes_3() const { return ___mappedTypes_3; }
	inline Hashtable_t1853889766 ** get_address_of_mappedTypes_3() { return &___mappedTypes_3; }
	inline void set_mappedTypes_3(Hashtable_t1853889766 * value)
	{
		___mappedTypes_3 = value;
		Il2CppCodeGenWriteBarrier(&___mappedTypes_3, value);
	}

	inline static int32_t get_offset_of_primitiveDerivedMappedTypes_4() { return static_cast<int32_t>(offsetof(XmlSchemaImporter_t2852143304, ___primitiveDerivedMappedTypes_4)); }
	inline Hashtable_t1853889766 * get_primitiveDerivedMappedTypes_4() const { return ___primitiveDerivedMappedTypes_4; }
	inline Hashtable_t1853889766 ** get_address_of_primitiveDerivedMappedTypes_4() { return &___primitiveDerivedMappedTypes_4; }
	inline void set_primitiveDerivedMappedTypes_4(Hashtable_t1853889766 * value)
	{
		___primitiveDerivedMappedTypes_4 = value;
		Il2CppCodeGenWriteBarrier(&___primitiveDerivedMappedTypes_4, value);
	}

	inline static int32_t get_offset_of_dataMappedTypes_5() { return static_cast<int32_t>(offsetof(XmlSchemaImporter_t2852143304, ___dataMappedTypes_5)); }
	inline Hashtable_t1853889766 * get_dataMappedTypes_5() const { return ___dataMappedTypes_5; }
	inline Hashtable_t1853889766 ** get_address_of_dataMappedTypes_5() { return &___dataMappedTypes_5; }
	inline void set_dataMappedTypes_5(Hashtable_t1853889766 * value)
	{
		___dataMappedTypes_5 = value;
		Il2CppCodeGenWriteBarrier(&___dataMappedTypes_5, value);
	}

	inline static int32_t get_offset_of_pendingMaps_6() { return static_cast<int32_t>(offsetof(XmlSchemaImporter_t2852143304, ___pendingMaps_6)); }
	inline Queue_t3637523393 * get_pendingMaps_6() const { return ___pendingMaps_6; }
	inline Queue_t3637523393 ** get_address_of_pendingMaps_6() { return &___pendingMaps_6; }
	inline void set_pendingMaps_6(Queue_t3637523393 * value)
	{
		___pendingMaps_6 = value;
		Il2CppCodeGenWriteBarrier(&___pendingMaps_6, value);
	}

	inline static int32_t get_offset_of_sharedAnonymousTypes_7() { return static_cast<int32_t>(offsetof(XmlSchemaImporter_t2852143304, ___sharedAnonymousTypes_7)); }
	inline Hashtable_t1853889766 * get_sharedAnonymousTypes_7() const { return ___sharedAnonymousTypes_7; }
	inline Hashtable_t1853889766 ** get_address_of_sharedAnonymousTypes_7() { return &___sharedAnonymousTypes_7; }
	inline void set_sharedAnonymousTypes_7(Hashtable_t1853889766 * value)
	{
		___sharedAnonymousTypes_7 = value;
		Il2CppCodeGenWriteBarrier(&___sharedAnonymousTypes_7, value);
	}

	inline static int32_t get_offset_of_encodedFormat_8() { return static_cast<int32_t>(offsetof(XmlSchemaImporter_t2852143304, ___encodedFormat_8)); }
	inline bool get_encodedFormat_8() const { return ___encodedFormat_8; }
	inline bool* get_address_of_encodedFormat_8() { return &___encodedFormat_8; }
	inline void set_encodedFormat_8(bool value)
	{
		___encodedFormat_8 = value;
	}

	inline static int32_t get_offset_of_auxXmlRefImporter_9() { return static_cast<int32_t>(offsetof(XmlSchemaImporter_t2852143304, ___auxXmlRefImporter_9)); }
	inline XmlReflectionImporter_t1777580912 * get_auxXmlRefImporter_9() const { return ___auxXmlRefImporter_9; }
	inline XmlReflectionImporter_t1777580912 ** get_address_of_auxXmlRefImporter_9() { return &___auxXmlRefImporter_9; }
	inline void set_auxXmlRefImporter_9(XmlReflectionImporter_t1777580912 * value)
	{
		___auxXmlRefImporter_9 = value;
		Il2CppCodeGenWriteBarrier(&___auxXmlRefImporter_9, value);
	}

	inline static int32_t get_offset_of_auxSoapRefImporter_10() { return static_cast<int32_t>(offsetof(XmlSchemaImporter_t2852143304, ___auxSoapRefImporter_10)); }
	inline SoapReflectionImporter_t749884629 * get_auxSoapRefImporter_10() const { return ___auxSoapRefImporter_10; }
	inline SoapReflectionImporter_t749884629 ** get_address_of_auxSoapRefImporter_10() { return &___auxSoapRefImporter_10; }
	inline void set_auxSoapRefImporter_10(SoapReflectionImporter_t749884629 * value)
	{
		___auxSoapRefImporter_10 = value;
		Il2CppCodeGenWriteBarrier(&___auxSoapRefImporter_10, value);
	}

	inline static int32_t get_offset_of_anyTypeImported_11() { return static_cast<int32_t>(offsetof(XmlSchemaImporter_t2852143304, ___anyTypeImported_11)); }
	inline bool get_anyTypeImported_11() const { return ___anyTypeImported_11; }
	inline bool* get_address_of_anyTypeImported_11() { return &___anyTypeImported_11; }
	inline void set_anyTypeImported_11(bool value)
	{
		___anyTypeImported_11 = value;
	}

	inline static int32_t get_offset_of_options_12() { return static_cast<int32_t>(offsetof(XmlSchemaImporter_t2852143304, ___options_12)); }
	inline int32_t get_options_12() const { return ___options_12; }
	inline int32_t* get_address_of_options_12() { return &___options_12; }
	inline void set_options_12(int32_t value)
	{
		___options_12 = value;
	}

	inline static int32_t get_offset_of_anyElement_16() { return static_cast<int32_t>(offsetof(XmlSchemaImporter_t2852143304, ___anyElement_16)); }
	inline XmlSchemaElement_t427880856 * get_anyElement_16() const { return ___anyElement_16; }
	inline XmlSchemaElement_t427880856 ** get_address_of_anyElement_16() { return &___anyElement_16; }
	inline void set_anyElement_16(XmlSchemaElement_t427880856 * value)
	{
		___anyElement_16 = value;
		Il2CppCodeGenWriteBarrier(&___anyElement_16, value);
	}
};

struct XmlSchemaImporter_t2852143304_StaticFields
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Serialization.XmlSchemaImporter::anyType
	XmlQualifiedName_t2760654312 * ___anyType_13;
	// System.Xml.XmlQualifiedName System.Xml.Serialization.XmlSchemaImporter::arrayType
	XmlQualifiedName_t2760654312 * ___arrayType_14;
	// System.Xml.XmlQualifiedName System.Xml.Serialization.XmlSchemaImporter::arrayTypeRefName
	XmlQualifiedName_t2760654312 * ___arrayTypeRefName_15;

public:
	inline static int32_t get_offset_of_anyType_13() { return static_cast<int32_t>(offsetof(XmlSchemaImporter_t2852143304_StaticFields, ___anyType_13)); }
	inline XmlQualifiedName_t2760654312 * get_anyType_13() const { return ___anyType_13; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_anyType_13() { return &___anyType_13; }
	inline void set_anyType_13(XmlQualifiedName_t2760654312 * value)
	{
		___anyType_13 = value;
		Il2CppCodeGenWriteBarrier(&___anyType_13, value);
	}

	inline static int32_t get_offset_of_arrayType_14() { return static_cast<int32_t>(offsetof(XmlSchemaImporter_t2852143304_StaticFields, ___arrayType_14)); }
	inline XmlQualifiedName_t2760654312 * get_arrayType_14() const { return ___arrayType_14; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_arrayType_14() { return &___arrayType_14; }
	inline void set_arrayType_14(XmlQualifiedName_t2760654312 * value)
	{
		___arrayType_14 = value;
		Il2CppCodeGenWriteBarrier(&___arrayType_14, value);
	}

	inline static int32_t get_offset_of_arrayTypeRefName_15() { return static_cast<int32_t>(offsetof(XmlSchemaImporter_t2852143304_StaticFields, ___arrayTypeRefName_15)); }
	inline XmlQualifiedName_t2760654312 * get_arrayTypeRefName_15() const { return ___arrayTypeRefName_15; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_arrayTypeRefName_15() { return &___arrayTypeRefName_15; }
	inline void set_arrayTypeRefName_15(XmlQualifiedName_t2760654312 * value)
	{
		___arrayTypeRefName_15 = value;
		Il2CppCodeGenWriteBarrier(&___arrayTypeRefName_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
