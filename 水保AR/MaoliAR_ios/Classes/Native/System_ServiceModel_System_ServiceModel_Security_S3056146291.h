﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Security_T3159509539.h"

// System.ServiceModel.ClientCredentialsSecurityTokenManager
struct ClientCredentialsSecurityTokenManager_t1905807029;
// System.ServiceModel.Security.SpnegoCommunicationObject
struct SpnegoCommunicationObject_t1015217490;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.SpnegoSecurityTokenProvider
struct  SpnegoSecurityTokenProvider_t3056146291  : public CommunicationSecurityTokenProvider_t3159509539
{
public:
	// System.ServiceModel.ClientCredentialsSecurityTokenManager System.ServiceModel.Security.SpnegoSecurityTokenProvider::manager
	ClientCredentialsSecurityTokenManager_t1905807029 * ___manager_0;
	// System.ServiceModel.Security.SpnegoCommunicationObject System.ServiceModel.Security.SpnegoSecurityTokenProvider::comm
	SpnegoCommunicationObject_t1015217490 * ___comm_1;

public:
	inline static int32_t get_offset_of_manager_0() { return static_cast<int32_t>(offsetof(SpnegoSecurityTokenProvider_t3056146291, ___manager_0)); }
	inline ClientCredentialsSecurityTokenManager_t1905807029 * get_manager_0() const { return ___manager_0; }
	inline ClientCredentialsSecurityTokenManager_t1905807029 ** get_address_of_manager_0() { return &___manager_0; }
	inline void set_manager_0(ClientCredentialsSecurityTokenManager_t1905807029 * value)
	{
		___manager_0 = value;
		Il2CppCodeGenWriteBarrier(&___manager_0, value);
	}

	inline static int32_t get_offset_of_comm_1() { return static_cast<int32_t>(offsetof(SpnegoSecurityTokenProvider_t3056146291, ___comm_1)); }
	inline SpnegoCommunicationObject_t1015217490 * get_comm_1() const { return ___comm_1; }
	inline SpnegoCommunicationObject_t1015217490 ** get_address_of_comm_1() { return &___comm_1; }
	inline void set_comm_1(SpnegoCommunicationObject_t1015217490 * value)
	{
		___comm_1 = value;
		Il2CppCodeGenWriteBarrier(&___comm_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
