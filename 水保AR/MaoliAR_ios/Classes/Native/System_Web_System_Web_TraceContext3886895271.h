﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Web_System_Web_TraceMode4125462420.h"

// System.Object
struct Il2CppObject;
// System.Web.HttpContext
struct HttpContext_t1969259010;
// System.Web.TraceManager
struct TraceManager_t157182125;
// System.Web.TraceData
struct TraceData_t4177544667;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.ComponentModel.EventHandlerList
struct EventHandlerList_t1108123056;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.TraceContext
struct  TraceContext_t3886895271  : public Il2CppObject
{
public:
	// System.Web.HttpContext System.Web.TraceContext::_Context
	HttpContext_t1969259010 * ____Context_1;
	// System.Web.TraceManager System.Web.TraceContext::_traceManager
	TraceManager_t157182125 * ____traceManager_2;
	// System.Boolean System.Web.TraceContext::_Enabled
	bool ____Enabled_3;
	// System.Web.TraceMode System.Web.TraceContext::_Mode
	int32_t ____Mode_4;
	// System.Web.TraceData System.Web.TraceContext::data
	TraceData_t4177544667 * ___data_5;
	// System.Boolean System.Web.TraceContext::data_saved
	bool ___data_saved_6;
	// System.Boolean System.Web.TraceContext::_haveTrace
	bool ____haveTrace_7;
	// System.Collections.Hashtable System.Web.TraceContext::view_states
	Hashtable_t1853889766 * ___view_states_8;
	// System.Collections.Hashtable System.Web.TraceContext::control_states
	Hashtable_t1853889766 * ___control_states_9;
	// System.Collections.Hashtable System.Web.TraceContext::sizes
	Hashtable_t1853889766 * ___sizes_10;
	// System.ComponentModel.EventHandlerList System.Web.TraceContext::events
	EventHandlerList_t1108123056 * ___events_11;

public:
	inline static int32_t get_offset_of__Context_1() { return static_cast<int32_t>(offsetof(TraceContext_t3886895271, ____Context_1)); }
	inline HttpContext_t1969259010 * get__Context_1() const { return ____Context_1; }
	inline HttpContext_t1969259010 ** get_address_of__Context_1() { return &____Context_1; }
	inline void set__Context_1(HttpContext_t1969259010 * value)
	{
		____Context_1 = value;
		Il2CppCodeGenWriteBarrier(&____Context_1, value);
	}

	inline static int32_t get_offset_of__traceManager_2() { return static_cast<int32_t>(offsetof(TraceContext_t3886895271, ____traceManager_2)); }
	inline TraceManager_t157182125 * get__traceManager_2() const { return ____traceManager_2; }
	inline TraceManager_t157182125 ** get_address_of__traceManager_2() { return &____traceManager_2; }
	inline void set__traceManager_2(TraceManager_t157182125 * value)
	{
		____traceManager_2 = value;
		Il2CppCodeGenWriteBarrier(&____traceManager_2, value);
	}

	inline static int32_t get_offset_of__Enabled_3() { return static_cast<int32_t>(offsetof(TraceContext_t3886895271, ____Enabled_3)); }
	inline bool get__Enabled_3() const { return ____Enabled_3; }
	inline bool* get_address_of__Enabled_3() { return &____Enabled_3; }
	inline void set__Enabled_3(bool value)
	{
		____Enabled_3 = value;
	}

	inline static int32_t get_offset_of__Mode_4() { return static_cast<int32_t>(offsetof(TraceContext_t3886895271, ____Mode_4)); }
	inline int32_t get__Mode_4() const { return ____Mode_4; }
	inline int32_t* get_address_of__Mode_4() { return &____Mode_4; }
	inline void set__Mode_4(int32_t value)
	{
		____Mode_4 = value;
	}

	inline static int32_t get_offset_of_data_5() { return static_cast<int32_t>(offsetof(TraceContext_t3886895271, ___data_5)); }
	inline TraceData_t4177544667 * get_data_5() const { return ___data_5; }
	inline TraceData_t4177544667 ** get_address_of_data_5() { return &___data_5; }
	inline void set_data_5(TraceData_t4177544667 * value)
	{
		___data_5 = value;
		Il2CppCodeGenWriteBarrier(&___data_5, value);
	}

	inline static int32_t get_offset_of_data_saved_6() { return static_cast<int32_t>(offsetof(TraceContext_t3886895271, ___data_saved_6)); }
	inline bool get_data_saved_6() const { return ___data_saved_6; }
	inline bool* get_address_of_data_saved_6() { return &___data_saved_6; }
	inline void set_data_saved_6(bool value)
	{
		___data_saved_6 = value;
	}

	inline static int32_t get_offset_of__haveTrace_7() { return static_cast<int32_t>(offsetof(TraceContext_t3886895271, ____haveTrace_7)); }
	inline bool get__haveTrace_7() const { return ____haveTrace_7; }
	inline bool* get_address_of__haveTrace_7() { return &____haveTrace_7; }
	inline void set__haveTrace_7(bool value)
	{
		____haveTrace_7 = value;
	}

	inline static int32_t get_offset_of_view_states_8() { return static_cast<int32_t>(offsetof(TraceContext_t3886895271, ___view_states_8)); }
	inline Hashtable_t1853889766 * get_view_states_8() const { return ___view_states_8; }
	inline Hashtable_t1853889766 ** get_address_of_view_states_8() { return &___view_states_8; }
	inline void set_view_states_8(Hashtable_t1853889766 * value)
	{
		___view_states_8 = value;
		Il2CppCodeGenWriteBarrier(&___view_states_8, value);
	}

	inline static int32_t get_offset_of_control_states_9() { return static_cast<int32_t>(offsetof(TraceContext_t3886895271, ___control_states_9)); }
	inline Hashtable_t1853889766 * get_control_states_9() const { return ___control_states_9; }
	inline Hashtable_t1853889766 ** get_address_of_control_states_9() { return &___control_states_9; }
	inline void set_control_states_9(Hashtable_t1853889766 * value)
	{
		___control_states_9 = value;
		Il2CppCodeGenWriteBarrier(&___control_states_9, value);
	}

	inline static int32_t get_offset_of_sizes_10() { return static_cast<int32_t>(offsetof(TraceContext_t3886895271, ___sizes_10)); }
	inline Hashtable_t1853889766 * get_sizes_10() const { return ___sizes_10; }
	inline Hashtable_t1853889766 ** get_address_of_sizes_10() { return &___sizes_10; }
	inline void set_sizes_10(Hashtable_t1853889766 * value)
	{
		___sizes_10 = value;
		Il2CppCodeGenWriteBarrier(&___sizes_10, value);
	}

	inline static int32_t get_offset_of_events_11() { return static_cast<int32_t>(offsetof(TraceContext_t3886895271, ___events_11)); }
	inline EventHandlerList_t1108123056 * get_events_11() const { return ___events_11; }
	inline EventHandlerList_t1108123056 ** get_address_of_events_11() { return &___events_11; }
	inline void set_events_11(EventHandlerList_t1108123056 * value)
	{
		___events_11 = value;
		Il2CppCodeGenWriteBarrier(&___events_11, value);
	}
};

struct TraceContext_t3886895271_StaticFields
{
public:
	// System.Object System.Web.TraceContext::traceFinishedEvent
	Il2CppObject * ___traceFinishedEvent_0;

public:
	inline static int32_t get_offset_of_traceFinishedEvent_0() { return static_cast<int32_t>(offsetof(TraceContext_t3886895271_StaticFields, ___traceFinishedEvent_0)); }
	inline Il2CppObject * get_traceFinishedEvent_0() const { return ___traceFinishedEvent_0; }
	inline Il2CppObject ** get_address_of_traceFinishedEvent_0() { return &___traceFinishedEvent_0; }
	inline void set_traceFinishedEvent_0(Il2CppObject * value)
	{
		___traceFinishedEvent_0 = value;
		Il2CppCodeGenWriteBarrier(&___traceFinishedEvent_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
