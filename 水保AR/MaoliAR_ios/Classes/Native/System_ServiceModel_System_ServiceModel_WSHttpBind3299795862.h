﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_WSHttpBind2163194083.h"

// System.ServiceModel.WSHttpSecurity
struct WSHttpSecurity_t1511783750;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.WSHttpBinding
struct  WSHttpBinding_t3299795862  : public WSHttpBindingBase_t2163194083
{
public:
	// System.ServiceModel.WSHttpSecurity System.ServiceModel.WSHttpBinding::security
	WSHttpSecurity_t1511783750 * ___security_9;

public:
	inline static int32_t get_offset_of_security_9() { return static_cast<int32_t>(offsetof(WSHttpBinding_t3299795862, ___security_9)); }
	inline WSHttpSecurity_t1511783750 * get_security_9() const { return ___security_9; }
	inline WSHttpSecurity_t1511783750 ** get_address_of_security_9() { return &___security_9; }
	inline void set_security_9(WSHttpSecurity_t1511783750 * value)
	{
		___security_9 = value;
		Il2CppCodeGenWriteBarrier(&___security_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
