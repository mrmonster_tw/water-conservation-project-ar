﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.HttpDigestClientElement
struct  HttpDigestClientElement_t1785924423  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct HttpDigestClientElement_t1785924423_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.HttpDigestClientElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.HttpDigestClientElement::impersonation_level
	ConfigurationProperty_t3590861854 * ___impersonation_level_14;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(HttpDigestClientElement_t1785924423_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier(&___properties_13, value);
	}

	inline static int32_t get_offset_of_impersonation_level_14() { return static_cast<int32_t>(offsetof(HttpDigestClientElement_t1785924423_StaticFields, ___impersonation_level_14)); }
	inline ConfigurationProperty_t3590861854 * get_impersonation_level_14() const { return ___impersonation_level_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_impersonation_level_14() { return &___impersonation_level_14; }
	inline void set_impersonation_level_14(ConfigurationProperty_t3590861854 * value)
	{
		___impersonation_level_14 = value;
		Il2CppCodeGenWriteBarrier(&___impersonation_level_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
