﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// Mono.Xml.Xsl.XslTemplate
struct XslTemplate_t152263049;
// Mono.Xml.XPath.Pattern
struct Pattern_t1136864796;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.XslModedTemplateTable/TemplateWithPriority
struct  TemplateWithPriority_t796810083  : public Il2CppObject
{
public:
	// System.Double Mono.Xml.Xsl.XslModedTemplateTable/TemplateWithPriority::Priority
	double ___Priority_0;
	// Mono.Xml.Xsl.XslTemplate Mono.Xml.Xsl.XslModedTemplateTable/TemplateWithPriority::Template
	XslTemplate_t152263049 * ___Template_1;
	// Mono.Xml.XPath.Pattern Mono.Xml.Xsl.XslModedTemplateTable/TemplateWithPriority::Pattern
	Pattern_t1136864796 * ___Pattern_2;
	// System.Int32 Mono.Xml.Xsl.XslModedTemplateTable/TemplateWithPriority::TemplateID
	int32_t ___TemplateID_3;

public:
	inline static int32_t get_offset_of_Priority_0() { return static_cast<int32_t>(offsetof(TemplateWithPriority_t796810083, ___Priority_0)); }
	inline double get_Priority_0() const { return ___Priority_0; }
	inline double* get_address_of_Priority_0() { return &___Priority_0; }
	inline void set_Priority_0(double value)
	{
		___Priority_0 = value;
	}

	inline static int32_t get_offset_of_Template_1() { return static_cast<int32_t>(offsetof(TemplateWithPriority_t796810083, ___Template_1)); }
	inline XslTemplate_t152263049 * get_Template_1() const { return ___Template_1; }
	inline XslTemplate_t152263049 ** get_address_of_Template_1() { return &___Template_1; }
	inline void set_Template_1(XslTemplate_t152263049 * value)
	{
		___Template_1 = value;
		Il2CppCodeGenWriteBarrier(&___Template_1, value);
	}

	inline static int32_t get_offset_of_Pattern_2() { return static_cast<int32_t>(offsetof(TemplateWithPriority_t796810083, ___Pattern_2)); }
	inline Pattern_t1136864796 * get_Pattern_2() const { return ___Pattern_2; }
	inline Pattern_t1136864796 ** get_address_of_Pattern_2() { return &___Pattern_2; }
	inline void set_Pattern_2(Pattern_t1136864796 * value)
	{
		___Pattern_2 = value;
		Il2CppCodeGenWriteBarrier(&___Pattern_2, value);
	}

	inline static int32_t get_offset_of_TemplateID_3() { return static_cast<int32_t>(offsetof(TemplateWithPriority_t796810083, ___TemplateID_3)); }
	inline int32_t get_TemplateID_3() const { return ___TemplateID_3; }
	inline int32_t* get_address_of_TemplateID_3() { return &___TemplateID_3; }
	inline void set_TemplateID_3(int32_t value)
	{
		___TemplateID_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
