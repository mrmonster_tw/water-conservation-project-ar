﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Outputter889105973.h"
#include "System_Xml_System_Xml_WriteState3983380671.h"

// System.Collections.Hashtable
struct Hashtable_t1853889766;
// Mono.Xml.Xsl.XslOutput
struct XslOutput_t472631936;
// Mono.Xml.Xsl.Emitter
struct Emitter_t942443340;
// System.IO.TextWriter
struct TextWriter_t3478189236;
// System.Text.StringBuilder
struct StringBuilder_t1712802186;
// Mono.Xml.Xsl.Attribute[]
struct AttributeU5BU5D_t4194715064;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t418790500;
// System.Collections.Specialized.ListDictionary
struct ListDictionary_t1624492310;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Xml.NameTable
struct NameTable_t3178203267;
// System.Text.Encoding
struct Encoding_t1523322056;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.GenericOutputter
struct  GenericOutputter_t1694878872  : public Outputter_t889105973
{
public:
	// System.Collections.Hashtable Mono.Xml.Xsl.GenericOutputter::_outputs
	Hashtable_t1853889766 * ____outputs_0;
	// Mono.Xml.Xsl.XslOutput Mono.Xml.Xsl.GenericOutputter::_currentOutput
	XslOutput_t472631936 * ____currentOutput_1;
	// Mono.Xml.Xsl.Emitter Mono.Xml.Xsl.GenericOutputter::_emitter
	Emitter_t942443340 * ____emitter_2;
	// System.IO.TextWriter Mono.Xml.Xsl.GenericOutputter::pendingTextWriter
	TextWriter_t3478189236 * ___pendingTextWriter_3;
	// System.Text.StringBuilder Mono.Xml.Xsl.GenericOutputter::pendingFirstSpaces
	StringBuilder_t1712802186 * ___pendingFirstSpaces_4;
	// System.Xml.WriteState Mono.Xml.Xsl.GenericOutputter::_state
	int32_t ____state_5;
	// Mono.Xml.Xsl.Attribute[] Mono.Xml.Xsl.GenericOutputter::pendingAttributes
	AttributeU5BU5D_t4194715064* ___pendingAttributes_6;
	// System.Int32 Mono.Xml.Xsl.GenericOutputter::pendingAttributesPos
	int32_t ___pendingAttributesPos_7;
	// System.Xml.XmlNamespaceManager Mono.Xml.Xsl.GenericOutputter::_nsManager
	XmlNamespaceManager_t418790500 * ____nsManager_8;
	// System.Collections.Specialized.ListDictionary Mono.Xml.Xsl.GenericOutputter::_currentNamespaceDecls
	ListDictionary_t1624492310 * ____currentNamespaceDecls_9;
	// System.Collections.ArrayList Mono.Xml.Xsl.GenericOutputter::newNamespaces
	ArrayList_t2718874744 * ___newNamespaces_10;
	// System.Xml.NameTable Mono.Xml.Xsl.GenericOutputter::_nt
	NameTable_t3178203267 * ____nt_11;
	// System.Text.Encoding Mono.Xml.Xsl.GenericOutputter::_encoding
	Encoding_t1523322056 * ____encoding_12;
	// System.Boolean Mono.Xml.Xsl.GenericOutputter::_canProcessAttributes
	bool ____canProcessAttributes_13;
	// System.Boolean Mono.Xml.Xsl.GenericOutputter::_insideCData
	bool ____insideCData_14;
	// System.Boolean Mono.Xml.Xsl.GenericOutputter::_omitXmlDeclaration
	bool ____omitXmlDeclaration_15;
	// System.Int32 Mono.Xml.Xsl.GenericOutputter::_xpCount
	int32_t ____xpCount_16;

public:
	inline static int32_t get_offset_of__outputs_0() { return static_cast<int32_t>(offsetof(GenericOutputter_t1694878872, ____outputs_0)); }
	inline Hashtable_t1853889766 * get__outputs_0() const { return ____outputs_0; }
	inline Hashtable_t1853889766 ** get_address_of__outputs_0() { return &____outputs_0; }
	inline void set__outputs_0(Hashtable_t1853889766 * value)
	{
		____outputs_0 = value;
		Il2CppCodeGenWriteBarrier(&____outputs_0, value);
	}

	inline static int32_t get_offset_of__currentOutput_1() { return static_cast<int32_t>(offsetof(GenericOutputter_t1694878872, ____currentOutput_1)); }
	inline XslOutput_t472631936 * get__currentOutput_1() const { return ____currentOutput_1; }
	inline XslOutput_t472631936 ** get_address_of__currentOutput_1() { return &____currentOutput_1; }
	inline void set__currentOutput_1(XslOutput_t472631936 * value)
	{
		____currentOutput_1 = value;
		Il2CppCodeGenWriteBarrier(&____currentOutput_1, value);
	}

	inline static int32_t get_offset_of__emitter_2() { return static_cast<int32_t>(offsetof(GenericOutputter_t1694878872, ____emitter_2)); }
	inline Emitter_t942443340 * get__emitter_2() const { return ____emitter_2; }
	inline Emitter_t942443340 ** get_address_of__emitter_2() { return &____emitter_2; }
	inline void set__emitter_2(Emitter_t942443340 * value)
	{
		____emitter_2 = value;
		Il2CppCodeGenWriteBarrier(&____emitter_2, value);
	}

	inline static int32_t get_offset_of_pendingTextWriter_3() { return static_cast<int32_t>(offsetof(GenericOutputter_t1694878872, ___pendingTextWriter_3)); }
	inline TextWriter_t3478189236 * get_pendingTextWriter_3() const { return ___pendingTextWriter_3; }
	inline TextWriter_t3478189236 ** get_address_of_pendingTextWriter_3() { return &___pendingTextWriter_3; }
	inline void set_pendingTextWriter_3(TextWriter_t3478189236 * value)
	{
		___pendingTextWriter_3 = value;
		Il2CppCodeGenWriteBarrier(&___pendingTextWriter_3, value);
	}

	inline static int32_t get_offset_of_pendingFirstSpaces_4() { return static_cast<int32_t>(offsetof(GenericOutputter_t1694878872, ___pendingFirstSpaces_4)); }
	inline StringBuilder_t1712802186 * get_pendingFirstSpaces_4() const { return ___pendingFirstSpaces_4; }
	inline StringBuilder_t1712802186 ** get_address_of_pendingFirstSpaces_4() { return &___pendingFirstSpaces_4; }
	inline void set_pendingFirstSpaces_4(StringBuilder_t1712802186 * value)
	{
		___pendingFirstSpaces_4 = value;
		Il2CppCodeGenWriteBarrier(&___pendingFirstSpaces_4, value);
	}

	inline static int32_t get_offset_of__state_5() { return static_cast<int32_t>(offsetof(GenericOutputter_t1694878872, ____state_5)); }
	inline int32_t get__state_5() const { return ____state_5; }
	inline int32_t* get_address_of__state_5() { return &____state_5; }
	inline void set__state_5(int32_t value)
	{
		____state_5 = value;
	}

	inline static int32_t get_offset_of_pendingAttributes_6() { return static_cast<int32_t>(offsetof(GenericOutputter_t1694878872, ___pendingAttributes_6)); }
	inline AttributeU5BU5D_t4194715064* get_pendingAttributes_6() const { return ___pendingAttributes_6; }
	inline AttributeU5BU5D_t4194715064** get_address_of_pendingAttributes_6() { return &___pendingAttributes_6; }
	inline void set_pendingAttributes_6(AttributeU5BU5D_t4194715064* value)
	{
		___pendingAttributes_6 = value;
		Il2CppCodeGenWriteBarrier(&___pendingAttributes_6, value);
	}

	inline static int32_t get_offset_of_pendingAttributesPos_7() { return static_cast<int32_t>(offsetof(GenericOutputter_t1694878872, ___pendingAttributesPos_7)); }
	inline int32_t get_pendingAttributesPos_7() const { return ___pendingAttributesPos_7; }
	inline int32_t* get_address_of_pendingAttributesPos_7() { return &___pendingAttributesPos_7; }
	inline void set_pendingAttributesPos_7(int32_t value)
	{
		___pendingAttributesPos_7 = value;
	}

	inline static int32_t get_offset_of__nsManager_8() { return static_cast<int32_t>(offsetof(GenericOutputter_t1694878872, ____nsManager_8)); }
	inline XmlNamespaceManager_t418790500 * get__nsManager_8() const { return ____nsManager_8; }
	inline XmlNamespaceManager_t418790500 ** get_address_of__nsManager_8() { return &____nsManager_8; }
	inline void set__nsManager_8(XmlNamespaceManager_t418790500 * value)
	{
		____nsManager_8 = value;
		Il2CppCodeGenWriteBarrier(&____nsManager_8, value);
	}

	inline static int32_t get_offset_of__currentNamespaceDecls_9() { return static_cast<int32_t>(offsetof(GenericOutputter_t1694878872, ____currentNamespaceDecls_9)); }
	inline ListDictionary_t1624492310 * get__currentNamespaceDecls_9() const { return ____currentNamespaceDecls_9; }
	inline ListDictionary_t1624492310 ** get_address_of__currentNamespaceDecls_9() { return &____currentNamespaceDecls_9; }
	inline void set__currentNamespaceDecls_9(ListDictionary_t1624492310 * value)
	{
		____currentNamespaceDecls_9 = value;
		Il2CppCodeGenWriteBarrier(&____currentNamespaceDecls_9, value);
	}

	inline static int32_t get_offset_of_newNamespaces_10() { return static_cast<int32_t>(offsetof(GenericOutputter_t1694878872, ___newNamespaces_10)); }
	inline ArrayList_t2718874744 * get_newNamespaces_10() const { return ___newNamespaces_10; }
	inline ArrayList_t2718874744 ** get_address_of_newNamespaces_10() { return &___newNamespaces_10; }
	inline void set_newNamespaces_10(ArrayList_t2718874744 * value)
	{
		___newNamespaces_10 = value;
		Il2CppCodeGenWriteBarrier(&___newNamespaces_10, value);
	}

	inline static int32_t get_offset_of__nt_11() { return static_cast<int32_t>(offsetof(GenericOutputter_t1694878872, ____nt_11)); }
	inline NameTable_t3178203267 * get__nt_11() const { return ____nt_11; }
	inline NameTable_t3178203267 ** get_address_of__nt_11() { return &____nt_11; }
	inline void set__nt_11(NameTable_t3178203267 * value)
	{
		____nt_11 = value;
		Il2CppCodeGenWriteBarrier(&____nt_11, value);
	}

	inline static int32_t get_offset_of__encoding_12() { return static_cast<int32_t>(offsetof(GenericOutputter_t1694878872, ____encoding_12)); }
	inline Encoding_t1523322056 * get__encoding_12() const { return ____encoding_12; }
	inline Encoding_t1523322056 ** get_address_of__encoding_12() { return &____encoding_12; }
	inline void set__encoding_12(Encoding_t1523322056 * value)
	{
		____encoding_12 = value;
		Il2CppCodeGenWriteBarrier(&____encoding_12, value);
	}

	inline static int32_t get_offset_of__canProcessAttributes_13() { return static_cast<int32_t>(offsetof(GenericOutputter_t1694878872, ____canProcessAttributes_13)); }
	inline bool get__canProcessAttributes_13() const { return ____canProcessAttributes_13; }
	inline bool* get_address_of__canProcessAttributes_13() { return &____canProcessAttributes_13; }
	inline void set__canProcessAttributes_13(bool value)
	{
		____canProcessAttributes_13 = value;
	}

	inline static int32_t get_offset_of__insideCData_14() { return static_cast<int32_t>(offsetof(GenericOutputter_t1694878872, ____insideCData_14)); }
	inline bool get__insideCData_14() const { return ____insideCData_14; }
	inline bool* get_address_of__insideCData_14() { return &____insideCData_14; }
	inline void set__insideCData_14(bool value)
	{
		____insideCData_14 = value;
	}

	inline static int32_t get_offset_of__omitXmlDeclaration_15() { return static_cast<int32_t>(offsetof(GenericOutputter_t1694878872, ____omitXmlDeclaration_15)); }
	inline bool get__omitXmlDeclaration_15() const { return ____omitXmlDeclaration_15; }
	inline bool* get_address_of__omitXmlDeclaration_15() { return &____omitXmlDeclaration_15; }
	inline void set__omitXmlDeclaration_15(bool value)
	{
		____omitXmlDeclaration_15 = value;
	}

	inline static int32_t get_offset_of__xpCount_16() { return static_cast<int32_t>(offsetof(GenericOutputter_t1694878872, ____xpCount_16)); }
	inline int32_t get__xpCount_16() const { return ____xpCount_16; }
	inline int32_t* get_address_of__xpCount_16() { return &____xpCount_16; }
	inline void set__xpCount_16(int32_t value)
	{
		____xpCount_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
