﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"

// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// UnityEngine.RenderBuffer[]
struct RenderBufferU5BU5D_t1615831949;
struct RenderBuffer_t586150500 ;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter/Frame
struct  Frame_t3511111948 
{
public:
	// UnityEngine.RenderTexture UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter/Frame::lumaTexture
	RenderTexture_t2108887433 * ___lumaTexture_0;
	// UnityEngine.RenderTexture UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter/Frame::chromaTexture
	RenderTexture_t2108887433 * ___chromaTexture_1;
	// System.Single UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter/Frame::time
	float ___time_2;
	// UnityEngine.RenderBuffer[] UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter/Frame::_mrt
	RenderBufferU5BU5D_t1615831949* ____mrt_3;

public:
	inline static int32_t get_offset_of_lumaTexture_0() { return static_cast<int32_t>(offsetof(Frame_t3511111948, ___lumaTexture_0)); }
	inline RenderTexture_t2108887433 * get_lumaTexture_0() const { return ___lumaTexture_0; }
	inline RenderTexture_t2108887433 ** get_address_of_lumaTexture_0() { return &___lumaTexture_0; }
	inline void set_lumaTexture_0(RenderTexture_t2108887433 * value)
	{
		___lumaTexture_0 = value;
		Il2CppCodeGenWriteBarrier(&___lumaTexture_0, value);
	}

	inline static int32_t get_offset_of_chromaTexture_1() { return static_cast<int32_t>(offsetof(Frame_t3511111948, ___chromaTexture_1)); }
	inline RenderTexture_t2108887433 * get_chromaTexture_1() const { return ___chromaTexture_1; }
	inline RenderTexture_t2108887433 ** get_address_of_chromaTexture_1() { return &___chromaTexture_1; }
	inline void set_chromaTexture_1(RenderTexture_t2108887433 * value)
	{
		___chromaTexture_1 = value;
		Il2CppCodeGenWriteBarrier(&___chromaTexture_1, value);
	}

	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(Frame_t3511111948, ___time_2)); }
	inline float get_time_2() const { return ___time_2; }
	inline float* get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(float value)
	{
		___time_2 = value;
	}

	inline static int32_t get_offset_of__mrt_3() { return static_cast<int32_t>(offsetof(Frame_t3511111948, ____mrt_3)); }
	inline RenderBufferU5BU5D_t1615831949* get__mrt_3() const { return ____mrt_3; }
	inline RenderBufferU5BU5D_t1615831949** get_address_of__mrt_3() { return &____mrt_3; }
	inline void set__mrt_3(RenderBufferU5BU5D_t1615831949* value)
	{
		____mrt_3 = value;
		Il2CppCodeGenWriteBarrier(&____mrt_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter/Frame
struct Frame_t3511111948_marshaled_pinvoke
{
	RenderTexture_t2108887433 * ___lumaTexture_0;
	RenderTexture_t2108887433 * ___chromaTexture_1;
	float ___time_2;
	RenderBuffer_t586150500 * ____mrt_3;
};
// Native definition for COM marshalling of UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter/Frame
struct Frame_t3511111948_marshaled_com
{
	RenderTexture_t2108887433 * ___lumaTexture_0;
	RenderTexture_t2108887433 * ___chromaTexture_1;
	float ___time_2;
	RenderBuffer_t586150500 * ____mrt_3;
};
