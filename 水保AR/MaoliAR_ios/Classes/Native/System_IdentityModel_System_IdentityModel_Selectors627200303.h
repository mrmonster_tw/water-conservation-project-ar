﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Selector2079378378.h"
#include "mscorlib_System_Security_Principal_TokenImpersonat3773270939.h"

// System.String
struct String_t;
// System.Net.NetworkCredential
struct NetworkCredential_t3282608323;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Selectors.KerberosSecurityTokenProvider
struct  KerberosSecurityTokenProvider_t627200303  : public SecurityTokenProvider_t2079378378
{
public:
	// System.String System.IdentityModel.Selectors.KerberosSecurityTokenProvider::name
	String_t* ___name_0;
	// System.Security.Principal.TokenImpersonationLevel System.IdentityModel.Selectors.KerberosSecurityTokenProvider::impersonation_level
	int32_t ___impersonation_level_1;
	// System.Net.NetworkCredential System.IdentityModel.Selectors.KerberosSecurityTokenProvider::credential
	NetworkCredential_t3282608323 * ___credential_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(KerberosSecurityTokenProvider_t627200303, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_impersonation_level_1() { return static_cast<int32_t>(offsetof(KerberosSecurityTokenProvider_t627200303, ___impersonation_level_1)); }
	inline int32_t get_impersonation_level_1() const { return ___impersonation_level_1; }
	inline int32_t* get_address_of_impersonation_level_1() { return &___impersonation_level_1; }
	inline void set_impersonation_level_1(int32_t value)
	{
		___impersonation_level_1 = value;
	}

	inline static int32_t get_offset_of_credential_2() { return static_cast<int32_t>(offsetof(KerberosSecurityTokenProvider_t627200303, ___credential_2)); }
	inline NetworkCredential_t3282608323 * get_credential_2() const { return ___credential_2; }
	inline NetworkCredential_t3282608323 ** get_address_of_credential_2() { return &___credential_2; }
	inline void set_credential_2(NetworkCredential_t3282608323 * value)
	{
		___credential_2 = value;
		Il2CppCodeGenWriteBarrier(&___credential_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
