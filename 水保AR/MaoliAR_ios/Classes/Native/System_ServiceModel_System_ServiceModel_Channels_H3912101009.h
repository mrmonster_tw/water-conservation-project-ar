﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_H3561535433.h"

// System.Net.HttpListenerContext
struct HttpListenerContext_t424880822;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.HttpListenerContextInfo
struct  HttpListenerContextInfo_t3912101009  : public HttpContextInfo_t3561535433
{
public:
	// System.Net.HttpListenerContext System.ServiceModel.Channels.HttpListenerContextInfo::ctx
	HttpListenerContext_t424880822 * ___ctx_0;

public:
	inline static int32_t get_offset_of_ctx_0() { return static_cast<int32_t>(offsetof(HttpListenerContextInfo_t3912101009, ___ctx_0)); }
	inline HttpListenerContext_t424880822 * get_ctx_0() const { return ___ctx_0; }
	inline HttpListenerContext_t424880822 ** get_address_of_ctx_0() { return &___ctx_0; }
	inline void set_ctx_0(HttpListenerContext_t424880822 * value)
	{
		___ctx_0 = value;
		Il2CppCodeGenWriteBarrier(&___ctx_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
