﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_UserControlParser2039383077.h"

// System.String[]
struct StringU5BU5D_t1281789340;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.PageThemeParser
struct  PageThemeParser_t3466356847  : public UserControlParser_t2039383077
{
public:
	// System.String[] System.Web.UI.PageThemeParser::linkedStyleSheets
	StringU5BU5D_t1281789340* ___linkedStyleSheets_62;

public:
	inline static int32_t get_offset_of_linkedStyleSheets_62() { return static_cast<int32_t>(offsetof(PageThemeParser_t3466356847, ___linkedStyleSheets_62)); }
	inline StringU5BU5D_t1281789340* get_linkedStyleSheets_62() const { return ___linkedStyleSheets_62; }
	inline StringU5BU5D_t1281789340** get_address_of_linkedStyleSheets_62() { return &___linkedStyleSheets_62; }
	inline void set_linkedStyleSheets_62(StringU5BU5D_t1281789340* value)
	{
		___linkedStyleSheets_62 = value;
		Il2CppCodeGenWriteBarrier(&___linkedStyleSheets_62, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
