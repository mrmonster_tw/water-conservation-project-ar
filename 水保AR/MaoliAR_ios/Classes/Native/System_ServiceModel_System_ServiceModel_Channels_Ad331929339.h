﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Collections_ObjectModel_ReadOnlyColl13919956.h"

// System.ServiceModel.Channels.AddressHeader[]
struct AddressHeaderU5BU5D_t2064441560;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.AddressHeaderCollection
struct  AddressHeaderCollection_t331929339  : public ReadOnlyCollection_1_t13919956
{
public:

public:
};

struct AddressHeaderCollection_t331929339_StaticFields
{
public:
	// System.ServiceModel.Channels.AddressHeader[] System.ServiceModel.Channels.AddressHeaderCollection::empty
	AddressHeaderU5BU5D_t2064441560* ___empty_1;

public:
	inline static int32_t get_offset_of_empty_1() { return static_cast<int32_t>(offsetof(AddressHeaderCollection_t331929339_StaticFields, ___empty_1)); }
	inline AddressHeaderU5BU5D_t2064441560* get_empty_1() const { return ___empty_1; }
	inline AddressHeaderU5BU5D_t2064441560** get_address_of_empty_1() { return &___empty_1; }
	inline void set_empty_1(AddressHeaderU5BU5D_t2064441560* value)
	{
		___empty_1 = value;
		Il2CppCodeGenWriteBarrier(&___empty_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
