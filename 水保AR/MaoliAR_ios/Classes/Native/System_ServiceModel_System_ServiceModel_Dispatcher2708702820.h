﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.EndpointAddress
struct EndpointAddress_t3119842923;
// System.String
struct String_t;
// System.ServiceModel.Dispatcher.ChannelDispatcher
struct ChannelDispatcher_t2781106991;
// System.ServiceModel.Dispatcher.MessageFilter
struct MessageFilter_t4072893799;
// System.ServiceModel.Dispatcher.DispatchRuntime
struct DispatchRuntime_t796075230;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.EndpointDispatcher
struct  EndpointDispatcher_t2708702820  : public Il2CppObject
{
public:
	// System.ServiceModel.EndpointAddress System.ServiceModel.Dispatcher.EndpointDispatcher::address
	EndpointAddress_t3119842923 * ___address_0;
	// System.String System.ServiceModel.Dispatcher.EndpointDispatcher::contract_name
	String_t* ___contract_name_1;
	// System.String System.ServiceModel.Dispatcher.EndpointDispatcher::contract_ns
	String_t* ___contract_ns_2;
	// System.ServiceModel.Dispatcher.ChannelDispatcher System.ServiceModel.Dispatcher.EndpointDispatcher::channel_dispatcher
	ChannelDispatcher_t2781106991 * ___channel_dispatcher_3;
	// System.ServiceModel.Dispatcher.MessageFilter System.ServiceModel.Dispatcher.EndpointDispatcher::address_filter
	MessageFilter_t4072893799 * ___address_filter_4;
	// System.ServiceModel.Dispatcher.MessageFilter System.ServiceModel.Dispatcher.EndpointDispatcher::contract_filter
	MessageFilter_t4072893799 * ___contract_filter_5;
	// System.Int32 System.ServiceModel.Dispatcher.EndpointDispatcher::filter_priority
	int32_t ___filter_priority_6;
	// System.ServiceModel.Dispatcher.DispatchRuntime System.ServiceModel.Dispatcher.EndpointDispatcher::dispatch_runtime
	DispatchRuntime_t796075230 * ___dispatch_runtime_7;

public:
	inline static int32_t get_offset_of_address_0() { return static_cast<int32_t>(offsetof(EndpointDispatcher_t2708702820, ___address_0)); }
	inline EndpointAddress_t3119842923 * get_address_0() const { return ___address_0; }
	inline EndpointAddress_t3119842923 ** get_address_of_address_0() { return &___address_0; }
	inline void set_address_0(EndpointAddress_t3119842923 * value)
	{
		___address_0 = value;
		Il2CppCodeGenWriteBarrier(&___address_0, value);
	}

	inline static int32_t get_offset_of_contract_name_1() { return static_cast<int32_t>(offsetof(EndpointDispatcher_t2708702820, ___contract_name_1)); }
	inline String_t* get_contract_name_1() const { return ___contract_name_1; }
	inline String_t** get_address_of_contract_name_1() { return &___contract_name_1; }
	inline void set_contract_name_1(String_t* value)
	{
		___contract_name_1 = value;
		Il2CppCodeGenWriteBarrier(&___contract_name_1, value);
	}

	inline static int32_t get_offset_of_contract_ns_2() { return static_cast<int32_t>(offsetof(EndpointDispatcher_t2708702820, ___contract_ns_2)); }
	inline String_t* get_contract_ns_2() const { return ___contract_ns_2; }
	inline String_t** get_address_of_contract_ns_2() { return &___contract_ns_2; }
	inline void set_contract_ns_2(String_t* value)
	{
		___contract_ns_2 = value;
		Il2CppCodeGenWriteBarrier(&___contract_ns_2, value);
	}

	inline static int32_t get_offset_of_channel_dispatcher_3() { return static_cast<int32_t>(offsetof(EndpointDispatcher_t2708702820, ___channel_dispatcher_3)); }
	inline ChannelDispatcher_t2781106991 * get_channel_dispatcher_3() const { return ___channel_dispatcher_3; }
	inline ChannelDispatcher_t2781106991 ** get_address_of_channel_dispatcher_3() { return &___channel_dispatcher_3; }
	inline void set_channel_dispatcher_3(ChannelDispatcher_t2781106991 * value)
	{
		___channel_dispatcher_3 = value;
		Il2CppCodeGenWriteBarrier(&___channel_dispatcher_3, value);
	}

	inline static int32_t get_offset_of_address_filter_4() { return static_cast<int32_t>(offsetof(EndpointDispatcher_t2708702820, ___address_filter_4)); }
	inline MessageFilter_t4072893799 * get_address_filter_4() const { return ___address_filter_4; }
	inline MessageFilter_t4072893799 ** get_address_of_address_filter_4() { return &___address_filter_4; }
	inline void set_address_filter_4(MessageFilter_t4072893799 * value)
	{
		___address_filter_4 = value;
		Il2CppCodeGenWriteBarrier(&___address_filter_4, value);
	}

	inline static int32_t get_offset_of_contract_filter_5() { return static_cast<int32_t>(offsetof(EndpointDispatcher_t2708702820, ___contract_filter_5)); }
	inline MessageFilter_t4072893799 * get_contract_filter_5() const { return ___contract_filter_5; }
	inline MessageFilter_t4072893799 ** get_address_of_contract_filter_5() { return &___contract_filter_5; }
	inline void set_contract_filter_5(MessageFilter_t4072893799 * value)
	{
		___contract_filter_5 = value;
		Il2CppCodeGenWriteBarrier(&___contract_filter_5, value);
	}

	inline static int32_t get_offset_of_filter_priority_6() { return static_cast<int32_t>(offsetof(EndpointDispatcher_t2708702820, ___filter_priority_6)); }
	inline int32_t get_filter_priority_6() const { return ___filter_priority_6; }
	inline int32_t* get_address_of_filter_priority_6() { return &___filter_priority_6; }
	inline void set_filter_priority_6(int32_t value)
	{
		___filter_priority_6 = value;
	}

	inline static int32_t get_offset_of_dispatch_runtime_7() { return static_cast<int32_t>(offsetof(EndpointDispatcher_t2708702820, ___dispatch_runtime_7)); }
	inline DispatchRuntime_t796075230 * get_dispatch_runtime_7() const { return ___dispatch_runtime_7; }
	inline DispatchRuntime_t796075230 ** get_address_of_dispatch_runtime_7() { return &___dispatch_runtime_7; }
	inline void set_dispatch_runtime_7(DispatchRuntime_t796075230 * value)
	{
		___dispatch_runtime_7 = value;
		Il2CppCodeGenWriteBarrier(&___dispatch_runtime_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
