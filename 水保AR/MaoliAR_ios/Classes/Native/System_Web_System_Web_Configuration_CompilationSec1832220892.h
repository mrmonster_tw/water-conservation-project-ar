﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3156163955.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.CompilationSection
struct  CompilationSection_t1832220892  : public ConfigurationSection_t3156163955
{
public:

public:
};

struct CompilationSection_t1832220892_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Web.Configuration.CompilationSection::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_17;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.CompilationSection::compilersProp
	ConfigurationProperty_t3590861854 * ___compilersProp_18;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.CompilationSection::tempDirectoryProp
	ConfigurationProperty_t3590861854 * ___tempDirectoryProp_19;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.CompilationSection::debugProp
	ConfigurationProperty_t3590861854 * ___debugProp_20;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.CompilationSection::strictProp
	ConfigurationProperty_t3590861854 * ___strictProp_21;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.CompilationSection::explicitProp
	ConfigurationProperty_t3590861854 * ___explicitProp_22;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.CompilationSection::batchProp
	ConfigurationProperty_t3590861854 * ___batchProp_23;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.CompilationSection::batchTimeoutProp
	ConfigurationProperty_t3590861854 * ___batchTimeoutProp_24;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.CompilationSection::maxBatchSizeProp
	ConfigurationProperty_t3590861854 * ___maxBatchSizeProp_25;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.CompilationSection::maxBatchGeneratedFileSizeProp
	ConfigurationProperty_t3590861854 * ___maxBatchGeneratedFileSizeProp_26;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.CompilationSection::numRecompilesBeforeAppRestartProp
	ConfigurationProperty_t3590861854 * ___numRecompilesBeforeAppRestartProp_27;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.CompilationSection::defaultLanguageProp
	ConfigurationProperty_t3590861854 * ___defaultLanguageProp_28;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.CompilationSection::assembliesProp
	ConfigurationProperty_t3590861854 * ___assembliesProp_29;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.CompilationSection::assemblyPostProcessorTypeProp
	ConfigurationProperty_t3590861854 * ___assemblyPostProcessorTypeProp_30;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.CompilationSection::buildProvidersProp
	ConfigurationProperty_t3590861854 * ___buildProvidersProp_31;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.CompilationSection::expressionBuildersProp
	ConfigurationProperty_t3590861854 * ___expressionBuildersProp_32;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.CompilationSection::urlLinePragmasProp
	ConfigurationProperty_t3590861854 * ___urlLinePragmasProp_33;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.CompilationSection::codeSubDirectoriesProp
	ConfigurationProperty_t3590861854 * ___codeSubDirectoriesProp_34;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.CompilationSection::optimizeCompilationsProp
	ConfigurationProperty_t3590861854 * ___optimizeCompilationsProp_35;

public:
	inline static int32_t get_offset_of_properties_17() { return static_cast<int32_t>(offsetof(CompilationSection_t1832220892_StaticFields, ___properties_17)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_17() const { return ___properties_17; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_17() { return &___properties_17; }
	inline void set_properties_17(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_17 = value;
		Il2CppCodeGenWriteBarrier(&___properties_17, value);
	}

	inline static int32_t get_offset_of_compilersProp_18() { return static_cast<int32_t>(offsetof(CompilationSection_t1832220892_StaticFields, ___compilersProp_18)); }
	inline ConfigurationProperty_t3590861854 * get_compilersProp_18() const { return ___compilersProp_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_compilersProp_18() { return &___compilersProp_18; }
	inline void set_compilersProp_18(ConfigurationProperty_t3590861854 * value)
	{
		___compilersProp_18 = value;
		Il2CppCodeGenWriteBarrier(&___compilersProp_18, value);
	}

	inline static int32_t get_offset_of_tempDirectoryProp_19() { return static_cast<int32_t>(offsetof(CompilationSection_t1832220892_StaticFields, ___tempDirectoryProp_19)); }
	inline ConfigurationProperty_t3590861854 * get_tempDirectoryProp_19() const { return ___tempDirectoryProp_19; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_tempDirectoryProp_19() { return &___tempDirectoryProp_19; }
	inline void set_tempDirectoryProp_19(ConfigurationProperty_t3590861854 * value)
	{
		___tempDirectoryProp_19 = value;
		Il2CppCodeGenWriteBarrier(&___tempDirectoryProp_19, value);
	}

	inline static int32_t get_offset_of_debugProp_20() { return static_cast<int32_t>(offsetof(CompilationSection_t1832220892_StaticFields, ___debugProp_20)); }
	inline ConfigurationProperty_t3590861854 * get_debugProp_20() const { return ___debugProp_20; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_debugProp_20() { return &___debugProp_20; }
	inline void set_debugProp_20(ConfigurationProperty_t3590861854 * value)
	{
		___debugProp_20 = value;
		Il2CppCodeGenWriteBarrier(&___debugProp_20, value);
	}

	inline static int32_t get_offset_of_strictProp_21() { return static_cast<int32_t>(offsetof(CompilationSection_t1832220892_StaticFields, ___strictProp_21)); }
	inline ConfigurationProperty_t3590861854 * get_strictProp_21() const { return ___strictProp_21; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_strictProp_21() { return &___strictProp_21; }
	inline void set_strictProp_21(ConfigurationProperty_t3590861854 * value)
	{
		___strictProp_21 = value;
		Il2CppCodeGenWriteBarrier(&___strictProp_21, value);
	}

	inline static int32_t get_offset_of_explicitProp_22() { return static_cast<int32_t>(offsetof(CompilationSection_t1832220892_StaticFields, ___explicitProp_22)); }
	inline ConfigurationProperty_t3590861854 * get_explicitProp_22() const { return ___explicitProp_22; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_explicitProp_22() { return &___explicitProp_22; }
	inline void set_explicitProp_22(ConfigurationProperty_t3590861854 * value)
	{
		___explicitProp_22 = value;
		Il2CppCodeGenWriteBarrier(&___explicitProp_22, value);
	}

	inline static int32_t get_offset_of_batchProp_23() { return static_cast<int32_t>(offsetof(CompilationSection_t1832220892_StaticFields, ___batchProp_23)); }
	inline ConfigurationProperty_t3590861854 * get_batchProp_23() const { return ___batchProp_23; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_batchProp_23() { return &___batchProp_23; }
	inline void set_batchProp_23(ConfigurationProperty_t3590861854 * value)
	{
		___batchProp_23 = value;
		Il2CppCodeGenWriteBarrier(&___batchProp_23, value);
	}

	inline static int32_t get_offset_of_batchTimeoutProp_24() { return static_cast<int32_t>(offsetof(CompilationSection_t1832220892_StaticFields, ___batchTimeoutProp_24)); }
	inline ConfigurationProperty_t3590861854 * get_batchTimeoutProp_24() const { return ___batchTimeoutProp_24; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_batchTimeoutProp_24() { return &___batchTimeoutProp_24; }
	inline void set_batchTimeoutProp_24(ConfigurationProperty_t3590861854 * value)
	{
		___batchTimeoutProp_24 = value;
		Il2CppCodeGenWriteBarrier(&___batchTimeoutProp_24, value);
	}

	inline static int32_t get_offset_of_maxBatchSizeProp_25() { return static_cast<int32_t>(offsetof(CompilationSection_t1832220892_StaticFields, ___maxBatchSizeProp_25)); }
	inline ConfigurationProperty_t3590861854 * get_maxBatchSizeProp_25() const { return ___maxBatchSizeProp_25; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_maxBatchSizeProp_25() { return &___maxBatchSizeProp_25; }
	inline void set_maxBatchSizeProp_25(ConfigurationProperty_t3590861854 * value)
	{
		___maxBatchSizeProp_25 = value;
		Il2CppCodeGenWriteBarrier(&___maxBatchSizeProp_25, value);
	}

	inline static int32_t get_offset_of_maxBatchGeneratedFileSizeProp_26() { return static_cast<int32_t>(offsetof(CompilationSection_t1832220892_StaticFields, ___maxBatchGeneratedFileSizeProp_26)); }
	inline ConfigurationProperty_t3590861854 * get_maxBatchGeneratedFileSizeProp_26() const { return ___maxBatchGeneratedFileSizeProp_26; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_maxBatchGeneratedFileSizeProp_26() { return &___maxBatchGeneratedFileSizeProp_26; }
	inline void set_maxBatchGeneratedFileSizeProp_26(ConfigurationProperty_t3590861854 * value)
	{
		___maxBatchGeneratedFileSizeProp_26 = value;
		Il2CppCodeGenWriteBarrier(&___maxBatchGeneratedFileSizeProp_26, value);
	}

	inline static int32_t get_offset_of_numRecompilesBeforeAppRestartProp_27() { return static_cast<int32_t>(offsetof(CompilationSection_t1832220892_StaticFields, ___numRecompilesBeforeAppRestartProp_27)); }
	inline ConfigurationProperty_t3590861854 * get_numRecompilesBeforeAppRestartProp_27() const { return ___numRecompilesBeforeAppRestartProp_27; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_numRecompilesBeforeAppRestartProp_27() { return &___numRecompilesBeforeAppRestartProp_27; }
	inline void set_numRecompilesBeforeAppRestartProp_27(ConfigurationProperty_t3590861854 * value)
	{
		___numRecompilesBeforeAppRestartProp_27 = value;
		Il2CppCodeGenWriteBarrier(&___numRecompilesBeforeAppRestartProp_27, value);
	}

	inline static int32_t get_offset_of_defaultLanguageProp_28() { return static_cast<int32_t>(offsetof(CompilationSection_t1832220892_StaticFields, ___defaultLanguageProp_28)); }
	inline ConfigurationProperty_t3590861854 * get_defaultLanguageProp_28() const { return ___defaultLanguageProp_28; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_defaultLanguageProp_28() { return &___defaultLanguageProp_28; }
	inline void set_defaultLanguageProp_28(ConfigurationProperty_t3590861854 * value)
	{
		___defaultLanguageProp_28 = value;
		Il2CppCodeGenWriteBarrier(&___defaultLanguageProp_28, value);
	}

	inline static int32_t get_offset_of_assembliesProp_29() { return static_cast<int32_t>(offsetof(CompilationSection_t1832220892_StaticFields, ___assembliesProp_29)); }
	inline ConfigurationProperty_t3590861854 * get_assembliesProp_29() const { return ___assembliesProp_29; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_assembliesProp_29() { return &___assembliesProp_29; }
	inline void set_assembliesProp_29(ConfigurationProperty_t3590861854 * value)
	{
		___assembliesProp_29 = value;
		Il2CppCodeGenWriteBarrier(&___assembliesProp_29, value);
	}

	inline static int32_t get_offset_of_assemblyPostProcessorTypeProp_30() { return static_cast<int32_t>(offsetof(CompilationSection_t1832220892_StaticFields, ___assemblyPostProcessorTypeProp_30)); }
	inline ConfigurationProperty_t3590861854 * get_assemblyPostProcessorTypeProp_30() const { return ___assemblyPostProcessorTypeProp_30; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_assemblyPostProcessorTypeProp_30() { return &___assemblyPostProcessorTypeProp_30; }
	inline void set_assemblyPostProcessorTypeProp_30(ConfigurationProperty_t3590861854 * value)
	{
		___assemblyPostProcessorTypeProp_30 = value;
		Il2CppCodeGenWriteBarrier(&___assemblyPostProcessorTypeProp_30, value);
	}

	inline static int32_t get_offset_of_buildProvidersProp_31() { return static_cast<int32_t>(offsetof(CompilationSection_t1832220892_StaticFields, ___buildProvidersProp_31)); }
	inline ConfigurationProperty_t3590861854 * get_buildProvidersProp_31() const { return ___buildProvidersProp_31; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_buildProvidersProp_31() { return &___buildProvidersProp_31; }
	inline void set_buildProvidersProp_31(ConfigurationProperty_t3590861854 * value)
	{
		___buildProvidersProp_31 = value;
		Il2CppCodeGenWriteBarrier(&___buildProvidersProp_31, value);
	}

	inline static int32_t get_offset_of_expressionBuildersProp_32() { return static_cast<int32_t>(offsetof(CompilationSection_t1832220892_StaticFields, ___expressionBuildersProp_32)); }
	inline ConfigurationProperty_t3590861854 * get_expressionBuildersProp_32() const { return ___expressionBuildersProp_32; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_expressionBuildersProp_32() { return &___expressionBuildersProp_32; }
	inline void set_expressionBuildersProp_32(ConfigurationProperty_t3590861854 * value)
	{
		___expressionBuildersProp_32 = value;
		Il2CppCodeGenWriteBarrier(&___expressionBuildersProp_32, value);
	}

	inline static int32_t get_offset_of_urlLinePragmasProp_33() { return static_cast<int32_t>(offsetof(CompilationSection_t1832220892_StaticFields, ___urlLinePragmasProp_33)); }
	inline ConfigurationProperty_t3590861854 * get_urlLinePragmasProp_33() const { return ___urlLinePragmasProp_33; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_urlLinePragmasProp_33() { return &___urlLinePragmasProp_33; }
	inline void set_urlLinePragmasProp_33(ConfigurationProperty_t3590861854 * value)
	{
		___urlLinePragmasProp_33 = value;
		Il2CppCodeGenWriteBarrier(&___urlLinePragmasProp_33, value);
	}

	inline static int32_t get_offset_of_codeSubDirectoriesProp_34() { return static_cast<int32_t>(offsetof(CompilationSection_t1832220892_StaticFields, ___codeSubDirectoriesProp_34)); }
	inline ConfigurationProperty_t3590861854 * get_codeSubDirectoriesProp_34() const { return ___codeSubDirectoriesProp_34; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_codeSubDirectoriesProp_34() { return &___codeSubDirectoriesProp_34; }
	inline void set_codeSubDirectoriesProp_34(ConfigurationProperty_t3590861854 * value)
	{
		___codeSubDirectoriesProp_34 = value;
		Il2CppCodeGenWriteBarrier(&___codeSubDirectoriesProp_34, value);
	}

	inline static int32_t get_offset_of_optimizeCompilationsProp_35() { return static_cast<int32_t>(offsetof(CompilationSection_t1832220892_StaticFields, ___optimizeCompilationsProp_35)); }
	inline ConfigurationProperty_t3590861854 * get_optimizeCompilationsProp_35() const { return ___optimizeCompilationsProp_35; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_optimizeCompilationsProp_35() { return &___optimizeCompilationsProp_35; }
	inline void set_optimizeCompilationsProp_35(ConfigurationProperty_t3590861854 * value)
	{
		___optimizeCompilationsProp_35 = value;
		Il2CppCodeGenWriteBarrier(&___optimizeCompilationsProp_35, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
