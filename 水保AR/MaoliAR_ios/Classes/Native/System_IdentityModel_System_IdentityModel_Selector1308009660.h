﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Selector1453713750.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Selectors.WindowsUserNameSecurityTokenAuthenticator
struct  WindowsUserNameSecurityTokenAuthenticator_t1308009660  : public UserNameSecurityTokenAuthenticator_t1453713750
{
public:
	// System.Boolean System.IdentityModel.Selectors.WindowsUserNameSecurityTokenAuthenticator::include_win_groups
	bool ___include_win_groups_0;

public:
	inline static int32_t get_offset_of_include_win_groups_0() { return static_cast<int32_t>(offsetof(WindowsUserNameSecurityTokenAuthenticator_t1308009660, ___include_win_groups_0)); }
	inline bool get_include_win_groups_0() const { return ___include_win_groups_0; }
	inline bool* get_address_of_include_win_groups_0() { return &___include_win_groups_0; }
	inline void set_include_win_groups_0(bool value)
	{
		___include_win_groups_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
