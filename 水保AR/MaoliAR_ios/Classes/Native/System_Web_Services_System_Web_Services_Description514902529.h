﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.Services.Description.SoapProtocolImporter
struct SoapProtocolImporter_t721795827;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.SoapExtensionImporter
struct  SoapExtensionImporter_t514902529  : public Il2CppObject
{
public:
	// System.Web.Services.Description.SoapProtocolImporter System.Web.Services.Description.SoapExtensionImporter::importContext
	SoapProtocolImporter_t721795827 * ___importContext_0;

public:
	inline static int32_t get_offset_of_importContext_0() { return static_cast<int32_t>(offsetof(SoapExtensionImporter_t514902529, ___importContext_0)); }
	inline SoapProtocolImporter_t721795827 * get_importContext_0() const { return ___importContext_0; }
	inline SoapProtocolImporter_t721795827 ** get_address_of_importContext_0() { return &___importContext_0; }
	inline void set_importContext_0(SoapProtocolImporter_t721795827 * value)
	{
		___importContext_0 = value;
		Il2CppCodeGenWriteBarrier(&___importContext_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
