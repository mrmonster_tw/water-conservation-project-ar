﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Claims_C3529661467.h"

// System.Collections.Generic.List`1<System.IdentityModel.Claims.Claim>
struct List_1_t3799121090;
// System.IdentityModel.Claims.ClaimSet
struct ClaimSet_t3529661467;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Claims.DefaultClaimSet
struct  DefaultClaimSet_t3476487852  : public ClaimSet_t3529661467
{
public:
	// System.Collections.Generic.List`1<System.IdentityModel.Claims.Claim> System.IdentityModel.Claims.DefaultClaimSet::list
	List_1_t3799121090 * ___list_1;
	// System.IdentityModel.Claims.ClaimSet System.IdentityModel.Claims.DefaultClaimSet::issuer
	ClaimSet_t3529661467 * ___issuer_2;

public:
	inline static int32_t get_offset_of_list_1() { return static_cast<int32_t>(offsetof(DefaultClaimSet_t3476487852, ___list_1)); }
	inline List_1_t3799121090 * get_list_1() const { return ___list_1; }
	inline List_1_t3799121090 ** get_address_of_list_1() { return &___list_1; }
	inline void set_list_1(List_1_t3799121090 * value)
	{
		___list_1 = value;
		Il2CppCodeGenWriteBarrier(&___list_1, value);
	}

	inline static int32_t get_offset_of_issuer_2() { return static_cast<int32_t>(offsetof(DefaultClaimSet_t3476487852, ___issuer_2)); }
	inline ClaimSet_t3529661467 * get_issuer_2() const { return ___issuer_2; }
	inline ClaimSet_t3529661467 ** get_address_of_issuer_2() { return &___issuer_2; }
	inline void set_issuer_2(ClaimSet_t3529661467 * value)
	{
		___issuer_2 = value;
		Il2CppCodeGenWriteBarrier(&___issuer_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
