﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Collections.Generic.KeyedByTypeCollection`1<System.Object>
struct KeyedByTypeCollection_1_t1429017627;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1<System.Object>
struct  ServiceModelExtensionCollectionElement_1_t2544877501  : public ConfigurationElement_t3318566633
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Collections.Generic.KeyedByTypeCollection`1<TServiceModelExtensionElement> System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1::_list
	KeyedByTypeCollection_1_t1429017627 * ____list_14;
	// System.Boolean System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1::is_modified
	bool ___is_modified_15;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(ServiceModelExtensionCollectionElement_1_t2544877501, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier(&___properties_13, value);
	}

	inline static int32_t get_offset_of__list_14() { return static_cast<int32_t>(offsetof(ServiceModelExtensionCollectionElement_1_t2544877501, ____list_14)); }
	inline KeyedByTypeCollection_1_t1429017627 * get__list_14() const { return ____list_14; }
	inline KeyedByTypeCollection_1_t1429017627 ** get_address_of__list_14() { return &____list_14; }
	inline void set__list_14(KeyedByTypeCollection_1_t1429017627 * value)
	{
		____list_14 = value;
		Il2CppCodeGenWriteBarrier(&____list_14, value);
	}

	inline static int32_t get_offset_of_is_modified_15() { return static_cast<int32_t>(offsetof(ServiceModelExtensionCollectionElement_1_t2544877501, ___is_modified_15)); }
	inline bool get_is_modified_15() const { return ___is_modified_15; }
	inline bool* get_address_of_is_modified_15() { return &___is_modified_15; }
	inline void set_is_modified_15(bool value)
	{
		___is_modified_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
