﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Cha19627360.h"

// System.ServiceModel.Channels.TransportBindingElement
struct TransportBindingElement_t2144904149;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.TransportChannelFactoryBase`1<System.Object>
struct  TransportChannelFactoryBase_1_t4020609293  : public ChannelFactoryBase_1_t19627360
{
public:
	// System.ServiceModel.Channels.TransportBindingElement System.ServiceModel.Channels.TransportChannelFactoryBase`1::<Transport>k__BackingField
	TransportBindingElement_t2144904149 * ___U3CTransportU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CTransportU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(TransportChannelFactoryBase_1_t4020609293, ___U3CTransportU3Ek__BackingField_14)); }
	inline TransportBindingElement_t2144904149 * get_U3CTransportU3Ek__BackingField_14() const { return ___U3CTransportU3Ek__BackingField_14; }
	inline TransportBindingElement_t2144904149 ** get_address_of_U3CTransportU3Ek__BackingField_14() { return &___U3CTransportU3Ek__BackingField_14; }
	inline void set_U3CTransportU3Ek__BackingField_14(TransportBindingElement_t2144904149 * value)
	{
		___U3CTransportU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTransportU3Ek__BackingField_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
