﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.ControlTraceData
struct  ControlTraceData_t1335153449  : public Il2CppObject
{
public:
	// System.String System.Web.ControlTraceData::ControlId
	String_t* ___ControlId_0;
	// System.Type System.Web.ControlTraceData::Type
	Type_t * ___Type_1;
	// System.Int32 System.Web.ControlTraceData::RenderSize
	int32_t ___RenderSize_2;
	// System.Int32 System.Web.ControlTraceData::ViewstateSize
	int32_t ___ViewstateSize_3;
	// System.Int32 System.Web.ControlTraceData::Depth
	int32_t ___Depth_4;
	// System.Int32 System.Web.ControlTraceData::ControlstateSize
	int32_t ___ControlstateSize_5;

public:
	inline static int32_t get_offset_of_ControlId_0() { return static_cast<int32_t>(offsetof(ControlTraceData_t1335153449, ___ControlId_0)); }
	inline String_t* get_ControlId_0() const { return ___ControlId_0; }
	inline String_t** get_address_of_ControlId_0() { return &___ControlId_0; }
	inline void set_ControlId_0(String_t* value)
	{
		___ControlId_0 = value;
		Il2CppCodeGenWriteBarrier(&___ControlId_0, value);
	}

	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(ControlTraceData_t1335153449, ___Type_1)); }
	inline Type_t * get_Type_1() const { return ___Type_1; }
	inline Type_t ** get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(Type_t * value)
	{
		___Type_1 = value;
		Il2CppCodeGenWriteBarrier(&___Type_1, value);
	}

	inline static int32_t get_offset_of_RenderSize_2() { return static_cast<int32_t>(offsetof(ControlTraceData_t1335153449, ___RenderSize_2)); }
	inline int32_t get_RenderSize_2() const { return ___RenderSize_2; }
	inline int32_t* get_address_of_RenderSize_2() { return &___RenderSize_2; }
	inline void set_RenderSize_2(int32_t value)
	{
		___RenderSize_2 = value;
	}

	inline static int32_t get_offset_of_ViewstateSize_3() { return static_cast<int32_t>(offsetof(ControlTraceData_t1335153449, ___ViewstateSize_3)); }
	inline int32_t get_ViewstateSize_3() const { return ___ViewstateSize_3; }
	inline int32_t* get_address_of_ViewstateSize_3() { return &___ViewstateSize_3; }
	inline void set_ViewstateSize_3(int32_t value)
	{
		___ViewstateSize_3 = value;
	}

	inline static int32_t get_offset_of_Depth_4() { return static_cast<int32_t>(offsetof(ControlTraceData_t1335153449, ___Depth_4)); }
	inline int32_t get_Depth_4() const { return ___Depth_4; }
	inline int32_t* get_address_of_Depth_4() { return &___Depth_4; }
	inline void set_Depth_4(int32_t value)
	{
		___Depth_4 = value;
	}

	inline static int32_t get_offset_of_ControlstateSize_5() { return static_cast<int32_t>(offsetof(ControlTraceData_t1335153449, ___ControlstateSize_5)); }
	inline int32_t get_ControlstateSize_5() const { return ___ControlstateSize_5; }
	inline int32_t* get_address_of_ControlstateSize_5() { return &___ControlstateSize_5; }
	inline void set_ControlstateSize_5(int32_t value)
	{
		___ControlstateSize_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
