﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_System_ServiceModel_TcpClientC3522739476.h"
#include "System_System_Net_Security_ProtectionLevel2559578148.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.TcpTransportSecurity
struct  TcpTransportSecurity_t3663278697  : public Il2CppObject
{
public:
	// System.ServiceModel.TcpClientCredentialType System.ServiceModel.TcpTransportSecurity::client
	int32_t ___client_0;
	// System.Net.Security.ProtectionLevel System.ServiceModel.TcpTransportSecurity::protection_level
	int32_t ___protection_level_1;

public:
	inline static int32_t get_offset_of_client_0() { return static_cast<int32_t>(offsetof(TcpTransportSecurity_t3663278697, ___client_0)); }
	inline int32_t get_client_0() const { return ___client_0; }
	inline int32_t* get_address_of_client_0() { return &___client_0; }
	inline void set_client_0(int32_t value)
	{
		___client_0 = value;
	}

	inline static int32_t get_offset_of_protection_level_1() { return static_cast<int32_t>(offsetof(TcpTransportSecurity_t3663278697, ___protection_level_1)); }
	inline int32_t get_protection_level_1() const { return ___protection_level_1; }
	inline int32_t* get_address_of_protection_level_1() { return &___protection_level_1; }
	inline void set_protection_level_1(int32_t value)
	{
		___protection_level_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
