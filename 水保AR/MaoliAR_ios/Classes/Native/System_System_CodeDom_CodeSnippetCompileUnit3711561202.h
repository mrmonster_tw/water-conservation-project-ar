﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeCompileUnit2527618915.h"

// System.CodeDom.CodeLinePragma
struct CodeLinePragma_t3085002198;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeSnippetCompileUnit
struct  CodeSnippetCompileUnit_t3711561202  : public CodeCompileUnit_t2527618915
{
public:
	// System.CodeDom.CodeLinePragma System.CodeDom.CodeSnippetCompileUnit::linePragma
	CodeLinePragma_t3085002198 * ___linePragma_6;
	// System.String System.CodeDom.CodeSnippetCompileUnit::value
	String_t* ___value_7;

public:
	inline static int32_t get_offset_of_linePragma_6() { return static_cast<int32_t>(offsetof(CodeSnippetCompileUnit_t3711561202, ___linePragma_6)); }
	inline CodeLinePragma_t3085002198 * get_linePragma_6() const { return ___linePragma_6; }
	inline CodeLinePragma_t3085002198 ** get_address_of_linePragma_6() { return &___linePragma_6; }
	inline void set_linePragma_6(CodeLinePragma_t3085002198 * value)
	{
		___linePragma_6 = value;
		Il2CppCodeGenWriteBarrier(&___linePragma_6, value);
	}

	inline static int32_t get_offset_of_value_7() { return static_cast<int32_t>(offsetof(CodeSnippetCompileUnit_t3711561202, ___value_7)); }
	inline String_t* get_value_7() const { return ___value_7; }
	inline String_t** get_address_of_value_7() { return &___value_7; }
	inline void set_value_7(String_t* value)
	{
		___value_7 = value;
		Il2CppCodeGenWriteBarrier(&___value_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
