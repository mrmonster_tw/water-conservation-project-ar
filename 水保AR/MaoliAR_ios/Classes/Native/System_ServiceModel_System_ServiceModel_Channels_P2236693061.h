﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Generic.List`1<System.ServiceModel.PeerNodeAddress>
struct List_1_t3570102114;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t1333520283;
// System.ServiceModel.Channels.PeerDuplexChannel
struct PeerDuplexChannel_t1758525;
// System.Action`1<System.ServiceModel.PeerResolvers.WelcomeInfo>
struct Action_1_t1222198172;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.PeerDuplexChannel/LocalPeerReceiver
struct  LocalPeerReceiver_t2236693061  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.ServiceModel.PeerNodeAddress> System.ServiceModel.Channels.PeerDuplexChannel/LocalPeerReceiver::connections
	List_1_t3570102114 * ___connections_0;
	// System.Threading.AutoResetEvent System.ServiceModel.Channels.PeerDuplexChannel/LocalPeerReceiver::connect_handle
	AutoResetEvent_t1333520283 * ___connect_handle_1;
	// System.ServiceModel.Channels.PeerDuplexChannel System.ServiceModel.Channels.PeerDuplexChannel/LocalPeerReceiver::owner
	PeerDuplexChannel_t1758525 * ___owner_2;
	// System.Action`1<System.ServiceModel.PeerResolvers.WelcomeInfo> System.ServiceModel.Channels.PeerDuplexChannel/LocalPeerReceiver::WelcomeReceived
	Action_1_t1222198172 * ___WelcomeReceived_3;

public:
	inline static int32_t get_offset_of_connections_0() { return static_cast<int32_t>(offsetof(LocalPeerReceiver_t2236693061, ___connections_0)); }
	inline List_1_t3570102114 * get_connections_0() const { return ___connections_0; }
	inline List_1_t3570102114 ** get_address_of_connections_0() { return &___connections_0; }
	inline void set_connections_0(List_1_t3570102114 * value)
	{
		___connections_0 = value;
		Il2CppCodeGenWriteBarrier(&___connections_0, value);
	}

	inline static int32_t get_offset_of_connect_handle_1() { return static_cast<int32_t>(offsetof(LocalPeerReceiver_t2236693061, ___connect_handle_1)); }
	inline AutoResetEvent_t1333520283 * get_connect_handle_1() const { return ___connect_handle_1; }
	inline AutoResetEvent_t1333520283 ** get_address_of_connect_handle_1() { return &___connect_handle_1; }
	inline void set_connect_handle_1(AutoResetEvent_t1333520283 * value)
	{
		___connect_handle_1 = value;
		Il2CppCodeGenWriteBarrier(&___connect_handle_1, value);
	}

	inline static int32_t get_offset_of_owner_2() { return static_cast<int32_t>(offsetof(LocalPeerReceiver_t2236693061, ___owner_2)); }
	inline PeerDuplexChannel_t1758525 * get_owner_2() const { return ___owner_2; }
	inline PeerDuplexChannel_t1758525 ** get_address_of_owner_2() { return &___owner_2; }
	inline void set_owner_2(PeerDuplexChannel_t1758525 * value)
	{
		___owner_2 = value;
		Il2CppCodeGenWriteBarrier(&___owner_2, value);
	}

	inline static int32_t get_offset_of_WelcomeReceived_3() { return static_cast<int32_t>(offsetof(LocalPeerReceiver_t2236693061, ___WelcomeReceived_3)); }
	inline Action_1_t1222198172 * get_WelcomeReceived_3() const { return ___WelcomeReceived_3; }
	inline Action_1_t1222198172 ** get_address_of_WelcomeReceived_3() { return &___WelcomeReceived_3; }
	inline void set_WelcomeReceived_3(Action_1_t1222198172 * value)
	{
		___WelcomeReceived_3 = value;
		Il2CppCodeGenWriteBarrier(&___WelcomeReceived_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
