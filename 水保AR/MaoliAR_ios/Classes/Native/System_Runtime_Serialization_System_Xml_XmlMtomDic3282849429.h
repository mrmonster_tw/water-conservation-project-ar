﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Runtime_Serialization_System_Xml_XmlDiction1044334689.h"

// System.IO.Stream
struct Stream_t1273022909;
// System.Text.Encoding
struct Encoding_t1523322056;
// System.Text.Encoding[]
struct EncodingU5BU5D_t670400281;
// System.Net.Mime.ContentType
struct ContentType_t768484892;
// System.Xml.XmlDictionaryReaderQuotas
struct XmlDictionaryReaderQuotas_t173030297;
// System.Xml.OnXmlDictionaryReaderClose
struct OnXmlDictionaryReaderClose_t3234185550;
// System.Collections.Generic.Dictionary`2<System.String,System.Xml.MimeEncodedStream>
struct Dictionary_2_t407628119;
// System.Xml.XmlReader
struct XmlReader_t3121518892;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlMtomDictionaryReader
struct  XmlMtomDictionaryReader_t3282849429  : public XmlDictionaryReader_t1044334689
{
public:
	// System.IO.Stream System.Xml.XmlMtomDictionaryReader::stream
	Stream_t1273022909 * ___stream_6;
	// System.Text.Encoding System.Xml.XmlMtomDictionaryReader::encoding
	Encoding_t1523322056 * ___encoding_7;
	// System.Text.Encoding[] System.Xml.XmlMtomDictionaryReader::encodings
	EncodingU5BU5D_t670400281* ___encodings_8;
	// System.Net.Mime.ContentType System.Xml.XmlMtomDictionaryReader::content_type
	ContentType_t768484892 * ___content_type_9;
	// System.Xml.XmlDictionaryReaderQuotas System.Xml.XmlMtomDictionaryReader::quotas
	XmlDictionaryReaderQuotas_t173030297 * ___quotas_10;
	// System.Xml.OnXmlDictionaryReaderClose System.Xml.XmlMtomDictionaryReader::on_close
	OnXmlDictionaryReaderClose_t3234185550 * ___on_close_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.Xml.MimeEncodedStream> System.Xml.XmlMtomDictionaryReader::readers
	Dictionary_2_t407628119 * ___readers_12;
	// System.Xml.XmlReader System.Xml.XmlMtomDictionaryReader::xml_reader
	XmlReader_t3121518892 * ___xml_reader_13;
	// System.Xml.XmlReader System.Xml.XmlMtomDictionaryReader::initial_reader
	XmlReader_t3121518892 * ___initial_reader_14;
	// System.Xml.XmlReader System.Xml.XmlMtomDictionaryReader::eof_reader
	XmlReader_t3121518892 * ___eof_reader_15;
	// System.Xml.XmlReader System.Xml.XmlMtomDictionaryReader::part_reader
	XmlReader_t3121518892 * ___part_reader_16;
	// System.Byte[] System.Xml.XmlMtomDictionaryReader::buffer
	ByteU5BU5D_t4116647657* ___buffer_17;
	// System.Int32 System.Xml.XmlMtomDictionaryReader::peek_char
	int32_t ___peek_char_18;
	// System.Net.Mime.ContentType System.Xml.XmlMtomDictionaryReader::current_content_type
	ContentType_t768484892 * ___current_content_type_19;
	// System.String System.Xml.XmlMtomDictionaryReader::current_content_id
	String_t* ___current_content_id_20;
	// System.String System.Xml.XmlMtomDictionaryReader::current_content_encoding
	String_t* ___current_content_encoding_21;

public:
	inline static int32_t get_offset_of_stream_6() { return static_cast<int32_t>(offsetof(XmlMtomDictionaryReader_t3282849429, ___stream_6)); }
	inline Stream_t1273022909 * get_stream_6() const { return ___stream_6; }
	inline Stream_t1273022909 ** get_address_of_stream_6() { return &___stream_6; }
	inline void set_stream_6(Stream_t1273022909 * value)
	{
		___stream_6 = value;
		Il2CppCodeGenWriteBarrier(&___stream_6, value);
	}

	inline static int32_t get_offset_of_encoding_7() { return static_cast<int32_t>(offsetof(XmlMtomDictionaryReader_t3282849429, ___encoding_7)); }
	inline Encoding_t1523322056 * get_encoding_7() const { return ___encoding_7; }
	inline Encoding_t1523322056 ** get_address_of_encoding_7() { return &___encoding_7; }
	inline void set_encoding_7(Encoding_t1523322056 * value)
	{
		___encoding_7 = value;
		Il2CppCodeGenWriteBarrier(&___encoding_7, value);
	}

	inline static int32_t get_offset_of_encodings_8() { return static_cast<int32_t>(offsetof(XmlMtomDictionaryReader_t3282849429, ___encodings_8)); }
	inline EncodingU5BU5D_t670400281* get_encodings_8() const { return ___encodings_8; }
	inline EncodingU5BU5D_t670400281** get_address_of_encodings_8() { return &___encodings_8; }
	inline void set_encodings_8(EncodingU5BU5D_t670400281* value)
	{
		___encodings_8 = value;
		Il2CppCodeGenWriteBarrier(&___encodings_8, value);
	}

	inline static int32_t get_offset_of_content_type_9() { return static_cast<int32_t>(offsetof(XmlMtomDictionaryReader_t3282849429, ___content_type_9)); }
	inline ContentType_t768484892 * get_content_type_9() const { return ___content_type_9; }
	inline ContentType_t768484892 ** get_address_of_content_type_9() { return &___content_type_9; }
	inline void set_content_type_9(ContentType_t768484892 * value)
	{
		___content_type_9 = value;
		Il2CppCodeGenWriteBarrier(&___content_type_9, value);
	}

	inline static int32_t get_offset_of_quotas_10() { return static_cast<int32_t>(offsetof(XmlMtomDictionaryReader_t3282849429, ___quotas_10)); }
	inline XmlDictionaryReaderQuotas_t173030297 * get_quotas_10() const { return ___quotas_10; }
	inline XmlDictionaryReaderQuotas_t173030297 ** get_address_of_quotas_10() { return &___quotas_10; }
	inline void set_quotas_10(XmlDictionaryReaderQuotas_t173030297 * value)
	{
		___quotas_10 = value;
		Il2CppCodeGenWriteBarrier(&___quotas_10, value);
	}

	inline static int32_t get_offset_of_on_close_11() { return static_cast<int32_t>(offsetof(XmlMtomDictionaryReader_t3282849429, ___on_close_11)); }
	inline OnXmlDictionaryReaderClose_t3234185550 * get_on_close_11() const { return ___on_close_11; }
	inline OnXmlDictionaryReaderClose_t3234185550 ** get_address_of_on_close_11() { return &___on_close_11; }
	inline void set_on_close_11(OnXmlDictionaryReaderClose_t3234185550 * value)
	{
		___on_close_11 = value;
		Il2CppCodeGenWriteBarrier(&___on_close_11, value);
	}

	inline static int32_t get_offset_of_readers_12() { return static_cast<int32_t>(offsetof(XmlMtomDictionaryReader_t3282849429, ___readers_12)); }
	inline Dictionary_2_t407628119 * get_readers_12() const { return ___readers_12; }
	inline Dictionary_2_t407628119 ** get_address_of_readers_12() { return &___readers_12; }
	inline void set_readers_12(Dictionary_2_t407628119 * value)
	{
		___readers_12 = value;
		Il2CppCodeGenWriteBarrier(&___readers_12, value);
	}

	inline static int32_t get_offset_of_xml_reader_13() { return static_cast<int32_t>(offsetof(XmlMtomDictionaryReader_t3282849429, ___xml_reader_13)); }
	inline XmlReader_t3121518892 * get_xml_reader_13() const { return ___xml_reader_13; }
	inline XmlReader_t3121518892 ** get_address_of_xml_reader_13() { return &___xml_reader_13; }
	inline void set_xml_reader_13(XmlReader_t3121518892 * value)
	{
		___xml_reader_13 = value;
		Il2CppCodeGenWriteBarrier(&___xml_reader_13, value);
	}

	inline static int32_t get_offset_of_initial_reader_14() { return static_cast<int32_t>(offsetof(XmlMtomDictionaryReader_t3282849429, ___initial_reader_14)); }
	inline XmlReader_t3121518892 * get_initial_reader_14() const { return ___initial_reader_14; }
	inline XmlReader_t3121518892 ** get_address_of_initial_reader_14() { return &___initial_reader_14; }
	inline void set_initial_reader_14(XmlReader_t3121518892 * value)
	{
		___initial_reader_14 = value;
		Il2CppCodeGenWriteBarrier(&___initial_reader_14, value);
	}

	inline static int32_t get_offset_of_eof_reader_15() { return static_cast<int32_t>(offsetof(XmlMtomDictionaryReader_t3282849429, ___eof_reader_15)); }
	inline XmlReader_t3121518892 * get_eof_reader_15() const { return ___eof_reader_15; }
	inline XmlReader_t3121518892 ** get_address_of_eof_reader_15() { return &___eof_reader_15; }
	inline void set_eof_reader_15(XmlReader_t3121518892 * value)
	{
		___eof_reader_15 = value;
		Il2CppCodeGenWriteBarrier(&___eof_reader_15, value);
	}

	inline static int32_t get_offset_of_part_reader_16() { return static_cast<int32_t>(offsetof(XmlMtomDictionaryReader_t3282849429, ___part_reader_16)); }
	inline XmlReader_t3121518892 * get_part_reader_16() const { return ___part_reader_16; }
	inline XmlReader_t3121518892 ** get_address_of_part_reader_16() { return &___part_reader_16; }
	inline void set_part_reader_16(XmlReader_t3121518892 * value)
	{
		___part_reader_16 = value;
		Il2CppCodeGenWriteBarrier(&___part_reader_16, value);
	}

	inline static int32_t get_offset_of_buffer_17() { return static_cast<int32_t>(offsetof(XmlMtomDictionaryReader_t3282849429, ___buffer_17)); }
	inline ByteU5BU5D_t4116647657* get_buffer_17() const { return ___buffer_17; }
	inline ByteU5BU5D_t4116647657** get_address_of_buffer_17() { return &___buffer_17; }
	inline void set_buffer_17(ByteU5BU5D_t4116647657* value)
	{
		___buffer_17 = value;
		Il2CppCodeGenWriteBarrier(&___buffer_17, value);
	}

	inline static int32_t get_offset_of_peek_char_18() { return static_cast<int32_t>(offsetof(XmlMtomDictionaryReader_t3282849429, ___peek_char_18)); }
	inline int32_t get_peek_char_18() const { return ___peek_char_18; }
	inline int32_t* get_address_of_peek_char_18() { return &___peek_char_18; }
	inline void set_peek_char_18(int32_t value)
	{
		___peek_char_18 = value;
	}

	inline static int32_t get_offset_of_current_content_type_19() { return static_cast<int32_t>(offsetof(XmlMtomDictionaryReader_t3282849429, ___current_content_type_19)); }
	inline ContentType_t768484892 * get_current_content_type_19() const { return ___current_content_type_19; }
	inline ContentType_t768484892 ** get_address_of_current_content_type_19() { return &___current_content_type_19; }
	inline void set_current_content_type_19(ContentType_t768484892 * value)
	{
		___current_content_type_19 = value;
		Il2CppCodeGenWriteBarrier(&___current_content_type_19, value);
	}

	inline static int32_t get_offset_of_current_content_id_20() { return static_cast<int32_t>(offsetof(XmlMtomDictionaryReader_t3282849429, ___current_content_id_20)); }
	inline String_t* get_current_content_id_20() const { return ___current_content_id_20; }
	inline String_t** get_address_of_current_content_id_20() { return &___current_content_id_20; }
	inline void set_current_content_id_20(String_t* value)
	{
		___current_content_id_20 = value;
		Il2CppCodeGenWriteBarrier(&___current_content_id_20, value);
	}

	inline static int32_t get_offset_of_current_content_encoding_21() { return static_cast<int32_t>(offsetof(XmlMtomDictionaryReader_t3282849429, ___current_content_encoding_21)); }
	inline String_t* get_current_content_encoding_21() const { return ___current_content_encoding_21; }
	inline String_t** get_address_of_current_content_encoding_21() { return &___current_content_encoding_21; }
	inline void set_current_content_encoding_21(String_t* value)
	{
		___current_content_encoding_21 = value;
		Il2CppCodeGenWriteBarrier(&___current_content_encoding_21, value);
	}
};

struct XmlMtomDictionaryReader_t3282849429_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlMtomDictionaryReader::<>f__switch$map8
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map8_22;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map8_22() { return static_cast<int32_t>(offsetof(XmlMtomDictionaryReader_t3282849429_StaticFields, ___U3CU3Ef__switchU24map8_22)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map8_22() const { return ___U3CU3Ef__switchU24map8_22; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map8_22() { return &___U3CU3Ef__switchU24map8_22; }
	inline void set_U3CU3Ef__switchU24map8_22(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map8_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map8_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
