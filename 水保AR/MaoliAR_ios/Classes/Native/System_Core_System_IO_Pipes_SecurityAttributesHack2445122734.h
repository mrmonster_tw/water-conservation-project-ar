﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"
#include "mscorlib_System_IntPtr840150181.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Pipes.SecurityAttributesHack
struct  SecurityAttributesHack_t2445122734 
{
public:
	// System.Int32 System.IO.Pipes.SecurityAttributesHack::Length
	int32_t ___Length_0;
	// System.IntPtr System.IO.Pipes.SecurityAttributesHack::SecurityDescriptor
	IntPtr_t ___SecurityDescriptor_1;
	// System.Boolean System.IO.Pipes.SecurityAttributesHack::Inheritable
	bool ___Inheritable_2;

public:
	inline static int32_t get_offset_of_Length_0() { return static_cast<int32_t>(offsetof(SecurityAttributesHack_t2445122734, ___Length_0)); }
	inline int32_t get_Length_0() const { return ___Length_0; }
	inline int32_t* get_address_of_Length_0() { return &___Length_0; }
	inline void set_Length_0(int32_t value)
	{
		___Length_0 = value;
	}

	inline static int32_t get_offset_of_SecurityDescriptor_1() { return static_cast<int32_t>(offsetof(SecurityAttributesHack_t2445122734, ___SecurityDescriptor_1)); }
	inline IntPtr_t get_SecurityDescriptor_1() const { return ___SecurityDescriptor_1; }
	inline IntPtr_t* get_address_of_SecurityDescriptor_1() { return &___SecurityDescriptor_1; }
	inline void set_SecurityDescriptor_1(IntPtr_t value)
	{
		___SecurityDescriptor_1 = value;
	}

	inline static int32_t get_offset_of_Inheritable_2() { return static_cast<int32_t>(offsetof(SecurityAttributesHack_t2445122734, ___Inheritable_2)); }
	inline bool get_Inheritable_2() const { return ___Inheritable_2; }
	inline bool* get_address_of_Inheritable_2() { return &___Inheritable_2; }
	inline void set_Inheritable_2(bool value)
	{
		___Inheritable_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.IO.Pipes.SecurityAttributesHack
struct SecurityAttributesHack_t2445122734_marshaled_pinvoke
{
	int32_t ___Length_0;
	intptr_t ___SecurityDescriptor_1;
	int32_t ___Inheritable_2;
};
// Native definition for COM marshalling of System.IO.Pipes.SecurityAttributesHack
struct SecurityAttributesHack_t2445122734_marshaled_com
{
	int32_t ___Length_0;
	intptr_t ___SecurityDescriptor_1;
	int32_t ___Inheritable_2;
};
