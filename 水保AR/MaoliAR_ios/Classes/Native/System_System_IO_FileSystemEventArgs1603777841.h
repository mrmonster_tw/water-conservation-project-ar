﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_EventArgs3591816995.h"
#include "System_System_IO_WatcherChangeTypes673177441.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileSystemEventArgs
struct  FileSystemEventArgs_t1603777841  : public EventArgs_t3591816995
{
public:
	// System.IO.WatcherChangeTypes System.IO.FileSystemEventArgs::changeType
	int32_t ___changeType_1;
	// System.String System.IO.FileSystemEventArgs::directory
	String_t* ___directory_2;
	// System.String System.IO.FileSystemEventArgs::name
	String_t* ___name_3;

public:
	inline static int32_t get_offset_of_changeType_1() { return static_cast<int32_t>(offsetof(FileSystemEventArgs_t1603777841, ___changeType_1)); }
	inline int32_t get_changeType_1() const { return ___changeType_1; }
	inline int32_t* get_address_of_changeType_1() { return &___changeType_1; }
	inline void set_changeType_1(int32_t value)
	{
		___changeType_1 = value;
	}

	inline static int32_t get_offset_of_directory_2() { return static_cast<int32_t>(offsetof(FileSystemEventArgs_t1603777841, ___directory_2)); }
	inline String_t* get_directory_2() const { return ___directory_2; }
	inline String_t** get_address_of_directory_2() { return &___directory_2; }
	inline void set_directory_2(String_t* value)
	{
		___directory_2 = value;
		Il2CppCodeGenWriteBarrier(&___directory_2, value);
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(FileSystemEventArgs_t1603777841, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier(&___name_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
