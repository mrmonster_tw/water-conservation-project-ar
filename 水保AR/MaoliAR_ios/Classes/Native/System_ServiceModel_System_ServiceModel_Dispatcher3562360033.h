﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.ServiceHostBase
struct ServiceHostBase_t3741910535;
// System.Collections.Generic.Dictionary`2<System.Xml.UniqueId,System.ServiceModel.InstanceContext>
struct Dictionary_2_t3073904349;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.SessionInstanceContextProvider
struct  SessionInstanceContextProvider_t3562360033  : public Il2CppObject
{
public:
	// System.ServiceModel.ServiceHostBase System.ServiceModel.Dispatcher.SessionInstanceContextProvider::host
	ServiceHostBase_t3741910535 * ___host_0;
	// System.Collections.Generic.Dictionary`2<System.Xml.UniqueId,System.ServiceModel.InstanceContext> System.ServiceModel.Dispatcher.SessionInstanceContextProvider::pool
	Dictionary_2_t3073904349 * ___pool_1;

public:
	inline static int32_t get_offset_of_host_0() { return static_cast<int32_t>(offsetof(SessionInstanceContextProvider_t3562360033, ___host_0)); }
	inline ServiceHostBase_t3741910535 * get_host_0() const { return ___host_0; }
	inline ServiceHostBase_t3741910535 ** get_address_of_host_0() { return &___host_0; }
	inline void set_host_0(ServiceHostBase_t3741910535 * value)
	{
		___host_0 = value;
		Il2CppCodeGenWriteBarrier(&___host_0, value);
	}

	inline static int32_t get_offset_of_pool_1() { return static_cast<int32_t>(offsetof(SessionInstanceContextProvider_t3562360033, ___pool_1)); }
	inline Dictionary_2_t3073904349 * get_pool_1() const { return ___pool_1; }
	inline Dictionary_2_t3073904349 ** get_address_of_pool_1() { return &___pool_1; }
	inline void set_pool_1(Dictionary_2_t3073904349 * value)
	{
		___pool_1 = value;
		Il2CppCodeGenWriteBarrier(&___pool_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
