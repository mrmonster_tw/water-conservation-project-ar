﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_MarshalByRefObject2760389100.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Hosting.VirtualFileBase
struct  VirtualFileBase_t3533200965  : public MarshalByRefObject_t2760389100
{
public:
	// System.String System.Web.Hosting.VirtualFileBase::vpath
	String_t* ___vpath_1;

public:
	inline static int32_t get_offset_of_vpath_1() { return static_cast<int32_t>(offsetof(VirtualFileBase_t3533200965, ___vpath_1)); }
	inline String_t* get_vpath_1() const { return ___vpath_1; }
	inline String_t** get_address_of_vpath_1() { return &___vpath_1; }
	inline void set_vpath_1(String_t* value)
	{
		___vpath_1 = value;
		Il2CppCodeGenWriteBarrier(&___vpath_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
