﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Cha19627360.h"

// System.ServiceModel.Channels.IChannelFactory`1<System.Object>
struct IChannelFactory_1_t139447917;
// System.Transactions.TransactionScope
struct TransactionScope_t3249669472;
// System.ServiceModel.TransactionProtocol
struct TransactionProtocol_t3972232485;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.TransactionChannelFactory`1<System.Object>
struct  TransactionChannelFactory_1_t886990376  : public ChannelFactoryBase_1_t19627360
{
public:
	// System.ServiceModel.Channels.IChannelFactory`1<TChannel> System.ServiceModel.Channels.TransactionChannelFactory`1::inner_factory
	Il2CppObject* ___inner_factory_14;
	// System.Transactions.TransactionScope System.ServiceModel.Channels.TransactionChannelFactory`1::txscope
	TransactionScope_t3249669472 * ___txscope_15;
	// System.ServiceModel.TransactionProtocol System.ServiceModel.Channels.TransactionChannelFactory`1::protocol
	TransactionProtocol_t3972232485 * ___protocol_16;

public:
	inline static int32_t get_offset_of_inner_factory_14() { return static_cast<int32_t>(offsetof(TransactionChannelFactory_1_t886990376, ___inner_factory_14)); }
	inline Il2CppObject* get_inner_factory_14() const { return ___inner_factory_14; }
	inline Il2CppObject** get_address_of_inner_factory_14() { return &___inner_factory_14; }
	inline void set_inner_factory_14(Il2CppObject* value)
	{
		___inner_factory_14 = value;
		Il2CppCodeGenWriteBarrier(&___inner_factory_14, value);
	}

	inline static int32_t get_offset_of_txscope_15() { return static_cast<int32_t>(offsetof(TransactionChannelFactory_1_t886990376, ___txscope_15)); }
	inline TransactionScope_t3249669472 * get_txscope_15() const { return ___txscope_15; }
	inline TransactionScope_t3249669472 ** get_address_of_txscope_15() { return &___txscope_15; }
	inline void set_txscope_15(TransactionScope_t3249669472 * value)
	{
		___txscope_15 = value;
		Il2CppCodeGenWriteBarrier(&___txscope_15, value);
	}

	inline static int32_t get_offset_of_protocol_16() { return static_cast<int32_t>(offsetof(TransactionChannelFactory_1_t886990376, ___protocol_16)); }
	inline TransactionProtocol_t3972232485 * get_protocol_16() const { return ___protocol_16; }
	inline TransactionProtocol_t3972232485 ** get_address_of_protocol_16() { return &___protocol_16; }
	inline void set_protocol_16(TransactionProtocol_t3972232485 * value)
	{
		___protocol_16 = value;
		Il2CppCodeGenWriteBarrier(&___protocol_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
