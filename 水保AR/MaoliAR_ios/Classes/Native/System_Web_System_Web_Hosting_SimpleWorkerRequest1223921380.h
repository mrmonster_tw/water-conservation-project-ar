﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_HttpWorkerRequest998871775.h"

// System.String
struct String_t;
// System.IO.TextWriter
struct TextWriter_t3478189236;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Hosting.SimpleWorkerRequest
struct  SimpleWorkerRequest_t1223921380  : public HttpWorkerRequest_t998871775
{
public:
	// System.String System.Web.Hosting.SimpleWorkerRequest::page
	String_t* ___page_3;
	// System.String System.Web.Hosting.SimpleWorkerRequest::query
	String_t* ___query_4;
	// System.String System.Web.Hosting.SimpleWorkerRequest::app_virtual_dir
	String_t* ___app_virtual_dir_5;
	// System.String System.Web.Hosting.SimpleWorkerRequest::app_physical_dir
	String_t* ___app_physical_dir_6;
	// System.String System.Web.Hosting.SimpleWorkerRequest::path_info
	String_t* ___path_info_7;
	// System.IO.TextWriter System.Web.Hosting.SimpleWorkerRequest::output
	TextWriter_t3478189236 * ___output_8;
	// System.Boolean System.Web.Hosting.SimpleWorkerRequest::hosted
	bool ___hosted_9;

public:
	inline static int32_t get_offset_of_page_3() { return static_cast<int32_t>(offsetof(SimpleWorkerRequest_t1223921380, ___page_3)); }
	inline String_t* get_page_3() const { return ___page_3; }
	inline String_t** get_address_of_page_3() { return &___page_3; }
	inline void set_page_3(String_t* value)
	{
		___page_3 = value;
		Il2CppCodeGenWriteBarrier(&___page_3, value);
	}

	inline static int32_t get_offset_of_query_4() { return static_cast<int32_t>(offsetof(SimpleWorkerRequest_t1223921380, ___query_4)); }
	inline String_t* get_query_4() const { return ___query_4; }
	inline String_t** get_address_of_query_4() { return &___query_4; }
	inline void set_query_4(String_t* value)
	{
		___query_4 = value;
		Il2CppCodeGenWriteBarrier(&___query_4, value);
	}

	inline static int32_t get_offset_of_app_virtual_dir_5() { return static_cast<int32_t>(offsetof(SimpleWorkerRequest_t1223921380, ___app_virtual_dir_5)); }
	inline String_t* get_app_virtual_dir_5() const { return ___app_virtual_dir_5; }
	inline String_t** get_address_of_app_virtual_dir_5() { return &___app_virtual_dir_5; }
	inline void set_app_virtual_dir_5(String_t* value)
	{
		___app_virtual_dir_5 = value;
		Il2CppCodeGenWriteBarrier(&___app_virtual_dir_5, value);
	}

	inline static int32_t get_offset_of_app_physical_dir_6() { return static_cast<int32_t>(offsetof(SimpleWorkerRequest_t1223921380, ___app_physical_dir_6)); }
	inline String_t* get_app_physical_dir_6() const { return ___app_physical_dir_6; }
	inline String_t** get_address_of_app_physical_dir_6() { return &___app_physical_dir_6; }
	inline void set_app_physical_dir_6(String_t* value)
	{
		___app_physical_dir_6 = value;
		Il2CppCodeGenWriteBarrier(&___app_physical_dir_6, value);
	}

	inline static int32_t get_offset_of_path_info_7() { return static_cast<int32_t>(offsetof(SimpleWorkerRequest_t1223921380, ___path_info_7)); }
	inline String_t* get_path_info_7() const { return ___path_info_7; }
	inline String_t** get_address_of_path_info_7() { return &___path_info_7; }
	inline void set_path_info_7(String_t* value)
	{
		___path_info_7 = value;
		Il2CppCodeGenWriteBarrier(&___path_info_7, value);
	}

	inline static int32_t get_offset_of_output_8() { return static_cast<int32_t>(offsetof(SimpleWorkerRequest_t1223921380, ___output_8)); }
	inline TextWriter_t3478189236 * get_output_8() const { return ___output_8; }
	inline TextWriter_t3478189236 ** get_address_of_output_8() { return &___output_8; }
	inline void set_output_8(TextWriter_t3478189236 * value)
	{
		___output_8 = value;
		Il2CppCodeGenWriteBarrier(&___output_8, value);
	}

	inline static int32_t get_offset_of_hosted_9() { return static_cast<int32_t>(offsetof(SimpleWorkerRequest_t1223921380, ___hosted_9)); }
	inline bool get_hosted_9() const { return ___hosted_9; }
	inline bool* get_address_of_hosted_9() { return &___hosted_9; }
	inline void set_hosted_9(bool value)
	{
		___hosted_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
