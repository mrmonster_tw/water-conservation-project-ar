﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Type
struct Type_t;
// System.CodeDom.Compiler.CompilerParameters
struct CompilerParameters_t2325000967;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.CompilerType
struct  CompilerType_t2533482247  : public Il2CppObject
{
public:
	// System.Type System.Web.Compilation.CompilerType::type
	Type_t * ___type_0;
	// System.CodeDom.Compiler.CompilerParameters System.Web.Compilation.CompilerType::parameters
	CompilerParameters_t2325000967 * ___parameters_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(CompilerType_t2533482247, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier(&___type_0, value);
	}

	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(CompilerType_t2533482247, ___parameters_1)); }
	inline CompilerParameters_t2325000967 * get_parameters_1() const { return ___parameters_1; }
	inline CompilerParameters_t2325000967 ** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(CompilerParameters_t2325000967 * value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
