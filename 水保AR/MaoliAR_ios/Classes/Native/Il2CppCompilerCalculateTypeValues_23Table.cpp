﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_ComponentModel_CollectionConverter3078846443.h"
#include "System_System_ComponentModel_ComponentCollection1445440302.h"
#include "System_System_ComponentModel_ComponentConverter118941221.h"
#include "System_System_ComponentModel_Component3620823400.h"
#include "System_System_ComponentModel_CultureInfoConverter81550636.h"
#include "System_System_ComponentModel_CultureInfoConverter_1184699714.h"
#include "System_System_ComponentModel_CustomTypeDescriptor3093649079.h"
#include "System_System_ComponentModel_DateTimeConverter2298802450.h"
#include "System_System_ComponentModel_DecimalConverter3574141665.h"
#include "System_System_ComponentModel_DefaultEventAttribute3124666540.h"
#include "System_System_ComponentModel_DefaultPropertyAttrib1952442862.h"
#include "System_System_ComponentModel_DefaultValueAttribute587583663.h"
#include "System_System_ComponentModel_DescriptionAttribute874390736.h"
#include "System_System_ComponentModel_DesignerAttribute2079716647.h"
#include "System_System_ComponentModel_DesignerCategoryAttri2912925731.h"
#include "System_System_ComponentModel_DesignerSerialization4084246596.h"
#include "System_System_ComponentModel_DesignerSerialization3481291396.h"
#include "System_System_ComponentModel_Design_Serialization_1570548024.h"
#include "System_System_ComponentModel_Design_Serialization_I657473484.h"
#include "System_System_ComponentModel_Design_Serialization_3074689342.h"
#include "System_System_ComponentModel_DoubleConverter805142290.h"
#include "System_System_ComponentModel_EditorAttribute1332199665.h"
#include "System_System_ComponentModel_EditorBrowsableAttrib1475454531.h"
#include "System_System_ComponentModel_EditorBrowsableState2839071299.h"
#include "System_System_ComponentModel_EnumConverter1688858217.h"
#include "System_System_ComponentModel_EnumConverter_EnumComp557155003.h"
#include "System_System_ComponentModel_ListEntry1182276877.h"
#include "System_System_ComponentModel_EventHandlerList1108123056.h"
#include "System_System_ComponentModel_ExpandableObjectConver420832579.h"
#include "System_System_ComponentModel_GuidConverter3396672461.h"
#include "System_System_ComponentModel_ImmutableObjectAttrib2959712128.h"
#include "System_System_ComponentModel_InstallerTypeAttribut3233088727.h"
#include "System_System_ComponentModel_Int16Converter1119562914.h"
#include "System_System_ComponentModel_Int32Converter677227065.h"
#include "System_System_ComponentModel_Int64Converter1092099567.h"
#include "System_System_ComponentModel_LocalizableAttribute3478561129.h"
#include "System_System_ComponentModel_MemberDescriptor3815403747.h"
#include "System_System_ComponentModel_MergablePropertyAttrib476798118.h"
#include "System_System_ComponentModel_NotifyParentPropertyA1405421815.h"
#include "System_System_ComponentModel_NullableConverter1985728604.h"
#include "System_System_ComponentModel_ParenthesizePropertyNa878408981.h"
#include "System_System_ComponentModel_PropertyDescriptorCol4164928659.h"
#include "System_System_ComponentModel_PropertyDescriptor3244362832.h"
#include "System_System_ComponentModel_ReadOnlyAttribute1907441566.h"
#include "System_System_ComponentModel_RecommendedAsConfigura279829077.h"
#include "System_System_ComponentModel_ReferenceConverter1811933861.h"
#include "System_System_ComponentModel_ReflectionPropertyDes3529497203.h"
#include "System_System_ComponentModel_RefreshPropertiesAttr2078493172.h"
#include "System_System_ComponentModel_RefreshProperties2310254728.h"
#include "System_System_ComponentModel_SByteConverter2970182448.h"
#include "System_System_ComponentModel_SingleConverter902207630.h"
#include "System_System_ComponentModel_StringConverter3216726494.h"
#include "System_System_ComponentModel_TimeSpanConverter3504031848.h"
#include "System_System_ComponentModel_ToolboxItemAttribute243705872.h"
#include "System_System_ComponentModel_ToolboxItemFilterAttr2990581383.h"
#include "System_System_ComponentModel_ToolboxItemFilterType3870514709.h"
#include "System_System_ComponentModel_TypeConverterAttribut3271584429.h"
#include "System_System_ComponentModel_TypeConverter2249118273.h"
#include "System_System_ComponentModel_TypeConverter_Standar2184948248.h"
#include "System_System_ComponentModel_TypeDescriptionProvid3232077895.h"
#include "System_System_ComponentModel_TypeDescriptionProvid4007109994.h"
#include "System_System_ComponentModel_TypeDescriptor3066613587.h"
#include "System_System_ComponentModel_TypeDescriptor_Wrappe3047182012.h"
#include "System_System_ComponentModel_TypeDescriptor_Defaul2709269259.h"
#include "System_System_ComponentModel_TypeDescriptor_Default675274121.h"
#include "System_System_ComponentModel_Info3260944019.h"
#include "System_System_ComponentModel_ComponentInfo1259248179.h"
#include "System_System_ComponentModel_TypeInfo323385814.h"
#include "System_System_ComponentModel_UInt16Converter819459975.h"
#include "System_System_ComponentModel_UInt32Converter3472493373.h"
#include "System_System_ComponentModel_UInt64Converter4189949036.h"
#include "System_System_ComponentModel_WeakObjectWrapper827463650.h"
#include "System_System_ComponentModel_WeakObjectWrapperCompa303980402.h"
#include "System_System_ComponentModel_Win32Exception3234146298.h"
#include "System_System_Configuration_ConfigurationException3515317685.h"
#include "System_System_Configuration_ConfigurationSettings822951835.h"
#include "System_System_Configuration_DefaultConfig1013547162.h"
#include "System_System_Configuration_SettingsBase3488873952.h"
#include "System_System_DefaultUriParser95882050.h"
#include "System_System_Diagnostics_AlphabeticalEnumConverte2601987229.h"
#include "System_System_Diagnostics_CorrelationManager2688752967.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (CollectionConverter_t3078846443), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (ComponentCollection_t1445440302), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (ComponentConverter_t118941221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (Component_t3620823400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2303[3] = 
{
	Component_t3620823400::get_offset_of_event_handlers_1(),
	Component_t3620823400::get_offset_of_mySite_2(),
	Component_t3620823400::get_offset_of_disposedEvent_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (CultureInfoConverter_t81550636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2304[1] = 
{
	CultureInfoConverter_t81550636::get_offset_of__standardValues_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (CultureInfoComparer_t1184699714), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (CustomTypeDescriptor_t3093649079), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2306[1] = 
{
	CustomTypeDescriptor_t3093649079::get_offset_of__parent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (DateTimeConverter_t2298802450), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (DecimalConverter_t3574141665), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (DefaultEventAttribute_t3124666540), -1, sizeof(DefaultEventAttribute_t3124666540_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2309[2] = 
{
	DefaultEventAttribute_t3124666540::get_offset_of_eventName_0(),
	DefaultEventAttribute_t3124666540_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (DefaultPropertyAttribute_t1952442862), -1, sizeof(DefaultPropertyAttribute_t1952442862_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2310[2] = 
{
	DefaultPropertyAttribute_t1952442862::get_offset_of_property_name_0(),
	DefaultPropertyAttribute_t1952442862_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (DefaultValueAttribute_t587583663), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2311[1] = 
{
	DefaultValueAttribute_t587583663::get_offset_of_DefaultValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (DescriptionAttribute_t874390736), -1, sizeof(DescriptionAttribute_t874390736_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2312[2] = 
{
	DescriptionAttribute_t874390736::get_offset_of_desc_0(),
	DescriptionAttribute_t874390736_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (DesignerAttribute_t2079716647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2313[2] = 
{
	DesignerAttribute_t2079716647::get_offset_of_name_0(),
	DesignerAttribute_t2079716647::get_offset_of_basetypename_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (DesignerCategoryAttribute_t2912925731), -1, sizeof(DesignerCategoryAttribute_t2912925731_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2314[5] = 
{
	DesignerCategoryAttribute_t2912925731::get_offset_of_category_0(),
	DesignerCategoryAttribute_t2912925731_StaticFields::get_offset_of_Component_1(),
	DesignerCategoryAttribute_t2912925731_StaticFields::get_offset_of_Form_2(),
	DesignerCategoryAttribute_t2912925731_StaticFields::get_offset_of_Generic_3(),
	DesignerCategoryAttribute_t2912925731_StaticFields::get_offset_of_Default_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (DesignerSerializationVisibilityAttribute_t4084246596), -1, sizeof(DesignerSerializationVisibilityAttribute_t4084246596_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2315[5] = 
{
	DesignerSerializationVisibilityAttribute_t4084246596::get_offset_of_visibility_0(),
	DesignerSerializationVisibilityAttribute_t4084246596_StaticFields::get_offset_of_Default_1(),
	DesignerSerializationVisibilityAttribute_t4084246596_StaticFields::get_offset_of_Content_2(),
	DesignerSerializationVisibilityAttribute_t4084246596_StaticFields::get_offset_of_Hidden_3(),
	DesignerSerializationVisibilityAttribute_t4084246596_StaticFields::get_offset_of_Visible_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (DesignerSerializationVisibility_t3481291396)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2316[4] = 
{
	DesignerSerializationVisibility_t3481291396::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (DesignerSerializerAttribute_t1570548024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2324[2] = 
{
	DesignerSerializerAttribute_t1570548024::get_offset_of_serializerTypeName_0(),
	DesignerSerializerAttribute_t1570548024::get_offset_of_baseSerializerTypeName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (InstanceDescriptor_t657473484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2325[3] = 
{
	InstanceDescriptor_t657473484::get_offset_of_member_0(),
	InstanceDescriptor_t657473484::get_offset_of_arguments_1(),
	InstanceDescriptor_t657473484::get_offset_of_isComplete_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (RootDesignerSerializerAttribute_t3074689342), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2326[3] = 
{
	RootDesignerSerializerAttribute_t3074689342::get_offset_of_serializer_0(),
	RootDesignerSerializerAttribute_t3074689342::get_offset_of_baseserializer_1(),
	RootDesignerSerializerAttribute_t3074689342::get_offset_of_reload_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (DoubleConverter_t805142290), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (EditorAttribute_t1332199665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2328[2] = 
{
	EditorAttribute_t1332199665::get_offset_of_name_0(),
	EditorAttribute_t1332199665::get_offset_of_basename_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (EditorBrowsableAttribute_t1475454531), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2329[1] = 
{
	EditorBrowsableAttribute_t1475454531::get_offset_of_state_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (EditorBrowsableState_t2839071299)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2330[4] = 
{
	EditorBrowsableState_t2839071299::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (EnumConverter_t1688858217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2331[2] = 
{
	EnumConverter_t1688858217::get_offset_of_type_0(),
	EnumConverter_t1688858217::get_offset_of_stdValues_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (EnumComparer_t557155003), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (ListEntry_t1182276877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2333[3] = 
{
	ListEntry_t1182276877::get_offset_of_key_0(),
	ListEntry_t1182276877::get_offset_of_value_1(),
	ListEntry_t1182276877::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (EventHandlerList_t1108123056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2334[2] = 
{
	EventHandlerList_t1108123056::get_offset_of_entries_0(),
	EventHandlerList_t1108123056::get_offset_of_null_entry_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (ExpandableObjectConverter_t420832579), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (GuidConverter_t3396672461), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (ImmutableObjectAttribute_t2959712128), -1, sizeof(ImmutableObjectAttribute_t2959712128_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2341[4] = 
{
	ImmutableObjectAttribute_t2959712128::get_offset_of_immutable_0(),
	ImmutableObjectAttribute_t2959712128_StaticFields::get_offset_of_Default_1(),
	ImmutableObjectAttribute_t2959712128_StaticFields::get_offset_of_No_2(),
	ImmutableObjectAttribute_t2959712128_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (InstallerTypeAttribute_t3233088727), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2342[1] = 
{
	InstallerTypeAttribute_t3233088727::get_offset_of_installer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (Int16Converter_t1119562914), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (Int32Converter_t677227065), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (Int64Converter_t1092099567), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (LocalizableAttribute_t3478561129), -1, sizeof(LocalizableAttribute_t3478561129_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2351[4] = 
{
	LocalizableAttribute_t3478561129::get_offset_of_localizable_0(),
	LocalizableAttribute_t3478561129_StaticFields::get_offset_of_Default_1(),
	LocalizableAttribute_t3478561129_StaticFields::get_offset_of_No_2(),
	LocalizableAttribute_t3478561129_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (MemberDescriptor_t3815403747), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2352[3] = 
{
	MemberDescriptor_t3815403747::get_offset_of_name_0(),
	MemberDescriptor_t3815403747::get_offset_of_attrs_1(),
	MemberDescriptor_t3815403747::get_offset_of_attrCollection_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (MergablePropertyAttribute_t476798118), -1, sizeof(MergablePropertyAttribute_t476798118_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2353[4] = 
{
	MergablePropertyAttribute_t476798118::get_offset_of_mergable_0(),
	MergablePropertyAttribute_t476798118_StaticFields::get_offset_of_Default_1(),
	MergablePropertyAttribute_t476798118_StaticFields::get_offset_of_No_2(),
	MergablePropertyAttribute_t476798118_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (NotifyParentPropertyAttribute_t1405421815), -1, sizeof(NotifyParentPropertyAttribute_t1405421815_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2354[4] = 
{
	NotifyParentPropertyAttribute_t1405421815::get_offset_of_notifyParent_0(),
	NotifyParentPropertyAttribute_t1405421815_StaticFields::get_offset_of_Default_1(),
	NotifyParentPropertyAttribute_t1405421815_StaticFields::get_offset_of_No_2(),
	NotifyParentPropertyAttribute_t1405421815_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (NullableConverter_t1985728604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2355[3] = 
{
	NullableConverter_t1985728604::get_offset_of_nullableType_0(),
	NullableConverter_t1985728604::get_offset_of_underlyingType_1(),
	NullableConverter_t1985728604::get_offset_of_underlyingTypeConverter_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (ParenthesizePropertyNameAttribute_t878408981), -1, sizeof(ParenthesizePropertyNameAttribute_t878408981_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2356[2] = 
{
	ParenthesizePropertyNameAttribute_t878408981::get_offset_of_parenthesis_0(),
	ParenthesizePropertyNameAttribute_t878408981_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (PropertyDescriptorCollection_t4164928659), -1, sizeof(PropertyDescriptorCollection_t4164928659_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2357[3] = 
{
	PropertyDescriptorCollection_t4164928659_StaticFields::get_offset_of_Empty_0(),
	PropertyDescriptorCollection_t4164928659::get_offset_of_properties_1(),
	PropertyDescriptorCollection_t4164928659::get_offset_of_readOnly_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (PropertyDescriptor_t3244362832), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2358[1] = 
{
	PropertyDescriptor_t3244362832::get_offset_of_converter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (ReadOnlyAttribute_t1907441566), -1, sizeof(ReadOnlyAttribute_t1907441566_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2359[4] = 
{
	ReadOnlyAttribute_t1907441566::get_offset_of_read_only_0(),
	ReadOnlyAttribute_t1907441566_StaticFields::get_offset_of_No_1(),
	ReadOnlyAttribute_t1907441566_StaticFields::get_offset_of_Yes_2(),
	ReadOnlyAttribute_t1907441566_StaticFields::get_offset_of_Default_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (RecommendedAsConfigurableAttribute_t279829077), -1, sizeof(RecommendedAsConfigurableAttribute_t279829077_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2360[4] = 
{
	RecommendedAsConfigurableAttribute_t279829077::get_offset_of_recommendedAsConfigurable_0(),
	RecommendedAsConfigurableAttribute_t279829077_StaticFields::get_offset_of_Default_1(),
	RecommendedAsConfigurableAttribute_t279829077_StaticFields::get_offset_of_No_2(),
	RecommendedAsConfigurableAttribute_t279829077_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (ReferenceConverter_t1811933861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2361[1] = 
{
	ReferenceConverter_t1811933861::get_offset_of_reference_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (ReflectionPropertyDescriptor_t3529497203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2362[6] = 
{
	ReflectionPropertyDescriptor_t3529497203::get_offset_of__member_4(),
	ReflectionPropertyDescriptor_t3529497203::get_offset_of__componentType_5(),
	ReflectionPropertyDescriptor_t3529497203::get_offset_of__propertyType_6(),
	ReflectionPropertyDescriptor_t3529497203::get_offset_of_getter_7(),
	ReflectionPropertyDescriptor_t3529497203::get_offset_of_setter_8(),
	ReflectionPropertyDescriptor_t3529497203::get_offset_of_accessors_inited_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (RefreshPropertiesAttribute_t2078493172), -1, sizeof(RefreshPropertiesAttribute_t2078493172_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2363[4] = 
{
	RefreshPropertiesAttribute_t2078493172::get_offset_of_refresh_0(),
	RefreshPropertiesAttribute_t2078493172_StaticFields::get_offset_of_All_1(),
	RefreshPropertiesAttribute_t2078493172_StaticFields::get_offset_of_Default_2(),
	RefreshPropertiesAttribute_t2078493172_StaticFields::get_offset_of_Repaint_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (RefreshProperties_t2310254728)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2364[4] = 
{
	RefreshProperties_t2310254728::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (SByteConverter_t2970182448), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { sizeof (SingleConverter_t902207630), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (StringConverter_t3216726494), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (TimeSpanConverter_t3504031848), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (ToolboxItemAttribute_t243705872), -1, sizeof(ToolboxItemAttribute_t243705872_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2369[4] = 
{
	ToolboxItemAttribute_t243705872_StaticFields::get_offset_of_Default_0(),
	ToolboxItemAttribute_t243705872_StaticFields::get_offset_of_None_1(),
	ToolboxItemAttribute_t243705872::get_offset_of_itemType_2(),
	ToolboxItemAttribute_t243705872::get_offset_of_itemTypeName_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (ToolboxItemFilterAttribute_t2990581383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2370[2] = 
{
	ToolboxItemFilterAttribute_t2990581383::get_offset_of_Filter_0(),
	ToolboxItemFilterAttribute_t2990581383::get_offset_of_ItemFilterType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (ToolboxItemFilterType_t3870514709)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2371[5] = 
{
	ToolboxItemFilterType_t3870514709::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (TypeConverterAttribute_t3271584429), -1, sizeof(TypeConverterAttribute_t3271584429_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2372[2] = 
{
	TypeConverterAttribute_t3271584429_StaticFields::get_offset_of_Default_0(),
	TypeConverterAttribute_t3271584429::get_offset_of_converter_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (TypeConverter_t2249118273), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (StandardValuesCollection_t2184948248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2374[1] = 
{
	StandardValuesCollection_t2184948248::get_offset_of_values_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (TypeDescriptionProvider_t3232077895), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2375[2] = 
{
	TypeDescriptionProvider_t3232077895::get_offset_of__emptyCustomTypeDescriptor_0(),
	TypeDescriptionProvider_t3232077895::get_offset_of__parent_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (EmptyCustomTypeDescriptor_t4007109994), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (TypeDescriptor_t3066613587), -1, sizeof(TypeDescriptor_t3066613587_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2377[10] = 
{
	TypeDescriptor_t3066613587_StaticFields::get_offset_of_creatingDefaultConverters_0(),
	TypeDescriptor_t3066613587_StaticFields::get_offset_of_defaultConverters_1(),
	TypeDescriptor_t3066613587_StaticFields::get_offset_of_componentTable_2(),
	TypeDescriptor_t3066613587_StaticFields::get_offset_of_typeTable_3(),
	TypeDescriptor_t3066613587_StaticFields::get_offset_of_editors_4(),
	TypeDescriptor_t3066613587_StaticFields::get_offset_of_typeDescriptionProvidersLock_5(),
	TypeDescriptor_t3066613587_StaticFields::get_offset_of_typeDescriptionProviders_6(),
	TypeDescriptor_t3066613587_StaticFields::get_offset_of_componentDescriptionProvidersLock_7(),
	TypeDescriptor_t3066613587_StaticFields::get_offset_of_componentDescriptionProviders_8(),
	TypeDescriptor_t3066613587_StaticFields::get_offset_of_onDispose_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (WrappedTypeDescriptionProvider_t3047182012), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2378[1] = 
{
	WrappedTypeDescriptionProvider_t3047182012::get_offset_of_U3CWrappedU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (DefaultTypeDescriptor_t2709269259), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2379[3] = 
{
	DefaultTypeDescriptor_t2709269259::get_offset_of_owner_1(),
	DefaultTypeDescriptor_t2709269259::get_offset_of_objectType_2(),
	DefaultTypeDescriptor_t2709269259::get_offset_of_instance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (DefaultTypeDescriptionProvider_t675274121), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (Info_t3260944019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2381[2] = 
{
	Info_t3260944019::get_offset_of__infoType_0(),
	Info_t3260944019::get_offset_of__attributes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (ComponentInfo_t1259248179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2382[2] = 
{
	ComponentInfo_t1259248179::get_offset_of__component_2(),
	ComponentInfo_t1259248179::get_offset_of__properties_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (TypeInfo_t323385814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2383[1] = 
{
	TypeInfo_t323385814::get_offset_of__properties_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (UInt16Converter_t819459975), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (UInt32Converter_t3472493373), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (UInt64Converter_t4189949036), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (WeakObjectWrapper_t827463650), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2387[2] = 
{
	WeakObjectWrapper_t827463650::get_offset_of_U3CTargetHashCodeU3Ek__BackingField_0(),
	WeakObjectWrapper_t827463650::get_offset_of_U3CWeakU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (WeakObjectWrapperComparer_t303980402), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (Win32Exception_t3234146298), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2389[1] = 
{
	Win32Exception_t3234146298::get_offset_of_native_error_code_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (ConfigurationException_t3515317685), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2390[2] = 
{
	ConfigurationException_t3515317685::get_offset_of_filename_11(),
	ConfigurationException_t3515317685::get_offset_of_line_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (ConfigurationSettings_t822951835), -1, sizeof(ConfigurationSettings_t822951835_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2391[2] = 
{
	ConfigurationSettings_t822951835_StaticFields::get_offset_of_config_0(),
	ConfigurationSettings_t822951835_StaticFields::get_offset_of_lockobj_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (DefaultConfig_t1013547162), -1, sizeof(DefaultConfig_t1013547162_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2392[1] = 
{
	DefaultConfig_t1013547162_StaticFields::get_offset_of_instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (SettingsBase_t3488873952), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { sizeof (DefaultUriParser_t95882050), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { sizeof (AlphabeticalEnumConverter_t2601987229), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { sizeof (CorrelationManager_t2688752967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2399[1] = 
{
	CorrelationManager_t2688752967::get_offset_of_op_stack_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
