﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"
#include "System_Xml_System_Xml_XPath_XPathNodeType3031007223.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.XPath.DTMXPathLinkedNode2
struct  DTMXPathLinkedNode2_t3353097823 
{
public:
	// System.Int32 Mono.Xml.XPath.DTMXPathLinkedNode2::FirstChild
	int32_t ___FirstChild_0;
	// System.Int32 Mono.Xml.XPath.DTMXPathLinkedNode2::Parent
	int32_t ___Parent_1;
	// System.Int32 Mono.Xml.XPath.DTMXPathLinkedNode2::PreviousSibling
	int32_t ___PreviousSibling_2;
	// System.Int32 Mono.Xml.XPath.DTMXPathLinkedNode2::NextSibling
	int32_t ___NextSibling_3;
	// System.Int32 Mono.Xml.XPath.DTMXPathLinkedNode2::FirstAttribute
	int32_t ___FirstAttribute_4;
	// System.Int32 Mono.Xml.XPath.DTMXPathLinkedNode2::FirstNamespace
	int32_t ___FirstNamespace_5;
	// System.Xml.XPath.XPathNodeType Mono.Xml.XPath.DTMXPathLinkedNode2::NodeType
	int32_t ___NodeType_6;
	// System.Int32 Mono.Xml.XPath.DTMXPathLinkedNode2::BaseURI
	int32_t ___BaseURI_7;
	// System.Boolean Mono.Xml.XPath.DTMXPathLinkedNode2::IsEmptyElement
	bool ___IsEmptyElement_8;
	// System.Int32 Mono.Xml.XPath.DTMXPathLinkedNode2::LocalName
	int32_t ___LocalName_9;
	// System.Int32 Mono.Xml.XPath.DTMXPathLinkedNode2::NamespaceURI
	int32_t ___NamespaceURI_10;
	// System.Int32 Mono.Xml.XPath.DTMXPathLinkedNode2::Prefix
	int32_t ___Prefix_11;
	// System.Int32 Mono.Xml.XPath.DTMXPathLinkedNode2::Value
	int32_t ___Value_12;
	// System.Int32 Mono.Xml.XPath.DTMXPathLinkedNode2::XmlLang
	int32_t ___XmlLang_13;
	// System.Int32 Mono.Xml.XPath.DTMXPathLinkedNode2::LineNumber
	int32_t ___LineNumber_14;
	// System.Int32 Mono.Xml.XPath.DTMXPathLinkedNode2::LinePosition
	int32_t ___LinePosition_15;

public:
	inline static int32_t get_offset_of_FirstChild_0() { return static_cast<int32_t>(offsetof(DTMXPathLinkedNode2_t3353097823, ___FirstChild_0)); }
	inline int32_t get_FirstChild_0() const { return ___FirstChild_0; }
	inline int32_t* get_address_of_FirstChild_0() { return &___FirstChild_0; }
	inline void set_FirstChild_0(int32_t value)
	{
		___FirstChild_0 = value;
	}

	inline static int32_t get_offset_of_Parent_1() { return static_cast<int32_t>(offsetof(DTMXPathLinkedNode2_t3353097823, ___Parent_1)); }
	inline int32_t get_Parent_1() const { return ___Parent_1; }
	inline int32_t* get_address_of_Parent_1() { return &___Parent_1; }
	inline void set_Parent_1(int32_t value)
	{
		___Parent_1 = value;
	}

	inline static int32_t get_offset_of_PreviousSibling_2() { return static_cast<int32_t>(offsetof(DTMXPathLinkedNode2_t3353097823, ___PreviousSibling_2)); }
	inline int32_t get_PreviousSibling_2() const { return ___PreviousSibling_2; }
	inline int32_t* get_address_of_PreviousSibling_2() { return &___PreviousSibling_2; }
	inline void set_PreviousSibling_2(int32_t value)
	{
		___PreviousSibling_2 = value;
	}

	inline static int32_t get_offset_of_NextSibling_3() { return static_cast<int32_t>(offsetof(DTMXPathLinkedNode2_t3353097823, ___NextSibling_3)); }
	inline int32_t get_NextSibling_3() const { return ___NextSibling_3; }
	inline int32_t* get_address_of_NextSibling_3() { return &___NextSibling_3; }
	inline void set_NextSibling_3(int32_t value)
	{
		___NextSibling_3 = value;
	}

	inline static int32_t get_offset_of_FirstAttribute_4() { return static_cast<int32_t>(offsetof(DTMXPathLinkedNode2_t3353097823, ___FirstAttribute_4)); }
	inline int32_t get_FirstAttribute_4() const { return ___FirstAttribute_4; }
	inline int32_t* get_address_of_FirstAttribute_4() { return &___FirstAttribute_4; }
	inline void set_FirstAttribute_4(int32_t value)
	{
		___FirstAttribute_4 = value;
	}

	inline static int32_t get_offset_of_FirstNamespace_5() { return static_cast<int32_t>(offsetof(DTMXPathLinkedNode2_t3353097823, ___FirstNamespace_5)); }
	inline int32_t get_FirstNamespace_5() const { return ___FirstNamespace_5; }
	inline int32_t* get_address_of_FirstNamespace_5() { return &___FirstNamespace_5; }
	inline void set_FirstNamespace_5(int32_t value)
	{
		___FirstNamespace_5 = value;
	}

	inline static int32_t get_offset_of_NodeType_6() { return static_cast<int32_t>(offsetof(DTMXPathLinkedNode2_t3353097823, ___NodeType_6)); }
	inline int32_t get_NodeType_6() const { return ___NodeType_6; }
	inline int32_t* get_address_of_NodeType_6() { return &___NodeType_6; }
	inline void set_NodeType_6(int32_t value)
	{
		___NodeType_6 = value;
	}

	inline static int32_t get_offset_of_BaseURI_7() { return static_cast<int32_t>(offsetof(DTMXPathLinkedNode2_t3353097823, ___BaseURI_7)); }
	inline int32_t get_BaseURI_7() const { return ___BaseURI_7; }
	inline int32_t* get_address_of_BaseURI_7() { return &___BaseURI_7; }
	inline void set_BaseURI_7(int32_t value)
	{
		___BaseURI_7 = value;
	}

	inline static int32_t get_offset_of_IsEmptyElement_8() { return static_cast<int32_t>(offsetof(DTMXPathLinkedNode2_t3353097823, ___IsEmptyElement_8)); }
	inline bool get_IsEmptyElement_8() const { return ___IsEmptyElement_8; }
	inline bool* get_address_of_IsEmptyElement_8() { return &___IsEmptyElement_8; }
	inline void set_IsEmptyElement_8(bool value)
	{
		___IsEmptyElement_8 = value;
	}

	inline static int32_t get_offset_of_LocalName_9() { return static_cast<int32_t>(offsetof(DTMXPathLinkedNode2_t3353097823, ___LocalName_9)); }
	inline int32_t get_LocalName_9() const { return ___LocalName_9; }
	inline int32_t* get_address_of_LocalName_9() { return &___LocalName_9; }
	inline void set_LocalName_9(int32_t value)
	{
		___LocalName_9 = value;
	}

	inline static int32_t get_offset_of_NamespaceURI_10() { return static_cast<int32_t>(offsetof(DTMXPathLinkedNode2_t3353097823, ___NamespaceURI_10)); }
	inline int32_t get_NamespaceURI_10() const { return ___NamespaceURI_10; }
	inline int32_t* get_address_of_NamespaceURI_10() { return &___NamespaceURI_10; }
	inline void set_NamespaceURI_10(int32_t value)
	{
		___NamespaceURI_10 = value;
	}

	inline static int32_t get_offset_of_Prefix_11() { return static_cast<int32_t>(offsetof(DTMXPathLinkedNode2_t3353097823, ___Prefix_11)); }
	inline int32_t get_Prefix_11() const { return ___Prefix_11; }
	inline int32_t* get_address_of_Prefix_11() { return &___Prefix_11; }
	inline void set_Prefix_11(int32_t value)
	{
		___Prefix_11 = value;
	}

	inline static int32_t get_offset_of_Value_12() { return static_cast<int32_t>(offsetof(DTMXPathLinkedNode2_t3353097823, ___Value_12)); }
	inline int32_t get_Value_12() const { return ___Value_12; }
	inline int32_t* get_address_of_Value_12() { return &___Value_12; }
	inline void set_Value_12(int32_t value)
	{
		___Value_12 = value;
	}

	inline static int32_t get_offset_of_XmlLang_13() { return static_cast<int32_t>(offsetof(DTMXPathLinkedNode2_t3353097823, ___XmlLang_13)); }
	inline int32_t get_XmlLang_13() const { return ___XmlLang_13; }
	inline int32_t* get_address_of_XmlLang_13() { return &___XmlLang_13; }
	inline void set_XmlLang_13(int32_t value)
	{
		___XmlLang_13 = value;
	}

	inline static int32_t get_offset_of_LineNumber_14() { return static_cast<int32_t>(offsetof(DTMXPathLinkedNode2_t3353097823, ___LineNumber_14)); }
	inline int32_t get_LineNumber_14() const { return ___LineNumber_14; }
	inline int32_t* get_address_of_LineNumber_14() { return &___LineNumber_14; }
	inline void set_LineNumber_14(int32_t value)
	{
		___LineNumber_14 = value;
	}

	inline static int32_t get_offset_of_LinePosition_15() { return static_cast<int32_t>(offsetof(DTMXPathLinkedNode2_t3353097823, ___LinePosition_15)); }
	inline int32_t get_LinePosition_15() const { return ___LinePosition_15; }
	inline int32_t* get_address_of_LinePosition_15() { return &___LinePosition_15; }
	inline void set_LinePosition_15(int32_t value)
	{
		___LinePosition_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Mono.Xml.XPath.DTMXPathLinkedNode2
struct DTMXPathLinkedNode2_t3353097823_marshaled_pinvoke
{
	int32_t ___FirstChild_0;
	int32_t ___Parent_1;
	int32_t ___PreviousSibling_2;
	int32_t ___NextSibling_3;
	int32_t ___FirstAttribute_4;
	int32_t ___FirstNamespace_5;
	int32_t ___NodeType_6;
	int32_t ___BaseURI_7;
	int32_t ___IsEmptyElement_8;
	int32_t ___LocalName_9;
	int32_t ___NamespaceURI_10;
	int32_t ___Prefix_11;
	int32_t ___Value_12;
	int32_t ___XmlLang_13;
	int32_t ___LineNumber_14;
	int32_t ___LinePosition_15;
};
// Native definition for COM marshalling of Mono.Xml.XPath.DTMXPathLinkedNode2
struct DTMXPathLinkedNode2_t3353097823_marshaled_com
{
	int32_t ___FirstChild_0;
	int32_t ___Parent_1;
	int32_t ___PreviousSibling_2;
	int32_t ___NextSibling_3;
	int32_t ___FirstAttribute_4;
	int32_t ___FirstNamespace_5;
	int32_t ___NodeType_6;
	int32_t ___BaseURI_7;
	int32_t ___IsEmptyElement_8;
	int32_t ___LocalName_9;
	int32_t ___NamespaceURI_10;
	int32_t ___Prefix_11;
	int32_t ___Value_12;
	int32_t ___XmlLang_13;
	int32_t ___LineNumber_14;
	int32_t ___LinePosition_15;
};
