﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range173988048.h"
#include "UnityEngine_UnityEngine_Plane1000493321.h"
#include "UnityEngine_UnityEngine_PropertyAttribute3677895545.h"
#include "UnityEngine_UnityEngine_TooltipAttribute3957072629.h"
#include "UnityEngine_UnityEngine_SpaceAttribute3956583069.h"
#include "UnityEngine_UnityEngine_RangeAttribute3337244227.h"
#include "UnityEngine_UnityEngine_MultilineAttribute2253019191.h"
#include "UnityEngine_UnityEngine_TextAreaAttribute3326046611.h"
#include "UnityEngine_UnityEngine_ColorUsageAttribute669369404.h"
#include "UnityEngine_UnityEngine_RangeInt2094684618.h"
#include "UnityEngine_UnityEngine_Ray3785851493.h"
#include "UnityEngine_UnityEngine_Rect2360479859.h"
#include "UnityEngine_UnityEngine_RuntimeInitializeLoadType378148151.h"
#include "UnityEngine_UnityEngine_RuntimeInitializeOnLoadMet3192313494.h"
#include "UnityEngine_UnityEngine_SelectionBaseAttribute3493465804.h"
#include "UnityEngine_UnityEngine_SerializePrivateVariables3872960625.h"
#include "UnityEngine_UnityEngine_SerializeField3286833614.h"
#include "UnityEngine_UnityEngine_PreferBinarySerialization2906007930.h"
#include "UnityEngine_UnityEngine_StackTraceUtility3465565809.h"
#include "UnityEngine_UnityEngine_UnityException3598173660.h"
#include "UnityEngine_UnityEngine_SystemClock1201361285.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType1530597702.h"
#include "UnityEngine_UnityEngine_TrackedReference1199777556.h"
#include "UnityEngine_UnityEngine_UnityAPICompatibilityVersi3842027601.h"
#include "UnityEngine_UnityEngine_Events_PersistentListenerMo232255230.h"
#include "UnityEngine_UnityEngine_Events_ArgumentCache2187958399.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall2703961024.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall832123510.h"
#include "UnityEngine_UnityEngine_Events_UnityEventCallState3448586328.h"
#include "UnityEngine_UnityEngine_Events_PersistentCall3407714124.h"
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup3050769227.h"
#include "UnityEngine_UnityEngine_Events_InvokableCallList2498835369.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase3960448221.h"
#include "UnityEngine_UnityEngine_Events_UnityAction3245792599.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent2581268647.h"
#include "UnityEngine_UnityEngine_UnityString1423233093.h"
#include "UnityEngine_UnityEngine_Vector22156229523.h"
#include "UnityEngine_UnityEngine_Vector43319028937.h"
#include "UnityEngine_UnityEngine_WaitForSecondsRealtime189548121.h"
#include "UnityEngine_UnityEngine_ThreadAndSerializationSafeA363116225.h"
#include "UnityEngine_UnityEngine_Collections_ReadOnlyAttrib2029203740.h"
#include "UnityEngine_UnityEngine_Collections_ReadWriteAttrib306517538.h"
#include "UnityEngine_UnityEngine_Collections_WriteOnlyAttrib595109273.h"
#include "UnityEngine_UnityEngine_Collections_DeallocateOnJo3131681843.h"
#include "UnityEngine_UnityEngine_Collections_NativeContaine2600515814.h"
#include "UnityEngine_UnityEngine_Collections_NativeContaine3790689680.h"
#include "UnityEngine_UnityEngine_Collections_NativeContaine1586929818.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Fram3985821396.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Fram4130423782.h"
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttri2337225216.h"
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAt3592494112.h"
#include "UnityEngine_UnityEngine_Logger274032455.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio1170575784.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio3081694049.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio3411787513.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio1491597365.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio3251856151.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio1684935770.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnection907692441.h"
#include "UnityEngine_UnityEngine_Experimental_Rendering_Ren4036911426.h"
#include "UnityEngine_UnityEngine_Scripting_PreserveAttribut1583619344.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode1703770351.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative4130846357.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri2859083114.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules96689094.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAt254868554.h"
#include "UnityEngine_UnityEngineInternal_GenericStack1310059385.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension4089902045.h"
#include "System_IdentityModel_Selectors_U3CModuleU3E692745525.h"
#include "System_Messaging_U3CModuleU3E692745525.h"
#include "Vuforia_UnityExtensions_U3CModuleU3E692745525.h"
#include "Vuforia_UnityExtensions_Vuforia_ARController116632334.h"
#include "Vuforia_UnityExtensions_Vuforia_ARController_U3CU32669575632.h"
#include "Vuforia_UnityExtensions_Vuforia_DigitalEyewearARCo1054226036.h"
#include "Vuforia_UnityExtensions_Vuforia_DigitalEyewearARCo2277580470.h"
#include "Vuforia_UnityExtensions_Vuforia_DigitalEyewearARCo3144873991.h"
#include "Vuforia_UnityExtensions_Vuforia_DigitalEyewearARCon568665021.h"
#include "Vuforia_UnityExtensions_Vuforia_DigitalEyewearARCo2043332680.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearDevice3223385723.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearDevice_EyeID263427581.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4100 = { sizeof (Range_t173988048)+ sizeof (Il2CppObject), sizeof(Range_t173988048 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4100[2] = 
{
	Range_t173988048::get_offset_of_from_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Range_t173988048::get_offset_of_count_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4101 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4102 = { sizeof (Plane_t1000493321)+ sizeof (Il2CppObject), sizeof(Plane_t1000493321 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4102[2] = 
{
	Plane_t1000493321::get_offset_of_m_Normal_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Plane_t1000493321::get_offset_of_m_Distance_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4103 = { sizeof (PropertyAttribute_t3677895545), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4104 = { sizeof (TooltipAttribute_t3957072629), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4104[1] = 
{
	TooltipAttribute_t3957072629::get_offset_of_tooltip_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4105 = { sizeof (SpaceAttribute_t3956583069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4105[1] = 
{
	SpaceAttribute_t3956583069::get_offset_of_height_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4106 = { sizeof (RangeAttribute_t3337244227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4106[2] = 
{
	RangeAttribute_t3337244227::get_offset_of_min_0(),
	RangeAttribute_t3337244227::get_offset_of_max_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4107 = { sizeof (MultilineAttribute_t2253019191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4107[1] = 
{
	MultilineAttribute_t2253019191::get_offset_of_lines_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4108 = { sizeof (TextAreaAttribute_t3326046611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4108[2] = 
{
	TextAreaAttribute_t3326046611::get_offset_of_minLines_0(),
	TextAreaAttribute_t3326046611::get_offset_of_maxLines_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4109 = { sizeof (ColorUsageAttribute_t669369404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4109[6] = 
{
	ColorUsageAttribute_t669369404::get_offset_of_showAlpha_0(),
	ColorUsageAttribute_t669369404::get_offset_of_hdr_1(),
	ColorUsageAttribute_t669369404::get_offset_of_minBrightness_2(),
	ColorUsageAttribute_t669369404::get_offset_of_maxBrightness_3(),
	ColorUsageAttribute_t669369404::get_offset_of_minExposureValue_4(),
	ColorUsageAttribute_t669369404::get_offset_of_maxExposureValue_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4110 = { sizeof (RangeInt_t2094684618)+ sizeof (Il2CppObject), sizeof(RangeInt_t2094684618 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4110[2] = 
{
	RangeInt_t2094684618::get_offset_of_start_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RangeInt_t2094684618::get_offset_of_length_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4111 = { sizeof (Ray_t3785851493)+ sizeof (Il2CppObject), sizeof(Ray_t3785851493 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4111[2] = 
{
	Ray_t3785851493::get_offset_of_m_Origin_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Ray_t3785851493::get_offset_of_m_Direction_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4112 = { sizeof (Rect_t2360479859)+ sizeof (Il2CppObject), sizeof(Rect_t2360479859 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4112[4] = 
{
	Rect_t2360479859::get_offset_of_m_XMin_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Rect_t2360479859::get_offset_of_m_YMin_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Rect_t2360479859::get_offset_of_m_Width_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Rect_t2360479859::get_offset_of_m_Height_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4113 = { sizeof (RuntimeInitializeLoadType_t378148151)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4113[3] = 
{
	RuntimeInitializeLoadType_t378148151::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4114 = { sizeof (RuntimeInitializeOnLoadMethodAttribute_t3192313494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4114[1] = 
{
	RuntimeInitializeOnLoadMethodAttribute_t3192313494::get_offset_of_U3CloadTypeU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4115 = { sizeof (SelectionBaseAttribute_t3493465804), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4116 = { sizeof (SerializePrivateVariables_t3872960625), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4117 = { sizeof (SerializeField_t3286833614), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4118 = { sizeof (PreferBinarySerialization_t2906007930), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4119 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4120 = { sizeof (StackTraceUtility_t3465565809), -1, sizeof(StackTraceUtility_t3465565809_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4120[1] = 
{
	StackTraceUtility_t3465565809_StaticFields::get_offset_of_projectFolder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4121 = { sizeof (UnityException_t3598173660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4121[2] = 
{
	0,
	UnityException_t3598173660::get_offset_of_unityStackTrace_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4122 = { sizeof (SystemClock_t1201361285), -1, sizeof(SystemClock_t1201361285_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4122[1] = 
{
	SystemClock_t1201361285_StaticFields::get_offset_of_s_Epoch_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4123 = { sizeof (TouchScreenKeyboardType_t1530597702)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4123[10] = 
{
	TouchScreenKeyboardType_t1530597702::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4124 = { sizeof (TrackedReference_t1199777556), sizeof(TrackedReference_t1199777556_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4124[1] = 
{
	TrackedReference_t1199777556::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4125 = { sizeof (UnityAPICompatibilityVersionAttribute_t3842027601), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4125[1] = 
{
	UnityAPICompatibilityVersionAttribute_t3842027601::get_offset_of__version_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4126 = { sizeof (PersistentListenerMode_t232255230)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4126[8] = 
{
	PersistentListenerMode_t232255230::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4127 = { sizeof (ArgumentCache_t2187958399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4127[6] = 
{
	ArgumentCache_t2187958399::get_offset_of_m_ObjectArgument_0(),
	ArgumentCache_t2187958399::get_offset_of_m_ObjectArgumentAssemblyTypeName_1(),
	ArgumentCache_t2187958399::get_offset_of_m_IntArgument_2(),
	ArgumentCache_t2187958399::get_offset_of_m_FloatArgument_3(),
	ArgumentCache_t2187958399::get_offset_of_m_StringArgument_4(),
	ArgumentCache_t2187958399::get_offset_of_m_BoolArgument_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4128 = { sizeof (BaseInvokableCall_t2703961024), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4129 = { sizeof (InvokableCall_t832123510), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4129[1] = 
{
	InvokableCall_t832123510::get_offset_of_Delegate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4130 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4130[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4131 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4131[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4132 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4132[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4133 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4133[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4134 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4134[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4135 = { sizeof (UnityEventCallState_t3448586328)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4135[4] = 
{
	UnityEventCallState_t3448586328::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4136 = { sizeof (PersistentCall_t3407714124), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4136[5] = 
{
	PersistentCall_t3407714124::get_offset_of_m_Target_0(),
	PersistentCall_t3407714124::get_offset_of_m_MethodName_1(),
	PersistentCall_t3407714124::get_offset_of_m_Mode_2(),
	PersistentCall_t3407714124::get_offset_of_m_Arguments_3(),
	PersistentCall_t3407714124::get_offset_of_m_CallState_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4137 = { sizeof (PersistentCallGroup_t3050769227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4137[1] = 
{
	PersistentCallGroup_t3050769227::get_offset_of_m_Calls_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4138 = { sizeof (InvokableCallList_t2498835369), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4138[4] = 
{
	InvokableCallList_t2498835369::get_offset_of_m_PersistentCalls_0(),
	InvokableCallList_t2498835369::get_offset_of_m_RuntimeCalls_1(),
	InvokableCallList_t2498835369::get_offset_of_m_ExecutingCalls_2(),
	InvokableCallList_t2498835369::get_offset_of_m_NeedsUpdate_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4139 = { sizeof (UnityEventBase_t3960448221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4139[4] = 
{
	UnityEventBase_t3960448221::get_offset_of_m_Calls_0(),
	UnityEventBase_t3960448221::get_offset_of_m_PersistentCalls_1(),
	UnityEventBase_t3960448221::get_offset_of_m_TypeName_2(),
	UnityEventBase_t3960448221::get_offset_of_m_CallsDirty_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4140 = { sizeof (UnityAction_t3245792599), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4141 = { sizeof (UnityEvent_t2581268647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4141[1] = 
{
	UnityEvent_t2581268647::get_offset_of_m_InvokeArray_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4142 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4143 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4143[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4144 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4145 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4145[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4146 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4147 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4147[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4148 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4149 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4149[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4150 = { sizeof (UnityString_t1423233093), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4151 = { sizeof (Vector2_t2156229523)+ sizeof (Il2CppObject), sizeof(Vector2_t2156229523 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4151[3] = 
{
	Vector2_t2156229523::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector2_t2156229523::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4152 = { sizeof (Vector4_t3319028937)+ sizeof (Il2CppObject), sizeof(Vector4_t3319028937 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4152[5] = 
{
	0,
	Vector4_t3319028937::get_offset_of_x_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t3319028937::get_offset_of_y_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t3319028937::get_offset_of_z_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t3319028937::get_offset_of_w_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4153 = { sizeof (WaitForSecondsRealtime_t189548121), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4153[1] = 
{
	WaitForSecondsRealtime_t189548121::get_offset_of_waitTime_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4154 = { sizeof (ThreadAndSerializationSafeAttribute_t363116225), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4155 = { sizeof (ReadOnlyAttribute_t2029203740), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4156 = { sizeof (ReadWriteAttribute_t306517538), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4157 = { sizeof (WriteOnlyAttribute_t595109273), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4158 = { sizeof (DeallocateOnJobCompletionAttribute_t3131681843), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4159 = { sizeof (NativeContainerAttribute_t2600515814), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4160 = { sizeof (NativeContainerSupportsAtomicWriteAttribute_t3790689680), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4161 = { sizeof (NativeContainerSupportsMinMaxWriteRestrictionAttribute_t1586929818), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4162 = { sizeof (FrameData_t3985821396)+ sizeof (Il2CppObject), sizeof(FrameData_t3985821396 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4162[6] = 
{
	FrameData_t3985821396::get_offset_of_m_FrameID_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t3985821396::get_offset_of_m_DeltaTime_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t3985821396::get_offset_of_m_Weight_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t3985821396::get_offset_of_m_EffectiveWeight_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t3985821396::get_offset_of_m_EffectiveSpeed_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t3985821396::get_offset_of_m_Flags_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4163 = { sizeof (Flags_t4130423782)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4163[3] = 
{
	Flags_t4130423782::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4164 = { sizeof (DefaultValueAttribute_t2337225216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4164[1] = 
{
	DefaultValueAttribute_t2337225216::get_offset_of_DefaultValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4165 = { sizeof (ExcludeFromDocsAttribute_t3592494112), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4166 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4167 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4168 = { sizeof (Logger_t274032455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4168[3] = 
{
	Logger_t274032455::get_offset_of_U3ClogHandlerU3Ek__BackingField_0(),
	Logger_t274032455::get_offset_of_U3ClogEnabledU3Ek__BackingField_1(),
	Logger_t274032455::get_offset_of_U3CfilterLogTypeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4169 = { sizeof (MessageEventArgs_t1170575784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4169[2] = 
{
	MessageEventArgs_t1170575784::get_offset_of_playerId_0(),
	MessageEventArgs_t1170575784::get_offset_of_data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4170 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4171 = { sizeof (PlayerConnection_t3081694049), -1, sizeof(PlayerConnection_t3081694049_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4171[3] = 
{
	PlayerConnection_t3081694049::get_offset_of_m_PlayerEditorConnectionEvents_2(),
	PlayerConnection_t3081694049::get_offset_of_m_connectedPlayers_3(),
	PlayerConnection_t3081694049_StaticFields::get_offset_of_s_Instance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4172 = { sizeof (PlayerEditorConnectionEvents_t3411787513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4172[3] = 
{
	PlayerEditorConnectionEvents_t3411787513::get_offset_of_messageTypeSubscribers_0(),
	PlayerEditorConnectionEvents_t3411787513::get_offset_of_connectionEvent_1(),
	PlayerEditorConnectionEvents_t3411787513::get_offset_of_disconnectionEvent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4173 = { sizeof (MessageEvent_t1491597365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4174 = { sizeof (ConnectionChangeEvent_t3251856151), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4175 = { sizeof (MessageTypeSubscribers_t1684935770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4175[3] = 
{
	MessageTypeSubscribers_t1684935770::get_offset_of_m_messageTypeId_0(),
	MessageTypeSubscribers_t1684935770::get_offset_of_subscriberCount_1(),
	MessageTypeSubscribers_t1684935770::get_offset_of_messageCallback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4176 = { sizeof (U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t907692441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4176[1] = 
{
	U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t907692441::get_offset_of_messageId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4177 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4178 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4179 = { sizeof (RenderPipelineManager_t4036911426), -1, sizeof(RenderPipelineManager_t4036911426_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4179[2] = 
{
	RenderPipelineManager_t4036911426_StaticFields::get_offset_of_s_CurrentPipelineAsset_0(),
	RenderPipelineManager_t4036911426_StaticFields::get_offset_of_U3CcurrentPipelineU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4180 = { sizeof (PreserveAttribute_t1583619344), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4181 = { sizeof (UsedByNativeCodeAttribute_t1703770351), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4182 = { sizeof (RequiredByNativeCodeAttribute_t4130846357), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4183 = { sizeof (FormerlySerializedAsAttribute_t2859083114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4183[1] = 
{
	FormerlySerializedAsAttribute_t2859083114::get_offset_of_m_oldName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4184 = { sizeof (TypeInferenceRules_t96689094)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4184[5] = 
{
	TypeInferenceRules_t96689094::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4185 = { sizeof (TypeInferenceRuleAttribute_t254868554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4185[1] = 
{
	TypeInferenceRuleAttribute_t254868554::get_offset_of__rule_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4186 = { sizeof (GenericStack_t1310059385), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4187 = { sizeof (NetFxCoreExtensions_t4089902045), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4188 = { sizeof (U3CModuleU3E_t692745550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4189 = { sizeof (U3CModuleU3E_t692745551), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4190 = { sizeof (U3CModuleU3E_t692745552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4191 = { sizeof (ARController_t116632334), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4191[1] = 
{
	ARController_t116632334::get_offset_of_mVuforiaBehaviour_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4192 = { sizeof (U3CU3Ec__DisplayClass11_0_t2669575632), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4192[1] = 
{
	U3CU3Ec__DisplayClass11_0_t2669575632::get_offset_of_controller_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4193 = { sizeof (DigitalEyewearARController_t1054226036), -1, sizeof(DigitalEyewearARController_t1054226036_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4193[28] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	DigitalEyewearARController_t1054226036::get_offset_of_mCameraOffset_7(),
	DigitalEyewearARController_t1054226036::get_offset_of_mDistortionRenderingMode_8(),
	DigitalEyewearARController_t1054226036::get_offset_of_mDistortionRenderingLayer_9(),
	DigitalEyewearARController_t1054226036::get_offset_of_mEyewearType_10(),
	DigitalEyewearARController_t1054226036::get_offset_of_mStereoFramework_11(),
	DigitalEyewearARController_t1054226036::get_offset_of_mSeeThroughConfiguration_12(),
	DigitalEyewearARController_t1054226036::get_offset_of_mViewerName_13(),
	DigitalEyewearARController_t1054226036::get_offset_of_mViewerManufacturer_14(),
	DigitalEyewearARController_t1054226036::get_offset_of_mUseCustomViewer_15(),
	DigitalEyewearARController_t1054226036::get_offset_of_mCustomViewer_16(),
	DigitalEyewearARController_t1054226036::get_offset_of_mCentralAnchorPoint_17(),
	DigitalEyewearARController_t1054226036::get_offset_of_mParentAnchorPoint_18(),
	DigitalEyewearARController_t1054226036::get_offset_of_mPrimaryCamera_19(),
	DigitalEyewearARController_t1054226036::get_offset_of_mPrimaryCameraOriginalRect_20(),
	DigitalEyewearARController_t1054226036::get_offset_of_mSecondaryCamera_21(),
	DigitalEyewearARController_t1054226036::get_offset_of_mSecondaryCameraOriginalRect_22(),
	DigitalEyewearARController_t1054226036::get_offset_of_mSecondaryCameraDisabledLocally_23(),
	DigitalEyewearARController_t1054226036::get_offset_of_mVuforiaBehaviour_24(),
	DigitalEyewearARController_t1054226036::get_offset_of_mDistortionRenderingBhvr_25(),
	DigitalEyewearARController_t1054226036::get_offset_of_mSetFocusPlaneAutomatically_26(),
	DigitalEyewearARController_t1054226036_StaticFields::get_offset_of_mInstance_27(),
	DigitalEyewearARController_t1054226036_StaticFields::get_offset_of_mPadlock_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4194 = { sizeof (EyewearType_t2277580470)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4194[4] = 
{
	EyewearType_t2277580470::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4195 = { sizeof (StereoFramework_t3144873991)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4195[4] = 
{
	StereoFramework_t3144873991::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4196 = { sizeof (SeeThroughConfiguration_t568665021)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4196[3] = 
{
	SeeThroughConfiguration_t568665021::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4197 = { sizeof (SerializableViewerParameters_t2043332680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4197[11] = 
{
	SerializableViewerParameters_t2043332680::get_offset_of_Version_0(),
	SerializableViewerParameters_t2043332680::get_offset_of_Name_1(),
	SerializableViewerParameters_t2043332680::get_offset_of_Manufacturer_2(),
	SerializableViewerParameters_t2043332680::get_offset_of_ButtonType_3(),
	SerializableViewerParameters_t2043332680::get_offset_of_ScreenToLensDistance_4(),
	SerializableViewerParameters_t2043332680::get_offset_of_InterLensDistance_5(),
	SerializableViewerParameters_t2043332680::get_offset_of_TrayAlignment_6(),
	SerializableViewerParameters_t2043332680::get_offset_of_LensCenterToTrayDistance_7(),
	SerializableViewerParameters_t2043332680::get_offset_of_DistortionCoefficients_8(),
	SerializableViewerParameters_t2043332680::get_offset_of_FieldOfView_9(),
	SerializableViewerParameters_t2043332680::get_offset_of_ContainsMagnet_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4198 = { sizeof (EyewearDevice_t3223385723), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4199 = { sizeof (EyeID_t263427581)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4199[4] = 
{
	EyeID_t263427581::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
