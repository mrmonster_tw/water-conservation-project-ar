﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Web_System_Web_Compilation_TagType2319653245.h"

// System.Text.RegularExpressions.Regex
struct Regex_t3657309853;
// System.Web.Compilation.ParserStack
struct ParserStack_t2907135086;
// System.Web.Compilation.BuilderLocationStack
struct BuilderLocationStack_t2907033206;
// System.Web.UI.TemplateParser
struct TemplateParser_t24149626;
// System.Text.StringBuilder
struct StringBuilder_t1712802186;
// System.Web.UI.RootBuilder
struct RootBuilder_t1594119744;
// System.Web.Compilation.ILocation
struct ILocation_t3961726794;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Web.Compilation.AspComponentFoundry
struct AspComponentFoundry_t3087970636;
// System.IO.Stream
struct Stream_t1273022909;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.AspGenerator
struct  AspGenerator_t2810561191  : public Il2CppObject
{
public:
	// System.Web.Compilation.ParserStack System.Web.Compilation.AspGenerator::pstack
	ParserStack_t2907135086 * ___pstack_5;
	// System.Web.Compilation.BuilderLocationStack System.Web.Compilation.AspGenerator::stack
	BuilderLocationStack_t2907033206 * ___stack_6;
	// System.Web.UI.TemplateParser System.Web.Compilation.AspGenerator::tparser
	TemplateParser_t24149626 * ___tparser_7;
	// System.Text.StringBuilder System.Web.Compilation.AspGenerator::text
	StringBuilder_t1712802186 * ___text_8;
	// System.Web.UI.RootBuilder System.Web.Compilation.AspGenerator::rootBuilder
	RootBuilder_t1594119744 * ___rootBuilder_9;
	// System.Boolean System.Web.Compilation.AspGenerator::inScript
	bool ___inScript_10;
	// System.Boolean System.Web.Compilation.AspGenerator::javascript
	bool ___javascript_11;
	// System.Boolean System.Web.Compilation.AspGenerator::ignore_text
	bool ___ignore_text_12;
	// System.Web.Compilation.ILocation System.Web.Compilation.AspGenerator::location
	Il2CppObject * ___location_13;
	// System.Boolean System.Web.Compilation.AspGenerator::isApplication
	bool ___isApplication_14;
	// System.Text.StringBuilder System.Web.Compilation.AspGenerator::tagInnerText
	StringBuilder_t1712802186 * ___tagInnerText_15;
	// System.Boolean System.Web.Compilation.AspGenerator::inForm
	bool ___inForm_17;
	// System.Boolean System.Web.Compilation.AspGenerator::useOtherTags
	bool ___useOtherTags_18;
	// System.Web.Compilation.TagType System.Web.Compilation.AspGenerator::lastTag
	int32_t ___lastTag_19;
	// System.Web.Compilation.AspComponentFoundry System.Web.Compilation.AspGenerator::componentFoundry
	AspComponentFoundry_t3087970636 * ___componentFoundry_20;
	// System.IO.Stream System.Web.Compilation.AspGenerator::inputStream
	Stream_t1273022909 * ___inputStream_21;

public:
	inline static int32_t get_offset_of_pstack_5() { return static_cast<int32_t>(offsetof(AspGenerator_t2810561191, ___pstack_5)); }
	inline ParserStack_t2907135086 * get_pstack_5() const { return ___pstack_5; }
	inline ParserStack_t2907135086 ** get_address_of_pstack_5() { return &___pstack_5; }
	inline void set_pstack_5(ParserStack_t2907135086 * value)
	{
		___pstack_5 = value;
		Il2CppCodeGenWriteBarrier(&___pstack_5, value);
	}

	inline static int32_t get_offset_of_stack_6() { return static_cast<int32_t>(offsetof(AspGenerator_t2810561191, ___stack_6)); }
	inline BuilderLocationStack_t2907033206 * get_stack_6() const { return ___stack_6; }
	inline BuilderLocationStack_t2907033206 ** get_address_of_stack_6() { return &___stack_6; }
	inline void set_stack_6(BuilderLocationStack_t2907033206 * value)
	{
		___stack_6 = value;
		Il2CppCodeGenWriteBarrier(&___stack_6, value);
	}

	inline static int32_t get_offset_of_tparser_7() { return static_cast<int32_t>(offsetof(AspGenerator_t2810561191, ___tparser_7)); }
	inline TemplateParser_t24149626 * get_tparser_7() const { return ___tparser_7; }
	inline TemplateParser_t24149626 ** get_address_of_tparser_7() { return &___tparser_7; }
	inline void set_tparser_7(TemplateParser_t24149626 * value)
	{
		___tparser_7 = value;
		Il2CppCodeGenWriteBarrier(&___tparser_7, value);
	}

	inline static int32_t get_offset_of_text_8() { return static_cast<int32_t>(offsetof(AspGenerator_t2810561191, ___text_8)); }
	inline StringBuilder_t1712802186 * get_text_8() const { return ___text_8; }
	inline StringBuilder_t1712802186 ** get_address_of_text_8() { return &___text_8; }
	inline void set_text_8(StringBuilder_t1712802186 * value)
	{
		___text_8 = value;
		Il2CppCodeGenWriteBarrier(&___text_8, value);
	}

	inline static int32_t get_offset_of_rootBuilder_9() { return static_cast<int32_t>(offsetof(AspGenerator_t2810561191, ___rootBuilder_9)); }
	inline RootBuilder_t1594119744 * get_rootBuilder_9() const { return ___rootBuilder_9; }
	inline RootBuilder_t1594119744 ** get_address_of_rootBuilder_9() { return &___rootBuilder_9; }
	inline void set_rootBuilder_9(RootBuilder_t1594119744 * value)
	{
		___rootBuilder_9 = value;
		Il2CppCodeGenWriteBarrier(&___rootBuilder_9, value);
	}

	inline static int32_t get_offset_of_inScript_10() { return static_cast<int32_t>(offsetof(AspGenerator_t2810561191, ___inScript_10)); }
	inline bool get_inScript_10() const { return ___inScript_10; }
	inline bool* get_address_of_inScript_10() { return &___inScript_10; }
	inline void set_inScript_10(bool value)
	{
		___inScript_10 = value;
	}

	inline static int32_t get_offset_of_javascript_11() { return static_cast<int32_t>(offsetof(AspGenerator_t2810561191, ___javascript_11)); }
	inline bool get_javascript_11() const { return ___javascript_11; }
	inline bool* get_address_of_javascript_11() { return &___javascript_11; }
	inline void set_javascript_11(bool value)
	{
		___javascript_11 = value;
	}

	inline static int32_t get_offset_of_ignore_text_12() { return static_cast<int32_t>(offsetof(AspGenerator_t2810561191, ___ignore_text_12)); }
	inline bool get_ignore_text_12() const { return ___ignore_text_12; }
	inline bool* get_address_of_ignore_text_12() { return &___ignore_text_12; }
	inline void set_ignore_text_12(bool value)
	{
		___ignore_text_12 = value;
	}

	inline static int32_t get_offset_of_location_13() { return static_cast<int32_t>(offsetof(AspGenerator_t2810561191, ___location_13)); }
	inline Il2CppObject * get_location_13() const { return ___location_13; }
	inline Il2CppObject ** get_address_of_location_13() { return &___location_13; }
	inline void set_location_13(Il2CppObject * value)
	{
		___location_13 = value;
		Il2CppCodeGenWriteBarrier(&___location_13, value);
	}

	inline static int32_t get_offset_of_isApplication_14() { return static_cast<int32_t>(offsetof(AspGenerator_t2810561191, ___isApplication_14)); }
	inline bool get_isApplication_14() const { return ___isApplication_14; }
	inline bool* get_address_of_isApplication_14() { return &___isApplication_14; }
	inline void set_isApplication_14(bool value)
	{
		___isApplication_14 = value;
	}

	inline static int32_t get_offset_of_tagInnerText_15() { return static_cast<int32_t>(offsetof(AspGenerator_t2810561191, ___tagInnerText_15)); }
	inline StringBuilder_t1712802186 * get_tagInnerText_15() const { return ___tagInnerText_15; }
	inline StringBuilder_t1712802186 ** get_address_of_tagInnerText_15() { return &___tagInnerText_15; }
	inline void set_tagInnerText_15(StringBuilder_t1712802186 * value)
	{
		___tagInnerText_15 = value;
		Il2CppCodeGenWriteBarrier(&___tagInnerText_15, value);
	}

	inline static int32_t get_offset_of_inForm_17() { return static_cast<int32_t>(offsetof(AspGenerator_t2810561191, ___inForm_17)); }
	inline bool get_inForm_17() const { return ___inForm_17; }
	inline bool* get_address_of_inForm_17() { return &___inForm_17; }
	inline void set_inForm_17(bool value)
	{
		___inForm_17 = value;
	}

	inline static int32_t get_offset_of_useOtherTags_18() { return static_cast<int32_t>(offsetof(AspGenerator_t2810561191, ___useOtherTags_18)); }
	inline bool get_useOtherTags_18() const { return ___useOtherTags_18; }
	inline bool* get_address_of_useOtherTags_18() { return &___useOtherTags_18; }
	inline void set_useOtherTags_18(bool value)
	{
		___useOtherTags_18 = value;
	}

	inline static int32_t get_offset_of_lastTag_19() { return static_cast<int32_t>(offsetof(AspGenerator_t2810561191, ___lastTag_19)); }
	inline int32_t get_lastTag_19() const { return ___lastTag_19; }
	inline int32_t* get_address_of_lastTag_19() { return &___lastTag_19; }
	inline void set_lastTag_19(int32_t value)
	{
		___lastTag_19 = value;
	}

	inline static int32_t get_offset_of_componentFoundry_20() { return static_cast<int32_t>(offsetof(AspGenerator_t2810561191, ___componentFoundry_20)); }
	inline AspComponentFoundry_t3087970636 * get_componentFoundry_20() const { return ___componentFoundry_20; }
	inline AspComponentFoundry_t3087970636 ** get_address_of_componentFoundry_20() { return &___componentFoundry_20; }
	inline void set_componentFoundry_20(AspComponentFoundry_t3087970636 * value)
	{
		___componentFoundry_20 = value;
		Il2CppCodeGenWriteBarrier(&___componentFoundry_20, value);
	}

	inline static int32_t get_offset_of_inputStream_21() { return static_cast<int32_t>(offsetof(AspGenerator_t2810561191, ___inputStream_21)); }
	inline Stream_t1273022909 * get_inputStream_21() const { return ___inputStream_21; }
	inline Stream_t1273022909 ** get_address_of_inputStream_21() { return &___inputStream_21; }
	inline void set_inputStream_21(Stream_t1273022909 * value)
	{
		___inputStream_21 = value;
		Il2CppCodeGenWriteBarrier(&___inputStream_21, value);
	}
};

struct AspGenerator_t2810561191_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex System.Web.Compilation.AspGenerator::DirectiveRegex
	Regex_t3657309853 * ___DirectiveRegex_0;
	// System.Text.RegularExpressions.Regex System.Web.Compilation.AspGenerator::runatServer
	Regex_t3657309853 * ___runatServer_1;
	// System.Text.RegularExpressions.Regex System.Web.Compilation.AspGenerator::endOfTag
	Regex_t3657309853 * ___endOfTag_2;
	// System.Text.RegularExpressions.Regex System.Web.Compilation.AspGenerator::expressionRegex
	Regex_t3657309853 * ___expressionRegex_3;
	// System.Text.RegularExpressions.Regex System.Web.Compilation.AspGenerator::clientCommentRegex
	Regex_t3657309853 * ___clientCommentRegex_4;
	// System.Collections.IDictionary System.Web.Compilation.AspGenerator::emptyHash
	Il2CppObject * ___emptyHash_16;

public:
	inline static int32_t get_offset_of_DirectiveRegex_0() { return static_cast<int32_t>(offsetof(AspGenerator_t2810561191_StaticFields, ___DirectiveRegex_0)); }
	inline Regex_t3657309853 * get_DirectiveRegex_0() const { return ___DirectiveRegex_0; }
	inline Regex_t3657309853 ** get_address_of_DirectiveRegex_0() { return &___DirectiveRegex_0; }
	inline void set_DirectiveRegex_0(Regex_t3657309853 * value)
	{
		___DirectiveRegex_0 = value;
		Il2CppCodeGenWriteBarrier(&___DirectiveRegex_0, value);
	}

	inline static int32_t get_offset_of_runatServer_1() { return static_cast<int32_t>(offsetof(AspGenerator_t2810561191_StaticFields, ___runatServer_1)); }
	inline Regex_t3657309853 * get_runatServer_1() const { return ___runatServer_1; }
	inline Regex_t3657309853 ** get_address_of_runatServer_1() { return &___runatServer_1; }
	inline void set_runatServer_1(Regex_t3657309853 * value)
	{
		___runatServer_1 = value;
		Il2CppCodeGenWriteBarrier(&___runatServer_1, value);
	}

	inline static int32_t get_offset_of_endOfTag_2() { return static_cast<int32_t>(offsetof(AspGenerator_t2810561191_StaticFields, ___endOfTag_2)); }
	inline Regex_t3657309853 * get_endOfTag_2() const { return ___endOfTag_2; }
	inline Regex_t3657309853 ** get_address_of_endOfTag_2() { return &___endOfTag_2; }
	inline void set_endOfTag_2(Regex_t3657309853 * value)
	{
		___endOfTag_2 = value;
		Il2CppCodeGenWriteBarrier(&___endOfTag_2, value);
	}

	inline static int32_t get_offset_of_expressionRegex_3() { return static_cast<int32_t>(offsetof(AspGenerator_t2810561191_StaticFields, ___expressionRegex_3)); }
	inline Regex_t3657309853 * get_expressionRegex_3() const { return ___expressionRegex_3; }
	inline Regex_t3657309853 ** get_address_of_expressionRegex_3() { return &___expressionRegex_3; }
	inline void set_expressionRegex_3(Regex_t3657309853 * value)
	{
		___expressionRegex_3 = value;
		Il2CppCodeGenWriteBarrier(&___expressionRegex_3, value);
	}

	inline static int32_t get_offset_of_clientCommentRegex_4() { return static_cast<int32_t>(offsetof(AspGenerator_t2810561191_StaticFields, ___clientCommentRegex_4)); }
	inline Regex_t3657309853 * get_clientCommentRegex_4() const { return ___clientCommentRegex_4; }
	inline Regex_t3657309853 ** get_address_of_clientCommentRegex_4() { return &___clientCommentRegex_4; }
	inline void set_clientCommentRegex_4(Regex_t3657309853 * value)
	{
		___clientCommentRegex_4 = value;
		Il2CppCodeGenWriteBarrier(&___clientCommentRegex_4, value);
	}

	inline static int32_t get_offset_of_emptyHash_16() { return static_cast<int32_t>(offsetof(AspGenerator_t2810561191_StaticFields, ___emptyHash_16)); }
	inline Il2CppObject * get_emptyHash_16() const { return ___emptyHash_16; }
	inline Il2CppObject ** get_address_of_emptyHash_16() { return &___emptyHash_16; }
	inline void set_emptyHash_16(Il2CppObject * value)
	{
		___emptyHash_16 = value;
		Il2CppCodeGenWriteBarrier(&___emptyHash_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
