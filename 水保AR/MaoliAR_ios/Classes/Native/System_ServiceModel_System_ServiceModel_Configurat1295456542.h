﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.PeerResolverElement
struct  PeerResolverElement_t1295456542  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct PeerResolverElement_t1295456542_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.PeerResolverElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.PeerResolverElement::custom
	ConfigurationProperty_t3590861854 * ___custom_14;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.PeerResolverElement::mode
	ConfigurationProperty_t3590861854 * ___mode_15;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.PeerResolverElement::referral_policy
	ConfigurationProperty_t3590861854 * ___referral_policy_16;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(PeerResolverElement_t1295456542_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier(&___properties_13, value);
	}

	inline static int32_t get_offset_of_custom_14() { return static_cast<int32_t>(offsetof(PeerResolverElement_t1295456542_StaticFields, ___custom_14)); }
	inline ConfigurationProperty_t3590861854 * get_custom_14() const { return ___custom_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_custom_14() { return &___custom_14; }
	inline void set_custom_14(ConfigurationProperty_t3590861854 * value)
	{
		___custom_14 = value;
		Il2CppCodeGenWriteBarrier(&___custom_14, value);
	}

	inline static int32_t get_offset_of_mode_15() { return static_cast<int32_t>(offsetof(PeerResolverElement_t1295456542_StaticFields, ___mode_15)); }
	inline ConfigurationProperty_t3590861854 * get_mode_15() const { return ___mode_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_mode_15() { return &___mode_15; }
	inline void set_mode_15(ConfigurationProperty_t3590861854 * value)
	{
		___mode_15 = value;
		Il2CppCodeGenWriteBarrier(&___mode_15, value);
	}

	inline static int32_t get_offset_of_referral_policy_16() { return static_cast<int32_t>(offsetof(PeerResolverElement_t1295456542_StaticFields, ___referral_policy_16)); }
	inline ConfigurationProperty_t3590861854 * get_referral_policy_16() const { return ___referral_policy_16; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_referral_policy_16() { return &___referral_policy_16; }
	inline void set_referral_policy_16(ConfigurationProperty_t3590861854 * value)
	{
		___referral_policy_16 = value;
		Il2CppCodeGenWriteBarrier(&___referral_policy_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
