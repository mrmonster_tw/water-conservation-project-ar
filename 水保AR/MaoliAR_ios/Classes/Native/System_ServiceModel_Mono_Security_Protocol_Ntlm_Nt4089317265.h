﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Ntlm.NtlmTargetInformation
struct  NtlmTargetInformation_t4089317265  : public Il2CppObject
{
public:
	// System.String Mono.Security.Protocol.Ntlm.NtlmTargetInformation::_server
	String_t* ____server_0;
	// System.String Mono.Security.Protocol.Ntlm.NtlmTargetInformation::_domain
	String_t* ____domain_1;
	// System.String Mono.Security.Protocol.Ntlm.NtlmTargetInformation::_dns_host
	String_t* ____dns_host_2;
	// System.String Mono.Security.Protocol.Ntlm.NtlmTargetInformation::_dns_domain
	String_t* ____dns_domain_3;

public:
	inline static int32_t get_offset_of__server_0() { return static_cast<int32_t>(offsetof(NtlmTargetInformation_t4089317265, ____server_0)); }
	inline String_t* get__server_0() const { return ____server_0; }
	inline String_t** get_address_of__server_0() { return &____server_0; }
	inline void set__server_0(String_t* value)
	{
		____server_0 = value;
		Il2CppCodeGenWriteBarrier(&____server_0, value);
	}

	inline static int32_t get_offset_of__domain_1() { return static_cast<int32_t>(offsetof(NtlmTargetInformation_t4089317265, ____domain_1)); }
	inline String_t* get__domain_1() const { return ____domain_1; }
	inline String_t** get_address_of__domain_1() { return &____domain_1; }
	inline void set__domain_1(String_t* value)
	{
		____domain_1 = value;
		Il2CppCodeGenWriteBarrier(&____domain_1, value);
	}

	inline static int32_t get_offset_of__dns_host_2() { return static_cast<int32_t>(offsetof(NtlmTargetInformation_t4089317265, ____dns_host_2)); }
	inline String_t* get__dns_host_2() const { return ____dns_host_2; }
	inline String_t** get_address_of__dns_host_2() { return &____dns_host_2; }
	inline void set__dns_host_2(String_t* value)
	{
		____dns_host_2 = value;
		Il2CppCodeGenWriteBarrier(&____dns_host_2, value);
	}

	inline static int32_t get_offset_of__dns_domain_3() { return static_cast<int32_t>(offsetof(NtlmTargetInformation_t4089317265, ____dns_domain_3)); }
	inline String_t* get__dns_domain_3() const { return ____dns_domain_3; }
	inline String_t** get_address_of__dns_domain_3() { return &____dns_domain_3; }
	inline void set__dns_domain_3(String_t* value)
	{
		____dns_domain_3 = value;
		Il2CppCodeGenWriteBarrier(&____dns_domain_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
