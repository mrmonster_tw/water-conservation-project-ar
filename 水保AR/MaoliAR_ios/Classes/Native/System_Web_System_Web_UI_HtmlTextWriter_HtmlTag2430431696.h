﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Web_System_Web_UI_HtmlTextWriterTag4267392773.h"
#include "System_Web_System_Web_UI_HtmlTextWriter_TagType3523149326.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.HtmlTextWriter/HtmlTag
struct  HtmlTag_t2430431696  : public Il2CppObject
{
public:
	// System.Web.UI.HtmlTextWriterTag System.Web.UI.HtmlTextWriter/HtmlTag::key
	int32_t ___key_0;
	// System.String System.Web.UI.HtmlTextWriter/HtmlTag::name
	String_t* ___name_1;
	// System.Web.UI.HtmlTextWriter/TagType System.Web.UI.HtmlTextWriter/HtmlTag::tag_type
	int32_t ___tag_type_2;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(HtmlTag_t2430431696, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(HtmlTag_t2430431696, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_tag_type_2() { return static_cast<int32_t>(offsetof(HtmlTag_t2430431696, ___tag_type_2)); }
	inline int32_t get_tag_type_2() const { return ___tag_type_2; }
	inline int32_t* get_address_of_tag_type_2() { return &___tag_type_2; }
	inline void set_tag_type_2(int32_t value)
	{
		___tag_type_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
