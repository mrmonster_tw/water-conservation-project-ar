﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3156163955.h"

// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.CustomErrorsSection
struct  CustomErrorsSection_t402377375  : public ConfigurationSection_t3156163955
{
public:

public:
};

struct CustomErrorsSection_t402377375_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Web.Configuration.CustomErrorsSection::defaultRedirectProp
	ConfigurationProperty_t3590861854 * ___defaultRedirectProp_17;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.CustomErrorsSection::errorsProp
	ConfigurationProperty_t3590861854 * ___errorsProp_18;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.CustomErrorsSection::modeProp
	ConfigurationProperty_t3590861854 * ___modeProp_19;
	// System.Configuration.ConfigurationPropertyCollection System.Web.Configuration.CustomErrorsSection::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_20;

public:
	inline static int32_t get_offset_of_defaultRedirectProp_17() { return static_cast<int32_t>(offsetof(CustomErrorsSection_t402377375_StaticFields, ___defaultRedirectProp_17)); }
	inline ConfigurationProperty_t3590861854 * get_defaultRedirectProp_17() const { return ___defaultRedirectProp_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_defaultRedirectProp_17() { return &___defaultRedirectProp_17; }
	inline void set_defaultRedirectProp_17(ConfigurationProperty_t3590861854 * value)
	{
		___defaultRedirectProp_17 = value;
		Il2CppCodeGenWriteBarrier(&___defaultRedirectProp_17, value);
	}

	inline static int32_t get_offset_of_errorsProp_18() { return static_cast<int32_t>(offsetof(CustomErrorsSection_t402377375_StaticFields, ___errorsProp_18)); }
	inline ConfigurationProperty_t3590861854 * get_errorsProp_18() const { return ___errorsProp_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_errorsProp_18() { return &___errorsProp_18; }
	inline void set_errorsProp_18(ConfigurationProperty_t3590861854 * value)
	{
		___errorsProp_18 = value;
		Il2CppCodeGenWriteBarrier(&___errorsProp_18, value);
	}

	inline static int32_t get_offset_of_modeProp_19() { return static_cast<int32_t>(offsetof(CustomErrorsSection_t402377375_StaticFields, ___modeProp_19)); }
	inline ConfigurationProperty_t3590861854 * get_modeProp_19() const { return ___modeProp_19; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_modeProp_19() { return &___modeProp_19; }
	inline void set_modeProp_19(ConfigurationProperty_t3590861854 * value)
	{
		___modeProp_19 = value;
		Il2CppCodeGenWriteBarrier(&___modeProp_19, value);
	}

	inline static int32_t get_offset_of_properties_20() { return static_cast<int32_t>(offsetof(CustomErrorsSection_t402377375_StaticFields, ___properties_20)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_20() const { return ___properties_20; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_20() { return &___properties_20; }
	inline void set_properties_20(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_20 = value;
		Il2CppCodeGenWriteBarrier(&___properties_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
