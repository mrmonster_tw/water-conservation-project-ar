﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Channels.IChannelListener`1<System.ServiceModel.Channels.IDuplexSessionChannel>
struct IChannelListener_1_t4046276067;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.ServiceModel.Dispatcher.ListenerLoopManager
struct ListenerLoopManager_t4138186672;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.ListenerLoopManager/<CreateAcceptor>c__AnonStorey1C`1<System.ServiceModel.Channels.IDuplexSessionChannel>
struct  U3CCreateAcceptorU3Ec__AnonStorey1C_1_t1316270919  : public Il2CppObject
{
public:
	// System.ServiceModel.Channels.IChannelListener`1<TChannel> System.ServiceModel.Dispatcher.ListenerLoopManager/<CreateAcceptor>c__AnonStorey1C`1::r
	Il2CppObject* ___r_0;
	// System.AsyncCallback System.ServiceModel.Dispatcher.ListenerLoopManager/<CreateAcceptor>c__AnonStorey1C`1::callback
	AsyncCallback_t3962456242 * ___callback_1;
	// System.ServiceModel.Dispatcher.ListenerLoopManager System.ServiceModel.Dispatcher.ListenerLoopManager/<CreateAcceptor>c__AnonStorey1C`1::<>f__this
	ListenerLoopManager_t4138186672 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(U3CCreateAcceptorU3Ec__AnonStorey1C_1_t1316270919, ___r_0)); }
	inline Il2CppObject* get_r_0() const { return ___r_0; }
	inline Il2CppObject** get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(Il2CppObject* value)
	{
		___r_0 = value;
		Il2CppCodeGenWriteBarrier(&___r_0, value);
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(U3CCreateAcceptorU3Ec__AnonStorey1C_1_t1316270919, ___callback_1)); }
	inline AsyncCallback_t3962456242 * get_callback_1() const { return ___callback_1; }
	inline AsyncCallback_t3962456242 ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(AsyncCallback_t3962456242 * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier(&___callback_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CCreateAcceptorU3Ec__AnonStorey1C_1_t1316270919, ___U3CU3Ef__this_2)); }
	inline ListenerLoopManager_t4138186672 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline ListenerLoopManager_t4138186672 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(ListenerLoopManager_t4138186672 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
