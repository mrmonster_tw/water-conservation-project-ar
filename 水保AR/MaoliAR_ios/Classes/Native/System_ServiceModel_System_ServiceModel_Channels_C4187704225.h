﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Bi859993683.h"

// System.ServiceModel.Channels.BindingElementCollection
struct BindingElementCollection_t2456870521;
// System.ServiceModel.Channels.Binding
struct Binding_t859993683;
// System.ServiceModel.Channels.ISecurityCapabilities
struct ISecurityCapabilities_t4129699402;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.CustomBinding
struct  CustomBinding_t4187704225  : public Binding_t859993683
{
public:
	// System.ServiceModel.Channels.BindingElementCollection System.ServiceModel.Channels.CustomBinding::elements
	BindingElementCollection_t2456870521 * ___elements_6;
	// System.ServiceModel.Channels.Binding System.ServiceModel.Channels.CustomBinding::binding
	Binding_t859993683 * ___binding_7;
	// System.ServiceModel.Channels.ISecurityCapabilities System.ServiceModel.Channels.CustomBinding::security
	Il2CppObject * ___security_8;
	// System.String System.ServiceModel.Channels.CustomBinding::scheme
	String_t* ___scheme_9;

public:
	inline static int32_t get_offset_of_elements_6() { return static_cast<int32_t>(offsetof(CustomBinding_t4187704225, ___elements_6)); }
	inline BindingElementCollection_t2456870521 * get_elements_6() const { return ___elements_6; }
	inline BindingElementCollection_t2456870521 ** get_address_of_elements_6() { return &___elements_6; }
	inline void set_elements_6(BindingElementCollection_t2456870521 * value)
	{
		___elements_6 = value;
		Il2CppCodeGenWriteBarrier(&___elements_6, value);
	}

	inline static int32_t get_offset_of_binding_7() { return static_cast<int32_t>(offsetof(CustomBinding_t4187704225, ___binding_7)); }
	inline Binding_t859993683 * get_binding_7() const { return ___binding_7; }
	inline Binding_t859993683 ** get_address_of_binding_7() { return &___binding_7; }
	inline void set_binding_7(Binding_t859993683 * value)
	{
		___binding_7 = value;
		Il2CppCodeGenWriteBarrier(&___binding_7, value);
	}

	inline static int32_t get_offset_of_security_8() { return static_cast<int32_t>(offsetof(CustomBinding_t4187704225, ___security_8)); }
	inline Il2CppObject * get_security_8() const { return ___security_8; }
	inline Il2CppObject ** get_address_of_security_8() { return &___security_8; }
	inline void set_security_8(Il2CppObject * value)
	{
		___security_8 = value;
		Il2CppCodeGenWriteBarrier(&___security_8, value);
	}

	inline static int32_t get_offset_of_scheme_9() { return static_cast<int32_t>(offsetof(CustomBinding_t4187704225, ___scheme_9)); }
	inline String_t* get_scheme_9() const { return ___scheme_9; }
	inline String_t** get_address_of_scheme_9() { return &___scheme_9; }
	inline void set_scheme_9(String_t* value)
	{
		___scheme_9 = value;
		Il2CppCodeGenWriteBarrier(&___scheme_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
