﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BasicDemo
struct  BasicDemo_t403825695  : public MonoBehaviour_t3962482529
{
public:
	// System.String BasicDemo::fromArduino
	String_t* ___fromArduino_2;
	// System.String BasicDemo::stringToEdit
	String_t* ___stringToEdit_3;

public:
	inline static int32_t get_offset_of_fromArduino_2() { return static_cast<int32_t>(offsetof(BasicDemo_t403825695, ___fromArduino_2)); }
	inline String_t* get_fromArduino_2() const { return ___fromArduino_2; }
	inline String_t** get_address_of_fromArduino_2() { return &___fromArduino_2; }
	inline void set_fromArduino_2(String_t* value)
	{
		___fromArduino_2 = value;
		Il2CppCodeGenWriteBarrier(&___fromArduino_2, value);
	}

	inline static int32_t get_offset_of_stringToEdit_3() { return static_cast<int32_t>(offsetof(BasicDemo_t403825695, ___stringToEdit_3)); }
	inline String_t* get_stringToEdit_3() const { return ___stringToEdit_3; }
	inline String_t** get_address_of_stringToEdit_3() { return &___stringToEdit_3; }
	inline void set_stringToEdit_3(String_t* value)
	{
		___stringToEdit_3 = value;
		Il2CppCodeGenWriteBarrier(&___stringToEdit_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
