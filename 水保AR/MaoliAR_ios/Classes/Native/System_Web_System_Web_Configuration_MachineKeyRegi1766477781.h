﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.MachineKeyRegistryStorage
struct  MachineKeyRegistryStorage_t1766477781  : public Il2CppObject
{
public:

public:
};

struct MachineKeyRegistryStorage_t1766477781_StaticFields
{
public:
	// System.String System.Web.Configuration.MachineKeyRegistryStorage::keyEncryption
	String_t* ___keyEncryption_0;
	// System.String System.Web.Configuration.MachineKeyRegistryStorage::keyValidation
	String_t* ___keyValidation_1;

public:
	inline static int32_t get_offset_of_keyEncryption_0() { return static_cast<int32_t>(offsetof(MachineKeyRegistryStorage_t1766477781_StaticFields, ___keyEncryption_0)); }
	inline String_t* get_keyEncryption_0() const { return ___keyEncryption_0; }
	inline String_t** get_address_of_keyEncryption_0() { return &___keyEncryption_0; }
	inline void set_keyEncryption_0(String_t* value)
	{
		___keyEncryption_0 = value;
		Il2CppCodeGenWriteBarrier(&___keyEncryption_0, value);
	}

	inline static int32_t get_offset_of_keyValidation_1() { return static_cast<int32_t>(offsetof(MachineKeyRegistryStorage_t1766477781_StaticFields, ___keyValidation_1)); }
	inline String_t* get_keyValidation_1() const { return ___keyValidation_1; }
	inline String_t** get_address_of_keyValidation_1() { return &___keyValidation_1; }
	inline void set_keyValidation_1(String_t* value)
	{
		___keyValidation_1 = value;
		Il2CppCodeGenWriteBarrier(&___keyValidation_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
