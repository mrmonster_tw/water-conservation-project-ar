﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeMemberMethod3833357554.h"

// System.CodeDom.CodeExpressionCollection
struct CodeExpressionCollection_t2370433003;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeConstructor
struct  CodeConstructor_t1817072848  : public CodeMemberMethod_t3833357554
{
public:
	// System.CodeDom.CodeExpressionCollection System.CodeDom.CodeConstructor::baseConstructorArgs
	CodeExpressionCollection_t2370433003 * ___baseConstructorArgs_18;
	// System.CodeDom.CodeExpressionCollection System.CodeDom.CodeConstructor::chainedConstructorArgs
	CodeExpressionCollection_t2370433003 * ___chainedConstructorArgs_19;

public:
	inline static int32_t get_offset_of_baseConstructorArgs_18() { return static_cast<int32_t>(offsetof(CodeConstructor_t1817072848, ___baseConstructorArgs_18)); }
	inline CodeExpressionCollection_t2370433003 * get_baseConstructorArgs_18() const { return ___baseConstructorArgs_18; }
	inline CodeExpressionCollection_t2370433003 ** get_address_of_baseConstructorArgs_18() { return &___baseConstructorArgs_18; }
	inline void set_baseConstructorArgs_18(CodeExpressionCollection_t2370433003 * value)
	{
		___baseConstructorArgs_18 = value;
		Il2CppCodeGenWriteBarrier(&___baseConstructorArgs_18, value);
	}

	inline static int32_t get_offset_of_chainedConstructorArgs_19() { return static_cast<int32_t>(offsetof(CodeConstructor_t1817072848, ___chainedConstructorArgs_19)); }
	inline CodeExpressionCollection_t2370433003 * get_chainedConstructorArgs_19() const { return ___chainedConstructorArgs_19; }
	inline CodeExpressionCollection_t2370433003 ** get_address_of_chainedConstructorArgs_19() { return &___chainedConstructorArgs_19; }
	inline void set_chainedConstructorArgs_19(CodeExpressionCollection_t2370433003 * value)
	{
		___chainedConstructorArgs_19 = value;
		Il2CppCodeGenWriteBarrier(&___chainedConstructorArgs_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
