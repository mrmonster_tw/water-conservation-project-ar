﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_DateTime3738529785.h"
#include "mscorlib_System_TimeSpan881159249.h"
#include "System_Web_System_Web_Caching_CacheItemPriority3871792821.h"

// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Web.Caching.CacheDependency
struct CacheDependency_t311635210;
// System.Web.Caching.CacheItemRemovedCallback
struct CacheItemRemovedCallback_t3206551617;
// System.Threading.Timer
struct Timer_t716671026;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Caching.CacheItem
struct  CacheItem_t2069182237  : public Il2CppObject
{
public:
	// System.Object System.Web.Caching.CacheItem::Value
	Il2CppObject * ___Value_0;
	// System.String System.Web.Caching.CacheItem::Key
	String_t* ___Key_1;
	// System.Web.Caching.CacheDependency System.Web.Caching.CacheItem::Dependency
	CacheDependency_t311635210 * ___Dependency_2;
	// System.DateTime System.Web.Caching.CacheItem::AbsoluteExpiration
	DateTime_t3738529785  ___AbsoluteExpiration_3;
	// System.TimeSpan System.Web.Caching.CacheItem::SlidingExpiration
	TimeSpan_t881159249  ___SlidingExpiration_4;
	// System.Web.Caching.CacheItemPriority System.Web.Caching.CacheItem::Priority
	int32_t ___Priority_5;
	// System.Web.Caching.CacheItemRemovedCallback System.Web.Caching.CacheItem::OnRemoveCallback
	CacheItemRemovedCallback_t3206551617 * ___OnRemoveCallback_6;
	// System.DateTime System.Web.Caching.CacheItem::LastChange
	DateTime_t3738529785  ___LastChange_7;
	// System.Threading.Timer System.Web.Caching.CacheItem::Timer
	Timer_t716671026 * ___Timer_8;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(CacheItem_t2069182237, ___Value_0)); }
	inline Il2CppObject * get_Value_0() const { return ___Value_0; }
	inline Il2CppObject ** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(Il2CppObject * value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier(&___Value_0, value);
	}

	inline static int32_t get_offset_of_Key_1() { return static_cast<int32_t>(offsetof(CacheItem_t2069182237, ___Key_1)); }
	inline String_t* get_Key_1() const { return ___Key_1; }
	inline String_t** get_address_of_Key_1() { return &___Key_1; }
	inline void set_Key_1(String_t* value)
	{
		___Key_1 = value;
		Il2CppCodeGenWriteBarrier(&___Key_1, value);
	}

	inline static int32_t get_offset_of_Dependency_2() { return static_cast<int32_t>(offsetof(CacheItem_t2069182237, ___Dependency_2)); }
	inline CacheDependency_t311635210 * get_Dependency_2() const { return ___Dependency_2; }
	inline CacheDependency_t311635210 ** get_address_of_Dependency_2() { return &___Dependency_2; }
	inline void set_Dependency_2(CacheDependency_t311635210 * value)
	{
		___Dependency_2 = value;
		Il2CppCodeGenWriteBarrier(&___Dependency_2, value);
	}

	inline static int32_t get_offset_of_AbsoluteExpiration_3() { return static_cast<int32_t>(offsetof(CacheItem_t2069182237, ___AbsoluteExpiration_3)); }
	inline DateTime_t3738529785  get_AbsoluteExpiration_3() const { return ___AbsoluteExpiration_3; }
	inline DateTime_t3738529785 * get_address_of_AbsoluteExpiration_3() { return &___AbsoluteExpiration_3; }
	inline void set_AbsoluteExpiration_3(DateTime_t3738529785  value)
	{
		___AbsoluteExpiration_3 = value;
	}

	inline static int32_t get_offset_of_SlidingExpiration_4() { return static_cast<int32_t>(offsetof(CacheItem_t2069182237, ___SlidingExpiration_4)); }
	inline TimeSpan_t881159249  get_SlidingExpiration_4() const { return ___SlidingExpiration_4; }
	inline TimeSpan_t881159249 * get_address_of_SlidingExpiration_4() { return &___SlidingExpiration_4; }
	inline void set_SlidingExpiration_4(TimeSpan_t881159249  value)
	{
		___SlidingExpiration_4 = value;
	}

	inline static int32_t get_offset_of_Priority_5() { return static_cast<int32_t>(offsetof(CacheItem_t2069182237, ___Priority_5)); }
	inline int32_t get_Priority_5() const { return ___Priority_5; }
	inline int32_t* get_address_of_Priority_5() { return &___Priority_5; }
	inline void set_Priority_5(int32_t value)
	{
		___Priority_5 = value;
	}

	inline static int32_t get_offset_of_OnRemoveCallback_6() { return static_cast<int32_t>(offsetof(CacheItem_t2069182237, ___OnRemoveCallback_6)); }
	inline CacheItemRemovedCallback_t3206551617 * get_OnRemoveCallback_6() const { return ___OnRemoveCallback_6; }
	inline CacheItemRemovedCallback_t3206551617 ** get_address_of_OnRemoveCallback_6() { return &___OnRemoveCallback_6; }
	inline void set_OnRemoveCallback_6(CacheItemRemovedCallback_t3206551617 * value)
	{
		___OnRemoveCallback_6 = value;
		Il2CppCodeGenWriteBarrier(&___OnRemoveCallback_6, value);
	}

	inline static int32_t get_offset_of_LastChange_7() { return static_cast<int32_t>(offsetof(CacheItem_t2069182237, ___LastChange_7)); }
	inline DateTime_t3738529785  get_LastChange_7() const { return ___LastChange_7; }
	inline DateTime_t3738529785 * get_address_of_LastChange_7() { return &___LastChange_7; }
	inline void set_LastChange_7(DateTime_t3738529785  value)
	{
		___LastChange_7 = value;
	}

	inline static int32_t get_offset_of_Timer_8() { return static_cast<int32_t>(offsetof(CacheItem_t2069182237, ___Timer_8)); }
	inline Timer_t716671026 * get_Timer_8() const { return ___Timer_8; }
	inline Timer_t716671026 ** get_address_of_Timer_8() { return &___Timer_8; }
	inline void set_Timer_8(Timer_t716671026 * value)
	{
		___Timer_8 = value;
		Il2CppCodeGenWriteBarrier(&___Timer_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
