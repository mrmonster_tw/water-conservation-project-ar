﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_Mono_CodeGeneration_CodeExpres2163794278.h"

// System.Type
struct Type_t;
// Mono.CodeGeneration.CodeExpression
struct CodeExpression_t2163794278;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.CodeGeneration.CodeNewArray
struct  CodeNewArray_t3766519279  : public CodeExpression_t2163794278
{
public:
	// System.Type Mono.CodeGeneration.CodeNewArray::elemType
	Type_t * ___elemType_0;
	// Mono.CodeGeneration.CodeExpression Mono.CodeGeneration.CodeNewArray::size
	CodeExpression_t2163794278 * ___size_1;

public:
	inline static int32_t get_offset_of_elemType_0() { return static_cast<int32_t>(offsetof(CodeNewArray_t3766519279, ___elemType_0)); }
	inline Type_t * get_elemType_0() const { return ___elemType_0; }
	inline Type_t ** get_address_of_elemType_0() { return &___elemType_0; }
	inline void set_elemType_0(Type_t * value)
	{
		___elemType_0 = value;
		Il2CppCodeGenWriteBarrier(&___elemType_0, value);
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(CodeNewArray_t3766519279, ___size_1)); }
	inline CodeExpression_t2163794278 * get_size_1() const { return ___size_1; }
	inline CodeExpression_t2163794278 ** get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(CodeExpression_t2163794278 * value)
	{
		___size_1 = value;
		Il2CppCodeGenWriteBarrier(&___size_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
