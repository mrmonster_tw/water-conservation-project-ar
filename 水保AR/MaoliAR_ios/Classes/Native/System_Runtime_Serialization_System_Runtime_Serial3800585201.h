﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Runtime.Serialization.KnownTypeCollection
struct KnownTypeCollection_t3509697551;
// System.Runtime.Serialization.IDataContractSurrogate
struct IDataContractSurrogate_t3505381635;
// System.Collections.Hashtable
struct Hashtable_t1853889766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.XmlFormatterDeserializer
struct  XmlFormatterDeserializer_t3800585201  : public Il2CppObject
{
public:
	// System.Runtime.Serialization.KnownTypeCollection System.Runtime.Serialization.XmlFormatterDeserializer::types
	KnownTypeCollection_t3509697551 * ___types_0;
	// System.Runtime.Serialization.IDataContractSurrogate System.Runtime.Serialization.XmlFormatterDeserializer::surrogate
	Il2CppObject * ___surrogate_1;
	// System.Collections.Hashtable System.Runtime.Serialization.XmlFormatterDeserializer::references
	Hashtable_t1853889766 * ___references_2;

public:
	inline static int32_t get_offset_of_types_0() { return static_cast<int32_t>(offsetof(XmlFormatterDeserializer_t3800585201, ___types_0)); }
	inline KnownTypeCollection_t3509697551 * get_types_0() const { return ___types_0; }
	inline KnownTypeCollection_t3509697551 ** get_address_of_types_0() { return &___types_0; }
	inline void set_types_0(KnownTypeCollection_t3509697551 * value)
	{
		___types_0 = value;
		Il2CppCodeGenWriteBarrier(&___types_0, value);
	}

	inline static int32_t get_offset_of_surrogate_1() { return static_cast<int32_t>(offsetof(XmlFormatterDeserializer_t3800585201, ___surrogate_1)); }
	inline Il2CppObject * get_surrogate_1() const { return ___surrogate_1; }
	inline Il2CppObject ** get_address_of_surrogate_1() { return &___surrogate_1; }
	inline void set_surrogate_1(Il2CppObject * value)
	{
		___surrogate_1 = value;
		Il2CppCodeGenWriteBarrier(&___surrogate_1, value);
	}

	inline static int32_t get_offset_of_references_2() { return static_cast<int32_t>(offsetof(XmlFormatterDeserializer_t3800585201, ___references_2)); }
	inline Hashtable_t1853889766 * get_references_2() const { return ___references_2; }
	inline Hashtable_t1853889766 ** get_address_of_references_2() { return &___references_2; }
	inline void set_references_2(Hashtable_t1853889766 * value)
	{
		___references_2 = value;
		Il2CppCodeGenWriteBarrier(&___references_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
