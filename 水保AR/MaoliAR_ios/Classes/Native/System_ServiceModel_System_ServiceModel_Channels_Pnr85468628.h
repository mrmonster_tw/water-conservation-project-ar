﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_P1753925450.h"
#include "System_ServiceModel_System_ServiceModel_PeerResolv1552258726.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.PnrpPeerResolverBindingElement
struct  PnrpPeerResolverBindingElement_t85468628  : public PeerResolverBindingElement_t1753925450
{
public:
	// System.ServiceModel.PeerResolvers.PeerReferralPolicy System.ServiceModel.Channels.PnrpPeerResolverBindingElement::<ReferralPolicy>k__BackingField
	int32_t ___U3CReferralPolicyU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CReferralPolicyU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PnrpPeerResolverBindingElement_t85468628, ___U3CReferralPolicyU3Ek__BackingField_0)); }
	inline int32_t get_U3CReferralPolicyU3Ek__BackingField_0() const { return ___U3CReferralPolicyU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CReferralPolicyU3Ek__BackingField_0() { return &___U3CReferralPolicyU3Ek__BackingField_0; }
	inline void set_U3CReferralPolicyU3Ek__BackingField_0(int32_t value)
	{
		___U3CReferralPolicyU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
