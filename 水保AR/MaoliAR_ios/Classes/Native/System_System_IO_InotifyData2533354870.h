﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.IO.FileSystemWatcher
struct FileSystemWatcher_t416760199;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.InotifyData
struct  InotifyData_t2533354870  : public Il2CppObject
{
public:
	// System.IO.FileSystemWatcher System.IO.InotifyData::FSW
	FileSystemWatcher_t416760199 * ___FSW_0;
	// System.String System.IO.InotifyData::Directory
	String_t* ___Directory_1;
	// System.Int32 System.IO.InotifyData::Watch
	int32_t ___Watch_2;

public:
	inline static int32_t get_offset_of_FSW_0() { return static_cast<int32_t>(offsetof(InotifyData_t2533354870, ___FSW_0)); }
	inline FileSystemWatcher_t416760199 * get_FSW_0() const { return ___FSW_0; }
	inline FileSystemWatcher_t416760199 ** get_address_of_FSW_0() { return &___FSW_0; }
	inline void set_FSW_0(FileSystemWatcher_t416760199 * value)
	{
		___FSW_0 = value;
		Il2CppCodeGenWriteBarrier(&___FSW_0, value);
	}

	inline static int32_t get_offset_of_Directory_1() { return static_cast<int32_t>(offsetof(InotifyData_t2533354870, ___Directory_1)); }
	inline String_t* get_Directory_1() const { return ___Directory_1; }
	inline String_t** get_address_of_Directory_1() { return &___Directory_1; }
	inline void set_Directory_1(String_t* value)
	{
		___Directory_1 = value;
		Il2CppCodeGenWriteBarrier(&___Directory_1, value);
	}

	inline static int32_t get_offset_of_Watch_2() { return static_cast<int32_t>(offsetof(InotifyData_t2533354870, ___Watch_2)); }
	inline int32_t get_Watch_2() const { return ___Watch_2; }
	inline int32_t* get_address_of_Watch_2() { return &___Watch_2; }
	inline void set_Watch_2(int32_t value)
	{
		___Watch_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
