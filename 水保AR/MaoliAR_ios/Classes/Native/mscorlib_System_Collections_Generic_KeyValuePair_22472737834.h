﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"

// System.Web.Compilation.DefaultResourceProvider/ResourceManagerCacheKey
struct ResourceManagerCacheKey_t3697749236;
// System.Resources.ResourceManager
struct ResourceManager_t4037989559;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Web.Compilation.DefaultResourceProvider/ResourceManagerCacheKey,System.Resources.ResourceManager>
struct  KeyValuePair_2_t2472737834 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	ResourceManagerCacheKey_t3697749236 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	ResourceManager_t4037989559 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2472737834, ___key_0)); }
	inline ResourceManagerCacheKey_t3697749236 * get_key_0() const { return ___key_0; }
	inline ResourceManagerCacheKey_t3697749236 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(ResourceManagerCacheKey_t3697749236 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier(&___key_0, value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2472737834, ___value_1)); }
	inline ResourceManager_t4037989559 * get_value_1() const { return ___value_1; }
	inline ResourceManager_t4037989559 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(ResourceManager_t4037989559 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
