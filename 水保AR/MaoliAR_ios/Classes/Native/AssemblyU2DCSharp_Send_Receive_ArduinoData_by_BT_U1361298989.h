﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Send_Receive_ArduinoData_by_BT/<wait>c__Iterator1
struct  U3CwaitU3Ec__Iterator1_t1361298989  : public Il2CppObject
{
public:
	// System.Single Send_Receive_ArduinoData_by_BT/<wait>c__Iterator1::<nowTime>__0
	float ___U3CnowTimeU3E__0_0;
	// System.Single Send_Receive_ArduinoData_by_BT/<wait>c__Iterator1::time
	float ___time_1;
	// System.Object Send_Receive_ArduinoData_by_BT/<wait>c__Iterator1::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean Send_Receive_ArduinoData_by_BT/<wait>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 Send_Receive_ArduinoData_by_BT/<wait>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CnowTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CwaitU3Ec__Iterator1_t1361298989, ___U3CnowTimeU3E__0_0)); }
	inline float get_U3CnowTimeU3E__0_0() const { return ___U3CnowTimeU3E__0_0; }
	inline float* get_address_of_U3CnowTimeU3E__0_0() { return &___U3CnowTimeU3E__0_0; }
	inline void set_U3CnowTimeU3E__0_0(float value)
	{
		___U3CnowTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_time_1() { return static_cast<int32_t>(offsetof(U3CwaitU3Ec__Iterator1_t1361298989, ___time_1)); }
	inline float get_time_1() const { return ___time_1; }
	inline float* get_address_of_time_1() { return &___time_1; }
	inline void set_time_1(float value)
	{
		___time_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CwaitU3Ec__Iterator1_t1361298989, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CwaitU3Ec__Iterator1_t1361298989, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CwaitU3Ec__Iterator1_t1361298989, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
