﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.UI.StateBag
struct StateBag_t282928164;
// System.Web.UI.CssStyleCollection
struct CssStyleCollection_t493833185;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.AttributeCollection
struct  AttributeCollection_t3488369622  : public Il2CppObject
{
public:
	// System.Web.UI.StateBag System.Web.UI.AttributeCollection::bag
	StateBag_t282928164 * ___bag_0;
	// System.Web.UI.CssStyleCollection System.Web.UI.AttributeCollection::styleCollection
	CssStyleCollection_t493833185 * ___styleCollection_1;

public:
	inline static int32_t get_offset_of_bag_0() { return static_cast<int32_t>(offsetof(AttributeCollection_t3488369622, ___bag_0)); }
	inline StateBag_t282928164 * get_bag_0() const { return ___bag_0; }
	inline StateBag_t282928164 ** get_address_of_bag_0() { return &___bag_0; }
	inline void set_bag_0(StateBag_t282928164 * value)
	{
		___bag_0 = value;
		Il2CppCodeGenWriteBarrier(&___bag_0, value);
	}

	inline static int32_t get_offset_of_styleCollection_1() { return static_cast<int32_t>(offsetof(AttributeCollection_t3488369622, ___styleCollection_1)); }
	inline CssStyleCollection_t493833185 * get_styleCollection_1() const { return ___styleCollection_1; }
	inline CssStyleCollection_t493833185 ** get_address_of_styleCollection_1() { return &___styleCollection_1; }
	inline void set_styleCollection_1(CssStyleCollection_t493833185 * value)
	{
		___styleCollection_1 = value;
		Il2CppCodeGenWriteBarrier(&___styleCollection_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
