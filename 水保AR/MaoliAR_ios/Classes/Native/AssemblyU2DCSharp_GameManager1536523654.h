﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "AssemblyU2DCSharp_GameManager_device_state2449749031.h"
#include "AssemblyU2DCSharp_GameManager_GameState3370388119.h"

// GameManager
struct GameManager_t1536523654;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.UI.Text
struct Text_t1901882714;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager
struct  GameManager_t1536523654  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject GameManager::Bt
	GameObject_t1113636619 * ___Bt_3;
	// UnityEngine.UI.Text GameManager::bt_text
	Text_t1901882714 * ___bt_text_4;
	// GameManager/device_state GameManager::_device_state
	int32_t ____device_state_5;
	// GameManager/GameState GameManager::_state
	int32_t ____state_6;
	// System.Boolean GameManager::L01_workDown
	bool ___L01_workDown_7;
	// System.Boolean GameManager::L02_workDown
	bool ___L02_workDown_8;
	// System.Boolean GameManager::L03_workDown
	bool ___L03_workDown_9;
	// System.Boolean GameManager::L04_workDown
	bool ___L04_workDown_10;
	// System.Boolean GameManager::L05_workDown
	bool ___L05_workDown_11;
	// UnityEngine.GameObject GameManager::Image_UI_down
	GameObject_t1113636619 * ___Image_UI_down_12;
	// UnityEngine.GameObject GameManager::Image_UI_down_nophone
	GameObject_t1113636619 * ___Image_UI_down_nophone_13;
	// UnityEngine.GameObject GameManager::Win_Panal
	GameObject_t1113636619 * ___Win_Panal_14;
	// UnityEngine.GameObject GameManager::Lose_Panel
	GameObject_t1113636619 * ___Lose_Panel_15;
	// UnityEngine.GameObject GameManager::Win_Panal2
	GameObject_t1113636619 * ___Win_Panal2_16;
	// UnityEngine.GameObject GameManager::Lose_Panel2
	GameObject_t1113636619 * ___Lose_Panel2_17;

public:
	inline static int32_t get_offset_of_Bt_3() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___Bt_3)); }
	inline GameObject_t1113636619 * get_Bt_3() const { return ___Bt_3; }
	inline GameObject_t1113636619 ** get_address_of_Bt_3() { return &___Bt_3; }
	inline void set_Bt_3(GameObject_t1113636619 * value)
	{
		___Bt_3 = value;
		Il2CppCodeGenWriteBarrier(&___Bt_3, value);
	}

	inline static int32_t get_offset_of_bt_text_4() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___bt_text_4)); }
	inline Text_t1901882714 * get_bt_text_4() const { return ___bt_text_4; }
	inline Text_t1901882714 ** get_address_of_bt_text_4() { return &___bt_text_4; }
	inline void set_bt_text_4(Text_t1901882714 * value)
	{
		___bt_text_4 = value;
		Il2CppCodeGenWriteBarrier(&___bt_text_4, value);
	}

	inline static int32_t get_offset_of__device_state_5() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ____device_state_5)); }
	inline int32_t get__device_state_5() const { return ____device_state_5; }
	inline int32_t* get_address_of__device_state_5() { return &____device_state_5; }
	inline void set__device_state_5(int32_t value)
	{
		____device_state_5 = value;
	}

	inline static int32_t get_offset_of__state_6() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ____state_6)); }
	inline int32_t get__state_6() const { return ____state_6; }
	inline int32_t* get_address_of__state_6() { return &____state_6; }
	inline void set__state_6(int32_t value)
	{
		____state_6 = value;
	}

	inline static int32_t get_offset_of_L01_workDown_7() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___L01_workDown_7)); }
	inline bool get_L01_workDown_7() const { return ___L01_workDown_7; }
	inline bool* get_address_of_L01_workDown_7() { return &___L01_workDown_7; }
	inline void set_L01_workDown_7(bool value)
	{
		___L01_workDown_7 = value;
	}

	inline static int32_t get_offset_of_L02_workDown_8() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___L02_workDown_8)); }
	inline bool get_L02_workDown_8() const { return ___L02_workDown_8; }
	inline bool* get_address_of_L02_workDown_8() { return &___L02_workDown_8; }
	inline void set_L02_workDown_8(bool value)
	{
		___L02_workDown_8 = value;
	}

	inline static int32_t get_offset_of_L03_workDown_9() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___L03_workDown_9)); }
	inline bool get_L03_workDown_9() const { return ___L03_workDown_9; }
	inline bool* get_address_of_L03_workDown_9() { return &___L03_workDown_9; }
	inline void set_L03_workDown_9(bool value)
	{
		___L03_workDown_9 = value;
	}

	inline static int32_t get_offset_of_L04_workDown_10() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___L04_workDown_10)); }
	inline bool get_L04_workDown_10() const { return ___L04_workDown_10; }
	inline bool* get_address_of_L04_workDown_10() { return &___L04_workDown_10; }
	inline void set_L04_workDown_10(bool value)
	{
		___L04_workDown_10 = value;
	}

	inline static int32_t get_offset_of_L05_workDown_11() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___L05_workDown_11)); }
	inline bool get_L05_workDown_11() const { return ___L05_workDown_11; }
	inline bool* get_address_of_L05_workDown_11() { return &___L05_workDown_11; }
	inline void set_L05_workDown_11(bool value)
	{
		___L05_workDown_11 = value;
	}

	inline static int32_t get_offset_of_Image_UI_down_12() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___Image_UI_down_12)); }
	inline GameObject_t1113636619 * get_Image_UI_down_12() const { return ___Image_UI_down_12; }
	inline GameObject_t1113636619 ** get_address_of_Image_UI_down_12() { return &___Image_UI_down_12; }
	inline void set_Image_UI_down_12(GameObject_t1113636619 * value)
	{
		___Image_UI_down_12 = value;
		Il2CppCodeGenWriteBarrier(&___Image_UI_down_12, value);
	}

	inline static int32_t get_offset_of_Image_UI_down_nophone_13() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___Image_UI_down_nophone_13)); }
	inline GameObject_t1113636619 * get_Image_UI_down_nophone_13() const { return ___Image_UI_down_nophone_13; }
	inline GameObject_t1113636619 ** get_address_of_Image_UI_down_nophone_13() { return &___Image_UI_down_nophone_13; }
	inline void set_Image_UI_down_nophone_13(GameObject_t1113636619 * value)
	{
		___Image_UI_down_nophone_13 = value;
		Il2CppCodeGenWriteBarrier(&___Image_UI_down_nophone_13, value);
	}

	inline static int32_t get_offset_of_Win_Panal_14() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___Win_Panal_14)); }
	inline GameObject_t1113636619 * get_Win_Panal_14() const { return ___Win_Panal_14; }
	inline GameObject_t1113636619 ** get_address_of_Win_Panal_14() { return &___Win_Panal_14; }
	inline void set_Win_Panal_14(GameObject_t1113636619 * value)
	{
		___Win_Panal_14 = value;
		Il2CppCodeGenWriteBarrier(&___Win_Panal_14, value);
	}

	inline static int32_t get_offset_of_Lose_Panel_15() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___Lose_Panel_15)); }
	inline GameObject_t1113636619 * get_Lose_Panel_15() const { return ___Lose_Panel_15; }
	inline GameObject_t1113636619 ** get_address_of_Lose_Panel_15() { return &___Lose_Panel_15; }
	inline void set_Lose_Panel_15(GameObject_t1113636619 * value)
	{
		___Lose_Panel_15 = value;
		Il2CppCodeGenWriteBarrier(&___Lose_Panel_15, value);
	}

	inline static int32_t get_offset_of_Win_Panal2_16() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___Win_Panal2_16)); }
	inline GameObject_t1113636619 * get_Win_Panal2_16() const { return ___Win_Panal2_16; }
	inline GameObject_t1113636619 ** get_address_of_Win_Panal2_16() { return &___Win_Panal2_16; }
	inline void set_Win_Panal2_16(GameObject_t1113636619 * value)
	{
		___Win_Panal2_16 = value;
		Il2CppCodeGenWriteBarrier(&___Win_Panal2_16, value);
	}

	inline static int32_t get_offset_of_Lose_Panel2_17() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___Lose_Panel2_17)); }
	inline GameObject_t1113636619 * get_Lose_Panel2_17() const { return ___Lose_Panel2_17; }
	inline GameObject_t1113636619 ** get_address_of_Lose_Panel2_17() { return &___Lose_Panel2_17; }
	inline void set_Lose_Panel2_17(GameObject_t1113636619 * value)
	{
		___Lose_Panel2_17 = value;
		Il2CppCodeGenWriteBarrier(&___Lose_Panel2_17, value);
	}
};

struct GameManager_t1536523654_StaticFields
{
public:
	// GameManager GameManager::Gmanager
	GameManager_t1536523654 * ___Gmanager_2;

public:
	inline static int32_t get_offset_of_Gmanager_2() { return static_cast<int32_t>(offsetof(GameManager_t1536523654_StaticFields, ___Gmanager_2)); }
	inline GameManager_t1536523654 * get_Gmanager_2() const { return ___Gmanager_2; }
	inline GameManager_t1536523654 ** get_address_of_Gmanager_2() { return &___Gmanager_2; }
	inline void set_Gmanager_2(GameManager_t1536523654 * value)
	{
		___Gmanager_2 = value;
		Il2CppCodeGenWriteBarrier(&___Gmanager_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
