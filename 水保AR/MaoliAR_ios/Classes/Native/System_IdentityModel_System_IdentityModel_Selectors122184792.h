﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Selector2079378378.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Selectors.UserNameSecurityTokenProvider
struct  UserNameSecurityTokenProvider_t122184792  : public SecurityTokenProvider_t2079378378
{
public:
	// System.String System.IdentityModel.Selectors.UserNameSecurityTokenProvider::user
	String_t* ___user_0;
	// System.String System.IdentityModel.Selectors.UserNameSecurityTokenProvider::pass
	String_t* ___pass_1;

public:
	inline static int32_t get_offset_of_user_0() { return static_cast<int32_t>(offsetof(UserNameSecurityTokenProvider_t122184792, ___user_0)); }
	inline String_t* get_user_0() const { return ___user_0; }
	inline String_t** get_address_of_user_0() { return &___user_0; }
	inline void set_user_0(String_t* value)
	{
		___user_0 = value;
		Il2CppCodeGenWriteBarrier(&___user_0, value);
	}

	inline static int32_t get_offset_of_pass_1() { return static_cast<int32_t>(offsetof(UserNameSecurityTokenProvider_t122184792, ___pass_1)); }
	inline String_t* get_pass_1() const { return ___pass_1; }
	inline String_t** get_address_of_pass_1() { return &___pass_1; }
	inline void set_pass_1(String_t* value)
	{
		___pass_1 = value;
		Il2CppCodeGenWriteBarrier(&___pass_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
