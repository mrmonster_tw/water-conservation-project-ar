﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIScrollView_OnDragNotification1437737811.h"
#include "AssemblyU2DCSharp_UIShowControlScheme428458459.h"
#include "AssemblyU2DCSharp_UISlider886033014.h"
#include "AssemblyU2DCSharp_UISlider_Direction3048248572.h"
#include "AssemblyU2DCSharp_UISoundVolume313711476.h"
#include "AssemblyU2DCSharp_UITable3168834800.h"
#include "AssemblyU2DCSharp_UITable_OnReposition3913508630.h"
#include "AssemblyU2DCSharp_UITable_Direction2487117792.h"
#include "AssemblyU2DCSharp_UITable_Sorting2823944879.h"
#include "AssemblyU2DCSharp_UIToggle4192126258.h"
#include "AssemblyU2DCSharp_UIToggle_Validate3702293971.h"
#include "AssemblyU2DCSharp_UIToggledComponents772955118.h"
#include "AssemblyU2DCSharp_UIToggledObjects3502557910.h"
#include "AssemblyU2DCSharp_UIWidgetContainer30162560.h"
#include "AssemblyU2DCSharp_UIWrapContent1188558554.h"
#include "AssemblyU2DCSharp_UIWrapContent_OnInitializeItem992046894.h"
#include "AssemblyU2DCSharp_ActiveAnimation3475256642.h"
#include "AssemblyU2DCSharp_AnimationOrTween_Trigger3745258312.h"
#include "AssemblyU2DCSharp_AnimationOrTween_Direction2061188385.h"
#include "AssemblyU2DCSharp_AnimationOrTween_EnableCondition1125033030.h"
#include "AssemblyU2DCSharp_AnimationOrTween_DisableConditio2151257518.h"
#include "AssemblyU2DCSharp_BMFont2757936676.h"
#include "AssemblyU2DCSharp_BMGlyph3344884546.h"
#include "AssemblyU2DCSharp_BMSymbol1586058841.h"
#include "AssemblyU2DCSharp_ByteReader1539670756.h"
#include "AssemblyU2DCSharp_EventDelegate2738326060.h"
#include "AssemblyU2DCSharp_EventDelegate_Parameter2966927026.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback3139336517.h"
#include "AssemblyU2DCSharp_Localization2163216738.h"
#include "AssemblyU2DCSharp_Localization_LoadFunction2078002637.h"
#include "AssemblyU2DCSharp_Localization_OnLocalizeNotificat3391620158.h"
#include "AssemblyU2DCSharp_NGUIDebug787955914.h"
#include "AssemblyU2DCSharp_NGUIMath3937908296.h"
#include "AssemblyU2DCSharp_NGUIText3089182085.h"
#include "AssemblyU2DCSharp_NGUIText_Alignment3228070485.h"
#include "AssemblyU2DCSharp_NGUIText_SymbolStyle3792107337.h"
#include "AssemblyU2DCSharp_NGUIText_GlyphInfo1020792323.h"
#include "AssemblyU2DCSharp_NGUITools1206951095.h"
#include "AssemblyU2DCSharp_PropertyBinding828139262.h"
#include "AssemblyU2DCSharp_PropertyBinding_UpdateCondition3398770213.h"
#include "AssemblyU2DCSharp_PropertyBinding_Direction1681948056.h"
#include "AssemblyU2DCSharp_PropertyReference223937415.h"
#include "AssemblyU2DCSharp_RealTime4034823134.h"
#include "AssemblyU2DCSharp_SpringPanel277350554.h"
#include "AssemblyU2DCSharp_SpringPanel_OnFinished3778785451.h"
#include "AssemblyU2DCSharp_UIBasicSprite1521297657.h"
#include "AssemblyU2DCSharp_UIBasicSprite_Type4088629396.h"
#include "AssemblyU2DCSharp_UIBasicSprite_FillDirection904769527.h"
#include "AssemblyU2DCSharp_UIBasicSprite_AdvancedType3940519926.h"
#include "AssemblyU2DCSharp_UIBasicSprite_Flip2552321477.h"
#include "AssemblyU2DCSharp_UIDrawCall1293405319.h"
#include "AssemblyU2DCSharp_UIDrawCall_Clipping1109313910.h"
#include "AssemblyU2DCSharp_UIDrawCall_OnRenderCallback133425655.h"
#include "AssemblyU2DCSharp_UIEventListener1665237878.h"
#include "AssemblyU2DCSharp_UIEventListener_VoidDelegate3914127870.h"
#include "AssemblyU2DCSharp_UIEventListener_BoolDelegate3089012064.h"
#include "AssemblyU2DCSharp_UIEventListener_FloatDelegate1747458064.h"
#include "AssemblyU2DCSharp_UIEventListener_VectorDelegate1966661092.h"
#include "AssemblyU2DCSharp_UIEventListener_ObjectDelegate2025096746.h"
#include "AssemblyU2DCSharp_UIEventListener_KeyCodeDelegate675056699.h"
#include "AssemblyU2DCSharp_UIGeometry1059483952.h"
#include "AssemblyU2DCSharp_UIRect2875960382.h"
#include "AssemblyU2DCSharp_UIRect_AnchorPoint1754718329.h"
#include "AssemblyU2DCSharp_UIRect_AnchorUpdate1570184075.h"
#include "AssemblyU2DCSharp_UISnapshotPoint2982659727.h"
#include "AssemblyU2DCSharp_UIWidget3538521925.h"
#include "AssemblyU2DCSharp_UIWidget_Pivot1798046373.h"
#include "AssemblyU2DCSharp_UIWidget_OnDimensionsChanged3101921181.h"
#include "AssemblyU2DCSharp_UIWidget_OnPostFillCallback2835645043.h"
#include "AssemblyU2DCSharp_UIWidget_AspectRatioSource168813522.h"
#include "AssemblyU2DCSharp_UIWidget_HitCheck2300079615.h"
#include "AssemblyU2DCSharp_AnimatedAlpha1840762679.h"
#include "AssemblyU2DCSharp_AnimatedColor3276574810.h"
#include "AssemblyU2DCSharp_AnimatedWidget1381166569.h"
#include "AssemblyU2DCSharp_SpringPosition3478173108.h"
#include "AssemblyU2DCSharp_SpringPosition_OnFinished3364492952.h"
#include "AssemblyU2DCSharp_TweenAlpha3706845226.h"
#include "AssemblyU2DCSharp_TweenColor2112002648.h"
#include "AssemblyU2DCSharp_TweenFOV2203484580.h"
#include "AssemblyU2DCSharp_TweenHeight4009371699.h"
#include "AssemblyU2DCSharp_TweenOrthoSize2102937296.h"
#include "AssemblyU2DCSharp_TweenPosition1378762002.h"
#include "AssemblyU2DCSharp_TweenRotation3072670746.h"
#include "AssemblyU2DCSharp_TweenScale2539309033.h"
#include "AssemblyU2DCSharp_TweenTransform1195296467.h"
#include "AssemblyU2DCSharp_TweenVolume3718612080.h"
#include "AssemblyU2DCSharp_TweenWidth2861389279.h"
#include "AssemblyU2DCSharp_UITweener260334902.h"
#include "AssemblyU2DCSharp_UITweener_Method1730494418.h"
#include "AssemblyU2DCSharp_UITweener_Style3120619385.h"
#include "AssemblyU2DCSharp_UI2DSprite1366157572.h"
#include "AssemblyU2DCSharp_UI2DSpriteAnimation3056508403.h"
#include "AssemblyU2DCSharp_UIAnchor2527798900.h"
#include "AssemblyU2DCSharp_UIAnchor_Side3584783117.h"
#include "AssemblyU2DCSharp_UIAtlas3195533529.h"
#include "AssemblyU2DCSharp_UIAtlas_Sprite2895597119.h"
#include "AssemblyU2DCSharp_UIAtlas_Coordinates1880298793.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5900 = { sizeof (OnDragNotification_t1437737811), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5901 = { sizeof (UIShowControlScheme_t428458459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5901[4] = 
{
	UIShowControlScheme_t428458459::get_offset_of_target_2(),
	UIShowControlScheme_t428458459::get_offset_of_mouse_3(),
	UIShowControlScheme_t428458459::get_offset_of_touch_4(),
	UIShowControlScheme_t428458459::get_offset_of_controller_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5902 = { sizeof (UISlider_t886033014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5902[4] = 
{
	UISlider_t886033014::get_offset_of_foreground_15(),
	UISlider_t886033014::get_offset_of_rawValue_16(),
	UISlider_t886033014::get_offset_of_direction_17(),
	UISlider_t886033014::get_offset_of_mInverted_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5903 = { sizeof (Direction_t3048248572)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5903[4] = 
{
	Direction_t3048248572::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5904 = { sizeof (UISoundVolume_t313711476), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5905 = { sizeof (UITable_t3168834800), -1, sizeof(UITable_t3168834800_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5905[17] = 
{
	UITable_t3168834800::get_offset_of_columns_2(),
	UITable_t3168834800::get_offset_of_direction_3(),
	UITable_t3168834800::get_offset_of_sorting_4(),
	UITable_t3168834800::get_offset_of_pivot_5(),
	UITable_t3168834800::get_offset_of_cellAlignment_6(),
	UITable_t3168834800::get_offset_of_hideInactive_7(),
	UITable_t3168834800::get_offset_of_keepWithinPanel_8(),
	UITable_t3168834800::get_offset_of_padding_9(),
	UITable_t3168834800::get_offset_of_onReposition_10(),
	UITable_t3168834800::get_offset_of_onCustomSort_11(),
	UITable_t3168834800::get_offset_of_mPanel_12(),
	UITable_t3168834800::get_offset_of_mInitDone_13(),
	UITable_t3168834800::get_offset_of_mReposition_14(),
	UITable_t3168834800_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_15(),
	UITable_t3168834800_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_16(),
	UITable_t3168834800_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_17(),
	UITable_t3168834800_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5906 = { sizeof (OnReposition_t3913508630), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5907 = { sizeof (Direction_t2487117792)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5907[3] = 
{
	Direction_t2487117792::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5908 = { sizeof (Sorting_t2823944879)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5908[6] = 
{
	Sorting_t2823944879::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5909 = { sizeof (UIToggle_t4192126258), -1, sizeof(UIToggle_t4192126258_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5909[18] = 
{
	UIToggle_t4192126258_StaticFields::get_offset_of_list_2(),
	UIToggle_t4192126258_StaticFields::get_offset_of_current_3(),
	UIToggle_t4192126258::get_offset_of_group_4(),
	UIToggle_t4192126258::get_offset_of_activeSprite_5(),
	UIToggle_t4192126258::get_offset_of_activeAnimation_6(),
	UIToggle_t4192126258::get_offset_of_animator_7(),
	UIToggle_t4192126258::get_offset_of_startsActive_8(),
	UIToggle_t4192126258::get_offset_of_instantTween_9(),
	UIToggle_t4192126258::get_offset_of_optionCanBeNone_10(),
	UIToggle_t4192126258::get_offset_of_onChange_11(),
	UIToggle_t4192126258::get_offset_of_validator_12(),
	UIToggle_t4192126258::get_offset_of_checkSprite_13(),
	UIToggle_t4192126258::get_offset_of_checkAnimation_14(),
	UIToggle_t4192126258::get_offset_of_eventReceiver_15(),
	UIToggle_t4192126258::get_offset_of_functionName_16(),
	UIToggle_t4192126258::get_offset_of_startsChecked_17(),
	UIToggle_t4192126258::get_offset_of_mIsActive_18(),
	UIToggle_t4192126258::get_offset_of_mStarted_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5910 = { sizeof (Validate_t3702293971), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5911 = { sizeof (UIToggledComponents_t772955118), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5911[4] = 
{
	UIToggledComponents_t772955118::get_offset_of_activate_2(),
	UIToggledComponents_t772955118::get_offset_of_deactivate_3(),
	UIToggledComponents_t772955118::get_offset_of_target_4(),
	UIToggledComponents_t772955118::get_offset_of_inverse_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5912 = { sizeof (UIToggledObjects_t3502557910), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5912[4] = 
{
	UIToggledObjects_t3502557910::get_offset_of_activate_2(),
	UIToggledObjects_t3502557910::get_offset_of_deactivate_3(),
	UIToggledObjects_t3502557910::get_offset_of_target_4(),
	UIToggledObjects_t3502557910::get_offset_of_inverse_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5913 = { sizeof (UIWidgetContainer_t30162560), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5914 = { sizeof (UIWrapContent_t1188558554), -1, sizeof(UIWrapContent_t1188558554_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5914[14] = 
{
	UIWrapContent_t1188558554::get_offset_of_itemSize_2(),
	UIWrapContent_t1188558554::get_offset_of_cullContent_3(),
	UIWrapContent_t1188558554::get_offset_of_minIndex_4(),
	UIWrapContent_t1188558554::get_offset_of_maxIndex_5(),
	UIWrapContent_t1188558554::get_offset_of_onInitializeItem_6(),
	UIWrapContent_t1188558554::get_offset_of_mTrans_7(),
	UIWrapContent_t1188558554::get_offset_of_mPanel_8(),
	UIWrapContent_t1188558554::get_offset_of_mScroll_9(),
	UIWrapContent_t1188558554::get_offset_of_mHorizontal_10(),
	UIWrapContent_t1188558554::get_offset_of_mFirstTime_11(),
	UIWrapContent_t1188558554::get_offset_of_mChildren_12(),
	UIWrapContent_t1188558554_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_13(),
	UIWrapContent_t1188558554_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_14(),
	UIWrapContent_t1188558554_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5915 = { sizeof (OnInitializeItem_t992046894), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5916 = { sizeof (ActiveAnimation_t3475256642), -1, sizeof(ActiveAnimation_t3475256642_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5916[10] = 
{
	ActiveAnimation_t3475256642_StaticFields::get_offset_of_current_2(),
	ActiveAnimation_t3475256642::get_offset_of_onFinished_3(),
	ActiveAnimation_t3475256642::get_offset_of_eventReceiver_4(),
	ActiveAnimation_t3475256642::get_offset_of_callWhenFinished_5(),
	ActiveAnimation_t3475256642::get_offset_of_mAnim_6(),
	ActiveAnimation_t3475256642::get_offset_of_mLastDirection_7(),
	ActiveAnimation_t3475256642::get_offset_of_mDisableDirection_8(),
	ActiveAnimation_t3475256642::get_offset_of_mNotify_9(),
	ActiveAnimation_t3475256642::get_offset_of_mAnimator_10(),
	ActiveAnimation_t3475256642::get_offset_of_mClip_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5917 = { sizeof (Trigger_t3745258312)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5917[15] = 
{
	Trigger_t3745258312::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5918 = { sizeof (Direction_t2061188385)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5918[4] = 
{
	Direction_t2061188385::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5919 = { sizeof (EnableCondition_t1125033030)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5919[4] = 
{
	EnableCondition_t1125033030::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5920 = { sizeof (DisableCondition_t2151257518)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5920[4] = 
{
	DisableCondition_t2151257518::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5921 = { sizeof (BMFont_t2757936676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5921[7] = 
{
	BMFont_t2757936676::get_offset_of_mSize_0(),
	BMFont_t2757936676::get_offset_of_mBase_1(),
	BMFont_t2757936676::get_offset_of_mWidth_2(),
	BMFont_t2757936676::get_offset_of_mHeight_3(),
	BMFont_t2757936676::get_offset_of_mSpriteName_4(),
	BMFont_t2757936676::get_offset_of_mSaved_5(),
	BMFont_t2757936676::get_offset_of_mDict_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5922 = { sizeof (BMGlyph_t3344884546), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5922[10] = 
{
	BMGlyph_t3344884546::get_offset_of_index_0(),
	BMGlyph_t3344884546::get_offset_of_x_1(),
	BMGlyph_t3344884546::get_offset_of_y_2(),
	BMGlyph_t3344884546::get_offset_of_width_3(),
	BMGlyph_t3344884546::get_offset_of_height_4(),
	BMGlyph_t3344884546::get_offset_of_offsetX_5(),
	BMGlyph_t3344884546::get_offset_of_offsetY_6(),
	BMGlyph_t3344884546::get_offset_of_advance_7(),
	BMGlyph_t3344884546::get_offset_of_channel_8(),
	BMGlyph_t3344884546::get_offset_of_kerning_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5923 = { sizeof (BMSymbol_t1586058841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5923[11] = 
{
	BMSymbol_t1586058841::get_offset_of_sequence_0(),
	BMSymbol_t1586058841::get_offset_of_spriteName_1(),
	BMSymbol_t1586058841::get_offset_of_mSprite_2(),
	BMSymbol_t1586058841::get_offset_of_mIsValid_3(),
	BMSymbol_t1586058841::get_offset_of_mLength_4(),
	BMSymbol_t1586058841::get_offset_of_mOffsetX_5(),
	BMSymbol_t1586058841::get_offset_of_mOffsetY_6(),
	BMSymbol_t1586058841::get_offset_of_mWidth_7(),
	BMSymbol_t1586058841::get_offset_of_mHeight_8(),
	BMSymbol_t1586058841::get_offset_of_mAdvance_9(),
	BMSymbol_t1586058841::get_offset_of_mUV_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5924 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5924[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5925 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5926 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5926[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5927 = { sizeof (ByteReader_t1539670756), -1, sizeof(ByteReader_t1539670756_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5927[3] = 
{
	ByteReader_t1539670756::get_offset_of_mBuffer_0(),
	ByteReader_t1539670756::get_offset_of_mOffset_1(),
	ByteReader_t1539670756_StaticFields::get_offset_of_mTemp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5928 = { sizeof (EventDelegate_t2738326060), -1, sizeof(EventDelegate_t2738326060_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5928[11] = 
{
	EventDelegate_t2738326060::get_offset_of_mTarget_0(),
	EventDelegate_t2738326060::get_offset_of_mMethodName_1(),
	EventDelegate_t2738326060::get_offset_of_mParameters_2(),
	EventDelegate_t2738326060::get_offset_of_oneShot_3(),
	EventDelegate_t2738326060::get_offset_of_mCachedCallback_4(),
	EventDelegate_t2738326060::get_offset_of_mRawDelegate_5(),
	EventDelegate_t2738326060::get_offset_of_mCached_6(),
	EventDelegate_t2738326060::get_offset_of_mMethod_7(),
	EventDelegate_t2738326060::get_offset_of_mParameterInfos_8(),
	EventDelegate_t2738326060::get_offset_of_mArgs_9(),
	EventDelegate_t2738326060_StaticFields::get_offset_of_s_Hash_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5929 = { sizeof (Parameter_t2966927026), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5929[7] = 
{
	Parameter_t2966927026::get_offset_of_obj_0(),
	Parameter_t2966927026::get_offset_of_field_1(),
	Parameter_t2966927026::get_offset_of_mValue_2(),
	Parameter_t2966927026::get_offset_of_expectedType_3(),
	Parameter_t2966927026::get_offset_of_cached_4(),
	Parameter_t2966927026::get_offset_of_propInfo_5(),
	Parameter_t2966927026::get_offset_of_fieldInfo_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5930 = { sizeof (Callback_t3139336517), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5931 = { sizeof (Localization_t2163216738), -1, sizeof(Localization_t2163216738_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5931[10] = 
{
	Localization_t2163216738_StaticFields::get_offset_of_loadFunction_0(),
	Localization_t2163216738_StaticFields::get_offset_of_onLocalize_1(),
	Localization_t2163216738_StaticFields::get_offset_of_localizationHasBeenSet_2(),
	Localization_t2163216738_StaticFields::get_offset_of_mLanguages_3(),
	Localization_t2163216738_StaticFields::get_offset_of_mOldDictionary_4(),
	Localization_t2163216738_StaticFields::get_offset_of_mDictionary_5(),
	Localization_t2163216738_StaticFields::get_offset_of_mReplacement_6(),
	Localization_t2163216738_StaticFields::get_offset_of_mLanguageIndex_7(),
	Localization_t2163216738_StaticFields::get_offset_of_mLanguage_8(),
	Localization_t2163216738_StaticFields::get_offset_of_mMerging_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5932 = { sizeof (LoadFunction_t2078002637), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5933 = { sizeof (OnLocalizeNotification_t3391620158), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5934 = { sizeof (NGUIDebug_t787955914), -1, sizeof(NGUIDebug_t787955914_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5934[3] = 
{
	NGUIDebug_t787955914_StaticFields::get_offset_of_mRayDebug_2(),
	NGUIDebug_t787955914_StaticFields::get_offset_of_mLines_3(),
	NGUIDebug_t787955914_StaticFields::get_offset_of_mInstance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5935 = { sizeof (NGUIMath_t3937908296), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5936 = { sizeof (NGUIText_t3089182085), -1, sizeof(NGUIText_t3089182085_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5936[35] = 
{
	NGUIText_t3089182085_StaticFields::get_offset_of_bitmapFont_0(),
	NGUIText_t3089182085_StaticFields::get_offset_of_dynamicFont_1(),
	NGUIText_t3089182085_StaticFields::get_offset_of_glyph_2(),
	NGUIText_t3089182085_StaticFields::get_offset_of_fontSize_3(),
	NGUIText_t3089182085_StaticFields::get_offset_of_fontScale_4(),
	NGUIText_t3089182085_StaticFields::get_offset_of_pixelDensity_5(),
	NGUIText_t3089182085_StaticFields::get_offset_of_fontStyle_6(),
	NGUIText_t3089182085_StaticFields::get_offset_of_alignment_7(),
	NGUIText_t3089182085_StaticFields::get_offset_of_tint_8(),
	NGUIText_t3089182085_StaticFields::get_offset_of_rectWidth_9(),
	NGUIText_t3089182085_StaticFields::get_offset_of_rectHeight_10(),
	NGUIText_t3089182085_StaticFields::get_offset_of_regionWidth_11(),
	NGUIText_t3089182085_StaticFields::get_offset_of_regionHeight_12(),
	NGUIText_t3089182085_StaticFields::get_offset_of_maxLines_13(),
	NGUIText_t3089182085_StaticFields::get_offset_of_gradient_14(),
	NGUIText_t3089182085_StaticFields::get_offset_of_gradientBottom_15(),
	NGUIText_t3089182085_StaticFields::get_offset_of_gradientTop_16(),
	NGUIText_t3089182085_StaticFields::get_offset_of_encoding_17(),
	NGUIText_t3089182085_StaticFields::get_offset_of_spacingX_18(),
	NGUIText_t3089182085_StaticFields::get_offset_of_spacingY_19(),
	NGUIText_t3089182085_StaticFields::get_offset_of_premultiply_20(),
	NGUIText_t3089182085_StaticFields::get_offset_of_symbolStyle_21(),
	NGUIText_t3089182085_StaticFields::get_offset_of_finalSize_22(),
	NGUIText_t3089182085_StaticFields::get_offset_of_finalSpacingX_23(),
	NGUIText_t3089182085_StaticFields::get_offset_of_finalLineHeight_24(),
	NGUIText_t3089182085_StaticFields::get_offset_of_baseline_25(),
	NGUIText_t3089182085_StaticFields::get_offset_of_useSymbols_26(),
	NGUIText_t3089182085_StaticFields::get_offset_of_mInvisible_27(),
	NGUIText_t3089182085_StaticFields::get_offset_of_mColors_28(),
	NGUIText_t3089182085_StaticFields::get_offset_of_mAlpha_29(),
	NGUIText_t3089182085_StaticFields::get_offset_of_mTempChar_30(),
	NGUIText_t3089182085_StaticFields::get_offset_of_mSizes_31(),
	NGUIText_t3089182085_StaticFields::get_offset_of_s_c0_32(),
	NGUIText_t3089182085_StaticFields::get_offset_of_s_c1_33(),
	NGUIText_t3089182085_StaticFields::get_offset_of_mBoldOffset_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5937 = { sizeof (Alignment_t3228070485)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5937[6] = 
{
	Alignment_t3228070485::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5938 = { sizeof (SymbolStyle_t3792107337)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5938[4] = 
{
	SymbolStyle_t3792107337::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5939 = { sizeof (GlyphInfo_t1020792323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5939[8] = 
{
	GlyphInfo_t1020792323::get_offset_of_v0_0(),
	GlyphInfo_t1020792323::get_offset_of_v1_1(),
	GlyphInfo_t1020792323::get_offset_of_u0_2(),
	GlyphInfo_t1020792323::get_offset_of_u1_3(),
	GlyphInfo_t1020792323::get_offset_of_u2_4(),
	GlyphInfo_t1020792323::get_offset_of_u3_5(),
	GlyphInfo_t1020792323::get_offset_of_advance_6(),
	GlyphInfo_t1020792323::get_offset_of_channel_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5940 = { sizeof (NGUITools_t1206951095), -1, sizeof(NGUITools_t1206951095_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5940[9] = 
{
	NGUITools_t1206951095_StaticFields::get_offset_of_mListener_0(),
	NGUITools_t1206951095_StaticFields::get_offset_of_mLoaded_1(),
	NGUITools_t1206951095_StaticFields::get_offset_of_mGlobalVolume_2(),
	NGUITools_t1206951095_StaticFields::get_offset_of_mLastTimestamp_3(),
	NGUITools_t1206951095_StaticFields::get_offset_of_mLastClip_4(),
	NGUITools_t1206951095_StaticFields::get_offset_of_mSides_5(),
	NGUITools_t1206951095_StaticFields::get_offset_of_keys_6(),
	NGUITools_t1206951095_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_7(),
	NGUITools_t1206951095_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5941 = { sizeof (PropertyBinding_t828139262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5941[6] = 
{
	PropertyBinding_t828139262::get_offset_of_source_2(),
	PropertyBinding_t828139262::get_offset_of_target_3(),
	PropertyBinding_t828139262::get_offset_of_direction_4(),
	PropertyBinding_t828139262::get_offset_of_update_5(),
	PropertyBinding_t828139262::get_offset_of_editMode_6(),
	PropertyBinding_t828139262::get_offset_of_mLastValue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5942 = { sizeof (UpdateCondition_t3398770213)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5942[5] = 
{
	UpdateCondition_t3398770213::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5943 = { sizeof (Direction_t1681948056)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5943[4] = 
{
	Direction_t1681948056::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5944 = { sizeof (PropertyReference_t223937415), -1, sizeof(PropertyReference_t223937415_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5944[5] = 
{
	PropertyReference_t223937415::get_offset_of_mTarget_0(),
	PropertyReference_t223937415::get_offset_of_mName_1(),
	PropertyReference_t223937415::get_offset_of_mField_2(),
	PropertyReference_t223937415::get_offset_of_mProperty_3(),
	PropertyReference_t223937415_StaticFields::get_offset_of_s_Hash_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5945 = { sizeof (RealTime_t4034823134), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5946 = { sizeof (SpringPanel_t277350554), -1, sizeof(SpringPanel_t277350554_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5946[7] = 
{
	SpringPanel_t277350554_StaticFields::get_offset_of_current_2(),
	SpringPanel_t277350554::get_offset_of_target_3(),
	SpringPanel_t277350554::get_offset_of_strength_4(),
	SpringPanel_t277350554::get_offset_of_onFinished_5(),
	SpringPanel_t277350554::get_offset_of_mPanel_6(),
	SpringPanel_t277350554::get_offset_of_mTrans_7(),
	SpringPanel_t277350554::get_offset_of_mDrag_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5947 = { sizeof (OnFinished_t3778785451), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5948 = { sizeof (UIBasicSprite_t1521297657), -1, sizeof(UIBasicSprite_t1521297657_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5948[14] = 
{
	UIBasicSprite_t1521297657::get_offset_of_mType_52(),
	UIBasicSprite_t1521297657::get_offset_of_mFillDirection_53(),
	UIBasicSprite_t1521297657::get_offset_of_mFillAmount_54(),
	UIBasicSprite_t1521297657::get_offset_of_mInvert_55(),
	UIBasicSprite_t1521297657::get_offset_of_mFlip_56(),
	UIBasicSprite_t1521297657::get_offset_of_mInnerUV_57(),
	UIBasicSprite_t1521297657::get_offset_of_mOuterUV_58(),
	UIBasicSprite_t1521297657::get_offset_of_centerType_59(),
	UIBasicSprite_t1521297657::get_offset_of_leftType_60(),
	UIBasicSprite_t1521297657::get_offset_of_rightType_61(),
	UIBasicSprite_t1521297657::get_offset_of_bottomType_62(),
	UIBasicSprite_t1521297657::get_offset_of_topType_63(),
	UIBasicSprite_t1521297657_StaticFields::get_offset_of_mTempPos_64(),
	UIBasicSprite_t1521297657_StaticFields::get_offset_of_mTempUVs_65(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5949 = { sizeof (Type_t4088629396)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5949[6] = 
{
	Type_t4088629396::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5950 = { sizeof (FillDirection_t904769527)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5950[6] = 
{
	FillDirection_t904769527::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5951 = { sizeof (AdvancedType_t3940519926)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5951[4] = 
{
	AdvancedType_t3940519926::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5952 = { sizeof (Flip_t2552321477)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5952[5] = 
{
	Flip_t2552321477::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5953 = { sizeof (UIDrawCall_t1293405319), -1, sizeof(UIDrawCall_t1293405319_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5953[35] = 
{
	UIDrawCall_t1293405319_StaticFields::get_offset_of_mActiveList_2(),
	UIDrawCall_t1293405319_StaticFields::get_offset_of_mInactiveList_3(),
	UIDrawCall_t1293405319::get_offset_of_widgetCount_4(),
	UIDrawCall_t1293405319::get_offset_of_depthStart_5(),
	UIDrawCall_t1293405319::get_offset_of_depthEnd_6(),
	UIDrawCall_t1293405319::get_offset_of_manager_7(),
	UIDrawCall_t1293405319::get_offset_of_panel_8(),
	UIDrawCall_t1293405319::get_offset_of_clipTexture_9(),
	UIDrawCall_t1293405319::get_offset_of_alwaysOnScreen_10(),
	UIDrawCall_t1293405319::get_offset_of_verts_11(),
	UIDrawCall_t1293405319::get_offset_of_norms_12(),
	UIDrawCall_t1293405319::get_offset_of_tans_13(),
	UIDrawCall_t1293405319::get_offset_of_uvs_14(),
	UIDrawCall_t1293405319::get_offset_of_cols_15(),
	UIDrawCall_t1293405319::get_offset_of_mMaterial_16(),
	UIDrawCall_t1293405319::get_offset_of_mTexture_17(),
	UIDrawCall_t1293405319::get_offset_of_mShader_18(),
	UIDrawCall_t1293405319::get_offset_of_mClipCount_19(),
	UIDrawCall_t1293405319::get_offset_of_mTrans_20(),
	UIDrawCall_t1293405319::get_offset_of_mMesh_21(),
	UIDrawCall_t1293405319::get_offset_of_mFilter_22(),
	UIDrawCall_t1293405319::get_offset_of_mRenderer_23(),
	UIDrawCall_t1293405319::get_offset_of_mDynamicMat_24(),
	UIDrawCall_t1293405319::get_offset_of_mIndices_25(),
	UIDrawCall_t1293405319::get_offset_of_mRebuildMat_26(),
	UIDrawCall_t1293405319::get_offset_of_mLegacyShader_27(),
	UIDrawCall_t1293405319::get_offset_of_mRenderQueue_28(),
	UIDrawCall_t1293405319::get_offset_of_mTriangles_29(),
	UIDrawCall_t1293405319::get_offset_of_isDirty_30(),
	UIDrawCall_t1293405319::get_offset_of_mTextureClip_31(),
	UIDrawCall_t1293405319::get_offset_of_onRender_32(),
	0,
	UIDrawCall_t1293405319_StaticFields::get_offset_of_mCache_34(),
	UIDrawCall_t1293405319_StaticFields::get_offset_of_ClipRange_35(),
	UIDrawCall_t1293405319_StaticFields::get_offset_of_ClipArgs_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5954 = { sizeof (Clipping_t1109313910)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5954[5] = 
{
	Clipping_t1109313910::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5955 = { sizeof (OnRenderCallback_t133425655), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5956 = { sizeof (UIEventListener_t1665237878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5956[16] = 
{
	UIEventListener_t1665237878::get_offset_of_parameter_2(),
	UIEventListener_t1665237878::get_offset_of_onSubmit_3(),
	UIEventListener_t1665237878::get_offset_of_onClick_4(),
	UIEventListener_t1665237878::get_offset_of_onDoubleClick_5(),
	UIEventListener_t1665237878::get_offset_of_onHover_6(),
	UIEventListener_t1665237878::get_offset_of_onPress_7(),
	UIEventListener_t1665237878::get_offset_of_onSelect_8(),
	UIEventListener_t1665237878::get_offset_of_onScroll_9(),
	UIEventListener_t1665237878::get_offset_of_onDragStart_10(),
	UIEventListener_t1665237878::get_offset_of_onDrag_11(),
	UIEventListener_t1665237878::get_offset_of_onDragOver_12(),
	UIEventListener_t1665237878::get_offset_of_onDragOut_13(),
	UIEventListener_t1665237878::get_offset_of_onDragEnd_14(),
	UIEventListener_t1665237878::get_offset_of_onDrop_15(),
	UIEventListener_t1665237878::get_offset_of_onKey_16(),
	UIEventListener_t1665237878::get_offset_of_onTooltip_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5957 = { sizeof (VoidDelegate_t3914127870), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5958 = { sizeof (BoolDelegate_t3089012064), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5959 = { sizeof (FloatDelegate_t1747458064), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5960 = { sizeof (VectorDelegate_t1966661092), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5961 = { sizeof (ObjectDelegate_t2025096746), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5962 = { sizeof (KeyCodeDelegate_t675056699), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5963 = { sizeof (UIGeometry_t1059483952), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5963[6] = 
{
	UIGeometry_t1059483952::get_offset_of_verts_0(),
	UIGeometry_t1059483952::get_offset_of_uvs_1(),
	UIGeometry_t1059483952::get_offset_of_cols_2(),
	UIGeometry_t1059483952::get_offset_of_mRtpVerts_3(),
	UIGeometry_t1059483952::get_offset_of_mRtpNormal_4(),
	UIGeometry_t1059483952::get_offset_of_mRtpTan_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5964 = { sizeof (UIRect_t2875960382), -1, sizeof(UIRect_t2875960382_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5964[20] = 
{
	UIRect_t2875960382::get_offset_of_leftAnchor_2(),
	UIRect_t2875960382::get_offset_of_rightAnchor_3(),
	UIRect_t2875960382::get_offset_of_bottomAnchor_4(),
	UIRect_t2875960382::get_offset_of_topAnchor_5(),
	UIRect_t2875960382::get_offset_of_updateAnchors_6(),
	UIRect_t2875960382::get_offset_of_mGo_7(),
	UIRect_t2875960382::get_offset_of_mTrans_8(),
	UIRect_t2875960382::get_offset_of_mChildren_9(),
	UIRect_t2875960382::get_offset_of_mChanged_10(),
	UIRect_t2875960382::get_offset_of_mStarted_11(),
	UIRect_t2875960382::get_offset_of_mParentFound_12(),
	UIRect_t2875960382::get_offset_of_mUpdateAnchors_13(),
	UIRect_t2875960382::get_offset_of_mUpdateFrame_14(),
	UIRect_t2875960382::get_offset_of_mAnchorsCached_15(),
	UIRect_t2875960382::get_offset_of_mRoot_16(),
	UIRect_t2875960382::get_offset_of_mParent_17(),
	UIRect_t2875960382::get_offset_of_mRootSet_18(),
	UIRect_t2875960382::get_offset_of_mCam_19(),
	UIRect_t2875960382::get_offset_of_finalAlpha_20(),
	UIRect_t2875960382_StaticFields::get_offset_of_mSides_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5965 = { sizeof (AnchorPoint_t1754718329), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5965[5] = 
{
	AnchorPoint_t1754718329::get_offset_of_target_0(),
	AnchorPoint_t1754718329::get_offset_of_relative_1(),
	AnchorPoint_t1754718329::get_offset_of_absolute_2(),
	AnchorPoint_t1754718329::get_offset_of_rect_3(),
	AnchorPoint_t1754718329::get_offset_of_targetCam_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5966 = { sizeof (AnchorUpdate_t1570184075)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5966[4] = 
{
	AnchorUpdate_t1570184075::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5967 = { sizeof (UISnapshotPoint_t2982659727), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5967[6] = 
{
	UISnapshotPoint_t2982659727::get_offset_of_isOrthographic_2(),
	UISnapshotPoint_t2982659727::get_offset_of_nearClip_3(),
	UISnapshotPoint_t2982659727::get_offset_of_farClip_4(),
	UISnapshotPoint_t2982659727::get_offset_of_fieldOfView_5(),
	UISnapshotPoint_t2982659727::get_offset_of_orthoSize_6(),
	UISnapshotPoint_t2982659727::get_offset_of_thumbnail_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5968 = { sizeof (UIWidget_t3538521925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5968[30] = 
{
	UIWidget_t3538521925::get_offset_of_mColor_22(),
	UIWidget_t3538521925::get_offset_of_mPivot_23(),
	UIWidget_t3538521925::get_offset_of_mWidth_24(),
	UIWidget_t3538521925::get_offset_of_mHeight_25(),
	UIWidget_t3538521925::get_offset_of_mDepth_26(),
	UIWidget_t3538521925::get_offset_of_onChange_27(),
	UIWidget_t3538521925::get_offset_of_onPostFill_28(),
	UIWidget_t3538521925::get_offset_of_mOnRender_29(),
	UIWidget_t3538521925::get_offset_of_autoResizeBoxCollider_30(),
	UIWidget_t3538521925::get_offset_of_hideIfOffScreen_31(),
	UIWidget_t3538521925::get_offset_of_keepAspectRatio_32(),
	UIWidget_t3538521925::get_offset_of_aspectRatio_33(),
	UIWidget_t3538521925::get_offset_of_hitCheck_34(),
	UIWidget_t3538521925::get_offset_of_panel_35(),
	UIWidget_t3538521925::get_offset_of_geometry_36(),
	UIWidget_t3538521925::get_offset_of_fillGeometry_37(),
	UIWidget_t3538521925::get_offset_of_mPlayMode_38(),
	UIWidget_t3538521925::get_offset_of_mDrawRegion_39(),
	UIWidget_t3538521925::get_offset_of_mLocalToPanel_40(),
	UIWidget_t3538521925::get_offset_of_mIsVisibleByAlpha_41(),
	UIWidget_t3538521925::get_offset_of_mIsVisibleByPanel_42(),
	UIWidget_t3538521925::get_offset_of_mIsInFront_43(),
	UIWidget_t3538521925::get_offset_of_mLastAlpha_44(),
	UIWidget_t3538521925::get_offset_of_mMoved_45(),
	UIWidget_t3538521925::get_offset_of_drawCall_46(),
	UIWidget_t3538521925::get_offset_of_mCorners_47(),
	UIWidget_t3538521925::get_offset_of_mAlphaFrameID_48(),
	UIWidget_t3538521925::get_offset_of_mMatrixFrame_49(),
	UIWidget_t3538521925::get_offset_of_mOldV0_50(),
	UIWidget_t3538521925::get_offset_of_mOldV1_51(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5969 = { sizeof (Pivot_t1798046373)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5969[10] = 
{
	Pivot_t1798046373::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5970 = { sizeof (OnDimensionsChanged_t3101921181), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5971 = { sizeof (OnPostFillCallback_t2835645043), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5972 = { sizeof (AspectRatioSource_t168813522)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5972[4] = 
{
	AspectRatioSource_t168813522::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5973 = { sizeof (HitCheck_t2300079615), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5974 = { sizeof (AnimatedAlpha_t1840762679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5974[3] = 
{
	AnimatedAlpha_t1840762679::get_offset_of_alpha_2(),
	AnimatedAlpha_t1840762679::get_offset_of_mWidget_3(),
	AnimatedAlpha_t1840762679::get_offset_of_mPanel_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5975 = { sizeof (AnimatedColor_t3276574810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5975[2] = 
{
	AnimatedColor_t3276574810::get_offset_of_color_2(),
	AnimatedColor_t3276574810::get_offset_of_mWidget_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5976 = { sizeof (AnimatedWidget_t1381166569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5976[3] = 
{
	AnimatedWidget_t1381166569::get_offset_of_width_2(),
	AnimatedWidget_t1381166569::get_offset_of_height_3(),
	AnimatedWidget_t1381166569::get_offset_of_mWidget_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5977 = { sizeof (SpringPosition_t3478173108), -1, sizeof(SpringPosition_t3478173108_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5977[12] = 
{
	SpringPosition_t3478173108_StaticFields::get_offset_of_current_2(),
	SpringPosition_t3478173108::get_offset_of_target_3(),
	SpringPosition_t3478173108::get_offset_of_strength_4(),
	SpringPosition_t3478173108::get_offset_of_worldSpace_5(),
	SpringPosition_t3478173108::get_offset_of_ignoreTimeScale_6(),
	SpringPosition_t3478173108::get_offset_of_updateScrollView_7(),
	SpringPosition_t3478173108::get_offset_of_onFinished_8(),
	SpringPosition_t3478173108::get_offset_of_eventReceiver_9(),
	SpringPosition_t3478173108::get_offset_of_callWhenFinished_10(),
	SpringPosition_t3478173108::get_offset_of_mTrans_11(),
	SpringPosition_t3478173108::get_offset_of_mThreshold_12(),
	SpringPosition_t3478173108::get_offset_of_mSv_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5978 = { sizeof (OnFinished_t3364492952), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5979 = { sizeof (TweenAlpha_t3706845226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5979[6] = 
{
	TweenAlpha_t3706845226::get_offset_of_from_20(),
	TweenAlpha_t3706845226::get_offset_of_to_21(),
	TweenAlpha_t3706845226::get_offset_of_mCached_22(),
	TweenAlpha_t3706845226::get_offset_of_mRect_23(),
	TweenAlpha_t3706845226::get_offset_of_mMat_24(),
	TweenAlpha_t3706845226::get_offset_of_mSr_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5980 = { sizeof (TweenColor_t2112002648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5980[7] = 
{
	TweenColor_t2112002648::get_offset_of_from_20(),
	TweenColor_t2112002648::get_offset_of_to_21(),
	TweenColor_t2112002648::get_offset_of_mCached_22(),
	TweenColor_t2112002648::get_offset_of_mWidget_23(),
	TweenColor_t2112002648::get_offset_of_mMat_24(),
	TweenColor_t2112002648::get_offset_of_mLight_25(),
	TweenColor_t2112002648::get_offset_of_mSr_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5981 = { sizeof (TweenFOV_t2203484580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5981[3] = 
{
	TweenFOV_t2203484580::get_offset_of_from_20(),
	TweenFOV_t2203484580::get_offset_of_to_21(),
	TweenFOV_t2203484580::get_offset_of_mCam_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5982 = { sizeof (TweenHeight_t4009371699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5982[5] = 
{
	TweenHeight_t4009371699::get_offset_of_from_20(),
	TweenHeight_t4009371699::get_offset_of_to_21(),
	TweenHeight_t4009371699::get_offset_of_updateTable_22(),
	TweenHeight_t4009371699::get_offset_of_mWidget_23(),
	TweenHeight_t4009371699::get_offset_of_mTable_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5983 = { sizeof (TweenOrthoSize_t2102937296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5983[3] = 
{
	TweenOrthoSize_t2102937296::get_offset_of_from_20(),
	TweenOrthoSize_t2102937296::get_offset_of_to_21(),
	TweenOrthoSize_t2102937296::get_offset_of_mCam_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5984 = { sizeof (TweenPosition_t1378762002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5984[5] = 
{
	TweenPosition_t1378762002::get_offset_of_from_20(),
	TweenPosition_t1378762002::get_offset_of_to_21(),
	TweenPosition_t1378762002::get_offset_of_worldSpace_22(),
	TweenPosition_t1378762002::get_offset_of_mTrans_23(),
	TweenPosition_t1378762002::get_offset_of_mRect_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5985 = { sizeof (TweenRotation_t3072670746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5985[4] = 
{
	TweenRotation_t3072670746::get_offset_of_from_20(),
	TweenRotation_t3072670746::get_offset_of_to_21(),
	TweenRotation_t3072670746::get_offset_of_quaternionLerp_22(),
	TweenRotation_t3072670746::get_offset_of_mTrans_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5986 = { sizeof (TweenScale_t2539309033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5986[5] = 
{
	TweenScale_t2539309033::get_offset_of_from_20(),
	TweenScale_t2539309033::get_offset_of_to_21(),
	TweenScale_t2539309033::get_offset_of_updateTable_22(),
	TweenScale_t2539309033::get_offset_of_mTrans_23(),
	TweenScale_t2539309033::get_offset_of_mTable_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5987 = { sizeof (TweenTransform_t1195296467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5987[7] = 
{
	TweenTransform_t1195296467::get_offset_of_from_20(),
	TweenTransform_t1195296467::get_offset_of_to_21(),
	TweenTransform_t1195296467::get_offset_of_parentWhenFinished_22(),
	TweenTransform_t1195296467::get_offset_of_mTrans_23(),
	TweenTransform_t1195296467::get_offset_of_mPos_24(),
	TweenTransform_t1195296467::get_offset_of_mRot_25(),
	TweenTransform_t1195296467::get_offset_of_mScale_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5988 = { sizeof (TweenVolume_t3718612080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5988[3] = 
{
	TweenVolume_t3718612080::get_offset_of_from_20(),
	TweenVolume_t3718612080::get_offset_of_to_21(),
	TweenVolume_t3718612080::get_offset_of_mSource_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5989 = { sizeof (TweenWidth_t2861389279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5989[5] = 
{
	TweenWidth_t2861389279::get_offset_of_from_20(),
	TweenWidth_t2861389279::get_offset_of_to_21(),
	TweenWidth_t2861389279::get_offset_of_updateTable_22(),
	TweenWidth_t2861389279::get_offset_of_mWidget_23(),
	TweenWidth_t2861389279::get_offset_of_mTable_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5990 = { sizeof (UITweener_t260334902), -1, sizeof(UITweener_t260334902_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5990[18] = 
{
	UITweener_t260334902_StaticFields::get_offset_of_current_2(),
	UITweener_t260334902::get_offset_of_method_3(),
	UITweener_t260334902::get_offset_of_style_4(),
	UITweener_t260334902::get_offset_of_animationCurve_5(),
	UITweener_t260334902::get_offset_of_ignoreTimeScale_6(),
	UITweener_t260334902::get_offset_of_delay_7(),
	UITweener_t260334902::get_offset_of_duration_8(),
	UITweener_t260334902::get_offset_of_steeperCurves_9(),
	UITweener_t260334902::get_offset_of_tweenGroup_10(),
	UITweener_t260334902::get_offset_of_onFinished_11(),
	UITweener_t260334902::get_offset_of_eventReceiver_12(),
	UITweener_t260334902::get_offset_of_callWhenFinished_13(),
	UITweener_t260334902::get_offset_of_mStarted_14(),
	UITweener_t260334902::get_offset_of_mStartTime_15(),
	UITweener_t260334902::get_offset_of_mDuration_16(),
	UITweener_t260334902::get_offset_of_mAmountPerDelta_17(),
	UITweener_t260334902::get_offset_of_mFactor_18(),
	UITweener_t260334902::get_offset_of_mTemp_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5991 = { sizeof (Method_t1730494418)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5991[7] = 
{
	Method_t1730494418::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5992 = { sizeof (Style_t3120619385)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5992[4] = 
{
	Style_t3120619385::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5993 = { sizeof (UI2DSprite_t1366157572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5993[8] = 
{
	UI2DSprite_t1366157572::get_offset_of_mSprite_66(),
	UI2DSprite_t1366157572::get_offset_of_mMat_67(),
	UI2DSprite_t1366157572::get_offset_of_mShader_68(),
	UI2DSprite_t1366157572::get_offset_of_mBorder_69(),
	UI2DSprite_t1366157572::get_offset_of_mFixedAspect_70(),
	UI2DSprite_t1366157572::get_offset_of_mPixelSize_71(),
	UI2DSprite_t1366157572::get_offset_of_nextSprite_72(),
	UI2DSprite_t1366157572::get_offset_of_mPMA_73(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5994 = { sizeof (UI2DSpriteAnimation_t3056508403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5994[8] = 
{
	UI2DSpriteAnimation_t3056508403::get_offset_of_framerate_2(),
	UI2DSpriteAnimation_t3056508403::get_offset_of_ignoreTimeScale_3(),
	UI2DSpriteAnimation_t3056508403::get_offset_of_loop_4(),
	UI2DSpriteAnimation_t3056508403::get_offset_of_frames_5(),
	UI2DSpriteAnimation_t3056508403::get_offset_of_mUnitySprite_6(),
	UI2DSpriteAnimation_t3056508403::get_offset_of_mNguiSprite_7(),
	UI2DSpriteAnimation_t3056508403::get_offset_of_mIndex_8(),
	UI2DSpriteAnimation_t3056508403::get_offset_of_mUpdate_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5995 = { sizeof (UIAnchor_t2527798900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5995[12] = 
{
	UIAnchor_t2527798900::get_offset_of_uiCamera_2(),
	UIAnchor_t2527798900::get_offset_of_container_3(),
	UIAnchor_t2527798900::get_offset_of_side_4(),
	UIAnchor_t2527798900::get_offset_of_runOnlyOnce_5(),
	UIAnchor_t2527798900::get_offset_of_relativeOffset_6(),
	UIAnchor_t2527798900::get_offset_of_pixelOffset_7(),
	UIAnchor_t2527798900::get_offset_of_widgetContainer_8(),
	UIAnchor_t2527798900::get_offset_of_mTrans_9(),
	UIAnchor_t2527798900::get_offset_of_mAnim_10(),
	UIAnchor_t2527798900::get_offset_of_mRect_11(),
	UIAnchor_t2527798900::get_offset_of_mRoot_12(),
	UIAnchor_t2527798900::get_offset_of_mStarted_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5996 = { sizeof (Side_t3584783117)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5996[10] = 
{
	Side_t3584783117::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5997 = { sizeof (UIAtlas_t3195533529), -1, sizeof(UIAtlas_t3195533529_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5997[9] = 
{
	UIAtlas_t3195533529::get_offset_of_material_2(),
	UIAtlas_t3195533529::get_offset_of_mSprites_3(),
	UIAtlas_t3195533529::get_offset_of_mPixelSize_4(),
	UIAtlas_t3195533529::get_offset_of_mReplacement_5(),
	UIAtlas_t3195533529::get_offset_of_mCoordinates_6(),
	UIAtlas_t3195533529::get_offset_of_sprites_7(),
	UIAtlas_t3195533529::get_offset_of_mPMA_8(),
	UIAtlas_t3195533529::get_offset_of_mSpriteIndices_9(),
	UIAtlas_t3195533529_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5998 = { sizeof (Sprite_t2895597119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5998[8] = 
{
	Sprite_t2895597119::get_offset_of_name_0(),
	Sprite_t2895597119::get_offset_of_outer_1(),
	Sprite_t2895597119::get_offset_of_inner_2(),
	Sprite_t2895597119::get_offset_of_rotated_3(),
	Sprite_t2895597119::get_offset_of_paddingLeft_4(),
	Sprite_t2895597119::get_offset_of_paddingRight_5(),
	Sprite_t2895597119::get_offset_of_paddingTop_6(),
	Sprite_t2895597119::get_offset_of_paddingBottom_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5999 = { sizeof (Coordinates_t1880298793)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5999[3] = 
{
	Coordinates_t1880298793::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
