﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lottery_Controller
struct  Lottery_Controller_t4098387144  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject Lottery_Controller::Mission_Get
	GameObject_t1113636619 * ___Mission_Get_2;
	// UnityEngine.GameObject Lottery_Controller::Mission_Lose
	GameObject_t1113636619 * ___Mission_Lose_3;
	// UnityEngine.GameObject Lottery_Controller::Confirm_Lottery
	GameObject_t1113636619 * ___Confirm_Lottery_4;
	// UnityEngine.GameObject Lottery_Controller::Cancel_lottery
	GameObject_t1113636619 * ___Cancel_lottery_5;
	// UnityEngine.GameObject Lottery_Controller::lottery_Win
	GameObject_t1113636619 * ___lottery_Win_6;
	// UnityEngine.GameObject Lottery_Controller::lottery_Lose
	GameObject_t1113636619 * ___lottery_Lose_7;
	// UnityEngine.GameObject[] Lottery_Controller::star_Images
	GameObjectU5BU5D_t3328599146* ___star_Images_8;
	// UnityEngine.GameObject Lottery_Controller::Parent_obj
	GameObject_t1113636619 * ___Parent_obj_9;
	// UnityEngine.GameObject Lottery_Controller::Shinning
	GameObject_t1113636619 * ___Shinning_10;
	// UnityEngine.GameObject[] Lottery_Controller::star_ImagesWIN
	GameObjectU5BU5D_t3328599146* ___star_ImagesWIN_11;
	// UnityEngine.GameObject Lottery_Controller::Parent_objWIN
	GameObject_t1113636619 * ___Parent_objWIN_12;
	// UnityEngine.GameObject Lottery_Controller::ShinningWIN
	GameObject_t1113636619 * ___ShinningWIN_13;
	// System.Single Lottery_Controller::Shinning_speed
	float ___Shinning_speed_14;

public:
	inline static int32_t get_offset_of_Mission_Get_2() { return static_cast<int32_t>(offsetof(Lottery_Controller_t4098387144, ___Mission_Get_2)); }
	inline GameObject_t1113636619 * get_Mission_Get_2() const { return ___Mission_Get_2; }
	inline GameObject_t1113636619 ** get_address_of_Mission_Get_2() { return &___Mission_Get_2; }
	inline void set_Mission_Get_2(GameObject_t1113636619 * value)
	{
		___Mission_Get_2 = value;
		Il2CppCodeGenWriteBarrier(&___Mission_Get_2, value);
	}

	inline static int32_t get_offset_of_Mission_Lose_3() { return static_cast<int32_t>(offsetof(Lottery_Controller_t4098387144, ___Mission_Lose_3)); }
	inline GameObject_t1113636619 * get_Mission_Lose_3() const { return ___Mission_Lose_3; }
	inline GameObject_t1113636619 ** get_address_of_Mission_Lose_3() { return &___Mission_Lose_3; }
	inline void set_Mission_Lose_3(GameObject_t1113636619 * value)
	{
		___Mission_Lose_3 = value;
		Il2CppCodeGenWriteBarrier(&___Mission_Lose_3, value);
	}

	inline static int32_t get_offset_of_Confirm_Lottery_4() { return static_cast<int32_t>(offsetof(Lottery_Controller_t4098387144, ___Confirm_Lottery_4)); }
	inline GameObject_t1113636619 * get_Confirm_Lottery_4() const { return ___Confirm_Lottery_4; }
	inline GameObject_t1113636619 ** get_address_of_Confirm_Lottery_4() { return &___Confirm_Lottery_4; }
	inline void set_Confirm_Lottery_4(GameObject_t1113636619 * value)
	{
		___Confirm_Lottery_4 = value;
		Il2CppCodeGenWriteBarrier(&___Confirm_Lottery_4, value);
	}

	inline static int32_t get_offset_of_Cancel_lottery_5() { return static_cast<int32_t>(offsetof(Lottery_Controller_t4098387144, ___Cancel_lottery_5)); }
	inline GameObject_t1113636619 * get_Cancel_lottery_5() const { return ___Cancel_lottery_5; }
	inline GameObject_t1113636619 ** get_address_of_Cancel_lottery_5() { return &___Cancel_lottery_5; }
	inline void set_Cancel_lottery_5(GameObject_t1113636619 * value)
	{
		___Cancel_lottery_5 = value;
		Il2CppCodeGenWriteBarrier(&___Cancel_lottery_5, value);
	}

	inline static int32_t get_offset_of_lottery_Win_6() { return static_cast<int32_t>(offsetof(Lottery_Controller_t4098387144, ___lottery_Win_6)); }
	inline GameObject_t1113636619 * get_lottery_Win_6() const { return ___lottery_Win_6; }
	inline GameObject_t1113636619 ** get_address_of_lottery_Win_6() { return &___lottery_Win_6; }
	inline void set_lottery_Win_6(GameObject_t1113636619 * value)
	{
		___lottery_Win_6 = value;
		Il2CppCodeGenWriteBarrier(&___lottery_Win_6, value);
	}

	inline static int32_t get_offset_of_lottery_Lose_7() { return static_cast<int32_t>(offsetof(Lottery_Controller_t4098387144, ___lottery_Lose_7)); }
	inline GameObject_t1113636619 * get_lottery_Lose_7() const { return ___lottery_Lose_7; }
	inline GameObject_t1113636619 ** get_address_of_lottery_Lose_7() { return &___lottery_Lose_7; }
	inline void set_lottery_Lose_7(GameObject_t1113636619 * value)
	{
		___lottery_Lose_7 = value;
		Il2CppCodeGenWriteBarrier(&___lottery_Lose_7, value);
	}

	inline static int32_t get_offset_of_star_Images_8() { return static_cast<int32_t>(offsetof(Lottery_Controller_t4098387144, ___star_Images_8)); }
	inline GameObjectU5BU5D_t3328599146* get_star_Images_8() const { return ___star_Images_8; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_star_Images_8() { return &___star_Images_8; }
	inline void set_star_Images_8(GameObjectU5BU5D_t3328599146* value)
	{
		___star_Images_8 = value;
		Il2CppCodeGenWriteBarrier(&___star_Images_8, value);
	}

	inline static int32_t get_offset_of_Parent_obj_9() { return static_cast<int32_t>(offsetof(Lottery_Controller_t4098387144, ___Parent_obj_9)); }
	inline GameObject_t1113636619 * get_Parent_obj_9() const { return ___Parent_obj_9; }
	inline GameObject_t1113636619 ** get_address_of_Parent_obj_9() { return &___Parent_obj_9; }
	inline void set_Parent_obj_9(GameObject_t1113636619 * value)
	{
		___Parent_obj_9 = value;
		Il2CppCodeGenWriteBarrier(&___Parent_obj_9, value);
	}

	inline static int32_t get_offset_of_Shinning_10() { return static_cast<int32_t>(offsetof(Lottery_Controller_t4098387144, ___Shinning_10)); }
	inline GameObject_t1113636619 * get_Shinning_10() const { return ___Shinning_10; }
	inline GameObject_t1113636619 ** get_address_of_Shinning_10() { return &___Shinning_10; }
	inline void set_Shinning_10(GameObject_t1113636619 * value)
	{
		___Shinning_10 = value;
		Il2CppCodeGenWriteBarrier(&___Shinning_10, value);
	}

	inline static int32_t get_offset_of_star_ImagesWIN_11() { return static_cast<int32_t>(offsetof(Lottery_Controller_t4098387144, ___star_ImagesWIN_11)); }
	inline GameObjectU5BU5D_t3328599146* get_star_ImagesWIN_11() const { return ___star_ImagesWIN_11; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_star_ImagesWIN_11() { return &___star_ImagesWIN_11; }
	inline void set_star_ImagesWIN_11(GameObjectU5BU5D_t3328599146* value)
	{
		___star_ImagesWIN_11 = value;
		Il2CppCodeGenWriteBarrier(&___star_ImagesWIN_11, value);
	}

	inline static int32_t get_offset_of_Parent_objWIN_12() { return static_cast<int32_t>(offsetof(Lottery_Controller_t4098387144, ___Parent_objWIN_12)); }
	inline GameObject_t1113636619 * get_Parent_objWIN_12() const { return ___Parent_objWIN_12; }
	inline GameObject_t1113636619 ** get_address_of_Parent_objWIN_12() { return &___Parent_objWIN_12; }
	inline void set_Parent_objWIN_12(GameObject_t1113636619 * value)
	{
		___Parent_objWIN_12 = value;
		Il2CppCodeGenWriteBarrier(&___Parent_objWIN_12, value);
	}

	inline static int32_t get_offset_of_ShinningWIN_13() { return static_cast<int32_t>(offsetof(Lottery_Controller_t4098387144, ___ShinningWIN_13)); }
	inline GameObject_t1113636619 * get_ShinningWIN_13() const { return ___ShinningWIN_13; }
	inline GameObject_t1113636619 ** get_address_of_ShinningWIN_13() { return &___ShinningWIN_13; }
	inline void set_ShinningWIN_13(GameObject_t1113636619 * value)
	{
		___ShinningWIN_13 = value;
		Il2CppCodeGenWriteBarrier(&___ShinningWIN_13, value);
	}

	inline static int32_t get_offset_of_Shinning_speed_14() { return static_cast<int32_t>(offsetof(Lottery_Controller_t4098387144, ___Shinning_speed_14)); }
	inline float get_Shinning_speed_14() const { return ___Shinning_speed_14; }
	inline float* get_address_of_Shinning_speed_14() { return &___Shinning_speed_14; }
	inline void set_Shinning_speed_14(float value)
	{
		___Shinning_speed_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
