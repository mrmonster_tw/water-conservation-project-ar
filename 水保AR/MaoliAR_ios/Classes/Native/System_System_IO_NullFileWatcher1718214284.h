﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.IO.IFileWatcher
struct IFileWatcher_t3899097327;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.NullFileWatcher
struct  NullFileWatcher_t1718214284  : public Il2CppObject
{
public:

public:
};

struct NullFileWatcher_t1718214284_StaticFields
{
public:
	// System.IO.IFileWatcher System.IO.NullFileWatcher::instance
	Il2CppObject * ___instance_0;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(NullFileWatcher_t1718214284_StaticFields, ___instance_0)); }
	inline Il2CppObject * get_instance_0() const { return ___instance_0; }
	inline Il2CppObject ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(Il2CppObject * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier(&___instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
