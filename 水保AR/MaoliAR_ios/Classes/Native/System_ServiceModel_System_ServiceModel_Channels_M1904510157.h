﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.IdentityModel.Selectors.SecurityTokenManager
struct SecurityTokenManager_t202133775;
// System.ServiceModel.Security.ChannelProtectionRequirements
struct ChannelProtectionRequirements_t2141883287;
// System.IdentityModel.Selectors.SecurityTokenSerializer
struct SecurityTokenSerializer_t972969391;
// System.ServiceModel.Channels.SecurityCapabilities
struct SecurityCapabilities_t1438243711;
// System.IdentityModel.Selectors.SecurityTokenAuthenticator
struct SecurityTokenAuthenticator_t1397312864;
// System.IdentityModel.Selectors.SecurityTokenResolver
struct SecurityTokenResolver_t3905425829;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.MessageSecurityBindingSupport
struct  MessageSecurityBindingSupport_t1904510157  : public Il2CppObject
{
public:
	// System.IdentityModel.Selectors.SecurityTokenManager System.ServiceModel.Channels.MessageSecurityBindingSupport::manager
	SecurityTokenManager_t202133775 * ___manager_0;
	// System.ServiceModel.Security.ChannelProtectionRequirements System.ServiceModel.Channels.MessageSecurityBindingSupport::requirements
	ChannelProtectionRequirements_t2141883287 * ___requirements_1;
	// System.IdentityModel.Selectors.SecurityTokenSerializer System.ServiceModel.Channels.MessageSecurityBindingSupport::serializer
	SecurityTokenSerializer_t972969391 * ___serializer_2;
	// System.ServiceModel.Channels.SecurityCapabilities System.ServiceModel.Channels.MessageSecurityBindingSupport::element_support
	SecurityCapabilities_t1438243711 * ___element_support_3;
	// System.IdentityModel.Selectors.SecurityTokenAuthenticator System.ServiceModel.Channels.MessageSecurityBindingSupport::authenticator
	SecurityTokenAuthenticator_t1397312864 * ___authenticator_4;
	// System.IdentityModel.Selectors.SecurityTokenResolver System.ServiceModel.Channels.MessageSecurityBindingSupport::auth_token_resolver
	SecurityTokenResolver_t3905425829 * ___auth_token_resolver_5;

public:
	inline static int32_t get_offset_of_manager_0() { return static_cast<int32_t>(offsetof(MessageSecurityBindingSupport_t1904510157, ___manager_0)); }
	inline SecurityTokenManager_t202133775 * get_manager_0() const { return ___manager_0; }
	inline SecurityTokenManager_t202133775 ** get_address_of_manager_0() { return &___manager_0; }
	inline void set_manager_0(SecurityTokenManager_t202133775 * value)
	{
		___manager_0 = value;
		Il2CppCodeGenWriteBarrier(&___manager_0, value);
	}

	inline static int32_t get_offset_of_requirements_1() { return static_cast<int32_t>(offsetof(MessageSecurityBindingSupport_t1904510157, ___requirements_1)); }
	inline ChannelProtectionRequirements_t2141883287 * get_requirements_1() const { return ___requirements_1; }
	inline ChannelProtectionRequirements_t2141883287 ** get_address_of_requirements_1() { return &___requirements_1; }
	inline void set_requirements_1(ChannelProtectionRequirements_t2141883287 * value)
	{
		___requirements_1 = value;
		Il2CppCodeGenWriteBarrier(&___requirements_1, value);
	}

	inline static int32_t get_offset_of_serializer_2() { return static_cast<int32_t>(offsetof(MessageSecurityBindingSupport_t1904510157, ___serializer_2)); }
	inline SecurityTokenSerializer_t972969391 * get_serializer_2() const { return ___serializer_2; }
	inline SecurityTokenSerializer_t972969391 ** get_address_of_serializer_2() { return &___serializer_2; }
	inline void set_serializer_2(SecurityTokenSerializer_t972969391 * value)
	{
		___serializer_2 = value;
		Il2CppCodeGenWriteBarrier(&___serializer_2, value);
	}

	inline static int32_t get_offset_of_element_support_3() { return static_cast<int32_t>(offsetof(MessageSecurityBindingSupport_t1904510157, ___element_support_3)); }
	inline SecurityCapabilities_t1438243711 * get_element_support_3() const { return ___element_support_3; }
	inline SecurityCapabilities_t1438243711 ** get_address_of_element_support_3() { return &___element_support_3; }
	inline void set_element_support_3(SecurityCapabilities_t1438243711 * value)
	{
		___element_support_3 = value;
		Il2CppCodeGenWriteBarrier(&___element_support_3, value);
	}

	inline static int32_t get_offset_of_authenticator_4() { return static_cast<int32_t>(offsetof(MessageSecurityBindingSupport_t1904510157, ___authenticator_4)); }
	inline SecurityTokenAuthenticator_t1397312864 * get_authenticator_4() const { return ___authenticator_4; }
	inline SecurityTokenAuthenticator_t1397312864 ** get_address_of_authenticator_4() { return &___authenticator_4; }
	inline void set_authenticator_4(SecurityTokenAuthenticator_t1397312864 * value)
	{
		___authenticator_4 = value;
		Il2CppCodeGenWriteBarrier(&___authenticator_4, value);
	}

	inline static int32_t get_offset_of_auth_token_resolver_5() { return static_cast<int32_t>(offsetof(MessageSecurityBindingSupport_t1904510157, ___auth_token_resolver_5)); }
	inline SecurityTokenResolver_t3905425829 * get_auth_token_resolver_5() const { return ___auth_token_resolver_5; }
	inline SecurityTokenResolver_t3905425829 ** get_address_of_auth_token_resolver_5() { return &___auth_token_resolver_5; }
	inline void set_auth_token_resolver_5(SecurityTokenResolver_t3905425829 * value)
	{
		___auth_token_resolver_5 = value;
		Il2CppCodeGenWriteBarrier(&___auth_token_resolver_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
