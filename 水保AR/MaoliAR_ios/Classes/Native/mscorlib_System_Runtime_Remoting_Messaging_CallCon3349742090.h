﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Hashtable
struct Hashtable_t1853889766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Remoting.Messaging.CallContext
struct  CallContext_t3349742090  : public Il2CppObject
{
public:

public:
};

struct CallContext_t3349742090_ThreadStaticFields
{
public:
	// System.Collections.Hashtable System.Runtime.Remoting.Messaging.CallContext::datastore
	Hashtable_t1853889766 * ___datastore_0;

public:
	inline static int32_t get_offset_of_datastore_0() { return static_cast<int32_t>(offsetof(CallContext_t3349742090_ThreadStaticFields, ___datastore_0)); }
	inline Hashtable_t1853889766 * get_datastore_0() const { return ___datastore_0; }
	inline Hashtable_t1853889766 ** get_address_of_datastore_0() { return &___datastore_0; }
	inline void set_datastore_0(Hashtable_t1853889766 * value)
	{
		___datastore_0 = value;
		Il2CppCodeGenWriteBarrier(&___datastore_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
