﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_EndpointId3899230012.h"

// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t714049126;
// System.Security.Cryptography.X509Certificates.X509Certificate2Collection
struct X509Certificate2Collection_t2111161276;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.X509CertificateEndpointIdentity
struct  X509CertificateEndpointIdentity_t3008261117  : public EndpointIdentity_t3899230012
{
public:
	// System.Security.Cryptography.X509Certificates.X509Certificate2 System.ServiceModel.X509CertificateEndpointIdentity::primary
	X509Certificate2_t714049126 * ___primary_2;
	// System.Security.Cryptography.X509Certificates.X509Certificate2Collection System.ServiceModel.X509CertificateEndpointIdentity::supporting
	X509Certificate2Collection_t2111161276 * ___supporting_3;
	// System.Security.Cryptography.X509Certificates.X509Certificate2Collection System.ServiceModel.X509CertificateEndpointIdentity::all
	X509Certificate2Collection_t2111161276 * ___all_4;

public:
	inline static int32_t get_offset_of_primary_2() { return static_cast<int32_t>(offsetof(X509CertificateEndpointIdentity_t3008261117, ___primary_2)); }
	inline X509Certificate2_t714049126 * get_primary_2() const { return ___primary_2; }
	inline X509Certificate2_t714049126 ** get_address_of_primary_2() { return &___primary_2; }
	inline void set_primary_2(X509Certificate2_t714049126 * value)
	{
		___primary_2 = value;
		Il2CppCodeGenWriteBarrier(&___primary_2, value);
	}

	inline static int32_t get_offset_of_supporting_3() { return static_cast<int32_t>(offsetof(X509CertificateEndpointIdentity_t3008261117, ___supporting_3)); }
	inline X509Certificate2Collection_t2111161276 * get_supporting_3() const { return ___supporting_3; }
	inline X509Certificate2Collection_t2111161276 ** get_address_of_supporting_3() { return &___supporting_3; }
	inline void set_supporting_3(X509Certificate2Collection_t2111161276 * value)
	{
		___supporting_3 = value;
		Il2CppCodeGenWriteBarrier(&___supporting_3, value);
	}

	inline static int32_t get_offset_of_all_4() { return static_cast<int32_t>(offsetof(X509CertificateEndpointIdentity_t3008261117, ___all_4)); }
	inline X509Certificate2Collection_t2111161276 * get_all_4() const { return ___all_4; }
	inline X509Certificate2Collection_t2111161276 ** get_address_of_all_4() { return &___all_4; }
	inline void set_all_4(X509Certificate2Collection_t2111161276 * value)
	{
		___all_4 = value;
		Il2CppCodeGenWriteBarrier(&___all_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
