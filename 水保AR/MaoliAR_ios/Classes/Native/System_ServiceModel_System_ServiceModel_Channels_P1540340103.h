﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_T4174286843.h"

// System.ServiceModel.Channels.PeerTransportBindingElement
struct PeerTransportBindingElement_t261693216;
// System.ServiceModel.Channels.MessageEncoder
struct MessageEncoder_t3063398011;
// System.ServiceModel.PeerResolver
struct PeerResolver_t1567980581;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.PeerChannelFactory`1<System.ServiceModel.Channels.IDuplexChannel>
struct  PeerChannelFactory_1_t1540340103  : public TransportChannelFactoryBase_1_t4174286843
{
public:
	// System.ServiceModel.Channels.PeerTransportBindingElement System.ServiceModel.Channels.PeerChannelFactory`1::source
	PeerTransportBindingElement_t261693216 * ___source_15;
	// System.ServiceModel.Channels.MessageEncoder System.ServiceModel.Channels.PeerChannelFactory`1::encoder
	MessageEncoder_t3063398011 * ___encoder_16;
	// System.ServiceModel.PeerResolver System.ServiceModel.Channels.PeerChannelFactory`1::<Resolver>k__BackingField
	PeerResolver_t1567980581 * ___U3CResolverU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_source_15() { return static_cast<int32_t>(offsetof(PeerChannelFactory_1_t1540340103, ___source_15)); }
	inline PeerTransportBindingElement_t261693216 * get_source_15() const { return ___source_15; }
	inline PeerTransportBindingElement_t261693216 ** get_address_of_source_15() { return &___source_15; }
	inline void set_source_15(PeerTransportBindingElement_t261693216 * value)
	{
		___source_15 = value;
		Il2CppCodeGenWriteBarrier(&___source_15, value);
	}

	inline static int32_t get_offset_of_encoder_16() { return static_cast<int32_t>(offsetof(PeerChannelFactory_1_t1540340103, ___encoder_16)); }
	inline MessageEncoder_t3063398011 * get_encoder_16() const { return ___encoder_16; }
	inline MessageEncoder_t3063398011 ** get_address_of_encoder_16() { return &___encoder_16; }
	inline void set_encoder_16(MessageEncoder_t3063398011 * value)
	{
		___encoder_16 = value;
		Il2CppCodeGenWriteBarrier(&___encoder_16, value);
	}

	inline static int32_t get_offset_of_U3CResolverU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(PeerChannelFactory_1_t1540340103, ___U3CResolverU3Ek__BackingField_17)); }
	inline PeerResolver_t1567980581 * get_U3CResolverU3Ek__BackingField_17() const { return ___U3CResolverU3Ek__BackingField_17; }
	inline PeerResolver_t1567980581 ** get_address_of_U3CResolverU3Ek__BackingField_17() { return &___U3CResolverU3Ek__BackingField_17; }
	inline void set_U3CResolverU3Ek__BackingField_17(PeerResolver_t1567980581 * value)
	{
		___U3CResolverU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CResolverU3Ek__BackingField_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
