﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Collections_Specialized_NameObjectCo2091847364.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpCookieCollection
struct  HttpCookieCollection_t1178559666  : public NameObjectCollectionBase_t2091847364
{
public:
	// System.Boolean System.Web.HttpCookieCollection::auto_fill
	bool ___auto_fill_10;

public:
	inline static int32_t get_offset_of_auto_fill_10() { return static_cast<int32_t>(offsetof(HttpCookieCollection_t1178559666, ___auto_fill_10)); }
	inline bool get_auto_fill_10() const { return ___auto_fill_10; }
	inline bool* get_address_of_auto_fill_10() { return &___auto_fill_10; }
	inline void set_auto_fill_10(bool value)
	{
		___auto_fill_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
