﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Microsoft_VisualBasic_VBCodeGenerator186779574.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.VisualBasic.VBCodeCompiler
struct  VBCodeCompiler_t761065191  : public VBCodeGenerator_t186779574
{
public:

public:
};

struct VBCodeCompiler_t761065191_StaticFields
{
public:
	// System.String Microsoft.VisualBasic.VBCodeCompiler::windowsMonoPath
	String_t* ___windowsMonoPath_8;
	// System.String Microsoft.VisualBasic.VBCodeCompiler::windowsvbncPath
	String_t* ___windowsvbncPath_9;

public:
	inline static int32_t get_offset_of_windowsMonoPath_8() { return static_cast<int32_t>(offsetof(VBCodeCompiler_t761065191_StaticFields, ___windowsMonoPath_8)); }
	inline String_t* get_windowsMonoPath_8() const { return ___windowsMonoPath_8; }
	inline String_t** get_address_of_windowsMonoPath_8() { return &___windowsMonoPath_8; }
	inline void set_windowsMonoPath_8(String_t* value)
	{
		___windowsMonoPath_8 = value;
		Il2CppCodeGenWriteBarrier(&___windowsMonoPath_8, value);
	}

	inline static int32_t get_offset_of_windowsvbncPath_9() { return static_cast<int32_t>(offsetof(VBCodeCompiler_t761065191_StaticFields, ___windowsvbncPath_9)); }
	inline String_t* get_windowsvbncPath_9() const { return ___windowsvbncPath_9; }
	inline String_t** get_address_of_windowsvbncPath_9() { return &___windowsvbncPath_9; }
	inline void set_windowsvbncPath_9(String_t* value)
	{
		___windowsvbncPath_9 = value;
		Il2CppCodeGenWriteBarrier(&___windowsvbncPath_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
