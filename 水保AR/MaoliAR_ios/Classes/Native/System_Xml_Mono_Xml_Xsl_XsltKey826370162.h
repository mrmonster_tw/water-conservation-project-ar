﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XPath_XPathFunction857746608.h"

// System.Xml.XPath.Expression
struct Expression_t1452783009;
// System.Xml.Xsl.IStaticXsltContext
struct IStaticXsltContext_t1496336571;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.XsltKey
struct  XsltKey_t826370162  : public XPathFunction_t857746608
{
public:
	// System.Xml.XPath.Expression Mono.Xml.Xsl.XsltKey::arg0
	Expression_t1452783009 * ___arg0_0;
	// System.Xml.XPath.Expression Mono.Xml.Xsl.XsltKey::arg1
	Expression_t1452783009 * ___arg1_1;
	// System.Xml.Xsl.IStaticXsltContext Mono.Xml.Xsl.XsltKey::staticContext
	Il2CppObject * ___staticContext_2;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XsltKey_t826370162, ___arg0_0)); }
	inline Expression_t1452783009 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1452783009 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1452783009 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier(&___arg0_0, value);
	}

	inline static int32_t get_offset_of_arg1_1() { return static_cast<int32_t>(offsetof(XsltKey_t826370162, ___arg1_1)); }
	inline Expression_t1452783009 * get_arg1_1() const { return ___arg1_1; }
	inline Expression_t1452783009 ** get_address_of_arg1_1() { return &___arg1_1; }
	inline void set_arg1_1(Expression_t1452783009 * value)
	{
		___arg1_1 = value;
		Il2CppCodeGenWriteBarrier(&___arg1_1, value);
	}

	inline static int32_t get_offset_of_staticContext_2() { return static_cast<int32_t>(offsetof(XsltKey_t826370162, ___staticContext_2)); }
	inline Il2CppObject * get_staticContext_2() const { return ___staticContext_2; }
	inline Il2CppObject ** get_address_of_staticContext_2() { return &___staticContext_2; }
	inline void set_staticContext_2(Il2CppObject * value)
	{
		___staticContext_2 = value;
		Il2CppCodeGenWriteBarrier(&___staticContext_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
