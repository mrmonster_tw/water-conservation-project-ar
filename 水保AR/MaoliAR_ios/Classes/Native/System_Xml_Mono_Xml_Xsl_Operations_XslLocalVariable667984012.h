﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Operations_XslGeneralVaria1456221871.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslLocalVariable
struct  XslLocalVariable_t667984012  : public XslGeneralVariable_t1456221871
{
public:
	// System.Int32 Mono.Xml.Xsl.Operations.XslLocalVariable::slot
	int32_t ___slot_4;

public:
	inline static int32_t get_offset_of_slot_4() { return static_cast<int32_t>(offsetof(XslLocalVariable_t667984012, ___slot_4)); }
	inline int32_t get_slot_4() const { return ___slot_4; }
	inline int32_t* get_address_of_slot_4() { return &___slot_4; }
	inline void set_slot_4(int32_t value)
	{
		___slot_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
