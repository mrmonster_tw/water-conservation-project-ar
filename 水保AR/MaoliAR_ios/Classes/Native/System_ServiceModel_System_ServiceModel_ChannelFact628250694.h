﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Co518829156.h"

// System.ServiceModel.Description.ServiceEndpoint
struct ServiceEndpoint_t4038493094;
// System.ServiceModel.Channels.IChannelFactory
struct IChannelFactory_t1438449236;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.ChannelFactory
struct  ChannelFactory_t628250694  : public CommunicationObject_t518829156
{
public:
	// System.ServiceModel.Description.ServiceEndpoint System.ServiceModel.ChannelFactory::service_endpoint
	ServiceEndpoint_t4038493094 * ___service_endpoint_9;
	// System.ServiceModel.Channels.IChannelFactory System.ServiceModel.ChannelFactory::factory
	Il2CppObject * ___factory_10;

public:
	inline static int32_t get_offset_of_service_endpoint_9() { return static_cast<int32_t>(offsetof(ChannelFactory_t628250694, ___service_endpoint_9)); }
	inline ServiceEndpoint_t4038493094 * get_service_endpoint_9() const { return ___service_endpoint_9; }
	inline ServiceEndpoint_t4038493094 ** get_address_of_service_endpoint_9() { return &___service_endpoint_9; }
	inline void set_service_endpoint_9(ServiceEndpoint_t4038493094 * value)
	{
		___service_endpoint_9 = value;
		Il2CppCodeGenWriteBarrier(&___service_endpoint_9, value);
	}

	inline static int32_t get_offset_of_factory_10() { return static_cast<int32_t>(offsetof(ChannelFactory_t628250694, ___factory_10)); }
	inline Il2CppObject * get_factory_10() const { return ___factory_10; }
	inline Il2CppObject ** get_address_of_factory_10() { return &___factory_10; }
	inline void set_factory_10(Il2CppObject * value)
	{
		___factory_10 = value;
		Il2CppCodeGenWriteBarrier(&___factory_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
