﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeStatement371410868.h"

// System.CodeDom.CodeEventReferenceExpression
struct CodeEventReferenceExpression_t4048173689;
// System.CodeDom.CodeExpression
struct CodeExpression_t2166265795;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeAttachEventStatement
struct  CodeAttachEventStatement_t2851702928  : public CodeStatement_t371410868
{
public:
	// System.CodeDom.CodeEventReferenceExpression System.CodeDom.CodeAttachEventStatement::eventRef
	CodeEventReferenceExpression_t4048173689 * ___eventRef_4;
	// System.CodeDom.CodeExpression System.CodeDom.CodeAttachEventStatement::listener
	CodeExpression_t2166265795 * ___listener_5;

public:
	inline static int32_t get_offset_of_eventRef_4() { return static_cast<int32_t>(offsetof(CodeAttachEventStatement_t2851702928, ___eventRef_4)); }
	inline CodeEventReferenceExpression_t4048173689 * get_eventRef_4() const { return ___eventRef_4; }
	inline CodeEventReferenceExpression_t4048173689 ** get_address_of_eventRef_4() { return &___eventRef_4; }
	inline void set_eventRef_4(CodeEventReferenceExpression_t4048173689 * value)
	{
		___eventRef_4 = value;
		Il2CppCodeGenWriteBarrier(&___eventRef_4, value);
	}

	inline static int32_t get_offset_of_listener_5() { return static_cast<int32_t>(offsetof(CodeAttachEventStatement_t2851702928, ___listener_5)); }
	inline CodeExpression_t2166265795 * get_listener_5() const { return ___listener_5; }
	inline CodeExpression_t2166265795 ** get_address_of_listener_5() { return &___listener_5; }
	inline void set_listener_5(CodeExpression_t2166265795 * value)
	{
		___listener_5 = value;
		Il2CppCodeGenWriteBarrier(&___listener_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
