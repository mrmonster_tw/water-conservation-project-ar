﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeExpression2166265795.h"

// System.String
struct String_t;
// System.CodeDom.CodeExpression
struct CodeExpression_t2166265795;
// System.CodeDom.CodeTypeReferenceCollection
struct CodeTypeReferenceCollection_t3857551471;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeMethodReferenceExpression
struct  CodeMethodReferenceExpression_t2645505394  : public CodeExpression_t2166265795
{
public:
	// System.String System.CodeDom.CodeMethodReferenceExpression::methodName
	String_t* ___methodName_1;
	// System.CodeDom.CodeExpression System.CodeDom.CodeMethodReferenceExpression::targetObject
	CodeExpression_t2166265795 * ___targetObject_2;
	// System.CodeDom.CodeTypeReferenceCollection System.CodeDom.CodeMethodReferenceExpression::typeArguments
	CodeTypeReferenceCollection_t3857551471 * ___typeArguments_3;

public:
	inline static int32_t get_offset_of_methodName_1() { return static_cast<int32_t>(offsetof(CodeMethodReferenceExpression_t2645505394, ___methodName_1)); }
	inline String_t* get_methodName_1() const { return ___methodName_1; }
	inline String_t** get_address_of_methodName_1() { return &___methodName_1; }
	inline void set_methodName_1(String_t* value)
	{
		___methodName_1 = value;
		Il2CppCodeGenWriteBarrier(&___methodName_1, value);
	}

	inline static int32_t get_offset_of_targetObject_2() { return static_cast<int32_t>(offsetof(CodeMethodReferenceExpression_t2645505394, ___targetObject_2)); }
	inline CodeExpression_t2166265795 * get_targetObject_2() const { return ___targetObject_2; }
	inline CodeExpression_t2166265795 ** get_address_of_targetObject_2() { return &___targetObject_2; }
	inline void set_targetObject_2(CodeExpression_t2166265795 * value)
	{
		___targetObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___targetObject_2, value);
	}

	inline static int32_t get_offset_of_typeArguments_3() { return static_cast<int32_t>(offsetof(CodeMethodReferenceExpression_t2645505394, ___typeArguments_3)); }
	inline CodeTypeReferenceCollection_t3857551471 * get_typeArguments_3() const { return ___typeArguments_3; }
	inline CodeTypeReferenceCollection_t3857551471 ** get_address_of_typeArguments_3() { return &___typeArguments_3; }
	inline void set_typeArguments_3(CodeTypeReferenceCollection_t3857551471 * value)
	{
		___typeArguments_3 = value;
		Il2CppCodeGenWriteBarrier(&___typeArguments_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
