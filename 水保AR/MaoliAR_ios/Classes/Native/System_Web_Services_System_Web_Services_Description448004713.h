﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Web.Services.Description.SoapProtocolImporter
struct SoapProtocolImporter_t721795827;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.SoapTransportImporter
struct  SoapTransportImporter_t448004713  : public Il2CppObject
{
public:
	// System.Web.Services.Description.SoapProtocolImporter System.Web.Services.Description.SoapTransportImporter::importContext
	SoapProtocolImporter_t721795827 * ___importContext_1;

public:
	inline static int32_t get_offset_of_importContext_1() { return static_cast<int32_t>(offsetof(SoapTransportImporter_t448004713, ___importContext_1)); }
	inline SoapProtocolImporter_t721795827 * get_importContext_1() const { return ___importContext_1; }
	inline SoapProtocolImporter_t721795827 ** get_address_of_importContext_1() { return &___importContext_1; }
	inline void set_importContext_1(SoapProtocolImporter_t721795827 * value)
	{
		___importContext_1 = value;
		Il2CppCodeGenWriteBarrier(&___importContext_1, value);
	}
};

struct SoapTransportImporter_t448004713_StaticFields
{
public:
	// System.Collections.ArrayList System.Web.Services.Description.SoapTransportImporter::transportImporters
	ArrayList_t2718874744 * ___transportImporters_0;

public:
	inline static int32_t get_offset_of_transportImporters_0() { return static_cast<int32_t>(offsetof(SoapTransportImporter_t448004713_StaticFields, ___transportImporters_0)); }
	inline ArrayList_t2718874744 * get_transportImporters_0() const { return ___transportImporters_0; }
	inline ArrayList_t2718874744 ** get_address_of_transportImporters_0() { return &___transportImporters_0; }
	inline void set_transportImporters_0(ArrayList_t2718874744 * value)
	{
		___transportImporters_0 = value;
		Il2CppCodeGenWriteBarrier(&___transportImporters_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
