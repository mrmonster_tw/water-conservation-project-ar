﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_Services_System_Web_Services_Description546804031.h"

// System.String
struct String_t;
// System.Web.Services.Description.ServiceDescription
struct ServiceDescription_t3693704363;
// System.Web.Services.Description.ServiceDescriptionFormatExtensionCollection
struct ServiceDescriptionFormatExtensionCollection_t2630153888;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.Import
struct  Import_t796004249  : public DocumentableItem_t546804031
{
public:
	// System.String System.Web.Services.Description.Import::location
	String_t* ___location_3;
	// System.String System.Web.Services.Description.Import::ns
	String_t* ___ns_4;
	// System.Web.Services.Description.ServiceDescription System.Web.Services.Description.Import::serviceDescription
	ServiceDescription_t3693704363 * ___serviceDescription_5;
	// System.Web.Services.Description.ServiceDescriptionFormatExtensionCollection System.Web.Services.Description.Import::extensions
	ServiceDescriptionFormatExtensionCollection_t2630153888 * ___extensions_6;

public:
	inline static int32_t get_offset_of_location_3() { return static_cast<int32_t>(offsetof(Import_t796004249, ___location_3)); }
	inline String_t* get_location_3() const { return ___location_3; }
	inline String_t** get_address_of_location_3() { return &___location_3; }
	inline void set_location_3(String_t* value)
	{
		___location_3 = value;
		Il2CppCodeGenWriteBarrier(&___location_3, value);
	}

	inline static int32_t get_offset_of_ns_4() { return static_cast<int32_t>(offsetof(Import_t796004249, ___ns_4)); }
	inline String_t* get_ns_4() const { return ___ns_4; }
	inline String_t** get_address_of_ns_4() { return &___ns_4; }
	inline void set_ns_4(String_t* value)
	{
		___ns_4 = value;
		Il2CppCodeGenWriteBarrier(&___ns_4, value);
	}

	inline static int32_t get_offset_of_serviceDescription_5() { return static_cast<int32_t>(offsetof(Import_t796004249, ___serviceDescription_5)); }
	inline ServiceDescription_t3693704363 * get_serviceDescription_5() const { return ___serviceDescription_5; }
	inline ServiceDescription_t3693704363 ** get_address_of_serviceDescription_5() { return &___serviceDescription_5; }
	inline void set_serviceDescription_5(ServiceDescription_t3693704363 * value)
	{
		___serviceDescription_5 = value;
		Il2CppCodeGenWriteBarrier(&___serviceDescription_5, value);
	}

	inline static int32_t get_offset_of_extensions_6() { return static_cast<int32_t>(offsetof(Import_t796004249, ___extensions_6)); }
	inline ServiceDescriptionFormatExtensionCollection_t2630153888 * get_extensions_6() const { return ___extensions_6; }
	inline ServiceDescriptionFormatExtensionCollection_t2630153888 ** get_address_of_extensions_6() { return &___extensions_6; }
	inline void set_extensions_6(ServiceDescriptionFormatExtensionCollection_t2630153888 * value)
	{
		___extensions_6 = value;
		Il2CppCodeGenWriteBarrier(&___extensions_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
