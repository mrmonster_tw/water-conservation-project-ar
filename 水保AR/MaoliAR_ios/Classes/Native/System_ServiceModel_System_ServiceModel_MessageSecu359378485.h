﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_System_ServiceModel_MessageCre1579346758.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.MessageSecurityOverHttp
struct  MessageSecurityOverHttp_t359378485  : public Il2CppObject
{
public:
	// System.ServiceModel.MessageCredentialType System.ServiceModel.MessageSecurityOverHttp::client_credential_type
	int32_t ___client_credential_type_0;
	// System.Boolean System.ServiceModel.MessageSecurityOverHttp::negotiate_service_credential
	bool ___negotiate_service_credential_1;

public:
	inline static int32_t get_offset_of_client_credential_type_0() { return static_cast<int32_t>(offsetof(MessageSecurityOverHttp_t359378485, ___client_credential_type_0)); }
	inline int32_t get_client_credential_type_0() const { return ___client_credential_type_0; }
	inline int32_t* get_address_of_client_credential_type_0() { return &___client_credential_type_0; }
	inline void set_client_credential_type_0(int32_t value)
	{
		___client_credential_type_0 = value;
	}

	inline static int32_t get_offset_of_negotiate_service_credential_1() { return static_cast<int32_t>(offsetof(MessageSecurityOverHttp_t359378485, ___negotiate_service_credential_1)); }
	inline bool get_negotiate_service_credential_1() const { return ___negotiate_service_credential_1; }
	inline bool* get_address_of_negotiate_service_credential_1() { return &___negotiate_service_credential_1; }
	inline void set_negotiate_service_credential_1(bool value)
	{
		___negotiate_service_credential_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
