﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Dispatcher4072893799.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.String>
struct ReadOnlyCollection_1_t3060026976;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.ActionMessageFilter
struct  ActionMessageFilter_t523700087  : public MessageFilter_t4072893799
{
public:
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.String> System.ServiceModel.Dispatcher.ActionMessageFilter::actions
	ReadOnlyCollection_1_t3060026976 * ___actions_0;

public:
	inline static int32_t get_offset_of_actions_0() { return static_cast<int32_t>(offsetof(ActionMessageFilter_t523700087, ___actions_0)); }
	inline ReadOnlyCollection_1_t3060026976 * get_actions_0() const { return ___actions_0; }
	inline ReadOnlyCollection_1_t3060026976 ** get_address_of_actions_0() { return &___actions_0; }
	inline void set_actions_0(ReadOnlyCollection_1_t3060026976 * value)
	{
		___actions_0 = value;
		Il2CppCodeGenWriteBarrier(&___actions_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
