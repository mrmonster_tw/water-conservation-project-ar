﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_Services_System_Web_Services_Descriptio3177955060.h"
#include "System_Web_Services_System_Web_Services_Descriptio3254587761.h"

// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2760654312;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.SoapHeaderBinding
struct  SoapHeaderBinding_t968443453  : public ServiceDescriptionFormatExtension_t3177955060
{
public:
	// System.Xml.XmlQualifiedName System.Web.Services.Description.SoapHeaderBinding::message
	XmlQualifiedName_t2760654312 * ___message_1;
	// System.String System.Web.Services.Description.SoapHeaderBinding::part
	String_t* ___part_2;
	// System.Web.Services.Description.SoapBindingUse System.Web.Services.Description.SoapHeaderBinding::use
	int32_t ___use_3;

public:
	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(SoapHeaderBinding_t968443453, ___message_1)); }
	inline XmlQualifiedName_t2760654312 * get_message_1() const { return ___message_1; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(XmlQualifiedName_t2760654312 * value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier(&___message_1, value);
	}

	inline static int32_t get_offset_of_part_2() { return static_cast<int32_t>(offsetof(SoapHeaderBinding_t968443453, ___part_2)); }
	inline String_t* get_part_2() const { return ___part_2; }
	inline String_t** get_address_of_part_2() { return &___part_2; }
	inline void set_part_2(String_t* value)
	{
		___part_2 = value;
		Il2CppCodeGenWriteBarrier(&___part_2, value);
	}

	inline static int32_t get_offset_of_use_3() { return static_cast<int32_t>(offsetof(SoapHeaderBinding_t968443453, ___use_3)); }
	inline int32_t get_use_3() const { return ___use_3; }
	inline int32_t* get_address_of_use_3() { return &___use_3; }
	inline void set_use_3(int32_t value)
	{
		___use_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
