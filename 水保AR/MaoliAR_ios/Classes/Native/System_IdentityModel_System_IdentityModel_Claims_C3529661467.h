﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.IdentityModel.Claims.ClaimSet
struct ClaimSet_t3529661467;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Claims.ClaimSet
struct  ClaimSet_t3529661467  : public Il2CppObject
{
public:

public:
};

struct ClaimSet_t3529661467_StaticFields
{
public:
	// System.IdentityModel.Claims.ClaimSet System.IdentityModel.Claims.ClaimSet::system
	ClaimSet_t3529661467 * ___system_0;

public:
	inline static int32_t get_offset_of_system_0() { return static_cast<int32_t>(offsetof(ClaimSet_t3529661467_StaticFields, ___system_0)); }
	inline ClaimSet_t3529661467 * get_system_0() const { return ___system_0; }
	inline ClaimSet_t3529661467 ** get_address_of_system_0() { return &___system_0; }
	inline void set_system_0(ClaimSet_t3529661467 * value)
	{
		___system_0 = value;
		Il2CppCodeGenWriteBarrier(&___system_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
