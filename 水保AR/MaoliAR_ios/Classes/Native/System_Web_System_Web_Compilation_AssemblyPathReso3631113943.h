﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.AssemblyPathResolver
struct  AssemblyPathResolver_t3631113943  : public Il2CppObject
{
public:

public:
};

struct AssemblyPathResolver_t3631113943_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> System.Web.Compilation.AssemblyPathResolver::assemblyCache
	Dictionary_2_t1632706988 * ___assemblyCache_0;

public:
	inline static int32_t get_offset_of_assemblyCache_0() { return static_cast<int32_t>(offsetof(AssemblyPathResolver_t3631113943_StaticFields, ___assemblyCache_0)); }
	inline Dictionary_2_t1632706988 * get_assemblyCache_0() const { return ___assemblyCache_0; }
	inline Dictionary_2_t1632706988 ** get_address_of_assemblyCache_0() { return &___assemblyCache_0; }
	inline void set_assemblyCache_0(Dictionary_2_t1632706988 * value)
	{
		___assemblyCache_0 = value;
		Il2CppCodeGenWriteBarrier(&___assemblyCache_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
