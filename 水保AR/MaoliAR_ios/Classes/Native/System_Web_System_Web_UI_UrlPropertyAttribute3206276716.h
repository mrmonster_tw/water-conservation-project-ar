﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.UrlPropertyAttribute
struct  UrlPropertyAttribute_t3206276716  : public Attribute_t861562559
{
public:
	// System.String System.Web.UI.UrlPropertyAttribute::filter
	String_t* ___filter_0;

public:
	inline static int32_t get_offset_of_filter_0() { return static_cast<int32_t>(offsetof(UrlPropertyAttribute_t3206276716, ___filter_0)); }
	inline String_t* get_filter_0() const { return ___filter_0; }
	inline String_t** get_address_of_filter_0() { return &___filter_0; }
	inline void set_filter_0(String_t* value)
	{
		___filter_0 = value;
		Il2CppCodeGenWriteBarrier(&___filter_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
