﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Serialization_SerializationS3488723117.h"

// System.String
struct String_t;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.SoapTypeSerializationSource
struct  SoapTypeSerializationSource_t2273034474  : public SerializationSource_t3488723117
{
public:
	// System.String System.Xml.Serialization.SoapTypeSerializationSource::attributeOverridesHash
	String_t* ___attributeOverridesHash_3;
	// System.Type System.Xml.Serialization.SoapTypeSerializationSource::type
	Type_t * ___type_4;

public:
	inline static int32_t get_offset_of_attributeOverridesHash_3() { return static_cast<int32_t>(offsetof(SoapTypeSerializationSource_t2273034474, ___attributeOverridesHash_3)); }
	inline String_t* get_attributeOverridesHash_3() const { return ___attributeOverridesHash_3; }
	inline String_t** get_address_of_attributeOverridesHash_3() { return &___attributeOverridesHash_3; }
	inline void set_attributeOverridesHash_3(String_t* value)
	{
		___attributeOverridesHash_3 = value;
		Il2CppCodeGenWriteBarrier(&___attributeOverridesHash_3, value);
	}

	inline static int32_t get_offset_of_type_4() { return static_cast<int32_t>(offsetof(SoapTypeSerializationSource_t2273034474, ___type_4)); }
	inline Type_t * get_type_4() const { return ___type_4; }
	inline Type_t ** get_address_of_type_4() { return &___type_4; }
	inline void set_type_4(Type_t * value)
	{
		___type_4 = value;
		Il2CppCodeGenWriteBarrier(&___type_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
