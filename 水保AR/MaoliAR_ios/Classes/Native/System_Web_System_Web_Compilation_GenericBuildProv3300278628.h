﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_Compilation_BuildProvider3736381005.h"

// System.Object
struct Il2CppObject;
// System.Web.Compilation.CompilerType
struct CompilerType_t2533482247;
// System.Web.Compilation.BaseCompiler
struct BaseCompiler_t69158820;
// System.IO.TextReader
struct TextReader_t283511965;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.GenericBuildProvider`1<System.Object>
struct  GenericBuildProvider_1_t3300278628  : public BuildProvider_t3736381005
{
public:
	// TParser System.Web.Compilation.GenericBuildProvider`1::_parser
	Il2CppObject * ____parser_3;
	// System.Web.Compilation.CompilerType System.Web.Compilation.GenericBuildProvider`1::_compilerType
	CompilerType_t2533482247 * ____compilerType_4;
	// System.Web.Compilation.BaseCompiler System.Web.Compilation.GenericBuildProvider`1::_compiler
	BaseCompiler_t69158820 * ____compiler_5;
	// System.IO.TextReader System.Web.Compilation.GenericBuildProvider`1::_reader
	TextReader_t283511965 * ____reader_6;
	// System.Boolean System.Web.Compilation.GenericBuildProvider`1::_parsed
	bool ____parsed_7;
	// System.Boolean System.Web.Compilation.GenericBuildProvider`1::_codeGenerated
	bool ____codeGenerated_8;

public:
	inline static int32_t get_offset_of__parser_3() { return static_cast<int32_t>(offsetof(GenericBuildProvider_1_t3300278628, ____parser_3)); }
	inline Il2CppObject * get__parser_3() const { return ____parser_3; }
	inline Il2CppObject ** get_address_of__parser_3() { return &____parser_3; }
	inline void set__parser_3(Il2CppObject * value)
	{
		____parser_3 = value;
		Il2CppCodeGenWriteBarrier(&____parser_3, value);
	}

	inline static int32_t get_offset_of__compilerType_4() { return static_cast<int32_t>(offsetof(GenericBuildProvider_1_t3300278628, ____compilerType_4)); }
	inline CompilerType_t2533482247 * get__compilerType_4() const { return ____compilerType_4; }
	inline CompilerType_t2533482247 ** get_address_of__compilerType_4() { return &____compilerType_4; }
	inline void set__compilerType_4(CompilerType_t2533482247 * value)
	{
		____compilerType_4 = value;
		Il2CppCodeGenWriteBarrier(&____compilerType_4, value);
	}

	inline static int32_t get_offset_of__compiler_5() { return static_cast<int32_t>(offsetof(GenericBuildProvider_1_t3300278628, ____compiler_5)); }
	inline BaseCompiler_t69158820 * get__compiler_5() const { return ____compiler_5; }
	inline BaseCompiler_t69158820 ** get_address_of__compiler_5() { return &____compiler_5; }
	inline void set__compiler_5(BaseCompiler_t69158820 * value)
	{
		____compiler_5 = value;
		Il2CppCodeGenWriteBarrier(&____compiler_5, value);
	}

	inline static int32_t get_offset_of__reader_6() { return static_cast<int32_t>(offsetof(GenericBuildProvider_1_t3300278628, ____reader_6)); }
	inline TextReader_t283511965 * get__reader_6() const { return ____reader_6; }
	inline TextReader_t283511965 ** get_address_of__reader_6() { return &____reader_6; }
	inline void set__reader_6(TextReader_t283511965 * value)
	{
		____reader_6 = value;
		Il2CppCodeGenWriteBarrier(&____reader_6, value);
	}

	inline static int32_t get_offset_of__parsed_7() { return static_cast<int32_t>(offsetof(GenericBuildProvider_1_t3300278628, ____parsed_7)); }
	inline bool get__parsed_7() const { return ____parsed_7; }
	inline bool* get_address_of__parsed_7() { return &____parsed_7; }
	inline void set__parsed_7(bool value)
	{
		____parsed_7 = value;
	}

	inline static int32_t get_offset_of__codeGenerated_8() { return static_cast<int32_t>(offsetof(GenericBuildProvider_1_t3300278628, ____codeGenerated_8)); }
	inline bool get__codeGenerated_8() const { return ____codeGenerated_8; }
	inline bool* get_address_of__codeGenerated_8() { return &____codeGenerated_8; }
	inline void set__codeGenerated_8(bool value)
	{
		____codeGenerated_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
