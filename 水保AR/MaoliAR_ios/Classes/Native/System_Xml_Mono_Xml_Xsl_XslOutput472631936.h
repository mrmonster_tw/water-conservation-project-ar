﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Xml_Mono_Xml_Xsl_OutputMethod245858122.h"
#include "System_Xml_Mono_Xml_Xsl_StandaloneType1639935827.h"

// System.String
struct String_t;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2760654312;
// System.Text.Encoding
struct Encoding_t1523322056;
// System.Xml.XmlQualifiedName[]
struct XmlQualifiedNameU5BU5D_t1471530361;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.XslOutput
struct  XslOutput_t472631936  : public Il2CppObject
{
public:
	// System.String Mono.Xml.Xsl.XslOutput::uri
	String_t* ___uri_0;
	// System.Xml.XmlQualifiedName Mono.Xml.Xsl.XslOutput::customMethod
	XmlQualifiedName_t2760654312 * ___customMethod_1;
	// Mono.Xml.Xsl.OutputMethod Mono.Xml.Xsl.XslOutput::method
	int32_t ___method_2;
	// System.String Mono.Xml.Xsl.XslOutput::version
	String_t* ___version_3;
	// System.Text.Encoding Mono.Xml.Xsl.XslOutput::encoding
	Encoding_t1523322056 * ___encoding_4;
	// System.Boolean Mono.Xml.Xsl.XslOutput::omitXmlDeclaration
	bool ___omitXmlDeclaration_5;
	// Mono.Xml.Xsl.StandaloneType Mono.Xml.Xsl.XslOutput::standalone
	int32_t ___standalone_6;
	// System.String Mono.Xml.Xsl.XslOutput::doctypePublic
	String_t* ___doctypePublic_7;
	// System.String Mono.Xml.Xsl.XslOutput::doctypeSystem
	String_t* ___doctypeSystem_8;
	// System.Xml.XmlQualifiedName[] Mono.Xml.Xsl.XslOutput::cdataSectionElements
	XmlQualifiedNameU5BU5D_t1471530361* ___cdataSectionElements_9;
	// System.String Mono.Xml.Xsl.XslOutput::indent
	String_t* ___indent_10;
	// System.String Mono.Xml.Xsl.XslOutput::mediaType
	String_t* ___mediaType_11;
	// System.String Mono.Xml.Xsl.XslOutput::stylesheetVersion
	String_t* ___stylesheetVersion_12;
	// System.Collections.ArrayList Mono.Xml.Xsl.XslOutput::cdSectsList
	ArrayList_t2718874744 * ___cdSectsList_13;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(XslOutput_t472631936, ___uri_0)); }
	inline String_t* get_uri_0() const { return ___uri_0; }
	inline String_t** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(String_t* value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier(&___uri_0, value);
	}

	inline static int32_t get_offset_of_customMethod_1() { return static_cast<int32_t>(offsetof(XslOutput_t472631936, ___customMethod_1)); }
	inline XmlQualifiedName_t2760654312 * get_customMethod_1() const { return ___customMethod_1; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_customMethod_1() { return &___customMethod_1; }
	inline void set_customMethod_1(XmlQualifiedName_t2760654312 * value)
	{
		___customMethod_1 = value;
		Il2CppCodeGenWriteBarrier(&___customMethod_1, value);
	}

	inline static int32_t get_offset_of_method_2() { return static_cast<int32_t>(offsetof(XslOutput_t472631936, ___method_2)); }
	inline int32_t get_method_2() const { return ___method_2; }
	inline int32_t* get_address_of_method_2() { return &___method_2; }
	inline void set_method_2(int32_t value)
	{
		___method_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(XslOutput_t472631936, ___version_3)); }
	inline String_t* get_version_3() const { return ___version_3; }
	inline String_t** get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(String_t* value)
	{
		___version_3 = value;
		Il2CppCodeGenWriteBarrier(&___version_3, value);
	}

	inline static int32_t get_offset_of_encoding_4() { return static_cast<int32_t>(offsetof(XslOutput_t472631936, ___encoding_4)); }
	inline Encoding_t1523322056 * get_encoding_4() const { return ___encoding_4; }
	inline Encoding_t1523322056 ** get_address_of_encoding_4() { return &___encoding_4; }
	inline void set_encoding_4(Encoding_t1523322056 * value)
	{
		___encoding_4 = value;
		Il2CppCodeGenWriteBarrier(&___encoding_4, value);
	}

	inline static int32_t get_offset_of_omitXmlDeclaration_5() { return static_cast<int32_t>(offsetof(XslOutput_t472631936, ___omitXmlDeclaration_5)); }
	inline bool get_omitXmlDeclaration_5() const { return ___omitXmlDeclaration_5; }
	inline bool* get_address_of_omitXmlDeclaration_5() { return &___omitXmlDeclaration_5; }
	inline void set_omitXmlDeclaration_5(bool value)
	{
		___omitXmlDeclaration_5 = value;
	}

	inline static int32_t get_offset_of_standalone_6() { return static_cast<int32_t>(offsetof(XslOutput_t472631936, ___standalone_6)); }
	inline int32_t get_standalone_6() const { return ___standalone_6; }
	inline int32_t* get_address_of_standalone_6() { return &___standalone_6; }
	inline void set_standalone_6(int32_t value)
	{
		___standalone_6 = value;
	}

	inline static int32_t get_offset_of_doctypePublic_7() { return static_cast<int32_t>(offsetof(XslOutput_t472631936, ___doctypePublic_7)); }
	inline String_t* get_doctypePublic_7() const { return ___doctypePublic_7; }
	inline String_t** get_address_of_doctypePublic_7() { return &___doctypePublic_7; }
	inline void set_doctypePublic_7(String_t* value)
	{
		___doctypePublic_7 = value;
		Il2CppCodeGenWriteBarrier(&___doctypePublic_7, value);
	}

	inline static int32_t get_offset_of_doctypeSystem_8() { return static_cast<int32_t>(offsetof(XslOutput_t472631936, ___doctypeSystem_8)); }
	inline String_t* get_doctypeSystem_8() const { return ___doctypeSystem_8; }
	inline String_t** get_address_of_doctypeSystem_8() { return &___doctypeSystem_8; }
	inline void set_doctypeSystem_8(String_t* value)
	{
		___doctypeSystem_8 = value;
		Il2CppCodeGenWriteBarrier(&___doctypeSystem_8, value);
	}

	inline static int32_t get_offset_of_cdataSectionElements_9() { return static_cast<int32_t>(offsetof(XslOutput_t472631936, ___cdataSectionElements_9)); }
	inline XmlQualifiedNameU5BU5D_t1471530361* get_cdataSectionElements_9() const { return ___cdataSectionElements_9; }
	inline XmlQualifiedNameU5BU5D_t1471530361** get_address_of_cdataSectionElements_9() { return &___cdataSectionElements_9; }
	inline void set_cdataSectionElements_9(XmlQualifiedNameU5BU5D_t1471530361* value)
	{
		___cdataSectionElements_9 = value;
		Il2CppCodeGenWriteBarrier(&___cdataSectionElements_9, value);
	}

	inline static int32_t get_offset_of_indent_10() { return static_cast<int32_t>(offsetof(XslOutput_t472631936, ___indent_10)); }
	inline String_t* get_indent_10() const { return ___indent_10; }
	inline String_t** get_address_of_indent_10() { return &___indent_10; }
	inline void set_indent_10(String_t* value)
	{
		___indent_10 = value;
		Il2CppCodeGenWriteBarrier(&___indent_10, value);
	}

	inline static int32_t get_offset_of_mediaType_11() { return static_cast<int32_t>(offsetof(XslOutput_t472631936, ___mediaType_11)); }
	inline String_t* get_mediaType_11() const { return ___mediaType_11; }
	inline String_t** get_address_of_mediaType_11() { return &___mediaType_11; }
	inline void set_mediaType_11(String_t* value)
	{
		___mediaType_11 = value;
		Il2CppCodeGenWriteBarrier(&___mediaType_11, value);
	}

	inline static int32_t get_offset_of_stylesheetVersion_12() { return static_cast<int32_t>(offsetof(XslOutput_t472631936, ___stylesheetVersion_12)); }
	inline String_t* get_stylesheetVersion_12() const { return ___stylesheetVersion_12; }
	inline String_t** get_address_of_stylesheetVersion_12() { return &___stylesheetVersion_12; }
	inline void set_stylesheetVersion_12(String_t* value)
	{
		___stylesheetVersion_12 = value;
		Il2CppCodeGenWriteBarrier(&___stylesheetVersion_12, value);
	}

	inline static int32_t get_offset_of_cdSectsList_13() { return static_cast<int32_t>(offsetof(XslOutput_t472631936, ___cdSectsList_13)); }
	inline ArrayList_t2718874744 * get_cdSectsList_13() const { return ___cdSectsList_13; }
	inline ArrayList_t2718874744 ** get_address_of_cdSectsList_13() { return &___cdSectsList_13; }
	inline void set_cdSectsList_13(ArrayList_t2718874744 * value)
	{
		___cdSectsList_13 = value;
		Il2CppCodeGenWriteBarrier(&___cdSectsList_13, value);
	}
};

struct XslOutput_t472631936_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.XslOutput::<>f__switch$map1C
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map1C_14;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.XslOutput::<>f__switch$map1D
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map1D_15;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.XslOutput::<>f__switch$map1E
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map1E_16;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.XslOutput::<>f__switch$map1F
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map1F_17;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.XslOutput::<>f__switch$map20
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map20_18;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1C_14() { return static_cast<int32_t>(offsetof(XslOutput_t472631936_StaticFields, ___U3CU3Ef__switchU24map1C_14)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map1C_14() const { return ___U3CU3Ef__switchU24map1C_14; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map1C_14() { return &___U3CU3Ef__switchU24map1C_14; }
	inline void set_U3CU3Ef__switchU24map1C_14(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map1C_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1C_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1D_15() { return static_cast<int32_t>(offsetof(XslOutput_t472631936_StaticFields, ___U3CU3Ef__switchU24map1D_15)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map1D_15() const { return ___U3CU3Ef__switchU24map1D_15; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map1D_15() { return &___U3CU3Ef__switchU24map1D_15; }
	inline void set_U3CU3Ef__switchU24map1D_15(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map1D_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1D_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1E_16() { return static_cast<int32_t>(offsetof(XslOutput_t472631936_StaticFields, ___U3CU3Ef__switchU24map1E_16)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map1E_16() const { return ___U3CU3Ef__switchU24map1E_16; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map1E_16() { return &___U3CU3Ef__switchU24map1E_16; }
	inline void set_U3CU3Ef__switchU24map1E_16(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map1E_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1E_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1F_17() { return static_cast<int32_t>(offsetof(XslOutput_t472631936_StaticFields, ___U3CU3Ef__switchU24map1F_17)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map1F_17() const { return ___U3CU3Ef__switchU24map1F_17; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map1F_17() { return &___U3CU3Ef__switchU24map1F_17; }
	inline void set_U3CU3Ef__switchU24map1F_17(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map1F_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1F_17, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map20_18() { return static_cast<int32_t>(offsetof(XslOutput_t472631936_StaticFields, ___U3CU3Ef__switchU24map20_18)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map20_18() const { return ___U3CU3Ef__switchU24map20_18; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map20_18() { return &___U3CU3Ef__switchU24map20_18; }
	inline void set_U3CU3Ef__switchU24map20_18(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map20_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map20_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
