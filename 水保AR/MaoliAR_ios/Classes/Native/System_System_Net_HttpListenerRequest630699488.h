﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String[]
struct StringU5BU5D_t1281789340;
// System.Net.CookieCollection
struct CookieCollection_t3881042616;
// System.Net.WebHeaderCollection
struct WebHeaderCollection_t1942268960;
// System.String
struct String_t;
// System.IO.Stream
struct Stream_t1273022909;
// System.Version
struct Version_t3456873960;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t407452768;
// System.Uri
struct Uri_t100236324;
// System.Net.HttpListenerContext
struct HttpListenerContext_t424880822;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpListenerRequest
struct  HttpListenerRequest_t630699488  : public Il2CppObject
{
public:
	// System.String[] System.Net.HttpListenerRequest::accept_types
	StringU5BU5D_t1281789340* ___accept_types_0;
	// System.Int64 System.Net.HttpListenerRequest::content_length
	int64_t ___content_length_1;
	// System.Boolean System.Net.HttpListenerRequest::cl_set
	bool ___cl_set_2;
	// System.Net.CookieCollection System.Net.HttpListenerRequest::cookies
	CookieCollection_t3881042616 * ___cookies_3;
	// System.Net.WebHeaderCollection System.Net.HttpListenerRequest::headers
	WebHeaderCollection_t1942268960 * ___headers_4;
	// System.String System.Net.HttpListenerRequest::method
	String_t* ___method_5;
	// System.IO.Stream System.Net.HttpListenerRequest::input_stream
	Stream_t1273022909 * ___input_stream_6;
	// System.Version System.Net.HttpListenerRequest::version
	Version_t3456873960 * ___version_7;
	// System.Collections.Specialized.NameValueCollection System.Net.HttpListenerRequest::query_string
	NameValueCollection_t407452768 * ___query_string_8;
	// System.String System.Net.HttpListenerRequest::raw_url
	String_t* ___raw_url_9;
	// System.Uri System.Net.HttpListenerRequest::url
	Uri_t100236324 * ___url_10;
	// System.Uri System.Net.HttpListenerRequest::referrer
	Uri_t100236324 * ___referrer_11;
	// System.String[] System.Net.HttpListenerRequest::user_languages
	StringU5BU5D_t1281789340* ___user_languages_12;
	// System.Net.HttpListenerContext System.Net.HttpListenerRequest::context
	HttpListenerContext_t424880822 * ___context_13;
	// System.Boolean System.Net.HttpListenerRequest::is_chunked
	bool ___is_chunked_14;

public:
	inline static int32_t get_offset_of_accept_types_0() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___accept_types_0)); }
	inline StringU5BU5D_t1281789340* get_accept_types_0() const { return ___accept_types_0; }
	inline StringU5BU5D_t1281789340** get_address_of_accept_types_0() { return &___accept_types_0; }
	inline void set_accept_types_0(StringU5BU5D_t1281789340* value)
	{
		___accept_types_0 = value;
		Il2CppCodeGenWriteBarrier(&___accept_types_0, value);
	}

	inline static int32_t get_offset_of_content_length_1() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___content_length_1)); }
	inline int64_t get_content_length_1() const { return ___content_length_1; }
	inline int64_t* get_address_of_content_length_1() { return &___content_length_1; }
	inline void set_content_length_1(int64_t value)
	{
		___content_length_1 = value;
	}

	inline static int32_t get_offset_of_cl_set_2() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___cl_set_2)); }
	inline bool get_cl_set_2() const { return ___cl_set_2; }
	inline bool* get_address_of_cl_set_2() { return &___cl_set_2; }
	inline void set_cl_set_2(bool value)
	{
		___cl_set_2 = value;
	}

	inline static int32_t get_offset_of_cookies_3() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___cookies_3)); }
	inline CookieCollection_t3881042616 * get_cookies_3() const { return ___cookies_3; }
	inline CookieCollection_t3881042616 ** get_address_of_cookies_3() { return &___cookies_3; }
	inline void set_cookies_3(CookieCollection_t3881042616 * value)
	{
		___cookies_3 = value;
		Il2CppCodeGenWriteBarrier(&___cookies_3, value);
	}

	inline static int32_t get_offset_of_headers_4() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___headers_4)); }
	inline WebHeaderCollection_t1942268960 * get_headers_4() const { return ___headers_4; }
	inline WebHeaderCollection_t1942268960 ** get_address_of_headers_4() { return &___headers_4; }
	inline void set_headers_4(WebHeaderCollection_t1942268960 * value)
	{
		___headers_4 = value;
		Il2CppCodeGenWriteBarrier(&___headers_4, value);
	}

	inline static int32_t get_offset_of_method_5() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___method_5)); }
	inline String_t* get_method_5() const { return ___method_5; }
	inline String_t** get_address_of_method_5() { return &___method_5; }
	inline void set_method_5(String_t* value)
	{
		___method_5 = value;
		Il2CppCodeGenWriteBarrier(&___method_5, value);
	}

	inline static int32_t get_offset_of_input_stream_6() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___input_stream_6)); }
	inline Stream_t1273022909 * get_input_stream_6() const { return ___input_stream_6; }
	inline Stream_t1273022909 ** get_address_of_input_stream_6() { return &___input_stream_6; }
	inline void set_input_stream_6(Stream_t1273022909 * value)
	{
		___input_stream_6 = value;
		Il2CppCodeGenWriteBarrier(&___input_stream_6, value);
	}

	inline static int32_t get_offset_of_version_7() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___version_7)); }
	inline Version_t3456873960 * get_version_7() const { return ___version_7; }
	inline Version_t3456873960 ** get_address_of_version_7() { return &___version_7; }
	inline void set_version_7(Version_t3456873960 * value)
	{
		___version_7 = value;
		Il2CppCodeGenWriteBarrier(&___version_7, value);
	}

	inline static int32_t get_offset_of_query_string_8() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___query_string_8)); }
	inline NameValueCollection_t407452768 * get_query_string_8() const { return ___query_string_8; }
	inline NameValueCollection_t407452768 ** get_address_of_query_string_8() { return &___query_string_8; }
	inline void set_query_string_8(NameValueCollection_t407452768 * value)
	{
		___query_string_8 = value;
		Il2CppCodeGenWriteBarrier(&___query_string_8, value);
	}

	inline static int32_t get_offset_of_raw_url_9() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___raw_url_9)); }
	inline String_t* get_raw_url_9() const { return ___raw_url_9; }
	inline String_t** get_address_of_raw_url_9() { return &___raw_url_9; }
	inline void set_raw_url_9(String_t* value)
	{
		___raw_url_9 = value;
		Il2CppCodeGenWriteBarrier(&___raw_url_9, value);
	}

	inline static int32_t get_offset_of_url_10() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___url_10)); }
	inline Uri_t100236324 * get_url_10() const { return ___url_10; }
	inline Uri_t100236324 ** get_address_of_url_10() { return &___url_10; }
	inline void set_url_10(Uri_t100236324 * value)
	{
		___url_10 = value;
		Il2CppCodeGenWriteBarrier(&___url_10, value);
	}

	inline static int32_t get_offset_of_referrer_11() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___referrer_11)); }
	inline Uri_t100236324 * get_referrer_11() const { return ___referrer_11; }
	inline Uri_t100236324 ** get_address_of_referrer_11() { return &___referrer_11; }
	inline void set_referrer_11(Uri_t100236324 * value)
	{
		___referrer_11 = value;
		Il2CppCodeGenWriteBarrier(&___referrer_11, value);
	}

	inline static int32_t get_offset_of_user_languages_12() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___user_languages_12)); }
	inline StringU5BU5D_t1281789340* get_user_languages_12() const { return ___user_languages_12; }
	inline StringU5BU5D_t1281789340** get_address_of_user_languages_12() { return &___user_languages_12; }
	inline void set_user_languages_12(StringU5BU5D_t1281789340* value)
	{
		___user_languages_12 = value;
		Il2CppCodeGenWriteBarrier(&___user_languages_12, value);
	}

	inline static int32_t get_offset_of_context_13() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___context_13)); }
	inline HttpListenerContext_t424880822 * get_context_13() const { return ___context_13; }
	inline HttpListenerContext_t424880822 ** get_address_of_context_13() { return &___context_13; }
	inline void set_context_13(HttpListenerContext_t424880822 * value)
	{
		___context_13 = value;
		Il2CppCodeGenWriteBarrier(&___context_13, value);
	}

	inline static int32_t get_offset_of_is_chunked_14() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488, ___is_chunked_14)); }
	inline bool get_is_chunked_14() const { return ___is_chunked_14; }
	inline bool* get_address_of_is_chunked_14() { return &___is_chunked_14; }
	inline void set_is_chunked_14(bool value)
	{
		___is_chunked_14 = value;
	}
};

struct HttpListenerRequest_t630699488_StaticFields
{
public:
	// System.Byte[] System.Net.HttpListenerRequest::_100continue
	ByteU5BU5D_t4116647657* ____100continue_15;
	// System.String[] System.Net.HttpListenerRequest::no_body_methods
	StringU5BU5D_t1281789340* ___no_body_methods_16;
	// System.Char[] System.Net.HttpListenerRequest::separators
	CharU5BU5D_t3528271667* ___separators_17;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Net.HttpListenerRequest::<>f__switch$mapC
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24mapC_18;

public:
	inline static int32_t get_offset_of__100continue_15() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488_StaticFields, ____100continue_15)); }
	inline ByteU5BU5D_t4116647657* get__100continue_15() const { return ____100continue_15; }
	inline ByteU5BU5D_t4116647657** get_address_of__100continue_15() { return &____100continue_15; }
	inline void set__100continue_15(ByteU5BU5D_t4116647657* value)
	{
		____100continue_15 = value;
		Il2CppCodeGenWriteBarrier(&____100continue_15, value);
	}

	inline static int32_t get_offset_of_no_body_methods_16() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488_StaticFields, ___no_body_methods_16)); }
	inline StringU5BU5D_t1281789340* get_no_body_methods_16() const { return ___no_body_methods_16; }
	inline StringU5BU5D_t1281789340** get_address_of_no_body_methods_16() { return &___no_body_methods_16; }
	inline void set_no_body_methods_16(StringU5BU5D_t1281789340* value)
	{
		___no_body_methods_16 = value;
		Il2CppCodeGenWriteBarrier(&___no_body_methods_16, value);
	}

	inline static int32_t get_offset_of_separators_17() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488_StaticFields, ___separators_17)); }
	inline CharU5BU5D_t3528271667* get_separators_17() const { return ___separators_17; }
	inline CharU5BU5D_t3528271667** get_address_of_separators_17() { return &___separators_17; }
	inline void set_separators_17(CharU5BU5D_t3528271667* value)
	{
		___separators_17 = value;
		Il2CppCodeGenWriteBarrier(&___separators_17, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapC_18() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t630699488_StaticFields, ___U3CU3Ef__switchU24mapC_18)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24mapC_18() const { return ___U3CU3Ef__switchU24mapC_18; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24mapC_18() { return &___U3CU3Ef__switchU24mapC_18; }
	inline void set_U3CU3Ef__switchU24mapC_18(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24mapC_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapC_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
