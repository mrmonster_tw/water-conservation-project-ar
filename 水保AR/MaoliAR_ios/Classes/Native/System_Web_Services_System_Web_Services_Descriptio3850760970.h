﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Web_Services_System_Web_Services_Description_10559386.h"

// System.Web.Services.Description.Binding
struct Binding_t3952932040;
// System.String
struct String_t;
// System.Xml.Serialization.CodeIdentifiers
struct CodeIdentifiers_t4095039290;
// System.CodeDom.CodeNamespace
struct CodeNamespace_t2165007136;
// System.CodeDom.CodeTypeDeclaration
struct CodeTypeDeclaration_t2359234283;
// System.Web.Services.Description.Message
struct Message_t1729997838;
// System.Web.Services.Description.Operation
struct Operation_t1010384202;
// System.Web.Services.Description.OperationBinding
struct OperationBinding_t314325322;
// System.Web.Services.Description.Port
struct Port_t4151257856;
// System.Web.Services.Description.PortType
struct PortType_t3119525474;
// System.Web.Services.Description.Service
struct Service_t2672957899;
// System.Web.Services.Description.ServiceDescriptionImporter
struct ServiceDescriptionImporter_t2272364891;
// System.Web.Services.Description.ImportInfo
struct ImportInfo_t202809424;
// System.Xml.Serialization.XmlSchemas
struct XmlSchemas_t3283371924;
// System.Collections.ArrayList
struct ArrayList_t2718874744;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.ProtocolImporter
struct  ProtocolImporter_t3850760970  : public Il2CppObject
{
public:
	// System.Web.Services.Description.Binding System.Web.Services.Description.ProtocolImporter::binding
	Binding_t3952932040 * ___binding_0;
	// System.String System.Web.Services.Description.ProtocolImporter::className
	String_t* ___className_1;
	// System.Xml.Serialization.CodeIdentifiers System.Web.Services.Description.ProtocolImporter::classNames
	CodeIdentifiers_t4095039290 * ___classNames_2;
	// System.CodeDom.CodeNamespace System.Web.Services.Description.ProtocolImporter::codeNamespace
	CodeNamespace_t2165007136 * ___codeNamespace_3;
	// System.CodeDom.CodeTypeDeclaration System.Web.Services.Description.ProtocolImporter::codeTypeDeclaration
	CodeTypeDeclaration_t2359234283 * ___codeTypeDeclaration_4;
	// System.Web.Services.Description.Message System.Web.Services.Description.ProtocolImporter::inputMessage
	Message_t1729997838 * ___inputMessage_5;
	// System.String System.Web.Services.Description.ProtocolImporter::methodName
	String_t* ___methodName_6;
	// System.Web.Services.Description.Operation System.Web.Services.Description.ProtocolImporter::operation
	Operation_t1010384202 * ___operation_7;
	// System.Web.Services.Description.OperationBinding System.Web.Services.Description.ProtocolImporter::operationBinding
	OperationBinding_t314325322 * ___operationBinding_8;
	// System.Web.Services.Description.Message System.Web.Services.Description.ProtocolImporter::outputMessage
	Message_t1729997838 * ___outputMessage_9;
	// System.Web.Services.Description.Port System.Web.Services.Description.ProtocolImporter::port
	Port_t4151257856 * ___port_10;
	// System.Web.Services.Description.PortType System.Web.Services.Description.ProtocolImporter::portType
	PortType_t3119525474 * ___portType_11;
	// System.Web.Services.Description.Service System.Web.Services.Description.ProtocolImporter::service
	Service_t2672957899 * ___service_12;
	// System.Web.Services.Description.ServiceDescriptionImportWarnings System.Web.Services.Description.ProtocolImporter::warnings
	int32_t ___warnings_13;
	// System.Web.Services.Description.ServiceDescriptionImporter System.Web.Services.Description.ProtocolImporter::descriptionImporter
	ServiceDescriptionImporter_t2272364891 * ___descriptionImporter_14;
	// System.Web.Services.Description.ImportInfo System.Web.Services.Description.ProtocolImporter::iinfo
	ImportInfo_t202809424 * ___iinfo_15;
	// System.Xml.Serialization.XmlSchemas System.Web.Services.Description.ProtocolImporter::xmlSchemas
	XmlSchemas_t3283371924 * ___xmlSchemas_16;
	// System.Xml.Serialization.XmlSchemas System.Web.Services.Description.ProtocolImporter::soapSchemas
	XmlSchemas_t3283371924 * ___soapSchemas_17;
	// System.Collections.ArrayList System.Web.Services.Description.ProtocolImporter::asyncTypes
	ArrayList_t2718874744 * ___asyncTypes_18;

public:
	inline static int32_t get_offset_of_binding_0() { return static_cast<int32_t>(offsetof(ProtocolImporter_t3850760970, ___binding_0)); }
	inline Binding_t3952932040 * get_binding_0() const { return ___binding_0; }
	inline Binding_t3952932040 ** get_address_of_binding_0() { return &___binding_0; }
	inline void set_binding_0(Binding_t3952932040 * value)
	{
		___binding_0 = value;
		Il2CppCodeGenWriteBarrier(&___binding_0, value);
	}

	inline static int32_t get_offset_of_className_1() { return static_cast<int32_t>(offsetof(ProtocolImporter_t3850760970, ___className_1)); }
	inline String_t* get_className_1() const { return ___className_1; }
	inline String_t** get_address_of_className_1() { return &___className_1; }
	inline void set_className_1(String_t* value)
	{
		___className_1 = value;
		Il2CppCodeGenWriteBarrier(&___className_1, value);
	}

	inline static int32_t get_offset_of_classNames_2() { return static_cast<int32_t>(offsetof(ProtocolImporter_t3850760970, ___classNames_2)); }
	inline CodeIdentifiers_t4095039290 * get_classNames_2() const { return ___classNames_2; }
	inline CodeIdentifiers_t4095039290 ** get_address_of_classNames_2() { return &___classNames_2; }
	inline void set_classNames_2(CodeIdentifiers_t4095039290 * value)
	{
		___classNames_2 = value;
		Il2CppCodeGenWriteBarrier(&___classNames_2, value);
	}

	inline static int32_t get_offset_of_codeNamespace_3() { return static_cast<int32_t>(offsetof(ProtocolImporter_t3850760970, ___codeNamespace_3)); }
	inline CodeNamespace_t2165007136 * get_codeNamespace_3() const { return ___codeNamespace_3; }
	inline CodeNamespace_t2165007136 ** get_address_of_codeNamespace_3() { return &___codeNamespace_3; }
	inline void set_codeNamespace_3(CodeNamespace_t2165007136 * value)
	{
		___codeNamespace_3 = value;
		Il2CppCodeGenWriteBarrier(&___codeNamespace_3, value);
	}

	inline static int32_t get_offset_of_codeTypeDeclaration_4() { return static_cast<int32_t>(offsetof(ProtocolImporter_t3850760970, ___codeTypeDeclaration_4)); }
	inline CodeTypeDeclaration_t2359234283 * get_codeTypeDeclaration_4() const { return ___codeTypeDeclaration_4; }
	inline CodeTypeDeclaration_t2359234283 ** get_address_of_codeTypeDeclaration_4() { return &___codeTypeDeclaration_4; }
	inline void set_codeTypeDeclaration_4(CodeTypeDeclaration_t2359234283 * value)
	{
		___codeTypeDeclaration_4 = value;
		Il2CppCodeGenWriteBarrier(&___codeTypeDeclaration_4, value);
	}

	inline static int32_t get_offset_of_inputMessage_5() { return static_cast<int32_t>(offsetof(ProtocolImporter_t3850760970, ___inputMessage_5)); }
	inline Message_t1729997838 * get_inputMessage_5() const { return ___inputMessage_5; }
	inline Message_t1729997838 ** get_address_of_inputMessage_5() { return &___inputMessage_5; }
	inline void set_inputMessage_5(Message_t1729997838 * value)
	{
		___inputMessage_5 = value;
		Il2CppCodeGenWriteBarrier(&___inputMessage_5, value);
	}

	inline static int32_t get_offset_of_methodName_6() { return static_cast<int32_t>(offsetof(ProtocolImporter_t3850760970, ___methodName_6)); }
	inline String_t* get_methodName_6() const { return ___methodName_6; }
	inline String_t** get_address_of_methodName_6() { return &___methodName_6; }
	inline void set_methodName_6(String_t* value)
	{
		___methodName_6 = value;
		Il2CppCodeGenWriteBarrier(&___methodName_6, value);
	}

	inline static int32_t get_offset_of_operation_7() { return static_cast<int32_t>(offsetof(ProtocolImporter_t3850760970, ___operation_7)); }
	inline Operation_t1010384202 * get_operation_7() const { return ___operation_7; }
	inline Operation_t1010384202 ** get_address_of_operation_7() { return &___operation_7; }
	inline void set_operation_7(Operation_t1010384202 * value)
	{
		___operation_7 = value;
		Il2CppCodeGenWriteBarrier(&___operation_7, value);
	}

	inline static int32_t get_offset_of_operationBinding_8() { return static_cast<int32_t>(offsetof(ProtocolImporter_t3850760970, ___operationBinding_8)); }
	inline OperationBinding_t314325322 * get_operationBinding_8() const { return ___operationBinding_8; }
	inline OperationBinding_t314325322 ** get_address_of_operationBinding_8() { return &___operationBinding_8; }
	inline void set_operationBinding_8(OperationBinding_t314325322 * value)
	{
		___operationBinding_8 = value;
		Il2CppCodeGenWriteBarrier(&___operationBinding_8, value);
	}

	inline static int32_t get_offset_of_outputMessage_9() { return static_cast<int32_t>(offsetof(ProtocolImporter_t3850760970, ___outputMessage_9)); }
	inline Message_t1729997838 * get_outputMessage_9() const { return ___outputMessage_9; }
	inline Message_t1729997838 ** get_address_of_outputMessage_9() { return &___outputMessage_9; }
	inline void set_outputMessage_9(Message_t1729997838 * value)
	{
		___outputMessage_9 = value;
		Il2CppCodeGenWriteBarrier(&___outputMessage_9, value);
	}

	inline static int32_t get_offset_of_port_10() { return static_cast<int32_t>(offsetof(ProtocolImporter_t3850760970, ___port_10)); }
	inline Port_t4151257856 * get_port_10() const { return ___port_10; }
	inline Port_t4151257856 ** get_address_of_port_10() { return &___port_10; }
	inline void set_port_10(Port_t4151257856 * value)
	{
		___port_10 = value;
		Il2CppCodeGenWriteBarrier(&___port_10, value);
	}

	inline static int32_t get_offset_of_portType_11() { return static_cast<int32_t>(offsetof(ProtocolImporter_t3850760970, ___portType_11)); }
	inline PortType_t3119525474 * get_portType_11() const { return ___portType_11; }
	inline PortType_t3119525474 ** get_address_of_portType_11() { return &___portType_11; }
	inline void set_portType_11(PortType_t3119525474 * value)
	{
		___portType_11 = value;
		Il2CppCodeGenWriteBarrier(&___portType_11, value);
	}

	inline static int32_t get_offset_of_service_12() { return static_cast<int32_t>(offsetof(ProtocolImporter_t3850760970, ___service_12)); }
	inline Service_t2672957899 * get_service_12() const { return ___service_12; }
	inline Service_t2672957899 ** get_address_of_service_12() { return &___service_12; }
	inline void set_service_12(Service_t2672957899 * value)
	{
		___service_12 = value;
		Il2CppCodeGenWriteBarrier(&___service_12, value);
	}

	inline static int32_t get_offset_of_warnings_13() { return static_cast<int32_t>(offsetof(ProtocolImporter_t3850760970, ___warnings_13)); }
	inline int32_t get_warnings_13() const { return ___warnings_13; }
	inline int32_t* get_address_of_warnings_13() { return &___warnings_13; }
	inline void set_warnings_13(int32_t value)
	{
		___warnings_13 = value;
	}

	inline static int32_t get_offset_of_descriptionImporter_14() { return static_cast<int32_t>(offsetof(ProtocolImporter_t3850760970, ___descriptionImporter_14)); }
	inline ServiceDescriptionImporter_t2272364891 * get_descriptionImporter_14() const { return ___descriptionImporter_14; }
	inline ServiceDescriptionImporter_t2272364891 ** get_address_of_descriptionImporter_14() { return &___descriptionImporter_14; }
	inline void set_descriptionImporter_14(ServiceDescriptionImporter_t2272364891 * value)
	{
		___descriptionImporter_14 = value;
		Il2CppCodeGenWriteBarrier(&___descriptionImporter_14, value);
	}

	inline static int32_t get_offset_of_iinfo_15() { return static_cast<int32_t>(offsetof(ProtocolImporter_t3850760970, ___iinfo_15)); }
	inline ImportInfo_t202809424 * get_iinfo_15() const { return ___iinfo_15; }
	inline ImportInfo_t202809424 ** get_address_of_iinfo_15() { return &___iinfo_15; }
	inline void set_iinfo_15(ImportInfo_t202809424 * value)
	{
		___iinfo_15 = value;
		Il2CppCodeGenWriteBarrier(&___iinfo_15, value);
	}

	inline static int32_t get_offset_of_xmlSchemas_16() { return static_cast<int32_t>(offsetof(ProtocolImporter_t3850760970, ___xmlSchemas_16)); }
	inline XmlSchemas_t3283371924 * get_xmlSchemas_16() const { return ___xmlSchemas_16; }
	inline XmlSchemas_t3283371924 ** get_address_of_xmlSchemas_16() { return &___xmlSchemas_16; }
	inline void set_xmlSchemas_16(XmlSchemas_t3283371924 * value)
	{
		___xmlSchemas_16 = value;
		Il2CppCodeGenWriteBarrier(&___xmlSchemas_16, value);
	}

	inline static int32_t get_offset_of_soapSchemas_17() { return static_cast<int32_t>(offsetof(ProtocolImporter_t3850760970, ___soapSchemas_17)); }
	inline XmlSchemas_t3283371924 * get_soapSchemas_17() const { return ___soapSchemas_17; }
	inline XmlSchemas_t3283371924 ** get_address_of_soapSchemas_17() { return &___soapSchemas_17; }
	inline void set_soapSchemas_17(XmlSchemas_t3283371924 * value)
	{
		___soapSchemas_17 = value;
		Il2CppCodeGenWriteBarrier(&___soapSchemas_17, value);
	}

	inline static int32_t get_offset_of_asyncTypes_18() { return static_cast<int32_t>(offsetof(ProtocolImporter_t3850760970, ___asyncTypes_18)); }
	inline ArrayList_t2718874744 * get_asyncTypes_18() const { return ___asyncTypes_18; }
	inline ArrayList_t2718874744 ** get_address_of_asyncTypes_18() { return &___asyncTypes_18; }
	inline void set_asyncTypes_18(ArrayList_t2718874744 * value)
	{
		___asyncTypes_18 = value;
		Il2CppCodeGenWriteBarrier(&___asyncTypes_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
