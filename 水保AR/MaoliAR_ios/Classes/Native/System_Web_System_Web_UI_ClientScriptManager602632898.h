﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Web.UI.ClientScriptManager/ScriptEntry
struct ScriptEntry_t1923521595;
// System.Web.UI.Page
struct Page_t770747966;
// System.Int32[]
struct Int32U5BU5D_t385246372;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.ClientScriptManager
struct  ClientScriptManager_t602632898  : public Il2CppObject
{
public:
	// System.Collections.Hashtable System.Web.UI.ClientScriptManager::registeredArrayDeclares
	Hashtable_t1853889766 * ___registeredArrayDeclares_0;
	// System.Web.UI.ClientScriptManager/ScriptEntry System.Web.UI.ClientScriptManager::clientScriptBlocks
	ScriptEntry_t1923521595 * ___clientScriptBlocks_1;
	// System.Web.UI.ClientScriptManager/ScriptEntry System.Web.UI.ClientScriptManager::startupScriptBlocks
	ScriptEntry_t1923521595 * ___startupScriptBlocks_2;
	// System.Collections.Hashtable System.Web.UI.ClientScriptManager::hiddenFields
	Hashtable_t1853889766 * ___hiddenFields_3;
	// System.Web.UI.ClientScriptManager/ScriptEntry System.Web.UI.ClientScriptManager::submitStatements
	ScriptEntry_t1923521595 * ___submitStatements_4;
	// System.Web.UI.Page System.Web.UI.ClientScriptManager::page
	Page_t770747966 * ___page_5;
	// System.Int32[] System.Web.UI.ClientScriptManager::eventValidationValues
	Int32U5BU5D_t385246372* ___eventValidationValues_6;
	// System.Int32 System.Web.UI.ClientScriptManager::eventValidationPos
	int32_t ___eventValidationPos_7;
	// System.Collections.Hashtable System.Web.UI.ClientScriptManager::expandoAttributes
	Hashtable_t1853889766 * ___expandoAttributes_8;
	// System.Boolean System.Web.UI.ClientScriptManager::_hasRegisteredForEventValidationOnCallback
	bool ____hasRegisteredForEventValidationOnCallback_9;
	// System.Boolean System.Web.UI.ClientScriptManager::_pageInRender
	bool ____pageInRender_10;
	// System.Boolean System.Web.UI.ClientScriptManager::_initCallBackRegistered
	bool ____initCallBackRegistered_11;
	// System.Boolean System.Web.UI.ClientScriptManager::_webFormClientScriptRendered
	bool ____webFormClientScriptRendered_12;
	// System.Boolean System.Web.UI.ClientScriptManager::_webFormClientScriptRequired
	bool ____webFormClientScriptRequired_13;
	// System.Boolean System.Web.UI.ClientScriptManager::_scriptTagOpened
	bool ____scriptTagOpened_14;

public:
	inline static int32_t get_offset_of_registeredArrayDeclares_0() { return static_cast<int32_t>(offsetof(ClientScriptManager_t602632898, ___registeredArrayDeclares_0)); }
	inline Hashtable_t1853889766 * get_registeredArrayDeclares_0() const { return ___registeredArrayDeclares_0; }
	inline Hashtable_t1853889766 ** get_address_of_registeredArrayDeclares_0() { return &___registeredArrayDeclares_0; }
	inline void set_registeredArrayDeclares_0(Hashtable_t1853889766 * value)
	{
		___registeredArrayDeclares_0 = value;
		Il2CppCodeGenWriteBarrier(&___registeredArrayDeclares_0, value);
	}

	inline static int32_t get_offset_of_clientScriptBlocks_1() { return static_cast<int32_t>(offsetof(ClientScriptManager_t602632898, ___clientScriptBlocks_1)); }
	inline ScriptEntry_t1923521595 * get_clientScriptBlocks_1() const { return ___clientScriptBlocks_1; }
	inline ScriptEntry_t1923521595 ** get_address_of_clientScriptBlocks_1() { return &___clientScriptBlocks_1; }
	inline void set_clientScriptBlocks_1(ScriptEntry_t1923521595 * value)
	{
		___clientScriptBlocks_1 = value;
		Il2CppCodeGenWriteBarrier(&___clientScriptBlocks_1, value);
	}

	inline static int32_t get_offset_of_startupScriptBlocks_2() { return static_cast<int32_t>(offsetof(ClientScriptManager_t602632898, ___startupScriptBlocks_2)); }
	inline ScriptEntry_t1923521595 * get_startupScriptBlocks_2() const { return ___startupScriptBlocks_2; }
	inline ScriptEntry_t1923521595 ** get_address_of_startupScriptBlocks_2() { return &___startupScriptBlocks_2; }
	inline void set_startupScriptBlocks_2(ScriptEntry_t1923521595 * value)
	{
		___startupScriptBlocks_2 = value;
		Il2CppCodeGenWriteBarrier(&___startupScriptBlocks_2, value);
	}

	inline static int32_t get_offset_of_hiddenFields_3() { return static_cast<int32_t>(offsetof(ClientScriptManager_t602632898, ___hiddenFields_3)); }
	inline Hashtable_t1853889766 * get_hiddenFields_3() const { return ___hiddenFields_3; }
	inline Hashtable_t1853889766 ** get_address_of_hiddenFields_3() { return &___hiddenFields_3; }
	inline void set_hiddenFields_3(Hashtable_t1853889766 * value)
	{
		___hiddenFields_3 = value;
		Il2CppCodeGenWriteBarrier(&___hiddenFields_3, value);
	}

	inline static int32_t get_offset_of_submitStatements_4() { return static_cast<int32_t>(offsetof(ClientScriptManager_t602632898, ___submitStatements_4)); }
	inline ScriptEntry_t1923521595 * get_submitStatements_4() const { return ___submitStatements_4; }
	inline ScriptEntry_t1923521595 ** get_address_of_submitStatements_4() { return &___submitStatements_4; }
	inline void set_submitStatements_4(ScriptEntry_t1923521595 * value)
	{
		___submitStatements_4 = value;
		Il2CppCodeGenWriteBarrier(&___submitStatements_4, value);
	}

	inline static int32_t get_offset_of_page_5() { return static_cast<int32_t>(offsetof(ClientScriptManager_t602632898, ___page_5)); }
	inline Page_t770747966 * get_page_5() const { return ___page_5; }
	inline Page_t770747966 ** get_address_of_page_5() { return &___page_5; }
	inline void set_page_5(Page_t770747966 * value)
	{
		___page_5 = value;
		Il2CppCodeGenWriteBarrier(&___page_5, value);
	}

	inline static int32_t get_offset_of_eventValidationValues_6() { return static_cast<int32_t>(offsetof(ClientScriptManager_t602632898, ___eventValidationValues_6)); }
	inline Int32U5BU5D_t385246372* get_eventValidationValues_6() const { return ___eventValidationValues_6; }
	inline Int32U5BU5D_t385246372** get_address_of_eventValidationValues_6() { return &___eventValidationValues_6; }
	inline void set_eventValidationValues_6(Int32U5BU5D_t385246372* value)
	{
		___eventValidationValues_6 = value;
		Il2CppCodeGenWriteBarrier(&___eventValidationValues_6, value);
	}

	inline static int32_t get_offset_of_eventValidationPos_7() { return static_cast<int32_t>(offsetof(ClientScriptManager_t602632898, ___eventValidationPos_7)); }
	inline int32_t get_eventValidationPos_7() const { return ___eventValidationPos_7; }
	inline int32_t* get_address_of_eventValidationPos_7() { return &___eventValidationPos_7; }
	inline void set_eventValidationPos_7(int32_t value)
	{
		___eventValidationPos_7 = value;
	}

	inline static int32_t get_offset_of_expandoAttributes_8() { return static_cast<int32_t>(offsetof(ClientScriptManager_t602632898, ___expandoAttributes_8)); }
	inline Hashtable_t1853889766 * get_expandoAttributes_8() const { return ___expandoAttributes_8; }
	inline Hashtable_t1853889766 ** get_address_of_expandoAttributes_8() { return &___expandoAttributes_8; }
	inline void set_expandoAttributes_8(Hashtable_t1853889766 * value)
	{
		___expandoAttributes_8 = value;
		Il2CppCodeGenWriteBarrier(&___expandoAttributes_8, value);
	}

	inline static int32_t get_offset_of__hasRegisteredForEventValidationOnCallback_9() { return static_cast<int32_t>(offsetof(ClientScriptManager_t602632898, ____hasRegisteredForEventValidationOnCallback_9)); }
	inline bool get__hasRegisteredForEventValidationOnCallback_9() const { return ____hasRegisteredForEventValidationOnCallback_9; }
	inline bool* get_address_of__hasRegisteredForEventValidationOnCallback_9() { return &____hasRegisteredForEventValidationOnCallback_9; }
	inline void set__hasRegisteredForEventValidationOnCallback_9(bool value)
	{
		____hasRegisteredForEventValidationOnCallback_9 = value;
	}

	inline static int32_t get_offset_of__pageInRender_10() { return static_cast<int32_t>(offsetof(ClientScriptManager_t602632898, ____pageInRender_10)); }
	inline bool get__pageInRender_10() const { return ____pageInRender_10; }
	inline bool* get_address_of__pageInRender_10() { return &____pageInRender_10; }
	inline void set__pageInRender_10(bool value)
	{
		____pageInRender_10 = value;
	}

	inline static int32_t get_offset_of__initCallBackRegistered_11() { return static_cast<int32_t>(offsetof(ClientScriptManager_t602632898, ____initCallBackRegistered_11)); }
	inline bool get__initCallBackRegistered_11() const { return ____initCallBackRegistered_11; }
	inline bool* get_address_of__initCallBackRegistered_11() { return &____initCallBackRegistered_11; }
	inline void set__initCallBackRegistered_11(bool value)
	{
		____initCallBackRegistered_11 = value;
	}

	inline static int32_t get_offset_of__webFormClientScriptRendered_12() { return static_cast<int32_t>(offsetof(ClientScriptManager_t602632898, ____webFormClientScriptRendered_12)); }
	inline bool get__webFormClientScriptRendered_12() const { return ____webFormClientScriptRendered_12; }
	inline bool* get_address_of__webFormClientScriptRendered_12() { return &____webFormClientScriptRendered_12; }
	inline void set__webFormClientScriptRendered_12(bool value)
	{
		____webFormClientScriptRendered_12 = value;
	}

	inline static int32_t get_offset_of__webFormClientScriptRequired_13() { return static_cast<int32_t>(offsetof(ClientScriptManager_t602632898, ____webFormClientScriptRequired_13)); }
	inline bool get__webFormClientScriptRequired_13() const { return ____webFormClientScriptRequired_13; }
	inline bool* get_address_of__webFormClientScriptRequired_13() { return &____webFormClientScriptRequired_13; }
	inline void set__webFormClientScriptRequired_13(bool value)
	{
		____webFormClientScriptRequired_13 = value;
	}

	inline static int32_t get_offset_of__scriptTagOpened_14() { return static_cast<int32_t>(offsetof(ClientScriptManager_t602632898, ____scriptTagOpened_14)); }
	inline bool get__scriptTagOpened_14() const { return ____scriptTagOpened_14; }
	inline bool* get_address_of__scriptTagOpened_14() { return &____scriptTagOpened_14; }
	inline void set__scriptTagOpened_14(bool value)
	{
		____scriptTagOpened_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
