﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.SoapTypeAttribute
struct  SoapTypeAttribute_t2580020517  : public Attribute_t861562559
{
public:
	// System.String System.Xml.Serialization.SoapTypeAttribute::ns
	String_t* ___ns_0;
	// System.String System.Xml.Serialization.SoapTypeAttribute::typeName
	String_t* ___typeName_1;
	// System.Boolean System.Xml.Serialization.SoapTypeAttribute::includeInSchema
	bool ___includeInSchema_2;

public:
	inline static int32_t get_offset_of_ns_0() { return static_cast<int32_t>(offsetof(SoapTypeAttribute_t2580020517, ___ns_0)); }
	inline String_t* get_ns_0() const { return ___ns_0; }
	inline String_t** get_address_of_ns_0() { return &___ns_0; }
	inline void set_ns_0(String_t* value)
	{
		___ns_0 = value;
		Il2CppCodeGenWriteBarrier(&___ns_0, value);
	}

	inline static int32_t get_offset_of_typeName_1() { return static_cast<int32_t>(offsetof(SoapTypeAttribute_t2580020517, ___typeName_1)); }
	inline String_t* get_typeName_1() const { return ___typeName_1; }
	inline String_t** get_address_of_typeName_1() { return &___typeName_1; }
	inline void set_typeName_1(String_t* value)
	{
		___typeName_1 = value;
		Il2CppCodeGenWriteBarrier(&___typeName_1, value);
	}

	inline static int32_t get_offset_of_includeInSchema_2() { return static_cast<int32_t>(offsetof(SoapTypeAttribute_t2580020517, ___includeInSchema_2)); }
	inline bool get_includeInSchema_2() const { return ___includeInSchema_2; }
	inline bool* get_address_of_includeInSchema_2() { return &___includeInSchema_2; }
	inline void set_includeInSchema_2(bool value)
	{
		___includeInSchema_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
