﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Web_System_Web_UI_ClientScriptManager_Scrip1536882111.h"

// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Web.UI.ClientScriptManager/ScriptEntry
struct ScriptEntry_t1923521595;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.ClientScriptManager/ScriptEntry
struct  ScriptEntry_t1923521595  : public Il2CppObject
{
public:
	// System.Type System.Web.UI.ClientScriptManager/ScriptEntry::Type
	Type_t * ___Type_0;
	// System.String System.Web.UI.ClientScriptManager/ScriptEntry::Key
	String_t* ___Key_1;
	// System.String System.Web.UI.ClientScriptManager/ScriptEntry::Script
	String_t* ___Script_2;
	// System.Web.UI.ClientScriptManager/ScriptEntryFormat System.Web.UI.ClientScriptManager/ScriptEntry::Format
	int32_t ___Format_3;
	// System.Web.UI.ClientScriptManager/ScriptEntry System.Web.UI.ClientScriptManager/ScriptEntry::Next
	ScriptEntry_t1923521595 * ___Next_4;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(ScriptEntry_t1923521595, ___Type_0)); }
	inline Type_t * get_Type_0() const { return ___Type_0; }
	inline Type_t ** get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(Type_t * value)
	{
		___Type_0 = value;
		Il2CppCodeGenWriteBarrier(&___Type_0, value);
	}

	inline static int32_t get_offset_of_Key_1() { return static_cast<int32_t>(offsetof(ScriptEntry_t1923521595, ___Key_1)); }
	inline String_t* get_Key_1() const { return ___Key_1; }
	inline String_t** get_address_of_Key_1() { return &___Key_1; }
	inline void set_Key_1(String_t* value)
	{
		___Key_1 = value;
		Il2CppCodeGenWriteBarrier(&___Key_1, value);
	}

	inline static int32_t get_offset_of_Script_2() { return static_cast<int32_t>(offsetof(ScriptEntry_t1923521595, ___Script_2)); }
	inline String_t* get_Script_2() const { return ___Script_2; }
	inline String_t** get_address_of_Script_2() { return &___Script_2; }
	inline void set_Script_2(String_t* value)
	{
		___Script_2 = value;
		Il2CppCodeGenWriteBarrier(&___Script_2, value);
	}

	inline static int32_t get_offset_of_Format_3() { return static_cast<int32_t>(offsetof(ScriptEntry_t1923521595, ___Format_3)); }
	inline int32_t get_Format_3() const { return ___Format_3; }
	inline int32_t* get_address_of_Format_3() { return &___Format_3; }
	inline void set_Format_3(int32_t value)
	{
		___Format_3 = value;
	}

	inline static int32_t get_offset_of_Next_4() { return static_cast<int32_t>(offsetof(ScriptEntry_t1923521595, ___Next_4)); }
	inline ScriptEntry_t1923521595 * get_Next_4() const { return ___Next_4; }
	inline ScriptEntry_t1923521595 ** get_address_of_Next_4() { return &___Next_4; }
	inline void set_Next_4(ScriptEntry_t1923521595 * value)
	{
		___Next_4 = value;
		Il2CppCodeGenWriteBarrier(&___Next_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
