﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.ArrayList
struct ArrayList_t2718874744;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Caching.CacheItemEnumerator
struct  CacheItemEnumerator_t1234489326  : public Il2CppObject
{
public:
	// System.Collections.ArrayList System.Web.Caching.CacheItemEnumerator::list
	ArrayList_t2718874744 * ___list_0;
	// System.Int32 System.Web.Caching.CacheItemEnumerator::pos
	int32_t ___pos_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(CacheItemEnumerator_t1234489326, ___list_0)); }
	inline ArrayList_t2718874744 * get_list_0() const { return ___list_0; }
	inline ArrayList_t2718874744 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ArrayList_t2718874744 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier(&___list_0, value);
	}

	inline static int32_t get_offset_of_pos_1() { return static_cast<int32_t>(offsetof(CacheItemEnumerator_t1234489326, ___pos_1)); }
	inline int32_t get_pos_1() const { return ___pos_1; }
	inline int32_t* get_address_of_pos_1() { return &___pos_1; }
	inline void set_pos_1(int32_t value)
	{
		___pos_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
