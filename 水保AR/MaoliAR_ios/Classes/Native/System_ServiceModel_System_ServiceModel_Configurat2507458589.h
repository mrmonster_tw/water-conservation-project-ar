﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_ComponentModel_TypeConverter2249118273.h"

// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.SecurityAlgorithmSuiteConverter
struct  SecurityAlgorithmSuiteConverter_t2507458589  : public TypeConverter_t2249118273
{
public:

public:
};

struct SecurityAlgorithmSuiteConverter_t2507458589_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Configuration.SecurityAlgorithmSuiteConverter::<>f__switch$map11
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map11_0;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map11_0() { return static_cast<int32_t>(offsetof(SecurityAlgorithmSuiteConverter_t2507458589_StaticFields, ___U3CU3Ef__switchU24map11_0)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map11_0() const { return ___U3CU3Ef__switchU24map11_0; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map11_0() { return &___U3CU3Ef__switchU24map11_0; }
	inline void set_U3CU3Ef__switchU24map11_0(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map11_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map11_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
