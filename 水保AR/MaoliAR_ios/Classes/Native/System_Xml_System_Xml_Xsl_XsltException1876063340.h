﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_SystemException176217640.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Xsl.XsltException
struct  XsltException_t1876063340  : public SystemException_t176217640
{
public:
	// System.Int32 System.Xml.Xsl.XsltException::lineNumber
	int32_t ___lineNumber_11;
	// System.Int32 System.Xml.Xsl.XsltException::linePosition
	int32_t ___linePosition_12;
	// System.String System.Xml.Xsl.XsltException::sourceUri
	String_t* ___sourceUri_13;
	// System.String System.Xml.Xsl.XsltException::templateFrames
	String_t* ___templateFrames_14;

public:
	inline static int32_t get_offset_of_lineNumber_11() { return static_cast<int32_t>(offsetof(XsltException_t1876063340, ___lineNumber_11)); }
	inline int32_t get_lineNumber_11() const { return ___lineNumber_11; }
	inline int32_t* get_address_of_lineNumber_11() { return &___lineNumber_11; }
	inline void set_lineNumber_11(int32_t value)
	{
		___lineNumber_11 = value;
	}

	inline static int32_t get_offset_of_linePosition_12() { return static_cast<int32_t>(offsetof(XsltException_t1876063340, ___linePosition_12)); }
	inline int32_t get_linePosition_12() const { return ___linePosition_12; }
	inline int32_t* get_address_of_linePosition_12() { return &___linePosition_12; }
	inline void set_linePosition_12(int32_t value)
	{
		___linePosition_12 = value;
	}

	inline static int32_t get_offset_of_sourceUri_13() { return static_cast<int32_t>(offsetof(XsltException_t1876063340, ___sourceUri_13)); }
	inline String_t* get_sourceUri_13() const { return ___sourceUri_13; }
	inline String_t** get_address_of_sourceUri_13() { return &___sourceUri_13; }
	inline void set_sourceUri_13(String_t* value)
	{
		___sourceUri_13 = value;
		Il2CppCodeGenWriteBarrier(&___sourceUri_13, value);
	}

	inline static int32_t get_offset_of_templateFrames_14() { return static_cast<int32_t>(offsetof(XsltException_t1876063340, ___templateFrames_14)); }
	inline String_t* get_templateFrames_14() const { return ___templateFrames_14; }
	inline String_t** get_address_of_templateFrames_14() { return &___templateFrames_14; }
	inline void set_templateFrames_14(String_t* value)
	{
		___templateFrames_14 = value;
		Il2CppCodeGenWriteBarrier(&___templateFrames_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
