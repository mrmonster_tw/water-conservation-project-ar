﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Bi859993683.h"

// System.Xml.XmlDictionaryReaderQuotas
struct XmlDictionaryReaderQuotas_t173030297;
// System.ServiceModel.PeerResolvers.PeerResolverSettings
struct PeerResolverSettings_t1743278054;
// System.ServiceModel.PeerSecuritySettings
struct PeerSecuritySettings_t2125602112;
// System.ServiceModel.Channels.PeerTransportBindingElement
struct PeerTransportBindingElement_t261693216;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.NetPeerTcpBinding
struct  NetPeerTcpBinding_t3507636973  : public Binding_t859993683
{
public:
	// System.Xml.XmlDictionaryReaderQuotas System.ServiceModel.NetPeerTcpBinding::reader_quotas
	XmlDictionaryReaderQuotas_t173030297 * ___reader_quotas_6;
	// System.ServiceModel.PeerResolvers.PeerResolverSettings System.ServiceModel.NetPeerTcpBinding::resolver
	PeerResolverSettings_t1743278054 * ___resolver_7;
	// System.ServiceModel.PeerSecuritySettings System.ServiceModel.NetPeerTcpBinding::security
	PeerSecuritySettings_t2125602112 * ___security_8;
	// System.ServiceModel.Channels.PeerTransportBindingElement System.ServiceModel.NetPeerTcpBinding::transport
	PeerTransportBindingElement_t261693216 * ___transport_9;

public:
	inline static int32_t get_offset_of_reader_quotas_6() { return static_cast<int32_t>(offsetof(NetPeerTcpBinding_t3507636973, ___reader_quotas_6)); }
	inline XmlDictionaryReaderQuotas_t173030297 * get_reader_quotas_6() const { return ___reader_quotas_6; }
	inline XmlDictionaryReaderQuotas_t173030297 ** get_address_of_reader_quotas_6() { return &___reader_quotas_6; }
	inline void set_reader_quotas_6(XmlDictionaryReaderQuotas_t173030297 * value)
	{
		___reader_quotas_6 = value;
		Il2CppCodeGenWriteBarrier(&___reader_quotas_6, value);
	}

	inline static int32_t get_offset_of_resolver_7() { return static_cast<int32_t>(offsetof(NetPeerTcpBinding_t3507636973, ___resolver_7)); }
	inline PeerResolverSettings_t1743278054 * get_resolver_7() const { return ___resolver_7; }
	inline PeerResolverSettings_t1743278054 ** get_address_of_resolver_7() { return &___resolver_7; }
	inline void set_resolver_7(PeerResolverSettings_t1743278054 * value)
	{
		___resolver_7 = value;
		Il2CppCodeGenWriteBarrier(&___resolver_7, value);
	}

	inline static int32_t get_offset_of_security_8() { return static_cast<int32_t>(offsetof(NetPeerTcpBinding_t3507636973, ___security_8)); }
	inline PeerSecuritySettings_t2125602112 * get_security_8() const { return ___security_8; }
	inline PeerSecuritySettings_t2125602112 ** get_address_of_security_8() { return &___security_8; }
	inline void set_security_8(PeerSecuritySettings_t2125602112 * value)
	{
		___security_8 = value;
		Il2CppCodeGenWriteBarrier(&___security_8, value);
	}

	inline static int32_t get_offset_of_transport_9() { return static_cast<int32_t>(offsetof(NetPeerTcpBinding_t3507636973, ___transport_9)); }
	inline PeerTransportBindingElement_t261693216 * get_transport_9() const { return ___transport_9; }
	inline PeerTransportBindingElement_t261693216 ** get_address_of_transport_9() { return &___transport_9; }
	inline void set_transport_9(PeerTransportBindingElement_t261693216 * value)
	{
		___transport_9 = value;
		Il2CppCodeGenWriteBarrier(&___transport_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
