﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.ServiceModel.Channels.AddressingVersion
struct AddressingVersion_t2257583243;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.AddressingVersion
struct  AddressingVersion_t2257583243  : public Il2CppObject
{
public:
	// System.String System.ServiceModel.Channels.AddressingVersion::name
	String_t* ___name_0;
	// System.String System.ServiceModel.Channels.AddressingVersion::address
	String_t* ___address_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(AddressingVersion_t2257583243, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_address_1() { return static_cast<int32_t>(offsetof(AddressingVersion_t2257583243, ___address_1)); }
	inline String_t* get_address_1() const { return ___address_1; }
	inline String_t** get_address_of_address_1() { return &___address_1; }
	inline void set_address_1(String_t* value)
	{
		___address_1 = value;
		Il2CppCodeGenWriteBarrier(&___address_1, value);
	}
};

struct AddressingVersion_t2257583243_StaticFields
{
public:
	// System.ServiceModel.Channels.AddressingVersion System.ServiceModel.Channels.AddressingVersion::addressing200408
	AddressingVersion_t2257583243 * ___addressing200408_2;
	// System.ServiceModel.Channels.AddressingVersion System.ServiceModel.Channels.AddressingVersion::addressing1_0
	AddressingVersion_t2257583243 * ___addressing1_0_3;
	// System.ServiceModel.Channels.AddressingVersion System.ServiceModel.Channels.AddressingVersion::none
	AddressingVersion_t2257583243 * ___none_4;

public:
	inline static int32_t get_offset_of_addressing200408_2() { return static_cast<int32_t>(offsetof(AddressingVersion_t2257583243_StaticFields, ___addressing200408_2)); }
	inline AddressingVersion_t2257583243 * get_addressing200408_2() const { return ___addressing200408_2; }
	inline AddressingVersion_t2257583243 ** get_address_of_addressing200408_2() { return &___addressing200408_2; }
	inline void set_addressing200408_2(AddressingVersion_t2257583243 * value)
	{
		___addressing200408_2 = value;
		Il2CppCodeGenWriteBarrier(&___addressing200408_2, value);
	}

	inline static int32_t get_offset_of_addressing1_0_3() { return static_cast<int32_t>(offsetof(AddressingVersion_t2257583243_StaticFields, ___addressing1_0_3)); }
	inline AddressingVersion_t2257583243 * get_addressing1_0_3() const { return ___addressing1_0_3; }
	inline AddressingVersion_t2257583243 ** get_address_of_addressing1_0_3() { return &___addressing1_0_3; }
	inline void set_addressing1_0_3(AddressingVersion_t2257583243 * value)
	{
		___addressing1_0_3 = value;
		Il2CppCodeGenWriteBarrier(&___addressing1_0_3, value);
	}

	inline static int32_t get_offset_of_none_4() { return static_cast<int32_t>(offsetof(AddressingVersion_t2257583243_StaticFields, ___none_4)); }
	inline AddressingVersion_t2257583243 * get_none_4() const { return ___none_4; }
	inline AddressingVersion_t2257583243 ** get_address_of_none_4() { return &___none_4; }
	inline void set_none_4(AddressingVersion_t2257583243 * value)
	{
		___none_4 = value;
		Il2CppCodeGenWriteBarrier(&___none_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
