﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_TimeSpan881159249.h"

// System.ServiceModel.Security.IdentityVerifier
struct IdentityVerifier_t614541315;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.LocalClientSecuritySettings
struct  LocalClientSecuritySettings_t2865192915  : public Il2CppObject
{
public:
	// System.Boolean System.ServiceModel.Channels.LocalClientSecuritySettings::cache_cookies
	bool ___cache_cookies_0;
	// System.Int32 System.ServiceModel.Channels.LocalClientSecuritySettings::cookie_renewal
	int32_t ___cookie_renewal_1;
	// System.Boolean System.ServiceModel.Channels.LocalClientSecuritySettings::detect_replays
	bool ___detect_replays_2;
	// System.ServiceModel.Security.IdentityVerifier System.ServiceModel.Channels.LocalClientSecuritySettings::verifier
	IdentityVerifier_t614541315 * ___verifier_3;
	// System.TimeSpan System.ServiceModel.Channels.LocalClientSecuritySettings::max_cookie_cache_time
	TimeSpan_t881159249  ___max_cookie_cache_time_4;
	// System.Boolean System.ServiceModel.Channels.LocalClientSecuritySettings::reconnect
	bool ___reconnect_5;
	// System.Int32 System.ServiceModel.Channels.LocalClientSecuritySettings::replay_cache_size
	int32_t ___replay_cache_size_6;
	// System.TimeSpan System.ServiceModel.Channels.LocalClientSecuritySettings::renewal_interval
	TimeSpan_t881159249  ___renewal_interval_7;
	// System.TimeSpan System.ServiceModel.Channels.LocalClientSecuritySettings::rollover_interval
	TimeSpan_t881159249  ___rollover_interval_8;
	// System.TimeSpan System.ServiceModel.Channels.LocalClientSecuritySettings::<MaxClockSkew>k__BackingField
	TimeSpan_t881159249  ___U3CMaxClockSkewU3Ek__BackingField_9;
	// System.TimeSpan System.ServiceModel.Channels.LocalClientSecuritySettings::<ReplayWindow>k__BackingField
	TimeSpan_t881159249  ___U3CReplayWindowU3Ek__BackingField_10;
	// System.TimeSpan System.ServiceModel.Channels.LocalClientSecuritySettings::<TimestampValidityDuration>k__BackingField
	TimeSpan_t881159249  ___U3CTimestampValidityDurationU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_cache_cookies_0() { return static_cast<int32_t>(offsetof(LocalClientSecuritySettings_t2865192915, ___cache_cookies_0)); }
	inline bool get_cache_cookies_0() const { return ___cache_cookies_0; }
	inline bool* get_address_of_cache_cookies_0() { return &___cache_cookies_0; }
	inline void set_cache_cookies_0(bool value)
	{
		___cache_cookies_0 = value;
	}

	inline static int32_t get_offset_of_cookie_renewal_1() { return static_cast<int32_t>(offsetof(LocalClientSecuritySettings_t2865192915, ___cookie_renewal_1)); }
	inline int32_t get_cookie_renewal_1() const { return ___cookie_renewal_1; }
	inline int32_t* get_address_of_cookie_renewal_1() { return &___cookie_renewal_1; }
	inline void set_cookie_renewal_1(int32_t value)
	{
		___cookie_renewal_1 = value;
	}

	inline static int32_t get_offset_of_detect_replays_2() { return static_cast<int32_t>(offsetof(LocalClientSecuritySettings_t2865192915, ___detect_replays_2)); }
	inline bool get_detect_replays_2() const { return ___detect_replays_2; }
	inline bool* get_address_of_detect_replays_2() { return &___detect_replays_2; }
	inline void set_detect_replays_2(bool value)
	{
		___detect_replays_2 = value;
	}

	inline static int32_t get_offset_of_verifier_3() { return static_cast<int32_t>(offsetof(LocalClientSecuritySettings_t2865192915, ___verifier_3)); }
	inline IdentityVerifier_t614541315 * get_verifier_3() const { return ___verifier_3; }
	inline IdentityVerifier_t614541315 ** get_address_of_verifier_3() { return &___verifier_3; }
	inline void set_verifier_3(IdentityVerifier_t614541315 * value)
	{
		___verifier_3 = value;
		Il2CppCodeGenWriteBarrier(&___verifier_3, value);
	}

	inline static int32_t get_offset_of_max_cookie_cache_time_4() { return static_cast<int32_t>(offsetof(LocalClientSecuritySettings_t2865192915, ___max_cookie_cache_time_4)); }
	inline TimeSpan_t881159249  get_max_cookie_cache_time_4() const { return ___max_cookie_cache_time_4; }
	inline TimeSpan_t881159249 * get_address_of_max_cookie_cache_time_4() { return &___max_cookie_cache_time_4; }
	inline void set_max_cookie_cache_time_4(TimeSpan_t881159249  value)
	{
		___max_cookie_cache_time_4 = value;
	}

	inline static int32_t get_offset_of_reconnect_5() { return static_cast<int32_t>(offsetof(LocalClientSecuritySettings_t2865192915, ___reconnect_5)); }
	inline bool get_reconnect_5() const { return ___reconnect_5; }
	inline bool* get_address_of_reconnect_5() { return &___reconnect_5; }
	inline void set_reconnect_5(bool value)
	{
		___reconnect_5 = value;
	}

	inline static int32_t get_offset_of_replay_cache_size_6() { return static_cast<int32_t>(offsetof(LocalClientSecuritySettings_t2865192915, ___replay_cache_size_6)); }
	inline int32_t get_replay_cache_size_6() const { return ___replay_cache_size_6; }
	inline int32_t* get_address_of_replay_cache_size_6() { return &___replay_cache_size_6; }
	inline void set_replay_cache_size_6(int32_t value)
	{
		___replay_cache_size_6 = value;
	}

	inline static int32_t get_offset_of_renewal_interval_7() { return static_cast<int32_t>(offsetof(LocalClientSecuritySettings_t2865192915, ___renewal_interval_7)); }
	inline TimeSpan_t881159249  get_renewal_interval_7() const { return ___renewal_interval_7; }
	inline TimeSpan_t881159249 * get_address_of_renewal_interval_7() { return &___renewal_interval_7; }
	inline void set_renewal_interval_7(TimeSpan_t881159249  value)
	{
		___renewal_interval_7 = value;
	}

	inline static int32_t get_offset_of_rollover_interval_8() { return static_cast<int32_t>(offsetof(LocalClientSecuritySettings_t2865192915, ___rollover_interval_8)); }
	inline TimeSpan_t881159249  get_rollover_interval_8() const { return ___rollover_interval_8; }
	inline TimeSpan_t881159249 * get_address_of_rollover_interval_8() { return &___rollover_interval_8; }
	inline void set_rollover_interval_8(TimeSpan_t881159249  value)
	{
		___rollover_interval_8 = value;
	}

	inline static int32_t get_offset_of_U3CMaxClockSkewU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(LocalClientSecuritySettings_t2865192915, ___U3CMaxClockSkewU3Ek__BackingField_9)); }
	inline TimeSpan_t881159249  get_U3CMaxClockSkewU3Ek__BackingField_9() const { return ___U3CMaxClockSkewU3Ek__BackingField_9; }
	inline TimeSpan_t881159249 * get_address_of_U3CMaxClockSkewU3Ek__BackingField_9() { return &___U3CMaxClockSkewU3Ek__BackingField_9; }
	inline void set_U3CMaxClockSkewU3Ek__BackingField_9(TimeSpan_t881159249  value)
	{
		___U3CMaxClockSkewU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CReplayWindowU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(LocalClientSecuritySettings_t2865192915, ___U3CReplayWindowU3Ek__BackingField_10)); }
	inline TimeSpan_t881159249  get_U3CReplayWindowU3Ek__BackingField_10() const { return ___U3CReplayWindowU3Ek__BackingField_10; }
	inline TimeSpan_t881159249 * get_address_of_U3CReplayWindowU3Ek__BackingField_10() { return &___U3CReplayWindowU3Ek__BackingField_10; }
	inline void set_U3CReplayWindowU3Ek__BackingField_10(TimeSpan_t881159249  value)
	{
		___U3CReplayWindowU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CTimestampValidityDurationU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(LocalClientSecuritySettings_t2865192915, ___U3CTimestampValidityDurationU3Ek__BackingField_11)); }
	inline TimeSpan_t881159249  get_U3CTimestampValidityDurationU3Ek__BackingField_11() const { return ___U3CTimestampValidityDurationU3Ek__BackingField_11; }
	inline TimeSpan_t881159249 * get_address_of_U3CTimestampValidityDurationU3Ek__BackingField_11() { return &___U3CTimestampValidityDurationU3Ek__BackingField_11; }
	inline void set_U3CTimestampValidityDurationU3Ek__BackingField_11(TimeSpan_t881159249  value)
	{
		___U3CTimestampValidityDurationU3Ek__BackingField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
