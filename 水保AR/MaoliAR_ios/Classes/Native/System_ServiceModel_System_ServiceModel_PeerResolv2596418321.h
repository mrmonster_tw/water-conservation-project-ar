﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_Guid3193532887.h"

// System.String
struct String_t;
// System.ServiceModel.PeerNodeAddress
struct PeerNodeAddress_t2098027372;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.PeerResolvers.RegisterInfoDC
struct  RegisterInfoDC_t2596418321  : public Il2CppObject
{
public:
	// System.Guid System.ServiceModel.PeerResolvers.RegisterInfoDC::client_id
	Guid_t  ___client_id_0;
	// System.String System.ServiceModel.PeerResolvers.RegisterInfoDC::mesh_id
	String_t* ___mesh_id_1;
	// System.ServiceModel.PeerNodeAddress System.ServiceModel.PeerResolvers.RegisterInfoDC::node_address
	PeerNodeAddress_t2098027372 * ___node_address_2;

public:
	inline static int32_t get_offset_of_client_id_0() { return static_cast<int32_t>(offsetof(RegisterInfoDC_t2596418321, ___client_id_0)); }
	inline Guid_t  get_client_id_0() const { return ___client_id_0; }
	inline Guid_t * get_address_of_client_id_0() { return &___client_id_0; }
	inline void set_client_id_0(Guid_t  value)
	{
		___client_id_0 = value;
	}

	inline static int32_t get_offset_of_mesh_id_1() { return static_cast<int32_t>(offsetof(RegisterInfoDC_t2596418321, ___mesh_id_1)); }
	inline String_t* get_mesh_id_1() const { return ___mesh_id_1; }
	inline String_t** get_address_of_mesh_id_1() { return &___mesh_id_1; }
	inline void set_mesh_id_1(String_t* value)
	{
		___mesh_id_1 = value;
		Il2CppCodeGenWriteBarrier(&___mesh_id_1, value);
	}

	inline static int32_t get_offset_of_node_address_2() { return static_cast<int32_t>(offsetof(RegisterInfoDC_t2596418321, ___node_address_2)); }
	inline PeerNodeAddress_t2098027372 * get_node_address_2() const { return ___node_address_2; }
	inline PeerNodeAddress_t2098027372 ** get_address_of_node_address_2() { return &___node_address_2; }
	inline void set_node_address_2(PeerNodeAddress_t2098027372 * value)
	{
		___node_address_2 = value;
		Il2CppCodeGenWriteBarrier(&___node_address_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
