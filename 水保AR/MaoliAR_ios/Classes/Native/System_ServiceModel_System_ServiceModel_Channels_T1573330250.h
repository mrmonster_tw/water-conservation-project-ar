﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_TimeSpan881159249.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.TcpConnectionPoolSettings
struct  TcpConnectionPoolSettings_t1573330250  : public Il2CppObject
{
public:
	// System.String System.ServiceModel.Channels.TcpConnectionPoolSettings::group_name
	String_t* ___group_name_0;
	// System.TimeSpan System.ServiceModel.Channels.TcpConnectionPoolSettings::idle_timeout
	TimeSpan_t881159249  ___idle_timeout_1;
	// System.TimeSpan System.ServiceModel.Channels.TcpConnectionPoolSettings::lease_timeout
	TimeSpan_t881159249  ___lease_timeout_2;
	// System.Int32 System.ServiceModel.Channels.TcpConnectionPoolSettings::max_conn
	int32_t ___max_conn_3;

public:
	inline static int32_t get_offset_of_group_name_0() { return static_cast<int32_t>(offsetof(TcpConnectionPoolSettings_t1573330250, ___group_name_0)); }
	inline String_t* get_group_name_0() const { return ___group_name_0; }
	inline String_t** get_address_of_group_name_0() { return &___group_name_0; }
	inline void set_group_name_0(String_t* value)
	{
		___group_name_0 = value;
		Il2CppCodeGenWriteBarrier(&___group_name_0, value);
	}

	inline static int32_t get_offset_of_idle_timeout_1() { return static_cast<int32_t>(offsetof(TcpConnectionPoolSettings_t1573330250, ___idle_timeout_1)); }
	inline TimeSpan_t881159249  get_idle_timeout_1() const { return ___idle_timeout_1; }
	inline TimeSpan_t881159249 * get_address_of_idle_timeout_1() { return &___idle_timeout_1; }
	inline void set_idle_timeout_1(TimeSpan_t881159249  value)
	{
		___idle_timeout_1 = value;
	}

	inline static int32_t get_offset_of_lease_timeout_2() { return static_cast<int32_t>(offsetof(TcpConnectionPoolSettings_t1573330250, ___lease_timeout_2)); }
	inline TimeSpan_t881159249  get_lease_timeout_2() const { return ___lease_timeout_2; }
	inline TimeSpan_t881159249 * get_address_of_lease_timeout_2() { return &___lease_timeout_2; }
	inline void set_lease_timeout_2(TimeSpan_t881159249  value)
	{
		___lease_timeout_2 = value;
	}

	inline static int32_t get_offset_of_max_conn_3() { return static_cast<int32_t>(offsetof(TcpConnectionPoolSettings_t1573330250, ___max_conn_3)); }
	inline int32_t get_max_conn_3() const { return ___max_conn_3; }
	inline int32_t* get_address_of_max_conn_3() { return &___max_conn_3; }
	inline void set_max_conn_3(int32_t value)
	{
		___max_conn_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
