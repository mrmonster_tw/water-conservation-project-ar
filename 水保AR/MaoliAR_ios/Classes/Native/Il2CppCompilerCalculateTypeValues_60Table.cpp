﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UICamera1356438871.h"
#include "AssemblyU2DCSharp_UICamera_ControlScheme4038670825.h"
#include "AssemblyU2DCSharp_UICamera_ClickNotification3778629126.h"
#include "AssemblyU2DCSharp_UICamera_MouseOrTouch3052596533.h"
#include "AssemblyU2DCSharp_UICamera_EventType561105572.h"
#include "AssemblyU2DCSharp_UICamera_GetKeyStateFunc2810275146.h"
#include "AssemblyU2DCSharp_UICamera_GetAxisFunc2592608932.h"
#include "AssemblyU2DCSharp_UICamera_GetAnyKeyFunc1761480072.h"
#include "AssemblyU2DCSharp_UICamera_OnScreenResize2279991692.h"
#include "AssemblyU2DCSharp_UICamera_OnCustomInput3508588789.h"
#include "AssemblyU2DCSharp_UICamera_OnSchemeChange1701155603.h"
#include "AssemblyU2DCSharp_UICamera_MoveDelegate16019400.h"
#include "AssemblyU2DCSharp_UICamera_VoidDelegate3100799918.h"
#include "AssemblyU2DCSharp_UICamera_BoolDelegate3825226153.h"
#include "AssemblyU2DCSharp_UICamera_FloatDelegate906524069.h"
#include "AssemblyU2DCSharp_UICamera_VectorDelegate435795517.h"
#include "AssemblyU2DCSharp_UICamera_ObjectDelegate2041570719.h"
#include "AssemblyU2DCSharp_UICamera_KeyCodeDelegate3064672302.h"
#include "AssemblyU2DCSharp_UICamera_DepthEntry628749918.h"
#include "AssemblyU2DCSharp_UICamera_Touch3749196807.h"
#include "AssemblyU2DCSharp_UICamera_GetTouchCountCallback3185863032.h"
#include "AssemblyU2DCSharp_UICamera_GetTouchCallback97678626.h"
#include "AssemblyU2DCSharp_UIColorPicker463494602.h"
#include "AssemblyU2DCSharp_UIFont2766063701.h"
#include "AssemblyU2DCSharp_UIInput421821618.h"
#include "AssemblyU2DCSharp_UIInput_InputType3084801673.h"
#include "AssemblyU2DCSharp_UIInput_Validation3618213463.h"
#include "AssemblyU2DCSharp_UIInput_KeyboardType3374647619.h"
#include "AssemblyU2DCSharp_UIInput_OnReturnKey3803455137.h"
#include "AssemblyU2DCSharp_UIInput_OnValidate1246632601.h"
#include "AssemblyU2DCSharp_UIInputOnGUI4239979770.h"
#include "AssemblyU2DCSharp_UILabel3248798549.h"
#include "AssemblyU2DCSharp_UILabel_Effect2533209744.h"
#include "AssemblyU2DCSharp_UILabel_Overflow884389639.h"
#include "AssemblyU2DCSharp_UILabel_Crispness2029465680.h"
#include "AssemblyU2DCSharp_UILocalize3543745742.h"
#include "AssemblyU2DCSharp_UIOrthoCamera1944225589.h"
#include "AssemblyU2DCSharp_UIPanel1716472341.h"
#include "AssemblyU2DCSharp_UIPanel_RenderQueue2721716586.h"
#include "AssemblyU2DCSharp_UIPanel_OnGeometryUpdated2462438111.h"
#include "AssemblyU2DCSharp_UIPanel_OnClippingMoved476625095.h"
#include "AssemblyU2DCSharp_UIRoot4022971450.h"
#include "AssemblyU2DCSharp_UIRoot_Scaling3068739704.h"
#include "AssemblyU2DCSharp_UIRoot_Constraint400694485.h"
#include "AssemblyU2DCSharp_UISprite194114938.h"
#include "AssemblyU2DCSharp_UISpriteAnimation1118314077.h"
#include "AssemblyU2DCSharp_UISpriteData900308526.h"
#include "AssemblyU2DCSharp_UIStretch3058335968.h"
#include "AssemblyU2DCSharp_UIStretch_Style3184300279.h"
#include "AssemblyU2DCSharp_UITextList2040849008.h"
#include "AssemblyU2DCSharp_UITextList_Style3106380496.h"
#include "AssemblyU2DCSharp_UITextList_Paragraph1111063719.h"
#include "AssemblyU2DCSharp_UITexture3471168817.h"
#include "AssemblyU2DCSharp_UITooltip30236576.h"
#include "AssemblyU2DCSharp_UIViewport1918760134.h"
#include "AssemblyU2DCSharp_OnEnableCallEvent3645046440.h"
#include "AssemblyU2DCSharp_TextCheck3911857046.h"
#include "AssemblyU2DCSharp_TextCheck_U3CwaitU3Ec__Iterator01024662273.h"
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviour3333547397.h"
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviour431762792.h"
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour822809409.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErro922997482.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEvent2395349464.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHandl95800019.h"
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandler2139335054.h"
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviour2892437830.h"
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour2200418350.h"
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayer2737599080.h"
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterB3931359467.h"
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayer2555589894.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponen4153723608.h"
#include "AssemblyU2DCSharp_Vuforia_WSAUnityPlayer3135728299.h"
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviour2745617306.h"
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviour2061511750.h"
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviour728125005.h"
#include "AssemblyU2DCSharp_Vuforia_PropBehaviour2792829701.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviour3655135626.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTargetB160667116.h"
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviour3650770673.h"
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviour87475147.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviour65964226.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviour2771985595.h"
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildin4262637471.h"
#include "AssemblyU2DCSharp_VRIntegrationHelper3162465173.h"
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviour1552899074.h"
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviour1436326451.h"
#include "AssemblyU2DCSharp_Vuforia_VuMarkBehaviour1178230459.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviour2151848540.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaConfiguration1763229349.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaRuntimeInitializat714775116.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour1831066704.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventH2143753312.h"
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour209462683.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Cameras_Abst1919708159.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Cameras_Abst2449601881.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Cameras_AutoC137911967.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Cameras_Free2000732766.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Cameras_HandH450595784.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Cameras_Look3260877718.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Cameras_Pivo3786953582.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6000 = { sizeof (UICamera_t1356438871), -1, sizeof(UICamera_t1356438871_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6000[95] = 
{
	UICamera_t1356438871_StaticFields::get_offset_of_list_2(),
	UICamera_t1356438871_StaticFields::get_offset_of_GetKeyDown_3(),
	UICamera_t1356438871_StaticFields::get_offset_of_GetKeyUp_4(),
	UICamera_t1356438871_StaticFields::get_offset_of_GetKey_5(),
	UICamera_t1356438871_StaticFields::get_offset_of_GetAxis_6(),
	UICamera_t1356438871_StaticFields::get_offset_of_GetAnyKeyDown_7(),
	UICamera_t1356438871_StaticFields::get_offset_of_onScreenResize_8(),
	UICamera_t1356438871::get_offset_of_eventType_9(),
	UICamera_t1356438871::get_offset_of_eventsGoToColliders_10(),
	UICamera_t1356438871::get_offset_of_eventReceiverMask_11(),
	UICamera_t1356438871::get_offset_of_debug_12(),
	UICamera_t1356438871::get_offset_of_useMouse_13(),
	UICamera_t1356438871::get_offset_of_useTouch_14(),
	UICamera_t1356438871::get_offset_of_allowMultiTouch_15(),
	UICamera_t1356438871::get_offset_of_useKeyboard_16(),
	UICamera_t1356438871::get_offset_of_useController_17(),
	UICamera_t1356438871::get_offset_of_stickyTooltip_18(),
	UICamera_t1356438871::get_offset_of_tooltipDelay_19(),
	UICamera_t1356438871::get_offset_of_longPressTooltip_20(),
	UICamera_t1356438871::get_offset_of_mouseDragThreshold_21(),
	UICamera_t1356438871::get_offset_of_mouseClickThreshold_22(),
	UICamera_t1356438871::get_offset_of_touchDragThreshold_23(),
	UICamera_t1356438871::get_offset_of_touchClickThreshold_24(),
	UICamera_t1356438871::get_offset_of_rangeDistance_25(),
	UICamera_t1356438871::get_offset_of_horizontalAxisName_26(),
	UICamera_t1356438871::get_offset_of_verticalAxisName_27(),
	UICamera_t1356438871::get_offset_of_horizontalPanAxisName_28(),
	UICamera_t1356438871::get_offset_of_verticalPanAxisName_29(),
	UICamera_t1356438871::get_offset_of_scrollAxisName_30(),
	UICamera_t1356438871::get_offset_of_commandClick_31(),
	UICamera_t1356438871::get_offset_of_submitKey0_32(),
	UICamera_t1356438871::get_offset_of_submitKey1_33(),
	UICamera_t1356438871::get_offset_of_cancelKey0_34(),
	UICamera_t1356438871::get_offset_of_cancelKey1_35(),
	UICamera_t1356438871_StaticFields::get_offset_of_onCustomInput_36(),
	UICamera_t1356438871_StaticFields::get_offset_of_showTooltips_37(),
	UICamera_t1356438871_StaticFields::get_offset_of_mDisableController_38(),
	UICamera_t1356438871_StaticFields::get_offset_of_mLastPos_39(),
	UICamera_t1356438871_StaticFields::get_offset_of_lastWorldPosition_40(),
	UICamera_t1356438871_StaticFields::get_offset_of_lastHit_41(),
	UICamera_t1356438871_StaticFields::get_offset_of_current_42(),
	UICamera_t1356438871_StaticFields::get_offset_of_currentCamera_43(),
	UICamera_t1356438871_StaticFields::get_offset_of_onSchemeChange_44(),
	UICamera_t1356438871_StaticFields::get_offset_of_currentTouchID_45(),
	UICamera_t1356438871_StaticFields::get_offset_of_mCurrentKey_46(),
	UICamera_t1356438871_StaticFields::get_offset_of_currentTouch_47(),
	UICamera_t1356438871_StaticFields::get_offset_of_mInputFocus_48(),
	UICamera_t1356438871_StaticFields::get_offset_of_mGenericHandler_49(),
	UICamera_t1356438871_StaticFields::get_offset_of_fallThrough_50(),
	UICamera_t1356438871_StaticFields::get_offset_of_onClick_51(),
	UICamera_t1356438871_StaticFields::get_offset_of_onDoubleClick_52(),
	UICamera_t1356438871_StaticFields::get_offset_of_onHover_53(),
	UICamera_t1356438871_StaticFields::get_offset_of_onPress_54(),
	UICamera_t1356438871_StaticFields::get_offset_of_onSelect_55(),
	UICamera_t1356438871_StaticFields::get_offset_of_onScroll_56(),
	UICamera_t1356438871_StaticFields::get_offset_of_onDrag_57(),
	UICamera_t1356438871_StaticFields::get_offset_of_onDragStart_58(),
	UICamera_t1356438871_StaticFields::get_offset_of_onDragOver_59(),
	UICamera_t1356438871_StaticFields::get_offset_of_onDragOut_60(),
	UICamera_t1356438871_StaticFields::get_offset_of_onDragEnd_61(),
	UICamera_t1356438871_StaticFields::get_offset_of_onDrop_62(),
	UICamera_t1356438871_StaticFields::get_offset_of_onKey_63(),
	UICamera_t1356438871_StaticFields::get_offset_of_onNavigate_64(),
	UICamera_t1356438871_StaticFields::get_offset_of_onPan_65(),
	UICamera_t1356438871_StaticFields::get_offset_of_onTooltip_66(),
	UICamera_t1356438871_StaticFields::get_offset_of_onMouseMove_67(),
	UICamera_t1356438871_StaticFields::get_offset_of_mMouse_68(),
	UICamera_t1356438871_StaticFields::get_offset_of_controller_69(),
	UICamera_t1356438871_StaticFields::get_offset_of_activeTouches_70(),
	UICamera_t1356438871_StaticFields::get_offset_of_mTouchIDs_71(),
	UICamera_t1356438871_StaticFields::get_offset_of_mWidth_72(),
	UICamera_t1356438871_StaticFields::get_offset_of_mHeight_73(),
	UICamera_t1356438871_StaticFields::get_offset_of_mTooltip_74(),
	UICamera_t1356438871::get_offset_of_mCam_75(),
	UICamera_t1356438871_StaticFields::get_offset_of_mTooltipTime_76(),
	UICamera_t1356438871::get_offset_of_mNextRaycast_77(),
	UICamera_t1356438871_StaticFields::get_offset_of_isDragging_78(),
	UICamera_t1356438871_StaticFields::get_offset_of_mRayHitObject_79(),
	UICamera_t1356438871_StaticFields::get_offset_of_mHover_80(),
	UICamera_t1356438871_StaticFields::get_offset_of_mSelected_81(),
	UICamera_t1356438871_StaticFields::get_offset_of_mHit_82(),
	UICamera_t1356438871_StaticFields::get_offset_of_mHits_83(),
	UICamera_t1356438871_StaticFields::get_offset_of_m2DPlane_84(),
	UICamera_t1356438871_StaticFields::get_offset_of_mNextEvent_85(),
	UICamera_t1356438871_StaticFields::get_offset_of_mNotifying_86(),
	UICamera_t1356438871_StaticFields::get_offset_of_mUsingTouchEvents_87(),
	UICamera_t1356438871_StaticFields::get_offset_of_GetInputTouchCount_88(),
	UICamera_t1356438871_StaticFields::get_offset_of_GetInputTouch_89(),
	UICamera_t1356438871_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_90(),
	UICamera_t1356438871_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_91(),
	UICamera_t1356438871_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_92(),
	UICamera_t1356438871_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_93(),
	UICamera_t1356438871_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_94(),
	UICamera_t1356438871_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_95(),
	UICamera_t1356438871_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_96(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6001 = { sizeof (ControlScheme_t4038670825)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6001[4] = 
{
	ControlScheme_t4038670825::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6002 = { sizeof (ClickNotification_t3778629126)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6002[4] = 
{
	ClickNotification_t3778629126::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6003 = { sizeof (MouseOrTouch_t3052596533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6003[17] = 
{
	MouseOrTouch_t3052596533::get_offset_of_key_0(),
	MouseOrTouch_t3052596533::get_offset_of_pos_1(),
	MouseOrTouch_t3052596533::get_offset_of_lastPos_2(),
	MouseOrTouch_t3052596533::get_offset_of_delta_3(),
	MouseOrTouch_t3052596533::get_offset_of_totalDelta_4(),
	MouseOrTouch_t3052596533::get_offset_of_pressedCam_5(),
	MouseOrTouch_t3052596533::get_offset_of_last_6(),
	MouseOrTouch_t3052596533::get_offset_of_current_7(),
	MouseOrTouch_t3052596533::get_offset_of_pressed_8(),
	MouseOrTouch_t3052596533::get_offset_of_dragged_9(),
	MouseOrTouch_t3052596533::get_offset_of_pressTime_10(),
	MouseOrTouch_t3052596533::get_offset_of_clickTime_11(),
	MouseOrTouch_t3052596533::get_offset_of_clickNotification_12(),
	MouseOrTouch_t3052596533::get_offset_of_touchBegan_13(),
	MouseOrTouch_t3052596533::get_offset_of_pressStarted_14(),
	MouseOrTouch_t3052596533::get_offset_of_dragStarted_15(),
	MouseOrTouch_t3052596533::get_offset_of_ignoreDelta_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6004 = { sizeof (EventType_t561105572)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6004[5] = 
{
	EventType_t561105572::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6005 = { sizeof (GetKeyStateFunc_t2810275146), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6006 = { sizeof (GetAxisFunc_t2592608932), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6007 = { sizeof (GetAnyKeyFunc_t1761480072), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6008 = { sizeof (OnScreenResize_t2279991692), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6009 = { sizeof (OnCustomInput_t3508588789), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6010 = { sizeof (OnSchemeChange_t1701155603), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6011 = { sizeof (MoveDelegate_t16019400), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6012 = { sizeof (VoidDelegate_t3100799918), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6013 = { sizeof (BoolDelegate_t3825226153), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6014 = { sizeof (FloatDelegate_t906524069), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6015 = { sizeof (VectorDelegate_t435795517), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6016 = { sizeof (ObjectDelegate_t2041570719), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6017 = { sizeof (KeyCodeDelegate_t3064672302), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6018 = { sizeof (DepthEntry_t628749918)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6018[4] = 
{
	DepthEntry_t628749918::get_offset_of_depth_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DepthEntry_t628749918::get_offset_of_hit_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DepthEntry_t628749918::get_offset_of_point_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DepthEntry_t628749918::get_offset_of_go_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6019 = { sizeof (Touch_t3749196807), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6019[4] = 
{
	Touch_t3749196807::get_offset_of_fingerId_0(),
	Touch_t3749196807::get_offset_of_phase_1(),
	Touch_t3749196807::get_offset_of_position_2(),
	Touch_t3749196807::get_offset_of_tapCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6020 = { sizeof (GetTouchCountCallback_t3185863032), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6021 = { sizeof (GetTouchCallback_t97678626), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6022 = { sizeof (UIColorPicker_t463494602), -1, sizeof(UIColorPicker_t463494602_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6022[14] = 
{
	UIColorPicker_t463494602_StaticFields::get_offset_of_current_2(),
	UIColorPicker_t463494602::get_offset_of_value_3(),
	UIColorPicker_t463494602::get_offset_of_selectionWidget_4(),
	UIColorPicker_t463494602::get_offset_of_onChange_5(),
	UIColorPicker_t463494602::get_offset_of_mTrans_6(),
	UIColorPicker_t463494602::get_offset_of_mUITex_7(),
	UIColorPicker_t463494602::get_offset_of_mTex_8(),
	UIColorPicker_t463494602::get_offset_of_mCam_9(),
	UIColorPicker_t463494602::get_offset_of_mPos_10(),
	UIColorPicker_t463494602::get_offset_of_mWidth_11(),
	UIColorPicker_t463494602::get_offset_of_mHeight_12(),
	UIColorPicker_t463494602_StaticFields::get_offset_of_mRed_13(),
	UIColorPicker_t463494602_StaticFields::get_offset_of_mGreen_14(),
	UIColorPicker_t463494602_StaticFields::get_offset_of_mBlue_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6023 = { sizeof (UIFont_t2766063701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6023[12] = 
{
	UIFont_t2766063701::get_offset_of_mMat_2(),
	UIFont_t2766063701::get_offset_of_mUVRect_3(),
	UIFont_t2766063701::get_offset_of_mFont_4(),
	UIFont_t2766063701::get_offset_of_mAtlas_5(),
	UIFont_t2766063701::get_offset_of_mReplacement_6(),
	UIFont_t2766063701::get_offset_of_mSymbols_7(),
	UIFont_t2766063701::get_offset_of_mDynamicFont_8(),
	UIFont_t2766063701::get_offset_of_mDynamicFontSize_9(),
	UIFont_t2766063701::get_offset_of_mDynamicFontStyle_10(),
	UIFont_t2766063701::get_offset_of_mSprite_11(),
	UIFont_t2766063701::get_offset_of_mPMA_12(),
	UIFont_t2766063701::get_offset_of_mPacked_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6024 = { sizeof (UIInput_t421821618), -1, sizeof(UIInput_t421821618_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6024[41] = 
{
	UIInput_t421821618_StaticFields::get_offset_of_current_2(),
	UIInput_t421821618_StaticFields::get_offset_of_selection_3(),
	UIInput_t421821618::get_offset_of_label_4(),
	UIInput_t421821618::get_offset_of_inputType_5(),
	UIInput_t421821618::get_offset_of_onReturnKey_6(),
	UIInput_t421821618::get_offset_of_keyboardType_7(),
	UIInput_t421821618::get_offset_of_hideInput_8(),
	UIInput_t421821618::get_offset_of_selectAllTextOnFocus_9(),
	UIInput_t421821618::get_offset_of_validation_10(),
	UIInput_t421821618::get_offset_of_characterLimit_11(),
	UIInput_t421821618::get_offset_of_savedAs_12(),
	UIInput_t421821618::get_offset_of_selectOnTab_13(),
	UIInput_t421821618::get_offset_of_activeTextColor_14(),
	UIInput_t421821618::get_offset_of_caretColor_15(),
	UIInput_t421821618::get_offset_of_selectionColor_16(),
	UIInput_t421821618::get_offset_of_onSubmit_17(),
	UIInput_t421821618::get_offset_of_onChange_18(),
	UIInput_t421821618::get_offset_of_onValidate_19(),
	UIInput_t421821618::get_offset_of_mValue_20(),
	UIInput_t421821618::get_offset_of_mDefaultText_21(),
	UIInput_t421821618::get_offset_of_mDefaultColor_22(),
	UIInput_t421821618::get_offset_of_mPosition_23(),
	UIInput_t421821618::get_offset_of_mDoInit_24(),
	UIInput_t421821618::get_offset_of_mPivot_25(),
	UIInput_t421821618::get_offset_of_mLoadSavedValue_26(),
	UIInput_t421821618_StaticFields::get_offset_of_mDrawStart_27(),
	UIInput_t421821618_StaticFields::get_offset_of_mLastIME_28(),
	UIInput_t421821618_StaticFields::get_offset_of_mKeyboard_29(),
	UIInput_t421821618_StaticFields::get_offset_of_mWaitForKeyboard_30(),
	UIInput_t421821618::get_offset_of_mSelectionStart_31(),
	UIInput_t421821618::get_offset_of_mSelectionEnd_32(),
	UIInput_t421821618::get_offset_of_mHighlight_33(),
	UIInput_t421821618::get_offset_of_mCaret_34(),
	UIInput_t421821618::get_offset_of_mBlankTex_35(),
	UIInput_t421821618::get_offset_of_mNextBlink_36(),
	UIInput_t421821618::get_offset_of_mLastAlpha_37(),
	UIInput_t421821618::get_offset_of_mCached_38(),
	UIInput_t421821618::get_offset_of_mSelectMe_39(),
	UIInput_t421821618::get_offset_of_mSelectTime_40(),
	UIInput_t421821618::get_offset_of_mCam_41(),
	UIInput_t421821618_StaticFields::get_offset_of_mIgnoreKey_42(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6025 = { sizeof (InputType_t3084801673)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6025[4] = 
{
	InputType_t3084801673::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6026 = { sizeof (Validation_t3618213463)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6026[8] = 
{
	Validation_t3618213463::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6027 = { sizeof (KeyboardType_t3374647619)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6027[9] = 
{
	KeyboardType_t3374647619::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6028 = { sizeof (OnReturnKey_t3803455137)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6028[4] = 
{
	OnReturnKey_t3803455137::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6029 = { sizeof (OnValidate_t1246632601), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6030 = { sizeof (UIInputOnGUI_t4239979770), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6031 = { sizeof (UILabel_t3248798549), -1, sizeof(UILabel_t3248798549_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6031[44] = 
{
	UILabel_t3248798549::get_offset_of_keepCrispWhenShrunk_52(),
	UILabel_t3248798549::get_offset_of_mTrueTypeFont_53(),
	UILabel_t3248798549::get_offset_of_mFont_54(),
	UILabel_t3248798549::get_offset_of_mText_55(),
	UILabel_t3248798549::get_offset_of_mFontSize_56(),
	UILabel_t3248798549::get_offset_of_mFontStyle_57(),
	UILabel_t3248798549::get_offset_of_mAlignment_58(),
	UILabel_t3248798549::get_offset_of_mEncoding_59(),
	UILabel_t3248798549::get_offset_of_mMaxLineCount_60(),
	UILabel_t3248798549::get_offset_of_mEffectStyle_61(),
	UILabel_t3248798549::get_offset_of_mEffectColor_62(),
	UILabel_t3248798549::get_offset_of_mSymbols_63(),
	UILabel_t3248798549::get_offset_of_mEffectDistance_64(),
	UILabel_t3248798549::get_offset_of_mOverflow_65(),
	UILabel_t3248798549::get_offset_of_mMaterial_66(),
	UILabel_t3248798549::get_offset_of_mApplyGradient_67(),
	UILabel_t3248798549::get_offset_of_mGradientTop_68(),
	UILabel_t3248798549::get_offset_of_mGradientBottom_69(),
	UILabel_t3248798549::get_offset_of_mSpacingX_70(),
	UILabel_t3248798549::get_offset_of_mSpacingY_71(),
	UILabel_t3248798549::get_offset_of_mUseFloatSpacing_72(),
	UILabel_t3248798549::get_offset_of_mFloatSpacingX_73(),
	UILabel_t3248798549::get_offset_of_mFloatSpacingY_74(),
	UILabel_t3248798549::get_offset_of_mShrinkToFit_75(),
	UILabel_t3248798549::get_offset_of_mMaxLineWidth_76(),
	UILabel_t3248798549::get_offset_of_mMaxLineHeight_77(),
	UILabel_t3248798549::get_offset_of_mLineWidth_78(),
	UILabel_t3248798549::get_offset_of_mMultiline_79(),
	UILabel_t3248798549::get_offset_of_mActiveTTF_80(),
	UILabel_t3248798549::get_offset_of_mDensity_81(),
	UILabel_t3248798549::get_offset_of_mShouldBeProcessed_82(),
	UILabel_t3248798549::get_offset_of_mProcessedText_83(),
	UILabel_t3248798549::get_offset_of_mPremultiply_84(),
	UILabel_t3248798549::get_offset_of_mCalculatedSize_85(),
	UILabel_t3248798549::get_offset_of_mScale_86(),
	UILabel_t3248798549::get_offset_of_mPrintedSize_87(),
	UILabel_t3248798549::get_offset_of_mLastWidth_88(),
	UILabel_t3248798549::get_offset_of_mLastHeight_89(),
	UILabel_t3248798549_StaticFields::get_offset_of_mList_90(),
	UILabel_t3248798549_StaticFields::get_offset_of_mFontUsage_91(),
	UILabel_t3248798549_StaticFields::get_offset_of_mTexRebuildAdded_92(),
	UILabel_t3248798549_StaticFields::get_offset_of_mTempVerts_93(),
	UILabel_t3248798549_StaticFields::get_offset_of_mTempIndices_94(),
	UILabel_t3248798549_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_95(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6032 = { sizeof (Effect_t2533209744)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6032[5] = 
{
	Effect_t2533209744::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6033 = { sizeof (Overflow_t884389639)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6033[5] = 
{
	Overflow_t884389639::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6034 = { sizeof (Crispness_t2029465680)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6034[4] = 
{
	Crispness_t2029465680::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6035 = { sizeof (UILocalize_t3543745742), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6035[2] = 
{
	UILocalize_t3543745742::get_offset_of_key_2(),
	UILocalize_t3543745742::get_offset_of_mStarted_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6036 = { sizeof (UIOrthoCamera_t1944225589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6036[2] = 
{
	UIOrthoCamera_t1944225589::get_offset_of_mCam_2(),
	UIOrthoCamera_t1944225589::get_offset_of_mTrans_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6037 = { sizeof (UIPanel_t1716472341), -1, sizeof(UIPanel_t1716472341_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6037[43] = 
{
	UIPanel_t1716472341_StaticFields::get_offset_of_list_22(),
	UIPanel_t1716472341::get_offset_of_onGeometryUpdated_23(),
	UIPanel_t1716472341::get_offset_of_showInPanelTool_24(),
	UIPanel_t1716472341::get_offset_of_generateNormals_25(),
	UIPanel_t1716472341::get_offset_of_widgetsAreStatic_26(),
	UIPanel_t1716472341::get_offset_of_cullWhileDragging_27(),
	UIPanel_t1716472341::get_offset_of_alwaysOnScreen_28(),
	UIPanel_t1716472341::get_offset_of_anchorOffset_29(),
	UIPanel_t1716472341::get_offset_of_softBorderPadding_30(),
	UIPanel_t1716472341::get_offset_of_renderQueue_31(),
	UIPanel_t1716472341::get_offset_of_startingRenderQueue_32(),
	UIPanel_t1716472341::get_offset_of_widgets_33(),
	UIPanel_t1716472341::get_offset_of_drawCalls_34(),
	UIPanel_t1716472341::get_offset_of_worldToLocal_35(),
	UIPanel_t1716472341::get_offset_of_drawCallClipRange_36(),
	UIPanel_t1716472341::get_offset_of_onClipMove_37(),
	UIPanel_t1716472341::get_offset_of_mClipTexture_38(),
	UIPanel_t1716472341::get_offset_of_mAlpha_39(),
	UIPanel_t1716472341::get_offset_of_mClipping_40(),
	UIPanel_t1716472341::get_offset_of_mClipRange_41(),
	UIPanel_t1716472341::get_offset_of_mClipSoftness_42(),
	UIPanel_t1716472341::get_offset_of_mDepth_43(),
	UIPanel_t1716472341::get_offset_of_mSortingOrder_44(),
	UIPanel_t1716472341::get_offset_of_mRebuild_45(),
	UIPanel_t1716472341::get_offset_of_mResized_46(),
	UIPanel_t1716472341::get_offset_of_mClipOffset_47(),
	UIPanel_t1716472341::get_offset_of_mMatrixFrame_48(),
	UIPanel_t1716472341::get_offset_of_mAlphaFrameID_49(),
	UIPanel_t1716472341::get_offset_of_mLayer_50(),
	UIPanel_t1716472341_StaticFields::get_offset_of_mTemp_51(),
	UIPanel_t1716472341::get_offset_of_mMin_52(),
	UIPanel_t1716472341::get_offset_of_mMax_53(),
	UIPanel_t1716472341::get_offset_of_mHalfPixelOffset_54(),
	UIPanel_t1716472341::get_offset_of_mSortWidgets_55(),
	UIPanel_t1716472341::get_offset_of_mUpdateScroll_56(),
	UIPanel_t1716472341::get_offset_of_mParentPanel_57(),
	UIPanel_t1716472341_StaticFields::get_offset_of_mCorners_58(),
	UIPanel_t1716472341_StaticFields::get_offset_of_mUpdateFrame_59(),
	UIPanel_t1716472341::get_offset_of_mOnRender_60(),
	UIPanel_t1716472341::get_offset_of_mForced_61(),
	UIPanel_t1716472341_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_62(),
	UIPanel_t1716472341_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_63(),
	UIPanel_t1716472341_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_64(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6038 = { sizeof (RenderQueue_t2721716586)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6038[4] = 
{
	RenderQueue_t2721716586::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6039 = { sizeof (OnGeometryUpdated_t2462438111), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6040 = { sizeof (OnClippingMoved_t476625095), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6041 = { sizeof (UIRoot_t4022971450), -1, sizeof(UIRoot_t4022971450_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6041[11] = 
{
	UIRoot_t4022971450_StaticFields::get_offset_of_list_2(),
	UIRoot_t4022971450::get_offset_of_scalingStyle_3(),
	UIRoot_t4022971450::get_offset_of_manualWidth_4(),
	UIRoot_t4022971450::get_offset_of_manualHeight_5(),
	UIRoot_t4022971450::get_offset_of_minimumHeight_6(),
	UIRoot_t4022971450::get_offset_of_maximumHeight_7(),
	UIRoot_t4022971450::get_offset_of_fitWidth_8(),
	UIRoot_t4022971450::get_offset_of_fitHeight_9(),
	UIRoot_t4022971450::get_offset_of_adjustByDPI_10(),
	UIRoot_t4022971450::get_offset_of_shrinkPortraitUI_11(),
	UIRoot_t4022971450::get_offset_of_mTrans_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6042 = { sizeof (Scaling_t3068739704)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6042[4] = 
{
	Scaling_t3068739704::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6043 = { sizeof (Constraint_t400694485)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6043[5] = 
{
	Constraint_t400694485::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6044 = { sizeof (UISprite_t194114938), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6044[5] = 
{
	UISprite_t194114938::get_offset_of_mAtlas_66(),
	UISprite_t194114938::get_offset_of_mSpriteName_67(),
	UISprite_t194114938::get_offset_of_mFillCenter_68(),
	UISprite_t194114938::get_offset_of_mSprite_69(),
	UISprite_t194114938::get_offset_of_mSpriteSet_70(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6045 = { sizeof (UISpriteAnimation_t1118314077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6045[9] = 
{
	UISpriteAnimation_t1118314077::get_offset_of_mFPS_2(),
	UISpriteAnimation_t1118314077::get_offset_of_mPrefix_3(),
	UISpriteAnimation_t1118314077::get_offset_of_mLoop_4(),
	UISpriteAnimation_t1118314077::get_offset_of_mSnap_5(),
	UISpriteAnimation_t1118314077::get_offset_of_mSprite_6(),
	UISpriteAnimation_t1118314077::get_offset_of_mDelta_7(),
	UISpriteAnimation_t1118314077::get_offset_of_mIndex_8(),
	UISpriteAnimation_t1118314077::get_offset_of_mActive_9(),
	UISpriteAnimation_t1118314077::get_offset_of_mSpriteNames_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6046 = { sizeof (UISpriteData_t900308526), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6046[13] = 
{
	UISpriteData_t900308526::get_offset_of_name_0(),
	UISpriteData_t900308526::get_offset_of_x_1(),
	UISpriteData_t900308526::get_offset_of_y_2(),
	UISpriteData_t900308526::get_offset_of_width_3(),
	UISpriteData_t900308526::get_offset_of_height_4(),
	UISpriteData_t900308526::get_offset_of_borderLeft_5(),
	UISpriteData_t900308526::get_offset_of_borderRight_6(),
	UISpriteData_t900308526::get_offset_of_borderTop_7(),
	UISpriteData_t900308526::get_offset_of_borderBottom_8(),
	UISpriteData_t900308526::get_offset_of_paddingLeft_9(),
	UISpriteData_t900308526::get_offset_of_paddingRight_10(),
	UISpriteData_t900308526::get_offset_of_paddingTop_11(),
	UISpriteData_t900308526::get_offset_of_paddingBottom_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6047 = { sizeof (UIStretch_t3058335968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6047[16] = 
{
	UIStretch_t3058335968::get_offset_of_uiCamera_2(),
	UIStretch_t3058335968::get_offset_of_container_3(),
	UIStretch_t3058335968::get_offset_of_style_4(),
	UIStretch_t3058335968::get_offset_of_runOnlyOnce_5(),
	UIStretch_t3058335968::get_offset_of_relativeSize_6(),
	UIStretch_t3058335968::get_offset_of_initialSize_7(),
	UIStretch_t3058335968::get_offset_of_borderPadding_8(),
	UIStretch_t3058335968::get_offset_of_widgetContainer_9(),
	UIStretch_t3058335968::get_offset_of_mTrans_10(),
	UIStretch_t3058335968::get_offset_of_mWidget_11(),
	UIStretch_t3058335968::get_offset_of_mSprite_12(),
	UIStretch_t3058335968::get_offset_of_mPanel_13(),
	UIStretch_t3058335968::get_offset_of_mRoot_14(),
	UIStretch_t3058335968::get_offset_of_mAnim_15(),
	UIStretch_t3058335968::get_offset_of_mRect_16(),
	UIStretch_t3058335968::get_offset_of_mStarted_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6048 = { sizeof (Style_t3184300279)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6048[8] = 
{
	Style_t3184300279::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6049 = { sizeof (UITextList_t2040849008), -1, sizeof(UITextList_t2040849008_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6049[11] = 
{
	UITextList_t2040849008::get_offset_of_textLabel_2(),
	UITextList_t2040849008::get_offset_of_scrollBar_3(),
	UITextList_t2040849008::get_offset_of_style_4(),
	UITextList_t2040849008::get_offset_of_paragraphHistory_5(),
	UITextList_t2040849008::get_offset_of_mSeparator_6(),
	UITextList_t2040849008::get_offset_of_mScroll_7(),
	UITextList_t2040849008::get_offset_of_mTotalLines_8(),
	UITextList_t2040849008::get_offset_of_mLastWidth_9(),
	UITextList_t2040849008::get_offset_of_mLastHeight_10(),
	UITextList_t2040849008::get_offset_of_mParagraphs_11(),
	UITextList_t2040849008_StaticFields::get_offset_of_mHistory_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6050 = { sizeof (Style_t3106380496)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6050[3] = 
{
	Style_t3106380496::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6051 = { sizeof (Paragraph_t1111063719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6051[2] = 
{
	Paragraph_t1111063719::get_offset_of_text_0(),
	Paragraph_t1111063719::get_offset_of_lines_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6052 = { sizeof (UITexture_t3471168817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6052[7] = 
{
	UITexture_t3471168817::get_offset_of_mRect_66(),
	UITexture_t3471168817::get_offset_of_mTexture_67(),
	UITexture_t3471168817::get_offset_of_mMat_68(),
	UITexture_t3471168817::get_offset_of_mShader_69(),
	UITexture_t3471168817::get_offset_of_mBorder_70(),
	UITexture_t3471168817::get_offset_of_mFixedAspect_71(),
	UITexture_t3471168817::get_offset_of_mPMA_72(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6053 = { sizeof (UITooltip_t30236576), -1, sizeof(UITooltip_t30236576_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6053[13] = 
{
	UITooltip_t30236576_StaticFields::get_offset_of_mInstance_2(),
	UITooltip_t30236576::get_offset_of_uiCamera_3(),
	UITooltip_t30236576::get_offset_of_text_4(),
	UITooltip_t30236576::get_offset_of_background_5(),
	UITooltip_t30236576::get_offset_of_appearSpeed_6(),
	UITooltip_t30236576::get_offset_of_scalingTransitions_7(),
	UITooltip_t30236576::get_offset_of_mTooltip_8(),
	UITooltip_t30236576::get_offset_of_mTrans_9(),
	UITooltip_t30236576::get_offset_of_mTarget_10(),
	UITooltip_t30236576::get_offset_of_mCurrent_11(),
	UITooltip_t30236576::get_offset_of_mPos_12(),
	UITooltip_t30236576::get_offset_of_mSize_13(),
	UITooltip_t30236576::get_offset_of_mWidgets_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6054 = { sizeof (UIViewport_t1918760134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6054[5] = 
{
	UIViewport_t1918760134::get_offset_of_sourceCamera_2(),
	UIViewport_t1918760134::get_offset_of_topLeft_3(),
	UIViewport_t1918760134::get_offset_of_bottomRight_4(),
	UIViewport_t1918760134::get_offset_of_fullSize_5(),
	UIViewport_t1918760134::get_offset_of_mCam_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6055 = { sizeof (OnEnableCallEvent_t3645046440), -1, sizeof(OnEnableCallEvent_t3645046440_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6055[3] = 
{
	OnEnableCallEvent_t3645046440::get_offset_of_b_2(),
	OnEnableCallEvent_t3645046440::get_offset_of_enb_3(),
	OnEnableCallEvent_t3645046440_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6056 = { sizeof (TextCheck_t3911857046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6056[2] = 
{
	TextCheck_t3911857046::get_offset_of_U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_2(),
	TextCheck_t3911857046::get_offset_of_Tmp_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6057 = { sizeof (U3CwaitU3Ec__Iterator0_t1024662273), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6057[4] = 
{
	U3CwaitU3Ec__Iterator0_t1024662273::get_offset_of_U24this_0(),
	U3CwaitU3Ec__Iterator0_t1024662273::get_offset_of_U24current_1(),
	U3CwaitU3Ec__Iterator0_t1024662273::get_offset_of_U24disposing_2(),
	U3CwaitU3Ec__Iterator0_t1024662273::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6058 = { sizeof (BackgroundPlaneBehaviour_t3333547397), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6059 = { sizeof (CloudRecoBehaviour_t431762792), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6060 = { sizeof (CylinderTargetBehaviour_t822809409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6061 = { sizeof (DefaultInitializationErrorHandler_t922997482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6061[3] = 
{
	DefaultInitializationErrorHandler_t922997482::get_offset_of_mErrorText_2(),
	DefaultInitializationErrorHandler_t922997482::get_offset_of_mErrorOccurred_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6062 = { sizeof (DefaultSmartTerrainEventHandler_t2395349464), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6062[3] = 
{
	DefaultSmartTerrainEventHandler_t2395349464::get_offset_of_mReconstructionBehaviour_2(),
	DefaultSmartTerrainEventHandler_t2395349464::get_offset_of_PropTemplate_3(),
	DefaultSmartTerrainEventHandler_t2395349464::get_offset_of_SurfaceTemplate_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6063 = { sizeof (DefaultTrackableEventHandler_t95800019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6063[3] = 
{
	DefaultTrackableEventHandler_t95800019::get_offset_of_TrackingFound_2(),
	DefaultTrackableEventHandler_t95800019::get_offset_of_TrackingLost_3(),
	DefaultTrackableEventHandler_t95800019::get_offset_of_mTrackableBehaviour_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6064 = { sizeof (GLErrorHandler_t2139335054), -1, sizeof(GLErrorHandler_t2139335054_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6064[3] = 
{
	GLErrorHandler_t2139335054_StaticFields::get_offset_of_mErrorText_2(),
	GLErrorHandler_t2139335054_StaticFields::get_offset_of_mErrorOccurred_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6065 = { sizeof (HideExcessAreaBehaviour_t2892437830), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6066 = { sizeof (ImageTargetBehaviour_t2200418350), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6067 = { sizeof (AndroidUnityPlayer_t2737599080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6067[6] = 
{
	0,
	0,
	AndroidUnityPlayer_t2737599080::get_offset_of_mScreenOrientation_2(),
	AndroidUnityPlayer_t2737599080::get_offset_of_mJavaScreenOrientation_3(),
	AndroidUnityPlayer_t2737599080::get_offset_of_mFramesSinceLastOrientationReset_4(),
	AndroidUnityPlayer_t2737599080::get_offset_of_mFramesSinceLastJavaOrientationCheck_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6068 = { sizeof (ComponentFactoryStarterBehaviour_t3931359467), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6069 = { sizeof (IOSUnityPlayer_t2555589894), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6069[1] = 
{
	IOSUnityPlayer_t2555589894::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6070 = { sizeof (VuforiaBehaviourComponentFactory_t4153723608), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6071 = { sizeof (WSAUnityPlayer_t3135728299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6071[1] = 
{
	WSAUnityPlayer_t3135728299::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6072 = { sizeof (MaskOutBehaviour_t2745617306), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6073 = { sizeof (MultiTargetBehaviour_t2061511750), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6074 = { sizeof (ObjectTargetBehaviour_t728125005), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6075 = { sizeof (PropBehaviour_t2792829701), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6076 = { sizeof (ReconstructionBehaviour_t3655135626), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6077 = { sizeof (ReconstructionFromTargetBehaviour_t160667116), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6078 = { sizeof (SurfaceBehaviour_t3650770673), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6079 = { sizeof (TextRecoBehaviour_t87475147), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6080 = { sizeof (TurnOffBehaviour_t65964226), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6081 = { sizeof (TurnOffWordBehaviour_t2771985595), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6082 = { sizeof (UserDefinedTargetBuildingBehaviour_t4262637471), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6083 = { sizeof (VRIntegrationHelper_t3162465173), -1, sizeof(VRIntegrationHelper_t3162465173_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6083[12] = 
{
	VRIntegrationHelper_t3162465173_StaticFields::get_offset_of_mLeftCameraMatrixOriginal_2(),
	VRIntegrationHelper_t3162465173_StaticFields::get_offset_of_mRightCameraMatrixOriginal_3(),
	VRIntegrationHelper_t3162465173_StaticFields::get_offset_of_mLeftCamera_4(),
	VRIntegrationHelper_t3162465173_StaticFields::get_offset_of_mRightCamera_5(),
	VRIntegrationHelper_t3162465173_StaticFields::get_offset_of_mLeftExcessAreaBehaviour_6(),
	VRIntegrationHelper_t3162465173_StaticFields::get_offset_of_mRightExcessAreaBehaviour_7(),
	VRIntegrationHelper_t3162465173_StaticFields::get_offset_of_mLeftCameraPixelRect_8(),
	VRIntegrationHelper_t3162465173_StaticFields::get_offset_of_mRightCameraPixelRect_9(),
	VRIntegrationHelper_t3162465173_StaticFields::get_offset_of_mLeftCameraDataAcquired_10(),
	VRIntegrationHelper_t3162465173_StaticFields::get_offset_of_mRightCameraDataAcquired_11(),
	VRIntegrationHelper_t3162465173::get_offset_of_IsLeft_12(),
	VRIntegrationHelper_t3162465173::get_offset_of_TrackableParent_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6084 = { sizeof (VideoBackgroundBehaviour_t1552899074), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6085 = { sizeof (VirtualButtonBehaviour_t1436326451), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6086 = { sizeof (VuMarkBehaviour_t1178230459), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6087 = { sizeof (VuforiaBehaviour_t2151848540), -1, sizeof(VuforiaBehaviour_t2151848540_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6087[1] = 
{
	VuforiaBehaviour_t2151848540_StaticFields::get_offset_of_mVuforiaBehaviour_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6088 = { sizeof (VuforiaConfiguration_t1763229349), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6089 = { sizeof (VuforiaRuntimeInitialization_t714775116), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6090 = { sizeof (WireframeBehaviour_t1831066704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6090[4] = 
{
	WireframeBehaviour_t1831066704::get_offset_of_lineMaterial_2(),
	WireframeBehaviour_t1831066704::get_offset_of_ShowLines_3(),
	WireframeBehaviour_t1831066704::get_offset_of_LineColor_4(),
	WireframeBehaviour_t1831066704::get_offset_of_mLineMaterial_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6091 = { sizeof (WireframeTrackableEventHandler_t2143753312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6091[1] = 
{
	WireframeTrackableEventHandler_t2143753312::get_offset_of_mTrackableBehaviour_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6092 = { sizeof (WordBehaviour_t209462683), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6093 = { sizeof (AbstractTargetFollower_t1919708159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6093[4] = 
{
	AbstractTargetFollower_t1919708159::get_offset_of_m_Target_2(),
	AbstractTargetFollower_t1919708159::get_offset_of_m_AutoTargetPlayer_3(),
	AbstractTargetFollower_t1919708159::get_offset_of_m_UpdateType_4(),
	AbstractTargetFollower_t1919708159::get_offset_of_targetRigidbody_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6094 = { sizeof (UpdateType_t2449601881)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6094[4] = 
{
	UpdateType_t2449601881::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6095 = { sizeof (AutoCam_t137911967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6095[12] = 
{
	AutoCam_t137911967::get_offset_of_m_MoveSpeed_9(),
	AutoCam_t137911967::get_offset_of_m_TurnSpeed_10(),
	AutoCam_t137911967::get_offset_of_m_RollSpeed_11(),
	AutoCam_t137911967::get_offset_of_m_FollowVelocity_12(),
	AutoCam_t137911967::get_offset_of_m_FollowTilt_13(),
	AutoCam_t137911967::get_offset_of_m_SpinTurnLimit_14(),
	AutoCam_t137911967::get_offset_of_m_TargetVelocityLowerLimit_15(),
	AutoCam_t137911967::get_offset_of_m_SmoothTurnTime_16(),
	AutoCam_t137911967::get_offset_of_m_LastFlatAngle_17(),
	AutoCam_t137911967::get_offset_of_m_CurrentTurnAmount_18(),
	AutoCam_t137911967::get_offset_of_m_TurnSpeedVelocityChange_19(),
	AutoCam_t137911967::get_offset_of_m_RollUp_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6096 = { sizeof (FreeLookCam_t2000732766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6096[13] = 
{
	FreeLookCam_t2000732766::get_offset_of_m_MoveSpeed_9(),
	FreeLookCam_t2000732766::get_offset_of_m_TurnSpeed_10(),
	FreeLookCam_t2000732766::get_offset_of_m_TurnSmoothing_11(),
	FreeLookCam_t2000732766::get_offset_of_m_TiltMax_12(),
	FreeLookCam_t2000732766::get_offset_of_m_TiltMin_13(),
	FreeLookCam_t2000732766::get_offset_of_m_LockCursor_14(),
	FreeLookCam_t2000732766::get_offset_of_m_VerticalAutoReturn_15(),
	FreeLookCam_t2000732766::get_offset_of_m_LookAngle_16(),
	FreeLookCam_t2000732766::get_offset_of_m_TiltAngle_17(),
	0,
	FreeLookCam_t2000732766::get_offset_of_m_PivotEulers_19(),
	FreeLookCam_t2000732766::get_offset_of_m_PivotTargetRot_20(),
	FreeLookCam_t2000732766::get_offset_of_m_TransformTargetRot_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6097 = { sizeof (HandHeldCam_t450595784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6097[4] = 
{
	HandHeldCam_t450595784::get_offset_of_m_SwaySpeed_11(),
	HandHeldCam_t450595784::get_offset_of_m_BaseSwayAmount_12(),
	HandHeldCam_t450595784::get_offset_of_m_TrackingSwayAmount_13(),
	HandHeldCam_t450595784::get_offset_of_m_TrackingBias_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6098 = { sizeof (LookatTarget_t3260877718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6098[5] = 
{
	LookatTarget_t3260877718::get_offset_of_m_RotationRange_6(),
	LookatTarget_t3260877718::get_offset_of_m_FollowSpeed_7(),
	LookatTarget_t3260877718::get_offset_of_m_FollowAngles_8(),
	LookatTarget_t3260877718::get_offset_of_m_OriginalRotation_9(),
	LookatTarget_t3260877718::get_offset_of_m_FollowVelocity_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6099 = { sizeof (PivotBasedCameraRig_t3786953582), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6099[3] = 
{
	PivotBasedCameraRig_t3786953582::get_offset_of_m_Cam_6(),
	PivotBasedCameraRig_t3786953582::get_offset_of_m_Pivot_7(),
	PivotBasedCameraRig_t3786953582::get_offset_of_m_LastTargetPosition_8(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
