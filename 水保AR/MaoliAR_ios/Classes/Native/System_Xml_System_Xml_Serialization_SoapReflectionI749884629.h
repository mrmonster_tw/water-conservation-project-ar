﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Xml.Serialization.SoapAttributeOverrides
struct SoapAttributeOverrides_t1295397514;
// System.String
struct String_t;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Xml.Serialization.ReflectionHelper
struct ReflectionHelper_t300476302;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.SoapReflectionImporter
struct  SoapReflectionImporter_t749884629  : public Il2CppObject
{
public:
	// System.Xml.Serialization.SoapAttributeOverrides System.Xml.Serialization.SoapReflectionImporter::attributeOverrides
	SoapAttributeOverrides_t1295397514 * ___attributeOverrides_0;
	// System.String System.Xml.Serialization.SoapReflectionImporter::initialDefaultNamespace
	String_t* ___initialDefaultNamespace_1;
	// System.Collections.ArrayList System.Xml.Serialization.SoapReflectionImporter::includedTypes
	ArrayList_t2718874744 * ___includedTypes_2;
	// System.Collections.ArrayList System.Xml.Serialization.SoapReflectionImporter::relatedMaps
	ArrayList_t2718874744 * ___relatedMaps_3;
	// System.Xml.Serialization.ReflectionHelper System.Xml.Serialization.SoapReflectionImporter::helper
	ReflectionHelper_t300476302 * ___helper_4;

public:
	inline static int32_t get_offset_of_attributeOverrides_0() { return static_cast<int32_t>(offsetof(SoapReflectionImporter_t749884629, ___attributeOverrides_0)); }
	inline SoapAttributeOverrides_t1295397514 * get_attributeOverrides_0() const { return ___attributeOverrides_0; }
	inline SoapAttributeOverrides_t1295397514 ** get_address_of_attributeOverrides_0() { return &___attributeOverrides_0; }
	inline void set_attributeOverrides_0(SoapAttributeOverrides_t1295397514 * value)
	{
		___attributeOverrides_0 = value;
		Il2CppCodeGenWriteBarrier(&___attributeOverrides_0, value);
	}

	inline static int32_t get_offset_of_initialDefaultNamespace_1() { return static_cast<int32_t>(offsetof(SoapReflectionImporter_t749884629, ___initialDefaultNamespace_1)); }
	inline String_t* get_initialDefaultNamespace_1() const { return ___initialDefaultNamespace_1; }
	inline String_t** get_address_of_initialDefaultNamespace_1() { return &___initialDefaultNamespace_1; }
	inline void set_initialDefaultNamespace_1(String_t* value)
	{
		___initialDefaultNamespace_1 = value;
		Il2CppCodeGenWriteBarrier(&___initialDefaultNamespace_1, value);
	}

	inline static int32_t get_offset_of_includedTypes_2() { return static_cast<int32_t>(offsetof(SoapReflectionImporter_t749884629, ___includedTypes_2)); }
	inline ArrayList_t2718874744 * get_includedTypes_2() const { return ___includedTypes_2; }
	inline ArrayList_t2718874744 ** get_address_of_includedTypes_2() { return &___includedTypes_2; }
	inline void set_includedTypes_2(ArrayList_t2718874744 * value)
	{
		___includedTypes_2 = value;
		Il2CppCodeGenWriteBarrier(&___includedTypes_2, value);
	}

	inline static int32_t get_offset_of_relatedMaps_3() { return static_cast<int32_t>(offsetof(SoapReflectionImporter_t749884629, ___relatedMaps_3)); }
	inline ArrayList_t2718874744 * get_relatedMaps_3() const { return ___relatedMaps_3; }
	inline ArrayList_t2718874744 ** get_address_of_relatedMaps_3() { return &___relatedMaps_3; }
	inline void set_relatedMaps_3(ArrayList_t2718874744 * value)
	{
		___relatedMaps_3 = value;
		Il2CppCodeGenWriteBarrier(&___relatedMaps_3, value);
	}

	inline static int32_t get_offset_of_helper_4() { return static_cast<int32_t>(offsetof(SoapReflectionImporter_t749884629, ___helper_4)); }
	inline ReflectionHelper_t300476302 * get_helper_4() const { return ___helper_4; }
	inline ReflectionHelper_t300476302 ** get_address_of_helper_4() { return &___helper_4; }
	inline void set_helper_4(ReflectionHelper_t300476302 * value)
	{
		___helper_4 = value;
		Il2CppCodeGenWriteBarrier(&___helper_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
