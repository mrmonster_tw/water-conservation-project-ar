﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Tokens_S1943429813.h"

// System.String
struct String_t;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Tokens.LocalIdKeyIdentifierClause
struct  LocalIdKeyIdentifierClause_t3641697518  : public SecurityKeyIdentifierClause_t1943429813
{
public:
	// System.String System.IdentityModel.Tokens.LocalIdKeyIdentifierClause::local_id
	String_t* ___local_id_3;
	// System.Type System.IdentityModel.Tokens.LocalIdKeyIdentifierClause::owner_type
	Type_t * ___owner_type_4;

public:
	inline static int32_t get_offset_of_local_id_3() { return static_cast<int32_t>(offsetof(LocalIdKeyIdentifierClause_t3641697518, ___local_id_3)); }
	inline String_t* get_local_id_3() const { return ___local_id_3; }
	inline String_t** get_address_of_local_id_3() { return &___local_id_3; }
	inline void set_local_id_3(String_t* value)
	{
		___local_id_3 = value;
		Il2CppCodeGenWriteBarrier(&___local_id_3, value);
	}

	inline static int32_t get_offset_of_owner_type_4() { return static_cast<int32_t>(offsetof(LocalIdKeyIdentifierClause_t3641697518, ___owner_type_4)); }
	inline Type_t * get_owner_type_4() const { return ___owner_type_4; }
	inline Type_t ** get_address_of_owner_type_4() { return &___owner_type_4; }
	inline void set_owner_type_4(Type_t * value)
	{
		___owner_type_4 = value;
		Il2CppCodeGenWriteBarrier(&___owner_type_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
