﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeTypeMember1555525554.h"

// System.CodeDom.CodeStatementCollection
struct CodeStatementCollection_t1265263501;
// System.CodeDom.CodeTypeReferenceCollection
struct CodeTypeReferenceCollection_t3857551471;
// System.CodeDom.CodeParameterDeclarationExpressionCollection
struct CodeParameterDeclarationExpressionCollection_t3391789433;
// System.CodeDom.CodeTypeReference
struct CodeTypeReference_t3809997434;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeMemberProperty
struct  CodeMemberProperty_t2419270920  : public CodeTypeMember_t1555525554
{
public:
	// System.CodeDom.CodeStatementCollection System.CodeDom.CodeMemberProperty::getStatements
	CodeStatementCollection_t1265263501 * ___getStatements_8;
	// System.Boolean System.CodeDom.CodeMemberProperty::hasGet
	bool ___hasGet_9;
	// System.Boolean System.CodeDom.CodeMemberProperty::hasSet
	bool ___hasSet_10;
	// System.CodeDom.CodeTypeReferenceCollection System.CodeDom.CodeMemberProperty::implementationTypes
	CodeTypeReferenceCollection_t3857551471 * ___implementationTypes_11;
	// System.CodeDom.CodeParameterDeclarationExpressionCollection System.CodeDom.CodeMemberProperty::parameters
	CodeParameterDeclarationExpressionCollection_t3391789433 * ___parameters_12;
	// System.CodeDom.CodeTypeReference System.CodeDom.CodeMemberProperty::privateImplementationType
	CodeTypeReference_t3809997434 * ___privateImplementationType_13;
	// System.CodeDom.CodeStatementCollection System.CodeDom.CodeMemberProperty::setStatements
	CodeStatementCollection_t1265263501 * ___setStatements_14;
	// System.CodeDom.CodeTypeReference System.CodeDom.CodeMemberProperty::type
	CodeTypeReference_t3809997434 * ___type_15;

public:
	inline static int32_t get_offset_of_getStatements_8() { return static_cast<int32_t>(offsetof(CodeMemberProperty_t2419270920, ___getStatements_8)); }
	inline CodeStatementCollection_t1265263501 * get_getStatements_8() const { return ___getStatements_8; }
	inline CodeStatementCollection_t1265263501 ** get_address_of_getStatements_8() { return &___getStatements_8; }
	inline void set_getStatements_8(CodeStatementCollection_t1265263501 * value)
	{
		___getStatements_8 = value;
		Il2CppCodeGenWriteBarrier(&___getStatements_8, value);
	}

	inline static int32_t get_offset_of_hasGet_9() { return static_cast<int32_t>(offsetof(CodeMemberProperty_t2419270920, ___hasGet_9)); }
	inline bool get_hasGet_9() const { return ___hasGet_9; }
	inline bool* get_address_of_hasGet_9() { return &___hasGet_9; }
	inline void set_hasGet_9(bool value)
	{
		___hasGet_9 = value;
	}

	inline static int32_t get_offset_of_hasSet_10() { return static_cast<int32_t>(offsetof(CodeMemberProperty_t2419270920, ___hasSet_10)); }
	inline bool get_hasSet_10() const { return ___hasSet_10; }
	inline bool* get_address_of_hasSet_10() { return &___hasSet_10; }
	inline void set_hasSet_10(bool value)
	{
		___hasSet_10 = value;
	}

	inline static int32_t get_offset_of_implementationTypes_11() { return static_cast<int32_t>(offsetof(CodeMemberProperty_t2419270920, ___implementationTypes_11)); }
	inline CodeTypeReferenceCollection_t3857551471 * get_implementationTypes_11() const { return ___implementationTypes_11; }
	inline CodeTypeReferenceCollection_t3857551471 ** get_address_of_implementationTypes_11() { return &___implementationTypes_11; }
	inline void set_implementationTypes_11(CodeTypeReferenceCollection_t3857551471 * value)
	{
		___implementationTypes_11 = value;
		Il2CppCodeGenWriteBarrier(&___implementationTypes_11, value);
	}

	inline static int32_t get_offset_of_parameters_12() { return static_cast<int32_t>(offsetof(CodeMemberProperty_t2419270920, ___parameters_12)); }
	inline CodeParameterDeclarationExpressionCollection_t3391789433 * get_parameters_12() const { return ___parameters_12; }
	inline CodeParameterDeclarationExpressionCollection_t3391789433 ** get_address_of_parameters_12() { return &___parameters_12; }
	inline void set_parameters_12(CodeParameterDeclarationExpressionCollection_t3391789433 * value)
	{
		___parameters_12 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_12, value);
	}

	inline static int32_t get_offset_of_privateImplementationType_13() { return static_cast<int32_t>(offsetof(CodeMemberProperty_t2419270920, ___privateImplementationType_13)); }
	inline CodeTypeReference_t3809997434 * get_privateImplementationType_13() const { return ___privateImplementationType_13; }
	inline CodeTypeReference_t3809997434 ** get_address_of_privateImplementationType_13() { return &___privateImplementationType_13; }
	inline void set_privateImplementationType_13(CodeTypeReference_t3809997434 * value)
	{
		___privateImplementationType_13 = value;
		Il2CppCodeGenWriteBarrier(&___privateImplementationType_13, value);
	}

	inline static int32_t get_offset_of_setStatements_14() { return static_cast<int32_t>(offsetof(CodeMemberProperty_t2419270920, ___setStatements_14)); }
	inline CodeStatementCollection_t1265263501 * get_setStatements_14() const { return ___setStatements_14; }
	inline CodeStatementCollection_t1265263501 ** get_address_of_setStatements_14() { return &___setStatements_14; }
	inline void set_setStatements_14(CodeStatementCollection_t1265263501 * value)
	{
		___setStatements_14 = value;
		Il2CppCodeGenWriteBarrier(&___setStatements_14, value);
	}

	inline static int32_t get_offset_of_type_15() { return static_cast<int32_t>(offsetof(CodeMemberProperty_t2419270920, ___type_15)); }
	inline CodeTypeReference_t3809997434 * get_type_15() const { return ___type_15; }
	inline CodeTypeReference_t3809997434 ** get_address_of_type_15() { return &___type_15; }
	inline void set_type_15(CodeTypeReference_t3809997434 * value)
	{
		___type_15 = value;
		Il2CppCodeGenWriteBarrier(&___type_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
