﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Xml.XmlDictionaryReaderQuotas
struct XmlDictionaryReaderQuotas_t173030297;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDictionaryReaderQuotas
struct  XmlDictionaryReaderQuotas_t173030297  : public Il2CppObject
{
public:
	// System.Boolean System.Xml.XmlDictionaryReaderQuotas::is_readonly
	bool ___is_readonly_1;
	// System.Int32 System.Xml.XmlDictionaryReaderQuotas::array_len
	int32_t ___array_len_2;
	// System.Int32 System.Xml.XmlDictionaryReaderQuotas::bytes
	int32_t ___bytes_3;
	// System.Int32 System.Xml.XmlDictionaryReaderQuotas::depth
	int32_t ___depth_4;
	// System.Int32 System.Xml.XmlDictionaryReaderQuotas::nt_chars
	int32_t ___nt_chars_5;
	// System.Int32 System.Xml.XmlDictionaryReaderQuotas::text_len
	int32_t ___text_len_6;

public:
	inline static int32_t get_offset_of_is_readonly_1() { return static_cast<int32_t>(offsetof(XmlDictionaryReaderQuotas_t173030297, ___is_readonly_1)); }
	inline bool get_is_readonly_1() const { return ___is_readonly_1; }
	inline bool* get_address_of_is_readonly_1() { return &___is_readonly_1; }
	inline void set_is_readonly_1(bool value)
	{
		___is_readonly_1 = value;
	}

	inline static int32_t get_offset_of_array_len_2() { return static_cast<int32_t>(offsetof(XmlDictionaryReaderQuotas_t173030297, ___array_len_2)); }
	inline int32_t get_array_len_2() const { return ___array_len_2; }
	inline int32_t* get_address_of_array_len_2() { return &___array_len_2; }
	inline void set_array_len_2(int32_t value)
	{
		___array_len_2 = value;
	}

	inline static int32_t get_offset_of_bytes_3() { return static_cast<int32_t>(offsetof(XmlDictionaryReaderQuotas_t173030297, ___bytes_3)); }
	inline int32_t get_bytes_3() const { return ___bytes_3; }
	inline int32_t* get_address_of_bytes_3() { return &___bytes_3; }
	inline void set_bytes_3(int32_t value)
	{
		___bytes_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(XmlDictionaryReaderQuotas_t173030297, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_nt_chars_5() { return static_cast<int32_t>(offsetof(XmlDictionaryReaderQuotas_t173030297, ___nt_chars_5)); }
	inline int32_t get_nt_chars_5() const { return ___nt_chars_5; }
	inline int32_t* get_address_of_nt_chars_5() { return &___nt_chars_5; }
	inline void set_nt_chars_5(int32_t value)
	{
		___nt_chars_5 = value;
	}

	inline static int32_t get_offset_of_text_len_6() { return static_cast<int32_t>(offsetof(XmlDictionaryReaderQuotas_t173030297, ___text_len_6)); }
	inline int32_t get_text_len_6() const { return ___text_len_6; }
	inline int32_t* get_address_of_text_len_6() { return &___text_len_6; }
	inline void set_text_len_6(int32_t value)
	{
		___text_len_6 = value;
	}
};

struct XmlDictionaryReaderQuotas_t173030297_StaticFields
{
public:
	// System.Xml.XmlDictionaryReaderQuotas System.Xml.XmlDictionaryReaderQuotas::max
	XmlDictionaryReaderQuotas_t173030297 * ___max_0;

public:
	inline static int32_t get_offset_of_max_0() { return static_cast<int32_t>(offsetof(XmlDictionaryReaderQuotas_t173030297_StaticFields, ___max_0)); }
	inline XmlDictionaryReaderQuotas_t173030297 * get_max_0() const { return ___max_0; }
	inline XmlDictionaryReaderQuotas_t173030297 ** get_address_of_max_0() { return &___max_0; }
	inline void set_max_0(XmlDictionaryReaderQuotas_t173030297 * value)
	{
		___max_0 = value;
		Il2CppCodeGenWriteBarrier(&___max_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
