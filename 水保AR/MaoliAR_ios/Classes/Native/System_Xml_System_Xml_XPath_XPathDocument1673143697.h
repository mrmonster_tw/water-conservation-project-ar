﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Xml.XPath.IXPathNavigable
struct IXPathNavigable_t2716156786;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathDocument
struct  XPathDocument_t1673143697  : public Il2CppObject
{
public:
	// System.Xml.XPath.IXPathNavigable System.Xml.XPath.XPathDocument::document
	Il2CppObject * ___document_0;

public:
	inline static int32_t get_offset_of_document_0() { return static_cast<int32_t>(offsetof(XPathDocument_t1673143697, ___document_0)); }
	inline Il2CppObject * get_document_0() const { return ___document_0; }
	inline Il2CppObject ** get_address_of_document_0() { return &___document_0; }
	inline void set_document_0(Il2CppObject * value)
	{
		___document_0 = value;
		Il2CppCodeGenWriteBarrier(&___document_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
