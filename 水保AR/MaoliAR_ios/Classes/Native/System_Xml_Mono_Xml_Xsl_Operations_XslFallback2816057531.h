﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Operations_XslCompiledElemen50593777.h"

// Mono.Xml.Xsl.Operations.XslOperation
struct XslOperation_t2153241355;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslFallback
struct  XslFallback_t2816057531  : public XslCompiledElement_t50593777
{
public:
	// Mono.Xml.Xsl.Operations.XslOperation Mono.Xml.Xsl.Operations.XslFallback::children
	XslOperation_t2153241355 * ___children_3;

public:
	inline static int32_t get_offset_of_children_3() { return static_cast<int32_t>(offsetof(XslFallback_t2816057531, ___children_3)); }
	inline XslOperation_t2153241355 * get_children_3() const { return ___children_3; }
	inline XslOperation_t2153241355 ** get_address_of_children_3() { return &___children_3; }
	inline void set_children_3(XslOperation_t2153241355 * value)
	{
		___children_3 = value;
		Il2CppCodeGenWriteBarrier(&___children_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
