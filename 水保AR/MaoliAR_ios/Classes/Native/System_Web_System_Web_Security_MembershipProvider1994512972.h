﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Provider2594774949.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Security.MembershipProvider
struct  MembershipProvider_t1994512972  : public ProviderBase_t2594774949
{
public:

public:
};

struct MembershipProvider_t1994512972_StaticFields
{
public:
	// System.Object System.Web.Security.MembershipProvider::validatingPasswordEvent
	Il2CppObject * ___validatingPasswordEvent_3;

public:
	inline static int32_t get_offset_of_validatingPasswordEvent_3() { return static_cast<int32_t>(offsetof(MembershipProvider_t1994512972_StaticFields, ___validatingPasswordEvent_3)); }
	inline Il2CppObject * get_validatingPasswordEvent_3() const { return ___validatingPasswordEvent_3; }
	inline Il2CppObject ** get_address_of_validatingPasswordEvent_3() { return &___validatingPasswordEvent_3; }
	inline void set_validatingPasswordEvent_3(Il2CppObject * value)
	{
		___validatingPasswordEvent_3 = value;
		Il2CppCodeGenWriteBarrier(&___validatingPasswordEvent_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
