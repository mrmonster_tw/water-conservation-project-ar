﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// BetterList`1<UIDrawCall>
struct BetterList_1_t448425637;
// UIPanel
struct UIPanel_t1716472341;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// BetterList`1<UnityEngine.Vector3>
struct BetterList_1_t2877333782;
// BetterList`1<UnityEngine.Vector4>
struct BetterList_1_t2474049255;
// BetterList`1<UnityEngine.Vector2>
struct BetterList_1_t1311249841;
// BetterList`1<UnityEngine.Color32>
struct BetterList_1_t1755521610;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.MeshFilter
struct MeshFilter_t3523625662;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// UIDrawCall/OnRenderCallback
struct OnRenderCallback_t133425655;
// System.Collections.Generic.List`1<System.Int32[]>
struct List_1_t1857321114;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDrawCall
struct  UIDrawCall_t1293405319  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UIDrawCall::widgetCount
	int32_t ___widgetCount_4;
	// System.Int32 UIDrawCall::depthStart
	int32_t ___depthStart_5;
	// System.Int32 UIDrawCall::depthEnd
	int32_t ___depthEnd_6;
	// UIPanel UIDrawCall::manager
	UIPanel_t1716472341 * ___manager_7;
	// UIPanel UIDrawCall::panel
	UIPanel_t1716472341 * ___panel_8;
	// UnityEngine.Texture2D UIDrawCall::clipTexture
	Texture2D_t3840446185 * ___clipTexture_9;
	// System.Boolean UIDrawCall::alwaysOnScreen
	bool ___alwaysOnScreen_10;
	// BetterList`1<UnityEngine.Vector3> UIDrawCall::verts
	BetterList_1_t2877333782 * ___verts_11;
	// BetterList`1<UnityEngine.Vector3> UIDrawCall::norms
	BetterList_1_t2877333782 * ___norms_12;
	// BetterList`1<UnityEngine.Vector4> UIDrawCall::tans
	BetterList_1_t2474049255 * ___tans_13;
	// BetterList`1<UnityEngine.Vector2> UIDrawCall::uvs
	BetterList_1_t1311249841 * ___uvs_14;
	// BetterList`1<UnityEngine.Color32> UIDrawCall::cols
	BetterList_1_t1755521610 * ___cols_15;
	// UnityEngine.Material UIDrawCall::mMaterial
	Material_t340375123 * ___mMaterial_16;
	// UnityEngine.Texture UIDrawCall::mTexture
	Texture_t3661962703 * ___mTexture_17;
	// UnityEngine.Shader UIDrawCall::mShader
	Shader_t4151988712 * ___mShader_18;
	// System.Int32 UIDrawCall::mClipCount
	int32_t ___mClipCount_19;
	// UnityEngine.Transform UIDrawCall::mTrans
	Transform_t3600365921 * ___mTrans_20;
	// UnityEngine.Mesh UIDrawCall::mMesh
	Mesh_t3648964284 * ___mMesh_21;
	// UnityEngine.MeshFilter UIDrawCall::mFilter
	MeshFilter_t3523625662 * ___mFilter_22;
	// UnityEngine.MeshRenderer UIDrawCall::mRenderer
	MeshRenderer_t587009260 * ___mRenderer_23;
	// UnityEngine.Material UIDrawCall::mDynamicMat
	Material_t340375123 * ___mDynamicMat_24;
	// System.Int32[] UIDrawCall::mIndices
	Int32U5BU5D_t385246372* ___mIndices_25;
	// System.Boolean UIDrawCall::mRebuildMat
	bool ___mRebuildMat_26;
	// System.Boolean UIDrawCall::mLegacyShader
	bool ___mLegacyShader_27;
	// System.Int32 UIDrawCall::mRenderQueue
	int32_t ___mRenderQueue_28;
	// System.Int32 UIDrawCall::mTriangles
	int32_t ___mTriangles_29;
	// System.Boolean UIDrawCall::isDirty
	bool ___isDirty_30;
	// System.Boolean UIDrawCall::mTextureClip
	bool ___mTextureClip_31;
	// UIDrawCall/OnRenderCallback UIDrawCall::onRender
	OnRenderCallback_t133425655 * ___onRender_32;

public:
	inline static int32_t get_offset_of_widgetCount_4() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___widgetCount_4)); }
	inline int32_t get_widgetCount_4() const { return ___widgetCount_4; }
	inline int32_t* get_address_of_widgetCount_4() { return &___widgetCount_4; }
	inline void set_widgetCount_4(int32_t value)
	{
		___widgetCount_4 = value;
	}

	inline static int32_t get_offset_of_depthStart_5() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___depthStart_5)); }
	inline int32_t get_depthStart_5() const { return ___depthStart_5; }
	inline int32_t* get_address_of_depthStart_5() { return &___depthStart_5; }
	inline void set_depthStart_5(int32_t value)
	{
		___depthStart_5 = value;
	}

	inline static int32_t get_offset_of_depthEnd_6() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___depthEnd_6)); }
	inline int32_t get_depthEnd_6() const { return ___depthEnd_6; }
	inline int32_t* get_address_of_depthEnd_6() { return &___depthEnd_6; }
	inline void set_depthEnd_6(int32_t value)
	{
		___depthEnd_6 = value;
	}

	inline static int32_t get_offset_of_manager_7() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___manager_7)); }
	inline UIPanel_t1716472341 * get_manager_7() const { return ___manager_7; }
	inline UIPanel_t1716472341 ** get_address_of_manager_7() { return &___manager_7; }
	inline void set_manager_7(UIPanel_t1716472341 * value)
	{
		___manager_7 = value;
		Il2CppCodeGenWriteBarrier(&___manager_7, value);
	}

	inline static int32_t get_offset_of_panel_8() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___panel_8)); }
	inline UIPanel_t1716472341 * get_panel_8() const { return ___panel_8; }
	inline UIPanel_t1716472341 ** get_address_of_panel_8() { return &___panel_8; }
	inline void set_panel_8(UIPanel_t1716472341 * value)
	{
		___panel_8 = value;
		Il2CppCodeGenWriteBarrier(&___panel_8, value);
	}

	inline static int32_t get_offset_of_clipTexture_9() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___clipTexture_9)); }
	inline Texture2D_t3840446185 * get_clipTexture_9() const { return ___clipTexture_9; }
	inline Texture2D_t3840446185 ** get_address_of_clipTexture_9() { return &___clipTexture_9; }
	inline void set_clipTexture_9(Texture2D_t3840446185 * value)
	{
		___clipTexture_9 = value;
		Il2CppCodeGenWriteBarrier(&___clipTexture_9, value);
	}

	inline static int32_t get_offset_of_alwaysOnScreen_10() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___alwaysOnScreen_10)); }
	inline bool get_alwaysOnScreen_10() const { return ___alwaysOnScreen_10; }
	inline bool* get_address_of_alwaysOnScreen_10() { return &___alwaysOnScreen_10; }
	inline void set_alwaysOnScreen_10(bool value)
	{
		___alwaysOnScreen_10 = value;
	}

	inline static int32_t get_offset_of_verts_11() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___verts_11)); }
	inline BetterList_1_t2877333782 * get_verts_11() const { return ___verts_11; }
	inline BetterList_1_t2877333782 ** get_address_of_verts_11() { return &___verts_11; }
	inline void set_verts_11(BetterList_1_t2877333782 * value)
	{
		___verts_11 = value;
		Il2CppCodeGenWriteBarrier(&___verts_11, value);
	}

	inline static int32_t get_offset_of_norms_12() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___norms_12)); }
	inline BetterList_1_t2877333782 * get_norms_12() const { return ___norms_12; }
	inline BetterList_1_t2877333782 ** get_address_of_norms_12() { return &___norms_12; }
	inline void set_norms_12(BetterList_1_t2877333782 * value)
	{
		___norms_12 = value;
		Il2CppCodeGenWriteBarrier(&___norms_12, value);
	}

	inline static int32_t get_offset_of_tans_13() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___tans_13)); }
	inline BetterList_1_t2474049255 * get_tans_13() const { return ___tans_13; }
	inline BetterList_1_t2474049255 ** get_address_of_tans_13() { return &___tans_13; }
	inline void set_tans_13(BetterList_1_t2474049255 * value)
	{
		___tans_13 = value;
		Il2CppCodeGenWriteBarrier(&___tans_13, value);
	}

	inline static int32_t get_offset_of_uvs_14() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___uvs_14)); }
	inline BetterList_1_t1311249841 * get_uvs_14() const { return ___uvs_14; }
	inline BetterList_1_t1311249841 ** get_address_of_uvs_14() { return &___uvs_14; }
	inline void set_uvs_14(BetterList_1_t1311249841 * value)
	{
		___uvs_14 = value;
		Il2CppCodeGenWriteBarrier(&___uvs_14, value);
	}

	inline static int32_t get_offset_of_cols_15() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___cols_15)); }
	inline BetterList_1_t1755521610 * get_cols_15() const { return ___cols_15; }
	inline BetterList_1_t1755521610 ** get_address_of_cols_15() { return &___cols_15; }
	inline void set_cols_15(BetterList_1_t1755521610 * value)
	{
		___cols_15 = value;
		Il2CppCodeGenWriteBarrier(&___cols_15, value);
	}

	inline static int32_t get_offset_of_mMaterial_16() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mMaterial_16)); }
	inline Material_t340375123 * get_mMaterial_16() const { return ___mMaterial_16; }
	inline Material_t340375123 ** get_address_of_mMaterial_16() { return &___mMaterial_16; }
	inline void set_mMaterial_16(Material_t340375123 * value)
	{
		___mMaterial_16 = value;
		Il2CppCodeGenWriteBarrier(&___mMaterial_16, value);
	}

	inline static int32_t get_offset_of_mTexture_17() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mTexture_17)); }
	inline Texture_t3661962703 * get_mTexture_17() const { return ___mTexture_17; }
	inline Texture_t3661962703 ** get_address_of_mTexture_17() { return &___mTexture_17; }
	inline void set_mTexture_17(Texture_t3661962703 * value)
	{
		___mTexture_17 = value;
		Il2CppCodeGenWriteBarrier(&___mTexture_17, value);
	}

	inline static int32_t get_offset_of_mShader_18() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mShader_18)); }
	inline Shader_t4151988712 * get_mShader_18() const { return ___mShader_18; }
	inline Shader_t4151988712 ** get_address_of_mShader_18() { return &___mShader_18; }
	inline void set_mShader_18(Shader_t4151988712 * value)
	{
		___mShader_18 = value;
		Il2CppCodeGenWriteBarrier(&___mShader_18, value);
	}

	inline static int32_t get_offset_of_mClipCount_19() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mClipCount_19)); }
	inline int32_t get_mClipCount_19() const { return ___mClipCount_19; }
	inline int32_t* get_address_of_mClipCount_19() { return &___mClipCount_19; }
	inline void set_mClipCount_19(int32_t value)
	{
		___mClipCount_19 = value;
	}

	inline static int32_t get_offset_of_mTrans_20() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mTrans_20)); }
	inline Transform_t3600365921 * get_mTrans_20() const { return ___mTrans_20; }
	inline Transform_t3600365921 ** get_address_of_mTrans_20() { return &___mTrans_20; }
	inline void set_mTrans_20(Transform_t3600365921 * value)
	{
		___mTrans_20 = value;
		Il2CppCodeGenWriteBarrier(&___mTrans_20, value);
	}

	inline static int32_t get_offset_of_mMesh_21() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mMesh_21)); }
	inline Mesh_t3648964284 * get_mMesh_21() const { return ___mMesh_21; }
	inline Mesh_t3648964284 ** get_address_of_mMesh_21() { return &___mMesh_21; }
	inline void set_mMesh_21(Mesh_t3648964284 * value)
	{
		___mMesh_21 = value;
		Il2CppCodeGenWriteBarrier(&___mMesh_21, value);
	}

	inline static int32_t get_offset_of_mFilter_22() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mFilter_22)); }
	inline MeshFilter_t3523625662 * get_mFilter_22() const { return ___mFilter_22; }
	inline MeshFilter_t3523625662 ** get_address_of_mFilter_22() { return &___mFilter_22; }
	inline void set_mFilter_22(MeshFilter_t3523625662 * value)
	{
		___mFilter_22 = value;
		Il2CppCodeGenWriteBarrier(&___mFilter_22, value);
	}

	inline static int32_t get_offset_of_mRenderer_23() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mRenderer_23)); }
	inline MeshRenderer_t587009260 * get_mRenderer_23() const { return ___mRenderer_23; }
	inline MeshRenderer_t587009260 ** get_address_of_mRenderer_23() { return &___mRenderer_23; }
	inline void set_mRenderer_23(MeshRenderer_t587009260 * value)
	{
		___mRenderer_23 = value;
		Il2CppCodeGenWriteBarrier(&___mRenderer_23, value);
	}

	inline static int32_t get_offset_of_mDynamicMat_24() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mDynamicMat_24)); }
	inline Material_t340375123 * get_mDynamicMat_24() const { return ___mDynamicMat_24; }
	inline Material_t340375123 ** get_address_of_mDynamicMat_24() { return &___mDynamicMat_24; }
	inline void set_mDynamicMat_24(Material_t340375123 * value)
	{
		___mDynamicMat_24 = value;
		Il2CppCodeGenWriteBarrier(&___mDynamicMat_24, value);
	}

	inline static int32_t get_offset_of_mIndices_25() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mIndices_25)); }
	inline Int32U5BU5D_t385246372* get_mIndices_25() const { return ___mIndices_25; }
	inline Int32U5BU5D_t385246372** get_address_of_mIndices_25() { return &___mIndices_25; }
	inline void set_mIndices_25(Int32U5BU5D_t385246372* value)
	{
		___mIndices_25 = value;
		Il2CppCodeGenWriteBarrier(&___mIndices_25, value);
	}

	inline static int32_t get_offset_of_mRebuildMat_26() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mRebuildMat_26)); }
	inline bool get_mRebuildMat_26() const { return ___mRebuildMat_26; }
	inline bool* get_address_of_mRebuildMat_26() { return &___mRebuildMat_26; }
	inline void set_mRebuildMat_26(bool value)
	{
		___mRebuildMat_26 = value;
	}

	inline static int32_t get_offset_of_mLegacyShader_27() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mLegacyShader_27)); }
	inline bool get_mLegacyShader_27() const { return ___mLegacyShader_27; }
	inline bool* get_address_of_mLegacyShader_27() { return &___mLegacyShader_27; }
	inline void set_mLegacyShader_27(bool value)
	{
		___mLegacyShader_27 = value;
	}

	inline static int32_t get_offset_of_mRenderQueue_28() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mRenderQueue_28)); }
	inline int32_t get_mRenderQueue_28() const { return ___mRenderQueue_28; }
	inline int32_t* get_address_of_mRenderQueue_28() { return &___mRenderQueue_28; }
	inline void set_mRenderQueue_28(int32_t value)
	{
		___mRenderQueue_28 = value;
	}

	inline static int32_t get_offset_of_mTriangles_29() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mTriangles_29)); }
	inline int32_t get_mTriangles_29() const { return ___mTriangles_29; }
	inline int32_t* get_address_of_mTriangles_29() { return &___mTriangles_29; }
	inline void set_mTriangles_29(int32_t value)
	{
		___mTriangles_29 = value;
	}

	inline static int32_t get_offset_of_isDirty_30() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___isDirty_30)); }
	inline bool get_isDirty_30() const { return ___isDirty_30; }
	inline bool* get_address_of_isDirty_30() { return &___isDirty_30; }
	inline void set_isDirty_30(bool value)
	{
		___isDirty_30 = value;
	}

	inline static int32_t get_offset_of_mTextureClip_31() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___mTextureClip_31)); }
	inline bool get_mTextureClip_31() const { return ___mTextureClip_31; }
	inline bool* get_address_of_mTextureClip_31() { return &___mTextureClip_31; }
	inline void set_mTextureClip_31(bool value)
	{
		___mTextureClip_31 = value;
	}

	inline static int32_t get_offset_of_onRender_32() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319, ___onRender_32)); }
	inline OnRenderCallback_t133425655 * get_onRender_32() const { return ___onRender_32; }
	inline OnRenderCallback_t133425655 ** get_address_of_onRender_32() { return &___onRender_32; }
	inline void set_onRender_32(OnRenderCallback_t133425655 * value)
	{
		___onRender_32 = value;
		Il2CppCodeGenWriteBarrier(&___onRender_32, value);
	}
};

struct UIDrawCall_t1293405319_StaticFields
{
public:
	// BetterList`1<UIDrawCall> UIDrawCall::mActiveList
	BetterList_1_t448425637 * ___mActiveList_2;
	// BetterList`1<UIDrawCall> UIDrawCall::mInactiveList
	BetterList_1_t448425637 * ___mInactiveList_3;
	// System.Collections.Generic.List`1<System.Int32[]> UIDrawCall::mCache
	List_1_t1857321114 * ___mCache_34;
	// System.Int32[] UIDrawCall::ClipRange
	Int32U5BU5D_t385246372* ___ClipRange_35;
	// System.Int32[] UIDrawCall::ClipArgs
	Int32U5BU5D_t385246372* ___ClipArgs_36;

public:
	inline static int32_t get_offset_of_mActiveList_2() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319_StaticFields, ___mActiveList_2)); }
	inline BetterList_1_t448425637 * get_mActiveList_2() const { return ___mActiveList_2; }
	inline BetterList_1_t448425637 ** get_address_of_mActiveList_2() { return &___mActiveList_2; }
	inline void set_mActiveList_2(BetterList_1_t448425637 * value)
	{
		___mActiveList_2 = value;
		Il2CppCodeGenWriteBarrier(&___mActiveList_2, value);
	}

	inline static int32_t get_offset_of_mInactiveList_3() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319_StaticFields, ___mInactiveList_3)); }
	inline BetterList_1_t448425637 * get_mInactiveList_3() const { return ___mInactiveList_3; }
	inline BetterList_1_t448425637 ** get_address_of_mInactiveList_3() { return &___mInactiveList_3; }
	inline void set_mInactiveList_3(BetterList_1_t448425637 * value)
	{
		___mInactiveList_3 = value;
		Il2CppCodeGenWriteBarrier(&___mInactiveList_3, value);
	}

	inline static int32_t get_offset_of_mCache_34() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319_StaticFields, ___mCache_34)); }
	inline List_1_t1857321114 * get_mCache_34() const { return ___mCache_34; }
	inline List_1_t1857321114 ** get_address_of_mCache_34() { return &___mCache_34; }
	inline void set_mCache_34(List_1_t1857321114 * value)
	{
		___mCache_34 = value;
		Il2CppCodeGenWriteBarrier(&___mCache_34, value);
	}

	inline static int32_t get_offset_of_ClipRange_35() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319_StaticFields, ___ClipRange_35)); }
	inline Int32U5BU5D_t385246372* get_ClipRange_35() const { return ___ClipRange_35; }
	inline Int32U5BU5D_t385246372** get_address_of_ClipRange_35() { return &___ClipRange_35; }
	inline void set_ClipRange_35(Int32U5BU5D_t385246372* value)
	{
		___ClipRange_35 = value;
		Il2CppCodeGenWriteBarrier(&___ClipRange_35, value);
	}

	inline static int32_t get_offset_of_ClipArgs_36() { return static_cast<int32_t>(offsetof(UIDrawCall_t1293405319_StaticFields, ___ClipArgs_36)); }
	inline Int32U5BU5D_t385246372* get_ClipArgs_36() const { return ___ClipArgs_36; }
	inline Int32U5BU5D_t385246372** get_address_of_ClipArgs_36() { return &___ClipArgs_36; }
	inline void set_ClipArgs_36(Int32U5BU5D_t385246372* value)
	{
		___ClipArgs_36 = value;
		Il2CppCodeGenWriteBarrier(&___ClipArgs_36, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
