﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_TemplateBuilder3864847030.h"

// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Web.Compilation.AspComponentFoundry
struct AspComponentFoundry_t3087970636;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.RootBuilder
struct  RootBuilder_t1594119744  : public TemplateBuilder_t3864847030
{
public:
	// System.Web.Compilation.AspComponentFoundry System.Web.UI.RootBuilder::foundry
	AspComponentFoundry_t3087970636 * ___foundry_34;

public:
	inline static int32_t get_offset_of_foundry_34() { return static_cast<int32_t>(offsetof(RootBuilder_t1594119744, ___foundry_34)); }
	inline AspComponentFoundry_t3087970636 * get_foundry_34() const { return ___foundry_34; }
	inline AspComponentFoundry_t3087970636 ** get_address_of_foundry_34() { return &___foundry_34; }
	inline void set_foundry_34(AspComponentFoundry_t3087970636 * value)
	{
		___foundry_34 = value;
		Il2CppCodeGenWriteBarrier(&___foundry_34, value);
	}
};

struct RootBuilder_t1594119744_StaticFields
{
public:
	// System.Collections.Hashtable System.Web.UI.RootBuilder::htmlControls
	Hashtable_t1853889766 * ___htmlControls_32;
	// System.Collections.Hashtable System.Web.UI.RootBuilder::htmlInputControls
	Hashtable_t1853889766 * ___htmlInputControls_33;

public:
	inline static int32_t get_offset_of_htmlControls_32() { return static_cast<int32_t>(offsetof(RootBuilder_t1594119744_StaticFields, ___htmlControls_32)); }
	inline Hashtable_t1853889766 * get_htmlControls_32() const { return ___htmlControls_32; }
	inline Hashtable_t1853889766 ** get_address_of_htmlControls_32() { return &___htmlControls_32; }
	inline void set_htmlControls_32(Hashtable_t1853889766 * value)
	{
		___htmlControls_32 = value;
		Il2CppCodeGenWriteBarrier(&___htmlControls_32, value);
	}

	inline static int32_t get_offset_of_htmlInputControls_33() { return static_cast<int32_t>(offsetof(RootBuilder_t1594119744_StaticFields, ___htmlInputControls_33)); }
	inline Hashtable_t1853889766 * get_htmlInputControls_33() const { return ___htmlInputControls_33; }
	inline Hashtable_t1853889766 ** get_address_of_htmlInputControls_33() { return &___htmlInputControls_33; }
	inline void set_htmlInputControls_33(Hashtable_t1853889766 * value)
	{
		___htmlInputControls_33 = value;
		Il2CppCodeGenWriteBarrier(&___htmlInputControls_33, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
