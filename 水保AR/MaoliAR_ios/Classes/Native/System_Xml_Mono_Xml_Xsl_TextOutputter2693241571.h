﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Outputter889105973.h"

// System.IO.TextWriter
struct TextWriter_t3478189236;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.TextOutputter
struct  TextOutputter_t2693241571  : public Outputter_t889105973
{
public:
	// System.IO.TextWriter Mono.Xml.Xsl.TextOutputter::_writer
	TextWriter_t3478189236 * ____writer_0;
	// System.Int32 Mono.Xml.Xsl.TextOutputter::_depth
	int32_t ____depth_1;
	// System.Boolean Mono.Xml.Xsl.TextOutputter::_ignoreNestedText
	bool ____ignoreNestedText_2;

public:
	inline static int32_t get_offset_of__writer_0() { return static_cast<int32_t>(offsetof(TextOutputter_t2693241571, ____writer_0)); }
	inline TextWriter_t3478189236 * get__writer_0() const { return ____writer_0; }
	inline TextWriter_t3478189236 ** get_address_of__writer_0() { return &____writer_0; }
	inline void set__writer_0(TextWriter_t3478189236 * value)
	{
		____writer_0 = value;
		Il2CppCodeGenWriteBarrier(&____writer_0, value);
	}

	inline static int32_t get_offset_of__depth_1() { return static_cast<int32_t>(offsetof(TextOutputter_t2693241571, ____depth_1)); }
	inline int32_t get__depth_1() const { return ____depth_1; }
	inline int32_t* get_address_of__depth_1() { return &____depth_1; }
	inline void set__depth_1(int32_t value)
	{
		____depth_1 = value;
	}

	inline static int32_t get_offset_of__ignoreNestedText_2() { return static_cast<int32_t>(offsetof(TextOutputter_t2693241571, ____ignoreNestedText_2)); }
	inline bool get__ignoreNestedText_2() const { return ____ignoreNestedText_2; }
	inline bool* get_address_of__ignoreNestedText_2() { return &____ignoreNestedText_2; }
	inline void set__ignoreNestedText_2(bool value)
	{
		____ignoreNestedText_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
