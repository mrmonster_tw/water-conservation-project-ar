﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Security_System_Security_Cryptography_Xml_T1105379765.h"

// System.Security.Cryptography.Xml.EncryptedXml
struct EncryptedXml_t2455217639;
// System.Object
struct Il2CppObject;
// System.Collections.ArrayList
struct ArrayList_t2718874744;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.Xml.XmlDecryptionTransform
struct  XmlDecryptionTransform_t4000891284  : public Transform_t1105379765
{
public:
	// System.Security.Cryptography.Xml.EncryptedXml System.Security.Cryptography.Xml.XmlDecryptionTransform::encryptedXml
	EncryptedXml_t2455217639 * ___encryptedXml_3;
	// System.Object System.Security.Cryptography.Xml.XmlDecryptionTransform::inputObj
	Il2CppObject * ___inputObj_4;
	// System.Collections.ArrayList System.Security.Cryptography.Xml.XmlDecryptionTransform::exceptUris
	ArrayList_t2718874744 * ___exceptUris_5;

public:
	inline static int32_t get_offset_of_encryptedXml_3() { return static_cast<int32_t>(offsetof(XmlDecryptionTransform_t4000891284, ___encryptedXml_3)); }
	inline EncryptedXml_t2455217639 * get_encryptedXml_3() const { return ___encryptedXml_3; }
	inline EncryptedXml_t2455217639 ** get_address_of_encryptedXml_3() { return &___encryptedXml_3; }
	inline void set_encryptedXml_3(EncryptedXml_t2455217639 * value)
	{
		___encryptedXml_3 = value;
		Il2CppCodeGenWriteBarrier(&___encryptedXml_3, value);
	}

	inline static int32_t get_offset_of_inputObj_4() { return static_cast<int32_t>(offsetof(XmlDecryptionTransform_t4000891284, ___inputObj_4)); }
	inline Il2CppObject * get_inputObj_4() const { return ___inputObj_4; }
	inline Il2CppObject ** get_address_of_inputObj_4() { return &___inputObj_4; }
	inline void set_inputObj_4(Il2CppObject * value)
	{
		___inputObj_4 = value;
		Il2CppCodeGenWriteBarrier(&___inputObj_4, value);
	}

	inline static int32_t get_offset_of_exceptUris_5() { return static_cast<int32_t>(offsetof(XmlDecryptionTransform_t4000891284, ___exceptUris_5)); }
	inline ArrayList_t2718874744 * get_exceptUris_5() const { return ___exceptUris_5; }
	inline ArrayList_t2718874744 ** get_address_of_exceptUris_5() { return &___exceptUris_5; }
	inline void set_exceptUris_5(ArrayList_t2718874744 * value)
	{
		___exceptUris_5 = value;
		Il2CppCodeGenWriteBarrier(&___exceptUris_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
