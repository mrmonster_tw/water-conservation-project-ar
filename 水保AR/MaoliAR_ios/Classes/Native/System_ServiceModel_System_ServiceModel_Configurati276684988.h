﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.NetNamedPipeSecurityElement
struct  NetNamedPipeSecurityElement_t276684988  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct NetNamedPipeSecurityElement_t276684988_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.NetNamedPipeSecurityElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.NetNamedPipeSecurityElement::mode
	ConfigurationProperty_t3590861854 * ___mode_14;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.NetNamedPipeSecurityElement::transport
	ConfigurationProperty_t3590861854 * ___transport_15;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(NetNamedPipeSecurityElement_t276684988_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier(&___properties_13, value);
	}

	inline static int32_t get_offset_of_mode_14() { return static_cast<int32_t>(offsetof(NetNamedPipeSecurityElement_t276684988_StaticFields, ___mode_14)); }
	inline ConfigurationProperty_t3590861854 * get_mode_14() const { return ___mode_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_mode_14() { return &___mode_14; }
	inline void set_mode_14(ConfigurationProperty_t3590861854 * value)
	{
		___mode_14 = value;
		Il2CppCodeGenWriteBarrier(&___mode_14, value);
	}

	inline static int32_t get_offset_of_transport_15() { return static_cast<int32_t>(offsetof(NetNamedPipeSecurityElement_t276684988_StaticFields, ___transport_15)); }
	inline ConfigurationProperty_t3590861854 * get_transport_15() const { return ___transport_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_transport_15() { return &___transport_15; }
	inline void set_transport_15(ConfigurationProperty_t3590861854 * value)
	{
		___transport_15 = value;
		Il2CppCodeGenWriteBarrier(&___transport_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
