﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3549286319.h"
#include "System_Web_System_Web_UI_WebControls_Unit_ParsingS3610471096.h"
#include "System_Web_System_Web_UI_WebControls_UnitConverter1266597561.h"
#include "mscorlib_System_Type2483944760.h"
#include "mscorlib_System_Boolean97287965.h"
#include "mscorlib_System_RuntimeTypeHandle3027515415.h"
#include "System_System_ComponentModel_TypeConverter2249118273.h"
#include "mscorlib_System_Globalization_CultureInfo4157843068.h"
#include "mscorlib_System_Object3080106164.h"
#include "System_Web_System_Web_UI_WebControls_Unit3186727900.h"
#include "mscorlib_System_String1847450689.h"
#include "mscorlib_System_Void1185182177.h"
#include "System_Web_System_Web_UI_WebControls_UnitType3617768156.h"
#include "System_Web_System_Web_UI_WebControls_ValidatedCont1434589485.h"
#include "System_System_ComponentModel_TypeConverter_Standar2184948248.h"
#include "mscorlib_System_Collections_ArrayList2718874744.h"
#include "mscorlib_System_Int322950945753.h"
#include "System_System_ComponentModel_ComponentCollection1445440302.h"
#include "mscorlib_System_Collections_ReadOnlyCollectionBase1836743899.h"
#include "System_Web_System_Web_UI_Control3006474639.h"
#include "System_Web_System_Web_UI_WebControls_ControlIDConv1795750277.h"
#include "System_System_ComponentModel_PropertyDescriptor3244362832.h"
#include "System_Web_System_Web_UI_WebControls_ValidatorDisp3259174783.h"
#include "System_Web_System_Web_UI_WebControls_VerticalAlign2849038207.h"
#include "System_Web_System_Web_UI_WebControls_VerticalAlign1776218746.h"
#include "System_System_ComponentModel_EnumConverter1688858217.h"
#include "System_Web_System_Web_UI_WebControls_WebControl2705811671.h"
#include "System_Web_System_Web_UI_HtmlTextWriterTag4267392773.h"
#include "mscorlib_System_Char3634460470.h"
#include "System_Web_System_Web_UI_StateBag282928164.h"
#include "System_Web_System_Web_UI_AttributeCollection3488369622.h"
#include "System_Web_System_Web_UI_WebControls_Style3589053173.h"
#include "mscorlib_System_Int162552820387.h"
#include "System_Web_System_Web_UI_HtmlTextWriter1404937429.h"
#include "System_Web_System_Web_UI_WebControls_BorderStyle3334519566.h"
#include "System_Web_System_Web_UI_HtmlTextWriterStyle3329380222.h"
#include "System_Web_System_Web_UI_HtmlTextWriterAttribute2713090206.h"
#include "System_Web_System_Web_UI_Pair771337780.h"
#include "System_Web_System_Web_UI_Adapters_ControlAdapter3811171780.h"
#include "System_Web_System_Web_UI_WebResourceAttribute4132945045.h"
#include "mscorlib_System_Attribute861562559.h"
#include "System_Web_System_Web_UplevelHelper1754666578.h"
#include "System_Web_System_Web_Util_AltSerialization1561118416.h"
#include "mscorlib_System_IO_BinaryWriter3992595042.h"
#include "mscorlib_System_TypeCode2987224087.h"
#include "mscorlib_System_DateTime3738529785.h"
#include "mscorlib_System_IO_Stream1273022909.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3197753202.h"
#include "mscorlib_System_Byte1134296376.h"
#include "mscorlib_System_Int643736567304.h"
#include "mscorlib_System_Decimal2948259380.h"
#include "mscorlib_System_Double594665363.h"
#include "mscorlib_System_SByte1669577662.h"
#include "mscorlib_System_Single1397266774.h"
#include "mscorlib_System_UInt162177724958.h"
#include "mscorlib_System_UInt322560061978.h"
#include "mscorlib_System_UInt644134040092.h"
#include "mscorlib_System_IO_BinaryReader2428077293.h"
#include "mscorlib_System_DBNull3725197148.h"
#include "mscorlib_System_ArgumentOutOfRangeException777629997.h"
#include "System_Web_System_Web_Util_DataSourceResolver2150809228.h"
#include "System_System_ComponentModel_PropertyDescriptorCol4164928659.h"
#include "System_Web_System_Web_HttpException2907797370.h"
#include "System_Web_System_Web_Util_FileUtils3174566191.h"
#include "mscorlib_System_Random108471755.h"
#include "System_Web_System_Web_Util_FileUtils_CreateTempFile844781177.h"
#include "mscorlib_System_IO_IOException4088381929.h"
#include "System_Web_System_Web_Util_Helpers1105960530.h"
#include "mscorlib_System_IntPtr840150181.h"
#include "mscorlib_System_AsyncCallback3962456242.h"
#include "System_Web_System_Web_Util_ICalls4071149221.h"
#include "System_Web_System_Web_Util_SearchPattern3593873567.h"
#include "System_Web_System_Web_Util_SearchPattern_Op1428817098.h"
#include "System_Web_System_Web_Util_SearchPattern_OpCode642418941.h"
#include "mscorlib_System_ArgumentException132251570.h"
#include "System_Web_System_Web_Util_StrUtils435421409.h"
#include "mscorlib_System_Text_StringBuilder1712802186.h"
#include "mscorlib_System_StringSplitOptions641086070.h"
#include "System_Web_System_Web_Util_TimeUtil4122492887.h"
#include "System_Web_System_Web_Util_UrlUtils1214298665.h"
#include "mscorlib_System_ArgumentNullException1615371798.h"
#include "System_Web_System_Web_Util_WebEncoding373074341.h"
#include "System_Web_System_Web_Configuration_GlobalizationSe244366663.h"
#include "mscorlib_System_Text_Encoding1523322056.h"
#include "System_Web_System_Web_VirtualPath4270372584.h"
#include "System_Web_System_Web_HttpContext1969259010.h"
#include "System_Web_System_Web_HttpRequest809700260.h"
#include "System_Web_System_Web_VirtualPathUtility3131300229.h"
#include "System_Web_System_Web_Configuration_MonoSettingsSe2949572421.h"
#include "mscorlib_System_NullReferenceException1023182353.h"
#include "System_Web_System_Web_WebCategoryAttribute1186646113.h"
#include "System_System_ComponentModel_CategoryAttribute39585132.h"
#include "System_Web_System_Web_WebROCollection4065660147.h"
#include "mscorlib_System_StringComparer3301955079.h"
#include "System_System_Collections_Specialized_NameValueColl407452768.h"
#include "System_System_Collections_Specialized_NameObjectCo2091847364.h"
#include "System_Web_System_Web_WebSysDescriptionAttribute2907340736.h"
#include "System_System_ComponentModel_DescriptionAttribute874390736.h"

// System.Web.UI.WebControls.UnitConverter
struct UnitConverter_t1266597561;
// System.ComponentModel.ITypeDescriptorContext
struct ITypeDescriptorContext_t2998566513;
// System.Type
struct Type_t;
// System.ComponentModel.TypeConverter
struct TypeConverter_t2249118273;
// System.Object
struct Il2CppObject;
// System.Globalization.CultureInfo
struct CultureInfo_t4157843068;
// System.String
struct String_t;
// System.Web.UI.WebControls.ValidatedControlConverter
struct ValidatedControlConverter_t1434589485;
// System.ComponentModel.TypeConverter/StandardValuesCollection
struct StandardValuesCollection_t2184948248;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Collections.ICollection
struct ICollection_t3904884886;
// System.Web.UI.WebControls.ControlIDConverter
struct ControlIDConverter_t1795750277;
// System.Web.UI.Control
struct Control_t3006474639;
// System.ComponentModel.PropertyDescriptor
struct PropertyDescriptor_t3244362832;
// System.Web.UI.WebControls.VerticalAlignConverter
struct VerticalAlignConverter_t1776218746;
// System.ComponentModel.EnumConverter
struct EnumConverter_t1688858217;
// System.Web.UI.WebControls.WebControl
struct WebControl_t2705811671;
// System.Web.UI.StateBag
struct StateBag_t282928164;
// System.Web.UI.AttributeCollection
struct AttributeCollection_t3488369622;
// System.Web.UI.WebControls.Style
struct Style_t3589053173;
// System.Web.UI.HtmlTextWriter
struct HtmlTextWriter_t1404937429;
// System.Web.UI.Adapters.ControlAdapter
struct ControlAdapter_t3811171780;
// System.Web.UI.Pair
struct Pair_t771337780;
// System.Web.UI.WebResourceAttribute
struct WebResourceAttribute_t4132945045;
// System.Attribute
struct Attribute_t861562559;
// System.IO.BinaryWriter
struct BinaryWriter_t3992595042;
// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
struct BinaryFormatter_t3197753202;
// System.IO.Stream
struct Stream_t1273022909;
// System.IO.BinaryReader
struct BinaryReader_t2428077293;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_t777629997;
// System.Collections.IEnumerable
struct IEnumerable_t1941168011;
// System.ComponentModel.PropertyDescriptorCollection
struct PropertyDescriptorCollection_t4164928659;
// System.Web.HttpException
struct HttpException_t2907797370;
// System.Random
struct Random_t108471755;
// System.Web.Util.FileUtils/CreateTempFile
struct CreateTempFile_t844781177;
// System.IFormatProvider
struct IFormatProvider_t2518567562;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Web.Util.SearchPattern
struct SearchPattern_t3593873567;
// System.Web.Util.SearchPattern/Op
struct Op_t1428817098;
// System.ArgumentException
struct ArgumentException_t132251570;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Text.StringBuilder
struct StringBuilder_t1712802186;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.ArgumentNullException
struct ArgumentNullException_t1615371798;
// System.Web.Configuration.GlobalizationSection
struct GlobalizationSection_t244366663;
// System.Text.Encoding
struct Encoding_t1523322056;
// System.Web.VirtualPath
struct VirtualPath_t4270372584;
// System.Web.HttpContext
struct HttpContext_t1969259010;
// System.Web.HttpRequest
struct HttpRequest_t809700260;
// System.Web.Configuration.MonoSettingsSection
struct MonoSettingsSection_t2949572421;
// System.NullReferenceException
struct NullReferenceException_t1023182353;
// System.Web.WebCategoryAttribute
struct WebCategoryAttribute_t1186646113;
// System.ComponentModel.CategoryAttribute
struct CategoryAttribute_t39585132;
// System.Web.WebROCollection
struct WebROCollection_t4065660147;
// System.StringComparer
struct StringComparer_t3301955079;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t407452768;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t1493878338;
// System.Collections.Specialized.NameObjectCollectionBase
struct NameObjectCollectionBase_t2091847364;
// System.Web.WebSysDescriptionAttribute
struct WebSysDescriptionAttribute_t2907340736;
// System.ComponentModel.DescriptionAttribute
struct DescriptionAttribute_t874390736;
extern const Il2CppType* String_t_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t UnitConverter_CanConvertFrom_m3655322769_MetadataUsageId;
extern const uint32_t UnitConverter_CanConvertTo_m1908905322_MetadataUsageId;
extern Il2CppClass* Unit_t3186727900_il2cpp_TypeInfo_var;
extern const uint32_t UnitConverter_ConvertTo_m1831711620_MetadataUsageId;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t UnitConverter_ConvertFrom_m3418345965_MetadataUsageId;
extern Il2CppClass* ITypeDescriptorContext_t2998566513_il2cpp_TypeInfo_var;
extern Il2CppClass* IContainer_t1776039496_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t2718874744_il2cpp_TypeInfo_var;
extern Il2CppClass* Control_t3006474639_il2cpp_TypeInfo_var;
extern Il2CppClass* StandardValuesCollection_t2184948248_il2cpp_TypeInfo_var;
extern const uint32_t ValidatedControlConverter_GetStandardValues_m186965859_MetadataUsageId;
extern const uint32_t WebControl__ctor_m272488634_MetadataUsageId;
extern Il2CppClass* CharU5BU5D_t3528271667_il2cpp_TypeInfo_var;
extern Il2CppClass* WebControl_t2705811671_il2cpp_TypeInfo_var;
extern const uint32_t WebControl__cctor_m2934243859_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1579007546;
extern const uint32_t WebControl_get_AccessKey_m2497713227_MetadataUsageId;
extern Il2CppClass* StateBag_t282928164_il2cpp_TypeInfo_var;
extern Il2CppClass* AttributeCollection_t3488369622_il2cpp_TypeInfo_var;
extern const uint32_t WebControl_get_Attributes_m495154787_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1591843997;
extern const uint32_t WebControl_get_TabIndex_m2405075608_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1390806371;
extern const uint32_t WebControl_get_ToolTip_m2192069348_MetadataUsageId;
extern Il2CppClass* HtmlTextWriter_t1404937429_il2cpp_TypeInfo_var;
extern const uint32_t WebControl_get_TagName_m2672078180_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral437919229;
extern const uint32_t WebControl_AddDisplayStyleAttribute_m3292372057_MetadataUsageId;
extern Il2CppClass* IEnumerable_t1941168011_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1853284238_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t3640265483_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1262761478;
extern const uint32_t WebControl_AddAttributesToRender_m1714954365_MetadataUsageId;
extern Il2CppClass* Style_t3589053173_il2cpp_TypeInfo_var;
extern const uint32_t WebControl_CreateControlStyle_m4061402130_MetadataUsageId;
extern Il2CppClass* Pair_t771337780_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1657994044;
extern Il2CppCodeGenString* _stringLiteral646179308;
extern const uint32_t WebControl_LoadViewState_m2630056878_MetadataUsageId;
extern Il2CppClass* Boolean_t97287965_il2cpp_TypeInfo_var;
extern const uint32_t WebControl_SaveViewState_m3996518795_MetadataUsageId;
extern Il2CppClass* Byte_t1134296376_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t3634460470_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t3738529785_il2cpp_TypeInfo_var;
extern Il2CppClass* Decimal_t2948259380_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t594665363_il2cpp_TypeInfo_var;
extern Il2CppClass* Int16_t2552820387_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t3736567304_il2cpp_TypeInfo_var;
extern Il2CppClass* BinaryFormatter_t3197753202_il2cpp_TypeInfo_var;
extern Il2CppClass* SByte_t1669577662_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t1397266774_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt16_t2177724958_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t2560061978_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt64_t4134040092_il2cpp_TypeInfo_var;
extern const uint32_t AltSerialization_Serialize_m3935920895_MetadataUsageId;
extern Il2CppClass* DBNull_t3725197148_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeCode_t2987224087_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2677446575;
extern const uint32_t AltSerialization_Deserialize_m3371567354_MetadataUsageId;
extern Il2CppClass* IListSource_t3731153367_il2cpp_TypeInfo_var;
extern Il2CppClass* ITypedList_t3693238378_il2cpp_TypeInfo_var;
extern Il2CppClass* PropertyDescriptorU5BU5D_t2649761905_il2cpp_TypeInfo_var;
extern Il2CppClass* HttpException_t2907797370_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_t2094931216_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3454431373;
extern Il2CppCodeGenString* _stringLiteral3122392217;
extern const uint32_t DataSourceResolver_ResolveDataSource_m2304101392_MetadataUsageId;
extern Il2CppClass* Random_t108471755_il2cpp_TypeInfo_var;
extern Il2CppClass* FileUtils_t3174566191_il2cpp_TypeInfo_var;
extern const uint32_t FileUtils__cctor_m45768956_MetadataUsageId;
extern Il2CppClass* Helpers_t1105960530_il2cpp_TypeInfo_var;
extern Il2CppClass* Path_t1605229823_il2cpp_TypeInfo_var;
extern Il2CppClass* IOException_t4088381929_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2593158628;
extern Il2CppCodeGenString* _stringLiteral3452614530;
extern Il2CppCodeGenString* _stringLiteral3452614616;
extern const uint32_t FileUtils_CreateTemporaryFile_m904766428_MetadataUsageId;
extern Il2CppClass* CultureInfo_t4157843068_il2cpp_TypeInfo_var;
extern const uint32_t Helpers__cctor_m1437973413_MetadataUsageId;
extern Il2CppClass* SearchPattern_t3593873567_il2cpp_TypeInfo_var;
extern const uint32_t SearchPattern__cctor_m386626602_MetadataUsageId;
extern Il2CppClass* ArgumentException_t132251570_il2cpp_TypeInfo_var;
extern Il2CppClass* Op_t1428817098_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1422575839;
extern Il2CppCodeGenString* _stringLiteral3452614534;
extern const uint32_t SearchPattern_Compile_m1523637325_MetadataUsageId;
extern const uint32_t SearchPattern_Match_m327281186_MetadataUsageId;
extern const uint32_t StrUtils_StartsWith_m3039927907_MetadataUsageId;
extern const uint32_t StrUtils_EndsWith_m890124797_MetadataUsageId;
extern Il2CppClass* StringBuilder_t1712802186_il2cpp_TypeInfo_var;
extern const uint32_t StrUtils_EscapeQuotesAndBackslashes_m2514145343_MetadataUsageId;
extern const uint32_t StrUtils_IsNullOrEmpty_m2227577989_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4099524215;
extern Il2CppCodeGenString* _stringLiteral1130917823;
extern const uint32_t TimeUtil_ToUtcTimeString_m1557420589_MetadataUsageId;
extern Il2CppClass* UrlUtils_t1214298665_il2cpp_TypeInfo_var;
extern const uint32_t UrlUtils__cctor_m3701932390_MetadataUsageId;
extern Il2CppClass* HttpRuntime_t1480753741_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1281789340_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3452614529;
extern Il2CppCodeGenString* _stringLiteral3452614536;
extern Il2CppCodeGenString* _stringLiteral3450582919;
extern const uint32_t UrlUtils_InsertSessionId_m2191444238_MetadataUsageId;
extern const uint32_t UrlUtils_GetSessionId_m1101998480_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3451041665;
extern const uint32_t UrlUtils_HasSessionId_m364033583_MetadataUsageId;
extern const uint32_t UrlUtils_RemoveSessionId_m2523203920_MetadataUsageId;
extern Il2CppClass* ArgumentNullException_t1615371798_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3439590103;
extern const uint32_t UrlUtils_Combine_m1528663317_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3450648450;
extern Il2CppCodeGenString* _stringLiteral962976914;
extern const uint32_t UrlUtils_Canonic_m4232072973_MetadataUsageId;
extern const uint32_t UrlUtils_GetDirectory_m1298890870_MetadataUsageId;
extern const uint32_t UrlUtils_RemoveDoubleSlashes_m2124540071_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3395286453;
extern const uint32_t UrlUtils_GetFile_m1187780462_MetadataUsageId;
extern Il2CppClass* WebEncoding_t373074341_il2cpp_TypeInfo_var;
extern Il2CppClass* WebConfigurationManager_t2451561378_il2cpp_TypeInfo_var;
extern Il2CppClass* GlobalizationSection_t244366663_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral992186917;
extern const uint32_t WebEncoding_get_GlobalizationConfig_m2546015474_MetadataUsageId;
extern Il2CppClass* Encoding_t1523322056_il2cpp_TypeInfo_var;
extern const uint32_t WebEncoding_get_FileEncoding_m4274600026_MetadataUsageId;
extern const uint32_t WebEncoding_get_ResponseEncoding_m919385210_MetadataUsageId;
extern const uint32_t WebEncoding_get_RequestEncoding_m3410676394_MetadataUsageId;
extern Il2CppClass* VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1448479444;
extern Il2CppCodeGenString* _stringLiteral3450582994;
extern const uint32_t VirtualPath__ctor_m3863291854_MetadataUsageId;
extern const uint32_t VirtualPath_get_Absolute_m3947569336_MetadataUsageId;
extern const uint32_t VirtualPath_get_AppRelative_m2233295149_MetadataUsageId;
extern const uint32_t VirtualPath_get_Extension_m209795241_MetadataUsageId;
extern const uint32_t VirtualPath_get_Directory_m1071987848_MetadataUsageId;
extern const uint32_t VirtualPath_get_DirectoryNoNormalize_m2890043560_MetadataUsageId;
extern const uint32_t VirtualPath_get_CurrentRequestDirectory_m3125580679_MetadataUsageId;
extern const uint32_t VirtualPath_MakeRooted_m3007486533_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3822744503;
extern Il2CppCodeGenString* _stringLiteral3452614643;
extern const uint32_t VirtualPath_ToString_m1191822246_MetadataUsageId;
extern Il2CppClass* VirtualPath_t4270372584_il2cpp_TypeInfo_var;
extern const uint32_t VirtualPath_PhysicalToVirtual_m75598891_MetadataUsageId;
extern Il2CppClass* MonoSettingsSection_t2949572421_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral790887489;
extern Il2CppCodeGenString* _stringLiteral3955796716;
extern const uint32_t VirtualPathUtility__cctor_m2171500222_MetadataUsageId;
extern const uint32_t VirtualPathUtility_AppendTrailingSlash_m659506801_MetadataUsageId;
extern const uint32_t VirtualPathUtility_Combine_m2177271470_MetadataUsageId;
extern const uint32_t VirtualPathUtility_GetDirectory_m2076898960_MetadataUsageId;
extern const uint32_t VirtualPathUtility_GetDirectory_m404138870_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1685589679;
extern const uint32_t VirtualPathUtility_GetExtension_m3822181684_MetadataUsageId;
extern const uint32_t VirtualPathUtility_GetFileName_m60487828_MetadataUsageId;
extern const uint32_t VirtualPathUtility_IsRooted_m3278026923_MetadataUsageId;
extern const uint32_t VirtualPathUtility_IsAbsolute_m2473455663_MetadataUsageId;
extern const uint32_t VirtualPathUtility_IsAppRelative_m548692695_MetadataUsageId;
extern Il2CppClass* NullReferenceException_t1023182353_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3450582914;
extern Il2CppCodeGenString* _stringLiteral1057303601;
extern const uint32_t VirtualPathUtility_MakeRelative_m344618986_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1047870583;
extern const uint32_t VirtualPathUtility_ToAbsoluteInternal_m1993485465_MetadataUsageId;
extern const uint32_t VirtualPathUtility_RemoveTrailingSlash_m2660928406_MetadataUsageId;
extern const uint32_t VirtualPathUtility_ToAbsolute_m373284300_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3722472157;
extern const uint32_t VirtualPathUtility_ToAbsolute_m3451502579_MetadataUsageId;
extern const uint32_t VirtualPathUtility_ToAbsolute_m409990745_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2251586184;
extern Il2CppCodeGenString* _stringLiteral3683762703;
extern Il2CppCodeGenString* _stringLiteral893733960;
extern const uint32_t VirtualPathUtility_ToAbsolute_m1728694453_MetadataUsageId;
extern const uint32_t VirtualPathUtility_ToAppRelative_m769660906_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1064770897;
extern Il2CppCodeGenString* _stringLiteral3452614610;
extern const uint32_t VirtualPathUtility_ToAppRelative_m867676899_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3632685578;
extern Il2CppCodeGenString* _stringLiteral3347900047;
extern const uint32_t VirtualPathUtility_Normalize_m1039358837_MetadataUsageId;
extern const uint32_t VirtualPathUtility_Canonize_m2348126131_MetadataUsageId;
extern Il2CppClass* Registry_t2858193406_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1559207716;
extern const uint32_t VirtualPathUtility_IsValidVirtualPath_m146402699_MetadataUsageId;
extern Il2CppClass* CategoryAttribute_t39585132_il2cpp_TypeInfo_var;
extern const uint32_t WebCategoryAttribute__ctor_m3219041960_MetadataUsageId;
extern Il2CppClass* StringComparer_t3301955079_il2cpp_TypeInfo_var;
extern const uint32_t WebROCollection__ctor_m3496122412_MetadataUsageId;
extern const uint32_t WebROCollection_ToString_m903395350_MetadataUsageId;
extern Il2CppClass* DescriptionAttribute_t874390736_il2cpp_TypeInfo_var;
extern const uint32_t WebSysDescriptionAttribute__ctor_m1594100564_MetadataUsageId;

// System.Char[]
struct CharU5BU5D_t3528271667  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.ComponentModel.PropertyDescriptor[]
struct PropertyDescriptorU5BU5D_t2649761905  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PropertyDescriptor_t3244362832 * m_Items[1];

public:
	inline PropertyDescriptor_t3244362832 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline PropertyDescriptor_t3244362832 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, PropertyDescriptor_t3244362832 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline PropertyDescriptor_t3244362832 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline PropertyDescriptor_t3244362832 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, PropertyDescriptor_t3244362832 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.String[]
struct StringU5BU5D_t1281789340  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};



// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m1620074514 (Il2CppObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.TypeConverter::CanConvertFrom(System.ComponentModel.ITypeDescriptorContext,System.Type)
extern "C"  bool TypeConverter_CanConvertFrom_m1106272217 (TypeConverter_t2249118273 * __this, Il2CppObject * p0, Type_t * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.TypeConverter::CanConvertTo(System.ComponentModel.ITypeDescriptorContext,System.Type)
extern "C"  bool TypeConverter_CanConvertTo_m3338360407 (TypeConverter_t2249118273 * __this, Il2CppObject * p0, Type_t * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.UI.WebControls.Unit::ToString(System.Globalization.CultureInfo)
extern "C"  String_t* Unit_ToString_m1301839610 (Unit_t3186727900 * __this, CultureInfo_t4157843068 * ___culture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.ComponentModel.TypeConverter::ConvertTo(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object,System.Type)
extern "C"  Il2CppObject * TypeConverter_ConvertTo_m3165899902 (TypeConverter_t2249118273 * __this, Il2CppObject * p0, CultureInfo_t4157843068 * p1, Il2CppObject * p2, Type_t * p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m88164663 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.UI.WebControls.Unit::.ctor(System.String,System.Globalization.CultureInfo)
extern "C"  void Unit__ctor_m980678834 (Unit_t3186727900 * __this, String_t* ___value0, CultureInfo_t4157843068 * ___culture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.ComponentModel.TypeConverter::ConvertFrom(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object)
extern "C"  Il2CppObject * TypeConverter_ConvertFrom_m1024238132 (TypeConverter_t2249118273 * __this, Il2CppObject * p0, CultureInfo_t4157843068 * p1, Il2CppObject * p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::.ctor()
extern "C"  void ArrayList__ctor_m4254721275 (ArrayList_t2718874744 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::get_Length()
extern "C"  int32_t String_get_Length_m3847582255 (String_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.TypeConverter/StandardValuesCollection::.ctor(System.Collections.ICollection)
extern "C"  void StandardValuesCollection__ctor_m260150618 (StandardValuesCollection_t2184948248 * __this, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.TypeConverter/StandardValuesCollection System.Web.UI.WebControls.ControlIDConverter::GetStandardValues(System.ComponentModel.ITypeDescriptorContext)
extern "C"  StandardValuesCollection_t2184948248 * ControlIDConverter_GetStandardValues_m2173371064 (ControlIDConverter_t1795750277 * __this, Il2CppObject * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.PropertyDescriptor System.Web.UI.WebControls.BaseValidator::GetValidationProperty(System.Object)
extern "C"  PropertyDescriptor_t3244362832 * BaseValidator_GetValidationProperty_m1551790743 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.EnumConverter::CanConvertFrom(System.ComponentModel.ITypeDescriptorContext,System.Type)
extern "C"  bool EnumConverter_CanConvertFrom_m4081436354 (EnumConverter_t1688858217 * __this, Il2CppObject * p0, Type_t * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.ComponentModel.EnumConverter::ConvertFrom(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object)
extern "C"  Il2CppObject * EnumConverter_ConvertFrom_m1625779591 (EnumConverter_t1688858217 * __this, Il2CppObject * p0, CultureInfo_t4157843068 * p1, Il2CppObject * p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.EnumConverter::CanConvertTo(System.ComponentModel.ITypeDescriptorContext,System.Type)
extern "C"  bool EnumConverter_CanConvertTo_m1239367246 (EnumConverter_t1688858217 * __this, Il2CppObject * p0, Type_t * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.ComponentModel.EnumConverter::ConvertTo(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object,System.Type)
extern "C"  Il2CppObject * EnumConverter_ConvertTo_m1137837802 (EnumConverter_t1688858217 * __this, Il2CppObject * p0, CultureInfo_t4157843068 * p1, Il2CppObject * p2, Type_t * p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.UI.Control::.ctor()
extern "C"  void Control__ctor_m1794429822 (Control_t3006474639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.UI.StateBag::GetString(System.String,System.String)
extern "C"  String_t* StateBag_GetString_m2746466793 (StateBag_t282928164 * __this, String_t* ___key0, String_t* ___def1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.UI.StateBag::.ctor(System.Boolean)
extern "C"  void StateBag__ctor_m2018118170 (StateBag_t282928164 * __this, bool ___ignoreCase0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Web.UI.Control::get_IsTrackingViewState()
extern "C"  bool Control_get_IsTrackingViewState_m2001955483 (Control_t3006474639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.UI.StateBag::TrackViewState()
extern "C"  void StateBag_TrackViewState_m3806295084 (StateBag_t282928164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.UI.AttributeCollection::.ctor(System.Web.UI.StateBag)
extern "C"  void AttributeCollection__ctor_m683384905 (AttributeCollection_t3488369622 * __this, StateBag_t282928164 * ___bag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Web.UI.WebControls.Style System.Web.UI.WebControls.WebControl::get_ControlStyle()
extern "C"  Style_t3589053173 * WebControl_get_ControlStyle_m1928967988 (WebControl_t2705811671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.UI.WebControls.Style::set_CssClass(System.String)
extern "C"  void Style_set_CssClass_m4224840121 (Style_t3589053173 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Web.UI.Control::get_EnableTheming()
extern "C"  bool Control_get_EnableTheming_m849969501 (Control_t3006474639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.UI.Control::get_SkinID()
extern "C"  String_t* Control_get_SkinID_m3192876181 (Control_t3006474639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System.Web.UI.StateBag::GetShort(System.String,System.Int16)
extern "C"  int16_t StateBag_GetShort_m2518203586 (StateBag_t282928164 * __this, String_t* ___key0, int16_t ___def1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.UI.WebControls.Style::set_Width(System.Web.UI.WebControls.Unit)
extern "C"  void Style_set_Width_m1142288022 (Style_t3589053173 * __this, Unit_t3186727900  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.UI.HtmlTextWriter::StaticGetTagName(System.Web.UI.HtmlTextWriterTag)
extern "C"  String_t* HtmlTextWriter_StaticGetTagName_m641329516 (Il2CppObject * __this /* static, unused */, int32_t ___tagKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Web.UI.WebControls.WebControl::get_ControlStyleCreated()
extern "C"  bool WebControl_get_ControlStyleCreated_m1542237126 (WebControl_t2705811671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Web.UI.WebControls.Unit System.Web.UI.WebControls.Style::get_BorderWidth()
extern "C"  Unit_t3186727900  Style_get_BorderWidth_m2473159276 (Style_t3589053173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Web.UI.WebControls.Unit::get_IsEmpty()
extern "C"  bool Unit_get_IsEmpty_m2953144017 (Unit_t3186727900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Web.UI.WebControls.BorderStyle System.Web.UI.WebControls.Style::get_BorderStyle()
extern "C"  int32_t Style_get_BorderStyle_m2782573478 (Style_t3589053173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Web.UI.WebControls.Unit System.Web.UI.WebControls.Style::get_Height()
extern "C"  Unit_t3186727900  Style_get_Height_m391371825 (Style_t3589053173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Web.UI.WebControls.Unit System.Web.UI.WebControls.Style::get_Width()
extern "C"  Unit_t3186727900  Style_get_Width_m2209399369 (Style_t3589053173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Inequality(System.String,System.String)
extern "C"  bool String_op_Inequality_m215368492 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Int16::ToString()
extern "C"  String_t* Int16_ToString_m1270547562 (int16_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.UI.WebControls.WebControl::AddDisplayStyleAttribute(System.Web.UI.HtmlTextWriter)
extern "C"  void WebControl_AddDisplayStyleAttribute_m3292372057 (WebControl_t2705811671 * __this, HtmlTextWriter_t1404937429 * ___writer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ICollection System.Web.UI.AttributeCollection::get_Keys()
extern "C"  Il2CppObject * AttributeCollection_get_Keys_m437097685 (AttributeCollection_t3488369622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.UI.AttributeCollection::get_Item(System.String)
extern "C"  String_t* AttributeCollection_get_Item_m2348777473 (AttributeCollection_t3488369622 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.UI.WebControls.Style::.ctor(System.Web.UI.StateBag)
extern "C"  void Style__ctor_m3151900008 (Style_t3589053173 * __this, StateBag_t282928164 * ___bag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.UI.Control::LoadViewState(System.Object)
extern "C"  void Control_LoadViewState_m3270187253 (Control_t3006474639 * __this, Il2CppObject * ___savedState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Web.UI.StateBag::get_Item(System.String)
extern "C"  Il2CppObject * StateBag_get_Item_m844854611 (StateBag_t282928164 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.UI.WebControls.Style::LoadBitState()
extern "C"  void Style_LoadBitState_m1621115954 (Style_t3589053173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.UI.StateBag::.ctor()
extern "C"  void StateBag__ctor_m3105408983 (StateBag_t282928164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.UI.StateBag::LoadViewState(System.Object)
extern "C"  void StateBag_LoadViewState_m1562960604 (StateBag_t282928164 * __this, Il2CppObject * ___savedState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Web.UI.StateBag::GetBool(System.String,System.Boolean)
extern "C"  bool StateBag_GetBool_m3269779479 (StateBag_t282928164 * __this, String_t* ___key0, bool ___def1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Web.UI.Adapters.ControlAdapter System.Web.UI.Control::get_Adapter()
extern "C"  ControlAdapter_t3811171780 * Control_get_Adapter_m1187056852 (Control_t3006474639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.UI.Control::Render(System.Web.UI.HtmlTextWriter)
extern "C"  void Control_Render_m1743172642 (Control_t3006474639 * __this, HtmlTextWriter_t1404937429 * ___writer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.UI.StateBag::set_Item(System.String,System.Object)
extern "C"  void StateBag_set_Item_m2669728000 (StateBag_t282928164 * __this, String_t* ___key0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.UI.WebControls.Style::SaveBitState()
extern "C"  void Style_SaveBitState_m4047915169 (Style_t3589053173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Web.UI.Control::SaveViewState()
extern "C"  Il2CppObject * Control_SaveViewState_m3298032528 (Control_t3006474639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Web.UI.StateBag::SaveViewState()
extern "C"  Il2CppObject * StateBag_SaveViewState_m662719031 (StateBag_t282928164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.UI.Pair::.ctor(System.Object,System.Object)
extern "C"  void Pair__ctor_m2201458026 (Pair_t771337780 * __this, Il2CppObject * ___first0, Il2CppObject * ___second1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.UI.StateBag::SetDirty(System.Boolean)
extern "C"  void StateBag_SetDirty_m3657863186 (StateBag_t282928164 * __this, bool ___dirty0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.UI.Control::TrackViewState()
extern "C"  void Control_TrackViewState_m4143870237 (Control_t3006474639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Attribute::.ctor()
extern "C"  void Attribute__ctor_m1529526131 (Attribute_t861562559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char System.String::get_Chars(System.Int32)
extern "C"  Il2CppChar String_get_Chars_m2986988803 (String_t* __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Web.UplevelHelper::DetermineUplevel_1_1(System.String,System.Boolean&,System.Int32)
extern "C"  bool UplevelHelper_DetermineUplevel_1_1_m1060501430 (Il2CppObject * __this /* static, unused */, String_t* ___ua0, bool* ___hasJavaScript1, int32_t ___ualength2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Web.UplevelHelper::ScanForMatch_2_2(System.String,System.Boolean&,System.Int32)
extern "C"  bool UplevelHelper_ScanForMatch_2_2_m3964995597 (Il2CppObject * __this /* static, unused */, String_t* ___ua0, bool* ___hasJavaScript1, int32_t ___ualength2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Web.UplevelHelper::ScanForMatch_2_3(System.String,System.Boolean&,System.Int32)
extern "C"  bool UplevelHelper_ScanForMatch_2_3_m4034542246 (Il2CppObject * __this /* static, unused */, String_t* ___ua0, bool* ___hasJavaScript1, int32_t ___ualength2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Web.UplevelHelper::ScanForMatch_2_5(System.String,System.Boolean&,System.Int32)
extern "C"  bool UplevelHelper_ScanForMatch_2_5_m864262504 (Il2CppObject * __this /* static, unused */, String_t* ___ua0, bool* ___hasJavaScript1, int32_t ___ualength2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TypeCode System.Type::GetTypeCode(System.Type)
extern "C"  int32_t Type_GetTypeCode_m480753082 (Il2CppObject * __this /* static, unused */, Type_t * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.DateTime::get_Ticks()
extern "C"  int64_t DateTime_get_Ticks_m1550640881 (DateTime_t3738529785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::.ctor()
extern "C"  void BinaryFormatter__ctor_m971003555 (BinaryFormatter_t3197753202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::Serialize(System.IO.Stream,System.Object)
extern "C"  void BinaryFormatter_Serialize_m1744386044 (BinaryFormatter_t3197753202 * __this, Stream_t1273022909 * p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.DateTime::.ctor(System.Int64)
extern "C"  void DateTime__ctor_m516789325 (DateTime_t3738529785 * __this, int64_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::Deserialize(System.IO.Stream)
extern "C"  Il2CppObject * BinaryFormatter_Deserialize_m193346007 (BinaryFormatter_t3197753202 * __this, Stream_t1273022909 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m904156431 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String)
extern "C"  void ArgumentOutOfRangeException__ctor_m3628145864 (ArgumentOutOfRangeException_t777629997 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.PropertyDescriptorCollection::get_Count()
extern "C"  int32_t PropertyDescriptorCollection_get_Count_m2540204170 (PropertyDescriptorCollection_t4164928659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.HttpException::.ctor(System.String)
extern "C"  void HttpException__ctor_m2549443145 (HttpException_t2907797370 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m920492651 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Random::.ctor()
extern "C"  void Random__ctor_m4122933043 (Random_t108471755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Monitor::Enter(System.Object)
extern "C"  void Monitor_Enter_m2249409497 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Monitor::Exit(System.Object)
extern "C"  void Monitor_Exit_m3585316909 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m3937257545 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Int32::ToString(System.String,System.IFormatProvider)
extern "C"  String_t* Int32_ToString_m2507389746 (int32_t* __this, String_t* p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object,System.Object,System.Object)
extern "C"  String_t* String_Format_m3339413201 (Il2CppObject * __this /* static, unused */, String_t* p0, Il2CppObject * p1, Il2CppObject * p2, Il2CppObject * p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.Path::Combine(System.String,System.String)
extern "C"  String_t* Path_Combine_m3389272516 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Web.Util.FileUtils/CreateTempFile::Invoke(System.String)
extern "C"  Il2CppObject * CreateTempFile_Invoke_m772578864 (CreateTempFile_t844781177 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo System.Globalization.CultureInfo::get_InvariantCulture()
extern "C"  CultureInfo_t4157843068 * CultureInfo_get_InvariantCulture_m3532445182 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.Util.SearchPattern::SetPattern(System.String,System.Boolean)
extern "C"  void SearchPattern_SetPattern_m239268423 (SearchPattern_t3593873567 * __this, String_t* ___pattern0, bool ___ignore1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.Util.SearchPattern::Compile(System.String)
extern "C"  void SearchPattern_Compile_m1523637325 (SearchPattern_t3593873567 * __this, String_t* ___pattern0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Web.Util.SearchPattern::Match(System.Web.Util.SearchPattern/Op,System.String,System.Int32)
extern "C"  bool SearchPattern_Match_m327281186 (SearchPattern_t3593873567 * __this, Op_t1428817098 * ___op0, String_t* ___text1, int32_t ___ptr2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String)
extern "C"  void ArgumentException__ctor_m1312628991 (ArgumentException_t132251570 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.Util.SearchPattern/Op::.ctor(System.Web.Util.SearchPattern/OpCode)
extern "C"  void Op__ctor_m2094078555 (Op_t1428817098 * __this, int32_t ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::IndexOfAny(System.Char[],System.Int32)
extern "C"  int32_t String_IndexOfAny_m2323029521 (String_t* __this, CharU5BU5D_t3528271667* p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Substring(System.Int32,System.Int32)
extern "C"  String_t* String_Substring_m1610150815 (String_t* __this, int32_t p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::ToLower()
extern "C"  String_t* String_ToLower_m2029374922 (String_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Web.Util.StrUtils::StartsWith(System.String,System.String,System.Boolean)
extern "C"  bool StrUtils_StartsWith_m3039927907 (Il2CppObject * __this /* static, unused */, String_t* ___str10, String_t* ___str21, bool ___ignore_case2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::Compare(System.String,System.Int32,System.String,System.Int32,System.Int32,System.Boolean,System.Globalization.CultureInfo)
extern "C"  int32_t String_Compare_m945110377 (Il2CppObject * __this /* static, unused */, String_t* p0, int32_t p1, String_t* p2, int32_t p3, int32_t p4, bool p5, CultureInfo_t4157843068 * p6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Web.Util.StrUtils::EndsWith(System.String,System.String,System.Boolean)
extern "C"  bool StrUtils_EndsWith_m890124797 (Il2CppObject * __this /* static, unused */, String_t* ___str10, String_t* ___str21, bool ___ignore_case2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor()
extern "C"  void StringBuilder__ctor_m3121283359 (StringBuilder_t1712802186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
extern "C"  StringBuilder_t1712802186 * StringBuilder_Append_m1965104174 (StringBuilder_t1712802186 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Char)
extern "C"  StringBuilder_t1712802186 * StringBuilder_Append_m2383614642 (StringBuilder_t1712802186 * __this, Il2CppChar p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.StringBuilder::ToString()
extern "C"  String_t* StringBuilder_ToString_m3317489284 (StringBuilder_t1712802186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C"  bool String_IsNullOrEmpty_m2969720369 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.String::Split(System.Char[],System.StringSplitOptions)
extern "C"  StringU5BU5D_t1281789340* String_Split_m1466646415 (String_t* __this, CharU5BU5D_t3528271667* p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.DateTime::ToUniversalTime()
extern "C"  DateTime_t3738529785  DateTime_ToUniversalTime_m1945318289 (DateTime_t3738529785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.DateTime::ToString(System.String,System.IFormatProvider)
extern "C"  String_t* DateTime_ToString_m2992030064 (DateTime_t3738529785 * __this, String_t* p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.Util.UrlUtils::GetDirectory(System.String)
extern "C"  String_t* UrlUtils_GetDirectory_m1298890870 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::EndsWith(System.String)
extern "C"  bool String_EndsWith_m1901926500 (String_t* __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.HttpRuntime::get_AppDomainAppVirtualPath()
extern "C"  String_t* HttpRuntime_get_AppDomainAppVirtualPath_m531373613 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::StartsWith(System.String)
extern "C"  bool String_StartsWith_m1759067526 (String_t* __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Substring(System.Int32)
extern "C"  String_t* String_Substring_m2848979100 (String_t* __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String[])
extern "C"  String_t* String_Concat_m1809518182 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1281789340* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.Util.UrlUtils::Canonic(System.String)
extern "C"  String_t* UrlUtils_Canonic_m4232072973 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Web.Util.StrUtils::StartsWith(System.String,System.String)
extern "C"  bool StrUtils_StartsWith_m2313343174 (Il2CppObject * __this /* static, unused */, String_t* ___str10, String_t* ___str21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::IndexOf(System.String)
extern "C"  int32_t String_IndexOf_m1977622757 (String_t* __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.Util.UrlUtils::GetFile(System.String)
extern "C"  String_t* UrlUtils_GetFile_m1187780462 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C"  void ArgumentNullException__ctor_m1170824041 (ArgumentNullException_t1615371798 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Replace(System.Char,System.Char)
extern "C"  String_t* String_Replace_m3726209165 (String_t* __this, Il2CppChar p0, Il2CppChar p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Web.Util.UrlUtils::IsRooted(System.String)
extern "C"  bool UrlUtils_IsRooted_m3690953646 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m3755062657 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.String::Split(System.Char[])
extern "C"  StringU5BU5D_t1281789340* String_Split_m3646115398 (String_t* __this, CharU5BU5D_t3528271667* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Join(System.String,System.String[],System.Int32,System.Int32)
extern "C"  String_t* String_Join_m29736248 (Il2CppObject * __this /* static, unused */, String_t* p0, StringU5BU5D_t1281789340* p1, int32_t p2, int32_t p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.Util.UrlUtils::RemoveDoubleSlashes(System.String)
extern "C"  String_t* UrlUtils_RemoveDoubleSlashes_m2124540071 (Il2CppObject * __this /* static, unused */, String_t* ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::LastIndexOf(System.Char)
extern "C"  int32_t String_LastIndexOf_m3451222878 (String_t* __this, Il2CppChar p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor(System.Int32)
extern "C"  void StringBuilder__ctor_m2367297767 (StringBuilder_t1712802186 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String,System.Int32,System.Int32)
extern "C"  StringBuilder_t1712802186 * StringBuilder_Append_m3214161208 (StringBuilder_t1712802186 * __this, String_t* p0, int32_t p1, int32_t p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object)
extern "C"  String_t* String_Format_m2844511972 (Il2CppObject * __this /* static, unused */, String_t* p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::IndexOf(System.Char)
extern "C"  int32_t String_IndexOf_m363431711 (String_t* __this, Il2CppChar p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Web.Configuration.WebConfigurationManager::GetWebApplicationSection(System.String)
extern "C"  Il2CppObject * WebConfigurationManager_GetWebApplicationSection_m4036650157 (Il2CppObject * __this /* static, unused */, String_t* ___sectionName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Web.Configuration.GlobalizationSection System.Web.Util.WebEncoding::get_GlobalizationConfig()
extern "C"  GlobalizationSection_t244366663 * WebEncoding_get_GlobalizationConfig_m2546015474 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Web.Configuration.GlobalizationSection::get_FileEncoding()
extern "C"  Encoding_t1523322056 * GlobalizationSection_get_FileEncoding_m1644580766 (GlobalizationSection_t244366663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Text.Encoding::get_Default()
extern "C"  Encoding_t1523322056 * Encoding_get_Default_m1632902165 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Web.Configuration.GlobalizationSection::get_ResponseEncoding()
extern "C"  Encoding_t1523322056 * GlobalizationSection_get_ResponseEncoding_m831683111 (GlobalizationSection_t244366663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Web.Configuration.GlobalizationSection::get_RequestEncoding()
extern "C"  Encoding_t1523322056 * GlobalizationSection_get_RequestEncoding_m2054852899 (GlobalizationSection_t244366663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.VirtualPath::.ctor(System.String,System.String,System.Boolean)
extern "C"  void VirtualPath__ctor_m3863291854 (VirtualPath_t4270372584 * __this, String_t* ___vpath0, String_t* ___physicalPath1, bool ___isFake2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.VirtualPath::set_CurrentRequestDirectory(System.String)
extern "C"  void VirtualPath_set_CurrentRequestDirectory_m4189897870 (VirtualPath_t4270372584 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Web.VirtualPathUtility::IsRooted(System.String)
extern "C"  bool VirtualPathUtility_IsRooted_m3278026923 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.VirtualPath::set_IsRooted(System.Boolean)
extern "C"  void VirtualPath_set_IsRooted_m733387534 (VirtualPath_t4270372584 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Web.VirtualPathUtility::IsAbsolute(System.String)
extern "C"  bool VirtualPathUtility_IsAbsolute_m2473455663 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.VirtualPath::set_IsAbsolute(System.Boolean)
extern "C"  void VirtualPath_set_IsAbsolute_m4213413353 (VirtualPath_t4270372584 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Web.VirtualPathUtility::IsAppRelative(System.String)
extern "C"  bool VirtualPathUtility_IsAppRelative_m548692695 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.VirtualPath::set_IsAppRelative(System.Boolean)
extern "C"  void VirtualPath_set_IsAppRelative_m3998062203 (VirtualPath_t4270372584 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.Path::GetFileName(System.String)
extern "C"  String_t* Path_GetFileName_m1354558116 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.VirtualPath::set_Original(System.String)
extern "C"  void VirtualPath_set_Original_m1325633546 (VirtualPath_t4270372584 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.VirtualPath::set_IsFake(System.Boolean)
extern "C"  void VirtualPath_set_IsFake_m1547374389 (VirtualPath_t4270372584 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Web.VirtualPath::get_IsAbsolute()
extern "C"  bool VirtualPath_get_IsAbsolute_m2856159077 (VirtualPath_t4270372584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.VirtualPath::get_Original()
extern "C"  String_t* VirtualPath_get_Original_m1834498342 (VirtualPath_t4270372584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.VirtualPath::MakeRooted(System.String)
extern "C"  String_t* VirtualPath_MakeRooted_m3007486533 (VirtualPath_t4270372584 * __this, String_t* ___original0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.VirtualPathUtility::ToAbsolute(System.String)
extern "C"  String_t* VirtualPathUtility_ToAbsolute_m373284300 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Web.VirtualPath::get_IsAppRelative()
extern "C"  bool VirtualPath_get_IsAppRelative_m1108160356 (VirtualPath_t4270372584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.VirtualPathUtility::ToAppRelative(System.String)
extern "C"  String_t* VirtualPathUtility_ToAppRelative_m769660906 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.VirtualPathUtility::GetExtension(System.String)
extern "C"  String_t* VirtualPathUtility_GetExtension_m3822181684 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.VirtualPath::get_Absolute()
extern "C"  String_t* VirtualPath_get_Absolute_m3947569336 (VirtualPath_t4270372584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.VirtualPathUtility::GetDirectory(System.String)
extern "C"  String_t* VirtualPathUtility_GetDirectory_m2076898960 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.VirtualPathUtility::GetDirectory(System.String,System.Boolean)
extern "C"  String_t* VirtualPathUtility_GetDirectory_m404138870 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, bool ___normalize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Web.HttpContext System.Web.HttpContext::get_Current()
extern "C"  HttpContext_t1969259010 * HttpContext_get_Current_m1955064578 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Web.HttpRequest System.Web.HttpContext::get_Request()
extern "C"  HttpRequest_t809700260 * HttpContext_get_Request_m1929562575 (HttpContext_t1969259010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.HttpRequest::get_CurrentExecutionFilePath()
extern "C"  String_t* HttpRequest_get_CurrentExecutionFilePath_m2109644776 (HttpRequest_t809700260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.HttpRequest::MapPath(System.String)
extern "C"  String_t* HttpRequest_MapPath_m3338942164 (HttpRequest_t809700260 * __this, String_t* ___virtualPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.VirtualPath::get_CurrentRequestDirectory()
extern "C"  String_t* VirtualPath_get_CurrentRequestDirectory_m3125580679 (VirtualPath_t4270372584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.VirtualPathUtility::Combine(System.String,System.String)
extern "C"  String_t* VirtualPathUtility_Combine_m2177271470 (Il2CppObject * __this /* static, unused */, String_t* ___basePath0, String_t* ___relativePath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Web.VirtualPath::get_IsFake()
extern "C"  bool VirtualPath_get_IsFake_m566634131 (VirtualPath_t4270372584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.VirtualPath::get_PhysicalPath()
extern "C"  String_t* VirtualPath_get_PhysicalPath_m2235883404 (VirtualPath_t4270372584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m2163913788 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, String_t* p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.HttpRuntime::get_AppDomainAppPath()
extern "C"  String_t* HttpRuntime_get_AppDomainAppPath_m94348122 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.VirtualPath::.ctor(System.String)
extern "C"  void VirtualPath__ctor_m3027638245 (VirtualPath_t4270372584 * __this, String_t* ___vpath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Web.HttpRuntime::get_RunningOnWindows()
extern "C"  bool HttpRuntime_get_RunningOnWindows_m22177484 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Web.Configuration.MonoSettingsSection::get_VerificationCompatibility()
extern "C"  int32_t MonoSettingsSection_get_VerificationCompatibility_m2509066582 (MonoSettingsSection_t2949572421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.VirtualPathUtility::Normalize(System.String)
extern "C"  String_t* VirtualPathUtility_Normalize_m1039358837 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::LastIndexOf(System.Char,System.Int32,System.Int32)
extern "C"  int32_t String_LastIndexOf_m3228770703 (String_t* __this, Il2CppChar p0, int32_t p1, int32_t p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Web.Util.StrUtils::IsNullOrEmpty(System.String)
extern "C"  bool StrUtils_IsNullOrEmpty_m2227577989 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.VirtualPathUtility::Canonize(System.String)
extern "C"  String_t* VirtualPathUtility_Canonize_m2348126131 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.VirtualPathUtility::RemoveTrailingSlash(System.String)
extern "C"  String_t* VirtualPathUtility_RemoveTrailingSlash_m2660928406 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NullReferenceException::.ctor()
extern "C"  void NullReferenceException__ctor_m744513393 (NullReferenceException_t1023182353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.VirtualPathUtility::ToAbsoluteInternal(System.String)
extern "C"  String_t* VirtualPathUtility_ToAbsoluteInternal_m1993485465 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::CompareOrdinal(System.String,System.String)
extern "C"  int32_t String_CompareOrdinal_m786132908 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.VirtualPathUtility::ToAbsolute(System.String,System.String)
extern "C"  String_t* VirtualPathUtility_ToAbsolute_m409990745 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, String_t* ___applicationPath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.VirtualPathUtility::ToAbsolute(System.String,System.Boolean)
extern "C"  String_t* VirtualPathUtility_ToAbsolute_m3451502579 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, bool ___normalize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.VirtualPathUtility::ToAbsolute(System.String,System.String,System.Boolean)
extern "C"  String_t* VirtualPathUtility_ToAbsolute_m1728694453 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, String_t* ___applicationPath1, bool ___normalize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String,System.String)
extern "C"  void ArgumentException__ctor_m1216717135 (ArgumentException_t132251570 * __this, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.VirtualPathUtility::ToAppRelative(System.String,System.String)
extern "C"  String_t* VirtualPathUtility_ToAppRelative_m867676899 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, String_t* ___applicationPath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::CompareOrdinal(System.String,System.Int32,System.String,System.Int32,System.Int32)
extern "C"  int32_t String_CompareOrdinal_m1012192092 (Il2CppObject * __this /* static, unused */, String_t* p0, int32_t p1, String_t* p2, int32_t p3, int32_t p4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::IndexOf(System.Char,System.Int32)
extern "C"  int32_t String_IndexOf_m2466398549 (String_t* __this, Il2CppChar p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Web.Util.StrUtils::SplitRemoveEmptyEntries(System.String,System.Char[])
extern "C"  StringU5BU5D_t1281789340* StrUtils_SplitRemoveEmptyEntries_m3488785249 (Il2CppObject * __this /* static, unused */, String_t* ___value0, CharU5BU5D_t3528271667* ___separator1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.StringBuilder::get_Length()
extern "C"  int32_t StringBuilder_get_Length_m3238060835 (StringBuilder_t1712802186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Microsoft.Win32.Registry::GetValue(System.String,System.String,System.Object)
extern "C"  Il2CppObject * Registry_GetValue_m1643530476 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, Il2CppObject * p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::IndexOfAny(System.Char[])
extern "C"  int32_t String_IndexOfAny_m4159774896 (String_t* __this, CharU5BU5D_t3528271667* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.CategoryAttribute::.ctor(System.String)
extern "C"  void CategoryAttribute__ctor_m3344259296 (CategoryAttribute_t39585132 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.StringComparer System.StringComparer::get_OrdinalIgnoreCase()
extern "C"  StringComparer_t3301955079 * StringComparer_get_OrdinalIgnoreCase_m2680809380 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.NameValueCollection::.ctor(System.Collections.IEqualityComparer)
extern "C"  void NameValueCollection__ctor_m1036121484 (NameValueCollection_t407452768 * __this, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.NameObjectCollectionBase::set_IsReadOnly(System.Boolean)
extern "C"  void NameObjectCollectionBase_set_IsReadOnly_m2503979362 (NameObjectCollectionBase_t2091847364 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.DescriptionAttribute::.ctor(System.String)
extern "C"  void DescriptionAttribute__ctor_m1483068985 (DescriptionAttribute_t874390736 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean System.Web.UI.WebControls.UnitConverter::CanConvertFrom(System.ComponentModel.ITypeDescriptorContext,System.Type)
extern "C"  bool UnitConverter_CanConvertFrom_m3655322769 (UnitConverter_t1266597561 * __this, Il2CppObject * ___context0, Type_t * ___sourceType1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitConverter_CanConvertFrom_m3655322769_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t * L_0 = ___sourceType1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(Type_t *)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		return (bool)1;
	}

IL_0012:
	{
		Il2CppObject * L_2 = ___context0;
		Type_t * L_3 = ___sourceType1;
		bool L_4 = TypeConverter_CanConvertFrom_m1106272217(__this, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean System.Web.UI.WebControls.UnitConverter::CanConvertTo(System.ComponentModel.ITypeDescriptorContext,System.Type)
extern "C"  bool UnitConverter_CanConvertTo_m1908905322 (UnitConverter_t1266597561 * __this, Il2CppObject * ___context0, Type_t * ___destinationType1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitConverter_CanConvertTo_m1908905322_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t * L_0 = ___destinationType1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(Type_t *)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		return (bool)1;
	}

IL_0012:
	{
		Il2CppObject * L_2 = ___context0;
		Type_t * L_3 = ___destinationType1;
		bool L_4 = TypeConverter_CanConvertTo_m3338360407(__this, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Object System.Web.UI.WebControls.UnitConverter::ConvertTo(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object,System.Type)
extern "C"  Il2CppObject * UnitConverter_ConvertTo_m1831711620 (UnitConverter_t1266597561 * __this, Il2CppObject * ___context0, CultureInfo_t4157843068 * ___culture1, Il2CppObject * ___value2, Type_t * ___destinationType3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitConverter_ConvertTo_m1831711620_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Unit_t3186727900  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject * L_0 = ___value2;
		if (!((Il2CppObject *)IsInstSealed(L_0, Unit_t3186727900_il2cpp_TypeInfo_var)))
		{
			goto IL_002c;
		}
	}
	{
		Type_t * L_1 = ___destinationType3;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_1) == ((Il2CppObject*)(Type_t *)L_2))))
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_3 = ___value2;
		V_0 = ((*(Unit_t3186727900 *)((Unit_t3186727900 *)UnBox(L_3, Unit_t3186727900_il2cpp_TypeInfo_var))));
		CultureInfo_t4157843068 * L_4 = ___culture1;
		String_t* L_5 = Unit_ToString_m1301839610((&V_0), L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_002c:
	{
		Il2CppObject * L_6 = ___context0;
		CultureInfo_t4157843068 * L_7 = ___culture1;
		Il2CppObject * L_8 = ___value2;
		Type_t * L_9 = ___destinationType3;
		Il2CppObject * L_10 = TypeConverter_ConvertTo_m3165899902(__this, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Object System.Web.UI.WebControls.UnitConverter::ConvertFrom(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object)
extern "C"  Il2CppObject * UnitConverter_ConvertFrom_m3418345965 (UnitConverter_t1266597561 * __this, Il2CppObject * ___context0, CultureInfo_t4157843068 * ___culture1, Il2CppObject * ___value2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitConverter_ConvertFrom_m3418345965_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___value2;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return NULL;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___value2;
		NullCheck(L_1);
		Type_t * L_2 = Object_GetType_m88164663(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Type_t * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_3) == ((Il2CppObject*)(Type_t *)L_4))))
		{
			goto IL_0031;
		}
	}
	{
		Il2CppObject * L_5 = ___value2;
		CultureInfo_t4157843068 * L_6 = ___culture1;
		Unit_t3186727900  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Unit__ctor_m980678834(&L_7, ((String_t*)CastclassSealed(L_5, String_t_il2cpp_TypeInfo_var)), L_6, /*hidden argument*/NULL);
		Unit_t3186727900  L_8 = L_7;
		Il2CppObject * L_9 = Box(Unit_t3186727900_il2cpp_TypeInfo_var, &L_8);
		return L_9;
	}

IL_0031:
	{
		Il2CppObject * L_10 = ___context0;
		CultureInfo_t4157843068 * L_11 = ___culture1;
		Il2CppObject * L_12 = ___value2;
		Il2CppObject * L_13 = TypeConverter_ConvertFrom_m1024238132(__this, L_10, L_11, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.ComponentModel.TypeConverter/StandardValuesCollection System.Web.UI.WebControls.ValidatedControlConverter::GetStandardValues(System.ComponentModel.ITypeDescriptorContext)
extern "C"  StandardValuesCollection_t2184948248 * ValidatedControlConverter_GetStandardValues_m186965859 (ValidatedControlConverter_t1434589485 * __this, Il2CppObject * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValidatedControlConverter_GetStandardValues_m186965859_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ArrayList_t2718874744 * V_0 = NULL;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	ComponentCollection_t1445440302 * V_3 = NULL;
	int32_t V_4 = 0;
	{
		Il2CppObject * L_0 = ___context0;
		if (!L_0)
		{
			goto IL_00b0;
		}
	}
	{
		Il2CppObject * L_1 = ___context0;
		NullCheck(L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.ComponentModel.IContainer System.ComponentModel.ITypeDescriptorContext::get_Container() */, ITypeDescriptorContext_t2998566513_il2cpp_TypeInfo_var, L_1);
		if (!L_2)
		{
			goto IL_00b0;
		}
	}
	{
		Il2CppObject * L_3 = ___context0;
		NullCheck(L_3);
		Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.ComponentModel.IContainer System.ComponentModel.ITypeDescriptorContext::get_Container() */, ITypeDescriptorContext_t2998566513_il2cpp_TypeInfo_var, L_3);
		NullCheck(L_4);
		ComponentCollection_t1445440302 * L_5 = InterfaceFuncInvoker0< ComponentCollection_t1445440302 * >::Invoke(0 /* System.ComponentModel.ComponentCollection System.ComponentModel.IContainer::get_Components() */, IContainer_t1776039496_il2cpp_TypeInfo_var, L_4);
		if (!L_5)
		{
			goto IL_00b0;
		}
	}
	{
		ArrayList_t2718874744 * L_6 = (ArrayList_t2718874744 *)il2cpp_codegen_object_new(ArrayList_t2718874744_il2cpp_TypeInfo_var);
		ArrayList__ctor_m4254721275(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		Il2CppObject * L_7 = ___context0;
		NullCheck(L_7);
		Il2CppObject * L_8 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.ComponentModel.IContainer System.ComponentModel.ITypeDescriptorContext::get_Container() */, ITypeDescriptorContext_t2998566513_il2cpp_TypeInfo_var, L_7);
		NullCheck(L_8);
		ComponentCollection_t1445440302 * L_9 = InterfaceFuncInvoker0< ComponentCollection_t1445440302 * >::Invoke(0 /* System.ComponentModel.ComponentCollection System.ComponentModel.IContainer::get_Components() */, IContainer_t1776039496_il2cpp_TypeInfo_var, L_8);
		V_3 = L_9;
		ComponentCollection_t1445440302 * L_10 = V_3;
		NullCheck(L_10);
		int32_t L_11 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.Collections.ReadOnlyCollectionBase::get_Count() */, L_10);
		V_1 = L_11;
		V_4 = 0;
		goto IL_008d;
	}

IL_0042:
	{
		ComponentCollection_t1445440302 * L_12 = V_3;
		int32_t L_13 = V_4;
		NullCheck(L_12);
		Il2CppObject * L_14 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(11 /* System.ComponentModel.IComponent System.ComponentModel.ComponentCollection::get_Item(System.Int32) */, L_12, L_13);
		bool L_15 = VirtFuncInvoker1< bool, Control_t3006474639 * >::Invoke(15 /* System.Boolean System.Web.UI.WebControls.ValidatedControlConverter::FilterControl(System.Web.UI.Control) */, __this, ((Control_t3006474639 *)CastclassClass(L_14, Control_t3006474639_il2cpp_TypeInfo_var)));
		if (!L_15)
		{
			goto IL_0087;
		}
	}
	{
		ComponentCollection_t1445440302 * L_16 = V_3;
		int32_t L_17 = V_4;
		NullCheck(L_16);
		Il2CppObject * L_18 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(11 /* System.ComponentModel.IComponent System.ComponentModel.ComponentCollection::get_Item(System.Int32) */, L_16, L_17);
		NullCheck(((Control_t3006474639 *)CastclassClass(L_18, Control_t3006474639_il2cpp_TypeInfo_var)));
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(13 /* System.String System.Web.UI.Control::get_ID() */, ((Control_t3006474639 *)CastclassClass(L_18, Control_t3006474639_il2cpp_TypeInfo_var)));
		V_2 = L_19;
		String_t* L_20 = V_2;
		if (!L_20)
		{
			goto IL_0087;
		}
	}
	{
		String_t* L_21 = V_2;
		NullCheck(L_21);
		int32_t L_22 = String_get_Length_m3847582255(L_21, /*hidden argument*/NULL);
		if ((((int32_t)L_22) <= ((int32_t)0)))
		{
			goto IL_0087;
		}
	}
	{
		ArrayList_t2718874744 * L_23 = V_0;
		String_t* L_24 = V_2;
		NullCheck(L_23);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_23, L_24);
	}

IL_0087:
	{
		int32_t L_25 = V_4;
		V_4 = ((int32_t)((int32_t)L_25+(int32_t)1));
	}

IL_008d:
	{
		int32_t L_26 = V_4;
		int32_t L_27 = V_1;
		if ((((int32_t)L_26) < ((int32_t)L_27)))
		{
			goto IL_0042;
		}
	}
	{
		ArrayList_t2718874744 * L_28 = V_0;
		NullCheck(L_28);
		VirtActionInvoker0::Invoke(47 /* System.Void System.Collections.ArrayList::Sort() */, L_28);
		ArrayList_t2718874744 * L_29 = V_0;
		NullCheck(L_29);
		int32_t L_30 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_29);
		if ((((int32_t)L_30) <= ((int32_t)0)))
		{
			goto IL_00ae;
		}
	}
	{
		ArrayList_t2718874744 * L_31 = V_0;
		StandardValuesCollection_t2184948248 * L_32 = (StandardValuesCollection_t2184948248 *)il2cpp_codegen_object_new(StandardValuesCollection_t2184948248_il2cpp_TypeInfo_var);
		StandardValuesCollection__ctor_m260150618(L_32, L_31, /*hidden argument*/NULL);
		return L_32;
	}

IL_00ae:
	{
		return (StandardValuesCollection_t2184948248 *)NULL;
	}

IL_00b0:
	{
		Il2CppObject * L_33 = ___context0;
		StandardValuesCollection_t2184948248 * L_34 = ControlIDConverter_GetStandardValues_m2173371064(__this, L_33, /*hidden argument*/NULL);
		return L_34;
	}
}
// System.Boolean System.Web.UI.WebControls.ValidatedControlConverter::FilterControl(System.Web.UI.Control)
extern "C"  bool ValidatedControlConverter_FilterControl_m3883505585 (ValidatedControlConverter_t1434589485 * __this, Control_t3006474639 * ___control0, const MethodInfo* method)
{
	{
		Control_t3006474639 * L_0 = ___control0;
		PropertyDescriptor_t3244362832 * L_1 = BaseValidator_GetValidationProperty_m1551790743(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return (bool)((((int32_t)((((Il2CppObject*)(PropertyDescriptor_t3244362832 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean System.Web.UI.WebControls.ValidatedControlConverter::GetStandardValuesExclusive(System.ComponentModel.ITypeDescriptorContext)
extern "C"  bool ValidatedControlConverter_GetStandardValuesExclusive_m3827686821 (ValidatedControlConverter_t1434589485 * __this, Il2CppObject * ___context0, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Web.UI.WebControls.ValidatedControlConverter::GetStandardValuesSupported(System.ComponentModel.ITypeDescriptorContext)
extern "C"  bool ValidatedControlConverter_GetStandardValuesSupported_m2437473291 (ValidatedControlConverter_t1434589485 * __this, Il2CppObject * ___context0, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Web.UI.WebControls.VerticalAlignConverter::CanConvertFrom(System.ComponentModel.ITypeDescriptorContext,System.Type)
extern "C"  bool VerticalAlignConverter_CanConvertFrom_m4286439966 (VerticalAlignConverter_t1776218746 * __this, Il2CppObject * ___context0, Type_t * ___sourceType1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___context0;
		Type_t * L_1 = ___sourceType1;
		bool L_2 = EnumConverter_CanConvertFrom_m4081436354(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Object System.Web.UI.WebControls.VerticalAlignConverter::ConvertFrom(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object)
extern "C"  Il2CppObject * VerticalAlignConverter_ConvertFrom_m1938642932 (VerticalAlignConverter_t1776218746 * __this, Il2CppObject * ___context0, CultureInfo_t4157843068 * ___culture1, Il2CppObject * ___value2, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___context0;
		CultureInfo_t4157843068 * L_1 = ___culture1;
		Il2CppObject * L_2 = ___value2;
		Il2CppObject * L_3 = EnumConverter_ConvertFrom_m1625779591(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean System.Web.UI.WebControls.VerticalAlignConverter::CanConvertTo(System.ComponentModel.ITypeDescriptorContext,System.Type)
extern "C"  bool VerticalAlignConverter_CanConvertTo_m1671840047 (VerticalAlignConverter_t1776218746 * __this, Il2CppObject * ___context0, Type_t * ___destinationType1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___context0;
		Type_t * L_1 = ___destinationType1;
		bool L_2 = EnumConverter_CanConvertTo_m1239367246(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Object System.Web.UI.WebControls.VerticalAlignConverter::ConvertTo(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object,System.Type)
extern "C"  Il2CppObject * VerticalAlignConverter_ConvertTo_m4051592597 (VerticalAlignConverter_t1776218746 * __this, Il2CppObject * ___context0, CultureInfo_t4157843068 * ___culture1, Il2CppObject * ___value2, Type_t * ___destinationType3, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___context0;
		CultureInfo_t4157843068 * L_1 = ___culture1;
		Il2CppObject * L_2 = ___value2;
		Type_t * L_3 = ___destinationType3;
		Il2CppObject * L_4 = EnumConverter_ConvertTo_m1137837802(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.Web.UI.WebControls.WebControl::.ctor(System.Web.UI.HtmlTextWriterTag)
extern "C"  void WebControl__ctor_m272488634 (WebControl_t2705811671 * __this, int32_t ___tag0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebControl__ctor_m272488634_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Control_t3006474639_il2cpp_TypeInfo_var);
		Control__ctor_m1794429822(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___tag0;
		__this->set_tag_29(L_0);
		__this->set_enabled_33((bool)1);
		return;
	}
}
// System.Void System.Web.UI.WebControls.WebControl::.cctor()
extern "C"  void WebControl__cctor_m2934243859 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebControl__cctor_m2934243859_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharU5BU5D_t3528271667* L_0 = ((CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)59));
		((WebControl_t2705811671_StaticFields*)WebControl_t2705811671_il2cpp_TypeInfo_var->static_fields)->set__script_trim_chars_35(L_0);
		return;
	}
}
// System.String System.Web.UI.WebControls.WebControl::get_AccessKey()
extern "C"  String_t* WebControl_get_AccessKey_m2497713227 (WebControl_t2705811671 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebControl_get_AccessKey_m2497713227_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StateBag_t282928164 * L_0 = VirtFuncInvoker0< StateBag_t282928164 * >::Invoke(23 /* System.Web.UI.StateBag System.Web.UI.Control::get_ViewState() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_0);
		String_t* L_2 = StateBag_GetString_m2746466793(L_0, _stringLiteral1579007546, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Web.UI.AttributeCollection System.Web.UI.WebControls.WebControl::get_Attributes()
extern "C"  AttributeCollection_t3488369622 * WebControl_get_Attributes_m495154787 (WebControl_t2705811671 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebControl_get_Attributes_m495154787_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AttributeCollection_t3488369622 * L_0 = __this->get_attributes_31();
		if (L_0)
		{
			goto IL_003e;
		}
	}
	{
		StateBag_t282928164 * L_1 = (StateBag_t282928164 *)il2cpp_codegen_object_new(StateBag_t282928164_il2cpp_TypeInfo_var);
		StateBag__ctor_m2018118170(L_1, (bool)1, /*hidden argument*/NULL);
		__this->set_attribute_state_32(L_1);
		bool L_2 = Control_get_IsTrackingViewState_m2001955483(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		StateBag_t282928164 * L_3 = __this->get_attribute_state_32();
		NullCheck(L_3);
		StateBag_TrackViewState_m3806295084(L_3, /*hidden argument*/NULL);
	}

IL_002d:
	{
		StateBag_t282928164 * L_4 = __this->get_attribute_state_32();
		AttributeCollection_t3488369622 * L_5 = (AttributeCollection_t3488369622 *)il2cpp_codegen_object_new(AttributeCollection_t3488369622_il2cpp_TypeInfo_var);
		AttributeCollection__ctor_m683384905(L_5, L_4, /*hidden argument*/NULL);
		__this->set_attributes_31(L_5);
	}

IL_003e:
	{
		AttributeCollection_t3488369622 * L_6 = __this->get_attributes_31();
		return L_6;
	}
}
// System.Web.UI.WebControls.Style System.Web.UI.WebControls.WebControl::get_ControlStyle()
extern "C"  Style_t3589053173 * WebControl_get_ControlStyle_m1928967988 (WebControl_t2705811671 * __this, const MethodInfo* method)
{
	{
		Style_t3589053173 * L_0 = __this->get_style_28();
		if (L_0)
		{
			goto IL_002d;
		}
	}
	{
		Style_t3589053173 * L_1 = VirtFuncInvoker0< Style_t3589053173 * >::Invoke(66 /* System.Web.UI.WebControls.Style System.Web.UI.WebControls.WebControl::CreateControlStyle() */, __this);
		__this->set_style_28(L_1);
		bool L_2 = Control_get_IsTrackingViewState_m2001955483(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		Style_t3589053173 * L_3 = __this->get_style_28();
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(20 /* System.Void System.Web.UI.WebControls.Style::TrackViewState() */, L_3);
	}

IL_002d:
	{
		Style_t3589053173 * L_4 = __this->get_style_28();
		return L_4;
	}
}
// System.Boolean System.Web.UI.WebControls.WebControl::get_ControlStyleCreated()
extern "C"  bool WebControl_get_ControlStyleCreated_m1542237126 (WebControl_t2705811671 * __this, const MethodInfo* method)
{
	{
		Style_t3589053173 * L_0 = __this->get_style_28();
		return (bool)((((int32_t)((((Il2CppObject*)(Style_t3589053173 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Web.UI.WebControls.WebControl::set_CssClass(System.String)
extern "C"  void WebControl_set_CssClass_m1828677653 (WebControl_t2705811671 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		Style_t3589053173 * L_0 = WebControl_get_ControlStyle_m1928967988(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___value0;
		NullCheck(L_0);
		Style_set_CssClass_m4224840121(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Web.UI.WebControls.WebControl::get_Enabled()
extern "C"  bool WebControl_get_Enabled_m375005046 (WebControl_t2705811671 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_enabled_33();
		return L_0;
	}
}
// System.Boolean System.Web.UI.WebControls.WebControl::get_EnableTheming()
extern "C"  bool WebControl_get_EnableTheming_m4249445934 (WebControl_t2705811671 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Control_get_EnableTheming_m849969501(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String System.Web.UI.WebControls.WebControl::get_SkinID()
extern "C"  String_t* WebControl_get_SkinID_m3376252195 (WebControl_t2705811671 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = Control_get_SkinID_m3192876181(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int16 System.Web.UI.WebControls.WebControl::get_TabIndex()
extern "C"  int16_t WebControl_get_TabIndex_m2405075608 (WebControl_t2705811671 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebControl_get_TabIndex_m2405075608_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StateBag_t282928164 * L_0 = VirtFuncInvoker0< StateBag_t282928164 * >::Invoke(23 /* System.Web.UI.StateBag System.Web.UI.Control::get_ViewState() */, __this);
		NullCheck(L_0);
		int16_t L_1 = StateBag_GetShort_m2518203586(L_0, _stringLiteral1591843997, (int16_t)0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String System.Web.UI.WebControls.WebControl::get_ToolTip()
extern "C"  String_t* WebControl_get_ToolTip_m2192069348 (WebControl_t2705811671 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebControl_get_ToolTip_m2192069348_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StateBag_t282928164 * L_0 = VirtFuncInvoker0< StateBag_t282928164 * >::Invoke(23 /* System.Web.UI.StateBag System.Web.UI.Control::get_ViewState() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_0);
		String_t* L_2 = StateBag_GetString_m2746466793(L_0, _stringLiteral1390806371, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Web.UI.WebControls.WebControl::set_Width(System.Web.UI.WebControls.Unit)
extern "C"  void WebControl_set_Width_m313526652 (WebControl_t2705811671 * __this, Unit_t3186727900  ___value0, const MethodInfo* method)
{
	{
		Style_t3589053173 * L_0 = WebControl_get_ControlStyle_m1928967988(__this, /*hidden argument*/NULL);
		Unit_t3186727900  L_1 = ___value0;
		NullCheck(L_0);
		Style_set_Width_m1142288022(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Web.UI.HtmlTextWriterTag System.Web.UI.WebControls.WebControl::get_TagKey()
extern "C"  int32_t WebControl_get_TagKey_m2913157442 (WebControl_t2705811671 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_tag_29();
		return L_0;
	}
}
// System.String System.Web.UI.WebControls.WebControl::get_TagName()
extern "C"  String_t* WebControl_get_TagName_m2672078180 (WebControl_t2705811671 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebControl_get_TagName_m2672078180_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get_tag_name_30();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(61 /* System.Web.UI.HtmlTextWriterTag System.Web.UI.WebControls.WebControl::get_TagKey() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(HtmlTextWriter_t1404937429_il2cpp_TypeInfo_var);
		String_t* L_2 = HtmlTextWriter_StaticGetTagName_m641329516(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		__this->set_tag_name_30(L_2);
	}

IL_001c:
	{
		String_t* L_3 = __this->get_tag_name_30();
		return L_3;
	}
}
// System.Void System.Web.UI.WebControls.WebControl::RenderBeginTag(System.Web.UI.HtmlTextWriter)
extern "C"  void WebControl_RenderBeginTag_m3254788859 (WebControl_t2705811671 * __this, HtmlTextWriter_t1404937429 * ___writer0, const MethodInfo* method)
{
	{
		HtmlTextWriter_t1404937429 * L_0 = ___writer0;
		VirtActionInvoker1< HtmlTextWriter_t1404937429 * >::Invoke(65 /* System.Void System.Web.UI.WebControls.WebControl::AddAttributesToRender(System.Web.UI.HtmlTextWriter) */, __this, L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(61 /* System.Web.UI.HtmlTextWriterTag System.Web.UI.WebControls.WebControl::get_TagKey() */, __this);
		if (L_1)
		{
			goto IL_0023;
		}
	}
	{
		HtmlTextWriter_t1404937429 * L_2 = ___writer0;
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(62 /* System.String System.Web.UI.WebControls.WebControl::get_TagName() */, __this);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(49 /* System.Void System.Web.UI.HtmlTextWriter::RenderBeginTag(System.String) */, L_2, L_3);
		goto IL_002f;
	}

IL_0023:
	{
		HtmlTextWriter_t1404937429 * L_4 = ___writer0;
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(61 /* System.Web.UI.HtmlTextWriterTag System.Web.UI.WebControls.WebControl::get_TagKey() */, __this);
		NullCheck(L_4);
		VirtActionInvoker1< int32_t >::Invoke(50 /* System.Void System.Web.UI.HtmlTextWriter::RenderBeginTag(System.Web.UI.HtmlTextWriterTag) */, L_4, L_5);
	}

IL_002f:
	{
		return;
	}
}
// System.Void System.Web.UI.WebControls.WebControl::RenderEndTag(System.Web.UI.HtmlTextWriter)
extern "C"  void WebControl_RenderEndTag_m2777557423 (WebControl_t2705811671 * __this, HtmlTextWriter_t1404937429 * ___writer0, const MethodInfo* method)
{
	{
		HtmlTextWriter_t1404937429 * L_0 = ___writer0;
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(51 /* System.Void System.Web.UI.HtmlTextWriter::RenderEndTag() */, L_0);
		return;
	}
}
// System.Void System.Web.UI.WebControls.WebControl::AddDisplayStyleAttribute(System.Web.UI.HtmlTextWriter)
extern "C"  void WebControl_AddDisplayStyleAttribute_m3292372057 (WebControl_t2705811671 * __this, HtmlTextWriter_t1404937429 * ___writer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebControl_AddDisplayStyleAttribute_m3292372057_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Unit_t3186727900  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Unit_t3186727900  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Unit_t3186727900  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		bool L_0 = WebControl_get_ControlStyleCreated_m1542237126(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Style_t3589053173 * L_1 = WebControl_get_ControlStyle_m1928967988(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Unit_t3186727900  L_2 = Style_get_BorderWidth_m2473159276(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		bool L_3 = Unit_get_IsEmpty_m2953144017((&V_0), /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0075;
		}
	}
	{
		Style_t3589053173 * L_4 = WebControl_get_ControlStyle_m1928967988(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = Style_get_BorderStyle_m2782573478(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_5) == ((int32_t)1)))
		{
			goto IL_0045;
		}
	}
	{
		Style_t3589053173 * L_6 = WebControl_get_ControlStyle_m1928967988(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = Style_get_BorderStyle_m2782573478(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0075;
		}
	}

IL_0045:
	{
		Style_t3589053173 * L_8 = WebControl_get_ControlStyle_m1928967988(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Unit_t3186727900  L_9 = Style_get_Height_m391371825(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		bool L_10 = Unit_get_IsEmpty_m2953144017((&V_1), /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0075;
		}
	}
	{
		Style_t3589053173 * L_11 = WebControl_get_ControlStyle_m1928967988(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Unit_t3186727900  L_12 = Style_get_Width_m2209399369(L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		bool L_13 = Unit_get_IsEmpty_m2953144017((&V_2), /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0082;
		}
	}

IL_0075:
	{
		HtmlTextWriter_t1404937429 * L_14 = ___writer0;
		NullCheck(L_14);
		VirtActionInvoker2< int32_t, String_t* >::Invoke(37 /* System.Void System.Web.UI.HtmlTextWriter::AddStyleAttribute(System.Web.UI.HtmlTextWriterStyle,System.String) */, L_14, ((int32_t)18), _stringLiteral437919229);
	}

IL_0082:
	{
		return;
	}
}
// System.Void System.Web.UI.WebControls.WebControl::AddAttributesToRender(System.Web.UI.HtmlTextWriter)
extern "C"  void WebControl_AddAttributesToRender_m1714954365 (WebControl_t2705811671 * __this, HtmlTextWriter_t1404937429 * ___writer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebControl_AddAttributesToRender_m1714954365_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	int16_t V_2 = 0;
	Il2CppObject * V_3 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(13 /* System.String System.Web.UI.Control::get_ID() */, __this);
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		HtmlTextWriter_t1404937429 * L_1 = ___writer0;
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Web.UI.Control::get_ClientID() */, __this);
		NullCheck(L_1);
		VirtActionInvoker2< int32_t, String_t* >::Invoke(31 /* System.Void System.Web.UI.HtmlTextWriter::AddAttribute(System.Web.UI.HtmlTextWriterAttribute,System.String) */, L_1, ((int32_t)17), L_2);
	}

IL_0019:
	{
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(53 /* System.String System.Web.UI.WebControls.WebControl::get_AccessKey() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_5 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003b;
		}
	}
	{
		HtmlTextWriter_t1404937429 * L_6 = ___writer0;
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(53 /* System.String System.Web.UI.WebControls.WebControl::get_AccessKey() */, __this);
		NullCheck(L_6);
		VirtActionInvoker2< int32_t, String_t* >::Invoke(31 /* System.Void System.Web.UI.HtmlTextWriter::AddAttribute(System.Web.UI.HtmlTextWriterAttribute,System.String) */, L_6, 0, L_7);
	}

IL_003b:
	{
		bool L_8 = __this->get_enabled_33();
		if (L_8)
		{
			goto IL_0054;
		}
	}
	{
		HtmlTextWriter_t1404937429 * L_9 = ___writer0;
		NullCheck(L_9);
		VirtActionInvoker3< int32_t, String_t*, bool >::Invoke(30 /* System.Void System.Web.UI.HtmlTextWriter::AddAttribute(System.Web.UI.HtmlTextWriterAttribute,System.String,System.Boolean) */, L_9, ((int32_t)13), _stringLiteral1262761478, (bool)0);
	}

IL_0054:
	{
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(59 /* System.String System.Web.UI.WebControls.WebControl::get_ToolTip() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_12 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0077;
		}
	}
	{
		HtmlTextWriter_t1404937429 * L_13 = ___writer0;
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(59 /* System.String System.Web.UI.WebControls.WebControl::get_ToolTip() */, __this);
		NullCheck(L_13);
		VirtActionInvoker2< int32_t, String_t* >::Invoke(31 /* System.Void System.Web.UI.HtmlTextWriter::AddAttribute(System.Web.UI.HtmlTextWriterAttribute,System.String) */, L_13, ((int32_t)34), L_14);
	}

IL_0077:
	{
		int16_t L_15 = VirtFuncInvoker0< int16_t >::Invoke(58 /* System.Int16 System.Web.UI.WebControls.WebControl::get_TabIndex() */, __this);
		if (!L_15)
		{
			goto IL_0098;
		}
	}
	{
		HtmlTextWriter_t1404937429 * L_16 = ___writer0;
		int16_t L_17 = VirtFuncInvoker0< int16_t >::Invoke(58 /* System.Int16 System.Web.UI.WebControls.WebControl::get_TabIndex() */, __this);
		V_2 = L_17;
		String_t* L_18 = Int16_ToString_m1270547562((&V_2), /*hidden argument*/NULL);
		NullCheck(L_16);
		VirtActionInvoker2< int32_t, String_t* >::Invoke(31 /* System.Void System.Web.UI.HtmlTextWriter::AddAttribute(System.Web.UI.HtmlTextWriterAttribute,System.String) */, L_16, ((int32_t)32), L_18);
	}

IL_0098:
	{
		Style_t3589053173 * L_19 = __this->get_style_28();
		if (!L_19)
		{
			goto IL_00d4;
		}
	}
	{
		Style_t3589053173 * L_20 = __this->get_style_28();
		NullCheck(L_20);
		bool L_21 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Web.UI.WebControls.Style::get_IsEmpty() */, L_20);
		if (L_21)
		{
			goto IL_00d4;
		}
	}
	{
		int32_t L_22 = VirtFuncInvoker0< int32_t >::Invoke(61 /* System.Web.UI.HtmlTextWriterTag System.Web.UI.WebControls.WebControl::get_TagKey() */, __this);
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)76)))))
		{
			goto IL_00c7;
		}
	}
	{
		HtmlTextWriter_t1404937429 * L_23 = ___writer0;
		WebControl_AddDisplayStyleAttribute_m3292372057(__this, L_23, /*hidden argument*/NULL);
	}

IL_00c7:
	{
		Style_t3589053173 * L_24 = __this->get_style_28();
		HtmlTextWriter_t1404937429 * L_25 = ___writer0;
		NullCheck(L_24);
		VirtActionInvoker2< HtmlTextWriter_t1404937429 *, WebControl_t2705811671 * >::Invoke(16 /* System.Void System.Web.UI.WebControls.Style::AddAttributesToRender(System.Web.UI.HtmlTextWriter,System.Web.UI.WebControls.WebControl) */, L_24, L_25, __this);
	}

IL_00d4:
	{
		AttributeCollection_t3488369622 * L_26 = __this->get_attributes_31();
		if (!L_26)
		{
			goto IL_0136;
		}
	}
	{
		AttributeCollection_t3488369622 * L_27 = __this->get_attributes_31();
		NullCheck(L_27);
		Il2CppObject * L_28 = AttributeCollection_get_Keys_m437097685(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		Il2CppObject * L_29 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t1941168011_il2cpp_TypeInfo_var, L_28);
		V_1 = L_29;
	}

IL_00f0:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0114;
		}

IL_00f5:
		{
			Il2CppObject * L_30 = V_1;
			NullCheck(L_30);
			Il2CppObject * L_31 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_30);
			V_0 = ((String_t*)CastclassSealed(L_31, String_t_il2cpp_TypeInfo_var));
			HtmlTextWriter_t1404937429 * L_32 = ___writer0;
			String_t* L_33 = V_0;
			AttributeCollection_t3488369622 * L_34 = __this->get_attributes_31();
			String_t* L_35 = V_0;
			NullCheck(L_34);
			String_t* L_36 = AttributeCollection_get_Item_m2348777473(L_34, L_35, /*hidden argument*/NULL);
			NullCheck(L_32);
			VirtActionInvoker2< String_t*, String_t* >::Invoke(33 /* System.Void System.Web.UI.HtmlTextWriter::AddAttribute(System.String,System.String) */, L_32, L_33, L_36);
		}

IL_0114:
		{
			Il2CppObject * L_37 = V_1;
			NullCheck(L_37);
			bool L_38 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_37);
			if (L_38)
			{
				goto IL_00f5;
			}
		}

IL_011f:
		{
			IL2CPP_LEAVE(0x136, FINALLY_0124);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0124;
	}

FINALLY_0124:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_39 = V_1;
			V_3 = ((Il2CppObject *)IsInst(L_39, IDisposable_t3640265483_il2cpp_TypeInfo_var));
			Il2CppObject * L_40 = V_3;
			if (L_40)
			{
				goto IL_012f;
			}
		}

IL_012e:
		{
			IL2CPP_END_FINALLY(292)
		}

IL_012f:
		{
			Il2CppObject * L_41 = V_3;
			NullCheck(L_41);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_41);
			IL2CPP_END_FINALLY(292)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(292)
	{
		IL2CPP_JUMP_TBL(0x136, IL_0136)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0136:
	{
		return;
	}
}
// System.Web.UI.WebControls.Style System.Web.UI.WebControls.WebControl::CreateControlStyle()
extern "C"  Style_t3589053173 * WebControl_CreateControlStyle_m4061402130 (WebControl_t2705811671 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebControl_CreateControlStyle_m4061402130_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StateBag_t282928164 * L_0 = VirtFuncInvoker0< StateBag_t282928164 * >::Invoke(23 /* System.Web.UI.StateBag System.Web.UI.Control::get_ViewState() */, __this);
		Style_t3589053173 * L_1 = (Style_t3589053173 *)il2cpp_codegen_object_new(Style_t3589053173_il2cpp_TypeInfo_var);
		Style__ctor_m3151900008(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.Web.UI.WebControls.WebControl::LoadViewState(System.Object)
extern "C"  void WebControl_LoadViewState_m2630056878 (WebControl_t2705811671 * __this, Il2CppObject * ___savedState0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebControl_LoadViewState_m2630056878_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Pair_t771337780 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___savedState0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		Il2CppObject * L_1 = ___savedState0;
		if (((Pair_t771337780 *)IsInstSealed(L_1, Pair_t771337780_il2cpp_TypeInfo_var)))
		{
			goto IL_0019;
		}
	}

IL_0011:
	{
		Control_LoadViewState_m3270187253(__this, NULL, /*hidden argument*/NULL);
		return;
	}

IL_0019:
	{
		Il2CppObject * L_2 = ___savedState0;
		V_0 = ((Pair_t771337780 *)CastclassSealed(L_2, Pair_t771337780_il2cpp_TypeInfo_var));
		Pair_t771337780 * L_3 = V_0;
		NullCheck(L_3);
		Il2CppObject * L_4 = L_3->get_First_0();
		Control_LoadViewState_m3270187253(__this, L_4, /*hidden argument*/NULL);
		StateBag_t282928164 * L_5 = VirtFuncInvoker0< StateBag_t282928164 * >::Invoke(23 /* System.Web.UI.StateBag System.Web.UI.Control::get_ViewState() */, __this);
		NullCheck(L_5);
		Il2CppObject * L_6 = StateBag_get_Item_m844854611(L_5, _stringLiteral1657994044, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004c;
		}
	}
	{
		Style_t3589053173 * L_7 = WebControl_get_ControlStyle_m1928967988(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Style_LoadBitState_m1621115954(L_7, /*hidden argument*/NULL);
	}

IL_004c:
	{
		Pair_t771337780 * L_8 = V_0;
		NullCheck(L_8);
		Il2CppObject * L_9 = L_8->get_Second_1();
		if (!L_9)
		{
			goto IL_00a5;
		}
	}
	{
		StateBag_t282928164 * L_10 = __this->get_attribute_state_32();
		if (L_10)
		{
			goto IL_0083;
		}
	}
	{
		StateBag_t282928164 * L_11 = (StateBag_t282928164 *)il2cpp_codegen_object_new(StateBag_t282928164_il2cpp_TypeInfo_var);
		StateBag__ctor_m3105408983(L_11, /*hidden argument*/NULL);
		__this->set_attribute_state_32(L_11);
		bool L_12 = Control_get_IsTrackingViewState_m2001955483(__this, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0083;
		}
	}
	{
		StateBag_t282928164 * L_13 = __this->get_attribute_state_32();
		NullCheck(L_13);
		StateBag_TrackViewState_m3806295084(L_13, /*hidden argument*/NULL);
	}

IL_0083:
	{
		StateBag_t282928164 * L_14 = __this->get_attribute_state_32();
		Pair_t771337780 * L_15 = V_0;
		NullCheck(L_15);
		Il2CppObject * L_16 = L_15->get_Second_1();
		NullCheck(L_14);
		StateBag_LoadViewState_m1562960604(L_14, L_16, /*hidden argument*/NULL);
		StateBag_t282928164 * L_17 = __this->get_attribute_state_32();
		AttributeCollection_t3488369622 * L_18 = (AttributeCollection_t3488369622 *)il2cpp_codegen_object_new(AttributeCollection_t3488369622_il2cpp_TypeInfo_var);
		AttributeCollection__ctor_m683384905(L_18, L_17, /*hidden argument*/NULL);
		__this->set_attributes_31(L_18);
	}

IL_00a5:
	{
		StateBag_t282928164 * L_19 = VirtFuncInvoker0< StateBag_t282928164 * >::Invoke(23 /* System.Web.UI.StateBag System.Web.UI.Control::get_ViewState() */, __this);
		bool L_20 = __this->get_enabled_33();
		NullCheck(L_19);
		bool L_21 = StateBag_GetBool_m3269779479(L_19, _stringLiteral646179308, L_20, /*hidden argument*/NULL);
		__this->set_enabled_33(L_21);
		return;
	}
}
// System.Void System.Web.UI.WebControls.WebControl::Render(System.Web.UI.HtmlTextWriter)
extern "C"  void WebControl_Render_m2462273688 (WebControl_t2705811671 * __this, HtmlTextWriter_t1404937429 * ___writer0, const MethodInfo* method)
{
	{
		ControlAdapter_t3811171780 * L_0 = Control_get_Adapter_m1187056852(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		ControlAdapter_t3811171780 * L_1 = Control_get_Adapter_m1187056852(__this, /*hidden argument*/NULL);
		HtmlTextWriter_t1404937429 * L_2 = ___writer0;
		NullCheck(L_1);
		VirtActionInvoker1< HtmlTextWriter_t1404937429 * >::Invoke(13 /* System.Void System.Web.UI.Adapters.ControlAdapter::Render(System.Web.UI.HtmlTextWriter) */, L_1, L_2);
		return;
	}

IL_0018:
	{
		HtmlTextWriter_t1404937429 * L_3 = ___writer0;
		VirtActionInvoker1< HtmlTextWriter_t1404937429 * >::Invoke(63 /* System.Void System.Web.UI.WebControls.WebControl::RenderBeginTag(System.Web.UI.HtmlTextWriter) */, __this, L_3);
		HtmlTextWriter_t1404937429 * L_4 = ___writer0;
		VirtActionInvoker1< HtmlTextWriter_t1404937429 * >::Invoke(67 /* System.Void System.Web.UI.WebControls.WebControl::RenderContents(System.Web.UI.HtmlTextWriter) */, __this, L_4);
		HtmlTextWriter_t1404937429 * L_5 = ___writer0;
		VirtActionInvoker1< HtmlTextWriter_t1404937429 * >::Invoke(64 /* System.Void System.Web.UI.WebControls.WebControl::RenderEndTag(System.Web.UI.HtmlTextWriter) */, __this, L_5);
		return;
	}
}
// System.Void System.Web.UI.WebControls.WebControl::RenderContents(System.Web.UI.HtmlTextWriter)
extern "C"  void WebControl_RenderContents_m2669260225 (WebControl_t2705811671 * __this, HtmlTextWriter_t1404937429 * ___writer0, const MethodInfo* method)
{
	{
		HtmlTextWriter_t1404937429 * L_0 = ___writer0;
		Control_Render_m1743172642(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Web.UI.WebControls.WebControl::SaveViewState()
extern "C"  Il2CppObject * WebControl_SaveViewState_m3996518795 (WebControl_t2705811671 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebControl_SaveViewState_m3996518795_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		bool L_0 = __this->get_track_enabled_state_34();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		StateBag_t282928164 * L_1 = VirtFuncInvoker0< StateBag_t282928164 * >::Invoke(23 /* System.Web.UI.StateBag System.Web.UI.Control::get_ViewState() */, __this);
		bool L_2 = __this->get_enabled_33();
		bool L_3 = L_2;
		Il2CppObject * L_4 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		StateBag_set_Item_m2669728000(L_1, _stringLiteral646179308, L_4, /*hidden argument*/NULL);
	}

IL_0026:
	{
		V_1 = NULL;
		Style_t3589053173 * L_5 = __this->get_style_28();
		if (!L_5)
		{
			goto IL_003e;
		}
	}
	{
		Style_t3589053173 * L_6 = __this->get_style_28();
		NullCheck(L_6);
		Style_SaveBitState_m4047915169(L_6, /*hidden argument*/NULL);
	}

IL_003e:
	{
		Il2CppObject * L_7 = Control_SaveViewState_m3298032528(__this, /*hidden argument*/NULL);
		V_0 = L_7;
		StateBag_t282928164 * L_8 = __this->get_attribute_state_32();
		if (!L_8)
		{
			goto IL_005c;
		}
	}
	{
		StateBag_t282928164 * L_9 = __this->get_attribute_state_32();
		NullCheck(L_9);
		Il2CppObject * L_10 = StateBag_SaveViewState_m662719031(L_9, /*hidden argument*/NULL);
		V_1 = L_10;
	}

IL_005c:
	{
		Il2CppObject * L_11 = V_0;
		if (L_11)
		{
			goto IL_006a;
		}
	}
	{
		Il2CppObject * L_12 = V_1;
		if (L_12)
		{
			goto IL_006a;
		}
	}
	{
		return NULL;
	}

IL_006a:
	{
		Il2CppObject * L_13 = V_0;
		Il2CppObject * L_14 = V_1;
		Pair_t771337780 * L_15 = (Pair_t771337780 *)il2cpp_codegen_object_new(Pair_t771337780_il2cpp_TypeInfo_var);
		Pair__ctor_m2201458026(L_15, L_13, L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
// System.Void System.Web.UI.WebControls.WebControl::TrackViewState()
extern "C"  void WebControl_TrackViewState_m313668565 (WebControl_t2705811671 * __this, const MethodInfo* method)
{
	{
		Style_t3589053173 * L_0 = __this->get_style_28();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Style_t3589053173 * L_1 = __this->get_style_28();
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(20 /* System.Void System.Web.UI.WebControls.Style::TrackViewState() */, L_1);
	}

IL_0016:
	{
		StateBag_t282928164 * L_2 = __this->get_attribute_state_32();
		if (!L_2)
		{
			goto IL_0038;
		}
	}
	{
		StateBag_t282928164 * L_3 = __this->get_attribute_state_32();
		NullCheck(L_3);
		StateBag_TrackViewState_m3806295084(L_3, /*hidden argument*/NULL);
		StateBag_t282928164 * L_4 = __this->get_attribute_state_32();
		NullCheck(L_4);
		StateBag_SetDirty_m3657863186(L_4, (bool)1, /*hidden argument*/NULL);
	}

IL_0038:
	{
		Control_TrackViewState_m4143870237(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Web.UI.WebResourceAttribute::.ctor(System.String,System.String)
extern "C"  void WebResourceAttribute__ctor_m426876041 (WebResourceAttribute_t4132945045 * __this, String_t* ___webResource0, String_t* ___contentType1, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___webResource0;
		__this->set_webResource_1(L_0);
		String_t* L_1 = ___contentType1;
		__this->set_contentType_2(L_1);
		return;
	}
}
// System.String System.Web.UI.WebResourceAttribute::get_ContentType()
extern "C"  String_t* WebResourceAttribute_get_ContentType_m2970806472 (WebResourceAttribute_t4132945045 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_contentType_2();
		return L_0;
	}
}
// System.Boolean System.Web.UI.WebResourceAttribute::get_PerformSubstitution()
extern "C"  bool WebResourceAttribute_get_PerformSubstitution_m2222296524 (WebResourceAttribute_t4132945045 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_performSubstitution_0();
		return L_0;
	}
}
// System.String System.Web.UI.WebResourceAttribute::get_WebResource()
extern "C"  String_t* WebResourceAttribute_get_WebResource_m442044088 (WebResourceAttribute_t4132945045 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_webResource_1();
		return L_0;
	}
}
// System.Boolean System.Web.UplevelHelper::IsUplevel(System.String)
extern "C"  bool UplevelHelper_IsUplevel_m1445892338 (Il2CppObject * __this /* static, unused */, String_t* ___ua0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		String_t* L_0 = ___ua0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)0;
	}

IL_0008:
	{
		String_t* L_1 = ___ua0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m3847582255(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if (L_3)
		{
			goto IL_0017;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		V_1 = (bool)0;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) <= ((int32_t)3)))
		{
			goto IL_006a;
		}
	}
	{
		String_t* L_5 = ___ua0;
		NullCheck(L_5);
		Il2CppChar L_6 = String_get_Chars_m2986988803(L_5, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)77)))))
		{
			goto IL_006a;
		}
	}
	{
		String_t* L_7 = ___ua0;
		NullCheck(L_7);
		Il2CppChar L_8 = String_get_Chars_m2986988803(L_7, 1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)111)))))
		{
			goto IL_006a;
		}
	}
	{
		String_t* L_9 = ___ua0;
		NullCheck(L_9);
		Il2CppChar L_10 = String_get_Chars_m2986988803(L_9, 2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_10) == ((uint32_t)((int32_t)122)))))
		{
			goto IL_006a;
		}
	}
	{
		String_t* L_11 = ___ua0;
		NullCheck(L_11);
		Il2CppChar L_12 = String_get_Chars_m2986988803(L_11, 3, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_12) == ((uint32_t)((int32_t)105)))))
		{
			goto IL_006a;
		}
	}
	{
		String_t* L_13 = ___ua0;
		int32_t L_14 = V_0;
		bool L_15 = UplevelHelper_DetermineUplevel_1_1_m1060501430(NULL /*static, unused*/, L_13, (&V_1), L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0068;
		}
	}
	{
		bool L_16 = V_1;
		return L_16;
	}

IL_0068:
	{
		return (bool)0;
	}

IL_006a:
	{
		int32_t L_17 = V_0;
		if ((((int32_t)L_17) <= ((int32_t)3)))
		{
			goto IL_00ab;
		}
	}
	{
		String_t* L_18 = ___ua0;
		NullCheck(L_18);
		Il2CppChar L_19 = String_get_Chars_m2986988803(L_18, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_19) == ((uint32_t)((int32_t)75)))))
		{
			goto IL_00ab;
		}
	}
	{
		String_t* L_20 = ___ua0;
		NullCheck(L_20);
		Il2CppChar L_21 = String_get_Chars_m2986988803(L_20, 1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_21) == ((uint32_t)((int32_t)111)))))
		{
			goto IL_00ab;
		}
	}
	{
		String_t* L_22 = ___ua0;
		NullCheck(L_22);
		Il2CppChar L_23 = String_get_Chars_m2986988803(L_22, 2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_23) == ((uint32_t)((int32_t)110)))))
		{
			goto IL_00ab;
		}
	}
	{
		String_t* L_24 = ___ua0;
		NullCheck(L_24);
		Il2CppChar L_25 = String_get_Chars_m2986988803(L_24, 3, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_25) == ((uint32_t)((int32_t)113)))))
		{
			goto IL_00ab;
		}
	}
	{
		return (bool)1;
	}

IL_00ab:
	{
		int32_t L_26 = V_0;
		if ((((int32_t)L_26) <= ((int32_t)3)))
		{
			goto IL_00ec;
		}
	}
	{
		String_t* L_27 = ___ua0;
		NullCheck(L_27);
		Il2CppChar L_28 = String_get_Chars_m2986988803(L_27, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_28) == ((uint32_t)((int32_t)79)))))
		{
			goto IL_00ec;
		}
	}
	{
		String_t* L_29 = ___ua0;
		NullCheck(L_29);
		Il2CppChar L_30 = String_get_Chars_m2986988803(L_29, 1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_30) == ((uint32_t)((int32_t)112)))))
		{
			goto IL_00ec;
		}
	}
	{
		String_t* L_31 = ___ua0;
		NullCheck(L_31);
		Il2CppChar L_32 = String_get_Chars_m2986988803(L_31, 2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_32) == ((uint32_t)((int32_t)101)))))
		{
			goto IL_00ec;
		}
	}
	{
		String_t* L_33 = ___ua0;
		NullCheck(L_33);
		Il2CppChar L_34 = String_get_Chars_m2986988803(L_33, 3, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_34) == ((uint32_t)((int32_t)114)))))
		{
			goto IL_00ec;
		}
	}
	{
		return (bool)1;
	}

IL_00ec:
	{
		return (bool)0;
	}
}
// System.Boolean System.Web.UplevelHelper::DetermineUplevel_1_1(System.String,System.Boolean&,System.Int32)
extern "C"  bool UplevelHelper_DetermineUplevel_1_1_m1060501430 (Il2CppObject * __this /* static, unused */, String_t* ___ua0, bool* ___hasJavaScript1, int32_t ___ualength2, const MethodInfo* method)
{
	{
		bool* L_0 = ___hasJavaScript1;
		*((int8_t*)(L_0)) = (int8_t)1;
		int32_t L_1 = ___ualength2;
		if ((((int32_t)L_1) <= ((int32_t)((int32_t)10))))
		{
			goto IL_0147;
		}
	}
	{
		String_t* L_2 = ___ua0;
		NullCheck(L_2);
		Il2CppChar L_3 = String_get_Chars_m2986988803(L_2, 7, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_0147;
		}
	}
	{
		String_t* L_4 = ___ua0;
		NullCheck(L_4);
		Il2CppChar L_5 = String_get_Chars_m2986988803(L_4, 8, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)52)))))
		{
			goto IL_0147;
		}
	}
	{
		String_t* L_6 = ___ua0;
		NullCheck(L_6);
		Il2CppChar L_7 = String_get_Chars_m2986988803(L_6, ((int32_t)9), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)46)))))
		{
			goto IL_0147;
		}
	}
	{
		String_t* L_8 = ___ua0;
		NullCheck(L_8);
		Il2CppChar L_9 = String_get_Chars_m2986988803(L_8, ((int32_t)10), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)48)))))
		{
			goto IL_0147;
		}
	}
	{
		int32_t L_10 = ___ualength2;
		if ((((int32_t)L_10) <= ((int32_t)((int32_t)28))))
		{
			goto IL_0142;
		}
	}
	{
		String_t* L_11 = ___ua0;
		NullCheck(L_11);
		Il2CppChar L_12 = String_get_Chars_m2986988803(L_11, ((int32_t)13), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_12) == ((uint32_t)((int32_t)65)))))
		{
			goto IL_0142;
		}
	}
	{
		String_t* L_13 = ___ua0;
		NullCheck(L_13);
		Il2CppChar L_14 = String_get_Chars_m2986988803(L_13, ((int32_t)14), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)99)))))
		{
			goto IL_0142;
		}
	}
	{
		String_t* L_15 = ___ua0;
		NullCheck(L_15);
		Il2CppChar L_16 = String_get_Chars_m2986988803(L_15, ((int32_t)15), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_16) == ((uint32_t)((int32_t)116)))))
		{
			goto IL_0142;
		}
	}
	{
		String_t* L_17 = ___ua0;
		NullCheck(L_17);
		Il2CppChar L_18 = String_get_Chars_m2986988803(L_17, ((int32_t)16), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)105)))))
		{
			goto IL_0142;
		}
	}
	{
		String_t* L_19 = ___ua0;
		NullCheck(L_19);
		Il2CppChar L_20 = String_get_Chars_m2986988803(L_19, ((int32_t)17), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_20) == ((uint32_t)((int32_t)118)))))
		{
			goto IL_0142;
		}
	}
	{
		String_t* L_21 = ___ua0;
		NullCheck(L_21);
		Il2CppChar L_22 = String_get_Chars_m2986988803(L_21, ((int32_t)18), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)101)))))
		{
			goto IL_0142;
		}
	}
	{
		String_t* L_23 = ___ua0;
		NullCheck(L_23);
		Il2CppChar L_24 = String_get_Chars_m2986988803(L_23, ((int32_t)19), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_24) == ((uint32_t)((int32_t)84)))))
		{
			goto IL_0142;
		}
	}
	{
		String_t* L_25 = ___ua0;
		NullCheck(L_25);
		Il2CppChar L_26 = String_get_Chars_m2986988803(L_25, ((int32_t)20), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_26) == ((uint32_t)((int32_t)111)))))
		{
			goto IL_0142;
		}
	}
	{
		String_t* L_27 = ___ua0;
		NullCheck(L_27);
		Il2CppChar L_28 = String_get_Chars_m2986988803(L_27, ((int32_t)21), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_28) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_0142;
		}
	}
	{
		String_t* L_29 = ___ua0;
		NullCheck(L_29);
		Il2CppChar L_30 = String_get_Chars_m2986988803(L_29, ((int32_t)22), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_30) == ((uint32_t)((int32_t)114)))))
		{
			goto IL_0142;
		}
	}
	{
		String_t* L_31 = ___ua0;
		NullCheck(L_31);
		Il2CppChar L_32 = String_get_Chars_m2986988803(L_31, ((int32_t)23), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_32) == ((uint32_t)((int32_t)105)))))
		{
			goto IL_0142;
		}
	}
	{
		String_t* L_33 = ___ua0;
		NullCheck(L_33);
		Il2CppChar L_34 = String_get_Chars_m2986988803(L_33, ((int32_t)24), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_34) == ((uint32_t)((int32_t)115)))))
		{
			goto IL_0142;
		}
	}
	{
		String_t* L_35 = ___ua0;
		NullCheck(L_35);
		Il2CppChar L_36 = String_get_Chars_m2986988803(L_35, ((int32_t)25), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_36) == ((uint32_t)((int32_t)116)))))
		{
			goto IL_0142;
		}
	}
	{
		String_t* L_37 = ___ua0;
		NullCheck(L_37);
		Il2CppChar L_38 = String_get_Chars_m2986988803(L_37, ((int32_t)26), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_38) == ((uint32_t)((int32_t)66)))))
		{
			goto IL_0142;
		}
	}
	{
		String_t* L_39 = ___ua0;
		NullCheck(L_39);
		Il2CppChar L_40 = String_get_Chars_m2986988803(L_39, ((int32_t)27), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_40) == ((uint32_t)((int32_t)111)))))
		{
			goto IL_0142;
		}
	}
	{
		String_t* L_41 = ___ua0;
		NullCheck(L_41);
		Il2CppChar L_42 = String_get_Chars_m2986988803(L_41, ((int32_t)28), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_42) == ((uint32_t)((int32_t)116)))))
		{
			goto IL_0142;
		}
	}
	{
		bool* L_43 = ___hasJavaScript1;
		*((int8_t*)(L_43)) = (int8_t)0;
		return (bool)1;
	}

IL_0142:
	{
		bool* L_44 = ___hasJavaScript1;
		*((int8_t*)(L_44)) = (int8_t)1;
		return (bool)1;
	}

IL_0147:
	{
		String_t* L_45 = ___ua0;
		bool* L_46 = ___hasJavaScript1;
		int32_t L_47 = ___ualength2;
		bool L_48 = UplevelHelper_ScanForMatch_2_2_m3964995597(NULL /*static, unused*/, L_45, L_46, L_47, /*hidden argument*/NULL);
		if (!L_48)
		{
			goto IL_0156;
		}
	}
	{
		return (bool)1;
	}

IL_0156:
	{
		String_t* L_49 = ___ua0;
		bool* L_50 = ___hasJavaScript1;
		int32_t L_51 = ___ualength2;
		bool L_52 = UplevelHelper_ScanForMatch_2_3_m4034542246(NULL /*static, unused*/, L_49, L_50, L_51, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_0165;
		}
	}
	{
		return (bool)1;
	}

IL_0165:
	{
		int32_t L_53 = ___ualength2;
		if ((((int32_t)L_53) <= ((int32_t)((int32_t)15))))
		{
			goto IL_01ae;
		}
	}
	{
		String_t* L_54 = ___ua0;
		NullCheck(L_54);
		Il2CppChar L_55 = String_get_Chars_m2986988803(L_54, ((int32_t)12), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_55) == ((uint32_t)((int32_t)40)))))
		{
			goto IL_01ae;
		}
	}
	{
		String_t* L_56 = ___ua0;
		NullCheck(L_56);
		Il2CppChar L_57 = String_get_Chars_m2986988803(L_56, ((int32_t)13), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_57) == ((uint32_t)((int32_t)77)))))
		{
			goto IL_01ae;
		}
	}
	{
		String_t* L_58 = ___ua0;
		NullCheck(L_58);
		Il2CppChar L_59 = String_get_Chars_m2986988803(L_58, ((int32_t)14), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_59) == ((uint32_t)((int32_t)97)))))
		{
			goto IL_01ae;
		}
	}
	{
		String_t* L_60 = ___ua0;
		NullCheck(L_60);
		Il2CppChar L_61 = String_get_Chars_m2986988803(L_60, ((int32_t)15), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_61) == ((uint32_t)((int32_t)99)))))
		{
			goto IL_01ae;
		}
	}
	{
		bool* L_62 = ___hasJavaScript1;
		*((int8_t*)(L_62)) = (int8_t)1;
		return (bool)1;
	}

IL_01ae:
	{
		String_t* L_63 = ___ua0;
		bool* L_64 = ___hasJavaScript1;
		int32_t L_65 = ___ualength2;
		bool L_66 = UplevelHelper_ScanForMatch_2_5_m864262504(NULL /*static, unused*/, L_63, L_64, L_65, /*hidden argument*/NULL);
		if (!L_66)
		{
			goto IL_01bd;
		}
	}
	{
		return (bool)1;
	}

IL_01bd:
	{
		int32_t L_67 = ___ualength2;
		if ((((int32_t)L_67) <= ((int32_t)((int32_t)15))))
		{
			goto IL_0206;
		}
	}
	{
		String_t* L_68 = ___ua0;
		NullCheck(L_68);
		Il2CppChar L_69 = String_get_Chars_m2986988803(L_68, ((int32_t)12), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_69) == ((uint32_t)((int32_t)71)))))
		{
			goto IL_0206;
		}
	}
	{
		String_t* L_70 = ___ua0;
		NullCheck(L_70);
		Il2CppChar L_71 = String_get_Chars_m2986988803(L_70, ((int32_t)13), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_71) == ((uint32_t)((int32_t)97)))))
		{
			goto IL_0206;
		}
	}
	{
		String_t* L_72 = ___ua0;
		NullCheck(L_72);
		Il2CppChar L_73 = String_get_Chars_m2986988803(L_72, ((int32_t)14), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_73) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_0206;
		}
	}
	{
		String_t* L_74 = ___ua0;
		NullCheck(L_74);
		Il2CppChar L_75 = String_get_Chars_m2986988803(L_74, ((int32_t)15), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_75) == ((uint32_t)((int32_t)101)))))
		{
			goto IL_0206;
		}
	}
	{
		bool* L_76 = ___hasJavaScript1;
		*((int8_t*)(L_76)) = (int8_t)1;
		return (bool)1;
	}

IL_0206:
	{
		int32_t L_77 = ___ualength2;
		if ((((int32_t)L_77) <= ((int32_t)((int32_t)28))))
		{
			goto IL_024f;
		}
	}
	{
		String_t* L_78 = ___ua0;
		NullCheck(L_78);
		Il2CppChar L_79 = String_get_Chars_m2986988803(L_78, ((int32_t)25), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_79) == ((uint32_t)((int32_t)75)))))
		{
			goto IL_024f;
		}
	}
	{
		String_t* L_80 = ___ua0;
		NullCheck(L_80);
		Il2CppChar L_81 = String_get_Chars_m2986988803(L_80, ((int32_t)26), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_81) == ((uint32_t)((int32_t)111)))))
		{
			goto IL_024f;
		}
	}
	{
		String_t* L_82 = ___ua0;
		NullCheck(L_82);
		Il2CppChar L_83 = String_get_Chars_m2986988803(L_82, ((int32_t)27), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_83) == ((uint32_t)((int32_t)110)))))
		{
			goto IL_024f;
		}
	}
	{
		String_t* L_84 = ___ua0;
		NullCheck(L_84);
		Il2CppChar L_85 = String_get_Chars_m2986988803(L_84, ((int32_t)28), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_85) == ((uint32_t)((int32_t)113)))))
		{
			goto IL_024f;
		}
	}
	{
		bool* L_86 = ___hasJavaScript1;
		*((int8_t*)(L_86)) = (int8_t)1;
		return (bool)1;
	}

IL_024f:
	{
		int32_t L_87 = ___ualength2;
		if ((((int32_t)L_87) <= ((int32_t)((int32_t)12))))
		{
			goto IL_0298;
		}
	}
	{
		String_t* L_88 = ___ua0;
		NullCheck(L_88);
		Il2CppChar L_89 = String_get_Chars_m2986988803(L_88, ((int32_t)9), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_89) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_0298;
		}
	}
	{
		String_t* L_90 = ___ua0;
		NullCheck(L_90);
		Il2CppChar L_91 = String_get_Chars_m2986988803(L_90, ((int32_t)10), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_91) == ((uint32_t)((int32_t)52)))))
		{
			goto IL_0298;
		}
	}
	{
		String_t* L_92 = ___ua0;
		NullCheck(L_92);
		Il2CppChar L_93 = String_get_Chars_m2986988803(L_92, ((int32_t)11), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_93) == ((uint32_t)((int32_t)46)))))
		{
			goto IL_0298;
		}
	}
	{
		String_t* L_94 = ___ua0;
		NullCheck(L_94);
		Il2CppChar L_95 = String_get_Chars_m2986988803(L_94, ((int32_t)12), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_95) == ((uint32_t)((int32_t)91)))))
		{
			goto IL_0298;
		}
	}
	{
		bool* L_96 = ___hasJavaScript1;
		*((int8_t*)(L_96)) = (int8_t)1;
		return (bool)1;
	}

IL_0298:
	{
		return (bool)0;
	}
}
// System.Boolean System.Web.UplevelHelper::ScanForMatch_2_2(System.String,System.Boolean&,System.Int32)
extern "C"  bool UplevelHelper_ScanForMatch_2_2_m3964995597 (Il2CppObject * __this /* static, unused */, String_t* ___ua0, bool* ___hasJavaScript1, int32_t ___ualength2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		bool* L_0 = ___hasJavaScript1;
		*((int8_t*)(L_0)) = (int8_t)1;
		int32_t L_1 = ___ualength2;
		if ((((int32_t)L_1) >= ((int32_t)((int32_t)25))))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = 0;
		int32_t L_2 = V_0;
		V_1 = ((int32_t)((int32_t)L_2+(int32_t)7));
		int32_t L_3 = ___ualength2;
		V_2 = L_3;
		goto IL_00a7;
	}

IL_001a:
	{
		String_t* L_4 = ___ua0;
		int32_t L_5 = V_0;
		NullCheck(L_4);
		Il2CppChar L_6 = String_get_Chars_m2986988803(L_4, L_5, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)41)))))
		{
			goto IL_009b;
		}
	}
	{
		String_t* L_7 = ___ua0;
		int32_t L_8 = V_1;
		NullCheck(L_7);
		Il2CppChar L_9 = String_get_Chars_m2986988803(L_7, L_8, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_009b;
		}
	}
	{
		String_t* L_10 = ___ua0;
		int32_t L_11 = V_0;
		NullCheck(L_10);
		Il2CppChar L_12 = String_get_Chars_m2986988803(L_10, ((int32_t)((int32_t)L_11+(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_12) == ((uint32_t)((int32_t)32)))))
		{
			goto IL_009b;
		}
	}
	{
		String_t* L_13 = ___ua0;
		int32_t L_14 = V_1;
		NullCheck(L_13);
		Il2CppChar L_15 = String_get_Chars_m2986988803(L_13, ((int32_t)((int32_t)L_14-(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)111)))))
		{
			goto IL_009b;
		}
	}
	{
		String_t* L_16 = ___ua0;
		int32_t L_17 = V_0;
		NullCheck(L_16);
		Il2CppChar L_18 = String_get_Chars_m2986988803(L_16, ((int32_t)((int32_t)L_17+(int32_t)2)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)71)))))
		{
			goto IL_009b;
		}
	}
	{
		String_t* L_19 = ___ua0;
		int32_t L_20 = V_1;
		NullCheck(L_19);
		Il2CppChar L_21 = String_get_Chars_m2986988803(L_19, ((int32_t)((int32_t)L_20-(int32_t)2)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_21) == ((uint32_t)((int32_t)107)))))
		{
			goto IL_009b;
		}
	}
	{
		String_t* L_22 = ___ua0;
		int32_t L_23 = V_0;
		NullCheck(L_22);
		Il2CppChar L_24 = String_get_Chars_m2986988803(L_22, ((int32_t)((int32_t)L_23+(int32_t)4)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_24) == ((uint32_t)((int32_t)99)))))
		{
			goto IL_009b;
		}
	}
	{
		String_t* L_25 = ___ua0;
		int32_t L_26 = V_1;
		NullCheck(L_25);
		Il2CppChar L_27 = String_get_Chars_m2986988803(L_25, ((int32_t)((int32_t)L_26-(int32_t)4)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_27) == ((uint32_t)((int32_t)101)))))
		{
			goto IL_009b;
		}
	}
	{
		bool* L_28 = ___hasJavaScript1;
		*((int8_t*)(L_28)) = (int8_t)1;
		return (bool)1;
	}

IL_009b:
	{
		int32_t L_29 = V_0;
		V_0 = ((int32_t)((int32_t)L_29+(int32_t)1));
		int32_t L_30 = V_1;
		V_1 = ((int32_t)((int32_t)L_30+(int32_t)1));
		int32_t L_31 = V_2;
		V_2 = ((int32_t)((int32_t)L_31-(int32_t)1));
	}

IL_00a7:
	{
		int32_t L_32 = V_2;
		if ((((int32_t)L_32) >= ((int32_t)8)))
		{
			goto IL_001a;
		}
	}
	{
		return (bool)0;
	}
}
// System.Boolean System.Web.UplevelHelper::ScanForMatch_2_3(System.String,System.Boolean&,System.Int32)
extern "C"  bool UplevelHelper_ScanForMatch_2_3_m4034542246 (Il2CppObject * __this /* static, unused */, String_t* ___ua0, bool* ___hasJavaScript1, int32_t ___ualength2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		bool* L_0 = ___hasJavaScript1;
		*((int8_t*)(L_0)) = (int8_t)1;
		int32_t L_1 = ___ualength2;
		if ((((int32_t)L_1) >= ((int32_t)((int32_t)24))))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = 0;
		int32_t L_2 = V_0;
		V_1 = ((int32_t)((int32_t)L_2+(int32_t)6));
		int32_t L_3 = ___ualength2;
		V_2 = L_3;
		goto IL_0097;
	}

IL_001a:
	{
		String_t* L_4 = ___ua0;
		int32_t L_5 = V_0;
		NullCheck(L_4);
		Il2CppChar L_6 = String_get_Chars_m2986988803(L_4, L_5, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)41)))))
		{
			goto IL_008b;
		}
	}
	{
		String_t* L_7 = ___ua0;
		int32_t L_8 = V_1;
		NullCheck(L_7);
		Il2CppChar L_9 = String_get_Chars_m2986988803(L_7, L_8, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)97)))))
		{
			goto IL_008b;
		}
	}
	{
		String_t* L_10 = ___ua0;
		int32_t L_11 = V_0;
		NullCheck(L_10);
		Il2CppChar L_12 = String_get_Chars_m2986988803(L_10, ((int32_t)((int32_t)L_11+(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_12) == ((uint32_t)((int32_t)32)))))
		{
			goto IL_008b;
		}
	}
	{
		String_t* L_13 = ___ua0;
		int32_t L_14 = V_1;
		NullCheck(L_13);
		Il2CppChar L_15 = String_get_Chars_m2986988803(L_13, ((int32_t)((int32_t)L_14-(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)114)))))
		{
			goto IL_008b;
		}
	}
	{
		String_t* L_16 = ___ua0;
		int32_t L_17 = V_0;
		NullCheck(L_16);
		Il2CppChar L_18 = String_get_Chars_m2986988803(L_16, ((int32_t)((int32_t)L_17+(int32_t)2)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)79)))))
		{
			goto IL_008b;
		}
	}
	{
		String_t* L_19 = ___ua0;
		int32_t L_20 = V_1;
		NullCheck(L_19);
		Il2CppChar L_21 = String_get_Chars_m2986988803(L_19, ((int32_t)((int32_t)L_20-(int32_t)2)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_21) == ((uint32_t)((int32_t)101)))))
		{
			goto IL_008b;
		}
	}
	{
		String_t* L_22 = ___ua0;
		int32_t L_23 = V_0;
		NullCheck(L_22);
		Il2CppChar L_24 = String_get_Chars_m2986988803(L_22, ((int32_t)((int32_t)L_23+(int32_t)3)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_24) == ((uint32_t)((int32_t)112)))))
		{
			goto IL_008b;
		}
	}
	{
		bool* L_25 = ___hasJavaScript1;
		*((int8_t*)(L_25)) = (int8_t)1;
		return (bool)1;
	}

IL_008b:
	{
		int32_t L_26 = V_0;
		V_0 = ((int32_t)((int32_t)L_26+(int32_t)1));
		int32_t L_27 = V_1;
		V_1 = ((int32_t)((int32_t)L_27+(int32_t)1));
		int32_t L_28 = V_2;
		V_2 = ((int32_t)((int32_t)L_28-(int32_t)1));
	}

IL_0097:
	{
		int32_t L_29 = V_2;
		if ((((int32_t)L_29) >= ((int32_t)7)))
		{
			goto IL_001a;
		}
	}
	{
		return (bool)0;
	}
}
// System.Boolean System.Web.UplevelHelper::ScanForMatch_2_5(System.String,System.Boolean&,System.Int32)
extern "C"  bool UplevelHelper_ScanForMatch_2_5_m864262504 (Il2CppObject * __this /* static, unused */, String_t* ___ua0, bool* ___hasJavaScript1, int32_t ___ualength2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		bool* L_0 = ___hasJavaScript1;
		*((int8_t*)(L_0)) = (int8_t)1;
		int32_t L_1 = ___ualength2;
		if ((((int32_t)L_1) >= ((int32_t)((int32_t)21))))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = 0;
		int32_t L_2 = V_0;
		V_1 = ((int32_t)((int32_t)L_2+(int32_t)5));
		int32_t L_3 = ___ualength2;
		V_2 = L_3;
		goto IL_0087;
	}

IL_001a:
	{
		String_t* L_4 = ___ua0;
		int32_t L_5 = V_0;
		NullCheck(L_4);
		Il2CppChar L_6 = String_get_Chars_m2986988803(L_4, L_5, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)40)))))
		{
			goto IL_007b;
		}
	}
	{
		String_t* L_7 = ___ua0;
		int32_t L_8 = V_1;
		NullCheck(L_7);
		Il2CppChar L_9 = String_get_Chars_m2986988803(L_7, L_8, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)76)))))
		{
			goto IL_007b;
		}
	}
	{
		String_t* L_10 = ___ua0;
		int32_t L_11 = V_0;
		NullCheck(L_10);
		Il2CppChar L_12 = String_get_Chars_m2986988803(L_10, ((int32_t)((int32_t)L_11+(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_12) == ((uint32_t)((int32_t)75)))))
		{
			goto IL_007b;
		}
	}
	{
		String_t* L_13 = ___ua0;
		int32_t L_14 = V_1;
		NullCheck(L_13);
		Il2CppChar L_15 = String_get_Chars_m2986988803(L_13, ((int32_t)((int32_t)L_14-(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)77)))))
		{
			goto IL_007b;
		}
	}
	{
		String_t* L_16 = ___ua0;
		int32_t L_17 = V_0;
		NullCheck(L_16);
		Il2CppChar L_18 = String_get_Chars_m2986988803(L_16, ((int32_t)((int32_t)L_17+(int32_t)3)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)84)))))
		{
			goto IL_007b;
		}
	}
	{
		String_t* L_19 = ___ua0;
		int32_t L_20 = V_1;
		NullCheck(L_19);
		Il2CppChar L_21 = String_get_Chars_m2986988803(L_19, ((int32_t)((int32_t)L_20-(int32_t)3)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_21) == ((uint32_t)((int32_t)72)))))
		{
			goto IL_007b;
		}
	}
	{
		bool* L_22 = ___hasJavaScript1;
		*((int8_t*)(L_22)) = (int8_t)1;
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_23 = V_0;
		V_0 = ((int32_t)((int32_t)L_23+(int32_t)1));
		int32_t L_24 = V_1;
		V_1 = ((int32_t)((int32_t)L_24+(int32_t)1));
		int32_t L_25 = V_2;
		V_2 = ((int32_t)((int32_t)L_25-(int32_t)1));
	}

IL_0087:
	{
		int32_t L_26 = V_2;
		if ((((int32_t)L_26) >= ((int32_t)6)))
		{
			goto IL_001a;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void System.Web.Util.AltSerialization::Serialize(System.IO.BinaryWriter,System.Object)
extern "C"  void AltSerialization_Serialize_m3935920895 (Il2CppObject * __this /* static, unused */, BinaryWriter_t3992595042 * ___w0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AltSerialization_Serialize_m3935920895_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	BinaryFormatter_t3197753202 * V_1 = NULL;
	int32_t V_2 = 0;
	DateTime_t3738529785  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t G_B3_0 = 0;
	{
		Il2CppObject * L_0 = ___value1;
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = ___value1;
		NullCheck(L_1);
		Type_t * L_2 = Object_GetType_m88164663(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_3 = Type_GetTypeCode_m480753082(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0017;
	}

IL_0016:
	{
		G_B3_0 = 0;
	}

IL_0017:
	{
		V_0 = G_B3_0;
		BinaryWriter_t3992595042 * L_4 = ___w0;
		int32_t L_5 = V_0;
		NullCheck(L_4);
		VirtActionInvoker1< uint8_t >::Invoke(10 /* System.Void System.IO.BinaryWriter::Write(System.Byte) */, L_4, (uint8_t)(((int32_t)((uint8_t)L_5))));
		int32_t L_6 = V_0;
		V_2 = L_6;
		int32_t L_7 = V_2;
		switch (L_7)
		{
			case 0:
			{
				goto IL_00ec;
			}
			case 1:
			{
				goto IL_0124;
			}
			case 2:
			{
				goto IL_00c5;
			}
			case 3:
			{
				goto IL_0079;
			}
			case 4:
			{
				goto IL_009b;
			}
			case 5:
			{
				goto IL_013c;
			}
			case 6:
			{
				goto IL_008a;
			}
			case 7:
			{
				goto IL_00f1;
			}
			case 8:
			{
				goto IL_016f;
			}
			case 9:
			{
				goto IL_0102;
			}
			case 10:
			{
				goto IL_0180;
			}
			case 11:
			{
				goto IL_0113;
			}
			case 12:
			{
				goto IL_0191;
			}
			case 13:
			{
				goto IL_014d;
			}
			case 14:
			{
				goto IL_00db;
			}
			case 15:
			{
				goto IL_00ca;
			}
			case 16:
			{
				goto IL_00ac;
			}
			case 17:
			{
				goto IL_01a2;
			}
			case 18:
			{
				goto IL_015e;
			}
		}
	}
	{
		goto IL_01a2;
	}

IL_0079:
	{
		BinaryWriter_t3992595042 * L_8 = ___w0;
		Il2CppObject * L_9 = ___value1;
		NullCheck(L_8);
		VirtActionInvoker1< bool >::Invoke(9 /* System.Void System.IO.BinaryWriter::Write(System.Boolean) */, L_8, ((*(bool*)((bool*)UnBox(L_9, Boolean_t97287965_il2cpp_TypeInfo_var)))));
		goto IL_01a2;
	}

IL_008a:
	{
		BinaryWriter_t3992595042 * L_10 = ___w0;
		Il2CppObject * L_11 = ___value1;
		NullCheck(L_10);
		VirtActionInvoker1< uint8_t >::Invoke(10 /* System.Void System.IO.BinaryWriter::Write(System.Byte) */, L_10, ((*(uint8_t*)((uint8_t*)UnBox(L_11, Byte_t1134296376_il2cpp_TypeInfo_var)))));
		goto IL_01a2;
	}

IL_009b:
	{
		BinaryWriter_t3992595042 * L_12 = ___w0;
		Il2CppObject * L_13 = ___value1;
		NullCheck(L_12);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.BinaryWriter::Write(System.Char) */, L_12, ((*(Il2CppChar*)((Il2CppChar*)UnBox(L_13, Char_t3634460470_il2cpp_TypeInfo_var)))));
		goto IL_01a2;
	}

IL_00ac:
	{
		BinaryWriter_t3992595042 * L_14 = ___w0;
		Il2CppObject * L_15 = ___value1;
		V_3 = ((*(DateTime_t3738529785 *)((DateTime_t3738529785 *)UnBox(L_15, DateTime_t3738529785_il2cpp_TypeInfo_var))));
		int64_t L_16 = DateTime_get_Ticks_m1550640881((&V_3), /*hidden argument*/NULL);
		NullCheck(L_14);
		VirtActionInvoker1< int64_t >::Invoke(20 /* System.Void System.IO.BinaryWriter::Write(System.Int64) */, L_14, L_16);
		goto IL_01a2;
	}

IL_00c5:
	{
		goto IL_01a2;
	}

IL_00ca:
	{
		BinaryWriter_t3992595042 * L_17 = ___w0;
		Il2CppObject * L_18 = ___value1;
		NullCheck(L_17);
		VirtActionInvoker1< Decimal_t2948259380  >::Invoke(16 /* System.Void System.IO.BinaryWriter::Write(System.Decimal) */, L_17, ((*(Decimal_t2948259380 *)((Decimal_t2948259380 *)UnBox(L_18, Decimal_t2948259380_il2cpp_TypeInfo_var)))));
		goto IL_01a2;
	}

IL_00db:
	{
		BinaryWriter_t3992595042 * L_19 = ___w0;
		Il2CppObject * L_20 = ___value1;
		NullCheck(L_19);
		VirtActionInvoker1< double >::Invoke(17 /* System.Void System.IO.BinaryWriter::Write(System.Double) */, L_19, ((*(double*)((double*)UnBox(L_20, Double_t594665363_il2cpp_TypeInfo_var)))));
		goto IL_01a2;
	}

IL_00ec:
	{
		goto IL_01a2;
	}

IL_00f1:
	{
		BinaryWriter_t3992595042 * L_21 = ___w0;
		Il2CppObject * L_22 = ___value1;
		NullCheck(L_21);
		VirtActionInvoker1< int16_t >::Invoke(18 /* System.Void System.IO.BinaryWriter::Write(System.Int16) */, L_21, ((*(int16_t*)((int16_t*)UnBox(L_22, Int16_t2552820387_il2cpp_TypeInfo_var)))));
		goto IL_01a2;
	}

IL_0102:
	{
		BinaryWriter_t3992595042 * L_23 = ___w0;
		Il2CppObject * L_24 = ___value1;
		NullCheck(L_23);
		VirtActionInvoker1< int32_t >::Invoke(19 /* System.Void System.IO.BinaryWriter::Write(System.Int32) */, L_23, ((*(int32_t*)((int32_t*)UnBox(L_24, Int32_t2950945753_il2cpp_TypeInfo_var)))));
		goto IL_01a2;
	}

IL_0113:
	{
		BinaryWriter_t3992595042 * L_25 = ___w0;
		Il2CppObject * L_26 = ___value1;
		NullCheck(L_25);
		VirtActionInvoker1< int64_t >::Invoke(20 /* System.Void System.IO.BinaryWriter::Write(System.Int64) */, L_25, ((*(int64_t*)((int64_t*)UnBox(L_26, Int64_t3736567304_il2cpp_TypeInfo_var)))));
		goto IL_01a2;
	}

IL_0124:
	{
		BinaryFormatter_t3197753202 * L_27 = (BinaryFormatter_t3197753202 *)il2cpp_codegen_object_new(BinaryFormatter_t3197753202_il2cpp_TypeInfo_var);
		BinaryFormatter__ctor_m971003555(L_27, /*hidden argument*/NULL);
		V_1 = L_27;
		BinaryFormatter_t3197753202 * L_28 = V_1;
		BinaryWriter_t3992595042 * L_29 = ___w0;
		NullCheck(L_29);
		Stream_t1273022909 * L_30 = VirtFuncInvoker0< Stream_t1273022909 * >::Invoke(5 /* System.IO.Stream System.IO.BinaryWriter::get_BaseStream() */, L_29);
		Il2CppObject * L_31 = ___value1;
		NullCheck(L_28);
		BinaryFormatter_Serialize_m1744386044(L_28, L_30, L_31, /*hidden argument*/NULL);
		goto IL_01a2;
	}

IL_013c:
	{
		BinaryWriter_t3992595042 * L_32 = ___w0;
		Il2CppObject * L_33 = ___value1;
		NullCheck(L_32);
		VirtActionInvoker1< int8_t >::Invoke(21 /* System.Void System.IO.BinaryWriter::Write(System.SByte) */, L_32, ((*(int8_t*)((int8_t*)UnBox(L_33, SByte_t1669577662_il2cpp_TypeInfo_var)))));
		goto IL_01a2;
	}

IL_014d:
	{
		BinaryWriter_t3992595042 * L_34 = ___w0;
		Il2CppObject * L_35 = ___value1;
		NullCheck(L_34);
		VirtActionInvoker1< float >::Invoke(22 /* System.Void System.IO.BinaryWriter::Write(System.Single) */, L_34, ((*(float*)((float*)UnBox(L_35, Single_t1397266774_il2cpp_TypeInfo_var)))));
		goto IL_01a2;
	}

IL_015e:
	{
		BinaryWriter_t3992595042 * L_36 = ___w0;
		Il2CppObject * L_37 = ___value1;
		NullCheck(L_36);
		VirtActionInvoker1< String_t* >::Invoke(23 /* System.Void System.IO.BinaryWriter::Write(System.String) */, L_36, ((String_t*)CastclassSealed(L_37, String_t_il2cpp_TypeInfo_var)));
		goto IL_01a2;
	}

IL_016f:
	{
		BinaryWriter_t3992595042 * L_38 = ___w0;
		Il2CppObject * L_39 = ___value1;
		NullCheck(L_38);
		VirtActionInvoker1< uint16_t >::Invoke(24 /* System.Void System.IO.BinaryWriter::Write(System.UInt16) */, L_38, ((*(uint16_t*)((uint16_t*)UnBox(L_39, UInt16_t2177724958_il2cpp_TypeInfo_var)))));
		goto IL_01a2;
	}

IL_0180:
	{
		BinaryWriter_t3992595042 * L_40 = ___w0;
		Il2CppObject * L_41 = ___value1;
		NullCheck(L_40);
		VirtActionInvoker1< uint32_t >::Invoke(25 /* System.Void System.IO.BinaryWriter::Write(System.UInt32) */, L_40, ((*(uint32_t*)((uint32_t*)UnBox(L_41, UInt32_t2560061978_il2cpp_TypeInfo_var)))));
		goto IL_01a2;
	}

IL_0191:
	{
		BinaryWriter_t3992595042 * L_42 = ___w0;
		Il2CppObject * L_43 = ___value1;
		NullCheck(L_42);
		VirtActionInvoker1< uint64_t >::Invoke(26 /* System.Void System.IO.BinaryWriter::Write(System.UInt64) */, L_42, ((*(uint64_t*)((uint64_t*)UnBox(L_43, UInt64_t4134040092_il2cpp_TypeInfo_var)))));
		goto IL_01a2;
	}

IL_01a2:
	{
		return;
	}
}
// System.Object System.Web.Util.AltSerialization::Deserialize(System.IO.BinaryReader)
extern "C"  Il2CppObject * AltSerialization_Deserialize_m3371567354 (Il2CppObject * __this /* static, unused */, BinaryReader_t2428077293 * ___r0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AltSerialization_Deserialize_m3371567354_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	BinaryFormatter_t3197753202 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		BinaryReader_t2428077293 * L_0 = ___r0;
		NullCheck(L_0);
		uint8_t L_1 = VirtFuncInvoker0< uint8_t >::Invoke(14 /* System.Byte System.IO.BinaryReader::ReadByte() */, L_0);
		V_0 = L_1;
		int32_t L_2 = V_0;
		V_2 = L_2;
		int32_t L_3 = V_2;
		switch (L_3)
		{
			case 0:
			{
				goto IL_00b3;
			}
			case 1:
			{
				goto IL_00d9;
			}
			case 2:
			{
				goto IL_0095;
			}
			case 3:
			{
				goto IL_0060;
			}
			case 4:
			{
				goto IL_0078;
			}
			case 5:
			{
				goto IL_00ec;
			}
			case 6:
			{
				goto IL_006c;
			}
			case 7:
			{
				goto IL_00b5;
			}
			case 8:
			{
				goto IL_010b;
			}
			case 9:
			{
				goto IL_00c1;
			}
			case 10:
			{
				goto IL_0117;
			}
			case 11:
			{
				goto IL_00cd;
			}
			case 12:
			{
				goto IL_0123;
			}
			case 13:
			{
				goto IL_00f8;
			}
			case 14:
			{
				goto IL_00a7;
			}
			case 15:
			{
				goto IL_009b;
			}
			case 16:
			{
				goto IL_0084;
			}
			case 17:
			{
				goto IL_012f;
			}
			case 18:
			{
				goto IL_0104;
			}
		}
	}
	{
		goto IL_012f;
	}

IL_0060:
	{
		BinaryReader_t2428077293 * L_4 = ___r0;
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(13 /* System.Boolean System.IO.BinaryReader::ReadBoolean() */, L_4);
		bool L_6 = L_5;
		Il2CppObject * L_7 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_6);
		return L_7;
	}

IL_006c:
	{
		BinaryReader_t2428077293 * L_8 = ___r0;
		NullCheck(L_8);
		uint8_t L_9 = VirtFuncInvoker0< uint8_t >::Invoke(14 /* System.Byte System.IO.BinaryReader::ReadByte() */, L_8);
		uint8_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Byte_t1134296376_il2cpp_TypeInfo_var, &L_10);
		return L_11;
	}

IL_0078:
	{
		BinaryReader_t2428077293 * L_12 = ___r0;
		NullCheck(L_12);
		Il2CppChar L_13 = VirtFuncInvoker0< Il2CppChar >::Invoke(16 /* System.Char System.IO.BinaryReader::ReadChar() */, L_12);
		Il2CppChar L_14 = L_13;
		Il2CppObject * L_15 = Box(Char_t3634460470_il2cpp_TypeInfo_var, &L_14);
		return L_15;
	}

IL_0084:
	{
		BinaryReader_t2428077293 * L_16 = ___r0;
		NullCheck(L_16);
		int64_t L_17 = VirtFuncInvoker0< int64_t >::Invoke(21 /* System.Int64 System.IO.BinaryReader::ReadInt64() */, L_16);
		DateTime_t3738529785  L_18;
		memset(&L_18, 0, sizeof(L_18));
		DateTime__ctor_m516789325(&L_18, L_17, /*hidden argument*/NULL);
		DateTime_t3738529785  L_19 = L_18;
		Il2CppObject * L_20 = Box(DateTime_t3738529785_il2cpp_TypeInfo_var, &L_19);
		return L_20;
	}

IL_0095:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DBNull_t3725197148_il2cpp_TypeInfo_var);
		DBNull_t3725197148 * L_21 = ((DBNull_t3725197148_StaticFields*)DBNull_t3725197148_il2cpp_TypeInfo_var->static_fields)->get_Value_0();
		return L_21;
	}

IL_009b:
	{
		BinaryReader_t2428077293 * L_22 = ___r0;
		NullCheck(L_22);
		Decimal_t2948259380  L_23 = VirtFuncInvoker0< Decimal_t2948259380  >::Invoke(17 /* System.Decimal System.IO.BinaryReader::ReadDecimal() */, L_22);
		Decimal_t2948259380  L_24 = L_23;
		Il2CppObject * L_25 = Box(Decimal_t2948259380_il2cpp_TypeInfo_var, &L_24);
		return L_25;
	}

IL_00a7:
	{
		BinaryReader_t2428077293 * L_26 = ___r0;
		NullCheck(L_26);
		double L_27 = VirtFuncInvoker0< double >::Invoke(18 /* System.Double System.IO.BinaryReader::ReadDouble() */, L_26);
		double L_28 = L_27;
		Il2CppObject * L_29 = Box(Double_t594665363_il2cpp_TypeInfo_var, &L_28);
		return L_29;
	}

IL_00b3:
	{
		return NULL;
	}

IL_00b5:
	{
		BinaryReader_t2428077293 * L_30 = ___r0;
		NullCheck(L_30);
		int16_t L_31 = VirtFuncInvoker0< int16_t >::Invoke(19 /* System.Int16 System.IO.BinaryReader::ReadInt16() */, L_30);
		int16_t L_32 = L_31;
		Il2CppObject * L_33 = Box(Int16_t2552820387_il2cpp_TypeInfo_var, &L_32);
		return L_33;
	}

IL_00c1:
	{
		BinaryReader_t2428077293 * L_34 = ___r0;
		NullCheck(L_34);
		int32_t L_35 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_34);
		int32_t L_36 = L_35;
		Il2CppObject * L_37 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_36);
		return L_37;
	}

IL_00cd:
	{
		BinaryReader_t2428077293 * L_38 = ___r0;
		NullCheck(L_38);
		int64_t L_39 = VirtFuncInvoker0< int64_t >::Invoke(21 /* System.Int64 System.IO.BinaryReader::ReadInt64() */, L_38);
		int64_t L_40 = L_39;
		Il2CppObject * L_41 = Box(Int64_t3736567304_il2cpp_TypeInfo_var, &L_40);
		return L_41;
	}

IL_00d9:
	{
		BinaryFormatter_t3197753202 * L_42 = (BinaryFormatter_t3197753202 *)il2cpp_codegen_object_new(BinaryFormatter_t3197753202_il2cpp_TypeInfo_var);
		BinaryFormatter__ctor_m971003555(L_42, /*hidden argument*/NULL);
		V_1 = L_42;
		BinaryFormatter_t3197753202 * L_43 = V_1;
		BinaryReader_t2428077293 * L_44 = ___r0;
		NullCheck(L_44);
		Stream_t1273022909 * L_45 = VirtFuncInvoker0< Stream_t1273022909 * >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_44);
		NullCheck(L_43);
		Il2CppObject * L_46 = BinaryFormatter_Deserialize_m193346007(L_43, L_45, /*hidden argument*/NULL);
		return L_46;
	}

IL_00ec:
	{
		BinaryReader_t2428077293 * L_47 = ___r0;
		NullCheck(L_47);
		int8_t L_48 = VirtFuncInvoker0< int8_t >::Invoke(22 /* System.SByte System.IO.BinaryReader::ReadSByte() */, L_47);
		int8_t L_49 = L_48;
		Il2CppObject * L_50 = Box(SByte_t1669577662_il2cpp_TypeInfo_var, &L_49);
		return L_50;
	}

IL_00f8:
	{
		BinaryReader_t2428077293 * L_51 = ___r0;
		NullCheck(L_51);
		float L_52 = VirtFuncInvoker0< float >::Invoke(24 /* System.Single System.IO.BinaryReader::ReadSingle() */, L_51);
		float L_53 = L_52;
		Il2CppObject * L_54 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_53);
		return L_54;
	}

IL_0104:
	{
		BinaryReader_t2428077293 * L_55 = ___r0;
		NullCheck(L_55);
		String_t* L_56 = VirtFuncInvoker0< String_t* >::Invoke(23 /* System.String System.IO.BinaryReader::ReadString() */, L_55);
		return L_56;
	}

IL_010b:
	{
		BinaryReader_t2428077293 * L_57 = ___r0;
		NullCheck(L_57);
		uint16_t L_58 = VirtFuncInvoker0< uint16_t >::Invoke(25 /* System.UInt16 System.IO.BinaryReader::ReadUInt16() */, L_57);
		uint16_t L_59 = L_58;
		Il2CppObject * L_60 = Box(UInt16_t2177724958_il2cpp_TypeInfo_var, &L_59);
		return L_60;
	}

IL_0117:
	{
		BinaryReader_t2428077293 * L_61 = ___r0;
		NullCheck(L_61);
		uint32_t L_62 = VirtFuncInvoker0< uint32_t >::Invoke(26 /* System.UInt32 System.IO.BinaryReader::ReadUInt32() */, L_61);
		uint32_t L_63 = L_62;
		Il2CppObject * L_64 = Box(UInt32_t2560061978_il2cpp_TypeInfo_var, &L_63);
		return L_64;
	}

IL_0123:
	{
		BinaryReader_t2428077293 * L_65 = ___r0;
		NullCheck(L_65);
		uint64_t L_66 = VirtFuncInvoker0< uint64_t >::Invoke(27 /* System.UInt64 System.IO.BinaryReader::ReadUInt64() */, L_65);
		uint64_t L_67 = L_66;
		Il2CppObject * L_68 = Box(UInt64_t4134040092_il2cpp_TypeInfo_var, &L_67);
		return L_68;
	}

IL_012f:
	{
		int32_t L_69 = V_0;
		int32_t L_70 = L_69;
		Il2CppObject * L_71 = Box(TypeCode_t2987224087_il2cpp_TypeInfo_var, &L_70);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_72 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral2677446575, L_71, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t777629997 * L_73 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_73, L_72, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_73);
	}
}
// System.Collections.IEnumerable System.Web.Util.DataSourceResolver::ResolveDataSource(System.Object,System.String)
extern "C"  Il2CppObject * DataSourceResolver_ResolveDataSource_m2304101392 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, String_t* ___data_member1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DataSourceResolver_ResolveDataSource_m2304101392_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	PropertyDescriptorCollection_t4164928659 * V_4 = NULL;
	PropertyDescriptor_t3244362832 * V_5 = NULL;
	PropertyDescriptor_t3244362832 * G_B14_0 = NULL;
	{
		Il2CppObject * L_0 = ___o0;
		V_0 = ((Il2CppObject *)IsInst(L_0, IEnumerable_t1941168011_il2cpp_TypeInfo_var));
		Il2CppObject * L_1 = V_0;
		if (!L_1)
		{
			goto IL_000f;
		}
	}
	{
		Il2CppObject * L_2 = V_0;
		return L_2;
	}

IL_000f:
	{
		Il2CppObject * L_3 = ___o0;
		V_1 = ((Il2CppObject *)IsInst(L_3, IListSource_t3731153367_il2cpp_TypeInfo_var));
		Il2CppObject * L_4 = V_1;
		if (L_4)
		{
			goto IL_001e;
		}
	}
	{
		return (Il2CppObject *)NULL;
	}

IL_001e:
	{
		Il2CppObject * L_5 = V_1;
		NullCheck(L_5);
		Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IList System.ComponentModel.IListSource::GetList() */, IListSource_t3731153367_il2cpp_TypeInfo_var, L_5);
		V_2 = L_6;
		Il2CppObject * L_7 = V_1;
		NullCheck(L_7);
		bool L_8 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.ComponentModel.IListSource::get_ContainsListCollection() */, IListSource_t3731153367_il2cpp_TypeInfo_var, L_7);
		if (L_8)
		{
			goto IL_0032;
		}
	}
	{
		Il2CppObject * L_9 = V_2;
		return L_9;
	}

IL_0032:
	{
		Il2CppObject * L_10 = V_2;
		V_3 = ((Il2CppObject *)IsInst(L_10, ITypedList_t3693238378_il2cpp_TypeInfo_var));
		Il2CppObject * L_11 = V_3;
		if (L_11)
		{
			goto IL_0041;
		}
	}
	{
		return (Il2CppObject *)NULL;
	}

IL_0041:
	{
		Il2CppObject * L_12 = V_3;
		NullCheck(L_12);
		PropertyDescriptorCollection_t4164928659 * L_13 = InterfaceFuncInvoker1< PropertyDescriptorCollection_t4164928659 *, PropertyDescriptorU5BU5D_t2649761905* >::Invoke(0 /* System.ComponentModel.PropertyDescriptorCollection System.ComponentModel.ITypedList::GetItemProperties(System.ComponentModel.PropertyDescriptor[]) */, ITypedList_t3693238378_il2cpp_TypeInfo_var, L_12, ((PropertyDescriptorU5BU5D_t2649761905*)SZArrayNew(PropertyDescriptorU5BU5D_t2649761905_il2cpp_TypeInfo_var, (uint32_t)0)));
		V_4 = L_13;
		PropertyDescriptorCollection_t4164928659 * L_14 = V_4;
		if (!L_14)
		{
			goto IL_0062;
		}
	}
	{
		PropertyDescriptorCollection_t4164928659 * L_15 = V_4;
		NullCheck(L_15);
		int32_t L_16 = PropertyDescriptorCollection_get_Count_m2540204170(L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_006d;
		}
	}

IL_0062:
	{
		HttpException_t2907797370 * L_17 = (HttpException_t2907797370 *)il2cpp_codegen_object_new(HttpException_t2907797370_il2cpp_TypeInfo_var);
		HttpException__ctor_m2549443145(L_17, _stringLiteral3454431373, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_17);
	}

IL_006d:
	{
		String_t* L_18 = ___data_member1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_20 = String_op_Equality_m920492651(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_008a;
		}
	}
	{
		PropertyDescriptorCollection_t4164928659 * L_21 = V_4;
		NullCheck(L_21);
		PropertyDescriptor_t3244362832 * L_22 = VirtFuncInvoker1< PropertyDescriptor_t3244362832 *, int32_t >::Invoke(35 /* System.ComponentModel.PropertyDescriptor System.ComponentModel.PropertyDescriptorCollection::get_Item(System.Int32) */, L_21, 0);
		G_B14_0 = L_22;
		goto IL_0093;
	}

IL_008a:
	{
		PropertyDescriptorCollection_t4164928659 * L_23 = V_4;
		String_t* L_24 = ___data_member1;
		NullCheck(L_23);
		PropertyDescriptor_t3244362832 * L_25 = VirtFuncInvoker2< PropertyDescriptor_t3244362832 *, String_t*, bool >::Invoke(30 /* System.ComponentModel.PropertyDescriptor System.ComponentModel.PropertyDescriptorCollection::Find(System.String,System.Boolean) */, L_23, L_24, (bool)1);
		G_B14_0 = L_25;
	}

IL_0093:
	{
		V_5 = G_B14_0;
		PropertyDescriptor_t3244362832 * L_26 = V_5;
		if (!L_26)
		{
			goto IL_00b0;
		}
	}
	{
		PropertyDescriptor_t3244362832 * L_27 = V_5;
		Il2CppObject * L_28 = V_2;
		NullCheck(L_28);
		Il2CppObject * L_29 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(2 /* System.Object System.Collections.IList::get_Item(System.Int32) */, IList_t2094931216_il2cpp_TypeInfo_var, L_28, 0);
		NullCheck(L_27);
		Il2CppObject * L_30 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(12 /* System.Object System.ComponentModel.PropertyDescriptor::GetValue(System.Object) */, L_27, L_29);
		V_0 = ((Il2CppObject *)IsInst(L_30, IEnumerable_t1941168011_il2cpp_TypeInfo_var));
	}

IL_00b0:
	{
		Il2CppObject * L_31 = V_0;
		if (L_31)
		{
			goto IL_00c1;
		}
	}
	{
		HttpException_t2907797370 * L_32 = (HttpException_t2907797370 *)il2cpp_codegen_object_new(HttpException_t2907797370_il2cpp_TypeInfo_var);
		HttpException__ctor_m2549443145(L_32, _stringLiteral3122392217, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_32);
	}

IL_00c1:
	{
		Il2CppObject * L_33 = V_0;
		return L_33;
	}
}
// System.Void System.Web.Util.FileUtils::.cctor()
extern "C"  void FileUtils__cctor_m45768956 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FileUtils__cctor_m45768956_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Random_t108471755 * L_0 = (Random_t108471755 *)il2cpp_codegen_object_new(Random_t108471755_il2cpp_TypeInfo_var);
		Random__ctor_m4122933043(L_0, /*hidden argument*/NULL);
		((FileUtils_t3174566191_StaticFields*)FileUtils_t3174566191_il2cpp_TypeInfo_var->static_fields)->set_rnd_0(L_0);
		return;
	}
}
// System.Object System.Web.Util.FileUtils::CreateTemporaryFile(System.String,System.String,System.String,System.Web.Util.FileUtils/CreateTempFile)
extern "C"  Il2CppObject * FileUtils_CreateTemporaryFile_m904766428 (Il2CppObject * __this /* static, unused */, String_t* ___tempdir0, String_t* ___prefix1, String_t* ___extension2, CreateTempFile_t844781177 * ___createFile3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FileUtils_CreateTemporaryFile_m904766428_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	int32_t V_2 = 0;
	Random_t108471755 * V_3 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	String_t* G_B11_0 = NULL;
	String_t* G_B11_1 = NULL;
	String_t* G_B10_0 = NULL;
	String_t* G_B10_1 = NULL;
	String_t* G_B12_0 = NULL;
	String_t* G_B12_1 = NULL;
	String_t* G_B12_2 = NULL;
	String_t* G_B14_0 = NULL;
	String_t* G_B14_1 = NULL;
	String_t* G_B14_2 = NULL;
	String_t* G_B14_3 = NULL;
	String_t* G_B13_0 = NULL;
	String_t* G_B13_1 = NULL;
	String_t* G_B13_2 = NULL;
	String_t* G_B13_3 = NULL;
	String_t* G_B15_0 = NULL;
	String_t* G_B15_1 = NULL;
	String_t* G_B15_2 = NULL;
	String_t* G_B15_3 = NULL;
	String_t* G_B15_4 = NULL;
	{
		String_t* L_0 = ___tempdir0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___tempdir0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m3847582255(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0013;
		}
	}

IL_0011:
	{
		return NULL;
	}

IL_0013:
	{
		CreateTempFile_t844781177 * L_3 = ___createFile3;
		if (L_3)
		{
			goto IL_001b;
		}
	}
	{
		return NULL;
	}

IL_001b:
	{
		V_0 = (String_t*)NULL;
		V_1 = NULL;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(FileUtils_t3174566191_il2cpp_TypeInfo_var);
		Random_t108471755 * L_4 = ((FileUtils_t3174566191_StaticFields*)FileUtils_t3174566191_il2cpp_TypeInfo_var->static_fields)->get_rnd_0();
		V_3 = L_4;
		Random_t108471755 * L_5 = V_3;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_002b:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(FileUtils_t3174566191_il2cpp_TypeInfo_var);
		Random_t108471755 * L_6 = ((FileUtils_t3174566191_StaticFields*)FileUtils_t3174566191_il2cpp_TypeInfo_var->static_fields)->get_rnd_0();
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 System.Random::Next() */, L_6);
		V_2 = L_7;
		IL2CPP_LEAVE(0x42, FINALLY_003b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_003b;
	}

FINALLY_003b:
	{ // begin finally (depth: 1)
		Random_t108471755 * L_8 = V_3;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(59)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(59)
	{
		IL2CPP_JUMP_TBL(0x42, IL_0042)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0042:
	{
		String_t* L_9 = ___tempdir0;
		String_t* L_10 = ___prefix1;
		G_B10_0 = _stringLiteral2593158628;
		G_B10_1 = L_9;
		if (!L_10)
		{
			G_B11_0 = _stringLiteral2593158628;
			G_B11_1 = L_9;
			goto IL_005e;
		}
	}
	{
		String_t* L_11 = ___prefix1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m3937257545(NULL /*static, unused*/, L_11, _stringLiteral3452614530, /*hidden argument*/NULL);
		G_B12_0 = L_12;
		G_B12_1 = G_B10_0;
		G_B12_2 = G_B10_1;
		goto IL_0063;
	}

IL_005e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B12_0 = L_13;
		G_B12_1 = G_B11_0;
		G_B12_2 = G_B11_1;
	}

IL_0063:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Helpers_t1105960530_il2cpp_TypeInfo_var);
		CultureInfo_t4157843068 * L_14 = ((Helpers_t1105960530_StaticFields*)Helpers_t1105960530_il2cpp_TypeInfo_var->static_fields)->get_InvariantCulture_0();
		String_t* L_15 = Int32_ToString_m2507389746((&V_2), _stringLiteral3452614616, L_14, /*hidden argument*/NULL);
		String_t* L_16 = ___extension2;
		G_B13_0 = L_15;
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
		G_B13_3 = G_B12_2;
		if (!L_16)
		{
			G_B14_0 = L_15;
			G_B14_1 = G_B12_0;
			G_B14_2 = G_B12_1;
			G_B14_3 = G_B12_2;
			goto IL_008a;
		}
	}
	{
		String_t* L_17 = ___extension2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3452614530, L_17, /*hidden argument*/NULL);
		G_B15_0 = L_18;
		G_B15_1 = G_B13_0;
		G_B15_2 = G_B13_1;
		G_B15_3 = G_B13_2;
		G_B15_4 = G_B13_3;
		goto IL_008f;
	}

IL_008a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B15_0 = L_19;
		G_B15_1 = G_B14_0;
		G_B15_2 = G_B14_1;
		G_B15_3 = G_B14_2;
		G_B15_4 = G_B14_3;
	}

IL_008f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Format_m3339413201(NULL /*static, unused*/, G_B15_3, G_B15_2, G_B15_1, G_B15_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_21 = Path_Combine_m3389272516(NULL /*static, unused*/, G_B15_4, L_20, /*hidden argument*/NULL);
		V_0 = L_21;
	}

IL_009a:
	try
	{ // begin try (depth: 1)
		CreateTempFile_t844781177 * L_22 = ___createFile3;
		String_t* L_23 = V_0;
		NullCheck(L_22);
		Il2CppObject * L_24 = CreateTempFile_Invoke_m772578864(L_22, L_23, /*hidden argument*/NULL);
		V_1 = L_24;
		goto IL_00b5;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (IOException_t4088381929_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_00a7;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_00ad;
		throw e;
	}

CATCH_00a7:
	{ // begin catch(System.IO.IOException)
		goto IL_00b5;
	} // end catch (depth: 1)

CATCH_00ad:
	{ // begin catch(System.Object)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_00b0:
		{
			goto IL_00b5;
		}
	} // end catch (depth: 1)

IL_00b5:
	{
		Il2CppObject * L_25 = V_1;
		if (!L_25)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject * L_26 = V_1;
		return L_26;
	}
}
// System.Void System.Web.Util.FileUtils/CreateTempFile::.ctor(System.Object,System.IntPtr)
extern "C"  void CreateTempFile__ctor_m2585689686 (CreateTempFile_t844781177 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Object System.Web.Util.FileUtils/CreateTempFile::Invoke(System.String)
extern "C"  Il2CppObject * CreateTempFile_Invoke_m772578864 (CreateTempFile_t844781177 * __this, String_t* ___path0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CreateTempFile_Invoke_m772578864((CreateTempFile_t844781177 *)__this->get_prev_9(),___path0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___path0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___path0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, String_t* ___path0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___path0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___path0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Web.Util.FileUtils/CreateTempFile::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CreateTempFile_BeginInvoke_m3885201582 (CreateTempFile_t844781177 * __this, String_t* ___path0, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___path0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Object System.Web.Util.FileUtils/CreateTempFile::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * CreateTempFile_EndInvoke_m1398075159 (CreateTempFile_t844781177 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Web.Util.Helpers::.cctor()
extern "C"  void Helpers__cctor_m1437973413 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Helpers__cctor_m1437973413_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t4157843068_il2cpp_TypeInfo_var);
		CultureInfo_t4157843068 * L_0 = CultureInfo_get_InvariantCulture_m3532445182(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Helpers_t1105960530_StaticFields*)Helpers_t1105960530_il2cpp_TypeInfo_var->static_fields)->set_InvariantCulture_0(L_0);
		return;
	}
}
// System.String System.Web.Util.ICalls::GetMachineConfigPath()
extern "C"  String_t* ICalls_GetMachineConfigPath_m3699741176 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*ICalls_GetMachineConfigPath_m3699741176_ftn) ();
	static ICalls_GetMachineConfigPath_m3699741176_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ICalls_GetMachineConfigPath_m3699741176_ftn)il2cpp_codegen_resolve_icall ("System.Web.Util.ICalls::GetMachineConfigPath()");
	return _il2cpp_icall_func();
}
// System.Void System.Web.Util.SearchPattern::.ctor(System.String,System.Boolean)
extern "C"  void SearchPattern__ctor_m1764878605 (SearchPattern_t3593873567 * __this, String_t* ___pattern0, bool ___ignore1, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___pattern0;
		bool L_1 = ___ignore1;
		SearchPattern_SetPattern_m239268423(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Web.Util.SearchPattern::.cctor()
extern "C"  void SearchPattern__cctor_m386626602 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SearchPattern__cctor_m386626602_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharU5BU5D_t3528271667* L_0 = ((CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)42));
		CharU5BU5D_t3528271667* L_1 = L_0;
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppChar)((int32_t)63));
		((SearchPattern_t3593873567_StaticFields*)SearchPattern_t3593873567_il2cpp_TypeInfo_var->static_fields)->set_WildcardChars_2(L_1);
		return;
	}
}
// System.Void System.Web.Util.SearchPattern::SetPattern(System.String,System.Boolean)
extern "C"  void SearchPattern_SetPattern_m239268423 (SearchPattern_t3593873567 * __this, String_t* ___pattern0, bool ___ignore1, const MethodInfo* method)
{
	{
		bool L_0 = ___ignore1;
		__this->set_ignore_1(L_0);
		String_t* L_1 = ___pattern0;
		SearchPattern_Compile_m1523637325(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Web.Util.SearchPattern::IsMatch(System.String)
extern "C"  bool SearchPattern_IsMatch_m2891693420 (SearchPattern_t3593873567 * __this, String_t* ___text0, const MethodInfo* method)
{
	{
		Op_t1428817098 * L_0 = __this->get_ops_0();
		String_t* L_1 = ___text0;
		bool L_2 = SearchPattern_Match_m327281186(__this, L_0, L_1, 0, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Web.Util.SearchPattern::Compile(System.String)
extern "C"  void SearchPattern_Compile_m1523637325 (SearchPattern_t3593873567 * __this, String_t* ___pattern0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SearchPattern_Compile_m1523637325_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Op_t1428817098 * V_1 = NULL;
	Op_t1428817098 * V_2 = NULL;
	int32_t V_3 = 0;
	Il2CppChar V_4 = 0x0;
	{
		String_t* L_0 = ___pattern0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentException_t132251570 * L_1 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_1, _stringLiteral1422575839, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		String_t* L_2 = ___pattern0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m920492651(NULL /*static, unused*/, L_2, _stringLiteral3452614534, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		Op_t1428817098 * L_4 = (Op_t1428817098 *)il2cpp_codegen_object_new(Op_t1428817098_il2cpp_TypeInfo_var);
		Op__ctor_m2094078555(L_4, 4, /*hidden argument*/NULL);
		__this->set_ops_0(L_4);
		return;
	}

IL_002e:
	{
		__this->set_ops_0((Op_t1428817098 *)NULL);
		V_0 = 0;
		V_1 = (Op_t1428817098 *)NULL;
		goto IL_00ee;
	}

IL_003e:
	{
		String_t* L_5 = ___pattern0;
		int32_t L_6 = V_0;
		NullCheck(L_5);
		Il2CppChar L_7 = String_get_Chars_m2986988803(L_5, L_6, /*hidden argument*/NULL);
		V_4 = L_7;
		Il2CppChar L_8 = V_4;
		if ((((int32_t)L_8) == ((int32_t)((int32_t)42))))
		{
			goto IL_006e;
		}
	}
	{
		Il2CppChar L_9 = V_4;
		if ((((int32_t)L_9) == ((int32_t)((int32_t)63))))
		{
			goto IL_005e;
		}
	}
	{
		goto IL_007e;
	}

IL_005e:
	{
		Op_t1428817098 * L_10 = (Op_t1428817098 *)il2cpp_codegen_object_new(Op_t1428817098_il2cpp_TypeInfo_var);
		Op__ctor_m2094078555(L_10, 1, /*hidden argument*/NULL);
		V_2 = L_10;
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
		goto IL_00d3;
	}

IL_006e:
	{
		Op_t1428817098 * L_12 = (Op_t1428817098 *)il2cpp_codegen_object_new(Op_t1428817098_il2cpp_TypeInfo_var);
		Op__ctor_m2094078555(L_12, 2, /*hidden argument*/NULL);
		V_2 = L_12;
		int32_t L_13 = V_0;
		V_0 = ((int32_t)((int32_t)L_13+(int32_t)1));
		goto IL_00d3;
	}

IL_007e:
	{
		Op_t1428817098 * L_14 = (Op_t1428817098 *)il2cpp_codegen_object_new(Op_t1428817098_il2cpp_TypeInfo_var);
		Op__ctor_m2094078555(L_14, 0, /*hidden argument*/NULL);
		V_2 = L_14;
		String_t* L_15 = ___pattern0;
		IL2CPP_RUNTIME_CLASS_INIT(SearchPattern_t3593873567_il2cpp_TypeInfo_var);
		CharU5BU5D_t3528271667* L_16 = ((SearchPattern_t3593873567_StaticFields*)SearchPattern_t3593873567_il2cpp_TypeInfo_var->static_fields)->get_WildcardChars_2();
		int32_t L_17 = V_0;
		NullCheck(L_15);
		int32_t L_18 = String_IndexOfAny_m2323029521(L_15, L_16, L_17, /*hidden argument*/NULL);
		V_3 = L_18;
		int32_t L_19 = V_3;
		if ((((int32_t)L_19) >= ((int32_t)0)))
		{
			goto IL_00a0;
		}
	}
	{
		String_t* L_20 = ___pattern0;
		NullCheck(L_20);
		int32_t L_21 = String_get_Length_m3847582255(L_20, /*hidden argument*/NULL);
		V_3 = L_21;
	}

IL_00a0:
	{
		Op_t1428817098 * L_22 = V_2;
		String_t* L_23 = ___pattern0;
		int32_t L_24 = V_0;
		int32_t L_25 = V_3;
		int32_t L_26 = V_0;
		NullCheck(L_23);
		String_t* L_27 = String_Substring_m1610150815(L_23, L_24, ((int32_t)((int32_t)L_25-(int32_t)L_26)), /*hidden argument*/NULL);
		NullCheck(L_22);
		L_22->set_Argument_1(L_27);
		bool L_28 = __this->get_ignore_1();
		if (!L_28)
		{
			goto IL_00cc;
		}
	}
	{
		Op_t1428817098 * L_29 = V_2;
		Op_t1428817098 * L_30 = V_2;
		NullCheck(L_30);
		String_t* L_31 = L_30->get_Argument_1();
		NullCheck(L_31);
		String_t* L_32 = String_ToLower_m2029374922(L_31, /*hidden argument*/NULL);
		NullCheck(L_29);
		L_29->set_Argument_1(L_32);
	}

IL_00cc:
	{
		int32_t L_33 = V_3;
		V_0 = L_33;
		goto IL_00d3;
	}

IL_00d3:
	{
		Op_t1428817098 * L_34 = V_1;
		if (L_34)
		{
			goto IL_00e5;
		}
	}
	{
		Op_t1428817098 * L_35 = V_2;
		__this->set_ops_0(L_35);
		goto IL_00ec;
	}

IL_00e5:
	{
		Op_t1428817098 * L_36 = V_1;
		Op_t1428817098 * L_37 = V_2;
		NullCheck(L_36);
		L_36->set_Next_2(L_37);
	}

IL_00ec:
	{
		Op_t1428817098 * L_38 = V_2;
		V_1 = L_38;
	}

IL_00ee:
	{
		int32_t L_39 = V_0;
		String_t* L_40 = ___pattern0;
		NullCheck(L_40);
		int32_t L_41 = String_get_Length_m3847582255(L_40, /*hidden argument*/NULL);
		if ((((int32_t)L_39) < ((int32_t)L_41)))
		{
			goto IL_003e;
		}
	}
	{
		Op_t1428817098 * L_42 = V_1;
		if (L_42)
		{
			goto IL_0111;
		}
	}
	{
		Op_t1428817098 * L_43 = (Op_t1428817098 *)il2cpp_codegen_object_new(Op_t1428817098_il2cpp_TypeInfo_var);
		Op__ctor_m2094078555(L_43, 3, /*hidden argument*/NULL);
		__this->set_ops_0(L_43);
		goto IL_011d;
	}

IL_0111:
	{
		Op_t1428817098 * L_44 = V_1;
		Op_t1428817098 * L_45 = (Op_t1428817098 *)il2cpp_codegen_object_new(Op_t1428817098_il2cpp_TypeInfo_var);
		Op__ctor_m2094078555(L_45, 3, /*hidden argument*/NULL);
		NullCheck(L_44);
		L_44->set_Next_2(L_45);
	}

IL_011d:
	{
		return;
	}
}
// System.Boolean System.Web.Util.SearchPattern::Match(System.Web.Util.SearchPattern/Op,System.String,System.Int32)
extern "C"  bool SearchPattern_Match_m327281186 (SearchPattern_t3593873567 * __this, Op_t1428817098 * ___op0, String_t* ___text1, int32_t ___ptr2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SearchPattern_Match_m327281186_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		goto IL_00de;
	}

IL_0005:
	{
		Op_t1428817098 * L_0 = ___op0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_Code_0();
		V_2 = L_1;
		int32_t L_2 = V_2;
		switch (L_2)
		{
			case 0:
			{
				goto IL_003d;
			}
			case 1:
			{
				goto IL_0091;
			}
			case 2:
			{
				goto IL_00a9;
			}
			case 3:
			{
				goto IL_002d;
			}
			case 4:
			{
				goto IL_002b;
			}
		}
	}
	{
		goto IL_00d6;
	}

IL_002b:
	{
		return (bool)1;
	}

IL_002d:
	{
		int32_t L_3 = ___ptr2;
		String_t* L_4 = ___text1;
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m3847582255(L_4, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)L_5))))
		{
			goto IL_003b;
		}
	}
	{
		return (bool)1;
	}

IL_003b:
	{
		return (bool)0;
	}

IL_003d:
	{
		Op_t1428817098 * L_6 = ___op0;
		NullCheck(L_6);
		String_t* L_7 = L_6->get_Argument_1();
		NullCheck(L_7);
		int32_t L_8 = String_get_Length_m3847582255(L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		int32_t L_9 = ___ptr2;
		int32_t L_10 = V_0;
		String_t* L_11 = ___text1;
		NullCheck(L_11);
		int32_t L_12 = String_get_Length_m3847582255(L_11, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_9+(int32_t)L_10))) <= ((int32_t)L_12)))
		{
			goto IL_0059;
		}
	}
	{
		return (bool)0;
	}

IL_0059:
	{
		String_t* L_13 = ___text1;
		int32_t L_14 = ___ptr2;
		int32_t L_15 = V_0;
		NullCheck(L_13);
		String_t* L_16 = String_Substring_m1610150815(L_13, L_14, L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		bool L_17 = __this->get_ignore_1();
		if (!L_17)
		{
			goto IL_0074;
		}
	}
	{
		String_t* L_18 = V_1;
		NullCheck(L_18);
		String_t* L_19 = String_ToLower_m2029374922(L_18, /*hidden argument*/NULL);
		V_1 = L_19;
	}

IL_0074:
	{
		String_t* L_20 = V_1;
		Op_t1428817098 * L_21 = ___op0;
		NullCheck(L_21);
		String_t* L_22 = L_21->get_Argument_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_23 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_20, L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0087;
		}
	}
	{
		return (bool)0;
	}

IL_0087:
	{
		int32_t L_24 = ___ptr2;
		int32_t L_25 = V_0;
		___ptr2 = ((int32_t)((int32_t)L_24+(int32_t)L_25));
		goto IL_00d6;
	}

IL_0091:
	{
		int32_t L_26 = ___ptr2;
		int32_t L_27 = ((int32_t)((int32_t)L_26+(int32_t)1));
		___ptr2 = L_27;
		String_t* L_28 = ___text1;
		NullCheck(L_28);
		int32_t L_29 = String_get_Length_m3847582255(L_28, /*hidden argument*/NULL);
		if ((((int32_t)L_27) <= ((int32_t)L_29)))
		{
			goto IL_00a4;
		}
	}
	{
		return (bool)0;
	}

IL_00a4:
	{
		goto IL_00d6;
	}

IL_00a9:
	{
		goto IL_00c8;
	}

IL_00ae:
	{
		Op_t1428817098 * L_30 = ___op0;
		NullCheck(L_30);
		Op_t1428817098 * L_31 = L_30->get_Next_2();
		String_t* L_32 = ___text1;
		int32_t L_33 = ___ptr2;
		bool L_34 = SearchPattern_Match_m327281186(__this, L_31, L_32, L_33, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_00c3;
		}
	}
	{
		return (bool)1;
	}

IL_00c3:
	{
		int32_t L_35 = ___ptr2;
		___ptr2 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_00c8:
	{
		int32_t L_36 = ___ptr2;
		String_t* L_37 = ___text1;
		NullCheck(L_37);
		int32_t L_38 = String_get_Length_m3847582255(L_37, /*hidden argument*/NULL);
		if ((((int32_t)L_36) <= ((int32_t)L_38)))
		{
			goto IL_00ae;
		}
	}
	{
		return (bool)0;
	}

IL_00d6:
	{
		Op_t1428817098 * L_39 = ___op0;
		NullCheck(L_39);
		Op_t1428817098 * L_40 = L_39->get_Next_2();
		___op0 = L_40;
	}

IL_00de:
	{
		Op_t1428817098 * L_41 = ___op0;
		if (L_41)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)1;
	}
}
// System.Void System.Web.Util.SearchPattern/Op::.ctor(System.Web.Util.SearchPattern/OpCode)
extern "C"  void Op__ctor_m2094078555 (Op_t1428817098 * __this, int32_t ___code0, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___code0;
		__this->set_Code_0(L_0);
		__this->set_Argument_1((String_t*)NULL);
		__this->set_Next_2((Op_t1428817098 *)NULL);
		return;
	}
}
// System.Boolean System.Web.Util.StrUtils::StartsWith(System.String,System.String)
extern "C"  bool StrUtils_StartsWith_m2313343174 (Il2CppObject * __this /* static, unused */, String_t* ___str10, String_t* ___str21, const MethodInfo* method)
{
	{
		String_t* L_0 = ___str10;
		String_t* L_1 = ___str21;
		bool L_2 = StrUtils_StartsWith_m3039927907(NULL /*static, unused*/, L_0, L_1, (bool)0, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean System.Web.Util.StrUtils::StartsWith(System.String,System.String,System.Boolean)
extern "C"  bool StrUtils_StartsWith_m3039927907 (Il2CppObject * __this /* static, unused */, String_t* ___str10, String_t* ___str21, bool ___ignore_case2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StrUtils_StartsWith_m3039927907_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___str21;
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m3847582255(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)1;
	}

IL_000f:
	{
		String_t* L_3 = ___str10;
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m3847582255(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		int32_t L_5 = V_0;
		int32_t L_6 = V_1;
		if ((((int32_t)L_5) <= ((int32_t)L_6)))
		{
			goto IL_001f;
		}
	}
	{
		return (bool)0;
	}

IL_001f:
	{
		String_t* L_7 = ___str10;
		String_t* L_8 = ___str21;
		int32_t L_9 = V_0;
		bool L_10 = ___ignore_case2;
		IL2CPP_RUNTIME_CLASS_INIT(Helpers_t1105960530_il2cpp_TypeInfo_var);
		CultureInfo_t4157843068 * L_11 = ((Helpers_t1105960530_StaticFields*)Helpers_t1105960530_il2cpp_TypeInfo_var->static_fields)->get_InvariantCulture_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_12 = String_Compare_m945110377(NULL /*static, unused*/, L_7, 0, L_8, 0, L_9, L_10, L_11, /*hidden argument*/NULL);
		return (bool)((((int32_t)0) == ((int32_t)L_12))? 1 : 0);
	}
}
// System.Boolean System.Web.Util.StrUtils::EndsWith(System.String,System.String)
extern "C"  bool StrUtils_EndsWith_m466541284 (Il2CppObject * __this /* static, unused */, String_t* ___str10, String_t* ___str21, const MethodInfo* method)
{
	{
		String_t* L_0 = ___str10;
		String_t* L_1 = ___str21;
		bool L_2 = StrUtils_EndsWith_m890124797(NULL /*static, unused*/, L_0, L_1, (bool)0, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean System.Web.Util.StrUtils::EndsWith(System.String,System.String,System.Boolean)
extern "C"  bool StrUtils_EndsWith_m890124797 (Il2CppObject * __this /* static, unused */, String_t* ___str10, String_t* ___str21, bool ___ignore_case2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StrUtils_EndsWith_m890124797_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___str21;
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m3847582255(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)1;
	}

IL_000f:
	{
		String_t* L_3 = ___str10;
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m3847582255(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		int32_t L_5 = V_0;
		int32_t L_6 = V_1;
		if ((((int32_t)L_5) <= ((int32_t)L_6)))
		{
			goto IL_001f;
		}
	}
	{
		return (bool)0;
	}

IL_001f:
	{
		String_t* L_7 = ___str10;
		int32_t L_8 = V_1;
		int32_t L_9 = V_0;
		String_t* L_10 = ___str21;
		int32_t L_11 = V_0;
		bool L_12 = ___ignore_case2;
		IL2CPP_RUNTIME_CLASS_INIT(Helpers_t1105960530_il2cpp_TypeInfo_var);
		CultureInfo_t4157843068 * L_13 = ((Helpers_t1105960530_StaticFields*)Helpers_t1105960530_il2cpp_TypeInfo_var->static_fields)->get_InvariantCulture_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_14 = String_Compare_m945110377(NULL /*static, unused*/, L_7, ((int32_t)((int32_t)L_8-(int32_t)L_9)), L_10, 0, L_11, L_12, L_13, /*hidden argument*/NULL);
		return (bool)((((int32_t)0) == ((int32_t)L_14))? 1 : 0);
	}
}
// System.String System.Web.Util.StrUtils::EscapeQuotesAndBackslashes(System.String)
extern "C"  String_t* StrUtils_EscapeQuotesAndBackslashes_m2514145343 (Il2CppObject * __this /* static, unused */, String_t* ___attributeValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StrUtils_EscapeQuotesAndBackslashes_m2514145343_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	int32_t V_1 = 0;
	Il2CppChar V_2 = 0x0;
	{
		V_0 = (StringBuilder_t1712802186 *)NULL;
		V_1 = 0;
		goto IL_006c;
	}

IL_0009:
	{
		String_t* L_0 = ___attributeValue0;
		int32_t L_1 = V_1;
		NullCheck(L_0);
		Il2CppChar L_2 = String_get_Chars_m2986988803(L_0, L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		Il2CppChar L_3 = V_2;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)39))))
		{
			goto IL_0029;
		}
	}
	{
		Il2CppChar L_4 = V_2;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)34))))
		{
			goto IL_0029;
		}
	}
	{
		Il2CppChar L_5 = V_2;
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_005a;
		}
	}

IL_0029:
	{
		StringBuilder_t1712802186 * L_6 = V_0;
		if (L_6)
		{
			goto IL_0044;
		}
	}
	{
		StringBuilder_t1712802186 * L_7 = (StringBuilder_t1712802186 *)il2cpp_codegen_object_new(StringBuilder_t1712802186_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3121283359(L_7, /*hidden argument*/NULL);
		V_0 = L_7;
		StringBuilder_t1712802186 * L_8 = V_0;
		String_t* L_9 = ___attributeValue0;
		int32_t L_10 = V_1;
		NullCheck(L_9);
		String_t* L_11 = String_Substring_m1610150815(L_9, 0, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		StringBuilder_Append_m1965104174(L_8, L_11, /*hidden argument*/NULL);
	}

IL_0044:
	{
		StringBuilder_t1712802186 * L_12 = V_0;
		NullCheck(L_12);
		StringBuilder_Append_m2383614642(L_12, ((int32_t)92), /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_13 = V_0;
		Il2CppChar L_14 = V_2;
		NullCheck(L_13);
		StringBuilder_Append_m2383614642(L_13, L_14, /*hidden argument*/NULL);
		goto IL_0068;
	}

IL_005a:
	{
		StringBuilder_t1712802186 * L_15 = V_0;
		if (!L_15)
		{
			goto IL_0068;
		}
	}
	{
		StringBuilder_t1712802186 * L_16 = V_0;
		Il2CppChar L_17 = V_2;
		NullCheck(L_16);
		StringBuilder_Append_m2383614642(L_16, L_17, /*hidden argument*/NULL);
	}

IL_0068:
	{
		int32_t L_18 = V_1;
		V_1 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_006c:
	{
		int32_t L_19 = V_1;
		String_t* L_20 = ___attributeValue0;
		NullCheck(L_20);
		int32_t L_21 = String_get_Length_m3847582255(L_20, /*hidden argument*/NULL);
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0009;
		}
	}
	{
		StringBuilder_t1712802186 * L_22 = V_0;
		if (!L_22)
		{
			goto IL_0085;
		}
	}
	{
		StringBuilder_t1712802186 * L_23 = V_0;
		NullCheck(L_23);
		String_t* L_24 = StringBuilder_ToString_m3317489284(L_23, /*hidden argument*/NULL);
		return L_24;
	}

IL_0085:
	{
		String_t* L_25 = ___attributeValue0;
		return L_25;
	}
}
// System.Boolean System.Web.Util.StrUtils::IsNullOrEmpty(System.String)
extern "C"  bool StrUtils_IsNullOrEmpty_m2227577989 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StrUtils_IsNullOrEmpty_m2227577989_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String[] System.Web.Util.StrUtils::SplitRemoveEmptyEntries(System.String,System.Char[])
extern "C"  StringU5BU5D_t1281789340* StrUtils_SplitRemoveEmptyEntries_m3488785249 (Il2CppObject * __this /* static, unused */, String_t* ___value0, CharU5BU5D_t3528271667* ___separator1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		CharU5BU5D_t3528271667* L_1 = ___separator1;
		NullCheck(L_0);
		StringU5BU5D_t1281789340* L_2 = String_Split_m1466646415(L_0, L_1, 1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String System.Web.Util.TimeUtil::ToUtcTimeString(System.DateTime)
extern "C"  String_t* TimeUtil_ToUtcTimeString_m1557420589 (Il2CppObject * __this /* static, unused */, DateTime_t3738529785  ___dt0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TimeUtil_ToUtcTimeString_m1557420589_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DateTime_t3738529785  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		DateTime_t3738529785  L_0 = DateTime_ToUniversalTime_m1945318289((&___dt0), /*hidden argument*/NULL);
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(Helpers_t1105960530_il2cpp_TypeInfo_var);
		CultureInfo_t4157843068 * L_1 = ((Helpers_t1105960530_StaticFields*)Helpers_t1105960530_il2cpp_TypeInfo_var->static_fields)->get_InvariantCulture_0();
		String_t* L_2 = DateTime_ToString_m2992030064((&V_0), _stringLiteral4099524215, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m3937257545(NULL /*static, unused*/, L_2, _stringLiteral1130917823, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void System.Web.Util.UrlUtils::.cctor()
extern "C"  void UrlUtils__cctor_m3701932390 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UrlUtils__cctor_m3701932390_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharU5BU5D_t3528271667* L_0 = ((CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)92));
		CharU5BU5D_t3528271667* L_1 = L_0;
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppChar)((int32_t)47));
		((UrlUtils_t1214298665_StaticFields*)UrlUtils_t1214298665_il2cpp_TypeInfo_var->static_fields)->set_path_sep_0(L_1);
		return;
	}
}
// System.String System.Web.Util.UrlUtils::InsertSessionId(System.String,System.String)
extern "C"  String_t* UrlUtils_InsertSessionId_m2191444238 (Il2CppObject * __this /* static, unused */, String_t* ___id0, String_t* ___path1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UrlUtils_InsertSessionId_m2191444238_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* G_B10_0 = NULL;
	{
		String_t* L_0 = ___path1;
		IL2CPP_RUNTIME_CLASS_INIT(UrlUtils_t1214298665_il2cpp_TypeInfo_var);
		String_t* L_1 = UrlUtils_GetDirectory_m1298890870(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = String_EndsWith_m1901926500(L_2, _stringLiteral3452614529, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0023;
		}
	}
	{
		String_t* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m3937257545(NULL /*static, unused*/, L_4, _stringLiteral3452614529, /*hidden argument*/NULL);
		V_0 = L_5;
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HttpRuntime_t1480753741_il2cpp_TypeInfo_var);
		String_t* L_6 = HttpRuntime_get_AppDomainAppVirtualPath_m531373613(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_6;
		String_t* L_7 = V_1;
		NullCheck(L_7);
		bool L_8 = String_EndsWith_m1901926500(L_7, _stringLiteral3452614529, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0045;
		}
	}
	{
		String_t* L_9 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m3937257545(NULL /*static, unused*/, L_9, _stringLiteral3452614529, /*hidden argument*/NULL);
		V_1 = L_10;
	}

IL_0045:
	{
		String_t* L_11 = ___path1;
		String_t* L_12 = V_1;
		NullCheck(L_11);
		bool L_13 = String_StartsWith_m1759067526(L_11, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_005f;
		}
	}
	{
		String_t* L_14 = ___path1;
		String_t* L_15 = V_1;
		NullCheck(L_15);
		int32_t L_16 = String_get_Length_m3847582255(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		String_t* L_17 = String_Substring_m2848979100(L_14, L_16, /*hidden argument*/NULL);
		___path1 = L_17;
	}

IL_005f:
	{
		String_t* L_18 = ___path1;
		NullCheck(L_18);
		Il2CppChar L_19 = String_get_Chars_m2986988803(L_18, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_19) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_008c;
		}
	}
	{
		String_t* L_20 = ___path1;
		NullCheck(L_20);
		int32_t L_21 = String_get_Length_m3847582255(L_20, /*hidden argument*/NULL);
		if ((((int32_t)L_21) <= ((int32_t)1)))
		{
			goto IL_0085;
		}
	}
	{
		String_t* L_22 = ___path1;
		NullCheck(L_22);
		String_t* L_23 = String_Substring_m2848979100(L_22, 1, /*hidden argument*/NULL);
		G_B10_0 = L_23;
		goto IL_008a;
	}

IL_0085:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B10_0 = L_24;
	}

IL_008a:
	{
		___path1 = G_B10_0;
	}

IL_008c:
	{
		StringU5BU5D_t1281789340* L_25 = ((StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)5));
		String_t* L_26 = V_1;
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, L_26);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_26);
		StringU5BU5D_t1281789340* L_27 = L_25;
		NullCheck(L_27);
		ArrayElementTypeCheck (L_27, _stringLiteral3452614536);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral3452614536);
		StringU5BU5D_t1281789340* L_28 = L_27;
		String_t* L_29 = ___id0;
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, L_29);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_29);
		StringU5BU5D_t1281789340* L_30 = L_28;
		NullCheck(L_30);
		ArrayElementTypeCheck (L_30, _stringLiteral3450582919);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral3450582919);
		StringU5BU5D_t1281789340* L_31 = L_30;
		String_t* L_32 = ___path1;
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_32);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_32);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = String_Concat_m1809518182(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UrlUtils_t1214298665_il2cpp_TypeInfo_var);
		String_t* L_34 = UrlUtils_Canonic_m4232072973(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		return L_34;
	}
}
// System.String System.Web.Util.UrlUtils::GetSessionId(System.String)
extern "C"  String_t* UrlUtils_GetSessionId_m1101998480 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UrlUtils_GetSessionId_m1101998480_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		String_t* L_0 = ___path0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_0008:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HttpRuntime_t1480753741_il2cpp_TypeInfo_var);
		String_t* L_1 = HttpRuntime_get_AppDomainAppVirtualPath_m531373613(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m3847582255(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = ___path0;
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m3847582255(L_4, /*hidden argument*/NULL);
		int32_t L_6 = V_1;
		if ((((int32_t)L_5) > ((int32_t)L_6)))
		{
			goto IL_0023;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_0023:
	{
		String_t* L_7 = ___path0;
		int32_t L_8 = V_1;
		NullCheck(L_7);
		String_t* L_9 = String_Substring_m2848979100(L_7, L_8, /*hidden argument*/NULL);
		___path0 = L_9;
		String_t* L_10 = ___path0;
		NullCheck(L_10);
		int32_t L_11 = String_get_Length_m3847582255(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		int32_t L_12 = V_2;
		if (!L_12)
		{
			goto IL_0047;
		}
	}
	{
		String_t* L_13 = ___path0;
		NullCheck(L_13);
		Il2CppChar L_14 = String_get_Chars_m2986988803(L_13, 0, /*hidden argument*/NULL);
		if ((((int32_t)L_14) == ((int32_t)((int32_t)47))))
		{
			goto IL_005a;
		}
	}

IL_0047:
	{
		Il2CppChar L_15 = ((Il2CppChar)((int32_t)47));
		Il2CppObject * L_16 = Box(Char_t3634460470_il2cpp_TypeInfo_var, &L_15);
		String_t* L_17 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m904156431(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		___path0 = L_18;
		int32_t L_19 = V_2;
		V_2 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_005a:
	{
		int32_t L_20 = V_2;
		if ((((int32_t)L_20) < ((int32_t)((int32_t)27))))
		{
			goto IL_007f;
		}
	}
	{
		String_t* L_21 = ___path0;
		NullCheck(L_21);
		Il2CppChar L_22 = String_get_Chars_m2986988803(L_21, 1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)40)))))
		{
			goto IL_007f;
		}
	}
	{
		String_t* L_23 = ___path0;
		NullCheck(L_23);
		Il2CppChar L_24 = String_get_Chars_m2986988803(L_23, ((int32_t)26), /*hidden argument*/NULL);
		if ((((int32_t)L_24) == ((int32_t)((int32_t)41))))
		{
			goto IL_0081;
		}
	}

IL_007f:
	{
		return (String_t*)NULL;
	}

IL_0081:
	{
		String_t* L_25 = ___path0;
		NullCheck(L_25);
		String_t* L_26 = String_Substring_m1610150815(L_25, 2, ((int32_t)24), /*hidden argument*/NULL);
		return L_26;
	}
}
// System.Boolean System.Web.Util.UrlUtils::HasSessionId(System.String)
extern "C"  bool UrlUtils_HasSessionId_m364033583 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UrlUtils_HasSessionId_m364033583_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B6_0 = 0;
	{
		String_t* L_0 = ___path0;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_1 = ___path0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m3847582255(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) >= ((int32_t)5)))
		{
			goto IL_0014;
		}
	}

IL_0012:
	{
		return (bool)0;
	}

IL_0014:
	{
		String_t* L_3 = ___path0;
		bool L_4 = StrUtils_StartsWith_m2313343174(NULL /*static, unused*/, L_3, _stringLiteral3451041665, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		String_t* L_5 = ___path0;
		NullCheck(L_5);
		int32_t L_6 = String_IndexOf_m1977622757(L_5, _stringLiteral3450582919, /*hidden argument*/NULL);
		G_B6_0 = ((((int32_t)L_6) > ((int32_t)2))? 1 : 0);
		goto IL_0035;
	}

IL_0034:
	{
		G_B6_0 = 0;
	}

IL_0035:
	{
		return (bool)G_B6_0;
	}
}
// System.String System.Web.Util.UrlUtils::RemoveSessionId(System.String,System.String)
extern "C"  String_t* UrlUtils_RemoveSessionId_m2523203920 (Il2CppObject * __this /* static, unused */, String_t* ___base_path0, String_t* ___file_path1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UrlUtils_RemoveSessionId_m2523203920_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	{
		String_t* L_0 = ___base_path0;
		NullCheck(L_0);
		int32_t L_1 = String_IndexOf_m1977622757(L_0, _stringLiteral3451041665, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = ___base_path0;
		int32_t L_3 = V_0;
		NullCheck(L_2);
		String_t* L_4 = String_Substring_m1610150815(L_2, 0, ((int32_t)((int32_t)L_3+(int32_t)1)), /*hidden argument*/NULL);
		V_1 = L_4;
		String_t* L_5 = V_1;
		NullCheck(L_5);
		bool L_6 = String_EndsWith_m1901926500(L_5, _stringLiteral3452614529, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0033;
		}
	}
	{
		String_t* L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m3937257545(NULL /*static, unused*/, L_7, _stringLiteral3452614529, /*hidden argument*/NULL);
		V_1 = L_8;
	}

IL_0033:
	{
		String_t* L_9 = ___base_path0;
		NullCheck(L_9);
		int32_t L_10 = String_IndexOf_m1977622757(L_9, _stringLiteral3450582919, /*hidden argument*/NULL);
		V_0 = L_10;
		int32_t L_11 = V_0;
		if ((((int32_t)L_11) == ((int32_t)(-1))))
		{
			goto IL_0082;
		}
	}
	{
		String_t* L_12 = ___base_path0;
		NullCheck(L_12);
		int32_t L_13 = String_get_Length_m3847582255(L_12, /*hidden argument*/NULL);
		int32_t L_14 = V_0;
		if ((((int32_t)L_13) <= ((int32_t)((int32_t)((int32_t)L_14+(int32_t)2)))))
		{
			goto IL_0082;
		}
	}
	{
		String_t* L_15 = ___base_path0;
		int32_t L_16 = V_0;
		NullCheck(L_15);
		String_t* L_17 = String_Substring_m2848979100(L_15, ((int32_t)((int32_t)L_16+(int32_t)2)), /*hidden argument*/NULL);
		V_2 = L_17;
		String_t* L_18 = V_2;
		NullCheck(L_18);
		bool L_19 = String_EndsWith_m1901926500(L_18, _stringLiteral3452614529, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_007a;
		}
	}
	{
		String_t* L_20 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m3937257545(NULL /*static, unused*/, L_20, _stringLiteral3452614529, /*hidden argument*/NULL);
		V_2 = L_21;
	}

IL_007a:
	{
		String_t* L_22 = V_1;
		String_t* L_23 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = String_Concat_m3937257545(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
		V_1 = L_24;
	}

IL_0082:
	{
		String_t* L_25 = V_1;
		String_t* L_26 = ___file_path1;
		IL2CPP_RUNTIME_CLASS_INIT(UrlUtils_t1214298665_il2cpp_TypeInfo_var);
		String_t* L_27 = UrlUtils_GetFile_m1187780462(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_28 = String_Concat_m3937257545(NULL /*static, unused*/, L_25, L_27, /*hidden argument*/NULL);
		String_t* L_29 = UrlUtils_Canonic_m4232072973(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		return L_29;
	}
}
// System.String System.Web.Util.UrlUtils::Combine(System.String,System.String)
extern "C"  String_t* UrlUtils_Combine_m1528663317 (Il2CppObject * __this /* static, unused */, String_t* ___basePath0, String_t* ___relPath1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UrlUtils_Combine_m1528663317_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Il2CppChar V_1 = 0x0;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* G_B17_0 = NULL;
	{
		String_t* L_0 = ___relPath1;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral3439590103, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		String_t* L_2 = ___relPath1;
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m3847582255(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if (L_4)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_5;
	}

IL_0024:
	{
		String_t* L_6 = ___relPath1;
		NullCheck(L_6);
		String_t* L_7 = String_Replace_m3726209165(L_6, ((int32_t)92), ((int32_t)47), /*hidden argument*/NULL);
		___relPath1 = L_7;
		String_t* L_8 = ___relPath1;
		IL2CPP_RUNTIME_CLASS_INIT(UrlUtils_t1214298665_il2cpp_TypeInfo_var);
		bool L_9 = UrlUtils_IsRooted_m3690953646(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0042;
		}
	}
	{
		String_t* L_10 = ___relPath1;
		IL2CPP_RUNTIME_CLASS_INIT(UrlUtils_t1214298665_il2cpp_TypeInfo_var);
		String_t* L_11 = UrlUtils_Canonic_m4232072973(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		return L_11;
	}

IL_0042:
	{
		String_t* L_12 = ___relPath1;
		NullCheck(L_12);
		Il2CppChar L_13 = String_get_Chars_m2986988803(L_12, 0, /*hidden argument*/NULL);
		V_1 = L_13;
		int32_t L_14 = V_0;
		if ((((int32_t)L_14) < ((int32_t)3)))
		{
			goto IL_0069;
		}
	}
	{
		Il2CppChar L_15 = V_1;
		if ((((int32_t)L_15) == ((int32_t)((int32_t)126))))
		{
			goto IL_0069;
		}
	}
	{
		Il2CppChar L_16 = V_1;
		if ((((int32_t)L_16) == ((int32_t)((int32_t)47))))
		{
			goto IL_0069;
		}
	}
	{
		Il2CppChar L_17 = V_1;
		if ((!(((uint32_t)L_17) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_011f;
		}
	}

IL_0069:
	{
		String_t* L_18 = ___basePath0;
		if (!L_18)
		{
			goto IL_0089;
		}
	}
	{
		String_t* L_19 = ___basePath0;
		NullCheck(L_19);
		int32_t L_20 = String_get_Length_m3847582255(L_19, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_20) == ((uint32_t)1))))
		{
			goto IL_0090;
		}
	}
	{
		String_t* L_21 = ___basePath0;
		NullCheck(L_21);
		Il2CppChar L_22 = String_get_Chars_m2986988803(L_21, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_0090;
		}
	}

IL_0089:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		___basePath0 = L_23;
	}

IL_0090:
	{
		Il2CppChar L_24 = V_1;
		if ((!(((uint32_t)L_24) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_00a2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B17_0 = L_25;
		goto IL_00a7;
	}

IL_00a2:
	{
		G_B17_0 = _stringLiteral3452614529;
	}

IL_00a7:
	{
		V_2 = G_B17_0;
		Il2CppChar L_26 = V_1;
		if ((!(((uint32_t)L_26) == ((uint32_t)((int32_t)126)))))
		{
			goto IL_0111;
		}
	}
	{
		int32_t L_27 = V_0;
		if ((!(((uint32_t)L_27) == ((uint32_t)1))))
		{
			goto IL_00c3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_28 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		___relPath1 = L_28;
		goto IL_00e7;
	}

IL_00c3:
	{
		int32_t L_29 = V_0;
		if ((((int32_t)L_29) <= ((int32_t)1)))
		{
			goto IL_00e7;
		}
	}
	{
		String_t* L_30 = ___relPath1;
		NullCheck(L_30);
		Il2CppChar L_31 = String_get_Chars_m2986988803(L_30, 1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_31) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_00e7;
		}
	}
	{
		String_t* L_32 = ___relPath1;
		NullCheck(L_32);
		String_t* L_33 = String_Substring_m2848979100(L_32, 2, /*hidden argument*/NULL);
		___relPath1 = L_33;
		V_2 = _stringLiteral3452614529;
	}

IL_00e7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HttpRuntime_t1480753741_il2cpp_TypeInfo_var);
		String_t* L_34 = HttpRuntime_get_AppDomainAppVirtualPath_m531373613(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_34;
		String_t* L_35 = V_3;
		NullCheck(L_35);
		bool L_36 = String_EndsWith_m1901926500(L_35, _stringLiteral3452614529, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_0103;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_37 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_2 = L_37;
	}

IL_0103:
	{
		String_t* L_38 = V_3;
		String_t* L_39 = V_2;
		String_t* L_40 = ___relPath1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_41 = String_Concat_m3755062657(NULL /*static, unused*/, L_38, L_39, L_40, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UrlUtils_t1214298665_il2cpp_TypeInfo_var);
		String_t* L_42 = UrlUtils_Canonic_m4232072973(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		return L_42;
	}

IL_0111:
	{
		String_t* L_43 = ___basePath0;
		String_t* L_44 = V_2;
		String_t* L_45 = ___relPath1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_46 = String_Concat_m3755062657(NULL /*static, unused*/, L_43, L_44, L_45, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UrlUtils_t1214298665_il2cpp_TypeInfo_var);
		String_t* L_47 = UrlUtils_Canonic_m4232072973(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		return L_47;
	}

IL_011f:
	{
		String_t* L_48 = ___basePath0;
		if (!L_48)
		{
			goto IL_013e;
		}
	}
	{
		String_t* L_49 = ___basePath0;
		NullCheck(L_49);
		int32_t L_50 = String_get_Length_m3847582255(L_49, /*hidden argument*/NULL);
		if (!L_50)
		{
			goto IL_013e;
		}
	}
	{
		String_t* L_51 = ___basePath0;
		NullCheck(L_51);
		Il2CppChar L_52 = String_get_Chars_m2986988803(L_51, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_52) == ((uint32_t)((int32_t)126)))))
		{
			goto IL_0145;
		}
	}

IL_013e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HttpRuntime_t1480753741_il2cpp_TypeInfo_var);
		String_t* L_53 = HttpRuntime_get_AppDomainAppVirtualPath_m531373613(NULL /*static, unused*/, /*hidden argument*/NULL);
		___basePath0 = L_53;
	}

IL_0145:
	{
		String_t* L_54 = ___basePath0;
		NullCheck(L_54);
		int32_t L_55 = String_get_Length_m3847582255(L_54, /*hidden argument*/NULL);
		if ((((int32_t)L_55) > ((int32_t)1)))
		{
			goto IL_0158;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_56 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		___basePath0 = L_56;
	}

IL_0158:
	{
		String_t* L_57 = ___basePath0;
		String_t* L_58 = ___relPath1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_59 = String_Concat_m3755062657(NULL /*static, unused*/, L_57, _stringLiteral3452614529, L_58, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UrlUtils_t1214298665_il2cpp_TypeInfo_var);
		String_t* L_60 = UrlUtils_Canonic_m4232072973(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
		return L_60;
	}
}
// System.String System.Web.Util.UrlUtils::Canonic(System.String)
extern "C"  String_t* UrlUtils_Canonic_m4232072973 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UrlUtils_Canonic_m4232072973_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	StringU5BU5D_t1281789340* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	{
		String_t* L_0 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(UrlUtils_t1214298665_il2cpp_TypeInfo_var);
		bool L_1 = UrlUtils_IsRooted_m3690953646(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = ___path0;
		NullCheck(L_2);
		bool L_3 = String_EndsWith_m1901926500(L_2, _stringLiteral3452614529, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = ___path0;
		CharU5BU5D_t3528271667* L_5 = ((UrlUtils_t1214298665_StaticFields*)UrlUtils_t1214298665_il2cpp_TypeInfo_var->static_fields)->get_path_sep_0();
		NullCheck(L_4);
		StringU5BU5D_t1281789340* L_6 = String_Split_m3646115398(L_4, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		StringU5BU5D_t1281789340* L_7 = V_2;
		NullCheck(L_7);
		V_3 = (((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))));
		V_4 = 0;
		V_5 = 0;
		goto IL_00a4;
	}

IL_002e:
	{
		StringU5BU5D_t1281789340* L_8 = V_2;
		int32_t L_9 = V_5;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		String_t* L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		V_6 = L_11;
		String_t* L_12 = V_6;
		NullCheck(L_12);
		int32_t L_13 = String_get_Length_m3847582255(L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0045;
		}
	}
	{
		goto IL_009e;
	}

IL_0045:
	{
		String_t* L_14 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_op_Equality_m920492651(NULL /*static, unused*/, L_14, _stringLiteral3452614530, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_005b;
		}
	}
	{
		goto IL_009e;
	}

IL_005b:
	{
		String_t* L_16 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_17 = String_op_Equality_m920492651(NULL /*static, unused*/, L_16, _stringLiteral3450648450, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0077;
		}
	}
	{
		int32_t L_18 = V_4;
		V_4 = ((int32_t)((int32_t)L_18-(int32_t)1));
		goto IL_009e;
	}

IL_0077:
	{
		int32_t L_19 = V_4;
		if ((((int32_t)L_19) >= ((int32_t)0)))
		{
			goto IL_0093;
		}
	}
	{
		bool L_20 = V_0;
		if (L_20)
		{
			goto IL_0090;
		}
	}
	{
		HttpException_t2907797370 * L_21 = (HttpException_t2907797370 *)il2cpp_codegen_object_new(HttpException_t2907797370_il2cpp_TypeInfo_var);
		HttpException__ctor_m2549443145(L_21, _stringLiteral962976914, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}

IL_0090:
	{
		V_4 = 0;
	}

IL_0093:
	{
		StringU5BU5D_t1281789340* L_22 = V_2;
		int32_t L_23 = V_4;
		int32_t L_24 = L_23;
		V_4 = ((int32_t)((int32_t)L_24+(int32_t)1));
		String_t* L_25 = V_6;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_25);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(L_24), (String_t*)L_25);
	}

IL_009e:
	{
		int32_t L_26 = V_5;
		V_5 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_00a4:
	{
		int32_t L_27 = V_5;
		int32_t L_28 = V_3;
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_29 = V_4;
		if ((((int32_t)L_29) >= ((int32_t)0)))
		{
			goto IL_00bf;
		}
	}
	{
		HttpException_t2907797370 * L_30 = (HttpException_t2907797370 *)il2cpp_codegen_object_new(HttpException_t2907797370_il2cpp_TypeInfo_var);
		HttpException__ctor_m2549443145(L_30, _stringLiteral962976914, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_30);
	}

IL_00bf:
	{
		int32_t L_31 = V_4;
		if (L_31)
		{
			goto IL_00cc;
		}
	}
	{
		return _stringLiteral3452614529;
	}

IL_00cc:
	{
		StringU5BU5D_t1281789340* L_32 = V_2;
		int32_t L_33 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Join_m29736248(NULL /*static, unused*/, _stringLiteral3452614529, L_32, 0, L_33, /*hidden argument*/NULL);
		V_7 = L_34;
		String_t* L_35 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(UrlUtils_t1214298665_il2cpp_TypeInfo_var);
		String_t* L_36 = UrlUtils_RemoveDoubleSlashes_m2124540071(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		V_7 = L_36;
		bool L_37 = V_0;
		if (!L_37)
		{
			goto IL_00f9;
		}
	}
	{
		String_t* L_38 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_39 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3452614529, L_38, /*hidden argument*/NULL);
		V_7 = L_39;
	}

IL_00f9:
	{
		bool L_40 = V_1;
		if (!L_40)
		{
			goto IL_010d;
		}
	}
	{
		String_t* L_41 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_42 = String_Concat_m3937257545(NULL /*static, unused*/, L_41, _stringLiteral3452614529, /*hidden argument*/NULL);
		V_7 = L_42;
	}

IL_010d:
	{
		String_t* L_43 = V_7;
		return L_43;
	}
}
// System.String System.Web.Util.UrlUtils::GetDirectory(System.String)
extern "C"  String_t* UrlUtils_GetDirectory_m1298890870 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UrlUtils_GetDirectory_m1298890870_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___url0;
		NullCheck(L_0);
		String_t* L_1 = String_Replace_m3726209165(L_0, ((int32_t)92), ((int32_t)47), /*hidden argument*/NULL);
		___url0 = L_1;
		String_t* L_2 = ___url0;
		NullCheck(L_2);
		int32_t L_3 = String_LastIndexOf_m3451222878(L_2, ((int32_t)47), /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_5 = V_0;
		String_t* L_6 = ___url0;
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m3847582255(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_5) >= ((int32_t)L_7)))
		{
			goto IL_002c;
		}
	}
	{
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002c:
	{
		String_t* L_9 = ___url0;
		int32_t L_10 = V_0;
		NullCheck(L_9);
		String_t* L_11 = String_Substring_m1610150815(L_9, 0, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UrlUtils_t1214298665_il2cpp_TypeInfo_var);
		String_t* L_12 = UrlUtils_RemoveDoubleSlashes_m2124540071(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return L_12;
	}

IL_003a:
	{
		return _stringLiteral3452614529;
	}
}
// System.String System.Web.Util.UrlUtils::RemoveDoubleSlashes(System.String)
extern "C"  String_t* UrlUtils_RemoveDoubleSlashes_m2124540071 (Il2CppObject * __this /* static, unused */, String_t* ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UrlUtils_RemoveDoubleSlashes_m2124540071_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	StringBuilder_t1712802186 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		V_0 = (-1);
		V_1 = 1;
		goto IL_0034;
	}

IL_0009:
	{
		String_t* L_0 = ___input0;
		int32_t L_1 = V_1;
		NullCheck(L_0);
		Il2CppChar L_2 = String_get_Chars_m2986988803(L_0, L_1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_0030;
		}
	}
	{
		String_t* L_3 = ___input0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		Il2CppChar L_5 = String_get_Chars_m2986988803(L_3, ((int32_t)((int32_t)L_4-(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_0030;
		}
	}
	{
		int32_t L_6 = V_1;
		V_0 = ((int32_t)((int32_t)L_6-(int32_t)1));
		goto IL_0040;
	}

IL_0030:
	{
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0034:
	{
		int32_t L_8 = V_1;
		String_t* L_9 = ___input0;
		NullCheck(L_9);
		int32_t L_10 = String_get_Length_m3847582255(L_9, /*hidden argument*/NULL);
		if ((((int32_t)L_8) < ((int32_t)L_10)))
		{
			goto IL_0009;
		}
	}

IL_0040:
	{
		int32_t L_11 = V_0;
		if ((!(((uint32_t)L_11) == ((uint32_t)(-1)))))
		{
			goto IL_0049;
		}
	}
	{
		String_t* L_12 = ___input0;
		return L_12;
	}

IL_0049:
	{
		String_t* L_13 = ___input0;
		NullCheck(L_13);
		int32_t L_14 = String_get_Length_m3847582255(L_13, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_15 = (StringBuilder_t1712802186 *)il2cpp_codegen_object_new(StringBuilder_t1712802186_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m2367297767(L_15, L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		StringBuilder_t1712802186 * L_16 = V_2;
		String_t* L_17 = ___input0;
		int32_t L_18 = V_0;
		NullCheck(L_16);
		StringBuilder_Append_m3214161208(L_16, L_17, 0, L_18, /*hidden argument*/NULL);
		int32_t L_19 = V_0;
		V_3 = L_19;
		goto IL_00ba;
	}

IL_0066:
	{
		String_t* L_20 = ___input0;
		int32_t L_21 = V_3;
		NullCheck(L_20);
		Il2CppChar L_22 = String_get_Chars_m2986988803(L_20, L_21, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_00a8;
		}
	}
	{
		int32_t L_23 = V_3;
		V_4 = ((int32_t)((int32_t)L_23+(int32_t)1));
		int32_t L_24 = V_4;
		String_t* L_25 = ___input0;
		NullCheck(L_25);
		int32_t L_26 = String_get_Length_m3847582255(L_25, /*hidden argument*/NULL);
		if ((((int32_t)L_24) >= ((int32_t)L_26)))
		{
			goto IL_009a;
		}
	}
	{
		String_t* L_27 = ___input0;
		int32_t L_28 = V_4;
		NullCheck(L_27);
		Il2CppChar L_29 = String_get_Chars_m2986988803(L_27, L_28, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_29) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_009a;
		}
	}
	{
		goto IL_00b6;
	}

IL_009a:
	{
		StringBuilder_t1712802186 * L_30 = V_2;
		NullCheck(L_30);
		StringBuilder_Append_m2383614642(L_30, ((int32_t)47), /*hidden argument*/NULL);
		goto IL_00b6;
	}

IL_00a8:
	{
		StringBuilder_t1712802186 * L_31 = V_2;
		String_t* L_32 = ___input0;
		int32_t L_33 = V_3;
		NullCheck(L_32);
		Il2CppChar L_34 = String_get_Chars_m2986988803(L_32, L_33, /*hidden argument*/NULL);
		NullCheck(L_31);
		StringBuilder_Append_m2383614642(L_31, L_34, /*hidden argument*/NULL);
	}

IL_00b6:
	{
		int32_t L_35 = V_3;
		V_3 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_00ba:
	{
		int32_t L_36 = V_3;
		String_t* L_37 = ___input0;
		NullCheck(L_37);
		int32_t L_38 = String_get_Length_m3847582255(L_37, /*hidden argument*/NULL);
		if ((((int32_t)L_36) < ((int32_t)L_38)))
		{
			goto IL_0066;
		}
	}
	{
		StringBuilder_t1712802186 * L_39 = V_2;
		NullCheck(L_39);
		String_t* L_40 = StringBuilder_ToString_m3317489284(L_39, /*hidden argument*/NULL);
		return L_40;
	}
}
// System.String System.Web.Util.UrlUtils::GetFile(System.String)
extern "C"  String_t* UrlUtils_GetFile_m1187780462 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UrlUtils_GetFile_m1187780462_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___url0;
		NullCheck(L_0);
		String_t* L_1 = String_Replace_m3726209165(L_0, ((int32_t)92), ((int32_t)47), /*hidden argument*/NULL);
		___url0 = L_1;
		String_t* L_2 = ___url0;
		NullCheck(L_2);
		int32_t L_3 = String_LastIndexOf_m3451222878(L_2, ((int32_t)47), /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0038;
		}
	}
	{
		String_t* L_5 = ___url0;
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m3847582255(L_5, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_7;
	}

IL_002e:
	{
		String_t* L_8 = ___url0;
		int32_t L_9 = V_0;
		NullCheck(L_8);
		String_t* L_10 = String_Substring_m2848979100(L_8, ((int32_t)((int32_t)L_9+(int32_t)1)), /*hidden argument*/NULL);
		return L_10;
	}

IL_0038:
	{
		String_t* L_11 = ___url0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral3395286453, L_11, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_13 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_13, L_12, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13);
	}
}
// System.Boolean System.Web.Util.UrlUtils::IsRooted(System.String)
extern "C"  bool UrlUtils_IsRooted_m3690953646 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	Il2CppChar V_0 = 0x0;
	{
		String_t* L_0 = ___path0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___path0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m3847582255(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0013;
		}
	}

IL_0011:
	{
		return (bool)1;
	}

IL_0013:
	{
		String_t* L_3 = ___path0;
		NullCheck(L_3);
		Il2CppChar L_4 = String_get_Chars_m2986988803(L_3, 0, /*hidden argument*/NULL);
		V_0 = L_4;
		Il2CppChar L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)47))))
		{
			goto IL_002b;
		}
	}
	{
		Il2CppChar L_6 = V_0;
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_002d;
		}
	}

IL_002b:
	{
		return (bool)1;
	}

IL_002d:
	{
		return (bool)0;
	}
}
// System.Boolean System.Web.Util.UrlUtils::IsRelativeUrl(System.String)
extern "C"  bool UrlUtils_IsRelativeUrl_m706272008 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___path0;
		NullCheck(L_0);
		Il2CppChar L_1 = String_get_Chars_m2986988803(L_0, 0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)((int32_t)47))))
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_2 = ___path0;
		NullCheck(L_2);
		int32_t L_3 = String_IndexOf_m363431711(L_2, ((int32_t)58), /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0);
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 0;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Web.Configuration.GlobalizationSection System.Web.Util.WebEncoding::get_GlobalizationConfig()
extern "C"  GlobalizationSection_t244366663 * WebEncoding_get_GlobalizationConfig_m2546015474 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebEncoding_get_GlobalizationConfig_m2546015474_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = ((WebEncoding_t373074341_StaticFields*)WebEncoding_t373074341_il2cpp_TypeInfo_var->static_fields)->get_cached_0();
		if (L_0)
		{
			goto IL_002f;
		}
	}

IL_000a:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(WebConfigurationManager_t2451561378_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = WebConfigurationManager_GetWebApplicationSection_m4036650157(NULL /*static, unused*/, _stringLiteral992186917, /*hidden argument*/NULL);
		((WebEncoding_t373074341_StaticFields*)WebEncoding_t373074341_il2cpp_TypeInfo_var->static_fields)->set_sect_1(((GlobalizationSection_t244366663 *)CastclassSealed(L_1, GlobalizationSection_t244366663_il2cpp_TypeInfo_var)));
		goto IL_0029;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0023;
		throw e;
	}

CATCH_0023:
	{ // begin catch(System.Object)
		goto IL_0029;
	} // end catch (depth: 1)

IL_0029:
	{
		((WebEncoding_t373074341_StaticFields*)WebEncoding_t373074341_il2cpp_TypeInfo_var->static_fields)->set_cached_0((bool)1);
	}

IL_002f:
	{
		GlobalizationSection_t244366663 * L_2 = ((WebEncoding_t373074341_StaticFields*)WebEncoding_t373074341_il2cpp_TypeInfo_var->static_fields)->get_sect_1();
		return L_2;
	}
}
// System.Text.Encoding System.Web.Util.WebEncoding::get_FileEncoding()
extern "C"  Encoding_t1523322056 * WebEncoding_get_FileEncoding_m4274600026 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebEncoding_get_FileEncoding_m4274600026_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Encoding_t1523322056 * G_B3_0 = NULL;
	{
		GlobalizationSection_t244366663 * L_0 = WebEncoding_get_GlobalizationConfig_m2546015474(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		GlobalizationSection_t244366663 * L_1 = WebEncoding_get_GlobalizationConfig_m2546015474(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Encoding_t1523322056 * L_2 = GlobalizationSection_get_FileEncoding_m1644580766(L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_001e;
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1523322056_il2cpp_TypeInfo_var);
		Encoding_t1523322056 * L_3 = Encoding_get_Default_m1632902165(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_001e:
	{
		return G_B3_0;
	}
}
// System.Text.Encoding System.Web.Util.WebEncoding::get_ResponseEncoding()
extern "C"  Encoding_t1523322056 * WebEncoding_get_ResponseEncoding_m919385210 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebEncoding_get_ResponseEncoding_m919385210_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Encoding_t1523322056 * G_B3_0 = NULL;
	{
		GlobalizationSection_t244366663 * L_0 = WebEncoding_get_GlobalizationConfig_m2546015474(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		GlobalizationSection_t244366663 * L_1 = WebEncoding_get_GlobalizationConfig_m2546015474(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Encoding_t1523322056 * L_2 = GlobalizationSection_get_ResponseEncoding_m831683111(L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_001e;
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1523322056_il2cpp_TypeInfo_var);
		Encoding_t1523322056 * L_3 = Encoding_get_Default_m1632902165(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_001e:
	{
		return G_B3_0;
	}
}
// System.Text.Encoding System.Web.Util.WebEncoding::get_RequestEncoding()
extern "C"  Encoding_t1523322056 * WebEncoding_get_RequestEncoding_m3410676394 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebEncoding_get_RequestEncoding_m3410676394_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Encoding_t1523322056 * G_B3_0 = NULL;
	{
		GlobalizationSection_t244366663 * L_0 = WebEncoding_get_GlobalizationConfig_m2546015474(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		GlobalizationSection_t244366663 * L_1 = WebEncoding_get_GlobalizationConfig_m2546015474(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Encoding_t1523322056 * L_2 = GlobalizationSection_get_RequestEncoding_m2054852899(L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_001e;
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1523322056_il2cpp_TypeInfo_var);
		Encoding_t1523322056 * L_3 = Encoding_get_Default_m1632902165(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_001e:
	{
		return G_B3_0;
	}
}
// System.Void System.Web.VirtualPath::.ctor(System.String)
extern "C"  void VirtualPath__ctor_m3027638245 (VirtualPath_t4270372584 * __this, String_t* ___vpath0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___vpath0;
		VirtualPath__ctor_m3863291854(__this, L_0, (String_t*)NULL, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Web.VirtualPath::.ctor(System.String,System.String)
extern "C"  void VirtualPath__ctor_m1919362345 (VirtualPath_t4270372584 * __this, String_t* ___vpath0, String_t* ___baseVirtualDir1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___vpath0;
		VirtualPath__ctor_m3863291854(__this, L_0, (String_t*)NULL, (bool)0, /*hidden argument*/NULL);
		String_t* L_1 = ___baseVirtualDir1;
		VirtualPath_set_CurrentRequestDirectory_m4189897870(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Web.VirtualPath::.ctor(System.String,System.String,System.Boolean)
extern "C"  void VirtualPath__ctor_m3863291854 (VirtualPath_t4270372584 * __this, String_t* ___vpath0, String_t* ___physicalPath1, bool ___isFake2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPath__ctor_m3863291854_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___vpath0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		bool L_1 = VirtualPathUtility_IsRooted_m3278026923(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		VirtualPath_set_IsRooted_m733387534(__this, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___vpath0;
		bool L_3 = VirtualPathUtility_IsAbsolute_m2473455663(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		VirtualPath_set_IsAbsolute_m4213413353(__this, L_3, /*hidden argument*/NULL);
		String_t* L_4 = ___vpath0;
		bool L_5 = VirtualPathUtility_IsAppRelative_m548692695(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		VirtualPath_set_IsAppRelative_m3998062203(__this, L_5, /*hidden argument*/NULL);
		bool L_6 = ___isFake2;
		if (!L_6)
		{
			goto IL_0074;
		}
	}
	{
		String_t* L_7 = ___physicalPath1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0046;
		}
	}
	{
		ArgumentException_t132251570 * L_9 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_9, _stringLiteral1448479444, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0046:
	{
		String_t* L_10 = ___physicalPath1;
		__this->set__physicalPath_7(L_10);
		String_t* L_11 = __this->get__physicalPath_7();
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_12 = Path_GetFileName_m1354558116(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3450582994, L_12, /*hidden argument*/NULL);
		VirtualPath_set_Original_m1325633546(__this, L_13, /*hidden argument*/NULL);
		VirtualPath_set_IsFake_m1547374389(__this, (bool)1, /*hidden argument*/NULL);
		goto IL_0082;
	}

IL_0074:
	{
		String_t* L_14 = ___vpath0;
		VirtualPath_set_Original_m1325633546(__this, L_14, /*hidden argument*/NULL);
		VirtualPath_set_IsFake_m1547374389(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_0082:
	{
		return;
	}
}
// System.Boolean System.Web.VirtualPath::get_IsAbsolute()
extern "C"  bool VirtualPath_get_IsAbsolute_m2856159077 (VirtualPath_t4270372584 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CIsAbsoluteU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void System.Web.VirtualPath::set_IsAbsolute(System.Boolean)
extern "C"  void VirtualPath_set_IsAbsolute_m4213413353 (VirtualPath_t4270372584 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CIsAbsoluteU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Boolean System.Web.VirtualPath::get_IsFake()
extern "C"  bool VirtualPath_get_IsFake_m566634131 (VirtualPath_t4270372584 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CIsFakeU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void System.Web.VirtualPath::set_IsFake(System.Boolean)
extern "C"  void VirtualPath_set_IsFake_m1547374389 (VirtualPath_t4270372584 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CIsFakeU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Void System.Web.VirtualPath::set_IsRooted(System.Boolean)
extern "C"  void VirtualPath_set_IsRooted_m733387534 (VirtualPath_t4270372584 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CIsRootedU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.Boolean System.Web.VirtualPath::get_IsAppRelative()
extern "C"  bool VirtualPath_get_IsAppRelative_m1108160356 (VirtualPath_t4270372584 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CIsAppRelativeU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void System.Web.VirtualPath::set_IsAppRelative(System.Boolean)
extern "C"  void VirtualPath_set_IsAppRelative_m3998062203 (VirtualPath_t4270372584 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CIsAppRelativeU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.String System.Web.VirtualPath::get_Original()
extern "C"  String_t* VirtualPath_get_Original_m1834498342 (VirtualPath_t4270372584 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3COriginalU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void System.Web.VirtualPath::set_Original(System.String)
extern "C"  void VirtualPath_set_Original_m1325633546 (VirtualPath_t4270372584 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3COriginalU3Ek__BackingField_12(L_0);
		return;
	}
}
// System.String System.Web.VirtualPath::get_Absolute()
extern "C"  String_t* VirtualPath_get_Absolute_m3947569336 (VirtualPath_t4270372584 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPath_get_Absolute_m3947569336_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		bool L_0 = VirtualPath_get_IsAbsolute_m2856159077(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_1 = VirtualPath_get_Original_m1834498342(__this, /*hidden argument*/NULL);
		return L_1;
	}

IL_0012:
	{
		String_t* L_2 = __this->get__absolute_0();
		if (L_2)
		{
			goto IL_0069;
		}
	}
	{
		String_t* L_3 = VirtualPath_get_Original_m1834498342(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		bool L_5 = VirtualPathUtility_IsRooted_m3278026923(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		String_t* L_6 = V_0;
		String_t* L_7 = VirtualPath_MakeRooted_m3007486533(__this, L_6, /*hidden argument*/NULL);
		__this->set__absolute_0(L_7);
		goto IL_0048;
	}

IL_0041:
	{
		String_t* L_8 = V_0;
		__this->set__absolute_0(L_8);
	}

IL_0048:
	{
		String_t* L_9 = __this->get__absolute_0();
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		bool L_10 = VirtualPathUtility_IsAppRelative_m548692695(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0069;
		}
	}
	{
		String_t* L_11 = __this->get__absolute_0();
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_12 = VirtualPathUtility_ToAbsolute_m373284300(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		__this->set__absolute_0(L_12);
	}

IL_0069:
	{
		String_t* L_13 = __this->get__absolute_0();
		return L_13;
	}
}
// System.String System.Web.VirtualPath::get_AppRelative()
extern "C"  String_t* VirtualPath_get_AppRelative_m2233295149 (VirtualPath_t4270372584 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPath_get_AppRelative_m2233295149_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		bool L_0 = VirtualPath_get_IsAppRelative_m1108160356(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_1 = VirtualPath_get_Original_m1834498342(__this, /*hidden argument*/NULL);
		return L_1;
	}

IL_0012:
	{
		String_t* L_2 = __this->get__appRelative_1();
		if (L_2)
		{
			goto IL_0069;
		}
	}
	{
		String_t* L_3 = VirtualPath_get_Original_m1834498342(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		bool L_5 = VirtualPathUtility_IsRooted_m3278026923(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		String_t* L_6 = V_0;
		String_t* L_7 = VirtualPath_MakeRooted_m3007486533(__this, L_6, /*hidden argument*/NULL);
		__this->set__appRelative_1(L_7);
		goto IL_0048;
	}

IL_0041:
	{
		String_t* L_8 = V_0;
		__this->set__appRelative_1(L_8);
	}

IL_0048:
	{
		String_t* L_9 = __this->get__appRelative_1();
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		bool L_10 = VirtualPathUtility_IsAbsolute_m2473455663(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0069;
		}
	}
	{
		String_t* L_11 = __this->get__appRelative_1();
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_12 = VirtualPathUtility_ToAppRelative_m769660906(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		__this->set__appRelative_1(L_12);
	}

IL_0069:
	{
		String_t* L_13 = __this->get__appRelative_1();
		return L_13;
	}
}
// System.String System.Web.VirtualPath::get_Extension()
extern "C"  String_t* VirtualPath_get_Extension_m209795241 (VirtualPath_t4270372584 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPath_get_Extension_m209795241_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get__extension_3();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = VirtualPath_get_Original_m1834498342(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_2 = VirtualPathUtility_GetExtension_m3822181684(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		__this->set__extension_3(L_2);
	}

IL_001c:
	{
		String_t* L_3 = __this->get__extension_3();
		return L_3;
	}
}
// System.String System.Web.VirtualPath::get_Directory()
extern "C"  String_t* VirtualPath_get_Directory_m1071987848 (VirtualPath_t4270372584 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPath_get_Directory_m1071987848_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get__directory_4();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = VirtualPath_get_Absolute_m3947569336(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_2 = VirtualPathUtility_GetDirectory_m2076898960(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		__this->set__directory_4(L_2);
	}

IL_001c:
	{
		String_t* L_3 = __this->get__directory_4();
		return L_3;
	}
}
// System.String System.Web.VirtualPath::get_DirectoryNoNormalize()
extern "C"  String_t* VirtualPath_get_DirectoryNoNormalize_m2890043560 (VirtualPath_t4270372584 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPath_get_DirectoryNoNormalize_m2890043560_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get__directoryNoNormalize_5();
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		String_t* L_1 = VirtualPath_get_Absolute_m3947569336(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_2 = VirtualPathUtility_GetDirectory_m404138870(NULL /*static, unused*/, L_1, (bool)0, /*hidden argument*/NULL);
		__this->set__directoryNoNormalize_5(L_2);
	}

IL_001d:
	{
		String_t* L_3 = __this->get__directoryNoNormalize_5();
		return L_3;
	}
}
// System.String System.Web.VirtualPath::get_CurrentRequestDirectory()
extern "C"  String_t* VirtualPath_get_CurrentRequestDirectory_m3125580679 (VirtualPath_t4270372584 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPath_get_CurrentRequestDirectory_m3125580679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HttpContext_t1969259010 * V_0 = NULL;
	HttpRequest_t809700260 * V_1 = NULL;
	HttpRequest_t809700260 * G_B5_0 = NULL;
	{
		String_t* L_0 = __this->get__currentRequestDirectory_6();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_1 = __this->get__currentRequestDirectory_6();
		return L_1;
	}

IL_0012:
	{
		HttpContext_t1969259010 * L_2 = HttpContext_get_Current_m1955064578(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		HttpContext_t1969259010 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		HttpContext_t1969259010 * L_4 = V_0;
		NullCheck(L_4);
		HttpRequest_t809700260 * L_5 = HttpContext_get_Request_m1929562575(L_4, /*hidden argument*/NULL);
		G_B5_0 = L_5;
		goto IL_002a;
	}

IL_0029:
	{
		G_B5_0 = ((HttpRequest_t809700260 *)(NULL));
	}

IL_002a:
	{
		V_1 = G_B5_0;
		HttpRequest_t809700260 * L_6 = V_1;
		if (!L_6)
		{
			goto IL_003d;
		}
	}
	{
		HttpRequest_t809700260 * L_7 = V_1;
		NullCheck(L_7);
		String_t* L_8 = HttpRequest_get_CurrentExecutionFilePath_m2109644776(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_9 = VirtualPathUtility_GetDirectory_m2076898960(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_003d:
	{
		return (String_t*)NULL;
	}
}
// System.Void System.Web.VirtualPath::set_CurrentRequestDirectory(System.String)
extern "C"  void VirtualPath_set_CurrentRequestDirectory_m4189897870 (VirtualPath_t4270372584 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__currentRequestDirectory_6(L_0);
		return;
	}
}
// System.String System.Web.VirtualPath::get_PhysicalPath()
extern "C"  String_t* VirtualPath_get_PhysicalPath_m2235883404 (VirtualPath_t4270372584 * __this, const MethodInfo* method)
{
	HttpContext_t1969259010 * V_0 = NULL;
	HttpRequest_t809700260 * V_1 = NULL;
	HttpRequest_t809700260 * G_B5_0 = NULL;
	{
		String_t* L_0 = __this->get__physicalPath_7();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_1 = __this->get__physicalPath_7();
		return L_1;
	}

IL_0012:
	{
		HttpContext_t1969259010 * L_2 = HttpContext_get_Current_m1955064578(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		HttpContext_t1969259010 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		HttpContext_t1969259010 * L_4 = V_0;
		NullCheck(L_4);
		HttpRequest_t809700260 * L_5 = HttpContext_get_Request_m1929562575(L_4, /*hidden argument*/NULL);
		G_B5_0 = L_5;
		goto IL_002a;
	}

IL_0029:
	{
		G_B5_0 = ((HttpRequest_t809700260 *)(NULL));
	}

IL_002a:
	{
		V_1 = G_B5_0;
		HttpRequest_t809700260 * L_6 = V_1;
		if (!L_6)
		{
			goto IL_0048;
		}
	}
	{
		HttpRequest_t809700260 * L_7 = V_1;
		String_t* L_8 = VirtualPath_get_Absolute_m3947569336(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_9 = HttpRequest_MapPath_m3338942164(L_7, L_8, /*hidden argument*/NULL);
		__this->set__physicalPath_7(L_9);
		goto IL_004a;
	}

IL_0048:
	{
		return (String_t*)NULL;
	}

IL_004a:
	{
		String_t* L_10 = __this->get__physicalPath_7();
		return L_10;
	}
}
// System.String System.Web.VirtualPath::MakeRooted(System.String)
extern "C"  String_t* VirtualPath_MakeRooted_m3007486533 (VirtualPath_t4270372584 * __this, String_t* ___original0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPath_MakeRooted_m3007486533_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = VirtualPath_get_CurrentRequestDirectory_m3125580679(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001a;
		}
	}
	{
		String_t* L_3 = V_0;
		String_t* L_4 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_5 = VirtualPathUtility_Combine_m2177271470(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HttpRuntime_t1480753741_il2cpp_TypeInfo_var);
		String_t* L_6 = HttpRuntime_get_AppDomainAppVirtualPath_m531373613(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_7 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_8 = VirtualPathUtility_Combine_m2177271470(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void System.Web.VirtualPath::Dispose()
extern "C"  void VirtualPath_Dispose_m4229231512 (VirtualPath_t4270372584 * __this, const MethodInfo* method)
{
	{
		__this->set__absolute_0((String_t*)NULL);
		__this->set__appRelative_1((String_t*)NULL);
		__this->set__appRelativeNotRooted_2((String_t*)NULL);
		__this->set__extension_3((String_t*)NULL);
		__this->set__directory_4((String_t*)NULL);
		return;
	}
}
// System.String System.Web.VirtualPath::ToString()
extern "C"  String_t* VirtualPath_ToString_m1191822246 (VirtualPath_t4270372584 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPath_ToString_m1191822246_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = VirtualPath_get_Original_m1834498342(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Type_t * L_3 = Object_GetType_m88164663(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, L_3);
		return L_4;
	}

IL_001e:
	{
		bool L_5 = VirtualPath_get_IsFake_m566634131(__this, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0040;
		}
	}
	{
		String_t* L_6 = V_0;
		String_t* L_7 = VirtualPath_get_PhysicalPath_m2235883404(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m2163913788(NULL /*static, unused*/, L_6, _stringLiteral3822744503, L_7, _stringLiteral3452614643, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0040:
	{
		String_t* L_9 = V_0;
		return L_9;
	}
}
// System.Web.VirtualPath System.Web.VirtualPath::PhysicalToVirtual(System.String)
extern "C"  VirtualPath_t4270372584 * VirtualPath_PhysicalToVirtual_m75598891 (Il2CppObject * __this /* static, unused */, String_t* ___physical_path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPath_PhysicalToVirtual_m75598891_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	{
		String_t* L_0 = ___physical_path0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (VirtualPath_t4270372584 *)NULL;
	}

IL_000d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HttpRuntime_t1480753741_il2cpp_TypeInfo_var);
		String_t* L_2 = HttpRuntime_get_AppDomainAppPath_m94348122(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = ___physical_path0;
		String_t* L_4 = V_0;
		bool L_5 = StrUtils_StartsWith_m2313343174(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0021;
		}
	}
	{
		return (VirtualPath_t4270372584 *)NULL;
	}

IL_0021:
	{
		String_t* L_6 = ___physical_path0;
		String_t* L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = String_get_Length_m3847582255(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_9 = String_Substring_m2848979100(L_6, ((int32_t)((int32_t)L_8-(int32_t)1)), /*hidden argument*/NULL);
		V_1 = L_9;
		String_t* L_10 = V_1;
		NullCheck(L_10);
		Il2CppChar L_11 = String_get_Chars_m2986988803(L_10, 0, /*hidden argument*/NULL);
		if ((((int32_t)L_11) == ((int32_t)((int32_t)47))))
		{
			goto IL_0040;
		}
	}
	{
		return (VirtualPath_t4270372584 *)NULL;
	}

IL_0040:
	{
		String_t* L_12 = V_1;
		VirtualPath_t4270372584 * L_13 = (VirtualPath_t4270372584 *)il2cpp_codegen_object_new(VirtualPath_t4270372584_il2cpp_TypeInfo_var);
		VirtualPath__ctor_m3027638245(L_13, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void System.Web.VirtualPathUtility::.cctor()
extern "C"  void VirtualPathUtility__cctor_m2171500222 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPathUtility__cctor_m2171500222_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MonoSettingsSection_t2949572421 * V_0 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CharU5BU5D_t3528271667* L_0 = ((CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)47));
		((VirtualPathUtility_t3131300229_StaticFields*)VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var->static_fields)->set_path_sep_2(L_0);
		CharU5BU5D_t3528271667* L_1 = ((CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)58));
		CharU5BU5D_t3528271667* L_2 = L_1;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppChar)((int32_t)42));
		((VirtualPathUtility_t3131300229_StaticFields*)VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var->static_fields)->set_invalidVirtualPathChars_3(L_2);
		((VirtualPathUtility_t3131300229_StaticFields*)VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var->static_fields)->set_aspNetVerificationKey_4(_stringLiteral790887489);
	}

IL_002f:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(HttpRuntime_t1480753741_il2cpp_TypeInfo_var);
			bool L_3 = HttpRuntime_get_RunningOnWindows_m22177484(NULL /*static, unused*/, /*hidden argument*/NULL);
			((VirtualPathUtility_t3131300229_StaticFields*)VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var->static_fields)->set_runningOnWindows_1(L_3);
			IL2CPP_RUNTIME_CLASS_INIT(WebConfigurationManager_t2451561378_il2cpp_TypeInfo_var);
			Il2CppObject * L_4 = WebConfigurationManager_GetWebApplicationSection_m4036650157(NULL /*static, unused*/, _stringLiteral3955796716, /*hidden argument*/NULL);
			V_0 = ((MonoSettingsSection_t2949572421 *)IsInstSealed(L_4, MonoSettingsSection_t2949572421_il2cpp_TypeInfo_var));
			MonoSettingsSection_t2949572421 * L_5 = V_0;
			if (!L_5)
			{
				goto IL_0060;
			}
		}

IL_004f:
		{
			MonoSettingsSection_t2949572421 * L_6 = V_0;
			NullCheck(L_6);
			int32_t L_7 = MonoSettingsSection_get_VerificationCompatibility_m2509066582(L_6, /*hidden argument*/NULL);
			((VirtualPathUtility_t3131300229_StaticFields*)VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var->static_fields)->set_monoSettingsVerifyCompatibility_0((bool)((((int32_t)((((int32_t)L_7) == ((int32_t)1))? 1 : 0)) == ((int32_t)0))? 1 : 0));
		}

IL_0060:
		{
			goto IL_006b;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0065;
		throw e;
	}

CATCH_0065:
	{ // begin catch(System.Object)
		goto IL_006b;
	} // end catch (depth: 1)

IL_006b:
	{
		return;
	}
}
// System.String System.Web.VirtualPathUtility::AppendTrailingSlash(System.String)
extern "C"  String_t* VirtualPathUtility_AppendTrailingSlash_m659506801 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPathUtility_AppendTrailingSlash_m659506801_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___virtualPath0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		String_t* L_1 = ___virtualPath0;
		return L_1;
	}

IL_0008:
	{
		String_t* L_2 = ___virtualPath0;
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m3847582255(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		String_t* L_5 = ___virtualPath0;
		int32_t L_6 = V_0;
		NullCheck(L_5);
		Il2CppChar L_7 = String_get_Chars_m2986988803(L_5, ((int32_t)((int32_t)L_6-(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_0027;
		}
	}

IL_0025:
	{
		String_t* L_8 = ___virtualPath0;
		return L_8;
	}

IL_0027:
	{
		String_t* L_9 = ___virtualPath0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m3937257545(NULL /*static, unused*/, L_9, _stringLiteral3452614529, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.String System.Web.VirtualPathUtility::Combine(System.String,System.String)
extern "C"  String_t* VirtualPathUtility_Combine_m2177271470 (Il2CppObject * __this /* static, unused */, String_t* ___basePath0, String_t* ___relativePath1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPathUtility_Combine_m2177271470_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___basePath0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_1 = VirtualPathUtility_Normalize_m1039358837(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___basePath0 = L_1;
		String_t* L_2 = ___relativePath1;
		bool L_3 = VirtualPathUtility_IsRooted_m3278026923(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001a;
		}
	}
	{
		String_t* L_4 = ___relativePath1;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_5 = VirtualPathUtility_Normalize_m1039358837(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_001a:
	{
		String_t* L_6 = ___basePath0;
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m3847582255(L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		String_t* L_8 = ___basePath0;
		int32_t L_9 = V_0;
		NullCheck(L_8);
		Il2CppChar L_10 = String_get_Chars_m2986988803(L_8, ((int32_t)((int32_t)L_9-(int32_t)1)), /*hidden argument*/NULL);
		if ((((int32_t)L_10) == ((int32_t)((int32_t)47))))
		{
			goto IL_0066;
		}
	}
	{
		int32_t L_11 = V_0;
		if ((((int32_t)L_11) <= ((int32_t)1)))
		{
			goto IL_0059;
		}
	}
	{
		String_t* L_12 = ___basePath0;
		NullCheck(L_12);
		int32_t L_13 = String_LastIndexOf_m3451222878(L_12, ((int32_t)47), /*hidden argument*/NULL);
		V_1 = L_13;
		int32_t L_14 = V_1;
		if ((((int32_t)L_14) < ((int32_t)0)))
		{
			goto IL_0054;
		}
	}
	{
		String_t* L_15 = ___basePath0;
		int32_t L_16 = V_1;
		NullCheck(L_15);
		String_t* L_17 = String_Substring_m1610150815(L_15, 0, ((int32_t)((int32_t)L_16+(int32_t)1)), /*hidden argument*/NULL);
		___basePath0 = L_17;
	}

IL_0054:
	{
		goto IL_0066;
	}

IL_0059:
	{
		String_t* L_18 = ___basePath0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m3937257545(NULL /*static, unused*/, L_18, _stringLiteral3452614529, /*hidden argument*/NULL);
		___basePath0 = L_19;
	}

IL_0066:
	{
		String_t* L_20 = ___basePath0;
		String_t* L_21 = ___relativePath1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m3937257545(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_23 = VirtualPathUtility_Normalize_m1039358837(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		return L_23;
	}
}
// System.String System.Web.VirtualPathUtility::GetDirectory(System.String)
extern "C"  String_t* VirtualPathUtility_GetDirectory_m2076898960 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPathUtility_GetDirectory_m2076898960_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___virtualPath0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_1 = VirtualPathUtility_GetDirectory_m404138870(NULL /*static, unused*/, L_0, (bool)1, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String System.Web.VirtualPathUtility::GetDirectory(System.String,System.Boolean)
extern "C"  String_t* VirtualPathUtility_GetDirectory_m404138870 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, bool ___normalize1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPathUtility_GetDirectory_m404138870_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		bool L_0 = ___normalize1;
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		String_t* L_1 = ___virtualPath0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_2 = VirtualPathUtility_Normalize_m1039358837(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		___virtualPath0 = L_2;
	}

IL_000e:
	{
		String_t* L_3 = ___virtualPath0;
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m3847582255(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = ___virtualPath0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		bool L_6 = VirtualPathUtility_IsAppRelative_m548692695(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) >= ((int32_t)3)))
		{
			goto IL_0036;
		}
	}
	{
		String_t* L_8 = ___virtualPath0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_9 = VirtualPathUtility_ToAbsolute_m373284300(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		___virtualPath0 = L_9;
		String_t* L_10 = ___virtualPath0;
		NullCheck(L_10);
		int32_t L_11 = String_get_Length_m3847582255(L_10, /*hidden argument*/NULL);
		V_0 = L_11;
	}

IL_0036:
	{
		int32_t L_12 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)1))))
		{
			goto IL_004d;
		}
	}
	{
		String_t* L_13 = ___virtualPath0;
		NullCheck(L_13);
		Il2CppChar L_14 = String_get_Chars_m2986988803(L_13, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_004d;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_004d:
	{
		String_t* L_15 = ___virtualPath0;
		int32_t L_16 = V_0;
		int32_t L_17 = V_0;
		NullCheck(L_15);
		int32_t L_18 = String_LastIndexOf_m3228770703(L_15, ((int32_t)47), ((int32_t)((int32_t)L_16-(int32_t)2)), ((int32_t)((int32_t)L_17-(int32_t)2)), /*hidden argument*/NULL);
		V_1 = L_18;
		int32_t L_19 = V_1;
		if ((((int32_t)L_19) <= ((int32_t)0)))
		{
			goto IL_006e;
		}
	}
	{
		String_t* L_20 = ___virtualPath0;
		int32_t L_21 = V_1;
		NullCheck(L_20);
		String_t* L_22 = String_Substring_m1610150815(L_20, 0, ((int32_t)((int32_t)L_21+(int32_t)1)), /*hidden argument*/NULL);
		return L_22;
	}

IL_006e:
	{
		return _stringLiteral3452614529;
	}
}
// System.String System.Web.VirtualPathUtility::GetExtension(System.String)
extern "C"  String_t* VirtualPathUtility_GetExtension_m3822181684 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPathUtility_GetExtension_m3822181684_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___virtualPath0;
		bool L_1 = StrUtils_IsNullOrEmpty_m2227577989(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_2 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_2, _stringLiteral1685589679, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		String_t* L_3 = ___virtualPath0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_4 = VirtualPathUtility_Canonize_m2348126131(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		___virtualPath0 = L_4;
		String_t* L_5 = ___virtualPath0;
		NullCheck(L_5);
		int32_t L_6 = String_LastIndexOf_m3451222878(L_5, ((int32_t)46), /*hidden argument*/NULL);
		V_0 = L_6;
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) == ((int32_t)(-1))))
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_8 = V_0;
		String_t* L_9 = ___virtualPath0;
		NullCheck(L_9);
		int32_t L_10 = String_get_Length_m3847582255(L_9, /*hidden argument*/NULL);
		if ((((int32_t)L_8) == ((int32_t)((int32_t)((int32_t)L_10-(int32_t)1)))))
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_11 = V_0;
		String_t* L_12 = ___virtualPath0;
		NullCheck(L_12);
		int32_t L_13 = String_LastIndexOf_m3451222878(L_12, ((int32_t)47), /*hidden argument*/NULL);
		if ((((int32_t)L_11) >= ((int32_t)L_13)))
		{
			goto IL_0050;
		}
	}

IL_004a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_14;
	}

IL_0050:
	{
		String_t* L_15 = ___virtualPath0;
		int32_t L_16 = V_0;
		NullCheck(L_15);
		String_t* L_17 = String_Substring_m2848979100(L_15, L_16, /*hidden argument*/NULL);
		return L_17;
	}
}
// System.String System.Web.VirtualPathUtility::GetFileName(System.String)
extern "C"  String_t* VirtualPathUtility_GetFileName_m60487828 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPathUtility_GetFileName_m60487828_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___virtualPath0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_1 = VirtualPathUtility_Normalize_m1039358837(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___virtualPath0 = L_1;
		String_t* L_2 = ___virtualPath0;
		bool L_3 = VirtualPathUtility_IsAppRelative_m548692695(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_4 = ___virtualPath0;
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m3847582255(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_5) >= ((int32_t)3)))
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_6 = ___virtualPath0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_7 = VirtualPathUtility_ToAbsolute_m373284300(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		___virtualPath0 = L_7;
	}

IL_0027:
	{
		String_t* L_8 = ___virtualPath0;
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_m3847582255(L_8, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)1))))
		{
			goto IL_0047;
		}
	}
	{
		String_t* L_10 = ___virtualPath0;
		NullCheck(L_10);
		Il2CppChar L_11 = String_get_Chars_m2986988803(L_10, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_11) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_0047;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_12;
	}

IL_0047:
	{
		String_t* L_13 = ___virtualPath0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_14 = VirtualPathUtility_RemoveTrailingSlash_m2660928406(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		___virtualPath0 = L_14;
		String_t* L_15 = ___virtualPath0;
		NullCheck(L_15);
		int32_t L_16 = String_LastIndexOf_m3451222878(L_15, ((int32_t)47), /*hidden argument*/NULL);
		V_0 = L_16;
		String_t* L_17 = ___virtualPath0;
		int32_t L_18 = V_0;
		NullCheck(L_17);
		String_t* L_19 = String_Substring_m2848979100(L_17, ((int32_t)((int32_t)L_18+(int32_t)1)), /*hidden argument*/NULL);
		return L_19;
	}
}
// System.Boolean System.Web.VirtualPathUtility::IsRooted(System.String)
extern "C"  bool VirtualPathUtility_IsRooted_m3278026923 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPathUtility_IsRooted_m3278026923_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___virtualPath0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		bool L_1 = VirtualPathUtility_IsAbsolute_m2473455663(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		String_t* L_2 = ___virtualPath0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		bool L_3 = VirtualPathUtility_IsAppRelative_m548692695(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0014;
	}

IL_0013:
	{
		G_B3_0 = 1;
	}

IL_0014:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean System.Web.VirtualPathUtility::IsAbsolute(System.String)
extern "C"  bool VirtualPathUtility_IsAbsolute_m2473455663 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPathUtility_IsAbsolute_m2473455663_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B5_0 = 0;
	{
		String_t* L_0 = ___virtualPath0;
		bool L_1 = StrUtils_IsNullOrEmpty_m2227577989(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_2 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_2, _stringLiteral1685589679, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		String_t* L_3 = ___virtualPath0;
		NullCheck(L_3);
		Il2CppChar L_4 = String_get_Chars_m2986988803(L_3, 0, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)((int32_t)47))))
		{
			goto IL_0031;
		}
	}
	{
		String_t* L_5 = ___virtualPath0;
		NullCheck(L_5);
		Il2CppChar L_6 = String_get_Chars_m2986988803(L_5, 0, /*hidden argument*/NULL);
		G_B5_0 = ((((int32_t)L_6) == ((int32_t)((int32_t)92)))? 1 : 0);
		goto IL_0032;
	}

IL_0031:
	{
		G_B5_0 = 1;
	}

IL_0032:
	{
		return (bool)G_B5_0;
	}
}
// System.Boolean System.Web.VirtualPathUtility::IsAppRelative(System.String)
extern "C"  bool VirtualPathUtility_IsAppRelative_m548692695 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPathUtility_IsAppRelative_m548692695_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___virtualPath0;
		bool L_1 = StrUtils_IsNullOrEmpty_m2227577989(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_2 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_2, _stringLiteral1685589679, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		String_t* L_3 = ___virtualPath0;
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m3847582255(L_3, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0032;
		}
	}
	{
		String_t* L_5 = ___virtualPath0;
		NullCheck(L_5);
		Il2CppChar L_6 = String_get_Chars_m2986988803(L_5, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)126)))))
		{
			goto IL_0032;
		}
	}
	{
		return (bool)1;
	}

IL_0032:
	{
		String_t* L_7 = ___virtualPath0;
		NullCheck(L_7);
		Il2CppChar L_8 = String_get_Chars_m2986988803(L_7, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)126)))))
		{
			goto IL_005e;
		}
	}
	{
		String_t* L_9 = ___virtualPath0;
		NullCheck(L_9);
		Il2CppChar L_10 = String_get_Chars_m2986988803(L_9, 1, /*hidden argument*/NULL);
		if ((((int32_t)L_10) == ((int32_t)((int32_t)47))))
		{
			goto IL_005c;
		}
	}
	{
		String_t* L_11 = ___virtualPath0;
		NullCheck(L_11);
		Il2CppChar L_12 = String_get_Chars_m2986988803(L_11, 1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_12) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_005e;
		}
	}

IL_005c:
	{
		return (bool)1;
	}

IL_005e:
	{
		return (bool)0;
	}
}
// System.String System.Web.VirtualPathUtility::MakeRelative(System.String,System.String)
extern "C"  String_t* VirtualPathUtility_MakeRelative_m344618986 (Il2CppObject * __this /* static, unused */, String_t* ___fromPath0, String_t* ___toPath1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPathUtility_MakeRelative_m344618986_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1281789340* V_0 = NULL;
	StringU5BU5D_t1281789340* V_1 = NULL;
	int32_t V_2 = 0;
	StringBuilder_t1712802186 * V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		String_t* L_0 = ___fromPath0;
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		String_t* L_1 = ___toPath1;
		if (L_1)
		{
			goto IL_0012;
		}
	}

IL_000c:
	{
		NullReferenceException_t1023182353 * L_2 = (NullReferenceException_t1023182353 *)il2cpp_codegen_object_new(NullReferenceException_t1023182353_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m744513393(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		String_t* L_3 = ___toPath1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_5 = String_op_Equality_m920492651(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0024;
		}
	}
	{
		String_t* L_6 = ___toPath1;
		return L_6;
	}

IL_0024:
	{
		String_t* L_7 = ___toPath1;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_8 = VirtualPathUtility_ToAbsoluteInternal_m1993485465(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		___toPath1 = L_8;
		String_t* L_9 = ___fromPath0;
		String_t* L_10 = VirtualPathUtility_ToAbsoluteInternal_m1993485465(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		___fromPath0 = L_10;
		String_t* L_11 = ___fromPath0;
		String_t* L_12 = ___toPath1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_13 = String_CompareOrdinal_m786132908(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_005b;
		}
	}
	{
		String_t* L_14 = ___fromPath0;
		String_t* L_15 = ___fromPath0;
		NullCheck(L_15);
		int32_t L_16 = String_get_Length_m3847582255(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		Il2CppChar L_17 = String_get_Chars_m2986988803(L_14, ((int32_t)((int32_t)L_16-(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_17) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_005b;
		}
	}
	{
		return _stringLiteral3450582914;
	}

IL_005b:
	{
		String_t* L_18 = ___toPath1;
		CharU5BU5D_t3528271667* L_19 = ((CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_19);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)47));
		NullCheck(L_18);
		StringU5BU5D_t1281789340* L_20 = String_Split_m3646115398(L_18, L_19, /*hidden argument*/NULL);
		V_0 = L_20;
		String_t* L_21 = ___fromPath0;
		CharU5BU5D_t3528271667* L_22 = ((CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_22);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)47));
		NullCheck(L_21);
		StringU5BU5D_t1281789340* L_23 = String_Split_m3646115398(L_21, L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		V_2 = 1;
		goto IL_00a5;
	}

IL_0086:
	{
		StringU5BU5D_t1281789340* L_24 = V_0;
		NullCheck(L_24);
		int32_t L_25 = V_2;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_24)->max_length))))) == ((int32_t)((int32_t)((int32_t)L_25+(int32_t)1)))))
		{
			goto IL_009c;
		}
	}
	{
		StringU5BU5D_t1281789340* L_26 = V_1;
		NullCheck(L_26);
		int32_t L_27 = V_2;
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_26)->max_length))))) == ((uint32_t)((int32_t)((int32_t)L_27+(int32_t)1))))))
		{
			goto IL_00a1;
		}
	}

IL_009c:
	{
		goto IL_00b5;
	}

IL_00a1:
	{
		int32_t L_28 = V_2;
		V_2 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_00a5:
	{
		StringU5BU5D_t1281789340* L_29 = V_0;
		int32_t L_30 = V_2;
		NullCheck(L_29);
		int32_t L_31 = L_30;
		String_t* L_32 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		StringU5BU5D_t1281789340* L_33 = V_1;
		int32_t L_34 = V_2;
		NullCheck(L_33);
		int32_t L_35 = L_34;
		String_t* L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_37 = String_op_Equality_m920492651(NULL /*static, unused*/, L_32, L_36, /*hidden argument*/NULL);
		if (L_37)
		{
			goto IL_0086;
		}
	}

IL_00b5:
	{
		StringBuilder_t1712802186 * L_38 = (StringBuilder_t1712802186 *)il2cpp_codegen_object_new(StringBuilder_t1712802186_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3121283359(L_38, /*hidden argument*/NULL);
		V_3 = L_38;
		V_4 = 1;
		goto IL_00d5;
	}

IL_00c3:
	{
		StringBuilder_t1712802186 * L_39 = V_3;
		NullCheck(L_39);
		StringBuilder_Append_m1965104174(L_39, _stringLiteral1057303601, /*hidden argument*/NULL);
		int32_t L_40 = V_4;
		V_4 = ((int32_t)((int32_t)L_40+(int32_t)1));
	}

IL_00d5:
	{
		int32_t L_41 = V_4;
		StringU5BU5D_t1281789340* L_42 = V_1;
		NullCheck(L_42);
		int32_t L_43 = V_2;
		if ((((int32_t)L_41) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_42)->max_length))))-(int32_t)L_43)))))
		{
			goto IL_00c3;
		}
	}
	{
		int32_t L_44 = V_2;
		V_5 = L_44;
		goto IL_010f;
	}

IL_00e9:
	{
		StringBuilder_t1712802186 * L_45 = V_3;
		StringU5BU5D_t1281789340* L_46 = V_0;
		int32_t L_47 = V_5;
		NullCheck(L_46);
		int32_t L_48 = L_47;
		String_t* L_49 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		NullCheck(L_45);
		StringBuilder_Append_m1965104174(L_45, L_49, /*hidden argument*/NULL);
		int32_t L_50 = V_5;
		StringU5BU5D_t1281789340* L_51 = V_0;
		NullCheck(L_51);
		if ((((int32_t)L_50) >= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_51)->max_length))))-(int32_t)1)))))
		{
			goto IL_0109;
		}
	}
	{
		StringBuilder_t1712802186 * L_52 = V_3;
		NullCheck(L_52);
		StringBuilder_Append_m2383614642(L_52, ((int32_t)47), /*hidden argument*/NULL);
	}

IL_0109:
	{
		int32_t L_53 = V_5;
		V_5 = ((int32_t)((int32_t)L_53+(int32_t)1));
	}

IL_010f:
	{
		int32_t L_54 = V_5;
		StringU5BU5D_t1281789340* L_55 = V_0;
		NullCheck(L_55);
		if ((((int32_t)L_54) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_55)->max_length)))))))
		{
			goto IL_00e9;
		}
	}
	{
		StringBuilder_t1712802186 * L_56 = V_3;
		NullCheck(L_56);
		String_t* L_57 = StringBuilder_ToString_m3317489284(L_56, /*hidden argument*/NULL);
		return L_57;
	}
}
// System.String System.Web.VirtualPathUtility::ToAbsoluteInternal(System.String)
extern "C"  String_t* VirtualPathUtility_ToAbsoluteInternal_m1993485465 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPathUtility_ToAbsoluteInternal_m1993485465_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___virtualPath0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		bool L_1 = VirtualPathUtility_IsAppRelative_m548692695(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		String_t* L_2 = ___virtualPath0;
		IL2CPP_RUNTIME_CLASS_INIT(HttpRuntime_t1480753741_il2cpp_TypeInfo_var);
		String_t* L_3 = HttpRuntime_get_AppDomainAppVirtualPath_m531373613(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_4 = VirtualPathUtility_ToAbsolute_m409990745(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0017:
	{
		String_t* L_5 = ___virtualPath0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		bool L_6 = VirtualPathUtility_IsAbsolute_m2473455663(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0029;
		}
	}
	{
		String_t* L_7 = ___virtualPath0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_8 = VirtualPathUtility_Normalize_m1039358837(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0029:
	{
		ArgumentOutOfRangeException_t777629997 * L_9 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_9, _stringLiteral1047870583, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}
}
// System.String System.Web.VirtualPathUtility::RemoveTrailingSlash(System.String)
extern "C"  String_t* VirtualPathUtility_RemoveTrailingSlash_m2660928406 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPathUtility_RemoveTrailingSlash_m2660928406_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___virtualPath0;
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		String_t* L_1 = ___virtualPath0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_3 = String_op_Equality_m920492651(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0018;
		}
	}

IL_0016:
	{
		return (String_t*)NULL;
	}

IL_0018:
	{
		String_t* L_4 = ___virtualPath0;
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m3847582255(L_4, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_5-(int32_t)1));
		int32_t L_6 = V_0;
		if (!L_6)
		{
			goto IL_0035;
		}
	}
	{
		String_t* L_7 = ___virtualPath0;
		int32_t L_8 = V_0;
		NullCheck(L_7);
		Il2CppChar L_9 = String_get_Chars_m2986988803(L_7, L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_9) == ((int32_t)((int32_t)47))))
		{
			goto IL_0037;
		}
	}

IL_0035:
	{
		String_t* L_10 = ___virtualPath0;
		return L_10;
	}

IL_0037:
	{
		String_t* L_11 = ___virtualPath0;
		int32_t L_12 = V_0;
		NullCheck(L_11);
		String_t* L_13 = String_Substring_m1610150815(L_11, 0, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.String System.Web.VirtualPathUtility::ToAbsolute(System.String)
extern "C"  String_t* VirtualPathUtility_ToAbsolute_m373284300 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPathUtility_ToAbsolute_m373284300_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___virtualPath0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_1 = VirtualPathUtility_ToAbsolute_m3451502579(NULL /*static, unused*/, L_0, (bool)1, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String System.Web.VirtualPathUtility::ToAbsolute(System.String,System.Boolean)
extern "C"  String_t* VirtualPathUtility_ToAbsolute_m3451502579 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, bool ___normalize1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPathUtility_ToAbsolute_m3451502579_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___virtualPath0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		bool L_1 = VirtualPathUtility_IsAbsolute_m2473455663(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		bool L_2 = ___normalize1;
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_3 = ___virtualPath0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_4 = VirtualPathUtility_Normalize_m1039358837(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0018:
	{
		String_t* L_5 = ___virtualPath0;
		return L_5;
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HttpRuntime_t1480753741_il2cpp_TypeInfo_var);
		String_t* L_6 = HttpRuntime_get_AppDomainAppVirtualPath_m531373613(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_6;
		String_t* L_7 = V_0;
		if (L_7)
		{
			goto IL_0031;
		}
	}
	{
		HttpException_t2907797370 * L_8 = (HttpException_t2907797370 *)il2cpp_codegen_object_new(HttpException_t2907797370_il2cpp_TypeInfo_var);
		HttpException__ctor_m2549443145(L_8, _stringLiteral3722472157, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0031:
	{
		String_t* L_9 = ___virtualPath0;
		NullCheck(L_9);
		int32_t L_10 = String_get_Length_m3847582255(L_9, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_10) == ((uint32_t)1))))
		{
			goto IL_004d;
		}
	}
	{
		String_t* L_11 = ___virtualPath0;
		NullCheck(L_11);
		Il2CppChar L_12 = String_get_Chars_m2986988803(L_11, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_12) == ((uint32_t)((int32_t)126)))))
		{
			goto IL_004d;
		}
	}
	{
		String_t* L_13 = V_0;
		return L_13;
	}

IL_004d:
	{
		String_t* L_14 = ___virtualPath0;
		String_t* L_15 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_16 = VirtualPathUtility_ToAbsolute_m409990745(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.String System.Web.VirtualPathUtility::ToAbsolute(System.String,System.String)
extern "C"  String_t* VirtualPathUtility_ToAbsolute_m409990745 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, String_t* ___applicationPath1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPathUtility_ToAbsolute_m409990745_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___virtualPath0;
		String_t* L_1 = ___applicationPath1;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_2 = VirtualPathUtility_ToAbsolute_m1728694453(NULL /*static, unused*/, L_0, L_1, (bool)1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String System.Web.VirtualPathUtility::ToAbsolute(System.String,System.String,System.Boolean)
extern "C"  String_t* VirtualPathUtility_ToAbsolute_m1728694453 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, String_t* ___applicationPath1, bool ___normalize2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPathUtility_ToAbsolute_m1728694453_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B9_0 = NULL;
	String_t* G_B8_0 = NULL;
	String_t* G_B10_0 = NULL;
	String_t* G_B10_1 = NULL;
	{
		String_t* L_0 = ___applicationPath1;
		bool L_1 = StrUtils_IsNullOrEmpty_m2227577989(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_2 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_2, _stringLiteral2251586184, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		String_t* L_3 = ___virtualPath0;
		bool L_4 = StrUtils_IsNullOrEmpty_m2227577989(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002c;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_5 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_5, _stringLiteral1685589679, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_002c:
	{
		String_t* L_6 = ___virtualPath0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		bool L_7 = VirtualPathUtility_IsAppRelative_m548692695(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0088;
		}
	}
	{
		String_t* L_8 = ___applicationPath1;
		NullCheck(L_8);
		Il2CppChar L_9 = String_get_Chars_m2986988803(L_8, 0, /*hidden argument*/NULL);
		if ((((int32_t)L_9) == ((int32_t)((int32_t)47))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t132251570 * L_10 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1216717135(L_10, _stringLiteral3683762703, _stringLiteral2251586184, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_0055:
	{
		String_t* L_11 = ___applicationPath1;
		String_t* L_12 = ___virtualPath0;
		NullCheck(L_12);
		int32_t L_13 = String_get_Length_m3847582255(L_12, /*hidden argument*/NULL);
		G_B8_0 = L_11;
		if ((!(((uint32_t)L_13) == ((uint32_t)1))))
		{
			G_B9_0 = L_11;
			goto IL_006c;
		}
	}
	{
		G_B10_0 = _stringLiteral3452614529;
		G_B10_1 = G_B8_0;
		goto IL_0073;
	}

IL_006c:
	{
		String_t* L_14 = ___virtualPath0;
		NullCheck(L_14);
		String_t* L_15 = String_Substring_m2848979100(L_14, 1, /*hidden argument*/NULL);
		G_B10_0 = L_15;
		G_B10_1 = G_B9_0;
	}

IL_0073:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3937257545(NULL /*static, unused*/, G_B10_1, G_B10_0, /*hidden argument*/NULL);
		V_0 = L_16;
		bool L_17 = ___normalize2;
		if (!L_17)
		{
			goto IL_0086;
		}
	}
	{
		String_t* L_18 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_19 = VirtualPathUtility_Normalize_m1039358837(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		return L_19;
	}

IL_0086:
	{
		String_t* L_20 = V_0;
		return L_20;
	}

IL_0088:
	{
		String_t* L_21 = ___virtualPath0;
		NullCheck(L_21);
		Il2CppChar L_22 = String_get_Chars_m2986988803(L_21, 0, /*hidden argument*/NULL);
		if ((((int32_t)L_22) == ((int32_t)((int32_t)47))))
		{
			goto IL_00a7;
		}
	}
	{
		String_t* L_23 = ___virtualPath0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral893733960, L_23, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_25 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_25, L_24, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_25);
	}

IL_00a7:
	{
		bool L_26 = ___normalize2;
		if (!L_26)
		{
			goto IL_00b4;
		}
	}
	{
		String_t* L_27 = ___virtualPath0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_28 = VirtualPathUtility_Normalize_m1039358837(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		return L_28;
	}

IL_00b4:
	{
		String_t* L_29 = ___virtualPath0;
		return L_29;
	}
}
// System.String System.Web.VirtualPathUtility::ToAppRelative(System.String)
extern "C"  String_t* VirtualPathUtility_ToAppRelative_m769660906 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPathUtility_ToAppRelative_m769660906_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(HttpRuntime_t1480753741_il2cpp_TypeInfo_var);
		String_t* L_0 = HttpRuntime_get_AppDomainAppVirtualPath_m531373613(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = V_0;
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		HttpException_t2907797370 * L_2 = (HttpException_t2907797370 *)il2cpp_codegen_object_new(HttpException_t2907797370_il2cpp_TypeInfo_var);
		HttpException__ctor_m2549443145(L_2, _stringLiteral3722472157, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		String_t* L_3 = ___virtualPath0;
		String_t* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_5 = VirtualPathUtility_ToAppRelative_m867676899(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.String System.Web.VirtualPathUtility::ToAppRelative(System.String,System.String)
extern "C"  String_t* VirtualPathUtility_ToAppRelative_m867676899 (Il2CppObject * __this /* static, unused */, String_t* ___virtualPath0, String_t* ___applicationPath1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPathUtility_ToAppRelative_m867676899_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___virtualPath0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_1 = VirtualPathUtility_Normalize_m1039358837(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___virtualPath0 = L_1;
		String_t* L_2 = ___virtualPath0;
		bool L_3 = VirtualPathUtility_IsAppRelative_m548692695(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0015;
		}
	}
	{
		String_t* L_4 = ___virtualPath0;
		return L_4;
	}

IL_0015:
	{
		String_t* L_5 = ___applicationPath1;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		bool L_6 = VirtualPathUtility_IsAbsolute_m2473455663(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0030;
		}
	}
	{
		ArgumentException_t132251570 * L_7 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1216717135(L_7, _stringLiteral1064770897, _stringLiteral2251586184, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0030:
	{
		String_t* L_8 = ___applicationPath1;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_9 = VirtualPathUtility_Normalize_m1039358837(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		___applicationPath1 = L_9;
		String_t* L_10 = ___applicationPath1;
		NullCheck(L_10);
		int32_t L_11 = String_get_Length_m3847582255(L_10, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_11) == ((uint32_t)1))))
		{
			goto IL_0050;
		}
	}
	{
		String_t* L_12 = ___virtualPath0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3452614610, L_12, /*hidden argument*/NULL);
		return L_13;
	}

IL_0050:
	{
		String_t* L_14 = ___applicationPath1;
		NullCheck(L_14);
		int32_t L_15 = String_get_Length_m3847582255(L_14, /*hidden argument*/NULL);
		V_0 = L_15;
		String_t* L_16 = ___virtualPath0;
		String_t* L_17 = ___applicationPath1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_18 = String_CompareOrdinal_m786132908(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_0069;
		}
	}
	{
		return _stringLiteral3450582994;
	}

IL_0069:
	{
		String_t* L_19 = ___virtualPath0;
		String_t* L_20 = ___applicationPath1;
		int32_t L_21 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_22 = String_CompareOrdinal_m1012192092(NULL /*static, unused*/, L_19, 0, L_20, 0, L_21, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_008a;
		}
	}
	{
		String_t* L_23 = ___virtualPath0;
		int32_t L_24 = V_0;
		NullCheck(L_23);
		String_t* L_25 = String_Substring_m2848979100(L_23, L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3452614610, L_25, /*hidden argument*/NULL);
		return L_26;
	}

IL_008a:
	{
		String_t* L_27 = ___virtualPath0;
		return L_27;
	}
}
// System.String System.Web.VirtualPathUtility::Normalize(System.String)
extern "C"  String_t* VirtualPathUtility_Normalize_m1039358837 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPathUtility_Normalize_m1039358837_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Il2CppChar V_1 = 0x0;
	bool V_2 = false;
	bool V_3 = false;
	StringU5BU5D_t1281789340* V_4 = NULL;
	StringU5BU5D_t1281789340* V_5 = NULL;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	String_t* V_9 = NULL;
	String_t* V_10 = NULL;
	StringBuilder_t1712802186 * V_11 = NULL;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	{
		String_t* L_0 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		bool L_1 = VirtualPathUtility_IsRooted_m3278026923(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_2 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral3632685578, L_2, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_4 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_4, L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_001c:
	{
		String_t* L_5 = ___path0;
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m3847582255(L_5, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_002a;
		}
	}
	{
		String_t* L_7 = ___path0;
		return L_7;
	}

IL_002a:
	{
		String_t* L_8 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		String_t* L_9 = VirtualPathUtility_Canonize_m2348126131(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		___path0 = L_9;
		String_t* L_10 = ___path0;
		NullCheck(L_10);
		int32_t L_11 = String_IndexOf_m363431711(L_10, ((int32_t)46), /*hidden argument*/NULL);
		V_0 = L_11;
		goto IL_007c;
	}

IL_0040:
	{
		int32_t L_12 = V_0;
		int32_t L_13 = ((int32_t)((int32_t)L_12+(int32_t)1));
		V_0 = L_13;
		String_t* L_14 = ___path0;
		NullCheck(L_14);
		int32_t L_15 = String_get_Length_m3847582255(L_14, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_13) == ((uint32_t)L_15))))
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0083;
	}

IL_0055:
	{
		String_t* L_16 = ___path0;
		int32_t L_17 = V_0;
		NullCheck(L_16);
		Il2CppChar L_18 = String_get_Chars_m2986988803(L_16, L_17, /*hidden argument*/NULL);
		V_1 = L_18;
		Il2CppChar L_19 = V_1;
		if ((((int32_t)L_19) == ((int32_t)((int32_t)47))))
		{
			goto IL_006d;
		}
	}
	{
		Il2CppChar L_20 = V_1;
		if ((!(((uint32_t)L_20) == ((uint32_t)((int32_t)46)))))
		{
			goto IL_0072;
		}
	}

IL_006d:
	{
		goto IL_0083;
	}

IL_0072:
	{
		String_t* L_21 = ___path0;
		int32_t L_22 = V_0;
		NullCheck(L_21);
		int32_t L_23 = String_IndexOf_m2466398549(L_21, ((int32_t)46), L_22, /*hidden argument*/NULL);
		V_0 = L_23;
	}

IL_007c:
	{
		int32_t L_24 = V_0;
		if ((((int32_t)L_24) >= ((int32_t)0)))
		{
			goto IL_0040;
		}
	}

IL_0083:
	{
		int32_t L_25 = V_0;
		if ((((int32_t)L_25) >= ((int32_t)0)))
		{
			goto IL_008c;
		}
	}
	{
		String_t* L_26 = ___path0;
		return L_26;
	}

IL_008c:
	{
		V_2 = (bool)0;
		V_3 = (bool)0;
		V_4 = (StringU5BU5D_t1281789340*)NULL;
		String_t* L_27 = ___path0;
		NullCheck(L_27);
		Il2CppChar L_28 = String_get_Chars_m2986988803(L_27, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_28) == ((uint32_t)((int32_t)126)))))
		{
			goto IL_00c3;
		}
	}
	{
		String_t* L_29 = ___path0;
		NullCheck(L_29);
		int32_t L_30 = String_get_Length_m3847582255(L_29, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_30) == ((uint32_t)2))))
		{
			goto IL_00b3;
		}
	}
	{
		return _stringLiteral3450582994;
	}

IL_00b3:
	{
		V_2 = (bool)1;
		String_t* L_31 = ___path0;
		NullCheck(L_31);
		String_t* L_32 = String_Substring_m2848979100(L_31, 1, /*hidden argument*/NULL);
		___path0 = L_32;
		goto IL_00d5;
	}

IL_00c3:
	{
		String_t* L_33 = ___path0;
		NullCheck(L_33);
		int32_t L_34 = String_get_Length_m3847582255(L_33, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_34) == ((uint32_t)1))))
		{
			goto IL_00d5;
		}
	}
	{
		return _stringLiteral3452614529;
	}

IL_00d5:
	{
		String_t* L_35 = ___path0;
		String_t* L_36 = ___path0;
		NullCheck(L_36);
		int32_t L_37 = String_get_Length_m3847582255(L_36, /*hidden argument*/NULL);
		NullCheck(L_35);
		Il2CppChar L_38 = String_get_Chars_m2986988803(L_35, ((int32_t)((int32_t)L_37-(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_38) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_00ec;
		}
	}
	{
		V_3 = (bool)1;
	}

IL_00ec:
	{
		String_t* L_39 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		CharU5BU5D_t3528271667* L_40 = ((VirtualPathUtility_t3131300229_StaticFields*)VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var->static_fields)->get_path_sep_2();
		StringU5BU5D_t1281789340* L_41 = StrUtils_SplitRemoveEmptyEntries_m3488785249(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		V_5 = L_41;
		StringU5BU5D_t1281789340* L_42 = V_5;
		NullCheck(L_42);
		V_6 = (((int32_t)((int32_t)(((Il2CppArray *)L_42)->max_length))));
		V_7 = 0;
		V_8 = 0;
		goto IL_01b6;
	}

IL_010a:
	{
		StringU5BU5D_t1281789340* L_43 = V_5;
		int32_t L_44 = V_8;
		NullCheck(L_43);
		int32_t L_45 = L_44;
		String_t* L_46 = (L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_45));
		V_9 = L_46;
		String_t* L_47 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_48 = String_op_Equality_m920492651(NULL /*static, unused*/, L_47, _stringLiteral3452614530, /*hidden argument*/NULL);
		if (!L_48)
		{
			goto IL_0127;
		}
	}
	{
		goto IL_01b0;
	}

IL_0127:
	{
		String_t* L_49 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_50 = String_op_Equality_m920492651(NULL /*static, unused*/, L_49, _stringLiteral3450648450, /*hidden argument*/NULL);
		if (!L_50)
		{
			goto IL_018a;
		}
	}
	{
		int32_t L_51 = V_7;
		V_7 = ((int32_t)((int32_t)L_51-(int32_t)1));
		int32_t L_52 = V_7;
		if ((((int32_t)L_52) < ((int32_t)0)))
		{
			goto IL_014b;
		}
	}
	{
		goto IL_01b0;
	}

IL_014b:
	{
		bool L_53 = V_2;
		if (!L_53)
		{
			goto IL_017f;
		}
	}
	{
		StringU5BU5D_t1281789340* L_54 = V_4;
		if (L_54)
		{
			goto IL_016d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HttpRuntime_t1480753741_il2cpp_TypeInfo_var);
		String_t* L_55 = HttpRuntime_get_AppDomainAppVirtualPath_m531373613(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_10 = L_55;
		String_t* L_56 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		CharU5BU5D_t3528271667* L_57 = ((VirtualPathUtility_t3131300229_StaticFields*)VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var->static_fields)->get_path_sep_2();
		StringU5BU5D_t1281789340* L_58 = StrUtils_SplitRemoveEmptyEntries_m3488785249(NULL /*static, unused*/, L_56, L_57, /*hidden argument*/NULL);
		V_4 = L_58;
	}

IL_016d:
	{
		StringU5BU5D_t1281789340* L_59 = V_4;
		NullCheck(L_59);
		int32_t L_60 = V_7;
		if ((((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_59)->max_length))))+(int32_t)L_60))) < ((int32_t)0)))
		{
			goto IL_017f;
		}
	}
	{
		goto IL_01b0;
	}

IL_017f:
	{
		HttpException_t2907797370 * L_61 = (HttpException_t2907797370 *)il2cpp_codegen_object_new(HttpException_t2907797370_il2cpp_TypeInfo_var);
		HttpException__ctor_m2549443145(L_61, _stringLiteral3347900047, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_61);
	}

IL_018a:
	{
		int32_t L_62 = V_7;
		if ((((int32_t)L_62) < ((int32_t)0)))
		{
			goto IL_019e;
		}
	}
	{
		StringU5BU5D_t1281789340* L_63 = V_5;
		int32_t L_64 = V_7;
		String_t* L_65 = V_9;
		NullCheck(L_63);
		ArrayElementTypeCheck (L_63, L_65);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(L_64), (String_t*)L_65);
		goto IL_01aa;
	}

IL_019e:
	{
		StringU5BU5D_t1281789340* L_66 = V_4;
		StringU5BU5D_t1281789340* L_67 = V_4;
		NullCheck(L_67);
		int32_t L_68 = V_7;
		String_t* L_69 = V_9;
		NullCheck(L_66);
		ArrayElementTypeCheck (L_66, L_69);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_67)->max_length))))+(int32_t)L_68))), (String_t*)L_69);
	}

IL_01aa:
	{
		int32_t L_70 = V_7;
		V_7 = ((int32_t)((int32_t)L_70+(int32_t)1));
	}

IL_01b0:
	{
		int32_t L_71 = V_8;
		V_8 = ((int32_t)((int32_t)L_71+(int32_t)1));
	}

IL_01b6:
	{
		int32_t L_72 = V_8;
		int32_t L_73 = V_6;
		if ((((int32_t)L_72) < ((int32_t)L_73)))
		{
			goto IL_010a;
		}
	}
	{
		StringBuilder_t1712802186 * L_74 = (StringBuilder_t1712802186 *)il2cpp_codegen_object_new(StringBuilder_t1712802186_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3121283359(L_74, /*hidden argument*/NULL);
		V_11 = L_74;
		StringU5BU5D_t1281789340* L_75 = V_4;
		if (!L_75)
		{
			goto IL_0217;
		}
	}
	{
		V_2 = (bool)0;
		StringU5BU5D_t1281789340* L_76 = V_4;
		NullCheck(L_76);
		V_12 = (((int32_t)((int32_t)(((Il2CppArray *)L_76)->max_length))));
		int32_t L_77 = V_7;
		if ((((int32_t)L_77) >= ((int32_t)0)))
		{
			goto IL_01e4;
		}
	}
	{
		int32_t L_78 = V_12;
		int32_t L_79 = V_7;
		V_12 = ((int32_t)((int32_t)L_78+(int32_t)L_79));
	}

IL_01e4:
	{
		V_13 = 0;
		goto IL_0209;
	}

IL_01ec:
	{
		StringBuilder_t1712802186 * L_80 = V_11;
		NullCheck(L_80);
		StringBuilder_Append_m2383614642(L_80, ((int32_t)47), /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_81 = V_11;
		StringU5BU5D_t1281789340* L_82 = V_4;
		int32_t L_83 = V_13;
		NullCheck(L_82);
		int32_t L_84 = L_83;
		String_t* L_85 = (L_82)->GetAt(static_cast<il2cpp_array_size_t>(L_84));
		NullCheck(L_81);
		StringBuilder_Append_m1965104174(L_81, L_85, /*hidden argument*/NULL);
		int32_t L_86 = V_13;
		V_13 = ((int32_t)((int32_t)L_86+(int32_t)1));
	}

IL_0209:
	{
		int32_t L_87 = V_13;
		int32_t L_88 = V_12;
		if ((((int32_t)L_87) < ((int32_t)L_88)))
		{
			goto IL_01ec;
		}
	}
	{
		goto IL_0227;
	}

IL_0217:
	{
		bool L_89 = V_2;
		if (!L_89)
		{
			goto IL_0227;
		}
	}
	{
		StringBuilder_t1712802186 * L_90 = V_11;
		NullCheck(L_90);
		StringBuilder_Append_m2383614642(L_90, ((int32_t)126), /*hidden argument*/NULL);
	}

IL_0227:
	{
		V_14 = 0;
		goto IL_024c;
	}

IL_022f:
	{
		StringBuilder_t1712802186 * L_91 = V_11;
		NullCheck(L_91);
		StringBuilder_Append_m2383614642(L_91, ((int32_t)47), /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_92 = V_11;
		StringU5BU5D_t1281789340* L_93 = V_5;
		int32_t L_94 = V_14;
		NullCheck(L_93);
		int32_t L_95 = L_94;
		String_t* L_96 = (L_93)->GetAt(static_cast<il2cpp_array_size_t>(L_95));
		NullCheck(L_92);
		StringBuilder_Append_m1965104174(L_92, L_96, /*hidden argument*/NULL);
		int32_t L_97 = V_14;
		V_14 = ((int32_t)((int32_t)L_97+(int32_t)1));
	}

IL_024c:
	{
		int32_t L_98 = V_14;
		int32_t L_99 = V_7;
		if ((((int32_t)L_98) < ((int32_t)L_99)))
		{
			goto IL_022f;
		}
	}
	{
		StringBuilder_t1712802186 * L_100 = V_11;
		NullCheck(L_100);
		int32_t L_101 = StringBuilder_get_Length_m3238060835(L_100, /*hidden argument*/NULL);
		if ((((int32_t)L_101) <= ((int32_t)0)))
		{
			goto IL_0277;
		}
	}
	{
		bool L_102 = V_3;
		if (!L_102)
		{
			goto IL_0272;
		}
	}
	{
		StringBuilder_t1712802186 * L_103 = V_11;
		NullCheck(L_103);
		StringBuilder_Append_m2383614642(L_103, ((int32_t)47), /*hidden argument*/NULL);
	}

IL_0272:
	{
		goto IL_027d;
	}

IL_0277:
	{
		return _stringLiteral3452614529;
	}

IL_027d:
	{
		StringBuilder_t1712802186 * L_104 = V_11;
		NullCheck(L_104);
		String_t* L_105 = StringBuilder_ToString_m3317489284(L_104, /*hidden argument*/NULL);
		return L_105;
	}
}
// System.String System.Web.VirtualPathUtility::Canonize(System.String)
extern "C"  String_t* VirtualPathUtility_Canonize_m2348126131 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPathUtility_Canonize_m2348126131_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	StringBuilder_t1712802186 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		V_0 = (-1);
		V_1 = 0;
		goto IL_005e;
	}

IL_0009:
	{
		String_t* L_0 = ___path0;
		int32_t L_1 = V_1;
		NullCheck(L_0);
		Il2CppChar L_2 = String_get_Chars_m2986988803(L_0, L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)((int32_t)92))))
		{
			goto IL_0053;
		}
	}
	{
		String_t* L_3 = ___path0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		Il2CppChar L_5 = String_get_Chars_m2986988803(L_3, L_4, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_005a;
		}
	}
	{
		int32_t L_6 = V_1;
		String_t* L_7 = ___path0;
		NullCheck(L_7);
		int32_t L_8 = String_get_Length_m3847582255(L_7, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_6+(int32_t)1))) >= ((int32_t)L_8)))
		{
			goto IL_005a;
		}
	}
	{
		String_t* L_9 = ___path0;
		int32_t L_10 = V_1;
		NullCheck(L_9);
		Il2CppChar L_11 = String_get_Chars_m2986988803(L_9, ((int32_t)((int32_t)L_10+(int32_t)1)), /*hidden argument*/NULL);
		if ((((int32_t)L_11) == ((int32_t)((int32_t)47))))
		{
			goto IL_0053;
		}
	}
	{
		String_t* L_12 = ___path0;
		int32_t L_13 = V_1;
		NullCheck(L_12);
		Il2CppChar L_14 = String_get_Chars_m2986988803(L_12, ((int32_t)((int32_t)L_13+(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_005a;
		}
	}

IL_0053:
	{
		int32_t L_15 = V_1;
		V_0 = L_15;
		goto IL_006a;
	}

IL_005a:
	{
		int32_t L_16 = V_1;
		V_1 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_005e:
	{
		int32_t L_17 = V_1;
		String_t* L_18 = ___path0;
		NullCheck(L_18);
		int32_t L_19 = String_get_Length_m3847582255(L_18, /*hidden argument*/NULL);
		if ((((int32_t)L_17) < ((int32_t)L_19)))
		{
			goto IL_0009;
		}
	}

IL_006a:
	{
		int32_t L_20 = V_0;
		if ((((int32_t)L_20) >= ((int32_t)0)))
		{
			goto IL_0073;
		}
	}
	{
		String_t* L_21 = ___path0;
		return L_21;
	}

IL_0073:
	{
		String_t* L_22 = ___path0;
		NullCheck(L_22);
		int32_t L_23 = String_get_Length_m3847582255(L_22, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_24 = (StringBuilder_t1712802186 *)il2cpp_codegen_object_new(StringBuilder_t1712802186_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m2367297767(L_24, L_23, /*hidden argument*/NULL);
		V_2 = L_24;
		StringBuilder_t1712802186 * L_25 = V_2;
		String_t* L_26 = ___path0;
		int32_t L_27 = V_0;
		NullCheck(L_25);
		StringBuilder_Append_m3214161208(L_25, L_26, 0, L_27, /*hidden argument*/NULL);
		int32_t L_28 = V_0;
		V_3 = L_28;
		goto IL_0101;
	}

IL_0090:
	{
		String_t* L_29 = ___path0;
		int32_t L_30 = V_3;
		NullCheck(L_29);
		Il2CppChar L_31 = String_get_Chars_m2986988803(L_29, L_30, /*hidden argument*/NULL);
		if ((((int32_t)L_31) == ((int32_t)((int32_t)92))))
		{
			goto IL_00ac;
		}
	}
	{
		String_t* L_32 = ___path0;
		int32_t L_33 = V_3;
		NullCheck(L_32);
		Il2CppChar L_34 = String_get_Chars_m2986988803(L_32, L_33, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_34) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_00ef;
		}
	}

IL_00ac:
	{
		int32_t L_35 = V_3;
		V_4 = ((int32_t)((int32_t)L_35+(int32_t)1));
		int32_t L_36 = V_4;
		String_t* L_37 = ___path0;
		NullCheck(L_37);
		int32_t L_38 = String_get_Length_m3847582255(L_37, /*hidden argument*/NULL);
		if ((((int32_t)L_36) >= ((int32_t)L_38)))
		{
			goto IL_00e1;
		}
	}
	{
		String_t* L_39 = ___path0;
		int32_t L_40 = V_4;
		NullCheck(L_39);
		Il2CppChar L_41 = String_get_Chars_m2986988803(L_39, L_40, /*hidden argument*/NULL);
		if ((((int32_t)L_41) == ((int32_t)((int32_t)92))))
		{
			goto IL_00dc;
		}
	}
	{
		String_t* L_42 = ___path0;
		int32_t L_43 = V_4;
		NullCheck(L_42);
		Il2CppChar L_44 = String_get_Chars_m2986988803(L_42, L_43, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_44) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_00e1;
		}
	}

IL_00dc:
	{
		goto IL_00fd;
	}

IL_00e1:
	{
		StringBuilder_t1712802186 * L_45 = V_2;
		NullCheck(L_45);
		StringBuilder_Append_m2383614642(L_45, ((int32_t)47), /*hidden argument*/NULL);
		goto IL_00fd;
	}

IL_00ef:
	{
		StringBuilder_t1712802186 * L_46 = V_2;
		String_t* L_47 = ___path0;
		int32_t L_48 = V_3;
		NullCheck(L_47);
		Il2CppChar L_49 = String_get_Chars_m2986988803(L_47, L_48, /*hidden argument*/NULL);
		NullCheck(L_46);
		StringBuilder_Append_m2383614642(L_46, L_49, /*hidden argument*/NULL);
	}

IL_00fd:
	{
		int32_t L_50 = V_3;
		V_3 = ((int32_t)((int32_t)L_50+(int32_t)1));
	}

IL_0101:
	{
		int32_t L_51 = V_3;
		String_t* L_52 = ___path0;
		NullCheck(L_52);
		int32_t L_53 = String_get_Length_m3847582255(L_52, /*hidden argument*/NULL);
		if ((((int32_t)L_51) < ((int32_t)L_53)))
		{
			goto IL_0090;
		}
	}
	{
		StringBuilder_t1712802186 * L_54 = V_2;
		NullCheck(L_54);
		String_t* L_55 = StringBuilder_ToString_m3317489284(L_54, /*hidden argument*/NULL);
		return L_55;
	}
}
// System.Boolean System.Web.VirtualPathUtility::IsValidVirtualPath(System.String)
extern "C"  bool VirtualPathUtility_IsValidVirtualPath_m146402699 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VirtualPathUtility_IsValidVirtualPath_m146402699_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___path0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)0;
	}

IL_0008:
	{
		V_0 = (bool)1;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		bool L_1 = ((VirtualPathUtility_t3131300229_StaticFields*)VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var->static_fields)->get_runningOnWindows_1();
		if (!L_1)
		{
			goto IL_004e;
		}
	}

IL_0014:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
			String_t* L_2 = ((VirtualPathUtility_t3131300229_StaticFields*)VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var->static_fields)->get_aspNetVerificationKey_4();
			IL2CPP_RUNTIME_CLASS_INIT(Registry_t2858193406_il2cpp_TypeInfo_var);
			Il2CppObject * L_3 = Registry_GetValue_m1643530476(NULL /*static, unused*/, L_2, _stringLiteral1559207716, NULL, /*hidden argument*/NULL);
			V_1 = L_3;
			Il2CppObject * L_4 = V_1;
			if (!L_4)
			{
				goto IL_0043;
			}
		}

IL_002b:
		{
			Il2CppObject * L_5 = V_1;
			if (!((Il2CppObject *)IsInstSealed(L_5, Int32_t2950945753_il2cpp_TypeInfo_var)))
			{
				goto IL_0043;
			}
		}

IL_0036:
		{
			Il2CppObject * L_6 = V_1;
			V_0 = (bool)((((int32_t)((((int32_t)((*(int32_t*)((int32_t*)UnBox(L_6, Int32_t2950945753_il2cpp_TypeInfo_var))))) == ((int32_t)1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		}

IL_0043:
		{
			goto IL_004e;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0048;
		throw e;
	}

CATCH_0048:
	{ // begin catch(System.Object)
		goto IL_004e;
	} // end catch (depth: 1)

IL_004e:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_005a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		bool L_8 = ((VirtualPathUtility_t3131300229_StaticFields*)VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var->static_fields)->get_monoSettingsVerifyCompatibility_0();
		V_0 = L_8;
	}

IL_005a:
	{
		bool L_9 = V_0;
		if (L_9)
		{
			goto IL_0062;
		}
	}
	{
		return (bool)1;
	}

IL_0062:
	{
		String_t* L_10 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var);
		CharU5BU5D_t3528271667* L_11 = ((VirtualPathUtility_t3131300229_StaticFields*)VirtualPathUtility_t3131300229_il2cpp_TypeInfo_var->static_fields)->get_invalidVirtualPathChars_3();
		NullCheck(L_10);
		int32_t L_12 = String_IndexOfAny_m4159774896(L_10, L_11, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_12) == ((int32_t)(-1)))? 1 : 0);
	}
}
// System.Void System.Web.WebCategoryAttribute::.ctor(System.String)
extern "C"  void WebCategoryAttribute__ctor_m3219041960 (WebCategoryAttribute_t1186646113 * __this, String_t* ___category0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebCategoryAttribute__ctor_m3219041960_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___category0;
		IL2CPP_RUNTIME_CLASS_INIT(CategoryAttribute_t39585132_il2cpp_TypeInfo_var);
		CategoryAttribute__ctor_m3344259296(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Web.WebROCollection::.ctor()
extern "C"  void WebROCollection__ctor_m3496122412 (WebROCollection_t4065660147 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebROCollection__ctor_m3496122412_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t3301955079_il2cpp_TypeInfo_var);
		StringComparer_t3301955079 * L_0 = StringComparer_get_OrdinalIgnoreCase_m2680809380(NULL /*static, unused*/, /*hidden argument*/NULL);
		NameValueCollection__ctor_m1036121484(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Web.WebROCollection::get_GotID()
extern "C"  bool WebROCollection_get_GotID_m2738794308 (WebROCollection_t4065660147 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_got_id_12();
		return L_0;
	}
}
// System.Int32 System.Web.WebROCollection::get_ID()
extern "C"  int32_t WebROCollection_get_ID_m1477520855 (WebROCollection_t4065660147 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_id_13();
		return L_0;
	}
}
// System.Void System.Web.WebROCollection::set_ID(System.Int32)
extern "C"  void WebROCollection_set_ID_m3868029491 (WebROCollection_t4065660147 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_got_id_12((bool)1);
		int32_t L_0 = ___value0;
		__this->set_id_13(L_0);
		return;
	}
}
// System.Void System.Web.WebROCollection::Protect()
extern "C"  void WebROCollection_Protect_m1626837399 (WebROCollection_t4065660147 * __this, const MethodInfo* method)
{
	{
		NameObjectCollectionBase_set_IsReadOnly_m2503979362(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Web.WebROCollection::Unprotect()
extern "C"  void WebROCollection_Unprotect_m2007151295 (WebROCollection_t4065660147 * __this, const MethodInfo* method)
{
	{
		NameObjectCollectionBase_set_IsReadOnly_m2503979362(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Web.WebROCollection::ToString()
extern "C"  String_t* WebROCollection_ToString_m903395350 (WebROCollection_t4065660147 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebROCollection_ToString_m903395350_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	String_t* V_1 = NULL;
	StringU5BU5D_t1281789340* V_2 = NULL;
	int32_t V_3 = 0;
	{
		StringBuilder_t1712802186 * L_0 = (StringBuilder_t1712802186 *)il2cpp_codegen_object_new(StringBuilder_t1712802186_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3121283359(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringU5BU5D_t1281789340* L_1 = VirtFuncInvoker0< StringU5BU5D_t1281789340* >::Invoke(16 /* System.String[] System.Collections.Specialized.NameValueCollection::get_AllKeys() */, __this);
		V_2 = L_1;
		V_3 = 0;
		goto IL_0062;
	}

IL_0014:
	{
		StringU5BU5D_t1281789340* L_2 = V_2;
		int32_t L_3 = V_3;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		String_t* L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_1 = L_5;
		StringBuilder_t1712802186 * L_6 = V_0;
		NullCheck(L_6);
		int32_t L_7 = StringBuilder_get_Length_m3238060835(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) <= ((int32_t)0)))
		{
			goto IL_002d;
		}
	}
	{
		StringBuilder_t1712802186 * L_8 = V_0;
		NullCheck(L_8);
		StringBuilder_Append_m2383614642(L_8, ((int32_t)38), /*hidden argument*/NULL);
	}

IL_002d:
	{
		String_t* L_9 = V_1;
		if (!L_9)
		{
			goto IL_0050;
		}
	}
	{
		String_t* L_10 = V_1;
		NullCheck(L_10);
		int32_t L_11 = String_get_Length_m3847582255(L_10, /*hidden argument*/NULL);
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_0050;
		}
	}
	{
		StringBuilder_t1712802186 * L_12 = V_0;
		String_t* L_13 = V_1;
		NullCheck(L_12);
		StringBuilder_Append_m1965104174(L_12, L_13, /*hidden argument*/NULL);
		StringBuilder_t1712802186 * L_14 = V_0;
		NullCheck(L_14);
		StringBuilder_Append_m2383614642(L_14, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_0050:
	{
		StringBuilder_t1712802186 * L_15 = V_0;
		String_t* L_16 = V_1;
		String_t* L_17 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(20 /* System.String System.Collections.Specialized.NameValueCollection::Get(System.String) */, __this, L_16);
		NullCheck(L_15);
		StringBuilder_Append_m1965104174(L_15, L_17, /*hidden argument*/NULL);
		int32_t L_18 = V_3;
		V_3 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0062:
	{
		int32_t L_19 = V_3;
		StringU5BU5D_t1281789340* L_20 = V_2;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_0014;
		}
	}
	{
		StringBuilder_t1712802186 * L_21 = V_0;
		NullCheck(L_21);
		String_t* L_22 = StringBuilder_ToString_m3317489284(L_21, /*hidden argument*/NULL);
		return L_22;
	}
}
// System.Void System.Web.WebSysDescriptionAttribute::.ctor(System.String)
extern "C"  void WebSysDescriptionAttribute__ctor_m1594100564 (WebSysDescriptionAttribute_t2907340736 * __this, String_t* ___description0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSysDescriptionAttribute__ctor_m1594100564_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___description0;
		IL2CPP_RUNTIME_CLASS_INIT(DescriptionAttribute_t874390736_il2cpp_TypeInfo_var);
		DescriptionAttribute__ctor_m1483068985(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
