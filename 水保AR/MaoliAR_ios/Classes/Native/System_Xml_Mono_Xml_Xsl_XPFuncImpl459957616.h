﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Xml_System_Xml_XPath_XPathResultType2828988488.h"

// System.Xml.XPath.XPathResultType[]
struct XPathResultTypeU5BU5D_t1515527577;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.XPFuncImpl
struct  XPFuncImpl_t459957616  : public Il2CppObject
{
public:
	// System.Int32 Mono.Xml.Xsl.XPFuncImpl::minargs
	int32_t ___minargs_0;
	// System.Int32 Mono.Xml.Xsl.XPFuncImpl::maxargs
	int32_t ___maxargs_1;
	// System.Xml.XPath.XPathResultType Mono.Xml.Xsl.XPFuncImpl::returnType
	int32_t ___returnType_2;
	// System.Xml.XPath.XPathResultType[] Mono.Xml.Xsl.XPFuncImpl::argTypes
	XPathResultTypeU5BU5D_t1515527577* ___argTypes_3;

public:
	inline static int32_t get_offset_of_minargs_0() { return static_cast<int32_t>(offsetof(XPFuncImpl_t459957616, ___minargs_0)); }
	inline int32_t get_minargs_0() const { return ___minargs_0; }
	inline int32_t* get_address_of_minargs_0() { return &___minargs_0; }
	inline void set_minargs_0(int32_t value)
	{
		___minargs_0 = value;
	}

	inline static int32_t get_offset_of_maxargs_1() { return static_cast<int32_t>(offsetof(XPFuncImpl_t459957616, ___maxargs_1)); }
	inline int32_t get_maxargs_1() const { return ___maxargs_1; }
	inline int32_t* get_address_of_maxargs_1() { return &___maxargs_1; }
	inline void set_maxargs_1(int32_t value)
	{
		___maxargs_1 = value;
	}

	inline static int32_t get_offset_of_returnType_2() { return static_cast<int32_t>(offsetof(XPFuncImpl_t459957616, ___returnType_2)); }
	inline int32_t get_returnType_2() const { return ___returnType_2; }
	inline int32_t* get_address_of_returnType_2() { return &___returnType_2; }
	inline void set_returnType_2(int32_t value)
	{
		___returnType_2 = value;
	}

	inline static int32_t get_offset_of_argTypes_3() { return static_cast<int32_t>(offsetof(XPFuncImpl_t459957616, ___argTypes_3)); }
	inline XPathResultTypeU5BU5D_t1515527577* get_argTypes_3() const { return ___argTypes_3; }
	inline XPathResultTypeU5BU5D_t1515527577** get_address_of_argTypes_3() { return &___argTypes_3; }
	inline void set_argTypes_3(XPathResultTypeU5BU5D_t1515527577* value)
	{
		___argTypes_3 = value;
		Il2CppCodeGenWriteBarrier(&___argTypes_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
