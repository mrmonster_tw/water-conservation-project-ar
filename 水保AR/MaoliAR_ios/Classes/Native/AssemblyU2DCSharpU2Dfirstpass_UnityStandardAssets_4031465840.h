﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat962350765.h"

// UnityEngine.Material
struct Material_t340375123;
// UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter/Frame[]
struct FrameU5BU5D_t2667834693;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter
struct  FrameBlendingFilter_t4031465840  : public Il2CppObject
{
public:
	// System.Boolean UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter::_useCompression
	bool ____useCompression_0;
	// UnityEngine.RenderTextureFormat UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter::_rawTextureFormat
	int32_t ____rawTextureFormat_1;
	// UnityEngine.Material UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter::_material
	Material_t340375123 * ____material_2;
	// UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter/Frame[] UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter::_frameList
	FrameU5BU5D_t2667834693* ____frameList_3;
	// System.Int32 UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter::_lastFrameCount
	int32_t ____lastFrameCount_4;

public:
	inline static int32_t get_offset_of__useCompression_0() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t4031465840, ____useCompression_0)); }
	inline bool get__useCompression_0() const { return ____useCompression_0; }
	inline bool* get_address_of__useCompression_0() { return &____useCompression_0; }
	inline void set__useCompression_0(bool value)
	{
		____useCompression_0 = value;
	}

	inline static int32_t get_offset_of__rawTextureFormat_1() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t4031465840, ____rawTextureFormat_1)); }
	inline int32_t get__rawTextureFormat_1() const { return ____rawTextureFormat_1; }
	inline int32_t* get_address_of__rawTextureFormat_1() { return &____rawTextureFormat_1; }
	inline void set__rawTextureFormat_1(int32_t value)
	{
		____rawTextureFormat_1 = value;
	}

	inline static int32_t get_offset_of__material_2() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t4031465840, ____material_2)); }
	inline Material_t340375123 * get__material_2() const { return ____material_2; }
	inline Material_t340375123 ** get_address_of__material_2() { return &____material_2; }
	inline void set__material_2(Material_t340375123 * value)
	{
		____material_2 = value;
		Il2CppCodeGenWriteBarrier(&____material_2, value);
	}

	inline static int32_t get_offset_of__frameList_3() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t4031465840, ____frameList_3)); }
	inline FrameU5BU5D_t2667834693* get__frameList_3() const { return ____frameList_3; }
	inline FrameU5BU5D_t2667834693** get_address_of__frameList_3() { return &____frameList_3; }
	inline void set__frameList_3(FrameU5BU5D_t2667834693* value)
	{
		____frameList_3 = value;
		Il2CppCodeGenWriteBarrier(&____frameList_3, value);
	}

	inline static int32_t get_offset_of__lastFrameCount_4() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t4031465840, ____lastFrameCount_4)); }
	inline int32_t get__lastFrameCount_4() const { return ____lastFrameCount_4; }
	inline int32_t* get_address_of__lastFrameCount_4() { return &____lastFrameCount_4; }
	inline void set__lastFrameCount_4(int32_t value)
	{
		____lastFrameCount_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
