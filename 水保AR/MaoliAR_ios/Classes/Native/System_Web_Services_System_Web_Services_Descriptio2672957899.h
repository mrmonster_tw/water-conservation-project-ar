﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_Services_System_Web_Services_Descriptio2466402210.h"

// System.Web.Services.Description.ServiceDescriptionFormatExtensionCollection
struct ServiceDescriptionFormatExtensionCollection_t2630153888;
// System.Web.Services.Description.PortCollection
struct PortCollection_t2517239564;
// System.Web.Services.Description.ServiceDescription
struct ServiceDescription_t3693704363;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.Service
struct  Service_t2672957899  : public NamedItem_t2466402210
{
public:
	// System.Web.Services.Description.ServiceDescriptionFormatExtensionCollection System.Web.Services.Description.Service::extensions
	ServiceDescriptionFormatExtensionCollection_t2630153888 * ___extensions_4;
	// System.Web.Services.Description.PortCollection System.Web.Services.Description.Service::ports
	PortCollection_t2517239564 * ___ports_5;
	// System.Web.Services.Description.ServiceDescription System.Web.Services.Description.Service::serviceDescription
	ServiceDescription_t3693704363 * ___serviceDescription_6;

public:
	inline static int32_t get_offset_of_extensions_4() { return static_cast<int32_t>(offsetof(Service_t2672957899, ___extensions_4)); }
	inline ServiceDescriptionFormatExtensionCollection_t2630153888 * get_extensions_4() const { return ___extensions_4; }
	inline ServiceDescriptionFormatExtensionCollection_t2630153888 ** get_address_of_extensions_4() { return &___extensions_4; }
	inline void set_extensions_4(ServiceDescriptionFormatExtensionCollection_t2630153888 * value)
	{
		___extensions_4 = value;
		Il2CppCodeGenWriteBarrier(&___extensions_4, value);
	}

	inline static int32_t get_offset_of_ports_5() { return static_cast<int32_t>(offsetof(Service_t2672957899, ___ports_5)); }
	inline PortCollection_t2517239564 * get_ports_5() const { return ___ports_5; }
	inline PortCollection_t2517239564 ** get_address_of_ports_5() { return &___ports_5; }
	inline void set_ports_5(PortCollection_t2517239564 * value)
	{
		___ports_5 = value;
		Il2CppCodeGenWriteBarrier(&___ports_5, value);
	}

	inline static int32_t get_offset_of_serviceDescription_6() { return static_cast<int32_t>(offsetof(Service_t2672957899, ___serviceDescription_6)); }
	inline ServiceDescription_t3693704363 * get_serviceDescription_6() const { return ___serviceDescription_6; }
	inline ServiceDescription_t3693704363 ** get_address_of_serviceDescription_6() { return &___serviceDescription_6; }
	inline void set_serviceDescription_6(ServiceDescription_t3693704363 * value)
	{
		___serviceDescription_6 = value;
		Il2CppCodeGenWriteBarrier(&___serviceDescription_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
