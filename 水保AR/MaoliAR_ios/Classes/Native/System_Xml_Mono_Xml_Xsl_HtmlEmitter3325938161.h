﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Emitter942443340.h"

// System.IO.TextWriter
struct TextWriter_t3478189236;
// System.Collections.Stack
struct Stack_t2329662280;
// System.Text.Encoding
struct Encoding_t1523322056;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.HtmlEmitter
struct  HtmlEmitter_t3325938161  : public Emitter_t942443340
{
public:
	// System.IO.TextWriter Mono.Xml.Xsl.HtmlEmitter::writer
	TextWriter_t3478189236 * ___writer_0;
	// System.Collections.Stack Mono.Xml.Xsl.HtmlEmitter::elementNameStack
	Stack_t2329662280 * ___elementNameStack_1;
	// System.Boolean Mono.Xml.Xsl.HtmlEmitter::openElement
	bool ___openElement_2;
	// System.Boolean Mono.Xml.Xsl.HtmlEmitter::openAttribute
	bool ___openAttribute_3;
	// System.Int32 Mono.Xml.Xsl.HtmlEmitter::nonHtmlDepth
	int32_t ___nonHtmlDepth_4;
	// System.Boolean Mono.Xml.Xsl.HtmlEmitter::indent
	bool ___indent_5;
	// System.Text.Encoding Mono.Xml.Xsl.HtmlEmitter::outputEncoding
	Encoding_t1523322056 * ___outputEncoding_6;
	// System.String Mono.Xml.Xsl.HtmlEmitter::mediaType
	String_t* ___mediaType_7;

public:
	inline static int32_t get_offset_of_writer_0() { return static_cast<int32_t>(offsetof(HtmlEmitter_t3325938161, ___writer_0)); }
	inline TextWriter_t3478189236 * get_writer_0() const { return ___writer_0; }
	inline TextWriter_t3478189236 ** get_address_of_writer_0() { return &___writer_0; }
	inline void set_writer_0(TextWriter_t3478189236 * value)
	{
		___writer_0 = value;
		Il2CppCodeGenWriteBarrier(&___writer_0, value);
	}

	inline static int32_t get_offset_of_elementNameStack_1() { return static_cast<int32_t>(offsetof(HtmlEmitter_t3325938161, ___elementNameStack_1)); }
	inline Stack_t2329662280 * get_elementNameStack_1() const { return ___elementNameStack_1; }
	inline Stack_t2329662280 ** get_address_of_elementNameStack_1() { return &___elementNameStack_1; }
	inline void set_elementNameStack_1(Stack_t2329662280 * value)
	{
		___elementNameStack_1 = value;
		Il2CppCodeGenWriteBarrier(&___elementNameStack_1, value);
	}

	inline static int32_t get_offset_of_openElement_2() { return static_cast<int32_t>(offsetof(HtmlEmitter_t3325938161, ___openElement_2)); }
	inline bool get_openElement_2() const { return ___openElement_2; }
	inline bool* get_address_of_openElement_2() { return &___openElement_2; }
	inline void set_openElement_2(bool value)
	{
		___openElement_2 = value;
	}

	inline static int32_t get_offset_of_openAttribute_3() { return static_cast<int32_t>(offsetof(HtmlEmitter_t3325938161, ___openAttribute_3)); }
	inline bool get_openAttribute_3() const { return ___openAttribute_3; }
	inline bool* get_address_of_openAttribute_3() { return &___openAttribute_3; }
	inline void set_openAttribute_3(bool value)
	{
		___openAttribute_3 = value;
	}

	inline static int32_t get_offset_of_nonHtmlDepth_4() { return static_cast<int32_t>(offsetof(HtmlEmitter_t3325938161, ___nonHtmlDepth_4)); }
	inline int32_t get_nonHtmlDepth_4() const { return ___nonHtmlDepth_4; }
	inline int32_t* get_address_of_nonHtmlDepth_4() { return &___nonHtmlDepth_4; }
	inline void set_nonHtmlDepth_4(int32_t value)
	{
		___nonHtmlDepth_4 = value;
	}

	inline static int32_t get_offset_of_indent_5() { return static_cast<int32_t>(offsetof(HtmlEmitter_t3325938161, ___indent_5)); }
	inline bool get_indent_5() const { return ___indent_5; }
	inline bool* get_address_of_indent_5() { return &___indent_5; }
	inline void set_indent_5(bool value)
	{
		___indent_5 = value;
	}

	inline static int32_t get_offset_of_outputEncoding_6() { return static_cast<int32_t>(offsetof(HtmlEmitter_t3325938161, ___outputEncoding_6)); }
	inline Encoding_t1523322056 * get_outputEncoding_6() const { return ___outputEncoding_6; }
	inline Encoding_t1523322056 ** get_address_of_outputEncoding_6() { return &___outputEncoding_6; }
	inline void set_outputEncoding_6(Encoding_t1523322056 * value)
	{
		___outputEncoding_6 = value;
		Il2CppCodeGenWriteBarrier(&___outputEncoding_6, value);
	}

	inline static int32_t get_offset_of_mediaType_7() { return static_cast<int32_t>(offsetof(HtmlEmitter_t3325938161, ___mediaType_7)); }
	inline String_t* get_mediaType_7() const { return ___mediaType_7; }
	inline String_t** get_address_of_mediaType_7() { return &___mediaType_7; }
	inline void set_mediaType_7(String_t* value)
	{
		___mediaType_7 = value;
		Il2CppCodeGenWriteBarrier(&___mediaType_7, value);
	}
};

struct HtmlEmitter_t3325938161_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.HtmlEmitter::<>f__switch$map13
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map13_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.HtmlEmitter::<>f__switch$map14
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map14_9;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.HtmlEmitter::<>f__switch$map15
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map15_10;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.HtmlEmitter::<>f__switch$map16
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map16_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.HtmlEmitter::<>f__switch$map17
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map17_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.HtmlEmitter::<>f__switch$map18
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map18_13;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map13_8() { return static_cast<int32_t>(offsetof(HtmlEmitter_t3325938161_StaticFields, ___U3CU3Ef__switchU24map13_8)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map13_8() const { return ___U3CU3Ef__switchU24map13_8; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map13_8() { return &___U3CU3Ef__switchU24map13_8; }
	inline void set_U3CU3Ef__switchU24map13_8(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map13_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map13_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map14_9() { return static_cast<int32_t>(offsetof(HtmlEmitter_t3325938161_StaticFields, ___U3CU3Ef__switchU24map14_9)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map14_9() const { return ___U3CU3Ef__switchU24map14_9; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map14_9() { return &___U3CU3Ef__switchU24map14_9; }
	inline void set_U3CU3Ef__switchU24map14_9(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map14_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map14_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map15_10() { return static_cast<int32_t>(offsetof(HtmlEmitter_t3325938161_StaticFields, ___U3CU3Ef__switchU24map15_10)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map15_10() const { return ___U3CU3Ef__switchU24map15_10; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map15_10() { return &___U3CU3Ef__switchU24map15_10; }
	inline void set_U3CU3Ef__switchU24map15_10(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map15_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map15_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map16_11() { return static_cast<int32_t>(offsetof(HtmlEmitter_t3325938161_StaticFields, ___U3CU3Ef__switchU24map16_11)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map16_11() const { return ___U3CU3Ef__switchU24map16_11; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map16_11() { return &___U3CU3Ef__switchU24map16_11; }
	inline void set_U3CU3Ef__switchU24map16_11(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map16_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map16_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map17_12() { return static_cast<int32_t>(offsetof(HtmlEmitter_t3325938161_StaticFields, ___U3CU3Ef__switchU24map17_12)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map17_12() const { return ___U3CU3Ef__switchU24map17_12; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map17_12() { return &___U3CU3Ef__switchU24map17_12; }
	inline void set_U3CU3Ef__switchU24map17_12(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map17_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map17_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map18_13() { return static_cast<int32_t>(offsetof(HtmlEmitter_t3325938161_StaticFields, ___U3CU3Ef__switchU24map18_13)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map18_13() const { return ___U3CU3Ef__switchU24map18_13; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map18_13() { return &___U3CU3Ef__switchU24map18_13; }
	inline void set_U3CU3Ef__switchU24map18_13(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map18_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map18_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
