﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Operations_XslCompiledElemen50593777.h"

// System.Xml.XPath.XPathExpression
struct XPathExpression_t1723793351;
// Mono.Xml.Xsl.Operations.XslOperation
struct XslOperation_t2153241355;
// Mono.Xml.Xsl.XslSortEvaluator
struct XslSortEvaluator_t824821660;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslForEach
struct  XslForEach_t3423108485  : public XslCompiledElement_t50593777
{
public:
	// System.Xml.XPath.XPathExpression Mono.Xml.Xsl.Operations.XslForEach::select
	XPathExpression_t1723793351 * ___select_3;
	// Mono.Xml.Xsl.Operations.XslOperation Mono.Xml.Xsl.Operations.XslForEach::children
	XslOperation_t2153241355 * ___children_4;
	// Mono.Xml.Xsl.XslSortEvaluator Mono.Xml.Xsl.Operations.XslForEach::sortEvaluator
	XslSortEvaluator_t824821660 * ___sortEvaluator_5;

public:
	inline static int32_t get_offset_of_select_3() { return static_cast<int32_t>(offsetof(XslForEach_t3423108485, ___select_3)); }
	inline XPathExpression_t1723793351 * get_select_3() const { return ___select_3; }
	inline XPathExpression_t1723793351 ** get_address_of_select_3() { return &___select_3; }
	inline void set_select_3(XPathExpression_t1723793351 * value)
	{
		___select_3 = value;
		Il2CppCodeGenWriteBarrier(&___select_3, value);
	}

	inline static int32_t get_offset_of_children_4() { return static_cast<int32_t>(offsetof(XslForEach_t3423108485, ___children_4)); }
	inline XslOperation_t2153241355 * get_children_4() const { return ___children_4; }
	inline XslOperation_t2153241355 ** get_address_of_children_4() { return &___children_4; }
	inline void set_children_4(XslOperation_t2153241355 * value)
	{
		___children_4 = value;
		Il2CppCodeGenWriteBarrier(&___children_4, value);
	}

	inline static int32_t get_offset_of_sortEvaluator_5() { return static_cast<int32_t>(offsetof(XslForEach_t3423108485, ___sortEvaluator_5)); }
	inline XslSortEvaluator_t824821660 * get_sortEvaluator_5() const { return ___sortEvaluator_5; }
	inline XslSortEvaluator_t824821660 ** get_address_of_sortEvaluator_5() { return &___sortEvaluator_5; }
	inline void set_sortEvaluator_5(XslSortEvaluator_t824821660 * value)
	{
		___sortEvaluator_5 = value;
		Il2CppCodeGenWriteBarrier(&___sortEvaluator_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
