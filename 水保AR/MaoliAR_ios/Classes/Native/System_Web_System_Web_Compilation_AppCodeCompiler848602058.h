﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Web.Compilation.AppCodeAssembly>
struct List_1_t2733458919;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.AppCodeCompiler
struct  AppCodeCompiler_t848602058  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.Web.Compilation.AppCodeAssembly> System.Web.Compilation.AppCodeCompiler::assemblies
	List_1_t2733458919 * ___assemblies_2;
	// System.String System.Web.Compilation.AppCodeCompiler::providerTypeName
	String_t* ___providerTypeName_3;

public:
	inline static int32_t get_offset_of_assemblies_2() { return static_cast<int32_t>(offsetof(AppCodeCompiler_t848602058, ___assemblies_2)); }
	inline List_1_t2733458919 * get_assemblies_2() const { return ___assemblies_2; }
	inline List_1_t2733458919 ** get_address_of_assemblies_2() { return &___assemblies_2; }
	inline void set_assemblies_2(List_1_t2733458919 * value)
	{
		___assemblies_2 = value;
		Il2CppCodeGenWriteBarrier(&___assemblies_2, value);
	}

	inline static int32_t get_offset_of_providerTypeName_3() { return static_cast<int32_t>(offsetof(AppCodeCompiler_t848602058, ___providerTypeName_3)); }
	inline String_t* get_providerTypeName_3() const { return ___providerTypeName_3; }
	inline String_t** get_address_of_providerTypeName_3() { return &___providerTypeName_3; }
	inline void set_providerTypeName_3(String_t* value)
	{
		___providerTypeName_3 = value;
		Il2CppCodeGenWriteBarrier(&___providerTypeName_3, value);
	}
};

struct AppCodeCompiler_t848602058_StaticFields
{
public:
	// System.Boolean System.Web.Compilation.AppCodeCompiler::_alreadyCompiled
	bool ____alreadyCompiled_0;
	// System.String System.Web.Compilation.AppCodeCompiler::DefaultAppCodeAssemblyName
	String_t* ___DefaultAppCodeAssemblyName_1;

public:
	inline static int32_t get_offset_of__alreadyCompiled_0() { return static_cast<int32_t>(offsetof(AppCodeCompiler_t848602058_StaticFields, ____alreadyCompiled_0)); }
	inline bool get__alreadyCompiled_0() const { return ____alreadyCompiled_0; }
	inline bool* get_address_of__alreadyCompiled_0() { return &____alreadyCompiled_0; }
	inline void set__alreadyCompiled_0(bool value)
	{
		____alreadyCompiled_0 = value;
	}

	inline static int32_t get_offset_of_DefaultAppCodeAssemblyName_1() { return static_cast<int32_t>(offsetof(AppCodeCompiler_t848602058_StaticFields, ___DefaultAppCodeAssemblyName_1)); }
	inline String_t* get_DefaultAppCodeAssemblyName_1() const { return ___DefaultAppCodeAssemblyName_1; }
	inline String_t** get_address_of_DefaultAppCodeAssemblyName_1() { return &___DefaultAppCodeAssemblyName_1; }
	inline void set_DefaultAppCodeAssemblyName_1(String_t* value)
	{
		___DefaultAppCodeAssemblyName_1 = value;
		Il2CppCodeGenWriteBarrier(&___DefaultAppCodeAssemblyName_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
