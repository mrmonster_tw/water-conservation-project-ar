﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Security_Se807295854.h"

// System.ServiceModel.Security.X509CertificateInitiatorServiceCredential
struct X509CertificateInitiatorServiceCredential_t3275324517;
// System.ServiceModel.Security.PeerCredential
struct PeerCredential_t4076012648;
// System.ServiceModel.Security.X509CertificateRecipientServiceCredential
struct X509CertificateRecipientServiceCredential_t1823977328;
// System.ServiceModel.Security.UserNamePasswordServiceCredential
struct UserNamePasswordServiceCredential_t3640294368;
// System.ServiceModel.Security.WindowsServiceCredential
struct WindowsServiceCredential_t1594975448;
// System.ServiceModel.Security.IssuedTokenServiceCredential
struct IssuedTokenServiceCredential_t922425120;
// System.ServiceModel.Security.SecureConversationServiceCredential
struct SecureConversationServiceCredential_t1277180297;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.ServiceCredentials
struct  ServiceCredentials_t3431196922  : public SecurityCredentialsManager_t807295854
{
public:
	// System.ServiceModel.Security.X509CertificateInitiatorServiceCredential System.ServiceModel.Description.ServiceCredentials::initiator
	X509CertificateInitiatorServiceCredential_t3275324517 * ___initiator_0;
	// System.ServiceModel.Security.PeerCredential System.ServiceModel.Description.ServiceCredentials::peer
	PeerCredential_t4076012648 * ___peer_1;
	// System.ServiceModel.Security.X509CertificateRecipientServiceCredential System.ServiceModel.Description.ServiceCredentials::recipient
	X509CertificateRecipientServiceCredential_t1823977328 * ___recipient_2;
	// System.ServiceModel.Security.UserNamePasswordServiceCredential System.ServiceModel.Description.ServiceCredentials::userpass
	UserNamePasswordServiceCredential_t3640294368 * ___userpass_3;
	// System.ServiceModel.Security.WindowsServiceCredential System.ServiceModel.Description.ServiceCredentials::windows
	WindowsServiceCredential_t1594975448 * ___windows_4;
	// System.ServiceModel.Security.IssuedTokenServiceCredential System.ServiceModel.Description.ServiceCredentials::issued_token
	IssuedTokenServiceCredential_t922425120 * ___issued_token_5;
	// System.ServiceModel.Security.SecureConversationServiceCredential System.ServiceModel.Description.ServiceCredentials::secure_conversation
	SecureConversationServiceCredential_t1277180297 * ___secure_conversation_6;

public:
	inline static int32_t get_offset_of_initiator_0() { return static_cast<int32_t>(offsetof(ServiceCredentials_t3431196922, ___initiator_0)); }
	inline X509CertificateInitiatorServiceCredential_t3275324517 * get_initiator_0() const { return ___initiator_0; }
	inline X509CertificateInitiatorServiceCredential_t3275324517 ** get_address_of_initiator_0() { return &___initiator_0; }
	inline void set_initiator_0(X509CertificateInitiatorServiceCredential_t3275324517 * value)
	{
		___initiator_0 = value;
		Il2CppCodeGenWriteBarrier(&___initiator_0, value);
	}

	inline static int32_t get_offset_of_peer_1() { return static_cast<int32_t>(offsetof(ServiceCredentials_t3431196922, ___peer_1)); }
	inline PeerCredential_t4076012648 * get_peer_1() const { return ___peer_1; }
	inline PeerCredential_t4076012648 ** get_address_of_peer_1() { return &___peer_1; }
	inline void set_peer_1(PeerCredential_t4076012648 * value)
	{
		___peer_1 = value;
		Il2CppCodeGenWriteBarrier(&___peer_1, value);
	}

	inline static int32_t get_offset_of_recipient_2() { return static_cast<int32_t>(offsetof(ServiceCredentials_t3431196922, ___recipient_2)); }
	inline X509CertificateRecipientServiceCredential_t1823977328 * get_recipient_2() const { return ___recipient_2; }
	inline X509CertificateRecipientServiceCredential_t1823977328 ** get_address_of_recipient_2() { return &___recipient_2; }
	inline void set_recipient_2(X509CertificateRecipientServiceCredential_t1823977328 * value)
	{
		___recipient_2 = value;
		Il2CppCodeGenWriteBarrier(&___recipient_2, value);
	}

	inline static int32_t get_offset_of_userpass_3() { return static_cast<int32_t>(offsetof(ServiceCredentials_t3431196922, ___userpass_3)); }
	inline UserNamePasswordServiceCredential_t3640294368 * get_userpass_3() const { return ___userpass_3; }
	inline UserNamePasswordServiceCredential_t3640294368 ** get_address_of_userpass_3() { return &___userpass_3; }
	inline void set_userpass_3(UserNamePasswordServiceCredential_t3640294368 * value)
	{
		___userpass_3 = value;
		Il2CppCodeGenWriteBarrier(&___userpass_3, value);
	}

	inline static int32_t get_offset_of_windows_4() { return static_cast<int32_t>(offsetof(ServiceCredentials_t3431196922, ___windows_4)); }
	inline WindowsServiceCredential_t1594975448 * get_windows_4() const { return ___windows_4; }
	inline WindowsServiceCredential_t1594975448 ** get_address_of_windows_4() { return &___windows_4; }
	inline void set_windows_4(WindowsServiceCredential_t1594975448 * value)
	{
		___windows_4 = value;
		Il2CppCodeGenWriteBarrier(&___windows_4, value);
	}

	inline static int32_t get_offset_of_issued_token_5() { return static_cast<int32_t>(offsetof(ServiceCredentials_t3431196922, ___issued_token_5)); }
	inline IssuedTokenServiceCredential_t922425120 * get_issued_token_5() const { return ___issued_token_5; }
	inline IssuedTokenServiceCredential_t922425120 ** get_address_of_issued_token_5() { return &___issued_token_5; }
	inline void set_issued_token_5(IssuedTokenServiceCredential_t922425120 * value)
	{
		___issued_token_5 = value;
		Il2CppCodeGenWriteBarrier(&___issued_token_5, value);
	}

	inline static int32_t get_offset_of_secure_conversation_6() { return static_cast<int32_t>(offsetof(ServiceCredentials_t3431196922, ___secure_conversation_6)); }
	inline SecureConversationServiceCredential_t1277180297 * get_secure_conversation_6() const { return ___secure_conversation_6; }
	inline SecureConversationServiceCredential_t1277180297 ** get_address_of_secure_conversation_6() { return &___secure_conversation_6; }
	inline void set_secure_conversation_6(SecureConversationServiceCredential_t1277180297 * value)
	{
		___secure_conversation_6 = value;
		Il2CppCodeGenWriteBarrier(&___secure_conversation_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
