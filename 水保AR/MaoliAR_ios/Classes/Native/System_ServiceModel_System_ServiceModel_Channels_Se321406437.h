﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Web.HttpContext
struct HttpContext_t1969259010;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.ServiceHostParser
struct  ServiceHostParser_t321406437  : public Il2CppObject
{
public:
	// System.String System.ServiceModel.Channels.ServiceHostParser::file
	String_t* ___file_0;
	// System.String System.ServiceModel.Channels.ServiceHostParser::url
	String_t* ___url_1;
	// System.String System.ServiceModel.Channels.ServiceHostParser::type_name
	String_t* ___type_name_2;
	// System.String System.ServiceModel.Channels.ServiceHostParser::language
	String_t* ___language_3;
	// System.String System.ServiceModel.Channels.ServiceHostParser::factory
	String_t* ___factory_4;
	// System.Boolean System.ServiceModel.Channels.ServiceHostParser::debug
	bool ___debug_5;
	// System.Boolean System.ServiceModel.Channels.ServiceHostParser::got_default_directive
	bool ___got_default_directive_6;
	// System.String System.ServiceModel.Channels.ServiceHostParser::program
	String_t* ___program_7;
	// System.Collections.ArrayList System.ServiceModel.Channels.ServiceHostParser::assemblies
	ArrayList_t2718874744 * ___assemblies_8;
	// System.Web.HttpContext System.ServiceModel.Channels.ServiceHostParser::context
	HttpContext_t1969259010 * ___context_9;

public:
	inline static int32_t get_offset_of_file_0() { return static_cast<int32_t>(offsetof(ServiceHostParser_t321406437, ___file_0)); }
	inline String_t* get_file_0() const { return ___file_0; }
	inline String_t** get_address_of_file_0() { return &___file_0; }
	inline void set_file_0(String_t* value)
	{
		___file_0 = value;
		Il2CppCodeGenWriteBarrier(&___file_0, value);
	}

	inline static int32_t get_offset_of_url_1() { return static_cast<int32_t>(offsetof(ServiceHostParser_t321406437, ___url_1)); }
	inline String_t* get_url_1() const { return ___url_1; }
	inline String_t** get_address_of_url_1() { return &___url_1; }
	inline void set_url_1(String_t* value)
	{
		___url_1 = value;
		Il2CppCodeGenWriteBarrier(&___url_1, value);
	}

	inline static int32_t get_offset_of_type_name_2() { return static_cast<int32_t>(offsetof(ServiceHostParser_t321406437, ___type_name_2)); }
	inline String_t* get_type_name_2() const { return ___type_name_2; }
	inline String_t** get_address_of_type_name_2() { return &___type_name_2; }
	inline void set_type_name_2(String_t* value)
	{
		___type_name_2 = value;
		Il2CppCodeGenWriteBarrier(&___type_name_2, value);
	}

	inline static int32_t get_offset_of_language_3() { return static_cast<int32_t>(offsetof(ServiceHostParser_t321406437, ___language_3)); }
	inline String_t* get_language_3() const { return ___language_3; }
	inline String_t** get_address_of_language_3() { return &___language_3; }
	inline void set_language_3(String_t* value)
	{
		___language_3 = value;
		Il2CppCodeGenWriteBarrier(&___language_3, value);
	}

	inline static int32_t get_offset_of_factory_4() { return static_cast<int32_t>(offsetof(ServiceHostParser_t321406437, ___factory_4)); }
	inline String_t* get_factory_4() const { return ___factory_4; }
	inline String_t** get_address_of_factory_4() { return &___factory_4; }
	inline void set_factory_4(String_t* value)
	{
		___factory_4 = value;
		Il2CppCodeGenWriteBarrier(&___factory_4, value);
	}

	inline static int32_t get_offset_of_debug_5() { return static_cast<int32_t>(offsetof(ServiceHostParser_t321406437, ___debug_5)); }
	inline bool get_debug_5() const { return ___debug_5; }
	inline bool* get_address_of_debug_5() { return &___debug_5; }
	inline void set_debug_5(bool value)
	{
		___debug_5 = value;
	}

	inline static int32_t get_offset_of_got_default_directive_6() { return static_cast<int32_t>(offsetof(ServiceHostParser_t321406437, ___got_default_directive_6)); }
	inline bool get_got_default_directive_6() const { return ___got_default_directive_6; }
	inline bool* get_address_of_got_default_directive_6() { return &___got_default_directive_6; }
	inline void set_got_default_directive_6(bool value)
	{
		___got_default_directive_6 = value;
	}

	inline static int32_t get_offset_of_program_7() { return static_cast<int32_t>(offsetof(ServiceHostParser_t321406437, ___program_7)); }
	inline String_t* get_program_7() const { return ___program_7; }
	inline String_t** get_address_of_program_7() { return &___program_7; }
	inline void set_program_7(String_t* value)
	{
		___program_7 = value;
		Il2CppCodeGenWriteBarrier(&___program_7, value);
	}

	inline static int32_t get_offset_of_assemblies_8() { return static_cast<int32_t>(offsetof(ServiceHostParser_t321406437, ___assemblies_8)); }
	inline ArrayList_t2718874744 * get_assemblies_8() const { return ___assemblies_8; }
	inline ArrayList_t2718874744 ** get_address_of_assemblies_8() { return &___assemblies_8; }
	inline void set_assemblies_8(ArrayList_t2718874744 * value)
	{
		___assemblies_8 = value;
		Il2CppCodeGenWriteBarrier(&___assemblies_8, value);
	}

	inline static int32_t get_offset_of_context_9() { return static_cast<int32_t>(offsetof(ServiceHostParser_t321406437, ___context_9)); }
	inline HttpContext_t1969259010 * get_context_9() const { return ___context_9; }
	inline HttpContext_t1969259010 ** get_address_of_context_9() { return &___context_9; }
	inline void set_context_9(HttpContext_t1969259010 * value)
	{
		___context_9 = value;
		Il2CppCodeGenWriteBarrier(&___context_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
