﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Web_System_Web_Compilation_AppResourceFileK1276421563.h"

// System.IO.FileInfo
struct FileInfo_t1169991790;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.AppResourceFileInfo
struct  AppResourceFileInfo_t3599744125  : public Il2CppObject
{
public:
	// System.Boolean System.Web.Compilation.AppResourceFileInfo::Embeddable
	bool ___Embeddable_0;
	// System.Boolean System.Web.Compilation.AppResourceFileInfo::Compilable
	bool ___Compilable_1;
	// System.IO.FileInfo System.Web.Compilation.AppResourceFileInfo::Info
	FileInfo_t1169991790 * ___Info_2;
	// System.Web.Compilation.AppResourceFileKind System.Web.Compilation.AppResourceFileInfo::Kind
	int32_t ___Kind_3;
	// System.Boolean System.Web.Compilation.AppResourceFileInfo::Seen
	bool ___Seen_4;

public:
	inline static int32_t get_offset_of_Embeddable_0() { return static_cast<int32_t>(offsetof(AppResourceFileInfo_t3599744125, ___Embeddable_0)); }
	inline bool get_Embeddable_0() const { return ___Embeddable_0; }
	inline bool* get_address_of_Embeddable_0() { return &___Embeddable_0; }
	inline void set_Embeddable_0(bool value)
	{
		___Embeddable_0 = value;
	}

	inline static int32_t get_offset_of_Compilable_1() { return static_cast<int32_t>(offsetof(AppResourceFileInfo_t3599744125, ___Compilable_1)); }
	inline bool get_Compilable_1() const { return ___Compilable_1; }
	inline bool* get_address_of_Compilable_1() { return &___Compilable_1; }
	inline void set_Compilable_1(bool value)
	{
		___Compilable_1 = value;
	}

	inline static int32_t get_offset_of_Info_2() { return static_cast<int32_t>(offsetof(AppResourceFileInfo_t3599744125, ___Info_2)); }
	inline FileInfo_t1169991790 * get_Info_2() const { return ___Info_2; }
	inline FileInfo_t1169991790 ** get_address_of_Info_2() { return &___Info_2; }
	inline void set_Info_2(FileInfo_t1169991790 * value)
	{
		___Info_2 = value;
		Il2CppCodeGenWriteBarrier(&___Info_2, value);
	}

	inline static int32_t get_offset_of_Kind_3() { return static_cast<int32_t>(offsetof(AppResourceFileInfo_t3599744125, ___Kind_3)); }
	inline int32_t get_Kind_3() const { return ___Kind_3; }
	inline int32_t* get_address_of_Kind_3() { return &___Kind_3; }
	inline void set_Kind_3(int32_t value)
	{
		___Kind_3 = value;
	}

	inline static int32_t get_offset_of_Seen_4() { return static_cast<int32_t>(offsetof(AppResourceFileInfo_t3599744125, ___Seen_4)); }
	inline bool get_Seen_4() const { return ___Seen_4; }
	inline bool* get_address_of_Seen_4() { return &___Seen_4; }
	inline void set_Seen_4(bool value)
	{
		___Seen_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
