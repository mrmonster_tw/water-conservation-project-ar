﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.EventHandler
struct EventHandler_t1348719766;
// System.Web.NoParamsDelegate
struct NoParamsDelegate_t1618427640;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.NoParamsInvoker
struct  NoParamsInvoker_t2754751357  : public Il2CppObject
{
public:
	// System.EventHandler System.Web.NoParamsInvoker::faked
	EventHandler_t1348719766 * ___faked_0;
	// System.Web.NoParamsDelegate System.Web.NoParamsInvoker::real
	NoParamsDelegate_t1618427640 * ___real_1;

public:
	inline static int32_t get_offset_of_faked_0() { return static_cast<int32_t>(offsetof(NoParamsInvoker_t2754751357, ___faked_0)); }
	inline EventHandler_t1348719766 * get_faked_0() const { return ___faked_0; }
	inline EventHandler_t1348719766 ** get_address_of_faked_0() { return &___faked_0; }
	inline void set_faked_0(EventHandler_t1348719766 * value)
	{
		___faked_0 = value;
		Il2CppCodeGenWriteBarrier(&___faked_0, value);
	}

	inline static int32_t get_offset_of_real_1() { return static_cast<int32_t>(offsetof(NoParamsInvoker_t2754751357, ___real_1)); }
	inline NoParamsDelegate_t1618427640 * get_real_1() const { return ___real_1; }
	inline NoParamsDelegate_t1618427640 ** get_address_of_real_1() { return &___real_1; }
	inline void set_real_1(NoParamsDelegate_t1618427640 * value)
	{
		___real_1 = value;
		Il2CppCodeGenWriteBarrier(&___real_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
