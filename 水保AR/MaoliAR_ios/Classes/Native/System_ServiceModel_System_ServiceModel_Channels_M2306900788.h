﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Me831925271.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.MessageHeader/RawMessageHeader
struct  RawMessageHeader_t2306900788  : public MessageHeader_t831925271
{
public:
	// System.Boolean System.ServiceModel.Channels.MessageHeader/RawMessageHeader::is_ref
	bool ___is_ref_6;
	// System.Boolean System.ServiceModel.Channels.MessageHeader/RawMessageHeader::must_understand
	bool ___must_understand_7;
	// System.Boolean System.ServiceModel.Channels.MessageHeader/RawMessageHeader::relay
	bool ___relay_8;
	// System.String System.ServiceModel.Channels.MessageHeader/RawMessageHeader::actor
	String_t* ___actor_9;
	// System.String System.ServiceModel.Channels.MessageHeader/RawMessageHeader::body
	String_t* ___body_10;
	// System.String System.ServiceModel.Channels.MessageHeader/RawMessageHeader::local_name
	String_t* ___local_name_11;
	// System.String System.ServiceModel.Channels.MessageHeader/RawMessageHeader::namespace_uri
	String_t* ___namespace_uri_12;

public:
	inline static int32_t get_offset_of_is_ref_6() { return static_cast<int32_t>(offsetof(RawMessageHeader_t2306900788, ___is_ref_6)); }
	inline bool get_is_ref_6() const { return ___is_ref_6; }
	inline bool* get_address_of_is_ref_6() { return &___is_ref_6; }
	inline void set_is_ref_6(bool value)
	{
		___is_ref_6 = value;
	}

	inline static int32_t get_offset_of_must_understand_7() { return static_cast<int32_t>(offsetof(RawMessageHeader_t2306900788, ___must_understand_7)); }
	inline bool get_must_understand_7() const { return ___must_understand_7; }
	inline bool* get_address_of_must_understand_7() { return &___must_understand_7; }
	inline void set_must_understand_7(bool value)
	{
		___must_understand_7 = value;
	}

	inline static int32_t get_offset_of_relay_8() { return static_cast<int32_t>(offsetof(RawMessageHeader_t2306900788, ___relay_8)); }
	inline bool get_relay_8() const { return ___relay_8; }
	inline bool* get_address_of_relay_8() { return &___relay_8; }
	inline void set_relay_8(bool value)
	{
		___relay_8 = value;
	}

	inline static int32_t get_offset_of_actor_9() { return static_cast<int32_t>(offsetof(RawMessageHeader_t2306900788, ___actor_9)); }
	inline String_t* get_actor_9() const { return ___actor_9; }
	inline String_t** get_address_of_actor_9() { return &___actor_9; }
	inline void set_actor_9(String_t* value)
	{
		___actor_9 = value;
		Il2CppCodeGenWriteBarrier(&___actor_9, value);
	}

	inline static int32_t get_offset_of_body_10() { return static_cast<int32_t>(offsetof(RawMessageHeader_t2306900788, ___body_10)); }
	inline String_t* get_body_10() const { return ___body_10; }
	inline String_t** get_address_of_body_10() { return &___body_10; }
	inline void set_body_10(String_t* value)
	{
		___body_10 = value;
		Il2CppCodeGenWriteBarrier(&___body_10, value);
	}

	inline static int32_t get_offset_of_local_name_11() { return static_cast<int32_t>(offsetof(RawMessageHeader_t2306900788, ___local_name_11)); }
	inline String_t* get_local_name_11() const { return ___local_name_11; }
	inline String_t** get_address_of_local_name_11() { return &___local_name_11; }
	inline void set_local_name_11(String_t* value)
	{
		___local_name_11 = value;
		Il2CppCodeGenWriteBarrier(&___local_name_11, value);
	}

	inline static int32_t get_offset_of_namespace_uri_12() { return static_cast<int32_t>(offsetof(RawMessageHeader_t2306900788, ___namespace_uri_12)); }
	inline String_t* get_namespace_uri_12() const { return ___namespace_uri_12; }
	inline String_t** get_address_of_namespace_uri_12() { return &___namespace_uri_12; }
	inline void set_namespace_uri_12(String_t* value)
	{
		___namespace_uri_12 = value;
		Il2CppCodeGenWriteBarrier(&___namespace_uri_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
