﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.UI.TemplateParser
struct TemplateParser_t24149626;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.PageParserFilter
struct  PageParserFilter_t169228355  : public Il2CppObject
{
public:
	// System.Web.UI.TemplateParser System.Web.UI.PageParserFilter::parser
	TemplateParser_t24149626 * ___parser_0;

public:
	inline static int32_t get_offset_of_parser_0() { return static_cast<int32_t>(offsetof(PageParserFilter_t169228355, ___parser_0)); }
	inline TemplateParser_t24149626 * get_parser_0() const { return ___parser_0; }
	inline TemplateParser_t24149626 ** get_address_of_parser_0() { return &___parser_0; }
	inline void set_parser_0(TemplateParser_t24149626 * value)
	{
		___parser_0 = value;
		Il2CppCodeGenWriteBarrier(&___parser_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
