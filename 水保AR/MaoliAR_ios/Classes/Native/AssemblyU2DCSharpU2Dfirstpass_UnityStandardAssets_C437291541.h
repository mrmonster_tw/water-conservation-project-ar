﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityStandardAssets.CinematicEffects.MotionBlur/Settings
struct Settings_t1356140371;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityStandardAssets.CinematicEffects.MotionBlur/ReconstructionFilter
struct ReconstructionFilter_t4011033762;
// UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter
struct FrameBlendingFilter_t4031465840;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.MotionBlur
struct  MotionBlur_t437291541  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.CinematicEffects.MotionBlur/Settings UnityStandardAssets.CinematicEffects.MotionBlur::_settings
	Settings_t1356140371 * ____settings_2;
	// UnityEngine.Shader UnityStandardAssets.CinematicEffects.MotionBlur::_reconstructionShader
	Shader_t4151988712 * ____reconstructionShader_3;
	// UnityEngine.Shader UnityStandardAssets.CinematicEffects.MotionBlur::_frameBlendingShader
	Shader_t4151988712 * ____frameBlendingShader_4;
	// UnityStandardAssets.CinematicEffects.MotionBlur/ReconstructionFilter UnityStandardAssets.CinematicEffects.MotionBlur::_reconstructionFilter
	ReconstructionFilter_t4011033762 * ____reconstructionFilter_5;
	// UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter UnityStandardAssets.CinematicEffects.MotionBlur::_frameBlendingFilter
	FrameBlendingFilter_t4031465840 * ____frameBlendingFilter_6;

public:
	inline static int32_t get_offset_of__settings_2() { return static_cast<int32_t>(offsetof(MotionBlur_t437291541, ____settings_2)); }
	inline Settings_t1356140371 * get__settings_2() const { return ____settings_2; }
	inline Settings_t1356140371 ** get_address_of__settings_2() { return &____settings_2; }
	inline void set__settings_2(Settings_t1356140371 * value)
	{
		____settings_2 = value;
		Il2CppCodeGenWriteBarrier(&____settings_2, value);
	}

	inline static int32_t get_offset_of__reconstructionShader_3() { return static_cast<int32_t>(offsetof(MotionBlur_t437291541, ____reconstructionShader_3)); }
	inline Shader_t4151988712 * get__reconstructionShader_3() const { return ____reconstructionShader_3; }
	inline Shader_t4151988712 ** get_address_of__reconstructionShader_3() { return &____reconstructionShader_3; }
	inline void set__reconstructionShader_3(Shader_t4151988712 * value)
	{
		____reconstructionShader_3 = value;
		Il2CppCodeGenWriteBarrier(&____reconstructionShader_3, value);
	}

	inline static int32_t get_offset_of__frameBlendingShader_4() { return static_cast<int32_t>(offsetof(MotionBlur_t437291541, ____frameBlendingShader_4)); }
	inline Shader_t4151988712 * get__frameBlendingShader_4() const { return ____frameBlendingShader_4; }
	inline Shader_t4151988712 ** get_address_of__frameBlendingShader_4() { return &____frameBlendingShader_4; }
	inline void set__frameBlendingShader_4(Shader_t4151988712 * value)
	{
		____frameBlendingShader_4 = value;
		Il2CppCodeGenWriteBarrier(&____frameBlendingShader_4, value);
	}

	inline static int32_t get_offset_of__reconstructionFilter_5() { return static_cast<int32_t>(offsetof(MotionBlur_t437291541, ____reconstructionFilter_5)); }
	inline ReconstructionFilter_t4011033762 * get__reconstructionFilter_5() const { return ____reconstructionFilter_5; }
	inline ReconstructionFilter_t4011033762 ** get_address_of__reconstructionFilter_5() { return &____reconstructionFilter_5; }
	inline void set__reconstructionFilter_5(ReconstructionFilter_t4011033762 * value)
	{
		____reconstructionFilter_5 = value;
		Il2CppCodeGenWriteBarrier(&____reconstructionFilter_5, value);
	}

	inline static int32_t get_offset_of__frameBlendingFilter_6() { return static_cast<int32_t>(offsetof(MotionBlur_t437291541, ____frameBlendingFilter_6)); }
	inline FrameBlendingFilter_t4031465840 * get__frameBlendingFilter_6() const { return ____frameBlendingFilter_6; }
	inline FrameBlendingFilter_t4031465840 ** get_address_of__frameBlendingFilter_6() { return &____frameBlendingFilter_6; }
	inline void set__frameBlendingFilter_6(FrameBlendingFilter_t4031465840 * value)
	{
		____frameBlendingFilter_6 = value;
		Il2CppCodeGenWriteBarrier(&____frameBlendingFilter_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
