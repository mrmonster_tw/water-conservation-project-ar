﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.Compiler.CompilerProviderOption
struct  CompilerProviderOption_t2418514121  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct CompilerProviderOption_t2418514121_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.CodeDom.Compiler.CompilerProviderOption::nameProp
	ConfigurationProperty_t3590861854 * ___nameProp_13;
	// System.Configuration.ConfigurationProperty System.CodeDom.Compiler.CompilerProviderOption::valueProp
	ConfigurationProperty_t3590861854 * ___valueProp_14;
	// System.Configuration.ConfigurationPropertyCollection System.CodeDom.Compiler.CompilerProviderOption::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_15;

public:
	inline static int32_t get_offset_of_nameProp_13() { return static_cast<int32_t>(offsetof(CompilerProviderOption_t2418514121_StaticFields, ___nameProp_13)); }
	inline ConfigurationProperty_t3590861854 * get_nameProp_13() const { return ___nameProp_13; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_nameProp_13() { return &___nameProp_13; }
	inline void set_nameProp_13(ConfigurationProperty_t3590861854 * value)
	{
		___nameProp_13 = value;
		Il2CppCodeGenWriteBarrier(&___nameProp_13, value);
	}

	inline static int32_t get_offset_of_valueProp_14() { return static_cast<int32_t>(offsetof(CompilerProviderOption_t2418514121_StaticFields, ___valueProp_14)); }
	inline ConfigurationProperty_t3590861854 * get_valueProp_14() const { return ___valueProp_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_valueProp_14() { return &___valueProp_14; }
	inline void set_valueProp_14(ConfigurationProperty_t3590861854 * value)
	{
		___valueProp_14 = value;
		Il2CppCodeGenWriteBarrier(&___valueProp_14, value);
	}

	inline static int32_t get_offset_of_properties_15() { return static_cast<int32_t>(offsetof(CompilerProviderOption_t2418514121_StaticFields, ___properties_15)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_15() const { return ___properties_15; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_15() { return &___properties_15; }
	inline void set_properties_15(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_15 = value;
		Il2CppCodeGenWriteBarrier(&___properties_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
