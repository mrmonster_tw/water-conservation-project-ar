﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_ClientRunti398573209.h"
#include "mscorlib_System_TimeSpan881159249.h"

// System.ServiceModel.InstanceContext
struct InstanceContext_t3593205954;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t1333520283;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.DuplexClientRuntimeChannel
struct  DuplexClientRuntimeChannel_t737641411  : public ClientRuntimeChannel_t398573209
{
public:
	// System.ServiceModel.InstanceContext System.ServiceModel.DuplexClientRuntimeChannel::callback_instance
	InstanceContext_t3593205954 * ___callback_instance_16;
	// System.Boolean System.ServiceModel.DuplexClientRuntimeChannel::loop
	bool ___loop_17;
	// System.TimeSpan System.ServiceModel.DuplexClientRuntimeChannel::receive_timeout
	TimeSpan_t881159249  ___receive_timeout_18;
	// System.Boolean System.ServiceModel.DuplexClientRuntimeChannel::receive_synchronously
	bool ___receive_synchronously_19;
	// System.IAsyncResult System.ServiceModel.DuplexClientRuntimeChannel::loop_result
	Il2CppObject * ___loop_result_20;
	// System.Threading.AutoResetEvent System.ServiceModel.DuplexClientRuntimeChannel::loop_handle
	AutoResetEvent_t1333520283 * ___loop_handle_21;
	// System.Threading.AutoResetEvent System.ServiceModel.DuplexClientRuntimeChannel::finish_handle
	AutoResetEvent_t1333520283 * ___finish_handle_22;

public:
	inline static int32_t get_offset_of_callback_instance_16() { return static_cast<int32_t>(offsetof(DuplexClientRuntimeChannel_t737641411, ___callback_instance_16)); }
	inline InstanceContext_t3593205954 * get_callback_instance_16() const { return ___callback_instance_16; }
	inline InstanceContext_t3593205954 ** get_address_of_callback_instance_16() { return &___callback_instance_16; }
	inline void set_callback_instance_16(InstanceContext_t3593205954 * value)
	{
		___callback_instance_16 = value;
		Il2CppCodeGenWriteBarrier(&___callback_instance_16, value);
	}

	inline static int32_t get_offset_of_loop_17() { return static_cast<int32_t>(offsetof(DuplexClientRuntimeChannel_t737641411, ___loop_17)); }
	inline bool get_loop_17() const { return ___loop_17; }
	inline bool* get_address_of_loop_17() { return &___loop_17; }
	inline void set_loop_17(bool value)
	{
		___loop_17 = value;
	}

	inline static int32_t get_offset_of_receive_timeout_18() { return static_cast<int32_t>(offsetof(DuplexClientRuntimeChannel_t737641411, ___receive_timeout_18)); }
	inline TimeSpan_t881159249  get_receive_timeout_18() const { return ___receive_timeout_18; }
	inline TimeSpan_t881159249 * get_address_of_receive_timeout_18() { return &___receive_timeout_18; }
	inline void set_receive_timeout_18(TimeSpan_t881159249  value)
	{
		___receive_timeout_18 = value;
	}

	inline static int32_t get_offset_of_receive_synchronously_19() { return static_cast<int32_t>(offsetof(DuplexClientRuntimeChannel_t737641411, ___receive_synchronously_19)); }
	inline bool get_receive_synchronously_19() const { return ___receive_synchronously_19; }
	inline bool* get_address_of_receive_synchronously_19() { return &___receive_synchronously_19; }
	inline void set_receive_synchronously_19(bool value)
	{
		___receive_synchronously_19 = value;
	}

	inline static int32_t get_offset_of_loop_result_20() { return static_cast<int32_t>(offsetof(DuplexClientRuntimeChannel_t737641411, ___loop_result_20)); }
	inline Il2CppObject * get_loop_result_20() const { return ___loop_result_20; }
	inline Il2CppObject ** get_address_of_loop_result_20() { return &___loop_result_20; }
	inline void set_loop_result_20(Il2CppObject * value)
	{
		___loop_result_20 = value;
		Il2CppCodeGenWriteBarrier(&___loop_result_20, value);
	}

	inline static int32_t get_offset_of_loop_handle_21() { return static_cast<int32_t>(offsetof(DuplexClientRuntimeChannel_t737641411, ___loop_handle_21)); }
	inline AutoResetEvent_t1333520283 * get_loop_handle_21() const { return ___loop_handle_21; }
	inline AutoResetEvent_t1333520283 ** get_address_of_loop_handle_21() { return &___loop_handle_21; }
	inline void set_loop_handle_21(AutoResetEvent_t1333520283 * value)
	{
		___loop_handle_21 = value;
		Il2CppCodeGenWriteBarrier(&___loop_handle_21, value);
	}

	inline static int32_t get_offset_of_finish_handle_22() { return static_cast<int32_t>(offsetof(DuplexClientRuntimeChannel_t737641411, ___finish_handle_22)); }
	inline AutoResetEvent_t1333520283 * get_finish_handle_22() const { return ___finish_handle_22; }
	inline AutoResetEvent_t1333520283 ** get_address_of_finish_handle_22() { return &___finish_handle_22; }
	inline void set_finish_handle_22(AutoResetEvent_t1333520283 * value)
	{
		___finish_handle_22 = value;
		Il2CppCodeGenWriteBarrier(&___finish_handle_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
