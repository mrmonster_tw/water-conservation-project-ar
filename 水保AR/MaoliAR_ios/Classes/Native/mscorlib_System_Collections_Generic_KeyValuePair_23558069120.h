﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManager_Tra4227350457.h"
#include "Vuforia_UnityExtensions_Vuforia_HoloLensExtendedTr2181165958.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>
struct  KeyValuePair_2_t3558069120 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	TrackableIdPair_t4227350457  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	PoseAgeEntry_t2181165958  ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3558069120, ___key_0)); }
	inline TrackableIdPair_t4227350457  get_key_0() const { return ___key_0; }
	inline TrackableIdPair_t4227350457 * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(TrackableIdPair_t4227350457  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3558069120, ___value_1)); }
	inline PoseAgeEntry_t2181165958  get_value_1() const { return ___value_1; }
	inline PoseAgeEntry_t2181165958 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(PoseAgeEntry_t2181165958  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
