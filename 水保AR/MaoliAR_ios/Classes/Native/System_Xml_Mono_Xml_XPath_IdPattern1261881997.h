﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_XPath_LocationPathPattern3015773238.h"

// System.String[]
struct StringU5BU5D_t1281789340;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.XPath.IdPattern
struct  IdPattern_t1261881997  : public LocationPathPattern_t3015773238
{
public:
	// System.String[] Mono.Xml.XPath.IdPattern::ids
	StringU5BU5D_t1281789340* ___ids_4;

public:
	inline static int32_t get_offset_of_ids_4() { return static_cast<int32_t>(offsetof(IdPattern_t1261881997, ___ids_4)); }
	inline StringU5BU5D_t1281789340* get_ids_4() const { return ___ids_4; }
	inline StringU5BU5D_t1281789340** get_address_of_ids_4() { return &___ids_4; }
	inline void set_ids_4(StringU5BU5D_t1281789340* value)
	{
		___ids_4 = value;
		Il2CppCodeGenWriteBarrier(&___ids_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
