﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"

// System.ComponentModel.BindableAttribute
struct BindableAttribute_t3255577478;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.BindableAttribute
struct  BindableAttribute_t3255577478  : public Attribute_t861562559
{
public:
	// System.Boolean System.ComponentModel.BindableAttribute::bindable
	bool ___bindable_0;

public:
	inline static int32_t get_offset_of_bindable_0() { return static_cast<int32_t>(offsetof(BindableAttribute_t3255577478, ___bindable_0)); }
	inline bool get_bindable_0() const { return ___bindable_0; }
	inline bool* get_address_of_bindable_0() { return &___bindable_0; }
	inline void set_bindable_0(bool value)
	{
		___bindable_0 = value;
	}
};

struct BindableAttribute_t3255577478_StaticFields
{
public:
	// System.ComponentModel.BindableAttribute System.ComponentModel.BindableAttribute::No
	BindableAttribute_t3255577478 * ___No_1;
	// System.ComponentModel.BindableAttribute System.ComponentModel.BindableAttribute::Yes
	BindableAttribute_t3255577478 * ___Yes_2;
	// System.ComponentModel.BindableAttribute System.ComponentModel.BindableAttribute::Default
	BindableAttribute_t3255577478 * ___Default_3;

public:
	inline static int32_t get_offset_of_No_1() { return static_cast<int32_t>(offsetof(BindableAttribute_t3255577478_StaticFields, ___No_1)); }
	inline BindableAttribute_t3255577478 * get_No_1() const { return ___No_1; }
	inline BindableAttribute_t3255577478 ** get_address_of_No_1() { return &___No_1; }
	inline void set_No_1(BindableAttribute_t3255577478 * value)
	{
		___No_1 = value;
		Il2CppCodeGenWriteBarrier(&___No_1, value);
	}

	inline static int32_t get_offset_of_Yes_2() { return static_cast<int32_t>(offsetof(BindableAttribute_t3255577478_StaticFields, ___Yes_2)); }
	inline BindableAttribute_t3255577478 * get_Yes_2() const { return ___Yes_2; }
	inline BindableAttribute_t3255577478 ** get_address_of_Yes_2() { return &___Yes_2; }
	inline void set_Yes_2(BindableAttribute_t3255577478 * value)
	{
		___Yes_2 = value;
		Il2CppCodeGenWriteBarrier(&___Yes_2, value);
	}

	inline static int32_t get_offset_of_Default_3() { return static_cast<int32_t>(offsetof(BindableAttribute_t3255577478_StaticFields, ___Default_3)); }
	inline BindableAttribute_t3255577478 * get_Default_3() const { return ___Default_3; }
	inline BindableAttribute_t3255577478 ** get_address_of_Default_3() { return &___Default_3; }
	inline void set_Default_3(BindableAttribute_t3255577478 * value)
	{
		___Default_3 = value;
		Il2CppCodeGenWriteBarrier(&___Default_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
