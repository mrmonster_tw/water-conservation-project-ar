﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.Reflection.Assembly
struct Assembly_t4102432799;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.DefaultResourceProvider/ResourceManagerCacheKey
struct  ResourceManagerCacheKey_t3697749236  : public Il2CppObject
{
public:
	// System.String System.Web.Compilation.DefaultResourceProvider/ResourceManagerCacheKey::_name
	String_t* ____name_0;
	// System.Reflection.Assembly System.Web.Compilation.DefaultResourceProvider/ResourceManagerCacheKey::_asm
	Assembly_t4102432799 * ____asm_1;

public:
	inline static int32_t get_offset_of__name_0() { return static_cast<int32_t>(offsetof(ResourceManagerCacheKey_t3697749236, ____name_0)); }
	inline String_t* get__name_0() const { return ____name_0; }
	inline String_t** get_address_of__name_0() { return &____name_0; }
	inline void set__name_0(String_t* value)
	{
		____name_0 = value;
		Il2CppCodeGenWriteBarrier(&____name_0, value);
	}

	inline static int32_t get_offset_of__asm_1() { return static_cast<int32_t>(offsetof(ResourceManagerCacheKey_t3697749236, ____asm_1)); }
	inline Assembly_t4102432799 * get__asm_1() const { return ____asm_1; }
	inline Assembly_t4102432799 ** get_address_of__asm_1() { return &____asm_1; }
	inline void set__asm_1(Assembly_t4102432799 * value)
	{
		____asm_1 = value;
		Il2CppCodeGenWriteBarrier(&____asm_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
