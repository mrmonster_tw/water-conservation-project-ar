﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_MessageSec2079539966.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.MessageSecurityVersion/MessageSecurityVersionImpl
struct  MessageSecurityVersionImpl_t66298087  : public MessageSecurityVersion_t2079539966
{
public:
	// System.Boolean System.ServiceModel.MessageSecurityVersion/MessageSecurityVersionImpl::wss11
	bool ___wss11_8;
	// System.Boolean System.ServiceModel.MessageSecurityVersion/MessageSecurityVersionImpl::basic_profile
	bool ___basic_profile_9;
	// System.Boolean System.ServiceModel.MessageSecurityVersion/MessageSecurityVersionImpl::use2007
	bool ___use2007_10;

public:
	inline static int32_t get_offset_of_wss11_8() { return static_cast<int32_t>(offsetof(MessageSecurityVersionImpl_t66298087, ___wss11_8)); }
	inline bool get_wss11_8() const { return ___wss11_8; }
	inline bool* get_address_of_wss11_8() { return &___wss11_8; }
	inline void set_wss11_8(bool value)
	{
		___wss11_8 = value;
	}

	inline static int32_t get_offset_of_basic_profile_9() { return static_cast<int32_t>(offsetof(MessageSecurityVersionImpl_t66298087, ___basic_profile_9)); }
	inline bool get_basic_profile_9() const { return ___basic_profile_9; }
	inline bool* get_address_of_basic_profile_9() { return &___basic_profile_9; }
	inline void set_basic_profile_9(bool value)
	{
		___basic_profile_9 = value;
	}

	inline static int32_t get_offset_of_use2007_10() { return static_cast<int32_t>(offsetof(MessageSecurityVersionImpl_t66298087, ___use2007_10)); }
	inline bool get_use2007_10() const { return ___use2007_10; }
	inline bool* get_address_of_use2007_10() { return &___use2007_10; }
	inline void set_use2007_10(bool value)
	{
		___use2007_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
