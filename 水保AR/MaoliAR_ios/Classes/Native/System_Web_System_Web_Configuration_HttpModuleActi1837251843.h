﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;
// System.Configuration.ConfigurationElementProperty
struct ConfigurationElementProperty_t939439970;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.HttpModuleAction
struct  HttpModuleAction_t1837251843  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct HttpModuleAction_t1837251843_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Web.Configuration.HttpModuleAction::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.HttpModuleAction::nameProp
	ConfigurationProperty_t3590861854 * ___nameProp_14;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.HttpModuleAction::typeProp
	ConfigurationProperty_t3590861854 * ___typeProp_15;
	// System.Configuration.ConfigurationElementProperty System.Web.Configuration.HttpModuleAction::elementProperty
	ConfigurationElementProperty_t939439970 * ___elementProperty_16;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(HttpModuleAction_t1837251843_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier(&___properties_13, value);
	}

	inline static int32_t get_offset_of_nameProp_14() { return static_cast<int32_t>(offsetof(HttpModuleAction_t1837251843_StaticFields, ___nameProp_14)); }
	inline ConfigurationProperty_t3590861854 * get_nameProp_14() const { return ___nameProp_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_nameProp_14() { return &___nameProp_14; }
	inline void set_nameProp_14(ConfigurationProperty_t3590861854 * value)
	{
		___nameProp_14 = value;
		Il2CppCodeGenWriteBarrier(&___nameProp_14, value);
	}

	inline static int32_t get_offset_of_typeProp_15() { return static_cast<int32_t>(offsetof(HttpModuleAction_t1837251843_StaticFields, ___typeProp_15)); }
	inline ConfigurationProperty_t3590861854 * get_typeProp_15() const { return ___typeProp_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_typeProp_15() { return &___typeProp_15; }
	inline void set_typeProp_15(ConfigurationProperty_t3590861854 * value)
	{
		___typeProp_15 = value;
		Il2CppCodeGenWriteBarrier(&___typeProp_15, value);
	}

	inline static int32_t get_offset_of_elementProperty_16() { return static_cast<int32_t>(offsetof(HttpModuleAction_t1837251843_StaticFields, ___elementProperty_16)); }
	inline ConfigurationElementProperty_t939439970 * get_elementProperty_16() const { return ___elementProperty_16; }
	inline ConfigurationElementProperty_t939439970 ** get_address_of_elementProperty_16() { return &___elementProperty_16; }
	inline void set_elementProperty_16(ConfigurationElementProperty_t939439970 * value)
	{
		___elementProperty_16 = value;
		Il2CppCodeGenWriteBarrier(&___elementProperty_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
