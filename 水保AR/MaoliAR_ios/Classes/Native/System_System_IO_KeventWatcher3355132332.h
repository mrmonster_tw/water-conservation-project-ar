﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.IO.KeventWatcher
struct KeventWatcher_t3355132332;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Threading.Thread
struct Thread_t2300836069;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.KeventWatcher
struct  KeventWatcher_t3355132332  : public Il2CppObject
{
public:

public:
};

struct KeventWatcher_t3355132332_StaticFields
{
public:
	// System.Boolean System.IO.KeventWatcher::failed
	bool ___failed_0;
	// System.IO.KeventWatcher System.IO.KeventWatcher::instance
	KeventWatcher_t3355132332 * ___instance_1;
	// System.Collections.Hashtable System.IO.KeventWatcher::watches
	Hashtable_t1853889766 * ___watches_2;
	// System.Collections.Hashtable System.IO.KeventWatcher::requests
	Hashtable_t1853889766 * ___requests_3;
	// System.Threading.Thread System.IO.KeventWatcher::thread
	Thread_t2300836069 * ___thread_4;
	// System.Int32 System.IO.KeventWatcher::conn
	int32_t ___conn_5;
	// System.Boolean System.IO.KeventWatcher::stop
	bool ___stop_6;

public:
	inline static int32_t get_offset_of_failed_0() { return static_cast<int32_t>(offsetof(KeventWatcher_t3355132332_StaticFields, ___failed_0)); }
	inline bool get_failed_0() const { return ___failed_0; }
	inline bool* get_address_of_failed_0() { return &___failed_0; }
	inline void set_failed_0(bool value)
	{
		___failed_0 = value;
	}

	inline static int32_t get_offset_of_instance_1() { return static_cast<int32_t>(offsetof(KeventWatcher_t3355132332_StaticFields, ___instance_1)); }
	inline KeventWatcher_t3355132332 * get_instance_1() const { return ___instance_1; }
	inline KeventWatcher_t3355132332 ** get_address_of_instance_1() { return &___instance_1; }
	inline void set_instance_1(KeventWatcher_t3355132332 * value)
	{
		___instance_1 = value;
		Il2CppCodeGenWriteBarrier(&___instance_1, value);
	}

	inline static int32_t get_offset_of_watches_2() { return static_cast<int32_t>(offsetof(KeventWatcher_t3355132332_StaticFields, ___watches_2)); }
	inline Hashtable_t1853889766 * get_watches_2() const { return ___watches_2; }
	inline Hashtable_t1853889766 ** get_address_of_watches_2() { return &___watches_2; }
	inline void set_watches_2(Hashtable_t1853889766 * value)
	{
		___watches_2 = value;
		Il2CppCodeGenWriteBarrier(&___watches_2, value);
	}

	inline static int32_t get_offset_of_requests_3() { return static_cast<int32_t>(offsetof(KeventWatcher_t3355132332_StaticFields, ___requests_3)); }
	inline Hashtable_t1853889766 * get_requests_3() const { return ___requests_3; }
	inline Hashtable_t1853889766 ** get_address_of_requests_3() { return &___requests_3; }
	inline void set_requests_3(Hashtable_t1853889766 * value)
	{
		___requests_3 = value;
		Il2CppCodeGenWriteBarrier(&___requests_3, value);
	}

	inline static int32_t get_offset_of_thread_4() { return static_cast<int32_t>(offsetof(KeventWatcher_t3355132332_StaticFields, ___thread_4)); }
	inline Thread_t2300836069 * get_thread_4() const { return ___thread_4; }
	inline Thread_t2300836069 ** get_address_of_thread_4() { return &___thread_4; }
	inline void set_thread_4(Thread_t2300836069 * value)
	{
		___thread_4 = value;
		Il2CppCodeGenWriteBarrier(&___thread_4, value);
	}

	inline static int32_t get_offset_of_conn_5() { return static_cast<int32_t>(offsetof(KeventWatcher_t3355132332_StaticFields, ___conn_5)); }
	inline int32_t get_conn_5() const { return ___conn_5; }
	inline int32_t* get_address_of_conn_5() { return &___conn_5; }
	inline void set_conn_5(int32_t value)
	{
		___conn_5 = value;
	}

	inline static int32_t get_offset_of_stop_6() { return static_cast<int32_t>(offsetof(KeventWatcher_t3355132332_StaticFields, ___stop_6)); }
	inline bool get_stop_6() const { return ___stop_6; }
	inline bool* get_address_of_stop_6() { return &___stop_6; }
	inline void set_stop_6(bool value)
	{
		___stop_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
