﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_M3561136435.h"

// System.ServiceModel.Channels.MtomMessageEncodingBindingElement
struct MtomMessageEncodingBindingElement_t3584114940;
// System.ServiceModel.Channels.MtomMessageEncoder
struct MtomMessageEncoder_t3033005575;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.MtomMessageEncoderFactory
struct  MtomMessageEncoderFactory_t1690669593  : public MessageEncoderFactory_t3561136435
{
public:
	// System.ServiceModel.Channels.MtomMessageEncodingBindingElement System.ServiceModel.Channels.MtomMessageEncoderFactory::owner
	MtomMessageEncodingBindingElement_t3584114940 * ___owner_0;
	// System.ServiceModel.Channels.MtomMessageEncoder System.ServiceModel.Channels.MtomMessageEncoderFactory::encoder
	MtomMessageEncoder_t3033005575 * ___encoder_1;

public:
	inline static int32_t get_offset_of_owner_0() { return static_cast<int32_t>(offsetof(MtomMessageEncoderFactory_t1690669593, ___owner_0)); }
	inline MtomMessageEncodingBindingElement_t3584114940 * get_owner_0() const { return ___owner_0; }
	inline MtomMessageEncodingBindingElement_t3584114940 ** get_address_of_owner_0() { return &___owner_0; }
	inline void set_owner_0(MtomMessageEncodingBindingElement_t3584114940 * value)
	{
		___owner_0 = value;
		Il2CppCodeGenWriteBarrier(&___owner_0, value);
	}

	inline static int32_t get_offset_of_encoder_1() { return static_cast<int32_t>(offsetof(MtomMessageEncoderFactory_t1690669593, ___encoder_1)); }
	inline MtomMessageEncoder_t3033005575 * get_encoder_1() const { return ___encoder_1; }
	inline MtomMessageEncoder_t3033005575 ** get_address_of_encoder_1() { return &___encoder_1; }
	inline void set_encoder_1(MtomMessageEncoder_t3033005575 * value)
	{
		___encoder_1 = value;
		Il2CppCodeGenWriteBarrier(&___encoder_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
