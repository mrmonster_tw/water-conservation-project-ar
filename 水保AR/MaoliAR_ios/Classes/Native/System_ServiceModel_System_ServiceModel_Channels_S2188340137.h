﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_I1191478253.h"

// System.ServiceModel.Channels.IReplyChannel
struct IReplyChannel_t3856619590;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.SecurityReplyChannel
struct  SecurityReplyChannel_t2188340137  : public InternalReplyChannelBase_t1191478253
{
public:
	// System.ServiceModel.Channels.IReplyChannel System.ServiceModel.Channels.SecurityReplyChannel::inner
	Il2CppObject * ___inner_15;

public:
	inline static int32_t get_offset_of_inner_15() { return static_cast<int32_t>(offsetof(SecurityReplyChannel_t2188340137, ___inner_15)); }
	inline Il2CppObject * get_inner_15() const { return ___inner_15; }
	inline Il2CppObject ** get_address_of_inner_15() { return &___inner_15; }
	inline void set_inner_15(Il2CppObject * value)
	{
		___inner_15 = value;
		Il2CppCodeGenWriteBarrier(&___inner_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
