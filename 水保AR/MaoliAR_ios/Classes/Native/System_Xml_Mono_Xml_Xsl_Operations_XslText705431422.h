﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Operations_XslCompiledElemen50593777.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslText
struct  XslText_t705431422  : public XslCompiledElement_t50593777
{
public:
	// System.Boolean Mono.Xml.Xsl.Operations.XslText::disableOutputEscaping
	bool ___disableOutputEscaping_3;
	// System.String Mono.Xml.Xsl.Operations.XslText::text
	String_t* ___text_4;
	// System.Boolean Mono.Xml.Xsl.Operations.XslText::isWhitespace
	bool ___isWhitespace_5;

public:
	inline static int32_t get_offset_of_disableOutputEscaping_3() { return static_cast<int32_t>(offsetof(XslText_t705431422, ___disableOutputEscaping_3)); }
	inline bool get_disableOutputEscaping_3() const { return ___disableOutputEscaping_3; }
	inline bool* get_address_of_disableOutputEscaping_3() { return &___disableOutputEscaping_3; }
	inline void set_disableOutputEscaping_3(bool value)
	{
		___disableOutputEscaping_3 = value;
	}

	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(XslText_t705431422, ___text_4)); }
	inline String_t* get_text_4() const { return ___text_4; }
	inline String_t** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(String_t* value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier(&___text_4, value);
	}

	inline static int32_t get_offset_of_isWhitespace_5() { return static_cast<int32_t>(offsetof(XslText_t705431422, ___isWhitespace_5)); }
	inline bool get_isWhitespace_5() const { return ___isWhitespace_5; }
	inline bool* get_address_of_isWhitespace_5() { return &___isWhitespace_5; }
	inline void set_isWhitespace_5(bool value)
	{
		___isWhitespace_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
