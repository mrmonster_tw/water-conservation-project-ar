﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "AssemblyU2DCSharp_QuestionedComponets_Imgs1080690280.h"
#include "AssemblyU2DCSharp_QuestionedComponets_Questions2798307503.h"

// ExpiredComponets
struct ExpiredComponets_t4043685999;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuestionedComponets
struct  QuestionedComponets_t1753441808  : public MonoBehaviour_t3962482529
{
public:
	// ExpiredComponets QuestionedComponets::ex
	ExpiredComponets_t4043685999 * ___ex_2;
	// QuestionedComponets/Imgs QuestionedComponets::imgs
	Imgs_t1080690280  ___imgs_3;
	// QuestionedComponets/Questions QuestionedComponets::questions
	Questions_t2798307503  ___questions_4;
	// System.String QuestionedComponets::PlayerSelectInfo
	String_t* ___PlayerSelectInfo_5;
	// System.String[] QuestionedComponets::resultsaver
	StringU5BU5D_t1281789340* ___resultsaver_6;

public:
	inline static int32_t get_offset_of_ex_2() { return static_cast<int32_t>(offsetof(QuestionedComponets_t1753441808, ___ex_2)); }
	inline ExpiredComponets_t4043685999 * get_ex_2() const { return ___ex_2; }
	inline ExpiredComponets_t4043685999 ** get_address_of_ex_2() { return &___ex_2; }
	inline void set_ex_2(ExpiredComponets_t4043685999 * value)
	{
		___ex_2 = value;
		Il2CppCodeGenWriteBarrier(&___ex_2, value);
	}

	inline static int32_t get_offset_of_imgs_3() { return static_cast<int32_t>(offsetof(QuestionedComponets_t1753441808, ___imgs_3)); }
	inline Imgs_t1080690280  get_imgs_3() const { return ___imgs_3; }
	inline Imgs_t1080690280 * get_address_of_imgs_3() { return &___imgs_3; }
	inline void set_imgs_3(Imgs_t1080690280  value)
	{
		___imgs_3 = value;
	}

	inline static int32_t get_offset_of_questions_4() { return static_cast<int32_t>(offsetof(QuestionedComponets_t1753441808, ___questions_4)); }
	inline Questions_t2798307503  get_questions_4() const { return ___questions_4; }
	inline Questions_t2798307503 * get_address_of_questions_4() { return &___questions_4; }
	inline void set_questions_4(Questions_t2798307503  value)
	{
		___questions_4 = value;
	}

	inline static int32_t get_offset_of_PlayerSelectInfo_5() { return static_cast<int32_t>(offsetof(QuestionedComponets_t1753441808, ___PlayerSelectInfo_5)); }
	inline String_t* get_PlayerSelectInfo_5() const { return ___PlayerSelectInfo_5; }
	inline String_t** get_address_of_PlayerSelectInfo_5() { return &___PlayerSelectInfo_5; }
	inline void set_PlayerSelectInfo_5(String_t* value)
	{
		___PlayerSelectInfo_5 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerSelectInfo_5, value);
	}

	inline static int32_t get_offset_of_resultsaver_6() { return static_cast<int32_t>(offsetof(QuestionedComponets_t1753441808, ___resultsaver_6)); }
	inline StringU5BU5D_t1281789340* get_resultsaver_6() const { return ___resultsaver_6; }
	inline StringU5BU5D_t1281789340** get_address_of_resultsaver_6() { return &___resultsaver_6; }
	inline void set_resultsaver_6(StringU5BU5D_t1281789340* value)
	{
		___resultsaver_6 = value;
		Il2CppCodeGenWriteBarrier(&___resultsaver_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
