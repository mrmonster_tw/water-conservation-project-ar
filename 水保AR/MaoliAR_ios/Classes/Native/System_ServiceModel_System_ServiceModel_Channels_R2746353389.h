﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_C1586116283.h"

// System.ServiceModel.Channels.ChannelListenerBase
struct ChannelListenerBase_t990592377;
// System.ServiceModel.Channels.ReplyChannelBase/TryReceiveDelegate
struct TryReceiveDelegate_t2389535752;
// System.Threading.Thread
struct Thread_t2300836069;
// System.IAsyncResult
struct IAsyncResult_t767004451;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.ReplyChannelBase
struct  ReplyChannelBase_t2746353389  : public ChannelBase_t1586116283
{
public:
	// System.ServiceModel.Channels.ChannelListenerBase System.ServiceModel.Channels.ReplyChannelBase::listener
	ChannelListenerBase_t990592377 * ___listener_10;
	// System.ServiceModel.Channels.ReplyChannelBase/TryReceiveDelegate System.ServiceModel.Channels.ReplyChannelBase::try_recv_delegate
	TryReceiveDelegate_t2389535752 * ___try_recv_delegate_11;
	// System.Threading.Thread System.ServiceModel.Channels.ReplyChannelBase::<CurrentAsyncThread>k__BackingField
	Thread_t2300836069 * ___U3CCurrentAsyncThreadU3Ek__BackingField_12;
	// System.IAsyncResult System.ServiceModel.Channels.ReplyChannelBase::<CurrentAsyncResult>k__BackingField
	Il2CppObject * ___U3CCurrentAsyncResultU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_listener_10() { return static_cast<int32_t>(offsetof(ReplyChannelBase_t2746353389, ___listener_10)); }
	inline ChannelListenerBase_t990592377 * get_listener_10() const { return ___listener_10; }
	inline ChannelListenerBase_t990592377 ** get_address_of_listener_10() { return &___listener_10; }
	inline void set_listener_10(ChannelListenerBase_t990592377 * value)
	{
		___listener_10 = value;
		Il2CppCodeGenWriteBarrier(&___listener_10, value);
	}

	inline static int32_t get_offset_of_try_recv_delegate_11() { return static_cast<int32_t>(offsetof(ReplyChannelBase_t2746353389, ___try_recv_delegate_11)); }
	inline TryReceiveDelegate_t2389535752 * get_try_recv_delegate_11() const { return ___try_recv_delegate_11; }
	inline TryReceiveDelegate_t2389535752 ** get_address_of_try_recv_delegate_11() { return &___try_recv_delegate_11; }
	inline void set_try_recv_delegate_11(TryReceiveDelegate_t2389535752 * value)
	{
		___try_recv_delegate_11 = value;
		Il2CppCodeGenWriteBarrier(&___try_recv_delegate_11, value);
	}

	inline static int32_t get_offset_of_U3CCurrentAsyncThreadU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(ReplyChannelBase_t2746353389, ___U3CCurrentAsyncThreadU3Ek__BackingField_12)); }
	inline Thread_t2300836069 * get_U3CCurrentAsyncThreadU3Ek__BackingField_12() const { return ___U3CCurrentAsyncThreadU3Ek__BackingField_12; }
	inline Thread_t2300836069 ** get_address_of_U3CCurrentAsyncThreadU3Ek__BackingField_12() { return &___U3CCurrentAsyncThreadU3Ek__BackingField_12; }
	inline void set_U3CCurrentAsyncThreadU3Ek__BackingField_12(Thread_t2300836069 * value)
	{
		___U3CCurrentAsyncThreadU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCurrentAsyncThreadU3Ek__BackingField_12, value);
	}

	inline static int32_t get_offset_of_U3CCurrentAsyncResultU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ReplyChannelBase_t2746353389, ___U3CCurrentAsyncResultU3Ek__BackingField_13)); }
	inline Il2CppObject * get_U3CCurrentAsyncResultU3Ek__BackingField_13() const { return ___U3CCurrentAsyncResultU3Ek__BackingField_13; }
	inline Il2CppObject ** get_address_of_U3CCurrentAsyncResultU3Ek__BackingField_13() { return &___U3CCurrentAsyncResultU3Ek__BackingField_13; }
	inline void set_U3CCurrentAsyncResultU3Ek__BackingField_13(Il2CppObject * value)
	{
		___U3CCurrentAsyncResultU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCurrentAsyncResultU3Ek__BackingField_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
