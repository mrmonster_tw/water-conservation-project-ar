﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Web_Services_System_Web_Services_Description992977419.h"
#include "System_Xml_System_Xml_Serialization_CodeGeneration2228734478.h"

// System.String
struct String_t;
// System.Xml.Serialization.XmlSchemas
struct XmlSchemas_t3283371924;
// System.Web.Services.Description.ServiceDescriptionCollection
struct ServiceDescriptionCollection_t1577359074;
// System.CodeDom.Compiler.CodeDomProvider
struct CodeDomProvider_t110349836;
// System.Xml.Serialization.ImportContext
struct ImportContext_t1801135953;
// System.Collections.ArrayList
struct ArrayList_t2718874744;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.ServiceDescriptionImporter
struct  ServiceDescriptionImporter_t2272364891  : public Il2CppObject
{
public:
	// System.String System.Web.Services.Description.ServiceDescriptionImporter::protocolName
	String_t* ___protocolName_0;
	// System.Xml.Serialization.XmlSchemas System.Web.Services.Description.ServiceDescriptionImporter::schemas
	XmlSchemas_t3283371924 * ___schemas_1;
	// System.Web.Services.Description.ServiceDescriptionCollection System.Web.Services.Description.ServiceDescriptionImporter::serviceDescriptions
	ServiceDescriptionCollection_t1577359074 * ___serviceDescriptions_2;
	// System.Web.Services.Description.ServiceDescriptionImportStyle System.Web.Services.Description.ServiceDescriptionImporter::style
	int32_t ___style_3;
	// System.Xml.Serialization.CodeGenerationOptions System.Web.Services.Description.ServiceDescriptionImporter::options
	int32_t ___options_4;
	// System.CodeDom.Compiler.CodeDomProvider System.Web.Services.Description.ServiceDescriptionImporter::codeGenerator
	CodeDomProvider_t110349836 * ___codeGenerator_5;
	// System.Xml.Serialization.ImportContext System.Web.Services.Description.ServiceDescriptionImporter::context
	ImportContext_t1801135953 * ___context_6;
	// System.Collections.ArrayList System.Web.Services.Description.ServiceDescriptionImporter::importInfo
	ArrayList_t2718874744 * ___importInfo_7;

public:
	inline static int32_t get_offset_of_protocolName_0() { return static_cast<int32_t>(offsetof(ServiceDescriptionImporter_t2272364891, ___protocolName_0)); }
	inline String_t* get_protocolName_0() const { return ___protocolName_0; }
	inline String_t** get_address_of_protocolName_0() { return &___protocolName_0; }
	inline void set_protocolName_0(String_t* value)
	{
		___protocolName_0 = value;
		Il2CppCodeGenWriteBarrier(&___protocolName_0, value);
	}

	inline static int32_t get_offset_of_schemas_1() { return static_cast<int32_t>(offsetof(ServiceDescriptionImporter_t2272364891, ___schemas_1)); }
	inline XmlSchemas_t3283371924 * get_schemas_1() const { return ___schemas_1; }
	inline XmlSchemas_t3283371924 ** get_address_of_schemas_1() { return &___schemas_1; }
	inline void set_schemas_1(XmlSchemas_t3283371924 * value)
	{
		___schemas_1 = value;
		Il2CppCodeGenWriteBarrier(&___schemas_1, value);
	}

	inline static int32_t get_offset_of_serviceDescriptions_2() { return static_cast<int32_t>(offsetof(ServiceDescriptionImporter_t2272364891, ___serviceDescriptions_2)); }
	inline ServiceDescriptionCollection_t1577359074 * get_serviceDescriptions_2() const { return ___serviceDescriptions_2; }
	inline ServiceDescriptionCollection_t1577359074 ** get_address_of_serviceDescriptions_2() { return &___serviceDescriptions_2; }
	inline void set_serviceDescriptions_2(ServiceDescriptionCollection_t1577359074 * value)
	{
		___serviceDescriptions_2 = value;
		Il2CppCodeGenWriteBarrier(&___serviceDescriptions_2, value);
	}

	inline static int32_t get_offset_of_style_3() { return static_cast<int32_t>(offsetof(ServiceDescriptionImporter_t2272364891, ___style_3)); }
	inline int32_t get_style_3() const { return ___style_3; }
	inline int32_t* get_address_of_style_3() { return &___style_3; }
	inline void set_style_3(int32_t value)
	{
		___style_3 = value;
	}

	inline static int32_t get_offset_of_options_4() { return static_cast<int32_t>(offsetof(ServiceDescriptionImporter_t2272364891, ___options_4)); }
	inline int32_t get_options_4() const { return ___options_4; }
	inline int32_t* get_address_of_options_4() { return &___options_4; }
	inline void set_options_4(int32_t value)
	{
		___options_4 = value;
	}

	inline static int32_t get_offset_of_codeGenerator_5() { return static_cast<int32_t>(offsetof(ServiceDescriptionImporter_t2272364891, ___codeGenerator_5)); }
	inline CodeDomProvider_t110349836 * get_codeGenerator_5() const { return ___codeGenerator_5; }
	inline CodeDomProvider_t110349836 ** get_address_of_codeGenerator_5() { return &___codeGenerator_5; }
	inline void set_codeGenerator_5(CodeDomProvider_t110349836 * value)
	{
		___codeGenerator_5 = value;
		Il2CppCodeGenWriteBarrier(&___codeGenerator_5, value);
	}

	inline static int32_t get_offset_of_context_6() { return static_cast<int32_t>(offsetof(ServiceDescriptionImporter_t2272364891, ___context_6)); }
	inline ImportContext_t1801135953 * get_context_6() const { return ___context_6; }
	inline ImportContext_t1801135953 ** get_address_of_context_6() { return &___context_6; }
	inline void set_context_6(ImportContext_t1801135953 * value)
	{
		___context_6 = value;
		Il2CppCodeGenWriteBarrier(&___context_6, value);
	}

	inline static int32_t get_offset_of_importInfo_7() { return static_cast<int32_t>(offsetof(ServiceDescriptionImporter_t2272364891, ___importInfo_7)); }
	inline ArrayList_t2718874744 * get_importInfo_7() const { return ___importInfo_7; }
	inline ArrayList_t2718874744 ** get_address_of_importInfo_7() { return &___importInfo_7; }
	inline void set_importInfo_7(ArrayList_t2718874744 * value)
	{
		___importInfo_7 = value;
		Il2CppCodeGenWriteBarrier(&___importInfo_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
