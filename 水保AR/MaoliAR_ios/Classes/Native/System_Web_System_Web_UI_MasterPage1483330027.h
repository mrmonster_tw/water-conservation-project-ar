﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_UserControl1418173744.h"

// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.String
struct String_t;
// System.Web.UI.MasterPage
struct MasterPage_t1483330027;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.MasterPage
struct  MasterPage_t1483330027  : public UserControl_t1418173744
{
public:
	// System.Collections.Hashtable System.Web.UI.MasterPage::definedContentTemplates
	Hashtable_t1853889766 * ___definedContentTemplates_39;
	// System.Collections.Hashtable System.Web.UI.MasterPage::templates
	Hashtable_t1853889766 * ___templates_40;
	// System.Collections.Generic.List`1<System.String> System.Web.UI.MasterPage::placeholders
	List_1_t3319525431 * ___placeholders_41;
	// System.String System.Web.UI.MasterPage::parentMasterPageFile
	String_t* ___parentMasterPageFile_42;
	// System.Web.UI.MasterPage System.Web.UI.MasterPage::parentMasterPage
	MasterPage_t1483330027 * ___parentMasterPage_43;

public:
	inline static int32_t get_offset_of_definedContentTemplates_39() { return static_cast<int32_t>(offsetof(MasterPage_t1483330027, ___definedContentTemplates_39)); }
	inline Hashtable_t1853889766 * get_definedContentTemplates_39() const { return ___definedContentTemplates_39; }
	inline Hashtable_t1853889766 ** get_address_of_definedContentTemplates_39() { return &___definedContentTemplates_39; }
	inline void set_definedContentTemplates_39(Hashtable_t1853889766 * value)
	{
		___definedContentTemplates_39 = value;
		Il2CppCodeGenWriteBarrier(&___definedContentTemplates_39, value);
	}

	inline static int32_t get_offset_of_templates_40() { return static_cast<int32_t>(offsetof(MasterPage_t1483330027, ___templates_40)); }
	inline Hashtable_t1853889766 * get_templates_40() const { return ___templates_40; }
	inline Hashtable_t1853889766 ** get_address_of_templates_40() { return &___templates_40; }
	inline void set_templates_40(Hashtable_t1853889766 * value)
	{
		___templates_40 = value;
		Il2CppCodeGenWriteBarrier(&___templates_40, value);
	}

	inline static int32_t get_offset_of_placeholders_41() { return static_cast<int32_t>(offsetof(MasterPage_t1483330027, ___placeholders_41)); }
	inline List_1_t3319525431 * get_placeholders_41() const { return ___placeholders_41; }
	inline List_1_t3319525431 ** get_address_of_placeholders_41() { return &___placeholders_41; }
	inline void set_placeholders_41(List_1_t3319525431 * value)
	{
		___placeholders_41 = value;
		Il2CppCodeGenWriteBarrier(&___placeholders_41, value);
	}

	inline static int32_t get_offset_of_parentMasterPageFile_42() { return static_cast<int32_t>(offsetof(MasterPage_t1483330027, ___parentMasterPageFile_42)); }
	inline String_t* get_parentMasterPageFile_42() const { return ___parentMasterPageFile_42; }
	inline String_t** get_address_of_parentMasterPageFile_42() { return &___parentMasterPageFile_42; }
	inline void set_parentMasterPageFile_42(String_t* value)
	{
		___parentMasterPageFile_42 = value;
		Il2CppCodeGenWriteBarrier(&___parentMasterPageFile_42, value);
	}

	inline static int32_t get_offset_of_parentMasterPage_43() { return static_cast<int32_t>(offsetof(MasterPage_t1483330027, ___parentMasterPage_43)); }
	inline MasterPage_t1483330027 * get_parentMasterPage_43() const { return ___parentMasterPage_43; }
	inline MasterPage_t1483330027 ** get_address_of_parentMasterPage_43() { return &___parentMasterPage_43; }
	inline void set_parentMasterPage_43(MasterPage_t1483330027 * value)
	{
		___parentMasterPage_43 = value;
		Il2CppCodeGenWriteBarrier(&___parentMasterPage_43, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
