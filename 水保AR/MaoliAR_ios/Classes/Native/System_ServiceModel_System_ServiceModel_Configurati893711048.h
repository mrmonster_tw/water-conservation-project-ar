﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.PeerCustomResolverElement
struct  PeerCustomResolverElement_t893711048  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct PeerCustomResolverElement_t893711048_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.PeerCustomResolverElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.PeerCustomResolverElement::address
	ConfigurationProperty_t3590861854 * ___address_14;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.PeerCustomResolverElement::binding
	ConfigurationProperty_t3590861854 * ___binding_15;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.PeerCustomResolverElement::binding_configuration
	ConfigurationProperty_t3590861854 * ___binding_configuration_16;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.PeerCustomResolverElement::headers
	ConfigurationProperty_t3590861854 * ___headers_17;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.PeerCustomResolverElement::identity
	ConfigurationProperty_t3590861854 * ___identity_18;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.PeerCustomResolverElement::resolver_type
	ConfigurationProperty_t3590861854 * ___resolver_type_19;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(PeerCustomResolverElement_t893711048_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier(&___properties_13, value);
	}

	inline static int32_t get_offset_of_address_14() { return static_cast<int32_t>(offsetof(PeerCustomResolverElement_t893711048_StaticFields, ___address_14)); }
	inline ConfigurationProperty_t3590861854 * get_address_14() const { return ___address_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_address_14() { return &___address_14; }
	inline void set_address_14(ConfigurationProperty_t3590861854 * value)
	{
		___address_14 = value;
		Il2CppCodeGenWriteBarrier(&___address_14, value);
	}

	inline static int32_t get_offset_of_binding_15() { return static_cast<int32_t>(offsetof(PeerCustomResolverElement_t893711048_StaticFields, ___binding_15)); }
	inline ConfigurationProperty_t3590861854 * get_binding_15() const { return ___binding_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_binding_15() { return &___binding_15; }
	inline void set_binding_15(ConfigurationProperty_t3590861854 * value)
	{
		___binding_15 = value;
		Il2CppCodeGenWriteBarrier(&___binding_15, value);
	}

	inline static int32_t get_offset_of_binding_configuration_16() { return static_cast<int32_t>(offsetof(PeerCustomResolverElement_t893711048_StaticFields, ___binding_configuration_16)); }
	inline ConfigurationProperty_t3590861854 * get_binding_configuration_16() const { return ___binding_configuration_16; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_binding_configuration_16() { return &___binding_configuration_16; }
	inline void set_binding_configuration_16(ConfigurationProperty_t3590861854 * value)
	{
		___binding_configuration_16 = value;
		Il2CppCodeGenWriteBarrier(&___binding_configuration_16, value);
	}

	inline static int32_t get_offset_of_headers_17() { return static_cast<int32_t>(offsetof(PeerCustomResolverElement_t893711048_StaticFields, ___headers_17)); }
	inline ConfigurationProperty_t3590861854 * get_headers_17() const { return ___headers_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_headers_17() { return &___headers_17; }
	inline void set_headers_17(ConfigurationProperty_t3590861854 * value)
	{
		___headers_17 = value;
		Il2CppCodeGenWriteBarrier(&___headers_17, value);
	}

	inline static int32_t get_offset_of_identity_18() { return static_cast<int32_t>(offsetof(PeerCustomResolverElement_t893711048_StaticFields, ___identity_18)); }
	inline ConfigurationProperty_t3590861854 * get_identity_18() const { return ___identity_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_identity_18() { return &___identity_18; }
	inline void set_identity_18(ConfigurationProperty_t3590861854 * value)
	{
		___identity_18 = value;
		Il2CppCodeGenWriteBarrier(&___identity_18, value);
	}

	inline static int32_t get_offset_of_resolver_type_19() { return static_cast<int32_t>(offsetof(PeerCustomResolverElement_t893711048_StaticFields, ___resolver_type_19)); }
	inline ConfigurationProperty_t3590861854 * get_resolver_type_19() const { return ___resolver_type_19; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_resolver_type_19() { return &___resolver_type_19; }
	inline void set_resolver_type_19(ConfigurationProperty_t3590861854 * value)
	{
		___resolver_type_19 = value;
		Il2CppCodeGenWriteBarrier(&___resolver_type_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
