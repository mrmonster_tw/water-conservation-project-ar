﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.ServiceHostBase
struct ServiceHostBase_t3741910535;
// System.Collections.Generic.Dictionary`2<System.Uri,System.ServiceModel.Dispatcher.ChannelDispatcher>
struct Dictionary_2_t1958020555;
// System.ServiceModel.Description.HttpGetWsdl
struct HttpGetWsdl_t3793260870;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.ServiceMetadataExtension
struct  ServiceMetadataExtension_t3543702878  : public Il2CppObject
{
public:
	// System.ServiceModel.ServiceHostBase System.ServiceModel.Description.ServiceMetadataExtension::owner
	ServiceHostBase_t3741910535 * ___owner_0;
	// System.Collections.Generic.Dictionary`2<System.Uri,System.ServiceModel.Dispatcher.ChannelDispatcher> System.ServiceModel.Description.ServiceMetadataExtension::dispatchers
	Dictionary_2_t1958020555 * ___dispatchers_1;
	// System.ServiceModel.Description.HttpGetWsdl System.ServiceModel.Description.ServiceMetadataExtension::instance
	HttpGetWsdl_t3793260870 * ___instance_2;

public:
	inline static int32_t get_offset_of_owner_0() { return static_cast<int32_t>(offsetof(ServiceMetadataExtension_t3543702878, ___owner_0)); }
	inline ServiceHostBase_t3741910535 * get_owner_0() const { return ___owner_0; }
	inline ServiceHostBase_t3741910535 ** get_address_of_owner_0() { return &___owner_0; }
	inline void set_owner_0(ServiceHostBase_t3741910535 * value)
	{
		___owner_0 = value;
		Il2CppCodeGenWriteBarrier(&___owner_0, value);
	}

	inline static int32_t get_offset_of_dispatchers_1() { return static_cast<int32_t>(offsetof(ServiceMetadataExtension_t3543702878, ___dispatchers_1)); }
	inline Dictionary_2_t1958020555 * get_dispatchers_1() const { return ___dispatchers_1; }
	inline Dictionary_2_t1958020555 ** get_address_of_dispatchers_1() { return &___dispatchers_1; }
	inline void set_dispatchers_1(Dictionary_2_t1958020555 * value)
	{
		___dispatchers_1 = value;
		Il2CppCodeGenWriteBarrier(&___dispatchers_1, value);
	}

	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(ServiceMetadataExtension_t3543702878, ___instance_2)); }
	inline HttpGetWsdl_t3793260870 * get_instance_2() const { return ___instance_2; }
	inline HttpGetWsdl_t3793260870 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(HttpGetWsdl_t3793260870 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

struct ServiceMetadataExtension_t3543702878_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Description.ServiceMetadataExtension::<>f__switch$map15
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map15_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map15_3() { return static_cast<int32_t>(offsetof(ServiceMetadataExtension_t3543702878_StaticFields, ___U3CU3Ef__switchU24map15_3)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map15_3() const { return ___U3CU3Ef__switchU24map15_3; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map15_3() { return &___U3CU3Ef__switchU24map15_3; }
	inline void set_U3CU3Ef__switchU24map15_3(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map15_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map15_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
