﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_WSHttpBind2163194083.h"

// System.ServiceModel.WSFederationHttpSecurity
struct WSFederationHttpSecurity_t3296238232;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.WSFederationHttpBinding
struct  WSFederationHttpBinding_t1523044440  : public WSHttpBindingBase_t2163194083
{
public:
	// System.ServiceModel.WSFederationHttpSecurity System.ServiceModel.WSFederationHttpBinding::security
	WSFederationHttpSecurity_t3296238232 * ___security_9;

public:
	inline static int32_t get_offset_of_security_9() { return static_cast<int32_t>(offsetof(WSFederationHttpBinding_t1523044440, ___security_9)); }
	inline WSFederationHttpSecurity_t3296238232 * get_security_9() const { return ___security_9; }
	inline WSFederationHttpSecurity_t3296238232 ** get_address_of_security_9() { return &___security_9; }
	inline void set_security_9(WSFederationHttpSecurity_t3296238232 * value)
	{
		___security_9 = value;
		Il2CppCodeGenWriteBarrier(&___security_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
