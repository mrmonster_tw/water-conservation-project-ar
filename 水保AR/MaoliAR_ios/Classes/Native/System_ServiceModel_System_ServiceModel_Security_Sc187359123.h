﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Generic.Dictionary`2<System.String,System.ServiceModel.Security.MessagePartSpecification>
struct Dictionary_2_t2154131446;
// System.ServiceModel.Security.MessagePartSpecification
struct MessagePartSpecification_t2368875147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.ScopedMessagePartSpecification
struct  ScopedMessagePartSpecification_t187359123  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.ServiceModel.Security.MessagePartSpecification> System.ServiceModel.Security.ScopedMessagePartSpecification::table
	Dictionary_2_t2154131446 * ___table_0;
	// System.ServiceModel.Security.MessagePartSpecification System.ServiceModel.Security.ScopedMessagePartSpecification::parts
	MessagePartSpecification_t2368875147 * ___parts_1;
	// System.Boolean System.ServiceModel.Security.ScopedMessagePartSpecification::is_readonly
	bool ___is_readonly_2;

public:
	inline static int32_t get_offset_of_table_0() { return static_cast<int32_t>(offsetof(ScopedMessagePartSpecification_t187359123, ___table_0)); }
	inline Dictionary_2_t2154131446 * get_table_0() const { return ___table_0; }
	inline Dictionary_2_t2154131446 ** get_address_of_table_0() { return &___table_0; }
	inline void set_table_0(Dictionary_2_t2154131446 * value)
	{
		___table_0 = value;
		Il2CppCodeGenWriteBarrier(&___table_0, value);
	}

	inline static int32_t get_offset_of_parts_1() { return static_cast<int32_t>(offsetof(ScopedMessagePartSpecification_t187359123, ___parts_1)); }
	inline MessagePartSpecification_t2368875147 * get_parts_1() const { return ___parts_1; }
	inline MessagePartSpecification_t2368875147 ** get_address_of_parts_1() { return &___parts_1; }
	inline void set_parts_1(MessagePartSpecification_t2368875147 * value)
	{
		___parts_1 = value;
		Il2CppCodeGenWriteBarrier(&___parts_1, value);
	}

	inline static int32_t get_offset_of_is_readonly_2() { return static_cast<int32_t>(offsetof(ScopedMessagePartSpecification_t187359123, ___is_readonly_2)); }
	inline bool get_is_readonly_2() const { return ___is_readonly_2; }
	inline bool* get_address_of_is_readonly_2() { return &___is_readonly_2; }
	inline void set_is_readonly_2(bool value)
	{
		___is_readonly_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
