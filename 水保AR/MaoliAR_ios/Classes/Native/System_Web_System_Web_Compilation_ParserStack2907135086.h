﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Collections.Stack
struct Stack_t2329662280;
// System.Web.Compilation.AspParser
struct AspParser_t4001025751;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.ParserStack
struct  ParserStack_t2907135086  : public Il2CppObject
{
public:
	// System.Collections.Hashtable System.Web.Compilation.ParserStack::files
	Hashtable_t1853889766 * ___files_0;
	// System.Collections.Stack System.Web.Compilation.ParserStack::parsers
	Stack_t2329662280 * ___parsers_1;
	// System.Web.Compilation.AspParser System.Web.Compilation.ParserStack::current
	AspParser_t4001025751 * ___current_2;

public:
	inline static int32_t get_offset_of_files_0() { return static_cast<int32_t>(offsetof(ParserStack_t2907135086, ___files_0)); }
	inline Hashtable_t1853889766 * get_files_0() const { return ___files_0; }
	inline Hashtable_t1853889766 ** get_address_of_files_0() { return &___files_0; }
	inline void set_files_0(Hashtable_t1853889766 * value)
	{
		___files_0 = value;
		Il2CppCodeGenWriteBarrier(&___files_0, value);
	}

	inline static int32_t get_offset_of_parsers_1() { return static_cast<int32_t>(offsetof(ParserStack_t2907135086, ___parsers_1)); }
	inline Stack_t2329662280 * get_parsers_1() const { return ___parsers_1; }
	inline Stack_t2329662280 ** get_address_of_parsers_1() { return &___parsers_1; }
	inline void set_parsers_1(Stack_t2329662280 * value)
	{
		___parsers_1 = value;
		Il2CppCodeGenWriteBarrier(&___parsers_1, value);
	}

	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(ParserStack_t2907135086, ___current_2)); }
	inline AspParser_t4001025751 * get_current_2() const { return ___current_2; }
	inline AspParser_t4001025751 ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(AspParser_t4001025751 * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier(&___current_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
