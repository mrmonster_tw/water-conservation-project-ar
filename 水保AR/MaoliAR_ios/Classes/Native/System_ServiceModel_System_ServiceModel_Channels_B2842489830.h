﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_System_ServiceModel_Descriptio1965852526.h"

// System.ServiceModel.Channels.BindingElementCollection
struct BindingElementCollection_t2456870521;
// System.ServiceModel.Channels.CustomBinding
struct CustomBinding_t4187704225;
// System.ServiceModel.Channels.BindingParameterCollection
struct BindingParameterCollection_t3413021685;
// System.Uri
struct Uri_t100236324;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.BindingContext
struct  BindingContext_t2842489830  : public Il2CppObject
{
public:
	// System.ServiceModel.Channels.CustomBinding System.ServiceModel.Channels.BindingContext::binding
	CustomBinding_t4187704225 * ___binding_1;
	// System.ServiceModel.Channels.BindingParameterCollection System.ServiceModel.Channels.BindingContext::parameters
	BindingParameterCollection_t3413021685 * ___parameters_2;
	// System.ServiceModel.Channels.BindingElementCollection System.ServiceModel.Channels.BindingContext::elements
	BindingElementCollection_t2456870521 * ___elements_3;
	// System.ServiceModel.Channels.BindingElementCollection System.ServiceModel.Channels.BindingContext::remaining_elements
	BindingElementCollection_t2456870521 * ___remaining_elements_4;
	// System.Uri System.ServiceModel.Channels.BindingContext::listen_uri_base
	Uri_t100236324 * ___listen_uri_base_5;
	// System.String System.ServiceModel.Channels.BindingContext::listen_uri_relative
	String_t* ___listen_uri_relative_6;
	// System.ServiceModel.Description.ListenUriMode System.ServiceModel.Channels.BindingContext::listen_uri_mode
	int32_t ___listen_uri_mode_7;

public:
	inline static int32_t get_offset_of_binding_1() { return static_cast<int32_t>(offsetof(BindingContext_t2842489830, ___binding_1)); }
	inline CustomBinding_t4187704225 * get_binding_1() const { return ___binding_1; }
	inline CustomBinding_t4187704225 ** get_address_of_binding_1() { return &___binding_1; }
	inline void set_binding_1(CustomBinding_t4187704225 * value)
	{
		___binding_1 = value;
		Il2CppCodeGenWriteBarrier(&___binding_1, value);
	}

	inline static int32_t get_offset_of_parameters_2() { return static_cast<int32_t>(offsetof(BindingContext_t2842489830, ___parameters_2)); }
	inline BindingParameterCollection_t3413021685 * get_parameters_2() const { return ___parameters_2; }
	inline BindingParameterCollection_t3413021685 ** get_address_of_parameters_2() { return &___parameters_2; }
	inline void set_parameters_2(BindingParameterCollection_t3413021685 * value)
	{
		___parameters_2 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_2, value);
	}

	inline static int32_t get_offset_of_elements_3() { return static_cast<int32_t>(offsetof(BindingContext_t2842489830, ___elements_3)); }
	inline BindingElementCollection_t2456870521 * get_elements_3() const { return ___elements_3; }
	inline BindingElementCollection_t2456870521 ** get_address_of_elements_3() { return &___elements_3; }
	inline void set_elements_3(BindingElementCollection_t2456870521 * value)
	{
		___elements_3 = value;
		Il2CppCodeGenWriteBarrier(&___elements_3, value);
	}

	inline static int32_t get_offset_of_remaining_elements_4() { return static_cast<int32_t>(offsetof(BindingContext_t2842489830, ___remaining_elements_4)); }
	inline BindingElementCollection_t2456870521 * get_remaining_elements_4() const { return ___remaining_elements_4; }
	inline BindingElementCollection_t2456870521 ** get_address_of_remaining_elements_4() { return &___remaining_elements_4; }
	inline void set_remaining_elements_4(BindingElementCollection_t2456870521 * value)
	{
		___remaining_elements_4 = value;
		Il2CppCodeGenWriteBarrier(&___remaining_elements_4, value);
	}

	inline static int32_t get_offset_of_listen_uri_base_5() { return static_cast<int32_t>(offsetof(BindingContext_t2842489830, ___listen_uri_base_5)); }
	inline Uri_t100236324 * get_listen_uri_base_5() const { return ___listen_uri_base_5; }
	inline Uri_t100236324 ** get_address_of_listen_uri_base_5() { return &___listen_uri_base_5; }
	inline void set_listen_uri_base_5(Uri_t100236324 * value)
	{
		___listen_uri_base_5 = value;
		Il2CppCodeGenWriteBarrier(&___listen_uri_base_5, value);
	}

	inline static int32_t get_offset_of_listen_uri_relative_6() { return static_cast<int32_t>(offsetof(BindingContext_t2842489830, ___listen_uri_relative_6)); }
	inline String_t* get_listen_uri_relative_6() const { return ___listen_uri_relative_6; }
	inline String_t** get_address_of_listen_uri_relative_6() { return &___listen_uri_relative_6; }
	inline void set_listen_uri_relative_6(String_t* value)
	{
		___listen_uri_relative_6 = value;
		Il2CppCodeGenWriteBarrier(&___listen_uri_relative_6, value);
	}

	inline static int32_t get_offset_of_listen_uri_mode_7() { return static_cast<int32_t>(offsetof(BindingContext_t2842489830, ___listen_uri_mode_7)); }
	inline int32_t get_listen_uri_mode_7() const { return ___listen_uri_mode_7; }
	inline int32_t* get_address_of_listen_uri_mode_7() { return &___listen_uri_mode_7; }
	inline void set_listen_uri_mode_7(int32_t value)
	{
		___listen_uri_mode_7 = value;
	}
};

struct BindingContext_t2842489830_StaticFields
{
public:
	// System.ServiceModel.Channels.BindingElementCollection System.ServiceModel.Channels.BindingContext::empty_collection
	BindingElementCollection_t2456870521 * ___empty_collection_0;

public:
	inline static int32_t get_offset_of_empty_collection_0() { return static_cast<int32_t>(offsetof(BindingContext_t2842489830_StaticFields, ___empty_collection_0)); }
	inline BindingElementCollection_t2456870521 * get_empty_collection_0() const { return ___empty_collection_0; }
	inline BindingElementCollection_t2456870521 ** get_address_of_empty_collection_0() { return &___empty_collection_0; }
	inline void set_empty_collection_0(BindingElementCollection_t2456870521 * value)
	{
		___empty_collection_0 = value;
		Il2CppCodeGenWriteBarrier(&___empty_collection_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
