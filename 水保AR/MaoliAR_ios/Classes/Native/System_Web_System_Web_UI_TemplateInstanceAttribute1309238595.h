﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"
#include "System_Web_System_Web_UI_TemplateInstance2414853837.h"

// System.Web.UI.TemplateInstanceAttribute
struct TemplateInstanceAttribute_t1309238595;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.TemplateInstanceAttribute
struct  TemplateInstanceAttribute_t1309238595  : public Attribute_t861562559
{
public:
	// System.Web.UI.TemplateInstance System.Web.UI.TemplateInstanceAttribute::_instance
	int32_t ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(TemplateInstanceAttribute_t1309238595, ____instance_0)); }
	inline int32_t get__instance_0() const { return ____instance_0; }
	inline int32_t* get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(int32_t value)
	{
		____instance_0 = value;
	}
};

struct TemplateInstanceAttribute_t1309238595_StaticFields
{
public:
	// System.Web.UI.TemplateInstanceAttribute System.Web.UI.TemplateInstanceAttribute::Single
	TemplateInstanceAttribute_t1309238595 * ___Single_1;
	// System.Web.UI.TemplateInstanceAttribute System.Web.UI.TemplateInstanceAttribute::Multiple
	TemplateInstanceAttribute_t1309238595 * ___Multiple_2;
	// System.Web.UI.TemplateInstanceAttribute System.Web.UI.TemplateInstanceAttribute::Default
	TemplateInstanceAttribute_t1309238595 * ___Default_3;

public:
	inline static int32_t get_offset_of_Single_1() { return static_cast<int32_t>(offsetof(TemplateInstanceAttribute_t1309238595_StaticFields, ___Single_1)); }
	inline TemplateInstanceAttribute_t1309238595 * get_Single_1() const { return ___Single_1; }
	inline TemplateInstanceAttribute_t1309238595 ** get_address_of_Single_1() { return &___Single_1; }
	inline void set_Single_1(TemplateInstanceAttribute_t1309238595 * value)
	{
		___Single_1 = value;
		Il2CppCodeGenWriteBarrier(&___Single_1, value);
	}

	inline static int32_t get_offset_of_Multiple_2() { return static_cast<int32_t>(offsetof(TemplateInstanceAttribute_t1309238595_StaticFields, ___Multiple_2)); }
	inline TemplateInstanceAttribute_t1309238595 * get_Multiple_2() const { return ___Multiple_2; }
	inline TemplateInstanceAttribute_t1309238595 ** get_address_of_Multiple_2() { return &___Multiple_2; }
	inline void set_Multiple_2(TemplateInstanceAttribute_t1309238595 * value)
	{
		___Multiple_2 = value;
		Il2CppCodeGenWriteBarrier(&___Multiple_2, value);
	}

	inline static int32_t get_offset_of_Default_3() { return static_cast<int32_t>(offsetof(TemplateInstanceAttribute_t1309238595_StaticFields, ___Default_3)); }
	inline TemplateInstanceAttribute_t1309238595 * get_Default_3() const { return ___Default_3; }
	inline TemplateInstanceAttribute_t1309238595 ** get_address_of_Default_3() { return &___Default_3; }
	inline void set_Default_3(TemplateInstanceAttribute_t1309238595 * value)
	{
		___Default_3 = value;
		Il2CppCodeGenWriteBarrier(&___Default_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
