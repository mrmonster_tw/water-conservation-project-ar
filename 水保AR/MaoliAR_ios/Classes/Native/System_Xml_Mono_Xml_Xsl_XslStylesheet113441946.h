﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t407452768;
// Mono.Xml.Xsl.XslTemplateTable
struct XslTemplateTable_t625046267;
// System.String
struct String_t;
// System.Xml.XmlQualifiedName[]
struct XmlQualifiedNameU5BU5D_t1471530361;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2760654312;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.XslStylesheet
struct  XslStylesheet_t113441946  : public Il2CppObject
{
public:
	// System.Collections.ArrayList Mono.Xml.Xsl.XslStylesheet::imports
	ArrayList_t2718874744 * ___imports_0;
	// System.Collections.Hashtable Mono.Xml.Xsl.XslStylesheet::spaceControls
	Hashtable_t1853889766 * ___spaceControls_1;
	// System.Collections.Specialized.NameValueCollection Mono.Xml.Xsl.XslStylesheet::namespaceAliases
	NameValueCollection_t407452768 * ___namespaceAliases_2;
	// System.Collections.Hashtable Mono.Xml.Xsl.XslStylesheet::parameters
	Hashtable_t1853889766 * ___parameters_3;
	// System.Collections.Hashtable Mono.Xml.Xsl.XslStylesheet::keys
	Hashtable_t1853889766 * ___keys_4;
	// System.Collections.Hashtable Mono.Xml.Xsl.XslStylesheet::variables
	Hashtable_t1853889766 * ___variables_5;
	// Mono.Xml.Xsl.XslTemplateTable Mono.Xml.Xsl.XslStylesheet::templates
	XslTemplateTable_t625046267 * ___templates_6;
	// System.String Mono.Xml.Xsl.XslStylesheet::baseURI
	String_t* ___baseURI_7;
	// System.String Mono.Xml.Xsl.XslStylesheet::version
	String_t* ___version_8;
	// System.Xml.XmlQualifiedName[] Mono.Xml.Xsl.XslStylesheet::extensionElementPrefixes
	XmlQualifiedNameU5BU5D_t1471530361* ___extensionElementPrefixes_9;
	// System.Xml.XmlQualifiedName[] Mono.Xml.Xsl.XslStylesheet::excludeResultPrefixes
	XmlQualifiedNameU5BU5D_t1471530361* ___excludeResultPrefixes_10;
	// System.Collections.ArrayList Mono.Xml.Xsl.XslStylesheet::stylesheetNamespaces
	ArrayList_t2718874744 * ___stylesheetNamespaces_11;
	// System.Collections.Hashtable Mono.Xml.Xsl.XslStylesheet::inProcessIncludes
	Hashtable_t1853889766 * ___inProcessIncludes_12;
	// System.Boolean Mono.Xml.Xsl.XslStylesheet::countedSpaceControlExistence
	bool ___countedSpaceControlExistence_13;
	// System.Boolean Mono.Xml.Xsl.XslStylesheet::cachedHasSpaceControls
	bool ___cachedHasSpaceControls_14;
	// System.Boolean Mono.Xml.Xsl.XslStylesheet::countedNamespaceAliases
	bool ___countedNamespaceAliases_16;
	// System.Boolean Mono.Xml.Xsl.XslStylesheet::cachedHasNamespaceAliases
	bool ___cachedHasNamespaceAliases_17;

public:
	inline static int32_t get_offset_of_imports_0() { return static_cast<int32_t>(offsetof(XslStylesheet_t113441946, ___imports_0)); }
	inline ArrayList_t2718874744 * get_imports_0() const { return ___imports_0; }
	inline ArrayList_t2718874744 ** get_address_of_imports_0() { return &___imports_0; }
	inline void set_imports_0(ArrayList_t2718874744 * value)
	{
		___imports_0 = value;
		Il2CppCodeGenWriteBarrier(&___imports_0, value);
	}

	inline static int32_t get_offset_of_spaceControls_1() { return static_cast<int32_t>(offsetof(XslStylesheet_t113441946, ___spaceControls_1)); }
	inline Hashtable_t1853889766 * get_spaceControls_1() const { return ___spaceControls_1; }
	inline Hashtable_t1853889766 ** get_address_of_spaceControls_1() { return &___spaceControls_1; }
	inline void set_spaceControls_1(Hashtable_t1853889766 * value)
	{
		___spaceControls_1 = value;
		Il2CppCodeGenWriteBarrier(&___spaceControls_1, value);
	}

	inline static int32_t get_offset_of_namespaceAliases_2() { return static_cast<int32_t>(offsetof(XslStylesheet_t113441946, ___namespaceAliases_2)); }
	inline NameValueCollection_t407452768 * get_namespaceAliases_2() const { return ___namespaceAliases_2; }
	inline NameValueCollection_t407452768 ** get_address_of_namespaceAliases_2() { return &___namespaceAliases_2; }
	inline void set_namespaceAliases_2(NameValueCollection_t407452768 * value)
	{
		___namespaceAliases_2 = value;
		Il2CppCodeGenWriteBarrier(&___namespaceAliases_2, value);
	}

	inline static int32_t get_offset_of_parameters_3() { return static_cast<int32_t>(offsetof(XslStylesheet_t113441946, ___parameters_3)); }
	inline Hashtable_t1853889766 * get_parameters_3() const { return ___parameters_3; }
	inline Hashtable_t1853889766 ** get_address_of_parameters_3() { return &___parameters_3; }
	inline void set_parameters_3(Hashtable_t1853889766 * value)
	{
		___parameters_3 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_3, value);
	}

	inline static int32_t get_offset_of_keys_4() { return static_cast<int32_t>(offsetof(XslStylesheet_t113441946, ___keys_4)); }
	inline Hashtable_t1853889766 * get_keys_4() const { return ___keys_4; }
	inline Hashtable_t1853889766 ** get_address_of_keys_4() { return &___keys_4; }
	inline void set_keys_4(Hashtable_t1853889766 * value)
	{
		___keys_4 = value;
		Il2CppCodeGenWriteBarrier(&___keys_4, value);
	}

	inline static int32_t get_offset_of_variables_5() { return static_cast<int32_t>(offsetof(XslStylesheet_t113441946, ___variables_5)); }
	inline Hashtable_t1853889766 * get_variables_5() const { return ___variables_5; }
	inline Hashtable_t1853889766 ** get_address_of_variables_5() { return &___variables_5; }
	inline void set_variables_5(Hashtable_t1853889766 * value)
	{
		___variables_5 = value;
		Il2CppCodeGenWriteBarrier(&___variables_5, value);
	}

	inline static int32_t get_offset_of_templates_6() { return static_cast<int32_t>(offsetof(XslStylesheet_t113441946, ___templates_6)); }
	inline XslTemplateTable_t625046267 * get_templates_6() const { return ___templates_6; }
	inline XslTemplateTable_t625046267 ** get_address_of_templates_6() { return &___templates_6; }
	inline void set_templates_6(XslTemplateTable_t625046267 * value)
	{
		___templates_6 = value;
		Il2CppCodeGenWriteBarrier(&___templates_6, value);
	}

	inline static int32_t get_offset_of_baseURI_7() { return static_cast<int32_t>(offsetof(XslStylesheet_t113441946, ___baseURI_7)); }
	inline String_t* get_baseURI_7() const { return ___baseURI_7; }
	inline String_t** get_address_of_baseURI_7() { return &___baseURI_7; }
	inline void set_baseURI_7(String_t* value)
	{
		___baseURI_7 = value;
		Il2CppCodeGenWriteBarrier(&___baseURI_7, value);
	}

	inline static int32_t get_offset_of_version_8() { return static_cast<int32_t>(offsetof(XslStylesheet_t113441946, ___version_8)); }
	inline String_t* get_version_8() const { return ___version_8; }
	inline String_t** get_address_of_version_8() { return &___version_8; }
	inline void set_version_8(String_t* value)
	{
		___version_8 = value;
		Il2CppCodeGenWriteBarrier(&___version_8, value);
	}

	inline static int32_t get_offset_of_extensionElementPrefixes_9() { return static_cast<int32_t>(offsetof(XslStylesheet_t113441946, ___extensionElementPrefixes_9)); }
	inline XmlQualifiedNameU5BU5D_t1471530361* get_extensionElementPrefixes_9() const { return ___extensionElementPrefixes_9; }
	inline XmlQualifiedNameU5BU5D_t1471530361** get_address_of_extensionElementPrefixes_9() { return &___extensionElementPrefixes_9; }
	inline void set_extensionElementPrefixes_9(XmlQualifiedNameU5BU5D_t1471530361* value)
	{
		___extensionElementPrefixes_9 = value;
		Il2CppCodeGenWriteBarrier(&___extensionElementPrefixes_9, value);
	}

	inline static int32_t get_offset_of_excludeResultPrefixes_10() { return static_cast<int32_t>(offsetof(XslStylesheet_t113441946, ___excludeResultPrefixes_10)); }
	inline XmlQualifiedNameU5BU5D_t1471530361* get_excludeResultPrefixes_10() const { return ___excludeResultPrefixes_10; }
	inline XmlQualifiedNameU5BU5D_t1471530361** get_address_of_excludeResultPrefixes_10() { return &___excludeResultPrefixes_10; }
	inline void set_excludeResultPrefixes_10(XmlQualifiedNameU5BU5D_t1471530361* value)
	{
		___excludeResultPrefixes_10 = value;
		Il2CppCodeGenWriteBarrier(&___excludeResultPrefixes_10, value);
	}

	inline static int32_t get_offset_of_stylesheetNamespaces_11() { return static_cast<int32_t>(offsetof(XslStylesheet_t113441946, ___stylesheetNamespaces_11)); }
	inline ArrayList_t2718874744 * get_stylesheetNamespaces_11() const { return ___stylesheetNamespaces_11; }
	inline ArrayList_t2718874744 ** get_address_of_stylesheetNamespaces_11() { return &___stylesheetNamespaces_11; }
	inline void set_stylesheetNamespaces_11(ArrayList_t2718874744 * value)
	{
		___stylesheetNamespaces_11 = value;
		Il2CppCodeGenWriteBarrier(&___stylesheetNamespaces_11, value);
	}

	inline static int32_t get_offset_of_inProcessIncludes_12() { return static_cast<int32_t>(offsetof(XslStylesheet_t113441946, ___inProcessIncludes_12)); }
	inline Hashtable_t1853889766 * get_inProcessIncludes_12() const { return ___inProcessIncludes_12; }
	inline Hashtable_t1853889766 ** get_address_of_inProcessIncludes_12() { return &___inProcessIncludes_12; }
	inline void set_inProcessIncludes_12(Hashtable_t1853889766 * value)
	{
		___inProcessIncludes_12 = value;
		Il2CppCodeGenWriteBarrier(&___inProcessIncludes_12, value);
	}

	inline static int32_t get_offset_of_countedSpaceControlExistence_13() { return static_cast<int32_t>(offsetof(XslStylesheet_t113441946, ___countedSpaceControlExistence_13)); }
	inline bool get_countedSpaceControlExistence_13() const { return ___countedSpaceControlExistence_13; }
	inline bool* get_address_of_countedSpaceControlExistence_13() { return &___countedSpaceControlExistence_13; }
	inline void set_countedSpaceControlExistence_13(bool value)
	{
		___countedSpaceControlExistence_13 = value;
	}

	inline static int32_t get_offset_of_cachedHasSpaceControls_14() { return static_cast<int32_t>(offsetof(XslStylesheet_t113441946, ___cachedHasSpaceControls_14)); }
	inline bool get_cachedHasSpaceControls_14() const { return ___cachedHasSpaceControls_14; }
	inline bool* get_address_of_cachedHasSpaceControls_14() { return &___cachedHasSpaceControls_14; }
	inline void set_cachedHasSpaceControls_14(bool value)
	{
		___cachedHasSpaceControls_14 = value;
	}

	inline static int32_t get_offset_of_countedNamespaceAliases_16() { return static_cast<int32_t>(offsetof(XslStylesheet_t113441946, ___countedNamespaceAliases_16)); }
	inline bool get_countedNamespaceAliases_16() const { return ___countedNamespaceAliases_16; }
	inline bool* get_address_of_countedNamespaceAliases_16() { return &___countedNamespaceAliases_16; }
	inline void set_countedNamespaceAliases_16(bool value)
	{
		___countedNamespaceAliases_16 = value;
	}

	inline static int32_t get_offset_of_cachedHasNamespaceAliases_17() { return static_cast<int32_t>(offsetof(XslStylesheet_t113441946, ___cachedHasNamespaceAliases_17)); }
	inline bool get_cachedHasNamespaceAliases_17() const { return ___cachedHasNamespaceAliases_17; }
	inline bool* get_address_of_cachedHasNamespaceAliases_17() { return &___cachedHasNamespaceAliases_17; }
	inline void set_cachedHasNamespaceAliases_17(bool value)
	{
		___cachedHasNamespaceAliases_17 = value;
	}
};

struct XslStylesheet_t113441946_StaticFields
{
public:
	// System.Xml.XmlQualifiedName Mono.Xml.Xsl.XslStylesheet::allMatchName
	XmlQualifiedName_t2760654312 * ___allMatchName_15;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.XslStylesheet::<>f__switch$map21
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map21_18;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.XslStylesheet::<>f__switch$map22
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map22_19;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.XslStylesheet::<>f__switch$map23
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map23_20;

public:
	inline static int32_t get_offset_of_allMatchName_15() { return static_cast<int32_t>(offsetof(XslStylesheet_t113441946_StaticFields, ___allMatchName_15)); }
	inline XmlQualifiedName_t2760654312 * get_allMatchName_15() const { return ___allMatchName_15; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_allMatchName_15() { return &___allMatchName_15; }
	inline void set_allMatchName_15(XmlQualifiedName_t2760654312 * value)
	{
		___allMatchName_15 = value;
		Il2CppCodeGenWriteBarrier(&___allMatchName_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map21_18() { return static_cast<int32_t>(offsetof(XslStylesheet_t113441946_StaticFields, ___U3CU3Ef__switchU24map21_18)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map21_18() const { return ___U3CU3Ef__switchU24map21_18; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map21_18() { return &___U3CU3Ef__switchU24map21_18; }
	inline void set_U3CU3Ef__switchU24map21_18(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map21_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map21_18, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map22_19() { return static_cast<int32_t>(offsetof(XslStylesheet_t113441946_StaticFields, ___U3CU3Ef__switchU24map22_19)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map22_19() const { return ___U3CU3Ef__switchU24map22_19; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map22_19() { return &___U3CU3Ef__switchU24map22_19; }
	inline void set_U3CU3Ef__switchU24map22_19(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map22_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map22_19, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map23_20() { return static_cast<int32_t>(offsetof(XslStylesheet_t113441946_StaticFields, ___U3CU3Ef__switchU24map23_20)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map23_20() const { return ___U3CU3Ef__switchU24map23_20; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map23_20() { return &___U3CU3Ef__switchU24map23_20; }
	inline void set_U3CU3Ef__switchU24map23_20(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map23_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map23_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
