﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_WebControls_WebControl2705811671.h"

// System.Web.UI.WebControls.TableCellCollection
struct TableCellCollection_t3482415303;
// System.Web.UI.WebControls.TableRowCollection
struct TableRowCollection_t3754565154;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.WebControls.TableRow
struct  TableRow_t1154691476  : public WebControl_t2705811671
{
public:
	// System.Web.UI.WebControls.TableCellCollection System.Web.UI.WebControls.TableRow::cells
	TableCellCollection_t3482415303 * ___cells_36;
	// System.Boolean System.Web.UI.WebControls.TableRow::tableRowSectionSet
	bool ___tableRowSectionSet_37;
	// System.Web.UI.WebControls.TableRowCollection System.Web.UI.WebControls.TableRow::<Container>k__BackingField
	TableRowCollection_t3754565154 * ___U3CContainerU3Ek__BackingField_38;

public:
	inline static int32_t get_offset_of_cells_36() { return static_cast<int32_t>(offsetof(TableRow_t1154691476, ___cells_36)); }
	inline TableCellCollection_t3482415303 * get_cells_36() const { return ___cells_36; }
	inline TableCellCollection_t3482415303 ** get_address_of_cells_36() { return &___cells_36; }
	inline void set_cells_36(TableCellCollection_t3482415303 * value)
	{
		___cells_36 = value;
		Il2CppCodeGenWriteBarrier(&___cells_36, value);
	}

	inline static int32_t get_offset_of_tableRowSectionSet_37() { return static_cast<int32_t>(offsetof(TableRow_t1154691476, ___tableRowSectionSet_37)); }
	inline bool get_tableRowSectionSet_37() const { return ___tableRowSectionSet_37; }
	inline bool* get_address_of_tableRowSectionSet_37() { return &___tableRowSectionSet_37; }
	inline void set_tableRowSectionSet_37(bool value)
	{
		___tableRowSectionSet_37 = value;
	}

	inline static int32_t get_offset_of_U3CContainerU3Ek__BackingField_38() { return static_cast<int32_t>(offsetof(TableRow_t1154691476, ___U3CContainerU3Ek__BackingField_38)); }
	inline TableRowCollection_t3754565154 * get_U3CContainerU3Ek__BackingField_38() const { return ___U3CContainerU3Ek__BackingField_38; }
	inline TableRowCollection_t3754565154 ** get_address_of_U3CContainerU3Ek__BackingField_38() { return &___U3CContainerU3Ek__BackingField_38; }
	inline void set_U3CContainerU3Ek__BackingField_38(TableRowCollection_t3754565154 * value)
	{
		___U3CContainerU3Ek__BackingField_38 = value;
		Il2CppCodeGenWriteBarrier(&___U3CContainerU3Ek__BackingField_38, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
