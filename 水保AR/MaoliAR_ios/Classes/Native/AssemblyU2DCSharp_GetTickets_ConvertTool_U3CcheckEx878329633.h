﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// UnityEngine.UI.Text
struct Text_t1901882714;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Predicate`1<UnityEngine.UI.Text>
struct Predicate_1_t2727176838;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetTickets/ConvertTool/<checkExpired>c__Iterator1
struct  U3CcheckExpiredU3Ec__Iterator1_t878329633  : public Il2CppObject
{
public:
	// UnityEngine.UI.Text GetTickets/ConvertTool/<checkExpired>c__Iterator1::<message>__0
	Text_t1901882714 * ___U3CmessageU3E__0_0;
	// System.Single GetTickets/ConvertTool/<checkExpired>c__Iterator1::<nowTime>__1
	float ___U3CnowTimeU3E__1_1;
	// System.Collections.Generic.List`1<System.String> GetTickets/ConvertTool/<checkExpired>c__Iterator1::<RestFulString>__0
	List_1_t3319525431 * ___U3CRestFulStringU3E__0_2;
	// System.String GetTickets/ConvertTool/<checkExpired>c__Iterator1::<elseType>__0
	String_t* ___U3CelseTypeU3E__0_3;
	// System.Char GetTickets/ConvertTool/<checkExpired>c__Iterator1::<specialChar>__0
	Il2CppChar ___U3CspecialCharU3E__0_4;
	// System.Object GetTickets/ConvertTool/<checkExpired>c__Iterator1::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean GetTickets/ConvertTool/<checkExpired>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 GetTickets/ConvertTool/<checkExpired>c__Iterator1::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CmessageU3E__0_0() { return static_cast<int32_t>(offsetof(U3CcheckExpiredU3Ec__Iterator1_t878329633, ___U3CmessageU3E__0_0)); }
	inline Text_t1901882714 * get_U3CmessageU3E__0_0() const { return ___U3CmessageU3E__0_0; }
	inline Text_t1901882714 ** get_address_of_U3CmessageU3E__0_0() { return &___U3CmessageU3E__0_0; }
	inline void set_U3CmessageU3E__0_0(Text_t1901882714 * value)
	{
		___U3CmessageU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmessageU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CnowTimeU3E__1_1() { return static_cast<int32_t>(offsetof(U3CcheckExpiredU3Ec__Iterator1_t878329633, ___U3CnowTimeU3E__1_1)); }
	inline float get_U3CnowTimeU3E__1_1() const { return ___U3CnowTimeU3E__1_1; }
	inline float* get_address_of_U3CnowTimeU3E__1_1() { return &___U3CnowTimeU3E__1_1; }
	inline void set_U3CnowTimeU3E__1_1(float value)
	{
		___U3CnowTimeU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CRestFulStringU3E__0_2() { return static_cast<int32_t>(offsetof(U3CcheckExpiredU3Ec__Iterator1_t878329633, ___U3CRestFulStringU3E__0_2)); }
	inline List_1_t3319525431 * get_U3CRestFulStringU3E__0_2() const { return ___U3CRestFulStringU3E__0_2; }
	inline List_1_t3319525431 ** get_address_of_U3CRestFulStringU3E__0_2() { return &___U3CRestFulStringU3E__0_2; }
	inline void set_U3CRestFulStringU3E__0_2(List_1_t3319525431 * value)
	{
		___U3CRestFulStringU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRestFulStringU3E__0_2, value);
	}

	inline static int32_t get_offset_of_U3CelseTypeU3E__0_3() { return static_cast<int32_t>(offsetof(U3CcheckExpiredU3Ec__Iterator1_t878329633, ___U3CelseTypeU3E__0_3)); }
	inline String_t* get_U3CelseTypeU3E__0_3() const { return ___U3CelseTypeU3E__0_3; }
	inline String_t** get_address_of_U3CelseTypeU3E__0_3() { return &___U3CelseTypeU3E__0_3; }
	inline void set_U3CelseTypeU3E__0_3(String_t* value)
	{
		___U3CelseTypeU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CelseTypeU3E__0_3, value);
	}

	inline static int32_t get_offset_of_U3CspecialCharU3E__0_4() { return static_cast<int32_t>(offsetof(U3CcheckExpiredU3Ec__Iterator1_t878329633, ___U3CspecialCharU3E__0_4)); }
	inline Il2CppChar get_U3CspecialCharU3E__0_4() const { return ___U3CspecialCharU3E__0_4; }
	inline Il2CppChar* get_address_of_U3CspecialCharU3E__0_4() { return &___U3CspecialCharU3E__0_4; }
	inline void set_U3CspecialCharU3E__0_4(Il2CppChar value)
	{
		___U3CspecialCharU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CcheckExpiredU3Ec__Iterator1_t878329633, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CcheckExpiredU3Ec__Iterator1_t878329633, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CcheckExpiredU3Ec__Iterator1_t878329633, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

struct U3CcheckExpiredU3Ec__Iterator1_t878329633_StaticFields
{
public:
	// System.Predicate`1<UnityEngine.UI.Text> GetTickets/ConvertTool/<checkExpired>c__Iterator1::<>f__am$cache0
	Predicate_1_t2727176838 * ___U3CU3Ef__amU24cache0_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_8() { return static_cast<int32_t>(offsetof(U3CcheckExpiredU3Ec__Iterator1_t878329633_StaticFields, ___U3CU3Ef__amU24cache0_8)); }
	inline Predicate_1_t2727176838 * get_U3CU3Ef__amU24cache0_8() const { return ___U3CU3Ef__amU24cache0_8; }
	inline Predicate_1_t2727176838 ** get_address_of_U3CU3Ef__amU24cache0_8() { return &___U3CU3Ef__amU24cache0_8; }
	inline void set_U3CU3Ef__amU24cache0_8(Predicate_1_t2727176838 * value)
	{
		___U3CU3Ef__amU24cache0_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
