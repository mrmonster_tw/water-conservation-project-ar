﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_T2144904149.h"
#include "System_ServiceModel_System_ServiceModel_HostNameCo1351856580.h"
#include "mscorlib_System_TimeSpan881159249.h"
#include "System_ServiceModel_System_ServiceModel_TransferMod436880017.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.ConnectionOrientedTransportBindingElement
struct  ConnectionOrientedTransportBindingElement_t3754390252  : public TransportBindingElement_t2144904149
{
public:
	// System.Int32 System.ServiceModel.Channels.ConnectionOrientedTransportBindingElement::connection_buf_size
	int32_t ___connection_buf_size_3;
	// System.Int32 System.ServiceModel.Channels.ConnectionOrientedTransportBindingElement::max_buf_size
	int32_t ___max_buf_size_4;
	// System.Int32 System.ServiceModel.Channels.ConnectionOrientedTransportBindingElement::max_pending_conn
	int32_t ___max_pending_conn_5;
	// System.Int32 System.ServiceModel.Channels.ConnectionOrientedTransportBindingElement::max_pending_accepts
	int32_t ___max_pending_accepts_6;
	// System.ServiceModel.HostNameComparisonMode System.ServiceModel.Channels.ConnectionOrientedTransportBindingElement::host_cmp_mode
	int32_t ___host_cmp_mode_7;
	// System.TimeSpan System.ServiceModel.Channels.ConnectionOrientedTransportBindingElement::max_output_delay
	TimeSpan_t881159249  ___max_output_delay_8;
	// System.TimeSpan System.ServiceModel.Channels.ConnectionOrientedTransportBindingElement::ch_init_timeout
	TimeSpan_t881159249  ___ch_init_timeout_9;
	// System.ServiceModel.TransferMode System.ServiceModel.Channels.ConnectionOrientedTransportBindingElement::transfer_mode
	int32_t ___transfer_mode_10;

public:
	inline static int32_t get_offset_of_connection_buf_size_3() { return static_cast<int32_t>(offsetof(ConnectionOrientedTransportBindingElement_t3754390252, ___connection_buf_size_3)); }
	inline int32_t get_connection_buf_size_3() const { return ___connection_buf_size_3; }
	inline int32_t* get_address_of_connection_buf_size_3() { return &___connection_buf_size_3; }
	inline void set_connection_buf_size_3(int32_t value)
	{
		___connection_buf_size_3 = value;
	}

	inline static int32_t get_offset_of_max_buf_size_4() { return static_cast<int32_t>(offsetof(ConnectionOrientedTransportBindingElement_t3754390252, ___max_buf_size_4)); }
	inline int32_t get_max_buf_size_4() const { return ___max_buf_size_4; }
	inline int32_t* get_address_of_max_buf_size_4() { return &___max_buf_size_4; }
	inline void set_max_buf_size_4(int32_t value)
	{
		___max_buf_size_4 = value;
	}

	inline static int32_t get_offset_of_max_pending_conn_5() { return static_cast<int32_t>(offsetof(ConnectionOrientedTransportBindingElement_t3754390252, ___max_pending_conn_5)); }
	inline int32_t get_max_pending_conn_5() const { return ___max_pending_conn_5; }
	inline int32_t* get_address_of_max_pending_conn_5() { return &___max_pending_conn_5; }
	inline void set_max_pending_conn_5(int32_t value)
	{
		___max_pending_conn_5 = value;
	}

	inline static int32_t get_offset_of_max_pending_accepts_6() { return static_cast<int32_t>(offsetof(ConnectionOrientedTransportBindingElement_t3754390252, ___max_pending_accepts_6)); }
	inline int32_t get_max_pending_accepts_6() const { return ___max_pending_accepts_6; }
	inline int32_t* get_address_of_max_pending_accepts_6() { return &___max_pending_accepts_6; }
	inline void set_max_pending_accepts_6(int32_t value)
	{
		___max_pending_accepts_6 = value;
	}

	inline static int32_t get_offset_of_host_cmp_mode_7() { return static_cast<int32_t>(offsetof(ConnectionOrientedTransportBindingElement_t3754390252, ___host_cmp_mode_7)); }
	inline int32_t get_host_cmp_mode_7() const { return ___host_cmp_mode_7; }
	inline int32_t* get_address_of_host_cmp_mode_7() { return &___host_cmp_mode_7; }
	inline void set_host_cmp_mode_7(int32_t value)
	{
		___host_cmp_mode_7 = value;
	}

	inline static int32_t get_offset_of_max_output_delay_8() { return static_cast<int32_t>(offsetof(ConnectionOrientedTransportBindingElement_t3754390252, ___max_output_delay_8)); }
	inline TimeSpan_t881159249  get_max_output_delay_8() const { return ___max_output_delay_8; }
	inline TimeSpan_t881159249 * get_address_of_max_output_delay_8() { return &___max_output_delay_8; }
	inline void set_max_output_delay_8(TimeSpan_t881159249  value)
	{
		___max_output_delay_8 = value;
	}

	inline static int32_t get_offset_of_ch_init_timeout_9() { return static_cast<int32_t>(offsetof(ConnectionOrientedTransportBindingElement_t3754390252, ___ch_init_timeout_9)); }
	inline TimeSpan_t881159249  get_ch_init_timeout_9() const { return ___ch_init_timeout_9; }
	inline TimeSpan_t881159249 * get_address_of_ch_init_timeout_9() { return &___ch_init_timeout_9; }
	inline void set_ch_init_timeout_9(TimeSpan_t881159249  value)
	{
		___ch_init_timeout_9 = value;
	}

	inline static int32_t get_offset_of_transfer_mode_10() { return static_cast<int32_t>(offsetof(ConnectionOrientedTransportBindingElement_t3754390252, ___transfer_mode_10)); }
	inline int32_t get_transfer_mode_10() const { return ___transfer_mode_10; }
	inline int32_t* get_address_of_transfer_mode_10() { return &___transfer_mode_10; }
	inline void set_transfer_mode_10(int32_t value)
	{
		___transfer_mode_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
