﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.ServiceEndpointElement
struct  ServiceEndpointElement_t1873720233  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct ServiceEndpointElement_t1873720233_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.ServiceEndpointElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.ServiceEndpointElement::address
	ConfigurationProperty_t3590861854 * ___address_14;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.ServiceEndpointElement::behavior_configuration
	ConfigurationProperty_t3590861854 * ___behavior_configuration_15;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.ServiceEndpointElement::binding
	ConfigurationProperty_t3590861854 * ___binding_16;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.ServiceEndpointElement::binding_configuration
	ConfigurationProperty_t3590861854 * ___binding_configuration_17;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.ServiceEndpointElement::binding_name
	ConfigurationProperty_t3590861854 * ___binding_name_18;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.ServiceEndpointElement::binding_namespace
	ConfigurationProperty_t3590861854 * ___binding_namespace_19;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.ServiceEndpointElement::contract
	ConfigurationProperty_t3590861854 * ___contract_20;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.ServiceEndpointElement::headers
	ConfigurationProperty_t3590861854 * ___headers_21;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.ServiceEndpointElement::identity
	ConfigurationProperty_t3590861854 * ___identity_22;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.ServiceEndpointElement::listen_uri
	ConfigurationProperty_t3590861854 * ___listen_uri_23;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.ServiceEndpointElement::listen_uri_mode
	ConfigurationProperty_t3590861854 * ___listen_uri_mode_24;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.ServiceEndpointElement::name
	ConfigurationProperty_t3590861854 * ___name_25;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(ServiceEndpointElement_t1873720233_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier(&___properties_13, value);
	}

	inline static int32_t get_offset_of_address_14() { return static_cast<int32_t>(offsetof(ServiceEndpointElement_t1873720233_StaticFields, ___address_14)); }
	inline ConfigurationProperty_t3590861854 * get_address_14() const { return ___address_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_address_14() { return &___address_14; }
	inline void set_address_14(ConfigurationProperty_t3590861854 * value)
	{
		___address_14 = value;
		Il2CppCodeGenWriteBarrier(&___address_14, value);
	}

	inline static int32_t get_offset_of_behavior_configuration_15() { return static_cast<int32_t>(offsetof(ServiceEndpointElement_t1873720233_StaticFields, ___behavior_configuration_15)); }
	inline ConfigurationProperty_t3590861854 * get_behavior_configuration_15() const { return ___behavior_configuration_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_behavior_configuration_15() { return &___behavior_configuration_15; }
	inline void set_behavior_configuration_15(ConfigurationProperty_t3590861854 * value)
	{
		___behavior_configuration_15 = value;
		Il2CppCodeGenWriteBarrier(&___behavior_configuration_15, value);
	}

	inline static int32_t get_offset_of_binding_16() { return static_cast<int32_t>(offsetof(ServiceEndpointElement_t1873720233_StaticFields, ___binding_16)); }
	inline ConfigurationProperty_t3590861854 * get_binding_16() const { return ___binding_16; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_binding_16() { return &___binding_16; }
	inline void set_binding_16(ConfigurationProperty_t3590861854 * value)
	{
		___binding_16 = value;
		Il2CppCodeGenWriteBarrier(&___binding_16, value);
	}

	inline static int32_t get_offset_of_binding_configuration_17() { return static_cast<int32_t>(offsetof(ServiceEndpointElement_t1873720233_StaticFields, ___binding_configuration_17)); }
	inline ConfigurationProperty_t3590861854 * get_binding_configuration_17() const { return ___binding_configuration_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_binding_configuration_17() { return &___binding_configuration_17; }
	inline void set_binding_configuration_17(ConfigurationProperty_t3590861854 * value)
	{
		___binding_configuration_17 = value;
		Il2CppCodeGenWriteBarrier(&___binding_configuration_17, value);
	}

	inline static int32_t get_offset_of_binding_name_18() { return static_cast<int32_t>(offsetof(ServiceEndpointElement_t1873720233_StaticFields, ___binding_name_18)); }
	inline ConfigurationProperty_t3590861854 * get_binding_name_18() const { return ___binding_name_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_binding_name_18() { return &___binding_name_18; }
	inline void set_binding_name_18(ConfigurationProperty_t3590861854 * value)
	{
		___binding_name_18 = value;
		Il2CppCodeGenWriteBarrier(&___binding_name_18, value);
	}

	inline static int32_t get_offset_of_binding_namespace_19() { return static_cast<int32_t>(offsetof(ServiceEndpointElement_t1873720233_StaticFields, ___binding_namespace_19)); }
	inline ConfigurationProperty_t3590861854 * get_binding_namespace_19() const { return ___binding_namespace_19; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_binding_namespace_19() { return &___binding_namespace_19; }
	inline void set_binding_namespace_19(ConfigurationProperty_t3590861854 * value)
	{
		___binding_namespace_19 = value;
		Il2CppCodeGenWriteBarrier(&___binding_namespace_19, value);
	}

	inline static int32_t get_offset_of_contract_20() { return static_cast<int32_t>(offsetof(ServiceEndpointElement_t1873720233_StaticFields, ___contract_20)); }
	inline ConfigurationProperty_t3590861854 * get_contract_20() const { return ___contract_20; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_contract_20() { return &___contract_20; }
	inline void set_contract_20(ConfigurationProperty_t3590861854 * value)
	{
		___contract_20 = value;
		Il2CppCodeGenWriteBarrier(&___contract_20, value);
	}

	inline static int32_t get_offset_of_headers_21() { return static_cast<int32_t>(offsetof(ServiceEndpointElement_t1873720233_StaticFields, ___headers_21)); }
	inline ConfigurationProperty_t3590861854 * get_headers_21() const { return ___headers_21; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_headers_21() { return &___headers_21; }
	inline void set_headers_21(ConfigurationProperty_t3590861854 * value)
	{
		___headers_21 = value;
		Il2CppCodeGenWriteBarrier(&___headers_21, value);
	}

	inline static int32_t get_offset_of_identity_22() { return static_cast<int32_t>(offsetof(ServiceEndpointElement_t1873720233_StaticFields, ___identity_22)); }
	inline ConfigurationProperty_t3590861854 * get_identity_22() const { return ___identity_22; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_identity_22() { return &___identity_22; }
	inline void set_identity_22(ConfigurationProperty_t3590861854 * value)
	{
		___identity_22 = value;
		Il2CppCodeGenWriteBarrier(&___identity_22, value);
	}

	inline static int32_t get_offset_of_listen_uri_23() { return static_cast<int32_t>(offsetof(ServiceEndpointElement_t1873720233_StaticFields, ___listen_uri_23)); }
	inline ConfigurationProperty_t3590861854 * get_listen_uri_23() const { return ___listen_uri_23; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_listen_uri_23() { return &___listen_uri_23; }
	inline void set_listen_uri_23(ConfigurationProperty_t3590861854 * value)
	{
		___listen_uri_23 = value;
		Il2CppCodeGenWriteBarrier(&___listen_uri_23, value);
	}

	inline static int32_t get_offset_of_listen_uri_mode_24() { return static_cast<int32_t>(offsetof(ServiceEndpointElement_t1873720233_StaticFields, ___listen_uri_mode_24)); }
	inline ConfigurationProperty_t3590861854 * get_listen_uri_mode_24() const { return ___listen_uri_mode_24; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_listen_uri_mode_24() { return &___listen_uri_mode_24; }
	inline void set_listen_uri_mode_24(ConfigurationProperty_t3590861854 * value)
	{
		___listen_uri_mode_24 = value;
		Il2CppCodeGenWriteBarrier(&___listen_uri_mode_24, value);
	}

	inline static int32_t get_offset_of_name_25() { return static_cast<int32_t>(offsetof(ServiceEndpointElement_t1873720233_StaticFields, ___name_25)); }
	inline ConfigurationProperty_t3590861854 * get_name_25() const { return ___name_25; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_name_25() { return &___name_25; }
	inline void set_name_25(ConfigurationProperty_t3590861854 * value)
	{
		___name_25 = value;
		Il2CppCodeGenWriteBarrier(&___name_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
