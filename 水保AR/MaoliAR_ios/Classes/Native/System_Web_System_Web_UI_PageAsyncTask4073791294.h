﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.BeginEventHandler
struct BeginEventHandler_t3912883583;
// System.Web.EndEventHandler
struct EndEventHandler_t957584525;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.PageAsyncTask
struct  PageAsyncTask_t4073791294  : public Il2CppObject
{
public:
	// System.Web.BeginEventHandler System.Web.UI.PageAsyncTask::beginHandler
	BeginEventHandler_t3912883583 * ___beginHandler_0;
	// System.Web.EndEventHandler System.Web.UI.PageAsyncTask::endHandler
	EndEventHandler_t957584525 * ___endHandler_1;
	// System.Web.EndEventHandler System.Web.UI.PageAsyncTask::timeoutHandler
	EndEventHandler_t957584525 * ___timeoutHandler_2;
	// System.Object System.Web.UI.PageAsyncTask::state
	Il2CppObject * ___state_3;

public:
	inline static int32_t get_offset_of_beginHandler_0() { return static_cast<int32_t>(offsetof(PageAsyncTask_t4073791294, ___beginHandler_0)); }
	inline BeginEventHandler_t3912883583 * get_beginHandler_0() const { return ___beginHandler_0; }
	inline BeginEventHandler_t3912883583 ** get_address_of_beginHandler_0() { return &___beginHandler_0; }
	inline void set_beginHandler_0(BeginEventHandler_t3912883583 * value)
	{
		___beginHandler_0 = value;
		Il2CppCodeGenWriteBarrier(&___beginHandler_0, value);
	}

	inline static int32_t get_offset_of_endHandler_1() { return static_cast<int32_t>(offsetof(PageAsyncTask_t4073791294, ___endHandler_1)); }
	inline EndEventHandler_t957584525 * get_endHandler_1() const { return ___endHandler_1; }
	inline EndEventHandler_t957584525 ** get_address_of_endHandler_1() { return &___endHandler_1; }
	inline void set_endHandler_1(EndEventHandler_t957584525 * value)
	{
		___endHandler_1 = value;
		Il2CppCodeGenWriteBarrier(&___endHandler_1, value);
	}

	inline static int32_t get_offset_of_timeoutHandler_2() { return static_cast<int32_t>(offsetof(PageAsyncTask_t4073791294, ___timeoutHandler_2)); }
	inline EndEventHandler_t957584525 * get_timeoutHandler_2() const { return ___timeoutHandler_2; }
	inline EndEventHandler_t957584525 ** get_address_of_timeoutHandler_2() { return &___timeoutHandler_2; }
	inline void set_timeoutHandler_2(EndEventHandler_t957584525 * value)
	{
		___timeoutHandler_2 = value;
		Il2CppCodeGenWriteBarrier(&___timeoutHandler_2, value);
	}

	inline static int32_t get_offset_of_state_3() { return static_cast<int32_t>(offsetof(PageAsyncTask_t4073791294, ___state_3)); }
	inline Il2CppObject * get_state_3() const { return ___state_3; }
	inline Il2CppObject ** get_address_of_state_3() { return &___state_3; }
	inline void set_state_3(Il2CppObject * value)
	{
		___state_3 = value;
		Il2CppCodeGenWriteBarrier(&___state_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
