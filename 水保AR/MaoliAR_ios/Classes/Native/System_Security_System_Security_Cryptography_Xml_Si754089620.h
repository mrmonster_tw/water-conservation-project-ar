﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t418790500;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Security.Cryptography.Xml.SignedInfo
struct SignedInfo_t1687583442;
// System.Security.Cryptography.Xml.KeyInfo
struct KeyInfo_t3757684699;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Xml.XmlElement
struct XmlElement_t561603118;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.Xml.Signature
struct  Signature_t754089620  : public Il2CppObject
{
public:
	// System.Collections.ArrayList System.Security.Cryptography.Xml.Signature::list
	ArrayList_t2718874744 * ___list_1;
	// System.Security.Cryptography.Xml.SignedInfo System.Security.Cryptography.Xml.Signature::info
	SignedInfo_t1687583442 * ___info_2;
	// System.Security.Cryptography.Xml.KeyInfo System.Security.Cryptography.Xml.Signature::key
	KeyInfo_t3757684699 * ___key_3;
	// System.String System.Security.Cryptography.Xml.Signature::id
	String_t* ___id_4;
	// System.Byte[] System.Security.Cryptography.Xml.Signature::signature
	ByteU5BU5D_t4116647657* ___signature_5;
	// System.Xml.XmlElement System.Security.Cryptography.Xml.Signature::element
	XmlElement_t561603118 * ___element_6;

public:
	inline static int32_t get_offset_of_list_1() { return static_cast<int32_t>(offsetof(Signature_t754089620, ___list_1)); }
	inline ArrayList_t2718874744 * get_list_1() const { return ___list_1; }
	inline ArrayList_t2718874744 ** get_address_of_list_1() { return &___list_1; }
	inline void set_list_1(ArrayList_t2718874744 * value)
	{
		___list_1 = value;
		Il2CppCodeGenWriteBarrier(&___list_1, value);
	}

	inline static int32_t get_offset_of_info_2() { return static_cast<int32_t>(offsetof(Signature_t754089620, ___info_2)); }
	inline SignedInfo_t1687583442 * get_info_2() const { return ___info_2; }
	inline SignedInfo_t1687583442 ** get_address_of_info_2() { return &___info_2; }
	inline void set_info_2(SignedInfo_t1687583442 * value)
	{
		___info_2 = value;
		Il2CppCodeGenWriteBarrier(&___info_2, value);
	}

	inline static int32_t get_offset_of_key_3() { return static_cast<int32_t>(offsetof(Signature_t754089620, ___key_3)); }
	inline KeyInfo_t3757684699 * get_key_3() const { return ___key_3; }
	inline KeyInfo_t3757684699 ** get_address_of_key_3() { return &___key_3; }
	inline void set_key_3(KeyInfo_t3757684699 * value)
	{
		___key_3 = value;
		Il2CppCodeGenWriteBarrier(&___key_3, value);
	}

	inline static int32_t get_offset_of_id_4() { return static_cast<int32_t>(offsetof(Signature_t754089620, ___id_4)); }
	inline String_t* get_id_4() const { return ___id_4; }
	inline String_t** get_address_of_id_4() { return &___id_4; }
	inline void set_id_4(String_t* value)
	{
		___id_4 = value;
		Il2CppCodeGenWriteBarrier(&___id_4, value);
	}

	inline static int32_t get_offset_of_signature_5() { return static_cast<int32_t>(offsetof(Signature_t754089620, ___signature_5)); }
	inline ByteU5BU5D_t4116647657* get_signature_5() const { return ___signature_5; }
	inline ByteU5BU5D_t4116647657** get_address_of_signature_5() { return &___signature_5; }
	inline void set_signature_5(ByteU5BU5D_t4116647657* value)
	{
		___signature_5 = value;
		Il2CppCodeGenWriteBarrier(&___signature_5, value);
	}

	inline static int32_t get_offset_of_element_6() { return static_cast<int32_t>(offsetof(Signature_t754089620, ___element_6)); }
	inline XmlElement_t561603118 * get_element_6() const { return ___element_6; }
	inline XmlElement_t561603118 ** get_address_of_element_6() { return &___element_6; }
	inline void set_element_6(XmlElement_t561603118 * value)
	{
		___element_6 = value;
		Il2CppCodeGenWriteBarrier(&___element_6, value);
	}
};

struct Signature_t754089620_StaticFields
{
public:
	// System.Xml.XmlNamespaceManager System.Security.Cryptography.Xml.Signature::dsigNsmgr
	XmlNamespaceManager_t418790500 * ___dsigNsmgr_0;

public:
	inline static int32_t get_offset_of_dsigNsmgr_0() { return static_cast<int32_t>(offsetof(Signature_t754089620_StaticFields, ___dsigNsmgr_0)); }
	inline XmlNamespaceManager_t418790500 * get_dsigNsmgr_0() const { return ___dsigNsmgr_0; }
	inline XmlNamespaceManager_t418790500 ** get_address_of_dsigNsmgr_0() { return &___dsigNsmgr_0; }
	inline void set_dsigNsmgr_0(XmlNamespaceManager_t418790500 * value)
	{
		___dsigNsmgr_0 = value;
		Il2CppCodeGenWriteBarrier(&___dsigNsmgr_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
