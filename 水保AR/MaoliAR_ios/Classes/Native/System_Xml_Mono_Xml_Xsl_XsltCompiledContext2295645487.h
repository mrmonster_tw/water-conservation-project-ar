﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Xsl_XsltContext2039362735.h"

// System.Collections.Hashtable
struct Hashtable_t1853889766;
// Mono.Xml.Xsl.XslTransformProcessor
struct XslTransformProcessor_t3405861191;
// Mono.Xml.Xsl.XsltCompiledContext/XsltContextInfo[]
struct XsltContextInfoU5BU5D_t3794080776;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.XsltCompiledContext
struct  XsltCompiledContext_t2295645487  : public XsltContext_t2039362735
{
public:
	// System.Collections.Hashtable Mono.Xml.Xsl.XsltCompiledContext::keyNameCache
	Hashtable_t1853889766 * ___keyNameCache_9;
	// System.Collections.Hashtable Mono.Xml.Xsl.XsltCompiledContext::keyIndexTables
	Hashtable_t1853889766 * ___keyIndexTables_10;
	// System.Collections.Hashtable Mono.Xml.Xsl.XsltCompiledContext::patternNavCaches
	Hashtable_t1853889766 * ___patternNavCaches_11;
	// Mono.Xml.Xsl.XslTransformProcessor Mono.Xml.Xsl.XsltCompiledContext::p
	XslTransformProcessor_t3405861191 * ___p_12;
	// Mono.Xml.Xsl.XsltCompiledContext/XsltContextInfo[] Mono.Xml.Xsl.XsltCompiledContext::scopes
	XsltContextInfoU5BU5D_t3794080776* ___scopes_13;
	// System.Int32 Mono.Xml.Xsl.XsltCompiledContext::scopeAt
	int32_t ___scopeAt_14;

public:
	inline static int32_t get_offset_of_keyNameCache_9() { return static_cast<int32_t>(offsetof(XsltCompiledContext_t2295645487, ___keyNameCache_9)); }
	inline Hashtable_t1853889766 * get_keyNameCache_9() const { return ___keyNameCache_9; }
	inline Hashtable_t1853889766 ** get_address_of_keyNameCache_9() { return &___keyNameCache_9; }
	inline void set_keyNameCache_9(Hashtable_t1853889766 * value)
	{
		___keyNameCache_9 = value;
		Il2CppCodeGenWriteBarrier(&___keyNameCache_9, value);
	}

	inline static int32_t get_offset_of_keyIndexTables_10() { return static_cast<int32_t>(offsetof(XsltCompiledContext_t2295645487, ___keyIndexTables_10)); }
	inline Hashtable_t1853889766 * get_keyIndexTables_10() const { return ___keyIndexTables_10; }
	inline Hashtable_t1853889766 ** get_address_of_keyIndexTables_10() { return &___keyIndexTables_10; }
	inline void set_keyIndexTables_10(Hashtable_t1853889766 * value)
	{
		___keyIndexTables_10 = value;
		Il2CppCodeGenWriteBarrier(&___keyIndexTables_10, value);
	}

	inline static int32_t get_offset_of_patternNavCaches_11() { return static_cast<int32_t>(offsetof(XsltCompiledContext_t2295645487, ___patternNavCaches_11)); }
	inline Hashtable_t1853889766 * get_patternNavCaches_11() const { return ___patternNavCaches_11; }
	inline Hashtable_t1853889766 ** get_address_of_patternNavCaches_11() { return &___patternNavCaches_11; }
	inline void set_patternNavCaches_11(Hashtable_t1853889766 * value)
	{
		___patternNavCaches_11 = value;
		Il2CppCodeGenWriteBarrier(&___patternNavCaches_11, value);
	}

	inline static int32_t get_offset_of_p_12() { return static_cast<int32_t>(offsetof(XsltCompiledContext_t2295645487, ___p_12)); }
	inline XslTransformProcessor_t3405861191 * get_p_12() const { return ___p_12; }
	inline XslTransformProcessor_t3405861191 ** get_address_of_p_12() { return &___p_12; }
	inline void set_p_12(XslTransformProcessor_t3405861191 * value)
	{
		___p_12 = value;
		Il2CppCodeGenWriteBarrier(&___p_12, value);
	}

	inline static int32_t get_offset_of_scopes_13() { return static_cast<int32_t>(offsetof(XsltCompiledContext_t2295645487, ___scopes_13)); }
	inline XsltContextInfoU5BU5D_t3794080776* get_scopes_13() const { return ___scopes_13; }
	inline XsltContextInfoU5BU5D_t3794080776** get_address_of_scopes_13() { return &___scopes_13; }
	inline void set_scopes_13(XsltContextInfoU5BU5D_t3794080776* value)
	{
		___scopes_13 = value;
		Il2CppCodeGenWriteBarrier(&___scopes_13, value);
	}

	inline static int32_t get_offset_of_scopeAt_14() { return static_cast<int32_t>(offsetof(XsltCompiledContext_t2295645487, ___scopeAt_14)); }
	inline int32_t get_scopeAt_14() const { return ___scopeAt_14; }
	inline int32_t* get_address_of_scopeAt_14() { return &___scopeAt_14; }
	inline void set_scopeAt_14(int32_t value)
	{
		___scopeAt_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
