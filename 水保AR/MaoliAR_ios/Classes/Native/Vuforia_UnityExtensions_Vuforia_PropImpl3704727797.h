﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainTracka2707361041.h"
#include "Vuforia_UnityExtensions_Vuforia_OrientedBoundingBo4089508388.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PropImpl
struct  PropImpl_t3704727797  : public SmartTerrainTrackableImpl_t2707361041
{
public:
	// Vuforia.OrientedBoundingBox3D Vuforia.PropImpl::mOrientedBoundingBox3D
	OrientedBoundingBox3D_t4089508388  ___mOrientedBoundingBox3D_7;

public:
	inline static int32_t get_offset_of_mOrientedBoundingBox3D_7() { return static_cast<int32_t>(offsetof(PropImpl_t3704727797, ___mOrientedBoundingBox3D_7)); }
	inline OrientedBoundingBox3D_t4089508388  get_mOrientedBoundingBox3D_7() const { return ___mOrientedBoundingBox3D_7; }
	inline OrientedBoundingBox3D_t4089508388 * get_address_of_mOrientedBoundingBox3D_7() { return &___mOrientedBoundingBox3D_7; }
	inline void set_mOrientedBoundingBox3D_7(OrientedBoundingBox3D_t4089508388  value)
	{
		___mOrientedBoundingBox3D_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
