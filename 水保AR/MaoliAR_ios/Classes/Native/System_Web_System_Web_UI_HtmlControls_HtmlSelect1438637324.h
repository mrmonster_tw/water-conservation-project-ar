﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_HtmlControls_HtmlContainer641877197.h"

// System.Web.UI.DataSourceView
struct DataSourceView_t281661168;
// System.Object
struct Il2CppObject;
// System.Web.UI.WebControls.ListItemCollection
struct ListItemCollection_t682790300;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.HtmlControls.HtmlSelect
struct  HtmlSelect_t1438637324  : public HtmlContainerControl_t641877197
{
public:
	// System.Web.UI.DataSourceView System.Web.UI.HtmlControls.HtmlSelect::_boundDataSourceView
	DataSourceView_t281661168 * ____boundDataSourceView_30;
	// System.Boolean System.Web.UI.HtmlControls.HtmlSelect::requiresDataBinding
	bool ___requiresDataBinding_31;
	// System.Boolean System.Web.UI.HtmlControls.HtmlSelect::_initialized
	bool ____initialized_32;
	// System.Object System.Web.UI.HtmlControls.HtmlSelect::datasource
	Il2CppObject * ___datasource_33;
	// System.Web.UI.WebControls.ListItemCollection System.Web.UI.HtmlControls.HtmlSelect::items
	ListItemCollection_t682790300 * ___items_34;

public:
	inline static int32_t get_offset_of__boundDataSourceView_30() { return static_cast<int32_t>(offsetof(HtmlSelect_t1438637324, ____boundDataSourceView_30)); }
	inline DataSourceView_t281661168 * get__boundDataSourceView_30() const { return ____boundDataSourceView_30; }
	inline DataSourceView_t281661168 ** get_address_of__boundDataSourceView_30() { return &____boundDataSourceView_30; }
	inline void set__boundDataSourceView_30(DataSourceView_t281661168 * value)
	{
		____boundDataSourceView_30 = value;
		Il2CppCodeGenWriteBarrier(&____boundDataSourceView_30, value);
	}

	inline static int32_t get_offset_of_requiresDataBinding_31() { return static_cast<int32_t>(offsetof(HtmlSelect_t1438637324, ___requiresDataBinding_31)); }
	inline bool get_requiresDataBinding_31() const { return ___requiresDataBinding_31; }
	inline bool* get_address_of_requiresDataBinding_31() { return &___requiresDataBinding_31; }
	inline void set_requiresDataBinding_31(bool value)
	{
		___requiresDataBinding_31 = value;
	}

	inline static int32_t get_offset_of__initialized_32() { return static_cast<int32_t>(offsetof(HtmlSelect_t1438637324, ____initialized_32)); }
	inline bool get__initialized_32() const { return ____initialized_32; }
	inline bool* get_address_of__initialized_32() { return &____initialized_32; }
	inline void set__initialized_32(bool value)
	{
		____initialized_32 = value;
	}

	inline static int32_t get_offset_of_datasource_33() { return static_cast<int32_t>(offsetof(HtmlSelect_t1438637324, ___datasource_33)); }
	inline Il2CppObject * get_datasource_33() const { return ___datasource_33; }
	inline Il2CppObject ** get_address_of_datasource_33() { return &___datasource_33; }
	inline void set_datasource_33(Il2CppObject * value)
	{
		___datasource_33 = value;
		Il2CppCodeGenWriteBarrier(&___datasource_33, value);
	}

	inline static int32_t get_offset_of_items_34() { return static_cast<int32_t>(offsetof(HtmlSelect_t1438637324, ___items_34)); }
	inline ListItemCollection_t682790300 * get_items_34() const { return ___items_34; }
	inline ListItemCollection_t682790300 ** get_address_of_items_34() { return &___items_34; }
	inline void set_items_34(ListItemCollection_t682790300 * value)
	{
		___items_34 = value;
		Il2CppCodeGenWriteBarrier(&___items_34, value);
	}
};

struct HtmlSelect_t1438637324_StaticFields
{
public:
	// System.Object System.Web.UI.HtmlControls.HtmlSelect::EventServerChange
	Il2CppObject * ___EventServerChange_35;

public:
	inline static int32_t get_offset_of_EventServerChange_35() { return static_cast<int32_t>(offsetof(HtmlSelect_t1438637324_StaticFields, ___EventServerChange_35)); }
	inline Il2CppObject * get_EventServerChange_35() const { return ___EventServerChange_35; }
	inline Il2CppObject ** get_address_of_EventServerChange_35() { return &___EventServerChange_35; }
	inline void set_EventServerChange_35(Il2CppObject * value)
	{
		___EventServerChange_35 = value;
		Il2CppCodeGenWriteBarrier(&___EventServerChange_35, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
