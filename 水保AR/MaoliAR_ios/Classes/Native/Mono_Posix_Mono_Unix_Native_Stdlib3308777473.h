﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_IntPtr840150181.h"

// Mono.Unix.Native.SignalHandler
struct SignalHandler_t1590986791;
// Mono.Unix.Native.SignalHandler[]
struct SignalHandlerU5BU5D_t3624871774;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unix.Native.Stdlib
struct  Stdlib_t3308777473  : public Il2CppObject
{
public:

public:
};

struct Stdlib_t3308777473_StaticFields
{
public:
	// System.IntPtr Mono.Unix.Native.Stdlib::_SIG_DFL
	IntPtr_t ____SIG_DFL_0;
	// System.IntPtr Mono.Unix.Native.Stdlib::_SIG_ERR
	IntPtr_t ____SIG_ERR_1;
	// System.IntPtr Mono.Unix.Native.Stdlib::_SIG_IGN
	IntPtr_t ____SIG_IGN_2;
	// Mono.Unix.Native.SignalHandler Mono.Unix.Native.Stdlib::SIG_DFL
	SignalHandler_t1590986791 * ___SIG_DFL_3;
	// Mono.Unix.Native.SignalHandler Mono.Unix.Native.Stdlib::SIG_ERR
	SignalHandler_t1590986791 * ___SIG_ERR_4;
	// Mono.Unix.Native.SignalHandler Mono.Unix.Native.Stdlib::SIG_IGN
	SignalHandler_t1590986791 * ___SIG_IGN_5;
	// Mono.Unix.Native.SignalHandler[] Mono.Unix.Native.Stdlib::registered_signals
	SignalHandlerU5BU5D_t3624871774* ___registered_signals_6;
	// System.Int32 Mono.Unix.Native.Stdlib::_IOFBF
	int32_t ____IOFBF_7;
	// System.Int32 Mono.Unix.Native.Stdlib::_IOLBF
	int32_t ____IOLBF_8;
	// System.Int32 Mono.Unix.Native.Stdlib::_IONBF
	int32_t ____IONBF_9;
	// System.Int32 Mono.Unix.Native.Stdlib::BUFSIZ
	int32_t ___BUFSIZ_10;
	// System.Int32 Mono.Unix.Native.Stdlib::EOF
	int32_t ___EOF_11;
	// System.Int32 Mono.Unix.Native.Stdlib::FOPEN_MAX
	int32_t ___FOPEN_MAX_12;
	// System.Int32 Mono.Unix.Native.Stdlib::FILENAME_MAX
	int32_t ___FILENAME_MAX_13;
	// System.Int32 Mono.Unix.Native.Stdlib::L_tmpnam
	int32_t ___L_tmpnam_14;
	// System.IntPtr Mono.Unix.Native.Stdlib::stderr
	IntPtr_t ___stderr_15;
	// System.IntPtr Mono.Unix.Native.Stdlib::stdin
	IntPtr_t ___stdin_16;
	// System.IntPtr Mono.Unix.Native.Stdlib::stdout
	IntPtr_t ___stdout_17;
	// System.Int32 Mono.Unix.Native.Stdlib::TMP_MAX
	int32_t ___TMP_MAX_18;
	// System.Object Mono.Unix.Native.Stdlib::tmpnam_lock
	Il2CppObject * ___tmpnam_lock_19;
	// System.Int32 Mono.Unix.Native.Stdlib::EXIT_FAILURE
	int32_t ___EXIT_FAILURE_20;
	// System.Int32 Mono.Unix.Native.Stdlib::EXIT_SUCCESS
	int32_t ___EXIT_SUCCESS_21;
	// System.Int32 Mono.Unix.Native.Stdlib::MB_CUR_MAX
	int32_t ___MB_CUR_MAX_22;
	// System.Int32 Mono.Unix.Native.Stdlib::RAND_MAX
	int32_t ___RAND_MAX_23;
	// System.Object Mono.Unix.Native.Stdlib::strerror_lock
	Il2CppObject * ___strerror_lock_24;

public:
	inline static int32_t get_offset_of__SIG_DFL_0() { return static_cast<int32_t>(offsetof(Stdlib_t3308777473_StaticFields, ____SIG_DFL_0)); }
	inline IntPtr_t get__SIG_DFL_0() const { return ____SIG_DFL_0; }
	inline IntPtr_t* get_address_of__SIG_DFL_0() { return &____SIG_DFL_0; }
	inline void set__SIG_DFL_0(IntPtr_t value)
	{
		____SIG_DFL_0 = value;
	}

	inline static int32_t get_offset_of__SIG_ERR_1() { return static_cast<int32_t>(offsetof(Stdlib_t3308777473_StaticFields, ____SIG_ERR_1)); }
	inline IntPtr_t get__SIG_ERR_1() const { return ____SIG_ERR_1; }
	inline IntPtr_t* get_address_of__SIG_ERR_1() { return &____SIG_ERR_1; }
	inline void set__SIG_ERR_1(IntPtr_t value)
	{
		____SIG_ERR_1 = value;
	}

	inline static int32_t get_offset_of__SIG_IGN_2() { return static_cast<int32_t>(offsetof(Stdlib_t3308777473_StaticFields, ____SIG_IGN_2)); }
	inline IntPtr_t get__SIG_IGN_2() const { return ____SIG_IGN_2; }
	inline IntPtr_t* get_address_of__SIG_IGN_2() { return &____SIG_IGN_2; }
	inline void set__SIG_IGN_2(IntPtr_t value)
	{
		____SIG_IGN_2 = value;
	}

	inline static int32_t get_offset_of_SIG_DFL_3() { return static_cast<int32_t>(offsetof(Stdlib_t3308777473_StaticFields, ___SIG_DFL_3)); }
	inline SignalHandler_t1590986791 * get_SIG_DFL_3() const { return ___SIG_DFL_3; }
	inline SignalHandler_t1590986791 ** get_address_of_SIG_DFL_3() { return &___SIG_DFL_3; }
	inline void set_SIG_DFL_3(SignalHandler_t1590986791 * value)
	{
		___SIG_DFL_3 = value;
		Il2CppCodeGenWriteBarrier(&___SIG_DFL_3, value);
	}

	inline static int32_t get_offset_of_SIG_ERR_4() { return static_cast<int32_t>(offsetof(Stdlib_t3308777473_StaticFields, ___SIG_ERR_4)); }
	inline SignalHandler_t1590986791 * get_SIG_ERR_4() const { return ___SIG_ERR_4; }
	inline SignalHandler_t1590986791 ** get_address_of_SIG_ERR_4() { return &___SIG_ERR_4; }
	inline void set_SIG_ERR_4(SignalHandler_t1590986791 * value)
	{
		___SIG_ERR_4 = value;
		Il2CppCodeGenWriteBarrier(&___SIG_ERR_4, value);
	}

	inline static int32_t get_offset_of_SIG_IGN_5() { return static_cast<int32_t>(offsetof(Stdlib_t3308777473_StaticFields, ___SIG_IGN_5)); }
	inline SignalHandler_t1590986791 * get_SIG_IGN_5() const { return ___SIG_IGN_5; }
	inline SignalHandler_t1590986791 ** get_address_of_SIG_IGN_5() { return &___SIG_IGN_5; }
	inline void set_SIG_IGN_5(SignalHandler_t1590986791 * value)
	{
		___SIG_IGN_5 = value;
		Il2CppCodeGenWriteBarrier(&___SIG_IGN_5, value);
	}

	inline static int32_t get_offset_of_registered_signals_6() { return static_cast<int32_t>(offsetof(Stdlib_t3308777473_StaticFields, ___registered_signals_6)); }
	inline SignalHandlerU5BU5D_t3624871774* get_registered_signals_6() const { return ___registered_signals_6; }
	inline SignalHandlerU5BU5D_t3624871774** get_address_of_registered_signals_6() { return &___registered_signals_6; }
	inline void set_registered_signals_6(SignalHandlerU5BU5D_t3624871774* value)
	{
		___registered_signals_6 = value;
		Il2CppCodeGenWriteBarrier(&___registered_signals_6, value);
	}

	inline static int32_t get_offset_of__IOFBF_7() { return static_cast<int32_t>(offsetof(Stdlib_t3308777473_StaticFields, ____IOFBF_7)); }
	inline int32_t get__IOFBF_7() const { return ____IOFBF_7; }
	inline int32_t* get_address_of__IOFBF_7() { return &____IOFBF_7; }
	inline void set__IOFBF_7(int32_t value)
	{
		____IOFBF_7 = value;
	}

	inline static int32_t get_offset_of__IOLBF_8() { return static_cast<int32_t>(offsetof(Stdlib_t3308777473_StaticFields, ____IOLBF_8)); }
	inline int32_t get__IOLBF_8() const { return ____IOLBF_8; }
	inline int32_t* get_address_of__IOLBF_8() { return &____IOLBF_8; }
	inline void set__IOLBF_8(int32_t value)
	{
		____IOLBF_8 = value;
	}

	inline static int32_t get_offset_of__IONBF_9() { return static_cast<int32_t>(offsetof(Stdlib_t3308777473_StaticFields, ____IONBF_9)); }
	inline int32_t get__IONBF_9() const { return ____IONBF_9; }
	inline int32_t* get_address_of__IONBF_9() { return &____IONBF_9; }
	inline void set__IONBF_9(int32_t value)
	{
		____IONBF_9 = value;
	}

	inline static int32_t get_offset_of_BUFSIZ_10() { return static_cast<int32_t>(offsetof(Stdlib_t3308777473_StaticFields, ___BUFSIZ_10)); }
	inline int32_t get_BUFSIZ_10() const { return ___BUFSIZ_10; }
	inline int32_t* get_address_of_BUFSIZ_10() { return &___BUFSIZ_10; }
	inline void set_BUFSIZ_10(int32_t value)
	{
		___BUFSIZ_10 = value;
	}

	inline static int32_t get_offset_of_EOF_11() { return static_cast<int32_t>(offsetof(Stdlib_t3308777473_StaticFields, ___EOF_11)); }
	inline int32_t get_EOF_11() const { return ___EOF_11; }
	inline int32_t* get_address_of_EOF_11() { return &___EOF_11; }
	inline void set_EOF_11(int32_t value)
	{
		___EOF_11 = value;
	}

	inline static int32_t get_offset_of_FOPEN_MAX_12() { return static_cast<int32_t>(offsetof(Stdlib_t3308777473_StaticFields, ___FOPEN_MAX_12)); }
	inline int32_t get_FOPEN_MAX_12() const { return ___FOPEN_MAX_12; }
	inline int32_t* get_address_of_FOPEN_MAX_12() { return &___FOPEN_MAX_12; }
	inline void set_FOPEN_MAX_12(int32_t value)
	{
		___FOPEN_MAX_12 = value;
	}

	inline static int32_t get_offset_of_FILENAME_MAX_13() { return static_cast<int32_t>(offsetof(Stdlib_t3308777473_StaticFields, ___FILENAME_MAX_13)); }
	inline int32_t get_FILENAME_MAX_13() const { return ___FILENAME_MAX_13; }
	inline int32_t* get_address_of_FILENAME_MAX_13() { return &___FILENAME_MAX_13; }
	inline void set_FILENAME_MAX_13(int32_t value)
	{
		___FILENAME_MAX_13 = value;
	}

	inline static int32_t get_offset_of_L_tmpnam_14() { return static_cast<int32_t>(offsetof(Stdlib_t3308777473_StaticFields, ___L_tmpnam_14)); }
	inline int32_t get_L_tmpnam_14() const { return ___L_tmpnam_14; }
	inline int32_t* get_address_of_L_tmpnam_14() { return &___L_tmpnam_14; }
	inline void set_L_tmpnam_14(int32_t value)
	{
		___L_tmpnam_14 = value;
	}

	inline static int32_t get_offset_of_stderr_15() { return static_cast<int32_t>(offsetof(Stdlib_t3308777473_StaticFields, ___stderr_15)); }
	inline IntPtr_t get_stderr_15() const { return ___stderr_15; }
	inline IntPtr_t* get_address_of_stderr_15() { return &___stderr_15; }
	inline void set_stderr_15(IntPtr_t value)
	{
		___stderr_15 = value;
	}

	inline static int32_t get_offset_of_stdin_16() { return static_cast<int32_t>(offsetof(Stdlib_t3308777473_StaticFields, ___stdin_16)); }
	inline IntPtr_t get_stdin_16() const { return ___stdin_16; }
	inline IntPtr_t* get_address_of_stdin_16() { return &___stdin_16; }
	inline void set_stdin_16(IntPtr_t value)
	{
		___stdin_16 = value;
	}

	inline static int32_t get_offset_of_stdout_17() { return static_cast<int32_t>(offsetof(Stdlib_t3308777473_StaticFields, ___stdout_17)); }
	inline IntPtr_t get_stdout_17() const { return ___stdout_17; }
	inline IntPtr_t* get_address_of_stdout_17() { return &___stdout_17; }
	inline void set_stdout_17(IntPtr_t value)
	{
		___stdout_17 = value;
	}

	inline static int32_t get_offset_of_TMP_MAX_18() { return static_cast<int32_t>(offsetof(Stdlib_t3308777473_StaticFields, ___TMP_MAX_18)); }
	inline int32_t get_TMP_MAX_18() const { return ___TMP_MAX_18; }
	inline int32_t* get_address_of_TMP_MAX_18() { return &___TMP_MAX_18; }
	inline void set_TMP_MAX_18(int32_t value)
	{
		___TMP_MAX_18 = value;
	}

	inline static int32_t get_offset_of_tmpnam_lock_19() { return static_cast<int32_t>(offsetof(Stdlib_t3308777473_StaticFields, ___tmpnam_lock_19)); }
	inline Il2CppObject * get_tmpnam_lock_19() const { return ___tmpnam_lock_19; }
	inline Il2CppObject ** get_address_of_tmpnam_lock_19() { return &___tmpnam_lock_19; }
	inline void set_tmpnam_lock_19(Il2CppObject * value)
	{
		___tmpnam_lock_19 = value;
		Il2CppCodeGenWriteBarrier(&___tmpnam_lock_19, value);
	}

	inline static int32_t get_offset_of_EXIT_FAILURE_20() { return static_cast<int32_t>(offsetof(Stdlib_t3308777473_StaticFields, ___EXIT_FAILURE_20)); }
	inline int32_t get_EXIT_FAILURE_20() const { return ___EXIT_FAILURE_20; }
	inline int32_t* get_address_of_EXIT_FAILURE_20() { return &___EXIT_FAILURE_20; }
	inline void set_EXIT_FAILURE_20(int32_t value)
	{
		___EXIT_FAILURE_20 = value;
	}

	inline static int32_t get_offset_of_EXIT_SUCCESS_21() { return static_cast<int32_t>(offsetof(Stdlib_t3308777473_StaticFields, ___EXIT_SUCCESS_21)); }
	inline int32_t get_EXIT_SUCCESS_21() const { return ___EXIT_SUCCESS_21; }
	inline int32_t* get_address_of_EXIT_SUCCESS_21() { return &___EXIT_SUCCESS_21; }
	inline void set_EXIT_SUCCESS_21(int32_t value)
	{
		___EXIT_SUCCESS_21 = value;
	}

	inline static int32_t get_offset_of_MB_CUR_MAX_22() { return static_cast<int32_t>(offsetof(Stdlib_t3308777473_StaticFields, ___MB_CUR_MAX_22)); }
	inline int32_t get_MB_CUR_MAX_22() const { return ___MB_CUR_MAX_22; }
	inline int32_t* get_address_of_MB_CUR_MAX_22() { return &___MB_CUR_MAX_22; }
	inline void set_MB_CUR_MAX_22(int32_t value)
	{
		___MB_CUR_MAX_22 = value;
	}

	inline static int32_t get_offset_of_RAND_MAX_23() { return static_cast<int32_t>(offsetof(Stdlib_t3308777473_StaticFields, ___RAND_MAX_23)); }
	inline int32_t get_RAND_MAX_23() const { return ___RAND_MAX_23; }
	inline int32_t* get_address_of_RAND_MAX_23() { return &___RAND_MAX_23; }
	inline void set_RAND_MAX_23(int32_t value)
	{
		___RAND_MAX_23 = value;
	}

	inline static int32_t get_offset_of_strerror_lock_24() { return static_cast<int32_t>(offsetof(Stdlib_t3308777473_StaticFields, ___strerror_lock_24)); }
	inline Il2CppObject * get_strerror_lock_24() const { return ___strerror_lock_24; }
	inline Il2CppObject ** get_address_of_strerror_lock_24() { return &___strerror_lock_24; }
	inline void set_strerror_lock_24(Il2CppObject * value)
	{
		___strerror_lock_24 = value;
		Il2CppCodeGenWriteBarrier(&___strerror_lock_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
