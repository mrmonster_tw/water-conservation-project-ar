﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_S1737011943.h"
#include "System_ServiceModel_System_ServiceModel_Security_M4027609129.h"

// System.ServiceModel.Security.Tokens.SecurityTokenParameters
struct SecurityTokenParameters_t2868958784;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.SymmetricSecurityBindingElement
struct  SymmetricSecurityBindingElement_t3733530694  : public SecurityBindingElement_t1737011943
{
public:
	// System.ServiceModel.Security.MessageProtectionOrder System.ServiceModel.Channels.SymmetricSecurityBindingElement::msg_protection_order
	int32_t ___msg_protection_order_11;
	// System.ServiceModel.Security.Tokens.SecurityTokenParameters System.ServiceModel.Channels.SymmetricSecurityBindingElement::protection_token_params
	SecurityTokenParameters_t2868958784 * ___protection_token_params_12;
	// System.Boolean System.ServiceModel.Channels.SymmetricSecurityBindingElement::require_sig_confirm
	bool ___require_sig_confirm_13;

public:
	inline static int32_t get_offset_of_msg_protection_order_11() { return static_cast<int32_t>(offsetof(SymmetricSecurityBindingElement_t3733530694, ___msg_protection_order_11)); }
	inline int32_t get_msg_protection_order_11() const { return ___msg_protection_order_11; }
	inline int32_t* get_address_of_msg_protection_order_11() { return &___msg_protection_order_11; }
	inline void set_msg_protection_order_11(int32_t value)
	{
		___msg_protection_order_11 = value;
	}

	inline static int32_t get_offset_of_protection_token_params_12() { return static_cast<int32_t>(offsetof(SymmetricSecurityBindingElement_t3733530694, ___protection_token_params_12)); }
	inline SecurityTokenParameters_t2868958784 * get_protection_token_params_12() const { return ___protection_token_params_12; }
	inline SecurityTokenParameters_t2868958784 ** get_address_of_protection_token_params_12() { return &___protection_token_params_12; }
	inline void set_protection_token_params_12(SecurityTokenParameters_t2868958784 * value)
	{
		___protection_token_params_12 = value;
		Il2CppCodeGenWriteBarrier(&___protection_token_params_12, value);
	}

	inline static int32_t get_offset_of_require_sig_confirm_13() { return static_cast<int32_t>(offsetof(SymmetricSecurityBindingElement_t3733530694, ___require_sig_confirm_13)); }
	inline bool get_require_sig_confirm_13() const { return ___require_sig_confirm_13; }
	inline bool* get_address_of_require_sig_confirm_13() { return &___require_sig_confirm_13; }
	inline void set_require_sig_confirm_13(bool value)
	{
		___require_sig_confirm_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
