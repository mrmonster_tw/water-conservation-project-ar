﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XmlWriter127905479.h"

// System.Text.Encoding
struct Encoding_t1523322056;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDictionaryWriter
struct  XmlDictionaryWriter_t1958650860  : public XmlWriter_t127905479
{
public:
	// System.Int32 System.Xml.XmlDictionaryWriter::depth
	int32_t ___depth_2;

public:
	inline static int32_t get_offset_of_depth_2() { return static_cast<int32_t>(offsetof(XmlDictionaryWriter_t1958650860, ___depth_2)); }
	inline int32_t get_depth_2() const { return ___depth_2; }
	inline int32_t* get_address_of_depth_2() { return &___depth_2; }
	inline void set_depth_2(int32_t value)
	{
		___depth_2 = value;
	}
};

struct XmlDictionaryWriter_t1958650860_StaticFields
{
public:
	// System.Text.Encoding System.Xml.XmlDictionaryWriter::utf8_unmarked
	Encoding_t1523322056 * ___utf8_unmarked_1;

public:
	inline static int32_t get_offset_of_utf8_unmarked_1() { return static_cast<int32_t>(offsetof(XmlDictionaryWriter_t1958650860_StaticFields, ___utf8_unmarked_1)); }
	inline Encoding_t1523322056 * get_utf8_unmarked_1() const { return ___utf8_unmarked_1; }
	inline Encoding_t1523322056 ** get_address_of_utf8_unmarked_1() { return &___utf8_unmarked_1; }
	inline void set_utf8_unmarked_1(Encoding_t1523322056 * value)
	{
		___utf8_unmarked_1 = value;
		Il2CppCodeGenWriteBarrier(&___utf8_unmarked_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
