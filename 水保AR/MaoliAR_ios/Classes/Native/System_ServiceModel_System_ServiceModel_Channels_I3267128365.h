﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_M1904510157.h"

// System.ServiceModel.Channels.ChannelFactoryBase
struct ChannelFactoryBase_t715356738;
// System.ServiceModel.EndpointAddress
struct EndpointAddress_t3119842923;
// System.IdentityModel.Tokens.SecurityToken
struct SecurityToken_t1271873540;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.InitiatorMessageSecurityBindingSupport
struct  InitiatorMessageSecurityBindingSupport_t3267128365  : public MessageSecurityBindingSupport_t1904510157
{
public:
	// System.ServiceModel.Channels.ChannelFactoryBase System.ServiceModel.Channels.InitiatorMessageSecurityBindingSupport::factory
	ChannelFactoryBase_t715356738 * ___factory_6;
	// System.ServiceModel.EndpointAddress System.ServiceModel.Channels.InitiatorMessageSecurityBindingSupport::message_to
	EndpointAddress_t3119842923 * ___message_to_7;
	// System.IdentityModel.Tokens.SecurityToken System.ServiceModel.Channels.InitiatorMessageSecurityBindingSupport::encryption_token
	SecurityToken_t1271873540 * ___encryption_token_8;
	// System.IdentityModel.Tokens.SecurityToken System.ServiceModel.Channels.InitiatorMessageSecurityBindingSupport::signing_token
	SecurityToken_t1271873540 * ___signing_token_9;

public:
	inline static int32_t get_offset_of_factory_6() { return static_cast<int32_t>(offsetof(InitiatorMessageSecurityBindingSupport_t3267128365, ___factory_6)); }
	inline ChannelFactoryBase_t715356738 * get_factory_6() const { return ___factory_6; }
	inline ChannelFactoryBase_t715356738 ** get_address_of_factory_6() { return &___factory_6; }
	inline void set_factory_6(ChannelFactoryBase_t715356738 * value)
	{
		___factory_6 = value;
		Il2CppCodeGenWriteBarrier(&___factory_6, value);
	}

	inline static int32_t get_offset_of_message_to_7() { return static_cast<int32_t>(offsetof(InitiatorMessageSecurityBindingSupport_t3267128365, ___message_to_7)); }
	inline EndpointAddress_t3119842923 * get_message_to_7() const { return ___message_to_7; }
	inline EndpointAddress_t3119842923 ** get_address_of_message_to_7() { return &___message_to_7; }
	inline void set_message_to_7(EndpointAddress_t3119842923 * value)
	{
		___message_to_7 = value;
		Il2CppCodeGenWriteBarrier(&___message_to_7, value);
	}

	inline static int32_t get_offset_of_encryption_token_8() { return static_cast<int32_t>(offsetof(InitiatorMessageSecurityBindingSupport_t3267128365, ___encryption_token_8)); }
	inline SecurityToken_t1271873540 * get_encryption_token_8() const { return ___encryption_token_8; }
	inline SecurityToken_t1271873540 ** get_address_of_encryption_token_8() { return &___encryption_token_8; }
	inline void set_encryption_token_8(SecurityToken_t1271873540 * value)
	{
		___encryption_token_8 = value;
		Il2CppCodeGenWriteBarrier(&___encryption_token_8, value);
	}

	inline static int32_t get_offset_of_signing_token_9() { return static_cast<int32_t>(offsetof(InitiatorMessageSecurityBindingSupport_t3267128365, ___signing_token_9)); }
	inline SecurityToken_t1271873540 * get_signing_token_9() const { return ___signing_token_9; }
	inline SecurityToken_t1271873540 ** get_address_of_signing_token_9() { return &___signing_token_9; }
	inline void set_signing_token_9(SecurityToken_t1271873540 * value)
	{
		___signing_token_9 = value;
		Il2CppCodeGenWriteBarrier(&___signing_token_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
