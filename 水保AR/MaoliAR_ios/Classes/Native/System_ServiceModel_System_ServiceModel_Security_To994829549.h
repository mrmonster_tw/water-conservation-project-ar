﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Selector2079378378.h"
#include "System_ServiceModel_System_ServiceModel_Security_S1263698963.h"
#include "mscorlib_System_TimeSpan881159249.h"

// System.ServiceModel.Security.Tokens.IssuedTokenCommunicationObject
struct IssuedTokenCommunicationObject_t3337991277;
// System.ServiceModel.MessageSecurityVersion
struct MessageSecurityVersion_t2079539966;
// System.ServiceModel.Security.IdentityVerifier
struct IdentityVerifier_t614541315;
// System.Collections.ObjectModel.Collection`1<System.Xml.XmlElement>
struct Collection_1_t3800926332;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.IssuedSecurityTokenProvider
struct  IssuedSecurityTokenProvider_t994829549  : public SecurityTokenProvider_t2079378378
{
public:
	// System.ServiceModel.Security.Tokens.IssuedTokenCommunicationObject System.ServiceModel.Security.Tokens.IssuedSecurityTokenProvider::comm
	IssuedTokenCommunicationObject_t3337991277 * ___comm_0;
	// System.ServiceModel.Security.SecurityKeyEntropyMode System.ServiceModel.Security.Tokens.IssuedSecurityTokenProvider::entropy_mode
	int32_t ___entropy_mode_1;
	// System.TimeSpan System.ServiceModel.Security.Tokens.IssuedSecurityTokenProvider::max_cache_time
	TimeSpan_t881159249  ___max_cache_time_2;
	// System.ServiceModel.MessageSecurityVersion System.ServiceModel.Security.Tokens.IssuedSecurityTokenProvider::version
	MessageSecurityVersion_t2079539966 * ___version_3;
	// System.Int32 System.ServiceModel.Security.Tokens.IssuedSecurityTokenProvider::threshold
	int32_t ___threshold_4;
	// System.ServiceModel.Security.IdentityVerifier System.ServiceModel.Security.Tokens.IssuedSecurityTokenProvider::verifier
	IdentityVerifier_t614541315 * ___verifier_5;
	// System.Boolean System.ServiceModel.Security.Tokens.IssuedSecurityTokenProvider::cache_issued_tokens
	bool ___cache_issued_tokens_6;
	// System.Collections.ObjectModel.Collection`1<System.Xml.XmlElement> System.ServiceModel.Security.Tokens.IssuedSecurityTokenProvider::request_params
	Collection_1_t3800926332 * ___request_params_7;

public:
	inline static int32_t get_offset_of_comm_0() { return static_cast<int32_t>(offsetof(IssuedSecurityTokenProvider_t994829549, ___comm_0)); }
	inline IssuedTokenCommunicationObject_t3337991277 * get_comm_0() const { return ___comm_0; }
	inline IssuedTokenCommunicationObject_t3337991277 ** get_address_of_comm_0() { return &___comm_0; }
	inline void set_comm_0(IssuedTokenCommunicationObject_t3337991277 * value)
	{
		___comm_0 = value;
		Il2CppCodeGenWriteBarrier(&___comm_0, value);
	}

	inline static int32_t get_offset_of_entropy_mode_1() { return static_cast<int32_t>(offsetof(IssuedSecurityTokenProvider_t994829549, ___entropy_mode_1)); }
	inline int32_t get_entropy_mode_1() const { return ___entropy_mode_1; }
	inline int32_t* get_address_of_entropy_mode_1() { return &___entropy_mode_1; }
	inline void set_entropy_mode_1(int32_t value)
	{
		___entropy_mode_1 = value;
	}

	inline static int32_t get_offset_of_max_cache_time_2() { return static_cast<int32_t>(offsetof(IssuedSecurityTokenProvider_t994829549, ___max_cache_time_2)); }
	inline TimeSpan_t881159249  get_max_cache_time_2() const { return ___max_cache_time_2; }
	inline TimeSpan_t881159249 * get_address_of_max_cache_time_2() { return &___max_cache_time_2; }
	inline void set_max_cache_time_2(TimeSpan_t881159249  value)
	{
		___max_cache_time_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(IssuedSecurityTokenProvider_t994829549, ___version_3)); }
	inline MessageSecurityVersion_t2079539966 * get_version_3() const { return ___version_3; }
	inline MessageSecurityVersion_t2079539966 ** get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(MessageSecurityVersion_t2079539966 * value)
	{
		___version_3 = value;
		Il2CppCodeGenWriteBarrier(&___version_3, value);
	}

	inline static int32_t get_offset_of_threshold_4() { return static_cast<int32_t>(offsetof(IssuedSecurityTokenProvider_t994829549, ___threshold_4)); }
	inline int32_t get_threshold_4() const { return ___threshold_4; }
	inline int32_t* get_address_of_threshold_4() { return &___threshold_4; }
	inline void set_threshold_4(int32_t value)
	{
		___threshold_4 = value;
	}

	inline static int32_t get_offset_of_verifier_5() { return static_cast<int32_t>(offsetof(IssuedSecurityTokenProvider_t994829549, ___verifier_5)); }
	inline IdentityVerifier_t614541315 * get_verifier_5() const { return ___verifier_5; }
	inline IdentityVerifier_t614541315 ** get_address_of_verifier_5() { return &___verifier_5; }
	inline void set_verifier_5(IdentityVerifier_t614541315 * value)
	{
		___verifier_5 = value;
		Il2CppCodeGenWriteBarrier(&___verifier_5, value);
	}

	inline static int32_t get_offset_of_cache_issued_tokens_6() { return static_cast<int32_t>(offsetof(IssuedSecurityTokenProvider_t994829549, ___cache_issued_tokens_6)); }
	inline bool get_cache_issued_tokens_6() const { return ___cache_issued_tokens_6; }
	inline bool* get_address_of_cache_issued_tokens_6() { return &___cache_issued_tokens_6; }
	inline void set_cache_issued_tokens_6(bool value)
	{
		___cache_issued_tokens_6 = value;
	}

	inline static int32_t get_offset_of_request_params_7() { return static_cast<int32_t>(offsetof(IssuedSecurityTokenProvider_t994829549, ___request_params_7)); }
	inline Collection_1_t3800926332 * get_request_params_7() const { return ___request_params_7; }
	inline Collection_1_t3800926332 ** get_address_of_request_params_7() { return &___request_params_7; }
	inline void set_request_params_7(Collection_1_t3800926332 * value)
	{
		___request_params_7 = value;
		Il2CppCodeGenWriteBarrier(&___request_params_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
