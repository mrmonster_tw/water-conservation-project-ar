﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_HtmlControls_HtmlContainer641877197.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.HtmlControls.HtmlTextArea
struct  HtmlTextArea_t1156814009  : public HtmlContainerControl_t641877197
{
public:

public:
};

struct HtmlTextArea_t1156814009_StaticFields
{
public:
	// System.Object System.Web.UI.HtmlControls.HtmlTextArea::serverChangeEvent
	Il2CppObject * ___serverChangeEvent_30;

public:
	inline static int32_t get_offset_of_serverChangeEvent_30() { return static_cast<int32_t>(offsetof(HtmlTextArea_t1156814009_StaticFields, ___serverChangeEvent_30)); }
	inline Il2CppObject * get_serverChangeEvent_30() const { return ___serverChangeEvent_30; }
	inline Il2CppObject ** get_address_of_serverChangeEvent_30() { return &___serverChangeEvent_30; }
	inline void set_serverChangeEvent_30(Il2CppObject * value)
	{
		___serverChangeEvent_30 = value;
		Il2CppCodeGenWriteBarrier(&___serverChangeEvent_30, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
