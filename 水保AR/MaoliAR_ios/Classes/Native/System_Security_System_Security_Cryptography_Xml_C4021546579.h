﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Security.Cryptography.Xml.CipherReference
struct CipherReference_t859746092;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.Xml.CipherData
struct  CipherData_t4021546579  : public Il2CppObject
{
public:
	// System.Byte[] System.Security.Cryptography.Xml.CipherData::cipherValue
	ByteU5BU5D_t4116647657* ___cipherValue_0;
	// System.Security.Cryptography.Xml.CipherReference System.Security.Cryptography.Xml.CipherData::cipherReference
	CipherReference_t859746092 * ___cipherReference_1;

public:
	inline static int32_t get_offset_of_cipherValue_0() { return static_cast<int32_t>(offsetof(CipherData_t4021546579, ___cipherValue_0)); }
	inline ByteU5BU5D_t4116647657* get_cipherValue_0() const { return ___cipherValue_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_cipherValue_0() { return &___cipherValue_0; }
	inline void set_cipherValue_0(ByteU5BU5D_t4116647657* value)
	{
		___cipherValue_0 = value;
		Il2CppCodeGenWriteBarrier(&___cipherValue_0, value);
	}

	inline static int32_t get_offset_of_cipherReference_1() { return static_cast<int32_t>(offsetof(CipherData_t4021546579, ___cipherReference_1)); }
	inline CipherReference_t859746092 * get_cipherReference_1() const { return ___cipherReference_1; }
	inline CipherReference_t859746092 ** get_address_of_cipherReference_1() { return &___cipherReference_1; }
	inline void set_cipherReference_1(CipherReference_t859746092 * value)
	{
		___cipherReference_1 = value;
		Il2CppCodeGenWriteBarrier(&___cipherReference_1, value);
	}
};

struct CipherData_t4021546579_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.Xml.CipherData::<>f__switch$map1
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map1_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_2() { return static_cast<int32_t>(offsetof(CipherData_t4021546579_StaticFields, ___U3CU3Ef__switchU24map1_2)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map1_2() const { return ___U3CU3Ef__switchU24map1_2; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map1_2() { return &___U3CU3Ef__switchU24map1_2; }
	inline void set_U3CU3Ef__switchU24map1_2(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
