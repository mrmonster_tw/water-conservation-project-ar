﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Web_System_Web_UI_DataSourceCapabilities29834462.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.DataSourceSelectArguments
struct  DataSourceSelectArguments_t3537378383  : public Il2CppObject
{
public:
	// System.String System.Web.UI.DataSourceSelectArguments::sortExpression
	String_t* ___sortExpression_0;
	// System.Int32 System.Web.UI.DataSourceSelectArguments::startingRowIndex
	int32_t ___startingRowIndex_1;
	// System.Int32 System.Web.UI.DataSourceSelectArguments::maxRows
	int32_t ___maxRows_2;
	// System.Boolean System.Web.UI.DataSourceSelectArguments::getTotalRowCount
	bool ___getTotalRowCount_3;
	// System.Int32 System.Web.UI.DataSourceSelectArguments::totalRowCount
	int32_t ___totalRowCount_4;
	// System.Web.UI.DataSourceCapabilities System.Web.UI.DataSourceSelectArguments::dsc
	int32_t ___dsc_5;

public:
	inline static int32_t get_offset_of_sortExpression_0() { return static_cast<int32_t>(offsetof(DataSourceSelectArguments_t3537378383, ___sortExpression_0)); }
	inline String_t* get_sortExpression_0() const { return ___sortExpression_0; }
	inline String_t** get_address_of_sortExpression_0() { return &___sortExpression_0; }
	inline void set_sortExpression_0(String_t* value)
	{
		___sortExpression_0 = value;
		Il2CppCodeGenWriteBarrier(&___sortExpression_0, value);
	}

	inline static int32_t get_offset_of_startingRowIndex_1() { return static_cast<int32_t>(offsetof(DataSourceSelectArguments_t3537378383, ___startingRowIndex_1)); }
	inline int32_t get_startingRowIndex_1() const { return ___startingRowIndex_1; }
	inline int32_t* get_address_of_startingRowIndex_1() { return &___startingRowIndex_1; }
	inline void set_startingRowIndex_1(int32_t value)
	{
		___startingRowIndex_1 = value;
	}

	inline static int32_t get_offset_of_maxRows_2() { return static_cast<int32_t>(offsetof(DataSourceSelectArguments_t3537378383, ___maxRows_2)); }
	inline int32_t get_maxRows_2() const { return ___maxRows_2; }
	inline int32_t* get_address_of_maxRows_2() { return &___maxRows_2; }
	inline void set_maxRows_2(int32_t value)
	{
		___maxRows_2 = value;
	}

	inline static int32_t get_offset_of_getTotalRowCount_3() { return static_cast<int32_t>(offsetof(DataSourceSelectArguments_t3537378383, ___getTotalRowCount_3)); }
	inline bool get_getTotalRowCount_3() const { return ___getTotalRowCount_3; }
	inline bool* get_address_of_getTotalRowCount_3() { return &___getTotalRowCount_3; }
	inline void set_getTotalRowCount_3(bool value)
	{
		___getTotalRowCount_3 = value;
	}

	inline static int32_t get_offset_of_totalRowCount_4() { return static_cast<int32_t>(offsetof(DataSourceSelectArguments_t3537378383, ___totalRowCount_4)); }
	inline int32_t get_totalRowCount_4() const { return ___totalRowCount_4; }
	inline int32_t* get_address_of_totalRowCount_4() { return &___totalRowCount_4; }
	inline void set_totalRowCount_4(int32_t value)
	{
		___totalRowCount_4 = value;
	}

	inline static int32_t get_offset_of_dsc_5() { return static_cast<int32_t>(offsetof(DataSourceSelectArguments_t3537378383, ___dsc_5)); }
	inline int32_t get_dsc_5() const { return ___dsc_5; }
	inline int32_t* get_address_of_dsc_5() { return &___dsc_5; }
	inline void set_dsc_5(int32_t value)
	{
		___dsc_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
