﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_System_ServiceModel_PeerResolv2102618862.h"
#include "System_ServiceModel_System_ServiceModel_PeerResolv1552258726.h"

// System.ServiceModel.PeerResolvers.PeerCustomResolverSettings
struct PeerCustomResolverSettings_t3246124786;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.PeerResolvers.PeerResolverSettings
struct  PeerResolverSettings_t1743278054  : public Il2CppObject
{
public:
	// System.ServiceModel.PeerResolvers.PeerCustomResolverSettings System.ServiceModel.PeerResolvers.PeerResolverSettings::custom
	PeerCustomResolverSettings_t3246124786 * ___custom_0;
	// System.ServiceModel.PeerResolvers.PeerResolverMode System.ServiceModel.PeerResolvers.PeerResolverSettings::<Mode>k__BackingField
	int32_t ___U3CModeU3Ek__BackingField_1;
	// System.ServiceModel.PeerResolvers.PeerReferralPolicy System.ServiceModel.PeerResolvers.PeerResolverSettings::<ReferralPolicy>k__BackingField
	int32_t ___U3CReferralPolicyU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_custom_0() { return static_cast<int32_t>(offsetof(PeerResolverSettings_t1743278054, ___custom_0)); }
	inline PeerCustomResolverSettings_t3246124786 * get_custom_0() const { return ___custom_0; }
	inline PeerCustomResolverSettings_t3246124786 ** get_address_of_custom_0() { return &___custom_0; }
	inline void set_custom_0(PeerCustomResolverSettings_t3246124786 * value)
	{
		___custom_0 = value;
		Il2CppCodeGenWriteBarrier(&___custom_0, value);
	}

	inline static int32_t get_offset_of_U3CModeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PeerResolverSettings_t1743278054, ___U3CModeU3Ek__BackingField_1)); }
	inline int32_t get_U3CModeU3Ek__BackingField_1() const { return ___U3CModeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CModeU3Ek__BackingField_1() { return &___U3CModeU3Ek__BackingField_1; }
	inline void set_U3CModeU3Ek__BackingField_1(int32_t value)
	{
		___U3CModeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CReferralPolicyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PeerResolverSettings_t1743278054, ___U3CReferralPolicyU3Ek__BackingField_2)); }
	inline int32_t get_U3CReferralPolicyU3Ek__BackingField_2() const { return ___U3CReferralPolicyU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CReferralPolicyU3Ek__BackingField_2() { return &___U3CReferralPolicyU3Ek__BackingField_2; }
	inline void set_U3CReferralPolicyU3Ek__BackingField_2(int32_t value)
	{
		___U3CReferralPolicyU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
