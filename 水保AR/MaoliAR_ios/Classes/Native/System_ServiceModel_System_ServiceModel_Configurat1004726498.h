﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Xml.XmlReader
struct XmlReader_t3121518892;
// System.ServiceModel.Channels.AddressHeader
struct AddressHeader_t3096310965;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.AddressHeaderCollectionElement/<DeserializeAddressHeaders>c__Iterator4
struct  U3CDeserializeAddressHeadersU3Ec__Iterator4_t1004726498  : public Il2CppObject
{
public:
	// System.Xml.XmlReader System.ServiceModel.Configuration.AddressHeaderCollectionElement/<DeserializeAddressHeaders>c__Iterator4::reader
	XmlReader_t3121518892 * ___reader_0;
	// System.Int32 System.ServiceModel.Configuration.AddressHeaderCollectionElement/<DeserializeAddressHeaders>c__Iterator4::$PC
	int32_t ___U24PC_1;
	// System.ServiceModel.Channels.AddressHeader System.ServiceModel.Configuration.AddressHeaderCollectionElement/<DeserializeAddressHeaders>c__Iterator4::$current
	AddressHeader_t3096310965 * ___U24current_2;
	// System.Xml.XmlReader System.ServiceModel.Configuration.AddressHeaderCollectionElement/<DeserializeAddressHeaders>c__Iterator4::<$>reader
	XmlReader_t3121518892 * ___U3CU24U3Ereader_3;

public:
	inline static int32_t get_offset_of_reader_0() { return static_cast<int32_t>(offsetof(U3CDeserializeAddressHeadersU3Ec__Iterator4_t1004726498, ___reader_0)); }
	inline XmlReader_t3121518892 * get_reader_0() const { return ___reader_0; }
	inline XmlReader_t3121518892 ** get_address_of_reader_0() { return &___reader_0; }
	inline void set_reader_0(XmlReader_t3121518892 * value)
	{
		___reader_0 = value;
		Il2CppCodeGenWriteBarrier(&___reader_0, value);
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CDeserializeAddressHeadersU3Ec__Iterator4_t1004726498, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDeserializeAddressHeadersU3Ec__Iterator4_t1004726498, ___U24current_2)); }
	inline AddressHeader_t3096310965 * get_U24current_2() const { return ___U24current_2; }
	inline AddressHeader_t3096310965 ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(AddressHeader_t3096310965 * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ereader_3() { return static_cast<int32_t>(offsetof(U3CDeserializeAddressHeadersU3Ec__Iterator4_t1004726498, ___U3CU24U3Ereader_3)); }
	inline XmlReader_t3121518892 * get_U3CU24U3Ereader_3() const { return ___U3CU24U3Ereader_3; }
	inline XmlReader_t3121518892 ** get_address_of_U3CU24U3Ereader_3() { return &___U3CU24U3Ereader_3; }
	inline void set_U3CU24U3Ereader_3(XmlReader_t3121518892 * value)
	{
		___U3CU24U3Ereader_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ereader_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
