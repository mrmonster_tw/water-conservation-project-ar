﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_C1586116283.h"

// System.ServiceModel.Channels.ChannelListenerBase
struct ChannelListenerBase_t990592377;
// System.ServiceModel.EndpointAddress
struct EndpointAddress_t3119842923;
// System.Uri
struct Uri_t100236324;
// System.Action`1<System.TimeSpan>
struct Action_1_t1053626844;
// System.ServiceModel.Channels.DuplexChannelBase/AsyncSendHandler
struct AsyncSendHandler_t4202970142;
// System.ServiceModel.Channels.DuplexChannelBase/AsyncReceiveHandler
struct AsyncReceiveHandler_t405913978;
// System.ServiceModel.Channels.DuplexChannelBase/TryReceiveHandler
struct TryReceiveHandler_t28742579;
// System.ServiceModel.Channels.DuplexChannelBase/AsyncWaitForMessageHandler
struct AsyncWaitForMessageHandler_t1048830322;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.DuplexChannelBase
struct  DuplexChannelBase_t2906515918  : public ChannelBase_t1586116283
{
public:
	// System.ServiceModel.Channels.ChannelListenerBase System.ServiceModel.Channels.DuplexChannelBase::channel_listener_base
	ChannelListenerBase_t990592377 * ___channel_listener_base_10;
	// System.ServiceModel.EndpointAddress System.ServiceModel.Channels.DuplexChannelBase::local_address
	EndpointAddress_t3119842923 * ___local_address_11;
	// System.ServiceModel.EndpointAddress System.ServiceModel.Channels.DuplexChannelBase::remote_address
	EndpointAddress_t3119842923 * ___remote_address_12;
	// System.Uri System.ServiceModel.Channels.DuplexChannelBase::via
	Uri_t100236324 * ___via_13;
	// System.Action`1<System.TimeSpan> System.ServiceModel.Channels.DuplexChannelBase::open_handler
	Action_1_t1053626844 * ___open_handler_14;
	// System.Action`1<System.TimeSpan> System.ServiceModel.Channels.DuplexChannelBase::close_handler
	Action_1_t1053626844 * ___close_handler_15;
	// System.ServiceModel.Channels.DuplexChannelBase/AsyncSendHandler System.ServiceModel.Channels.DuplexChannelBase::send_handler
	AsyncSendHandler_t4202970142 * ___send_handler_16;
	// System.ServiceModel.Channels.DuplexChannelBase/AsyncReceiveHandler System.ServiceModel.Channels.DuplexChannelBase::receive_handler
	AsyncReceiveHandler_t405913978 * ___receive_handler_17;
	// System.ServiceModel.Channels.DuplexChannelBase/TryReceiveHandler System.ServiceModel.Channels.DuplexChannelBase::try_receive_handler
	TryReceiveHandler_t28742579 * ___try_receive_handler_18;
	// System.ServiceModel.Channels.DuplexChannelBase/AsyncWaitForMessageHandler System.ServiceModel.Channels.DuplexChannelBase::wait_handler
	AsyncWaitForMessageHandler_t1048830322 * ___wait_handler_19;

public:
	inline static int32_t get_offset_of_channel_listener_base_10() { return static_cast<int32_t>(offsetof(DuplexChannelBase_t2906515918, ___channel_listener_base_10)); }
	inline ChannelListenerBase_t990592377 * get_channel_listener_base_10() const { return ___channel_listener_base_10; }
	inline ChannelListenerBase_t990592377 ** get_address_of_channel_listener_base_10() { return &___channel_listener_base_10; }
	inline void set_channel_listener_base_10(ChannelListenerBase_t990592377 * value)
	{
		___channel_listener_base_10 = value;
		Il2CppCodeGenWriteBarrier(&___channel_listener_base_10, value);
	}

	inline static int32_t get_offset_of_local_address_11() { return static_cast<int32_t>(offsetof(DuplexChannelBase_t2906515918, ___local_address_11)); }
	inline EndpointAddress_t3119842923 * get_local_address_11() const { return ___local_address_11; }
	inline EndpointAddress_t3119842923 ** get_address_of_local_address_11() { return &___local_address_11; }
	inline void set_local_address_11(EndpointAddress_t3119842923 * value)
	{
		___local_address_11 = value;
		Il2CppCodeGenWriteBarrier(&___local_address_11, value);
	}

	inline static int32_t get_offset_of_remote_address_12() { return static_cast<int32_t>(offsetof(DuplexChannelBase_t2906515918, ___remote_address_12)); }
	inline EndpointAddress_t3119842923 * get_remote_address_12() const { return ___remote_address_12; }
	inline EndpointAddress_t3119842923 ** get_address_of_remote_address_12() { return &___remote_address_12; }
	inline void set_remote_address_12(EndpointAddress_t3119842923 * value)
	{
		___remote_address_12 = value;
		Il2CppCodeGenWriteBarrier(&___remote_address_12, value);
	}

	inline static int32_t get_offset_of_via_13() { return static_cast<int32_t>(offsetof(DuplexChannelBase_t2906515918, ___via_13)); }
	inline Uri_t100236324 * get_via_13() const { return ___via_13; }
	inline Uri_t100236324 ** get_address_of_via_13() { return &___via_13; }
	inline void set_via_13(Uri_t100236324 * value)
	{
		___via_13 = value;
		Il2CppCodeGenWriteBarrier(&___via_13, value);
	}

	inline static int32_t get_offset_of_open_handler_14() { return static_cast<int32_t>(offsetof(DuplexChannelBase_t2906515918, ___open_handler_14)); }
	inline Action_1_t1053626844 * get_open_handler_14() const { return ___open_handler_14; }
	inline Action_1_t1053626844 ** get_address_of_open_handler_14() { return &___open_handler_14; }
	inline void set_open_handler_14(Action_1_t1053626844 * value)
	{
		___open_handler_14 = value;
		Il2CppCodeGenWriteBarrier(&___open_handler_14, value);
	}

	inline static int32_t get_offset_of_close_handler_15() { return static_cast<int32_t>(offsetof(DuplexChannelBase_t2906515918, ___close_handler_15)); }
	inline Action_1_t1053626844 * get_close_handler_15() const { return ___close_handler_15; }
	inline Action_1_t1053626844 ** get_address_of_close_handler_15() { return &___close_handler_15; }
	inline void set_close_handler_15(Action_1_t1053626844 * value)
	{
		___close_handler_15 = value;
		Il2CppCodeGenWriteBarrier(&___close_handler_15, value);
	}

	inline static int32_t get_offset_of_send_handler_16() { return static_cast<int32_t>(offsetof(DuplexChannelBase_t2906515918, ___send_handler_16)); }
	inline AsyncSendHandler_t4202970142 * get_send_handler_16() const { return ___send_handler_16; }
	inline AsyncSendHandler_t4202970142 ** get_address_of_send_handler_16() { return &___send_handler_16; }
	inline void set_send_handler_16(AsyncSendHandler_t4202970142 * value)
	{
		___send_handler_16 = value;
		Il2CppCodeGenWriteBarrier(&___send_handler_16, value);
	}

	inline static int32_t get_offset_of_receive_handler_17() { return static_cast<int32_t>(offsetof(DuplexChannelBase_t2906515918, ___receive_handler_17)); }
	inline AsyncReceiveHandler_t405913978 * get_receive_handler_17() const { return ___receive_handler_17; }
	inline AsyncReceiveHandler_t405913978 ** get_address_of_receive_handler_17() { return &___receive_handler_17; }
	inline void set_receive_handler_17(AsyncReceiveHandler_t405913978 * value)
	{
		___receive_handler_17 = value;
		Il2CppCodeGenWriteBarrier(&___receive_handler_17, value);
	}

	inline static int32_t get_offset_of_try_receive_handler_18() { return static_cast<int32_t>(offsetof(DuplexChannelBase_t2906515918, ___try_receive_handler_18)); }
	inline TryReceiveHandler_t28742579 * get_try_receive_handler_18() const { return ___try_receive_handler_18; }
	inline TryReceiveHandler_t28742579 ** get_address_of_try_receive_handler_18() { return &___try_receive_handler_18; }
	inline void set_try_receive_handler_18(TryReceiveHandler_t28742579 * value)
	{
		___try_receive_handler_18 = value;
		Il2CppCodeGenWriteBarrier(&___try_receive_handler_18, value);
	}

	inline static int32_t get_offset_of_wait_handler_19() { return static_cast<int32_t>(offsetof(DuplexChannelBase_t2906515918, ___wait_handler_19)); }
	inline AsyncWaitForMessageHandler_t1048830322 * get_wait_handler_19() const { return ___wait_handler_19; }
	inline AsyncWaitForMessageHandler_t1048830322 ** get_address_of_wait_handler_19() { return &___wait_handler_19; }
	inline void set_wait_handler_19(AsyncWaitForMessageHandler_t1048830322 * value)
	{
		___wait_handler_19 = value;
		Il2CppCodeGenWriteBarrier(&___wait_handler_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
