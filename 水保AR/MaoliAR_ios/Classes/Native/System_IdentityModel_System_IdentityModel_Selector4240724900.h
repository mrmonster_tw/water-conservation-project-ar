﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.IdentityModel.Selectors.UserNamePasswordValidator
struct UserNamePasswordValidator_t4240724900;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Selectors.UserNamePasswordValidator
struct  UserNamePasswordValidator_t4240724900  : public Il2CppObject
{
public:

public:
};

struct UserNamePasswordValidator_t4240724900_StaticFields
{
public:
	// System.IdentityModel.Selectors.UserNamePasswordValidator System.IdentityModel.Selectors.UserNamePasswordValidator::none_validator
	UserNamePasswordValidator_t4240724900 * ___none_validator_0;

public:
	inline static int32_t get_offset_of_none_validator_0() { return static_cast<int32_t>(offsetof(UserNamePasswordValidator_t4240724900_StaticFields, ___none_validator_0)); }
	inline UserNamePasswordValidator_t4240724900 * get_none_validator_0() const { return ___none_validator_0; }
	inline UserNamePasswordValidator_t4240724900 ** get_address_of_none_validator_0() { return &___none_validator_0; }
	inline void set_none_validator_0(UserNamePasswordValidator_t4240724900 * value)
	{
		___none_validator_0 = value;
		Il2CppCodeGenWriteBarrier(&___none_validator_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
