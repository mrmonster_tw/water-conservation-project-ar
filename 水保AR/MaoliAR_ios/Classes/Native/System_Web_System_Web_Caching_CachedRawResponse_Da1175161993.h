﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Byte[]
struct ByteU5BU5D_t4116647657;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Caching.CachedRawResponse/DataItem
struct  DataItem_t1175161993  : public Il2CppObject
{
public:
	// System.Byte[] System.Web.Caching.CachedRawResponse/DataItem::Buffer
	ByteU5BU5D_t4116647657* ___Buffer_0;
	// System.Int64 System.Web.Caching.CachedRawResponse/DataItem::Length
	int64_t ___Length_1;

public:
	inline static int32_t get_offset_of_Buffer_0() { return static_cast<int32_t>(offsetof(DataItem_t1175161993, ___Buffer_0)); }
	inline ByteU5BU5D_t4116647657* get_Buffer_0() const { return ___Buffer_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_Buffer_0() { return &___Buffer_0; }
	inline void set_Buffer_0(ByteU5BU5D_t4116647657* value)
	{
		___Buffer_0 = value;
		Il2CppCodeGenWriteBarrier(&___Buffer_0, value);
	}

	inline static int32_t get_offset_of_Length_1() { return static_cast<int32_t>(offsetof(DataItem_t1175161993, ___Length_1)); }
	inline int64_t get_Length_1() const { return ___Length_1; }
	inline int64_t* get_address_of_Length_1() { return &___Length_1; }
	inline void set_Length_1(int64_t value)
	{
		___Length_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
