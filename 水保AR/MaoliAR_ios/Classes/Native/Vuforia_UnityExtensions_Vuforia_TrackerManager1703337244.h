﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// Vuforia.TrackerManager
struct TrackerManager_t1703337244;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackerManager
struct  TrackerManager_t1703337244  : public Il2CppObject
{
public:

public:
};

struct TrackerManager_t1703337244_StaticFields
{
public:
	// Vuforia.TrackerManager Vuforia.TrackerManager::mInstance
	TrackerManager_t1703337244 * ___mInstance_0;

public:
	inline static int32_t get_offset_of_mInstance_0() { return static_cast<int32_t>(offsetof(TrackerManager_t1703337244_StaticFields, ___mInstance_0)); }
	inline TrackerManager_t1703337244 * get_mInstance_0() const { return ___mInstance_0; }
	inline TrackerManager_t1703337244 ** get_address_of_mInstance_0() { return &___mInstance_0; }
	inline void set_mInstance_0(TrackerManager_t1703337244 * value)
	{
		___mInstance_0 = value;
		Il2CppCodeGenWriteBarrier(&___mInstance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
