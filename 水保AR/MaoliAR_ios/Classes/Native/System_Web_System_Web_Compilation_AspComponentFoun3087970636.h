﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Collections.Generic.Dictionary`2<System.String,System.Web.Compilation.AspComponent>
struct Dictionary_2_t376032173;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.AspComponentFoundry
struct  AspComponentFoundry_t3087970636  : public Il2CppObject
{
public:
	// System.Collections.Hashtable System.Web.Compilation.AspComponentFoundry::foundries
	Hashtable_t1853889766 * ___foundries_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Web.Compilation.AspComponent> System.Web.Compilation.AspComponentFoundry::components
	Dictionary_2_t376032173 * ___components_1;

public:
	inline static int32_t get_offset_of_foundries_0() { return static_cast<int32_t>(offsetof(AspComponentFoundry_t3087970636, ___foundries_0)); }
	inline Hashtable_t1853889766 * get_foundries_0() const { return ___foundries_0; }
	inline Hashtable_t1853889766 ** get_address_of_foundries_0() { return &___foundries_0; }
	inline void set_foundries_0(Hashtable_t1853889766 * value)
	{
		___foundries_0 = value;
		Il2CppCodeGenWriteBarrier(&___foundries_0, value);
	}

	inline static int32_t get_offset_of_components_1() { return static_cast<int32_t>(offsetof(AspComponentFoundry_t3087970636, ___components_1)); }
	inline Dictionary_2_t376032173 * get_components_1() const { return ___components_1; }
	inline Dictionary_2_t376032173 ** get_address_of_components_1() { return &___components_1; }
	inline void set_components_1(Dictionary_2_t376032173 * value)
	{
		___components_1 = value;
		Il2CppCodeGenWriteBarrier(&___components_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
