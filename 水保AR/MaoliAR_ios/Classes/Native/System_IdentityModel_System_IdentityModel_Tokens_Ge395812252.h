﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Tokens_S1271873540.h"

// System.Xml.XmlElement
struct XmlElement_t561603118;
// System.IdentityModel.Tokens.SecurityToken
struct SecurityToken_t1271873540;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Tokens.GenericXmlSecurityToken
struct  GenericXmlSecurityToken_t395812252  : public SecurityToken_t1271873540
{
public:
	// System.Xml.XmlElement System.IdentityModel.Tokens.GenericXmlSecurityToken::xml
	XmlElement_t561603118 * ___xml_0;
	// System.IdentityModel.Tokens.SecurityToken System.IdentityModel.Tokens.GenericXmlSecurityToken::proof_token
	SecurityToken_t1271873540 * ___proof_token_1;

public:
	inline static int32_t get_offset_of_xml_0() { return static_cast<int32_t>(offsetof(GenericXmlSecurityToken_t395812252, ___xml_0)); }
	inline XmlElement_t561603118 * get_xml_0() const { return ___xml_0; }
	inline XmlElement_t561603118 ** get_address_of_xml_0() { return &___xml_0; }
	inline void set_xml_0(XmlElement_t561603118 * value)
	{
		___xml_0 = value;
		Il2CppCodeGenWriteBarrier(&___xml_0, value);
	}

	inline static int32_t get_offset_of_proof_token_1() { return static_cast<int32_t>(offsetof(GenericXmlSecurityToken_t395812252, ___proof_token_1)); }
	inline SecurityToken_t1271873540 * get_proof_token_1() const { return ___proof_token_1; }
	inline SecurityToken_t1271873540 ** get_address_of_proof_token_1() { return &___proof_token_1; }
	inline void set_proof_token_1(SecurityToken_t1271873540 * value)
	{
		___proof_token_1 = value;
		Il2CppCodeGenWriteBarrier(&___proof_token_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
