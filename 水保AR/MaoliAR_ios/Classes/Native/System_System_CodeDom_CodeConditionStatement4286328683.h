﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeStatement371410868.h"

// System.CodeDom.CodeExpression
struct CodeExpression_t2166265795;
// System.CodeDom.CodeStatementCollection
struct CodeStatementCollection_t1265263501;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeConditionStatement
struct  CodeConditionStatement_t4286328683  : public CodeStatement_t371410868
{
public:
	// System.CodeDom.CodeExpression System.CodeDom.CodeConditionStatement::condition
	CodeExpression_t2166265795 * ___condition_4;
	// System.CodeDom.CodeStatementCollection System.CodeDom.CodeConditionStatement::trueStatements
	CodeStatementCollection_t1265263501 * ___trueStatements_5;
	// System.CodeDom.CodeStatementCollection System.CodeDom.CodeConditionStatement::falseStatements
	CodeStatementCollection_t1265263501 * ___falseStatements_6;

public:
	inline static int32_t get_offset_of_condition_4() { return static_cast<int32_t>(offsetof(CodeConditionStatement_t4286328683, ___condition_4)); }
	inline CodeExpression_t2166265795 * get_condition_4() const { return ___condition_4; }
	inline CodeExpression_t2166265795 ** get_address_of_condition_4() { return &___condition_4; }
	inline void set_condition_4(CodeExpression_t2166265795 * value)
	{
		___condition_4 = value;
		Il2CppCodeGenWriteBarrier(&___condition_4, value);
	}

	inline static int32_t get_offset_of_trueStatements_5() { return static_cast<int32_t>(offsetof(CodeConditionStatement_t4286328683, ___trueStatements_5)); }
	inline CodeStatementCollection_t1265263501 * get_trueStatements_5() const { return ___trueStatements_5; }
	inline CodeStatementCollection_t1265263501 ** get_address_of_trueStatements_5() { return &___trueStatements_5; }
	inline void set_trueStatements_5(CodeStatementCollection_t1265263501 * value)
	{
		___trueStatements_5 = value;
		Il2CppCodeGenWriteBarrier(&___trueStatements_5, value);
	}

	inline static int32_t get_offset_of_falseStatements_6() { return static_cast<int32_t>(offsetof(CodeConditionStatement_t4286328683, ___falseStatements_6)); }
	inline CodeStatementCollection_t1265263501 * get_falseStatements_6() const { return ___falseStatements_6; }
	inline CodeStatementCollection_t1265263501 ** get_address_of_falseStatements_6() { return &___falseStatements_6; }
	inline void set_falseStatements_6(CodeStatementCollection_t1265263501 * value)
	{
		___falseStatements_6 = value;
		Il2CppCodeGenWriteBarrier(&___falseStatements_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
