﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Security_T3373317487.h"

// System.ServiceModel.Description.WSTrustSecurityTokenServiceProxy
struct WSTrustSecurityTokenServiceProxy_t2160987012;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.IssuedTokenCommunicationObject
struct  IssuedTokenCommunicationObject_t3337991277  : public ProviderCommunicationObject_t3373317487
{
public:
	// System.ServiceModel.Description.WSTrustSecurityTokenServiceProxy System.ServiceModel.Security.Tokens.IssuedTokenCommunicationObject::comm
	WSTrustSecurityTokenServiceProxy_t2160987012 * ___comm_15;

public:
	inline static int32_t get_offset_of_comm_15() { return static_cast<int32_t>(offsetof(IssuedTokenCommunicationObject_t3337991277, ___comm_15)); }
	inline WSTrustSecurityTokenServiceProxy_t2160987012 * get_comm_15() const { return ___comm_15; }
	inline WSTrustSecurityTokenServiceProxy_t2160987012 ** get_address_of_comm_15() { return &___comm_15; }
	inline void set_comm_15(WSTrustSecurityTokenServiceProxy_t2160987012 * value)
	{
		___comm_15 = value;
		Il2CppCodeGenWriteBarrier(&___comm_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
