﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_MulticastDelegate157516450.h"
#include "mscorlib_System_Void1185182177.h"
#include "mscorlib_System_Int322950945753.h"

// UIWidget
struct UIWidget_t3538521925;
// BetterList`1<UnityEngine.Vector3>
struct BetterList_1_t2877333782;
// BetterList`1<UnityEngine.Vector2>
struct BetterList_1_t1311249841;
// BetterList`1<UnityEngine.Color32>
struct BetterList_1_t1755521610;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWidget/OnPostFillCallback
struct  OnPostFillCallback_t2835645043  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
