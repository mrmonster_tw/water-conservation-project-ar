﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Xml.XmlDictionaryString
struct XmlDictionaryString_t3504120266;
// System.Xml.IXmlDictionary
struct IXmlDictionary_t3978089843;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDictionaryString
struct  XmlDictionaryString_t3504120266  : public Il2CppObject
{
public:
	// System.Xml.IXmlDictionary System.Xml.XmlDictionaryString::dict
	Il2CppObject * ___dict_1;
	// System.String System.Xml.XmlDictionaryString::value
	String_t* ___value_2;
	// System.Int32 System.Xml.XmlDictionaryString::key
	int32_t ___key_3;

public:
	inline static int32_t get_offset_of_dict_1() { return static_cast<int32_t>(offsetof(XmlDictionaryString_t3504120266, ___dict_1)); }
	inline Il2CppObject * get_dict_1() const { return ___dict_1; }
	inline Il2CppObject ** get_address_of_dict_1() { return &___dict_1; }
	inline void set_dict_1(Il2CppObject * value)
	{
		___dict_1 = value;
		Il2CppCodeGenWriteBarrier(&___dict_1, value);
	}

	inline static int32_t get_offset_of_value_2() { return static_cast<int32_t>(offsetof(XmlDictionaryString_t3504120266, ___value_2)); }
	inline String_t* get_value_2() const { return ___value_2; }
	inline String_t** get_address_of_value_2() { return &___value_2; }
	inline void set_value_2(String_t* value)
	{
		___value_2 = value;
		Il2CppCodeGenWriteBarrier(&___value_2, value);
	}

	inline static int32_t get_offset_of_key_3() { return static_cast<int32_t>(offsetof(XmlDictionaryString_t3504120266, ___key_3)); }
	inline int32_t get_key_3() const { return ___key_3; }
	inline int32_t* get_address_of_key_3() { return &___key_3; }
	inline void set_key_3(int32_t value)
	{
		___key_3 = value;
	}
};

struct XmlDictionaryString_t3504120266_StaticFields
{
public:
	// System.Xml.XmlDictionaryString System.Xml.XmlDictionaryString::empty
	XmlDictionaryString_t3504120266 * ___empty_0;

public:
	inline static int32_t get_offset_of_empty_0() { return static_cast<int32_t>(offsetof(XmlDictionaryString_t3504120266_StaticFields, ___empty_0)); }
	inline XmlDictionaryString_t3504120266 * get_empty_0() const { return ___empty_0; }
	inline XmlDictionaryString_t3504120266 ** get_address_of_empty_0() { return &___empty_0; }
	inline void set_empty_0(XmlDictionaryString_t3504120266 * value)
	{
		___empty_0 = value;
		Il2CppCodeGenWriteBarrier(&___empty_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
