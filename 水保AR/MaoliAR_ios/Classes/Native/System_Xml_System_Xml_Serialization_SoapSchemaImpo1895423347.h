﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Serialization_SchemaImporter2185830201.h"

// System.Xml.Serialization.XmlSchemaImporter
struct XmlSchemaImporter_t2852143304;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.SoapSchemaImporter
struct  SoapSchemaImporter_t1895423347  : public SchemaImporter_t2185830201
{
public:
	// System.Xml.Serialization.XmlSchemaImporter System.Xml.Serialization.SoapSchemaImporter::_importer
	XmlSchemaImporter_t2852143304 * ____importer_0;

public:
	inline static int32_t get_offset_of__importer_0() { return static_cast<int32_t>(offsetof(SoapSchemaImporter_t1895423347, ____importer_0)); }
	inline XmlSchemaImporter_t2852143304 * get__importer_0() const { return ____importer_0; }
	inline XmlSchemaImporter_t2852143304 ** get_address_of__importer_0() { return &____importer_0; }
	inline void set__importer_0(XmlSchemaImporter_t2852143304 * value)
	{
		____importer_0 = value;
		Il2CppCodeGenWriteBarrier(&____importer_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
