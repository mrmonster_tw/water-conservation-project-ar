﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.UI.Text
struct Text_t1901882714;
// System.Predicate`1<UnityEngine.UI.Text>
struct Predicate_1_t2727176838;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tips_Controller
struct  Tips_Controller_t170528621  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject Tips_Controller::existGameBtn
	GameObject_t1113636619 * ___existGameBtn_2;
	// UnityEngine.GameObject[] Tips_Controller::Level_Tips
	GameObjectU5BU5D_t3328599146* ___Level_Tips_3;
	// UnityEngine.GameObject[] Tips_Controller::Level_bt
	GameObjectU5BU5D_t3328599146* ___Level_bt_4;
	// UnityEngine.GameObject[] Tips_Controller::Level_bg
	GameObjectU5BU5D_t3328599146* ___Level_bg_5;
	// UnityEngine.GameObject[] Tips_Controller::All_Images
	GameObjectU5BU5D_t3328599146* ___All_Images_6;
	// UnityEngine.GameObject[] Tips_Controller::All_Images_notMobile
	GameObjectU5BU5D_t3328599146* ___All_Images_notMobile_7;
	// UnityEngine.GameObject Tips_Controller::bt_skip
	GameObject_t1113636619 * ___bt_skip_8;
	// UnityEngine.UI.Text Tips_Controller::請點擊任意處以繼續遊戲
	Text_t1901882714 * ___U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_9;

public:
	inline static int32_t get_offset_of_existGameBtn_2() { return static_cast<int32_t>(offsetof(Tips_Controller_t170528621, ___existGameBtn_2)); }
	inline GameObject_t1113636619 * get_existGameBtn_2() const { return ___existGameBtn_2; }
	inline GameObject_t1113636619 ** get_address_of_existGameBtn_2() { return &___existGameBtn_2; }
	inline void set_existGameBtn_2(GameObject_t1113636619 * value)
	{
		___existGameBtn_2 = value;
		Il2CppCodeGenWriteBarrier(&___existGameBtn_2, value);
	}

	inline static int32_t get_offset_of_Level_Tips_3() { return static_cast<int32_t>(offsetof(Tips_Controller_t170528621, ___Level_Tips_3)); }
	inline GameObjectU5BU5D_t3328599146* get_Level_Tips_3() const { return ___Level_Tips_3; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_Level_Tips_3() { return &___Level_Tips_3; }
	inline void set_Level_Tips_3(GameObjectU5BU5D_t3328599146* value)
	{
		___Level_Tips_3 = value;
		Il2CppCodeGenWriteBarrier(&___Level_Tips_3, value);
	}

	inline static int32_t get_offset_of_Level_bt_4() { return static_cast<int32_t>(offsetof(Tips_Controller_t170528621, ___Level_bt_4)); }
	inline GameObjectU5BU5D_t3328599146* get_Level_bt_4() const { return ___Level_bt_4; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_Level_bt_4() { return &___Level_bt_4; }
	inline void set_Level_bt_4(GameObjectU5BU5D_t3328599146* value)
	{
		___Level_bt_4 = value;
		Il2CppCodeGenWriteBarrier(&___Level_bt_4, value);
	}

	inline static int32_t get_offset_of_Level_bg_5() { return static_cast<int32_t>(offsetof(Tips_Controller_t170528621, ___Level_bg_5)); }
	inline GameObjectU5BU5D_t3328599146* get_Level_bg_5() const { return ___Level_bg_5; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_Level_bg_5() { return &___Level_bg_5; }
	inline void set_Level_bg_5(GameObjectU5BU5D_t3328599146* value)
	{
		___Level_bg_5 = value;
		Il2CppCodeGenWriteBarrier(&___Level_bg_5, value);
	}

	inline static int32_t get_offset_of_All_Images_6() { return static_cast<int32_t>(offsetof(Tips_Controller_t170528621, ___All_Images_6)); }
	inline GameObjectU5BU5D_t3328599146* get_All_Images_6() const { return ___All_Images_6; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_All_Images_6() { return &___All_Images_6; }
	inline void set_All_Images_6(GameObjectU5BU5D_t3328599146* value)
	{
		___All_Images_6 = value;
		Il2CppCodeGenWriteBarrier(&___All_Images_6, value);
	}

	inline static int32_t get_offset_of_All_Images_notMobile_7() { return static_cast<int32_t>(offsetof(Tips_Controller_t170528621, ___All_Images_notMobile_7)); }
	inline GameObjectU5BU5D_t3328599146* get_All_Images_notMobile_7() const { return ___All_Images_notMobile_7; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_All_Images_notMobile_7() { return &___All_Images_notMobile_7; }
	inline void set_All_Images_notMobile_7(GameObjectU5BU5D_t3328599146* value)
	{
		___All_Images_notMobile_7 = value;
		Il2CppCodeGenWriteBarrier(&___All_Images_notMobile_7, value);
	}

	inline static int32_t get_offset_of_bt_skip_8() { return static_cast<int32_t>(offsetof(Tips_Controller_t170528621, ___bt_skip_8)); }
	inline GameObject_t1113636619 * get_bt_skip_8() const { return ___bt_skip_8; }
	inline GameObject_t1113636619 ** get_address_of_bt_skip_8() { return &___bt_skip_8; }
	inline void set_bt_skip_8(GameObject_t1113636619 * value)
	{
		___bt_skip_8 = value;
		Il2CppCodeGenWriteBarrier(&___bt_skip_8, value);
	}

	inline static int32_t get_offset_of_U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_9() { return static_cast<int32_t>(offsetof(Tips_Controller_t170528621, ___U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_9)); }
	inline Text_t1901882714 * get_U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_9() const { return ___U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_9; }
	inline Text_t1901882714 ** get_address_of_U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_9() { return &___U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_9; }
	inline void set_U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_9(Text_t1901882714 * value)
	{
		___U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_9 = value;
		Il2CppCodeGenWriteBarrier(&___U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_9, value);
	}
};

struct Tips_Controller_t170528621_StaticFields
{
public:
	// System.Predicate`1<UnityEngine.UI.Text> Tips_Controller::<>f__am$cache0
	Predicate_1_t2727176838 * ___U3CU3Ef__amU24cache0_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_10() { return static_cast<int32_t>(offsetof(Tips_Controller_t170528621_StaticFields, ___U3CU3Ef__amU24cache0_10)); }
	inline Predicate_1_t2727176838 * get_U3CU3Ef__amU24cache0_10() const { return ___U3CU3Ef__amU24cache0_10; }
	inline Predicate_1_t2727176838 ** get_address_of_U3CU3Ef__amU24cache0_10() { return &___U3CU3Ef__amU24cache0_10; }
	inline void set_U3CU3Ef__amU24cache0_10(Predicate_1_t2727176838 * value)
	{
		___U3CU3Ef__amU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
