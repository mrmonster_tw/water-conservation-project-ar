﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;
// System.Configuration.ConfigurationElementProperty
struct ConfigurationElementProperty_t939439970;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.TagPrefixInfo
struct  TagPrefixInfo_t1180606465  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct TagPrefixInfo_t1180606465_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Web.Configuration.TagPrefixInfo::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.TagPrefixInfo::tagPrefixProp
	ConfigurationProperty_t3590861854 * ___tagPrefixProp_14;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.TagPrefixInfo::namespaceProp
	ConfigurationProperty_t3590861854 * ___namespaceProp_15;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.TagPrefixInfo::assemblyProp
	ConfigurationProperty_t3590861854 * ___assemblyProp_16;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.TagPrefixInfo::tagNameProp
	ConfigurationProperty_t3590861854 * ___tagNameProp_17;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.TagPrefixInfo::sourceProp
	ConfigurationProperty_t3590861854 * ___sourceProp_18;
	// System.Configuration.ConfigurationElementProperty System.Web.Configuration.TagPrefixInfo::elementProperty
	ConfigurationElementProperty_t939439970 * ___elementProperty_19;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(TagPrefixInfo_t1180606465_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier(&___properties_13, value);
	}

	inline static int32_t get_offset_of_tagPrefixProp_14() { return static_cast<int32_t>(offsetof(TagPrefixInfo_t1180606465_StaticFields, ___tagPrefixProp_14)); }
	inline ConfigurationProperty_t3590861854 * get_tagPrefixProp_14() const { return ___tagPrefixProp_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_tagPrefixProp_14() { return &___tagPrefixProp_14; }
	inline void set_tagPrefixProp_14(ConfigurationProperty_t3590861854 * value)
	{
		___tagPrefixProp_14 = value;
		Il2CppCodeGenWriteBarrier(&___tagPrefixProp_14, value);
	}

	inline static int32_t get_offset_of_namespaceProp_15() { return static_cast<int32_t>(offsetof(TagPrefixInfo_t1180606465_StaticFields, ___namespaceProp_15)); }
	inline ConfigurationProperty_t3590861854 * get_namespaceProp_15() const { return ___namespaceProp_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_namespaceProp_15() { return &___namespaceProp_15; }
	inline void set_namespaceProp_15(ConfigurationProperty_t3590861854 * value)
	{
		___namespaceProp_15 = value;
		Il2CppCodeGenWriteBarrier(&___namespaceProp_15, value);
	}

	inline static int32_t get_offset_of_assemblyProp_16() { return static_cast<int32_t>(offsetof(TagPrefixInfo_t1180606465_StaticFields, ___assemblyProp_16)); }
	inline ConfigurationProperty_t3590861854 * get_assemblyProp_16() const { return ___assemblyProp_16; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_assemblyProp_16() { return &___assemblyProp_16; }
	inline void set_assemblyProp_16(ConfigurationProperty_t3590861854 * value)
	{
		___assemblyProp_16 = value;
		Il2CppCodeGenWriteBarrier(&___assemblyProp_16, value);
	}

	inline static int32_t get_offset_of_tagNameProp_17() { return static_cast<int32_t>(offsetof(TagPrefixInfo_t1180606465_StaticFields, ___tagNameProp_17)); }
	inline ConfigurationProperty_t3590861854 * get_tagNameProp_17() const { return ___tagNameProp_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_tagNameProp_17() { return &___tagNameProp_17; }
	inline void set_tagNameProp_17(ConfigurationProperty_t3590861854 * value)
	{
		___tagNameProp_17 = value;
		Il2CppCodeGenWriteBarrier(&___tagNameProp_17, value);
	}

	inline static int32_t get_offset_of_sourceProp_18() { return static_cast<int32_t>(offsetof(TagPrefixInfo_t1180606465_StaticFields, ___sourceProp_18)); }
	inline ConfigurationProperty_t3590861854 * get_sourceProp_18() const { return ___sourceProp_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_sourceProp_18() { return &___sourceProp_18; }
	inline void set_sourceProp_18(ConfigurationProperty_t3590861854 * value)
	{
		___sourceProp_18 = value;
		Il2CppCodeGenWriteBarrier(&___sourceProp_18, value);
	}

	inline static int32_t get_offset_of_elementProperty_19() { return static_cast<int32_t>(offsetof(TagPrefixInfo_t1180606465_StaticFields, ___elementProperty_19)); }
	inline ConfigurationElementProperty_t939439970 * get_elementProperty_19() const { return ___elementProperty_19; }
	inline ConfigurationElementProperty_t939439970 ** get_address_of_elementProperty_19() { return &___elementProperty_19; }
	inline void set_elementProperty_19(ConfigurationElementProperty_t939439970 * value)
	{
		___elementProperty_19 = value;
		Il2CppCodeGenWriteBarrier(&___elementProperty_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
