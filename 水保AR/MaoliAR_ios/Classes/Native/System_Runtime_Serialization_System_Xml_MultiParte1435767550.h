﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Runtime_Serialization_System_Xml_DummyState3823067943.h"

// System.Xml.XmlReader
struct XmlReader_t3121518892;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.MultiPartedXmlReader
struct  MultiPartedXmlReader_t1435767550  : public DummyStateXmlReader_t3823067943
{
public:
	// System.Xml.XmlReader System.Xml.MultiPartedXmlReader::owner
	XmlReader_t3121518892 * ___owner_6;
	// System.String System.Xml.MultiPartedXmlReader::value
	String_t* ___value_7;

public:
	inline static int32_t get_offset_of_owner_6() { return static_cast<int32_t>(offsetof(MultiPartedXmlReader_t1435767550, ___owner_6)); }
	inline XmlReader_t3121518892 * get_owner_6() const { return ___owner_6; }
	inline XmlReader_t3121518892 ** get_address_of_owner_6() { return &___owner_6; }
	inline void set_owner_6(XmlReader_t3121518892 * value)
	{
		___owner_6 = value;
		Il2CppCodeGenWriteBarrier(&___owner_6, value);
	}

	inline static int32_t get_offset_of_value_7() { return static_cast<int32_t>(offsetof(MultiPartedXmlReader_t1435767550, ___value_7)); }
	inline String_t* get_value_7() const { return ___value_7; }
	inline String_t** get_address_of_value_7() { return &___value_7; }
	inline void set_value_7(String_t* value)
	{
		___value_7 = value;
		Il2CppCodeGenWriteBarrier(&___value_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
