﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_TimeSpan881159249.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.ServiceTimeoutsBehavior
struct  ServiceTimeoutsBehavior_t3730647462  : public Il2CppObject
{
public:
	// System.TimeSpan System.ServiceModel.Description.ServiceTimeoutsBehavior::<TransactionTimeout>k__BackingField
	TimeSpan_t881159249  ___U3CTransactionTimeoutU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CTransactionTimeoutU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ServiceTimeoutsBehavior_t3730647462, ___U3CTransactionTimeoutU3Ek__BackingField_0)); }
	inline TimeSpan_t881159249  get_U3CTransactionTimeoutU3Ek__BackingField_0() const { return ___U3CTransactionTimeoutU3Ek__BackingField_0; }
	inline TimeSpan_t881159249 * get_address_of_U3CTransactionTimeoutU3Ek__BackingField_0() { return &___U3CTransactionTimeoutU3Ek__BackingField_0; }
	inline void set_U3CTransactionTimeoutU3Ek__BackingField_0(TimeSpan_t881159249  value)
	{
		___U3CTransactionTimeoutU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
