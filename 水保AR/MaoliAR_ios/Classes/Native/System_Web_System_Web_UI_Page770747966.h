﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_TemplateControl1922940480.h"
#include "mscorlib_System_TimeSpan881159249.h"
#include "System_Web_System_Web_UI_ViewStateEncryptionMode3676930203.h"

// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Web.UI.ValidatorCollection
struct ValidatorCollection_t3538853993;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Web.UI.IPostBackEventHandler
struct IPostBackEventHandler_t3297155504;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t407452768;
// System.Web.UI.ClientScriptManager
struct ClientScriptManager_t602632898;
// System.Web.UI.PageStatePersister
struct PageStatePersister_t2741789537;
// System.Globalization.CultureInfo
struct CultureInfo_t4157843068;
// System.Web.HttpContext
struct HttpContext_t1969259010;
// System.Web.HttpApplicationState
struct HttpApplicationState_t2618076950;
// System.Web.HttpResponse
struct HttpResponse_t1542361753;
// System.Web.HttpRequest
struct HttpRequest_t809700260;
// System.Web.Caching.Cache
struct Cache_t4020976765;
// System.Web.UI.HtmlControls.HtmlHead
struct HtmlHead_t3571196538;
// System.Web.UI.MasterPage
struct MasterPage_t1483330027;
// System.Web.UI.Page
struct Page_t770747966;
// System.Collections.Generic.List`1<System.Web.UI.Control>
struct List_1_t183582085;
// System.Web.UI.HtmlControls.HtmlForm
struct HtmlForm_t2905737469;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Collections.Generic.List`1<System.Web.UI.PageAsyncTask>
struct List_1_t1250898740;
// System.Object
struct Il2CppObject;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Web.UI.PageTheme
struct PageTheme_t802797672;
// System.Collections.Stack
struct Stack_t2329662280;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.Page
struct  Page_t770747966  : public TemplateControl_t1922940480
{
public:
	// System.Boolean System.Web.UI.Page::_eventValidation
	bool ____eventValidation_37;
	// System.Object[] System.Web.UI.Page::_savedControlState
	ObjectU5BU5D_t2843939325* ____savedControlState_38;
	// System.Boolean System.Web.UI.Page::_doLoadPreviousPage
	bool ____doLoadPreviousPage_39;
	// System.String System.Web.UI.Page::_focusedControlID
	String_t* ____focusedControlID_40;
	// System.Boolean System.Web.UI.Page::_hasEnabledControlArray
	bool ____hasEnabledControlArray_41;
	// System.Boolean System.Web.UI.Page::_viewState
	bool ____viewState_42;
	// System.Boolean System.Web.UI.Page::_viewStateMac
	bool ____viewStateMac_43;
	// System.String System.Web.UI.Page::_errorPage
	String_t* ____errorPage_44;
	// System.Boolean System.Web.UI.Page::is_validated
	bool ___is_validated_45;
	// System.Web.UI.ValidatorCollection System.Web.UI.Page::_validators
	ValidatorCollection_t3538853993 * ____validators_46;
	// System.Boolean System.Web.UI.Page::renderingForm
	bool ___renderingForm_47;
	// System.String System.Web.UI.Page::_savedViewState
	String_t* ____savedViewState_48;
	// System.Collections.ArrayList System.Web.UI.Page::_requiresPostBack
	ArrayList_t2718874744 * ____requiresPostBack_49;
	// System.Collections.ArrayList System.Web.UI.Page::_requiresPostBackCopy
	ArrayList_t2718874744 * ____requiresPostBackCopy_50;
	// System.Collections.ArrayList System.Web.UI.Page::requiresPostDataChanged
	ArrayList_t2718874744 * ___requiresPostDataChanged_51;
	// System.Web.UI.IPostBackEventHandler System.Web.UI.Page::requiresRaiseEvent
	Il2CppObject * ___requiresRaiseEvent_52;
	// System.Web.UI.IPostBackEventHandler System.Web.UI.Page::formPostedRequiresRaiseEvent
	Il2CppObject * ___formPostedRequiresRaiseEvent_53;
	// System.Collections.Specialized.NameValueCollection System.Web.UI.Page::secondPostData
	NameValueCollection_t407452768 * ___secondPostData_54;
	// System.Boolean System.Web.UI.Page::requiresPostBackScript
	bool ___requiresPostBackScript_55;
	// System.Boolean System.Web.UI.Page::postBackScriptRendered
	bool ___postBackScriptRendered_56;
	// System.Boolean System.Web.UI.Page::requiresFormScriptDeclaration
	bool ___requiresFormScriptDeclaration_57;
	// System.Boolean System.Web.UI.Page::formScriptDeclarationRendered
	bool ___formScriptDeclarationRendered_58;
	// System.Boolean System.Web.UI.Page::handleViewState
	bool ___handleViewState_59;
	// System.Collections.Specialized.NameValueCollection System.Web.UI.Page::_requestValueCollection
	NameValueCollection_t407452768 * ____requestValueCollection_60;
	// System.String System.Web.UI.Page::clientTarget
	String_t* ___clientTarget_61;
	// System.Web.UI.ClientScriptManager System.Web.UI.Page::scriptManager
	ClientScriptManager_t602632898 * ___scriptManager_62;
	// System.Boolean System.Web.UI.Page::allow_load
	bool ___allow_load_63;
	// System.Web.UI.PageStatePersister System.Web.UI.Page::page_state_persister
	PageStatePersister_t2741789537 * ___page_state_persister_64;
	// System.Globalization.CultureInfo System.Web.UI.Page::_appCulture
	CultureInfo_t4157843068 * ____appCulture_65;
	// System.Globalization.CultureInfo System.Web.UI.Page::_appUICulture
	CultureInfo_t4157843068 * ____appUICulture_66;
	// System.Web.HttpContext System.Web.UI.Page::_context
	HttpContext_t1969259010 * ____context_67;
	// System.Web.HttpApplicationState System.Web.UI.Page::_application
	HttpApplicationState_t2618076950 * ____application_68;
	// System.Web.HttpResponse System.Web.UI.Page::_response
	HttpResponse_t1542361753 * ____response_69;
	// System.Web.HttpRequest System.Web.UI.Page::_request
	HttpRequest_t809700260 * ____request_70;
	// System.Web.Caching.Cache System.Web.UI.Page::_cache
	Cache_t4020976765 * ____cache_71;
	// System.Web.UI.HtmlControls.HtmlHead System.Web.UI.Page::htmlHeader
	HtmlHead_t3571196538 * ___htmlHeader_72;
	// System.Web.UI.MasterPage System.Web.UI.Page::masterPage
	MasterPage_t1483330027 * ___masterPage_73;
	// System.String System.Web.UI.Page::masterPageFile
	String_t* ___masterPageFile_74;
	// System.Web.UI.Page System.Web.UI.Page::previousPage
	Page_t770747966 * ___previousPage_75;
	// System.Boolean System.Web.UI.Page::isCrossPagePostBack
	bool ___isCrossPagePostBack_76;
	// System.Boolean System.Web.UI.Page::isPostBack
	bool ___isPostBack_77;
	// System.Boolean System.Web.UI.Page::isCallback
	bool ___isCallback_78;
	// System.Collections.Generic.List`1<System.Web.UI.Control> System.Web.UI.Page::requireStateControls
	List_1_t183582085 * ___requireStateControls_79;
	// System.Web.UI.HtmlControls.HtmlForm System.Web.UI.Page::_form
	HtmlForm_t2905737469 * ____form_80;
	// System.String System.Web.UI.Page::_title
	String_t* ____title_81;
	// System.String System.Web.UI.Page::_theme
	String_t* ____theme_82;
	// System.String System.Web.UI.Page::_styleSheetTheme
	String_t* ____styleSheetTheme_83;
	// System.Collections.Hashtable System.Web.UI.Page::items
	Hashtable_t1853889766 * ___items_84;
	// System.Boolean System.Web.UI.Page::_maintainScrollPositionOnPostBack
	bool ____maintainScrollPositionOnPostBack_85;
	// System.TimeSpan System.Web.UI.Page::asyncTimeout
	TimeSpan_t881159249  ___asyncTimeout_86;
	// System.Collections.Generic.List`1<System.Web.UI.PageAsyncTask> System.Web.UI.Page::parallelTasks
	List_1_t1250898740 * ___parallelTasks_87;
	// System.Collections.Generic.List`1<System.Web.UI.PageAsyncTask> System.Web.UI.Page::serialTasks
	List_1_t1250898740 * ___serialTasks_88;
	// System.Web.UI.ViewStateEncryptionMode System.Web.UI.Page::viewStateEncryptionMode
	int32_t ___viewStateEncryptionMode_89;
	// System.Boolean System.Web.UI.Page::controlRegisteredForViewStateEncryption
	bool ___controlRegisteredForViewStateEncryption_90;
	// System.String System.Web.UI.Page::_validationStartupScript
	String_t* ____validationStartupScript_91;
	// System.String System.Web.UI.Page::_validationOnSubmitStatement
	String_t* ____validationOnSubmitStatement_92;
	// System.String System.Web.UI.Page::_validationInitializeScript
	String_t* ____validationInitializeScript_93;
	// System.String System.Web.UI.Page::_webFormScriptReference
	String_t* ____webFormScriptReference_94;
	// System.Int32 System.Web.UI.Page::event_mask
	int32_t ___event_mask_101;
	// System.Collections.Hashtable System.Web.UI.Page::contentTemplates
	Hashtable_t1853889766 * ___contentTemplates_106;
	// System.Web.UI.PageTheme System.Web.UI.Page::_pageTheme
	PageTheme_t802797672 * ____pageTheme_107;
	// System.Web.UI.PageTheme System.Web.UI.Page::_styleSheetPageTheme
	PageTheme_t802797672 * ____styleSheetPageTheme_108;
	// System.Collections.Stack System.Web.UI.Page::dataItemCtx
	Stack_t2329662280 * ___dataItemCtx_109;

public:
	inline static int32_t get_offset_of__eventValidation_37() { return static_cast<int32_t>(offsetof(Page_t770747966, ____eventValidation_37)); }
	inline bool get__eventValidation_37() const { return ____eventValidation_37; }
	inline bool* get_address_of__eventValidation_37() { return &____eventValidation_37; }
	inline void set__eventValidation_37(bool value)
	{
		____eventValidation_37 = value;
	}

	inline static int32_t get_offset_of__savedControlState_38() { return static_cast<int32_t>(offsetof(Page_t770747966, ____savedControlState_38)); }
	inline ObjectU5BU5D_t2843939325* get__savedControlState_38() const { return ____savedControlState_38; }
	inline ObjectU5BU5D_t2843939325** get_address_of__savedControlState_38() { return &____savedControlState_38; }
	inline void set__savedControlState_38(ObjectU5BU5D_t2843939325* value)
	{
		____savedControlState_38 = value;
		Il2CppCodeGenWriteBarrier(&____savedControlState_38, value);
	}

	inline static int32_t get_offset_of__doLoadPreviousPage_39() { return static_cast<int32_t>(offsetof(Page_t770747966, ____doLoadPreviousPage_39)); }
	inline bool get__doLoadPreviousPage_39() const { return ____doLoadPreviousPage_39; }
	inline bool* get_address_of__doLoadPreviousPage_39() { return &____doLoadPreviousPage_39; }
	inline void set__doLoadPreviousPage_39(bool value)
	{
		____doLoadPreviousPage_39 = value;
	}

	inline static int32_t get_offset_of__focusedControlID_40() { return static_cast<int32_t>(offsetof(Page_t770747966, ____focusedControlID_40)); }
	inline String_t* get__focusedControlID_40() const { return ____focusedControlID_40; }
	inline String_t** get_address_of__focusedControlID_40() { return &____focusedControlID_40; }
	inline void set__focusedControlID_40(String_t* value)
	{
		____focusedControlID_40 = value;
		Il2CppCodeGenWriteBarrier(&____focusedControlID_40, value);
	}

	inline static int32_t get_offset_of__hasEnabledControlArray_41() { return static_cast<int32_t>(offsetof(Page_t770747966, ____hasEnabledControlArray_41)); }
	inline bool get__hasEnabledControlArray_41() const { return ____hasEnabledControlArray_41; }
	inline bool* get_address_of__hasEnabledControlArray_41() { return &____hasEnabledControlArray_41; }
	inline void set__hasEnabledControlArray_41(bool value)
	{
		____hasEnabledControlArray_41 = value;
	}

	inline static int32_t get_offset_of__viewState_42() { return static_cast<int32_t>(offsetof(Page_t770747966, ____viewState_42)); }
	inline bool get__viewState_42() const { return ____viewState_42; }
	inline bool* get_address_of__viewState_42() { return &____viewState_42; }
	inline void set__viewState_42(bool value)
	{
		____viewState_42 = value;
	}

	inline static int32_t get_offset_of__viewStateMac_43() { return static_cast<int32_t>(offsetof(Page_t770747966, ____viewStateMac_43)); }
	inline bool get__viewStateMac_43() const { return ____viewStateMac_43; }
	inline bool* get_address_of__viewStateMac_43() { return &____viewStateMac_43; }
	inline void set__viewStateMac_43(bool value)
	{
		____viewStateMac_43 = value;
	}

	inline static int32_t get_offset_of__errorPage_44() { return static_cast<int32_t>(offsetof(Page_t770747966, ____errorPage_44)); }
	inline String_t* get__errorPage_44() const { return ____errorPage_44; }
	inline String_t** get_address_of__errorPage_44() { return &____errorPage_44; }
	inline void set__errorPage_44(String_t* value)
	{
		____errorPage_44 = value;
		Il2CppCodeGenWriteBarrier(&____errorPage_44, value);
	}

	inline static int32_t get_offset_of_is_validated_45() { return static_cast<int32_t>(offsetof(Page_t770747966, ___is_validated_45)); }
	inline bool get_is_validated_45() const { return ___is_validated_45; }
	inline bool* get_address_of_is_validated_45() { return &___is_validated_45; }
	inline void set_is_validated_45(bool value)
	{
		___is_validated_45 = value;
	}

	inline static int32_t get_offset_of__validators_46() { return static_cast<int32_t>(offsetof(Page_t770747966, ____validators_46)); }
	inline ValidatorCollection_t3538853993 * get__validators_46() const { return ____validators_46; }
	inline ValidatorCollection_t3538853993 ** get_address_of__validators_46() { return &____validators_46; }
	inline void set__validators_46(ValidatorCollection_t3538853993 * value)
	{
		____validators_46 = value;
		Il2CppCodeGenWriteBarrier(&____validators_46, value);
	}

	inline static int32_t get_offset_of_renderingForm_47() { return static_cast<int32_t>(offsetof(Page_t770747966, ___renderingForm_47)); }
	inline bool get_renderingForm_47() const { return ___renderingForm_47; }
	inline bool* get_address_of_renderingForm_47() { return &___renderingForm_47; }
	inline void set_renderingForm_47(bool value)
	{
		___renderingForm_47 = value;
	}

	inline static int32_t get_offset_of__savedViewState_48() { return static_cast<int32_t>(offsetof(Page_t770747966, ____savedViewState_48)); }
	inline String_t* get__savedViewState_48() const { return ____savedViewState_48; }
	inline String_t** get_address_of__savedViewState_48() { return &____savedViewState_48; }
	inline void set__savedViewState_48(String_t* value)
	{
		____savedViewState_48 = value;
		Il2CppCodeGenWriteBarrier(&____savedViewState_48, value);
	}

	inline static int32_t get_offset_of__requiresPostBack_49() { return static_cast<int32_t>(offsetof(Page_t770747966, ____requiresPostBack_49)); }
	inline ArrayList_t2718874744 * get__requiresPostBack_49() const { return ____requiresPostBack_49; }
	inline ArrayList_t2718874744 ** get_address_of__requiresPostBack_49() { return &____requiresPostBack_49; }
	inline void set__requiresPostBack_49(ArrayList_t2718874744 * value)
	{
		____requiresPostBack_49 = value;
		Il2CppCodeGenWriteBarrier(&____requiresPostBack_49, value);
	}

	inline static int32_t get_offset_of__requiresPostBackCopy_50() { return static_cast<int32_t>(offsetof(Page_t770747966, ____requiresPostBackCopy_50)); }
	inline ArrayList_t2718874744 * get__requiresPostBackCopy_50() const { return ____requiresPostBackCopy_50; }
	inline ArrayList_t2718874744 ** get_address_of__requiresPostBackCopy_50() { return &____requiresPostBackCopy_50; }
	inline void set__requiresPostBackCopy_50(ArrayList_t2718874744 * value)
	{
		____requiresPostBackCopy_50 = value;
		Il2CppCodeGenWriteBarrier(&____requiresPostBackCopy_50, value);
	}

	inline static int32_t get_offset_of_requiresPostDataChanged_51() { return static_cast<int32_t>(offsetof(Page_t770747966, ___requiresPostDataChanged_51)); }
	inline ArrayList_t2718874744 * get_requiresPostDataChanged_51() const { return ___requiresPostDataChanged_51; }
	inline ArrayList_t2718874744 ** get_address_of_requiresPostDataChanged_51() { return &___requiresPostDataChanged_51; }
	inline void set_requiresPostDataChanged_51(ArrayList_t2718874744 * value)
	{
		___requiresPostDataChanged_51 = value;
		Il2CppCodeGenWriteBarrier(&___requiresPostDataChanged_51, value);
	}

	inline static int32_t get_offset_of_requiresRaiseEvent_52() { return static_cast<int32_t>(offsetof(Page_t770747966, ___requiresRaiseEvent_52)); }
	inline Il2CppObject * get_requiresRaiseEvent_52() const { return ___requiresRaiseEvent_52; }
	inline Il2CppObject ** get_address_of_requiresRaiseEvent_52() { return &___requiresRaiseEvent_52; }
	inline void set_requiresRaiseEvent_52(Il2CppObject * value)
	{
		___requiresRaiseEvent_52 = value;
		Il2CppCodeGenWriteBarrier(&___requiresRaiseEvent_52, value);
	}

	inline static int32_t get_offset_of_formPostedRequiresRaiseEvent_53() { return static_cast<int32_t>(offsetof(Page_t770747966, ___formPostedRequiresRaiseEvent_53)); }
	inline Il2CppObject * get_formPostedRequiresRaiseEvent_53() const { return ___formPostedRequiresRaiseEvent_53; }
	inline Il2CppObject ** get_address_of_formPostedRequiresRaiseEvent_53() { return &___formPostedRequiresRaiseEvent_53; }
	inline void set_formPostedRequiresRaiseEvent_53(Il2CppObject * value)
	{
		___formPostedRequiresRaiseEvent_53 = value;
		Il2CppCodeGenWriteBarrier(&___formPostedRequiresRaiseEvent_53, value);
	}

	inline static int32_t get_offset_of_secondPostData_54() { return static_cast<int32_t>(offsetof(Page_t770747966, ___secondPostData_54)); }
	inline NameValueCollection_t407452768 * get_secondPostData_54() const { return ___secondPostData_54; }
	inline NameValueCollection_t407452768 ** get_address_of_secondPostData_54() { return &___secondPostData_54; }
	inline void set_secondPostData_54(NameValueCollection_t407452768 * value)
	{
		___secondPostData_54 = value;
		Il2CppCodeGenWriteBarrier(&___secondPostData_54, value);
	}

	inline static int32_t get_offset_of_requiresPostBackScript_55() { return static_cast<int32_t>(offsetof(Page_t770747966, ___requiresPostBackScript_55)); }
	inline bool get_requiresPostBackScript_55() const { return ___requiresPostBackScript_55; }
	inline bool* get_address_of_requiresPostBackScript_55() { return &___requiresPostBackScript_55; }
	inline void set_requiresPostBackScript_55(bool value)
	{
		___requiresPostBackScript_55 = value;
	}

	inline static int32_t get_offset_of_postBackScriptRendered_56() { return static_cast<int32_t>(offsetof(Page_t770747966, ___postBackScriptRendered_56)); }
	inline bool get_postBackScriptRendered_56() const { return ___postBackScriptRendered_56; }
	inline bool* get_address_of_postBackScriptRendered_56() { return &___postBackScriptRendered_56; }
	inline void set_postBackScriptRendered_56(bool value)
	{
		___postBackScriptRendered_56 = value;
	}

	inline static int32_t get_offset_of_requiresFormScriptDeclaration_57() { return static_cast<int32_t>(offsetof(Page_t770747966, ___requiresFormScriptDeclaration_57)); }
	inline bool get_requiresFormScriptDeclaration_57() const { return ___requiresFormScriptDeclaration_57; }
	inline bool* get_address_of_requiresFormScriptDeclaration_57() { return &___requiresFormScriptDeclaration_57; }
	inline void set_requiresFormScriptDeclaration_57(bool value)
	{
		___requiresFormScriptDeclaration_57 = value;
	}

	inline static int32_t get_offset_of_formScriptDeclarationRendered_58() { return static_cast<int32_t>(offsetof(Page_t770747966, ___formScriptDeclarationRendered_58)); }
	inline bool get_formScriptDeclarationRendered_58() const { return ___formScriptDeclarationRendered_58; }
	inline bool* get_address_of_formScriptDeclarationRendered_58() { return &___formScriptDeclarationRendered_58; }
	inline void set_formScriptDeclarationRendered_58(bool value)
	{
		___formScriptDeclarationRendered_58 = value;
	}

	inline static int32_t get_offset_of_handleViewState_59() { return static_cast<int32_t>(offsetof(Page_t770747966, ___handleViewState_59)); }
	inline bool get_handleViewState_59() const { return ___handleViewState_59; }
	inline bool* get_address_of_handleViewState_59() { return &___handleViewState_59; }
	inline void set_handleViewState_59(bool value)
	{
		___handleViewState_59 = value;
	}

	inline static int32_t get_offset_of__requestValueCollection_60() { return static_cast<int32_t>(offsetof(Page_t770747966, ____requestValueCollection_60)); }
	inline NameValueCollection_t407452768 * get__requestValueCollection_60() const { return ____requestValueCollection_60; }
	inline NameValueCollection_t407452768 ** get_address_of__requestValueCollection_60() { return &____requestValueCollection_60; }
	inline void set__requestValueCollection_60(NameValueCollection_t407452768 * value)
	{
		____requestValueCollection_60 = value;
		Il2CppCodeGenWriteBarrier(&____requestValueCollection_60, value);
	}

	inline static int32_t get_offset_of_clientTarget_61() { return static_cast<int32_t>(offsetof(Page_t770747966, ___clientTarget_61)); }
	inline String_t* get_clientTarget_61() const { return ___clientTarget_61; }
	inline String_t** get_address_of_clientTarget_61() { return &___clientTarget_61; }
	inline void set_clientTarget_61(String_t* value)
	{
		___clientTarget_61 = value;
		Il2CppCodeGenWriteBarrier(&___clientTarget_61, value);
	}

	inline static int32_t get_offset_of_scriptManager_62() { return static_cast<int32_t>(offsetof(Page_t770747966, ___scriptManager_62)); }
	inline ClientScriptManager_t602632898 * get_scriptManager_62() const { return ___scriptManager_62; }
	inline ClientScriptManager_t602632898 ** get_address_of_scriptManager_62() { return &___scriptManager_62; }
	inline void set_scriptManager_62(ClientScriptManager_t602632898 * value)
	{
		___scriptManager_62 = value;
		Il2CppCodeGenWriteBarrier(&___scriptManager_62, value);
	}

	inline static int32_t get_offset_of_allow_load_63() { return static_cast<int32_t>(offsetof(Page_t770747966, ___allow_load_63)); }
	inline bool get_allow_load_63() const { return ___allow_load_63; }
	inline bool* get_address_of_allow_load_63() { return &___allow_load_63; }
	inline void set_allow_load_63(bool value)
	{
		___allow_load_63 = value;
	}

	inline static int32_t get_offset_of_page_state_persister_64() { return static_cast<int32_t>(offsetof(Page_t770747966, ___page_state_persister_64)); }
	inline PageStatePersister_t2741789537 * get_page_state_persister_64() const { return ___page_state_persister_64; }
	inline PageStatePersister_t2741789537 ** get_address_of_page_state_persister_64() { return &___page_state_persister_64; }
	inline void set_page_state_persister_64(PageStatePersister_t2741789537 * value)
	{
		___page_state_persister_64 = value;
		Il2CppCodeGenWriteBarrier(&___page_state_persister_64, value);
	}

	inline static int32_t get_offset_of__appCulture_65() { return static_cast<int32_t>(offsetof(Page_t770747966, ____appCulture_65)); }
	inline CultureInfo_t4157843068 * get__appCulture_65() const { return ____appCulture_65; }
	inline CultureInfo_t4157843068 ** get_address_of__appCulture_65() { return &____appCulture_65; }
	inline void set__appCulture_65(CultureInfo_t4157843068 * value)
	{
		____appCulture_65 = value;
		Il2CppCodeGenWriteBarrier(&____appCulture_65, value);
	}

	inline static int32_t get_offset_of__appUICulture_66() { return static_cast<int32_t>(offsetof(Page_t770747966, ____appUICulture_66)); }
	inline CultureInfo_t4157843068 * get__appUICulture_66() const { return ____appUICulture_66; }
	inline CultureInfo_t4157843068 ** get_address_of__appUICulture_66() { return &____appUICulture_66; }
	inline void set__appUICulture_66(CultureInfo_t4157843068 * value)
	{
		____appUICulture_66 = value;
		Il2CppCodeGenWriteBarrier(&____appUICulture_66, value);
	}

	inline static int32_t get_offset_of__context_67() { return static_cast<int32_t>(offsetof(Page_t770747966, ____context_67)); }
	inline HttpContext_t1969259010 * get__context_67() const { return ____context_67; }
	inline HttpContext_t1969259010 ** get_address_of__context_67() { return &____context_67; }
	inline void set__context_67(HttpContext_t1969259010 * value)
	{
		____context_67 = value;
		Il2CppCodeGenWriteBarrier(&____context_67, value);
	}

	inline static int32_t get_offset_of__application_68() { return static_cast<int32_t>(offsetof(Page_t770747966, ____application_68)); }
	inline HttpApplicationState_t2618076950 * get__application_68() const { return ____application_68; }
	inline HttpApplicationState_t2618076950 ** get_address_of__application_68() { return &____application_68; }
	inline void set__application_68(HttpApplicationState_t2618076950 * value)
	{
		____application_68 = value;
		Il2CppCodeGenWriteBarrier(&____application_68, value);
	}

	inline static int32_t get_offset_of__response_69() { return static_cast<int32_t>(offsetof(Page_t770747966, ____response_69)); }
	inline HttpResponse_t1542361753 * get__response_69() const { return ____response_69; }
	inline HttpResponse_t1542361753 ** get_address_of__response_69() { return &____response_69; }
	inline void set__response_69(HttpResponse_t1542361753 * value)
	{
		____response_69 = value;
		Il2CppCodeGenWriteBarrier(&____response_69, value);
	}

	inline static int32_t get_offset_of__request_70() { return static_cast<int32_t>(offsetof(Page_t770747966, ____request_70)); }
	inline HttpRequest_t809700260 * get__request_70() const { return ____request_70; }
	inline HttpRequest_t809700260 ** get_address_of__request_70() { return &____request_70; }
	inline void set__request_70(HttpRequest_t809700260 * value)
	{
		____request_70 = value;
		Il2CppCodeGenWriteBarrier(&____request_70, value);
	}

	inline static int32_t get_offset_of__cache_71() { return static_cast<int32_t>(offsetof(Page_t770747966, ____cache_71)); }
	inline Cache_t4020976765 * get__cache_71() const { return ____cache_71; }
	inline Cache_t4020976765 ** get_address_of__cache_71() { return &____cache_71; }
	inline void set__cache_71(Cache_t4020976765 * value)
	{
		____cache_71 = value;
		Il2CppCodeGenWriteBarrier(&____cache_71, value);
	}

	inline static int32_t get_offset_of_htmlHeader_72() { return static_cast<int32_t>(offsetof(Page_t770747966, ___htmlHeader_72)); }
	inline HtmlHead_t3571196538 * get_htmlHeader_72() const { return ___htmlHeader_72; }
	inline HtmlHead_t3571196538 ** get_address_of_htmlHeader_72() { return &___htmlHeader_72; }
	inline void set_htmlHeader_72(HtmlHead_t3571196538 * value)
	{
		___htmlHeader_72 = value;
		Il2CppCodeGenWriteBarrier(&___htmlHeader_72, value);
	}

	inline static int32_t get_offset_of_masterPage_73() { return static_cast<int32_t>(offsetof(Page_t770747966, ___masterPage_73)); }
	inline MasterPage_t1483330027 * get_masterPage_73() const { return ___masterPage_73; }
	inline MasterPage_t1483330027 ** get_address_of_masterPage_73() { return &___masterPage_73; }
	inline void set_masterPage_73(MasterPage_t1483330027 * value)
	{
		___masterPage_73 = value;
		Il2CppCodeGenWriteBarrier(&___masterPage_73, value);
	}

	inline static int32_t get_offset_of_masterPageFile_74() { return static_cast<int32_t>(offsetof(Page_t770747966, ___masterPageFile_74)); }
	inline String_t* get_masterPageFile_74() const { return ___masterPageFile_74; }
	inline String_t** get_address_of_masterPageFile_74() { return &___masterPageFile_74; }
	inline void set_masterPageFile_74(String_t* value)
	{
		___masterPageFile_74 = value;
		Il2CppCodeGenWriteBarrier(&___masterPageFile_74, value);
	}

	inline static int32_t get_offset_of_previousPage_75() { return static_cast<int32_t>(offsetof(Page_t770747966, ___previousPage_75)); }
	inline Page_t770747966 * get_previousPage_75() const { return ___previousPage_75; }
	inline Page_t770747966 ** get_address_of_previousPage_75() { return &___previousPage_75; }
	inline void set_previousPage_75(Page_t770747966 * value)
	{
		___previousPage_75 = value;
		Il2CppCodeGenWriteBarrier(&___previousPage_75, value);
	}

	inline static int32_t get_offset_of_isCrossPagePostBack_76() { return static_cast<int32_t>(offsetof(Page_t770747966, ___isCrossPagePostBack_76)); }
	inline bool get_isCrossPagePostBack_76() const { return ___isCrossPagePostBack_76; }
	inline bool* get_address_of_isCrossPagePostBack_76() { return &___isCrossPagePostBack_76; }
	inline void set_isCrossPagePostBack_76(bool value)
	{
		___isCrossPagePostBack_76 = value;
	}

	inline static int32_t get_offset_of_isPostBack_77() { return static_cast<int32_t>(offsetof(Page_t770747966, ___isPostBack_77)); }
	inline bool get_isPostBack_77() const { return ___isPostBack_77; }
	inline bool* get_address_of_isPostBack_77() { return &___isPostBack_77; }
	inline void set_isPostBack_77(bool value)
	{
		___isPostBack_77 = value;
	}

	inline static int32_t get_offset_of_isCallback_78() { return static_cast<int32_t>(offsetof(Page_t770747966, ___isCallback_78)); }
	inline bool get_isCallback_78() const { return ___isCallback_78; }
	inline bool* get_address_of_isCallback_78() { return &___isCallback_78; }
	inline void set_isCallback_78(bool value)
	{
		___isCallback_78 = value;
	}

	inline static int32_t get_offset_of_requireStateControls_79() { return static_cast<int32_t>(offsetof(Page_t770747966, ___requireStateControls_79)); }
	inline List_1_t183582085 * get_requireStateControls_79() const { return ___requireStateControls_79; }
	inline List_1_t183582085 ** get_address_of_requireStateControls_79() { return &___requireStateControls_79; }
	inline void set_requireStateControls_79(List_1_t183582085 * value)
	{
		___requireStateControls_79 = value;
		Il2CppCodeGenWriteBarrier(&___requireStateControls_79, value);
	}

	inline static int32_t get_offset_of__form_80() { return static_cast<int32_t>(offsetof(Page_t770747966, ____form_80)); }
	inline HtmlForm_t2905737469 * get__form_80() const { return ____form_80; }
	inline HtmlForm_t2905737469 ** get_address_of__form_80() { return &____form_80; }
	inline void set__form_80(HtmlForm_t2905737469 * value)
	{
		____form_80 = value;
		Il2CppCodeGenWriteBarrier(&____form_80, value);
	}

	inline static int32_t get_offset_of__title_81() { return static_cast<int32_t>(offsetof(Page_t770747966, ____title_81)); }
	inline String_t* get__title_81() const { return ____title_81; }
	inline String_t** get_address_of__title_81() { return &____title_81; }
	inline void set__title_81(String_t* value)
	{
		____title_81 = value;
		Il2CppCodeGenWriteBarrier(&____title_81, value);
	}

	inline static int32_t get_offset_of__theme_82() { return static_cast<int32_t>(offsetof(Page_t770747966, ____theme_82)); }
	inline String_t* get__theme_82() const { return ____theme_82; }
	inline String_t** get_address_of__theme_82() { return &____theme_82; }
	inline void set__theme_82(String_t* value)
	{
		____theme_82 = value;
		Il2CppCodeGenWriteBarrier(&____theme_82, value);
	}

	inline static int32_t get_offset_of__styleSheetTheme_83() { return static_cast<int32_t>(offsetof(Page_t770747966, ____styleSheetTheme_83)); }
	inline String_t* get__styleSheetTheme_83() const { return ____styleSheetTheme_83; }
	inline String_t** get_address_of__styleSheetTheme_83() { return &____styleSheetTheme_83; }
	inline void set__styleSheetTheme_83(String_t* value)
	{
		____styleSheetTheme_83 = value;
		Il2CppCodeGenWriteBarrier(&____styleSheetTheme_83, value);
	}

	inline static int32_t get_offset_of_items_84() { return static_cast<int32_t>(offsetof(Page_t770747966, ___items_84)); }
	inline Hashtable_t1853889766 * get_items_84() const { return ___items_84; }
	inline Hashtable_t1853889766 ** get_address_of_items_84() { return &___items_84; }
	inline void set_items_84(Hashtable_t1853889766 * value)
	{
		___items_84 = value;
		Il2CppCodeGenWriteBarrier(&___items_84, value);
	}

	inline static int32_t get_offset_of__maintainScrollPositionOnPostBack_85() { return static_cast<int32_t>(offsetof(Page_t770747966, ____maintainScrollPositionOnPostBack_85)); }
	inline bool get__maintainScrollPositionOnPostBack_85() const { return ____maintainScrollPositionOnPostBack_85; }
	inline bool* get_address_of__maintainScrollPositionOnPostBack_85() { return &____maintainScrollPositionOnPostBack_85; }
	inline void set__maintainScrollPositionOnPostBack_85(bool value)
	{
		____maintainScrollPositionOnPostBack_85 = value;
	}

	inline static int32_t get_offset_of_asyncTimeout_86() { return static_cast<int32_t>(offsetof(Page_t770747966, ___asyncTimeout_86)); }
	inline TimeSpan_t881159249  get_asyncTimeout_86() const { return ___asyncTimeout_86; }
	inline TimeSpan_t881159249 * get_address_of_asyncTimeout_86() { return &___asyncTimeout_86; }
	inline void set_asyncTimeout_86(TimeSpan_t881159249  value)
	{
		___asyncTimeout_86 = value;
	}

	inline static int32_t get_offset_of_parallelTasks_87() { return static_cast<int32_t>(offsetof(Page_t770747966, ___parallelTasks_87)); }
	inline List_1_t1250898740 * get_parallelTasks_87() const { return ___parallelTasks_87; }
	inline List_1_t1250898740 ** get_address_of_parallelTasks_87() { return &___parallelTasks_87; }
	inline void set_parallelTasks_87(List_1_t1250898740 * value)
	{
		___parallelTasks_87 = value;
		Il2CppCodeGenWriteBarrier(&___parallelTasks_87, value);
	}

	inline static int32_t get_offset_of_serialTasks_88() { return static_cast<int32_t>(offsetof(Page_t770747966, ___serialTasks_88)); }
	inline List_1_t1250898740 * get_serialTasks_88() const { return ___serialTasks_88; }
	inline List_1_t1250898740 ** get_address_of_serialTasks_88() { return &___serialTasks_88; }
	inline void set_serialTasks_88(List_1_t1250898740 * value)
	{
		___serialTasks_88 = value;
		Il2CppCodeGenWriteBarrier(&___serialTasks_88, value);
	}

	inline static int32_t get_offset_of_viewStateEncryptionMode_89() { return static_cast<int32_t>(offsetof(Page_t770747966, ___viewStateEncryptionMode_89)); }
	inline int32_t get_viewStateEncryptionMode_89() const { return ___viewStateEncryptionMode_89; }
	inline int32_t* get_address_of_viewStateEncryptionMode_89() { return &___viewStateEncryptionMode_89; }
	inline void set_viewStateEncryptionMode_89(int32_t value)
	{
		___viewStateEncryptionMode_89 = value;
	}

	inline static int32_t get_offset_of_controlRegisteredForViewStateEncryption_90() { return static_cast<int32_t>(offsetof(Page_t770747966, ___controlRegisteredForViewStateEncryption_90)); }
	inline bool get_controlRegisteredForViewStateEncryption_90() const { return ___controlRegisteredForViewStateEncryption_90; }
	inline bool* get_address_of_controlRegisteredForViewStateEncryption_90() { return &___controlRegisteredForViewStateEncryption_90; }
	inline void set_controlRegisteredForViewStateEncryption_90(bool value)
	{
		___controlRegisteredForViewStateEncryption_90 = value;
	}

	inline static int32_t get_offset_of__validationStartupScript_91() { return static_cast<int32_t>(offsetof(Page_t770747966, ____validationStartupScript_91)); }
	inline String_t* get__validationStartupScript_91() const { return ____validationStartupScript_91; }
	inline String_t** get_address_of__validationStartupScript_91() { return &____validationStartupScript_91; }
	inline void set__validationStartupScript_91(String_t* value)
	{
		____validationStartupScript_91 = value;
		Il2CppCodeGenWriteBarrier(&____validationStartupScript_91, value);
	}

	inline static int32_t get_offset_of__validationOnSubmitStatement_92() { return static_cast<int32_t>(offsetof(Page_t770747966, ____validationOnSubmitStatement_92)); }
	inline String_t* get__validationOnSubmitStatement_92() const { return ____validationOnSubmitStatement_92; }
	inline String_t** get_address_of__validationOnSubmitStatement_92() { return &____validationOnSubmitStatement_92; }
	inline void set__validationOnSubmitStatement_92(String_t* value)
	{
		____validationOnSubmitStatement_92 = value;
		Il2CppCodeGenWriteBarrier(&____validationOnSubmitStatement_92, value);
	}

	inline static int32_t get_offset_of__validationInitializeScript_93() { return static_cast<int32_t>(offsetof(Page_t770747966, ____validationInitializeScript_93)); }
	inline String_t* get__validationInitializeScript_93() const { return ____validationInitializeScript_93; }
	inline String_t** get_address_of__validationInitializeScript_93() { return &____validationInitializeScript_93; }
	inline void set__validationInitializeScript_93(String_t* value)
	{
		____validationInitializeScript_93 = value;
		Il2CppCodeGenWriteBarrier(&____validationInitializeScript_93, value);
	}

	inline static int32_t get_offset_of__webFormScriptReference_94() { return static_cast<int32_t>(offsetof(Page_t770747966, ____webFormScriptReference_94)); }
	inline String_t* get__webFormScriptReference_94() const { return ____webFormScriptReference_94; }
	inline String_t** get_address_of__webFormScriptReference_94() { return &____webFormScriptReference_94; }
	inline void set__webFormScriptReference_94(String_t* value)
	{
		____webFormScriptReference_94 = value;
		Il2CppCodeGenWriteBarrier(&____webFormScriptReference_94, value);
	}

	inline static int32_t get_offset_of_event_mask_101() { return static_cast<int32_t>(offsetof(Page_t770747966, ___event_mask_101)); }
	inline int32_t get_event_mask_101() const { return ___event_mask_101; }
	inline int32_t* get_address_of_event_mask_101() { return &___event_mask_101; }
	inline void set_event_mask_101(int32_t value)
	{
		___event_mask_101 = value;
	}

	inline static int32_t get_offset_of_contentTemplates_106() { return static_cast<int32_t>(offsetof(Page_t770747966, ___contentTemplates_106)); }
	inline Hashtable_t1853889766 * get_contentTemplates_106() const { return ___contentTemplates_106; }
	inline Hashtable_t1853889766 ** get_address_of_contentTemplates_106() { return &___contentTemplates_106; }
	inline void set_contentTemplates_106(Hashtable_t1853889766 * value)
	{
		___contentTemplates_106 = value;
		Il2CppCodeGenWriteBarrier(&___contentTemplates_106, value);
	}

	inline static int32_t get_offset_of__pageTheme_107() { return static_cast<int32_t>(offsetof(Page_t770747966, ____pageTheme_107)); }
	inline PageTheme_t802797672 * get__pageTheme_107() const { return ____pageTheme_107; }
	inline PageTheme_t802797672 ** get_address_of__pageTheme_107() { return &____pageTheme_107; }
	inline void set__pageTheme_107(PageTheme_t802797672 * value)
	{
		____pageTheme_107 = value;
		Il2CppCodeGenWriteBarrier(&____pageTheme_107, value);
	}

	inline static int32_t get_offset_of__styleSheetPageTheme_108() { return static_cast<int32_t>(offsetof(Page_t770747966, ____styleSheetPageTheme_108)); }
	inline PageTheme_t802797672 * get__styleSheetPageTheme_108() const { return ____styleSheetPageTheme_108; }
	inline PageTheme_t802797672 ** get_address_of__styleSheetPageTheme_108() { return &____styleSheetPageTheme_108; }
	inline void set__styleSheetPageTheme_108(PageTheme_t802797672 * value)
	{
		____styleSheetPageTheme_108 = value;
		Il2CppCodeGenWriteBarrier(&____styleSheetPageTheme_108, value);
	}

	inline static int32_t get_offset_of_dataItemCtx_109() { return static_cast<int32_t>(offsetof(Page_t770747966, ___dataItemCtx_109)); }
	inline Stack_t2329662280 * get_dataItemCtx_109() const { return ___dataItemCtx_109; }
	inline Stack_t2329662280 ** get_address_of_dataItemCtx_109() { return &___dataItemCtx_109; }
	inline void set_dataItemCtx_109(Stack_t2329662280 * value)
	{
		___dataItemCtx_109 = value;
		Il2CppCodeGenWriteBarrier(&___dataItemCtx_109, value);
	}
};

struct Page_t770747966_StaticFields
{
public:
	// System.String System.Web.UI.Page::machineKeyConfigPath
	String_t* ___machineKeyConfigPath_36;
	// System.Object System.Web.UI.Page::InitCompleteEvent
	Il2CppObject * ___InitCompleteEvent_95;
	// System.Object System.Web.UI.Page::LoadCompleteEvent
	Il2CppObject * ___LoadCompleteEvent_96;
	// System.Object System.Web.UI.Page::PreInitEvent
	Il2CppObject * ___PreInitEvent_97;
	// System.Object System.Web.UI.Page::PreLoadEvent
	Il2CppObject * ___PreLoadEvent_98;
	// System.Object System.Web.UI.Page::PreRenderCompleteEvent
	Il2CppObject * ___PreRenderCompleteEvent_99;
	// System.Object System.Web.UI.Page::SaveStateCompleteEvent
	Il2CppObject * ___SaveStateCompleteEvent_100;
	// System.Byte[] System.Web.UI.Page::AES_IV
	ByteU5BU5D_t4116647657* ___AES_IV_102;
	// System.Byte[] System.Web.UI.Page::TripleDES_IV
	ByteU5BU5D_t4116647657* ___TripleDES_IV_103;
	// System.Object System.Web.UI.Page::locker
	Il2CppObject * ___locker_104;
	// System.Boolean System.Web.UI.Page::isEncryptionInitialized
	bool ___isEncryptionInitialized_105;

public:
	inline static int32_t get_offset_of_machineKeyConfigPath_36() { return static_cast<int32_t>(offsetof(Page_t770747966_StaticFields, ___machineKeyConfigPath_36)); }
	inline String_t* get_machineKeyConfigPath_36() const { return ___machineKeyConfigPath_36; }
	inline String_t** get_address_of_machineKeyConfigPath_36() { return &___machineKeyConfigPath_36; }
	inline void set_machineKeyConfigPath_36(String_t* value)
	{
		___machineKeyConfigPath_36 = value;
		Il2CppCodeGenWriteBarrier(&___machineKeyConfigPath_36, value);
	}

	inline static int32_t get_offset_of_InitCompleteEvent_95() { return static_cast<int32_t>(offsetof(Page_t770747966_StaticFields, ___InitCompleteEvent_95)); }
	inline Il2CppObject * get_InitCompleteEvent_95() const { return ___InitCompleteEvent_95; }
	inline Il2CppObject ** get_address_of_InitCompleteEvent_95() { return &___InitCompleteEvent_95; }
	inline void set_InitCompleteEvent_95(Il2CppObject * value)
	{
		___InitCompleteEvent_95 = value;
		Il2CppCodeGenWriteBarrier(&___InitCompleteEvent_95, value);
	}

	inline static int32_t get_offset_of_LoadCompleteEvent_96() { return static_cast<int32_t>(offsetof(Page_t770747966_StaticFields, ___LoadCompleteEvent_96)); }
	inline Il2CppObject * get_LoadCompleteEvent_96() const { return ___LoadCompleteEvent_96; }
	inline Il2CppObject ** get_address_of_LoadCompleteEvent_96() { return &___LoadCompleteEvent_96; }
	inline void set_LoadCompleteEvent_96(Il2CppObject * value)
	{
		___LoadCompleteEvent_96 = value;
		Il2CppCodeGenWriteBarrier(&___LoadCompleteEvent_96, value);
	}

	inline static int32_t get_offset_of_PreInitEvent_97() { return static_cast<int32_t>(offsetof(Page_t770747966_StaticFields, ___PreInitEvent_97)); }
	inline Il2CppObject * get_PreInitEvent_97() const { return ___PreInitEvent_97; }
	inline Il2CppObject ** get_address_of_PreInitEvent_97() { return &___PreInitEvent_97; }
	inline void set_PreInitEvent_97(Il2CppObject * value)
	{
		___PreInitEvent_97 = value;
		Il2CppCodeGenWriteBarrier(&___PreInitEvent_97, value);
	}

	inline static int32_t get_offset_of_PreLoadEvent_98() { return static_cast<int32_t>(offsetof(Page_t770747966_StaticFields, ___PreLoadEvent_98)); }
	inline Il2CppObject * get_PreLoadEvent_98() const { return ___PreLoadEvent_98; }
	inline Il2CppObject ** get_address_of_PreLoadEvent_98() { return &___PreLoadEvent_98; }
	inline void set_PreLoadEvent_98(Il2CppObject * value)
	{
		___PreLoadEvent_98 = value;
		Il2CppCodeGenWriteBarrier(&___PreLoadEvent_98, value);
	}

	inline static int32_t get_offset_of_PreRenderCompleteEvent_99() { return static_cast<int32_t>(offsetof(Page_t770747966_StaticFields, ___PreRenderCompleteEvent_99)); }
	inline Il2CppObject * get_PreRenderCompleteEvent_99() const { return ___PreRenderCompleteEvent_99; }
	inline Il2CppObject ** get_address_of_PreRenderCompleteEvent_99() { return &___PreRenderCompleteEvent_99; }
	inline void set_PreRenderCompleteEvent_99(Il2CppObject * value)
	{
		___PreRenderCompleteEvent_99 = value;
		Il2CppCodeGenWriteBarrier(&___PreRenderCompleteEvent_99, value);
	}

	inline static int32_t get_offset_of_SaveStateCompleteEvent_100() { return static_cast<int32_t>(offsetof(Page_t770747966_StaticFields, ___SaveStateCompleteEvent_100)); }
	inline Il2CppObject * get_SaveStateCompleteEvent_100() const { return ___SaveStateCompleteEvent_100; }
	inline Il2CppObject ** get_address_of_SaveStateCompleteEvent_100() { return &___SaveStateCompleteEvent_100; }
	inline void set_SaveStateCompleteEvent_100(Il2CppObject * value)
	{
		___SaveStateCompleteEvent_100 = value;
		Il2CppCodeGenWriteBarrier(&___SaveStateCompleteEvent_100, value);
	}

	inline static int32_t get_offset_of_AES_IV_102() { return static_cast<int32_t>(offsetof(Page_t770747966_StaticFields, ___AES_IV_102)); }
	inline ByteU5BU5D_t4116647657* get_AES_IV_102() const { return ___AES_IV_102; }
	inline ByteU5BU5D_t4116647657** get_address_of_AES_IV_102() { return &___AES_IV_102; }
	inline void set_AES_IV_102(ByteU5BU5D_t4116647657* value)
	{
		___AES_IV_102 = value;
		Il2CppCodeGenWriteBarrier(&___AES_IV_102, value);
	}

	inline static int32_t get_offset_of_TripleDES_IV_103() { return static_cast<int32_t>(offsetof(Page_t770747966_StaticFields, ___TripleDES_IV_103)); }
	inline ByteU5BU5D_t4116647657* get_TripleDES_IV_103() const { return ___TripleDES_IV_103; }
	inline ByteU5BU5D_t4116647657** get_address_of_TripleDES_IV_103() { return &___TripleDES_IV_103; }
	inline void set_TripleDES_IV_103(ByteU5BU5D_t4116647657* value)
	{
		___TripleDES_IV_103 = value;
		Il2CppCodeGenWriteBarrier(&___TripleDES_IV_103, value);
	}

	inline static int32_t get_offset_of_locker_104() { return static_cast<int32_t>(offsetof(Page_t770747966_StaticFields, ___locker_104)); }
	inline Il2CppObject * get_locker_104() const { return ___locker_104; }
	inline Il2CppObject ** get_address_of_locker_104() { return &___locker_104; }
	inline void set_locker_104(Il2CppObject * value)
	{
		___locker_104 = value;
		Il2CppCodeGenWriteBarrier(&___locker_104, value);
	}

	inline static int32_t get_offset_of_isEncryptionInitialized_105() { return static_cast<int32_t>(offsetof(Page_t770747966_StaticFields, ___isEncryptionInitialized_105)); }
	inline bool get_isEncryptionInitialized_105() const { return ___isEncryptionInitialized_105; }
	inline bool* get_address_of_isEncryptionInitialized_105() { return &___isEncryptionInitialized_105; }
	inline void set_isEncryptionInitialized_105(bool value)
	{
		___isEncryptionInitialized_105 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
