﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_Services_System_Web_Services_Descriptio1012652750.h"

// System.Web.Services.Description.ServiceDescriptionFormatExtensionCollection
struct ServiceDescriptionFormatExtensionCollection_t2630153888;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.OperationInput
struct  OperationInput_t2042839389  : public OperationMessage_t1012652750
{
public:
	// System.Web.Services.Description.ServiceDescriptionFormatExtensionCollection System.Web.Services.Description.OperationInput::extensions
	ServiceDescriptionFormatExtensionCollection_t2630153888 * ___extensions_6;

public:
	inline static int32_t get_offset_of_extensions_6() { return static_cast<int32_t>(offsetof(OperationInput_t2042839389, ___extensions_6)); }
	inline ServiceDescriptionFormatExtensionCollection_t2630153888 * get_extensions_6() const { return ___extensions_6; }
	inline ServiceDescriptionFormatExtensionCollection_t2630153888 ** get_address_of_extensions_6() { return &___extensions_6; }
	inline void set_extensions_6(ServiceDescriptionFormatExtensionCollection_t2630153888 * value)
	{
		___extensions_6 = value;
		Il2CppCodeGenWriteBarrier(&___extensions_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
