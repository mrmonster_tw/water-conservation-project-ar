﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "Mono_Web_Mono_Web_Util_SettingsMappingWhatOperatio2264203516.h"

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Web.Util.SettingsMappingWhatContents
struct  SettingsMappingWhatContents_t3035976441  : public Il2CppObject
{
public:
	// Mono.Web.Util.SettingsMappingWhatOperation Mono.Web.Util.SettingsMappingWhatContents::_operation
	int32_t ____operation_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Mono.Web.Util.SettingsMappingWhatContents::_attributes
	Dictionary_2_t1632706988 * ____attributes_1;

public:
	inline static int32_t get_offset_of__operation_0() { return static_cast<int32_t>(offsetof(SettingsMappingWhatContents_t3035976441, ____operation_0)); }
	inline int32_t get__operation_0() const { return ____operation_0; }
	inline int32_t* get_address_of__operation_0() { return &____operation_0; }
	inline void set__operation_0(int32_t value)
	{
		____operation_0 = value;
	}

	inline static int32_t get_offset_of__attributes_1() { return static_cast<int32_t>(offsetof(SettingsMappingWhatContents_t3035976441, ____attributes_1)); }
	inline Dictionary_2_t1632706988 * get__attributes_1() const { return ____attributes_1; }
	inline Dictionary_2_t1632706988 ** get_address_of__attributes_1() { return &____attributes_1; }
	inline void set__attributes_1(Dictionary_2_t1632706988 * value)
	{
		____attributes_1 = value;
		Il2CppCodeGenWriteBarrier(&____attributes_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
