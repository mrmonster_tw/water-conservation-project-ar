﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.ExpressionBuilder
struct  ExpressionBuilder_t2707979487  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct ExpressionBuilder_t2707979487_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Web.Configuration.ExpressionBuilder::expressionPrefixProp
	ConfigurationProperty_t3590861854 * ___expressionPrefixProp_13;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.ExpressionBuilder::typeProp
	ConfigurationProperty_t3590861854 * ___typeProp_14;
	// System.Configuration.ConfigurationPropertyCollection System.Web.Configuration.ExpressionBuilder::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_15;

public:
	inline static int32_t get_offset_of_expressionPrefixProp_13() { return static_cast<int32_t>(offsetof(ExpressionBuilder_t2707979487_StaticFields, ___expressionPrefixProp_13)); }
	inline ConfigurationProperty_t3590861854 * get_expressionPrefixProp_13() const { return ___expressionPrefixProp_13; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_expressionPrefixProp_13() { return &___expressionPrefixProp_13; }
	inline void set_expressionPrefixProp_13(ConfigurationProperty_t3590861854 * value)
	{
		___expressionPrefixProp_13 = value;
		Il2CppCodeGenWriteBarrier(&___expressionPrefixProp_13, value);
	}

	inline static int32_t get_offset_of_typeProp_14() { return static_cast<int32_t>(offsetof(ExpressionBuilder_t2707979487_StaticFields, ___typeProp_14)); }
	inline ConfigurationProperty_t3590861854 * get_typeProp_14() const { return ___typeProp_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_typeProp_14() { return &___typeProp_14; }
	inline void set_typeProp_14(ConfigurationProperty_t3590861854 * value)
	{
		___typeProp_14 = value;
		Il2CppCodeGenWriteBarrier(&___typeProp_14, value);
	}

	inline static int32_t get_offset_of_properties_15() { return static_cast<int32_t>(offsetof(ExpressionBuilder_t2707979487_StaticFields, ___properties_15)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_15() const { return ___properties_15; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_15() { return &___properties_15; }
	inline void set_properties_15(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_15 = value;
		Il2CppCodeGenWriteBarrier(&___properties_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
