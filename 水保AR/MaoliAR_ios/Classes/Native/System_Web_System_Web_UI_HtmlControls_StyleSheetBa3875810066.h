﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.ArrayList
struct ArrayList_t2718874744;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.HtmlControls.StyleSheetBag
struct  StyleSheetBag_t3875810066  : public Il2CppObject
{
public:
	// System.Collections.ArrayList System.Web.UI.HtmlControls.StyleSheetBag::entries
	ArrayList_t2718874744 * ___entries_0;

public:
	inline static int32_t get_offset_of_entries_0() { return static_cast<int32_t>(offsetof(StyleSheetBag_t3875810066, ___entries_0)); }
	inline ArrayList_t2718874744 * get_entries_0() const { return ___entries_0; }
	inline ArrayList_t2718874744 ** get_address_of_entries_0() { return &___entries_0; }
	inline void set_entries_0(ArrayList_t2718874744 * value)
	{
		___entries_0 = value;
		Il2CppCodeGenWriteBarrier(&___entries_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
