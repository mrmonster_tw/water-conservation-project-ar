﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Xml.XmlDictionary
struct XmlDictionary_t238028028;
// System.Collections.Generic.Dictionary`2<System.String,System.Xml.XmlDictionaryString>
struct Dictionary_2_t3289376565;
// System.Collections.Generic.List`1<System.Xml.XmlDictionaryString>
struct List_1_t681227712;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDictionary
struct  XmlDictionary_t238028028  : public Il2CppObject
{
public:
	// System.Boolean System.Xml.XmlDictionary::is_readonly
	bool ___is_readonly_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Xml.XmlDictionaryString> System.Xml.XmlDictionary::dict
	Dictionary_2_t3289376565 * ___dict_2;
	// System.Collections.Generic.List`1<System.Xml.XmlDictionaryString> System.Xml.XmlDictionary::list
	List_1_t681227712 * ___list_3;

public:
	inline static int32_t get_offset_of_is_readonly_1() { return static_cast<int32_t>(offsetof(XmlDictionary_t238028028, ___is_readonly_1)); }
	inline bool get_is_readonly_1() const { return ___is_readonly_1; }
	inline bool* get_address_of_is_readonly_1() { return &___is_readonly_1; }
	inline void set_is_readonly_1(bool value)
	{
		___is_readonly_1 = value;
	}

	inline static int32_t get_offset_of_dict_2() { return static_cast<int32_t>(offsetof(XmlDictionary_t238028028, ___dict_2)); }
	inline Dictionary_2_t3289376565 * get_dict_2() const { return ___dict_2; }
	inline Dictionary_2_t3289376565 ** get_address_of_dict_2() { return &___dict_2; }
	inline void set_dict_2(Dictionary_2_t3289376565 * value)
	{
		___dict_2 = value;
		Il2CppCodeGenWriteBarrier(&___dict_2, value);
	}

	inline static int32_t get_offset_of_list_3() { return static_cast<int32_t>(offsetof(XmlDictionary_t238028028, ___list_3)); }
	inline List_1_t681227712 * get_list_3() const { return ___list_3; }
	inline List_1_t681227712 ** get_address_of_list_3() { return &___list_3; }
	inline void set_list_3(List_1_t681227712 * value)
	{
		___list_3 = value;
		Il2CppCodeGenWriteBarrier(&___list_3, value);
	}
};

struct XmlDictionary_t238028028_StaticFields
{
public:
	// System.Xml.XmlDictionary System.Xml.XmlDictionary::empty
	XmlDictionary_t238028028 * ___empty_0;

public:
	inline static int32_t get_offset_of_empty_0() { return static_cast<int32_t>(offsetof(XmlDictionary_t238028028_StaticFields, ___empty_0)); }
	inline XmlDictionary_t238028028 * get_empty_0() const { return ___empty_0; }
	inline XmlDictionary_t238028028 ** get_address_of_empty_0() { return &___empty_0; }
	inline void set_empty_0(XmlDictionary_t238028028 * value)
	{
		___empty_0 = value;
		Il2CppCodeGenWriteBarrier(&___empty_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
