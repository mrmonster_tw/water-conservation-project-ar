﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_DateTime3738529785.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.WsuTimestamp
struct  WsuTimestamp_t48765479  : public Il2CppObject
{
public:
	// System.String System.ServiceModel.Channels.WsuTimestamp::id
	String_t* ___id_0;
	// System.DateTime System.ServiceModel.Channels.WsuTimestamp::created
	DateTime_t3738529785  ___created_1;
	// System.DateTime System.ServiceModel.Channels.WsuTimestamp::expires
	DateTime_t3738529785  ___expires_2;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(WsuTimestamp_t48765479, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier(&___id_0, value);
	}

	inline static int32_t get_offset_of_created_1() { return static_cast<int32_t>(offsetof(WsuTimestamp_t48765479, ___created_1)); }
	inline DateTime_t3738529785  get_created_1() const { return ___created_1; }
	inline DateTime_t3738529785 * get_address_of_created_1() { return &___created_1; }
	inline void set_created_1(DateTime_t3738529785  value)
	{
		___created_1 = value;
	}

	inline static int32_t get_offset_of_expires_2() { return static_cast<int32_t>(offsetof(WsuTimestamp_t48765479, ___expires_2)); }
	inline DateTime_t3738529785  get_expires_2() const { return ___expires_2; }
	inline DateTime_t3738529785 * get_address_of_expires_2() { return &___expires_2; }
	inline void set_expires_2(DateTime_t3738529785  value)
	{
		___expires_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
