﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_D2906515918.h"

// System.ServiceModel.Channels.TcpChannelInfo
struct TcpChannelInfo_t709592533;
// System.Net.Sockets.TcpClient
struct TcpClient_t822906377;
// System.ServiceModel.Channels.TcpBinaryFrameManager
struct TcpBinaryFrameManager_t2884120632;
// System.ServiceModel.Channels.TcpDuplexSessionChannel/TcpDuplexSession
struct TcpDuplexSession_t413950622;
// System.ServiceModel.EndpointAddress
struct EndpointAddress_t3119842923;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.TcpDuplexSessionChannel
struct  TcpDuplexSessionChannel_t645469680  : public DuplexChannelBase_t2906515918
{
public:
	// System.ServiceModel.Channels.TcpChannelInfo System.ServiceModel.Channels.TcpDuplexSessionChannel::info
	TcpChannelInfo_t709592533 * ___info_20;
	// System.Net.Sockets.TcpClient System.ServiceModel.Channels.TcpDuplexSessionChannel::client
	TcpClient_t822906377 * ___client_21;
	// System.Boolean System.ServiceModel.Channels.TcpDuplexSessionChannel::is_service_side
	bool ___is_service_side_22;
	// System.ServiceModel.Channels.TcpBinaryFrameManager System.ServiceModel.Channels.TcpDuplexSessionChannel::frame
	TcpBinaryFrameManager_t2884120632 * ___frame_23;
	// System.ServiceModel.Channels.TcpDuplexSessionChannel/TcpDuplexSession System.ServiceModel.Channels.TcpDuplexSessionChannel::session
	TcpDuplexSession_t413950622 * ___session_24;
	// System.ServiceModel.EndpointAddress System.ServiceModel.Channels.TcpDuplexSessionChannel::counterpart_address
	EndpointAddress_t3119842923 * ___counterpart_address_25;

public:
	inline static int32_t get_offset_of_info_20() { return static_cast<int32_t>(offsetof(TcpDuplexSessionChannel_t645469680, ___info_20)); }
	inline TcpChannelInfo_t709592533 * get_info_20() const { return ___info_20; }
	inline TcpChannelInfo_t709592533 ** get_address_of_info_20() { return &___info_20; }
	inline void set_info_20(TcpChannelInfo_t709592533 * value)
	{
		___info_20 = value;
		Il2CppCodeGenWriteBarrier(&___info_20, value);
	}

	inline static int32_t get_offset_of_client_21() { return static_cast<int32_t>(offsetof(TcpDuplexSessionChannel_t645469680, ___client_21)); }
	inline TcpClient_t822906377 * get_client_21() const { return ___client_21; }
	inline TcpClient_t822906377 ** get_address_of_client_21() { return &___client_21; }
	inline void set_client_21(TcpClient_t822906377 * value)
	{
		___client_21 = value;
		Il2CppCodeGenWriteBarrier(&___client_21, value);
	}

	inline static int32_t get_offset_of_is_service_side_22() { return static_cast<int32_t>(offsetof(TcpDuplexSessionChannel_t645469680, ___is_service_side_22)); }
	inline bool get_is_service_side_22() const { return ___is_service_side_22; }
	inline bool* get_address_of_is_service_side_22() { return &___is_service_side_22; }
	inline void set_is_service_side_22(bool value)
	{
		___is_service_side_22 = value;
	}

	inline static int32_t get_offset_of_frame_23() { return static_cast<int32_t>(offsetof(TcpDuplexSessionChannel_t645469680, ___frame_23)); }
	inline TcpBinaryFrameManager_t2884120632 * get_frame_23() const { return ___frame_23; }
	inline TcpBinaryFrameManager_t2884120632 ** get_address_of_frame_23() { return &___frame_23; }
	inline void set_frame_23(TcpBinaryFrameManager_t2884120632 * value)
	{
		___frame_23 = value;
		Il2CppCodeGenWriteBarrier(&___frame_23, value);
	}

	inline static int32_t get_offset_of_session_24() { return static_cast<int32_t>(offsetof(TcpDuplexSessionChannel_t645469680, ___session_24)); }
	inline TcpDuplexSession_t413950622 * get_session_24() const { return ___session_24; }
	inline TcpDuplexSession_t413950622 ** get_address_of_session_24() { return &___session_24; }
	inline void set_session_24(TcpDuplexSession_t413950622 * value)
	{
		___session_24 = value;
		Il2CppCodeGenWriteBarrier(&___session_24, value);
	}

	inline static int32_t get_offset_of_counterpart_address_25() { return static_cast<int32_t>(offsetof(TcpDuplexSessionChannel_t645469680, ___counterpart_address_25)); }
	inline EndpointAddress_t3119842923 * get_counterpart_address_25() const { return ___counterpart_address_25; }
	inline EndpointAddress_t3119842923 ** get_address_of_counterpart_address_25() { return &___counterpart_address_25; }
	inline void set_counterpart_address_25(EndpointAddress_t3119842923 * value)
	{
		___counterpart_address_25 = value;
		Il2CppCodeGenWriteBarrier(&___counterpart_address_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
