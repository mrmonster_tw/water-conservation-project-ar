﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3549286319.h"
#include "System_Transactions_U3CModuleU3E692745525.h"
#include "System_Transactions_System_MonoTODOAttribute4131080581.h"
#include "mscorlib_System_Void1185182177.h"
#include "mscorlib_System_Attribute861562559.h"
#include "mscorlib_System_String1847450689.h"
#include "System_Transactions_System_Transactions_Committable152884219.h"
#include "System_Transactions_System_Transactions_Transactio2865697824.h"
#include "System_Transactions_System_Transactions_Transactio3472000926.h"
#include "mscorlib_System_Runtime_Serialization_Serialization950877179.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon3711869237.h"
#include "mscorlib_System_NotImplementedException3489357830.h"
#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_Threading_WaitHandle1743403487.h"
#include "mscorlib_System_Boolean97287965.h"
#include "System_Transactions_System_Transactions_Enlistment1988529039.h"
#include "System_Transactions_System_Transactions_EnterpriseS695190331.h"
#include "System_Transactions_System_Transactions_IsolationL4247150849.h"
#include "System_Transactions_System_Transactions_PreparingE4199633836.h"
#include "System_Transactions_System_Transactions_SinglePhas2412016327.h"
#include "mscorlib_System_Collections_ArrayList2718874744.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1748158447.h"
#include "mscorlib_System_Collections_Generic_List_1_gen446791853.h"
#include "System_Transactions_System_Transactions_Transactio2459298917.h"
#include "System_Transactions_System_Transactions_Transaction192984806.h"
#include "mscorlib_System_Int322950945753.h"
#include "mscorlib_System_Exception1436737249.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3637402324.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2146457487.h"
#include "System_Transactions_System_Transactions_Transactio1281392668.h"
#include "System_Transactions_System_Transactions_Transactio3249669472.h"
#include "mscorlib_System_InvalidOperationException56020091.h"
#include "System_Transactions_System_Transactions_Transactio2116885608.h"
#include "mscorlib_System_SystemException176217640.h"
#include "mscorlib_System_DateTime3738529785.h"
#include "mscorlib_System_Guid3193532887.h"
#include "System_Transactions_System_Transactions_Transactio2760750674.h"
#include "mscorlib_System_TimeSpan881159249.h"
#include "System_Transactions_System_Transactions_Transactio1495782207.h"

// System.MonoTODOAttribute
struct MonoTODOAttribute_t4131080586;
// System.Attribute
struct Attribute_t861562559;
// System.String
struct String_t;
// System.Transactions.CommittableTransaction
struct CommittableTransaction_t152884219;
// System.Transactions.Transaction
struct Transaction_t3472000926;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.NotImplementedException
struct NotImplementedException_t3489357830;
// System.Object
struct Il2CppObject;
// System.Threading.WaitHandle
struct WaitHandle_t1743403487;
// System.Transactions.Enlistment
struct Enlistment_t1988529039;
// System.Transactions.PreparingEnlistment
struct PreparingEnlistment_t4199633836;
// System.Transactions.IEnlistmentNotification
struct IEnlistmentNotification_t276083705;
// System.Transactions.SinglePhaseEnlistment
struct SinglePhaseEnlistment_t2412016327;
// System.Transactions.ISinglePhaseNotification
struct ISinglePhaseNotification_t3269684407;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Collections.Generic.List`1<System.Transactions.IEnlistmentNotification>
struct List_1_t1748158447;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.Collections.Generic.List`1<System.Transactions.ISinglePhaseNotification>
struct List_1_t446791853;
// System.Transactions.TransactionInformation
struct TransactionInformation_t2459298917;
// System.Exception
struct Exception_t1436737249;
// System.Transactions.TransactionException
struct TransactionException_t1281392668;
// System.Transactions.TransactionScope
struct TransactionScope_t3249669472;
// System.InvalidOperationException
struct InvalidOperationException_t56020091;
// System.Transactions.TransactionAbortedException
struct TransactionAbortedException_t2116885608;
// System.SystemException
struct SystemException_t176217640;
extern Il2CppClass* TransactionOptions_t2865697824_il2cpp_TypeInfo_var;
extern const uint32_t CommittableTransaction__ctor_m771829362_MetadataUsageId;
extern Il2CppClass* NotImplementedException_t3489357830_il2cpp_TypeInfo_var;
extern const uint32_t CommittableTransaction_System_Runtime_Serialization_ISerializable_GetObjectData_m902494777_MetadataUsageId;
extern Il2CppClass* IAsyncResult_t767004451_il2cpp_TypeInfo_var;
extern const uint32_t CommittableTransaction_System_IAsyncResult_get_AsyncWaitHandle_m825552598_MetadataUsageId;
extern const uint32_t CommittableTransaction_System_IAsyncResult_get_CompletedSynchronously_m4024486187_MetadataUsageId;
extern const uint32_t CommittableTransaction_System_IAsyncResult_get_IsCompleted_m2412393092_MetadataUsageId;
extern Il2CppClass* ArrayList_t2718874744_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1748158447_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t446791853_il2cpp_TypeInfo_var;
extern Il2CppClass* TransactionInformation_t2459298917_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m367584120_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m2157780702_MethodInfo_var;
extern const uint32_t Transaction__ctor_m1487238460_MetadataUsageId;
extern const uint32_t Transaction_System_Runtime_Serialization_ISerializable_GetObjectData_m1788113767_MetadataUsageId;
extern Il2CppClass* Transaction_t3472000926_il2cpp_TypeInfo_var;
extern const uint32_t Transaction_get_CurrentInternal_m2539383803_MetadataUsageId;
extern const uint32_t Transaction_set_CurrentInternal_m3438268600_MetadataUsageId;
extern const uint32_t Transaction_Equals_m3288205670_MetadataUsageId;
extern Il2CppClass* TransactionException_t1281392668_il2cpp_TypeInfo_var;
extern Il2CppClass* Enlistment_t1988529039_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnlistmentNotification_t276083705_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t3637402324_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t3640265483_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m197419245_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2109621835_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2553881418_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3966086873_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m673984641_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3658361563;
extern const uint32_t Transaction_Rollback_m3410269003_MetadataUsageId;
extern Il2CppClass* InvalidOperationException_t56020091_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4289221870;
extern const uint32_t Transaction_CommitInternal_m3300735138_MetadataUsageId;
extern Il2CppClass* ISinglePhaseNotification_t3269684407_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m2597957279_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1029352870_MethodInfo_var;
extern const uint32_t Transaction_DoCommit_m4142857842_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4216811564;
extern const uint32_t Transaction_InitScope_m402924676_MetadataUsageId;
extern Il2CppClass* PreparingEnlistment_t4199633836_il2cpp_TypeInfo_var;
extern const uint32_t Transaction_DoPreparePhase_m3654094297_MetadataUsageId;
extern const uint32_t Transaction_DoCommitPhase_m3733774821_MetadataUsageId;
extern Il2CppClass* SinglePhaseEnlistment_t2412016327_il2cpp_TypeInfo_var;
extern const uint32_t Transaction_DoSingleCommit_m37203013_MetadataUsageId;
extern Il2CppClass* TransactionAbortedException_t2116885608_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3179484437;
extern const uint32_t Transaction_CheckAborted_m2334107562_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2911134571;
extern const uint32_t Transaction_EnsureIncompleteCurrentScope_m977027993_MetadataUsageId;
extern Il2CppClass* Guid_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t3738529785_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3451500438;
extern const uint32_t TransactionInformation__ctor_m903375629_MetadataUsageId;
extern Il2CppClass* TransactionManager_t2760750674_il2cpp_TypeInfo_var;
extern const uint32_t TransactionManager__cctor_m2466764276_MetadataUsageId;
extern const uint32_t TransactionManager_get_DefaultTimeout_m708447596_MetadataUsageId;
extern const uint32_t TransactionOptions_Equals_m3142779001_MetadataUsageId;
extern Il2CppClass* TimeSpan_t881159249_il2cpp_TypeInfo_var;
extern const uint32_t TransactionOptions_op_Equality_m3173535161_MetadataUsageId;
extern const uint32_t TransactionScope__ctor_m3710765102_MetadataUsageId;
extern Il2CppClass* TransactionScope_t3249669472_il2cpp_TypeInfo_var;
extern const uint32_t TransactionScope__ctor_m3704102479_MetadataUsageId;
extern const uint32_t TransactionScope__cctor_m3418824945_MetadataUsageId;
extern const uint32_t TransactionScope_InitTransaction_m517888600_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral111013970;
extern const uint32_t TransactionScope_Complete_m1749845536_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral955001179;
extern Il2CppCodeGenString* _stringLiteral1447952811;
extern const uint32_t TransactionScope_Dispose_m1851654666_MetadataUsageId;



// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const MethodInfo* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t2146457487  List_1_GetEnumerator_m2930774921_gshared (List_1_t257213610 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m470245444_gshared (Enumerator_t2146457487 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2142368520_gshared (Enumerator_t2146457487 * __this, const MethodInfo* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_get_Item_m2287542950_gshared (List_1_t257213610 * __this, int32_t p0, const MethodInfo* method);

// System.Void System.Attribute::.ctor()
extern "C"  void Attribute__ctor_m1529526131 (Attribute_t861562559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.CommittableTransaction::.ctor(System.Transactions.TransactionOptions)
extern "C"  void CommittableTransaction__ctor_m3164134090 (CommittableTransaction_t152884219 * __this, TransactionOptions_t2865697824  ___options0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::.ctor()
extern "C"  void Transaction__ctor_m1487238460 (Transaction_t3472000926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotImplementedException::.ctor()
extern "C"  void NotImplementedException__ctor_m3058704252 (NotImplementedException_t3489357830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Enlistment::.ctor()
extern "C"  void Enlistment__ctor_m1844752968 (Enlistment_t1988529039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::.ctor()
extern "C"  void ArrayList__ctor_m4254721275 (ArrayList_t2718874744 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Transactions.IEnlistmentNotification>::.ctor()
#define List_1__ctor_m367584120(__this, method) ((  void (*) (List_1_t1748158447 *, const MethodInfo*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Transactions.ISinglePhaseNotification>::.ctor()
#define List_1__ctor_m2157780702(__this, method) ((  void (*) (List_1_t446791853 *, const MethodInfo*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void System.Transactions.TransactionInformation::.ctor()
extern "C"  void TransactionInformation__ctor_m903375629 (TransactionInformation_t2459298917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::EnsureIncompleteCurrentScope()
extern "C"  void Transaction_EnsureIncompleteCurrentScope_m977027993 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Transactions.TransactionInformation System.Transactions.Transaction::get_TransactionInformation()
extern "C"  TransactionInformation_t2459298917 * Transaction_get_TransactionInformation_m1885024619 (Transaction_t3472000926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Transactions.TransactionStatus System.Transactions.TransactionInformation::get_Status()
extern "C"  int32_t TransactionInformation_get_Status_m1160921797 (TransactionInformation_t2459298917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::Rollback()
extern "C"  void Transaction_Rollback_m2307478764 (Transaction_t3472000926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Transactions.Transaction::Equals(System.Transactions.Transaction)
extern "C"  bool Transaction_Equals_m4051287636 (Transaction_t3472000926 * __this, Transaction_t3472000926 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Object::ReferenceEquals(System.Object,System.Object)
extern "C"  bool Object_ReferenceEquals_m610702577 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::Rollback(System.Exception)
extern "C"  void Transaction_Rollback_m3533416245 (Transaction_t3472000926 * __this, Exception_t1436737249 * ___ex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::Rollback(System.Exception,System.Transactions.IEnlistmentNotification)
extern "C"  void Transaction_Rollback_m3410269003 (Transaction_t3472000926 * __this, Exception_t1436737249 * ___ex0, Il2CppObject * ___enlisted1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.TransactionException::.ctor(System.String)
extern "C"  void TransactionException__ctor_m2213981865 (TransactionException_t1281392668 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Transactions.IEnlistmentNotification>::GetEnumerator()
#define List_1_GetEnumerator_m197419245(__this, method) ((  Enumerator_t3637402324  (*) (List_1_t1748158447 *, const MethodInfo*))List_1_GetEnumerator_m2930774921_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<System.Transactions.IEnlistmentNotification>::get_Current()
#define Enumerator_get_Current_m2109621835(__this, method) ((  Il2CppObject * (*) (Enumerator_t3637402324 *, const MethodInfo*))Enumerator_get_Current_m470245444_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Transactions.IEnlistmentNotification>::MoveNext()
#define Enumerator_MoveNext_m2553881418(__this, method) ((  bool (*) (Enumerator_t3637402324 *, const MethodInfo*))Enumerator_MoveNext_m2142368520_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Transactions.ISinglePhaseNotification>::get_Count()
#define List_1_get_Count_m3966086873(__this, method) ((  int32_t (*) (List_1_t446791853 *, const MethodInfo*))List_1_get_Count_m2934127733_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<System.Transactions.ISinglePhaseNotification>::get_Item(System.Int32)
#define List_1_get_Item_m673984641(__this, p0, method) ((  Il2CppObject * (*) (List_1_t446791853 *, int32_t, const MethodInfo*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Void System.Transactions.Transaction::set_Aborted(System.Boolean)
extern "C"  void Transaction_set_Aborted_m541403278 (Transaction_t3472000926 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.TransactionInformation::set_Status(System.Transactions.TransactionStatus)
extern "C"  void TransactionInformation_set_Status_m1910598492 (TransactionInformation_t2459298917 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.InvalidOperationException::.ctor(System.String)
extern "C"  void InvalidOperationException__ctor_m237278729 (InvalidOperationException_t56020091 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::DoCommit()
extern "C"  void Transaction_DoCommit_m4142857842 (Transaction_t3472000926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Transactions.TransactionScope System.Transactions.Transaction::get_Scope()
extern "C"  TransactionScope_t3249669472 * Transaction_get_Scope_m2234757998 (Transaction_t3472000926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::CheckAborted()
extern "C"  void Transaction_CheckAborted_m2334107562 (Transaction_t3472000926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<System.Transactions.IEnlistmentNotification>::get_Count()
#define List_1_get_Count_m2597957279(__this, method) ((  int32_t (*) (List_1_t1748158447 *, const MethodInfo*))List_1_get_Count_m2934127733_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<System.Transactions.IEnlistmentNotification>::get_Item(System.Int32)
#define List_1_get_Item_m1029352870(__this, p0, method) ((  Il2CppObject * (*) (List_1_t1748158447 *, int32_t, const MethodInfo*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Void System.Transactions.Transaction::DoSingleCommit(System.Transactions.ISinglePhaseNotification)
extern "C"  void Transaction_DoSingleCommit_m37203013 (Transaction_t3472000926 * __this, Il2CppObject * ___single0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::Complete()
extern "C"  void Transaction_Complete_m1999140662 (Transaction_t3472000926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::DoPreparePhase()
extern "C"  void Transaction_DoPreparePhase_m3654094297 (Transaction_t3472000926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::DoCommitPhase()
extern "C"  void Transaction_DoCommitPhase_m3733774821 (Transaction_t3472000926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::set_Scope(System.Transactions.TransactionScope)
extern "C"  void Transaction_set_Scope_m3616494776 (Transaction_t3472000926 * __this, TransactionScope_t3249669472 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.PreparingEnlistment::.ctor(System.Transactions.Transaction,System.Transactions.IEnlistmentNotification)
extern "C"  void PreparingEnlistment__ctor_m3964674037 (PreparingEnlistment_t4199633836 * __this, Transaction_t3472000926 * ___tx0, Il2CppObject * ___enlisted1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Transactions.PreparingEnlistment::get_IsPrepared()
extern "C"  bool PreparingEnlistment_get_IsPrepared_m428429483 (PreparingEnlistment_t4199633836 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.SinglePhaseEnlistment::.ctor(System.Transactions.Transaction,System.Transactions.ISinglePhaseNotification)
extern "C"  void SinglePhaseEnlistment__ctor_m943583346 (SinglePhaseEnlistment_t2412016327 * __this, Transaction_t3472000926 * ___tx0, Il2CppObject * ___enlisted1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.TransactionAbortedException::.ctor(System.String,System.Exception)
extern "C"  void TransactionAbortedException__ctor_m2107813208 (TransactionAbortedException_t2116885608 * __this, String_t* ___message0, Exception_t1436737249 * ___innerException1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Transactions.Transaction System.Transactions.Transaction::get_CurrentInternal()
extern "C"  Transaction_t3472000926 * Transaction_get_CurrentInternal_m2539383803 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Transactions.Transaction::op_Equality(System.Transactions.Transaction,System.Transactions.Transaction)
extern "C"  bool Transaction_op_Equality_m567471953 (Il2CppObject * __this /* static, unused */, Transaction_t3472000926 * ___x0, Transaction_t3472000926 * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Transactions.TransactionScope::get_IsComplete()
extern "C"  bool TransactionScope_get_IsComplete_m171974465 (TransactionScope_t3249669472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.TransactionException::.ctor()
extern "C"  void TransactionException__ctor_m1338117290 (TransactionException_t1281392668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.TransactionException::.ctor(System.String,System.Exception)
extern "C"  void TransactionException__ctor_m2285503530 (TransactionException_t1281392668 * __this, String_t* ___message0, Exception_t1436737249 * ___innerException1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.TransactionException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void TransactionException__ctor_m96674906 (TransactionException_t1281392668 * __this, SerializationInfo_t950877179 * ___info0, StreamingContext_t3711869237  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.SystemException::.ctor()
extern "C"  void SystemException__ctor_m4274309232 (SystemException_t176217640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.SystemException::.ctor(System.String)
extern "C"  void SystemException__ctor_m3298527747 (SystemException_t176217640 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.SystemException::.ctor(System.String,System.Exception)
extern "C"  void SystemException__ctor_m4132668650 (SystemException_t176217640 * __this, String_t* p0, Exception_t1436737249 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.SystemException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void SystemException__ctor_m1515048899 (SystemException_t176217640 * __this, SerializationInfo_t950877179 * p0, StreamingContext_t3711869237  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.DateTime::get_Now()
extern "C"  DateTime_t3738529785  DateTime_get_Now_m1277138875 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.DateTime::ToUniversalTime()
extern "C"  DateTime_t3738529785  DateTime_ToUniversalTime_m1945318289 (DateTime_t3738529785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid System.Guid::NewGuid()
extern "C"  Guid_t  Guid_NewGuid_m923091018 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Guid::ToString()
extern "C"  String_t* Guid_ToString_m3279186591 (Guid_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m3937257545 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.TimeSpan::.ctor(System.Int32,System.Int32,System.Int32)
extern "C"  void TimeSpan__ctor_m3689759052 (TimeSpan_t881159249 * __this, int32_t p0, int32_t p1, int32_t p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.TransactionOptions::.ctor(System.Transactions.IsolationLevel,System.TimeSpan)
extern "C"  void TransactionOptions__ctor_m175069428 (TransactionOptions_t2865697824 * __this, int32_t ___level0, TimeSpan_t881159249  ___timeout1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Transactions.TransactionOptions::op_Equality(System.Transactions.TransactionOptions,System.Transactions.TransactionOptions)
extern "C"  bool TransactionOptions_op_Equality_m3173535161 (Il2CppObject * __this /* static, unused */, TransactionOptions_t2865697824  ___o10, TransactionOptions_t2865697824  ___o21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Transactions.TransactionOptions::Equals(System.Object)
extern "C"  bool TransactionOptions_Equals_m3142779001 (TransactionOptions_t2865697824 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.TimeSpan::GetHashCode()
extern "C"  int32_t TimeSpan_GetHashCode_m1939414618 (TimeSpan_t881159249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Transactions.TransactionOptions::GetHashCode()
extern "C"  int32_t TransactionOptions_GetHashCode_m1806108975 (TransactionOptions_t2865697824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.TimeSpan::op_Equality(System.TimeSpan,System.TimeSpan)
extern "C"  bool TimeSpan_op_Equality_m1999885032 (Il2CppObject * __this /* static, unused */, TimeSpan_t881159249  p0, TimeSpan_t881159249  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan System.Transactions.TransactionManager::get_DefaultTimeout()
extern "C"  TimeSpan_t881159249  TransactionManager_get_DefaultTimeout_m708447596 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.TransactionScope::.ctor(System.Transactions.Transaction,System.TimeSpan)
extern "C"  void TransactionScope__ctor_m2571889887 (TransactionScope_t3249669472 * __this, Transaction_t3472000926 * ___transaction0, TimeSpan_t881159249  ___timeout1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.TransactionScope::.ctor(System.Transactions.Transaction,System.TimeSpan,System.Transactions.EnterpriseServicesInteropOption)
extern "C"  void TransactionScope__ctor_m3704102479 (TransactionScope_t3249669472 * __this, Transaction_t3472000926 * ___transaction0, TimeSpan_t881159249  ___timeout1, int32_t ___opt2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.TransactionScope::Initialize(System.Transactions.TransactionScopeOption,System.Transactions.Transaction,System.Transactions.TransactionOptions,System.Transactions.EnterpriseServicesInteropOption,System.TimeSpan)
extern "C"  void TransactionScope_Initialize_m4122624809 (TransactionScope_t3249669472 * __this, int32_t ___scopeOption0, Transaction_t3472000926 * ___tx1, TransactionOptions_t2865697824  ___options2, int32_t ___interop3, TimeSpan_t881159249  ___timeout4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Transactions.Transaction System.Transactions.TransactionScope::InitTransaction(System.Transactions.Transaction,System.Transactions.TransactionScopeOption)
extern "C"  Transaction_t3472000926 * TransactionScope_InitTransaction_m517888600 (TransactionScope_t3249669472 * __this, Transaction_t3472000926 * ___tx0, int32_t ___scopeOption1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::set_CurrentInternal(System.Transactions.Transaction)
extern "C"  void Transaction_set_CurrentInternal_m3438268600 (Il2CppObject * __this /* static, unused */, Transaction_t3472000926 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Transactions.Transaction::op_Inequality(System.Transactions.Transaction,System.Transactions.Transaction)
extern "C"  bool Transaction_op_Inequality_m2626562296 (Il2CppObject * __this /* static, unused */, Transaction_t3472000926 * ___x0, Transaction_t3472000926 * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::InitScope(System.Transactions.TransactionScope)
extern "C"  void Transaction_InitScope_m402924676 (Transaction_t3472000926 * __this, TransactionScope_t3249669472 * ___scope0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::CommitInternal()
extern "C"  void Transaction_CommitInternal_m3300735138 (Transaction_t3472000926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.MonoTODOAttribute::.ctor()
extern "C"  void MonoTODOAttribute__ctor_m2076838029 (MonoTODOAttribute_t4131080586 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.MonoTODOAttribute::.ctor(System.String)
extern "C"  void MonoTODOAttribute__ctor_m3846736629 (MonoTODOAttribute_t4131080586 * __this, String_t* ___comment0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___comment0;
		__this->set_comment_0(L_0);
		return;
	}
}
// System.Void System.Transactions.CommittableTransaction::.ctor()
extern "C"  void CommittableTransaction__ctor_m771829362 (CommittableTransaction_t152884219 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CommittableTransaction__ctor_m771829362_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TransactionOptions_t2865697824  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (TransactionOptions_t2865697824_il2cpp_TypeInfo_var, (&V_0));
		TransactionOptions_t2865697824  L_0 = V_0;
		CommittableTransaction__ctor_m3164134090(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.CommittableTransaction::.ctor(System.Transactions.TransactionOptions)
extern "C"  void CommittableTransaction__ctor_m3164134090 (CommittableTransaction_t152884219 * __this, TransactionOptions_t2865697824  ___options0, const MethodInfo* method)
{
	{
		Transaction__ctor_m1487238460(__this, /*hidden argument*/NULL);
		TransactionOptions_t2865697824  L_0 = ___options0;
		__this->set_options_11(L_0);
		return;
	}
}
// System.Void System.Transactions.CommittableTransaction::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void CommittableTransaction_System_Runtime_Serialization_ISerializable_GetObjectData_m902494777 (CommittableTransaction_t152884219 * __this, SerializationInfo_t950877179 * ___info0, StreamingContext_t3711869237  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CommittableTransaction_System_Runtime_Serialization_ISerializable_GetObjectData_m902494777_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t3489357830 * L_0 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Object System.Transactions.CommittableTransaction::System.IAsyncResult.get_AsyncState()
extern "C"  Il2CppObject * CommittableTransaction_System_IAsyncResult_get_AsyncState_m796330198 (CommittableTransaction_t152884219 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_user_defined_state_12();
		return L_0;
	}
}
// System.Threading.WaitHandle System.Transactions.CommittableTransaction::System.IAsyncResult.get_AsyncWaitHandle()
extern "C"  WaitHandle_t1743403487 * CommittableTransaction_System_IAsyncResult_get_AsyncWaitHandle_m825552598 (CommittableTransaction_t152884219 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CommittableTransaction_System_IAsyncResult_get_AsyncWaitHandle_m825552598_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_asyncResult_13();
		NullCheck(L_0);
		WaitHandle_t1743403487 * L_1 = InterfaceFuncInvoker0< WaitHandle_t1743403487 * >::Invoke(1 /* System.Threading.WaitHandle System.IAsyncResult::get_AsyncWaitHandle() */, IAsyncResult_t767004451_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean System.Transactions.CommittableTransaction::System.IAsyncResult.get_CompletedSynchronously()
extern "C"  bool CommittableTransaction_System_IAsyncResult_get_CompletedSynchronously_m4024486187 (CommittableTransaction_t152884219 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CommittableTransaction_System_IAsyncResult_get_CompletedSynchronously_m4024486187_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_asyncResult_13();
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(2 /* System.Boolean System.IAsyncResult::get_CompletedSynchronously() */, IAsyncResult_t767004451_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean System.Transactions.CommittableTransaction::System.IAsyncResult.get_IsCompleted()
extern "C"  bool CommittableTransaction_System_IAsyncResult_get_IsCompleted_m2412393092 (CommittableTransaction_t152884219 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CommittableTransaction_System_IAsyncResult_get_IsCompleted_m2412393092_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_asyncResult_13();
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(3 /* System.Boolean System.IAsyncResult::get_IsCompleted() */, IAsyncResult_t767004451_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Void System.Transactions.Enlistment::.ctor()
extern "C"  void Enlistment__ctor_m1844752968 (Enlistment_t1988529039 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		__this->set_done_0((bool)0);
		return;
	}
}
// System.Void System.Transactions.PreparingEnlistment::.ctor(System.Transactions.Transaction,System.Transactions.IEnlistmentNotification)
extern "C"  void PreparingEnlistment__ctor_m3964674037 (PreparingEnlistment_t4199633836 * __this, Transaction_t3472000926 * ___tx0, Il2CppObject * ___enlisted1, const MethodInfo* method)
{
	{
		Enlistment__ctor_m1844752968(__this, /*hidden argument*/NULL);
		Transaction_t3472000926 * L_0 = ___tx0;
		__this->set_tx_2(L_0);
		Il2CppObject * L_1 = ___enlisted1;
		__this->set_enlisted_3(L_1);
		return;
	}
}
// System.Boolean System.Transactions.PreparingEnlistment::get_IsPrepared()
extern "C"  bool PreparingEnlistment_get_IsPrepared_m428429483 (PreparingEnlistment_t4199633836 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_prepared_1();
		return L_0;
	}
}
// System.Void System.Transactions.SinglePhaseEnlistment::.ctor(System.Transactions.Transaction,System.Transactions.ISinglePhaseNotification)
extern "C"  void SinglePhaseEnlistment__ctor_m943583346 (SinglePhaseEnlistment_t2412016327 * __this, Transaction_t3472000926 * ___tx0, Il2CppObject * ___enlisted1, const MethodInfo* method)
{
	{
		Enlistment__ctor_m1844752968(__this, /*hidden argument*/NULL);
		Transaction_t3472000926 * L_0 = ___tx0;
		__this->set_tx_1(L_0);
		Il2CppObject * L_1 = ___enlisted1;
		__this->set_enlisted_2(L_1);
		return;
	}
}
// System.Void System.Transactions.Transaction::.ctor()
extern "C"  void Transaction__ctor_m1487238460 (Transaction_t3472000926 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transaction__ctor_m1487238460_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ArrayList_t2718874744 * L_0 = (ArrayList_t2718874744 *)il2cpp_codegen_object_new(ArrayList_t2718874744_il2cpp_TypeInfo_var);
		ArrayList__ctor_m4254721275(L_0, /*hidden argument*/NULL);
		__this->set_dependents_3(L_0);
		List_1_t1748158447 * L_1 = (List_1_t1748158447 *)il2cpp_codegen_object_new(List_1_t1748158447_il2cpp_TypeInfo_var);
		List_1__ctor_m367584120(L_1, /*hidden argument*/List_1__ctor_m367584120_MethodInfo_var);
		__this->set_volatiles_4(L_1);
		List_1_t446791853 * L_2 = (List_1_t446791853 *)il2cpp_codegen_object_new(List_1_t446791853_il2cpp_TypeInfo_var);
		List_1__ctor_m2157780702(L_2, /*hidden argument*/List_1__ctor_m2157780702_MethodInfo_var);
		__this->set_durables_5(L_2);
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		TransactionInformation_t2459298917 * L_3 = (TransactionInformation_t2459298917 *)il2cpp_codegen_object_new(TransactionInformation_t2459298917_il2cpp_TypeInfo_var);
		TransactionInformation__ctor_m903375629(L_3, /*hidden argument*/NULL);
		__this->set_info_2(L_3);
		__this->set_level_1(0);
		return;
	}
}
// System.Void System.Transactions.Transaction::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Transaction_System_Runtime_Serialization_ISerializable_GetObjectData_m1788113767 (Transaction_t3472000926 * __this, SerializationInfo_t950877179 * ___info0, StreamingContext_t3711869237  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transaction_System_Runtime_Serialization_ISerializable_GetObjectData_m1788113767_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t3489357830 * L_0 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Transactions.Transaction System.Transactions.Transaction::get_CurrentInternal()
extern "C"  Transaction_t3472000926 * Transaction_get_CurrentInternal_m2539383803 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transaction_get_CurrentInternal_m2539383803_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transaction_t3472000926 * L_0 = ((Transaction_t3472000926_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(Transaction_t3472000926_il2cpp_TypeInfo_var))->get_ambient_0();
		return L_0;
	}
}
// System.Void System.Transactions.Transaction::set_CurrentInternal(System.Transactions.Transaction)
extern "C"  void Transaction_set_CurrentInternal_m3438268600 (Il2CppObject * __this /* static, unused */, Transaction_t3472000926 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transaction_set_CurrentInternal_m3438268600_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transaction_t3472000926 * L_0 = ___value0;
		((Transaction_t3472000926_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(Transaction_t3472000926_il2cpp_TypeInfo_var))->set_ambient_0(L_0);
		return;
	}
}
// System.Transactions.TransactionInformation System.Transactions.Transaction::get_TransactionInformation()
extern "C"  TransactionInformation_t2459298917 * Transaction_get_TransactionInformation_m1885024619 (Transaction_t3472000926 * __this, const MethodInfo* method)
{
	{
		Transaction_EnsureIncompleteCurrentScope_m977027993(NULL /*static, unused*/, /*hidden argument*/NULL);
		TransactionInformation_t2459298917 * L_0 = __this->get_info_2();
		return L_0;
	}
}
// System.Void System.Transactions.Transaction::Dispose()
extern "C"  void Transaction_Dispose_m2395642011 (Transaction_t3472000926 * __this, const MethodInfo* method)
{
	{
		TransactionInformation_t2459298917 * L_0 = Transaction_get_TransactionInformation_m1885024619(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = TransactionInformation_get_Status_m1160921797(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		Transaction_Rollback_m2307478764(__this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Boolean System.Transactions.Transaction::Equals(System.Object)
extern "C"  bool Transaction_Equals_m3288205670 (Transaction_t3472000926 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transaction_Equals_m3288205670_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		bool L_1 = Transaction_Equals_m4051287636(__this, ((Transaction_t3472000926 *)IsInstClass(L_0, Transaction_t3472000926_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Transactions.Transaction::Equals(System.Transactions.Transaction)
extern "C"  bool Transaction_Equals_m4051287636 (Transaction_t3472000926 * __this, Transaction_t3472000926 * ___t0, const MethodInfo* method)
{
	int32_t G_B7_0 = 0;
	{
		Transaction_t3472000926 * L_0 = ___t0;
		bool L_1 = Object_ReferenceEquals_m610702577(NULL /*static, unused*/, L_0, __this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (bool)1;
	}

IL_000e:
	{
		Transaction_t3472000926 * L_2 = ___t0;
		bool L_3 = Object_ReferenceEquals_m610702577(NULL /*static, unused*/, L_2, NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		return (bool)0;
	}

IL_001c:
	{
		int32_t L_4 = __this->get_level_1();
		Transaction_t3472000926 * L_5 = ___t0;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_level_1();
		if ((!(((uint32_t)L_4) == ((uint32_t)L_6))))
		{
			goto IL_003d;
		}
	}
	{
		TransactionInformation_t2459298917 * L_7 = __this->get_info_2();
		Transaction_t3472000926 * L_8 = ___t0;
		NullCheck(L_8);
		TransactionInformation_t2459298917 * L_9 = L_8->get_info_2();
		G_B7_0 = ((((Il2CppObject*)(TransactionInformation_t2459298917 *)L_7) == ((Il2CppObject*)(TransactionInformation_t2459298917 *)L_9))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B7_0 = 0;
	}

IL_003e:
	{
		return (bool)G_B7_0;
	}
}
// System.Int32 System.Transactions.Transaction::GetHashCode()
extern "C"  int32_t Transaction_GetHashCode_m2622560793 (Transaction_t3472000926 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_level_1();
		TransactionInformation_t2459298917 * L_1 = __this->get_info_2();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_1);
		ArrayList_t2718874744 * L_3 = __this->get_dependents_3();
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_3);
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_0^(int32_t)L_2))^(int32_t)L_4));
	}
}
// System.Void System.Transactions.Transaction::Rollback()
extern "C"  void Transaction_Rollback_m2307478764 (Transaction_t3472000926 * __this, const MethodInfo* method)
{
	{
		Transaction_Rollback_m3533416245(__this, (Exception_t1436737249 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.Transaction::Rollback(System.Exception)
extern "C"  void Transaction_Rollback_m3533416245 (Transaction_t3472000926 * __this, Exception_t1436737249 * ___ex0, const MethodInfo* method)
{
	{
		Transaction_EnsureIncompleteCurrentScope_m977027993(NULL /*static, unused*/, /*hidden argument*/NULL);
		Exception_t1436737249 * L_0 = ___ex0;
		Transaction_Rollback_m3410269003(__this, L_0, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.Transaction::Rollback(System.Exception,System.Transactions.IEnlistmentNotification)
extern "C"  void Transaction_Rollback_m3410269003 (Transaction_t3472000926 * __this, Exception_t1436737249 * ___ex0, Il2CppObject * ___enlisted1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transaction_Rollback_m3410269003_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Enlistment_t1988529039 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Enumerator_t3637402324  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = __this->get_aborted_8();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		TransactionInformation_t2459298917 * L_1 = __this->get_info_2();
		NullCheck(L_1);
		int32_t L_2 = TransactionInformation_get_Status_m1160921797(L_1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_0028;
		}
	}
	{
		TransactionException_t1281392668 * L_3 = (TransactionException_t1281392668 *)il2cpp_codegen_object_new(TransactionException_t1281392668_il2cpp_TypeInfo_var);
		TransactionException__ctor_m2213981865(L_3, _stringLiteral3658361563, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0028:
	{
		Exception_t1436737249 * L_4 = ___ex0;
		__this->set_innerException_10(L_4);
		Enlistment_t1988529039 * L_5 = (Enlistment_t1988529039 *)il2cpp_codegen_object_new(Enlistment_t1988529039_il2cpp_TypeInfo_var);
		Enlistment__ctor_m1844752968(L_5, /*hidden argument*/NULL);
		V_0 = L_5;
		List_1_t1748158447 * L_6 = __this->get_volatiles_4();
		NullCheck(L_6);
		Enumerator_t3637402324  L_7 = List_1_GetEnumerator_m197419245(L_6, /*hidden argument*/List_1_GetEnumerator_m197419245_MethodInfo_var);
		V_2 = L_7;
	}

IL_0041:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005c;
		}

IL_0046:
		{
			Il2CppObject * L_8 = Enumerator_get_Current_m2109621835((&V_2), /*hidden argument*/Enumerator_get_Current_m2109621835_MethodInfo_var);
			V_1 = L_8;
			Il2CppObject * L_9 = V_1;
			Il2CppObject * L_10 = ___enlisted1;
			if ((((Il2CppObject*)(Il2CppObject *)L_9) == ((Il2CppObject*)(Il2CppObject *)L_10)))
			{
				goto IL_005c;
			}
		}

IL_0055:
		{
			Il2CppObject * L_11 = V_1;
			Enlistment_t1988529039 * L_12 = V_0;
			NullCheck(L_11);
			InterfaceActionInvoker1< Enlistment_t1988529039 * >::Invoke(2 /* System.Void System.Transactions.IEnlistmentNotification::Rollback(System.Transactions.Enlistment) */, IEnlistmentNotification_t276083705_il2cpp_TypeInfo_var, L_11, L_12);
		}

IL_005c:
		{
			bool L_13 = Enumerator_MoveNext_m2553881418((&V_2), /*hidden argument*/Enumerator_MoveNext_m2553881418_MethodInfo_var);
			if (L_13)
			{
				goto IL_0046;
			}
		}

IL_0068:
		{
			IL2CPP_LEAVE(0x79, FINALLY_006d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_006d;
	}

FINALLY_006d:
	{ // begin finally (depth: 1)
		Enumerator_t3637402324  L_14 = V_2;
		Enumerator_t3637402324  L_15 = L_14;
		Il2CppObject * L_16 = Box(Enumerator_t3637402324_il2cpp_TypeInfo_var, &L_15);
		NullCheck((Il2CppObject *)L_16);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (Il2CppObject *)L_16);
		IL2CPP_END_FINALLY(109)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(109)
	{
		IL2CPP_JUMP_TBL(0x79, IL_0079)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0079:
	{
		List_1_t446791853 * L_17 = __this->get_durables_5();
		NullCheck(L_17);
		int32_t L_18 = List_1_get_Count_m3966086873(L_17, /*hidden argument*/List_1_get_Count_m3966086873_MethodInfo_var);
		if ((((int32_t)L_18) <= ((int32_t)0)))
		{
			goto IL_00ae;
		}
	}
	{
		List_1_t446791853 * L_19 = __this->get_durables_5();
		NullCheck(L_19);
		Il2CppObject * L_20 = List_1_get_Item_m673984641(L_19, 0, /*hidden argument*/List_1_get_Item_m673984641_MethodInfo_var);
		Il2CppObject * L_21 = ___enlisted1;
		if ((((Il2CppObject*)(Il2CppObject *)L_20) == ((Il2CppObject*)(Il2CppObject *)L_21)))
		{
			goto IL_00ae;
		}
	}
	{
		List_1_t446791853 * L_22 = __this->get_durables_5();
		NullCheck(L_22);
		Il2CppObject * L_23 = List_1_get_Item_m673984641(L_22, 0, /*hidden argument*/List_1_get_Item_m673984641_MethodInfo_var);
		Enlistment_t1988529039 * L_24 = V_0;
		NullCheck(L_23);
		InterfaceActionInvoker1< Enlistment_t1988529039 * >::Invoke(2 /* System.Void System.Transactions.IEnlistmentNotification::Rollback(System.Transactions.Enlistment) */, IEnlistmentNotification_t276083705_il2cpp_TypeInfo_var, L_23, L_24);
	}

IL_00ae:
	{
		Transaction_set_Aborted_m541403278(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.Transaction::set_Aborted(System.Boolean)
extern "C"  void Transaction_set_Aborted_m541403278 (Transaction_t3472000926 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_aborted_8(L_0);
		bool L_1 = __this->get_aborted_8();
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		TransactionInformation_t2459298917 * L_2 = __this->get_info_2();
		NullCheck(L_2);
		TransactionInformation_set_Status_m1910598492(L_2, 2, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// System.Transactions.TransactionScope System.Transactions.Transaction::get_Scope()
extern "C"  TransactionScope_t3249669472 * Transaction_get_Scope_m2234757998 (Transaction_t3472000926 * __this, const MethodInfo* method)
{
	{
		TransactionScope_t3249669472 * L_0 = __this->get_scope_9();
		return L_0;
	}
}
// System.Void System.Transactions.Transaction::set_Scope(System.Transactions.TransactionScope)
extern "C"  void Transaction_set_Scope_m3616494776 (Transaction_t3472000926 * __this, TransactionScope_t3249669472 * ___value0, const MethodInfo* method)
{
	{
		TransactionScope_t3249669472 * L_0 = ___value0;
		__this->set_scope_9(L_0);
		return;
	}
}
// System.Void System.Transactions.Transaction::CommitInternal()
extern "C"  void Transaction_CommitInternal_m3300735138 (Transaction_t3472000926 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transaction_CommitInternal_m3300735138_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_committed_7();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = __this->get_committing_6();
		if (!L_1)
		{
			goto IL_0021;
		}
	}

IL_0016:
	{
		InvalidOperationException_t56020091 * L_2 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_2, _stringLiteral4289221870, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0021:
	{
		__this->set_committing_6((bool)1);
		Transaction_DoCommit_m4142857842(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.Transaction::DoCommit()
extern "C"  void Transaction_DoCommit_m4142857842 (Transaction_t3472000926 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transaction_DoCommit_m4142857842_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		TransactionScope_t3249669472 * L_0 = Transaction_get_Scope_m2234757998(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		Transaction_Rollback_m3410269003(__this, (Exception_t1436737249 *)NULL, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		Transaction_CheckAborted_m2334107562(__this, /*hidden argument*/NULL);
	}

IL_0019:
	{
		List_1_t1748158447 * L_1 = __this->get_volatiles_4();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m2597957279(L_1, /*hidden argument*/List_1_get_Count_m2597957279_MethodInfo_var);
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_0060;
		}
	}
	{
		List_1_t446791853 * L_3 = __this->get_durables_5();
		NullCheck(L_3);
		int32_t L_4 = List_1_get_Count_m3966086873(L_3, /*hidden argument*/List_1_get_Count_m3966086873_MethodInfo_var);
		if (L_4)
		{
			goto IL_0060;
		}
	}
	{
		List_1_t1748158447 * L_5 = __this->get_volatiles_4();
		NullCheck(L_5);
		Il2CppObject * L_6 = List_1_get_Item_m1029352870(L_5, 0, /*hidden argument*/List_1_get_Item_m1029352870_MethodInfo_var);
		V_0 = ((Il2CppObject *)IsInst(L_6, ISinglePhaseNotification_t3269684407_il2cpp_TypeInfo_var));
		Il2CppObject * L_7 = V_0;
		if (!L_7)
		{
			goto IL_0060;
		}
	}
	{
		Il2CppObject * L_8 = V_0;
		Transaction_DoSingleCommit_m37203013(__this, L_8, /*hidden argument*/NULL);
		Transaction_Complete_m1999140662(__this, /*hidden argument*/NULL);
		return;
	}

IL_0060:
	{
		List_1_t1748158447 * L_9 = __this->get_volatiles_4();
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Count_m2597957279(L_9, /*hidden argument*/List_1_get_Count_m2597957279_MethodInfo_var);
		if ((((int32_t)L_10) <= ((int32_t)0)))
		{
			goto IL_0077;
		}
	}
	{
		Transaction_DoPreparePhase_m3654094297(__this, /*hidden argument*/NULL);
	}

IL_0077:
	{
		List_1_t446791853 * L_11 = __this->get_durables_5();
		NullCheck(L_11);
		int32_t L_12 = List_1_get_Count_m3966086873(L_11, /*hidden argument*/List_1_get_Count_m3966086873_MethodInfo_var);
		if ((((int32_t)L_12) <= ((int32_t)0)))
		{
			goto IL_009a;
		}
	}
	{
		List_1_t446791853 * L_13 = __this->get_durables_5();
		NullCheck(L_13);
		Il2CppObject * L_14 = List_1_get_Item_m673984641(L_13, 0, /*hidden argument*/List_1_get_Item_m673984641_MethodInfo_var);
		Transaction_DoSingleCommit_m37203013(__this, L_14, /*hidden argument*/NULL);
	}

IL_009a:
	{
		List_1_t1748158447 * L_15 = __this->get_volatiles_4();
		NullCheck(L_15);
		int32_t L_16 = List_1_get_Count_m2597957279(L_15, /*hidden argument*/List_1_get_Count_m2597957279_MethodInfo_var);
		if ((((int32_t)L_16) <= ((int32_t)0)))
		{
			goto IL_00b1;
		}
	}
	{
		Transaction_DoCommitPhase_m3733774821(__this, /*hidden argument*/NULL);
	}

IL_00b1:
	{
		Transaction_Complete_m1999140662(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.Transaction::Complete()
extern "C"  void Transaction_Complete_m1999140662 (Transaction_t3472000926 * __this, const MethodInfo* method)
{
	{
		__this->set_committing_6((bool)0);
		__this->set_committed_7((bool)1);
		bool L_0 = __this->get_aborted_8();
		if (L_0)
		{
			goto IL_0025;
		}
	}
	{
		TransactionInformation_t2459298917 * L_1 = __this->get_info_2();
		NullCheck(L_1);
		TransactionInformation_set_Status_m1910598492(L_1, 1, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Void System.Transactions.Transaction::InitScope(System.Transactions.TransactionScope)
extern "C"  void Transaction_InitScope_m402924676 (Transaction_t3472000926 * __this, TransactionScope_t3249669472 * ___scope0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transaction_InitScope_m402924676_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transaction_CheckAborted_m2334107562(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_committed_7();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, _stringLiteral4216811564, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001c:
	{
		TransactionScope_t3249669472 * L_2 = ___scope0;
		Transaction_set_Scope_m3616494776(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.Transaction::DoPreparePhase()
extern "C"  void Transaction_DoPreparePhase_m3654094297 (Transaction_t3472000926 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transaction_DoPreparePhase_m3654094297_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PreparingEnlistment_t4199633836 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Enumerator_t3637402324  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1748158447 * L_0 = __this->get_volatiles_4();
		NullCheck(L_0);
		Enumerator_t3637402324  L_1 = List_1_GetEnumerator_m197419245(L_0, /*hidden argument*/List_1_GetEnumerator_m197419245_MethodInfo_var);
		V_2 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003f;
		}

IL_0011:
		{
			Il2CppObject * L_2 = Enumerator_get_Current_m2109621835((&V_2), /*hidden argument*/Enumerator_get_Current_m2109621835_MethodInfo_var);
			V_1 = L_2;
			Il2CppObject * L_3 = V_1;
			PreparingEnlistment_t4199633836 * L_4 = (PreparingEnlistment_t4199633836 *)il2cpp_codegen_object_new(PreparingEnlistment_t4199633836_il2cpp_TypeInfo_var);
			PreparingEnlistment__ctor_m3964674037(L_4, __this, L_3, /*hidden argument*/NULL);
			V_0 = L_4;
			Il2CppObject * L_5 = V_1;
			PreparingEnlistment_t4199633836 * L_6 = V_0;
			NullCheck(L_5);
			InterfaceActionInvoker1< PreparingEnlistment_t4199633836 * >::Invoke(1 /* System.Void System.Transactions.IEnlistmentNotification::Prepare(System.Transactions.PreparingEnlistment) */, IEnlistmentNotification_t276083705_il2cpp_TypeInfo_var, L_5, L_6);
			PreparingEnlistment_t4199633836 * L_7 = V_0;
			NullCheck(L_7);
			bool L_8 = PreparingEnlistment_get_IsPrepared_m428429483(L_7, /*hidden argument*/NULL);
			if (L_8)
			{
				goto IL_003f;
			}
		}

IL_0033:
		{
			Transaction_set_Aborted_m541403278(__this, (bool)1, /*hidden argument*/NULL);
			goto IL_004b;
		}

IL_003f:
		{
			bool L_9 = Enumerator_MoveNext_m2553881418((&V_2), /*hidden argument*/Enumerator_MoveNext_m2553881418_MethodInfo_var);
			if (L_9)
			{
				goto IL_0011;
			}
		}

IL_004b:
		{
			IL2CPP_LEAVE(0x5C, FINALLY_0050);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0050;
	}

FINALLY_0050:
	{ // begin finally (depth: 1)
		Enumerator_t3637402324  L_10 = V_2;
		Enumerator_t3637402324  L_11 = L_10;
		Il2CppObject * L_12 = Box(Enumerator_t3637402324_il2cpp_TypeInfo_var, &L_11);
		NullCheck((Il2CppObject *)L_12);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
		IL2CPP_END_FINALLY(80)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(80)
	{
		IL2CPP_JUMP_TBL(0x5C, IL_005c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_005c:
	{
		Transaction_CheckAborted_m2334107562(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.Transaction::DoCommitPhase()
extern "C"  void Transaction_DoCommitPhase_m3733774821 (Transaction_t3472000926 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transaction_DoCommitPhase_m3733774821_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Enumerator_t3637402324  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enlistment_t1988529039 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1748158447 * L_0 = __this->get_volatiles_4();
		NullCheck(L_0);
		Enumerator_t3637402324  L_1 = List_1_GetEnumerator_m197419245(L_0, /*hidden argument*/List_1_GetEnumerator_m197419245_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0026;
		}

IL_0011:
		{
			Il2CppObject * L_2 = Enumerator_get_Current_m2109621835((&V_1), /*hidden argument*/Enumerator_get_Current_m2109621835_MethodInfo_var);
			V_0 = L_2;
			Enlistment_t1988529039 * L_3 = (Enlistment_t1988529039 *)il2cpp_codegen_object_new(Enlistment_t1988529039_il2cpp_TypeInfo_var);
			Enlistment__ctor_m1844752968(L_3, /*hidden argument*/NULL);
			V_2 = L_3;
			Il2CppObject * L_4 = V_0;
			Enlistment_t1988529039 * L_5 = V_2;
			NullCheck(L_4);
			InterfaceActionInvoker1< Enlistment_t1988529039 * >::Invoke(0 /* System.Void System.Transactions.IEnlistmentNotification::Commit(System.Transactions.Enlistment) */, IEnlistmentNotification_t276083705_il2cpp_TypeInfo_var, L_4, L_5);
		}

IL_0026:
		{
			bool L_6 = Enumerator_MoveNext_m2553881418((&V_1), /*hidden argument*/Enumerator_MoveNext_m2553881418_MethodInfo_var);
			if (L_6)
			{
				goto IL_0011;
			}
		}

IL_0032:
		{
			IL2CPP_LEAVE(0x43, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		Enumerator_t3637402324  L_7 = V_1;
		Enumerator_t3637402324  L_8 = L_7;
		Il2CppObject * L_9 = Box(Enumerator_t3637402324_il2cpp_TypeInfo_var, &L_8);
		NullCheck((Il2CppObject *)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
		IL2CPP_END_FINALLY(55)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0043:
	{
		return;
	}
}
// System.Void System.Transactions.Transaction::DoSingleCommit(System.Transactions.ISinglePhaseNotification)
extern "C"  void Transaction_DoSingleCommit_m37203013 (Transaction_t3472000926 * __this, Il2CppObject * ___single0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transaction_DoSingleCommit_m37203013_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SinglePhaseEnlistment_t2412016327 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___single0;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		Il2CppObject * L_1 = ___single0;
		SinglePhaseEnlistment_t2412016327 * L_2 = (SinglePhaseEnlistment_t2412016327 *)il2cpp_codegen_object_new(SinglePhaseEnlistment_t2412016327_il2cpp_TypeInfo_var);
		SinglePhaseEnlistment__ctor_m943583346(L_2, __this, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Il2CppObject * L_3 = ___single0;
		SinglePhaseEnlistment_t2412016327 * L_4 = V_0;
		NullCheck(L_3);
		InterfaceActionInvoker1< SinglePhaseEnlistment_t2412016327 * >::Invoke(0 /* System.Void System.Transactions.ISinglePhaseNotification::SinglePhaseCommit(System.Transactions.SinglePhaseEnlistment) */, ISinglePhaseNotification_t3269684407_il2cpp_TypeInfo_var, L_3, L_4);
		Transaction_CheckAborted_m2334107562(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.Transaction::CheckAborted()
extern "C"  void Transaction_CheckAborted_m2334107562 (Transaction_t3472000926 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transaction_CheckAborted_m2334107562_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_aborted_8();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Exception_t1436737249 * L_1 = __this->get_innerException_10();
		TransactionAbortedException_t2116885608 * L_2 = (TransactionAbortedException_t2116885608 *)il2cpp_codegen_object_new(TransactionAbortedException_t2116885608_il2cpp_TypeInfo_var);
		TransactionAbortedException__ctor_m2107813208(L_2, _stringLiteral3179484437, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001c:
	{
		return;
	}
}
// System.Void System.Transactions.Transaction::EnsureIncompleteCurrentScope()
extern "C"  void Transaction_EnsureIncompleteCurrentScope_m977027993 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transaction_EnsureIncompleteCurrentScope_m977027993_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transaction_t3472000926 * L_0 = Transaction_get_CurrentInternal_m2539383803(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Transaction_op_Equality_m567471953(NULL /*static, unused*/, L_0, (Transaction_t3472000926 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		Transaction_t3472000926 * L_2 = Transaction_get_CurrentInternal_m2539383803(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		TransactionScope_t3249669472 * L_3 = Transaction_get_Scope_m2234757998(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		Transaction_t3472000926 * L_4 = Transaction_get_CurrentInternal_m2539383803(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		TransactionScope_t3249669472 * L_5 = Transaction_get_Scope_m2234757998(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = TransactionScope_get_IsComplete_m171974465(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		InvalidOperationException_t56020091 * L_7 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_7, _stringLiteral2911134571, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_003f:
	{
		return;
	}
}
// System.Boolean System.Transactions.Transaction::op_Equality(System.Transactions.Transaction,System.Transactions.Transaction)
extern "C"  bool Transaction_op_Equality_m567471953 (Il2CppObject * __this /* static, unused */, Transaction_t3472000926 * ___x0, Transaction_t3472000926 * ___y1, const MethodInfo* method)
{
	{
		Transaction_t3472000926 * L_0 = ___x0;
		bool L_1 = Object_ReferenceEquals_m610702577(NULL /*static, unused*/, L_0, NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		Transaction_t3472000926 * L_2 = ___y1;
		bool L_3 = Object_ReferenceEquals_m610702577(NULL /*static, unused*/, L_2, NULL, /*hidden argument*/NULL);
		return L_3;
	}

IL_0014:
	{
		Transaction_t3472000926 * L_4 = ___x0;
		Transaction_t3472000926 * L_5 = ___y1;
		NullCheck(L_4);
		bool L_6 = Transaction_Equals_m4051287636(L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean System.Transactions.Transaction::op_Inequality(System.Transactions.Transaction,System.Transactions.Transaction)
extern "C"  bool Transaction_op_Inequality_m2626562296 (Il2CppObject * __this /* static, unused */, Transaction_t3472000926 * ___x0, Transaction_t3472000926 * ___y1, const MethodInfo* method)
{
	{
		Transaction_t3472000926 * L_0 = ___x0;
		Transaction_t3472000926 * L_1 = ___y1;
		bool L_2 = Transaction_op_Equality_m567471953(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Transactions.TransactionAbortedException::.ctor()
extern "C"  void TransactionAbortedException__ctor_m2820384312 (TransactionAbortedException_t2116885608 * __this, const MethodInfo* method)
{
	{
		TransactionException__ctor_m1338117290(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.TransactionAbortedException::.ctor(System.String,System.Exception)
extern "C"  void TransactionAbortedException__ctor_m2107813208 (TransactionAbortedException_t2116885608 * __this, String_t* ___message0, Exception_t1436737249 * ___innerException1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1436737249 * L_1 = ___innerException1;
		TransactionException__ctor_m2285503530(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.TransactionAbortedException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void TransactionAbortedException__ctor_m1248569569 (TransactionAbortedException_t2116885608 * __this, SerializationInfo_t950877179 * ___info0, StreamingContext_t3711869237  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t950877179 * L_0 = ___info0;
		StreamingContext_t3711869237  L_1 = ___context1;
		TransactionException__ctor_m96674906(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.TransactionException::.ctor()
extern "C"  void TransactionException__ctor_m1338117290 (TransactionException_t1281392668 * __this, const MethodInfo* method)
{
	{
		SystemException__ctor_m4274309232(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.TransactionException::.ctor(System.String)
extern "C"  void TransactionException__ctor_m2213981865 (TransactionException_t1281392668 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		SystemException__ctor_m3298527747(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.TransactionException::.ctor(System.String,System.Exception)
extern "C"  void TransactionException__ctor_m2285503530 (TransactionException_t1281392668 * __this, String_t* ___message0, Exception_t1436737249 * ___innerException1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1436737249 * L_1 = ___innerException1;
		SystemException__ctor_m4132668650(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.TransactionException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void TransactionException__ctor_m96674906 (TransactionException_t1281392668 * __this, SerializationInfo_t950877179 * ___info0, StreamingContext_t3711869237  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t950877179 * L_0 = ___info0;
		StreamingContext_t3711869237  L_1 = ___context1;
		SystemException__ctor_m1515048899(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.TransactionInformation::.ctor()
extern "C"  void TransactionInformation__ctor_m903375629 (TransactionInformation_t2459298917 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransactionInformation__ctor_m903375629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DateTime_t3738529785  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Guid_t  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		Guid_t  L_0 = ((Guid_t_StaticFields*)Guid_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_11();
		__this->set_dtcId_1(L_0);
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		__this->set_status_3(0);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t3738529785_il2cpp_TypeInfo_var);
		DateTime_t3738529785  L_1 = DateTime_get_Now_m1277138875(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		DateTime_t3738529785  L_2 = DateTime_ToUniversalTime_m1945318289((&V_0), /*hidden argument*/NULL);
		__this->set_creation_time_2(L_2);
		Guid_t  L_3 = Guid_NewGuid_m923091018(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = Guid_ToString_m3279186591((&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m3937257545(NULL /*static, unused*/, L_4, _stringLiteral3451500438, /*hidden argument*/NULL);
		__this->set_local_id_0(L_5);
		return;
	}
}
// System.Transactions.TransactionStatus System.Transactions.TransactionInformation::get_Status()
extern "C"  int32_t TransactionInformation_get_Status_m1160921797 (TransactionInformation_t2459298917 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_status_3();
		return L_0;
	}
}
// System.Void System.Transactions.TransactionInformation::set_Status(System.Transactions.TransactionStatus)
extern "C"  void TransactionInformation_set_Status_m1910598492 (TransactionInformation_t2459298917 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_status_3(L_0);
		return;
	}
}
// System.Void System.Transactions.TransactionManager::.cctor()
extern "C"  void TransactionManager__cctor_m2466764276 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransactionManager__cctor_m2466764276_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TimeSpan_t881159249  L_0;
		memset(&L_0, 0, sizeof(L_0));
		TimeSpan__ctor_m3689759052(&L_0, 0, 1, 0, /*hidden argument*/NULL);
		((TransactionManager_t2760750674_StaticFields*)TransactionManager_t2760750674_il2cpp_TypeInfo_var->static_fields)->set_defaultTimeout_0(L_0);
		TimeSpan_t881159249  L_1;
		memset(&L_1, 0, sizeof(L_1));
		TimeSpan__ctor_m3689759052(&L_1, 0, ((int32_t)10), 0, /*hidden argument*/NULL);
		((TransactionManager_t2760750674_StaticFields*)TransactionManager_t2760750674_il2cpp_TypeInfo_var->static_fields)->set_maxTimeout_1(L_1);
		return;
	}
}
// System.TimeSpan System.Transactions.TransactionManager::get_DefaultTimeout()
extern "C"  TimeSpan_t881159249  TransactionManager_get_DefaultTimeout_m708447596 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransactionManager_get_DefaultTimeout_m708447596_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TransactionManager_t2760750674_il2cpp_TypeInfo_var);
		TimeSpan_t881159249  L_0 = ((TransactionManager_t2760750674_StaticFields*)TransactionManager_t2760750674_il2cpp_TypeInfo_var->static_fields)->get_defaultTimeout_0();
		return L_0;
	}
}
// System.Void System.Transactions.TransactionOptions::.ctor(System.Transactions.IsolationLevel,System.TimeSpan)
extern "C"  void TransactionOptions__ctor_m175069428 (TransactionOptions_t2865697824 * __this, int32_t ___level0, TimeSpan_t881159249  ___timeout1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___level0;
		__this->set_level_0(L_0);
		TimeSpan_t881159249  L_1 = ___timeout1;
		__this->set_timeout_1(L_1);
		return;
	}
}
extern "C"  void TransactionOptions__ctor_m175069428_AdjustorThunk (Il2CppObject * __this, int32_t ___level0, TimeSpan_t881159249  ___timeout1, const MethodInfo* method)
{
	TransactionOptions_t2865697824 * _thisAdjusted = reinterpret_cast<TransactionOptions_t2865697824 *>(__this + 1);
	TransactionOptions__ctor_m175069428(_thisAdjusted, ___level0, ___timeout1, method);
}
// System.Boolean System.Transactions.TransactionOptions::Equals(System.Object)
extern "C"  bool TransactionOptions_Equals_m3142779001 (TransactionOptions_t2865697824 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransactionOptions_Equals_m3142779001_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		if (((Il2CppObject *)IsInstSealed(L_0, TransactionOptions_t2865697824_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___obj0;
		bool L_2 = TransactionOptions_op_Equality_m3173535161(NULL /*static, unused*/, (*(TransactionOptions_t2865697824 *)__this), ((*(TransactionOptions_t2865697824 *)((TransactionOptions_t2865697824 *)UnBox(L_1, TransactionOptions_t2865697824_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  bool TransactionOptions_Equals_m3142779001_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	TransactionOptions_t2865697824 * _thisAdjusted = reinterpret_cast<TransactionOptions_t2865697824 *>(__this + 1);
	return TransactionOptions_Equals_m3142779001(_thisAdjusted, ___obj0, method);
}
// System.Int32 System.Transactions.TransactionOptions::GetHashCode()
extern "C"  int32_t TransactionOptions_GetHashCode_m1806108975 (TransactionOptions_t2865697824 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_level_0();
		TimeSpan_t881159249 * L_1 = __this->get_address_of_timeout_1();
		int32_t L_2 = TimeSpan_GetHashCode_m1939414618(L_1, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_0^(int32_t)L_2));
	}
}
extern "C"  int32_t TransactionOptions_GetHashCode_m1806108975_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	TransactionOptions_t2865697824 * _thisAdjusted = reinterpret_cast<TransactionOptions_t2865697824 *>(__this + 1);
	return TransactionOptions_GetHashCode_m1806108975(_thisAdjusted, method);
}
// System.Boolean System.Transactions.TransactionOptions::op_Equality(System.Transactions.TransactionOptions,System.Transactions.TransactionOptions)
extern "C"  bool TransactionOptions_op_Equality_m3173535161 (Il2CppObject * __this /* static, unused */, TransactionOptions_t2865697824  ___o10, TransactionOptions_t2865697824  ___o21, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransactionOptions_op_Equality_m3173535161_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = (&___o10)->get_level_0();
		int32_t L_1 = (&___o21)->get_level_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0028;
		}
	}
	{
		TimeSpan_t881159249  L_2 = (&___o10)->get_timeout_1();
		TimeSpan_t881159249  L_3 = (&___o21)->get_timeout_1();
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t881159249_il2cpp_TypeInfo_var);
		bool L_4 = TimeSpan_op_Equality_m1999885032(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_4));
		goto IL_0029;
	}

IL_0028:
	{
		G_B3_0 = 0;
	}

IL_0029:
	{
		return (bool)G_B3_0;
	}
}
// System.Void System.Transactions.TransactionScope::.ctor(System.Transactions.Transaction)
extern "C"  void TransactionScope__ctor_m3710765102 (TransactionScope_t3249669472 * __this, Transaction_t3472000926 * ___transaction0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransactionScope__ctor_m3710765102_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transaction_t3472000926 * L_0 = ___transaction0;
		IL2CPP_RUNTIME_CLASS_INIT(TransactionManager_t2760750674_il2cpp_TypeInfo_var);
		TimeSpan_t881159249  L_1 = TransactionManager_get_DefaultTimeout_m708447596(NULL /*static, unused*/, /*hidden argument*/NULL);
		TransactionScope__ctor_m2571889887(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.TransactionScope::.ctor(System.Transactions.Transaction,System.TimeSpan)
extern "C"  void TransactionScope__ctor_m2571889887 (TransactionScope_t3249669472 * __this, Transaction_t3472000926 * ___transaction0, TimeSpan_t881159249  ___timeout1, const MethodInfo* method)
{
	{
		Transaction_t3472000926 * L_0 = ___transaction0;
		TimeSpan_t881159249  L_1 = ___timeout1;
		TransactionScope__ctor_m3704102479(__this, L_0, L_1, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.TransactionScope::.ctor(System.Transactions.Transaction,System.TimeSpan,System.Transactions.EnterpriseServicesInteropOption)
extern "C"  void TransactionScope__ctor_m3704102479 (TransactionScope_t3249669472 * __this, Transaction_t3472000926 * ___transaction0, TimeSpan_t881159249  ___timeout1, int32_t ___opt2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransactionScope__ctor_m3704102479_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		Transaction_t3472000926 * L_0 = ___transaction0;
		IL2CPP_RUNTIME_CLASS_INIT(TransactionScope_t3249669472_il2cpp_TypeInfo_var);
		TransactionOptions_t2865697824  L_1 = ((TransactionScope_t3249669472_StaticFields*)TransactionScope_t3249669472_il2cpp_TypeInfo_var->static_fields)->get_defaultOptions_0();
		int32_t L_2 = ___opt2;
		TimeSpan_t881159249  L_3 = ___timeout1;
		TransactionScope_Initialize_m4122624809(__this, 0, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.TransactionScope::.cctor()
extern "C"  void TransactionScope__cctor_m3418824945 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransactionScope__cctor_m3418824945_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TransactionManager_t2760750674_il2cpp_TypeInfo_var);
		TimeSpan_t881159249  L_0 = TransactionManager_get_DefaultTimeout_m708447596(NULL /*static, unused*/, /*hidden argument*/NULL);
		TransactionOptions_t2865697824  L_1;
		memset(&L_1, 0, sizeof(L_1));
		TransactionOptions__ctor_m175069428(&L_1, 0, L_0, /*hidden argument*/NULL);
		((TransactionScope_t3249669472_StaticFields*)TransactionScope_t3249669472_il2cpp_TypeInfo_var->static_fields)->set_defaultOptions_0(L_1);
		return;
	}
}
// System.Void System.Transactions.TransactionScope::Initialize(System.Transactions.TransactionScopeOption,System.Transactions.Transaction,System.Transactions.TransactionOptions,System.Transactions.EnterpriseServicesInteropOption,System.TimeSpan)
extern "C"  void TransactionScope_Initialize_m4122624809 (TransactionScope_t3249669472 * __this, int32_t ___scopeOption0, Transaction_t3472000926 * ___tx1, TransactionOptions_t2865697824  ___options2, int32_t ___interop3, TimeSpan_t881159249  ___timeout4, const MethodInfo* method)
{
	Transaction_t3472000926 * V_0 = NULL;
	{
		__this->set_completed_6((bool)0);
		__this->set_isRoot_7((bool)0);
		__this->set_nested_4(0);
		Transaction_t3472000926 * L_0 = Transaction_get_CurrentInternal_m2539383803(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_oldTransaction_2(L_0);
		Transaction_t3472000926 * L_1 = ___tx1;
		int32_t L_2 = ___scopeOption0;
		Transaction_t3472000926 * L_3 = TransactionScope_InitTransaction_m517888600(__this, L_1, L_2, /*hidden argument*/NULL);
		Transaction_t3472000926 * L_4 = L_3;
		V_0 = L_4;
		__this->set_transaction_1(L_4);
		Transaction_t3472000926 * L_5 = V_0;
		Transaction_set_CurrentInternal_m3438268600(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		Transaction_t3472000926 * L_6 = __this->get_transaction_1();
		bool L_7 = Transaction_op_Inequality_m2626562296(NULL /*static, unused*/, L_6, (Transaction_t3472000926 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0053;
		}
	}
	{
		Transaction_t3472000926 * L_8 = __this->get_transaction_1();
		NullCheck(L_8);
		Transaction_InitScope_m402924676(L_8, __this, /*hidden argument*/NULL);
	}

IL_0053:
	{
		TransactionScope_t3249669472 * L_9 = __this->get_parentScope_3();
		if (!L_9)
		{
			goto IL_0071;
		}
	}
	{
		TransactionScope_t3249669472 * L_10 = __this->get_parentScope_3();
		TransactionScope_t3249669472 * L_11 = L_10;
		NullCheck(L_11);
		int32_t L_12 = L_11->get_nested_4();
		NullCheck(L_11);
		L_11->set_nested_4(((int32_t)((int32_t)L_12+(int32_t)1)));
	}

IL_0071:
	{
		return;
	}
}
// System.Transactions.Transaction System.Transactions.TransactionScope::InitTransaction(System.Transactions.Transaction,System.Transactions.TransactionScopeOption)
extern "C"  Transaction_t3472000926 * TransactionScope_InitTransaction_m517888600 (TransactionScope_t3249669472 * __this, Transaction_t3472000926 * ___tx0, int32_t ___scopeOption1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransactionScope_InitTransaction_m517888600_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transaction_t3472000926 * L_0 = ___tx0;
		bool L_1 = Transaction_op_Inequality_m2626562296(NULL /*static, unused*/, L_0, (Transaction_t3472000926 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		Transaction_t3472000926 * L_2 = ___tx0;
		return L_2;
	}

IL_000e:
	{
		int32_t L_3 = ___scopeOption1;
		if ((!(((uint32_t)L_3) == ((uint32_t)2))))
		{
			goto IL_0037;
		}
	}
	{
		Transaction_t3472000926 * L_4 = Transaction_get_CurrentInternal_m2539383803(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_5 = Transaction_op_Inequality_m2626562296(NULL /*static, unused*/, L_4, (Transaction_t3472000926 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		Transaction_t3472000926 * L_6 = Transaction_get_CurrentInternal_m2539383803(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		TransactionScope_t3249669472 * L_7 = Transaction_get_Scope_m2234757998(L_6, /*hidden argument*/NULL);
		__this->set_parentScope_3(L_7);
	}

IL_0035:
	{
		return (Transaction_t3472000926 *)NULL;
	}

IL_0037:
	{
		int32_t L_8 = ___scopeOption1;
		if (L_8)
		{
			goto IL_0070;
		}
	}
	{
		Transaction_t3472000926 * L_9 = Transaction_get_CurrentInternal_m2539383803(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_10 = Transaction_op_Equality_m567471953(NULL /*static, unused*/, L_9, (Transaction_t3472000926 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_005a;
		}
	}
	{
		__this->set_isRoot_7((bool)1);
		Transaction_t3472000926 * L_11 = (Transaction_t3472000926 *)il2cpp_codegen_object_new(Transaction_t3472000926_il2cpp_TypeInfo_var);
		Transaction__ctor_m1487238460(L_11, /*hidden argument*/NULL);
		return L_11;
	}

IL_005a:
	{
		Transaction_t3472000926 * L_12 = Transaction_get_CurrentInternal_m2539383803(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		TransactionScope_t3249669472 * L_13 = Transaction_get_Scope_m2234757998(L_12, /*hidden argument*/NULL);
		__this->set_parentScope_3(L_13);
		Transaction_t3472000926 * L_14 = Transaction_get_CurrentInternal_m2539383803(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_14;
	}

IL_0070:
	{
		Transaction_t3472000926 * L_15 = Transaction_get_CurrentInternal_m2539383803(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_16 = Transaction_op_Inequality_m2626562296(NULL /*static, unused*/, L_15, (Transaction_t3472000926 *)NULL, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0090;
		}
	}
	{
		Transaction_t3472000926 * L_17 = Transaction_get_CurrentInternal_m2539383803(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_17);
		TransactionScope_t3249669472 * L_18 = Transaction_get_Scope_m2234757998(L_17, /*hidden argument*/NULL);
		__this->set_parentScope_3(L_18);
	}

IL_0090:
	{
		__this->set_isRoot_7((bool)1);
		Transaction_t3472000926 * L_19 = (Transaction_t3472000926 *)il2cpp_codegen_object_new(Transaction_t3472000926_il2cpp_TypeInfo_var);
		Transaction__ctor_m1487238460(L_19, /*hidden argument*/NULL);
		return L_19;
	}
}
// System.Void System.Transactions.TransactionScope::Complete()
extern "C"  void TransactionScope_Complete_m1749845536 (TransactionScope_t3249669472 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransactionScope_Complete_m1749845536_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_completed_6();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, _stringLiteral111013970, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		__this->set_completed_6((bool)1);
		return;
	}
}
// System.Boolean System.Transactions.TransactionScope::get_IsComplete()
extern "C"  bool TransactionScope_get_IsComplete_m171974465 (TransactionScope_t3249669472 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_completed_6();
		return L_0;
	}
}
// System.Void System.Transactions.TransactionScope::Dispose()
extern "C"  void TransactionScope_Dispose_m1851654666 (TransactionScope_t3249669472 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransactionScope_Dispose_m1851654666_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_disposed_5();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		__this->set_disposed_5((bool)1);
		TransactionScope_t3249669472 * L_1 = __this->get_parentScope_3();
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		TransactionScope_t3249669472 * L_2 = __this->get_parentScope_3();
		TransactionScope_t3249669472 * L_3 = L_2;
		NullCheck(L_3);
		int32_t L_4 = L_3->get_nested_4();
		NullCheck(L_3);
		L_3->set_nested_4(((int32_t)((int32_t)L_4-(int32_t)1)));
	}

IL_0031:
	{
		int32_t L_5 = __this->get_nested_4();
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_0053;
		}
	}
	{
		Transaction_t3472000926 * L_6 = __this->get_transaction_1();
		NullCheck(L_6);
		Transaction_Rollback_m2307478764(L_6, /*hidden argument*/NULL);
		InvalidOperationException_t56020091 * L_7 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_7, _stringLiteral955001179, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0053:
	{
		Transaction_t3472000926 * L_8 = Transaction_get_CurrentInternal_m2539383803(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transaction_t3472000926 * L_9 = __this->get_transaction_1();
		bool L_10 = Transaction_op_Inequality_m2626562296(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00a9;
		}
	}
	{
		Transaction_t3472000926 * L_11 = __this->get_transaction_1();
		bool L_12 = Transaction_op_Inequality_m2626562296(NULL /*static, unused*/, L_11, (Transaction_t3472000926 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0084;
		}
	}
	{
		Transaction_t3472000926 * L_13 = __this->get_transaction_1();
		NullCheck(L_13);
		Transaction_Rollback_m2307478764(L_13, /*hidden argument*/NULL);
	}

IL_0084:
	{
		Transaction_t3472000926 * L_14 = Transaction_get_CurrentInternal_m2539383803(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_15 = Transaction_op_Inequality_m2626562296(NULL /*static, unused*/, L_14, (Transaction_t3472000926 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_009e;
		}
	}
	{
		Transaction_t3472000926 * L_16 = Transaction_get_CurrentInternal_m2539383803(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transaction_Rollback_m2307478764(L_16, /*hidden argument*/NULL);
	}

IL_009e:
	{
		InvalidOperationException_t56020091 * L_17 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_17, _stringLiteral1447952811, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_17);
	}

IL_00a9:
	{
		Transaction_t3472000926 * L_18 = Transaction_get_CurrentInternal_m2539383803(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transaction_t3472000926 * L_19 = __this->get_oldTransaction_2();
		bool L_20 = Transaction_op_Equality_m567471953(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00e0;
		}
	}
	{
		Transaction_t3472000926 * L_21 = __this->get_oldTransaction_2();
		bool L_22 = Transaction_op_Inequality_m2626562296(NULL /*static, unused*/, L_21, (Transaction_t3472000926 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00e0;
		}
	}
	{
		Transaction_t3472000926 * L_23 = __this->get_oldTransaction_2();
		TransactionScope_t3249669472 * L_24 = __this->get_parentScope_3();
		NullCheck(L_23);
		Transaction_set_Scope_m3616494776(L_23, L_24, /*hidden argument*/NULL);
	}

IL_00e0:
	{
		Transaction_t3472000926 * L_25 = __this->get_oldTransaction_2();
		Transaction_set_CurrentInternal_m3438268600(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		Transaction_t3472000926 * L_26 = __this->get_transaction_1();
		bool L_27 = Transaction_op_Equality_m567471953(NULL /*static, unused*/, L_26, (Transaction_t3472000926 *)NULL, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00fd;
		}
	}
	{
		return;
	}

IL_00fd:
	{
		Transaction_t3472000926 * L_28 = __this->get_transaction_1();
		NullCheck(L_28);
		Transaction_set_Scope_m3616494776(L_28, (TransactionScope_t3249669472 *)NULL, /*hidden argument*/NULL);
		bool L_29 = TransactionScope_get_IsComplete_m171974465(__this, /*hidden argument*/NULL);
		if (L_29)
		{
			goto IL_0120;
		}
	}
	{
		Transaction_t3472000926 * L_30 = __this->get_transaction_1();
		NullCheck(L_30);
		Transaction_Rollback_m2307478764(L_30, /*hidden argument*/NULL);
		return;
	}

IL_0120:
	{
		bool L_31 = __this->get_isRoot_7();
		if (L_31)
		{
			goto IL_012c;
		}
	}
	{
		return;
	}

IL_012c:
	{
		Transaction_t3472000926 * L_32 = __this->get_transaction_1();
		NullCheck(L_32);
		Transaction_CommitInternal_m3300735138(L_32, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
