﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Tokens_S1678653605.h"

// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Tokens.InMemorySymmetricSecurityKey
struct  InMemorySymmetricSecurityKey_t3171133310  : public SymmetricSecurityKey_t1678653605
{
public:
	// System.Byte[] System.IdentityModel.Tokens.InMemorySymmetricSecurityKey::key
	ByteU5BU5D_t4116647657* ___key_0;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(InMemorySymmetricSecurityKey_t3171133310, ___key_0)); }
	inline ByteU5BU5D_t4116647657* get_key_0() const { return ___key_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(ByteU5BU5D_t4116647657* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier(&___key_0, value);
	}
};

struct InMemorySymmetricSecurityKey_t3171133310_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.IdentityModel.Tokens.InMemorySymmetricSecurityKey::<>f__switch$map1
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map1_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.IdentityModel.Tokens.InMemorySymmetricSecurityKey::<>f__switch$map2
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map2_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_1() { return static_cast<int32_t>(offsetof(InMemorySymmetricSecurityKey_t3171133310_StaticFields, ___U3CU3Ef__switchU24map1_1)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map1_1() const { return ___U3CU3Ef__switchU24map1_1; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map1_1() { return &___U3CU3Ef__switchU24map1_1; }
	inline void set_U3CU3Ef__switchU24map1_1(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2_2() { return static_cast<int32_t>(offsetof(InMemorySymmetricSecurityKey_t3171133310_StaticFields, ___U3CU3Ef__switchU24map2_2)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map2_2() const { return ___U3CU3Ef__switchU24map2_2; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map2_2() { return &___U3CU3Ef__switchU24map2_2; }
	inline void set_U3CU3Ef__switchU24map2_2(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map2_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
