﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.EndpointAddress
struct EndpointAddress_t3119842923;
// System.ServiceModel.Channels.AddressingVersion
struct AddressingVersion_t2257583243;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.MetadataReference
struct  MetadataReference_t2463107448  : public Il2CppObject
{
public:
	// System.ServiceModel.EndpointAddress System.ServiceModel.Description.MetadataReference::address
	EndpointAddress_t3119842923 * ___address_0;
	// System.ServiceModel.Channels.AddressingVersion System.ServiceModel.Description.MetadataReference::address_version
	AddressingVersion_t2257583243 * ___address_version_1;

public:
	inline static int32_t get_offset_of_address_0() { return static_cast<int32_t>(offsetof(MetadataReference_t2463107448, ___address_0)); }
	inline EndpointAddress_t3119842923 * get_address_0() const { return ___address_0; }
	inline EndpointAddress_t3119842923 ** get_address_of_address_0() { return &___address_0; }
	inline void set_address_0(EndpointAddress_t3119842923 * value)
	{
		___address_0 = value;
		Il2CppCodeGenWriteBarrier(&___address_0, value);
	}

	inline static int32_t get_offset_of_address_version_1() { return static_cast<int32_t>(offsetof(MetadataReference_t2463107448, ___address_version_1)); }
	inline AddressingVersion_t2257583243 * get_address_version_1() const { return ___address_version_1; }
	inline AddressingVersion_t2257583243 ** get_address_of_address_version_1() { return &___address_version_1; }
	inline void set_address_version_1(AddressingVersion_t2257583243 * value)
	{
		___address_version_1 = value;
		Il2CppCodeGenWriteBarrier(&___address_version_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
