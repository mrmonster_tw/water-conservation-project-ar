﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Object[]
struct ObjectU5BU5D_t2843939325;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.DefaultOperationInvoker/InvokeAsyncResult
struct  InvokeAsyncResult_t1234526977  : public Il2CppObject
{
public:
	// System.IAsyncResult System.ServiceModel.Dispatcher.DefaultOperationInvoker/InvokeAsyncResult::Source
	Il2CppObject * ___Source_0;
	// System.Object[] System.ServiceModel.Dispatcher.DefaultOperationInvoker/InvokeAsyncResult::Parameters
	ObjectU5BU5D_t2843939325* ___Parameters_1;

public:
	inline static int32_t get_offset_of_Source_0() { return static_cast<int32_t>(offsetof(InvokeAsyncResult_t1234526977, ___Source_0)); }
	inline Il2CppObject * get_Source_0() const { return ___Source_0; }
	inline Il2CppObject ** get_address_of_Source_0() { return &___Source_0; }
	inline void set_Source_0(Il2CppObject * value)
	{
		___Source_0 = value;
		Il2CppCodeGenWriteBarrier(&___Source_0, value);
	}

	inline static int32_t get_offset_of_Parameters_1() { return static_cast<int32_t>(offsetof(InvokeAsyncResult_t1234526977, ___Parameters_1)); }
	inline ObjectU5BU5D_t2843939325* get_Parameters_1() const { return ___Parameters_1; }
	inline ObjectU5BU5D_t2843939325** get_address_of_Parameters_1() { return &___Parameters_1; }
	inline void set_Parameters_1(ObjectU5BU5D_t2843939325* value)
	{
		___Parameters_1 = value;
		Il2CppCodeGenWriteBarrier(&___Parameters_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
