﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_EventArgs3591816995.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.ImageClickEventArgs
struct  ImageClickEventArgs_t1740531002  : public EventArgs_t3591816995
{
public:
	// System.Int32 System.Web.UI.ImageClickEventArgs::X
	int32_t ___X_1;
	// System.Int32 System.Web.UI.ImageClickEventArgs::Y
	int32_t ___Y_2;

public:
	inline static int32_t get_offset_of_X_1() { return static_cast<int32_t>(offsetof(ImageClickEventArgs_t1740531002, ___X_1)); }
	inline int32_t get_X_1() const { return ___X_1; }
	inline int32_t* get_address_of_X_1() { return &___X_1; }
	inline void set_X_1(int32_t value)
	{
		___X_1 = value;
	}

	inline static int32_t get_offset_of_Y_2() { return static_cast<int32_t>(offsetof(ImageClickEventArgs_t1740531002, ___Y_2)); }
	inline int32_t get_Y_2() const { return ___Y_2; }
	inline int32_t* get_address_of_Y_2() { return &___Y_2; }
	inline void set_Y_2(int32_t value)
	{
		___Y_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
