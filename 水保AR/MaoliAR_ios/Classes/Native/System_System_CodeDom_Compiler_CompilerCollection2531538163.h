﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configura446763386.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Collections.Generic.List`1<System.CodeDom.Compiler.CompilerInfo>
struct List_1_t2419869542;
// System.Collections.Generic.Dictionary`2<System.String,System.CodeDom.Compiler.CompilerInfo>
struct Dictionary_2_t733051099;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.Compiler.CompilerCollection
struct  CompilerCollection_t2531538163  : public ConfigurationElementCollection_t446763386
{
public:

public:
};

struct CompilerCollection_t2531538163_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.CodeDom.Compiler.CompilerCollection::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_23;
	// System.Collections.Generic.List`1<System.CodeDom.Compiler.CompilerInfo> System.CodeDom.Compiler.CompilerCollection::compiler_infos
	List_1_t2419869542 * ___compiler_infos_24;
	// System.Collections.Generic.Dictionary`2<System.String,System.CodeDom.Compiler.CompilerInfo> System.CodeDom.Compiler.CompilerCollection::compiler_languages
	Dictionary_2_t733051099 * ___compiler_languages_25;
	// System.Collections.Generic.Dictionary`2<System.String,System.CodeDom.Compiler.CompilerInfo> System.CodeDom.Compiler.CompilerCollection::compiler_extensions
	Dictionary_2_t733051099 * ___compiler_extensions_26;

public:
	inline static int32_t get_offset_of_properties_23() { return static_cast<int32_t>(offsetof(CompilerCollection_t2531538163_StaticFields, ___properties_23)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_23() const { return ___properties_23; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_23() { return &___properties_23; }
	inline void set_properties_23(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_23 = value;
		Il2CppCodeGenWriteBarrier(&___properties_23, value);
	}

	inline static int32_t get_offset_of_compiler_infos_24() { return static_cast<int32_t>(offsetof(CompilerCollection_t2531538163_StaticFields, ___compiler_infos_24)); }
	inline List_1_t2419869542 * get_compiler_infos_24() const { return ___compiler_infos_24; }
	inline List_1_t2419869542 ** get_address_of_compiler_infos_24() { return &___compiler_infos_24; }
	inline void set_compiler_infos_24(List_1_t2419869542 * value)
	{
		___compiler_infos_24 = value;
		Il2CppCodeGenWriteBarrier(&___compiler_infos_24, value);
	}

	inline static int32_t get_offset_of_compiler_languages_25() { return static_cast<int32_t>(offsetof(CompilerCollection_t2531538163_StaticFields, ___compiler_languages_25)); }
	inline Dictionary_2_t733051099 * get_compiler_languages_25() const { return ___compiler_languages_25; }
	inline Dictionary_2_t733051099 ** get_address_of_compiler_languages_25() { return &___compiler_languages_25; }
	inline void set_compiler_languages_25(Dictionary_2_t733051099 * value)
	{
		___compiler_languages_25 = value;
		Il2CppCodeGenWriteBarrier(&___compiler_languages_25, value);
	}

	inline static int32_t get_offset_of_compiler_extensions_26() { return static_cast<int32_t>(offsetof(CompilerCollection_t2531538163_StaticFields, ___compiler_extensions_26)); }
	inline Dictionary_2_t733051099 * get_compiler_extensions_26() const { return ___compiler_extensions_26; }
	inline Dictionary_2_t733051099 ** get_address_of_compiler_extensions_26() { return &___compiler_extensions_26; }
	inline void set_compiler_extensions_26(Dictionary_2_t733051099 * value)
	{
		___compiler_extensions_26 = value;
		Il2CppCodeGenWriteBarrier(&___compiler_extensions_26, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
