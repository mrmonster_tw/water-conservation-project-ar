﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.PropertyInfo>
struct Dictionary_2_t3131689015;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.DataBinder
struct  DataBinder_t4263271672  : public Il2CppObject
{
public:

public:
};

struct DataBinder_t4263271672_ThreadStaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.PropertyInfo> System.Web.UI.DataBinder::dataItemCache
	Dictionary_2_t3131689015 * ___dataItemCache_0;

public:
	inline static int32_t get_offset_of_dataItemCache_0() { return static_cast<int32_t>(offsetof(DataBinder_t4263271672_ThreadStaticFields, ___dataItemCache_0)); }
	inline Dictionary_2_t3131689015 * get_dataItemCache_0() const { return ___dataItemCache_0; }
	inline Dictionary_2_t3131689015 ** get_address_of_dataItemCache_0() { return &___dataItemCache_0; }
	inline void set_dataItemCache_0(Dictionary_2_t3131689015 * value)
	{
		___dataItemCache_0 = value;
		Il2CppCodeGenWriteBarrier(&___dataItemCache_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
