﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// Mono.CodeGeneration.CodeBuilder
struct CodeBuilder_t3190018006;
// System.Type[]
struct TypeU5BU5D_t3940880105;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.CodeGeneration.CodeProperty
struct  CodeProperty_t3279272335  : public Il2CppObject
{
public:
	// System.Reflection.PropertyInfo Mono.CodeGeneration.CodeProperty::propertyInfo
	PropertyInfo_t * ___propertyInfo_0;
	// Mono.CodeGeneration.CodeBuilder Mono.CodeGeneration.CodeProperty::get_builder
	CodeBuilder_t3190018006 * ___get_builder_1;
	// Mono.CodeGeneration.CodeBuilder Mono.CodeGeneration.CodeProperty::set_builder
	CodeBuilder_t3190018006 * ___set_builder_2;
	// System.Type[] Mono.CodeGeneration.CodeProperty::parameterTypes
	TypeU5BU5D_t3940880105* ___parameterTypes_3;

public:
	inline static int32_t get_offset_of_propertyInfo_0() { return static_cast<int32_t>(offsetof(CodeProperty_t3279272335, ___propertyInfo_0)); }
	inline PropertyInfo_t * get_propertyInfo_0() const { return ___propertyInfo_0; }
	inline PropertyInfo_t ** get_address_of_propertyInfo_0() { return &___propertyInfo_0; }
	inline void set_propertyInfo_0(PropertyInfo_t * value)
	{
		___propertyInfo_0 = value;
		Il2CppCodeGenWriteBarrier(&___propertyInfo_0, value);
	}

	inline static int32_t get_offset_of_get_builder_1() { return static_cast<int32_t>(offsetof(CodeProperty_t3279272335, ___get_builder_1)); }
	inline CodeBuilder_t3190018006 * get_get_builder_1() const { return ___get_builder_1; }
	inline CodeBuilder_t3190018006 ** get_address_of_get_builder_1() { return &___get_builder_1; }
	inline void set_get_builder_1(CodeBuilder_t3190018006 * value)
	{
		___get_builder_1 = value;
		Il2CppCodeGenWriteBarrier(&___get_builder_1, value);
	}

	inline static int32_t get_offset_of_set_builder_2() { return static_cast<int32_t>(offsetof(CodeProperty_t3279272335, ___set_builder_2)); }
	inline CodeBuilder_t3190018006 * get_set_builder_2() const { return ___set_builder_2; }
	inline CodeBuilder_t3190018006 ** get_address_of_set_builder_2() { return &___set_builder_2; }
	inline void set_set_builder_2(CodeBuilder_t3190018006 * value)
	{
		___set_builder_2 = value;
		Il2CppCodeGenWriteBarrier(&___set_builder_2, value);
	}

	inline static int32_t get_offset_of_parameterTypes_3() { return static_cast<int32_t>(offsetof(CodeProperty_t3279272335, ___parameterTypes_3)); }
	inline TypeU5BU5D_t3940880105* get_parameterTypes_3() const { return ___parameterTypes_3; }
	inline TypeU5BU5D_t3940880105** get_address_of_parameterTypes_3() { return &___parameterTypes_3; }
	inline void set_parameterTypes_3(TypeU5BU5D_t3940880105* value)
	{
		___parameterTypes_3 = value;
		Il2CppCodeGenWriteBarrier(&___parameterTypes_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
