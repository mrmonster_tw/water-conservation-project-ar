﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.IDictionary
struct IDictionary_t1363984059;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeObject
struct  CodeObject_t3927604602  : public Il2CppObject
{
public:
	// System.Collections.IDictionary System.CodeDom.CodeObject::userData
	Il2CppObject * ___userData_0;

public:
	inline static int32_t get_offset_of_userData_0() { return static_cast<int32_t>(offsetof(CodeObject_t3927604602, ___userData_0)); }
	inline Il2CppObject * get_userData_0() const { return ___userData_0; }
	inline Il2CppObject ** get_address_of_userData_0() { return &___userData_0; }
	inline void set_userData_0(Il2CppObject * value)
	{
		___userData_0 = value;
		Il2CppCodeGenWriteBarrier(&___userData_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
