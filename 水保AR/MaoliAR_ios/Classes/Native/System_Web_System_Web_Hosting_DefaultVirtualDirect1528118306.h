﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_Hosting_VirtualDirectory3182525004.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Hosting.DefaultVirtualDirectory
struct  DefaultVirtualDirectory_t1528118306  : public VirtualDirectory_t3182525004
{
public:
	// System.String System.Web.Hosting.DefaultVirtualDirectory::phys_dir
	String_t* ___phys_dir_2;
	// System.String System.Web.Hosting.DefaultVirtualDirectory::virtual_dir
	String_t* ___virtual_dir_3;

public:
	inline static int32_t get_offset_of_phys_dir_2() { return static_cast<int32_t>(offsetof(DefaultVirtualDirectory_t1528118306, ___phys_dir_2)); }
	inline String_t* get_phys_dir_2() const { return ___phys_dir_2; }
	inline String_t** get_address_of_phys_dir_2() { return &___phys_dir_2; }
	inline void set_phys_dir_2(String_t* value)
	{
		___phys_dir_2 = value;
		Il2CppCodeGenWriteBarrier(&___phys_dir_2, value);
	}

	inline static int32_t get_offset_of_virtual_dir_3() { return static_cast<int32_t>(offsetof(DefaultVirtualDirectory_t1528118306, ___virtual_dir_3)); }
	inline String_t* get_virtual_dir_3() const { return ___virtual_dir_3; }
	inline String_t** get_address_of_virtual_dir_3() { return &___virtual_dir_3; }
	inline void set_virtual_dir_3(String_t* value)
	{
		___virtual_dir_3 = value;
		Il2CppCodeGenWriteBarrier(&___virtual_dir_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
