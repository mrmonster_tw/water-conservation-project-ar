﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.Text.Encoding
struct Encoding_t1523322056;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Resources.ResXFileRef
struct  ResXFileRef_t4219341967  : public Il2CppObject
{
public:
	// System.String System.Resources.ResXFileRef::filename
	String_t* ___filename_0;
	// System.String System.Resources.ResXFileRef::typename
	String_t* ___typename_1;
	// System.Text.Encoding System.Resources.ResXFileRef::textFileEncoding
	Encoding_t1523322056 * ___textFileEncoding_2;

public:
	inline static int32_t get_offset_of_filename_0() { return static_cast<int32_t>(offsetof(ResXFileRef_t4219341967, ___filename_0)); }
	inline String_t* get_filename_0() const { return ___filename_0; }
	inline String_t** get_address_of_filename_0() { return &___filename_0; }
	inline void set_filename_0(String_t* value)
	{
		___filename_0 = value;
		Il2CppCodeGenWriteBarrier(&___filename_0, value);
	}

	inline static int32_t get_offset_of_typename_1() { return static_cast<int32_t>(offsetof(ResXFileRef_t4219341967, ___typename_1)); }
	inline String_t* get_typename_1() const { return ___typename_1; }
	inline String_t** get_address_of_typename_1() { return &___typename_1; }
	inline void set_typename_1(String_t* value)
	{
		___typename_1 = value;
		Il2CppCodeGenWriteBarrier(&___typename_1, value);
	}

	inline static int32_t get_offset_of_textFileEncoding_2() { return static_cast<int32_t>(offsetof(ResXFileRef_t4219341967, ___textFileEncoding_2)); }
	inline Encoding_t1523322056 * get_textFileEncoding_2() const { return ___textFileEncoding_2; }
	inline Encoding_t1523322056 ** get_address_of_textFileEncoding_2() { return &___textFileEncoding_2; }
	inline void set_textFileEncoding_2(Encoding_t1523322056 * value)
	{
		___textFileEncoding_2 = value;
		Il2CppCodeGenWriteBarrier(&___textFileEncoding_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
