﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.PartialCachingAttribute
struct  PartialCachingAttribute_t7999397  : public Attribute_t861562559
{
public:
	// System.Int32 System.Web.UI.PartialCachingAttribute::duration
	int32_t ___duration_0;
	// System.String System.Web.UI.PartialCachingAttribute::varyByControls
	String_t* ___varyByControls_1;
	// System.String System.Web.UI.PartialCachingAttribute::varyByCustom
	String_t* ___varyByCustom_2;
	// System.String System.Web.UI.PartialCachingAttribute::varyByParams
	String_t* ___varyByParams_3;
	// System.Boolean System.Web.UI.PartialCachingAttribute::shared
	bool ___shared_4;

public:
	inline static int32_t get_offset_of_duration_0() { return static_cast<int32_t>(offsetof(PartialCachingAttribute_t7999397, ___duration_0)); }
	inline int32_t get_duration_0() const { return ___duration_0; }
	inline int32_t* get_address_of_duration_0() { return &___duration_0; }
	inline void set_duration_0(int32_t value)
	{
		___duration_0 = value;
	}

	inline static int32_t get_offset_of_varyByControls_1() { return static_cast<int32_t>(offsetof(PartialCachingAttribute_t7999397, ___varyByControls_1)); }
	inline String_t* get_varyByControls_1() const { return ___varyByControls_1; }
	inline String_t** get_address_of_varyByControls_1() { return &___varyByControls_1; }
	inline void set_varyByControls_1(String_t* value)
	{
		___varyByControls_1 = value;
		Il2CppCodeGenWriteBarrier(&___varyByControls_1, value);
	}

	inline static int32_t get_offset_of_varyByCustom_2() { return static_cast<int32_t>(offsetof(PartialCachingAttribute_t7999397, ___varyByCustom_2)); }
	inline String_t* get_varyByCustom_2() const { return ___varyByCustom_2; }
	inline String_t** get_address_of_varyByCustom_2() { return &___varyByCustom_2; }
	inline void set_varyByCustom_2(String_t* value)
	{
		___varyByCustom_2 = value;
		Il2CppCodeGenWriteBarrier(&___varyByCustom_2, value);
	}

	inline static int32_t get_offset_of_varyByParams_3() { return static_cast<int32_t>(offsetof(PartialCachingAttribute_t7999397, ___varyByParams_3)); }
	inline String_t* get_varyByParams_3() const { return ___varyByParams_3; }
	inline String_t** get_address_of_varyByParams_3() { return &___varyByParams_3; }
	inline void set_varyByParams_3(String_t* value)
	{
		___varyByParams_3 = value;
		Il2CppCodeGenWriteBarrier(&___varyByParams_3, value);
	}

	inline static int32_t get_offset_of_shared_4() { return static_cast<int32_t>(offsetof(PartialCachingAttribute_t7999397, ___shared_4)); }
	inline bool get_shared_4() const { return ___shared_4; }
	inline bool* get_address_of_shared_4() { return &___shared_4; }
	inline void set_shared_4(bool value)
	{
		___shared_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
