﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Runtime_Serialization_System_Xml_XmlDiction1958650860.h"
#include "System_Xml_System_Xml_WriteState3983380671.h"
#include "System_Xml_System_Xml_XmlSpace3324193251.h"
#include "System_Runtime_Serialization_System_Xml_XmlBinaryD2611062235.h"

// System.Xml.XmlBinaryDictionaryWriter/MyBinaryWriter
struct MyBinaryWriter_t3654536405;
// System.Xml.IXmlDictionary
struct IXmlDictionary_t3978089843;
// System.Xml.XmlDictionary
struct XmlDictionary_t238028028;
// System.Xml.XmlBinaryWriterSession
struct XmlBinaryWriterSession_t1730638232;
// System.Text.Encoding
struct Encoding_t1523322056;
// System.IO.MemoryStream
struct MemoryStream_t94973147;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>
struct List_1_t2440142076;
// System.String
struct String_t;
// System.Collections.Generic.Stack`1<System.Int32>
struct Stack_1_t3794335208;
// System.Collections.Generic.Stack`1<System.String>
struct Stack_1_t2690840144;
// System.Collections.Generic.Stack`1<System.Xml.XmlSpace>
struct Stack_1_t4167582706;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlBinaryDictionaryWriter
struct  XmlBinaryDictionaryWriter_t676787090  : public XmlDictionaryWriter_t1958650860
{
public:
	// System.Xml.XmlBinaryDictionaryWriter/MyBinaryWriter System.Xml.XmlBinaryDictionaryWriter::original
	MyBinaryWriter_t3654536405 * ___original_3;
	// System.Xml.XmlBinaryDictionaryWriter/MyBinaryWriter System.Xml.XmlBinaryDictionaryWriter::writer
	MyBinaryWriter_t3654536405 * ___writer_4;
	// System.Xml.XmlBinaryDictionaryWriter/MyBinaryWriter System.Xml.XmlBinaryDictionaryWriter::buffer_writer
	MyBinaryWriter_t3654536405 * ___buffer_writer_5;
	// System.Xml.IXmlDictionary System.Xml.XmlBinaryDictionaryWriter::dict_ext
	Il2CppObject * ___dict_ext_6;
	// System.Xml.XmlDictionary System.Xml.XmlBinaryDictionaryWriter::dict_int
	XmlDictionary_t238028028 * ___dict_int_7;
	// System.Xml.XmlBinaryWriterSession System.Xml.XmlBinaryDictionaryWriter::session
	XmlBinaryWriterSession_t1730638232 * ___session_8;
	// System.Boolean System.Xml.XmlBinaryDictionaryWriter::owns_stream
	bool ___owns_stream_9;
	// System.Text.Encoding System.Xml.XmlBinaryDictionaryWriter::utf8Enc
	Encoding_t1523322056 * ___utf8Enc_10;
	// System.IO.MemoryStream System.Xml.XmlBinaryDictionaryWriter::buffer
	MemoryStream_t94973147 * ___buffer_11;
	// System.Xml.WriteState System.Xml.XmlBinaryDictionaryWriter::state
	int32_t ___state_12;
	// System.Boolean System.Xml.XmlBinaryDictionaryWriter::open_start_element
	bool ___open_start_element_13;
	// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>> System.Xml.XmlBinaryDictionaryWriter::namespaces
	List_1_t2440142076 * ___namespaces_14;
	// System.String System.Xml.XmlBinaryDictionaryWriter::xml_lang
	String_t* ___xml_lang_15;
	// System.Xml.XmlSpace System.Xml.XmlBinaryDictionaryWriter::xml_space
	int32_t ___xml_space_16;
	// System.Int32 System.Xml.XmlBinaryDictionaryWriter::ns_index
	int32_t ___ns_index_17;
	// System.Collections.Generic.Stack`1<System.Int32> System.Xml.XmlBinaryDictionaryWriter::ns_index_stack
	Stack_1_t3794335208 * ___ns_index_stack_18;
	// System.Collections.Generic.Stack`1<System.String> System.Xml.XmlBinaryDictionaryWriter::xml_lang_stack
	Stack_1_t2690840144 * ___xml_lang_stack_19;
	// System.Collections.Generic.Stack`1<System.Xml.XmlSpace> System.Xml.XmlBinaryDictionaryWriter::xml_space_stack
	Stack_1_t4167582706 * ___xml_space_stack_20;
	// System.Collections.Generic.Stack`1<System.String> System.Xml.XmlBinaryDictionaryWriter::element_ns_stack
	Stack_1_t2690840144 * ___element_ns_stack_21;
	// System.String System.Xml.XmlBinaryDictionaryWriter::element_ns
	String_t* ___element_ns_22;
	// System.Int32 System.Xml.XmlBinaryDictionaryWriter::element_count
	int32_t ___element_count_23;
	// System.String System.Xml.XmlBinaryDictionaryWriter::element_prefix
	String_t* ___element_prefix_24;
	// System.String System.Xml.XmlBinaryDictionaryWriter::attr_value
	String_t* ___attr_value_25;
	// System.String System.Xml.XmlBinaryDictionaryWriter::current_attr_prefix
	String_t* ___current_attr_prefix_26;
	// System.Object System.Xml.XmlBinaryDictionaryWriter::current_attr_name
	Il2CppObject * ___current_attr_name_27;
	// System.Object System.Xml.XmlBinaryDictionaryWriter::current_attr_ns
	Il2CppObject * ___current_attr_ns_28;
	// System.Boolean System.Xml.XmlBinaryDictionaryWriter::attr_typed_value
	bool ___attr_typed_value_29;
	// System.Xml.XmlBinaryDictionaryWriter/SaveTarget System.Xml.XmlBinaryDictionaryWriter::save_target
	int32_t ___save_target_30;

public:
	inline static int32_t get_offset_of_original_3() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090, ___original_3)); }
	inline MyBinaryWriter_t3654536405 * get_original_3() const { return ___original_3; }
	inline MyBinaryWriter_t3654536405 ** get_address_of_original_3() { return &___original_3; }
	inline void set_original_3(MyBinaryWriter_t3654536405 * value)
	{
		___original_3 = value;
		Il2CppCodeGenWriteBarrier(&___original_3, value);
	}

	inline static int32_t get_offset_of_writer_4() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090, ___writer_4)); }
	inline MyBinaryWriter_t3654536405 * get_writer_4() const { return ___writer_4; }
	inline MyBinaryWriter_t3654536405 ** get_address_of_writer_4() { return &___writer_4; }
	inline void set_writer_4(MyBinaryWriter_t3654536405 * value)
	{
		___writer_4 = value;
		Il2CppCodeGenWriteBarrier(&___writer_4, value);
	}

	inline static int32_t get_offset_of_buffer_writer_5() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090, ___buffer_writer_5)); }
	inline MyBinaryWriter_t3654536405 * get_buffer_writer_5() const { return ___buffer_writer_5; }
	inline MyBinaryWriter_t3654536405 ** get_address_of_buffer_writer_5() { return &___buffer_writer_5; }
	inline void set_buffer_writer_5(MyBinaryWriter_t3654536405 * value)
	{
		___buffer_writer_5 = value;
		Il2CppCodeGenWriteBarrier(&___buffer_writer_5, value);
	}

	inline static int32_t get_offset_of_dict_ext_6() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090, ___dict_ext_6)); }
	inline Il2CppObject * get_dict_ext_6() const { return ___dict_ext_6; }
	inline Il2CppObject ** get_address_of_dict_ext_6() { return &___dict_ext_6; }
	inline void set_dict_ext_6(Il2CppObject * value)
	{
		___dict_ext_6 = value;
		Il2CppCodeGenWriteBarrier(&___dict_ext_6, value);
	}

	inline static int32_t get_offset_of_dict_int_7() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090, ___dict_int_7)); }
	inline XmlDictionary_t238028028 * get_dict_int_7() const { return ___dict_int_7; }
	inline XmlDictionary_t238028028 ** get_address_of_dict_int_7() { return &___dict_int_7; }
	inline void set_dict_int_7(XmlDictionary_t238028028 * value)
	{
		___dict_int_7 = value;
		Il2CppCodeGenWriteBarrier(&___dict_int_7, value);
	}

	inline static int32_t get_offset_of_session_8() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090, ___session_8)); }
	inline XmlBinaryWriterSession_t1730638232 * get_session_8() const { return ___session_8; }
	inline XmlBinaryWriterSession_t1730638232 ** get_address_of_session_8() { return &___session_8; }
	inline void set_session_8(XmlBinaryWriterSession_t1730638232 * value)
	{
		___session_8 = value;
		Il2CppCodeGenWriteBarrier(&___session_8, value);
	}

	inline static int32_t get_offset_of_owns_stream_9() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090, ___owns_stream_9)); }
	inline bool get_owns_stream_9() const { return ___owns_stream_9; }
	inline bool* get_address_of_owns_stream_9() { return &___owns_stream_9; }
	inline void set_owns_stream_9(bool value)
	{
		___owns_stream_9 = value;
	}

	inline static int32_t get_offset_of_utf8Enc_10() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090, ___utf8Enc_10)); }
	inline Encoding_t1523322056 * get_utf8Enc_10() const { return ___utf8Enc_10; }
	inline Encoding_t1523322056 ** get_address_of_utf8Enc_10() { return &___utf8Enc_10; }
	inline void set_utf8Enc_10(Encoding_t1523322056 * value)
	{
		___utf8Enc_10 = value;
		Il2CppCodeGenWriteBarrier(&___utf8Enc_10, value);
	}

	inline static int32_t get_offset_of_buffer_11() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090, ___buffer_11)); }
	inline MemoryStream_t94973147 * get_buffer_11() const { return ___buffer_11; }
	inline MemoryStream_t94973147 ** get_address_of_buffer_11() { return &___buffer_11; }
	inline void set_buffer_11(MemoryStream_t94973147 * value)
	{
		___buffer_11 = value;
		Il2CppCodeGenWriteBarrier(&___buffer_11, value);
	}

	inline static int32_t get_offset_of_state_12() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090, ___state_12)); }
	inline int32_t get_state_12() const { return ___state_12; }
	inline int32_t* get_address_of_state_12() { return &___state_12; }
	inline void set_state_12(int32_t value)
	{
		___state_12 = value;
	}

	inline static int32_t get_offset_of_open_start_element_13() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090, ___open_start_element_13)); }
	inline bool get_open_start_element_13() const { return ___open_start_element_13; }
	inline bool* get_address_of_open_start_element_13() { return &___open_start_element_13; }
	inline void set_open_start_element_13(bool value)
	{
		___open_start_element_13 = value;
	}

	inline static int32_t get_offset_of_namespaces_14() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090, ___namespaces_14)); }
	inline List_1_t2440142076 * get_namespaces_14() const { return ___namespaces_14; }
	inline List_1_t2440142076 ** get_address_of_namespaces_14() { return &___namespaces_14; }
	inline void set_namespaces_14(List_1_t2440142076 * value)
	{
		___namespaces_14 = value;
		Il2CppCodeGenWriteBarrier(&___namespaces_14, value);
	}

	inline static int32_t get_offset_of_xml_lang_15() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090, ___xml_lang_15)); }
	inline String_t* get_xml_lang_15() const { return ___xml_lang_15; }
	inline String_t** get_address_of_xml_lang_15() { return &___xml_lang_15; }
	inline void set_xml_lang_15(String_t* value)
	{
		___xml_lang_15 = value;
		Il2CppCodeGenWriteBarrier(&___xml_lang_15, value);
	}

	inline static int32_t get_offset_of_xml_space_16() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090, ___xml_space_16)); }
	inline int32_t get_xml_space_16() const { return ___xml_space_16; }
	inline int32_t* get_address_of_xml_space_16() { return &___xml_space_16; }
	inline void set_xml_space_16(int32_t value)
	{
		___xml_space_16 = value;
	}

	inline static int32_t get_offset_of_ns_index_17() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090, ___ns_index_17)); }
	inline int32_t get_ns_index_17() const { return ___ns_index_17; }
	inline int32_t* get_address_of_ns_index_17() { return &___ns_index_17; }
	inline void set_ns_index_17(int32_t value)
	{
		___ns_index_17 = value;
	}

	inline static int32_t get_offset_of_ns_index_stack_18() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090, ___ns_index_stack_18)); }
	inline Stack_1_t3794335208 * get_ns_index_stack_18() const { return ___ns_index_stack_18; }
	inline Stack_1_t3794335208 ** get_address_of_ns_index_stack_18() { return &___ns_index_stack_18; }
	inline void set_ns_index_stack_18(Stack_1_t3794335208 * value)
	{
		___ns_index_stack_18 = value;
		Il2CppCodeGenWriteBarrier(&___ns_index_stack_18, value);
	}

	inline static int32_t get_offset_of_xml_lang_stack_19() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090, ___xml_lang_stack_19)); }
	inline Stack_1_t2690840144 * get_xml_lang_stack_19() const { return ___xml_lang_stack_19; }
	inline Stack_1_t2690840144 ** get_address_of_xml_lang_stack_19() { return &___xml_lang_stack_19; }
	inline void set_xml_lang_stack_19(Stack_1_t2690840144 * value)
	{
		___xml_lang_stack_19 = value;
		Il2CppCodeGenWriteBarrier(&___xml_lang_stack_19, value);
	}

	inline static int32_t get_offset_of_xml_space_stack_20() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090, ___xml_space_stack_20)); }
	inline Stack_1_t4167582706 * get_xml_space_stack_20() const { return ___xml_space_stack_20; }
	inline Stack_1_t4167582706 ** get_address_of_xml_space_stack_20() { return &___xml_space_stack_20; }
	inline void set_xml_space_stack_20(Stack_1_t4167582706 * value)
	{
		___xml_space_stack_20 = value;
		Il2CppCodeGenWriteBarrier(&___xml_space_stack_20, value);
	}

	inline static int32_t get_offset_of_element_ns_stack_21() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090, ___element_ns_stack_21)); }
	inline Stack_1_t2690840144 * get_element_ns_stack_21() const { return ___element_ns_stack_21; }
	inline Stack_1_t2690840144 ** get_address_of_element_ns_stack_21() { return &___element_ns_stack_21; }
	inline void set_element_ns_stack_21(Stack_1_t2690840144 * value)
	{
		___element_ns_stack_21 = value;
		Il2CppCodeGenWriteBarrier(&___element_ns_stack_21, value);
	}

	inline static int32_t get_offset_of_element_ns_22() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090, ___element_ns_22)); }
	inline String_t* get_element_ns_22() const { return ___element_ns_22; }
	inline String_t** get_address_of_element_ns_22() { return &___element_ns_22; }
	inline void set_element_ns_22(String_t* value)
	{
		___element_ns_22 = value;
		Il2CppCodeGenWriteBarrier(&___element_ns_22, value);
	}

	inline static int32_t get_offset_of_element_count_23() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090, ___element_count_23)); }
	inline int32_t get_element_count_23() const { return ___element_count_23; }
	inline int32_t* get_address_of_element_count_23() { return &___element_count_23; }
	inline void set_element_count_23(int32_t value)
	{
		___element_count_23 = value;
	}

	inline static int32_t get_offset_of_element_prefix_24() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090, ___element_prefix_24)); }
	inline String_t* get_element_prefix_24() const { return ___element_prefix_24; }
	inline String_t** get_address_of_element_prefix_24() { return &___element_prefix_24; }
	inline void set_element_prefix_24(String_t* value)
	{
		___element_prefix_24 = value;
		Il2CppCodeGenWriteBarrier(&___element_prefix_24, value);
	}

	inline static int32_t get_offset_of_attr_value_25() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090, ___attr_value_25)); }
	inline String_t* get_attr_value_25() const { return ___attr_value_25; }
	inline String_t** get_address_of_attr_value_25() { return &___attr_value_25; }
	inline void set_attr_value_25(String_t* value)
	{
		___attr_value_25 = value;
		Il2CppCodeGenWriteBarrier(&___attr_value_25, value);
	}

	inline static int32_t get_offset_of_current_attr_prefix_26() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090, ___current_attr_prefix_26)); }
	inline String_t* get_current_attr_prefix_26() const { return ___current_attr_prefix_26; }
	inline String_t** get_address_of_current_attr_prefix_26() { return &___current_attr_prefix_26; }
	inline void set_current_attr_prefix_26(String_t* value)
	{
		___current_attr_prefix_26 = value;
		Il2CppCodeGenWriteBarrier(&___current_attr_prefix_26, value);
	}

	inline static int32_t get_offset_of_current_attr_name_27() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090, ___current_attr_name_27)); }
	inline Il2CppObject * get_current_attr_name_27() const { return ___current_attr_name_27; }
	inline Il2CppObject ** get_address_of_current_attr_name_27() { return &___current_attr_name_27; }
	inline void set_current_attr_name_27(Il2CppObject * value)
	{
		___current_attr_name_27 = value;
		Il2CppCodeGenWriteBarrier(&___current_attr_name_27, value);
	}

	inline static int32_t get_offset_of_current_attr_ns_28() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090, ___current_attr_ns_28)); }
	inline Il2CppObject * get_current_attr_ns_28() const { return ___current_attr_ns_28; }
	inline Il2CppObject ** get_address_of_current_attr_ns_28() { return &___current_attr_ns_28; }
	inline void set_current_attr_ns_28(Il2CppObject * value)
	{
		___current_attr_ns_28 = value;
		Il2CppCodeGenWriteBarrier(&___current_attr_ns_28, value);
	}

	inline static int32_t get_offset_of_attr_typed_value_29() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090, ___attr_typed_value_29)); }
	inline bool get_attr_typed_value_29() const { return ___attr_typed_value_29; }
	inline bool* get_address_of_attr_typed_value_29() { return &___attr_typed_value_29; }
	inline void set_attr_typed_value_29(bool value)
	{
		___attr_typed_value_29 = value;
	}

	inline static int32_t get_offset_of_save_target_30() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090, ___save_target_30)); }
	inline int32_t get_save_target_30() const { return ___save_target_30; }
	inline int32_t* get_address_of_save_target_30() { return &___save_target_30; }
	inline void set_save_target_30(int32_t value)
	{
		___save_target_30 = value;
	}
};

struct XmlBinaryDictionaryWriter_t676787090_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlBinaryDictionaryWriter::<>f__switch$map3
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map3_31;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlBinaryDictionaryWriter::<>f__switch$map4
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map4_32;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlBinaryDictionaryWriter::<>f__switch$map5
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map5_33;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlBinaryDictionaryWriter::<>f__switch$map6
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map6_34;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlBinaryDictionaryWriter::<>f__switch$map7
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map7_35;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map3_31() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090_StaticFields, ___U3CU3Ef__switchU24map3_31)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map3_31() const { return ___U3CU3Ef__switchU24map3_31; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map3_31() { return &___U3CU3Ef__switchU24map3_31; }
	inline void set_U3CU3Ef__switchU24map3_31(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map3_31 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map3_31, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4_32() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090_StaticFields, ___U3CU3Ef__switchU24map4_32)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map4_32() const { return ___U3CU3Ef__switchU24map4_32; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map4_32() { return &___U3CU3Ef__switchU24map4_32; }
	inline void set_U3CU3Ef__switchU24map4_32(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map4_32 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map4_32, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map5_33() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090_StaticFields, ___U3CU3Ef__switchU24map5_33)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map5_33() const { return ___U3CU3Ef__switchU24map5_33; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map5_33() { return &___U3CU3Ef__switchU24map5_33; }
	inline void set_U3CU3Ef__switchU24map5_33(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map5_33 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map5_33, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map6_34() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090_StaticFields, ___U3CU3Ef__switchU24map6_34)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map6_34() const { return ___U3CU3Ef__switchU24map6_34; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map6_34() { return &___U3CU3Ef__switchU24map6_34; }
	inline void set_U3CU3Ef__switchU24map6_34(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map6_34 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map6_34, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map7_35() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryWriter_t676787090_StaticFields, ___U3CU3Ef__switchU24map7_35)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map7_35() const { return ___U3CU3Ef__switchU24map7_35; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map7_35() { return &___U3CU3Ef__switchU24map7_35; }
	inline void set_U3CU3Ef__switchU24map7_35(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map7_35 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map7_35, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
