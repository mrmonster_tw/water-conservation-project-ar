﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_System_ServiceModel_BasicHttpM3060458099.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.BasicHttpMessageSecurity
struct  BasicHttpMessageSecurity_t1555777380  : public Il2CppObject
{
public:
	// System.ServiceModel.BasicHttpMessageCredentialType System.ServiceModel.BasicHttpMessageSecurity::ctype
	int32_t ___ctype_0;

public:
	inline static int32_t get_offset_of_ctype_0() { return static_cast<int32_t>(offsetof(BasicHttpMessageSecurity_t1555777380, ___ctype_0)); }
	inline int32_t get_ctype_0() const { return ___ctype_0; }
	inline int32_t* get_address_of_ctype_0() { return &___ctype_0; }
	inline void set_ctype_0(int32_t value)
	{
		___ctype_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
