﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"
#include "System_System_IO_InotifyEvent2637353984.h"
#include "System_System_IO_InotifyMask3323194736.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.InotifyEvent
struct  InotifyEvent_t2637353984 
{
public:
	// System.Int32 System.IO.InotifyEvent::WatchDescriptor
	int32_t ___WatchDescriptor_1;
	// System.IO.InotifyMask System.IO.InotifyEvent::Mask
	uint32_t ___Mask_2;
	// System.String System.IO.InotifyEvent::Name
	String_t* ___Name_3;

public:
	inline static int32_t get_offset_of_WatchDescriptor_1() { return static_cast<int32_t>(offsetof(InotifyEvent_t2637353984, ___WatchDescriptor_1)); }
	inline int32_t get_WatchDescriptor_1() const { return ___WatchDescriptor_1; }
	inline int32_t* get_address_of_WatchDescriptor_1() { return &___WatchDescriptor_1; }
	inline void set_WatchDescriptor_1(int32_t value)
	{
		___WatchDescriptor_1 = value;
	}

	inline static int32_t get_offset_of_Mask_2() { return static_cast<int32_t>(offsetof(InotifyEvent_t2637353984, ___Mask_2)); }
	inline uint32_t get_Mask_2() const { return ___Mask_2; }
	inline uint32_t* get_address_of_Mask_2() { return &___Mask_2; }
	inline void set_Mask_2(uint32_t value)
	{
		___Mask_2 = value;
	}

	inline static int32_t get_offset_of_Name_3() { return static_cast<int32_t>(offsetof(InotifyEvent_t2637353984, ___Name_3)); }
	inline String_t* get_Name_3() const { return ___Name_3; }
	inline String_t** get_address_of_Name_3() { return &___Name_3; }
	inline void set_Name_3(String_t* value)
	{
		___Name_3 = value;
		Il2CppCodeGenWriteBarrier(&___Name_3, value);
	}
};

struct InotifyEvent_t2637353984_StaticFields
{
public:
	// System.IO.InotifyEvent System.IO.InotifyEvent::Default
	InotifyEvent_t2637353984  ___Default_0;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(InotifyEvent_t2637353984_StaticFields, ___Default_0)); }
	inline InotifyEvent_t2637353984  get_Default_0() const { return ___Default_0; }
	inline InotifyEvent_t2637353984 * get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(InotifyEvent_t2637353984  value)
	{
		___Default_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.IO.InotifyEvent
struct InotifyEvent_t2637353984_marshaled_pinvoke
{
	int32_t ___WatchDescriptor_1;
	uint32_t ___Mask_2;
	char* ___Name_3;
};
// Native definition for COM marshalling of System.IO.InotifyEvent
struct InotifyEvent_t2637353984_marshaled_com
{
	int32_t ___WatchDescriptor_1;
	uint32_t ___Mask_2;
	Il2CppChar* ___Name_3;
};
