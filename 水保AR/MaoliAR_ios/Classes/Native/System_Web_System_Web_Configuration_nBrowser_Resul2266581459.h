﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_Configuration_CapabilitiesResul2339653.h"

// System.Collections.Generic.Dictionary`2<System.Type,System.Type>
struct Dictionary_2_t633324528;
// System.Collections.Specialized.StringCollection
struct StringCollection_t167406615;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.nBrowser.Result
struct  Result_t2266581459  : public CapabilitiesResult_t2339653
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.Type> System.Web.Configuration.nBrowser.Result::AdapterTypeMap
	Dictionary_2_t633324528 * ___AdapterTypeMap_6;
	// System.Collections.Specialized.StringCollection System.Web.Configuration.nBrowser.Result::Track
	StringCollection_t167406615 * ___Track_7;
	// System.Type System.Web.Configuration.nBrowser.Result::MarkupTextWriter
	Type_t * ___MarkupTextWriter_8;

public:
	inline static int32_t get_offset_of_AdapterTypeMap_6() { return static_cast<int32_t>(offsetof(Result_t2266581459, ___AdapterTypeMap_6)); }
	inline Dictionary_2_t633324528 * get_AdapterTypeMap_6() const { return ___AdapterTypeMap_6; }
	inline Dictionary_2_t633324528 ** get_address_of_AdapterTypeMap_6() { return &___AdapterTypeMap_6; }
	inline void set_AdapterTypeMap_6(Dictionary_2_t633324528 * value)
	{
		___AdapterTypeMap_6 = value;
		Il2CppCodeGenWriteBarrier(&___AdapterTypeMap_6, value);
	}

	inline static int32_t get_offset_of_Track_7() { return static_cast<int32_t>(offsetof(Result_t2266581459, ___Track_7)); }
	inline StringCollection_t167406615 * get_Track_7() const { return ___Track_7; }
	inline StringCollection_t167406615 ** get_address_of_Track_7() { return &___Track_7; }
	inline void set_Track_7(StringCollection_t167406615 * value)
	{
		___Track_7 = value;
		Il2CppCodeGenWriteBarrier(&___Track_7, value);
	}

	inline static int32_t get_offset_of_MarkupTextWriter_8() { return static_cast<int32_t>(offsetof(Result_t2266581459, ___MarkupTextWriter_8)); }
	inline Type_t * get_MarkupTextWriter_8() const { return ___MarkupTextWriter_8; }
	inline Type_t ** get_address_of_MarkupTextWriter_8() { return &___MarkupTextWriter_8; }
	inline void set_MarkupTextWriter_8(Type_t * value)
	{
		___MarkupTextWriter_8 = value;
		Il2CppCodeGenWriteBarrier(&___MarkupTextWriter_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
