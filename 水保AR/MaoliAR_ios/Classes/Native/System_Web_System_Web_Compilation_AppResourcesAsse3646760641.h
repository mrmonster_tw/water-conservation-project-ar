﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Threading.Mutex
struct Mutex_t3066672582;
// System.Collections.Specialized.StringCollection
struct StringCollection_t167406615;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.AppResourcesAssemblyBuilder/<BuildSatelliteAssembly>c__AnonStorey6
struct  U3CBuildSatelliteAssemblyU3Ec__AnonStorey6_t3646760641  : public Il2CppObject
{
public:
	// System.Threading.Mutex System.Web.Compilation.AppResourcesAssemblyBuilder/<BuildSatelliteAssembly>c__AnonStorey6::alMutex
	Mutex_t3066672582 * ___alMutex_0;
	// System.Collections.Specialized.StringCollection System.Web.Compilation.AppResourcesAssemblyBuilder/<BuildSatelliteAssembly>c__AnonStorey6::alOutput
	StringCollection_t167406615 * ___alOutput_1;

public:
	inline static int32_t get_offset_of_alMutex_0() { return static_cast<int32_t>(offsetof(U3CBuildSatelliteAssemblyU3Ec__AnonStorey6_t3646760641, ___alMutex_0)); }
	inline Mutex_t3066672582 * get_alMutex_0() const { return ___alMutex_0; }
	inline Mutex_t3066672582 ** get_address_of_alMutex_0() { return &___alMutex_0; }
	inline void set_alMutex_0(Mutex_t3066672582 * value)
	{
		___alMutex_0 = value;
		Il2CppCodeGenWriteBarrier(&___alMutex_0, value);
	}

	inline static int32_t get_offset_of_alOutput_1() { return static_cast<int32_t>(offsetof(U3CBuildSatelliteAssemblyU3Ec__AnonStorey6_t3646760641, ___alOutput_1)); }
	inline StringCollection_t167406615 * get_alOutput_1() const { return ___alOutput_1; }
	inline StringCollection_t167406615 ** get_address_of_alOutput_1() { return &___alOutput_1; }
	inline void set_alOutput_1(StringCollection_t167406615 * value)
	{
		___alOutput_1 = value;
		Il2CppCodeGenWriteBarrier(&___alOutput_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
