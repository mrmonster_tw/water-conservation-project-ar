﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_ScriptCompilerInfo3765441597.h"

// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.JScriptCompilerInfo
struct  JScriptCompilerInfo_t2884625701  : public ScriptCompilerInfo_t3765441597
{
public:

public:
};

struct JScriptCompilerInfo_t2884625701_StaticFields
{
public:
	// System.Type Mono.Xml.Xsl.JScriptCompilerInfo::providerType
	Type_t * ___providerType_2;

public:
	inline static int32_t get_offset_of_providerType_2() { return static_cast<int32_t>(offsetof(JScriptCompilerInfo_t2884625701_StaticFields, ___providerType_2)); }
	inline Type_t * get_providerType_2() const { return ___providerType_2; }
	inline Type_t ** get_address_of_providerType_2() { return &___providerType_2; }
	inline void set_providerType_2(Type_t * value)
	{
		___providerType_2 = value;
		Il2CppCodeGenWriteBarrier(&___providerType_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
