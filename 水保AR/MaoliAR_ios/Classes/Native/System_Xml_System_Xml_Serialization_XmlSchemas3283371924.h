﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Collections_CollectionBase2727926298.h"

// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t1853889766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSchemas
struct  XmlSchemas_t3283371924  : public CollectionBase_t2727926298
{
public:
	// System.Collections.Hashtable System.Xml.Serialization.XmlSchemas::table
	Hashtable_t1853889766 * ___table_2;

public:
	inline static int32_t get_offset_of_table_2() { return static_cast<int32_t>(offsetof(XmlSchemas_t3283371924, ___table_2)); }
	inline Hashtable_t1853889766 * get_table_2() const { return ___table_2; }
	inline Hashtable_t1853889766 ** get_address_of_table_2() { return &___table_2; }
	inline void set_table_2(Hashtable_t1853889766 * value)
	{
		___table_2 = value;
		Il2CppCodeGenWriteBarrier(&___table_2, value);
	}
};

struct XmlSchemas_t3283371924_StaticFields
{
public:
	// System.String System.Xml.Serialization.XmlSchemas::msdataNS
	String_t* ___msdataNS_1;

public:
	inline static int32_t get_offset_of_msdataNS_1() { return static_cast<int32_t>(offsetof(XmlSchemas_t3283371924_StaticFields, ___msdataNS_1)); }
	inline String_t* get_msdataNS_1() const { return ___msdataNS_1; }
	inline String_t** get_address_of_msdataNS_1() { return &___msdataNS_1; }
	inline void set_msdataNS_1(String_t* value)
	{
		___msdataNS_1 = value;
		Il2CppCodeGenWriteBarrier(&___msdataNS_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
