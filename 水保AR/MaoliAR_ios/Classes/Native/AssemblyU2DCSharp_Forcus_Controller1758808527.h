﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.AnimationClip
struct AnimationClip_t2318505987;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Forcus_Controller
struct  Forcus_Controller_t1758808527  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Animator Forcus_Controller::animator
	Animator_t434523843 * ___animator_2;
	// UnityEngine.AnimationClip Forcus_Controller::zoom_in
	AnimationClip_t2318505987 * ___zoom_in_3;
	// UnityEngine.AnimationClip Forcus_Controller::zoom_out
	AnimationClip_t2318505987 * ___zoom_out_4;

public:
	inline static int32_t get_offset_of_animator_2() { return static_cast<int32_t>(offsetof(Forcus_Controller_t1758808527, ___animator_2)); }
	inline Animator_t434523843 * get_animator_2() const { return ___animator_2; }
	inline Animator_t434523843 ** get_address_of_animator_2() { return &___animator_2; }
	inline void set_animator_2(Animator_t434523843 * value)
	{
		___animator_2 = value;
		Il2CppCodeGenWriteBarrier(&___animator_2, value);
	}

	inline static int32_t get_offset_of_zoom_in_3() { return static_cast<int32_t>(offsetof(Forcus_Controller_t1758808527, ___zoom_in_3)); }
	inline AnimationClip_t2318505987 * get_zoom_in_3() const { return ___zoom_in_3; }
	inline AnimationClip_t2318505987 ** get_address_of_zoom_in_3() { return &___zoom_in_3; }
	inline void set_zoom_in_3(AnimationClip_t2318505987 * value)
	{
		___zoom_in_3 = value;
		Il2CppCodeGenWriteBarrier(&___zoom_in_3, value);
	}

	inline static int32_t get_offset_of_zoom_out_4() { return static_cast<int32_t>(offsetof(Forcus_Controller_t1758808527, ___zoom_out_4)); }
	inline AnimationClip_t2318505987 * get_zoom_out_4() const { return ___zoom_out_4; }
	inline AnimationClip_t2318505987 ** get_address_of_zoom_out_4() { return &___zoom_out_4; }
	inline void set_zoom_out_4(AnimationClip_t2318505987 * value)
	{
		___zoom_out_4 = value;
		Il2CppCodeGenWriteBarrier(&___zoom_out_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
