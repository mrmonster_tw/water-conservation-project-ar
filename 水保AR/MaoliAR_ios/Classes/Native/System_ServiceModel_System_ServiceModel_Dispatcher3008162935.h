﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_Collections_Generic_Syn2160944627.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.DispatchOperation/DispatchOperationCollection
struct  DispatchOperationCollection_t3008162935  : public SynchronizedKeyedCollection_2_t2160944627
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
