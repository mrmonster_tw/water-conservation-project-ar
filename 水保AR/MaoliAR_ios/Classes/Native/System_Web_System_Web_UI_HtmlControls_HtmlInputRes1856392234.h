﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_HtmlControls_HtmlInputButt695971026.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.HtmlControls.HtmlInputReset
struct  HtmlInputReset_t1856392234  : public HtmlInputButton_t695971026
{
public:

public:
};

struct HtmlInputReset_t1856392234_StaticFields
{
public:
	// System.Object System.Web.UI.HtmlControls.HtmlInputReset::ServerClickEvent
	Il2CppObject * ___ServerClickEvent_31;

public:
	inline static int32_t get_offset_of_ServerClickEvent_31() { return static_cast<int32_t>(offsetof(HtmlInputReset_t1856392234_StaticFields, ___ServerClickEvent_31)); }
	inline Il2CppObject * get_ServerClickEvent_31() const { return ___ServerClickEvent_31; }
	inline Il2CppObject ** get_address_of_ServerClickEvent_31() { return &___ServerClickEvent_31; }
	inline void set_ServerClickEvent_31(Il2CppObject * value)
	{
		___ServerClickEvent_31 = value;
		Il2CppCodeGenWriteBarrier(&___ServerClickEvent_31, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
