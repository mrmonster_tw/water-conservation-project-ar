﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t143221404;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Play_Level_Music
struct  Play_Level_Music_t2585779503  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioClip[] Play_Level_Music::audio
	AudioClipU5BU5D_t143221404* ___audio_2;

public:
	inline static int32_t get_offset_of_audio_2() { return static_cast<int32_t>(offsetof(Play_Level_Music_t2585779503, ___audio_2)); }
	inline AudioClipU5BU5D_t143221404* get_audio_2() const { return ___audio_2; }
	inline AudioClipU5BU5D_t143221404** get_address_of_audio_2() { return &___audio_2; }
	inline void set_audio_2(AudioClipU5BU5D_t143221404* value)
	{
		___audio_2 = value;
		Il2CppCodeGenWriteBarrier(&___audio_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
