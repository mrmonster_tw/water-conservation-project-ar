﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// System.String
struct String_t;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t4131667876;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BtConnection
struct  BtConnection_t2892495216  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct BtConnection_t2892495216_StaticFields
{
public:
	// UnityEngine.AndroidJavaObject BtConnection::ajc
	AndroidJavaObject_t4131667876 * ___ajc_12;
	// System.Boolean BtConnection::PluginReady
	bool ___PluginReady_13;
	// System.Int32 BtConnection::modeNumber
	int32_t ___modeNumber_15;

public:
	inline static int32_t get_offset_of_ajc_12() { return static_cast<int32_t>(offsetof(BtConnection_t2892495216_StaticFields, ___ajc_12)); }
	inline AndroidJavaObject_t4131667876 * get_ajc_12() const { return ___ajc_12; }
	inline AndroidJavaObject_t4131667876 ** get_address_of_ajc_12() { return &___ajc_12; }
	inline void set_ajc_12(AndroidJavaObject_t4131667876 * value)
	{
		___ajc_12 = value;
		Il2CppCodeGenWriteBarrier(&___ajc_12, value);
	}

	inline static int32_t get_offset_of_PluginReady_13() { return static_cast<int32_t>(offsetof(BtConnection_t2892495216_StaticFields, ___PluginReady_13)); }
	inline bool get_PluginReady_13() const { return ___PluginReady_13; }
	inline bool* get_address_of_PluginReady_13() { return &___PluginReady_13; }
	inline void set_PluginReady_13(bool value)
	{
		___PluginReady_13 = value;
	}

	inline static int32_t get_offset_of_modeNumber_15() { return static_cast<int32_t>(offsetof(BtConnection_t2892495216_StaticFields, ___modeNumber_15)); }
	inline int32_t get_modeNumber_15() const { return ___modeNumber_15; }
	inline int32_t* get_address_of_modeNumber_15() { return &___modeNumber_15; }
	inline void set_modeNumber_15(int32_t value)
	{
		___modeNumber_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
