﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_LayerMask3493934918.h"

// UnityEngine.Camera
struct Camera_t4157153871;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraTargeting
struct  CameraTargeting_t675235903  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.LayerMask CameraTargeting::targetingLayerMask
	LayerMask_t3493934918  ___targetingLayerMask_2;
	// System.Single CameraTargeting::targetingRayLength
	float ___targetingRayLength_3;
	// UnityEngine.Camera CameraTargeting::cam
	Camera_t4157153871 * ___cam_4;
	// System.String CameraTargeting::info
	String_t* ___info_5;

public:
	inline static int32_t get_offset_of_targetingLayerMask_2() { return static_cast<int32_t>(offsetof(CameraTargeting_t675235903, ___targetingLayerMask_2)); }
	inline LayerMask_t3493934918  get_targetingLayerMask_2() const { return ___targetingLayerMask_2; }
	inline LayerMask_t3493934918 * get_address_of_targetingLayerMask_2() { return &___targetingLayerMask_2; }
	inline void set_targetingLayerMask_2(LayerMask_t3493934918  value)
	{
		___targetingLayerMask_2 = value;
	}

	inline static int32_t get_offset_of_targetingRayLength_3() { return static_cast<int32_t>(offsetof(CameraTargeting_t675235903, ___targetingRayLength_3)); }
	inline float get_targetingRayLength_3() const { return ___targetingRayLength_3; }
	inline float* get_address_of_targetingRayLength_3() { return &___targetingRayLength_3; }
	inline void set_targetingRayLength_3(float value)
	{
		___targetingRayLength_3 = value;
	}

	inline static int32_t get_offset_of_cam_4() { return static_cast<int32_t>(offsetof(CameraTargeting_t675235903, ___cam_4)); }
	inline Camera_t4157153871 * get_cam_4() const { return ___cam_4; }
	inline Camera_t4157153871 ** get_address_of_cam_4() { return &___cam_4; }
	inline void set_cam_4(Camera_t4157153871 * value)
	{
		___cam_4 = value;
		Il2CppCodeGenWriteBarrier(&___cam_4, value);
	}

	inline static int32_t get_offset_of_info_5() { return static_cast<int32_t>(offsetof(CameraTargeting_t675235903, ___info_5)); }
	inline String_t* get_info_5() const { return ___info_5; }
	inline String_t** get_address_of_info_5() { return &___info_5; }
	inline void set_info_5(String_t* value)
	{
		___info_5 = value;
		Il2CppCodeGenWriteBarrier(&___info_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
