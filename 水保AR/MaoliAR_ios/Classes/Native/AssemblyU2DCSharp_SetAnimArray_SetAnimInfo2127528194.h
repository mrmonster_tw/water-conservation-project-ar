﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"
#include "UnityEngine_UnityEngine_AnimatorControllerParamete3317225440.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SetAnimArray/SetAnimInfo
struct  SetAnimInfo_t2127528194 
{
public:
	// UnityEngine.AnimatorControllerParameterType SetAnimArray/SetAnimInfo::type
	int32_t ___type_0;
	// System.Single SetAnimArray/SetAnimInfo::latetime
	float ___latetime_1;
	// System.String SetAnimArray/SetAnimInfo::ParameterName
	String_t* ___ParameterName_2;
	// System.Boolean SetAnimArray/SetAnimInfo::use判斷
	bool ___useU5224U65B7_3;
	// System.Boolean SetAnimArray/SetAnimInfo::bool參數
	bool ___boolU53C3U6578_4;
	// System.Single SetAnimArray/SetAnimInfo::Float參數
	float ___FloatU53C3U6578_5;
	// System.Int32 SetAnimArray/SetAnimInfo::Int參數
	int32_t ___IntU53C3U6578_6;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(SetAnimInfo_t2127528194, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_latetime_1() { return static_cast<int32_t>(offsetof(SetAnimInfo_t2127528194, ___latetime_1)); }
	inline float get_latetime_1() const { return ___latetime_1; }
	inline float* get_address_of_latetime_1() { return &___latetime_1; }
	inline void set_latetime_1(float value)
	{
		___latetime_1 = value;
	}

	inline static int32_t get_offset_of_ParameterName_2() { return static_cast<int32_t>(offsetof(SetAnimInfo_t2127528194, ___ParameterName_2)); }
	inline String_t* get_ParameterName_2() const { return ___ParameterName_2; }
	inline String_t** get_address_of_ParameterName_2() { return &___ParameterName_2; }
	inline void set_ParameterName_2(String_t* value)
	{
		___ParameterName_2 = value;
		Il2CppCodeGenWriteBarrier(&___ParameterName_2, value);
	}

	inline static int32_t get_offset_of_useU5224U65B7_3() { return static_cast<int32_t>(offsetof(SetAnimInfo_t2127528194, ___useU5224U65B7_3)); }
	inline bool get_useU5224U65B7_3() const { return ___useU5224U65B7_3; }
	inline bool* get_address_of_useU5224U65B7_3() { return &___useU5224U65B7_3; }
	inline void set_useU5224U65B7_3(bool value)
	{
		___useU5224U65B7_3 = value;
	}

	inline static int32_t get_offset_of_boolU53C3U6578_4() { return static_cast<int32_t>(offsetof(SetAnimInfo_t2127528194, ___boolU53C3U6578_4)); }
	inline bool get_boolU53C3U6578_4() const { return ___boolU53C3U6578_4; }
	inline bool* get_address_of_boolU53C3U6578_4() { return &___boolU53C3U6578_4; }
	inline void set_boolU53C3U6578_4(bool value)
	{
		___boolU53C3U6578_4 = value;
	}

	inline static int32_t get_offset_of_FloatU53C3U6578_5() { return static_cast<int32_t>(offsetof(SetAnimInfo_t2127528194, ___FloatU53C3U6578_5)); }
	inline float get_FloatU53C3U6578_5() const { return ___FloatU53C3U6578_5; }
	inline float* get_address_of_FloatU53C3U6578_5() { return &___FloatU53C3U6578_5; }
	inline void set_FloatU53C3U6578_5(float value)
	{
		___FloatU53C3U6578_5 = value;
	}

	inline static int32_t get_offset_of_IntU53C3U6578_6() { return static_cast<int32_t>(offsetof(SetAnimInfo_t2127528194, ___IntU53C3U6578_6)); }
	inline int32_t get_IntU53C3U6578_6() const { return ___IntU53C3U6578_6; }
	inline int32_t* get_address_of_IntU53C3U6578_6() { return &___IntU53C3U6578_6; }
	inline void set_IntU53C3U6578_6(int32_t value)
	{
		___IntU53C3U6578_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of SetAnimArray/SetAnimInfo
struct SetAnimInfo_t2127528194_marshaled_pinvoke
{
	int32_t ___type_0;
	float ___latetime_1;
	char* ___ParameterName_2;
	int32_t ___useU5224U65B7_3;
	int32_t ___boolU53C3U6578_4;
	float ___FloatU53C3U6578_5;
	int32_t ___IntU53C3U6578_6;
};
// Native definition for COM marshalling of SetAnimArray/SetAnimInfo
struct SetAnimInfo_t2127528194_marshaled_com
{
	int32_t ___type_0;
	float ___latetime_1;
	Il2CppChar* ___ParameterName_2;
	int32_t ___useU5224U65B7_3;
	int32_t ___boolU53C3U6578_4;
	float ___FloatU53C3U6578_5;
	int32_t ___IntU53C3U6578_6;
};
