﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.Web.Services.Description.ServiceDescription
struct ServiceDescription_t3693704363;
// System.Web.Services.Description.WebReference
struct WebReference_t2299867357;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.ImportInfo
struct  ImportInfo_t202809424  : public Il2CppObject
{
public:
	// System.String System.Web.Services.Description.ImportInfo::_appSettingUrlKey
	String_t* ____appSettingUrlKey_0;
	// System.String System.Web.Services.Description.ImportInfo::_appSettingBaseUrl
	String_t* ____appSettingBaseUrl_1;
	// System.Web.Services.Description.ServiceDescription System.Web.Services.Description.ImportInfo::_serviceDescription
	ServiceDescription_t3693704363 * ____serviceDescription_2;
	// System.Web.Services.Description.WebReference System.Web.Services.Description.ImportInfo::_reference
	WebReference_t2299867357 * ____reference_3;

public:
	inline static int32_t get_offset_of__appSettingUrlKey_0() { return static_cast<int32_t>(offsetof(ImportInfo_t202809424, ____appSettingUrlKey_0)); }
	inline String_t* get__appSettingUrlKey_0() const { return ____appSettingUrlKey_0; }
	inline String_t** get_address_of__appSettingUrlKey_0() { return &____appSettingUrlKey_0; }
	inline void set__appSettingUrlKey_0(String_t* value)
	{
		____appSettingUrlKey_0 = value;
		Il2CppCodeGenWriteBarrier(&____appSettingUrlKey_0, value);
	}

	inline static int32_t get_offset_of__appSettingBaseUrl_1() { return static_cast<int32_t>(offsetof(ImportInfo_t202809424, ____appSettingBaseUrl_1)); }
	inline String_t* get__appSettingBaseUrl_1() const { return ____appSettingBaseUrl_1; }
	inline String_t** get_address_of__appSettingBaseUrl_1() { return &____appSettingBaseUrl_1; }
	inline void set__appSettingBaseUrl_1(String_t* value)
	{
		____appSettingBaseUrl_1 = value;
		Il2CppCodeGenWriteBarrier(&____appSettingBaseUrl_1, value);
	}

	inline static int32_t get_offset_of__serviceDescription_2() { return static_cast<int32_t>(offsetof(ImportInfo_t202809424, ____serviceDescription_2)); }
	inline ServiceDescription_t3693704363 * get__serviceDescription_2() const { return ____serviceDescription_2; }
	inline ServiceDescription_t3693704363 ** get_address_of__serviceDescription_2() { return &____serviceDescription_2; }
	inline void set__serviceDescription_2(ServiceDescription_t3693704363 * value)
	{
		____serviceDescription_2 = value;
		Il2CppCodeGenWriteBarrier(&____serviceDescription_2, value);
	}

	inline static int32_t get_offset_of__reference_3() { return static_cast<int32_t>(offsetof(ImportInfo_t202809424, ____reference_3)); }
	inline WebReference_t2299867357 * get__reference_3() const { return ____reference_3; }
	inline WebReference_t2299867357 ** get_address_of__reference_3() { return &____reference_3; }
	inline void set__reference_3(WebReference_t2299867357 * value)
	{
		____reference_3 = value;
		Il2CppCodeGenWriteBarrier(&____reference_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
