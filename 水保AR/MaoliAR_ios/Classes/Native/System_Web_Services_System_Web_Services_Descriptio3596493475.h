﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_Services_System_Web_Services_Descriptio3764124827.h"

// System.Web.Services.Description.ServiceDescriptionFormatExtensionCollection
struct ServiceDescriptionFormatExtensionCollection_t2630153888;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.FaultBinding
struct  FaultBinding_t3596493475  : public MessageBinding_t3764124827
{
public:
	// System.Web.Services.Description.ServiceDescriptionFormatExtensionCollection System.Web.Services.Description.FaultBinding::extensions
	ServiceDescriptionFormatExtensionCollection_t2630153888 * ___extensions_5;

public:
	inline static int32_t get_offset_of_extensions_5() { return static_cast<int32_t>(offsetof(FaultBinding_t3596493475, ___extensions_5)); }
	inline ServiceDescriptionFormatExtensionCollection_t2630153888 * get_extensions_5() const { return ___extensions_5; }
	inline ServiceDescriptionFormatExtensionCollection_t2630153888 ** get_address_of_extensions_5() { return &___extensions_5; }
	inline void set_extensions_5(ServiceDescriptionFormatExtensionCollection_t2630153888 * value)
	{
		___extensions_5 = value;
		Il2CppCodeGenWriteBarrier(&___extensions_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
