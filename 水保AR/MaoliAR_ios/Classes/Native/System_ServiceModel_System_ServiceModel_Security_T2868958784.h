﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_System_ServiceModel_Security_T4097770718.h"
#include "System_ServiceModel_System_ServiceModel_Security_T3428665768.h"

// System.ServiceModel.Channels.BindingContext
struct BindingContext_t2842489830;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.SecurityTokenParameters
struct  SecurityTokenParameters_t2868958784  : public Il2CppObject
{
public:
	// System.ServiceModel.Security.Tokens.SecurityTokenInclusionMode System.ServiceModel.Security.Tokens.SecurityTokenParameters::inclusion_mode
	int32_t ___inclusion_mode_0;
	// System.ServiceModel.Security.Tokens.SecurityTokenReferenceStyle System.ServiceModel.Security.Tokens.SecurityTokenParameters::reference_style
	int32_t ___reference_style_1;
	// System.Boolean System.ServiceModel.Security.Tokens.SecurityTokenParameters::require_derived_keys
	bool ___require_derived_keys_2;
	// System.ServiceModel.Channels.BindingContext System.ServiceModel.Security.Tokens.SecurityTokenParameters::issuer_binding_context
	BindingContext_t2842489830 * ___issuer_binding_context_3;

public:
	inline static int32_t get_offset_of_inclusion_mode_0() { return static_cast<int32_t>(offsetof(SecurityTokenParameters_t2868958784, ___inclusion_mode_0)); }
	inline int32_t get_inclusion_mode_0() const { return ___inclusion_mode_0; }
	inline int32_t* get_address_of_inclusion_mode_0() { return &___inclusion_mode_0; }
	inline void set_inclusion_mode_0(int32_t value)
	{
		___inclusion_mode_0 = value;
	}

	inline static int32_t get_offset_of_reference_style_1() { return static_cast<int32_t>(offsetof(SecurityTokenParameters_t2868958784, ___reference_style_1)); }
	inline int32_t get_reference_style_1() const { return ___reference_style_1; }
	inline int32_t* get_address_of_reference_style_1() { return &___reference_style_1; }
	inline void set_reference_style_1(int32_t value)
	{
		___reference_style_1 = value;
	}

	inline static int32_t get_offset_of_require_derived_keys_2() { return static_cast<int32_t>(offsetof(SecurityTokenParameters_t2868958784, ___require_derived_keys_2)); }
	inline bool get_require_derived_keys_2() const { return ___require_derived_keys_2; }
	inline bool* get_address_of_require_derived_keys_2() { return &___require_derived_keys_2; }
	inline void set_require_derived_keys_2(bool value)
	{
		___require_derived_keys_2 = value;
	}

	inline static int32_t get_offset_of_issuer_binding_context_3() { return static_cast<int32_t>(offsetof(SecurityTokenParameters_t2868958784, ___issuer_binding_context_3)); }
	inline BindingContext_t2842489830 * get_issuer_binding_context_3() const { return ___issuer_binding_context_3; }
	inline BindingContext_t2842489830 ** get_address_of_issuer_binding_context_3() { return &___issuer_binding_context_3; }
	inline void set_issuer_binding_context_3(BindingContext_t2842489830 * value)
	{
		___issuer_binding_context_3 = value;
		Il2CppCodeGenWriteBarrier(&___issuer_binding_context_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
