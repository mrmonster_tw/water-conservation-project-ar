﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_M3531153504.h"

// System.Xml.XmlDictionaryReaderQuotas
struct XmlDictionaryReaderQuotas_t173030297;
// System.ServiceModel.Channels.MessageVersion
struct MessageVersion_t1958933598;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.BinaryMessageEncodingBindingElement
struct  BinaryMessageEncodingBindingElement_t4154157131  : public MessageEncodingBindingElement_t3531153504
{
public:
	// System.Int32 System.ServiceModel.Channels.BinaryMessageEncodingBindingElement::max_read_pool_size
	int32_t ___max_read_pool_size_0;
	// System.Int32 System.ServiceModel.Channels.BinaryMessageEncodingBindingElement::max_write_pool_size
	int32_t ___max_write_pool_size_1;
	// System.Xml.XmlDictionaryReaderQuotas System.ServiceModel.Channels.BinaryMessageEncodingBindingElement::quotas
	XmlDictionaryReaderQuotas_t173030297 * ___quotas_2;
	// System.ServiceModel.Channels.MessageVersion System.ServiceModel.Channels.BinaryMessageEncodingBindingElement::version
	MessageVersion_t1958933598 * ___version_3;

public:
	inline static int32_t get_offset_of_max_read_pool_size_0() { return static_cast<int32_t>(offsetof(BinaryMessageEncodingBindingElement_t4154157131, ___max_read_pool_size_0)); }
	inline int32_t get_max_read_pool_size_0() const { return ___max_read_pool_size_0; }
	inline int32_t* get_address_of_max_read_pool_size_0() { return &___max_read_pool_size_0; }
	inline void set_max_read_pool_size_0(int32_t value)
	{
		___max_read_pool_size_0 = value;
	}

	inline static int32_t get_offset_of_max_write_pool_size_1() { return static_cast<int32_t>(offsetof(BinaryMessageEncodingBindingElement_t4154157131, ___max_write_pool_size_1)); }
	inline int32_t get_max_write_pool_size_1() const { return ___max_write_pool_size_1; }
	inline int32_t* get_address_of_max_write_pool_size_1() { return &___max_write_pool_size_1; }
	inline void set_max_write_pool_size_1(int32_t value)
	{
		___max_write_pool_size_1 = value;
	}

	inline static int32_t get_offset_of_quotas_2() { return static_cast<int32_t>(offsetof(BinaryMessageEncodingBindingElement_t4154157131, ___quotas_2)); }
	inline XmlDictionaryReaderQuotas_t173030297 * get_quotas_2() const { return ___quotas_2; }
	inline XmlDictionaryReaderQuotas_t173030297 ** get_address_of_quotas_2() { return &___quotas_2; }
	inline void set_quotas_2(XmlDictionaryReaderQuotas_t173030297 * value)
	{
		___quotas_2 = value;
		Il2CppCodeGenWriteBarrier(&___quotas_2, value);
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(BinaryMessageEncodingBindingElement_t4154157131, ___version_3)); }
	inline MessageVersion_t1958933598 * get_version_3() const { return ___version_3; }
	inline MessageVersion_t1958933598 ** get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(MessageVersion_t1958933598 * value)
	{
		___version_3 = value;
		Il2CppCodeGenWriteBarrier(&___version_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
