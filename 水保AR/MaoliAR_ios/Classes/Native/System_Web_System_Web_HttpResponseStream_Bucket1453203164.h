﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.HttpResponseStream/Bucket
struct Bucket_t1453203164;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpResponseStream/Bucket
struct  Bucket_t1453203164  : public Il2CppObject
{
public:
	// System.Web.HttpResponseStream/Bucket System.Web.HttpResponseStream/Bucket::Next
	Bucket_t1453203164 * ___Next_0;

public:
	inline static int32_t get_offset_of_Next_0() { return static_cast<int32_t>(offsetof(Bucket_t1453203164, ___Next_0)); }
	inline Bucket_t1453203164 * get_Next_0() const { return ___Next_0; }
	inline Bucket_t1453203164 ** get_address_of_Next_0() { return &___Next_0; }
	inline void set_Next_0(Bucket_t1453203164 * value)
	{
		___Next_0 = value;
		Il2CppCodeGenWriteBarrier(&___Next_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
