﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Security_T3159509539.h"

// System.ServiceModel.Security.Tokens.SslCommunicationObject
struct SslCommunicationObject_t3237646940;
// System.ServiceModel.ClientCredentialsSecurityTokenManager
struct ClientCredentialsSecurityTokenManager_t1905807029;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.SslSecurityTokenProvider
struct  SslSecurityTokenProvider_t2013392034  : public CommunicationSecurityTokenProvider_t3159509539
{
public:
	// System.ServiceModel.Security.Tokens.SslCommunicationObject System.ServiceModel.Security.Tokens.SslSecurityTokenProvider::comm
	SslCommunicationObject_t3237646940 * ___comm_0;
	// System.ServiceModel.ClientCredentialsSecurityTokenManager System.ServiceModel.Security.Tokens.SslSecurityTokenProvider::manager
	ClientCredentialsSecurityTokenManager_t1905807029 * ___manager_1;

public:
	inline static int32_t get_offset_of_comm_0() { return static_cast<int32_t>(offsetof(SslSecurityTokenProvider_t2013392034, ___comm_0)); }
	inline SslCommunicationObject_t3237646940 * get_comm_0() const { return ___comm_0; }
	inline SslCommunicationObject_t3237646940 ** get_address_of_comm_0() { return &___comm_0; }
	inline void set_comm_0(SslCommunicationObject_t3237646940 * value)
	{
		___comm_0 = value;
		Il2CppCodeGenWriteBarrier(&___comm_0, value);
	}

	inline static int32_t get_offset_of_manager_1() { return static_cast<int32_t>(offsetof(SslSecurityTokenProvider_t2013392034, ___manager_1)); }
	inline ClientCredentialsSecurityTokenManager_t1905807029 * get_manager_1() const { return ___manager_1; }
	inline ClientCredentialsSecurityTokenManager_t1905807029 ** get_address_of_manager_1() { return &___manager_1; }
	inline void set_manager_1(ClientCredentialsSecurityTokenManager_t1905807029 * value)
	{
		___manager_1 = value;
		Il2CppCodeGenWriteBarrier(&___manager_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
