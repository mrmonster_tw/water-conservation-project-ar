﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Operations_XslCompiledElemen50593777.h"

// Mono.Xml.Xsl.Operations.XslAvt
struct XslAvt_t1645109359;
// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// Mono.Xml.Xsl.Operations.XslOperation
struct XslOperation_t2153241355;
// System.Xml.XmlQualifiedName[]
struct XmlQualifiedNameU5BU5D_t1471530361;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslElement
struct  XslElement_t1808680497  : public XslCompiledElement_t50593777
{
public:
	// Mono.Xml.Xsl.Operations.XslAvt Mono.Xml.Xsl.Operations.XslElement::name
	XslAvt_t1645109359 * ___name_3;
	// Mono.Xml.Xsl.Operations.XslAvt Mono.Xml.Xsl.Operations.XslElement::ns
	XslAvt_t1645109359 * ___ns_4;
	// System.String Mono.Xml.Xsl.Operations.XslElement::calcName
	String_t* ___calcName_5;
	// System.String Mono.Xml.Xsl.Operations.XslElement::calcNs
	String_t* ___calcNs_6;
	// System.String Mono.Xml.Xsl.Operations.XslElement::calcPrefix
	String_t* ___calcPrefix_7;
	// System.Collections.Hashtable Mono.Xml.Xsl.Operations.XslElement::nsDecls
	Hashtable_t1853889766 * ___nsDecls_8;
	// System.Boolean Mono.Xml.Xsl.Operations.XslElement::isEmptyElement
	bool ___isEmptyElement_9;
	// Mono.Xml.Xsl.Operations.XslOperation Mono.Xml.Xsl.Operations.XslElement::value
	XslOperation_t2153241355 * ___value_10;
	// System.Xml.XmlQualifiedName[] Mono.Xml.Xsl.Operations.XslElement::useAttributeSets
	XmlQualifiedNameU5BU5D_t1471530361* ___useAttributeSets_11;

public:
	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(XslElement_t1808680497, ___name_3)); }
	inline XslAvt_t1645109359 * get_name_3() const { return ___name_3; }
	inline XslAvt_t1645109359 ** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(XslAvt_t1645109359 * value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier(&___name_3, value);
	}

	inline static int32_t get_offset_of_ns_4() { return static_cast<int32_t>(offsetof(XslElement_t1808680497, ___ns_4)); }
	inline XslAvt_t1645109359 * get_ns_4() const { return ___ns_4; }
	inline XslAvt_t1645109359 ** get_address_of_ns_4() { return &___ns_4; }
	inline void set_ns_4(XslAvt_t1645109359 * value)
	{
		___ns_4 = value;
		Il2CppCodeGenWriteBarrier(&___ns_4, value);
	}

	inline static int32_t get_offset_of_calcName_5() { return static_cast<int32_t>(offsetof(XslElement_t1808680497, ___calcName_5)); }
	inline String_t* get_calcName_5() const { return ___calcName_5; }
	inline String_t** get_address_of_calcName_5() { return &___calcName_5; }
	inline void set_calcName_5(String_t* value)
	{
		___calcName_5 = value;
		Il2CppCodeGenWriteBarrier(&___calcName_5, value);
	}

	inline static int32_t get_offset_of_calcNs_6() { return static_cast<int32_t>(offsetof(XslElement_t1808680497, ___calcNs_6)); }
	inline String_t* get_calcNs_6() const { return ___calcNs_6; }
	inline String_t** get_address_of_calcNs_6() { return &___calcNs_6; }
	inline void set_calcNs_6(String_t* value)
	{
		___calcNs_6 = value;
		Il2CppCodeGenWriteBarrier(&___calcNs_6, value);
	}

	inline static int32_t get_offset_of_calcPrefix_7() { return static_cast<int32_t>(offsetof(XslElement_t1808680497, ___calcPrefix_7)); }
	inline String_t* get_calcPrefix_7() const { return ___calcPrefix_7; }
	inline String_t** get_address_of_calcPrefix_7() { return &___calcPrefix_7; }
	inline void set_calcPrefix_7(String_t* value)
	{
		___calcPrefix_7 = value;
		Il2CppCodeGenWriteBarrier(&___calcPrefix_7, value);
	}

	inline static int32_t get_offset_of_nsDecls_8() { return static_cast<int32_t>(offsetof(XslElement_t1808680497, ___nsDecls_8)); }
	inline Hashtable_t1853889766 * get_nsDecls_8() const { return ___nsDecls_8; }
	inline Hashtable_t1853889766 ** get_address_of_nsDecls_8() { return &___nsDecls_8; }
	inline void set_nsDecls_8(Hashtable_t1853889766 * value)
	{
		___nsDecls_8 = value;
		Il2CppCodeGenWriteBarrier(&___nsDecls_8, value);
	}

	inline static int32_t get_offset_of_isEmptyElement_9() { return static_cast<int32_t>(offsetof(XslElement_t1808680497, ___isEmptyElement_9)); }
	inline bool get_isEmptyElement_9() const { return ___isEmptyElement_9; }
	inline bool* get_address_of_isEmptyElement_9() { return &___isEmptyElement_9; }
	inline void set_isEmptyElement_9(bool value)
	{
		___isEmptyElement_9 = value;
	}

	inline static int32_t get_offset_of_value_10() { return static_cast<int32_t>(offsetof(XslElement_t1808680497, ___value_10)); }
	inline XslOperation_t2153241355 * get_value_10() const { return ___value_10; }
	inline XslOperation_t2153241355 ** get_address_of_value_10() { return &___value_10; }
	inline void set_value_10(XslOperation_t2153241355 * value)
	{
		___value_10 = value;
		Il2CppCodeGenWriteBarrier(&___value_10, value);
	}

	inline static int32_t get_offset_of_useAttributeSets_11() { return static_cast<int32_t>(offsetof(XslElement_t1808680497, ___useAttributeSets_11)); }
	inline XmlQualifiedNameU5BU5D_t1471530361* get_useAttributeSets_11() const { return ___useAttributeSets_11; }
	inline XmlQualifiedNameU5BU5D_t1471530361** get_address_of_useAttributeSets_11() { return &___useAttributeSets_11; }
	inline void set_useAttributeSets_11(XmlQualifiedNameU5BU5D_t1471530361* value)
	{
		___useAttributeSets_11 = value;
		Il2CppCodeGenWriteBarrier(&___useAttributeSets_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
