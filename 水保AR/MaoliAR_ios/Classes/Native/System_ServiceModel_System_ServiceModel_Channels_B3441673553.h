﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.BodyWriter
struct  BodyWriter_t3441673553  : public Il2CppObject
{
public:
	// System.Boolean System.ServiceModel.Channels.BodyWriter::is_buffered
	bool ___is_buffered_0;

public:
	inline static int32_t get_offset_of_is_buffered_0() { return static_cast<int32_t>(offsetof(BodyWriter_t3441673553, ___is_buffered_0)); }
	inline bool get_is_buffered_0() const { return ___is_buffered_0; }
	inline bool* get_address_of_is_buffered_0() { return &___is_buffered_0; }
	inline void set_is_buffered_0(bool value)
	{
		___is_buffered_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
