﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Operations_XslCompiledElemen50593777.h"

// Mono.Xml.Xsl.Operations.XslOperation
struct XslOperation_t2153241355;
// System.Xml.XmlQualifiedName[]
struct XmlQualifiedNameU5BU5D_t1471530361;
// System.Collections.Hashtable
struct Hashtable_t1853889766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslCopy
struct  XslCopy_t2770797771  : public XslCompiledElement_t50593777
{
public:
	// Mono.Xml.Xsl.Operations.XslOperation Mono.Xml.Xsl.Operations.XslCopy::children
	XslOperation_t2153241355 * ___children_3;
	// System.Xml.XmlQualifiedName[] Mono.Xml.Xsl.Operations.XslCopy::useAttributeSets
	XmlQualifiedNameU5BU5D_t1471530361* ___useAttributeSets_4;
	// System.Collections.Hashtable Mono.Xml.Xsl.Operations.XslCopy::nsDecls
	Hashtable_t1853889766 * ___nsDecls_5;

public:
	inline static int32_t get_offset_of_children_3() { return static_cast<int32_t>(offsetof(XslCopy_t2770797771, ___children_3)); }
	inline XslOperation_t2153241355 * get_children_3() const { return ___children_3; }
	inline XslOperation_t2153241355 ** get_address_of_children_3() { return &___children_3; }
	inline void set_children_3(XslOperation_t2153241355 * value)
	{
		___children_3 = value;
		Il2CppCodeGenWriteBarrier(&___children_3, value);
	}

	inline static int32_t get_offset_of_useAttributeSets_4() { return static_cast<int32_t>(offsetof(XslCopy_t2770797771, ___useAttributeSets_4)); }
	inline XmlQualifiedNameU5BU5D_t1471530361* get_useAttributeSets_4() const { return ___useAttributeSets_4; }
	inline XmlQualifiedNameU5BU5D_t1471530361** get_address_of_useAttributeSets_4() { return &___useAttributeSets_4; }
	inline void set_useAttributeSets_4(XmlQualifiedNameU5BU5D_t1471530361* value)
	{
		___useAttributeSets_4 = value;
		Il2CppCodeGenWriteBarrier(&___useAttributeSets_4, value);
	}

	inline static int32_t get_offset_of_nsDecls_5() { return static_cast<int32_t>(offsetof(XslCopy_t2770797771, ___nsDecls_5)); }
	inline Hashtable_t1853889766 * get_nsDecls_5() const { return ___nsDecls_5; }
	inline Hashtable_t1853889766 ** get_address_of_nsDecls_5() { return &___nsDecls_5; }
	inline void set_nsDecls_5(Hashtable_t1853889766 * value)
	{
		___nsDecls_5 = value;
		Il2CppCodeGenWriteBarrier(&___nsDecls_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
