﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_HtmlControls_HtmlGenericC1038584562.h"

// System.String
struct String_t;
// System.Web.UI.HtmlControls.HtmlTitle
struct HtmlTitle_t1385856414;
// System.Web.UI.HtmlControls.StyleSheetBag
struct StyleSheetBag_t3875810066;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.HtmlControls.HtmlHead
struct  HtmlHead_t3571196538  : public HtmlGenericControl_t1038584562
{
public:
	// System.String System.Web.UI.HtmlControls.HtmlHead::titleText
	String_t* ___titleText_30;
	// System.Web.UI.HtmlControls.HtmlTitle System.Web.UI.HtmlControls.HtmlHead::title
	HtmlTitle_t1385856414 * ___title_31;
	// System.Web.UI.HtmlControls.StyleSheetBag System.Web.UI.HtmlControls.HtmlHead::styleSheet
	StyleSheetBag_t3875810066 * ___styleSheet_32;

public:
	inline static int32_t get_offset_of_titleText_30() { return static_cast<int32_t>(offsetof(HtmlHead_t3571196538, ___titleText_30)); }
	inline String_t* get_titleText_30() const { return ___titleText_30; }
	inline String_t** get_address_of_titleText_30() { return &___titleText_30; }
	inline void set_titleText_30(String_t* value)
	{
		___titleText_30 = value;
		Il2CppCodeGenWriteBarrier(&___titleText_30, value);
	}

	inline static int32_t get_offset_of_title_31() { return static_cast<int32_t>(offsetof(HtmlHead_t3571196538, ___title_31)); }
	inline HtmlTitle_t1385856414 * get_title_31() const { return ___title_31; }
	inline HtmlTitle_t1385856414 ** get_address_of_title_31() { return &___title_31; }
	inline void set_title_31(HtmlTitle_t1385856414 * value)
	{
		___title_31 = value;
		Il2CppCodeGenWriteBarrier(&___title_31, value);
	}

	inline static int32_t get_offset_of_styleSheet_32() { return static_cast<int32_t>(offsetof(HtmlHead_t3571196538, ___styleSheet_32)); }
	inline StyleSheetBag_t3875810066 * get_styleSheet_32() const { return ___styleSheet_32; }
	inline StyleSheetBag_t3875810066 ** get_address_of_styleSheet_32() { return &___styleSheet_32; }
	inline void set_styleSheet_32(StyleSheetBag_t3875810066 * value)
	{
		___styleSheet_32 = value;
		Il2CppCodeGenWriteBarrier(&___styleSheet_32, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
