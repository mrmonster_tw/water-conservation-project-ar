﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.EndpointAddress
struct EndpointAddress_t3119842923;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.EndpointAddressAugust2004
struct  EndpointAddressAugust2004_t2643301447  : public Il2CppObject
{
public:
	// System.ServiceModel.EndpointAddress System.ServiceModel.EndpointAddressAugust2004::address
	EndpointAddress_t3119842923 * ___address_0;

public:
	inline static int32_t get_offset_of_address_0() { return static_cast<int32_t>(offsetof(EndpointAddressAugust2004_t2643301447, ___address_0)); }
	inline EndpointAddress_t3119842923 * get_address_0() const { return ___address_0; }
	inline EndpointAddress_t3119842923 ** get_address_of_address_0() { return &___address_0; }
	inline void set_address_0(EndpointAddress_t3119842923 * value)
	{
		___address_0 = value;
		Il2CppCodeGenWriteBarrier(&___address_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
