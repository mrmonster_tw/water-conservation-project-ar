﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Specialized.HybridDictionary
struct HybridDictionary_t4070033136;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.StateBag
struct  StateBag_t282928164  : public Il2CppObject
{
public:
	// System.Collections.Specialized.HybridDictionary System.Web.UI.StateBag::ht
	HybridDictionary_t4070033136 * ___ht_0;
	// System.Boolean System.Web.UI.StateBag::track
	bool ___track_1;

public:
	inline static int32_t get_offset_of_ht_0() { return static_cast<int32_t>(offsetof(StateBag_t282928164, ___ht_0)); }
	inline HybridDictionary_t4070033136 * get_ht_0() const { return ___ht_0; }
	inline HybridDictionary_t4070033136 ** get_address_of_ht_0() { return &___ht_0; }
	inline void set_ht_0(HybridDictionary_t4070033136 * value)
	{
		___ht_0 = value;
		Il2CppCodeGenWriteBarrier(&___ht_0, value);
	}

	inline static int32_t get_offset_of_track_1() { return static_cast<int32_t>(offsetof(StateBag_t282928164, ___track_1)); }
	inline bool get_track_1() const { return ___track_1; }
	inline bool* get_address_of_track_1() { return &___track_1; }
	inline void set_track_1(bool value)
	{
		___track_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
