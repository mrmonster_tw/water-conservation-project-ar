﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Security_T2868958784.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.SslSecurityTokenParameters
struct  SslSecurityTokenParameters_t4188723207  : public SecurityTokenParameters_t2868958784
{
public:
	// System.Boolean System.ServiceModel.Security.Tokens.SslSecurityTokenParameters::cert
	bool ___cert_4;
	// System.Boolean System.ServiceModel.Security.Tokens.SslSecurityTokenParameters::cancel
	bool ___cancel_5;

public:
	inline static int32_t get_offset_of_cert_4() { return static_cast<int32_t>(offsetof(SslSecurityTokenParameters_t4188723207, ___cert_4)); }
	inline bool get_cert_4() const { return ___cert_4; }
	inline bool* get_address_of_cert_4() { return &___cert_4; }
	inline void set_cert_4(bool value)
	{
		___cert_4 = value;
	}

	inline static int32_t get_offset_of_cancel_5() { return static_cast<int32_t>(offsetof(SslSecurityTokenParameters_t4188723207, ___cancel_5)); }
	inline bool get_cancel_5() const { return ___cancel_5; }
	inline bool* get_address_of_cancel_5() { return &___cancel_5; }
	inline void set_cancel_5(bool value)
	{
		___cancel_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
