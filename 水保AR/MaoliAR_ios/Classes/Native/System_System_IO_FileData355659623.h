﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_IO_FileAttributes3417205536.h"
#include "mscorlib_System_DateTime3738529785.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileData
struct  FileData_t355659623  : public Il2CppObject
{
public:
	// System.String System.IO.FileData::Directory
	String_t* ___Directory_0;
	// System.IO.FileAttributes System.IO.FileData::Attributes
	int32_t ___Attributes_1;
	// System.Boolean System.IO.FileData::NotExists
	bool ___NotExists_2;
	// System.DateTime System.IO.FileData::CreationTime
	DateTime_t3738529785  ___CreationTime_3;
	// System.DateTime System.IO.FileData::LastWriteTime
	DateTime_t3738529785  ___LastWriteTime_4;

public:
	inline static int32_t get_offset_of_Directory_0() { return static_cast<int32_t>(offsetof(FileData_t355659623, ___Directory_0)); }
	inline String_t* get_Directory_0() const { return ___Directory_0; }
	inline String_t** get_address_of_Directory_0() { return &___Directory_0; }
	inline void set_Directory_0(String_t* value)
	{
		___Directory_0 = value;
		Il2CppCodeGenWriteBarrier(&___Directory_0, value);
	}

	inline static int32_t get_offset_of_Attributes_1() { return static_cast<int32_t>(offsetof(FileData_t355659623, ___Attributes_1)); }
	inline int32_t get_Attributes_1() const { return ___Attributes_1; }
	inline int32_t* get_address_of_Attributes_1() { return &___Attributes_1; }
	inline void set_Attributes_1(int32_t value)
	{
		___Attributes_1 = value;
	}

	inline static int32_t get_offset_of_NotExists_2() { return static_cast<int32_t>(offsetof(FileData_t355659623, ___NotExists_2)); }
	inline bool get_NotExists_2() const { return ___NotExists_2; }
	inline bool* get_address_of_NotExists_2() { return &___NotExists_2; }
	inline void set_NotExists_2(bool value)
	{
		___NotExists_2 = value;
	}

	inline static int32_t get_offset_of_CreationTime_3() { return static_cast<int32_t>(offsetof(FileData_t355659623, ___CreationTime_3)); }
	inline DateTime_t3738529785  get_CreationTime_3() const { return ___CreationTime_3; }
	inline DateTime_t3738529785 * get_address_of_CreationTime_3() { return &___CreationTime_3; }
	inline void set_CreationTime_3(DateTime_t3738529785  value)
	{
		___CreationTime_3 = value;
	}

	inline static int32_t get_offset_of_LastWriteTime_4() { return static_cast<int32_t>(offsetof(FileData_t355659623, ___LastWriteTime_4)); }
	inline DateTime_t3738529785  get_LastWriteTime_4() const { return ___LastWriteTime_4; }
	inline DateTime_t3738529785 * get_address_of_LastWriteTime_4() { return &___LastWriteTime_4; }
	inline void set_LastWriteTime_4(DateTime_t3738529785  value)
	{
		___LastWriteTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
