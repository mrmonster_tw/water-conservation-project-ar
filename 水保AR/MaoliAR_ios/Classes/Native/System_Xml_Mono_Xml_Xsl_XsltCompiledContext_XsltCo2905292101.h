﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.XsltCompiledContext/XsltContextInfo
struct  XsltContextInfo_t2905292101  : public Il2CppObject
{
public:
	// System.Boolean Mono.Xml.Xsl.XsltCompiledContext/XsltContextInfo::IsCData
	bool ___IsCData_0;
	// System.Boolean Mono.Xml.Xsl.XsltCompiledContext/XsltContextInfo::PreserveWhitespace
	bool ___PreserveWhitespace_1;
	// System.String Mono.Xml.Xsl.XsltCompiledContext/XsltContextInfo::ElementPrefix
	String_t* ___ElementPrefix_2;
	// System.String Mono.Xml.Xsl.XsltCompiledContext/XsltContextInfo::ElementNamespace
	String_t* ___ElementNamespace_3;

public:
	inline static int32_t get_offset_of_IsCData_0() { return static_cast<int32_t>(offsetof(XsltContextInfo_t2905292101, ___IsCData_0)); }
	inline bool get_IsCData_0() const { return ___IsCData_0; }
	inline bool* get_address_of_IsCData_0() { return &___IsCData_0; }
	inline void set_IsCData_0(bool value)
	{
		___IsCData_0 = value;
	}

	inline static int32_t get_offset_of_PreserveWhitespace_1() { return static_cast<int32_t>(offsetof(XsltContextInfo_t2905292101, ___PreserveWhitespace_1)); }
	inline bool get_PreserveWhitespace_1() const { return ___PreserveWhitespace_1; }
	inline bool* get_address_of_PreserveWhitespace_1() { return &___PreserveWhitespace_1; }
	inline void set_PreserveWhitespace_1(bool value)
	{
		___PreserveWhitespace_1 = value;
	}

	inline static int32_t get_offset_of_ElementPrefix_2() { return static_cast<int32_t>(offsetof(XsltContextInfo_t2905292101, ___ElementPrefix_2)); }
	inline String_t* get_ElementPrefix_2() const { return ___ElementPrefix_2; }
	inline String_t** get_address_of_ElementPrefix_2() { return &___ElementPrefix_2; }
	inline void set_ElementPrefix_2(String_t* value)
	{
		___ElementPrefix_2 = value;
		Il2CppCodeGenWriteBarrier(&___ElementPrefix_2, value);
	}

	inline static int32_t get_offset_of_ElementNamespace_3() { return static_cast<int32_t>(offsetof(XsltContextInfo_t2905292101, ___ElementNamespace_3)); }
	inline String_t* get_ElementNamespace_3() const { return ___ElementNamespace_3; }
	inline String_t** get_address_of_ElementNamespace_3() { return &___ElementNamespace_3; }
	inline void set_ElementNamespace_3(String_t* value)
	{
		___ElementNamespace_3 = value;
		Il2CppCodeGenWriteBarrier(&___ElementNamespace_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
