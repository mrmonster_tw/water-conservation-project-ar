﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Policy_E4159608136.h"
#include "mscorlib_System_DateTime3738529785.h"

// System.Collections.ObjectModel.Collection`1<System.IdentityModel.Claims.ClaimSet>
struct Collection_1_t2474017385;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IdentityModel.Claims.ClaimSet>
struct ReadOnlyCollection_1_t447270458;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Collections.Generic.Dictionary`2<System.IdentityModel.Policy.IAuthorizationPolicy,System.IdentityModel.Claims.ClaimSet>
struct Dictionary_2_t2646510926;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Policy.DefaultEvaluationContext
struct  DefaultEvaluationContext_t426032594  : public EvaluationContext_t4159608136
{
public:
	// System.DateTime System.IdentityModel.Policy.DefaultEvaluationContext::expiration_time
	DateTime_t3738529785  ___expiration_time_0;
	// System.Int32 System.IdentityModel.Policy.DefaultEvaluationContext::generation
	int32_t ___generation_1;
	// System.Collections.ObjectModel.Collection`1<System.IdentityModel.Claims.ClaimSet> System.IdentityModel.Policy.DefaultEvaluationContext::claim_sets
	Collection_1_t2474017385 * ___claim_sets_2;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IdentityModel.Claims.ClaimSet> System.IdentityModel.Policy.DefaultEvaluationContext::exposed_claim_sets
	ReadOnlyCollection_1_t447270458 * ___exposed_claim_sets_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> System.IdentityModel.Policy.DefaultEvaluationContext::properties
	Dictionary_2_t2865362463 * ___properties_4;
	// System.Collections.Generic.Dictionary`2<System.IdentityModel.Policy.IAuthorizationPolicy,System.IdentityModel.Claims.ClaimSet> System.IdentityModel.Policy.DefaultEvaluationContext::claim_set_map
	Dictionary_2_t2646510926 * ___claim_set_map_5;

public:
	inline static int32_t get_offset_of_expiration_time_0() { return static_cast<int32_t>(offsetof(DefaultEvaluationContext_t426032594, ___expiration_time_0)); }
	inline DateTime_t3738529785  get_expiration_time_0() const { return ___expiration_time_0; }
	inline DateTime_t3738529785 * get_address_of_expiration_time_0() { return &___expiration_time_0; }
	inline void set_expiration_time_0(DateTime_t3738529785  value)
	{
		___expiration_time_0 = value;
	}

	inline static int32_t get_offset_of_generation_1() { return static_cast<int32_t>(offsetof(DefaultEvaluationContext_t426032594, ___generation_1)); }
	inline int32_t get_generation_1() const { return ___generation_1; }
	inline int32_t* get_address_of_generation_1() { return &___generation_1; }
	inline void set_generation_1(int32_t value)
	{
		___generation_1 = value;
	}

	inline static int32_t get_offset_of_claim_sets_2() { return static_cast<int32_t>(offsetof(DefaultEvaluationContext_t426032594, ___claim_sets_2)); }
	inline Collection_1_t2474017385 * get_claim_sets_2() const { return ___claim_sets_2; }
	inline Collection_1_t2474017385 ** get_address_of_claim_sets_2() { return &___claim_sets_2; }
	inline void set_claim_sets_2(Collection_1_t2474017385 * value)
	{
		___claim_sets_2 = value;
		Il2CppCodeGenWriteBarrier(&___claim_sets_2, value);
	}

	inline static int32_t get_offset_of_exposed_claim_sets_3() { return static_cast<int32_t>(offsetof(DefaultEvaluationContext_t426032594, ___exposed_claim_sets_3)); }
	inline ReadOnlyCollection_1_t447270458 * get_exposed_claim_sets_3() const { return ___exposed_claim_sets_3; }
	inline ReadOnlyCollection_1_t447270458 ** get_address_of_exposed_claim_sets_3() { return &___exposed_claim_sets_3; }
	inline void set_exposed_claim_sets_3(ReadOnlyCollection_1_t447270458 * value)
	{
		___exposed_claim_sets_3 = value;
		Il2CppCodeGenWriteBarrier(&___exposed_claim_sets_3, value);
	}

	inline static int32_t get_offset_of_properties_4() { return static_cast<int32_t>(offsetof(DefaultEvaluationContext_t426032594, ___properties_4)); }
	inline Dictionary_2_t2865362463 * get_properties_4() const { return ___properties_4; }
	inline Dictionary_2_t2865362463 ** get_address_of_properties_4() { return &___properties_4; }
	inline void set_properties_4(Dictionary_2_t2865362463 * value)
	{
		___properties_4 = value;
		Il2CppCodeGenWriteBarrier(&___properties_4, value);
	}

	inline static int32_t get_offset_of_claim_set_map_5() { return static_cast<int32_t>(offsetof(DefaultEvaluationContext_t426032594, ___claim_set_map_5)); }
	inline Dictionary_2_t2646510926 * get_claim_set_map_5() const { return ___claim_set_map_5; }
	inline Dictionary_2_t2646510926 ** get_address_of_claim_set_map_5() { return &___claim_set_map_5; }
	inline void set_claim_set_map_5(Dictionary_2_t2646510926 * value)
	{
		___claim_set_map_5 = value;
		Il2CppCodeGenWriteBarrier(&___claim_set_map_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
