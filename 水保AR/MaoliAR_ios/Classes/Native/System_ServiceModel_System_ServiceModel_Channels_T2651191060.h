﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_I1191478253.h"

// System.Net.Sockets.TcpClient
struct TcpClient_t822906377;
// System.ServiceModel.Channels.TcpChannelInfo
struct TcpChannelInfo_t709592533;
// System.ServiceModel.Channels.TcpBinaryFrameManager
struct TcpBinaryFrameManager_t2884120632;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.TcpReplyChannel
struct  TcpReplyChannel_t2651191060  : public InternalReplyChannelBase_t1191478253
{
public:
	// System.Net.Sockets.TcpClient System.ServiceModel.Channels.TcpReplyChannel::client
	TcpClient_t822906377 * ___client_15;
	// System.ServiceModel.Channels.TcpChannelInfo System.ServiceModel.Channels.TcpReplyChannel::info
	TcpChannelInfo_t709592533 * ___info_16;
	// System.ServiceModel.Channels.TcpBinaryFrameManager System.ServiceModel.Channels.TcpReplyChannel::frame
	TcpBinaryFrameManager_t2884120632 * ___frame_17;

public:
	inline static int32_t get_offset_of_client_15() { return static_cast<int32_t>(offsetof(TcpReplyChannel_t2651191060, ___client_15)); }
	inline TcpClient_t822906377 * get_client_15() const { return ___client_15; }
	inline TcpClient_t822906377 ** get_address_of_client_15() { return &___client_15; }
	inline void set_client_15(TcpClient_t822906377 * value)
	{
		___client_15 = value;
		Il2CppCodeGenWriteBarrier(&___client_15, value);
	}

	inline static int32_t get_offset_of_info_16() { return static_cast<int32_t>(offsetof(TcpReplyChannel_t2651191060, ___info_16)); }
	inline TcpChannelInfo_t709592533 * get_info_16() const { return ___info_16; }
	inline TcpChannelInfo_t709592533 ** get_address_of_info_16() { return &___info_16; }
	inline void set_info_16(TcpChannelInfo_t709592533 * value)
	{
		___info_16 = value;
		Il2CppCodeGenWriteBarrier(&___info_16, value);
	}

	inline static int32_t get_offset_of_frame_17() { return static_cast<int32_t>(offsetof(TcpReplyChannel_t2651191060, ___frame_17)); }
	inline TcpBinaryFrameManager_t2884120632 * get_frame_17() const { return ___frame_17; }
	inline TcpBinaryFrameManager_t2884120632 ** get_address_of_frame_17() { return &___frame_17; }
	inline void set_frame_17(TcpBinaryFrameManager_t2884120632 * value)
	{
		___frame_17 = value;
		Il2CppCodeGenWriteBarrier(&___frame_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
