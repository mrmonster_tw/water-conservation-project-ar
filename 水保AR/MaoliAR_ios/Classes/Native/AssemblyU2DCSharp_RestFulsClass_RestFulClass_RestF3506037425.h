﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "AssemblyU2DCSharp_RestFulsClass_RestFulClass_Inser1296884003.h"
#include "AssemblyU2DCSharp_RestFulsClass_RestFulClass_GetSc4269622460.h"

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestFulsClass.RestFulClass/RestFul/Datas/<Setdata>c__Iterator2
struct  U3CSetdataU3Ec__Iterator2_t3506037425  : public Il2CppObject
{
public:
	// RestFulsClass.RestFulClass/InsertTypes RestFulsClass.RestFulClass/RestFul/Datas/<Setdata>c__Iterator2::insertTypes
	int32_t ___insertTypes_0;
	// System.String RestFulsClass.RestFulClass/RestFul/Datas/<Setdata>c__Iterator2::<Func>__0
	String_t* ___U3CFuncU3E__0_1;
	// RestFulsClass.RestFulClass/GetSceneMode RestFulsClass.RestFulClass/RestFul/Datas/<Setdata>c__Iterator2::sceneMode
	int32_t ___sceneMode_2;
	// System.String RestFulsClass.RestFulClass/RestFul/Datas/<Setdata>c__Iterator2::<requstUrl>__0
	String_t* ___U3CrequstUrlU3E__0_3;
	// System.String RestFulsClass.RestFulClass/RestFul/Datas/<Setdata>c__Iterator2::<elseData>__0
	String_t* ___U3CelseDataU3E__0_4;
	// System.String[] RestFulsClass.RestFulClass/RestFul/Datas/<Setdata>c__Iterator2::elseStr
	StringU5BU5D_t1281789340* ___elseStr_5;
	// System.String[] RestFulsClass.RestFulClass/RestFul/Datas/<Setdata>c__Iterator2::$locvar0
	StringU5BU5D_t1281789340* ___U24locvar0_6;
	// System.Int32 RestFulsClass.RestFulClass/RestFul/Datas/<Setdata>c__Iterator2::$locvar1
	int32_t ___U24locvar1_7;
	// System.Object RestFulsClass.RestFulClass/RestFul/Datas/<Setdata>c__Iterator2::$current
	Il2CppObject * ___U24current_8;
	// System.Boolean RestFulsClass.RestFulClass/RestFul/Datas/<Setdata>c__Iterator2::$disposing
	bool ___U24disposing_9;
	// System.Int32 RestFulsClass.RestFulClass/RestFul/Datas/<Setdata>c__Iterator2::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_insertTypes_0() { return static_cast<int32_t>(offsetof(U3CSetdataU3Ec__Iterator2_t3506037425, ___insertTypes_0)); }
	inline int32_t get_insertTypes_0() const { return ___insertTypes_0; }
	inline int32_t* get_address_of_insertTypes_0() { return &___insertTypes_0; }
	inline void set_insertTypes_0(int32_t value)
	{
		___insertTypes_0 = value;
	}

	inline static int32_t get_offset_of_U3CFuncU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSetdataU3Ec__Iterator2_t3506037425, ___U3CFuncU3E__0_1)); }
	inline String_t* get_U3CFuncU3E__0_1() const { return ___U3CFuncU3E__0_1; }
	inline String_t** get_address_of_U3CFuncU3E__0_1() { return &___U3CFuncU3E__0_1; }
	inline void set_U3CFuncU3E__0_1(String_t* value)
	{
		___U3CFuncU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CFuncU3E__0_1, value);
	}

	inline static int32_t get_offset_of_sceneMode_2() { return static_cast<int32_t>(offsetof(U3CSetdataU3Ec__Iterator2_t3506037425, ___sceneMode_2)); }
	inline int32_t get_sceneMode_2() const { return ___sceneMode_2; }
	inline int32_t* get_address_of_sceneMode_2() { return &___sceneMode_2; }
	inline void set_sceneMode_2(int32_t value)
	{
		___sceneMode_2 = value;
	}

	inline static int32_t get_offset_of_U3CrequstUrlU3E__0_3() { return static_cast<int32_t>(offsetof(U3CSetdataU3Ec__Iterator2_t3506037425, ___U3CrequstUrlU3E__0_3)); }
	inline String_t* get_U3CrequstUrlU3E__0_3() const { return ___U3CrequstUrlU3E__0_3; }
	inline String_t** get_address_of_U3CrequstUrlU3E__0_3() { return &___U3CrequstUrlU3E__0_3; }
	inline void set_U3CrequstUrlU3E__0_3(String_t* value)
	{
		___U3CrequstUrlU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrequstUrlU3E__0_3, value);
	}

	inline static int32_t get_offset_of_U3CelseDataU3E__0_4() { return static_cast<int32_t>(offsetof(U3CSetdataU3Ec__Iterator2_t3506037425, ___U3CelseDataU3E__0_4)); }
	inline String_t* get_U3CelseDataU3E__0_4() const { return ___U3CelseDataU3E__0_4; }
	inline String_t** get_address_of_U3CelseDataU3E__0_4() { return &___U3CelseDataU3E__0_4; }
	inline void set_U3CelseDataU3E__0_4(String_t* value)
	{
		___U3CelseDataU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CelseDataU3E__0_4, value);
	}

	inline static int32_t get_offset_of_elseStr_5() { return static_cast<int32_t>(offsetof(U3CSetdataU3Ec__Iterator2_t3506037425, ___elseStr_5)); }
	inline StringU5BU5D_t1281789340* get_elseStr_5() const { return ___elseStr_5; }
	inline StringU5BU5D_t1281789340** get_address_of_elseStr_5() { return &___elseStr_5; }
	inline void set_elseStr_5(StringU5BU5D_t1281789340* value)
	{
		___elseStr_5 = value;
		Il2CppCodeGenWriteBarrier(&___elseStr_5, value);
	}

	inline static int32_t get_offset_of_U24locvar0_6() { return static_cast<int32_t>(offsetof(U3CSetdataU3Ec__Iterator2_t3506037425, ___U24locvar0_6)); }
	inline StringU5BU5D_t1281789340* get_U24locvar0_6() const { return ___U24locvar0_6; }
	inline StringU5BU5D_t1281789340** get_address_of_U24locvar0_6() { return &___U24locvar0_6; }
	inline void set_U24locvar0_6(StringU5BU5D_t1281789340* value)
	{
		___U24locvar0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_6, value);
	}

	inline static int32_t get_offset_of_U24locvar1_7() { return static_cast<int32_t>(offsetof(U3CSetdataU3Ec__Iterator2_t3506037425, ___U24locvar1_7)); }
	inline int32_t get_U24locvar1_7() const { return ___U24locvar1_7; }
	inline int32_t* get_address_of_U24locvar1_7() { return &___U24locvar1_7; }
	inline void set_U24locvar1_7(int32_t value)
	{
		___U24locvar1_7 = value;
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CSetdataU3Ec__Iterator2_t3506037425, ___U24current_8)); }
	inline Il2CppObject * get_U24current_8() const { return ___U24current_8; }
	inline Il2CppObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(Il2CppObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_8, value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CSetdataU3Ec__Iterator2_t3506037425, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CSetdataU3Ec__Iterator2_t3506037425, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
