﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.AsyncOperation
struct AsyncOperation_t1445031843;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Level_06_Controller
struct  Level_06_Controller_t3711603533  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] Level_06_Controller::star_Images
	GameObjectU5BU5D_t3328599146* ___star_Images_2;
	// UnityEngine.GameObject Level_06_Controller::Parent_obj
	GameObject_t1113636619 * ___Parent_obj_3;
	// UnityEngine.GameObject Level_06_Controller::Shinning
	GameObject_t1113636619 * ___Shinning_4;
	// System.Single Level_06_Controller::Shinning_speed
	float ___Shinning_speed_5;
	// UnityEngine.UI.Image Level_06_Controller::black
	Image_t2670269651 * ___black_6;
	// UnityEngine.AsyncOperation Level_06_Controller::op
	AsyncOperation_t1445031843 * ___op_7;

public:
	inline static int32_t get_offset_of_star_Images_2() { return static_cast<int32_t>(offsetof(Level_06_Controller_t3711603533, ___star_Images_2)); }
	inline GameObjectU5BU5D_t3328599146* get_star_Images_2() const { return ___star_Images_2; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_star_Images_2() { return &___star_Images_2; }
	inline void set_star_Images_2(GameObjectU5BU5D_t3328599146* value)
	{
		___star_Images_2 = value;
		Il2CppCodeGenWriteBarrier(&___star_Images_2, value);
	}

	inline static int32_t get_offset_of_Parent_obj_3() { return static_cast<int32_t>(offsetof(Level_06_Controller_t3711603533, ___Parent_obj_3)); }
	inline GameObject_t1113636619 * get_Parent_obj_3() const { return ___Parent_obj_3; }
	inline GameObject_t1113636619 ** get_address_of_Parent_obj_3() { return &___Parent_obj_3; }
	inline void set_Parent_obj_3(GameObject_t1113636619 * value)
	{
		___Parent_obj_3 = value;
		Il2CppCodeGenWriteBarrier(&___Parent_obj_3, value);
	}

	inline static int32_t get_offset_of_Shinning_4() { return static_cast<int32_t>(offsetof(Level_06_Controller_t3711603533, ___Shinning_4)); }
	inline GameObject_t1113636619 * get_Shinning_4() const { return ___Shinning_4; }
	inline GameObject_t1113636619 ** get_address_of_Shinning_4() { return &___Shinning_4; }
	inline void set_Shinning_4(GameObject_t1113636619 * value)
	{
		___Shinning_4 = value;
		Il2CppCodeGenWriteBarrier(&___Shinning_4, value);
	}

	inline static int32_t get_offset_of_Shinning_speed_5() { return static_cast<int32_t>(offsetof(Level_06_Controller_t3711603533, ___Shinning_speed_5)); }
	inline float get_Shinning_speed_5() const { return ___Shinning_speed_5; }
	inline float* get_address_of_Shinning_speed_5() { return &___Shinning_speed_5; }
	inline void set_Shinning_speed_5(float value)
	{
		___Shinning_speed_5 = value;
	}

	inline static int32_t get_offset_of_black_6() { return static_cast<int32_t>(offsetof(Level_06_Controller_t3711603533, ___black_6)); }
	inline Image_t2670269651 * get_black_6() const { return ___black_6; }
	inline Image_t2670269651 ** get_address_of_black_6() { return &___black_6; }
	inline void set_black_6(Image_t2670269651 * value)
	{
		___black_6 = value;
		Il2CppCodeGenWriteBarrier(&___black_6, value);
	}

	inline static int32_t get_offset_of_op_7() { return static_cast<int32_t>(offsetof(Level_06_Controller_t3711603533, ___op_7)); }
	inline AsyncOperation_t1445031843 * get_op_7() const { return ___op_7; }
	inline AsyncOperation_t1445031843 ** get_address_of_op_7() { return &___op_7; }
	inline void set_op_7(AsyncOperation_t1445031843 * value)
	{
		___op_7 = value;
		Il2CppCodeGenWriteBarrier(&___op_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
