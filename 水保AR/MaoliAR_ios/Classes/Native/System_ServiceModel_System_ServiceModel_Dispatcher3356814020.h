﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Dispatcher2537743531.h"

// System.ServiceModel.Channels.IDuplexChannel
struct IDuplexChannel_t3233783714;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.OperationInvokerHandler
struct  OperationInvokerHandler_t3356814020  : public BaseRequestProcessorHandler_t2537743531
{
public:
	// System.ServiceModel.Channels.IDuplexChannel System.ServiceModel.Dispatcher.OperationInvokerHandler::duplex
	Il2CppObject * ___duplex_1;

public:
	inline static int32_t get_offset_of_duplex_1() { return static_cast<int32_t>(offsetof(OperationInvokerHandler_t3356814020, ___duplex_1)); }
	inline Il2CppObject * get_duplex_1() const { return ___duplex_1; }
	inline Il2CppObject ** get_address_of_duplex_1() { return &___duplex_1; }
	inline void set_duplex_1(Il2CppObject * value)
	{
		___duplex_1 = value;
		Il2CppCodeGenWriteBarrier(&___duplex_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
