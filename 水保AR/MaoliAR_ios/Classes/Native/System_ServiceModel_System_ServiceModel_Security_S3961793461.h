﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Security_S3911394864.h"
#include "System_ServiceModel_System_ServiceModel_Security_S2144301020.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.SupportingTokenSpecification
struct  SupportingTokenSpecification_t3961793461  : public SecurityTokenSpecification_t3911394864
{
public:
	// System.ServiceModel.Security.SecurityTokenAttachmentMode System.ServiceModel.Security.SupportingTokenSpecification::mode
	int32_t ___mode_2;

public:
	inline static int32_t get_offset_of_mode_2() { return static_cast<int32_t>(offsetof(SupportingTokenSpecification_t3961793461, ___mode_2)); }
	inline int32_t get_mode_2() const { return ___mode_2; }
	inline int32_t* get_address_of_mode_2() { return &___mode_2; }
	inline void set_mode_2(int32_t value)
	{
		___mode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
