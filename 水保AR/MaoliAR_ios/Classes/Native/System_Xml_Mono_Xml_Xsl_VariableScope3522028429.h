﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// Mono.Xml.Xsl.VariableScope
struct VariableScope_t3522028429;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.VariableScope
struct  VariableScope_t3522028429  : public Il2CppObject
{
public:
	// System.Collections.ArrayList Mono.Xml.Xsl.VariableScope::variableNames
	ArrayList_t2718874744 * ___variableNames_0;
	// System.Collections.Hashtable Mono.Xml.Xsl.VariableScope::variables
	Hashtable_t1853889766 * ___variables_1;
	// Mono.Xml.Xsl.VariableScope Mono.Xml.Xsl.VariableScope::parent
	VariableScope_t3522028429 * ___parent_2;
	// System.Int32 Mono.Xml.Xsl.VariableScope::nextSlot
	int32_t ___nextSlot_3;
	// System.Int32 Mono.Xml.Xsl.VariableScope::highTide
	int32_t ___highTide_4;

public:
	inline static int32_t get_offset_of_variableNames_0() { return static_cast<int32_t>(offsetof(VariableScope_t3522028429, ___variableNames_0)); }
	inline ArrayList_t2718874744 * get_variableNames_0() const { return ___variableNames_0; }
	inline ArrayList_t2718874744 ** get_address_of_variableNames_0() { return &___variableNames_0; }
	inline void set_variableNames_0(ArrayList_t2718874744 * value)
	{
		___variableNames_0 = value;
		Il2CppCodeGenWriteBarrier(&___variableNames_0, value);
	}

	inline static int32_t get_offset_of_variables_1() { return static_cast<int32_t>(offsetof(VariableScope_t3522028429, ___variables_1)); }
	inline Hashtable_t1853889766 * get_variables_1() const { return ___variables_1; }
	inline Hashtable_t1853889766 ** get_address_of_variables_1() { return &___variables_1; }
	inline void set_variables_1(Hashtable_t1853889766 * value)
	{
		___variables_1 = value;
		Il2CppCodeGenWriteBarrier(&___variables_1, value);
	}

	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(VariableScope_t3522028429, ___parent_2)); }
	inline VariableScope_t3522028429 * get_parent_2() const { return ___parent_2; }
	inline VariableScope_t3522028429 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(VariableScope_t3522028429 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_nextSlot_3() { return static_cast<int32_t>(offsetof(VariableScope_t3522028429, ___nextSlot_3)); }
	inline int32_t get_nextSlot_3() const { return ___nextSlot_3; }
	inline int32_t* get_address_of_nextSlot_3() { return &___nextSlot_3; }
	inline void set_nextSlot_3(int32_t value)
	{
		___nextSlot_3 = value;
	}

	inline static int32_t get_offset_of_highTide_4() { return static_cast<int32_t>(offsetof(VariableScope_t3522028429, ___highTide_4)); }
	inline int32_t get_highTide_4() const { return ___highTide_4; }
	inline int32_t* get_address_of_highTide_4() { return &___highTide_4; }
	inline void set_highTide_4(int32_t value)
	{
		___highTide_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
