﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Communicati147767313.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.ActionNotSupportedException
struct  ActionNotSupportedException_t2864863127  : public CommunicationException_t147767313
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
