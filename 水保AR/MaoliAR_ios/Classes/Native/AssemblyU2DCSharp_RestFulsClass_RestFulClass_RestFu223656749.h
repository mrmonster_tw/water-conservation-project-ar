﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestFulsClass.RestFulClass/RestFul/WaitResults
struct  WaitResults_t223656749 
{
public:
	union
	{
		struct
		{
		};
		uint8_t WaitResults_t223656749__padding[1];
	};

public:
};

struct WaitResults_t223656749_StaticFields
{
public:
	// System.Boolean RestFulsClass.RestFulClass/RestFul/WaitResults::waitResult
	bool ___waitResult_0;

public:
	inline static int32_t get_offset_of_waitResult_0() { return static_cast<int32_t>(offsetof(WaitResults_t223656749_StaticFields, ___waitResult_0)); }
	inline bool get_waitResult_0() const { return ___waitResult_0; }
	inline bool* get_address_of_waitResult_0() { return &___waitResult_0; }
	inline void set_waitResult_0(bool value)
	{
		___waitResult_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
