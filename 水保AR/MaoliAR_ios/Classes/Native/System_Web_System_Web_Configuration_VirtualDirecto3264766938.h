﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.VirtualDirectoryMapping
struct  VirtualDirectoryMapping_t3264766938  : public Il2CppObject
{
public:
	// System.String System.Web.Configuration.VirtualDirectoryMapping::physicalDirectory
	String_t* ___physicalDirectory_0;
	// System.Boolean System.Web.Configuration.VirtualDirectoryMapping::isAppRoot
	bool ___isAppRoot_1;
	// System.String System.Web.Configuration.VirtualDirectoryMapping::configFileBaseName
	String_t* ___configFileBaseName_2;
	// System.String System.Web.Configuration.VirtualDirectoryMapping::virtualDirectory
	String_t* ___virtualDirectory_3;

public:
	inline static int32_t get_offset_of_physicalDirectory_0() { return static_cast<int32_t>(offsetof(VirtualDirectoryMapping_t3264766938, ___physicalDirectory_0)); }
	inline String_t* get_physicalDirectory_0() const { return ___physicalDirectory_0; }
	inline String_t** get_address_of_physicalDirectory_0() { return &___physicalDirectory_0; }
	inline void set_physicalDirectory_0(String_t* value)
	{
		___physicalDirectory_0 = value;
		Il2CppCodeGenWriteBarrier(&___physicalDirectory_0, value);
	}

	inline static int32_t get_offset_of_isAppRoot_1() { return static_cast<int32_t>(offsetof(VirtualDirectoryMapping_t3264766938, ___isAppRoot_1)); }
	inline bool get_isAppRoot_1() const { return ___isAppRoot_1; }
	inline bool* get_address_of_isAppRoot_1() { return &___isAppRoot_1; }
	inline void set_isAppRoot_1(bool value)
	{
		___isAppRoot_1 = value;
	}

	inline static int32_t get_offset_of_configFileBaseName_2() { return static_cast<int32_t>(offsetof(VirtualDirectoryMapping_t3264766938, ___configFileBaseName_2)); }
	inline String_t* get_configFileBaseName_2() const { return ___configFileBaseName_2; }
	inline String_t** get_address_of_configFileBaseName_2() { return &___configFileBaseName_2; }
	inline void set_configFileBaseName_2(String_t* value)
	{
		___configFileBaseName_2 = value;
		Il2CppCodeGenWriteBarrier(&___configFileBaseName_2, value);
	}

	inline static int32_t get_offset_of_virtualDirectory_3() { return static_cast<int32_t>(offsetof(VirtualDirectoryMapping_t3264766938, ___virtualDirectory_3)); }
	inline String_t* get_virtualDirectory_3() const { return ___virtualDirectory_3; }
	inline String_t** get_address_of_virtualDirectory_3() { return &___virtualDirectory_3; }
	inline void set_virtualDirectory_3(String_t* value)
	{
		___virtualDirectory_3 = value;
		Il2CppCodeGenWriteBarrier(&___virtualDirectory_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
