﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.IO.Stream
struct Stream_t1273022909;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Text.Encoding
struct Encoding_t1523322056;
// System.Text.StringBuilder
struct StringBuilder_t1712802186;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpMultipart
struct  HttpMultipart_t476075009  : public Il2CppObject
{
public:
	// System.IO.Stream System.Web.HttpMultipart::data
	Stream_t1273022909 * ___data_0;
	// System.String System.Web.HttpMultipart::boundary
	String_t* ___boundary_1;
	// System.Byte[] System.Web.HttpMultipart::boundary_bytes
	ByteU5BU5D_t4116647657* ___boundary_bytes_2;
	// System.Byte[] System.Web.HttpMultipart::buffer
	ByteU5BU5D_t4116647657* ___buffer_3;
	// System.Boolean System.Web.HttpMultipart::at_eof
	bool ___at_eof_4;
	// System.Text.Encoding System.Web.HttpMultipart::encoding
	Encoding_t1523322056 * ___encoding_5;
	// System.Text.StringBuilder System.Web.HttpMultipart::sb
	StringBuilder_t1712802186 * ___sb_6;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(HttpMultipart_t476075009, ___data_0)); }
	inline Stream_t1273022909 * get_data_0() const { return ___data_0; }
	inline Stream_t1273022909 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(Stream_t1273022909 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier(&___data_0, value);
	}

	inline static int32_t get_offset_of_boundary_1() { return static_cast<int32_t>(offsetof(HttpMultipart_t476075009, ___boundary_1)); }
	inline String_t* get_boundary_1() const { return ___boundary_1; }
	inline String_t** get_address_of_boundary_1() { return &___boundary_1; }
	inline void set_boundary_1(String_t* value)
	{
		___boundary_1 = value;
		Il2CppCodeGenWriteBarrier(&___boundary_1, value);
	}

	inline static int32_t get_offset_of_boundary_bytes_2() { return static_cast<int32_t>(offsetof(HttpMultipart_t476075009, ___boundary_bytes_2)); }
	inline ByteU5BU5D_t4116647657* get_boundary_bytes_2() const { return ___boundary_bytes_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_boundary_bytes_2() { return &___boundary_bytes_2; }
	inline void set_boundary_bytes_2(ByteU5BU5D_t4116647657* value)
	{
		___boundary_bytes_2 = value;
		Il2CppCodeGenWriteBarrier(&___boundary_bytes_2, value);
	}

	inline static int32_t get_offset_of_buffer_3() { return static_cast<int32_t>(offsetof(HttpMultipart_t476075009, ___buffer_3)); }
	inline ByteU5BU5D_t4116647657* get_buffer_3() const { return ___buffer_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_buffer_3() { return &___buffer_3; }
	inline void set_buffer_3(ByteU5BU5D_t4116647657* value)
	{
		___buffer_3 = value;
		Il2CppCodeGenWriteBarrier(&___buffer_3, value);
	}

	inline static int32_t get_offset_of_at_eof_4() { return static_cast<int32_t>(offsetof(HttpMultipart_t476075009, ___at_eof_4)); }
	inline bool get_at_eof_4() const { return ___at_eof_4; }
	inline bool* get_address_of_at_eof_4() { return &___at_eof_4; }
	inline void set_at_eof_4(bool value)
	{
		___at_eof_4 = value;
	}

	inline static int32_t get_offset_of_encoding_5() { return static_cast<int32_t>(offsetof(HttpMultipart_t476075009, ___encoding_5)); }
	inline Encoding_t1523322056 * get_encoding_5() const { return ___encoding_5; }
	inline Encoding_t1523322056 ** get_address_of_encoding_5() { return &___encoding_5; }
	inline void set_encoding_5(Encoding_t1523322056 * value)
	{
		___encoding_5 = value;
		Il2CppCodeGenWriteBarrier(&___encoding_5, value);
	}

	inline static int32_t get_offset_of_sb_6() { return static_cast<int32_t>(offsetof(HttpMultipart_t476075009, ___sb_6)); }
	inline StringBuilder_t1712802186 * get_sb_6() const { return ___sb_6; }
	inline StringBuilder_t1712802186 ** get_address_of_sb_6() { return &___sb_6; }
	inline void set_sb_6(StringBuilder_t1712802186 * value)
	{
		___sb_6 = value;
		Il2CppCodeGenWriteBarrier(&___sb_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
