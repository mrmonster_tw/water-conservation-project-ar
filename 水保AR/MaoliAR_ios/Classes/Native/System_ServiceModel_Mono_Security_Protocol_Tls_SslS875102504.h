﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_Mono_Security_Protocol_Tls_Ssl1667413407.h"

// Mono.Security.Protocol.Tls.CertificateValidationCallback
struct CertificateValidationCallback_t4091668219;
// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback
struct PrivateKeySelectionCallback_t3240194218;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.SslServerStream
struct  SslServerStream_t875102505  : public SslStreamBase_t1667413408
{
public:
	// Mono.Security.Protocol.Tls.CertificateValidationCallback Mono.Security.Protocol.Tls.SslServerStream::ClientCertValidation
	CertificateValidationCallback_t4091668219 * ___ClientCertValidation_15;
	// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback Mono.Security.Protocol.Tls.SslServerStream::PrivateKeySelection
	PrivateKeySelectionCallback_t3240194218 * ___PrivateKeySelection_16;

public:
	inline static int32_t get_offset_of_ClientCertValidation_15() { return static_cast<int32_t>(offsetof(SslServerStream_t875102505, ___ClientCertValidation_15)); }
	inline CertificateValidationCallback_t4091668219 * get_ClientCertValidation_15() const { return ___ClientCertValidation_15; }
	inline CertificateValidationCallback_t4091668219 ** get_address_of_ClientCertValidation_15() { return &___ClientCertValidation_15; }
	inline void set_ClientCertValidation_15(CertificateValidationCallback_t4091668219 * value)
	{
		___ClientCertValidation_15 = value;
		Il2CppCodeGenWriteBarrier(&___ClientCertValidation_15, value);
	}

	inline static int32_t get_offset_of_PrivateKeySelection_16() { return static_cast<int32_t>(offsetof(SslServerStream_t875102505, ___PrivateKeySelection_16)); }
	inline PrivateKeySelectionCallback_t3240194218 * get_PrivateKeySelection_16() const { return ___PrivateKeySelection_16; }
	inline PrivateKeySelectionCallback_t3240194218 ** get_address_of_PrivateKeySelection_16() { return &___PrivateKeySelection_16; }
	inline void set_PrivateKeySelection_16(PrivateKeySelectionCallback_t3240194218 * value)
	{
		___PrivateKeySelection_16 = value;
		Il2CppCodeGenWriteBarrier(&___PrivateKeySelection_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
