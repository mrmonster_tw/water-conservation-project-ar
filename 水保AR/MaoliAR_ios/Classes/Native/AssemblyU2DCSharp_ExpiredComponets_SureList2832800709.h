﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"

// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Animator[]
struct AnimatorU5BU5D_t608880338;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExpiredComponets/SureList
struct  SureList_t2832800709 
{
public:
	// UnityEngine.RectTransform ExpiredComponets/SureList::sureListRoot
	RectTransform_t3704657025 * ___sureListRoot_0;
	// UnityEngine.RectTransform ExpiredComponets/SureList::sureResult
	RectTransform_t3704657025 * ___sureResult_1;
	// UnityEngine.RectTransform ExpiredComponets/SureList::message
	RectTransform_t3704657025 * ___message_2;
	// UnityEngine.Animator[] ExpiredComponets/SureList::sureListAnimator
	AnimatorU5BU5D_t608880338* ___sureListAnimator_3;
	// UnityEngine.Sprite[] ExpiredComponets/SureList::resultSprite
	SpriteU5BU5D_t2581906349* ___resultSprite_4;

public:
	inline static int32_t get_offset_of_sureListRoot_0() { return static_cast<int32_t>(offsetof(SureList_t2832800709, ___sureListRoot_0)); }
	inline RectTransform_t3704657025 * get_sureListRoot_0() const { return ___sureListRoot_0; }
	inline RectTransform_t3704657025 ** get_address_of_sureListRoot_0() { return &___sureListRoot_0; }
	inline void set_sureListRoot_0(RectTransform_t3704657025 * value)
	{
		___sureListRoot_0 = value;
		Il2CppCodeGenWriteBarrier(&___sureListRoot_0, value);
	}

	inline static int32_t get_offset_of_sureResult_1() { return static_cast<int32_t>(offsetof(SureList_t2832800709, ___sureResult_1)); }
	inline RectTransform_t3704657025 * get_sureResult_1() const { return ___sureResult_1; }
	inline RectTransform_t3704657025 ** get_address_of_sureResult_1() { return &___sureResult_1; }
	inline void set_sureResult_1(RectTransform_t3704657025 * value)
	{
		___sureResult_1 = value;
		Il2CppCodeGenWriteBarrier(&___sureResult_1, value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(SureList_t2832800709, ___message_2)); }
	inline RectTransform_t3704657025 * get_message_2() const { return ___message_2; }
	inline RectTransform_t3704657025 ** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(RectTransform_t3704657025 * value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier(&___message_2, value);
	}

	inline static int32_t get_offset_of_sureListAnimator_3() { return static_cast<int32_t>(offsetof(SureList_t2832800709, ___sureListAnimator_3)); }
	inline AnimatorU5BU5D_t608880338* get_sureListAnimator_3() const { return ___sureListAnimator_3; }
	inline AnimatorU5BU5D_t608880338** get_address_of_sureListAnimator_3() { return &___sureListAnimator_3; }
	inline void set_sureListAnimator_3(AnimatorU5BU5D_t608880338* value)
	{
		___sureListAnimator_3 = value;
		Il2CppCodeGenWriteBarrier(&___sureListAnimator_3, value);
	}

	inline static int32_t get_offset_of_resultSprite_4() { return static_cast<int32_t>(offsetof(SureList_t2832800709, ___resultSprite_4)); }
	inline SpriteU5BU5D_t2581906349* get_resultSprite_4() const { return ___resultSprite_4; }
	inline SpriteU5BU5D_t2581906349** get_address_of_resultSprite_4() { return &___resultSprite_4; }
	inline void set_resultSprite_4(SpriteU5BU5D_t2581906349* value)
	{
		___resultSprite_4 = value;
		Il2CppCodeGenWriteBarrier(&___resultSprite_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of ExpiredComponets/SureList
struct SureList_t2832800709_marshaled_pinvoke
{
	RectTransform_t3704657025 * ___sureListRoot_0;
	RectTransform_t3704657025 * ___sureResult_1;
	RectTransform_t3704657025 * ___message_2;
	AnimatorU5BU5D_t608880338* ___sureListAnimator_3;
	SpriteU5BU5D_t2581906349* ___resultSprite_4;
};
// Native definition for COM marshalling of ExpiredComponets/SureList
struct SureList_t2832800709_marshaled_com
{
	RectTransform_t3704657025 * ___sureListRoot_0;
	RectTransform_t3704657025 * ___sureResult_1;
	RectTransform_t3704657025 * ___message_2;
	AnimatorU5BU5D_t608880338* ___sureListAnimator_3;
	SpriteU5BU5D_t2581906349* ___resultSprite_4;
};
