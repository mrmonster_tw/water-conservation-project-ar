﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.Configuration.GlobalizationSection
struct GlobalizationSection_t244366663;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Util.WebEncoding
struct  WebEncoding_t373074341  : public Il2CppObject
{
public:

public:
};

struct WebEncoding_t373074341_StaticFields
{
public:
	// System.Boolean System.Web.Util.WebEncoding::cached
	bool ___cached_0;
	// System.Web.Configuration.GlobalizationSection System.Web.Util.WebEncoding::sect
	GlobalizationSection_t244366663 * ___sect_1;

public:
	inline static int32_t get_offset_of_cached_0() { return static_cast<int32_t>(offsetof(WebEncoding_t373074341_StaticFields, ___cached_0)); }
	inline bool get_cached_0() const { return ___cached_0; }
	inline bool* get_address_of_cached_0() { return &___cached_0; }
	inline void set_cached_0(bool value)
	{
		___cached_0 = value;
	}

	inline static int32_t get_offset_of_sect_1() { return static_cast<int32_t>(offsetof(WebEncoding_t373074341_StaticFields, ___sect_1)); }
	inline GlobalizationSection_t244366663 * get_sect_1() const { return ___sect_1; }
	inline GlobalizationSection_t244366663 ** get_address_of_sect_1() { return &___sect_1; }
	inline void set_sect_1(GlobalizationSection_t244366663 * value)
	{
		___sect_1 = value;
		Il2CppCodeGenWriteBarrier(&___sect_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
