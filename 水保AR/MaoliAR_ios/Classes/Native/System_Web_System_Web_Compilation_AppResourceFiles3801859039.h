﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Generic.List`1<System.Web.Compilation.AppResourceFileInfo>
struct List_1_t776851571;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.AppResourceFilesCollection
struct  AppResourceFilesCollection_t3801859039  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.Web.Compilation.AppResourceFileInfo> System.Web.Compilation.AppResourceFilesCollection::files
	List_1_t776851571 * ___files_0;
	// System.Boolean System.Web.Compilation.AppResourceFilesCollection::isGlobal
	bool ___isGlobal_1;
	// System.String System.Web.Compilation.AppResourceFilesCollection::sourceDir
	String_t* ___sourceDir_2;

public:
	inline static int32_t get_offset_of_files_0() { return static_cast<int32_t>(offsetof(AppResourceFilesCollection_t3801859039, ___files_0)); }
	inline List_1_t776851571 * get_files_0() const { return ___files_0; }
	inline List_1_t776851571 ** get_address_of_files_0() { return &___files_0; }
	inline void set_files_0(List_1_t776851571 * value)
	{
		___files_0 = value;
		Il2CppCodeGenWriteBarrier(&___files_0, value);
	}

	inline static int32_t get_offset_of_isGlobal_1() { return static_cast<int32_t>(offsetof(AppResourceFilesCollection_t3801859039, ___isGlobal_1)); }
	inline bool get_isGlobal_1() const { return ___isGlobal_1; }
	inline bool* get_address_of_isGlobal_1() { return &___isGlobal_1; }
	inline void set_isGlobal_1(bool value)
	{
		___isGlobal_1 = value;
	}

	inline static int32_t get_offset_of_sourceDir_2() { return static_cast<int32_t>(offsetof(AppResourceFilesCollection_t3801859039, ___sourceDir_2)); }
	inline String_t* get_sourceDir_2() const { return ___sourceDir_2; }
	inline String_t** get_address_of_sourceDir_2() { return &___sourceDir_2; }
	inline void set_sourceDir_2(String_t* value)
	{
		___sourceDir_2 = value;
		Il2CppCodeGenWriteBarrier(&___sourceDir_2, value);
	}
};

struct AppResourceFilesCollection_t3801859039_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Web.Compilation.AppResourceFilesCollection::<>f__switch$map5
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map5_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map5_3() { return static_cast<int32_t>(offsetof(AppResourceFilesCollection_t3801859039_StaticFields, ___U3CU3Ef__switchU24map5_3)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map5_3() const { return ___U3CU3Ef__switchU24map5_3; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map5_3() { return &___U3CU3Ef__switchU24map5_3; }
	inline void set_U3CU3Ef__switchU24map5_3(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map5_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map5_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
