﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "Mono_Web_Mono_Web_Util_SettingsMappingPlatform242231579.h"

// System.Object
struct Il2CppObject;
// Mono.Web.Util.SettingsMappingManager
struct SettingsMappingManager_t1609949884;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.Type,Mono.Web.Util.SettingsMapping>
struct Dictionary_2_t2606747707;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t132545152;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Web.Util.SettingsMappingManager
struct  SettingsMappingManager_t1609949884  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,Mono.Web.Util.SettingsMapping> Mono.Web.Util.SettingsMappingManager::_mappers
	Dictionary_2_t2606747707 * ____mappers_3;

public:
	inline static int32_t get_offset_of__mappers_3() { return static_cast<int32_t>(offsetof(SettingsMappingManager_t1609949884, ____mappers_3)); }
	inline Dictionary_2_t2606747707 * get__mappers_3() const { return ____mappers_3; }
	inline Dictionary_2_t2606747707 ** get_address_of__mappers_3() { return &____mappers_3; }
	inline void set__mappers_3(Dictionary_2_t2606747707 * value)
	{
		____mappers_3 = value;
		Il2CppCodeGenWriteBarrier(&____mappers_3, value);
	}
};

struct SettingsMappingManager_t1609949884_StaticFields
{
public:
	// System.Object Mono.Web.Util.SettingsMappingManager::mapperLock
	Il2CppObject * ___mapperLock_0;
	// Mono.Web.Util.SettingsMappingManager Mono.Web.Util.SettingsMappingManager::_instance
	SettingsMappingManager_t1609949884 * ____instance_1;
	// System.String Mono.Web.Util.SettingsMappingManager::_mappingFile
	String_t* ____mappingFile_2;
	// System.Collections.Generic.Dictionary`2<System.Object,System.Object> Mono.Web.Util.SettingsMappingManager::_mappedSections
	Dictionary_2_t132545152 * ____mappedSections_4;
	// Mono.Web.Util.SettingsMappingPlatform Mono.Web.Util.SettingsMappingManager::_myPlatform
	int32_t ____myPlatform_5;
	// System.Boolean Mono.Web.Util.SettingsMappingManager::_runningOnWindows
	bool ____runningOnWindows_6;

public:
	inline static int32_t get_offset_of_mapperLock_0() { return static_cast<int32_t>(offsetof(SettingsMappingManager_t1609949884_StaticFields, ___mapperLock_0)); }
	inline Il2CppObject * get_mapperLock_0() const { return ___mapperLock_0; }
	inline Il2CppObject ** get_address_of_mapperLock_0() { return &___mapperLock_0; }
	inline void set_mapperLock_0(Il2CppObject * value)
	{
		___mapperLock_0 = value;
		Il2CppCodeGenWriteBarrier(&___mapperLock_0, value);
	}

	inline static int32_t get_offset_of__instance_1() { return static_cast<int32_t>(offsetof(SettingsMappingManager_t1609949884_StaticFields, ____instance_1)); }
	inline SettingsMappingManager_t1609949884 * get__instance_1() const { return ____instance_1; }
	inline SettingsMappingManager_t1609949884 ** get_address_of__instance_1() { return &____instance_1; }
	inline void set__instance_1(SettingsMappingManager_t1609949884 * value)
	{
		____instance_1 = value;
		Il2CppCodeGenWriteBarrier(&____instance_1, value);
	}

	inline static int32_t get_offset_of__mappingFile_2() { return static_cast<int32_t>(offsetof(SettingsMappingManager_t1609949884_StaticFields, ____mappingFile_2)); }
	inline String_t* get__mappingFile_2() const { return ____mappingFile_2; }
	inline String_t** get_address_of__mappingFile_2() { return &____mappingFile_2; }
	inline void set__mappingFile_2(String_t* value)
	{
		____mappingFile_2 = value;
		Il2CppCodeGenWriteBarrier(&____mappingFile_2, value);
	}

	inline static int32_t get_offset_of__mappedSections_4() { return static_cast<int32_t>(offsetof(SettingsMappingManager_t1609949884_StaticFields, ____mappedSections_4)); }
	inline Dictionary_2_t132545152 * get__mappedSections_4() const { return ____mappedSections_4; }
	inline Dictionary_2_t132545152 ** get_address_of__mappedSections_4() { return &____mappedSections_4; }
	inline void set__mappedSections_4(Dictionary_2_t132545152 * value)
	{
		____mappedSections_4 = value;
		Il2CppCodeGenWriteBarrier(&____mappedSections_4, value);
	}

	inline static int32_t get_offset_of__myPlatform_5() { return static_cast<int32_t>(offsetof(SettingsMappingManager_t1609949884_StaticFields, ____myPlatform_5)); }
	inline int32_t get__myPlatform_5() const { return ____myPlatform_5; }
	inline int32_t* get_address_of__myPlatform_5() { return &____myPlatform_5; }
	inline void set__myPlatform_5(int32_t value)
	{
		____myPlatform_5 = value;
	}

	inline static int32_t get_offset_of__runningOnWindows_6() { return static_cast<int32_t>(offsetof(SettingsMappingManager_t1609949884_StaticFields, ____runningOnWindows_6)); }
	inline bool get__runningOnWindows_6() const { return ____runningOnWindows_6; }
	inline bool* get_address_of__runningOnWindows_6() { return &____runningOnWindows_6; }
	inline void set__runningOnWindows_6(bool value)
	{
		____runningOnWindows_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
