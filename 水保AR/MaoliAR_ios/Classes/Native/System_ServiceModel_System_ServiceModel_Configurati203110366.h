﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Configurati338211620.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.StandardBindingCollectionElement`2<System.ServiceModel.NetPeerTcpBinding,System.ServiceModel.Configuration.NetPeerTcpBindingElement>
struct  StandardBindingCollectionElement_2_t203110366  : public BindingCollectionElement_t338211620
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.StandardBindingCollectionElement`2::_properties
	ConfigurationPropertyCollection_t2852175726 * ____properties_13;

public:
	inline static int32_t get_offset_of__properties_13() { return static_cast<int32_t>(offsetof(StandardBindingCollectionElement_2_t203110366, ____properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get__properties_13() const { return ____properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of__properties_13() { return &____properties_13; }
	inline void set__properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		____properties_13 = value;
		Il2CppCodeGenWriteBarrier(&____properties_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
