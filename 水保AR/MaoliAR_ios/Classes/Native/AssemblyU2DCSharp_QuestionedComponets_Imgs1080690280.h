﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"

// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuestionedComponets/Imgs
struct  Imgs_t1080690280 
{
public:
	// UnityEngine.Sprite[] QuestionedComponets/Imgs::BackgroundImgArray_1
	SpriteU5BU5D_t2581906349* ___BackgroundImgArray_1_0;
	// UnityEngine.Sprite[] QuestionedComponets/Imgs::BackgroundImgArray_2
	SpriteU5BU5D_t2581906349* ___BackgroundImgArray_2_1;

public:
	inline static int32_t get_offset_of_BackgroundImgArray_1_0() { return static_cast<int32_t>(offsetof(Imgs_t1080690280, ___BackgroundImgArray_1_0)); }
	inline SpriteU5BU5D_t2581906349* get_BackgroundImgArray_1_0() const { return ___BackgroundImgArray_1_0; }
	inline SpriteU5BU5D_t2581906349** get_address_of_BackgroundImgArray_1_0() { return &___BackgroundImgArray_1_0; }
	inline void set_BackgroundImgArray_1_0(SpriteU5BU5D_t2581906349* value)
	{
		___BackgroundImgArray_1_0 = value;
		Il2CppCodeGenWriteBarrier(&___BackgroundImgArray_1_0, value);
	}

	inline static int32_t get_offset_of_BackgroundImgArray_2_1() { return static_cast<int32_t>(offsetof(Imgs_t1080690280, ___BackgroundImgArray_2_1)); }
	inline SpriteU5BU5D_t2581906349* get_BackgroundImgArray_2_1() const { return ___BackgroundImgArray_2_1; }
	inline SpriteU5BU5D_t2581906349** get_address_of_BackgroundImgArray_2_1() { return &___BackgroundImgArray_2_1; }
	inline void set_BackgroundImgArray_2_1(SpriteU5BU5D_t2581906349* value)
	{
		___BackgroundImgArray_2_1 = value;
		Il2CppCodeGenWriteBarrier(&___BackgroundImgArray_2_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of QuestionedComponets/Imgs
struct Imgs_t1080690280_marshaled_pinvoke
{
	SpriteU5BU5D_t2581906349* ___BackgroundImgArray_1_0;
	SpriteU5BU5D_t2581906349* ___BackgroundImgArray_2_1;
};
// Native definition for COM marshalling of QuestionedComponets/Imgs
struct Imgs_t1080690280_marshaled_com
{
	SpriteU5BU5D_t2581906349* ___BackgroundImgArray_1_0;
	SpriteU5BU5D_t2581906349* ___BackgroundImgArray_2_1;
};
