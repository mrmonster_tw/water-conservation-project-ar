﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_Mono_Security_Protocol_Tls_Con3971234707.h"

// Mono.Security.Protocol.Tls.SslServerStream
struct SslServerStream_t875102505;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.ServerContext
struct  ServerContext_t3848440994  : public Context_t3971234708
{
public:
	// Mono.Security.Protocol.Tls.SslServerStream Mono.Security.Protocol.Tls.ServerContext::sslStream
	SslServerStream_t875102505 * ___sslStream_29;
	// System.Boolean Mono.Security.Protocol.Tls.ServerContext::clientCertificateRequired
	bool ___clientCertificateRequired_30;

public:
	inline static int32_t get_offset_of_sslStream_29() { return static_cast<int32_t>(offsetof(ServerContext_t3848440994, ___sslStream_29)); }
	inline SslServerStream_t875102505 * get_sslStream_29() const { return ___sslStream_29; }
	inline SslServerStream_t875102505 ** get_address_of_sslStream_29() { return &___sslStream_29; }
	inline void set_sslStream_29(SslServerStream_t875102505 * value)
	{
		___sslStream_29 = value;
		Il2CppCodeGenWriteBarrier(&___sslStream_29, value);
	}

	inline static int32_t get_offset_of_clientCertificateRequired_30() { return static_cast<int32_t>(offsetof(ServerContext_t3848440994, ___clientCertificateRequired_30)); }
	inline bool get_clientCertificateRequired_30() const { return ___clientCertificateRequired_30; }
	inline bool* get_address_of_clientCertificateRequired_30() { return &___clientCertificateRequired_30; }
	inline void set_clientCertificateRequired_30(bool value)
	{
		___clientCertificateRequired_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
