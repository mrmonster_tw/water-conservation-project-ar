﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_SessionState_SessionStateStor182923317.h"

// System.Web.Configuration.SessionStateSection
struct SessionStateSection_t500006340;
// System.Web.SessionState.RemoteStateServer
struct RemoteStateServer_t3071674104;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.SessionState.SessionStateServerHandler
struct  SessionStateServerHandler_t103977851  : public SessionStateStoreProviderBase_t182923317
{
public:
	// System.Web.Configuration.SessionStateSection System.Web.SessionState.SessionStateServerHandler::config
	SessionStateSection_t500006340 * ___config_3;
	// System.Web.SessionState.RemoteStateServer System.Web.SessionState.SessionStateServerHandler::stateServer
	RemoteStateServer_t3071674104 * ___stateServer_4;

public:
	inline static int32_t get_offset_of_config_3() { return static_cast<int32_t>(offsetof(SessionStateServerHandler_t103977851, ___config_3)); }
	inline SessionStateSection_t500006340 * get_config_3() const { return ___config_3; }
	inline SessionStateSection_t500006340 ** get_address_of_config_3() { return &___config_3; }
	inline void set_config_3(SessionStateSection_t500006340 * value)
	{
		___config_3 = value;
		Il2CppCodeGenWriteBarrier(&___config_3, value);
	}

	inline static int32_t get_offset_of_stateServer_4() { return static_cast<int32_t>(offsetof(SessionStateServerHandler_t103977851, ___stateServer_4)); }
	inline RemoteStateServer_t3071674104 * get_stateServer_4() const { return ___stateServer_4; }
	inline RemoteStateServer_t3071674104 ** get_address_of_stateServer_4() { return &___stateServer_4; }
	inline void set_stateServer_4(RemoteStateServer_t3071674104 * value)
	{
		___stateServer_4 = value;
		Il2CppCodeGenWriteBarrier(&___stateServer_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
