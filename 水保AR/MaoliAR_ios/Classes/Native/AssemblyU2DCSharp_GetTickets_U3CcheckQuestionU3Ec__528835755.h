﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// GetTickets
struct GetTickets_t911946028;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetTickets/<checkQuestion>c__Iterator2
struct  U3CcheckQuestionU3Ec__Iterator2_t528835755  : public Il2CppObject
{
public:
	// System.String GetTickets/<checkQuestion>c__Iterator2::url
	String_t* ___url_0;
	// System.String[] GetTickets/<checkQuestion>c__Iterator2::<temps>__0
	StringU5BU5D_t1281789340* ___U3CtempsU3E__0_1;
	// System.Boolean GetTickets/<checkQuestion>c__Iterator2::<Questioned>__0
	bool ___U3CQuestionedU3E__0_2;
	// System.Int32 GetTickets/<checkQuestion>c__Iterator2::mode
	int32_t ___mode_3;
	// System.Int32 GetTickets/<checkQuestion>c__Iterator2::number
	int32_t ___number_4;
	// GetTickets GetTickets/<checkQuestion>c__Iterator2::$this
	GetTickets_t911946028 * ___U24this_5;
	// System.Object GetTickets/<checkQuestion>c__Iterator2::$current
	Il2CppObject * ___U24current_6;
	// System.Boolean GetTickets/<checkQuestion>c__Iterator2::$disposing
	bool ___U24disposing_7;
	// System.Int32 GetTickets/<checkQuestion>c__Iterator2::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CcheckQuestionU3Ec__Iterator2_t528835755, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier(&___url_0, value);
	}

	inline static int32_t get_offset_of_U3CtempsU3E__0_1() { return static_cast<int32_t>(offsetof(U3CcheckQuestionU3Ec__Iterator2_t528835755, ___U3CtempsU3E__0_1)); }
	inline StringU5BU5D_t1281789340* get_U3CtempsU3E__0_1() const { return ___U3CtempsU3E__0_1; }
	inline StringU5BU5D_t1281789340** get_address_of_U3CtempsU3E__0_1() { return &___U3CtempsU3E__0_1; }
	inline void set_U3CtempsU3E__0_1(StringU5BU5D_t1281789340* value)
	{
		___U3CtempsU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtempsU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CQuestionedU3E__0_2() { return static_cast<int32_t>(offsetof(U3CcheckQuestionU3Ec__Iterator2_t528835755, ___U3CQuestionedU3E__0_2)); }
	inline bool get_U3CQuestionedU3E__0_2() const { return ___U3CQuestionedU3E__0_2; }
	inline bool* get_address_of_U3CQuestionedU3E__0_2() { return &___U3CQuestionedU3E__0_2; }
	inline void set_U3CQuestionedU3E__0_2(bool value)
	{
		___U3CQuestionedU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_mode_3() { return static_cast<int32_t>(offsetof(U3CcheckQuestionU3Ec__Iterator2_t528835755, ___mode_3)); }
	inline int32_t get_mode_3() const { return ___mode_3; }
	inline int32_t* get_address_of_mode_3() { return &___mode_3; }
	inline void set_mode_3(int32_t value)
	{
		___mode_3 = value;
	}

	inline static int32_t get_offset_of_number_4() { return static_cast<int32_t>(offsetof(U3CcheckQuestionU3Ec__Iterator2_t528835755, ___number_4)); }
	inline int32_t get_number_4() const { return ___number_4; }
	inline int32_t* get_address_of_number_4() { return &___number_4; }
	inline void set_number_4(int32_t value)
	{
		___number_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CcheckQuestionU3Ec__Iterator2_t528835755, ___U24this_5)); }
	inline GetTickets_t911946028 * get_U24this_5() const { return ___U24this_5; }
	inline GetTickets_t911946028 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(GetTickets_t911946028 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_5, value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CcheckQuestionU3Ec__Iterator2_t528835755, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CcheckQuestionU3Ec__Iterator2_t528835755, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CcheckQuestionU3Ec__Iterator2_t528835755, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
