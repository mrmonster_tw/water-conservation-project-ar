﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_Mono_CodeGeneration_CodeExpres2163794278.h"

// Mono.CodeGeneration.CodeExpression
struct CodeExpression_t2163794278;
// Mono.CodeGeneration.CodeExpression[]
struct CodeExpressionU5BU5D_t422010435;
// System.Reflection.MethodBase
struct MethodBase_t609368412;
// Mono.CodeGeneration.CodeMethod
struct CodeMethod_t1546731557;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.CodeGeneration.CodeMethodCall
struct  CodeMethodCall_t3555455132  : public CodeExpression_t2163794278
{
public:
	// Mono.CodeGeneration.CodeExpression Mono.CodeGeneration.CodeMethodCall::target
	CodeExpression_t2163794278 * ___target_0;
	// Mono.CodeGeneration.CodeExpression[] Mono.CodeGeneration.CodeMethodCall::parameters
	CodeExpressionU5BU5D_t422010435* ___parameters_1;
	// System.Reflection.MethodBase Mono.CodeGeneration.CodeMethodCall::method
	MethodBase_t609368412 * ___method_2;
	// Mono.CodeGeneration.CodeMethod Mono.CodeGeneration.CodeMethodCall::codeMethod
	CodeMethod_t1546731557 * ___codeMethod_3;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(CodeMethodCall_t3555455132, ___target_0)); }
	inline CodeExpression_t2163794278 * get_target_0() const { return ___target_0; }
	inline CodeExpression_t2163794278 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(CodeExpression_t2163794278 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier(&___target_0, value);
	}

	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(CodeMethodCall_t3555455132, ___parameters_1)); }
	inline CodeExpressionU5BU5D_t422010435* get_parameters_1() const { return ___parameters_1; }
	inline CodeExpressionU5BU5D_t422010435** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(CodeExpressionU5BU5D_t422010435* value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_1, value);
	}

	inline static int32_t get_offset_of_method_2() { return static_cast<int32_t>(offsetof(CodeMethodCall_t3555455132, ___method_2)); }
	inline MethodBase_t609368412 * get_method_2() const { return ___method_2; }
	inline MethodBase_t609368412 ** get_address_of_method_2() { return &___method_2; }
	inline void set_method_2(MethodBase_t609368412 * value)
	{
		___method_2 = value;
		Il2CppCodeGenWriteBarrier(&___method_2, value);
	}

	inline static int32_t get_offset_of_codeMethod_3() { return static_cast<int32_t>(offsetof(CodeMethodCall_t3555455132, ___codeMethod_3)); }
	inline CodeMethod_t1546731557 * get_codeMethod_3() const { return ___codeMethod_3; }
	inline CodeMethod_t1546731557 ** get_address_of_codeMethod_3() { return &___codeMethod_3; }
	inline void set_codeMethod_3(CodeMethod_t1546731557 * value)
	{
		___codeMethod_3 = value;
		Il2CppCodeGenWriteBarrier(&___codeMethod_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
