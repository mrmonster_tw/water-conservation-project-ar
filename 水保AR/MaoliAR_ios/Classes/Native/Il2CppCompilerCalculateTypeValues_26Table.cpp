﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_Sockets_Socket_SocketAsyncCall1521370843.h"
#include "System_System_Net_Sockets_SocketError3760144386.h"
#include "System_System_Net_Sockets_SocketException3852068672.h"
#include "System_System_Net_Sockets_SocketFlags2969870452.h"
#include "System_System_Net_Sockets_SocketOptionLevel201167901.h"
#include "System_System_Net_Sockets_SocketOptionName403346465.h"
#include "System_System_Net_Sockets_SocketShutdown2687738148.h"
#include "System_System_Net_Sockets_SocketType2175930299.h"
#include "System_System_Net_Sockets_TcpClient822906377.h"
#include "System_System_Net_Sockets_TcpClient_Properties2921633695.h"
#include "System_System_Net_Sockets_TcpListener3499576757.h"
#include "System_System_Net_WebAsyncResult3421962937.h"
#include "System_System_Net_ReadState245281014.h"
#include "System_System_Net_WebConnection3982808322.h"
#include "System_System_Net_WebConnection_AbortHelper1490877826.h"
#include "System_System_Net_WebConnectionData3835660455.h"
#include "System_System_Net_WebConnectionGroup1712379988.h"
#include "System_System_Net_WebConnectionStream2170064850.h"
#include "System_System_Net_WebException3237156354.h"
#include "System_System_Net_WebExceptionStatus1731416715.h"
#include "System_System_Net_WebHeaderCollection1942268960.h"
#include "System_System_Net_WebProxy2016760542.h"
#include "System_System_Net_WebRequest1939381076.h"
#include "System_System_Net_WebResponse229922639.h"
#include "System_System_Security_Authentication_SslProtocols928472600.h"
#include "System_System_Security_Cryptography_AsnDecodeStatus788588755.h"
#include "System_System_Security_Cryptography_AsnEncodedData382354011.h"
#include "System_System_Security_Cryptography_OidCollection4234766844.h"
#include "System_System_Security_Cryptography_Oid3552120260.h"
#include "System_System_Security_Cryptography_OidEnumerator899373898.h"
#include "System_System_Security_Cryptography_X509Certificate968238685.h"
#include "System_Mono_Security_X509_OSX509Certificates2347337007.h"
#include "System_Mono_Security_X509_OSX509Certificates_SecTr1115184440.h"
#include "System_System_Security_Cryptography_X509Certificat3779582684.h"
#include "System_System_Security_Cryptography_X509Certificat2864310644.h"
#include "System_System_Security_Cryptography_X509Certificat1492274484.h"
#include "System_System_Security_Cryptography_X509Certificate875709727.h"
#include "System_System_Security_Cryptography_X509Certificate254051580.h"
#include "System_System_Security_Cryptography_X509Certificat2828968862.h"
#include "System_System_Security_Cryptography_X509Certificat2111161276.h"
#include "System_System_Security_Cryptography_X509Certificate714049126.h"
#include "System_System_Security_Cryptography_X509Certificat3316713812.h"
#include "System_System_Security_Cryptography_X509Certificat3399372417.h"
#include "System_System_Security_Cryptography_X509Certificate855273292.h"
#include "System_System_Security_Cryptography_X509Certificate194917408.h"
#include "System_System_Security_Cryptography_X509Certificat3110968994.h"
#include "System_System_Security_Cryptography_X509Certificat1464056338.h"
#include "System_System_Security_Cryptography_X509Certificat3014253456.h"
#include "System_System_Security_Cryptography_X509Certificat2426922870.h"
#include "System_System_Security_Cryptography_X509Certificate133602714.h"
#include "System_System_Security_Cryptography_X509Certificat1026973125.h"
#include "System_System_Security_Cryptography_X509Certificat4189149453.h"
#include "System_System_Security_Cryptography_X509Certificat1350454579.h"
#include "System_System_Security_Cryptography_X509Certificat3272255153.h"
#include "System_System_Security_Cryptography_X509Certificat2479560659.h"
#include "System_System_Security_Cryptography_X509Certificat3058503971.h"
#include "System_System_Security_Cryptography_X509Certificat2042101591.h"
#include "System_System_Security_Cryptography_X509Certificat1431795504.h"
#include "System_System_Security_Cryptography_X509Certificat2096517340.h"
#include "System_System_Security_Cryptography_X509Certificat2592711905.h"
#include "System_System_Security_Cryptography_X509Certificat2571829933.h"
#include "System_System_Security_Cryptography_X509Certificat2922691911.h"
#include "System_System_Security_Cryptography_X509Certificat4258825542.h"
#include "System_System_Security_Cryptography_X509Certificat3929505454.h"
#include "System_System_Security_Cryptography_X509Certificat2086244306.h"
#include "System_System_SRDescriptionAttribute1210185586.h"
#include "System_System_Text_RegularExpressions_OpCode1565867503.h"
#include "System_System_Text_RegularExpressions_OpFlags23120214.h"
#include "System_System_Text_RegularExpressions_Position2536274344.h"
#include "System_System_Text_RegularExpressions_BaseMachine2554639499.h"
#include "System_System_Text_RegularExpressions_BaseMachine_M281819667.h"
#include "System_System_Text_RegularExpressions_FactoryCache2327118887.h"
#include "System_System_Text_RegularExpressions_FactoryCache2725523001.h"
#include "System_System_Text_RegularExpressions_MRUList4121573800.h"
#include "System_System_Text_RegularExpressions_MRUList_Node2049086415.h"
#include "System_System_Text_RegularExpressions_CaptureColle1760593541.h"
#include "System_System_Text_RegularExpressions_Capture2232016050.h"
#include "System_System_Text_RegularExpressions_CILCompiler3234566002.h"
#include "System_System_Text_RegularExpressions_CILCompiler_3942143166.h"
#include "System_System_Text_RegularExpressions_Category1200126069.h"
#include "System_System_Text_RegularExpressions_CategoryUtil3167997394.h"
#include "System_System_Text_RegularExpressions_LinkRef2971865410.h"
#include "System_System_Text_RegularExpressions_InterpreterFa533216624.h"
#include "System_System_Text_RegularExpressions_PatternCompi4036359803.h"
#include "System_System_Text_RegularExpressions_PatternCompil976787442.h"
#include "System_System_Text_RegularExpressions_PatternCompi3395949159.h"
#include "System_System_Text_RegularExpressions_LinkStack887727776.h"
#include "System_System_Text_RegularExpressions_Mark3471605523.h"
#include "System_System_Text_RegularExpressions_GroupCollectio69770484.h"
#include "System_System_Text_RegularExpressions_Group2468205786.h"
#include "System_System_Text_RegularExpressions_Interpreter582715701.h"
#include "System_System_Text_RegularExpressions_Interpreter_2189327687.h"
#include "System_System_Text_RegularExpressions_Interpreter_1214863076.h"
#include "System_System_Text_RegularExpressions_Interpreter_3692532274.h"
#include "System_System_Text_RegularExpressions_Interval1802865632.h"
#include "System_System_Text_RegularExpressions_IntervalColl2609070824.h"
#include "System_System_Text_RegularExpressions_IntervalColle737725276.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2600 = { sizeof (SocketAsyncCall_t1521370843), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2601 = { sizeof (SocketError_t3760144386)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2601[48] = 
{
	SocketError_t3760144386::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2602 = { sizeof (SocketException_t3852068672), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2603 = { sizeof (SocketFlags_t2969870452)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2603[11] = 
{
	SocketFlags_t2969870452::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2604 = { sizeof (SocketOptionLevel_t201167901)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2604[6] = 
{
	SocketOptionLevel_t201167901::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2605 = { sizeof (SocketOptionName_t403346465)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2605[44] = 
{
	SocketOptionName_t403346465::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2606 = { sizeof (SocketShutdown_t2687738148)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2606[4] = 
{
	SocketShutdown_t2687738148::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2607 = { sizeof (SocketType_t2175930299)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2607[7] = 
{
	SocketType_t2175930299::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2608 = { sizeof (TcpClient_t822906377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2608[7] = 
{
	TcpClient_t822906377::get_offset_of_stream_0(),
	TcpClient_t822906377::get_offset_of_active_1(),
	TcpClient_t822906377::get_offset_of_client_2(),
	TcpClient_t822906377::get_offset_of_disposed_3(),
	TcpClient_t822906377::get_offset_of_values_4(),
	TcpClient_t822906377::get_offset_of_recv_timeout_5(),
	TcpClient_t822906377::get_offset_of_send_timeout_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2609 = { sizeof (Properties_t2921633695)+ sizeof (Il2CppObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2609[7] = 
{
	Properties_t2921633695::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2610 = { sizeof (TcpListener_t3499576757), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2610[3] = 
{
	TcpListener_t3499576757::get_offset_of_active_0(),
	TcpListener_t3499576757::get_offset_of_server_1(),
	TcpListener_t3499576757::get_offset_of_savedEP_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2611 = { sizeof (WebAsyncResult_t3421962937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2611[17] = 
{
	WebAsyncResult_t3421962937::get_offset_of_handle_0(),
	WebAsyncResult_t3421962937::get_offset_of_synch_1(),
	WebAsyncResult_t3421962937::get_offset_of_isCompleted_2(),
	WebAsyncResult_t3421962937::get_offset_of_cb_3(),
	WebAsyncResult_t3421962937::get_offset_of_state_4(),
	WebAsyncResult_t3421962937::get_offset_of_nbytes_5(),
	WebAsyncResult_t3421962937::get_offset_of_innerAsyncResult_6(),
	WebAsyncResult_t3421962937::get_offset_of_callbackDone_7(),
	WebAsyncResult_t3421962937::get_offset_of_exc_8(),
	WebAsyncResult_t3421962937::get_offset_of_response_9(),
	WebAsyncResult_t3421962937::get_offset_of_writeStream_10(),
	WebAsyncResult_t3421962937::get_offset_of_buffer_11(),
	WebAsyncResult_t3421962937::get_offset_of_offset_12(),
	WebAsyncResult_t3421962937::get_offset_of_size_13(),
	WebAsyncResult_t3421962937::get_offset_of_locker_14(),
	WebAsyncResult_t3421962937::get_offset_of_EndCalled_15(),
	WebAsyncResult_t3421962937::get_offset_of_AsyncWriteAll_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2612 = { sizeof (ReadState_t245281014)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2612[5] = 
{
	ReadState_t245281014::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2613 = { sizeof (WebConnection_t3982808322), -1, sizeof(WebConnection_t3982808322_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2613[32] = 
{
	WebConnection_t3982808322::get_offset_of_sPoint_0(),
	WebConnection_t3982808322::get_offset_of_nstream_1(),
	WebConnection_t3982808322::get_offset_of_socket_2(),
	WebConnection_t3982808322::get_offset_of_socketLock_3(),
	WebConnection_t3982808322::get_offset_of_status_4(),
	WebConnection_t3982808322::get_offset_of_initConn_5(),
	WebConnection_t3982808322::get_offset_of_keepAlive_6(),
	WebConnection_t3982808322::get_offset_of_buffer_7(),
	WebConnection_t3982808322_StaticFields::get_offset_of_readDoneDelegate_8(),
	WebConnection_t3982808322::get_offset_of_abortHandler_9(),
	WebConnection_t3982808322::get_offset_of_abortHelper_10(),
	WebConnection_t3982808322::get_offset_of_readState_11(),
	WebConnection_t3982808322::get_offset_of_Data_12(),
	WebConnection_t3982808322::get_offset_of_chunkedRead_13(),
	WebConnection_t3982808322::get_offset_of_chunkStream_14(),
	WebConnection_t3982808322::get_offset_of_queue_15(),
	WebConnection_t3982808322::get_offset_of_reused_16(),
	WebConnection_t3982808322::get_offset_of_position_17(),
	WebConnection_t3982808322::get_offset_of_busy_18(),
	WebConnection_t3982808322::get_offset_of_priority_request_19(),
	WebConnection_t3982808322::get_offset_of_ntlm_credentials_20(),
	WebConnection_t3982808322::get_offset_of_ntlm_authenticated_21(),
	WebConnection_t3982808322::get_offset_of_unsafe_sharing_22(),
	WebConnection_t3982808322::get_offset_of_ssl_23(),
	WebConnection_t3982808322::get_offset_of_certsAvailable_24(),
	WebConnection_t3982808322::get_offset_of_connect_exception_25(),
	WebConnection_t3982808322_StaticFields::get_offset_of_classLock_26(),
	WebConnection_t3982808322_StaticFields::get_offset_of_sslStream_27(),
	WebConnection_t3982808322_StaticFields::get_offset_of_piClient_28(),
	WebConnection_t3982808322_StaticFields::get_offset_of_piServer_29(),
	WebConnection_t3982808322_StaticFields::get_offset_of_piTrustFailure_30(),
	WebConnection_t3982808322_StaticFields::get_offset_of_method_GetSecurityPolicyFromNonMainThread_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2614 = { sizeof (AbortHelper_t1490877826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2614[1] = 
{
	AbortHelper_t1490877826::get_offset_of_Connection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2615 = { sizeof (WebConnectionData_t3835660455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2615[7] = 
{
	WebConnectionData_t3835660455::get_offset_of_request_0(),
	WebConnectionData_t3835660455::get_offset_of_StatusCode_1(),
	WebConnectionData_t3835660455::get_offset_of_StatusDescription_2(),
	WebConnectionData_t3835660455::get_offset_of_Headers_3(),
	WebConnectionData_t3835660455::get_offset_of_Version_4(),
	WebConnectionData_t3835660455::get_offset_of_stream_5(),
	WebConnectionData_t3835660455::get_offset_of_Challenge_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2616 = { sizeof (WebConnectionGroup_t1712379988), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2616[5] = 
{
	WebConnectionGroup_t1712379988::get_offset_of_sPoint_0(),
	WebConnectionGroup_t1712379988::get_offset_of_name_1(),
	WebConnectionGroup_t1712379988::get_offset_of_connections_2(),
	WebConnectionGroup_t1712379988::get_offset_of_rnd_3(),
	WebConnectionGroup_t1712379988::get_offset_of_queue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2617 = { sizeof (WebConnectionStream_t2170064850), -1, sizeof(WebConnectionStream_t2170064850_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2617[27] = 
{
	WebConnectionStream_t2170064850_StaticFields::get_offset_of_crlf_2(),
	WebConnectionStream_t2170064850::get_offset_of_isRead_3(),
	WebConnectionStream_t2170064850::get_offset_of_cnc_4(),
	WebConnectionStream_t2170064850::get_offset_of_request_5(),
	WebConnectionStream_t2170064850::get_offset_of_readBuffer_6(),
	WebConnectionStream_t2170064850::get_offset_of_readBufferOffset_7(),
	WebConnectionStream_t2170064850::get_offset_of_readBufferSize_8(),
	WebConnectionStream_t2170064850::get_offset_of_contentLength_9(),
	WebConnectionStream_t2170064850::get_offset_of_totalRead_10(),
	WebConnectionStream_t2170064850::get_offset_of_totalWritten_11(),
	WebConnectionStream_t2170064850::get_offset_of_nextReadCalled_12(),
	WebConnectionStream_t2170064850::get_offset_of_pendingReads_13(),
	WebConnectionStream_t2170064850::get_offset_of_pendingWrites_14(),
	WebConnectionStream_t2170064850::get_offset_of_pending_15(),
	WebConnectionStream_t2170064850::get_offset_of_allowBuffering_16(),
	WebConnectionStream_t2170064850::get_offset_of_sendChunked_17(),
	WebConnectionStream_t2170064850::get_offset_of_writeBuffer_18(),
	WebConnectionStream_t2170064850::get_offset_of_requestWritten_19(),
	WebConnectionStream_t2170064850::get_offset_of_headers_20(),
	WebConnectionStream_t2170064850::get_offset_of_disposed_21(),
	WebConnectionStream_t2170064850::get_offset_of_headersSent_22(),
	WebConnectionStream_t2170064850::get_offset_of_locker_23(),
	WebConnectionStream_t2170064850::get_offset_of_initRead_24(),
	WebConnectionStream_t2170064850::get_offset_of_read_eof_25(),
	WebConnectionStream_t2170064850::get_offset_of_complete_request_written_26(),
	WebConnectionStream_t2170064850::get_offset_of_read_timeout_27(),
	WebConnectionStream_t2170064850::get_offset_of_write_timeout_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2618 = { sizeof (WebException_t3237156354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2618[2] = 
{
	WebException_t3237156354::get_offset_of_response_12(),
	WebException_t3237156354::get_offset_of_status_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2619 = { sizeof (WebExceptionStatus_t1731416715)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2619[22] = 
{
	WebExceptionStatus_t1731416715::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2620 = { sizeof (WebHeaderCollection_t1942268960), -1, sizeof(WebHeaderCollection_t1942268960_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2620[5] = 
{
	WebHeaderCollection_t1942268960_StaticFields::get_offset_of_restricted_12(),
	WebHeaderCollection_t1942268960_StaticFields::get_offset_of_multiValue_13(),
	WebHeaderCollection_t1942268960_StaticFields::get_offset_of_restricted_response_14(),
	WebHeaderCollection_t1942268960::get_offset_of_internallyCreated_15(),
	WebHeaderCollection_t1942268960_StaticFields::get_offset_of_allowed_chars_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2621 = { sizeof (WebProxy_t2016760542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2621[5] = 
{
	WebProxy_t2016760542::get_offset_of_address_0(),
	WebProxy_t2016760542::get_offset_of_bypassOnLocal_1(),
	WebProxy_t2016760542::get_offset_of_bypassList_2(),
	WebProxy_t2016760542::get_offset_of_credentials_3(),
	WebProxy_t2016760542::get_offset_of_useDefaultCredentials_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2622 = { sizeof (WebRequest_t1939381076), -1, sizeof(WebRequest_t1939381076_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2622[5] = 
{
	WebRequest_t1939381076_StaticFields::get_offset_of_prefixes_1(),
	WebRequest_t1939381076_StaticFields::get_offset_of_isDefaultWebProxySet_2(),
	WebRequest_t1939381076_StaticFields::get_offset_of_defaultWebProxy_3(),
	WebRequest_t1939381076::get_offset_of_authentication_level_4(),
	WebRequest_t1939381076_StaticFields::get_offset_of_lockobj_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2623 = { sizeof (WebResponse_t229922639), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2624 = { sizeof (SslProtocols_t928472600)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2624[6] = 
{
	SslProtocols_t928472600::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2625 = { sizeof (AsnDecodeStatus_t788588755)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2625[7] = 
{
	AsnDecodeStatus_t788588755::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2626 = { sizeof (AsnEncodedData_t382354011), -1, sizeof(AsnEncodedData_t382354011_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2626[3] = 
{
	AsnEncodedData_t382354011::get_offset_of__oid_0(),
	AsnEncodedData_t382354011::get_offset_of__raw_1(),
	AsnEncodedData_t382354011_StaticFields::get_offset_of_U3CU3Ef__switchU24map13_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2627 = { sizeof (OidCollection_t4234766844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2627[2] = 
{
	OidCollection_t4234766844::get_offset_of__list_0(),
	OidCollection_t4234766844::get_offset_of__readOnly_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2628 = { sizeof (Oid_t3552120260), -1, sizeof(Oid_t3552120260_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2628[3] = 
{
	Oid_t3552120260::get_offset_of__value_0(),
	Oid_t3552120260::get_offset_of__name_1(),
	Oid_t3552120260_StaticFields::get_offset_of_U3CU3Ef__switchU24map14_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2629 = { sizeof (OidEnumerator_t899373898), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2629[2] = 
{
	OidEnumerator_t899373898::get_offset_of__collection_0(),
	OidEnumerator_t899373898::get_offset_of__position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2630 = { sizeof (OpenFlags_t968238685)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2630[6] = 
{
	OpenFlags_t968238685::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2631 = { sizeof (OSX509Certificates_t2347337007), -1, sizeof(OSX509Certificates_t2347337007_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2631[1] = 
{
	OSX509Certificates_t2347337007_StaticFields::get_offset_of_sslsecpolicy_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2632 = { sizeof (SecTrustResult_t1115184440)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2632[9] = 
{
	SecTrustResult_t1115184440::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2633 = { sizeof (PublicKey_t3779582684), -1, sizeof(PublicKey_t3779582684_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2633[5] = 
{
	PublicKey_t3779582684::get_offset_of__key_0(),
	PublicKey_t3779582684::get_offset_of__keyValue_1(),
	PublicKey_t3779582684::get_offset_of__params_2(),
	PublicKey_t3779582684::get_offset_of__oid_3(),
	PublicKey_t3779582684_StaticFields::get_offset_of_U3CU3Ef__switchU24map16_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2634 = { sizeof (StoreLocation_t2864310644)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2634[3] = 
{
	StoreLocation_t2864310644::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2635 = { sizeof (StoreName_t1492274484)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2635[9] = 
{
	StoreName_t1492274484::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2636 = { sizeof (X500DistinguishedName_t875709727), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2636[1] = 
{
	X500DistinguishedName_t875709727::get_offset_of_name_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2637 = { sizeof (X500DistinguishedNameFlags_t254051580)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2637[11] = 
{
	X500DistinguishedNameFlags_t254051580::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2638 = { sizeof (X509BasicConstraintsExtension_t2828968862), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2638[6] = 
{
	0,
	0,
	X509BasicConstraintsExtension_t2828968862::get_offset_of__certificateAuthority_6(),
	X509BasicConstraintsExtension_t2828968862::get_offset_of__hasPathLengthConstraint_7(),
	X509BasicConstraintsExtension_t2828968862::get_offset_of__pathLengthConstraint_8(),
	X509BasicConstraintsExtension_t2828968862::get_offset_of__status_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2639 = { sizeof (X509Certificate2Collection_t2111161276), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2640 = { sizeof (X509Certificate2_t714049126), -1, sizeof(X509Certificate2_t714049126_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2640[13] = 
{
	X509Certificate2_t714049126::get_offset_of__archived_5(),
	X509Certificate2_t714049126::get_offset_of__extensions_6(),
	X509Certificate2_t714049126::get_offset_of__name_7(),
	X509Certificate2_t714049126::get_offset_of__serial_8(),
	X509Certificate2_t714049126::get_offset_of__publicKey_9(),
	X509Certificate2_t714049126::get_offset_of_issuer_name_10(),
	X509Certificate2_t714049126::get_offset_of_subject_name_11(),
	X509Certificate2_t714049126::get_offset_of_signature_algorithm_12(),
	X509Certificate2_t714049126::get_offset_of__cert_13(),
	X509Certificate2_t714049126_StaticFields::get_offset_of_empty_error_14(),
	X509Certificate2_t714049126_StaticFields::get_offset_of_commonName_15(),
	X509Certificate2_t714049126_StaticFields::get_offset_of_email_16(),
	X509Certificate2_t714049126_StaticFields::get_offset_of_signedData_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2641 = { sizeof (X509Certificate2Enumerator_t3316713812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2641[1] = 
{
	X509Certificate2Enumerator_t3316713812::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2642 = { sizeof (X509CertificateCollection_t3399372417), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2643 = { sizeof (X509CertificateEnumerator_t855273292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2643[1] = 
{
	X509CertificateEnumerator_t855273292::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2644 = { sizeof (X509Chain_t194917408), -1, sizeof(X509Chain_t194917408_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2644[15] = 
{
	X509Chain_t194917408::get_offset_of_location_0(),
	X509Chain_t194917408::get_offset_of_elements_1(),
	X509Chain_t194917408::get_offset_of_policy_2(),
	X509Chain_t194917408::get_offset_of_status_3(),
	X509Chain_t194917408_StaticFields::get_offset_of_Empty_4(),
	X509Chain_t194917408::get_offset_of_max_path_length_5(),
	X509Chain_t194917408::get_offset_of_working_issuer_name_6(),
	X509Chain_t194917408::get_offset_of_working_public_key_7(),
	X509Chain_t194917408::get_offset_of_bce_restriction_8(),
	X509Chain_t194917408::get_offset_of_roots_9(),
	X509Chain_t194917408::get_offset_of_cas_10(),
	X509Chain_t194917408::get_offset_of_collection_11(),
	X509Chain_t194917408_StaticFields::get_offset_of_U3CU3Ef__switchU24map17_12(),
	X509Chain_t194917408_StaticFields::get_offset_of_U3CU3Ef__switchU24map18_13(),
	X509Chain_t194917408_StaticFields::get_offset_of_U3CU3Ef__switchU24map19_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2645 = { sizeof (X509ChainElementCollection_t3110968994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2645[1] = 
{
	X509ChainElementCollection_t3110968994::get_offset_of__list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2646 = { sizeof (X509ChainElement_t1464056338), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2646[4] = 
{
	X509ChainElement_t1464056338::get_offset_of_certificate_0(),
	X509ChainElement_t1464056338::get_offset_of_status_1(),
	X509ChainElement_t1464056338::get_offset_of_info_2(),
	X509ChainElement_t1464056338::get_offset_of_compressed_status_flags_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2647 = { sizeof (X509ChainElementEnumerator_t3014253456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2647[1] = 
{
	X509ChainElementEnumerator_t3014253456::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2648 = { sizeof (X509ChainPolicy_t2426922870), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2648[8] = 
{
	X509ChainPolicy_t2426922870::get_offset_of_apps_0(),
	X509ChainPolicy_t2426922870::get_offset_of_cert_1(),
	X509ChainPolicy_t2426922870::get_offset_of_store_2(),
	X509ChainPolicy_t2426922870::get_offset_of_rflag_3(),
	X509ChainPolicy_t2426922870::get_offset_of_mode_4(),
	X509ChainPolicy_t2426922870::get_offset_of_timeout_5(),
	X509ChainPolicy_t2426922870::get_offset_of_vflags_6(),
	X509ChainPolicy_t2426922870::get_offset_of_vtime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2649 = { sizeof (X509ChainStatus_t133602714)+ sizeof (Il2CppObject), sizeof(X509ChainStatus_t133602714_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2649[2] = 
{
	X509ChainStatus_t133602714::get_offset_of_status_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	X509ChainStatus_t133602714::get_offset_of_info_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2650 = { sizeof (X509ChainStatusFlags_t1026973125)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2650[24] = 
{
	X509ChainStatusFlags_t1026973125::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2651 = { sizeof (X509EnhancedKeyUsageExtension_t4189149453), -1, sizeof(X509EnhancedKeyUsageExtension_t4189149453_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2651[3] = 
{
	X509EnhancedKeyUsageExtension_t4189149453::get_offset_of__enhKeyUsage_4(),
	X509EnhancedKeyUsageExtension_t4189149453::get_offset_of__status_5(),
	X509EnhancedKeyUsageExtension_t4189149453_StaticFields::get_offset_of_U3CU3Ef__switchU24map1A_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2652 = { sizeof (X509ExtensionCollection_t1350454579), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2652[1] = 
{
	X509ExtensionCollection_t1350454579::get_offset_of__list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2653 = { sizeof (X509Extension_t3272255153), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2653[1] = 
{
	X509Extension_t3272255153::get_offset_of__critical_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2654 = { sizeof (X509ExtensionEnumerator_t2479560659), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2654[1] = 
{
	X509ExtensionEnumerator_t2479560659::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2655 = { sizeof (X509FindType_t3058503971)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2655[16] = 
{
	X509FindType_t3058503971::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2656 = { sizeof (X509KeyUsageExtension_t2042101591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2656[5] = 
{
	0,
	0,
	0,
	X509KeyUsageExtension_t2042101591::get_offset_of__keyUsages_7(),
	X509KeyUsageExtension_t2042101591::get_offset_of__status_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2657 = { sizeof (X509KeyUsageFlags_t1431795504)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2657[11] = 
{
	X509KeyUsageFlags_t1431795504::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2658 = { sizeof (X509NameType_t2096517340)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2658[7] = 
{
	X509NameType_t2096517340::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2659 = { sizeof (X509RevocationFlag_t2592711905)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2659[4] = 
{
	X509RevocationFlag_t2592711905::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2660 = { sizeof (X509RevocationMode_t2571829933)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2660[4] = 
{
	X509RevocationMode_t2571829933::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2661 = { sizeof (X509Store_t2922691911), -1, sizeof(X509Store_t2922691911_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2661[6] = 
{
	X509Store_t2922691911::get_offset_of__name_0(),
	X509Store_t2922691911::get_offset_of__location_1(),
	X509Store_t2922691911::get_offset_of_list_2(),
	X509Store_t2922691911::get_offset_of__flags_3(),
	X509Store_t2922691911::get_offset_of_store_4(),
	X509Store_t2922691911_StaticFields::get_offset_of_U3CU3Ef__switchU24map1B_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2662 = { sizeof (X509SubjectKeyIdentifierExtension_t4258825542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2662[5] = 
{
	0,
	0,
	X509SubjectKeyIdentifierExtension_t4258825542::get_offset_of__subjectKeyIdentifier_6(),
	X509SubjectKeyIdentifierExtension_t4258825542::get_offset_of__ski_7(),
	X509SubjectKeyIdentifierExtension_t4258825542::get_offset_of__status_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2663 = { sizeof (X509SubjectKeyIdentifierHashAlgorithm_t3929505454)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2663[4] = 
{
	X509SubjectKeyIdentifierHashAlgorithm_t3929505454::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2664 = { sizeof (X509VerificationFlags_t2086244306)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2664[15] = 
{
	X509VerificationFlags_t2086244306::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2665 = { sizeof (SRDescriptionAttribute_t1210185586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2665[1] = 
{
	SRDescriptionAttribute_t1210185586::get_offset_of_isReplaced_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2666 = { sizeof (OpCode_t1565867503)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2666[26] = 
{
	OpCode_t1565867503::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2667 = { sizeof (OpFlags_t23120214)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2667[6] = 
{
	OpFlags_t23120214::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2668 = { sizeof (Position_t2536274344)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2668[11] = 
{
	Position_t2536274344::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2669 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2670 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2671 = { sizeof (BaseMachine_t2554639499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2671[1] = 
{
	BaseMachine_t2554639499::get_offset_of_needs_groups_or_captures_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2672 = { sizeof (MatchAppendEvaluator_t281819667), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2673 = { sizeof (FactoryCache_t2327118887), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2673[3] = 
{
	FactoryCache_t2327118887::get_offset_of_capacity_0(),
	FactoryCache_t2327118887::get_offset_of_factories_1(),
	FactoryCache_t2327118887::get_offset_of_mru_list_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2674 = { sizeof (Key_t2725523001), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2674[2] = 
{
	Key_t2725523001::get_offset_of_pattern_0(),
	Key_t2725523001::get_offset_of_options_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2675 = { sizeof (MRUList_t4121573800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2675[2] = 
{
	MRUList_t4121573800::get_offset_of_head_0(),
	MRUList_t4121573800::get_offset_of_tail_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2676 = { sizeof (Node_t2049086415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2676[3] = 
{
	Node_t2049086415::get_offset_of_value_0(),
	Node_t2049086415::get_offset_of_previous_1(),
	Node_t2049086415::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2677 = { sizeof (CaptureCollection_t1760593541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2677[1] = 
{
	CaptureCollection_t1760593541::get_offset_of_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2678 = { sizeof (Capture_t2232016050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2678[3] = 
{
	Capture_t2232016050::get_offset_of_index_0(),
	Capture_t2232016050::get_offset_of_length_1(),
	Capture_t2232016050::get_offset_of_text_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2679 = { sizeof (CILCompiler_t3234566002), -1, sizeof(CILCompiler_t3234566002_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2679[32] = 
{
	CILCompiler_t3234566002::get_offset_of_eval_methods_2(),
	CILCompiler_t3234566002::get_offset_of_eval_methods_defined_3(),
	CILCompiler_t3234566002::get_offset_of_generic_ops_4(),
	CILCompiler_t3234566002::get_offset_of_op_flags_5(),
	CILCompiler_t3234566002::get_offset_of_labels_6(),
	CILCompiler_t3234566002_StaticFields::get_offset_of_fi_str_7(),
	CILCompiler_t3234566002_StaticFields::get_offset_of_fi_string_start_8(),
	CILCompiler_t3234566002_StaticFields::get_offset_of_fi_string_end_9(),
	CILCompiler_t3234566002_StaticFields::get_offset_of_fi_program_10(),
	CILCompiler_t3234566002_StaticFields::get_offset_of_fi_marks_11(),
	CILCompiler_t3234566002_StaticFields::get_offset_of_fi_groups_12(),
	CILCompiler_t3234566002_StaticFields::get_offset_of_fi_deep_13(),
	CILCompiler_t3234566002_StaticFields::get_offset_of_fi_stack_14(),
	CILCompiler_t3234566002_StaticFields::get_offset_of_fi_mark_start_15(),
	CILCompiler_t3234566002_StaticFields::get_offset_of_fi_mark_end_16(),
	CILCompiler_t3234566002_StaticFields::get_offset_of_fi_mark_index_17(),
	CILCompiler_t3234566002_StaticFields::get_offset_of_mi_stack_get_count_18(),
	CILCompiler_t3234566002_StaticFields::get_offset_of_mi_stack_set_count_19(),
	CILCompiler_t3234566002_StaticFields::get_offset_of_mi_stack_push_20(),
	CILCompiler_t3234566002_StaticFields::get_offset_of_mi_stack_pop_21(),
	CILCompiler_t3234566002_StaticFields::get_offset_of_mi_set_start_of_match_22(),
	CILCompiler_t3234566002_StaticFields::get_offset_of_mi_is_word_char_23(),
	CILCompiler_t3234566002_StaticFields::get_offset_of_mi_reset_groups_24(),
	CILCompiler_t3234566002_StaticFields::get_offset_of_mi_checkpoint_25(),
	CILCompiler_t3234566002_StaticFields::get_offset_of_mi_backtrack_26(),
	CILCompiler_t3234566002_StaticFields::get_offset_of_mi_open_27(),
	CILCompiler_t3234566002_StaticFields::get_offset_of_mi_close_28(),
	CILCompiler_t3234566002_StaticFields::get_offset_of_mi_get_last_defined_29(),
	CILCompiler_t3234566002_StaticFields::get_offset_of_mi_mark_get_index_30(),
	CILCompiler_t3234566002_StaticFields::get_offset_of_mi_mark_get_length_31(),
	CILCompiler_t3234566002_StaticFields::get_offset_of_trace_compile_32(),
	CILCompiler_t3234566002::get_offset_of_local_textinfo_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2680 = { sizeof (Frame_t3942143166), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2680[2] = 
{
	Frame_t3942143166::get_offset_of_label_pass_0(),
	Frame_t3942143166::get_offset_of_label_fail_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2681 = { sizeof (Category_t1200126069)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2681[146] = 
{
	Category_t1200126069::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2682 = { sizeof (CategoryUtils_t3167997394), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2683 = { sizeof (LinkRef_t2971865410), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2684 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2685 = { sizeof (InterpreterFactory_t533216624), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2685[4] = 
{
	InterpreterFactory_t533216624::get_offset_of_mapping_0(),
	InterpreterFactory_t533216624::get_offset_of_pattern_1(),
	InterpreterFactory_t533216624::get_offset_of_namesMapping_2(),
	InterpreterFactory_t533216624::get_offset_of_gap_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2686 = { sizeof (PatternCompiler_t4036359803), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2686[1] = 
{
	PatternCompiler_t4036359803::get_offset_of_pgm_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2687 = { sizeof (PatternLinkStack_t976787442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2687[1] = 
{
	PatternLinkStack_t976787442::get_offset_of_link_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2688 = { sizeof (Link_t3395949159)+ sizeof (Il2CppObject), sizeof(Link_t3395949159 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2688[2] = 
{
	Link_t3395949159::get_offset_of_base_addr_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Link_t3395949159::get_offset_of_offset_addr_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2689 = { sizeof (LinkStack_t887727776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2689[1] = 
{
	LinkStack_t887727776::get_offset_of_stack_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2690 = { sizeof (Mark_t3471605523)+ sizeof (Il2CppObject), sizeof(Mark_t3471605523 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2690[3] = 
{
	Mark_t3471605523::get_offset_of_Start_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Mark_t3471605523::get_offset_of_End_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Mark_t3471605523::get_offset_of_Previous_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2691 = { sizeof (GroupCollection_t69770484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2691[2] = 
{
	GroupCollection_t69770484::get_offset_of_list_0(),
	GroupCollection_t69770484::get_offset_of_gap_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2692 = { sizeof (Group_t2468205786), -1, sizeof(Group_t2468205786_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2692[3] = 
{
	Group_t2468205786_StaticFields::get_offset_of_Fail_3(),
	Group_t2468205786::get_offset_of_success_4(),
	Group_t2468205786::get_offset_of_captures_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2693 = { sizeof (Interpreter_t582715701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2693[16] = 
{
	Interpreter_t582715701::get_offset_of_program_1(),
	Interpreter_t582715701::get_offset_of_program_start_2(),
	Interpreter_t582715701::get_offset_of_text_3(),
	Interpreter_t582715701::get_offset_of_text_end_4(),
	Interpreter_t582715701::get_offset_of_group_count_5(),
	Interpreter_t582715701::get_offset_of_match_min_6(),
	Interpreter_t582715701::get_offset_of_qs_7(),
	Interpreter_t582715701::get_offset_of_scan_ptr_8(),
	Interpreter_t582715701::get_offset_of_repeat_9(),
	Interpreter_t582715701::get_offset_of_fast_10(),
	Interpreter_t582715701::get_offset_of_stack_11(),
	Interpreter_t582715701::get_offset_of_deep_12(),
	Interpreter_t582715701::get_offset_of_marks_13(),
	Interpreter_t582715701::get_offset_of_mark_start_14(),
	Interpreter_t582715701::get_offset_of_mark_end_15(),
	Interpreter_t582715701::get_offset_of_groups_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2694 = { sizeof (IntStack_t2189327687)+ sizeof (Il2CppObject), sizeof(IntStack_t2189327687_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2694[2] = 
{
	IntStack_t2189327687::get_offset_of_values_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IntStack_t2189327687::get_offset_of_count_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2695 = { sizeof (RepeatContext_t1214863076), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2695[7] = 
{
	RepeatContext_t1214863076::get_offset_of_start_0(),
	RepeatContext_t1214863076::get_offset_of_min_1(),
	RepeatContext_t1214863076::get_offset_of_max_2(),
	RepeatContext_t1214863076::get_offset_of_lazy_3(),
	RepeatContext_t1214863076::get_offset_of_expr_pc_4(),
	RepeatContext_t1214863076::get_offset_of_previous_5(),
	RepeatContext_t1214863076::get_offset_of_count_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2696 = { sizeof (Mode_t3692532274)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2696[4] = 
{
	Mode_t3692532274::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2697 = { sizeof (Interval_t1802865632)+ sizeof (Il2CppObject), sizeof(Interval_t1802865632_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2697[3] = 
{
	Interval_t1802865632::get_offset_of_low_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Interval_t1802865632::get_offset_of_high_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Interval_t1802865632::get_offset_of_contiguous_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2698 = { sizeof (IntervalCollection_t2609070824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2698[1] = 
{
	IntervalCollection_t2609070824::get_offset_of_intervals_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2699 = { sizeof (Enumerator_t737725276), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2699[2] = 
{
	Enumerator_t737725276::get_offset_of_list_0(),
	Enumerator_t737725276::get_offset_of_ptr_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
