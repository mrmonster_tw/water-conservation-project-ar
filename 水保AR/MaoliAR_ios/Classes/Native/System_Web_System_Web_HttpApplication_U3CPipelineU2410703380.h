﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Delegate
struct Delegate_t1188392813;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.Web.IHttpHandler
struct IHttpHandler_t2624218893;
// System.IO.FileNotFoundException
struct FileNotFoundException_t225391025;
// System.IO.DirectoryNotFoundException
struct DirectoryNotFoundException_t1220582502;
// System.Exception
struct Exception_t1436737249;
// System.Web.IHttpAsyncHandler
struct IHttpAsyncHandler_t2838010551;
// System.Object
struct Il2CppObject;
// System.Web.HttpApplication
struct HttpApplication_t3359645917;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpApplication/<Pipeline>c__Iterator2
struct  U3CPipelineU3Ec__Iterator2_t2410703380  : public Il2CppObject
{
public:
	// System.Delegate System.Web.HttpApplication/<Pipeline>c__Iterator2::<eventHandler>__0
	Delegate_t1188392813 * ___U3CeventHandlerU3E__0_0;
	// System.Collections.IEnumerator System.Web.HttpApplication/<Pipeline>c__Iterator2::<$s_357>__1
	Il2CppObject * ___U3CU24s_357U3E__1_1;
	// System.Boolean System.Web.HttpApplication/<Pipeline>c__Iterator2::<stop>__2
	bool ___U3CstopU3E__2_2;
	// System.Collections.IEnumerator System.Web.HttpApplication/<Pipeline>c__Iterator2::<$s_358>__3
	Il2CppObject * ___U3CU24s_358U3E__3_3;
	// System.Boolean System.Web.HttpApplication/<Pipeline>c__Iterator2::<stop>__4
	bool ___U3CstopU3E__4_4;
	// System.Collections.IEnumerator System.Web.HttpApplication/<Pipeline>c__Iterator2::<$s_359>__5
	Il2CppObject * ___U3CU24s_359U3E__5_5;
	// System.Boolean System.Web.HttpApplication/<Pipeline>c__Iterator2::<stop>__6
	bool ___U3CstopU3E__6_6;
	// System.Collections.IEnumerator System.Web.HttpApplication/<Pipeline>c__Iterator2::<$s_360>__7
	Il2CppObject * ___U3CU24s_360U3E__7_7;
	// System.Boolean System.Web.HttpApplication/<Pipeline>c__Iterator2::<stop>__8
	bool ___U3CstopU3E__8_8;
	// System.Collections.IEnumerator System.Web.HttpApplication/<Pipeline>c__Iterator2::<$s_361>__9
	Il2CppObject * ___U3CU24s_361U3E__9_9;
	// System.Boolean System.Web.HttpApplication/<Pipeline>c__Iterator2::<stop>__10
	bool ___U3CstopU3E__10_10;
	// System.Collections.IEnumerator System.Web.HttpApplication/<Pipeline>c__Iterator2::<$s_362>__11
	Il2CppObject * ___U3CU24s_362U3E__11_11;
	// System.Boolean System.Web.HttpApplication/<Pipeline>c__Iterator2::<stop>__12
	bool ___U3CstopU3E__12_12;
	// System.Collections.IEnumerator System.Web.HttpApplication/<Pipeline>c__Iterator2::<$s_363>__13
	Il2CppObject * ___U3CU24s_363U3E__13_13;
	// System.Boolean System.Web.HttpApplication/<Pipeline>c__Iterator2::<stop>__14
	bool ___U3CstopU3E__14_14;
	// System.Collections.IEnumerator System.Web.HttpApplication/<Pipeline>c__Iterator2::<$s_364>__15
	Il2CppObject * ___U3CU24s_364U3E__15_15;
	// System.Boolean System.Web.HttpApplication/<Pipeline>c__Iterator2::<stop>__16
	bool ___U3CstopU3E__16_16;
	// System.Collections.IEnumerator System.Web.HttpApplication/<Pipeline>c__Iterator2::<$s_365>__17
	Il2CppObject * ___U3CU24s_365U3E__17_17;
	// System.Boolean System.Web.HttpApplication/<Pipeline>c__Iterator2::<stop>__18
	bool ___U3CstopU3E__18_18;
	// System.Web.IHttpHandler System.Web.HttpApplication/<Pipeline>c__Iterator2::<handler>__19
	Il2CppObject * ___U3ChandlerU3E__19_19;
	// System.IO.FileNotFoundException System.Web.HttpApplication/<Pipeline>c__Iterator2::<fnf>__20
	FileNotFoundException_t225391025 * ___U3CfnfU3E__20_20;
	// System.IO.DirectoryNotFoundException System.Web.HttpApplication/<Pipeline>c__Iterator2::<dnf>__21
	DirectoryNotFoundException_t1220582502 * ___U3CdnfU3E__21_21;
	// System.Exception System.Web.HttpApplication/<Pipeline>c__Iterator2::<e>__22
	Exception_t1436737249 * ___U3CeU3E__22_22;
	// System.Collections.IEnumerator System.Web.HttpApplication/<Pipeline>c__Iterator2::<$s_366>__23
	Il2CppObject * ___U3CU24s_366U3E__23_23;
	// System.Boolean System.Web.HttpApplication/<Pipeline>c__Iterator2::<stop>__24
	bool ___U3CstopU3E__24_24;
	// System.Collections.IEnumerator System.Web.HttpApplication/<Pipeline>c__Iterator2::<$s_367>__25
	Il2CppObject * ___U3CU24s_367U3E__25_25;
	// System.Boolean System.Web.HttpApplication/<Pipeline>c__Iterator2::<stop>__26
	bool ___U3CstopU3E__26_26;
	// System.Collections.IEnumerator System.Web.HttpApplication/<Pipeline>c__Iterator2::<$s_368>__27
	Il2CppObject * ___U3CU24s_368U3E__27_27;
	// System.Boolean System.Web.HttpApplication/<Pipeline>c__Iterator2::<stop>__28
	bool ___U3CstopU3E__28_28;
	// System.Collections.IEnumerator System.Web.HttpApplication/<Pipeline>c__Iterator2::<$s_369>__29
	Il2CppObject * ___U3CU24s_369U3E__29_29;
	// System.Boolean System.Web.HttpApplication/<Pipeline>c__Iterator2::<stop>__30
	bool ___U3CstopU3E__30_30;
	// System.Web.IHttpHandler System.Web.HttpApplication/<Pipeline>c__Iterator2::<ctxHandler>__31
	Il2CppObject * ___U3CctxHandlerU3E__31_31;
	// System.Web.IHttpAsyncHandler System.Web.HttpApplication/<Pipeline>c__Iterator2::<async_handler>__32
	Il2CppObject * ___U3Casync_handlerU3E__32_32;
	// System.Collections.IEnumerator System.Web.HttpApplication/<Pipeline>c__Iterator2::<$s_370>__33
	Il2CppObject * ___U3CU24s_370U3E__33_33;
	// System.Boolean System.Web.HttpApplication/<Pipeline>c__Iterator2::<stop>__34
	bool ___U3CstopU3E__34_34;
	// System.Collections.IEnumerator System.Web.HttpApplication/<Pipeline>c__Iterator2::<$s_371>__35
	Il2CppObject * ___U3CU24s_371U3E__35_35;
	// System.Boolean System.Web.HttpApplication/<Pipeline>c__Iterator2::<stop>__36
	bool ___U3CstopU3E__36_36;
	// System.Collections.IEnumerator System.Web.HttpApplication/<Pipeline>c__Iterator2::<$s_372>__37
	Il2CppObject * ___U3CU24s_372U3E__37_37;
	// System.Boolean System.Web.HttpApplication/<Pipeline>c__Iterator2::<stop>__38
	bool ___U3CstopU3E__38_38;
	// System.Collections.IEnumerator System.Web.HttpApplication/<Pipeline>c__Iterator2::<$s_373>__39
	Il2CppObject * ___U3CU24s_373U3E__39_39;
	// System.Boolean System.Web.HttpApplication/<Pipeline>c__Iterator2::<stop>__40
	bool ___U3CstopU3E__40_40;
	// System.Collections.IEnumerator System.Web.HttpApplication/<Pipeline>c__Iterator2::<$s_374>__41
	Il2CppObject * ___U3CU24s_374U3E__41_41;
	// System.Boolean System.Web.HttpApplication/<Pipeline>c__Iterator2::<stop>__42
	bool ___U3CstopU3E__42_42;
	// System.Collections.IEnumerator System.Web.HttpApplication/<Pipeline>c__Iterator2::<$s_375>__43
	Il2CppObject * ___U3CU24s_375U3E__43_43;
	// System.Boolean System.Web.HttpApplication/<Pipeline>c__Iterator2::<stop>__44
	bool ___U3CstopU3E__44_44;
	// System.Collections.IEnumerator System.Web.HttpApplication/<Pipeline>c__Iterator2::<$s_376>__45
	Il2CppObject * ___U3CU24s_376U3E__45_45;
	// System.Boolean System.Web.HttpApplication/<Pipeline>c__Iterator2::<stop>__46
	bool ___U3CstopU3E__46_46;
	// System.Int32 System.Web.HttpApplication/<Pipeline>c__Iterator2::$PC
	int32_t ___U24PC_47;
	// System.Object System.Web.HttpApplication/<Pipeline>c__Iterator2::$current
	Il2CppObject * ___U24current_48;
	// System.Web.HttpApplication System.Web.HttpApplication/<Pipeline>c__Iterator2::<>f__this
	HttpApplication_t3359645917 * ___U3CU3Ef__this_49;

public:
	inline static int32_t get_offset_of_U3CeventHandlerU3E__0_0() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CeventHandlerU3E__0_0)); }
	inline Delegate_t1188392813 * get_U3CeventHandlerU3E__0_0() const { return ___U3CeventHandlerU3E__0_0; }
	inline Delegate_t1188392813 ** get_address_of_U3CeventHandlerU3E__0_0() { return &___U3CeventHandlerU3E__0_0; }
	inline void set_U3CeventHandlerU3E__0_0(Delegate_t1188392813 * value)
	{
		___U3CeventHandlerU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CeventHandlerU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CU24s_357U3E__1_1() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CU24s_357U3E__1_1)); }
	inline Il2CppObject * get_U3CU24s_357U3E__1_1() const { return ___U3CU24s_357U3E__1_1; }
	inline Il2CppObject ** get_address_of_U3CU24s_357U3E__1_1() { return &___U3CU24s_357U3E__1_1; }
	inline void set_U3CU24s_357U3E__1_1(Il2CppObject * value)
	{
		___U3CU24s_357U3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_357U3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CstopU3E__2_2() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CstopU3E__2_2)); }
	inline bool get_U3CstopU3E__2_2() const { return ___U3CstopU3E__2_2; }
	inline bool* get_address_of_U3CstopU3E__2_2() { return &___U3CstopU3E__2_2; }
	inline void set_U3CstopU3E__2_2(bool value)
	{
		___U3CstopU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CU24s_358U3E__3_3() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CU24s_358U3E__3_3)); }
	inline Il2CppObject * get_U3CU24s_358U3E__3_3() const { return ___U3CU24s_358U3E__3_3; }
	inline Il2CppObject ** get_address_of_U3CU24s_358U3E__3_3() { return &___U3CU24s_358U3E__3_3; }
	inline void set_U3CU24s_358U3E__3_3(Il2CppObject * value)
	{
		___U3CU24s_358U3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_358U3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CstopU3E__4_4() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CstopU3E__4_4)); }
	inline bool get_U3CstopU3E__4_4() const { return ___U3CstopU3E__4_4; }
	inline bool* get_address_of_U3CstopU3E__4_4() { return &___U3CstopU3E__4_4; }
	inline void set_U3CstopU3E__4_4(bool value)
	{
		___U3CstopU3E__4_4 = value;
	}

	inline static int32_t get_offset_of_U3CU24s_359U3E__5_5() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CU24s_359U3E__5_5)); }
	inline Il2CppObject * get_U3CU24s_359U3E__5_5() const { return ___U3CU24s_359U3E__5_5; }
	inline Il2CppObject ** get_address_of_U3CU24s_359U3E__5_5() { return &___U3CU24s_359U3E__5_5; }
	inline void set_U3CU24s_359U3E__5_5(Il2CppObject * value)
	{
		___U3CU24s_359U3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_359U3E__5_5, value);
	}

	inline static int32_t get_offset_of_U3CstopU3E__6_6() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CstopU3E__6_6)); }
	inline bool get_U3CstopU3E__6_6() const { return ___U3CstopU3E__6_6; }
	inline bool* get_address_of_U3CstopU3E__6_6() { return &___U3CstopU3E__6_6; }
	inline void set_U3CstopU3E__6_6(bool value)
	{
		___U3CstopU3E__6_6 = value;
	}

	inline static int32_t get_offset_of_U3CU24s_360U3E__7_7() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CU24s_360U3E__7_7)); }
	inline Il2CppObject * get_U3CU24s_360U3E__7_7() const { return ___U3CU24s_360U3E__7_7; }
	inline Il2CppObject ** get_address_of_U3CU24s_360U3E__7_7() { return &___U3CU24s_360U3E__7_7; }
	inline void set_U3CU24s_360U3E__7_7(Il2CppObject * value)
	{
		___U3CU24s_360U3E__7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_360U3E__7_7, value);
	}

	inline static int32_t get_offset_of_U3CstopU3E__8_8() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CstopU3E__8_8)); }
	inline bool get_U3CstopU3E__8_8() const { return ___U3CstopU3E__8_8; }
	inline bool* get_address_of_U3CstopU3E__8_8() { return &___U3CstopU3E__8_8; }
	inline void set_U3CstopU3E__8_8(bool value)
	{
		___U3CstopU3E__8_8 = value;
	}

	inline static int32_t get_offset_of_U3CU24s_361U3E__9_9() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CU24s_361U3E__9_9)); }
	inline Il2CppObject * get_U3CU24s_361U3E__9_9() const { return ___U3CU24s_361U3E__9_9; }
	inline Il2CppObject ** get_address_of_U3CU24s_361U3E__9_9() { return &___U3CU24s_361U3E__9_9; }
	inline void set_U3CU24s_361U3E__9_9(Il2CppObject * value)
	{
		___U3CU24s_361U3E__9_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_361U3E__9_9, value);
	}

	inline static int32_t get_offset_of_U3CstopU3E__10_10() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CstopU3E__10_10)); }
	inline bool get_U3CstopU3E__10_10() const { return ___U3CstopU3E__10_10; }
	inline bool* get_address_of_U3CstopU3E__10_10() { return &___U3CstopU3E__10_10; }
	inline void set_U3CstopU3E__10_10(bool value)
	{
		___U3CstopU3E__10_10 = value;
	}

	inline static int32_t get_offset_of_U3CU24s_362U3E__11_11() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CU24s_362U3E__11_11)); }
	inline Il2CppObject * get_U3CU24s_362U3E__11_11() const { return ___U3CU24s_362U3E__11_11; }
	inline Il2CppObject ** get_address_of_U3CU24s_362U3E__11_11() { return &___U3CU24s_362U3E__11_11; }
	inline void set_U3CU24s_362U3E__11_11(Il2CppObject * value)
	{
		___U3CU24s_362U3E__11_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_362U3E__11_11, value);
	}

	inline static int32_t get_offset_of_U3CstopU3E__12_12() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CstopU3E__12_12)); }
	inline bool get_U3CstopU3E__12_12() const { return ___U3CstopU3E__12_12; }
	inline bool* get_address_of_U3CstopU3E__12_12() { return &___U3CstopU3E__12_12; }
	inline void set_U3CstopU3E__12_12(bool value)
	{
		___U3CstopU3E__12_12 = value;
	}

	inline static int32_t get_offset_of_U3CU24s_363U3E__13_13() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CU24s_363U3E__13_13)); }
	inline Il2CppObject * get_U3CU24s_363U3E__13_13() const { return ___U3CU24s_363U3E__13_13; }
	inline Il2CppObject ** get_address_of_U3CU24s_363U3E__13_13() { return &___U3CU24s_363U3E__13_13; }
	inline void set_U3CU24s_363U3E__13_13(Il2CppObject * value)
	{
		___U3CU24s_363U3E__13_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_363U3E__13_13, value);
	}

	inline static int32_t get_offset_of_U3CstopU3E__14_14() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CstopU3E__14_14)); }
	inline bool get_U3CstopU3E__14_14() const { return ___U3CstopU3E__14_14; }
	inline bool* get_address_of_U3CstopU3E__14_14() { return &___U3CstopU3E__14_14; }
	inline void set_U3CstopU3E__14_14(bool value)
	{
		___U3CstopU3E__14_14 = value;
	}

	inline static int32_t get_offset_of_U3CU24s_364U3E__15_15() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CU24s_364U3E__15_15)); }
	inline Il2CppObject * get_U3CU24s_364U3E__15_15() const { return ___U3CU24s_364U3E__15_15; }
	inline Il2CppObject ** get_address_of_U3CU24s_364U3E__15_15() { return &___U3CU24s_364U3E__15_15; }
	inline void set_U3CU24s_364U3E__15_15(Il2CppObject * value)
	{
		___U3CU24s_364U3E__15_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_364U3E__15_15, value);
	}

	inline static int32_t get_offset_of_U3CstopU3E__16_16() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CstopU3E__16_16)); }
	inline bool get_U3CstopU3E__16_16() const { return ___U3CstopU3E__16_16; }
	inline bool* get_address_of_U3CstopU3E__16_16() { return &___U3CstopU3E__16_16; }
	inline void set_U3CstopU3E__16_16(bool value)
	{
		___U3CstopU3E__16_16 = value;
	}

	inline static int32_t get_offset_of_U3CU24s_365U3E__17_17() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CU24s_365U3E__17_17)); }
	inline Il2CppObject * get_U3CU24s_365U3E__17_17() const { return ___U3CU24s_365U3E__17_17; }
	inline Il2CppObject ** get_address_of_U3CU24s_365U3E__17_17() { return &___U3CU24s_365U3E__17_17; }
	inline void set_U3CU24s_365U3E__17_17(Il2CppObject * value)
	{
		___U3CU24s_365U3E__17_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_365U3E__17_17, value);
	}

	inline static int32_t get_offset_of_U3CstopU3E__18_18() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CstopU3E__18_18)); }
	inline bool get_U3CstopU3E__18_18() const { return ___U3CstopU3E__18_18; }
	inline bool* get_address_of_U3CstopU3E__18_18() { return &___U3CstopU3E__18_18; }
	inline void set_U3CstopU3E__18_18(bool value)
	{
		___U3CstopU3E__18_18 = value;
	}

	inline static int32_t get_offset_of_U3ChandlerU3E__19_19() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3ChandlerU3E__19_19)); }
	inline Il2CppObject * get_U3ChandlerU3E__19_19() const { return ___U3ChandlerU3E__19_19; }
	inline Il2CppObject ** get_address_of_U3ChandlerU3E__19_19() { return &___U3ChandlerU3E__19_19; }
	inline void set_U3ChandlerU3E__19_19(Il2CppObject * value)
	{
		___U3ChandlerU3E__19_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3ChandlerU3E__19_19, value);
	}

	inline static int32_t get_offset_of_U3CfnfU3E__20_20() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CfnfU3E__20_20)); }
	inline FileNotFoundException_t225391025 * get_U3CfnfU3E__20_20() const { return ___U3CfnfU3E__20_20; }
	inline FileNotFoundException_t225391025 ** get_address_of_U3CfnfU3E__20_20() { return &___U3CfnfU3E__20_20; }
	inline void set_U3CfnfU3E__20_20(FileNotFoundException_t225391025 * value)
	{
		___U3CfnfU3E__20_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfnfU3E__20_20, value);
	}

	inline static int32_t get_offset_of_U3CdnfU3E__21_21() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CdnfU3E__21_21)); }
	inline DirectoryNotFoundException_t1220582502 * get_U3CdnfU3E__21_21() const { return ___U3CdnfU3E__21_21; }
	inline DirectoryNotFoundException_t1220582502 ** get_address_of_U3CdnfU3E__21_21() { return &___U3CdnfU3E__21_21; }
	inline void set_U3CdnfU3E__21_21(DirectoryNotFoundException_t1220582502 * value)
	{
		___U3CdnfU3E__21_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdnfU3E__21_21, value);
	}

	inline static int32_t get_offset_of_U3CeU3E__22_22() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CeU3E__22_22)); }
	inline Exception_t1436737249 * get_U3CeU3E__22_22() const { return ___U3CeU3E__22_22; }
	inline Exception_t1436737249 ** get_address_of_U3CeU3E__22_22() { return &___U3CeU3E__22_22; }
	inline void set_U3CeU3E__22_22(Exception_t1436737249 * value)
	{
		___U3CeU3E__22_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CeU3E__22_22, value);
	}

	inline static int32_t get_offset_of_U3CU24s_366U3E__23_23() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CU24s_366U3E__23_23)); }
	inline Il2CppObject * get_U3CU24s_366U3E__23_23() const { return ___U3CU24s_366U3E__23_23; }
	inline Il2CppObject ** get_address_of_U3CU24s_366U3E__23_23() { return &___U3CU24s_366U3E__23_23; }
	inline void set_U3CU24s_366U3E__23_23(Il2CppObject * value)
	{
		___U3CU24s_366U3E__23_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_366U3E__23_23, value);
	}

	inline static int32_t get_offset_of_U3CstopU3E__24_24() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CstopU3E__24_24)); }
	inline bool get_U3CstopU3E__24_24() const { return ___U3CstopU3E__24_24; }
	inline bool* get_address_of_U3CstopU3E__24_24() { return &___U3CstopU3E__24_24; }
	inline void set_U3CstopU3E__24_24(bool value)
	{
		___U3CstopU3E__24_24 = value;
	}

	inline static int32_t get_offset_of_U3CU24s_367U3E__25_25() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CU24s_367U3E__25_25)); }
	inline Il2CppObject * get_U3CU24s_367U3E__25_25() const { return ___U3CU24s_367U3E__25_25; }
	inline Il2CppObject ** get_address_of_U3CU24s_367U3E__25_25() { return &___U3CU24s_367U3E__25_25; }
	inline void set_U3CU24s_367U3E__25_25(Il2CppObject * value)
	{
		___U3CU24s_367U3E__25_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_367U3E__25_25, value);
	}

	inline static int32_t get_offset_of_U3CstopU3E__26_26() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CstopU3E__26_26)); }
	inline bool get_U3CstopU3E__26_26() const { return ___U3CstopU3E__26_26; }
	inline bool* get_address_of_U3CstopU3E__26_26() { return &___U3CstopU3E__26_26; }
	inline void set_U3CstopU3E__26_26(bool value)
	{
		___U3CstopU3E__26_26 = value;
	}

	inline static int32_t get_offset_of_U3CU24s_368U3E__27_27() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CU24s_368U3E__27_27)); }
	inline Il2CppObject * get_U3CU24s_368U3E__27_27() const { return ___U3CU24s_368U3E__27_27; }
	inline Il2CppObject ** get_address_of_U3CU24s_368U3E__27_27() { return &___U3CU24s_368U3E__27_27; }
	inline void set_U3CU24s_368U3E__27_27(Il2CppObject * value)
	{
		___U3CU24s_368U3E__27_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_368U3E__27_27, value);
	}

	inline static int32_t get_offset_of_U3CstopU3E__28_28() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CstopU3E__28_28)); }
	inline bool get_U3CstopU3E__28_28() const { return ___U3CstopU3E__28_28; }
	inline bool* get_address_of_U3CstopU3E__28_28() { return &___U3CstopU3E__28_28; }
	inline void set_U3CstopU3E__28_28(bool value)
	{
		___U3CstopU3E__28_28 = value;
	}

	inline static int32_t get_offset_of_U3CU24s_369U3E__29_29() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CU24s_369U3E__29_29)); }
	inline Il2CppObject * get_U3CU24s_369U3E__29_29() const { return ___U3CU24s_369U3E__29_29; }
	inline Il2CppObject ** get_address_of_U3CU24s_369U3E__29_29() { return &___U3CU24s_369U3E__29_29; }
	inline void set_U3CU24s_369U3E__29_29(Il2CppObject * value)
	{
		___U3CU24s_369U3E__29_29 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_369U3E__29_29, value);
	}

	inline static int32_t get_offset_of_U3CstopU3E__30_30() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CstopU3E__30_30)); }
	inline bool get_U3CstopU3E__30_30() const { return ___U3CstopU3E__30_30; }
	inline bool* get_address_of_U3CstopU3E__30_30() { return &___U3CstopU3E__30_30; }
	inline void set_U3CstopU3E__30_30(bool value)
	{
		___U3CstopU3E__30_30 = value;
	}

	inline static int32_t get_offset_of_U3CctxHandlerU3E__31_31() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CctxHandlerU3E__31_31)); }
	inline Il2CppObject * get_U3CctxHandlerU3E__31_31() const { return ___U3CctxHandlerU3E__31_31; }
	inline Il2CppObject ** get_address_of_U3CctxHandlerU3E__31_31() { return &___U3CctxHandlerU3E__31_31; }
	inline void set_U3CctxHandlerU3E__31_31(Il2CppObject * value)
	{
		___U3CctxHandlerU3E__31_31 = value;
		Il2CppCodeGenWriteBarrier(&___U3CctxHandlerU3E__31_31, value);
	}

	inline static int32_t get_offset_of_U3Casync_handlerU3E__32_32() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3Casync_handlerU3E__32_32)); }
	inline Il2CppObject * get_U3Casync_handlerU3E__32_32() const { return ___U3Casync_handlerU3E__32_32; }
	inline Il2CppObject ** get_address_of_U3Casync_handlerU3E__32_32() { return &___U3Casync_handlerU3E__32_32; }
	inline void set_U3Casync_handlerU3E__32_32(Il2CppObject * value)
	{
		___U3Casync_handlerU3E__32_32 = value;
		Il2CppCodeGenWriteBarrier(&___U3Casync_handlerU3E__32_32, value);
	}

	inline static int32_t get_offset_of_U3CU24s_370U3E__33_33() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CU24s_370U3E__33_33)); }
	inline Il2CppObject * get_U3CU24s_370U3E__33_33() const { return ___U3CU24s_370U3E__33_33; }
	inline Il2CppObject ** get_address_of_U3CU24s_370U3E__33_33() { return &___U3CU24s_370U3E__33_33; }
	inline void set_U3CU24s_370U3E__33_33(Il2CppObject * value)
	{
		___U3CU24s_370U3E__33_33 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_370U3E__33_33, value);
	}

	inline static int32_t get_offset_of_U3CstopU3E__34_34() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CstopU3E__34_34)); }
	inline bool get_U3CstopU3E__34_34() const { return ___U3CstopU3E__34_34; }
	inline bool* get_address_of_U3CstopU3E__34_34() { return &___U3CstopU3E__34_34; }
	inline void set_U3CstopU3E__34_34(bool value)
	{
		___U3CstopU3E__34_34 = value;
	}

	inline static int32_t get_offset_of_U3CU24s_371U3E__35_35() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CU24s_371U3E__35_35)); }
	inline Il2CppObject * get_U3CU24s_371U3E__35_35() const { return ___U3CU24s_371U3E__35_35; }
	inline Il2CppObject ** get_address_of_U3CU24s_371U3E__35_35() { return &___U3CU24s_371U3E__35_35; }
	inline void set_U3CU24s_371U3E__35_35(Il2CppObject * value)
	{
		___U3CU24s_371U3E__35_35 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_371U3E__35_35, value);
	}

	inline static int32_t get_offset_of_U3CstopU3E__36_36() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CstopU3E__36_36)); }
	inline bool get_U3CstopU3E__36_36() const { return ___U3CstopU3E__36_36; }
	inline bool* get_address_of_U3CstopU3E__36_36() { return &___U3CstopU3E__36_36; }
	inline void set_U3CstopU3E__36_36(bool value)
	{
		___U3CstopU3E__36_36 = value;
	}

	inline static int32_t get_offset_of_U3CU24s_372U3E__37_37() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CU24s_372U3E__37_37)); }
	inline Il2CppObject * get_U3CU24s_372U3E__37_37() const { return ___U3CU24s_372U3E__37_37; }
	inline Il2CppObject ** get_address_of_U3CU24s_372U3E__37_37() { return &___U3CU24s_372U3E__37_37; }
	inline void set_U3CU24s_372U3E__37_37(Il2CppObject * value)
	{
		___U3CU24s_372U3E__37_37 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_372U3E__37_37, value);
	}

	inline static int32_t get_offset_of_U3CstopU3E__38_38() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CstopU3E__38_38)); }
	inline bool get_U3CstopU3E__38_38() const { return ___U3CstopU3E__38_38; }
	inline bool* get_address_of_U3CstopU3E__38_38() { return &___U3CstopU3E__38_38; }
	inline void set_U3CstopU3E__38_38(bool value)
	{
		___U3CstopU3E__38_38 = value;
	}

	inline static int32_t get_offset_of_U3CU24s_373U3E__39_39() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CU24s_373U3E__39_39)); }
	inline Il2CppObject * get_U3CU24s_373U3E__39_39() const { return ___U3CU24s_373U3E__39_39; }
	inline Il2CppObject ** get_address_of_U3CU24s_373U3E__39_39() { return &___U3CU24s_373U3E__39_39; }
	inline void set_U3CU24s_373U3E__39_39(Il2CppObject * value)
	{
		___U3CU24s_373U3E__39_39 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_373U3E__39_39, value);
	}

	inline static int32_t get_offset_of_U3CstopU3E__40_40() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CstopU3E__40_40)); }
	inline bool get_U3CstopU3E__40_40() const { return ___U3CstopU3E__40_40; }
	inline bool* get_address_of_U3CstopU3E__40_40() { return &___U3CstopU3E__40_40; }
	inline void set_U3CstopU3E__40_40(bool value)
	{
		___U3CstopU3E__40_40 = value;
	}

	inline static int32_t get_offset_of_U3CU24s_374U3E__41_41() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CU24s_374U3E__41_41)); }
	inline Il2CppObject * get_U3CU24s_374U3E__41_41() const { return ___U3CU24s_374U3E__41_41; }
	inline Il2CppObject ** get_address_of_U3CU24s_374U3E__41_41() { return &___U3CU24s_374U3E__41_41; }
	inline void set_U3CU24s_374U3E__41_41(Il2CppObject * value)
	{
		___U3CU24s_374U3E__41_41 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_374U3E__41_41, value);
	}

	inline static int32_t get_offset_of_U3CstopU3E__42_42() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CstopU3E__42_42)); }
	inline bool get_U3CstopU3E__42_42() const { return ___U3CstopU3E__42_42; }
	inline bool* get_address_of_U3CstopU3E__42_42() { return &___U3CstopU3E__42_42; }
	inline void set_U3CstopU3E__42_42(bool value)
	{
		___U3CstopU3E__42_42 = value;
	}

	inline static int32_t get_offset_of_U3CU24s_375U3E__43_43() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CU24s_375U3E__43_43)); }
	inline Il2CppObject * get_U3CU24s_375U3E__43_43() const { return ___U3CU24s_375U3E__43_43; }
	inline Il2CppObject ** get_address_of_U3CU24s_375U3E__43_43() { return &___U3CU24s_375U3E__43_43; }
	inline void set_U3CU24s_375U3E__43_43(Il2CppObject * value)
	{
		___U3CU24s_375U3E__43_43 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_375U3E__43_43, value);
	}

	inline static int32_t get_offset_of_U3CstopU3E__44_44() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CstopU3E__44_44)); }
	inline bool get_U3CstopU3E__44_44() const { return ___U3CstopU3E__44_44; }
	inline bool* get_address_of_U3CstopU3E__44_44() { return &___U3CstopU3E__44_44; }
	inline void set_U3CstopU3E__44_44(bool value)
	{
		___U3CstopU3E__44_44 = value;
	}

	inline static int32_t get_offset_of_U3CU24s_376U3E__45_45() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CU24s_376U3E__45_45)); }
	inline Il2CppObject * get_U3CU24s_376U3E__45_45() const { return ___U3CU24s_376U3E__45_45; }
	inline Il2CppObject ** get_address_of_U3CU24s_376U3E__45_45() { return &___U3CU24s_376U3E__45_45; }
	inline void set_U3CU24s_376U3E__45_45(Il2CppObject * value)
	{
		___U3CU24s_376U3E__45_45 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_376U3E__45_45, value);
	}

	inline static int32_t get_offset_of_U3CstopU3E__46_46() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CstopU3E__46_46)); }
	inline bool get_U3CstopU3E__46_46() const { return ___U3CstopU3E__46_46; }
	inline bool* get_address_of_U3CstopU3E__46_46() { return &___U3CstopU3E__46_46; }
	inline void set_U3CstopU3E__46_46(bool value)
	{
		___U3CstopU3E__46_46 = value;
	}

	inline static int32_t get_offset_of_U24PC_47() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U24PC_47)); }
	inline int32_t get_U24PC_47() const { return ___U24PC_47; }
	inline int32_t* get_address_of_U24PC_47() { return &___U24PC_47; }
	inline void set_U24PC_47(int32_t value)
	{
		___U24PC_47 = value;
	}

	inline static int32_t get_offset_of_U24current_48() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U24current_48)); }
	inline Il2CppObject * get_U24current_48() const { return ___U24current_48; }
	inline Il2CppObject ** get_address_of_U24current_48() { return &___U24current_48; }
	inline void set_U24current_48(Il2CppObject * value)
	{
		___U24current_48 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_48, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_49() { return static_cast<int32_t>(offsetof(U3CPipelineU3Ec__Iterator2_t2410703380, ___U3CU3Ef__this_49)); }
	inline HttpApplication_t3359645917 * get_U3CU3Ef__this_49() const { return ___U3CU3Ef__this_49; }
	inline HttpApplication_t3359645917 ** get_address_of_U3CU3Ef__this_49() { return &___U3CU3Ef__this_49; }
	inline void set_U3CU3Ef__this_49(HttpApplication_t3359645917 * value)
	{
		___U3CU3Ef__this_49 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_49, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
