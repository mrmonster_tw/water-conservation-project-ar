﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_M3063398011.h"

// System.ServiceModel.Channels.BinaryMessageEncoderFactory
struct BinaryMessageEncoderFactory_t4125415926;
// System.Xml.XmlBinaryReaderSession
struct XmlBinaryReaderSession_t3909229952;
// System.Xml.XmlBinaryWriterSession
struct XmlBinaryWriterSession_t1730638232;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.BinaryMessageEncoder
struct  BinaryMessageEncoder_t3396373419  : public MessageEncoder_t3063398011
{
public:
	// System.ServiceModel.Channels.BinaryMessageEncoderFactory System.ServiceModel.Channels.BinaryMessageEncoder::owner
	BinaryMessageEncoderFactory_t4125415926 * ___owner_0;
	// System.Boolean System.ServiceModel.Channels.BinaryMessageEncoder::session
	bool ___session_1;
	// System.Xml.XmlBinaryReaderSession System.ServiceModel.Channels.BinaryMessageEncoder::<CurrentReaderSession>k__BackingField
	XmlBinaryReaderSession_t3909229952 * ___U3CCurrentReaderSessionU3Ek__BackingField_2;
	// System.Xml.XmlBinaryWriterSession System.ServiceModel.Channels.BinaryMessageEncoder::<CurrentWriterSession>k__BackingField
	XmlBinaryWriterSession_t1730638232 * ___U3CCurrentWriterSessionU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_owner_0() { return static_cast<int32_t>(offsetof(BinaryMessageEncoder_t3396373419, ___owner_0)); }
	inline BinaryMessageEncoderFactory_t4125415926 * get_owner_0() const { return ___owner_0; }
	inline BinaryMessageEncoderFactory_t4125415926 ** get_address_of_owner_0() { return &___owner_0; }
	inline void set_owner_0(BinaryMessageEncoderFactory_t4125415926 * value)
	{
		___owner_0 = value;
		Il2CppCodeGenWriteBarrier(&___owner_0, value);
	}

	inline static int32_t get_offset_of_session_1() { return static_cast<int32_t>(offsetof(BinaryMessageEncoder_t3396373419, ___session_1)); }
	inline bool get_session_1() const { return ___session_1; }
	inline bool* get_address_of_session_1() { return &___session_1; }
	inline void set_session_1(bool value)
	{
		___session_1 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentReaderSessionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BinaryMessageEncoder_t3396373419, ___U3CCurrentReaderSessionU3Ek__BackingField_2)); }
	inline XmlBinaryReaderSession_t3909229952 * get_U3CCurrentReaderSessionU3Ek__BackingField_2() const { return ___U3CCurrentReaderSessionU3Ek__BackingField_2; }
	inline XmlBinaryReaderSession_t3909229952 ** get_address_of_U3CCurrentReaderSessionU3Ek__BackingField_2() { return &___U3CCurrentReaderSessionU3Ek__BackingField_2; }
	inline void set_U3CCurrentReaderSessionU3Ek__BackingField_2(XmlBinaryReaderSession_t3909229952 * value)
	{
		___U3CCurrentReaderSessionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCurrentReaderSessionU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CCurrentWriterSessionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BinaryMessageEncoder_t3396373419, ___U3CCurrentWriterSessionU3Ek__BackingField_3)); }
	inline XmlBinaryWriterSession_t1730638232 * get_U3CCurrentWriterSessionU3Ek__BackingField_3() const { return ___U3CCurrentWriterSessionU3Ek__BackingField_3; }
	inline XmlBinaryWriterSession_t1730638232 ** get_address_of_U3CCurrentWriterSessionU3Ek__BackingField_3() { return &___U3CCurrentWriterSessionU3Ek__BackingField_3; }
	inline void set_U3CCurrentWriterSessionU3Ek__BackingField_3(XmlBinaryWriterSession_t1730638232 * value)
	{
		___U3CCurrentWriterSessionU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCurrentWriterSessionU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
