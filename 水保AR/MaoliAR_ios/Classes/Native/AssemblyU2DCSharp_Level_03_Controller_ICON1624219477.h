﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.UI.Image[]
struct ImageU5BU5D_t2439009922;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Level_03_Controller_ICON
struct  Level_03_Controller_ICON_t1624219477  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image[] Level_03_Controller_ICON::cars
	ImageU5BU5D_t2439009922* ___cars_2;

public:
	inline static int32_t get_offset_of_cars_2() { return static_cast<int32_t>(offsetof(Level_03_Controller_ICON_t1624219477, ___cars_2)); }
	inline ImageU5BU5D_t2439009922* get_cars_2() const { return ___cars_2; }
	inline ImageU5BU5D_t2439009922** get_address_of_cars_2() { return &___cars_2; }
	inline void set_cars_2(ImageU5BU5D_t2439009922* value)
	{
		___cars_2 = value;
		Il2CppCodeGenWriteBarrier(&___cars_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
