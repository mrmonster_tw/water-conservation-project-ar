﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t4177511560;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpCacheVaryByContentEncodings
struct  HttpCacheVaryByContentEncodings_t1137512671  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Boolean> System.Web.HttpCacheVaryByContentEncodings::encodings
	Dictionary_2_t4177511560 * ___encodings_0;

public:
	inline static int32_t get_offset_of_encodings_0() { return static_cast<int32_t>(offsetof(HttpCacheVaryByContentEncodings_t1137512671, ___encodings_0)); }
	inline Dictionary_2_t4177511560 * get_encodings_0() const { return ___encodings_0; }
	inline Dictionary_2_t4177511560 ** get_address_of_encodings_0() { return &___encodings_0; }
	inline void set_encodings_0(Dictionary_2_t4177511560 * value)
	{
		___encodings_0 = value;
		Il2CppCodeGenWriteBarrier(&___encodings_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
