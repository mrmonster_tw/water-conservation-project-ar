﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// Mono.Xml.Xsl.XsltCompiledContext
struct XsltCompiledContext_t2295645487;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Collections.Hashtable
struct Hashtable_t1853889766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.KeyIndexTable
struct  KeyIndexTable_t1422728481  : public Il2CppObject
{
public:
	// Mono.Xml.Xsl.XsltCompiledContext Mono.Xml.Xsl.KeyIndexTable::ctx
	XsltCompiledContext_t2295645487 * ___ctx_0;
	// System.Collections.ArrayList Mono.Xml.Xsl.KeyIndexTable::keys
	ArrayList_t2718874744 * ___keys_1;
	// System.Collections.Hashtable Mono.Xml.Xsl.KeyIndexTable::mappedDocuments
	Hashtable_t1853889766 * ___mappedDocuments_2;

public:
	inline static int32_t get_offset_of_ctx_0() { return static_cast<int32_t>(offsetof(KeyIndexTable_t1422728481, ___ctx_0)); }
	inline XsltCompiledContext_t2295645487 * get_ctx_0() const { return ___ctx_0; }
	inline XsltCompiledContext_t2295645487 ** get_address_of_ctx_0() { return &___ctx_0; }
	inline void set_ctx_0(XsltCompiledContext_t2295645487 * value)
	{
		___ctx_0 = value;
		Il2CppCodeGenWriteBarrier(&___ctx_0, value);
	}

	inline static int32_t get_offset_of_keys_1() { return static_cast<int32_t>(offsetof(KeyIndexTable_t1422728481, ___keys_1)); }
	inline ArrayList_t2718874744 * get_keys_1() const { return ___keys_1; }
	inline ArrayList_t2718874744 ** get_address_of_keys_1() { return &___keys_1; }
	inline void set_keys_1(ArrayList_t2718874744 * value)
	{
		___keys_1 = value;
		Il2CppCodeGenWriteBarrier(&___keys_1, value);
	}

	inline static int32_t get_offset_of_mappedDocuments_2() { return static_cast<int32_t>(offsetof(KeyIndexTable_t1422728481, ___mappedDocuments_2)); }
	inline Hashtable_t1853889766 * get_mappedDocuments_2() const { return ___mappedDocuments_2; }
	inline Hashtable_t1853889766 ** get_address_of_mappedDocuments_2() { return &___mappedDocuments_2; }
	inline void set_mappedDocuments_2(Hashtable_t1853889766 * value)
	{
		___mappedDocuments_2 = value;
		Il2CppCodeGenWriteBarrier(&___mappedDocuments_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
