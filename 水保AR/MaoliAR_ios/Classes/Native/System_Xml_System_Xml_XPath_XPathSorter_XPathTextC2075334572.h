﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Xml_System_Xml_XPath_XmlCaseOrder176547839.h"

// System.Globalization.CultureInfo
struct CultureInfo_t4157843068;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathSorter/XPathTextComparer
struct  XPathTextComparer_t2075334572  : public Il2CppObject
{
public:
	// System.Int32 System.Xml.XPath.XPathSorter/XPathTextComparer::_nMulSort
	int32_t ____nMulSort_0;
	// System.Int32 System.Xml.XPath.XPathSorter/XPathTextComparer::_nMulCase
	int32_t ____nMulCase_1;
	// System.Xml.XPath.XmlCaseOrder System.Xml.XPath.XPathSorter/XPathTextComparer::_orderCase
	int32_t ____orderCase_2;
	// System.Globalization.CultureInfo System.Xml.XPath.XPathSorter/XPathTextComparer::_ci
	CultureInfo_t4157843068 * ____ci_3;

public:
	inline static int32_t get_offset_of__nMulSort_0() { return static_cast<int32_t>(offsetof(XPathTextComparer_t2075334572, ____nMulSort_0)); }
	inline int32_t get__nMulSort_0() const { return ____nMulSort_0; }
	inline int32_t* get_address_of__nMulSort_0() { return &____nMulSort_0; }
	inline void set__nMulSort_0(int32_t value)
	{
		____nMulSort_0 = value;
	}

	inline static int32_t get_offset_of__nMulCase_1() { return static_cast<int32_t>(offsetof(XPathTextComparer_t2075334572, ____nMulCase_1)); }
	inline int32_t get__nMulCase_1() const { return ____nMulCase_1; }
	inline int32_t* get_address_of__nMulCase_1() { return &____nMulCase_1; }
	inline void set__nMulCase_1(int32_t value)
	{
		____nMulCase_1 = value;
	}

	inline static int32_t get_offset_of__orderCase_2() { return static_cast<int32_t>(offsetof(XPathTextComparer_t2075334572, ____orderCase_2)); }
	inline int32_t get__orderCase_2() const { return ____orderCase_2; }
	inline int32_t* get_address_of__orderCase_2() { return &____orderCase_2; }
	inline void set__orderCase_2(int32_t value)
	{
		____orderCase_2 = value;
	}

	inline static int32_t get_offset_of__ci_3() { return static_cast<int32_t>(offsetof(XPathTextComparer_t2075334572, ____ci_3)); }
	inline CultureInfo_t4157843068 * get__ci_3() const { return ____ci_3; }
	inline CultureInfo_t4157843068 ** get_address_of__ci_3() { return &____ci_3; }
	inline void set__ci_3(CultureInfo_t4157843068 * value)
	{
		____ci_3 = value;
		Il2CppCodeGenWriteBarrier(&____ci_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
