﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeExpression2166265795.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeVariableReferenceExpression
struct  CodeVariableReferenceExpression_t2242020292  : public CodeExpression_t2166265795
{
public:
	// System.String System.CodeDom.CodeVariableReferenceExpression::variableName
	String_t* ___variableName_1;

public:
	inline static int32_t get_offset_of_variableName_1() { return static_cast<int32_t>(offsetof(CodeVariableReferenceExpression_t2242020292, ___variableName_1)); }
	inline String_t* get_variableName_1() const { return ___variableName_1; }
	inline String_t** get_address_of_variableName_1() { return &___variableName_1; }
	inline void set_variableName_1(String_t* value)
	{
		___variableName_1 = value;
		Il2CppCodeGenWriteBarrier(&___variableName_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
