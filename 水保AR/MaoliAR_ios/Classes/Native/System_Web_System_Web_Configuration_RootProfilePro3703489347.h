﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_Configuration_ProfilePropert2271557816.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Web.Configuration.ProfileGroupSettingsCollection
struct ProfileGroupSettingsCollection_t1931628525;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.RootProfilePropertySettingsCollection
struct  RootProfilePropertySettingsCollection_t3703489347  : public ProfilePropertySettingsCollection_t2271557816
{
public:
	// System.Web.Configuration.ProfileGroupSettingsCollection System.Web.Configuration.RootProfilePropertySettingsCollection::groupSettings
	ProfileGroupSettingsCollection_t1931628525 * ___groupSettings_25;

public:
	inline static int32_t get_offset_of_groupSettings_25() { return static_cast<int32_t>(offsetof(RootProfilePropertySettingsCollection_t3703489347, ___groupSettings_25)); }
	inline ProfileGroupSettingsCollection_t1931628525 * get_groupSettings_25() const { return ___groupSettings_25; }
	inline ProfileGroupSettingsCollection_t1931628525 ** get_address_of_groupSettings_25() { return &___groupSettings_25; }
	inline void set_groupSettings_25(ProfileGroupSettingsCollection_t1931628525 * value)
	{
		___groupSettings_25 = value;
		Il2CppCodeGenWriteBarrier(&___groupSettings_25, value);
	}
};

struct RootProfilePropertySettingsCollection_t3703489347_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Web.Configuration.RootProfilePropertySettingsCollection::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_24;

public:
	inline static int32_t get_offset_of_properties_24() { return static_cast<int32_t>(offsetof(RootProfilePropertySettingsCollection_t3703489347_StaticFields, ___properties_24)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_24() const { return ___properties_24; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_24() { return &___properties_24; }
	inline void set_properties_24(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_24 = value;
		Il2CppCodeGenWriteBarrier(&___properties_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
