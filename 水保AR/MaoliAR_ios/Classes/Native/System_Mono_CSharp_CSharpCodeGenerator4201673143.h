﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_Compiler_CodeGenerator1606279797.h"

// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t96558379;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.CSharp.CSharpCodeGenerator
struct  CSharpCodeGenerator_t4201673143  : public CodeGenerator_t1606279797
{
public:
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Mono.CSharp.CSharpCodeGenerator::providerOptions
	Il2CppObject* ___providerOptions_6;
	// System.Boolean Mono.CSharp.CSharpCodeGenerator::dont_write_semicolon
	bool ___dont_write_semicolon_7;

public:
	inline static int32_t get_offset_of_providerOptions_6() { return static_cast<int32_t>(offsetof(CSharpCodeGenerator_t4201673143, ___providerOptions_6)); }
	inline Il2CppObject* get_providerOptions_6() const { return ___providerOptions_6; }
	inline Il2CppObject** get_address_of_providerOptions_6() { return &___providerOptions_6; }
	inline void set_providerOptions_6(Il2CppObject* value)
	{
		___providerOptions_6 = value;
		Il2CppCodeGenWriteBarrier(&___providerOptions_6, value);
	}

	inline static int32_t get_offset_of_dont_write_semicolon_7() { return static_cast<int32_t>(offsetof(CSharpCodeGenerator_t4201673143, ___dont_write_semicolon_7)); }
	inline bool get_dont_write_semicolon_7() const { return ___dont_write_semicolon_7; }
	inline bool* get_address_of_dont_write_semicolon_7() { return &___dont_write_semicolon_7; }
	inline void set_dont_write_semicolon_7(bool value)
	{
		___dont_write_semicolon_7 = value;
	}
};

struct CSharpCodeGenerator_t4201673143_StaticFields
{
public:
	// System.Collections.Hashtable Mono.CSharp.CSharpCodeGenerator::keywordsTable
	Hashtable_t1853889766 * ___keywordsTable_8;
	// System.String[] Mono.CSharp.CSharpCodeGenerator::keywords
	StringU5BU5D_t1281789340* ___keywords_9;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.CSharp.CSharpCodeGenerator::<>f__switch$map0
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map0_10;

public:
	inline static int32_t get_offset_of_keywordsTable_8() { return static_cast<int32_t>(offsetof(CSharpCodeGenerator_t4201673143_StaticFields, ___keywordsTable_8)); }
	inline Hashtable_t1853889766 * get_keywordsTable_8() const { return ___keywordsTable_8; }
	inline Hashtable_t1853889766 ** get_address_of_keywordsTable_8() { return &___keywordsTable_8; }
	inline void set_keywordsTable_8(Hashtable_t1853889766 * value)
	{
		___keywordsTable_8 = value;
		Il2CppCodeGenWriteBarrier(&___keywordsTable_8, value);
	}

	inline static int32_t get_offset_of_keywords_9() { return static_cast<int32_t>(offsetof(CSharpCodeGenerator_t4201673143_StaticFields, ___keywords_9)); }
	inline StringU5BU5D_t1281789340* get_keywords_9() const { return ___keywords_9; }
	inline StringU5BU5D_t1281789340** get_address_of_keywords_9() { return &___keywords_9; }
	inline void set_keywords_9(StringU5BU5D_t1281789340* value)
	{
		___keywords_9 = value;
		Il2CppCodeGenWriteBarrier(&___keywords_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_10() { return static_cast<int32_t>(offsetof(CSharpCodeGenerator_t4201673143_StaticFields, ___U3CU3Ef__switchU24map0_10)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map0_10() const { return ___U3CU3Ef__switchU24map0_10; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map0_10() { return &___U3CU3Ef__switchU24map0_10; }
	inline void set_U3CU3Ef__switchU24map0_10(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map0_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map0_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
