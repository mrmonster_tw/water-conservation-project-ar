﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Cryptography_SHA384Managed74158575.h"
#include "mscorlib_System_Security_Cryptography_SHA5121346946930.h"
#include "mscorlib_System_Security_Cryptography_SHA512Manage1787336339.h"
#include "mscorlib_System_Security_Cryptography_SHAConstants1360924992.h"
#include "mscorlib_System_Security_Cryptography_SignatureDes1971889425.h"
#include "mscorlib_System_Security_Cryptography_DSASignature1163053634.h"
#include "mscorlib_System_Security_Cryptography_RSAPKCS1SHA12887980541.h"
#include "mscorlib_System_Security_Cryptography_SymmetricAlg4254223087.h"
#include "mscorlib_System_Security_Cryptography_ToBase64Tran2551557057.h"
#include "mscorlib_System_Security_Cryptography_TripleDES92303514.h"
#include "mscorlib_System_Security_Cryptography_TripleDESCry3595206342.h"
#include "mscorlib_System_Security_Cryptography_TripleDESTran964169060.h"
#include "mscorlib_System_Security_Cryptography_X509Certifica713131622.h"
#include "mscorlib_System_Security_Cryptography_X509Certifica441861693.h"
#include "mscorlib_System_Security_Permissions_EnvironmentPe1242010617.h"
#include "mscorlib_System_Security_Permissions_EnvironmentPe1745565475.h"
#include "mscorlib_System_Security_Permissions_FileDialogPerm986095094.h"
#include "mscorlib_System_Security_Permissions_FileDialogPer4172829844.h"
#include "mscorlib_System_Security_Permissions_FileIOPermiss3596906749.h"
#include "mscorlib_System_Security_Permissions_FileIOPermiss1559273540.h"
#include "mscorlib_System_Security_Permissions_GacIdentityPe2991335275.h"
#include "mscorlib_System_Security_Permissions_IsolatedStora3203638662.h"
#include "mscorlib_System_Security_Permissions_IsolatedStora2861120981.h"
#include "mscorlib_System_Security_Permissions_IsolatedStora2996329588.h"
#include "mscorlib_System_Security_Permissions_KeyContainerP1460699488.h"
#include "mscorlib_System_Security_Permissions_KeyContainerP3026022710.h"
#include "mscorlib_System_Security_Permissions_KeyContainerP4144114052.h"
#include "mscorlib_System_Security_Permissions_KeyContainerP3641947072.h"
#include "mscorlib_System_Security_Permissions_KeyContainerP3864969297.h"
#include "mscorlib_System_Security_Permissions_PermissionSta1178999876.h"
#include "mscorlib_System_Security_Permissions_PublisherIdent594127488.h"
#include "mscorlib_System_Security_Permissions_ReflectionPer2208099681.h"
#include "mscorlib_System_Security_Permissions_ReflectionPer4199904140.h"
#include "mscorlib_System_Security_Permissions_RegistryPermi4244934776.h"
#include "mscorlib_System_Security_Permissions_RegistryPermis307445458.h"
#include "mscorlib_System_Security_Permissions_SecurityAction569814076.h"
#include "mscorlib_System_Security_Permissions_SecurityPermi2954997752.h"
#include "mscorlib_System_Security_Permissions_SecurityPermi3459270124.h"
#include "mscorlib_System_Security_Permissions_SiteIdentityP4239769539.h"
#include "mscorlib_System_Security_Permissions_StrongNameIde2780499738.h"
#include "mscorlib_System_Security_Permissions_StrongNameIde4156255223.h"
#include "mscorlib_System_Security_Permissions_StrongNamePub3355146440.h"
#include "mscorlib_System_Security_Permissions_UIPermission826475846.h"
#include "mscorlib_System_Security_Permissions_UIPermissionC4002838840.h"
#include "mscorlib_System_Security_Permissions_UIPermissionWi985109583.h"
#include "mscorlib_System_Security_Permissions_UrlIdentityPerm48040392.h"
#include "mscorlib_System_Security_Permissions_ZoneIdentityP2250593031.h"
#include "mscorlib_System_Security_Policy_AllMembershipCondit198137363.h"
#include "mscorlib_System_Security_Policy_ApplicationTrust3270368423.h"
#include "mscorlib_System_Security_Policy_CodeConnectAccess1103527196.h"
#include "mscorlib_System_Security_Policy_CodeGroup3811807446.h"
#include "mscorlib_System_Security_Policy_DefaultPolicies2520506789.h"
#include "mscorlib_System_Security_Policy_DefaultPolicies_Ke3006169375.h"
#include "mscorlib_System_Security_Policy_Evidence2008144148.h"
#include "mscorlib_System_Security_Policy_Evidence_EvidenceE1708166667.h"
#include "mscorlib_System_Security_Policy_FileCodeGroup1720965944.h"
#include "mscorlib_System_Security_Policy_FirstMatchCodeGroup885469689.h"
#include "mscorlib_System_Security_Policy_GacInstalled3565883570.h"
#include "mscorlib_System_Security_Policy_Hash5925575.h"
#include "mscorlib_System_Security_Policy_MembershipConditio2246572704.h"
#include "mscorlib_System_Security_Policy_NetCodeGroup2217812384.h"
#include "mscorlib_System_Security_Policy_PermissionRequestEvi59447972.h"
#include "mscorlib_System_Security_Policy_PolicyException1520028310.h"
#include "mscorlib_System_Security_Policy_PolicyLevel2891196107.h"
#include "mscorlib_System_Security_Policy_PolicyStatement3052133691.h"
#include "mscorlib_System_Security_Policy_PolicyStatementAtt1674167676.h"
#include "mscorlib_System_Security_Policy_Publisher2758056332.h"
#include "mscorlib_System_Security_Policy_Site1075497104.h"
#include "mscorlib_System_Security_Policy_StrongName3675724614.h"
#include "mscorlib_System_Security_Policy_StrongNameMembersh2614563360.h"
#include "mscorlib_System_Security_Policy_UnionCodeGroup3773268997.h"
#include "mscorlib_System_Security_Policy_Url207802215.h"
#include "mscorlib_System_Security_Policy_Zone2011285646.h"
#include "mscorlib_System_Security_Policy_ZoneMembershipCond3195636716.h"
#include "mscorlib_System_Security_Principal_GenericIdentity2319019448.h"
#include "mscorlib_System_Security_Principal_GenericPrincipa2520297622.h"
#include "mscorlib_System_Security_Principal_PrincipalPolicy1761212333.h"
#include "mscorlib_System_Security_Principal_TokenImpersonat3773270939.h"
#include "mscorlib_System_Security_Principal_WindowsAccountT2283000883.h"
#include "mscorlib_System_Security_Principal_WindowsIdentity2948242406.h"
#include "mscorlib_System_Security_Principal_WindowsPrincipal239041500.h"
#include "mscorlib_System_Text_ASCIIEncoding3446586211.h"
#include "mscorlib_System_Text_Decoder2204182725.h"
#include "mscorlib_System_Text_DecoderExceptionFallback3981484394.h"
#include "mscorlib_System_Text_DecoderExceptionFallbackBuffer90938522.h"
#include "mscorlib_System_Text_DecoderFallback3123823036.h"
#include "mscorlib_System_Text_DecoderFallbackBuffer2402303981.h"
#include "mscorlib_System_Text_DecoderFallbackException1661362184.h"
#include "mscorlib_System_Text_DecoderReplacementFallback1462101135.h"
#include "mscorlib_System_Text_DecoderReplacementFallbackBuff841144779.h"
#include "mscorlib_System_Text_EncoderExceptionFallback1243849599.h"
#include "mscorlib_System_Text_EncoderExceptionFallbackBuffe3597232471.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize900 = { sizeof (SHA384Managed_t74158575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable900[14] = 
{
	SHA384Managed_t74158575::get_offset_of_xBuf_4(),
	SHA384Managed_t74158575::get_offset_of_xBufOff_5(),
	SHA384Managed_t74158575::get_offset_of_byteCount1_6(),
	SHA384Managed_t74158575::get_offset_of_byteCount2_7(),
	SHA384Managed_t74158575::get_offset_of_H1_8(),
	SHA384Managed_t74158575::get_offset_of_H2_9(),
	SHA384Managed_t74158575::get_offset_of_H3_10(),
	SHA384Managed_t74158575::get_offset_of_H4_11(),
	SHA384Managed_t74158575::get_offset_of_H5_12(),
	SHA384Managed_t74158575::get_offset_of_H6_13(),
	SHA384Managed_t74158575::get_offset_of_H7_14(),
	SHA384Managed_t74158575::get_offset_of_H8_15(),
	SHA384Managed_t74158575::get_offset_of_W_16(),
	SHA384Managed_t74158575::get_offset_of_wOff_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize901 = { sizeof (SHA512_t1346946930), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize902 = { sizeof (SHA512Managed_t1787336339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable902[14] = 
{
	SHA512Managed_t1787336339::get_offset_of_xBuf_4(),
	SHA512Managed_t1787336339::get_offset_of_xBufOff_5(),
	SHA512Managed_t1787336339::get_offset_of_byteCount1_6(),
	SHA512Managed_t1787336339::get_offset_of_byteCount2_7(),
	SHA512Managed_t1787336339::get_offset_of_H1_8(),
	SHA512Managed_t1787336339::get_offset_of_H2_9(),
	SHA512Managed_t1787336339::get_offset_of_H3_10(),
	SHA512Managed_t1787336339::get_offset_of_H4_11(),
	SHA512Managed_t1787336339::get_offset_of_H5_12(),
	SHA512Managed_t1787336339::get_offset_of_H6_13(),
	SHA512Managed_t1787336339::get_offset_of_H7_14(),
	SHA512Managed_t1787336339::get_offset_of_H8_15(),
	SHA512Managed_t1787336339::get_offset_of_W_16(),
	SHA512Managed_t1787336339::get_offset_of_wOff_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize903 = { sizeof (SHAConstants_t1360924992), -1, sizeof(SHAConstants_t1360924992_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable903[2] = 
{
	SHAConstants_t1360924992_StaticFields::get_offset_of_K1_0(),
	SHAConstants_t1360924992_StaticFields::get_offset_of_K2_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize904 = { sizeof (SignatureDescription_t1971889425), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable904[4] = 
{
	SignatureDescription_t1971889425::get_offset_of__DeformatterAlgorithm_0(),
	SignatureDescription_t1971889425::get_offset_of__DigestAlgorithm_1(),
	SignatureDescription_t1971889425::get_offset_of__FormatterAlgorithm_2(),
	SignatureDescription_t1971889425::get_offset_of__KeyAlgorithm_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize905 = { sizeof (DSASignatureDescription_t1163053634), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize906 = { sizeof (RSAPKCS1SHA1SignatureDescription_t2887980541), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize907 = { sizeof (SymmetricAlgorithm_t4254223087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable907[10] = 
{
	SymmetricAlgorithm_t4254223087::get_offset_of_BlockSizeValue_0(),
	SymmetricAlgorithm_t4254223087::get_offset_of_IVValue_1(),
	SymmetricAlgorithm_t4254223087::get_offset_of_KeySizeValue_2(),
	SymmetricAlgorithm_t4254223087::get_offset_of_KeyValue_3(),
	SymmetricAlgorithm_t4254223087::get_offset_of_LegalBlockSizesValue_4(),
	SymmetricAlgorithm_t4254223087::get_offset_of_LegalKeySizesValue_5(),
	SymmetricAlgorithm_t4254223087::get_offset_of_FeedbackSizeValue_6(),
	SymmetricAlgorithm_t4254223087::get_offset_of_ModeValue_7(),
	SymmetricAlgorithm_t4254223087::get_offset_of_PaddingValue_8(),
	SymmetricAlgorithm_t4254223087::get_offset_of_m_disposed_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize908 = { sizeof (ToBase64Transform_t2551557057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable908[1] = 
{
	ToBase64Transform_t2551557057::get_offset_of_m_disposed_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize909 = { sizeof (TripleDES_t92303514), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize910 = { sizeof (TripleDESCryptoServiceProvider_t3595206342), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize911 = { sizeof (TripleDESTransform_t964169060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable911[6] = 
{
	TripleDESTransform_t964169060::get_offset_of_E1_12(),
	TripleDESTransform_t964169060::get_offset_of_D2_13(),
	TripleDESTransform_t964169060::get_offset_of_E3_14(),
	TripleDESTransform_t964169060::get_offset_of_D1_15(),
	TripleDESTransform_t964169060::get_offset_of_E2_16(),
	TripleDESTransform_t964169060::get_offset_of_D3_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize912 = { sizeof (X509Certificate_t713131622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable912[5] = 
{
	X509Certificate_t713131622::get_offset_of_x509_0(),
	X509Certificate_t713131622::get_offset_of_hideDates_1(),
	X509Certificate_t713131622::get_offset_of_cachedCertificateHash_2(),
	X509Certificate_t713131622::get_offset_of_issuer_name_3(),
	X509Certificate_t713131622::get_offset_of_subject_name_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize913 = { sizeof (X509KeyStorageFlags_t441861693)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable913[7] = 
{
	X509KeyStorageFlags_t441861693::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize914 = { sizeof (EnvironmentPermission_t1242010617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable914[3] = 
{
	EnvironmentPermission_t1242010617::get_offset_of__state_0(),
	EnvironmentPermission_t1242010617::get_offset_of_readList_1(),
	EnvironmentPermission_t1242010617::get_offset_of_writeList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize915 = { sizeof (EnvironmentPermissionAccess_t1745565475)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable915[5] = 
{
	EnvironmentPermissionAccess_t1745565475::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize916 = { sizeof (FileDialogPermission_t986095094), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable916[1] = 
{
	FileDialogPermission_t986095094::get_offset_of__access_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize917 = { sizeof (FileDialogPermissionAccess_t4172829844)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable917[5] = 
{
	FileDialogPermissionAccess_t4172829844::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize918 = { sizeof (FileIOPermission_t3596906749), -1, sizeof(FileIOPermission_t3596906749_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable918[9] = 
{
	FileIOPermission_t3596906749_StaticFields::get_offset_of_BadPathNameCharacters_0(),
	FileIOPermission_t3596906749_StaticFields::get_offset_of_BadFileNameCharacters_1(),
	FileIOPermission_t3596906749::get_offset_of_m_Unrestricted_2(),
	FileIOPermission_t3596906749::get_offset_of_m_AllFilesAccess_3(),
	FileIOPermission_t3596906749::get_offset_of_m_AllLocalFilesAccess_4(),
	FileIOPermission_t3596906749::get_offset_of_readList_5(),
	FileIOPermission_t3596906749::get_offset_of_writeList_6(),
	FileIOPermission_t3596906749::get_offset_of_appendList_7(),
	FileIOPermission_t3596906749::get_offset_of_pathList_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize919 = { sizeof (FileIOPermissionAccess_t1559273540)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable919[7] = 
{
	FileIOPermissionAccess_t1559273540::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize920 = { sizeof (GacIdentityPermission_t2991335275), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize921 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize922 = { sizeof (IsolatedStorageContainment_t3203638662)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable922[13] = 
{
	IsolatedStorageContainment_t3203638662::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize923 = { sizeof (IsolatedStorageFilePermission_t2861120981), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize924 = { sizeof (IsolatedStoragePermission_t2996329588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable924[5] = 
{
	IsolatedStoragePermission_t2996329588::get_offset_of_m_userQuota_0(),
	IsolatedStoragePermission_t2996329588::get_offset_of_m_machineQuota_1(),
	IsolatedStoragePermission_t2996329588::get_offset_of_m_expirationDays_2(),
	IsolatedStoragePermission_t2996329588::get_offset_of_m_permanentData_3(),
	IsolatedStoragePermission_t2996329588::get_offset_of_m_allowed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize925 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize926 = { sizeof (KeyContainerPermission_t1460699488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable926[2] = 
{
	KeyContainerPermission_t1460699488::get_offset_of__accessEntries_0(),
	KeyContainerPermission_t1460699488::get_offset_of__flags_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize927 = { sizeof (KeyContainerPermissionAccessEntry_t3026022710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable927[6] = 
{
	KeyContainerPermissionAccessEntry_t3026022710::get_offset_of__flags_0(),
	KeyContainerPermissionAccessEntry_t3026022710::get_offset_of__containerName_1(),
	KeyContainerPermissionAccessEntry_t3026022710::get_offset_of__spec_2(),
	KeyContainerPermissionAccessEntry_t3026022710::get_offset_of__store_3(),
	KeyContainerPermissionAccessEntry_t3026022710::get_offset_of__providerName_4(),
	KeyContainerPermissionAccessEntry_t3026022710::get_offset_of__type_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize928 = { sizeof (KeyContainerPermissionAccessEntryCollection_t4144114052), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable928[1] = 
{
	KeyContainerPermissionAccessEntryCollection_t4144114052::get_offset_of__list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize929 = { sizeof (KeyContainerPermissionAccessEntryEnumerator_t3641947072), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable929[1] = 
{
	KeyContainerPermissionAccessEntryEnumerator_t3641947072::get_offset_of_e_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize930 = { sizeof (KeyContainerPermissionFlags_t3864969297)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable930[12] = 
{
	KeyContainerPermissionFlags_t3864969297::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize931 = { sizeof (PermissionState_t1178999876)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable931[3] = 
{
	PermissionState_t1178999876::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize932 = { sizeof (PublisherIdentityPermission_t594127488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable932[1] = 
{
	PublisherIdentityPermission_t594127488::get_offset_of_x509_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize933 = { sizeof (ReflectionPermission_t2208099681), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable933[1] = 
{
	ReflectionPermission_t2208099681::get_offset_of_flags_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize934 = { sizeof (ReflectionPermissionFlag_t4199904140)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable934[7] = 
{
	ReflectionPermissionFlag_t4199904140::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize935 = { sizeof (RegistryPermission_t4244934776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable935[4] = 
{
	RegistryPermission_t4244934776::get_offset_of__state_0(),
	RegistryPermission_t4244934776::get_offset_of_createList_1(),
	RegistryPermission_t4244934776::get_offset_of_readList_2(),
	RegistryPermission_t4244934776::get_offset_of_writeList_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize936 = { sizeof (RegistryPermissionAccess_t307445458)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable936[6] = 
{
	RegistryPermissionAccess_t307445458::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize937 = { sizeof (SecurityAction_t569814076)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable937[10] = 
{
	SecurityAction_t569814076::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize938 = { sizeof (SecurityPermission_t2954997752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable938[1] = 
{
	SecurityPermission_t2954997752::get_offset_of_flags_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize939 = { sizeof (SecurityPermissionFlag_t3459270124)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable939[17] = 
{
	SecurityPermissionFlag_t3459270124::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize940 = { sizeof (SiteIdentityPermission_t4239769539), -1, sizeof(SiteIdentityPermission_t4239769539_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable940[2] = 
{
	SiteIdentityPermission_t4239769539::get_offset_of__site_0(),
	SiteIdentityPermission_t4239769539_StaticFields::get_offset_of_valid_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize941 = { sizeof (StrongNameIdentityPermission_t2780499738), -1, sizeof(StrongNameIdentityPermission_t2780499738_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable941[3] = 
{
	StrongNameIdentityPermission_t2780499738_StaticFields::get_offset_of_defaultVersion_0(),
	StrongNameIdentityPermission_t2780499738::get_offset_of__state_1(),
	StrongNameIdentityPermission_t2780499738::get_offset_of__list_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize942 = { sizeof (SNIP_t4156255223)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable942[3] = 
{
	SNIP_t4156255223::get_offset_of_PublicKey_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SNIP_t4156255223::get_offset_of_Name_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SNIP_t4156255223::get_offset_of_AssemblyVersion_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize943 = { sizeof (StrongNamePublicKeyBlob_t3355146440), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable943[1] = 
{
	StrongNamePublicKeyBlob_t3355146440::get_offset_of_pubkey_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize944 = { sizeof (UIPermission_t826475846), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable944[2] = 
{
	UIPermission_t826475846::get_offset_of__window_0(),
	UIPermission_t826475846::get_offset_of__clipboard_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize945 = { sizeof (UIPermissionClipboard_t4002838840)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable945[4] = 
{
	UIPermissionClipboard_t4002838840::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize946 = { sizeof (UIPermissionWindow_t985109583)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable946[5] = 
{
	UIPermissionWindow_t985109583::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize947 = { sizeof (UrlIdentityPermission_t48040392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable947[1] = 
{
	UrlIdentityPermission_t48040392::get_offset_of_url_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize948 = { sizeof (ZoneIdentityPermission_t2250593031), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable948[1] = 
{
	ZoneIdentityPermission_t2250593031::get_offset_of_zone_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize949 = { sizeof (AllMembershipCondition_t198137363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable949[1] = 
{
	AllMembershipCondition_t198137363::get_offset_of_version_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize950 = { sizeof (ApplicationTrust_t3270368423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable950[6] = 
{
	ApplicationTrust_t3270368423::get_offset_of__appid_0(),
	ApplicationTrust_t3270368423::get_offset_of__defaultPolicy_1(),
	ApplicationTrust_t3270368423::get_offset_of__xtranfo_2(),
	ApplicationTrust_t3270368423::get_offset_of__trustrun_3(),
	ApplicationTrust_t3270368423::get_offset_of__persist_4(),
	ApplicationTrust_t3270368423::get_offset_of_fullTrustAssemblies_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize951 = { sizeof (CodeConnectAccess_t1103527196), -1, sizeof(CodeConnectAccess_t1103527196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable951[6] = 
{
	CodeConnectAccess_t1103527196_StaticFields::get_offset_of_AnyScheme_0(),
	CodeConnectAccess_t1103527196_StaticFields::get_offset_of_DefaultPort_1(),
	CodeConnectAccess_t1103527196_StaticFields::get_offset_of_OriginPort_2(),
	CodeConnectAccess_t1103527196_StaticFields::get_offset_of_OriginScheme_3(),
	CodeConnectAccess_t1103527196::get_offset_of__scheme_4(),
	CodeConnectAccess_t1103527196::get_offset_of__port_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize952 = { sizeof (CodeGroup_t3811807446), -1, sizeof(CodeGroup_t3811807446_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable952[6] = 
{
	CodeGroup_t3811807446::get_offset_of_m_policy_0(),
	CodeGroup_t3811807446::get_offset_of_m_membershipCondition_1(),
	CodeGroup_t3811807446::get_offset_of_m_description_2(),
	CodeGroup_t3811807446::get_offset_of_m_name_3(),
	CodeGroup_t3811807446::get_offset_of_m_children_4(),
	CodeGroup_t3811807446_StaticFields::get_offset_of_U3CU3Ef__switchU24map2E_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize953 = { sizeof (DefaultPolicies_t2520506789), -1, sizeof(DefaultPolicies_t2520506789_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable953[12] = 
{
	DefaultPolicies_t2520506789_StaticFields::get_offset_of__fxVersion_0(),
	DefaultPolicies_t2520506789_StaticFields::get_offset_of__ecmaKey_1(),
	DefaultPolicies_t2520506789_StaticFields::get_offset_of__ecma_2(),
	DefaultPolicies_t2520506789_StaticFields::get_offset_of__msFinalKey_3(),
	DefaultPolicies_t2520506789_StaticFields::get_offset_of__msFinal_4(),
	DefaultPolicies_t2520506789_StaticFields::get_offset_of__fullTrust_5(),
	DefaultPolicies_t2520506789_StaticFields::get_offset_of__localIntranet_6(),
	DefaultPolicies_t2520506789_StaticFields::get_offset_of__internet_7(),
	DefaultPolicies_t2520506789_StaticFields::get_offset_of__skipVerification_8(),
	DefaultPolicies_t2520506789_StaticFields::get_offset_of__execution_9(),
	DefaultPolicies_t2520506789_StaticFields::get_offset_of__nothing_10(),
	DefaultPolicies_t2520506789_StaticFields::get_offset_of__everything_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize954 = { sizeof (Key_t3006169375)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable954[3] = 
{
	Key_t3006169375::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize955 = { sizeof (Evidence_t2008144148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable955[4] = 
{
	Evidence_t2008144148::get_offset_of__locked_0(),
	Evidence_t2008144148::get_offset_of_hostEvidenceList_1(),
	Evidence_t2008144148::get_offset_of_assemblyEvidenceList_2(),
	Evidence_t2008144148::get_offset_of__hashCode_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize956 = { sizeof (EvidenceEnumerator_t1708166667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable956[3] = 
{
	EvidenceEnumerator_t1708166667::get_offset_of_currentEnum_0(),
	EvidenceEnumerator_t1708166667::get_offset_of_hostEnum_1(),
	EvidenceEnumerator_t1708166667::get_offset_of_assemblyEnum_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize957 = { sizeof (FileCodeGroup_t1720965944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable957[1] = 
{
	FileCodeGroup_t1720965944::get_offset_of_m_access_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize958 = { sizeof (FirstMatchCodeGroup_t885469689), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize959 = { sizeof (GacInstalled_t3565883570), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize960 = { sizeof (Hash_t5925575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable960[2] = 
{
	Hash_t5925575::get_offset_of_assembly_0(),
	Hash_t5925575::get_offset_of_data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize961 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize962 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize963 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize964 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize965 = { sizeof (MembershipConditionHelper_t2246572704), -1, sizeof(MembershipConditionHelper_t2246572704_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable965[1] = 
{
	MembershipConditionHelper_t2246572704_StaticFields::get_offset_of_XmlTag_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize966 = { sizeof (NetCodeGroup_t2217812384), -1, sizeof(NetCodeGroup_t2217812384_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable966[4] = 
{
	NetCodeGroup_t2217812384_StaticFields::get_offset_of_AbsentOriginScheme_6(),
	NetCodeGroup_t2217812384_StaticFields::get_offset_of_AnyOtherOriginScheme_7(),
	NetCodeGroup_t2217812384::get_offset_of__rules_8(),
	NetCodeGroup_t2217812384::get_offset_of__hashcode_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize967 = { sizeof (PermissionRequestEvidence_t59447972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable967[3] = 
{
	PermissionRequestEvidence_t59447972::get_offset_of_requested_0(),
	PermissionRequestEvidence_t59447972::get_offset_of_optional_1(),
	PermissionRequestEvidence_t59447972::get_offset_of_denied_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize968 = { sizeof (PolicyException_t1520028310), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize969 = { sizeof (PolicyLevel_t2891196107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable969[8] = 
{
	PolicyLevel_t2891196107::get_offset_of_label_0(),
	PolicyLevel_t2891196107::get_offset_of_root_code_group_1(),
	PolicyLevel_t2891196107::get_offset_of_full_trust_assemblies_2(),
	PolicyLevel_t2891196107::get_offset_of_named_permission_sets_3(),
	PolicyLevel_t2891196107::get_offset_of__location_4(),
	PolicyLevel_t2891196107::get_offset_of__type_5(),
	PolicyLevel_t2891196107::get_offset_of_fullNames_6(),
	PolicyLevel_t2891196107::get_offset_of_xml_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize970 = { sizeof (PolicyStatement_t3052133691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable970[2] = 
{
	PolicyStatement_t3052133691::get_offset_of_perms_0(),
	PolicyStatement_t3052133691::get_offset_of_attrs_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize971 = { sizeof (PolicyStatementAttribute_t1674167676)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable971[5] = 
{
	PolicyStatementAttribute_t1674167676::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize972 = { sizeof (Publisher_t2758056332), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable972[1] = 
{
	Publisher_t2758056332::get_offset_of_m_cert_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize973 = { sizeof (Site_t1075497104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable973[1] = 
{
	Site_t1075497104::get_offset_of_origin_site_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize974 = { sizeof (StrongName_t3675724614), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable974[3] = 
{
	StrongName_t3675724614::get_offset_of_publickey_0(),
	StrongName_t3675724614::get_offset_of_name_1(),
	StrongName_t3675724614::get_offset_of_version_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize975 = { sizeof (StrongNameMembershipCondition_t2614563360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable975[4] = 
{
	StrongNameMembershipCondition_t2614563360::get_offset_of_version_0(),
	StrongNameMembershipCondition_t2614563360::get_offset_of_blob_1(),
	StrongNameMembershipCondition_t2614563360::get_offset_of_name_2(),
	StrongNameMembershipCondition_t2614563360::get_offset_of_assemblyVersion_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize976 = { sizeof (UnionCodeGroup_t3773268997), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize977 = { sizeof (Url_t207802215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable977[1] = 
{
	Url_t207802215::get_offset_of_origin_url_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize978 = { sizeof (Zone_t2011285646), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable978[1] = 
{
	Zone_t2011285646::get_offset_of_zone_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize979 = { sizeof (ZoneMembershipCondition_t3195636716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable979[2] = 
{
	ZoneMembershipCondition_t3195636716::get_offset_of_version_0(),
	ZoneMembershipCondition_t3195636716::get_offset_of_zone_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize980 = { sizeof (GenericIdentity_t2319019448), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable980[2] = 
{
	GenericIdentity_t2319019448::get_offset_of_m_name_0(),
	GenericIdentity_t2319019448::get_offset_of_m_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize981 = { sizeof (GenericPrincipal_t2520297622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable981[2] = 
{
	GenericPrincipal_t2520297622::get_offset_of_m_identity_0(),
	GenericPrincipal_t2520297622::get_offset_of_m_roles_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize982 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize983 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize984 = { sizeof (PrincipalPolicy_t1761212333)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable984[4] = 
{
	PrincipalPolicy_t1761212333::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize985 = { sizeof (TokenImpersonationLevel_t3773270939)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable985[6] = 
{
	TokenImpersonationLevel_t3773270939::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize986 = { sizeof (WindowsAccountType_t2283000883)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable986[5] = 
{
	WindowsAccountType_t2283000883::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize987 = { sizeof (WindowsIdentity_t2948242406), -1, sizeof(WindowsIdentity_t2948242406_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable987[7] = 
{
	WindowsIdentity_t2948242406::get_offset_of__token_0(),
	WindowsIdentity_t2948242406::get_offset_of__type_1(),
	WindowsIdentity_t2948242406::get_offset_of__account_2(),
	WindowsIdentity_t2948242406::get_offset_of__authenticated_3(),
	WindowsIdentity_t2948242406::get_offset_of__name_4(),
	WindowsIdentity_t2948242406::get_offset_of__info_5(),
	WindowsIdentity_t2948242406_StaticFields::get_offset_of_invalidWindows_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize988 = { sizeof (WindowsPrincipal_t239041500), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable988[1] = 
{
	WindowsPrincipal_t239041500::get_offset_of__identity_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize989 = { sizeof (ASCIIEncoding_t3446586211), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize990 = { sizeof (Decoder_t2204182725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable990[2] = 
{
	Decoder_t2204182725::get_offset_of_fallback_0(),
	Decoder_t2204182725::get_offset_of_fallback_buffer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize991 = { sizeof (DecoderExceptionFallback_t3981484394), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize992 = { sizeof (DecoderExceptionFallbackBuffer_t90938522), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize993 = { sizeof (DecoderFallback_t3123823036), -1, sizeof(DecoderFallback_t3123823036_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable993[3] = 
{
	DecoderFallback_t3123823036_StaticFields::get_offset_of_exception_fallback_0(),
	DecoderFallback_t3123823036_StaticFields::get_offset_of_replacement_fallback_1(),
	DecoderFallback_t3123823036_StaticFields::get_offset_of_standard_safe_fallback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize994 = { sizeof (DecoderFallbackBuffer_t2402303981), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize995 = { sizeof (DecoderFallbackException_t1661362184), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable995[2] = 
{
	DecoderFallbackException_t1661362184::get_offset_of_bytes_unknown_13(),
	DecoderFallbackException_t1661362184::get_offset_of_index_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize996 = { sizeof (DecoderReplacementFallback_t1462101135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable996[1] = 
{
	DecoderReplacementFallback_t1462101135::get_offset_of_replacement_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize997 = { sizeof (DecoderReplacementFallbackBuffer_t841144779), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable997[3] = 
{
	DecoderReplacementFallbackBuffer_t841144779::get_offset_of_fallback_assigned_0(),
	DecoderReplacementFallbackBuffer_t841144779::get_offset_of_current_1(),
	DecoderReplacementFallbackBuffer_t841144779::get_offset_of_replacement_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize998 = { sizeof (EncoderExceptionFallback_t1243849599), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize999 = { sizeof (EncoderExceptionFallbackBuffer_t3597232471), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
