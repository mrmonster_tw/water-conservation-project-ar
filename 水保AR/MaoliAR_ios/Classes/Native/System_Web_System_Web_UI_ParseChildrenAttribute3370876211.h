﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"

// System.String
struct String_t;
// System.Web.UI.ParseChildrenAttribute
struct ParseChildrenAttribute_t3370876211;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.ParseChildrenAttribute
struct  ParseChildrenAttribute_t3370876211  : public Attribute_t861562559
{
public:
	// System.Boolean System.Web.UI.ParseChildrenAttribute::childrenAsProperties
	bool ___childrenAsProperties_0;
	// System.String System.Web.UI.ParseChildrenAttribute::defaultProperty
	String_t* ___defaultProperty_1;
	// System.Type System.Web.UI.ParseChildrenAttribute::childType
	Type_t * ___childType_5;

public:
	inline static int32_t get_offset_of_childrenAsProperties_0() { return static_cast<int32_t>(offsetof(ParseChildrenAttribute_t3370876211, ___childrenAsProperties_0)); }
	inline bool get_childrenAsProperties_0() const { return ___childrenAsProperties_0; }
	inline bool* get_address_of_childrenAsProperties_0() { return &___childrenAsProperties_0; }
	inline void set_childrenAsProperties_0(bool value)
	{
		___childrenAsProperties_0 = value;
	}

	inline static int32_t get_offset_of_defaultProperty_1() { return static_cast<int32_t>(offsetof(ParseChildrenAttribute_t3370876211, ___defaultProperty_1)); }
	inline String_t* get_defaultProperty_1() const { return ___defaultProperty_1; }
	inline String_t** get_address_of_defaultProperty_1() { return &___defaultProperty_1; }
	inline void set_defaultProperty_1(String_t* value)
	{
		___defaultProperty_1 = value;
		Il2CppCodeGenWriteBarrier(&___defaultProperty_1, value);
	}

	inline static int32_t get_offset_of_childType_5() { return static_cast<int32_t>(offsetof(ParseChildrenAttribute_t3370876211, ___childType_5)); }
	inline Type_t * get_childType_5() const { return ___childType_5; }
	inline Type_t ** get_address_of_childType_5() { return &___childType_5; }
	inline void set_childType_5(Type_t * value)
	{
		___childType_5 = value;
		Il2CppCodeGenWriteBarrier(&___childType_5, value);
	}
};

struct ParseChildrenAttribute_t3370876211_StaticFields
{
public:
	// System.Web.UI.ParseChildrenAttribute System.Web.UI.ParseChildrenAttribute::Default
	ParseChildrenAttribute_t3370876211 * ___Default_2;
	// System.Web.UI.ParseChildrenAttribute System.Web.UI.ParseChildrenAttribute::ParseAsChildren
	ParseChildrenAttribute_t3370876211 * ___ParseAsChildren_3;
	// System.Web.UI.ParseChildrenAttribute System.Web.UI.ParseChildrenAttribute::ParseAsProperties
	ParseChildrenAttribute_t3370876211 * ___ParseAsProperties_4;

public:
	inline static int32_t get_offset_of_Default_2() { return static_cast<int32_t>(offsetof(ParseChildrenAttribute_t3370876211_StaticFields, ___Default_2)); }
	inline ParseChildrenAttribute_t3370876211 * get_Default_2() const { return ___Default_2; }
	inline ParseChildrenAttribute_t3370876211 ** get_address_of_Default_2() { return &___Default_2; }
	inline void set_Default_2(ParseChildrenAttribute_t3370876211 * value)
	{
		___Default_2 = value;
		Il2CppCodeGenWriteBarrier(&___Default_2, value);
	}

	inline static int32_t get_offset_of_ParseAsChildren_3() { return static_cast<int32_t>(offsetof(ParseChildrenAttribute_t3370876211_StaticFields, ___ParseAsChildren_3)); }
	inline ParseChildrenAttribute_t3370876211 * get_ParseAsChildren_3() const { return ___ParseAsChildren_3; }
	inline ParseChildrenAttribute_t3370876211 ** get_address_of_ParseAsChildren_3() { return &___ParseAsChildren_3; }
	inline void set_ParseAsChildren_3(ParseChildrenAttribute_t3370876211 * value)
	{
		___ParseAsChildren_3 = value;
		Il2CppCodeGenWriteBarrier(&___ParseAsChildren_3, value);
	}

	inline static int32_t get_offset_of_ParseAsProperties_4() { return static_cast<int32_t>(offsetof(ParseChildrenAttribute_t3370876211_StaticFields, ___ParseAsProperties_4)); }
	inline ParseChildrenAttribute_t3370876211 * get_ParseAsProperties_4() const { return ___ParseAsProperties_4; }
	inline ParseChildrenAttribute_t3370876211 ** get_address_of_ParseAsProperties_4() { return &___ParseAsProperties_4; }
	inline void set_ParseAsProperties_4(ParseChildrenAttribute_t3370876211 * value)
	{
		___ParseAsProperties_4 = value;
		Il2CppCodeGenWriteBarrier(&___ParseAsProperties_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
