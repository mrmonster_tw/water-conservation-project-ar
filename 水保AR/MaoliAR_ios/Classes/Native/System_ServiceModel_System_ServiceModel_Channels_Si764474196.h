﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_M2736340907.h"

// System.ServiceModel.Channels.BodyWriter
struct BodyWriter_t3441673553;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.SimpleMessage
struct  SimpleMessage_t764474196  : public MessageImplBase_t2736340907
{
public:
	// System.ServiceModel.Channels.BodyWriter System.ServiceModel.Channels.SimpleMessage::body
	BodyWriter_t3441673553 * ___body_5;
	// System.Boolean System.ServiceModel.Channels.SimpleMessage::is_fault
	bool ___is_fault_6;

public:
	inline static int32_t get_offset_of_body_5() { return static_cast<int32_t>(offsetof(SimpleMessage_t764474196, ___body_5)); }
	inline BodyWriter_t3441673553 * get_body_5() const { return ___body_5; }
	inline BodyWriter_t3441673553 ** get_address_of_body_5() { return &___body_5; }
	inline void set_body_5(BodyWriter_t3441673553 * value)
	{
		___body_5 = value;
		Il2CppCodeGenWriteBarrier(&___body_5, value);
	}

	inline static int32_t get_offset_of_is_fault_6() { return static_cast<int32_t>(offsetof(SimpleMessage_t764474196, ___is_fault_6)); }
	inline bool get_is_fault_6() const { return ___is_fault_6; }
	inline bool* get_address_of_is_fault_6() { return &___is_fault_6; }
	inline void set_is_fault_6(bool value)
	{
		___is_fault_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
