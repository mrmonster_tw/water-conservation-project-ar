﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.FederatedMessageSecurityOverHttp
struct  FederatedMessageSecurityOverHttp_t2993240700  : public Il2CppObject
{
public:
	// System.Boolean System.ServiceModel.FederatedMessageSecurityOverHttp::negotiate
	bool ___negotiate_0;

public:
	inline static int32_t get_offset_of_negotiate_0() { return static_cast<int32_t>(offsetof(FederatedMessageSecurityOverHttp_t2993240700, ___negotiate_0)); }
	inline bool get_negotiate_0() const { return ___negotiate_0; }
	inline bool* get_address_of_negotiate_0() { return &___negotiate_0; }
	inline void set_negotiate_0(bool value)
	{
		___negotiate_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
