﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// Tools
struct Tools_t613821292;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tools
struct  Tools_t613821292  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Tools::resetMemory
	bool ___resetMemory_3;

public:
	inline static int32_t get_offset_of_resetMemory_3() { return static_cast<int32_t>(offsetof(Tools_t613821292, ___resetMemory_3)); }
	inline bool get_resetMemory_3() const { return ___resetMemory_3; }
	inline bool* get_address_of_resetMemory_3() { return &___resetMemory_3; }
	inline void set_resetMemory_3(bool value)
	{
		___resetMemory_3 = value;
	}
};

struct Tools_t613821292_StaticFields
{
public:
	// Tools Tools::This
	Tools_t613821292 * ___This_2;

public:
	inline static int32_t get_offset_of_This_2() { return static_cast<int32_t>(offsetof(Tools_t613821292_StaticFields, ___This_2)); }
	inline Tools_t613821292 * get_This_2() const { return ___This_2; }
	inline Tools_t613821292 ** get_address_of_This_2() { return &___This_2; }
	inline void set_This_2(Tools_t613821292 * value)
	{
		___This_2 = value;
		Il2CppCodeGenWriteBarrier(&___This_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
