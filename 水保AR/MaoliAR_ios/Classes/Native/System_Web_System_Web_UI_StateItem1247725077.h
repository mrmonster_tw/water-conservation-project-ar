﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.StateItem
struct  StateItem_t1247725077  : public Il2CppObject
{
public:
	// System.Boolean System.Web.UI.StateItem::_isDirty
	bool ____isDirty_0;
	// System.Object System.Web.UI.StateItem::_value
	Il2CppObject * ____value_1;

public:
	inline static int32_t get_offset_of__isDirty_0() { return static_cast<int32_t>(offsetof(StateItem_t1247725077, ____isDirty_0)); }
	inline bool get__isDirty_0() const { return ____isDirty_0; }
	inline bool* get_address_of__isDirty_0() { return &____isDirty_0; }
	inline void set__isDirty_0(bool value)
	{
		____isDirty_0 = value;
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(StateItem_t1247725077, ____value_1)); }
	inline Il2CppObject * get__value_1() const { return ____value_1; }
	inline Il2CppObject ** get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(Il2CppObject * value)
	{
		____value_1 = value;
		Il2CppCodeGenWriteBarrier(&____value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
