﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Configuration.SoapEnvelopeProcessingElement
struct  SoapEnvelopeProcessingElement_t432830953  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct SoapEnvelopeProcessingElement_t432830953_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Web.Services.Configuration.SoapEnvelopeProcessingElement::strictProp
	ConfigurationProperty_t3590861854 * ___strictProp_13;
	// System.Configuration.ConfigurationProperty System.Web.Services.Configuration.SoapEnvelopeProcessingElement::readTimeoutProp
	ConfigurationProperty_t3590861854 * ___readTimeoutProp_14;
	// System.Configuration.ConfigurationPropertyCollection System.Web.Services.Configuration.SoapEnvelopeProcessingElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_15;

public:
	inline static int32_t get_offset_of_strictProp_13() { return static_cast<int32_t>(offsetof(SoapEnvelopeProcessingElement_t432830953_StaticFields, ___strictProp_13)); }
	inline ConfigurationProperty_t3590861854 * get_strictProp_13() const { return ___strictProp_13; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_strictProp_13() { return &___strictProp_13; }
	inline void set_strictProp_13(ConfigurationProperty_t3590861854 * value)
	{
		___strictProp_13 = value;
		Il2CppCodeGenWriteBarrier(&___strictProp_13, value);
	}

	inline static int32_t get_offset_of_readTimeoutProp_14() { return static_cast<int32_t>(offsetof(SoapEnvelopeProcessingElement_t432830953_StaticFields, ___readTimeoutProp_14)); }
	inline ConfigurationProperty_t3590861854 * get_readTimeoutProp_14() const { return ___readTimeoutProp_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_readTimeoutProp_14() { return &___readTimeoutProp_14; }
	inline void set_readTimeoutProp_14(ConfigurationProperty_t3590861854 * value)
	{
		___readTimeoutProp_14 = value;
		Il2CppCodeGenWriteBarrier(&___readTimeoutProp_14, value);
	}

	inline static int32_t get_offset_of_properties_15() { return static_cast<int32_t>(offsetof(SoapEnvelopeProcessingElement_t432830953_StaticFields, ___properties_15)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_15() const { return ___properties_15; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_15() { return &___properties_15; }
	inline void set_properties_15(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_15 = value;
		Il2CppCodeGenWriteBarrier(&___properties_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
