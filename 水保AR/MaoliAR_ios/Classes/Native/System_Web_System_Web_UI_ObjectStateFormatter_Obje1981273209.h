﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Web.UI.ObjectStateFormatter/ObjectFormatter[]
struct ObjectFormatterU5BU5D_t3573634436;
// System.Web.UI.ObjectStateFormatter/BinaryObjectFormatter
struct BinaryObjectFormatter_t2045863156;
// System.Web.UI.ObjectStateFormatter/TypeFormatter
struct TypeFormatter_t1484190866;
// System.Web.UI.ObjectStateFormatter/EnumFormatter
struct EnumFormatter_t1983866999;
// System.Web.UI.ObjectStateFormatter/SingleRankArrayFormatter
struct SingleRankArrayFormatter_t1493189349;
// System.Web.UI.ObjectStateFormatter/TypeConverterFormatter
struct TypeConverterFormatter_t1198826992;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.ObjectStateFormatter/ObjectFormatter
struct  ObjectFormatter_t1981273209  : public Il2CppObject
{
public:
	// System.Byte System.Web.UI.ObjectStateFormatter/ObjectFormatter::PrimaryId
	uint8_t ___PrimaryId_8;
	// System.Byte System.Web.UI.ObjectStateFormatter/ObjectFormatter::SecondaryId
	uint8_t ___SecondaryId_9;
	// System.Byte System.Web.UI.ObjectStateFormatter/ObjectFormatter::TertiaryId
	uint8_t ___TertiaryId_10;

public:
	inline static int32_t get_offset_of_PrimaryId_8() { return static_cast<int32_t>(offsetof(ObjectFormatter_t1981273209, ___PrimaryId_8)); }
	inline uint8_t get_PrimaryId_8() const { return ___PrimaryId_8; }
	inline uint8_t* get_address_of_PrimaryId_8() { return &___PrimaryId_8; }
	inline void set_PrimaryId_8(uint8_t value)
	{
		___PrimaryId_8 = value;
	}

	inline static int32_t get_offset_of_SecondaryId_9() { return static_cast<int32_t>(offsetof(ObjectFormatter_t1981273209, ___SecondaryId_9)); }
	inline uint8_t get_SecondaryId_9() const { return ___SecondaryId_9; }
	inline uint8_t* get_address_of_SecondaryId_9() { return &___SecondaryId_9; }
	inline void set_SecondaryId_9(uint8_t value)
	{
		___SecondaryId_9 = value;
	}

	inline static int32_t get_offset_of_TertiaryId_10() { return static_cast<int32_t>(offsetof(ObjectFormatter_t1981273209, ___TertiaryId_10)); }
	inline uint8_t get_TertiaryId_10() const { return ___TertiaryId_10; }
	inline uint8_t* get_address_of_TertiaryId_10() { return &___TertiaryId_10; }
	inline void set_TertiaryId_10(uint8_t value)
	{
		___TertiaryId_10 = value;
	}
};

struct ObjectFormatter_t1981273209_StaticFields
{
public:
	// System.Collections.Hashtable System.Web.UI.ObjectStateFormatter/ObjectFormatter::writeMap
	Hashtable_t1853889766 * ___writeMap_0;
	// System.Web.UI.ObjectStateFormatter/ObjectFormatter[] System.Web.UI.ObjectStateFormatter/ObjectFormatter::readMap
	ObjectFormatterU5BU5D_t3573634436* ___readMap_1;
	// System.Web.UI.ObjectStateFormatter/BinaryObjectFormatter System.Web.UI.ObjectStateFormatter/ObjectFormatter::binaryObjectFormatter
	BinaryObjectFormatter_t2045863156 * ___binaryObjectFormatter_2;
	// System.Web.UI.ObjectStateFormatter/TypeFormatter System.Web.UI.ObjectStateFormatter/ObjectFormatter::typeFormatter
	TypeFormatter_t1484190866 * ___typeFormatter_3;
	// System.Web.UI.ObjectStateFormatter/EnumFormatter System.Web.UI.ObjectStateFormatter/ObjectFormatter::enumFormatter
	EnumFormatter_t1983866999 * ___enumFormatter_4;
	// System.Web.UI.ObjectStateFormatter/SingleRankArrayFormatter System.Web.UI.ObjectStateFormatter/ObjectFormatter::singleRankArrayFormatter
	SingleRankArrayFormatter_t1493189349 * ___singleRankArrayFormatter_5;
	// System.Web.UI.ObjectStateFormatter/TypeConverterFormatter System.Web.UI.ObjectStateFormatter/ObjectFormatter::typeConverterFormatter
	TypeConverterFormatter_t1198826992 * ___typeConverterFormatter_6;
	// System.Byte System.Web.UI.ObjectStateFormatter/ObjectFormatter::nextId
	uint8_t ___nextId_7;

public:
	inline static int32_t get_offset_of_writeMap_0() { return static_cast<int32_t>(offsetof(ObjectFormatter_t1981273209_StaticFields, ___writeMap_0)); }
	inline Hashtable_t1853889766 * get_writeMap_0() const { return ___writeMap_0; }
	inline Hashtable_t1853889766 ** get_address_of_writeMap_0() { return &___writeMap_0; }
	inline void set_writeMap_0(Hashtable_t1853889766 * value)
	{
		___writeMap_0 = value;
		Il2CppCodeGenWriteBarrier(&___writeMap_0, value);
	}

	inline static int32_t get_offset_of_readMap_1() { return static_cast<int32_t>(offsetof(ObjectFormatter_t1981273209_StaticFields, ___readMap_1)); }
	inline ObjectFormatterU5BU5D_t3573634436* get_readMap_1() const { return ___readMap_1; }
	inline ObjectFormatterU5BU5D_t3573634436** get_address_of_readMap_1() { return &___readMap_1; }
	inline void set_readMap_1(ObjectFormatterU5BU5D_t3573634436* value)
	{
		___readMap_1 = value;
		Il2CppCodeGenWriteBarrier(&___readMap_1, value);
	}

	inline static int32_t get_offset_of_binaryObjectFormatter_2() { return static_cast<int32_t>(offsetof(ObjectFormatter_t1981273209_StaticFields, ___binaryObjectFormatter_2)); }
	inline BinaryObjectFormatter_t2045863156 * get_binaryObjectFormatter_2() const { return ___binaryObjectFormatter_2; }
	inline BinaryObjectFormatter_t2045863156 ** get_address_of_binaryObjectFormatter_2() { return &___binaryObjectFormatter_2; }
	inline void set_binaryObjectFormatter_2(BinaryObjectFormatter_t2045863156 * value)
	{
		___binaryObjectFormatter_2 = value;
		Il2CppCodeGenWriteBarrier(&___binaryObjectFormatter_2, value);
	}

	inline static int32_t get_offset_of_typeFormatter_3() { return static_cast<int32_t>(offsetof(ObjectFormatter_t1981273209_StaticFields, ___typeFormatter_3)); }
	inline TypeFormatter_t1484190866 * get_typeFormatter_3() const { return ___typeFormatter_3; }
	inline TypeFormatter_t1484190866 ** get_address_of_typeFormatter_3() { return &___typeFormatter_3; }
	inline void set_typeFormatter_3(TypeFormatter_t1484190866 * value)
	{
		___typeFormatter_3 = value;
		Il2CppCodeGenWriteBarrier(&___typeFormatter_3, value);
	}

	inline static int32_t get_offset_of_enumFormatter_4() { return static_cast<int32_t>(offsetof(ObjectFormatter_t1981273209_StaticFields, ___enumFormatter_4)); }
	inline EnumFormatter_t1983866999 * get_enumFormatter_4() const { return ___enumFormatter_4; }
	inline EnumFormatter_t1983866999 ** get_address_of_enumFormatter_4() { return &___enumFormatter_4; }
	inline void set_enumFormatter_4(EnumFormatter_t1983866999 * value)
	{
		___enumFormatter_4 = value;
		Il2CppCodeGenWriteBarrier(&___enumFormatter_4, value);
	}

	inline static int32_t get_offset_of_singleRankArrayFormatter_5() { return static_cast<int32_t>(offsetof(ObjectFormatter_t1981273209_StaticFields, ___singleRankArrayFormatter_5)); }
	inline SingleRankArrayFormatter_t1493189349 * get_singleRankArrayFormatter_5() const { return ___singleRankArrayFormatter_5; }
	inline SingleRankArrayFormatter_t1493189349 ** get_address_of_singleRankArrayFormatter_5() { return &___singleRankArrayFormatter_5; }
	inline void set_singleRankArrayFormatter_5(SingleRankArrayFormatter_t1493189349 * value)
	{
		___singleRankArrayFormatter_5 = value;
		Il2CppCodeGenWriteBarrier(&___singleRankArrayFormatter_5, value);
	}

	inline static int32_t get_offset_of_typeConverterFormatter_6() { return static_cast<int32_t>(offsetof(ObjectFormatter_t1981273209_StaticFields, ___typeConverterFormatter_6)); }
	inline TypeConverterFormatter_t1198826992 * get_typeConverterFormatter_6() const { return ___typeConverterFormatter_6; }
	inline TypeConverterFormatter_t1198826992 ** get_address_of_typeConverterFormatter_6() { return &___typeConverterFormatter_6; }
	inline void set_typeConverterFormatter_6(TypeConverterFormatter_t1198826992 * value)
	{
		___typeConverterFormatter_6 = value;
		Il2CppCodeGenWriteBarrier(&___typeConverterFormatter_6, value);
	}

	inline static int32_t get_offset_of_nextId_7() { return static_cast<int32_t>(offsetof(ObjectFormatter_t1981273209_StaticFields, ___nextId_7)); }
	inline uint8_t get_nextId_7() const { return ___nextId_7; }
	inline uint8_t* get_address_of_nextId_7() { return &___nextId_7; }
	inline void set_nextId_7(uint8_t value)
	{
		___nextId_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
