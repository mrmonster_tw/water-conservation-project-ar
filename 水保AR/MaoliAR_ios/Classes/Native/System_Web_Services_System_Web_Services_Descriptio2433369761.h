﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_Services_System_Web_Services_Descriptio3177955060.h"
#include "System_Web_Services_System_Web_Services_Descriptio3254587761.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.SoapBodyBinding
struct  SoapBodyBinding_t2433369761  : public ServiceDescriptionFormatExtension_t3177955060
{
public:
	// System.String System.Web.Services.Description.SoapBodyBinding::ns
	String_t* ___ns_1;
	// System.Web.Services.Description.SoapBindingUse System.Web.Services.Description.SoapBodyBinding::use
	int32_t ___use_2;

public:
	inline static int32_t get_offset_of_ns_1() { return static_cast<int32_t>(offsetof(SoapBodyBinding_t2433369761, ___ns_1)); }
	inline String_t* get_ns_1() const { return ___ns_1; }
	inline String_t** get_address_of_ns_1() { return &___ns_1; }
	inline void set_ns_1(String_t* value)
	{
		___ns_1 = value;
		Il2CppCodeGenWriteBarrier(&___ns_1, value);
	}

	inline static int32_t get_offset_of_use_2() { return static_cast<int32_t>(offsetof(SoapBodyBinding_t2433369761, ___use_2)); }
	inline int32_t get_use_2() const { return ___use_2; }
	inline int32_t* get_address_of_use_2() { return &___use_2; }
	inline void set_use_2(int32_t value)
	{
		___use_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
