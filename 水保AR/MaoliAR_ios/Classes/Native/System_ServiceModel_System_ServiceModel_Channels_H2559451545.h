﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Ht555124713.h"

// System.Collections.Generic.Dictionary`2<System.Uri,System.Net.HttpListener>
struct Dictionary_2_t165365620;
// System.Net.HttpListener
struct HttpListener_t988452056;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.HttpSimpleListenerManager
struct  HttpSimpleListenerManager_t2559451545  : public HttpListenerManager_t555124713
{
public:
	// System.Net.HttpListener System.ServiceModel.Channels.HttpSimpleListenerManager::http_listener
	HttpListener_t988452056 * ___http_listener_8;

public:
	inline static int32_t get_offset_of_http_listener_8() { return static_cast<int32_t>(offsetof(HttpSimpleListenerManager_t2559451545, ___http_listener_8)); }
	inline HttpListener_t988452056 * get_http_listener_8() const { return ___http_listener_8; }
	inline HttpListener_t988452056 ** get_address_of_http_listener_8() { return &___http_listener_8; }
	inline void set_http_listener_8(HttpListener_t988452056 * value)
	{
		___http_listener_8 = value;
		Il2CppCodeGenWriteBarrier(&___http_listener_8, value);
	}
};

struct HttpSimpleListenerManager_t2559451545_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Uri,System.Net.HttpListener> System.ServiceModel.Channels.HttpSimpleListenerManager::opened_listeners
	Dictionary_2_t165365620 * ___opened_listeners_7;

public:
	inline static int32_t get_offset_of_opened_listeners_7() { return static_cast<int32_t>(offsetof(HttpSimpleListenerManager_t2559451545_StaticFields, ___opened_listeners_7)); }
	inline Dictionary_2_t165365620 * get_opened_listeners_7() const { return ___opened_listeners_7; }
	inline Dictionary_2_t165365620 ** get_address_of_opened_listeners_7() { return &___opened_listeners_7; }
	inline void set_opened_listeners_7(Dictionary_2_t165365620 * value)
	{
		___opened_listeners_7 = value;
		Il2CppCodeGenWriteBarrier(&___opened_listeners_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
