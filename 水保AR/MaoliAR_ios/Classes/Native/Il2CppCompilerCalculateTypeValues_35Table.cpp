﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Web_System_Web_UI_IDReferencePropertyAttrib1719921500.h"
#include "System_Web_System_Web_UI_ImageClickEventArgs1740531002.h"
#include "System_Web_System_Web_UI_IndexedString3379624379.h"
#include "System_Web_System_Web_UI_LiteralControl249601277.h"
#include "System_Web_System_Web_UI_LosFormatter2034865563.h"
#include "System_Web_System_Web_UI_MasterPage1483330027.h"
#include "System_Web_System_Web_UI_MasterPageControlBuilder1194179076.h"
#include "System_Web_System_Web_UI_MasterPageParser2534398727.h"
#include "System_Web_System_Web_UI_MinimizableAttributeTypeCo982439845.h"
#include "System_Web_System_Web_UI_ObjectStateFormatter1543808709.h"
#include "System_Web_System_Web_UI_ObjectStateFormatter_Writ4048875981.h"
#include "System_Web_System_Web_UI_ObjectStateFormatter_Read3753404210.h"
#include "System_Web_System_Web_UI_ObjectStateFormatter_Obje1981273209.h"
#include "System_Web_System_Web_UI_ObjectStateFormatter_Stri3828688682.h"
#include "System_Web_System_Web_UI_ObjectStateFormatter_Index421145034.h"
#include "System_Web_System_Web_UI_ObjectStateFormatter_Int64181992990.h"
#include "System_Web_System_Web_UI_ObjectStateFormatter_Int34180763861.h"
#include "System_Web_System_Web_UI_ObjectStateFormatter_Int11075651843.h"
#include "System_Web_System_Web_UI_ObjectStateFormatter_Byte2521617483.h"
#include "System_Web_System_Web_UI_ObjectStateFormatter_Bool3201589112.h"
#include "System_Web_System_Web_UI_ObjectStateFormatter_CharF597432637.h"
#include "System_Web_System_Web_UI_ObjectStateFormatter_Date3698563052.h"
#include "System_Web_System_Web_UI_ObjectStateFormatter_PairF188360010.h"
#include "System_Web_System_Web_UI_ObjectStateFormatter_Tripl732666697.h"
#include "System_Web_System_Web_UI_ObjectStateFormatter_Array349177227.h"
#include "System_Web_System_Web_UI_ObjectStateFormatter_Hash2187487685.h"
#include "System_Web_System_Web_UI_ObjectStateFormatter_Obje1799921284.h"
#include "System_Web_System_Web_UI_ObjectStateFormatter_Colo1525993769.h"
#include "System_Web_System_Web_UI_ObjectStateFormatter_Enum1983866999.h"
#include "System_Web_System_Web_UI_ObjectStateFormatter_Type1484190866.h"
#include "System_Web_System_Web_UI_ObjectStateFormatter_Sing1493189349.h"
#include "System_Web_System_Web_UI_ObjectStateFormatter_Font2452266376.h"
#include "System_Web_System_Web_UI_ObjectStateFormatter_Unit2363462124.h"
#include "System_Web_System_Web_UI_ObjectStateFormatter_Type1198826992.h"
#include "System_Web_System_Web_UI_ObjectStateFormatter_Bina2045863156.h"
#include "System_Web_System_Web_UI_ObjectTagBuilder2708400301.h"
#include "System_Web_System_Web_UI_ObjectTag1714262980.h"
#include "System_Web_System_Web_UI_OutputCacheLocation1771607847.h"
#include "System_Web_System_Web_UI_Page770747966.h"
#include "System_Web_System_Web_UI_PageAsyncTask4073791294.h"
#include "System_Web_System_Web_UI_PageParser2342141287.h"
#include "System_Web_System_Web_UI_PageParserFilter169228355.h"
#include "System_Web_System_Web_UI_PageStatePersister2741789537.h"
#include "System_Web_System_Web_UI_PageTheme802797672.h"
#include "System_Web_System_Web_UI_PageThemeFileParser771511796.h"
#include "System_Web_System_Web_UI_PageThemeParser3466356847.h"
#include "System_Web_System_Web_UI_Pair771337780.h"
#include "System_Web_System_Web_UI_ParseChildrenAttribute3370876211.h"
#include "System_Web_System_Web_UI_PartialCachingAttribute7999397.h"
#include "System_Web_System_Web_UI_PersistChildrenAttribute3161469854.h"
#include "System_Web_System_Web_UI_PersistenceModeAttribute1611866060.h"
#include "System_Web_System_Web_UI_PersistenceMode2222411918.h"
#include "System_Web_System_Web_UI_PostBackOptions689177206.h"
#include "System_Web_System_Web_UI_PropertyEntry4184962072.h"
#include "System_Web_System_Web_UI_RootBuilder1594119744.h"
#include "System_Web_System_Web_UI_StateBag282928164.h"
#include "System_Web_System_Web_UI_StateItem1247725077.h"
#include "System_Web_System_Web_UI_StringPropertyBuilder3511621612.h"
#include "System_Web_System_Web_UI_SupportsEventValidationAt4063163006.h"
#include "System_Web_System_Web_UI_TagPrefixAttribute1587840331.h"
#include "System_Web_System_Web_UI_TemplateBuilder3864847030.h"
#include "System_Web_System_Web_UI_TemplateBinding3663525489.h"
#include "System_Web_System_Web_UI_TemplateContainerAttribut1232723725.h"
#include "System_Web_System_Web_UI_TemplateControl1922940480.h"
#include "System_Web_System_Web_UI_TemplateControl_EvtInfo1160105574.h"
#include "System_Web_System_Web_UI_TemplateControlParser3072921516.h"
#include "System_Web_System_Web_UI_TemplateInstance2414853837.h"
#include "System_Web_System_Web_UI_TemplateInstanceAttribute1309238595.h"
#include "System_Web_System_Web_UI_ServerSideScript2595428029.h"
#include "System_Web_System_Web_UI_TemplateParser24149626.h"
#include "System_Web_System_Web_UI_TemplateParser_OutputCach4174059373.h"
#include "System_Web_System_Web_UI_ThemeableAttribute1523904358.h"
#include "System_Web_System_Web_UI_ToolboxDataAttribute3280476760.h"
#include "System_Web_System_Web_UI_Triplet2158800263.h"
#include "System_Web_System_Web_UI_UnknownAttributeDescripto1586516698.h"
#include "System_Web_System_Web_UI_UrlPropertyAttribute3206276716.h"
#include "System_Web_System_Web_UI_UserControlControlBuilder1094625493.h"
#include "System_Web_System_Web_UI_UserControl1418173744.h"
#include "System_Web_System_Web_UI_UserControlParser2039383077.h"
#include "System_Web_System_Web_UI_ValidationPropertyAttribu3086378718.h"
#include "System_Web_System_Web_UI_ValidatorCollection3538853993.h"
#include "System_Web_System_Web_UI_ViewStateEncryptionMode3676930203.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3500 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3501 = { sizeof (IDReferencePropertyAttribute_t1719921500), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3501[1] = 
{
	IDReferencePropertyAttribute_t1719921500::get_offset_of_controlType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3502 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3503 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3504 = { sizeof (ImageClickEventArgs_t1740531002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3504[2] = 
{
	ImageClickEventArgs_t1740531002::get_offset_of_X_1(),
	ImageClickEventArgs_t1740531002::get_offset_of_Y_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3505 = { sizeof (IndexedString_t3379624379), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3505[1] = 
{
	IndexedString_t3379624379::get_offset_of_U3CValueU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3506 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3507 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3508 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3509 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3510 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3511 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3512 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3513 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3514 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3515 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3516 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3517 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3518 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3519 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3520 = { sizeof (LiteralControl_t249601277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3520[1] = 
{
	LiteralControl_t249601277::get_offset_of__text_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3521 = { sizeof (LosFormatter_t2034865563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3521[1] = 
{
	LosFormatter_t2034865563::get_offset_of_osf_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3522 = { sizeof (MasterPage_t1483330027), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3522[5] = 
{
	MasterPage_t1483330027::get_offset_of_definedContentTemplates_39(),
	MasterPage_t1483330027::get_offset_of_templates_40(),
	MasterPage_t1483330027::get_offset_of_placeholders_41(),
	MasterPage_t1483330027::get_offset_of_parentMasterPageFile_42(),
	MasterPage_t1483330027::get_offset_of_parentMasterPage_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3523 = { sizeof (MasterPageControlBuilder_t1194179076), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3524 = { sizeof (MasterPageParser_t2534398727), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3524[4] = 
{
	MasterPageParser_t2534398727::get_offset_of_masterType_62(),
	MasterPageParser_t2534398727::get_offset_of_masterTypeVirtualPath_63(),
	MasterPageParser_t2534398727::get_offset_of_contentPlaceHolderIds_64(),
	MasterPageParser_t2534398727::get_offset_of_cacheEntryName_65(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3525 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3525[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3526 = { sizeof (MinimizableAttributeTypeConverter_t982439845), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3527 = { sizeof (ObjectStateFormatter_t1543808709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3527[3] = 
{
	ObjectStateFormatter_t1543808709::get_offset_of_page_0(),
	ObjectStateFormatter_t1543808709::get_offset_of_algo_1(),
	ObjectStateFormatter_t1543808709::get_offset_of_vkey_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3528 = { sizeof (WriterContext_t4048875981), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3528[3] = 
{
	WriterContext_t4048875981::get_offset_of_cache_0(),
	WriterContext_t4048875981::get_offset_of_nextKey_1(),
	WriterContext_t4048875981::get_offset_of_key_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3529 = { sizeof (ReaderContext_t3753404210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3529[1] = 
{
	ReaderContext_t3753404210::get_offset_of_cache_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3530 = { sizeof (ObjectFormatter_t1981273209), -1, sizeof(ObjectFormatter_t1981273209_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3530[11] = 
{
	ObjectFormatter_t1981273209_StaticFields::get_offset_of_writeMap_0(),
	ObjectFormatter_t1981273209_StaticFields::get_offset_of_readMap_1(),
	ObjectFormatter_t1981273209_StaticFields::get_offset_of_binaryObjectFormatter_2(),
	ObjectFormatter_t1981273209_StaticFields::get_offset_of_typeFormatter_3(),
	ObjectFormatter_t1981273209_StaticFields::get_offset_of_enumFormatter_4(),
	ObjectFormatter_t1981273209_StaticFields::get_offset_of_singleRankArrayFormatter_5(),
	ObjectFormatter_t1981273209_StaticFields::get_offset_of_typeConverterFormatter_6(),
	ObjectFormatter_t1981273209_StaticFields::get_offset_of_nextId_7(),
	ObjectFormatter_t1981273209::get_offset_of_PrimaryId_8(),
	ObjectFormatter_t1981273209::get_offset_of_SecondaryId_9(),
	ObjectFormatter_t1981273209::get_offset_of_TertiaryId_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3531 = { sizeof (StringFormatter_t3828688682), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3532 = { sizeof (IndexedStringFormatter_t421145034), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3533 = { sizeof (Int64Formatter_t4181992990), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3534 = { sizeof (Int32Formatter_t4180763861), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3535 = { sizeof (Int16Formatter_t1075651843), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3536 = { sizeof (ByteFormatter_t2521617483), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3537 = { sizeof (BooleanFormatter_t3201589112), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3538 = { sizeof (CharFormatter_t597432637), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3539 = { sizeof (DateTimeFormatter_t3698563052), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3540 = { sizeof (PairFormatter_t188360010), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3541 = { sizeof (TripletFormatter_t732666697), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3542 = { sizeof (ArrayListFormatter_t349177227), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3543 = { sizeof (HashtableFormatter_t2187487685), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3544 = { sizeof (ObjectArrayFormatter_t1799921284), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3545 = { sizeof (ColorFormatter_t1525993769), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3546 = { sizeof (EnumFormatter_t1983866999), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3547 = { sizeof (TypeFormatter_t1484190866), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3548 = { sizeof (SingleRankArrayFormatter_t1493189349), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3548[1] = 
{
	SingleRankArrayFormatter_t1493189349::get_offset_of__binaryFormatter_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3549 = { sizeof (FontUnitFormatter_t2452266376), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3550 = { sizeof (UnitFormatter_t2363462124), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3551 = { sizeof (TypeConverterFormatter_t1198826992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3551[1] = 
{
	TypeConverterFormatter_t1198826992::get_offset_of_converter_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3552 = { sizeof (BinaryObjectFormatter_t2045863156), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3553 = { sizeof (ObjectTagBuilder_t2708400301), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3553[3] = 
{
	ObjectTagBuilder_t2708400301::get_offset_of_id_28(),
	ObjectTagBuilder_t2708400301::get_offset_of_scope_29(),
	ObjectTagBuilder_t2708400301::get_offset_of_type_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3554 = { sizeof (ObjectTag_t1714262980), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3555 = { sizeof (OutputCacheLocation_t1771607847)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3555[7] = 
{
	OutputCacheLocation_t1771607847::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3556 = { sizeof (Page_t770747966), -1, sizeof(Page_t770747966_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3556[74] = 
{
	Page_t770747966_StaticFields::get_offset_of_machineKeyConfigPath_36(),
	Page_t770747966::get_offset_of__eventValidation_37(),
	Page_t770747966::get_offset_of__savedControlState_38(),
	Page_t770747966::get_offset_of__doLoadPreviousPage_39(),
	Page_t770747966::get_offset_of__focusedControlID_40(),
	Page_t770747966::get_offset_of__hasEnabledControlArray_41(),
	Page_t770747966::get_offset_of__viewState_42(),
	Page_t770747966::get_offset_of__viewStateMac_43(),
	Page_t770747966::get_offset_of__errorPage_44(),
	Page_t770747966::get_offset_of_is_validated_45(),
	Page_t770747966::get_offset_of__validators_46(),
	Page_t770747966::get_offset_of_renderingForm_47(),
	Page_t770747966::get_offset_of__savedViewState_48(),
	Page_t770747966::get_offset_of__requiresPostBack_49(),
	Page_t770747966::get_offset_of__requiresPostBackCopy_50(),
	Page_t770747966::get_offset_of_requiresPostDataChanged_51(),
	Page_t770747966::get_offset_of_requiresRaiseEvent_52(),
	Page_t770747966::get_offset_of_formPostedRequiresRaiseEvent_53(),
	Page_t770747966::get_offset_of_secondPostData_54(),
	Page_t770747966::get_offset_of_requiresPostBackScript_55(),
	Page_t770747966::get_offset_of_postBackScriptRendered_56(),
	Page_t770747966::get_offset_of_requiresFormScriptDeclaration_57(),
	Page_t770747966::get_offset_of_formScriptDeclarationRendered_58(),
	Page_t770747966::get_offset_of_handleViewState_59(),
	Page_t770747966::get_offset_of__requestValueCollection_60(),
	Page_t770747966::get_offset_of_clientTarget_61(),
	Page_t770747966::get_offset_of_scriptManager_62(),
	Page_t770747966::get_offset_of_allow_load_63(),
	Page_t770747966::get_offset_of_page_state_persister_64(),
	Page_t770747966::get_offset_of__appCulture_65(),
	Page_t770747966::get_offset_of__appUICulture_66(),
	Page_t770747966::get_offset_of__context_67(),
	Page_t770747966::get_offset_of__application_68(),
	Page_t770747966::get_offset_of__response_69(),
	Page_t770747966::get_offset_of__request_70(),
	Page_t770747966::get_offset_of__cache_71(),
	Page_t770747966::get_offset_of_htmlHeader_72(),
	Page_t770747966::get_offset_of_masterPage_73(),
	Page_t770747966::get_offset_of_masterPageFile_74(),
	Page_t770747966::get_offset_of_previousPage_75(),
	Page_t770747966::get_offset_of_isCrossPagePostBack_76(),
	Page_t770747966::get_offset_of_isPostBack_77(),
	Page_t770747966::get_offset_of_isCallback_78(),
	Page_t770747966::get_offset_of_requireStateControls_79(),
	Page_t770747966::get_offset_of__form_80(),
	Page_t770747966::get_offset_of__title_81(),
	Page_t770747966::get_offset_of__theme_82(),
	Page_t770747966::get_offset_of__styleSheetTheme_83(),
	Page_t770747966::get_offset_of_items_84(),
	Page_t770747966::get_offset_of__maintainScrollPositionOnPostBack_85(),
	Page_t770747966::get_offset_of_asyncTimeout_86(),
	Page_t770747966::get_offset_of_parallelTasks_87(),
	Page_t770747966::get_offset_of_serialTasks_88(),
	Page_t770747966::get_offset_of_viewStateEncryptionMode_89(),
	Page_t770747966::get_offset_of_controlRegisteredForViewStateEncryption_90(),
	Page_t770747966::get_offset_of__validationStartupScript_91(),
	Page_t770747966::get_offset_of__validationOnSubmitStatement_92(),
	Page_t770747966::get_offset_of__validationInitializeScript_93(),
	Page_t770747966::get_offset_of__webFormScriptReference_94(),
	Page_t770747966_StaticFields::get_offset_of_InitCompleteEvent_95(),
	Page_t770747966_StaticFields::get_offset_of_LoadCompleteEvent_96(),
	Page_t770747966_StaticFields::get_offset_of_PreInitEvent_97(),
	Page_t770747966_StaticFields::get_offset_of_PreLoadEvent_98(),
	Page_t770747966_StaticFields::get_offset_of_PreRenderCompleteEvent_99(),
	Page_t770747966_StaticFields::get_offset_of_SaveStateCompleteEvent_100(),
	Page_t770747966::get_offset_of_event_mask_101(),
	Page_t770747966_StaticFields::get_offset_of_AES_IV_102(),
	Page_t770747966_StaticFields::get_offset_of_TripleDES_IV_103(),
	Page_t770747966_StaticFields::get_offset_of_locker_104(),
	Page_t770747966_StaticFields::get_offset_of_isEncryptionInitialized_105(),
	Page_t770747966::get_offset_of_contentTemplates_106(),
	Page_t770747966::get_offset_of__pageTheme_107(),
	Page_t770747966::get_offset_of__styleSheetPageTheme_108(),
	Page_t770747966::get_offset_of_dataItemCtx_109(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3557 = { sizeof (PageAsyncTask_t4073791294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3557[4] = 
{
	PageAsyncTask_t4073791294::get_offset_of_beginHandler_0(),
	PageAsyncTask_t4073791294::get_offset_of_endHandler_1(),
	PageAsyncTask_t4073791294::get_offset_of_timeoutHandler_2(),
	PageAsyncTask_t4073791294::get_offset_of_state_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3558 = { sizeof (PageParser_t2342141287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3558[30] = 
{
	PageParser_t2342141287::get_offset_of_enableSessionState_61(),
	PageParser_t2342141287::get_offset_of_enableViewStateMac_62(),
	PageParser_t2342141287::get_offset_of_enableViewStateMacSet_63(),
	PageParser_t2342141287::get_offset_of_smartNavigation_64(),
	PageParser_t2342141287::get_offset_of_haveTrace_65(),
	PageParser_t2342141287::get_offset_of_trace_66(),
	PageParser_t2342141287::get_offset_of_notBuffer_67(),
	PageParser_t2342141287::get_offset_of_tracemode_68(),
	PageParser_t2342141287::get_offset_of_contentType_69(),
	PageParser_t2342141287::get_offset_of_codepage_70(),
	PageParser_t2342141287::get_offset_of_responseEncoding_71(),
	PageParser_t2342141287::get_offset_of_lcid_72(),
	PageParser_t2342141287::get_offset_of_clientTarget_73(),
	PageParser_t2342141287::get_offset_of_masterPage_74(),
	PageParser_t2342141287::get_offset_of_title_75(),
	PageParser_t2342141287::get_offset_of_theme_76(),
	PageParser_t2342141287::get_offset_of_culture_77(),
	PageParser_t2342141287::get_offset_of_uiculture_78(),
	PageParser_t2342141287::get_offset_of_errorPage_79(),
	PageParser_t2342141287::get_offset_of_validateRequest_80(),
	PageParser_t2342141287::get_offset_of_async_81(),
	PageParser_t2342141287::get_offset_of_asyncTimeout_82(),
	PageParser_t2342141287::get_offset_of_masterType_83(),
	PageParser_t2342141287::get_offset_of_masterVirtualPath_84(),
	PageParser_t2342141287::get_offset_of_styleSheetTheme_85(),
	PageParser_t2342141287::get_offset_of_enable_event_validation_86(),
	PageParser_t2342141287::get_offset_of_maintainScrollPositionOnPostBack_87(),
	PageParser_t2342141287::get_offset_of_maxPageStateFieldLength_88(),
	PageParser_t2342141287::get_offset_of_previousPageType_89(),
	PageParser_t2342141287::get_offset_of_previousPageVirtualPath_90(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3559 = { sizeof (PageParserFilter_t169228355), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3559[1] = 
{
	PageParserFilter_t169228355::get_offset_of_parser_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3560 = { sizeof (PageStatePersister_t2741789537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3560[4] = 
{
	PageStatePersister_t2741789537::get_offset_of_control_state_0(),
	PageStatePersister_t2741789537::get_offset_of_view_state_1(),
	PageStatePersister_t2741789537::get_offset_of_page_2(),
	PageStatePersister_t2741789537::get_offset_of_state_formatter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3561 = { sizeof (PageTheme_t802797672), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3561[1] = 
{
	PageTheme_t802797672::get_offset_of__page_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3562 = { sizeof (PageThemeFileParser_t771511796), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3563 = { sizeof (PageThemeParser_t3466356847), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3563[1] = 
{
	PageThemeParser_t3466356847::get_offset_of_linkedStyleSheets_62(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3564 = { sizeof (Pair_t771337780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3564[2] = 
{
	Pair_t771337780::get_offset_of_First_0(),
	Pair_t771337780::get_offset_of_Second_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3565 = { sizeof (ParseChildrenAttribute_t3370876211), -1, sizeof(ParseChildrenAttribute_t3370876211_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3565[6] = 
{
	ParseChildrenAttribute_t3370876211::get_offset_of_childrenAsProperties_0(),
	ParseChildrenAttribute_t3370876211::get_offset_of_defaultProperty_1(),
	ParseChildrenAttribute_t3370876211_StaticFields::get_offset_of_Default_2(),
	ParseChildrenAttribute_t3370876211_StaticFields::get_offset_of_ParseAsChildren_3(),
	ParseChildrenAttribute_t3370876211_StaticFields::get_offset_of_ParseAsProperties_4(),
	ParseChildrenAttribute_t3370876211::get_offset_of_childType_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3566 = { sizeof (PartialCachingAttribute_t7999397), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3566[5] = 
{
	PartialCachingAttribute_t7999397::get_offset_of_duration_0(),
	PartialCachingAttribute_t7999397::get_offset_of_varyByControls_1(),
	PartialCachingAttribute_t7999397::get_offset_of_varyByCustom_2(),
	PartialCachingAttribute_t7999397::get_offset_of_varyByParams_3(),
	PartialCachingAttribute_t7999397::get_offset_of_shared_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3567 = { sizeof (PersistChildrenAttribute_t3161469854), -1, sizeof(PersistChildrenAttribute_t3161469854_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3567[5] = 
{
	PersistChildrenAttribute_t3161469854::get_offset_of_persist_0(),
	PersistChildrenAttribute_t3161469854::get_offset_of_usesCustomPersistence_1(),
	PersistChildrenAttribute_t3161469854_StaticFields::get_offset_of_Default_2(),
	PersistChildrenAttribute_t3161469854_StaticFields::get_offset_of_Yes_3(),
	PersistChildrenAttribute_t3161469854_StaticFields::get_offset_of_No_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3568 = { sizeof (PersistenceModeAttribute_t1611866060), -1, sizeof(PersistenceModeAttribute_t1611866060_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3568[6] = 
{
	PersistenceModeAttribute_t1611866060::get_offset_of_mode_0(),
	PersistenceModeAttribute_t1611866060_StaticFields::get_offset_of_Attribute_1(),
	PersistenceModeAttribute_t1611866060_StaticFields::get_offset_of_Default_2(),
	PersistenceModeAttribute_t1611866060_StaticFields::get_offset_of_EncodedInnerDefaultProperty_3(),
	PersistenceModeAttribute_t1611866060_StaticFields::get_offset_of_InnerDefaultProperty_4(),
	PersistenceModeAttribute_t1611866060_StaticFields::get_offset_of_InnerProperty_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3569 = { sizeof (PersistenceMode_t2222411918)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3569[5] = 
{
	PersistenceMode_t2222411918::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3570 = { sizeof (PostBackOptions_t689177206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3570[9] = 
{
	PostBackOptions_t689177206::get_offset_of_control_0(),
	PostBackOptions_t689177206::get_offset_of_argument_1(),
	PostBackOptions_t689177206::get_offset_of_actionUrl_2(),
	PostBackOptions_t689177206::get_offset_of_autoPostBack_3(),
	PostBackOptions_t689177206::get_offset_of_requiresJavaScriptProtocol_4(),
	PostBackOptions_t689177206::get_offset_of_trackFocus_5(),
	PostBackOptions_t689177206::get_offset_of_clientSubmit_6(),
	PostBackOptions_t689177206::get_offset_of_performValidation_7(),
	PostBackOptions_t689177206::get_offset_of_validationGroup_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3571 = { sizeof (PropertyEntry_t4184962072), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3571[3] = 
{
	PropertyEntry_t4184962072::get_offset_of_type_0(),
	PropertyEntry_t4184962072::get_offset_of_name_1(),
	PropertyEntry_t4184962072::get_offset_of_pinfo_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3572 = { sizeof (RootBuilder_t1594119744), -1, sizeof(RootBuilder_t1594119744_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3572[3] = 
{
	RootBuilder_t1594119744_StaticFields::get_offset_of_htmlControls_32(),
	RootBuilder_t1594119744_StaticFields::get_offset_of_htmlInputControls_33(),
	RootBuilder_t1594119744::get_offset_of_foundry_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3573 = { sizeof (StateBag_t282928164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3573[2] = 
{
	StateBag_t282928164::get_offset_of_ht_0(),
	StateBag_t282928164::get_offset_of_track_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3574 = { sizeof (StateItem_t1247725077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3574[2] = 
{
	StateItem_t1247725077::get_offset_of__isDirty_0(),
	StateItem_t1247725077::get_offset_of__value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3575 = { sizeof (StringPropertyBuilder_t3511621612), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3575[1] = 
{
	StringPropertyBuilder_t3511621612::get_offset_of_prop_name_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3576 = { sizeof (SupportsEventValidationAttribute_t4063163006), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3577 = { sizeof (TagPrefixAttribute_t1587840331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3577[2] = 
{
	TagPrefixAttribute_t1587840331::get_offset_of_namespaceName_0(),
	TagPrefixAttribute_t1587840331::get_offset_of_tagPrefix_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3578 = { sizeof (TemplateBuilder_t3864847030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3578[4] = 
{
	TemplateBuilder_t3864847030::get_offset_of_text_28(),
	TemplateBuilder_t3864847030::get_offset_of_containerAttribute_29(),
	TemplateBuilder_t3864847030::get_offset_of_instanceAttribute_30(),
	TemplateBuilder_t3864847030::get_offset_of_bindings_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3579 = { sizeof (TemplateBinding_t3663525489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3579[4] = 
{
	TemplateBinding_t3663525489::get_offset_of_ControlType_0(),
	TemplateBinding_t3663525489::get_offset_of_ControlProperty_1(),
	TemplateBinding_t3663525489::get_offset_of_ControlId_2(),
	TemplateBinding_t3663525489::get_offset_of_FieldName_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3580 = { sizeof (TemplateContainerAttribute_t1232723725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3580[2] = 
{
	TemplateContainerAttribute_t1232723725::get_offset_of_containerType_0(),
	TemplateContainerAttribute_t1232723725::get_offset_of_direction_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3581 = { sizeof (TemplateControl_t1922940480), -1, sizeof(TemplateControl_t1922940480_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3581[8] = 
{
	TemplateControl_t1922940480_StaticFields::get_offset_of__System_Web_Assembly_28(),
	TemplateControl_t1922940480_StaticFields::get_offset_of_abortTransaction_29(),
	TemplateControl_t1922940480_StaticFields::get_offset_of_commitTransaction_30(),
	TemplateControl_t1922940480_StaticFields::get_offset_of_error_31(),
	TemplateControl_t1922940480_StaticFields::get_offset_of_methodNames_32(),
	TemplateControl_t1922940480::get_offset_of__appRelativeVirtualPath_33(),
	TemplateControl_t1922940480_StaticFields::get_offset_of_auto_event_info_34(),
	TemplateControl_t1922940480_StaticFields::get_offset_of_auto_event_info_monitor_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3582 = { sizeof (EvtInfo_t1160105574), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3582[4] = 
{
	EvtInfo_t1160105574::get_offset_of_method_0(),
	EvtInfo_t1160105574::get_offset_of_methodName_1(),
	EvtInfo_t1160105574::get_offset_of_evt_2(),
	EvtInfo_t1160105574::get_offset_of_noParams_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3583 = { sizeof (TemplateControlParser_t3072921516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3583[4] = 
{
	TemplateControlParser_t3072921516::get_offset_of_autoEventWireup_57(),
	TemplateControlParser_t3072921516::get_offset_of_enableViewState_58(),
	TemplateControlParser_t3072921516::get_offset_of_compilationMode_59(),
	TemplateControlParser_t3072921516::get_offset_of_reader_60(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3584 = { sizeof (TemplateInstance_t2414853837)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3584[3] = 
{
	TemplateInstance_t2414853837::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3585 = { sizeof (TemplateInstanceAttribute_t1309238595), -1, sizeof(TemplateInstanceAttribute_t1309238595_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3585[4] = 
{
	TemplateInstanceAttribute_t1309238595::get_offset_of__instance_0(),
	TemplateInstanceAttribute_t1309238595_StaticFields::get_offset_of_Single_1(),
	TemplateInstanceAttribute_t1309238595_StaticFields::get_offset_of_Multiple_2(),
	TemplateInstanceAttribute_t1309238595_StaticFields::get_offset_of_Default_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3586 = { sizeof (ServerSideScript_t2595428029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3586[2] = 
{
	ServerSideScript_t2595428029::get_offset_of_Script_0(),
	ServerSideScript_t2595428029::get_offset_of_Location_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3587 = { sizeof (TemplateParser_t24149626), -1, sizeof(TemplateParser_t24149626_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3587[52] = 
{
	TemplateParser_t24149626::get_offset_of_inputFile_5(),
	TemplateParser_t24149626::get_offset_of_mainAttributes_6(),
	TemplateParser_t24149626::get_offset_of_dependencies_7(),
	TemplateParser_t24149626::get_offset_of_assemblies_8(),
	TemplateParser_t24149626::get_offset_of_anames_9(),
	TemplateParser_t24149626::get_offset_of_binDirAssemblies_10(),
	TemplateParser_t24149626::get_offset_of_namespacesCache_11(),
	TemplateParser_t24149626::get_offset_of_imports_12(),
	TemplateParser_t24149626::get_offset_of_interfaces_13(),
	TemplateParser_t24149626::get_offset_of_scripts_14(),
	TemplateParser_t24149626::get_offset_of_baseType_15(),
	TemplateParser_t24149626::get_offset_of_baseTypeIsGlobal_16(),
	TemplateParser_t24149626::get_offset_of_className_17(),
	TemplateParser_t24149626::get_offset_of_rootBuilder_18(),
	TemplateParser_t24149626::get_offset_of_debug_19(),
	TemplateParser_t24149626::get_offset_of_compilerOptions_20(),
	TemplateParser_t24149626::get_offset_of_language_21(),
	TemplateParser_t24149626::get_offset_of_implicitLanguage_22(),
	TemplateParser_t24149626::get_offset_of_strictOn_23(),
	TemplateParser_t24149626::get_offset_of_explicitOn_24(),
	TemplateParser_t24149626::get_offset_of_linePragmasOn_25(),
	TemplateParser_t24149626::get_offset_of_output_cache_26(),
	TemplateParser_t24149626::get_offset_of_oc_duration_27(),
	TemplateParser_t24149626::get_offset_of_oc_header_28(),
	TemplateParser_t24149626::get_offset_of_oc_custom_29(),
	TemplateParser_t24149626::get_offset_of_oc_param_30(),
	TemplateParser_t24149626::get_offset_of_oc_controls_31(),
	TemplateParser_t24149626::get_offset_of_oc_content_encodings_32(),
	TemplateParser_t24149626::get_offset_of_oc_cacheprofile_33(),
	TemplateParser_t24149626::get_offset_of_oc_sqldependency_34(),
	TemplateParser_t24149626::get_offset_of_oc_nostore_35(),
	TemplateParser_t24149626::get_offset_of_oc_parsed_params_36(),
	TemplateParser_t24149626::get_offset_of_oc_shared_37(),
	TemplateParser_t24149626::get_offset_of_oc_location_38(),
	TemplateParser_t24149626::get_offset_of_allowedMainDirectives_39(),
	TemplateParser_t24149626::get_offset_of_md5checksum_40(),
	TemplateParser_t24149626::get_offset_of_src_41(),
	TemplateParser_t24149626::get_offset_of_srcIsLegacy_42(),
	TemplateParser_t24149626::get_offset_of_partialClassName_43(),
	TemplateParser_t24149626::get_offset_of_codeFileBaseClass_44(),
	TemplateParser_t24149626::get_offset_of_metaResourceKey_45(),
	TemplateParser_t24149626::get_offset_of_codeFileBaseClassType_46(),
	TemplateParser_t24149626::get_offset_of_pageParserFilterType_47(),
	TemplateParser_t24149626::get_offset_of_pageParserFilter_48(),
	TemplateParser_t24149626::get_offset_of_unknownMainAttributes_49(),
	TemplateParser_t24149626::get_offset_of_includeDirs_50(),
	TemplateParser_t24149626::get_offset_of_registeredTagNames_51(),
	TemplateParser_t24149626::get_offset_of_directiveLocation_52(),
	TemplateParser_t24149626::get_offset_of_appAssemblyIndex_53(),
	TemplateParser_t24149626_StaticFields::get_offset_of_autoClassCounter_54(),
	TemplateParser_t24149626::get_offset_of_U3CAspGeneratorU3Ek__BackingField_55(),
	TemplateParser_t24149626_StaticFields::get_offset_of_U3CU3Ef__switchU24map4_56(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3588 = { sizeof (OutputCacheParsedParams_t4174059373)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3588[9] = 
{
	OutputCacheParsedParams_t4174059373::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3589 = { sizeof (ThemeableAttribute_t1523904358), -1, sizeof(ThemeableAttribute_t1523904358_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3589[4] = 
{
	ThemeableAttribute_t1523904358::get_offset_of_themeable_0(),
	ThemeableAttribute_t1523904358_StaticFields::get_offset_of_Default_1(),
	ThemeableAttribute_t1523904358_StaticFields::get_offset_of_No_2(),
	ThemeableAttribute_t1523904358_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3590 = { sizeof (ToolboxDataAttribute_t3280476760), -1, sizeof(ToolboxDataAttribute_t3280476760_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3590[2] = 
{
	ToolboxDataAttribute_t3280476760_StaticFields::get_offset_of_Default_0(),
	ToolboxDataAttribute_t3280476760::get_offset_of_data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3591 = { sizeof (Triplet_t2158800263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3591[3] = 
{
	Triplet_t2158800263::get_offset_of_First_0(),
	Triplet_t2158800263::get_offset_of_Second_1(),
	Triplet_t2158800263::get_offset_of_Third_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3592 = { sizeof (UnknownAttributeDescriptor_t1586516698), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3592[2] = 
{
	UnknownAttributeDescriptor_t1586516698::get_offset_of_Info_0(),
	UnknownAttributeDescriptor_t1586516698::get_offset_of_Value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3593 = { sizeof (UrlPropertyAttribute_t3206276716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3593[1] = 
{
	UrlPropertyAttribute_t3206276716::get_offset_of_filter_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3594 = { sizeof (UserControlControlBuilder_t1094625493), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3595 = { sizeof (UserControl_t1418173744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3595[3] = 
{
	UserControl_t1418173744::get_offset_of_initialized_36(),
	UserControl_t1418173744::get_offset_of_attributes_37(),
	UserControl_t1418173744::get_offset_of_attrBag_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3596 = { sizeof (UserControlParser_t2039383077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3596[1] = 
{
	UserControlParser_t2039383077::get_offset_of_masterPage_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3597 = { sizeof (ValidationPropertyAttribute_t3086378718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3597[1] = 
{
	ValidationPropertyAttribute_t3086378718::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3598 = { sizeof (ValidatorCollection_t3538853993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3598[1] = 
{
	ValidatorCollection_t3538853993::get_offset_of__validators_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3599 = { sizeof (ViewStateEncryptionMode_t3676930203)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3599[4] = 
{
	ViewStateEncryptionMode_t3676930203::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
