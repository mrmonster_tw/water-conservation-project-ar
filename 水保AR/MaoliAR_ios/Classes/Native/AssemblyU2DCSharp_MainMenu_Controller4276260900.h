﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_GetTickets911946028.h"

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Text
struct Text_t1901882714;
// TweenScale
struct TweenScale_t2539309033;
// System.Predicate`1<UnityEngine.UI.Image>
struct Predicate_1_t3495563775;
// System.Predicate`1<UnityEngine.UI.Text>
struct Predicate_1_t2727176838;
// System.Predicate`1<TweenScale>
struct Predicate_1_t3364603157;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainMenu_Controller
struct  MainMenu_Controller_t4276260900  : public GetTickets_t911946028
{
public:
	// System.Boolean MainMenu_Controller::NetworkingCheck
	bool ___NetworkingCheck_25;
	// UnityEngine.GameObject[] MainMenu_Controller::BeforeStart
	GameObjectU5BU5D_t3328599146* ___BeforeStart_26;
	// System.Single MainMenu_Controller::persent
	float ___persent_27;
	// UnityEngine.UI.Image MainMenu_Controller::Image_Bar_front
	Image_t2670269651 * ___Image_Bar_front_28;
	// UnityEngine.UI.Text MainMenu_Controller::Text_Bar_Persent
	Text_t1901882714 * ___Text_Bar_Persent_29;
	// TweenScale MainMenu_Controller::Image_BFS
	TweenScale_t2539309033 * ___Image_BFS_30;
	// System.Boolean MainMenu_Controller::userClick
	bool ___userClick_35;

public:
	inline static int32_t get_offset_of_NetworkingCheck_25() { return static_cast<int32_t>(offsetof(MainMenu_Controller_t4276260900, ___NetworkingCheck_25)); }
	inline bool get_NetworkingCheck_25() const { return ___NetworkingCheck_25; }
	inline bool* get_address_of_NetworkingCheck_25() { return &___NetworkingCheck_25; }
	inline void set_NetworkingCheck_25(bool value)
	{
		___NetworkingCheck_25 = value;
	}

	inline static int32_t get_offset_of_BeforeStart_26() { return static_cast<int32_t>(offsetof(MainMenu_Controller_t4276260900, ___BeforeStart_26)); }
	inline GameObjectU5BU5D_t3328599146* get_BeforeStart_26() const { return ___BeforeStart_26; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_BeforeStart_26() { return &___BeforeStart_26; }
	inline void set_BeforeStart_26(GameObjectU5BU5D_t3328599146* value)
	{
		___BeforeStart_26 = value;
		Il2CppCodeGenWriteBarrier(&___BeforeStart_26, value);
	}

	inline static int32_t get_offset_of_persent_27() { return static_cast<int32_t>(offsetof(MainMenu_Controller_t4276260900, ___persent_27)); }
	inline float get_persent_27() const { return ___persent_27; }
	inline float* get_address_of_persent_27() { return &___persent_27; }
	inline void set_persent_27(float value)
	{
		___persent_27 = value;
	}

	inline static int32_t get_offset_of_Image_Bar_front_28() { return static_cast<int32_t>(offsetof(MainMenu_Controller_t4276260900, ___Image_Bar_front_28)); }
	inline Image_t2670269651 * get_Image_Bar_front_28() const { return ___Image_Bar_front_28; }
	inline Image_t2670269651 ** get_address_of_Image_Bar_front_28() { return &___Image_Bar_front_28; }
	inline void set_Image_Bar_front_28(Image_t2670269651 * value)
	{
		___Image_Bar_front_28 = value;
		Il2CppCodeGenWriteBarrier(&___Image_Bar_front_28, value);
	}

	inline static int32_t get_offset_of_Text_Bar_Persent_29() { return static_cast<int32_t>(offsetof(MainMenu_Controller_t4276260900, ___Text_Bar_Persent_29)); }
	inline Text_t1901882714 * get_Text_Bar_Persent_29() const { return ___Text_Bar_Persent_29; }
	inline Text_t1901882714 ** get_address_of_Text_Bar_Persent_29() { return &___Text_Bar_Persent_29; }
	inline void set_Text_Bar_Persent_29(Text_t1901882714 * value)
	{
		___Text_Bar_Persent_29 = value;
		Il2CppCodeGenWriteBarrier(&___Text_Bar_Persent_29, value);
	}

	inline static int32_t get_offset_of_Image_BFS_30() { return static_cast<int32_t>(offsetof(MainMenu_Controller_t4276260900, ___Image_BFS_30)); }
	inline TweenScale_t2539309033 * get_Image_BFS_30() const { return ___Image_BFS_30; }
	inline TweenScale_t2539309033 ** get_address_of_Image_BFS_30() { return &___Image_BFS_30; }
	inline void set_Image_BFS_30(TweenScale_t2539309033 * value)
	{
		___Image_BFS_30 = value;
		Il2CppCodeGenWriteBarrier(&___Image_BFS_30, value);
	}

	inline static int32_t get_offset_of_userClick_35() { return static_cast<int32_t>(offsetof(MainMenu_Controller_t4276260900, ___userClick_35)); }
	inline bool get_userClick_35() const { return ___userClick_35; }
	inline bool* get_address_of_userClick_35() { return &___userClick_35; }
	inline void set_userClick_35(bool value)
	{
		___userClick_35 = value;
	}
};

struct MainMenu_Controller_t4276260900_StaticFields
{
public:
	// System.Int32 MainMenu_Controller::inputCount
	int32_t ___inputCount_34;
	// System.Predicate`1<UnityEngine.UI.Image> MainMenu_Controller::<>f__am$cache0
	Predicate_1_t3495563775 * ___U3CU3Ef__amU24cache0_36;
	// System.Predicate`1<UnityEngine.UI.Text> MainMenu_Controller::<>f__am$cache1
	Predicate_1_t2727176838 * ___U3CU3Ef__amU24cache1_37;
	// System.Predicate`1<TweenScale> MainMenu_Controller::<>f__am$cache2
	Predicate_1_t3364603157 * ___U3CU3Ef__amU24cache2_38;

public:
	inline static int32_t get_offset_of_inputCount_34() { return static_cast<int32_t>(offsetof(MainMenu_Controller_t4276260900_StaticFields, ___inputCount_34)); }
	inline int32_t get_inputCount_34() const { return ___inputCount_34; }
	inline int32_t* get_address_of_inputCount_34() { return &___inputCount_34; }
	inline void set_inputCount_34(int32_t value)
	{
		___inputCount_34 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_36() { return static_cast<int32_t>(offsetof(MainMenu_Controller_t4276260900_StaticFields, ___U3CU3Ef__amU24cache0_36)); }
	inline Predicate_1_t3495563775 * get_U3CU3Ef__amU24cache0_36() const { return ___U3CU3Ef__amU24cache0_36; }
	inline Predicate_1_t3495563775 ** get_address_of_U3CU3Ef__amU24cache0_36() { return &___U3CU3Ef__amU24cache0_36; }
	inline void set_U3CU3Ef__amU24cache0_36(Predicate_1_t3495563775 * value)
	{
		___U3CU3Ef__amU24cache0_36 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_36, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_37() { return static_cast<int32_t>(offsetof(MainMenu_Controller_t4276260900_StaticFields, ___U3CU3Ef__amU24cache1_37)); }
	inline Predicate_1_t2727176838 * get_U3CU3Ef__amU24cache1_37() const { return ___U3CU3Ef__amU24cache1_37; }
	inline Predicate_1_t2727176838 ** get_address_of_U3CU3Ef__amU24cache1_37() { return &___U3CU3Ef__amU24cache1_37; }
	inline void set_U3CU3Ef__amU24cache1_37(Predicate_1_t2727176838 * value)
	{
		___U3CU3Ef__amU24cache1_37 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_37, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_38() { return static_cast<int32_t>(offsetof(MainMenu_Controller_t4276260900_StaticFields, ___U3CU3Ef__amU24cache2_38)); }
	inline Predicate_1_t3364603157 * get_U3CU3Ef__amU24cache2_38() const { return ___U3CU3Ef__amU24cache2_38; }
	inline Predicate_1_t3364603157 ** get_address_of_U3CU3Ef__amU24cache2_38() { return &___U3CU3Ef__amU24cache2_38; }
	inline void set_U3CU3Ef__amU24cache2_38(Predicate_1_t3364603157 * value)
	{
		___U3CU3Ef__amU24cache2_38 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_38, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
