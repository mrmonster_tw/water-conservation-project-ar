﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Operations_XslGeneralVaria1456221871.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslGlobalVariable
struct  XslGlobalVariable_t2754044456  : public XslGeneralVariable_t1456221871
{
public:

public:
};

struct XslGlobalVariable_t2754044456_StaticFields
{
public:
	// System.Object Mono.Xml.Xsl.Operations.XslGlobalVariable::busyObject
	Il2CppObject * ___busyObject_4;

public:
	inline static int32_t get_offset_of_busyObject_4() { return static_cast<int32_t>(offsetof(XslGlobalVariable_t2754044456_StaticFields, ___busyObject_4)); }
	inline Il2CppObject * get_busyObject_4() const { return ___busyObject_4; }
	inline Il2CppObject ** get_address_of_busyObject_4() { return &___busyObject_4; }
	inline void set_busyObject_4(Il2CppObject * value)
	{
		___busyObject_4 = value;
		Il2CppCodeGenWriteBarrier(&___busyObject_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
