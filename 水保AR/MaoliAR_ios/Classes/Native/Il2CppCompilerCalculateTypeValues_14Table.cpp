﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Security_System_Security_Cryptography_Xml_K1254893728.h"
#include "System_Security_System_Security_Cryptography_Xml_M3366168394.h"
#include "System_Security_System_Security_Cryptography_Xml_R1944003019.h"
#include "System_Security_System_Security_Cryptography_Xml_R2222396100.h"
#include "System_Security_System_Security_Cryptography_Xml_R3247853290.h"
#include "System_Security_System_Security_Cryptography_Xml_Si754089620.h"
#include "System_Security_System_Security_Cryptography_Xml_S1687583442.h"
#include "System_Security_System_Security_Cryptography_Xml_S2168796367.h"
#include "System_Security_System_Security_Cryptography_Xml_Sy405371701.h"
#include "System_Security_System_Security_Cryptography_Xml_T1669092815.h"
#include "System_Security_System_Security_Cryptography_Xml_T1105379765.h"
#include "System_Security_System_Security_Cryptography_Xml_X3270105241.h"
#include "System_Security_System_Security_Cryptography_Xml_X4000891284.h"
#include "System_Security_System_Security_Cryptography_Xml_Xm260084727.h"
#include "System_Security_System_Security_Cryptography_Xml_X3949211521.h"
#include "System_Security_System_Security_Cryptography_Xml_X1074875822.h"
#include "System_Security_System_Security_Cryptography_Xml_X2851260348.h"
#include "System_Security_System_Security_Cryptography_Xml_Xm586418029.h"
#include "System_Security_System_Security_Cryptography_Xml_X1461379654.h"
#include "System_Security_System_Security_Cryptography_Xml_X4194189051.h"
#include "System_Security_System_Security_Cryptography_Xml_X1962434658.h"
#include "System_Security_System_Security_Cryptography_Xml_X2680727779.h"
#include "System_Security_System_Security_Cryptography_Xml_Xml59088751.h"
#include "System_Security_System_Security_Cryptography_Xml_X2973394665.h"
#include "System_Security_System_Security_Cryptography_Xml_X1498580954.h"
#include "System_Security_System_Security_Cryptography_Xml_X1967805337.h"
#include "System_Security_System_MonoTODOAttribute4131080581.h"
#include "System_Security_U3CPrivateImplementationDetailsU3E3057255361.h"
#include "System_Security_U3CPrivateImplementationDetailsU3E3244137463.h"
#include "System_Xml_U3CModuleU3E692745525.h"
#include "System_Xml_Mono_Xml_XPath_XPathParser4136515887.h"
#include "System_Xml_Mono_Xml_XPath_XPathParser_YYRules2124747439.h"
#include "System_Xml_Mono_Xml_XPath_yyParser_yyException1362412556.h"
#include "System_Xml_Mono_Xml_XPath_yyParser_yyUnexpectedEof2049021987.h"
#include "System_Xml_Mono_Xml_Xsl_XsltPatternParser433897766.h"
#include "System_Xml_Mono_Xml_Xsl_XsltPatternParser_YYRules468763346.h"
#include "System_Xml_Mono_Xml_Xsl_yyParser_yyException795026074.h"
#include "System_Xml_Mono_Xml_Xsl_yyParser_yyUnexpectedEof3119335031.h"
#include "System_Xml_Mono_Xml_Xsl_Tokenizer3184446799.h"
#include "System_Xml_Locale4128636107.h"
#include "System_Xml_System_MonoTODOAttribute4131080581.h"
#include "System_Xml_Mono_Xml_Schema_XmlSchemaValidatingRead3113890617.h"
#include "System_Xml_Mono_Xml_Schema_XmlSchemaValidatingReade286117946.h"
#include "System_Xml_Mono_Xml_Schema_XsdIdentitySelector574258590.h"
#include "System_Xml_Mono_Xml_Schema_XsdIdentityField1964115728.h"
#include "System_Xml_Mono_Xml_Schema_XsdIdentityPath991900844.h"
#include "System_Xml_Mono_Xml_Schema_XsdIdentityStep1480907129.h"
#include "System_Xml_Mono_Xml_Schema_XsdKeyEntryField3552275292.h"
#include "System_Xml_Mono_Xml_Schema_XsdKeyEntryFieldCollect3698183622.h"
#include "System_Xml_Mono_Xml_Schema_XsdKeyEntry693496666.h"
#include "System_Xml_Mono_Xml_Schema_XsdKeyEntryCollection3090959213.h"
#include "System_Xml_Mono_Xml_Schema_XsdKeyTable2156891743.h"
#include "System_Xml_Mono_Xml_Schema_XsdParticleStateManager726654767.h"
#include "System_Xml_Mono_Xml_Schema_XsdValidationState376578997.h"
#include "System_Xml_Mono_Xml_Schema_XsdElementValidationSta2214590119.h"
#include "System_Xml_Mono_Xml_Schema_XsdSequenceValidationSta429792968.h"
#include "System_Xml_Mono_Xml_Schema_XsdChoiceValidationStat2566230191.h"
#include "System_Xml_Mono_Xml_Schema_XsdAllValidationState2703884157.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnyValidationState3421545252.h"
#include "System_Xml_Mono_Xml_Schema_XsdAppendedValidationSt3608891238.h"
#include "System_Xml_Mono_Xml_Schema_XsdEmptyValidationState1344146143.h"
#include "System_Xml_Mono_Xml_Schema_XsdInvalidValidationSta3749995458.h"
#include "System_Xml_Mono_Xml_Schema_XsdValidatingReader3961132625.h"
#include "System_Xml_Mono_Xml_Schema_XsdValidationContext1104170526.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDManager1008806102.h"
#include "System_Xml_Mono_Xml_Schema_XsdWildcard2790389089.h"
#include "System_Xml_Mono_Xml_XPath_DTMXPathDocument22335062454.h"
#include "System_Xml_Mono_Xml_XPath_DTMXPathDocumentBuilder23353543192.h"
#include "System_Xml_Mono_Xml_XPath_DTMXPathDocumentWriter22031536402.h"
#include "System_Xml_Mono_Xml_XPath_DTMXPathNavigator24118996007.h"
#include "System_Xml_Mono_Xml_XPath_DTMXPathLinkedNode23353097823.h"
#include "System_Xml_Mono_Xml_XPath_DTMXPathAttributeNode23707096872.h"
#include "System_Xml_Mono_Xml_XPath_DTMXPathNamespaceNode21119120712.h"
#include "System_Xml_Mono_Xml_XPath_IdPattern1261881997.h"
#include "System_Xml_Mono_Xml_XPath_KeyPattern747253878.h"
#include "System_Xml_Mono_Xml_XPath_LocationPathPattern3015773238.h"
#include "System_Xml_Mono_Xml_XPath_Pattern1136864796.h"
#include "System_Xml_Mono_Xml_XPath_UnionPattern467140937.h"
#include "System_Xml_Mono_Xml_XPath_XPathEditableDocument3642283412.h"
#include "System_Xml_Mono_Xml_XPath_XmlDocumentInsertionWrit2537499277.h"
#include "System_Xml_Mono_Xml_XPath_XmlDocumentEditableNavigat22456660.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslApplyImports1877513678.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslApplyTemplat2247116683.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslAttribute1907873804.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslAvt1645109359.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslAvt_AvtPart3412049665.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslAvt_SimpleAvt729895317.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslAvt_XPathAvt3066905968.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslCallTemplate509413441.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslChoose3297598844.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslComment1785218384.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslCompiledElem1729269235.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslCompiledElemen50593777.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslCopy2770797771.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslCopyOf800898188.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslElement1808680497.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1400 = { sizeof (KeyReference_t1254893728), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1401 = { sizeof (Manifest_t3366168394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1401[3] = 
{
	Manifest_t3366168394::get_offset_of_references_0(),
	Manifest_t3366168394::get_offset_of_id_1(),
	Manifest_t3366168394::get_offset_of_element_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1402 = { sizeof (Reference_t1944003019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1402[7] = 
{
	Reference_t1944003019::get_offset_of_chain_0(),
	Reference_t1944003019::get_offset_of_digestMethod_1(),
	Reference_t1944003019::get_offset_of_digestValue_2(),
	Reference_t1944003019::get_offset_of_id_3(),
	Reference_t1944003019::get_offset_of_uri_4(),
	Reference_t1944003019::get_offset_of_type_5(),
	Reference_t1944003019::get_offset_of_element_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1403 = { sizeof (ReferenceList_t2222396100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1403[1] = 
{
	ReferenceList_t2222396100::get_offset_of_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1404 = { sizeof (RSAKeyValue_t3247853290), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1404[1] = 
{
	RSAKeyValue_t3247853290::get_offset_of_rsa_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1405 = { sizeof (Signature_t754089620), -1, sizeof(Signature_t754089620_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1405[7] = 
{
	Signature_t754089620_StaticFields::get_offset_of_dsigNsmgr_0(),
	Signature_t754089620::get_offset_of_list_1(),
	Signature_t754089620::get_offset_of_info_2(),
	Signature_t754089620::get_offset_of_key_3(),
	Signature_t754089620::get_offset_of_id_4(),
	Signature_t754089620::get_offset_of_signature_5(),
	Signature_t754089620::get_offset_of_element_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1406 = { sizeof (SignedInfo_t1687583442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1406[6] = 
{
	SignedInfo_t1687583442::get_offset_of_references_0(),
	SignedInfo_t1687583442::get_offset_of_c14nMethod_1(),
	SignedInfo_t1687583442::get_offset_of_id_2(),
	SignedInfo_t1687583442::get_offset_of_signatureMethod_3(),
	SignedInfo_t1687583442::get_offset_of_signatureLength_4(),
	SignedInfo_t1687583442::get_offset_of_element_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1407 = { sizeof (SignedXml_t2168796367), -1, sizeof(SignedXml_t2168796367_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1407[10] = 
{
	SignedXml_t2168796367::get_offset_of_m_signature_0(),
	SignedXml_t2168796367::get_offset_of_key_1(),
	SignedXml_t2168796367::get_offset_of_envdoc_2(),
	SignedXml_t2168796367::get_offset_of_pkEnumerator_3(),
	SignedXml_t2168796367::get_offset_of_signatureElement_4(),
	SignedXml_t2168796367::get_offset_of_hashes_5(),
	SignedXml_t2168796367::get_offset_of_xmlResolver_6(),
	SignedXml_t2168796367::get_offset_of_manifests_7(),
	SignedXml_t2168796367::get_offset_of__x509Enumerator_8(),
	SignedXml_t2168796367_StaticFields::get_offset_of_whitespaceChars_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1408 = { sizeof (SymmetricKeyWrap_t405371701), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1409 = { sizeof (TransformChain_t1669092815), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1409[1] = 
{
	TransformChain_t1669092815::get_offset_of_chain_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1410 = { sizeof (Transform_t1105379765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1410[3] = 
{
	Transform_t1105379765::get_offset_of_algo_0(),
	Transform_t1105379765::get_offset_of_xmlResolver_1(),
	Transform_t1105379765::get_offset_of_propagated_namespaces_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1411 = { sizeof (X509IssuerSerial_t3270105241)+ sizeof (Il2CppObject), sizeof(X509IssuerSerial_t3270105241_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1411[2] = 
{
	X509IssuerSerial_t3270105241::get_offset_of__issuerName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	X509IssuerSerial_t3270105241::get_offset_of__serialNumber_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1412 = { sizeof (XmlDecryptionTransform_t4000891284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1412[3] = 
{
	XmlDecryptionTransform_t4000891284::get_offset_of_encryptedXml_3(),
	XmlDecryptionTransform_t4000891284::get_offset_of_inputObj_4(),
	XmlDecryptionTransform_t4000891284::get_offset_of_exceptUris_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1413 = { sizeof (XmlDsigBase64Transform_t260084727), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1413[1] = 
{
	XmlDsigBase64Transform_t260084727::get_offset_of_cs_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1414 = { sizeof (XmlDsigC14NTransform_t3949211521), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1414[2] = 
{
	XmlDsigC14NTransform_t3949211521::get_offset_of_canonicalizer_3(),
	XmlDsigC14NTransform_t3949211521::get_offset_of_s_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1415 = { sizeof (XmlDsigC14NWithCommentsTransform_t1074875822), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1416 = { sizeof (XmlDsigEnvelopedSignatureTransform_t2851260348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1416[2] = 
{
	XmlDsigEnvelopedSignatureTransform_t2851260348::get_offset_of_comments_3(),
	XmlDsigEnvelopedSignatureTransform_t2851260348::get_offset_of_inputObj_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1417 = { sizeof (XmlDsigExcC14NTransform_t586418029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1417[3] = 
{
	XmlDsigExcC14NTransform_t586418029::get_offset_of_canonicalizer_3(),
	XmlDsigExcC14NTransform_t586418029::get_offset_of_s_4(),
	XmlDsigExcC14NTransform_t586418029::get_offset_of_inclusiveNamespacesPrefixList_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1418 = { sizeof (XmlDsigExcC14NWithCommentsTransform_t1461379654), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1419 = { sizeof (XmlDsigNodeList_t4194189051), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1419[1] = 
{
	XmlDsigNodeList_t4194189051::get_offset_of__rgNodes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1420 = { sizeof (XmlDsigXPathTransform_t1962434658), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1420[3] = 
{
	XmlDsigXPathTransform_t1962434658::get_offset_of_xpath_3(),
	XmlDsigXPathTransform_t1962434658::get_offset_of_doc_4(),
	XmlDsigXPathTransform_t1962434658::get_offset_of_ctx_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1421 = { sizeof (XmlDsigXPathContext_t2680727779), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1421[1] = 
{
	XmlDsigXPathContext_t2680727779::get_offset_of_here_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1422 = { sizeof (XmlDsigXPathFunctionHere_t59088751), -1, sizeof(XmlDsigXPathFunctionHere_t59088751_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1422[2] = 
{
	XmlDsigXPathFunctionHere_t59088751_StaticFields::get_offset_of_types_0(),
	XmlDsigXPathFunctionHere_t59088751::get_offset_of_xpathNode_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1423 = { sizeof (XmlDsigXsltTransform_t2973394665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1423[3] = 
{
	XmlDsigXsltTransform_t2973394665::get_offset_of_comments_3(),
	XmlDsigXsltTransform_t2973394665::get_offset_of_xnl_4(),
	XmlDsigXsltTransform_t2973394665::get_offset_of_inputDoc_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1424 = { sizeof (XmlSignature_t1498580954), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1425 = { sizeof (XmlSignatureStreamReader_t1967805337), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1425[2] = 
{
	XmlSignatureStreamReader_t1967805337::get_offset_of_source_2(),
	XmlSignatureStreamReader_t1967805337::get_offset_of_cache_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1426 = { sizeof (MonoTODOAttribute_t4131080583), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1426[1] = 
{
	MonoTODOAttribute_t4131080583::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1427 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255363), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1427[5] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_U24U24fieldU2D1_1(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_U24U24fieldU2D2_2(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_U24U24fieldU2D3_3(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_U24U24fieldU2D4_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1428 = { sizeof (U24ArrayTypeU248_t3244137464)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU248_t3244137464 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1429 = { sizeof (U3CModuleU3E_t692745529), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1430 = { sizeof (XPathParser_t4136515887), -1, sizeof(XPathParser_t4136515887_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1430[17] = 
{
	XPathParser_t4136515887::get_offset_of_Context_0(),
	XPathParser_t4136515887::get_offset_of_ErrorOutput_1(),
	XPathParser_t4136515887::get_offset_of_eof_token_2(),
	XPathParser_t4136515887::get_offset_of_debug_3(),
	XPathParser_t4136515887_StaticFields::get_offset_of_yyFinal_4(),
	XPathParser_t4136515887_StaticFields::get_offset_of_yyNames_5(),
	XPathParser_t4136515887::get_offset_of_yyExpectingState_6(),
	XPathParser_t4136515887::get_offset_of_yyMax_7(),
	XPathParser_t4136515887_StaticFields::get_offset_of_yyLhs_8(),
	XPathParser_t4136515887_StaticFields::get_offset_of_yyLen_9(),
	XPathParser_t4136515887_StaticFields::get_offset_of_yyDefRed_10(),
	XPathParser_t4136515887_StaticFields::get_offset_of_yyDgoto_11(),
	XPathParser_t4136515887_StaticFields::get_offset_of_yySindex_12(),
	XPathParser_t4136515887_StaticFields::get_offset_of_yyRindex_13(),
	XPathParser_t4136515887_StaticFields::get_offset_of_yyGindex_14(),
	XPathParser_t4136515887_StaticFields::get_offset_of_yyTable_15(),
	XPathParser_t4136515887_StaticFields::get_offset_of_yyCheck_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1431 = { sizeof (YYRules_t2124747439), -1, sizeof(YYRules_t2124747439_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1431[1] = 
{
	YYRules_t2124747439_StaticFields::get_offset_of_yyRule_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1432 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1433 = { sizeof (yyException_t1362412556), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1434 = { sizeof (yyUnexpectedEof_t2049021987), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1435 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1436 = { sizeof (XsltPatternParser_t433897766), -1, sizeof(XsltPatternParser_t433897766_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1436[17] = 
{
	XsltPatternParser_t433897766::get_offset_of_Context_0(),
	XsltPatternParser_t433897766::get_offset_of_ErrorOutput_1(),
	XsltPatternParser_t433897766::get_offset_of_eof_token_2(),
	XsltPatternParser_t433897766::get_offset_of_debug_3(),
	XsltPatternParser_t433897766_StaticFields::get_offset_of_yyFinal_4(),
	XsltPatternParser_t433897766_StaticFields::get_offset_of_yyNames_5(),
	XsltPatternParser_t433897766::get_offset_of_yyExpectingState_6(),
	XsltPatternParser_t433897766::get_offset_of_yyMax_7(),
	XsltPatternParser_t433897766_StaticFields::get_offset_of_yyLhs_8(),
	XsltPatternParser_t433897766_StaticFields::get_offset_of_yyLen_9(),
	XsltPatternParser_t433897766_StaticFields::get_offset_of_yyDefRed_10(),
	XsltPatternParser_t433897766_StaticFields::get_offset_of_yyDgoto_11(),
	XsltPatternParser_t433897766_StaticFields::get_offset_of_yySindex_12(),
	XsltPatternParser_t433897766_StaticFields::get_offset_of_yyRindex_13(),
	XsltPatternParser_t433897766_StaticFields::get_offset_of_yyGindex_14(),
	XsltPatternParser_t433897766_StaticFields::get_offset_of_yyTable_15(),
	XsltPatternParser_t433897766_StaticFields::get_offset_of_yyCheck_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1437 = { sizeof (YYRules_t468763346), -1, sizeof(YYRules_t468763346_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1437[1] = 
{
	YYRules_t468763346_StaticFields::get_offset_of_yyRule_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1438 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1439 = { sizeof (yyException_t795026074), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1440 = { sizeof (yyUnexpectedEof_t3119335031), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1441 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1442 = { sizeof (Tokenizer_t3184446799), -1, sizeof(Tokenizer_t3184446799_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1442[10] = 
{
	Tokenizer_t3184446799::get_offset_of_m_rgchInput_0(),
	Tokenizer_t3184446799::get_offset_of_m_ich_1(),
	Tokenizer_t3184446799::get_offset_of_m_cch_2(),
	Tokenizer_t3184446799::get_offset_of_m_iToken_3(),
	Tokenizer_t3184446799::get_offset_of_m_iTokenPrev_4(),
	Tokenizer_t3184446799::get_offset_of_m_objToken_5(),
	Tokenizer_t3184446799::get_offset_of_m_fPrevWasOperator_6(),
	Tokenizer_t3184446799::get_offset_of_m_fThisIsOperator_7(),
	Tokenizer_t3184446799_StaticFields::get_offset_of_s_mapTokens_8(),
	Tokenizer_t3184446799_StaticFields::get_offset_of_s_rgTokenMap_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1443 = { sizeof (Locale_t4128636110), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1444 = { sizeof (MonoTODOAttribute_t4131080584), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1445 = { sizeof (XmlSchemaValidatingReader_t3113890617), -1, sizeof(XmlSchemaValidatingReader_t3113890617_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1445[17] = 
{
	XmlSchemaValidatingReader_t3113890617_StaticFields::get_offset_of_emptyAttributeArray_3(),
	XmlSchemaValidatingReader_t3113890617::get_offset_of_reader_4(),
	XmlSchemaValidatingReader_t3113890617::get_offset_of_options_5(),
	XmlSchemaValidatingReader_t3113890617::get_offset_of_v_6(),
	XmlSchemaValidatingReader_t3113890617::get_offset_of_getter_7(),
	XmlSchemaValidatingReader_t3113890617::get_offset_of_xsinfo_8(),
	XmlSchemaValidatingReader_t3113890617::get_offset_of_readerLineInfo_9(),
	XmlSchemaValidatingReader_t3113890617::get_offset_of_nsResolver_10(),
	XmlSchemaValidatingReader_t3113890617::get_offset_of_defaultAttributes_11(),
	XmlSchemaValidatingReader_t3113890617::get_offset_of_currentDefaultAttribute_12(),
	XmlSchemaValidatingReader_t3113890617::get_offset_of_defaultAttributesCache_13(),
	XmlSchemaValidatingReader_t3113890617::get_offset_of_defaultAttributeConsumed_14(),
	XmlSchemaValidatingReader_t3113890617::get_offset_of_currentAttrType_15(),
	XmlSchemaValidatingReader_t3113890617::get_offset_of_validationDone_16(),
	XmlSchemaValidatingReader_t3113890617::get_offset_of_element_17(),
	XmlSchemaValidatingReader_t3113890617_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_18(),
	XmlSchemaValidatingReader_t3113890617_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1446 = { sizeof (U3CXmlSchemaValidatingReaderU3Ec__AnonStorey4_t286117946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1446[2] = 
{
	U3CXmlSchemaValidatingReaderU3Ec__AnonStorey4_t286117946::get_offset_of_settings_0(),
	U3CXmlSchemaValidatingReaderU3Ec__AnonStorey4_t286117946::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1447 = { sizeof (XsdIdentitySelector_t574258590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1447[3] = 
{
	XsdIdentitySelector_t574258590::get_offset_of_selectorPaths_0(),
	XsdIdentitySelector_t574258590::get_offset_of_fields_1(),
	XsdIdentitySelector_t574258590::get_offset_of_cachedFields_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1448 = { sizeof (XsdIdentityField_t1964115728), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1448[2] = 
{
	XsdIdentityField_t1964115728::get_offset_of_fieldPaths_0(),
	XsdIdentityField_t1964115728::get_offset_of_index_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1449 = { sizeof (XsdIdentityPath_t991900844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1449[2] = 
{
	XsdIdentityPath_t991900844::get_offset_of_OrderedSteps_0(),
	XsdIdentityPath_t991900844::get_offset_of_Descendants_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1450 = { sizeof (XsdIdentityStep_t1480907129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1450[6] = 
{
	XsdIdentityStep_t1480907129::get_offset_of_IsCurrent_0(),
	XsdIdentityStep_t1480907129::get_offset_of_IsAttribute_1(),
	XsdIdentityStep_t1480907129::get_offset_of_IsAnyName_2(),
	XsdIdentityStep_t1480907129::get_offset_of_NsName_3(),
	XsdIdentityStep_t1480907129::get_offset_of_Name_4(),
	XsdIdentityStep_t1480907129::get_offset_of_Namespace_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1451 = { sizeof (XsdKeyEntryField_t3552275292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1451[13] = 
{
	XsdKeyEntryField_t3552275292::get_offset_of_entry_0(),
	XsdKeyEntryField_t3552275292::get_offset_of_field_1(),
	XsdKeyEntryField_t3552275292::get_offset_of_FieldFound_2(),
	XsdKeyEntryField_t3552275292::get_offset_of_FieldLineNumber_3(),
	XsdKeyEntryField_t3552275292::get_offset_of_FieldLinePosition_4(),
	XsdKeyEntryField_t3552275292::get_offset_of_FieldHasLineInfo_5(),
	XsdKeyEntryField_t3552275292::get_offset_of_FieldType_6(),
	XsdKeyEntryField_t3552275292::get_offset_of_Identity_7(),
	XsdKeyEntryField_t3552275292::get_offset_of_IsXsiNil_8(),
	XsdKeyEntryField_t3552275292::get_offset_of_FieldFoundDepth_9(),
	XsdKeyEntryField_t3552275292::get_offset_of_FieldFoundPath_10(),
	XsdKeyEntryField_t3552275292::get_offset_of_Consuming_11(),
	XsdKeyEntryField_t3552275292::get_offset_of_Consumed_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1452 = { sizeof (XsdKeyEntryFieldCollection_t3698183622), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1453 = { sizeof (XsdKeyEntry_t693496666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1453[8] = 
{
	XsdKeyEntry_t693496666::get_offset_of_StartDepth_0(),
	XsdKeyEntry_t693496666::get_offset_of_SelectorLineNumber_1(),
	XsdKeyEntry_t693496666::get_offset_of_SelectorLinePosition_2(),
	XsdKeyEntry_t693496666::get_offset_of_SelectorHasLineInfo_3(),
	XsdKeyEntry_t693496666::get_offset_of_KeyFields_4(),
	XsdKeyEntry_t693496666::get_offset_of_KeyRefFound_5(),
	XsdKeyEntry_t693496666::get_offset_of_OwnerSequence_6(),
	XsdKeyEntry_t693496666::get_offset_of_keyFound_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1454 = { sizeof (XsdKeyEntryCollection_t3090959213), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1455 = { sizeof (XsdKeyTable_t2156891743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1455[9] = 
{
	XsdKeyTable_t2156891743::get_offset_of_alwaysTrue_0(),
	XsdKeyTable_t2156891743::get_offset_of_selector_1(),
	XsdKeyTable_t2156891743::get_offset_of_source_2(),
	XsdKeyTable_t2156891743::get_offset_of_qname_3(),
	XsdKeyTable_t2156891743::get_offset_of_refKeyName_4(),
	XsdKeyTable_t2156891743::get_offset_of_Entries_5(),
	XsdKeyTable_t2156891743::get_offset_of_FinishedEntries_6(),
	XsdKeyTable_t2156891743::get_offset_of_StartDepth_7(),
	XsdKeyTable_t2156891743::get_offset_of_ReferencedKey_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1456 = { sizeof (XsdParticleStateManager_t726654767), -1, sizeof(XsdParticleStateManager_t726654767_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1456[6] = 
{
	XsdParticleStateManager_t726654767::get_offset_of_table_0(),
	XsdParticleStateManager_t726654767::get_offset_of_processContents_1(),
	XsdParticleStateManager_t726654767::get_offset_of_CurrentElement_2(),
	XsdParticleStateManager_t726654767::get_offset_of_ContextStack_3(),
	XsdParticleStateManager_t726654767::get_offset_of_Context_4(),
	XsdParticleStateManager_t726654767_StaticFields::get_offset_of_U3CU3Ef__switchU24map2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1457 = { sizeof (XsdValidationState_t376578997), -1, sizeof(XsdValidationState_t376578997_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1457[3] = 
{
	XsdValidationState_t376578997_StaticFields::get_offset_of_invalid_0(),
	XsdValidationState_t376578997::get_offset_of_occured_1(),
	XsdValidationState_t376578997::get_offset_of_manager_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1458 = { sizeof (XsdElementValidationState_t2214590119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1458[1] = 
{
	XsdElementValidationState_t2214590119::get_offset_of_element_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1459 = { sizeof (XsdSequenceValidationState_t429792968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1459[4] = 
{
	XsdSequenceValidationState_t429792968::get_offset_of_seq_3(),
	XsdSequenceValidationState_t429792968::get_offset_of_current_4(),
	XsdSequenceValidationState_t429792968::get_offset_of_currentAutomata_5(),
	XsdSequenceValidationState_t429792968::get_offset_of_emptiable_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1460 = { sizeof (XsdChoiceValidationState_t2566230191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1460[3] = 
{
	XsdChoiceValidationState_t2566230191::get_offset_of_choice_3(),
	XsdChoiceValidationState_t2566230191::get_offset_of_emptiable_4(),
	XsdChoiceValidationState_t2566230191::get_offset_of_emptiableComputed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1461 = { sizeof (XsdAllValidationState_t2703884157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1461[2] = 
{
	XsdAllValidationState_t2703884157::get_offset_of_all_3(),
	XsdAllValidationState_t2703884157::get_offset_of_consumed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1462 = { sizeof (XsdAnyValidationState_t3421545252), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1462[1] = 
{
	XsdAnyValidationState_t3421545252::get_offset_of_any_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1463 = { sizeof (XsdAppendedValidationState_t3608891238), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1463[2] = 
{
	XsdAppendedValidationState_t3608891238::get_offset_of_head_3(),
	XsdAppendedValidationState_t3608891238::get_offset_of_rest_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1464 = { sizeof (XsdEmptyValidationState_t1344146143), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1465 = { sizeof (XsdInvalidValidationState_t3749995458), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1466 = { sizeof (XsdValidatingReader_t3961132625), -1, sizeof(XsdValidatingReader_t3961132625_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1466[30] = 
{
	XsdValidatingReader_t3961132625_StaticFields::get_offset_of_emptyAttributeArray_3(),
	XsdValidatingReader_t3961132625::get_offset_of_reader_4(),
	XsdValidatingReader_t3961132625::get_offset_of_resolver_5(),
	XsdValidatingReader_t3961132625::get_offset_of_sourceReaderSchemaInfo_6(),
	XsdValidatingReader_t3961132625::get_offset_of_readerLineInfo_7(),
	XsdValidatingReader_t3961132625::get_offset_of_validationType_8(),
	XsdValidatingReader_t3961132625::get_offset_of_schemas_9(),
	XsdValidatingReader_t3961132625::get_offset_of_namespaces_10(),
	XsdValidatingReader_t3961132625::get_offset_of_validationStarted_11(),
	XsdValidatingReader_t3961132625::get_offset_of_checkIdentity_12(),
	XsdValidatingReader_t3961132625::get_offset_of_idManager_13(),
	XsdValidatingReader_t3961132625::get_offset_of_checkKeyConstraints_14(),
	XsdValidatingReader_t3961132625::get_offset_of_keyTables_15(),
	XsdValidatingReader_t3961132625::get_offset_of_currentKeyFieldConsumers_16(),
	XsdValidatingReader_t3961132625::get_offset_of_tmpKeyrefPool_17(),
	XsdValidatingReader_t3961132625::get_offset_of_elementQNameStack_18(),
	XsdValidatingReader_t3961132625::get_offset_of_state_19(),
	XsdValidatingReader_t3961132625::get_offset_of_skipValidationDepth_20(),
	XsdValidatingReader_t3961132625::get_offset_of_xsiNilDepth_21(),
	XsdValidatingReader_t3961132625::get_offset_of_storedCharacters_22(),
	XsdValidatingReader_t3961132625::get_offset_of_shouldValidateCharacters_23(),
	XsdValidatingReader_t3961132625::get_offset_of_defaultAttributes_24(),
	XsdValidatingReader_t3961132625::get_offset_of_currentDefaultAttribute_25(),
	XsdValidatingReader_t3961132625::get_offset_of_defaultAttributesCache_26(),
	XsdValidatingReader_t3961132625::get_offset_of_defaultAttributeConsumed_27(),
	XsdValidatingReader_t3961132625::get_offset_of_currentAttrType_28(),
	XsdValidatingReader_t3961132625::get_offset_of_ValidationEventHandler_29(),
	XsdValidatingReader_t3961132625_StaticFields::get_offset_of_U3CU3Ef__switchU24map3_30(),
	XsdValidatingReader_t3961132625_StaticFields::get_offset_of_U3CU3Ef__switchU24map4_31(),
	XsdValidatingReader_t3961132625_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1467 = { sizeof (XsdValidationContext_t1104170526), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1467[3] = 
{
	XsdValidationContext_t1104170526::get_offset_of_xsi_type_0(),
	XsdValidationContext_t1104170526::get_offset_of_State_1(),
	XsdValidationContext_t1104170526::get_offset_of_element_stack_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1468 = { sizeof (XsdIDManager_t1008806102), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1468[3] = 
{
	XsdIDManager_t1008806102::get_offset_of_idList_0(),
	XsdIDManager_t1008806102::get_offset_of_missingIDReferences_1(),
	XsdIDManager_t1008806102::get_offset_of_thisElementId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1469 = { sizeof (XsdWildcard_t2790389089), -1, sizeof(XsdWildcard_t2790389089_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1469[10] = 
{
	XsdWildcard_t2790389089::get_offset_of_xsobj_0(),
	XsdWildcard_t2790389089::get_offset_of_ResolvedProcessing_1(),
	XsdWildcard_t2790389089::get_offset_of_TargetNamespace_2(),
	XsdWildcard_t2790389089::get_offset_of_SkipCompile_3(),
	XsdWildcard_t2790389089::get_offset_of_HasValueAny_4(),
	XsdWildcard_t2790389089::get_offset_of_HasValueLocal_5(),
	XsdWildcard_t2790389089::get_offset_of_HasValueOther_6(),
	XsdWildcard_t2790389089::get_offset_of_HasValueTargetNamespace_7(),
	XsdWildcard_t2790389089::get_offset_of_ResolvedNamespaces_8(),
	XsdWildcard_t2790389089_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1470 = { sizeof (DTMXPathDocument2_t2335062454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1470[8] = 
{
	DTMXPathDocument2_t2335062454::get_offset_of_root_0(),
	DTMXPathDocument2_t2335062454::get_offset_of_NameTable_1(),
	DTMXPathDocument2_t2335062454::get_offset_of_Nodes_2(),
	DTMXPathDocument2_t2335062454::get_offset_of_Attributes_3(),
	DTMXPathDocument2_t2335062454::get_offset_of_Namespaces_4(),
	DTMXPathDocument2_t2335062454::get_offset_of_AtomicStringPool_5(),
	DTMXPathDocument2_t2335062454::get_offset_of_NonAtomicStringPool_6(),
	DTMXPathDocument2_t2335062454::get_offset_of_IdTable_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1471 = { sizeof (DTMXPathDocumentBuilder2_t3353543192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1471[27] = 
{
	DTMXPathDocumentBuilder2_t3353543192::get_offset_of_xmlReader_0(),
	DTMXPathDocumentBuilder2_t3353543192::get_offset_of_validatingReader_1(),
	DTMXPathDocumentBuilder2_t3353543192::get_offset_of_xmlSpace_2(),
	DTMXPathDocumentBuilder2_t3353543192::get_offset_of_nameTable_3(),
	DTMXPathDocumentBuilder2_t3353543192::get_offset_of_lineInfo_4(),
	DTMXPathDocumentBuilder2_t3353543192::get_offset_of_nodeCapacity_5(),
	DTMXPathDocumentBuilder2_t3353543192::get_offset_of_attributeCapacity_6(),
	DTMXPathDocumentBuilder2_t3353543192::get_offset_of_nsCapacity_7(),
	DTMXPathDocumentBuilder2_t3353543192::get_offset_of_nodes_8(),
	DTMXPathDocumentBuilder2_t3353543192::get_offset_of_attributes_9(),
	DTMXPathDocumentBuilder2_t3353543192::get_offset_of_namespaces_10(),
	DTMXPathDocumentBuilder2_t3353543192::get_offset_of_atomicStringPool_11(),
	DTMXPathDocumentBuilder2_t3353543192::get_offset_of_atomicIndex_12(),
	DTMXPathDocumentBuilder2_t3353543192::get_offset_of_nonAtomicStringPool_13(),
	DTMXPathDocumentBuilder2_t3353543192::get_offset_of_nonAtomicIndex_14(),
	DTMXPathDocumentBuilder2_t3353543192::get_offset_of_idTable_15(),
	DTMXPathDocumentBuilder2_t3353543192::get_offset_of_nodeIndex_16(),
	DTMXPathDocumentBuilder2_t3353543192::get_offset_of_attributeIndex_17(),
	DTMXPathDocumentBuilder2_t3353543192::get_offset_of_nsIndex_18(),
	DTMXPathDocumentBuilder2_t3353543192::get_offset_of_hasAttributes_19(),
	DTMXPathDocumentBuilder2_t3353543192::get_offset_of_hasLocalNs_20(),
	DTMXPathDocumentBuilder2_t3353543192::get_offset_of_attrIndexAtStart_21(),
	DTMXPathDocumentBuilder2_t3353543192::get_offset_of_nsIndexAtStart_22(),
	DTMXPathDocumentBuilder2_t3353543192::get_offset_of_lastNsInScope_23(),
	DTMXPathDocumentBuilder2_t3353543192::get_offset_of_skipRead_24(),
	DTMXPathDocumentBuilder2_t3353543192::get_offset_of_parentStack_25(),
	DTMXPathDocumentBuilder2_t3353543192::get_offset_of_parentStackIndex_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1472 = { sizeof (DTMXPathDocumentWriter2_t2031536402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1472[26] = 
{
	DTMXPathDocumentWriter2_t2031536402::get_offset_of_nameTable_1(),
	DTMXPathDocumentWriter2_t2031536402::get_offset_of_nodeCapacity_2(),
	DTMXPathDocumentWriter2_t2031536402::get_offset_of_attributeCapacity_3(),
	DTMXPathDocumentWriter2_t2031536402::get_offset_of_nsCapacity_4(),
	DTMXPathDocumentWriter2_t2031536402::get_offset_of_nodes_5(),
	DTMXPathDocumentWriter2_t2031536402::get_offset_of_attributes_6(),
	DTMXPathDocumentWriter2_t2031536402::get_offset_of_namespaces_7(),
	DTMXPathDocumentWriter2_t2031536402::get_offset_of_atomicStringPool_8(),
	DTMXPathDocumentWriter2_t2031536402::get_offset_of_atomicIndex_9(),
	DTMXPathDocumentWriter2_t2031536402::get_offset_of_nonAtomicStringPool_10(),
	DTMXPathDocumentWriter2_t2031536402::get_offset_of_nonAtomicIndex_11(),
	DTMXPathDocumentWriter2_t2031536402::get_offset_of_idTable_12(),
	DTMXPathDocumentWriter2_t2031536402::get_offset_of_nodeIndex_13(),
	DTMXPathDocumentWriter2_t2031536402::get_offset_of_attributeIndex_14(),
	DTMXPathDocumentWriter2_t2031536402::get_offset_of_nsIndex_15(),
	DTMXPathDocumentWriter2_t2031536402::get_offset_of_parentStack_16(),
	DTMXPathDocumentWriter2_t2031536402::get_offset_of_parentStackIndex_17(),
	DTMXPathDocumentWriter2_t2031536402::get_offset_of_hasAttributes_18(),
	DTMXPathDocumentWriter2_t2031536402::get_offset_of_hasLocalNs_19(),
	DTMXPathDocumentWriter2_t2031536402::get_offset_of_attrIndexAtStart_20(),
	DTMXPathDocumentWriter2_t2031536402::get_offset_of_nsIndexAtStart_21(),
	DTMXPathDocumentWriter2_t2031536402::get_offset_of_lastNsInScope_22(),
	DTMXPathDocumentWriter2_t2031536402::get_offset_of_prevSibling_23(),
	DTMXPathDocumentWriter2_t2031536402::get_offset_of_state_24(),
	DTMXPathDocumentWriter2_t2031536402::get_offset_of_openNamespace_25(),
	DTMXPathDocumentWriter2_t2031536402::get_offset_of_isClosed_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1473 = { sizeof (DTMXPathNavigator2_t4118996007), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1473[6] = 
{
	DTMXPathNavigator2_t4118996007::get_offset_of_document_2(),
	DTMXPathNavigator2_t4118996007::get_offset_of_currentIsNode_3(),
	DTMXPathNavigator2_t4118996007::get_offset_of_currentIsAttr_4(),
	DTMXPathNavigator2_t4118996007::get_offset_of_currentNode_5(),
	DTMXPathNavigator2_t4118996007::get_offset_of_currentAttr_6(),
	DTMXPathNavigator2_t4118996007::get_offset_of_currentNs_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1474 = { sizeof (DTMXPathLinkedNode2_t3353097823)+ sizeof (Il2CppObject), sizeof(DTMXPathLinkedNode2_t3353097823_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1474[16] = 
{
	DTMXPathLinkedNode2_t3353097823::get_offset_of_FirstChild_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097823::get_offset_of_Parent_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097823::get_offset_of_PreviousSibling_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097823::get_offset_of_NextSibling_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097823::get_offset_of_FirstAttribute_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097823::get_offset_of_FirstNamespace_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097823::get_offset_of_NodeType_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097823::get_offset_of_BaseURI_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097823::get_offset_of_IsEmptyElement_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097823::get_offset_of_LocalName_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097823::get_offset_of_NamespaceURI_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097823::get_offset_of_Prefix_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097823::get_offset_of_Value_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097823::get_offset_of_XmlLang_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097823::get_offset_of_LineNumber_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097823::get_offset_of_LinePosition_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1475 = { sizeof (DTMXPathAttributeNode2_t3707096872)+ sizeof (Il2CppObject), sizeof(DTMXPathAttributeNode2_t3707096872 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1475[8] = 
{
	DTMXPathAttributeNode2_t3707096872::get_offset_of_OwnerElement_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathAttributeNode2_t3707096872::get_offset_of_NextAttribute_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathAttributeNode2_t3707096872::get_offset_of_LocalName_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathAttributeNode2_t3707096872::get_offset_of_NamespaceURI_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathAttributeNode2_t3707096872::get_offset_of_Prefix_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathAttributeNode2_t3707096872::get_offset_of_Value_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathAttributeNode2_t3707096872::get_offset_of_LineNumber_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathAttributeNode2_t3707096872::get_offset_of_LinePosition_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1476 = { sizeof (DTMXPathNamespaceNode2_t1119120712)+ sizeof (Il2CppObject), sizeof(DTMXPathNamespaceNode2_t1119120712 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1476[4] = 
{
	DTMXPathNamespaceNode2_t1119120712::get_offset_of_DeclaredElement_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathNamespaceNode2_t1119120712::get_offset_of_NextNamespace_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathNamespaceNode2_t1119120712::get_offset_of_Name_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathNamespaceNode2_t1119120712::get_offset_of_Namespace_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1477 = { sizeof (IdPattern_t1261881997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1477[1] = 
{
	IdPattern_t1261881997::get_offset_of_ids_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1478 = { sizeof (KeyPattern_t747253878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1478[1] = 
{
	KeyPattern_t747253878::get_offset_of_key_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1479 = { sizeof (LocationPathPattern_t3015773238), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1479[4] = 
{
	LocationPathPattern_t3015773238::get_offset_of_patternPrevious_0(),
	LocationPathPattern_t3015773238::get_offset_of_isAncestor_1(),
	LocationPathPattern_t3015773238::get_offset_of_nodeTest_2(),
	LocationPathPattern_t3015773238::get_offset_of_filter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1480 = { sizeof (Pattern_t1136864796), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1481 = { sizeof (UnionPattern_t467140937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1481[2] = 
{
	UnionPattern_t467140937::get_offset_of_p0_0(),
	UnionPattern_t467140937::get_offset_of_p1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1482 = { sizeof (XPathEditableDocument_t3642283412), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1482[1] = 
{
	XPathEditableDocument_t3642283412::get_offset_of_node_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1483 = { sizeof (XmlDocumentInsertionWriter_t2537499277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1483[6] = 
{
	XmlDocumentInsertionWriter_t2537499277::get_offset_of_parent_1(),
	XmlDocumentInsertionWriter_t2537499277::get_offset_of_current_2(),
	XmlDocumentInsertionWriter_t2537499277::get_offset_of_nextSibling_3(),
	XmlDocumentInsertionWriter_t2537499277::get_offset_of_state_4(),
	XmlDocumentInsertionWriter_t2537499277::get_offset_of_attribute_5(),
	XmlDocumentInsertionWriter_t2537499277::get_offset_of_Closed_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1484 = { sizeof (XmlDocumentEditableNavigator_t22456660), -1, sizeof(XmlDocumentEditableNavigator_t22456660_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1484[3] = 
{
	XmlDocumentEditableNavigator_t22456660_StaticFields::get_offset_of_isXmlDocumentNavigatorImpl_2(),
	XmlDocumentEditableNavigator_t22456660::get_offset_of_document_3(),
	XmlDocumentEditableNavigator_t22456660::get_offset_of_navigator_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1485 = { sizeof (XslApplyImports_t1877513678), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1486 = { sizeof (XslApplyTemplates_t2247116683), -1, sizeof(XslApplyTemplates_t2247116683_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1486[5] = 
{
	XslApplyTemplates_t2247116683::get_offset_of_select_3(),
	XslApplyTemplates_t2247116683::get_offset_of_mode_4(),
	XslApplyTemplates_t2247116683::get_offset_of_withParams_5(),
	XslApplyTemplates_t2247116683::get_offset_of_sortEvaluator_6(),
	XslApplyTemplates_t2247116683_StaticFields::get_offset_of_U3CU3Ef__switchU24map7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1487 = { sizeof (XslAttribute_t1907873804), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1487[7] = 
{
	XslAttribute_t1907873804::get_offset_of_name_3(),
	XslAttribute_t1907873804::get_offset_of_ns_4(),
	XslAttribute_t1907873804::get_offset_of_calcName_5(),
	XslAttribute_t1907873804::get_offset_of_calcNs_6(),
	XslAttribute_t1907873804::get_offset_of_calcPrefix_7(),
	XslAttribute_t1907873804::get_offset_of_nsDecls_8(),
	XslAttribute_t1907873804::get_offset_of_value_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1488 = { sizeof (XslAvt_t1645109359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1488[2] = 
{
	XslAvt_t1645109359::get_offset_of_simpleString_0(),
	XslAvt_t1645109359::get_offset_of_avtParts_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1489 = { sizeof (AvtPart_t3412049665), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1490 = { sizeof (SimpleAvtPart_t729895317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1490[1] = 
{
	SimpleAvtPart_t729895317::get_offset_of_val_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1491 = { sizeof (XPathAvtPart_t3066905968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1491[1] = 
{
	XPathAvtPart_t3066905968::get_offset_of_expr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1492 = { sizeof (XslCallTemplate_t509413441), -1, sizeof(XslCallTemplate_t509413441_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1492[3] = 
{
	XslCallTemplate_t509413441::get_offset_of_name_3(),
	XslCallTemplate_t509413441::get_offset_of_withParams_4(),
	XslCallTemplate_t509413441_StaticFields::get_offset_of_U3CU3Ef__switchU24map8_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1493 = { sizeof (XslChoose_t3297598844), -1, sizeof(XslChoose_t3297598844_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1493[3] = 
{
	XslChoose_t3297598844::get_offset_of_defaultChoice_3(),
	XslChoose_t3297598844::get_offset_of_conditions_4(),
	XslChoose_t3297598844_StaticFields::get_offset_of_U3CU3Ef__switchU24map9_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1494 = { sizeof (XslComment_t1785218384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1494[1] = 
{
	XslComment_t1785218384::get_offset_of_value_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1495 = { sizeof (XslCompiledElementBase_t1729269235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1495[3] = 
{
	XslCompiledElementBase_t1729269235::get_offset_of_lineNumber_0(),
	XslCompiledElementBase_t1729269235::get_offset_of_linePosition_1(),
	XslCompiledElementBase_t1729269235::get_offset_of_debugInput_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1496 = { sizeof (XslCompiledElement_t50593777), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1497 = { sizeof (XslCopy_t2770797771), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1497[3] = 
{
	XslCopy_t2770797771::get_offset_of_children_3(),
	XslCopy_t2770797771::get_offset_of_useAttributeSets_4(),
	XslCopy_t2770797771::get_offset_of_nsDecls_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1498 = { sizeof (XslCopyOf_t800898188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1498[1] = 
{
	XslCopyOf_t800898188::get_offset_of_select_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1499 = { sizeof (XslElement_t1808680497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1499[9] = 
{
	XslElement_t1808680497::get_offset_of_name_3(),
	XslElement_t1808680497::get_offset_of_ns_4(),
	XslElement_t1808680497::get_offset_of_calcName_5(),
	XslElement_t1808680497::get_offset_of_calcNs_6(),
	XslElement_t1808680497::get_offset_of_calcPrefix_7(),
	XslElement_t1808680497::get_offset_of_nsDecls_8(),
	XslElement_t1808680497::get_offset_of_isEmptyElement_9(),
	XslElement_t1808680497::get_offset_of_value_10(),
	XslElement_t1808680497::get_offset_of_useAttributeSets_11(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
