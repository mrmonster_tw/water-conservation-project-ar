﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.IdentityModel.Claims.Claim
struct Claim_t2327046348;
// System.Collections.Generic.IEqualityComparer`1<System.IdentityModel.Claims.Claim>
struct IEqualityComparer_1_t139411070;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.EndpointIdentity
struct  EndpointIdentity_t3899230012  : public Il2CppObject
{
public:
	// System.IdentityModel.Claims.Claim System.ServiceModel.EndpointIdentity::claim
	Claim_t2327046348 * ___claim_0;
	// System.Collections.Generic.IEqualityComparer`1<System.IdentityModel.Claims.Claim> System.ServiceModel.EndpointIdentity::comparer
	Il2CppObject* ___comparer_1;

public:
	inline static int32_t get_offset_of_claim_0() { return static_cast<int32_t>(offsetof(EndpointIdentity_t3899230012, ___claim_0)); }
	inline Claim_t2327046348 * get_claim_0() const { return ___claim_0; }
	inline Claim_t2327046348 ** get_address_of_claim_0() { return &___claim_0; }
	inline void set_claim_0(Claim_t2327046348 * value)
	{
		___claim_0 = value;
		Il2CppCodeGenWriteBarrier(&___claim_0, value);
	}

	inline static int32_t get_offset_of_comparer_1() { return static_cast<int32_t>(offsetof(EndpointIdentity_t3899230012, ___comparer_1)); }
	inline Il2CppObject* get_comparer_1() const { return ___comparer_1; }
	inline Il2CppObject** get_address_of_comparer_1() { return &___comparer_1; }
	inline void set_comparer_1(Il2CppObject* value)
	{
		___comparer_1 = value;
		Il2CppCodeGenWriteBarrier(&___comparer_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
