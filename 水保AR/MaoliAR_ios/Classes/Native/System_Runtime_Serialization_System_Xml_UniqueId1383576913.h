﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_Guid3193532887.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.UniqueId
struct  UniqueId_t1383576913  : public Il2CppObject
{
public:
	// System.Guid System.Xml.UniqueId::guid
	Guid_t  ___guid_0;
	// System.String System.Xml.UniqueId::id
	String_t* ___id_1;

public:
	inline static int32_t get_offset_of_guid_0() { return static_cast<int32_t>(offsetof(UniqueId_t1383576913, ___guid_0)); }
	inline Guid_t  get_guid_0() const { return ___guid_0; }
	inline Guid_t * get_address_of_guid_0() { return &___guid_0; }
	inline void set_guid_0(Guid_t  value)
	{
		___guid_0 = value;
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(UniqueId_t1383576913, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier(&___id_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
