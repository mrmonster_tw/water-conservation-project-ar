﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_Reflection_BindingFlags2721792723.h"

// System.Web.UI.ControlBuilder
struct ControlBuilder_t2523018631;
// System.Web.UI.TemplateParser
struct TemplateParser_t24149626;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.CodeDom.CodeMemberMethod
struct CodeMemberMethod_t3833357554;
// System.CodeDom.CodeStatementCollection
struct CodeStatementCollection_t1265263501;
// System.Web.Compilation.ILocation
struct ILocation_t3961726794;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.ControlBuilder
struct  ControlBuilder_t2523018631  : public Il2CppObject
{
public:
	// System.Web.UI.ControlBuilder System.Web.UI.ControlBuilder::myNamingContainer
	ControlBuilder_t2523018631 * ___myNamingContainer_1;
	// System.Web.UI.TemplateParser System.Web.UI.ControlBuilder::parser
	TemplateParser_t24149626 * ___parser_2;
	// System.Type System.Web.UI.ControlBuilder::parserType
	Type_t * ___parserType_3;
	// System.Web.UI.ControlBuilder System.Web.UI.ControlBuilder::parentBuilder
	ControlBuilder_t2523018631 * ___parentBuilder_4;
	// System.Type System.Web.UI.ControlBuilder::type
	Type_t * ___type_5;
	// System.String System.Web.UI.ControlBuilder::tagName
	String_t* ___tagName_6;
	// System.String System.Web.UI.ControlBuilder::originalTagName
	String_t* ___originalTagName_7;
	// System.String System.Web.UI.ControlBuilder::id
	String_t* ___id_8;
	// System.Collections.IDictionary System.Web.UI.ControlBuilder::attribs
	Il2CppObject * ___attribs_9;
	// System.Int32 System.Web.UI.ControlBuilder::line
	int32_t ___line_10;
	// System.String System.Web.UI.ControlBuilder::fileName
	String_t* ___fileName_11;
	// System.Boolean System.Web.UI.ControlBuilder::childrenAsProperties
	bool ___childrenAsProperties_12;
	// System.Boolean System.Web.UI.ControlBuilder::isIParserAccessor
	bool ___isIParserAccessor_13;
	// System.Boolean System.Web.UI.ControlBuilder::hasAspCode
	bool ___hasAspCode_14;
	// System.Web.UI.ControlBuilder System.Web.UI.ControlBuilder::defaultPropertyBuilder
	ControlBuilder_t2523018631 * ___defaultPropertyBuilder_15;
	// System.Collections.ArrayList System.Web.UI.ControlBuilder::children
	ArrayList_t2718874744 * ___children_16;
	// System.Collections.ArrayList System.Web.UI.ControlBuilder::templateChildren
	ArrayList_t2718874744 * ___templateChildren_17;
	// System.Boolean System.Web.UI.ControlBuilder::haveParserVariable
	bool ___haveParserVariable_19;
	// System.CodeDom.CodeMemberMethod System.Web.UI.ControlBuilder::method
	CodeMemberMethod_t3833357554 * ___method_20;
	// System.CodeDom.CodeStatementCollection System.Web.UI.ControlBuilder::methodStatements
	CodeStatementCollection_t1265263501 * ___methodStatements_21;
	// System.CodeDom.CodeMemberMethod System.Web.UI.ControlBuilder::renderMethod
	CodeMemberMethod_t3833357554 * ___renderMethod_22;
	// System.Int32 System.Web.UI.ControlBuilder::renderIndex
	int32_t ___renderIndex_23;
	// System.Boolean System.Web.UI.ControlBuilder::isProperty
	bool ___isProperty_24;
	// System.Web.Compilation.ILocation System.Web.UI.ControlBuilder::location
	Il2CppObject * ___location_25;
	// System.Collections.ArrayList System.Web.UI.ControlBuilder::otherTags
	ArrayList_t2718874744 * ___otherTags_26;
	// System.CodeDom.CodeMemberMethod System.Web.UI.ControlBuilder::<DataBindingMethod>k__BackingField
	CodeMemberMethod_t3833357554 * ___U3CDataBindingMethodU3Ek__BackingField_27;

public:
	inline static int32_t get_offset_of_myNamingContainer_1() { return static_cast<int32_t>(offsetof(ControlBuilder_t2523018631, ___myNamingContainer_1)); }
	inline ControlBuilder_t2523018631 * get_myNamingContainer_1() const { return ___myNamingContainer_1; }
	inline ControlBuilder_t2523018631 ** get_address_of_myNamingContainer_1() { return &___myNamingContainer_1; }
	inline void set_myNamingContainer_1(ControlBuilder_t2523018631 * value)
	{
		___myNamingContainer_1 = value;
		Il2CppCodeGenWriteBarrier(&___myNamingContainer_1, value);
	}

	inline static int32_t get_offset_of_parser_2() { return static_cast<int32_t>(offsetof(ControlBuilder_t2523018631, ___parser_2)); }
	inline TemplateParser_t24149626 * get_parser_2() const { return ___parser_2; }
	inline TemplateParser_t24149626 ** get_address_of_parser_2() { return &___parser_2; }
	inline void set_parser_2(TemplateParser_t24149626 * value)
	{
		___parser_2 = value;
		Il2CppCodeGenWriteBarrier(&___parser_2, value);
	}

	inline static int32_t get_offset_of_parserType_3() { return static_cast<int32_t>(offsetof(ControlBuilder_t2523018631, ___parserType_3)); }
	inline Type_t * get_parserType_3() const { return ___parserType_3; }
	inline Type_t ** get_address_of_parserType_3() { return &___parserType_3; }
	inline void set_parserType_3(Type_t * value)
	{
		___parserType_3 = value;
		Il2CppCodeGenWriteBarrier(&___parserType_3, value);
	}

	inline static int32_t get_offset_of_parentBuilder_4() { return static_cast<int32_t>(offsetof(ControlBuilder_t2523018631, ___parentBuilder_4)); }
	inline ControlBuilder_t2523018631 * get_parentBuilder_4() const { return ___parentBuilder_4; }
	inline ControlBuilder_t2523018631 ** get_address_of_parentBuilder_4() { return &___parentBuilder_4; }
	inline void set_parentBuilder_4(ControlBuilder_t2523018631 * value)
	{
		___parentBuilder_4 = value;
		Il2CppCodeGenWriteBarrier(&___parentBuilder_4, value);
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(ControlBuilder_t2523018631, ___type_5)); }
	inline Type_t * get_type_5() const { return ___type_5; }
	inline Type_t ** get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(Type_t * value)
	{
		___type_5 = value;
		Il2CppCodeGenWriteBarrier(&___type_5, value);
	}

	inline static int32_t get_offset_of_tagName_6() { return static_cast<int32_t>(offsetof(ControlBuilder_t2523018631, ___tagName_6)); }
	inline String_t* get_tagName_6() const { return ___tagName_6; }
	inline String_t** get_address_of_tagName_6() { return &___tagName_6; }
	inline void set_tagName_6(String_t* value)
	{
		___tagName_6 = value;
		Il2CppCodeGenWriteBarrier(&___tagName_6, value);
	}

	inline static int32_t get_offset_of_originalTagName_7() { return static_cast<int32_t>(offsetof(ControlBuilder_t2523018631, ___originalTagName_7)); }
	inline String_t* get_originalTagName_7() const { return ___originalTagName_7; }
	inline String_t** get_address_of_originalTagName_7() { return &___originalTagName_7; }
	inline void set_originalTagName_7(String_t* value)
	{
		___originalTagName_7 = value;
		Il2CppCodeGenWriteBarrier(&___originalTagName_7, value);
	}

	inline static int32_t get_offset_of_id_8() { return static_cast<int32_t>(offsetof(ControlBuilder_t2523018631, ___id_8)); }
	inline String_t* get_id_8() const { return ___id_8; }
	inline String_t** get_address_of_id_8() { return &___id_8; }
	inline void set_id_8(String_t* value)
	{
		___id_8 = value;
		Il2CppCodeGenWriteBarrier(&___id_8, value);
	}

	inline static int32_t get_offset_of_attribs_9() { return static_cast<int32_t>(offsetof(ControlBuilder_t2523018631, ___attribs_9)); }
	inline Il2CppObject * get_attribs_9() const { return ___attribs_9; }
	inline Il2CppObject ** get_address_of_attribs_9() { return &___attribs_9; }
	inline void set_attribs_9(Il2CppObject * value)
	{
		___attribs_9 = value;
		Il2CppCodeGenWriteBarrier(&___attribs_9, value);
	}

	inline static int32_t get_offset_of_line_10() { return static_cast<int32_t>(offsetof(ControlBuilder_t2523018631, ___line_10)); }
	inline int32_t get_line_10() const { return ___line_10; }
	inline int32_t* get_address_of_line_10() { return &___line_10; }
	inline void set_line_10(int32_t value)
	{
		___line_10 = value;
	}

	inline static int32_t get_offset_of_fileName_11() { return static_cast<int32_t>(offsetof(ControlBuilder_t2523018631, ___fileName_11)); }
	inline String_t* get_fileName_11() const { return ___fileName_11; }
	inline String_t** get_address_of_fileName_11() { return &___fileName_11; }
	inline void set_fileName_11(String_t* value)
	{
		___fileName_11 = value;
		Il2CppCodeGenWriteBarrier(&___fileName_11, value);
	}

	inline static int32_t get_offset_of_childrenAsProperties_12() { return static_cast<int32_t>(offsetof(ControlBuilder_t2523018631, ___childrenAsProperties_12)); }
	inline bool get_childrenAsProperties_12() const { return ___childrenAsProperties_12; }
	inline bool* get_address_of_childrenAsProperties_12() { return &___childrenAsProperties_12; }
	inline void set_childrenAsProperties_12(bool value)
	{
		___childrenAsProperties_12 = value;
	}

	inline static int32_t get_offset_of_isIParserAccessor_13() { return static_cast<int32_t>(offsetof(ControlBuilder_t2523018631, ___isIParserAccessor_13)); }
	inline bool get_isIParserAccessor_13() const { return ___isIParserAccessor_13; }
	inline bool* get_address_of_isIParserAccessor_13() { return &___isIParserAccessor_13; }
	inline void set_isIParserAccessor_13(bool value)
	{
		___isIParserAccessor_13 = value;
	}

	inline static int32_t get_offset_of_hasAspCode_14() { return static_cast<int32_t>(offsetof(ControlBuilder_t2523018631, ___hasAspCode_14)); }
	inline bool get_hasAspCode_14() const { return ___hasAspCode_14; }
	inline bool* get_address_of_hasAspCode_14() { return &___hasAspCode_14; }
	inline void set_hasAspCode_14(bool value)
	{
		___hasAspCode_14 = value;
	}

	inline static int32_t get_offset_of_defaultPropertyBuilder_15() { return static_cast<int32_t>(offsetof(ControlBuilder_t2523018631, ___defaultPropertyBuilder_15)); }
	inline ControlBuilder_t2523018631 * get_defaultPropertyBuilder_15() const { return ___defaultPropertyBuilder_15; }
	inline ControlBuilder_t2523018631 ** get_address_of_defaultPropertyBuilder_15() { return &___defaultPropertyBuilder_15; }
	inline void set_defaultPropertyBuilder_15(ControlBuilder_t2523018631 * value)
	{
		___defaultPropertyBuilder_15 = value;
		Il2CppCodeGenWriteBarrier(&___defaultPropertyBuilder_15, value);
	}

	inline static int32_t get_offset_of_children_16() { return static_cast<int32_t>(offsetof(ControlBuilder_t2523018631, ___children_16)); }
	inline ArrayList_t2718874744 * get_children_16() const { return ___children_16; }
	inline ArrayList_t2718874744 ** get_address_of_children_16() { return &___children_16; }
	inline void set_children_16(ArrayList_t2718874744 * value)
	{
		___children_16 = value;
		Il2CppCodeGenWriteBarrier(&___children_16, value);
	}

	inline static int32_t get_offset_of_templateChildren_17() { return static_cast<int32_t>(offsetof(ControlBuilder_t2523018631, ___templateChildren_17)); }
	inline ArrayList_t2718874744 * get_templateChildren_17() const { return ___templateChildren_17; }
	inline ArrayList_t2718874744 ** get_address_of_templateChildren_17() { return &___templateChildren_17; }
	inline void set_templateChildren_17(ArrayList_t2718874744 * value)
	{
		___templateChildren_17 = value;
		Il2CppCodeGenWriteBarrier(&___templateChildren_17, value);
	}

	inline static int32_t get_offset_of_haveParserVariable_19() { return static_cast<int32_t>(offsetof(ControlBuilder_t2523018631, ___haveParserVariable_19)); }
	inline bool get_haveParserVariable_19() const { return ___haveParserVariable_19; }
	inline bool* get_address_of_haveParserVariable_19() { return &___haveParserVariable_19; }
	inline void set_haveParserVariable_19(bool value)
	{
		___haveParserVariable_19 = value;
	}

	inline static int32_t get_offset_of_method_20() { return static_cast<int32_t>(offsetof(ControlBuilder_t2523018631, ___method_20)); }
	inline CodeMemberMethod_t3833357554 * get_method_20() const { return ___method_20; }
	inline CodeMemberMethod_t3833357554 ** get_address_of_method_20() { return &___method_20; }
	inline void set_method_20(CodeMemberMethod_t3833357554 * value)
	{
		___method_20 = value;
		Il2CppCodeGenWriteBarrier(&___method_20, value);
	}

	inline static int32_t get_offset_of_methodStatements_21() { return static_cast<int32_t>(offsetof(ControlBuilder_t2523018631, ___methodStatements_21)); }
	inline CodeStatementCollection_t1265263501 * get_methodStatements_21() const { return ___methodStatements_21; }
	inline CodeStatementCollection_t1265263501 ** get_address_of_methodStatements_21() { return &___methodStatements_21; }
	inline void set_methodStatements_21(CodeStatementCollection_t1265263501 * value)
	{
		___methodStatements_21 = value;
		Il2CppCodeGenWriteBarrier(&___methodStatements_21, value);
	}

	inline static int32_t get_offset_of_renderMethod_22() { return static_cast<int32_t>(offsetof(ControlBuilder_t2523018631, ___renderMethod_22)); }
	inline CodeMemberMethod_t3833357554 * get_renderMethod_22() const { return ___renderMethod_22; }
	inline CodeMemberMethod_t3833357554 ** get_address_of_renderMethod_22() { return &___renderMethod_22; }
	inline void set_renderMethod_22(CodeMemberMethod_t3833357554 * value)
	{
		___renderMethod_22 = value;
		Il2CppCodeGenWriteBarrier(&___renderMethod_22, value);
	}

	inline static int32_t get_offset_of_renderIndex_23() { return static_cast<int32_t>(offsetof(ControlBuilder_t2523018631, ___renderIndex_23)); }
	inline int32_t get_renderIndex_23() const { return ___renderIndex_23; }
	inline int32_t* get_address_of_renderIndex_23() { return &___renderIndex_23; }
	inline void set_renderIndex_23(int32_t value)
	{
		___renderIndex_23 = value;
	}

	inline static int32_t get_offset_of_isProperty_24() { return static_cast<int32_t>(offsetof(ControlBuilder_t2523018631, ___isProperty_24)); }
	inline bool get_isProperty_24() const { return ___isProperty_24; }
	inline bool* get_address_of_isProperty_24() { return &___isProperty_24; }
	inline void set_isProperty_24(bool value)
	{
		___isProperty_24 = value;
	}

	inline static int32_t get_offset_of_location_25() { return static_cast<int32_t>(offsetof(ControlBuilder_t2523018631, ___location_25)); }
	inline Il2CppObject * get_location_25() const { return ___location_25; }
	inline Il2CppObject ** get_address_of_location_25() { return &___location_25; }
	inline void set_location_25(Il2CppObject * value)
	{
		___location_25 = value;
		Il2CppCodeGenWriteBarrier(&___location_25, value);
	}

	inline static int32_t get_offset_of_otherTags_26() { return static_cast<int32_t>(offsetof(ControlBuilder_t2523018631, ___otherTags_26)); }
	inline ArrayList_t2718874744 * get_otherTags_26() const { return ___otherTags_26; }
	inline ArrayList_t2718874744 ** get_address_of_otherTags_26() { return &___otherTags_26; }
	inline void set_otherTags_26(ArrayList_t2718874744 * value)
	{
		___otherTags_26 = value;
		Il2CppCodeGenWriteBarrier(&___otherTags_26, value);
	}

	inline static int32_t get_offset_of_U3CDataBindingMethodU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(ControlBuilder_t2523018631, ___U3CDataBindingMethodU3Ek__BackingField_27)); }
	inline CodeMemberMethod_t3833357554 * get_U3CDataBindingMethodU3Ek__BackingField_27() const { return ___U3CDataBindingMethodU3Ek__BackingField_27; }
	inline CodeMemberMethod_t3833357554 ** get_address_of_U3CDataBindingMethodU3Ek__BackingField_27() { return &___U3CDataBindingMethodU3Ek__BackingField_27; }
	inline void set_U3CDataBindingMethodU3Ek__BackingField_27(CodeMemberMethod_t3833357554 * value)
	{
		___U3CDataBindingMethodU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDataBindingMethodU3Ek__BackingField_27, value);
	}
};

struct ControlBuilder_t2523018631_StaticFields
{
public:
	// System.Reflection.BindingFlags System.Web.UI.ControlBuilder::FlagsNoCase
	int32_t ___FlagsNoCase_0;
	// System.Int32 System.Web.UI.ControlBuilder::nextID
	int32_t ___nextID_18;

public:
	inline static int32_t get_offset_of_FlagsNoCase_0() { return static_cast<int32_t>(offsetof(ControlBuilder_t2523018631_StaticFields, ___FlagsNoCase_0)); }
	inline int32_t get_FlagsNoCase_0() const { return ___FlagsNoCase_0; }
	inline int32_t* get_address_of_FlagsNoCase_0() { return &___FlagsNoCase_0; }
	inline void set_FlagsNoCase_0(int32_t value)
	{
		___FlagsNoCase_0 = value;
	}

	inline static int32_t get_offset_of_nextID_18() { return static_cast<int32_t>(offsetof(ControlBuilder_t2523018631_StaticFields, ___nextID_18)); }
	inline int32_t get_nextID_18() const { return ___nextID_18; }
	inline int32_t* get_address_of_nextID_18() { return &___nextID_18; }
	inline void set_nextID_18(int32_t value)
	{
		___nextID_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
