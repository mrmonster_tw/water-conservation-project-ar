﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.X509PeerCertificateElement
struct  X509PeerCertificateElement_t2429237239  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct X509PeerCertificateElement_t2429237239_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.X509PeerCertificateElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.X509PeerCertificateElement::find_value
	ConfigurationProperty_t3590861854 * ___find_value_14;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.X509PeerCertificateElement::store_location
	ConfigurationProperty_t3590861854 * ___store_location_15;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.X509PeerCertificateElement::store_name
	ConfigurationProperty_t3590861854 * ___store_name_16;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.X509PeerCertificateElement::x509_find_type
	ConfigurationProperty_t3590861854 * ___x509_find_type_17;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(X509PeerCertificateElement_t2429237239_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier(&___properties_13, value);
	}

	inline static int32_t get_offset_of_find_value_14() { return static_cast<int32_t>(offsetof(X509PeerCertificateElement_t2429237239_StaticFields, ___find_value_14)); }
	inline ConfigurationProperty_t3590861854 * get_find_value_14() const { return ___find_value_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_find_value_14() { return &___find_value_14; }
	inline void set_find_value_14(ConfigurationProperty_t3590861854 * value)
	{
		___find_value_14 = value;
		Il2CppCodeGenWriteBarrier(&___find_value_14, value);
	}

	inline static int32_t get_offset_of_store_location_15() { return static_cast<int32_t>(offsetof(X509PeerCertificateElement_t2429237239_StaticFields, ___store_location_15)); }
	inline ConfigurationProperty_t3590861854 * get_store_location_15() const { return ___store_location_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_store_location_15() { return &___store_location_15; }
	inline void set_store_location_15(ConfigurationProperty_t3590861854 * value)
	{
		___store_location_15 = value;
		Il2CppCodeGenWriteBarrier(&___store_location_15, value);
	}

	inline static int32_t get_offset_of_store_name_16() { return static_cast<int32_t>(offsetof(X509PeerCertificateElement_t2429237239_StaticFields, ___store_name_16)); }
	inline ConfigurationProperty_t3590861854 * get_store_name_16() const { return ___store_name_16; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_store_name_16() { return &___store_name_16; }
	inline void set_store_name_16(ConfigurationProperty_t3590861854 * value)
	{
		___store_name_16 = value;
		Il2CppCodeGenWriteBarrier(&___store_name_16, value);
	}

	inline static int32_t get_offset_of_x509_find_type_17() { return static_cast<int32_t>(offsetof(X509PeerCertificateElement_t2429237239_StaticFields, ___x509_find_type_17)); }
	inline ConfigurationProperty_t3590861854 * get_x509_find_type_17() const { return ___x509_find_type_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_x509_find_type_17() { return &___x509_find_type_17; }
	inline void set_x509_find_type_17(ConfigurationProperty_t3590861854 * value)
	{
		___x509_find_type_17 = value;
		Il2CppCodeGenWriteBarrier(&___x509_find_type_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
