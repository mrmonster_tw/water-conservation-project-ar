﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Security_T2868958784.h"
#include "System_ServiceModel_System_ServiceModel_Security_To681329718.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.X509SecurityTokenParameters
struct  X509SecurityTokenParameters_t3162014039  : public SecurityTokenParameters_t2868958784
{
public:
	// System.ServiceModel.Security.Tokens.X509KeyIdentifierClauseType System.ServiceModel.Security.Tokens.X509SecurityTokenParameters::reference_style
	int32_t ___reference_style_4;

public:
	inline static int32_t get_offset_of_reference_style_4() { return static_cast<int32_t>(offsetof(X509SecurityTokenParameters_t3162014039, ___reference_style_4)); }
	inline int32_t get_reference_style_4() const { return ___reference_style_4; }
	inline int32_t* get_address_of_reference_style_4() { return &___reference_style_4; }
	inline void set_reference_style_4(int32_t value)
	{
		___reference_style_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
