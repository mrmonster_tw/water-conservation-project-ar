﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4116647657;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.WstBinaryExchange
struct  WstBinaryExchange_t134690608  : public Il2CppObject
{
public:
	// System.String System.ServiceModel.Description.WstBinaryExchange::ValueType
	String_t* ___ValueType_0;
	// System.String System.ServiceModel.Description.WstBinaryExchange::EncodingType
	String_t* ___EncodingType_1;
	// System.Byte[] System.ServiceModel.Description.WstBinaryExchange::Value
	ByteU5BU5D_t4116647657* ___Value_2;

public:
	inline static int32_t get_offset_of_ValueType_0() { return static_cast<int32_t>(offsetof(WstBinaryExchange_t134690608, ___ValueType_0)); }
	inline String_t* get_ValueType_0() const { return ___ValueType_0; }
	inline String_t** get_address_of_ValueType_0() { return &___ValueType_0; }
	inline void set_ValueType_0(String_t* value)
	{
		___ValueType_0 = value;
		Il2CppCodeGenWriteBarrier(&___ValueType_0, value);
	}

	inline static int32_t get_offset_of_EncodingType_1() { return static_cast<int32_t>(offsetof(WstBinaryExchange_t134690608, ___EncodingType_1)); }
	inline String_t* get_EncodingType_1() const { return ___EncodingType_1; }
	inline String_t** get_address_of_EncodingType_1() { return &___EncodingType_1; }
	inline void set_EncodingType_1(String_t* value)
	{
		___EncodingType_1 = value;
		Il2CppCodeGenWriteBarrier(&___EncodingType_1, value);
	}

	inline static int32_t get_offset_of_Value_2() { return static_cast<int32_t>(offsetof(WstBinaryExchange_t134690608, ___Value_2)); }
	inline ByteU5BU5D_t4116647657* get_Value_2() const { return ___Value_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_Value_2() { return &___Value_2; }
	inline void set_Value_2(ByteU5BU5D_t4116647657* value)
	{
		___Value_2 = value;
		Il2CppCodeGenWriteBarrier(&___Value_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
