﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeLinePragma
struct  CodeLinePragma_t3085002198  : public Il2CppObject
{
public:
	// System.String System.CodeDom.CodeLinePragma::fileName
	String_t* ___fileName_0;
	// System.Int32 System.CodeDom.CodeLinePragma::lineNumber
	int32_t ___lineNumber_1;

public:
	inline static int32_t get_offset_of_fileName_0() { return static_cast<int32_t>(offsetof(CodeLinePragma_t3085002198, ___fileName_0)); }
	inline String_t* get_fileName_0() const { return ___fileName_0; }
	inline String_t** get_address_of_fileName_0() { return &___fileName_0; }
	inline void set_fileName_0(String_t* value)
	{
		___fileName_0 = value;
		Il2CppCodeGenWriteBarrier(&___fileName_0, value);
	}

	inline static int32_t get_offset_of_lineNumber_1() { return static_cast<int32_t>(offsetof(CodeLinePragma_t3085002198, ___lineNumber_1)); }
	inline int32_t get_lineNumber_1() const { return ___lineNumber_1; }
	inline int32_t* get_address_of_lineNumber_1() { return &___lineNumber_1; }
	inline void set_lineNumber_1(int32_t value)
	{
		___lineNumber_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
