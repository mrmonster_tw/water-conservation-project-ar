﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_HostSecurityManager1435734729.h"
#include "mscorlib_System_Security_HostSecurityManagerOptions756318624.h"
#include "mscorlib_System_Security_NamedPermissionSet2915669112.h"
#include "mscorlib_System_Security_PermissionBuilder1403083973.h"
#include "mscorlib_System_Security_PermissionSet223948603.h"
#include "mscorlib_System_Security_PolicyLevelType244468749.h"
#include "mscorlib_System_Security_SecureString3041467854.h"
#include "mscorlib_System_Security_SecurityContext2435442044.h"
#include "mscorlib_System_Security_SecurityCriticalAttribute2279322844.h"
#include "mscorlib_System_Security_SecurityCriticalScope606020417.h"
#include "mscorlib_System_Security_SecurityElement1046076091.h"
#include "mscorlib_System_Security_SecurityElement_SecurityA3566489056.h"
#include "mscorlib_System_Security_RuntimeDeclSecurityEntry3144469156.h"
#include "mscorlib_System_Security_RuntimeSecurityFrame536173748.h"
#include "mscorlib_System_Security_SecurityFrame1422462475.h"
#include "mscorlib_System_Security_SecurityException975544473.h"
#include "mscorlib_System_Security_RuntimeDeclSecurityActions582952764.h"
#include "mscorlib_System_Security_SecurityManager3383402582.h"
#include "mscorlib_System_Security_SecuritySafeCriticalAttri3505979402.h"
#include "mscorlib_System_Security_SecurityTreatAsSafeAttrib3506736250.h"
#include "mscorlib_System_Security_SecurityZone1272287263.h"
#include "mscorlib_System_Security_SuppressUnmanagedCodeSecur744305558.h"
#include "mscorlib_System_Security_UnverifiableCodeAttribute3228760399.h"
#include "mscorlib_System_Security_XmlSyntaxException2973594484.h"
#include "mscorlib_System_Security_AccessControl_CommonObjec1466880084.h"
#include "mscorlib_System_Security_AccessControl_NativeObjec3028773534.h"
#include "mscorlib_System_Security_AccessControl_ObjectSecur3035716992.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricAlg932037087.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricKe3370779160.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricKey937192061.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricSi2681190756.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricSi3486936014.h"
#include "mscorlib_System_Security_Cryptography_Base64Consta4102031153.h"
#include "mscorlib_System_Security_Cryptography_CipherMode84635067.h"
#include "mscorlib_System_Security_Cryptography_CryptoConfig4201145714.h"
#include "mscorlib_System_Security_Cryptography_CryptoConfig2779508855.h"
#include "mscorlib_System_Security_Cryptography_Cryptographic248831461.h"
#include "mscorlib_System_Security_Cryptography_Cryptographi2790575154.h"
#include "mscorlib_System_Security_Cryptography_CryptoStream2702504504.h"
#include "mscorlib_System_Security_Cryptography_CryptoStream3639658227.h"
#include "mscorlib_System_Security_Cryptography_CspParameters239852639.h"
#include "mscorlib_System_Security_Cryptography_CspProviderF4094439141.h"
#include "mscorlib_System_Security_Cryptography_DES821106792.h"
#include "mscorlib_System_Security_Cryptography_DESTransform4088905499.h"
#include "mscorlib_System_Security_Cryptography_DESCryptoSer1519490285.h"
#include "mscorlib_System_Security_Cryptography_DSA2386879874.h"
#include "mscorlib_System_Security_Cryptography_DSACryptoSer3992668923.h"
#include "mscorlib_System_Security_Cryptography_DSAParameter1885824122.h"
#include "mscorlib_System_Security_Cryptography_DSASignature3677955172.h"
#include "mscorlib_System_Security_Cryptography_DSASignature2007981259.h"
#include "mscorlib_System_Security_Cryptography_FromBase64Tr2018065788.h"
#include "mscorlib_System_Security_Cryptography_FromBase64Tr2896579788.h"
#include "mscorlib_System_Security_Cryptography_HashAlgorith1432317219.h"
#include "mscorlib_System_Security_Cryptography_HMAC2621101144.h"
#include "mscorlib_System_Security_Cryptography_HMACMD52742219965.h"
#include "mscorlib_System_Security_Cryptography_HMACRIPEMD163724196729.h"
#include "mscorlib_System_Security_Cryptography_HMACSHA11952596188.h"
#include "mscorlib_System_Security_Cryptography_HMACSHA2563249253224.h"
#include "mscorlib_System_Security_Cryptography_HMACSHA384117937311.h"
#include "mscorlib_System_Security_Cryptography_HMACSHA512923916539.h"
#include "mscorlib_System_Security_Cryptography_KeyedHashAlgo112861511.h"
#include "mscorlib_System_Security_Cryptography_KeySizes85027896.h"
#include "mscorlib_System_Security_Cryptography_MACTripleDES1631262397.h"
#include "mscorlib_System_Security_Cryptography_MD53177620429.h"
#include "mscorlib_System_Security_Cryptography_MD5CryptoSer3005586042.h"
#include "mscorlib_System_Security_Cryptography_PaddingMode2546806710.h"
#include "mscorlib_System_Security_Cryptography_RandomNumberG386037858.h"
#include "mscorlib_System_Security_Cryptography_RC23167825714.h"
#include "mscorlib_System_Security_Cryptography_RC2CryptoServ662919463.h"
#include "mscorlib_System_Security_Cryptography_RC2Transform458321487.h"
#include "mscorlib_System_Security_Cryptography_Rijndael2986313634.h"
#include "mscorlib_System_Security_Cryptography_RijndaelMana3586970409.h"
#include "mscorlib_System_Security_Cryptography_RijndaelTrans631222241.h"
#include "mscorlib_System_Security_Cryptography_RijndaelMana4102601014.h"
#include "mscorlib_System_Security_Cryptography_RIPEMD160268675434.h"
#include "mscorlib_System_Security_Cryptography_RIPEMD160Man2491561941.h"
#include "mscorlib_System_Security_Cryptography_RNGCryptoSer3397414743.h"
#include "mscorlib_System_Security_Cryptography_RSA2385438082.h"
#include "mscorlib_System_Security_Cryptography_RSACryptoSer2683512874.h"
#include "mscorlib_System_Security_Cryptography_RSAOAEPKeyEx3344463048.h"
#include "mscorlib_System_Security_Cryptography_RSAOAEPKeyEx2041696538.h"
#include "mscorlib_System_Security_Cryptography_RSAParameter1728406613.h"
#include "mscorlib_System_Security_Cryptography_RSAPKCS1KeyE2578812791.h"
#include "mscorlib_System_Security_Cryptography_RSAPKCS1KeyE2761096101.h"
#include "mscorlib_System_Security_Cryptography_RSAPKCS1Sign3767223008.h"
#include "mscorlib_System_Security_Cryptography_RSAPKCS1Sign1514197062.h"
#include "mscorlib_System_Security_Cryptography_SHA11803193667.h"
#include "mscorlib_System_Security_Cryptography_SHA1Internal3251293021.h"
#include "mscorlib_System_Security_Cryptography_SHA1CryptoSe3661059764.h"
#include "mscorlib_System_Security_Cryptography_SHA1Managed1754513891.h"
#include "mscorlib_System_Security_Cryptography_SHA2563672283617.h"
#include "mscorlib_System_Security_Cryptography_SHA256Managed955042874.h"
#include "mscorlib_System_Security_Cryptography_SHA384540967702.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize800 = { sizeof (HostSecurityManager_t1435734729), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize801 = { sizeof (HostSecurityManagerOptions_t756318624)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable801[8] = 
{
	HostSecurityManagerOptions_t756318624::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize802 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize803 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize804 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize805 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize806 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize807 = { sizeof (NamedPermissionSet_t2915669112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable807[2] = 
{
	NamedPermissionSet_t2915669112::get_offset_of_name_7(),
	NamedPermissionSet_t2915669112::get_offset_of_description_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize808 = { sizeof (PermissionBuilder_t1403083973), -1, sizeof(PermissionBuilder_t1403083973_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable808[1] = 
{
	PermissionBuilder_t1403083973_StaticFields::get_offset_of_psNone_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize809 = { sizeof (PermissionSet_t223948603), -1, sizeof(PermissionSet_t223948603_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable809[7] = 
{
	PermissionSet_t223948603_StaticFields::get_offset_of_psUnrestricted_0(),
	PermissionSet_t223948603::get_offset_of_state_1(),
	PermissionSet_t223948603::get_offset_of_list_2(),
	PermissionSet_t223948603::get_offset_of__policyLevel_3(),
	PermissionSet_t223948603::get_offset_of__declsec_4(),
	PermissionSet_t223948603::get_offset_of__readOnly_5(),
	PermissionSet_t223948603_StaticFields::get_offset_of_action_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize810 = { sizeof (PolicyLevelType_t244468749)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable810[5] = 
{
	PolicyLevelType_t244468749::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize811 = { sizeof (SecureString_t3041467854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable811[3] = 
{
	SecureString_t3041467854::get_offset_of_length_0(),
	SecureString_t3041467854::get_offset_of_disposed_1(),
	SecureString_t3041467854::get_offset_of_data_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize812 = { sizeof (SecurityContext_t2435442044), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable812[4] = 
{
	SecurityContext_t2435442044::get_offset_of__capture_0(),
	SecurityContext_t2435442044::get_offset_of__winid_1(),
	SecurityContext_t2435442044::get_offset_of__stack_2(),
	SecurityContext_t2435442044::get_offset_of__suppressFlow_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize813 = { sizeof (SecurityCriticalAttribute_t2279322844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable813[1] = 
{
	SecurityCriticalAttribute_t2279322844::get_offset_of__scope_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize814 = { sizeof (SecurityCriticalScope_t606020417)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable814[3] = 
{
	SecurityCriticalScope_t606020417::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize815 = { sizeof (SecurityElement_t1046076091), -1, sizeof(SecurityElement_t1046076091_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable815[9] = 
{
	SecurityElement_t1046076091::get_offset_of_text_0(),
	SecurityElement_t1046076091::get_offset_of_tag_1(),
	SecurityElement_t1046076091::get_offset_of_attributes_2(),
	SecurityElement_t1046076091::get_offset_of_children_3(),
	SecurityElement_t1046076091_StaticFields::get_offset_of_invalid_tag_chars_4(),
	SecurityElement_t1046076091_StaticFields::get_offset_of_invalid_text_chars_5(),
	SecurityElement_t1046076091_StaticFields::get_offset_of_invalid_attr_name_chars_6(),
	SecurityElement_t1046076091_StaticFields::get_offset_of_invalid_attr_value_chars_7(),
	SecurityElement_t1046076091_StaticFields::get_offset_of_invalid_chars_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize816 = { sizeof (SecurityAttribute_t3566489056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable816[2] = 
{
	SecurityAttribute_t3566489056::get_offset_of__name_0(),
	SecurityAttribute_t3566489056::get_offset_of__value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize817 = { sizeof (RuntimeDeclSecurityEntry_t3144469156)+ sizeof (Il2CppObject), sizeof(RuntimeDeclSecurityEntry_t3144469156 ), 0, 0 };
extern const int32_t g_FieldOffsetTable817[3] = 
{
	RuntimeDeclSecurityEntry_t3144469156::get_offset_of_blob_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RuntimeDeclSecurityEntry_t3144469156::get_offset_of_size_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RuntimeDeclSecurityEntry_t3144469156::get_offset_of_index_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize818 = { sizeof (RuntimeSecurityFrame_t536173748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable818[5] = 
{
	RuntimeSecurityFrame_t536173748::get_offset_of_domain_0(),
	RuntimeSecurityFrame_t536173748::get_offset_of_method_1(),
	RuntimeSecurityFrame_t536173748::get_offset_of_assert_2(),
	RuntimeSecurityFrame_t536173748::get_offset_of_deny_3(),
	RuntimeSecurityFrame_t536173748::get_offset_of_permitonly_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize819 = { sizeof (SecurityFrame_t1422462475)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable819[5] = 
{
	SecurityFrame_t1422462475::get_offset_of__domain_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SecurityFrame_t1422462475::get_offset_of__method_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SecurityFrame_t1422462475::get_offset_of__assert_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SecurityFrame_t1422462475::get_offset_of__deny_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SecurityFrame_t1422462475::get_offset_of__permitonly_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize820 = { sizeof (SecurityException_t975544473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable820[8] = 
{
	SecurityException_t975544473::get_offset_of_permissionState_11(),
	SecurityException_t975544473::get_offset_of_permissionType_12(),
	SecurityException_t975544473::get_offset_of__granted_13(),
	SecurityException_t975544473::get_offset_of__refused_14(),
	SecurityException_t975544473::get_offset_of__demanded_15(),
	SecurityException_t975544473::get_offset_of__firstperm_16(),
	SecurityException_t975544473::get_offset_of__method_17(),
	SecurityException_t975544473::get_offset_of__evidence_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize821 = { sizeof (RuntimeDeclSecurityActions_t582952764)+ sizeof (Il2CppObject), sizeof(RuntimeDeclSecurityActions_t582952764 ), 0, 0 };
extern const int32_t g_FieldOffsetTable821[3] = 
{
	RuntimeDeclSecurityActions_t582952764::get_offset_of_cas_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RuntimeDeclSecurityActions_t582952764::get_offset_of_noncas_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RuntimeDeclSecurityActions_t582952764::get_offset_of_choice_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize822 = { sizeof (SecurityManager_t3383402582), -1, sizeof(SecurityManager_t3383402582_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable822[5] = 
{
	SecurityManager_t3383402582_StaticFields::get_offset_of__lockObject_0(),
	SecurityManager_t3383402582_StaticFields::get_offset_of__hierarchy_1(),
	SecurityManager_t3383402582_StaticFields::get_offset_of__declsecCache_2(),
	SecurityManager_t3383402582_StaticFields::get_offset_of__level_3(),
	SecurityManager_t3383402582_StaticFields::get_offset_of__execution_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize823 = { sizeof (SecuritySafeCriticalAttribute_t3505979402), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize824 = { sizeof (SecurityTreatAsSafeAttribute_t3506736250), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize825 = { sizeof (SecurityZone_t1272287263)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable825[7] = 
{
	SecurityZone_t1272287263::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize826 = { sizeof (SuppressUnmanagedCodeSecurityAttribute_t744305558), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize827 = { sizeof (UnverifiableCodeAttribute_t3228760399), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize828 = { sizeof (XmlSyntaxException_t2973594484), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize829 = { sizeof (CommonObjectSecurity_t1466880084), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize830 = { sizeof (NativeObjectSecurity_t3028773534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize831 = { sizeof (ObjectSecurity_t3035716992), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize832 = { sizeof (AsymmetricAlgorithm_t932037087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable832[2] = 
{
	AsymmetricAlgorithm_t932037087::get_offset_of_KeySizeValue_0(),
	AsymmetricAlgorithm_t932037087::get_offset_of_LegalKeySizesValue_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize833 = { sizeof (AsymmetricKeyExchangeDeformatter_t3370779160), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize834 = { sizeof (AsymmetricKeyExchangeFormatter_t937192061), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize835 = { sizeof (AsymmetricSignatureDeformatter_t2681190756), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize836 = { sizeof (AsymmetricSignatureFormatter_t3486936014), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize837 = { sizeof (Base64Constants_t4102031153), -1, sizeof(Base64Constants_t4102031153_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable837[2] = 
{
	Base64Constants_t4102031153_StaticFields::get_offset_of_EncodeTable_0(),
	Base64Constants_t4102031153_StaticFields::get_offset_of_DecodeTable_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize838 = { sizeof (CipherMode_t84635067)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable838[6] = 
{
	CipherMode_t84635067::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize839 = { sizeof (CryptoConfig_t4201145714), -1, sizeof(CryptoConfig_t4201145714_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable839[3] = 
{
	CryptoConfig_t4201145714_StaticFields::get_offset_of_lockObject_0(),
	CryptoConfig_t4201145714_StaticFields::get_offset_of_algorithms_1(),
	CryptoConfig_t4201145714_StaticFields::get_offset_of_oid_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize840 = { sizeof (CryptoHandler_t2779508855), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable840[5] = 
{
	CryptoHandler_t2779508855::get_offset_of_algorithms_0(),
	CryptoHandler_t2779508855::get_offset_of_oid_1(),
	CryptoHandler_t2779508855::get_offset_of_names_2(),
	CryptoHandler_t2779508855::get_offset_of_classnames_3(),
	CryptoHandler_t2779508855::get_offset_of_level_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize841 = { sizeof (CryptographicException_t248831461), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize842 = { sizeof (CryptographicUnexpectedOperationException_t2790575154), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize843 = { sizeof (CryptoStream_t2702504504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable843[15] = 
{
	CryptoStream_t2702504504::get_offset_of__stream_2(),
	CryptoStream_t2702504504::get_offset_of__transform_3(),
	CryptoStream_t2702504504::get_offset_of__mode_4(),
	CryptoStream_t2702504504::get_offset_of__currentBlock_5(),
	CryptoStream_t2702504504::get_offset_of__disposed_6(),
	CryptoStream_t2702504504::get_offset_of__flushedFinalBlock_7(),
	CryptoStream_t2702504504::get_offset_of__partialCount_8(),
	CryptoStream_t2702504504::get_offset_of__endOfStream_9(),
	CryptoStream_t2702504504::get_offset_of__waitingBlock_10(),
	CryptoStream_t2702504504::get_offset_of__waitingCount_11(),
	CryptoStream_t2702504504::get_offset_of__transformedBlock_12(),
	CryptoStream_t2702504504::get_offset_of__transformedPos_13(),
	CryptoStream_t2702504504::get_offset_of__transformedCount_14(),
	CryptoStream_t2702504504::get_offset_of__workingBlock_15(),
	CryptoStream_t2702504504::get_offset_of__workingCount_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize844 = { sizeof (CryptoStreamMode_t3639658227)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable844[3] = 
{
	CryptoStreamMode_t3639658227::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize845 = { sizeof (CspParameters_t239852639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable845[5] = 
{
	CspParameters_t239852639::get_offset_of__Flags_0(),
	CspParameters_t239852639::get_offset_of_KeyContainerName_1(),
	CspParameters_t239852639::get_offset_of_KeyNumber_2(),
	CspParameters_t239852639::get_offset_of_ProviderName_3(),
	CspParameters_t239852639::get_offset_of_ProviderType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize846 = { sizeof (CspProviderFlags_t4094439141)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable846[9] = 
{
	CspProviderFlags_t4094439141::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize847 = { sizeof (DES_t821106792), -1, sizeof(DES_t821106792_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable847[2] = 
{
	DES_t821106792_StaticFields::get_offset_of_weakKeys_10(),
	DES_t821106792_StaticFields::get_offset_of_semiWeakKeys_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize848 = { sizeof (DESTransform_t4088905499), -1, sizeof(DESTransform_t4088905499_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable848[13] = 
{
	DESTransform_t4088905499_StaticFields::get_offset_of_KEY_BIT_SIZE_12(),
	DESTransform_t4088905499_StaticFields::get_offset_of_KEY_BYTE_SIZE_13(),
	DESTransform_t4088905499_StaticFields::get_offset_of_BLOCK_BIT_SIZE_14(),
	DESTransform_t4088905499_StaticFields::get_offset_of_BLOCK_BYTE_SIZE_15(),
	DESTransform_t4088905499::get_offset_of_keySchedule_16(),
	DESTransform_t4088905499::get_offset_of_byteBuff_17(),
	DESTransform_t4088905499::get_offset_of_dwordBuff_18(),
	DESTransform_t4088905499_StaticFields::get_offset_of_spBoxes_19(),
	DESTransform_t4088905499_StaticFields::get_offset_of_PC1_20(),
	DESTransform_t4088905499_StaticFields::get_offset_of_leftRotTotal_21(),
	DESTransform_t4088905499_StaticFields::get_offset_of_PC2_22(),
	DESTransform_t4088905499_StaticFields::get_offset_of_ipTab_23(),
	DESTransform_t4088905499_StaticFields::get_offset_of_fpTab_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize849 = { sizeof (DESCryptoServiceProvider_t1519490285), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize850 = { sizeof (DSA_t2386879874), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize851 = { sizeof (DSACryptoServiceProvider_t3992668923), -1, sizeof(DSACryptoServiceProvider_t3992668923_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable851[7] = 
{
	DSACryptoServiceProvider_t3992668923::get_offset_of_store_2(),
	DSACryptoServiceProvider_t3992668923::get_offset_of_persistKey_3(),
	DSACryptoServiceProvider_t3992668923::get_offset_of_persisted_4(),
	DSACryptoServiceProvider_t3992668923::get_offset_of_privateKeyExportable_5(),
	DSACryptoServiceProvider_t3992668923::get_offset_of_m_disposed_6(),
	DSACryptoServiceProvider_t3992668923::get_offset_of_dsa_7(),
	DSACryptoServiceProvider_t3992668923_StaticFields::get_offset_of_useMachineKeyStore_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize852 = { sizeof (DSAParameters_t1885824122)+ sizeof (Il2CppObject), sizeof(DSAParameters_t1885824122_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable852[8] = 
{
	DSAParameters_t1885824122::get_offset_of_Counter_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DSAParameters_t1885824122::get_offset_of_G_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DSAParameters_t1885824122::get_offset_of_J_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DSAParameters_t1885824122::get_offset_of_P_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DSAParameters_t1885824122::get_offset_of_Q_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DSAParameters_t1885824122::get_offset_of_Seed_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DSAParameters_t1885824122::get_offset_of_X_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DSAParameters_t1885824122::get_offset_of_Y_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize853 = { sizeof (DSASignatureDeformatter_t3677955172), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable853[1] = 
{
	DSASignatureDeformatter_t3677955172::get_offset_of_dsa_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize854 = { sizeof (DSASignatureFormatter_t2007981259), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable854[1] = 
{
	DSASignatureFormatter_t2007981259::get_offset_of_dsa_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize855 = { sizeof (FromBase64TransformMode_t2018065788)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable855[3] = 
{
	FromBase64TransformMode_t2018065788::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize856 = { sizeof (FromBase64Transform_t2896579788), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable856[5] = 
{
	FromBase64Transform_t2896579788::get_offset_of_mode_0(),
	FromBase64Transform_t2896579788::get_offset_of_accumulator_1(),
	FromBase64Transform_t2896579788::get_offset_of_accPtr_2(),
	FromBase64Transform_t2896579788::get_offset_of_m_disposed_3(),
	FromBase64Transform_t2896579788::get_offset_of_lookupTable_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize857 = { sizeof (HashAlgorithm_t1432317219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable857[4] = 
{
	HashAlgorithm_t1432317219::get_offset_of_HashValue_0(),
	HashAlgorithm_t1432317219::get_offset_of_HashSizeValue_1(),
	HashAlgorithm_t1432317219::get_offset_of_State_2(),
	HashAlgorithm_t1432317219::get_offset_of_disposed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize858 = { sizeof (HMAC_t2621101144), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable858[5] = 
{
	HMAC_t2621101144::get_offset_of__disposed_5(),
	HMAC_t2621101144::get_offset_of__hashName_6(),
	HMAC_t2621101144::get_offset_of__algo_7(),
	HMAC_t2621101144::get_offset_of__block_8(),
	HMAC_t2621101144::get_offset_of__blockSizeValue_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize859 = { sizeof (HMACMD5_t2742219965), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize860 = { sizeof (HMACRIPEMD160_t3724196729), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize861 = { sizeof (HMACSHA1_t1952596188), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize862 = { sizeof (HMACSHA256_t3249253224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize863 = { sizeof (HMACSHA384_t117937311), -1, sizeof(HMACSHA384_t117937311_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable863[2] = 
{
	HMACSHA384_t117937311_StaticFields::get_offset_of_legacy_mode_10(),
	HMACSHA384_t117937311::get_offset_of_legacy_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize864 = { sizeof (HMACSHA512_t923916539), -1, sizeof(HMACSHA512_t923916539_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable864[2] = 
{
	HMACSHA512_t923916539_StaticFields::get_offset_of_legacy_mode_10(),
	HMACSHA512_t923916539::get_offset_of_legacy_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize865 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize866 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize867 = { sizeof (KeyedHashAlgorithm_t112861511), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable867[1] = 
{
	KeyedHashAlgorithm_t112861511::get_offset_of_KeyValue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize868 = { sizeof (KeySizes_t85027896), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable868[3] = 
{
	KeySizes_t85027896::get_offset_of__maxSize_0(),
	KeySizes_t85027896::get_offset_of__minSize_1(),
	KeySizes_t85027896::get_offset_of__skipSize_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize869 = { sizeof (MACTripleDES_t1631262397), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable869[3] = 
{
	MACTripleDES_t1631262397::get_offset_of_tdes_5(),
	MACTripleDES_t1631262397::get_offset_of_mac_6(),
	MACTripleDES_t1631262397::get_offset_of_m_disposed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize870 = { sizeof (MD5_t3177620429), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize871 = { sizeof (MD5CryptoServiceProvider_t3005586042), -1, sizeof(MD5CryptoServiceProvider_t3005586042_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable871[6] = 
{
	MD5CryptoServiceProvider_t3005586042::get_offset_of__H_4(),
	MD5CryptoServiceProvider_t3005586042::get_offset_of_buff_5(),
	MD5CryptoServiceProvider_t3005586042::get_offset_of_count_6(),
	MD5CryptoServiceProvider_t3005586042::get_offset_of__ProcessingBuffer_7(),
	MD5CryptoServiceProvider_t3005586042::get_offset_of__ProcessingBufferCount_8(),
	MD5CryptoServiceProvider_t3005586042_StaticFields::get_offset_of_K_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize872 = { sizeof (PaddingMode_t2546806710)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable872[6] = 
{
	PaddingMode_t2546806710::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize873 = { sizeof (RandomNumberGenerator_t386037858), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize874 = { sizeof (RC2_t3167825714), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable874[1] = 
{
	RC2_t3167825714::get_offset_of_EffectiveKeySizeValue_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize875 = { sizeof (RC2CryptoServiceProvider_t662919463), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize876 = { sizeof (RC2Transform_t458321487), -1, sizeof(RC2Transform_t458321487_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable876[7] = 
{
	RC2Transform_t458321487::get_offset_of_R0_12(),
	RC2Transform_t458321487::get_offset_of_R1_13(),
	RC2Transform_t458321487::get_offset_of_R2_14(),
	RC2Transform_t458321487::get_offset_of_R3_15(),
	RC2Transform_t458321487::get_offset_of_K_16(),
	RC2Transform_t458321487::get_offset_of_j_17(),
	RC2Transform_t458321487_StaticFields::get_offset_of_pitable_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize877 = { sizeof (Rijndael_t2986313634), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize878 = { sizeof (RijndaelManaged_t3586970409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize879 = { sizeof (RijndaelTransform_t631222241), -1, sizeof(RijndaelTransform_t631222241_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable879[15] = 
{
	RijndaelTransform_t631222241::get_offset_of_expandedKey_12(),
	RijndaelTransform_t631222241::get_offset_of_Nb_13(),
	RijndaelTransform_t631222241::get_offset_of_Nk_14(),
	RijndaelTransform_t631222241::get_offset_of_Nr_15(),
	RijndaelTransform_t631222241_StaticFields::get_offset_of_Rcon_16(),
	RijndaelTransform_t631222241_StaticFields::get_offset_of_SBox_17(),
	RijndaelTransform_t631222241_StaticFields::get_offset_of_iSBox_18(),
	RijndaelTransform_t631222241_StaticFields::get_offset_of_T0_19(),
	RijndaelTransform_t631222241_StaticFields::get_offset_of_T1_20(),
	RijndaelTransform_t631222241_StaticFields::get_offset_of_T2_21(),
	RijndaelTransform_t631222241_StaticFields::get_offset_of_T3_22(),
	RijndaelTransform_t631222241_StaticFields::get_offset_of_iT0_23(),
	RijndaelTransform_t631222241_StaticFields::get_offset_of_iT1_24(),
	RijndaelTransform_t631222241_StaticFields::get_offset_of_iT2_25(),
	RijndaelTransform_t631222241_StaticFields::get_offset_of_iT3_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize880 = { sizeof (RijndaelManagedTransform_t4102601014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable880[2] = 
{
	RijndaelManagedTransform_t4102601014::get_offset_of__st_0(),
	RijndaelManagedTransform_t4102601014::get_offset_of__bs_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize881 = { sizeof (RIPEMD160_t268675434), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize882 = { sizeof (RIPEMD160Managed_t2491561941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable882[5] = 
{
	RIPEMD160Managed_t2491561941::get_offset_of__ProcessingBuffer_4(),
	RIPEMD160Managed_t2491561941::get_offset_of__X_5(),
	RIPEMD160Managed_t2491561941::get_offset_of__HashValue_6(),
	RIPEMD160Managed_t2491561941::get_offset_of__Length_7(),
	RIPEMD160Managed_t2491561941::get_offset_of__ProcessingBufferCount_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize883 = { sizeof (RNGCryptoServiceProvider_t3397414743), -1, sizeof(RNGCryptoServiceProvider_t3397414743_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable883[2] = 
{
	RNGCryptoServiceProvider_t3397414743_StaticFields::get_offset_of__lock_0(),
	RNGCryptoServiceProvider_t3397414743::get_offset_of__handle_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize884 = { sizeof (RSA_t2385438082), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize885 = { sizeof (RSACryptoServiceProvider_t2683512874), -1, sizeof(RSACryptoServiceProvider_t2683512874_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable885[8] = 
{
	RSACryptoServiceProvider_t2683512874::get_offset_of_store_2(),
	RSACryptoServiceProvider_t2683512874::get_offset_of_persistKey_3(),
	RSACryptoServiceProvider_t2683512874::get_offset_of_persisted_4(),
	RSACryptoServiceProvider_t2683512874::get_offset_of_privateKeyExportable_5(),
	RSACryptoServiceProvider_t2683512874::get_offset_of_m_disposed_6(),
	RSACryptoServiceProvider_t2683512874::get_offset_of_rsa_7(),
	RSACryptoServiceProvider_t2683512874_StaticFields::get_offset_of_useMachineKeyStore_8(),
	RSACryptoServiceProvider_t2683512874_StaticFields::get_offset_of_U3CU3Ef__switchU24map2D_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize886 = { sizeof (RSAOAEPKeyExchangeDeformatter_t3344463048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable886[1] = 
{
	RSAOAEPKeyExchangeDeformatter_t3344463048::get_offset_of_rsa_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize887 = { sizeof (RSAOAEPKeyExchangeFormatter_t2041696538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable887[2] = 
{
	RSAOAEPKeyExchangeFormatter_t2041696538::get_offset_of_rsa_0(),
	RSAOAEPKeyExchangeFormatter_t2041696538::get_offset_of_random_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize888 = { sizeof (RSAParameters_t1728406613)+ sizeof (Il2CppObject), sizeof(RSAParameters_t1728406613_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable888[8] = 
{
	RSAParameters_t1728406613::get_offset_of_P_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RSAParameters_t1728406613::get_offset_of_Q_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RSAParameters_t1728406613::get_offset_of_D_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RSAParameters_t1728406613::get_offset_of_DP_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RSAParameters_t1728406613::get_offset_of_DQ_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RSAParameters_t1728406613::get_offset_of_InverseQ_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RSAParameters_t1728406613::get_offset_of_Modulus_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RSAParameters_t1728406613::get_offset_of_Exponent_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize889 = { sizeof (RSAPKCS1KeyExchangeDeformatter_t2578812791), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable889[1] = 
{
	RSAPKCS1KeyExchangeDeformatter_t2578812791::get_offset_of_rsa_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize890 = { sizeof (RSAPKCS1KeyExchangeFormatter_t2761096101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable890[2] = 
{
	RSAPKCS1KeyExchangeFormatter_t2761096101::get_offset_of_rsa_0(),
	RSAPKCS1KeyExchangeFormatter_t2761096101::get_offset_of_random_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize891 = { sizeof (RSAPKCS1SignatureDeformatter_t3767223008), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable891[2] = 
{
	RSAPKCS1SignatureDeformatter_t3767223008::get_offset_of_rsa_0(),
	RSAPKCS1SignatureDeformatter_t3767223008::get_offset_of_hashName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize892 = { sizeof (RSAPKCS1SignatureFormatter_t1514197062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable892[2] = 
{
	RSAPKCS1SignatureFormatter_t1514197062::get_offset_of_rsa_0(),
	RSAPKCS1SignatureFormatter_t1514197062::get_offset_of_hash_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize893 = { sizeof (SHA1_t1803193667), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize894 = { sizeof (SHA1Internal_t3251293021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable894[5] = 
{
	SHA1Internal_t3251293021::get_offset_of__H_0(),
	SHA1Internal_t3251293021::get_offset_of_count_1(),
	SHA1Internal_t3251293021::get_offset_of__ProcessingBuffer_2(),
	SHA1Internal_t3251293021::get_offset_of__ProcessingBufferCount_3(),
	SHA1Internal_t3251293021::get_offset_of_buff_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize895 = { sizeof (SHA1CryptoServiceProvider_t3661059764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable895[1] = 
{
	SHA1CryptoServiceProvider_t3661059764::get_offset_of_sha_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize896 = { sizeof (SHA1Managed_t1754513891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable896[1] = 
{
	SHA1Managed_t1754513891::get_offset_of_sha_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize897 = { sizeof (SHA256_t3672283617), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize898 = { sizeof (SHA256Managed_t955042874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable898[5] = 
{
	SHA256Managed_t955042874::get_offset_of__H_4(),
	SHA256Managed_t955042874::get_offset_of_count_5(),
	SHA256Managed_t955042874::get_offset_of__ProcessingBuffer_6(),
	SHA256Managed_t955042874::get_offset_of__ProcessingBufferCount_7(),
	SHA256Managed_t955042874::get_offset_of_buff_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize899 = { sizeof (SHA384_t540967702), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
