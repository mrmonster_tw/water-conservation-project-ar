﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Xml.XPath.XPathNavigator
struct XPathNavigator_t787956054;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.MessageBuffer
struct  MessageBuffer_t778472114  : public Il2CppObject
{
public:
	// System.Xml.XPath.XPathNavigator System.ServiceModel.Channels.MessageBuffer::nav_cache
	XPathNavigator_t787956054 * ___nav_cache_0;

public:
	inline static int32_t get_offset_of_nav_cache_0() { return static_cast<int32_t>(offsetof(MessageBuffer_t778472114, ___nav_cache_0)); }
	inline XPathNavigator_t787956054 * get_nav_cache_0() const { return ___nav_cache_0; }
	inline XPathNavigator_t787956054 ** get_address_of_nav_cache_0() { return &___nav_cache_0; }
	inline void set_nav_cache_0(XPathNavigator_t787956054 * value)
	{
		___nav_cache_0 = value;
		Il2CppCodeGenWriteBarrier(&___nav_cache_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
