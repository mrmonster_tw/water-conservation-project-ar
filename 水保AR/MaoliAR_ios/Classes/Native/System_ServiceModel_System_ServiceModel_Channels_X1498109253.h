﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_B3441673553.h"

// System.Xml.XmlDictionaryReader
struct XmlDictionaryReader_t1044334689;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.XmlReaderBodyWriter
struct  XmlReaderBodyWriter_t1498109253  : public BodyWriter_t3441673553
{
public:
	// System.Xml.XmlDictionaryReader System.ServiceModel.Channels.XmlReaderBodyWriter::reader
	XmlDictionaryReader_t1044334689 * ___reader_1;
	// System.String System.ServiceModel.Channels.XmlReaderBodyWriter::xml
	String_t* ___xml_2;

public:
	inline static int32_t get_offset_of_reader_1() { return static_cast<int32_t>(offsetof(XmlReaderBodyWriter_t1498109253, ___reader_1)); }
	inline XmlDictionaryReader_t1044334689 * get_reader_1() const { return ___reader_1; }
	inline XmlDictionaryReader_t1044334689 ** get_address_of_reader_1() { return &___reader_1; }
	inline void set_reader_1(XmlDictionaryReader_t1044334689 * value)
	{
		___reader_1 = value;
		Il2CppCodeGenWriteBarrier(&___reader_1, value);
	}

	inline static int32_t get_offset_of_xml_2() { return static_cast<int32_t>(offsetof(XmlReaderBodyWriter_t1498109253, ___xml_2)); }
	inline String_t* get_xml_2() const { return ___xml_2; }
	inline String_t** get_address_of_xml_2() { return &___xml_2; }
	inline void set_xml_2(String_t* value)
	{
		___xml_2 = value;
		Il2CppCodeGenWriteBarrier(&___xml_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
