﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeStatement371410868.h"

// System.CodeDom.CodeExpression
struct CodeExpression_t2166265795;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeAssignStatement
struct  CodeAssignStatement_t1561974704  : public CodeStatement_t371410868
{
public:
	// System.CodeDom.CodeExpression System.CodeDom.CodeAssignStatement::left
	CodeExpression_t2166265795 * ___left_4;
	// System.CodeDom.CodeExpression System.CodeDom.CodeAssignStatement::right
	CodeExpression_t2166265795 * ___right_5;

public:
	inline static int32_t get_offset_of_left_4() { return static_cast<int32_t>(offsetof(CodeAssignStatement_t1561974704, ___left_4)); }
	inline CodeExpression_t2166265795 * get_left_4() const { return ___left_4; }
	inline CodeExpression_t2166265795 ** get_address_of_left_4() { return &___left_4; }
	inline void set_left_4(CodeExpression_t2166265795 * value)
	{
		___left_4 = value;
		Il2CppCodeGenWriteBarrier(&___left_4, value);
	}

	inline static int32_t get_offset_of_right_5() { return static_cast<int32_t>(offsetof(CodeAssignStatement_t1561974704, ___right_5)); }
	inline CodeExpression_t2166265795 * get_right_5() const { return ___right_5; }
	inline CodeExpression_t2166265795 ** get_address_of_right_5() { return &___right_5; }
	inline void set_right_5(CodeExpression_t2166265795 * value)
	{
		___right_5 = value;
		Il2CppCodeGenWriteBarrier(&___right_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
