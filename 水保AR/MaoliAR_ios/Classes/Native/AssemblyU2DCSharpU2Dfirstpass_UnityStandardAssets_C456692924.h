﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3193728992.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2342083791.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3807027201.h"

// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.Material
struct Material_t340375123;
// UnityStandardAssets.CinematicEffects.RenderTextureUtility
struct RenderTextureUtility_t1946006342;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.LensAberrations
struct  LensAberrations_t456692924  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.CinematicEffects.LensAberrations/DistortionSettings UnityStandardAssets.CinematicEffects.LensAberrations::distortion
	DistortionSettings_t3193728992  ___distortion_2;
	// UnityStandardAssets.CinematicEffects.LensAberrations/VignetteSettings UnityStandardAssets.CinematicEffects.LensAberrations::vignette
	VignetteSettings_t2342083791  ___vignette_3;
	// UnityStandardAssets.CinematicEffects.LensAberrations/ChromaticAberrationSettings UnityStandardAssets.CinematicEffects.LensAberrations::chromaticAberration
	ChromaticAberrationSettings_t3807027201  ___chromaticAberration_4;
	// UnityEngine.Shader UnityStandardAssets.CinematicEffects.LensAberrations::m_Shader
	Shader_t4151988712 * ___m_Shader_5;
	// UnityEngine.Material UnityStandardAssets.CinematicEffects.LensAberrations::m_Material
	Material_t340375123 * ___m_Material_6;
	// UnityStandardAssets.CinematicEffects.RenderTextureUtility UnityStandardAssets.CinematicEffects.LensAberrations::m_RTU
	RenderTextureUtility_t1946006342 * ___m_RTU_7;

public:
	inline static int32_t get_offset_of_distortion_2() { return static_cast<int32_t>(offsetof(LensAberrations_t456692924, ___distortion_2)); }
	inline DistortionSettings_t3193728992  get_distortion_2() const { return ___distortion_2; }
	inline DistortionSettings_t3193728992 * get_address_of_distortion_2() { return &___distortion_2; }
	inline void set_distortion_2(DistortionSettings_t3193728992  value)
	{
		___distortion_2 = value;
	}

	inline static int32_t get_offset_of_vignette_3() { return static_cast<int32_t>(offsetof(LensAberrations_t456692924, ___vignette_3)); }
	inline VignetteSettings_t2342083791  get_vignette_3() const { return ___vignette_3; }
	inline VignetteSettings_t2342083791 * get_address_of_vignette_3() { return &___vignette_3; }
	inline void set_vignette_3(VignetteSettings_t2342083791  value)
	{
		___vignette_3 = value;
	}

	inline static int32_t get_offset_of_chromaticAberration_4() { return static_cast<int32_t>(offsetof(LensAberrations_t456692924, ___chromaticAberration_4)); }
	inline ChromaticAberrationSettings_t3807027201  get_chromaticAberration_4() const { return ___chromaticAberration_4; }
	inline ChromaticAberrationSettings_t3807027201 * get_address_of_chromaticAberration_4() { return &___chromaticAberration_4; }
	inline void set_chromaticAberration_4(ChromaticAberrationSettings_t3807027201  value)
	{
		___chromaticAberration_4 = value;
	}

	inline static int32_t get_offset_of_m_Shader_5() { return static_cast<int32_t>(offsetof(LensAberrations_t456692924, ___m_Shader_5)); }
	inline Shader_t4151988712 * get_m_Shader_5() const { return ___m_Shader_5; }
	inline Shader_t4151988712 ** get_address_of_m_Shader_5() { return &___m_Shader_5; }
	inline void set_m_Shader_5(Shader_t4151988712 * value)
	{
		___m_Shader_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_Shader_5, value);
	}

	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(LensAberrations_t456692924, ___m_Material_6)); }
	inline Material_t340375123 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t340375123 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t340375123 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_Material_6, value);
	}

	inline static int32_t get_offset_of_m_RTU_7() { return static_cast<int32_t>(offsetof(LensAberrations_t456692924, ___m_RTU_7)); }
	inline RenderTextureUtility_t1946006342 * get_m_RTU_7() const { return ___m_RTU_7; }
	inline RenderTextureUtility_t1946006342 ** get_address_of_m_RTU_7() { return &___m_RTU_7; }
	inline void set_m_RTU_7(RenderTextureUtility_t1946006342 * value)
	{
		___m_RTU_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_RTU_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
