﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Hashtable
struct Hashtable_t1853889766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Xsl.XsltArgumentList
struct  XsltArgumentList_t741251601  : public Il2CppObject
{
public:
	// System.Collections.Hashtable System.Xml.Xsl.XsltArgumentList::extensionObjects
	Hashtable_t1853889766 * ___extensionObjects_0;
	// System.Collections.Hashtable System.Xml.Xsl.XsltArgumentList::parameters
	Hashtable_t1853889766 * ___parameters_1;

public:
	inline static int32_t get_offset_of_extensionObjects_0() { return static_cast<int32_t>(offsetof(XsltArgumentList_t741251601, ___extensionObjects_0)); }
	inline Hashtable_t1853889766 * get_extensionObjects_0() const { return ___extensionObjects_0; }
	inline Hashtable_t1853889766 ** get_address_of_extensionObjects_0() { return &___extensionObjects_0; }
	inline void set_extensionObjects_0(Hashtable_t1853889766 * value)
	{
		___extensionObjects_0 = value;
		Il2CppCodeGenWriteBarrier(&___extensionObjects_0, value);
	}

	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(XsltArgumentList_t741251601, ___parameters_1)); }
	inline Hashtable_t1853889766 * get_parameters_1() const { return ___parameters_1; }
	inline Hashtable_t1853889766 ** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(Hashtable_t1853889766 * value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
