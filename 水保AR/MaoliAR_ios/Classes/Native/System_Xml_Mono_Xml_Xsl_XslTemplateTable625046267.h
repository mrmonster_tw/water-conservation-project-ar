﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Hashtable
struct Hashtable_t1853889766;
// Mono.Xml.Xsl.XslStylesheet
struct XslStylesheet_t113441946;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.XslTemplateTable
struct  XslTemplateTable_t625046267  : public Il2CppObject
{
public:
	// System.Collections.Hashtable Mono.Xml.Xsl.XslTemplateTable::templateTables
	Hashtable_t1853889766 * ___templateTables_0;
	// System.Collections.Hashtable Mono.Xml.Xsl.XslTemplateTable::namedTemplates
	Hashtable_t1853889766 * ___namedTemplates_1;
	// Mono.Xml.Xsl.XslStylesheet Mono.Xml.Xsl.XslTemplateTable::parent
	XslStylesheet_t113441946 * ___parent_2;

public:
	inline static int32_t get_offset_of_templateTables_0() { return static_cast<int32_t>(offsetof(XslTemplateTable_t625046267, ___templateTables_0)); }
	inline Hashtable_t1853889766 * get_templateTables_0() const { return ___templateTables_0; }
	inline Hashtable_t1853889766 ** get_address_of_templateTables_0() { return &___templateTables_0; }
	inline void set_templateTables_0(Hashtable_t1853889766 * value)
	{
		___templateTables_0 = value;
		Il2CppCodeGenWriteBarrier(&___templateTables_0, value);
	}

	inline static int32_t get_offset_of_namedTemplates_1() { return static_cast<int32_t>(offsetof(XslTemplateTable_t625046267, ___namedTemplates_1)); }
	inline Hashtable_t1853889766 * get_namedTemplates_1() const { return ___namedTemplates_1; }
	inline Hashtable_t1853889766 ** get_address_of_namedTemplates_1() { return &___namedTemplates_1; }
	inline void set_namedTemplates_1(Hashtable_t1853889766 * value)
	{
		___namedTemplates_1 = value;
		Il2CppCodeGenWriteBarrier(&___namedTemplates_1, value);
	}

	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(XslTemplateTable_t625046267, ___parent_2)); }
	inline XslStylesheet_t113441946 * get_parent_2() const { return ___parent_2; }
	inline XslStylesheet_t113441946 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(XslStylesheet_t113441946 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
