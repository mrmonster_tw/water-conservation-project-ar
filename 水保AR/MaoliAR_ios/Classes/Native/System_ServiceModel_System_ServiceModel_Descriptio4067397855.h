﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.ObjectModel.Collection`1<System.Xml.XmlAttribute>
struct Collection_1_t118208177;
// System.ServiceModel.Description.MetadataSectionSerializer
struct MetadataSectionSerializer_t3352489376;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.MetadataSection
struct  MetadataSection_t4067397855  : public Il2CppObject
{
public:
	// System.String System.ServiceModel.Description.MetadataSection::dialect
	String_t* ___dialect_0;
	// System.String System.ServiceModel.Description.MetadataSection::identifier
	String_t* ___identifier_1;
	// System.Object System.ServiceModel.Description.MetadataSection::metadata
	Il2CppObject * ___metadata_2;
	// System.Collections.ObjectModel.Collection`1<System.Xml.XmlAttribute> System.ServiceModel.Description.MetadataSection::attributes
	Collection_1_t118208177 * ___attributes_3;

public:
	inline static int32_t get_offset_of_dialect_0() { return static_cast<int32_t>(offsetof(MetadataSection_t4067397855, ___dialect_0)); }
	inline String_t* get_dialect_0() const { return ___dialect_0; }
	inline String_t** get_address_of_dialect_0() { return &___dialect_0; }
	inline void set_dialect_0(String_t* value)
	{
		___dialect_0 = value;
		Il2CppCodeGenWriteBarrier(&___dialect_0, value);
	}

	inline static int32_t get_offset_of_identifier_1() { return static_cast<int32_t>(offsetof(MetadataSection_t4067397855, ___identifier_1)); }
	inline String_t* get_identifier_1() const { return ___identifier_1; }
	inline String_t** get_address_of_identifier_1() { return &___identifier_1; }
	inline void set_identifier_1(String_t* value)
	{
		___identifier_1 = value;
		Il2CppCodeGenWriteBarrier(&___identifier_1, value);
	}

	inline static int32_t get_offset_of_metadata_2() { return static_cast<int32_t>(offsetof(MetadataSection_t4067397855, ___metadata_2)); }
	inline Il2CppObject * get_metadata_2() const { return ___metadata_2; }
	inline Il2CppObject ** get_address_of_metadata_2() { return &___metadata_2; }
	inline void set_metadata_2(Il2CppObject * value)
	{
		___metadata_2 = value;
		Il2CppCodeGenWriteBarrier(&___metadata_2, value);
	}

	inline static int32_t get_offset_of_attributes_3() { return static_cast<int32_t>(offsetof(MetadataSection_t4067397855, ___attributes_3)); }
	inline Collection_1_t118208177 * get_attributes_3() const { return ___attributes_3; }
	inline Collection_1_t118208177 ** get_address_of_attributes_3() { return &___attributes_3; }
	inline void set_attributes_3(Collection_1_t118208177 * value)
	{
		___attributes_3 = value;
		Il2CppCodeGenWriteBarrier(&___attributes_3, value);
	}
};

struct MetadataSection_t4067397855_StaticFields
{
public:
	// System.ServiceModel.Description.MetadataSectionSerializer System.ServiceModel.Description.MetadataSection::serializer
	MetadataSectionSerializer_t3352489376 * ___serializer_4;

public:
	inline static int32_t get_offset_of_serializer_4() { return static_cast<int32_t>(offsetof(MetadataSection_t4067397855_StaticFields, ___serializer_4)); }
	inline MetadataSectionSerializer_t3352489376 * get_serializer_4() const { return ___serializer_4; }
	inline MetadataSectionSerializer_t3352489376 ** get_address_of_serializer_4() { return &___serializer_4; }
	inline void set_serializer_4(MetadataSectionSerializer_t3352489376 * value)
	{
		___serializer_4 = value;
		Il2CppCodeGenWriteBarrier(&___serializer_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
