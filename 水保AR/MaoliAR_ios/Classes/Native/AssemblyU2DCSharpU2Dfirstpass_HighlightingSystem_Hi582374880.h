﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"
#include "UnityEngine_UnityEngine_Matrix4x41817901843.h"
#include "UnityEngine_UnityEngine_Rendering_CameraEvent2033959522.h"
#include "UnityEngine_UnityEngine_Rendering_RenderTargetIden2079184500.h"

// System.String
struct String_t;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t2206337031;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// UnityEngine.Camera
struct Camera_t4157153871;
// System.String[]
struct StringU5BU5D_t1281789340;
// UnityEngine.Shader[]
struct ShaderU5BU5D_t2047402361;
// UnityEngine.Material[]
struct MaterialU5BU5D_t561872642;
// UnityEngine.Material
struct Material_t340375123;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HighlightingSystem.HighlightingBase
struct  HighlightingBase_t582374880  : public MonoBehaviour_t3962482529
{
public:
	// System.Single HighlightingSystem.HighlightingBase::offsetFactor
	float ___offsetFactor_12;
	// System.Single HighlightingSystem.HighlightingBase::offsetUnits
	float ___offsetUnits_13;
	// UnityEngine.Rendering.CommandBuffer HighlightingSystem.HighlightingBase::renderBuffer
	CommandBuffer_t2206337031 * ___renderBuffer_14;
	// System.Boolean HighlightingSystem.HighlightingBase::isDirty
	bool ___isDirty_15;
	// System.Int32 HighlightingSystem.HighlightingBase::cachedWidth
	int32_t ___cachedWidth_16;
	// System.Int32 HighlightingSystem.HighlightingBase::cachedHeight
	int32_t ___cachedHeight_17;
	// System.Int32 HighlightingSystem.HighlightingBase::cachedAA
	int32_t ___cachedAA_18;
	// System.Int32 HighlightingSystem.HighlightingBase::_downsampleFactor
	int32_t ____downsampleFactor_19;
	// System.Int32 HighlightingSystem.HighlightingBase::_iterations
	int32_t ____iterations_20;
	// System.Single HighlightingSystem.HighlightingBase::_blurMinSpread
	float ____blurMinSpread_21;
	// System.Single HighlightingSystem.HighlightingBase::_blurSpread
	float ____blurSpread_22;
	// System.Single HighlightingSystem.HighlightingBase::_blurIntensity
	float ____blurIntensity_23;
	// UnityEngine.Rendering.RenderTargetIdentifier HighlightingSystem.HighlightingBase::highlightingBufferID
	RenderTargetIdentifier_t2079184500  ___highlightingBufferID_24;
	// UnityEngine.RenderTexture HighlightingSystem.HighlightingBase::highlightingBuffer
	RenderTexture_t2108887433 * ___highlightingBuffer_25;
	// UnityEngine.Camera HighlightingSystem.HighlightingBase::cam
	Camera_t4157153871 * ___cam_26;
	// System.Boolean HighlightingSystem.HighlightingBase::isSupported
	bool ___isSupported_27;
	// System.Boolean HighlightingSystem.HighlightingBase::isDepthAvailable
	bool ___isDepthAvailable_28;
	// UnityEngine.Material HighlightingSystem.HighlightingBase::blurMaterial
	Material_t340375123 * ___blurMaterial_37;

public:
	inline static int32_t get_offset_of_offsetFactor_12() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ___offsetFactor_12)); }
	inline float get_offsetFactor_12() const { return ___offsetFactor_12; }
	inline float* get_address_of_offsetFactor_12() { return &___offsetFactor_12; }
	inline void set_offsetFactor_12(float value)
	{
		___offsetFactor_12 = value;
	}

	inline static int32_t get_offset_of_offsetUnits_13() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ___offsetUnits_13)); }
	inline float get_offsetUnits_13() const { return ___offsetUnits_13; }
	inline float* get_address_of_offsetUnits_13() { return &___offsetUnits_13; }
	inline void set_offsetUnits_13(float value)
	{
		___offsetUnits_13 = value;
	}

	inline static int32_t get_offset_of_renderBuffer_14() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ___renderBuffer_14)); }
	inline CommandBuffer_t2206337031 * get_renderBuffer_14() const { return ___renderBuffer_14; }
	inline CommandBuffer_t2206337031 ** get_address_of_renderBuffer_14() { return &___renderBuffer_14; }
	inline void set_renderBuffer_14(CommandBuffer_t2206337031 * value)
	{
		___renderBuffer_14 = value;
		Il2CppCodeGenWriteBarrier(&___renderBuffer_14, value);
	}

	inline static int32_t get_offset_of_isDirty_15() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ___isDirty_15)); }
	inline bool get_isDirty_15() const { return ___isDirty_15; }
	inline bool* get_address_of_isDirty_15() { return &___isDirty_15; }
	inline void set_isDirty_15(bool value)
	{
		___isDirty_15 = value;
	}

	inline static int32_t get_offset_of_cachedWidth_16() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ___cachedWidth_16)); }
	inline int32_t get_cachedWidth_16() const { return ___cachedWidth_16; }
	inline int32_t* get_address_of_cachedWidth_16() { return &___cachedWidth_16; }
	inline void set_cachedWidth_16(int32_t value)
	{
		___cachedWidth_16 = value;
	}

	inline static int32_t get_offset_of_cachedHeight_17() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ___cachedHeight_17)); }
	inline int32_t get_cachedHeight_17() const { return ___cachedHeight_17; }
	inline int32_t* get_address_of_cachedHeight_17() { return &___cachedHeight_17; }
	inline void set_cachedHeight_17(int32_t value)
	{
		___cachedHeight_17 = value;
	}

	inline static int32_t get_offset_of_cachedAA_18() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ___cachedAA_18)); }
	inline int32_t get_cachedAA_18() const { return ___cachedAA_18; }
	inline int32_t* get_address_of_cachedAA_18() { return &___cachedAA_18; }
	inline void set_cachedAA_18(int32_t value)
	{
		___cachedAA_18 = value;
	}

	inline static int32_t get_offset_of__downsampleFactor_19() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ____downsampleFactor_19)); }
	inline int32_t get__downsampleFactor_19() const { return ____downsampleFactor_19; }
	inline int32_t* get_address_of__downsampleFactor_19() { return &____downsampleFactor_19; }
	inline void set__downsampleFactor_19(int32_t value)
	{
		____downsampleFactor_19 = value;
	}

	inline static int32_t get_offset_of__iterations_20() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ____iterations_20)); }
	inline int32_t get__iterations_20() const { return ____iterations_20; }
	inline int32_t* get_address_of__iterations_20() { return &____iterations_20; }
	inline void set__iterations_20(int32_t value)
	{
		____iterations_20 = value;
	}

	inline static int32_t get_offset_of__blurMinSpread_21() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ____blurMinSpread_21)); }
	inline float get__blurMinSpread_21() const { return ____blurMinSpread_21; }
	inline float* get_address_of__blurMinSpread_21() { return &____blurMinSpread_21; }
	inline void set__blurMinSpread_21(float value)
	{
		____blurMinSpread_21 = value;
	}

	inline static int32_t get_offset_of__blurSpread_22() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ____blurSpread_22)); }
	inline float get__blurSpread_22() const { return ____blurSpread_22; }
	inline float* get_address_of__blurSpread_22() { return &____blurSpread_22; }
	inline void set__blurSpread_22(float value)
	{
		____blurSpread_22 = value;
	}

	inline static int32_t get_offset_of__blurIntensity_23() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ____blurIntensity_23)); }
	inline float get__blurIntensity_23() const { return ____blurIntensity_23; }
	inline float* get_address_of__blurIntensity_23() { return &____blurIntensity_23; }
	inline void set__blurIntensity_23(float value)
	{
		____blurIntensity_23 = value;
	}

	inline static int32_t get_offset_of_highlightingBufferID_24() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ___highlightingBufferID_24)); }
	inline RenderTargetIdentifier_t2079184500  get_highlightingBufferID_24() const { return ___highlightingBufferID_24; }
	inline RenderTargetIdentifier_t2079184500 * get_address_of_highlightingBufferID_24() { return &___highlightingBufferID_24; }
	inline void set_highlightingBufferID_24(RenderTargetIdentifier_t2079184500  value)
	{
		___highlightingBufferID_24 = value;
	}

	inline static int32_t get_offset_of_highlightingBuffer_25() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ___highlightingBuffer_25)); }
	inline RenderTexture_t2108887433 * get_highlightingBuffer_25() const { return ___highlightingBuffer_25; }
	inline RenderTexture_t2108887433 ** get_address_of_highlightingBuffer_25() { return &___highlightingBuffer_25; }
	inline void set_highlightingBuffer_25(RenderTexture_t2108887433 * value)
	{
		___highlightingBuffer_25 = value;
		Il2CppCodeGenWriteBarrier(&___highlightingBuffer_25, value);
	}

	inline static int32_t get_offset_of_cam_26() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ___cam_26)); }
	inline Camera_t4157153871 * get_cam_26() const { return ___cam_26; }
	inline Camera_t4157153871 ** get_address_of_cam_26() { return &___cam_26; }
	inline void set_cam_26(Camera_t4157153871 * value)
	{
		___cam_26 = value;
		Il2CppCodeGenWriteBarrier(&___cam_26, value);
	}

	inline static int32_t get_offset_of_isSupported_27() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ___isSupported_27)); }
	inline bool get_isSupported_27() const { return ___isSupported_27; }
	inline bool* get_address_of_isSupported_27() { return &___isSupported_27; }
	inline void set_isSupported_27(bool value)
	{
		___isSupported_27 = value;
	}

	inline static int32_t get_offset_of_isDepthAvailable_28() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ___isDepthAvailable_28)); }
	inline bool get_isDepthAvailable_28() const { return ___isDepthAvailable_28; }
	inline bool* get_address_of_isDepthAvailable_28() { return &___isDepthAvailable_28; }
	inline void set_isDepthAvailable_28(bool value)
	{
		___isDepthAvailable_28 = value;
	}

	inline static int32_t get_offset_of_blurMaterial_37() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ___blurMaterial_37)); }
	inline Material_t340375123 * get_blurMaterial_37() const { return ___blurMaterial_37; }
	inline Material_t340375123 ** get_address_of_blurMaterial_37() { return &___blurMaterial_37; }
	inline void set_blurMaterial_37(Material_t340375123 * value)
	{
		___blurMaterial_37 = value;
		Il2CppCodeGenWriteBarrier(&___blurMaterial_37, value);
	}
};

struct HighlightingBase_t582374880_StaticFields
{
public:
	// UnityEngine.Color HighlightingSystem.HighlightingBase::colorClear
	Color_t2555686324  ___colorClear_2;
	// System.String HighlightingSystem.HighlightingBase::renderBufferName
	String_t* ___renderBufferName_3;
	// UnityEngine.Matrix4x4 HighlightingSystem.HighlightingBase::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_4;
	// UnityEngine.Rendering.RenderTargetIdentifier HighlightingSystem.HighlightingBase::cameraTargetID
	RenderTargetIdentifier_t2079184500  ___cameraTargetID_6;
	// UnityEngine.Mesh HighlightingSystem.HighlightingBase::quad
	Mesh_t3648964284 * ___quad_7;
	// System.Int32 HighlightingSystem.HighlightingBase::graphicsDeviceVersion
	int32_t ___graphicsDeviceVersion_11;
	// System.String[] HighlightingSystem.HighlightingBase::shaderPaths
	StringU5BU5D_t1281789340* ___shaderPaths_32;
	// UnityEngine.Shader[] HighlightingSystem.HighlightingBase::shaders
	ShaderU5BU5D_t2047402361* ___shaders_33;
	// UnityEngine.Material[] HighlightingSystem.HighlightingBase::materials
	MaterialU5BU5D_t561872642* ___materials_34;
	// UnityEngine.Material HighlightingSystem.HighlightingBase::cutMaterial
	Material_t340375123 * ___cutMaterial_35;
	// UnityEngine.Material HighlightingSystem.HighlightingBase::compMaterial
	Material_t340375123 * ___compMaterial_36;
	// System.Boolean HighlightingSystem.HighlightingBase::initialized
	bool ___initialized_38;

public:
	inline static int32_t get_offset_of_colorClear_2() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880_StaticFields, ___colorClear_2)); }
	inline Color_t2555686324  get_colorClear_2() const { return ___colorClear_2; }
	inline Color_t2555686324 * get_address_of_colorClear_2() { return &___colorClear_2; }
	inline void set_colorClear_2(Color_t2555686324  value)
	{
		___colorClear_2 = value;
	}

	inline static int32_t get_offset_of_renderBufferName_3() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880_StaticFields, ___renderBufferName_3)); }
	inline String_t* get_renderBufferName_3() const { return ___renderBufferName_3; }
	inline String_t** get_address_of_renderBufferName_3() { return &___renderBufferName_3; }
	inline void set_renderBufferName_3(String_t* value)
	{
		___renderBufferName_3 = value;
		Il2CppCodeGenWriteBarrier(&___renderBufferName_3, value);
	}

	inline static int32_t get_offset_of_identityMatrix_4() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880_StaticFields, ___identityMatrix_4)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_4() const { return ___identityMatrix_4; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_4() { return &___identityMatrix_4; }
	inline void set_identityMatrix_4(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_4 = value;
	}

	inline static int32_t get_offset_of_cameraTargetID_6() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880_StaticFields, ___cameraTargetID_6)); }
	inline RenderTargetIdentifier_t2079184500  get_cameraTargetID_6() const { return ___cameraTargetID_6; }
	inline RenderTargetIdentifier_t2079184500 * get_address_of_cameraTargetID_6() { return &___cameraTargetID_6; }
	inline void set_cameraTargetID_6(RenderTargetIdentifier_t2079184500  value)
	{
		___cameraTargetID_6 = value;
	}

	inline static int32_t get_offset_of_quad_7() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880_StaticFields, ___quad_7)); }
	inline Mesh_t3648964284 * get_quad_7() const { return ___quad_7; }
	inline Mesh_t3648964284 ** get_address_of_quad_7() { return &___quad_7; }
	inline void set_quad_7(Mesh_t3648964284 * value)
	{
		___quad_7 = value;
		Il2CppCodeGenWriteBarrier(&___quad_7, value);
	}

	inline static int32_t get_offset_of_graphicsDeviceVersion_11() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880_StaticFields, ___graphicsDeviceVersion_11)); }
	inline int32_t get_graphicsDeviceVersion_11() const { return ___graphicsDeviceVersion_11; }
	inline int32_t* get_address_of_graphicsDeviceVersion_11() { return &___graphicsDeviceVersion_11; }
	inline void set_graphicsDeviceVersion_11(int32_t value)
	{
		___graphicsDeviceVersion_11 = value;
	}

	inline static int32_t get_offset_of_shaderPaths_32() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880_StaticFields, ___shaderPaths_32)); }
	inline StringU5BU5D_t1281789340* get_shaderPaths_32() const { return ___shaderPaths_32; }
	inline StringU5BU5D_t1281789340** get_address_of_shaderPaths_32() { return &___shaderPaths_32; }
	inline void set_shaderPaths_32(StringU5BU5D_t1281789340* value)
	{
		___shaderPaths_32 = value;
		Il2CppCodeGenWriteBarrier(&___shaderPaths_32, value);
	}

	inline static int32_t get_offset_of_shaders_33() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880_StaticFields, ___shaders_33)); }
	inline ShaderU5BU5D_t2047402361* get_shaders_33() const { return ___shaders_33; }
	inline ShaderU5BU5D_t2047402361** get_address_of_shaders_33() { return &___shaders_33; }
	inline void set_shaders_33(ShaderU5BU5D_t2047402361* value)
	{
		___shaders_33 = value;
		Il2CppCodeGenWriteBarrier(&___shaders_33, value);
	}

	inline static int32_t get_offset_of_materials_34() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880_StaticFields, ___materials_34)); }
	inline MaterialU5BU5D_t561872642* get_materials_34() const { return ___materials_34; }
	inline MaterialU5BU5D_t561872642** get_address_of_materials_34() { return &___materials_34; }
	inline void set_materials_34(MaterialU5BU5D_t561872642* value)
	{
		___materials_34 = value;
		Il2CppCodeGenWriteBarrier(&___materials_34, value);
	}

	inline static int32_t get_offset_of_cutMaterial_35() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880_StaticFields, ___cutMaterial_35)); }
	inline Material_t340375123 * get_cutMaterial_35() const { return ___cutMaterial_35; }
	inline Material_t340375123 ** get_address_of_cutMaterial_35() { return &___cutMaterial_35; }
	inline void set_cutMaterial_35(Material_t340375123 * value)
	{
		___cutMaterial_35 = value;
		Il2CppCodeGenWriteBarrier(&___cutMaterial_35, value);
	}

	inline static int32_t get_offset_of_compMaterial_36() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880_StaticFields, ___compMaterial_36)); }
	inline Material_t340375123 * get_compMaterial_36() const { return ___compMaterial_36; }
	inline Material_t340375123 ** get_address_of_compMaterial_36() { return &___compMaterial_36; }
	inline void set_compMaterial_36(Material_t340375123 * value)
	{
		___compMaterial_36 = value;
		Il2CppCodeGenWriteBarrier(&___compMaterial_36, value);
	}

	inline static int32_t get_offset_of_initialized_38() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880_StaticFields, ___initialized_38)); }
	inline bool get_initialized_38() const { return ___initialized_38; }
	inline bool* get_address_of_initialized_38() { return &___initialized_38; }
	inline void set_initialized_38(bool value)
	{
		___initialized_38 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
