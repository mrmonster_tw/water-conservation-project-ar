﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_T4020609293.h"

// System.ServiceModel.Channels.MessageEncoder
struct MessageEncoder_t3063398011;
// System.ServiceModel.Description.ClientCredentials
struct ClientCredentials_t1418047401;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.HttpChannelFactory`1<System.Object>
struct  HttpChannelFactory_1_t115714936  : public TransportChannelFactoryBase_1_t4020609293
{
public:
	// System.ServiceModel.Channels.MessageEncoder System.ServiceModel.Channels.HttpChannelFactory`1::encoder
	MessageEncoder_t3063398011 * ___encoder_15;
	// System.ServiceModel.Description.ClientCredentials System.ServiceModel.Channels.HttpChannelFactory`1::<ClientCredentials>k__BackingField
	ClientCredentials_t1418047401 * ___U3CClientCredentialsU3Ek__BackingField_16;

public:
	inline static int32_t get_offset_of_encoder_15() { return static_cast<int32_t>(offsetof(HttpChannelFactory_1_t115714936, ___encoder_15)); }
	inline MessageEncoder_t3063398011 * get_encoder_15() const { return ___encoder_15; }
	inline MessageEncoder_t3063398011 ** get_address_of_encoder_15() { return &___encoder_15; }
	inline void set_encoder_15(MessageEncoder_t3063398011 * value)
	{
		___encoder_15 = value;
		Il2CppCodeGenWriteBarrier(&___encoder_15, value);
	}

	inline static int32_t get_offset_of_U3CClientCredentialsU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(HttpChannelFactory_1_t115714936, ___U3CClientCredentialsU3Ek__BackingField_16)); }
	inline ClientCredentials_t1418047401 * get_U3CClientCredentialsU3Ek__BackingField_16() const { return ___U3CClientCredentialsU3Ek__BackingField_16; }
	inline ClientCredentials_t1418047401 ** get_address_of_U3CClientCredentialsU3Ek__BackingField_16() { return &___U3CClientCredentialsU3Ek__BackingField_16; }
	inline void set_U3CClientCredentialsU3Ek__BackingField_16(ClientCredentials_t1418047401 * value)
	{
		___U3CClientCredentialsU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CClientCredentialsU3Ek__BackingField_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
