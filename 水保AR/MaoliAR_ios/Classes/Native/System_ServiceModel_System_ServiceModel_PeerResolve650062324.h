﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.PeerResolvers.UnregisterInfoDC
struct UnregisterInfoDC_t1004748920;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.PeerResolvers.UnregisterInfo
struct  UnregisterInfo_t650062324  : public Il2CppObject
{
public:
	// System.ServiceModel.PeerResolvers.UnregisterInfoDC System.ServiceModel.PeerResolvers.UnregisterInfo::body
	UnregisterInfoDC_t1004748920 * ___body_0;

public:
	inline static int32_t get_offset_of_body_0() { return static_cast<int32_t>(offsetof(UnregisterInfo_t650062324, ___body_0)); }
	inline UnregisterInfoDC_t1004748920 * get_body_0() const { return ___body_0; }
	inline UnregisterInfoDC_t1004748920 ** get_address_of_body_0() { return &___body_0; }
	inline void set_body_0(UnregisterInfoDC_t1004748920 * value)
	{
		___body_0 = value;
		Il2CppCodeGenWriteBarrier(&___body_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
