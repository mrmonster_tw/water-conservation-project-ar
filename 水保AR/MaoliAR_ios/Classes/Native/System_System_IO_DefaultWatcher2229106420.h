﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.IO.DefaultWatcher
struct DefaultWatcher_t2229106420;
// System.Threading.Thread
struct Thread_t2300836069;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.String[]
struct StringU5BU5D_t1281789340;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.DefaultWatcher
struct  DefaultWatcher_t2229106420  : public Il2CppObject
{
public:

public:
};

struct DefaultWatcher_t2229106420_StaticFields
{
public:
	// System.IO.DefaultWatcher System.IO.DefaultWatcher::instance
	DefaultWatcher_t2229106420 * ___instance_0;
	// System.Threading.Thread System.IO.DefaultWatcher::thread
	Thread_t2300836069 * ___thread_1;
	// System.Collections.Hashtable System.IO.DefaultWatcher::watches
	Hashtable_t1853889766 * ___watches_2;
	// System.String[] System.IO.DefaultWatcher::NoStringsArray
	StringU5BU5D_t1281789340* ___NoStringsArray_3;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(DefaultWatcher_t2229106420_StaticFields, ___instance_0)); }
	inline DefaultWatcher_t2229106420 * get_instance_0() const { return ___instance_0; }
	inline DefaultWatcher_t2229106420 ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(DefaultWatcher_t2229106420 * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier(&___instance_0, value);
	}

	inline static int32_t get_offset_of_thread_1() { return static_cast<int32_t>(offsetof(DefaultWatcher_t2229106420_StaticFields, ___thread_1)); }
	inline Thread_t2300836069 * get_thread_1() const { return ___thread_1; }
	inline Thread_t2300836069 ** get_address_of_thread_1() { return &___thread_1; }
	inline void set_thread_1(Thread_t2300836069 * value)
	{
		___thread_1 = value;
		Il2CppCodeGenWriteBarrier(&___thread_1, value);
	}

	inline static int32_t get_offset_of_watches_2() { return static_cast<int32_t>(offsetof(DefaultWatcher_t2229106420_StaticFields, ___watches_2)); }
	inline Hashtable_t1853889766 * get_watches_2() const { return ___watches_2; }
	inline Hashtable_t1853889766 ** get_address_of_watches_2() { return &___watches_2; }
	inline void set_watches_2(Hashtable_t1853889766 * value)
	{
		___watches_2 = value;
		Il2CppCodeGenWriteBarrier(&___watches_2, value);
	}

	inline static int32_t get_offset_of_NoStringsArray_3() { return static_cast<int32_t>(offsetof(DefaultWatcher_t2229106420_StaticFields, ___NoStringsArray_3)); }
	inline StringU5BU5D_t1281789340* get_NoStringsArray_3() const { return ___NoStringsArray_3; }
	inline StringU5BU5D_t1281789340** get_address_of_NoStringsArray_3() { return &___NoStringsArray_3; }
	inline void set_NoStringsArray_3(StringU5BU5D_t1281789340* value)
	{
		___NoStringsArray_3 = value;
		Il2CppCodeGenWriteBarrier(&___NoStringsArray_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
