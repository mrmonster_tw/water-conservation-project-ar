﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearDevice_Eyewe664929988.h"
#include "Vuforia_UnityExtensions_Vuforia_NullHoloLensApiAbs2968904009.h"
#include "Vuforia_UnityExtensions_Vuforia_DeviceTracker2315692373.h"
#include "Vuforia_UnityExtensions_Vuforia_DeviceTrackerARCon1095592542.h"
#include "Vuforia_UnityExtensions_Vuforia_DistortionRenderin2592134457.h"
#include "Vuforia_UnityExtensions_Vuforia_DistortionRenderin1858983148.h"
#include "Vuforia_UnityExtensions_Vuforia_DelegateHelper3231076514.h"
#include "Vuforia_UnityExtensions_Vuforia_PlayModeEyewearUse1641351337.h"
#include "Vuforia_UnityExtensions_Vuforia_PlayModeEyewearCal3548891370.h"
#include "Vuforia_UnityExtensions_Vuforia_PlayModeEyewearDev2403363459.h"
#include "Vuforia_UnityExtensions_Vuforia_DedicatedEyewearDe2070131990.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraConfiguratio1452827745.h"
#include "Vuforia_UnityExtensions_Vuforia_BaseCameraConfigur3118151474.h"
#include "Vuforia_UnityExtensions_Vuforia_BaseStereoViewerCa2611724717.h"
#include "Vuforia_UnityExtensions_Vuforia_StereoViewerCamera4050045721.h"
#include "Vuforia_UnityExtensions_Vuforia_HoloLensExtendedTr2009717195.h"
#include "Vuforia_UnityExtensions_Vuforia_HoloLensExtendedTr1612729179.h"
#include "Vuforia_UnityExtensions_Vuforia_HoloLensExtendedTr2181165958.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaExtendedTrac262318595.h"
#include "Vuforia_UnityExtensions_Vuforia_VuMarkManagerImpl1545617414.h"
#include "Vuforia_UnityExtensions_Vuforia_InstanceIdImpl2824054591.h"
#include "Vuforia_UnityExtensions_Vuforia_VuMarkTargetImpl1052843922.h"
#include "Vuforia_UnityExtensions_Vuforia_VuMarkTemplateImpl667343433.h"
#include "Vuforia_UnityExtensions_Vuforia_MixedRealityControll13217384.h"
#include "Vuforia_UnityExtensions_Vuforia_MixedRealityContro2694983447.h"
#include "Vuforia_UnityExtensions_Vuforia_RotationalDeviceTr2847210804.h"
#include "Vuforia_UnityExtensions_Vuforia_RotationalDeviceTr1953038946.h"
#include "Vuforia_UnityExtensions_Vuforia_CustomViewerParamet463762113.h"
#include "Vuforia_UnityExtensions_Vuforia_DeviceTrackingMana3849131975.h"
#include "Vuforia_UnityExtensions_Vuforia_FactorySetter2184571743.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearCalibration3765540215.h"
#include "Vuforia_UnityExtensions_Vuforia_BackgroundPlaneAbs4147679365.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearUserCalibra3089732268.h"
#include "Vuforia_UnityExtensions_Vuforia_RotationalPlayMode4048275232.h"
#include "Vuforia_UnityExtensions_Vuforia_RotationalDeviceTr1573407597.h"
#include "Vuforia_UnityExtensions_Vuforia_IOSCamRecoveringHe4035151671.h"
#include "Vuforia_UnityExtensions_Vuforia_MeshUtils922322213.h"
#include "Vuforia_UnityExtensions_Vuforia_ExternalStereoCame3614889463.h"
#include "Vuforia_UnityExtensions_Vuforia_NullHideExcessAreaC465635818.h"
#include "Vuforia_UnityExtensions_Vuforia_StencilHideExcessA2657238862.h"
#include "Vuforia_UnityExtensions_Vuforia_LegacyHideExcessAr1318580222.h"
#include "Vuforia_UnityExtensions_Vuforia_DedicatedEyewearCa2854098828.h"
#include "Vuforia_UnityExtensions_Vuforia_NullCameraConfigur2773452281.h"
#include "Vuforia_UnityExtensions_Vuforia_MonoCameraConfigura112386736.h"
#include "Vuforia_UnityExtensions_Vuforia_UnityCameraExtensi3394190193.h"
#include "Vuforia_UnityExtensions_Vuforia_View3879626884.h"
#include "Vuforia_UnityExtensions_Vuforia_ViewerParameters3396315024.h"
#include "Vuforia_UnityExtensions_Vuforia_ViewerParametersLi3991990123.h"
#include "Vuforia_UnityExtensions_Vuforia_ObjectTargetAbstra2569206187.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice960297568.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice_Camer2478715656.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice_Focus3379290461.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera637748435.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice_Video2066817255.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice_Camer1483002240.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice_Camer1354848506.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice_Int643875541155.h"
#include "Vuforia_UnityExtensions_Vuforia_CloudRecoAbstractB3388674407.h"
#include "Vuforia_UnityExtensions_Vuforia_ReconstructionImpl3350922794.h"
#include "Vuforia_UnityExtensions_Vuforia_HideExcessAreaAbst3369390328.h"
#include "Vuforia_UnityExtensions_Vuforia_HideExcessAreaAbstr259981078.h"
#include "Vuforia_UnityExtensions_Vuforia_ObjectTargetImpl3614635090.h"
#include "Vuforia_UnityExtensions_Vuforia_UnityPlayer3127875071.h"
#include "Vuforia_UnityExtensions_Vuforia_NullUnityPlayer2922069181.h"
#include "Vuforia_UnityExtensions_Vuforia_PlayModeUnityPlaye3763348594.h"
#include "Vuforia_UnityExtensions_Vuforia_ReconstructionFromT676137874.h"
#include "Vuforia_UnityExtensions_Vuforia_Device64880687.h"
#include "Vuforia_UnityExtensions_Vuforia_Device_Mode3830573374.h"
#include "Vuforia_UnityExtensions_Vuforia_ViewerButtonType3221680132.h"
#include "Vuforia_UnityExtensions_Vuforia_ViewerTrayAlignmen2810797062.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractBeh3510878193.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractCon2684447159.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractCon1909783692.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractCon2828783036.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractCon3815674388.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractCon1174662728.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractConf588160133.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractCon3112457179.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractCon1841344395.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractConf802847339.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaDeinitBehav1700985552.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRuntime1949122020.h"
#include "Vuforia_UnityExtensions_Vuforia_VuMarkAbstractBehav580651545.h"
#include "Vuforia_UnityExtensions_Vuforia_VuMarkManager2982459596.h"
#include "Vuforia_UnityExtensions_Vuforia_InstanceIdType420283664.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4200 = { sizeof (EyewearCalibrationReading_t664929988)+ sizeof (Il2CppObject), sizeof(EyewearCalibrationReading_t664929988_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4200[5] = 
{
	EyewearCalibrationReading_t664929988::get_offset_of_pose_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EyewearCalibrationReading_t664929988::get_offset_of_scale_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EyewearCalibrationReading_t664929988::get_offset_of_centerX_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EyewearCalibrationReading_t664929988::get_offset_of_centerY_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EyewearCalibrationReading_t664929988::get_offset_of_unused_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4201 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4202 = { sizeof (NullHoloLensApiAbstraction_t2968904009), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4203 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4204 = { sizeof (DeviceTracker_t2315692373), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4205 = { sizeof (DeviceTrackerARController_t1095592542), -1, sizeof(DeviceTrackerARController_t1095592542_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4205[13] = 
{
	DeviceTrackerARController_t1095592542_StaticFields::get_offset_of_DEFAULT_HEAD_PIVOT_1(),
	DeviceTrackerARController_t1095592542_StaticFields::get_offset_of_DEFAULT_HANDHELD_PIVOT_2(),
	DeviceTrackerARController_t1095592542::get_offset_of_mAutoInitTracker_3(),
	DeviceTrackerARController_t1095592542::get_offset_of_mAutoStartTracker_4(),
	DeviceTrackerARController_t1095592542::get_offset_of_mPosePrediction_5(),
	DeviceTrackerARController_t1095592542::get_offset_of_mModelCorrectionMode_6(),
	DeviceTrackerARController_t1095592542::get_offset_of_mModelTransformEnabled_7(),
	DeviceTrackerARController_t1095592542::get_offset_of_mModelTransform_8(),
	DeviceTrackerARController_t1095592542::get_offset_of_mTrackerStarted_9(),
	DeviceTrackerARController_t1095592542::get_offset_of_mTrackerWasActiveBeforePause_10(),
	DeviceTrackerARController_t1095592542::get_offset_of_mTrackerWasActiveBeforeDisabling_11(),
	DeviceTrackerARController_t1095592542_StaticFields::get_offset_of_mInstance_12(),
	DeviceTrackerARController_t1095592542_StaticFields::get_offset_of_mPadlock_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4206 = { sizeof (DistortionRenderingMode_t2592134457)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4206[4] = 
{
	DistortionRenderingMode_t2592134457::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4207 = { sizeof (DistortionRenderingBehaviour_t1858983148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4207[12] = 
{
	DistortionRenderingBehaviour_t1858983148::get_offset_of_mSingleTexture_2(),
	DistortionRenderingBehaviour_t1858983148::get_offset_of_mRenderLayer_3(),
	DistortionRenderingBehaviour_t1858983148::get_offset_of_mOriginalCullingMasks_4(),
	DistortionRenderingBehaviour_t1858983148::get_offset_of_mStereoCameras_5(),
	DistortionRenderingBehaviour_t1858983148::get_offset_of_mMeshes_6(),
	DistortionRenderingBehaviour_t1858983148::get_offset_of_mTextures_7(),
	DistortionRenderingBehaviour_t1858983148::get_offset_of_mStarted_8(),
	DistortionRenderingBehaviour_t1858983148::get_offset_of_mVideoBackgroundChanged_9(),
	DistortionRenderingBehaviour_t1858983148::get_offset_of_mOriginalLeftViewport_10(),
	DistortionRenderingBehaviour_t1858983148::get_offset_of_mOriginalRightViewport_11(),
	DistortionRenderingBehaviour_t1858983148::get_offset_of_mDualTextureLeftViewport_12(),
	DistortionRenderingBehaviour_t1858983148::get_offset_of_mDualTextureRightViewport_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4208 = { sizeof (DelegateHelper_t3231076514), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4209 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4210 = { sizeof (PlayModeEyewearUserCalibratorImpl_t1641351337), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4211 = { sizeof (PlayModeEyewearCalibrationProfileManagerImpl_t3548891370), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4212 = { sizeof (PlayModeEyewearDevice_t2403363459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4212[3] = 
{
	PlayModeEyewearDevice_t2403363459::get_offset_of_mProfileManager_1(),
	PlayModeEyewearDevice_t2403363459::get_offset_of_mCalibrator_2(),
	PlayModeEyewearDevice_t2403363459::get_offset_of_mDummyPredictiveTracking_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4213 = { sizeof (DedicatedEyewearDevice_t2070131990), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4213[2] = 
{
	DedicatedEyewearDevice_t2070131990::get_offset_of_mProfileManager_1(),
	DedicatedEyewearDevice_t2070131990::get_offset_of_mCalibrator_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4214 = { sizeof (CameraConfigurationUtility_t1452827745), -1, sizeof(CameraConfigurationUtility_t1452827745_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4214[6] = 
{
	CameraConfigurationUtility_t1452827745_StaticFields::get_offset_of_MIN_CENTER_0(),
	CameraConfigurationUtility_t1452827745_StaticFields::get_offset_of_MAX_CENTER_1(),
	CameraConfigurationUtility_t1452827745_StaticFields::get_offset_of_MAX_BOTTOM_2(),
	CameraConfigurationUtility_t1452827745_StaticFields::get_offset_of_MAX_TOP_3(),
	CameraConfigurationUtility_t1452827745_StaticFields::get_offset_of_MAX_LEFT_4(),
	CameraConfigurationUtility_t1452827745_StaticFields::get_offset_of_MAX_RIGHT_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4215 = { sizeof (BaseCameraConfiguration_t3118151474), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4215[10] = 
{
	BaseCameraConfiguration_t3118151474::get_offset_of_mCameraDeviceMode_0(),
	BaseCameraConfiguration_t3118151474::get_offset_of_mLastVideoBackGroundMirroredFromSDK_1(),
	BaseCameraConfiguration_t3118151474::get_offset_of_mOnVideoBackgroundConfigChanged_2(),
	BaseCameraConfiguration_t3118151474::get_offset_of_mVideoBackgroundBehaviours_3(),
	BaseCameraConfiguration_t3118151474::get_offset_of_mVideoBackgroundViewportRect_4(),
	BaseCameraConfiguration_t3118151474::get_offset_of_mRenderVideoBackground_5(),
	BaseCameraConfiguration_t3118151474::get_offset_of_mProjectionOrientation_6(),
	BaseCameraConfiguration_t3118151474::get_offset_of_mInitialReflection_7(),
	BaseCameraConfiguration_t3118151474::get_offset_of_mBackgroundPlaneBehaviour_8(),
	BaseCameraConfiguration_t3118151474::get_offset_of_mCameraParameterChanged_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4216 = { sizeof (BaseStereoViewerCameraConfiguration_t2611724717), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4216[5] = 
{
	BaseStereoViewerCameraConfiguration_t2611724717::get_offset_of_mPrimaryCamera_10(),
	BaseStereoViewerCameraConfiguration_t2611724717::get_offset_of_mSecondaryCamera_11(),
	BaseStereoViewerCameraConfiguration_t2611724717::get_offset_of_mSkewFrustum_12(),
	BaseStereoViewerCameraConfiguration_t2611724717::get_offset_of_mScreenWidth_13(),
	BaseStereoViewerCameraConfiguration_t2611724717::get_offset_of_mScreenHeight_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4217 = { sizeof (StereoViewerCameraConfiguration_t4050045721), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4217[7] = 
{
	0,
	StereoViewerCameraConfiguration_t4050045721::get_offset_of_mLastAppliedLeftNearClipPlane_16(),
	StereoViewerCameraConfiguration_t4050045721::get_offset_of_mLastAppliedLeftFarClipPlane_17(),
	StereoViewerCameraConfiguration_t4050045721::get_offset_of_mLastAppliedRightNearClipPlane_18(),
	StereoViewerCameraConfiguration_t4050045721::get_offset_of_mLastAppliedRightFarClipPlane_19(),
	StereoViewerCameraConfiguration_t4050045721::get_offset_of_mCameraOffset_20(),
	StereoViewerCameraConfiguration_t4050045721::get_offset_of_mIsDistorted_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4218 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4219 = { sizeof (HoloLensExtendedTrackingManager_t2009717195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4219[15] = 
{
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mNumFramesStablePose_0(),
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mMaxPoseRelDistance_1(),
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mMaxPoseAngleDiff_2(),
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mMaxCamPoseAbsDistance_3(),
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mMaxCamPoseAngleDiff_4(),
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mMinNumFramesPoseOff_5(),
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mMinPoseUpdateRelDistance_6(),
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mMinPoseUpdateAngleDiff_7(),
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mTrackableSizeInViewThreshold_8(),
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mMaxDistanceFromViewCenterForPoseUpdate_9(),
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mSetWorldAnchors_10(),
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mTrackingList_11(),
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mTrackablesExtendedTrackingEnabled_12(),
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mTrackablesCurrentlyExtendedTracked_13(),
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mExtendedTrackablesState_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4220 = { sizeof (PoseInfo_t1612729179)+ sizeof (Il2CppObject), sizeof(PoseInfo_t1612729179 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4220[3] = 
{
	PoseInfo_t1612729179::get_offset_of_Position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PoseInfo_t1612729179::get_offset_of_Rotation_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PoseInfo_t1612729179::get_offset_of_NumFramesPoseWasOff_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4221 = { sizeof (PoseAgeEntry_t2181165958)+ sizeof (Il2CppObject), sizeof(PoseAgeEntry_t2181165958 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4221[3] = 
{
	PoseAgeEntry_t2181165958::get_offset_of_Pose_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PoseAgeEntry_t2181165958::get_offset_of_CameraPose_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PoseAgeEntry_t2181165958::get_offset_of_Age_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4222 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4223 = { sizeof (VuforiaExtendedTrackingManager_t262318595), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4224 = { sizeof (VuMarkManagerImpl_t1545617414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4224[6] = 
{
	VuMarkManagerImpl_t1545617414::get_offset_of_mBehaviours_0(),
	VuMarkManagerImpl_t1545617414::get_offset_of_mActiveVuMarkTargets_1(),
	VuMarkManagerImpl_t1545617414::get_offset_of_mDestroyedBehaviours_2(),
	VuMarkManagerImpl_t1545617414::get_offset_of_mOnVuMarkDetected_3(),
	VuMarkManagerImpl_t1545617414::get_offset_of_mOnVuMarkLost_4(),
	VuMarkManagerImpl_t1545617414::get_offset_of_mOnVuMarkBehaviourDetected_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4225 = { sizeof (InstanceIdImpl_t2824054591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4225[5] = 
{
	InstanceIdImpl_t2824054591::get_offset_of_mDataType_0(),
	InstanceIdImpl_t2824054591::get_offset_of_mBuffer_1(),
	InstanceIdImpl_t2824054591::get_offset_of_mNumericValue_2(),
	InstanceIdImpl_t2824054591::get_offset_of_mDataLength_3(),
	InstanceIdImpl_t2824054591::get_offset_of_mCachedStringValue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4226 = { sizeof (VuMarkTargetImpl_t1052843922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4226[5] = 
{
	VuMarkTargetImpl_t1052843922::get_offset_of_mVuMarkTemplate_0(),
	VuMarkTargetImpl_t1052843922::get_offset_of_mInstanceId_1(),
	VuMarkTargetImpl_t1052843922::get_offset_of_mTargetId_2(),
	VuMarkTargetImpl_t1052843922::get_offset_of_mInstanceImage_3(),
	VuMarkTargetImpl_t1052843922::get_offset_of_mInstanceImageHeader_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4227 = { sizeof (VuMarkTemplateImpl_t667343433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4227[3] = 
{
	VuMarkTemplateImpl_t667343433::get_offset_of_mUserData_4(),
	VuMarkTemplateImpl_t667343433::get_offset_of_mOrigin_5(),
	VuMarkTemplateImpl_t667343433::get_offset_of_mTrackingFromRuntimeAppearance_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4228 = { sizeof (MixedRealityController_t13217384), -1, sizeof(MixedRealityController_t13217384_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4228[13] = 
{
	MixedRealityController_t13217384_StaticFields::get_offset_of_mInstance_0(),
	MixedRealityController_t13217384::get_offset_of_mVuforiaBehaviour_1(),
	MixedRealityController_t13217384::get_offset_of_mDigitalEyewearBehaviour_2(),
	MixedRealityController_t13217384::get_offset_of_mVideoBackgroundManager_3(),
	MixedRealityController_t13217384::get_offset_of_mViewerHasBeenSetExternally_4(),
	MixedRealityController_t13217384::get_offset_of_mViewerParameters_5(),
	MixedRealityController_t13217384::get_offset_of_mFrameWorkHasBeenSetExternally_6(),
	MixedRealityController_t13217384::get_offset_of_mStereoFramework_7(),
	MixedRealityController_t13217384::get_offset_of_mCentralAnchorPoint_8(),
	MixedRealityController_t13217384::get_offset_of_mLeftCameraOfExternalSDK_9(),
	MixedRealityController_t13217384::get_offset_of_mRightCameraOfExternalSDK_10(),
	MixedRealityController_t13217384::get_offset_of_mObjectTrackerStopped_11(),
	MixedRealityController_t13217384::get_offset_of_mAutoStopCameraIfNotRequired_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4229 = { sizeof (Mode_t2694983447)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4229[7] = 
{
	Mode_t2694983447::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4230 = { sizeof (RotationalDeviceTracker_t2847210804), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4231 = { sizeof (MODEL_CORRECTION_MODE_t1953038946)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4231[4] = 
{
	MODEL_CORRECTION_MODE_t1953038946::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4232 = { sizeof (CustomViewerParameters_t463762113), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4232[7] = 
{
	CustomViewerParameters_t463762113::get_offset_of_mVersion_1(),
	CustomViewerParameters_t463762113::get_offset_of_mName_2(),
	CustomViewerParameters_t463762113::get_offset_of_mManufacturer_3(),
	CustomViewerParameters_t463762113::get_offset_of_mButtonType_4(),
	CustomViewerParameters_t463762113::get_offset_of_mScreenToLensDistance_5(),
	CustomViewerParameters_t463762113::get_offset_of_mTrayAlignment_6(),
	CustomViewerParameters_t463762113::get_offset_of_mMagnet_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4233 = { sizeof (DeviceTrackingManager_t3849131975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4233[4] = 
{
	DeviceTrackingManager_t3849131975::get_offset_of_mDeviceTrackerPositonOffset_0(),
	DeviceTrackingManager_t3849131975::get_offset_of_mDeviceTrackerRotationOffset_1(),
	DeviceTrackingManager_t3849131975::get_offset_of_mBeforeDevicePoseUpdated_2(),
	DeviceTrackingManager_t3849131975::get_offset_of_mAfterDevicePoseUpdated_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4234 = { sizeof (FactorySetter_t2184571743), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4235 = { sizeof (EyewearCalibrationProfileManagerImpl_t3765540215), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4236 = { sizeof (BackgroundPlaneAbstractBehaviour_t4147679365), -1, sizeof(BackgroundPlaneAbstractBehaviour_t4147679365_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4236[13] = 
{
	BackgroundPlaneAbstractBehaviour_t4147679365::get_offset_of_mTextureInfo_2(),
	BackgroundPlaneAbstractBehaviour_t4147679365::get_offset_of_mViewWidth_3(),
	BackgroundPlaneAbstractBehaviour_t4147679365::get_offset_of_mViewHeight_4(),
	BackgroundPlaneAbstractBehaviour_t4147679365::get_offset_of_mNumFramesToUpdateVideoBg_5(),
	BackgroundPlaneAbstractBehaviour_t4147679365::get_offset_of_mCamera_6(),
	BackgroundPlaneAbstractBehaviour_t4147679365_StaticFields::get_offset_of_maxDisplacement_7(),
	BackgroundPlaneAbstractBehaviour_t4147679365::get_offset_of_defaultNumDivisions_8(),
	BackgroundPlaneAbstractBehaviour_t4147679365::get_offset_of_mMesh_9(),
	BackgroundPlaneAbstractBehaviour_t4147679365::get_offset_of_mStereoDepth_10(),
	BackgroundPlaneAbstractBehaviour_t4147679365::get_offset_of_mBackgroundOffset_11(),
	BackgroundPlaneAbstractBehaviour_t4147679365::get_offset_of_mVuforiaBehaviour_12(),
	BackgroundPlaneAbstractBehaviour_t4147679365::get_offset_of_mBackgroundPlacedCallback_13(),
	BackgroundPlaneAbstractBehaviour_t4147679365::get_offset_of_mNumDivisions_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4237 = { sizeof (EyewearUserCalibratorImpl_t3089732268), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4238 = { sizeof (RotationalPlayModeDeviceTrackerImpl_t4048275232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4238[3] = 
{
	RotationalPlayModeDeviceTrackerImpl_t4048275232::get_offset_of_mRotation_1(),
	RotationalPlayModeDeviceTrackerImpl_t4048275232::get_offset_of_mModelCorrectionTransform_2(),
	RotationalPlayModeDeviceTrackerImpl_t4048275232::get_offset_of_mModelCorrection_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4239 = { sizeof (RotationalDeviceTrackerImpl_t1573407597), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4240 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4241 = { sizeof (IOSCamRecoveringHelper_t4035151671), -1, sizeof(IOSCamRecoveringHelper_t4035151671_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4241[9] = 
{
	0,
	0,
	0,
	IOSCamRecoveringHelper_t4035151671_StaticFields::get_offset_of_sHasJustResumed_3(),
	IOSCamRecoveringHelper_t4035151671_StaticFields::get_offset_of_sCheckFailedFrameAfterResume_4(),
	IOSCamRecoveringHelper_t4035151671_StaticFields::get_offset_of_sCheckedFailedFrameCounter_5(),
	IOSCamRecoveringHelper_t4035151671_StaticFields::get_offset_of_sWaitToRecoverCameraRestart_6(),
	IOSCamRecoveringHelper_t4035151671_StaticFields::get_offset_of_sWaitedFrameRecoverCounter_7(),
	IOSCamRecoveringHelper_t4035151671_StaticFields::get_offset_of_sRecoveryAttemptCounter_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4242 = { sizeof (MeshUtils_t922322213), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4243 = { sizeof (ExternalStereoCameraConfiguration_t3614889463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4243[21] = 
{
	0,
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mLastAppliedLeftNearClipPlane_16(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mLastAppliedLeftFarClipPlane_17(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mLastAppliedRightNearClipPlane_18(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mLastAppliedRightFarClipPlane_19(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mLastAppliedLeftVerticalVirtualFoV_20(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mLastAppliedLeftHorizontalVirtualFoV_21(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mLastAppliedRightVerticalVirtualFoV_22(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mLastAppliedRightHorizontalVirtualFoV_23(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mLastAppliedLeftProjection_24(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mLastAppliedRightProjection_25(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mNewLeftNearClipPlane_26(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mNewLeftFarClipPlane_27(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mNewRightNearClipPlane_28(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mNewRightFarClipPlane_29(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mNewLeftVerticalVirtualFoV_30(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mNewLeftHorizontalVirtualFoV_31(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mNewRightVerticalVirtualFoV_32(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mNewRightHorizontalVirtualFoV_33(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mExternallySetLeftMatrix_34(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mExternallySetRightMatrix_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4244 = { sizeof (NullHideExcessAreaClipping_t465635818), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4245 = { sizeof (StencilHideExcessAreaClipping_t2657238862), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4245[13] = 
{
	StencilHideExcessAreaClipping_t2657238862::get_offset_of_mGameObject_0(),
	StencilHideExcessAreaClipping_t2657238862::get_offset_of_mMatteShader_1(),
	StencilHideExcessAreaClipping_t2657238862::get_offset_of_mClippingPlane_2(),
	StencilHideExcessAreaClipping_t2657238862::get_offset_of_mCamera_3(),
	StencilHideExcessAreaClipping_t2657238862::get_offset_of_mCameraNearPlane_4(),
	StencilHideExcessAreaClipping_t2657238862::get_offset_of_mCameraFarPlane_5(),
	StencilHideExcessAreaClipping_t2657238862::get_offset_of_mCameraPixelRect_6(),
	StencilHideExcessAreaClipping_t2657238862::get_offset_of_mCameraFieldOfView_7(),
	StencilHideExcessAreaClipping_t2657238862::get_offset_of_mVuforiaBehaviour_8(),
	StencilHideExcessAreaClipping_t2657238862::get_offset_of_mPlanesActivated_9(),
	StencilHideExcessAreaClipping_t2657238862::get_offset_of_mBgPlane_10(),
	StencilHideExcessAreaClipping_t2657238862::get_offset_of_mBgPlaneLocalPos_11(),
	StencilHideExcessAreaClipping_t2657238862::get_offset_of_mBgPlaneLocalScale_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4246 = { sizeof (LegacyHideExcessAreaClipping_t1318580222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4246[21] = 
{
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mGameObject_0(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mMatteShader_1(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mBgPlane_2(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mLeftPlane_3(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mRightPlane_4(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mTopPlane_5(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mBottomPlane_6(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mCamera_7(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mBgPlaneLocalPos_8(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mBgPlaneLocalScale_9(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mCameraNearPlane_10(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mCameraPixelRect_11(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mCameraFieldOFView_12(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mVuforiaBehaviour_13(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mHideBehaviours_14(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mDeactivatedHideBehaviours_15(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mPlanesActivated_16(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mLeftPlaneCachedScale_17(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mRightPlaneCachedScale_18(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mBottomPlaneCachedScale_19(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mTopPlaneCachedScale_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4247 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4248 = { sizeof (DedicatedEyewearCameraConfiguration_t2854098828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4248[13] = 
{
	0,
	DedicatedEyewearCameraConfiguration_t2854098828::get_offset_of_mPrimaryCamera_11(),
	DedicatedEyewearCameraConfiguration_t2854098828::get_offset_of_mSecondaryCamera_12(),
	DedicatedEyewearCameraConfiguration_t2854098828::get_offset_of_mScreenWidth_13(),
	DedicatedEyewearCameraConfiguration_t2854098828::get_offset_of_mScreenHeight_14(),
	DedicatedEyewearCameraConfiguration_t2854098828::get_offset_of_mNeedToCheckStereo_15(),
	DedicatedEyewearCameraConfiguration_t2854098828::get_offset_of_mLastAppliedNearClipPlane_16(),
	DedicatedEyewearCameraConfiguration_t2854098828::get_offset_of_mLastAppliedFarClipPlane_17(),
	DedicatedEyewearCameraConfiguration_t2854098828::get_offset_of_mLastAppliedVirtualFoV_18(),
	DedicatedEyewearCameraConfiguration_t2854098828::get_offset_of_mNewNearClipPlane_19(),
	DedicatedEyewearCameraConfiguration_t2854098828::get_offset_of_mNewFarClipPlane_20(),
	DedicatedEyewearCameraConfiguration_t2854098828::get_offset_of_mNewVirtualFoV_21(),
	DedicatedEyewearCameraConfiguration_t2854098828::get_offset_of_mEyewearDevice_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4249 = { sizeof (NullCameraConfiguration_t2773452281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4249[1] = 
{
	NullCameraConfiguration_t2773452281::get_offset_of_mProjectionOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4250 = { sizeof (MonoCameraConfiguration_t112386736), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4250[7] = 
{
	0,
	MonoCameraConfiguration_t112386736::get_offset_of_mPrimaryCamera_11(),
	MonoCameraConfiguration_t112386736::get_offset_of_mCameraViewPortWidth_12(),
	MonoCameraConfiguration_t112386736::get_offset_of_mCameraViewPortHeight_13(),
	MonoCameraConfiguration_t112386736::get_offset_of_mLastAppliedNearClipPlane_14(),
	MonoCameraConfiguration_t112386736::get_offset_of_mLastAppliedFarClipPlane_15(),
	MonoCameraConfiguration_t112386736::get_offset_of_mLastAppliedFoV_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4251 = { sizeof (UnityCameraExtensions_t3394190193), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4251[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4252 = { sizeof (View_t3879626884)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4252[6] = 
{
	View_t3879626884::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4253 = { sizeof (ViewerParameters_t3396315024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4253[1] = 
{
	ViewerParameters_t3396315024::get_offset_of_mNativeVP_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4254 = { sizeof (ViewerParametersList_t3991990123), -1, sizeof(ViewerParametersList_t3991990123_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4254[2] = 
{
	ViewerParametersList_t3991990123::get_offset_of_mNativeVPL_0(),
	ViewerParametersList_t3991990123_StaticFields::get_offset_of_mListForAuthoringTools_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4255 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4256 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4257 = { sizeof (ObjectTargetAbstractBehaviour_t2569206187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4257[12] = 
{
	ObjectTargetAbstractBehaviour_t2569206187::get_offset_of_mObjectTarget_20(),
	ObjectTargetAbstractBehaviour_t2569206187::get_offset_of_mAspectRatioXY_21(),
	ObjectTargetAbstractBehaviour_t2569206187::get_offset_of_mAspectRatioXZ_22(),
	ObjectTargetAbstractBehaviour_t2569206187::get_offset_of_mShowBoundingBox_23(),
	ObjectTargetAbstractBehaviour_t2569206187::get_offset_of_mBBoxMin_24(),
	ObjectTargetAbstractBehaviour_t2569206187::get_offset_of_mBBoxMax_25(),
	ObjectTargetAbstractBehaviour_t2569206187::get_offset_of_mPreviewImage_26(),
	ObjectTargetAbstractBehaviour_t2569206187::get_offset_of_mLength_27(),
	ObjectTargetAbstractBehaviour_t2569206187::get_offset_of_mWidth_28(),
	ObjectTargetAbstractBehaviour_t2569206187::get_offset_of_mHeight_29(),
	ObjectTargetAbstractBehaviour_t2569206187::get_offset_of_mLastTransformScale_30(),
	ObjectTargetAbstractBehaviour_t2569206187::get_offset_of_mLastSize_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4258 = { sizeof (CameraDevice_t960297568), -1, sizeof(CameraDevice_t960297568_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4258[1] = 
{
	CameraDevice_t960297568_StaticFields::get_offset_of_mInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4259 = { sizeof (CameraDeviceMode_t2478715656)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4259[4] = 
{
	CameraDeviceMode_t2478715656::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4260 = { sizeof (FocusMode_t3379290461)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4260[6] = 
{
	FocusMode_t3379290461::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4261 = { sizeof (CameraDirection_t637748435)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4261[4] = 
{
	CameraDirection_t637748435::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4262 = { sizeof (VideoModeData_t2066817255)+ sizeof (Il2CppObject), sizeof(VideoModeData_t2066817255 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4262[4] = 
{
	VideoModeData_t2066817255::get_offset_of_width_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoModeData_t2066817255::get_offset_of_height_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoModeData_t2066817255::get_offset_of_frameRate_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoModeData_t2066817255::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4263 = { sizeof (CameraField_t1483002240)+ sizeof (Il2CppObject), sizeof(CameraField_t1483002240_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4263[2] = 
{
	CameraField_t1483002240::get_offset_of_Type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CameraField_t1483002240::get_offset_of_Key_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4264 = { sizeof (DataType_t1354848506)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4264[7] = 
{
	DataType_t1354848506::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4265 = { sizeof (Int64Range_t3875541155)+ sizeof (Il2CppObject), sizeof(Int64Range_t3875541155 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4265[2] = 
{
	Int64Range_t3875541155::get_offset_of_fromInt_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Int64Range_t3875541155::get_offset_of_toInt_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4266 = { sizeof (CloudRecoAbstractBehaviour_t3388674407), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4266[9] = 
{
	CloudRecoAbstractBehaviour_t3388674407::get_offset_of_mObjectTracker_2(),
	CloudRecoAbstractBehaviour_t3388674407::get_offset_of_mCurrentlyInitializing_3(),
	CloudRecoAbstractBehaviour_t3388674407::get_offset_of_mInitSuccess_4(),
	CloudRecoAbstractBehaviour_t3388674407::get_offset_of_mCloudRecoStarted_5(),
	CloudRecoAbstractBehaviour_t3388674407::get_offset_of_mOnInitializedCalled_6(),
	CloudRecoAbstractBehaviour_t3388674407::get_offset_of_mHandlers_7(),
	CloudRecoAbstractBehaviour_t3388674407::get_offset_of_mTargetFinderStartedBeforeDisable_8(),
	CloudRecoAbstractBehaviour_t3388674407::get_offset_of_AccessKey_9(),
	CloudRecoAbstractBehaviour_t3388674407::get_offset_of_SecretKey_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4267 = { sizeof (ReconstructionImpl_t3350922794), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4267[5] = 
{
	ReconstructionImpl_t3350922794::get_offset_of_mNativeReconstructionPtr_0(),
	ReconstructionImpl_t3350922794::get_offset_of_mMaximumAreaIsSet_1(),
	ReconstructionImpl_t3350922794::get_offset_of_mMaximumArea_2(),
	ReconstructionImpl_t3350922794::get_offset_of_mNavMeshPadding_3(),
	ReconstructionImpl_t3350922794::get_offset_of_mNavMeshUpdatesEnabled_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4268 = { sizeof (HideExcessAreaAbstractBehaviour_t3369390328), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4268[7] = 
{
	HideExcessAreaAbstractBehaviour_t3369390328::get_offset_of_mClippingImpl_2(),
	HideExcessAreaAbstractBehaviour_t3369390328::get_offset_of_mClippingMode_3(),
	HideExcessAreaAbstractBehaviour_t3369390328::get_offset_of_mVuforiaBehaviour_4(),
	HideExcessAreaAbstractBehaviour_t3369390328::get_offset_of_mVideoBgMgr_5(),
	HideExcessAreaAbstractBehaviour_t3369390328::get_offset_of_mPlaneOffset_6(),
	HideExcessAreaAbstractBehaviour_t3369390328::get_offset_of_mSceneScaledDown_7(),
	HideExcessAreaAbstractBehaviour_t3369390328::get_offset_of_mStarted_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4269 = { sizeof (CLIPPING_MODE_t259981078)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4269[4] = 
{
	CLIPPING_MODE_t259981078::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4270 = { sizeof (ObjectTargetImpl_t3614635090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4270[2] = 
{
	ObjectTargetImpl_t3614635090::get_offset_of_mSize_2(),
	ObjectTargetImpl_t3614635090::get_offset_of_mDataSet_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4271 = { sizeof (UnityPlayer_t3127875071), -1, sizeof(UnityPlayer_t3127875071_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4271[1] = 
{
	UnityPlayer_t3127875071_StaticFields::get_offset_of_sPlayer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4272 = { sizeof (NullUnityPlayer_t2922069181), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4273 = { sizeof (PlayModeUnityPlayer_t3763348594), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4274 = { sizeof (ReconstructionFromTargetImpl_t676137874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4274[6] = 
{
	ReconstructionFromTargetImpl_t676137874::get_offset_of_mOccluderMin_5(),
	ReconstructionFromTargetImpl_t676137874::get_offset_of_mOccluderMax_6(),
	ReconstructionFromTargetImpl_t676137874::get_offset_of_mOccluderOffset_7(),
	ReconstructionFromTargetImpl_t676137874::get_offset_of_mOccluderRotation_8(),
	ReconstructionFromTargetImpl_t676137874::get_offset_of_mInitializationTarget_9(),
	ReconstructionFromTargetImpl_t676137874::get_offset_of_mCanAutoSetInitializationTarget_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4275 = { sizeof (Device_t64880687), -1, sizeof(Device_t64880687_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4275[1] = 
{
	Device_t64880687_StaticFields::get_offset_of_mInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4276 = { sizeof (Mode_t3830573374)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4276[3] = 
{
	Mode_t3830573374::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4277 = { sizeof (ViewerButtonType_t3221680132)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4277[5] = 
{
	ViewerButtonType_t3221680132::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4278 = { sizeof (ViewerTrayAlignment_t2810797062)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4278[4] = 
{
	ViewerTrayAlignment_t2810797062::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4279 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4280 = { sizeof (VuforiaAbstractBehaviour_t3510878193), -1, sizeof(VuforiaAbstractBehaviour_t3510878193_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4280[17] = 
{
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_mWorldCenterMode_2(),
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_mWorldCenter_3(),
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_mCentralAnchorPoint_4(),
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_mParentAnchorPoint_5(),
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_mPrimaryCamera_6(),
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_mSecondaryCamera_7(),
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_mWereBindingFieldsExposed_8(),
	VuforiaAbstractBehaviour_t3510878193_StaticFields::get_offset_of_BehaviourCreated_9(),
	VuforiaAbstractBehaviour_t3510878193_StaticFields::get_offset_of_BehaviourDestroyed_10(),
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_AwakeEvent_11(),
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_OnEnableEvent_12(),
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_StartEvent_13(),
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_UpdateEvent_14(),
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_OnLevelWasLoadedEvent_15(),
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_OnApplicationPauseEvent_16(),
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_OnDisableEvent_17(),
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_OnDestroyEvent_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4281 = { sizeof (VuforiaAbstractConfiguration_t2684447159), -1, sizeof(VuforiaAbstractConfiguration_t2684447159_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4281[9] = 
{
	VuforiaAbstractConfiguration_t2684447159_StaticFields::get_offset_of_mInstance_2(),
	VuforiaAbstractConfiguration_t2684447159_StaticFields::get_offset_of_mPadlock_3(),
	VuforiaAbstractConfiguration_t2684447159::get_offset_of_vuforia_4(),
	VuforiaAbstractConfiguration_t2684447159::get_offset_of_digitalEyewear_5(),
	VuforiaAbstractConfiguration_t2684447159::get_offset_of_databaseLoad_6(),
	VuforiaAbstractConfiguration_t2684447159::get_offset_of_videoBackground_7(),
	VuforiaAbstractConfiguration_t2684447159::get_offset_of_smartTerrainTracker_8(),
	VuforiaAbstractConfiguration_t2684447159::get_offset_of_deviceTracker_9(),
	VuforiaAbstractConfiguration_t2684447159::get_offset_of_webcam_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4282 = { sizeof (GenericVuforiaConfiguration_t1909783692), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4282[8] = 
{
	GenericVuforiaConfiguration_t1909783692::get_offset_of_vuforiaLicenseKey_0(),
	GenericVuforiaConfiguration_t1909783692::get_offset_of_delayedInitialization_1(),
	GenericVuforiaConfiguration_t1909783692::get_offset_of_cameraDeviceModeSetting_2(),
	GenericVuforiaConfiguration_t1909783692::get_offset_of_maxSimultaneousImageTargets_3(),
	GenericVuforiaConfiguration_t1909783692::get_offset_of_maxSimultaneousObjectTargets_4(),
	GenericVuforiaConfiguration_t1909783692::get_offset_of_useDelayedLoadingObjectTargets_5(),
	GenericVuforiaConfiguration_t1909783692::get_offset_of_cameraDirection_6(),
	GenericVuforiaConfiguration_t1909783692::get_offset_of_mirrorVideoBackground_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4283 = { sizeof (DigitalEyewearConfiguration_t2828783036), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4283[10] = 
{
	DigitalEyewearConfiguration_t2828783036::get_offset_of_cameraOffset_0(),
	DigitalEyewearConfiguration_t2828783036::get_offset_of_distortionRenderingMode_1(),
	DigitalEyewearConfiguration_t2828783036::get_offset_of_distortionRenderingLayer_2(),
	DigitalEyewearConfiguration_t2828783036::get_offset_of_eyewearType_3(),
	DigitalEyewearConfiguration_t2828783036::get_offset_of_stereoFramework_4(),
	DigitalEyewearConfiguration_t2828783036::get_offset_of_seeThroughConfiguration_5(),
	DigitalEyewearConfiguration_t2828783036::get_offset_of_viewerName_6(),
	DigitalEyewearConfiguration_t2828783036::get_offset_of_viewerManufacturer_7(),
	DigitalEyewearConfiguration_t2828783036::get_offset_of_useCustomViewer_8(),
	DigitalEyewearConfiguration_t2828783036::get_offset_of_customViewer_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4284 = { sizeof (DatabaseLoadConfiguration_t3815674388), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4284[2] = 
{
	DatabaseLoadConfiguration_t3815674388::get_offset_of_dataSetsToLoad_0(),
	DatabaseLoadConfiguration_t3815674388::get_offset_of_dataSetsToActivate_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4285 = { sizeof (VideoBackgroundConfiguration_t1174662728), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4285[3] = 
{
	VideoBackgroundConfiguration_t1174662728::get_offset_of_clippingMode_0(),
	VideoBackgroundConfiguration_t1174662728::get_offset_of_matteShader_1(),
	VideoBackgroundConfiguration_t1174662728::get_offset_of_videoBackgroundEnabled_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4286 = { sizeof (TrackerConfiguration_t588160133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4286[2] = 
{
	TrackerConfiguration_t588160133::get_offset_of_autoInitTracker_0(),
	TrackerConfiguration_t588160133::get_offset_of_autoStartTracker_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4287 = { sizeof (SmartTerrainTrackerConfiguration_t3112457179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4287[2] = 
{
	SmartTerrainTrackerConfiguration_t3112457179::get_offset_of_autoInitBuilder_2(),
	SmartTerrainTrackerConfiguration_t3112457179::get_offset_of_sceneUnitsToMillimeter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4288 = { sizeof (DeviceTrackerConfiguration_t1841344395), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4288[4] = 
{
	DeviceTrackerConfiguration_t1841344395::get_offset_of_posePrediction_2(),
	DeviceTrackerConfiguration_t1841344395::get_offset_of_modelCorrectionMode_3(),
	DeviceTrackerConfiguration_t1841344395::get_offset_of_modelTransformEnabled_4(),
	DeviceTrackerConfiguration_t1841344395::get_offset_of_modelTransform_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4289 = { sizeof (WebCamConfiguration_t802847339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4289[4] = 
{
	WebCamConfiguration_t802847339::get_offset_of_deviceNameSetInEditor_0(),
	WebCamConfiguration_t802847339::get_offset_of_flipHorizontally_1(),
	WebCamConfiguration_t802847339::get_offset_of_turnOffWebCam_2(),
	WebCamConfiguration_t802847339::get_offset_of_renderTextureLayer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4290 = { sizeof (VuforiaDeinitBehaviour_t1700985552), -1, sizeof(VuforiaDeinitBehaviour_t1700985552_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4290[1] = 
{
	VuforiaDeinitBehaviour_t1700985552_StaticFields::get_offset_of_mAppIsQuitting_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4291 = { sizeof (VuforiaRuntime_t1949122020), -1, sizeof(VuforiaRuntime_t1949122020_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4291[6] = 
{
	VuforiaRuntime_t1949122020::get_offset_of_mOnVuforiaInitError_0(),
	VuforiaRuntime_t1949122020::get_offset_of_mFailedToInitialize_1(),
	VuforiaRuntime_t1949122020::get_offset_of_mInitError_2(),
	VuforiaRuntime_t1949122020::get_offset_of_mHasInitialized_3(),
	VuforiaRuntime_t1949122020_StaticFields::get_offset_of_mInstance_4(),
	VuforiaRuntime_t1949122020_StaticFields::get_offset_of_mPadlock_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4292 = { sizeof (VuMarkAbstractBehaviour_t580651545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4292[16] = 
{
	VuMarkAbstractBehaviour_t580651545::get_offset_of_mAspectRatio_20(),
	VuMarkAbstractBehaviour_t580651545::get_offset_of_mWidth_21(),
	VuMarkAbstractBehaviour_t580651545::get_offset_of_mHeight_22(),
	VuMarkAbstractBehaviour_t580651545::get_offset_of_mPreviewImage_23(),
	VuMarkAbstractBehaviour_t580651545::get_offset_of_mIdType_24(),
	VuMarkAbstractBehaviour_t580651545::get_offset_of_mIdLength_25(),
	VuMarkAbstractBehaviour_t580651545::get_offset_of_mBoundingBox_26(),
	VuMarkAbstractBehaviour_t580651545::get_offset_of_mOrigin_27(),
	VuMarkAbstractBehaviour_t580651545::get_offset_of_mTrackingFromRuntimeAppearance_28(),
	VuMarkAbstractBehaviour_t580651545::get_offset_of_mVuMarkTemplate_29(),
	VuMarkAbstractBehaviour_t580651545::get_offset_of_mVuMarkTarget_30(),
	VuMarkAbstractBehaviour_t580651545::get_offset_of_mVuMarkResultId_31(),
	VuMarkAbstractBehaviour_t580651545::get_offset_of_mOnTargetAssigned_32(),
	VuMarkAbstractBehaviour_t580651545::get_offset_of_mOnTargetLost_33(),
	VuMarkAbstractBehaviour_t580651545::get_offset_of_mLastTransformScale_34(),
	VuMarkAbstractBehaviour_t580651545::get_offset_of_mLastSize_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4293 = { sizeof (VuMarkManager_t2982459596), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4294 = { sizeof (InstanceIdType_t420283664)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4294[4] = 
{
	InstanceIdType_t420283664::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4295 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4296 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4297 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4298 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4299 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
