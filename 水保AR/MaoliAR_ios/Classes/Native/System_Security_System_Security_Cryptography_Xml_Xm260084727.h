﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Security_System_Security_Cryptography_Xml_T1105379765.h"

// System.Security.Cryptography.CryptoStream
struct CryptoStream_t2702504504;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.Xml.XmlDsigBase64Transform
struct  XmlDsigBase64Transform_t260084727  : public Transform_t1105379765
{
public:
	// System.Security.Cryptography.CryptoStream System.Security.Cryptography.Xml.XmlDsigBase64Transform::cs
	CryptoStream_t2702504504 * ___cs_3;

public:
	inline static int32_t get_offset_of_cs_3() { return static_cast<int32_t>(offsetof(XmlDsigBase64Transform_t260084727, ___cs_3)); }
	inline CryptoStream_t2702504504 * get_cs_3() const { return ___cs_3; }
	inline CryptoStream_t2702504504 ** get_address_of_cs_3() { return &___cs_3; }
	inline void set_cs_3(CryptoStream_t2702504504 * value)
	{
		___cs_3 = value;
		Il2CppCodeGenWriteBarrier(&___cs_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
