﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.String[]
struct StringU5BU5D_t1281789340;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.Directive
struct  Directive_t1556494119  : public Il2CppObject
{
public:

public:
};

struct Directive_t1556494119_StaticFields
{
public:
	// System.Collections.Hashtable System.Web.Compilation.Directive::directivesHash
	Hashtable_t1853889766 * ___directivesHash_0;
	// System.String[] System.Web.Compilation.Directive::page_atts
	StringU5BU5D_t1281789340* ___page_atts_1;
	// System.String[] System.Web.Compilation.Directive::control_atts
	StringU5BU5D_t1281789340* ___control_atts_2;
	// System.String[] System.Web.Compilation.Directive::import_atts
	StringU5BU5D_t1281789340* ___import_atts_3;
	// System.String[] System.Web.Compilation.Directive::implements_atts
	StringU5BU5D_t1281789340* ___implements_atts_4;
	// System.String[] System.Web.Compilation.Directive::assembly_atts
	StringU5BU5D_t1281789340* ___assembly_atts_5;
	// System.String[] System.Web.Compilation.Directive::register_atts
	StringU5BU5D_t1281789340* ___register_atts_6;
	// System.String[] System.Web.Compilation.Directive::outputcache_atts
	StringU5BU5D_t1281789340* ___outputcache_atts_7;
	// System.String[] System.Web.Compilation.Directive::reference_atts
	StringU5BU5D_t1281789340* ___reference_atts_8;
	// System.String[] System.Web.Compilation.Directive::webservice_atts
	StringU5BU5D_t1281789340* ___webservice_atts_9;
	// System.String[] System.Web.Compilation.Directive::application_atts
	StringU5BU5D_t1281789340* ___application_atts_10;
	// System.String[] System.Web.Compilation.Directive::mastertype_atts
	StringU5BU5D_t1281789340* ___mastertype_atts_11;
	// System.String[] System.Web.Compilation.Directive::previouspagetype_atts
	StringU5BU5D_t1281789340* ___previouspagetype_atts_12;

public:
	inline static int32_t get_offset_of_directivesHash_0() { return static_cast<int32_t>(offsetof(Directive_t1556494119_StaticFields, ___directivesHash_0)); }
	inline Hashtable_t1853889766 * get_directivesHash_0() const { return ___directivesHash_0; }
	inline Hashtable_t1853889766 ** get_address_of_directivesHash_0() { return &___directivesHash_0; }
	inline void set_directivesHash_0(Hashtable_t1853889766 * value)
	{
		___directivesHash_0 = value;
		Il2CppCodeGenWriteBarrier(&___directivesHash_0, value);
	}

	inline static int32_t get_offset_of_page_atts_1() { return static_cast<int32_t>(offsetof(Directive_t1556494119_StaticFields, ___page_atts_1)); }
	inline StringU5BU5D_t1281789340* get_page_atts_1() const { return ___page_atts_1; }
	inline StringU5BU5D_t1281789340** get_address_of_page_atts_1() { return &___page_atts_1; }
	inline void set_page_atts_1(StringU5BU5D_t1281789340* value)
	{
		___page_atts_1 = value;
		Il2CppCodeGenWriteBarrier(&___page_atts_1, value);
	}

	inline static int32_t get_offset_of_control_atts_2() { return static_cast<int32_t>(offsetof(Directive_t1556494119_StaticFields, ___control_atts_2)); }
	inline StringU5BU5D_t1281789340* get_control_atts_2() const { return ___control_atts_2; }
	inline StringU5BU5D_t1281789340** get_address_of_control_atts_2() { return &___control_atts_2; }
	inline void set_control_atts_2(StringU5BU5D_t1281789340* value)
	{
		___control_atts_2 = value;
		Il2CppCodeGenWriteBarrier(&___control_atts_2, value);
	}

	inline static int32_t get_offset_of_import_atts_3() { return static_cast<int32_t>(offsetof(Directive_t1556494119_StaticFields, ___import_atts_3)); }
	inline StringU5BU5D_t1281789340* get_import_atts_3() const { return ___import_atts_3; }
	inline StringU5BU5D_t1281789340** get_address_of_import_atts_3() { return &___import_atts_3; }
	inline void set_import_atts_3(StringU5BU5D_t1281789340* value)
	{
		___import_atts_3 = value;
		Il2CppCodeGenWriteBarrier(&___import_atts_3, value);
	}

	inline static int32_t get_offset_of_implements_atts_4() { return static_cast<int32_t>(offsetof(Directive_t1556494119_StaticFields, ___implements_atts_4)); }
	inline StringU5BU5D_t1281789340* get_implements_atts_4() const { return ___implements_atts_4; }
	inline StringU5BU5D_t1281789340** get_address_of_implements_atts_4() { return &___implements_atts_4; }
	inline void set_implements_atts_4(StringU5BU5D_t1281789340* value)
	{
		___implements_atts_4 = value;
		Il2CppCodeGenWriteBarrier(&___implements_atts_4, value);
	}

	inline static int32_t get_offset_of_assembly_atts_5() { return static_cast<int32_t>(offsetof(Directive_t1556494119_StaticFields, ___assembly_atts_5)); }
	inline StringU5BU5D_t1281789340* get_assembly_atts_5() const { return ___assembly_atts_5; }
	inline StringU5BU5D_t1281789340** get_address_of_assembly_atts_5() { return &___assembly_atts_5; }
	inline void set_assembly_atts_5(StringU5BU5D_t1281789340* value)
	{
		___assembly_atts_5 = value;
		Il2CppCodeGenWriteBarrier(&___assembly_atts_5, value);
	}

	inline static int32_t get_offset_of_register_atts_6() { return static_cast<int32_t>(offsetof(Directive_t1556494119_StaticFields, ___register_atts_6)); }
	inline StringU5BU5D_t1281789340* get_register_atts_6() const { return ___register_atts_6; }
	inline StringU5BU5D_t1281789340** get_address_of_register_atts_6() { return &___register_atts_6; }
	inline void set_register_atts_6(StringU5BU5D_t1281789340* value)
	{
		___register_atts_6 = value;
		Il2CppCodeGenWriteBarrier(&___register_atts_6, value);
	}

	inline static int32_t get_offset_of_outputcache_atts_7() { return static_cast<int32_t>(offsetof(Directive_t1556494119_StaticFields, ___outputcache_atts_7)); }
	inline StringU5BU5D_t1281789340* get_outputcache_atts_7() const { return ___outputcache_atts_7; }
	inline StringU5BU5D_t1281789340** get_address_of_outputcache_atts_7() { return &___outputcache_atts_7; }
	inline void set_outputcache_atts_7(StringU5BU5D_t1281789340* value)
	{
		___outputcache_atts_7 = value;
		Il2CppCodeGenWriteBarrier(&___outputcache_atts_7, value);
	}

	inline static int32_t get_offset_of_reference_atts_8() { return static_cast<int32_t>(offsetof(Directive_t1556494119_StaticFields, ___reference_atts_8)); }
	inline StringU5BU5D_t1281789340* get_reference_atts_8() const { return ___reference_atts_8; }
	inline StringU5BU5D_t1281789340** get_address_of_reference_atts_8() { return &___reference_atts_8; }
	inline void set_reference_atts_8(StringU5BU5D_t1281789340* value)
	{
		___reference_atts_8 = value;
		Il2CppCodeGenWriteBarrier(&___reference_atts_8, value);
	}

	inline static int32_t get_offset_of_webservice_atts_9() { return static_cast<int32_t>(offsetof(Directive_t1556494119_StaticFields, ___webservice_atts_9)); }
	inline StringU5BU5D_t1281789340* get_webservice_atts_9() const { return ___webservice_atts_9; }
	inline StringU5BU5D_t1281789340** get_address_of_webservice_atts_9() { return &___webservice_atts_9; }
	inline void set_webservice_atts_9(StringU5BU5D_t1281789340* value)
	{
		___webservice_atts_9 = value;
		Il2CppCodeGenWriteBarrier(&___webservice_atts_9, value);
	}

	inline static int32_t get_offset_of_application_atts_10() { return static_cast<int32_t>(offsetof(Directive_t1556494119_StaticFields, ___application_atts_10)); }
	inline StringU5BU5D_t1281789340* get_application_atts_10() const { return ___application_atts_10; }
	inline StringU5BU5D_t1281789340** get_address_of_application_atts_10() { return &___application_atts_10; }
	inline void set_application_atts_10(StringU5BU5D_t1281789340* value)
	{
		___application_atts_10 = value;
		Il2CppCodeGenWriteBarrier(&___application_atts_10, value);
	}

	inline static int32_t get_offset_of_mastertype_atts_11() { return static_cast<int32_t>(offsetof(Directive_t1556494119_StaticFields, ___mastertype_atts_11)); }
	inline StringU5BU5D_t1281789340* get_mastertype_atts_11() const { return ___mastertype_atts_11; }
	inline StringU5BU5D_t1281789340** get_address_of_mastertype_atts_11() { return &___mastertype_atts_11; }
	inline void set_mastertype_atts_11(StringU5BU5D_t1281789340* value)
	{
		___mastertype_atts_11 = value;
		Il2CppCodeGenWriteBarrier(&___mastertype_atts_11, value);
	}

	inline static int32_t get_offset_of_previouspagetype_atts_12() { return static_cast<int32_t>(offsetof(Directive_t1556494119_StaticFields, ___previouspagetype_atts_12)); }
	inline StringU5BU5D_t1281789340* get_previouspagetype_atts_12() const { return ___previouspagetype_atts_12; }
	inline StringU5BU5D_t1281789340** get_address_of_previouspagetype_atts_12() { return &___previouspagetype_atts_12; }
	inline void set_previouspagetype_atts_12(StringU5BU5D_t1281789340* value)
	{
		___previouspagetype_atts_12 = value;
		Il2CppCodeGenWriteBarrier(&___previouspagetype_atts_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
