﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Description.MessageDescriptionCollection
struct MessageDescriptionCollection_t3177516813;
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t390618515;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.BaseMessagesFormatter
struct  BaseMessagesFormatter_t2309470997  : public Il2CppObject
{
public:
	// System.ServiceModel.Description.MessageDescriptionCollection System.ServiceModel.Dispatcher.BaseMessagesFormatter::messages
	MessageDescriptionCollection_t3177516813 * ___messages_0;
	// System.Boolean System.ServiceModel.Dispatcher.BaseMessagesFormatter::isAsync
	bool ___isAsync_1;
	// System.Reflection.ParameterInfo[] System.ServiceModel.Dispatcher.BaseMessagesFormatter::requestMethodParams
	ParameterInfoU5BU5D_t390618515* ___requestMethodParams_2;
	// System.Reflection.ParameterInfo[] System.ServiceModel.Dispatcher.BaseMessagesFormatter::replyMethodParams
	ParameterInfoU5BU5D_t390618515* ___replyMethodParams_3;

public:
	inline static int32_t get_offset_of_messages_0() { return static_cast<int32_t>(offsetof(BaseMessagesFormatter_t2309470997, ___messages_0)); }
	inline MessageDescriptionCollection_t3177516813 * get_messages_0() const { return ___messages_0; }
	inline MessageDescriptionCollection_t3177516813 ** get_address_of_messages_0() { return &___messages_0; }
	inline void set_messages_0(MessageDescriptionCollection_t3177516813 * value)
	{
		___messages_0 = value;
		Il2CppCodeGenWriteBarrier(&___messages_0, value);
	}

	inline static int32_t get_offset_of_isAsync_1() { return static_cast<int32_t>(offsetof(BaseMessagesFormatter_t2309470997, ___isAsync_1)); }
	inline bool get_isAsync_1() const { return ___isAsync_1; }
	inline bool* get_address_of_isAsync_1() { return &___isAsync_1; }
	inline void set_isAsync_1(bool value)
	{
		___isAsync_1 = value;
	}

	inline static int32_t get_offset_of_requestMethodParams_2() { return static_cast<int32_t>(offsetof(BaseMessagesFormatter_t2309470997, ___requestMethodParams_2)); }
	inline ParameterInfoU5BU5D_t390618515* get_requestMethodParams_2() const { return ___requestMethodParams_2; }
	inline ParameterInfoU5BU5D_t390618515** get_address_of_requestMethodParams_2() { return &___requestMethodParams_2; }
	inline void set_requestMethodParams_2(ParameterInfoU5BU5D_t390618515* value)
	{
		___requestMethodParams_2 = value;
		Il2CppCodeGenWriteBarrier(&___requestMethodParams_2, value);
	}

	inline static int32_t get_offset_of_replyMethodParams_3() { return static_cast<int32_t>(offsetof(BaseMessagesFormatter_t2309470997, ___replyMethodParams_3)); }
	inline ParameterInfoU5BU5D_t390618515* get_replyMethodParams_3() const { return ___replyMethodParams_3; }
	inline ParameterInfoU5BU5D_t390618515** get_address_of_replyMethodParams_3() { return &___replyMethodParams_3; }
	inline void set_replyMethodParams_3(ParameterInfoU5BU5D_t390618515* value)
	{
		___replyMethodParams_3 = value;
		Il2CppCodeGenWriteBarrier(&___replyMethodParams_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
