﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.IO.InotifyData
struct InotifyData_t2533354870;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.ParentInotifyData
struct  ParentInotifyData_t1149622319  : public Il2CppObject
{
public:
	// System.Boolean System.IO.ParentInotifyData::IncludeSubdirs
	bool ___IncludeSubdirs_0;
	// System.Boolean System.IO.ParentInotifyData::Enabled
	bool ___Enabled_1;
	// System.Collections.ArrayList System.IO.ParentInotifyData::children
	ArrayList_t2718874744 * ___children_2;
	// System.IO.InotifyData System.IO.ParentInotifyData::data
	InotifyData_t2533354870 * ___data_3;

public:
	inline static int32_t get_offset_of_IncludeSubdirs_0() { return static_cast<int32_t>(offsetof(ParentInotifyData_t1149622319, ___IncludeSubdirs_0)); }
	inline bool get_IncludeSubdirs_0() const { return ___IncludeSubdirs_0; }
	inline bool* get_address_of_IncludeSubdirs_0() { return &___IncludeSubdirs_0; }
	inline void set_IncludeSubdirs_0(bool value)
	{
		___IncludeSubdirs_0 = value;
	}

	inline static int32_t get_offset_of_Enabled_1() { return static_cast<int32_t>(offsetof(ParentInotifyData_t1149622319, ___Enabled_1)); }
	inline bool get_Enabled_1() const { return ___Enabled_1; }
	inline bool* get_address_of_Enabled_1() { return &___Enabled_1; }
	inline void set_Enabled_1(bool value)
	{
		___Enabled_1 = value;
	}

	inline static int32_t get_offset_of_children_2() { return static_cast<int32_t>(offsetof(ParentInotifyData_t1149622319, ___children_2)); }
	inline ArrayList_t2718874744 * get_children_2() const { return ___children_2; }
	inline ArrayList_t2718874744 ** get_address_of_children_2() { return &___children_2; }
	inline void set_children_2(ArrayList_t2718874744 * value)
	{
		___children_2 = value;
		Il2CppCodeGenWriteBarrier(&___children_2, value);
	}

	inline static int32_t get_offset_of_data_3() { return static_cast<int32_t>(offsetof(ParentInotifyData_t1149622319, ___data_3)); }
	inline InotifyData_t2533354870 * get_data_3() const { return ___data_3; }
	inline InotifyData_t2533354870 ** get_address_of_data_3() { return &___data_3; }
	inline void set_data_3(InotifyData_t2533354870 * value)
	{
		___data_3 = value;
		Il2CppCodeGenWriteBarrier(&___data_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
