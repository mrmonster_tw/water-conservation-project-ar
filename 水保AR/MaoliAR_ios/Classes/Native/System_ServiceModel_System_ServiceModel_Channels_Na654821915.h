﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_C3754390252.h"

// System.ServiceModel.Channels.NamedPipeConnectionPoolSettings
struct NamedPipeConnectionPoolSettings_t179073301;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.NamedPipeTransportBindingElement
struct  NamedPipeTransportBindingElement_t654821915  : public ConnectionOrientedTransportBindingElement_t3754390252
{
public:
	// System.ServiceModel.Channels.NamedPipeConnectionPoolSettings System.ServiceModel.Channels.NamedPipeTransportBindingElement::pool
	NamedPipeConnectionPoolSettings_t179073301 * ___pool_11;

public:
	inline static int32_t get_offset_of_pool_11() { return static_cast<int32_t>(offsetof(NamedPipeTransportBindingElement_t654821915, ___pool_11)); }
	inline NamedPipeConnectionPoolSettings_t179073301 * get_pool_11() const { return ___pool_11; }
	inline NamedPipeConnectionPoolSettings_t179073301 ** get_address_of_pool_11() { return &___pool_11; }
	inline void set_pool_11(NamedPipeConnectionPoolSettings_t179073301 * value)
	{
		___pool_11 = value;
		Il2CppCodeGenWriteBarrier(&___pool_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
