﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Level_Controller/<Awake>c__AnonStorey0
struct  U3CAwakeU3Ec__AnonStorey0_t4163605756  : public Il2CppObject
{
public:
	// System.String Level_Controller/<Awake>c__AnonStorey0::findStr
	String_t* ___findStr_0;

public:
	inline static int32_t get_offset_of_findStr_0() { return static_cast<int32_t>(offsetof(U3CAwakeU3Ec__AnonStorey0_t4163605756, ___findStr_0)); }
	inline String_t* get_findStr_0() const { return ___findStr_0; }
	inline String_t** get_address_of_findStr_0() { return &___findStr_0; }
	inline void set_findStr_0(String_t* value)
	{
		___findStr_0 = value;
		Il2CppCodeGenWriteBarrier(&___findStr_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
