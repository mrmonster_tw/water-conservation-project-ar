﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeExpression2166265795.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeArgumentReferenceExpression
struct  CodeArgumentReferenceExpression_t2053988171  : public CodeExpression_t2166265795
{
public:
	// System.String System.CodeDom.CodeArgumentReferenceExpression::parameterName
	String_t* ___parameterName_1;

public:
	inline static int32_t get_offset_of_parameterName_1() { return static_cast<int32_t>(offsetof(CodeArgumentReferenceExpression_t2053988171, ___parameterName_1)); }
	inline String_t* get_parameterName_1() const { return ___parameterName_1; }
	inline String_t** get_address_of_parameterName_1() { return &___parameterName_1; }
	inline void set_parameterName_1(String_t* value)
	{
		___parameterName_1 = value;
		Il2CppCodeGenWriteBarrier(&___parameterName_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
