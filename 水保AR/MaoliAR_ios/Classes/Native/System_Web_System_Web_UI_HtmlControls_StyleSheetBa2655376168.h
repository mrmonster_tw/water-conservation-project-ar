﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.UI.WebControls.Style
struct Style_t3589053173;
// System.String
struct String_t;
// System.Web.UI.IUrlResolutionService
struct IUrlResolutionService_t4016525173;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.HtmlControls.StyleSheetBag/StyleEntry
struct  StyleEntry_t2655376168  : public Il2CppObject
{
public:
	// System.Web.UI.WebControls.Style System.Web.UI.HtmlControls.StyleSheetBag/StyleEntry::Style
	Style_t3589053173 * ___Style_0;
	// System.String System.Web.UI.HtmlControls.StyleSheetBag/StyleEntry::Selection
	String_t* ___Selection_1;
	// System.Web.UI.IUrlResolutionService System.Web.UI.HtmlControls.StyleSheetBag/StyleEntry::UrlResolver
	Il2CppObject * ___UrlResolver_2;

public:
	inline static int32_t get_offset_of_Style_0() { return static_cast<int32_t>(offsetof(StyleEntry_t2655376168, ___Style_0)); }
	inline Style_t3589053173 * get_Style_0() const { return ___Style_0; }
	inline Style_t3589053173 ** get_address_of_Style_0() { return &___Style_0; }
	inline void set_Style_0(Style_t3589053173 * value)
	{
		___Style_0 = value;
		Il2CppCodeGenWriteBarrier(&___Style_0, value);
	}

	inline static int32_t get_offset_of_Selection_1() { return static_cast<int32_t>(offsetof(StyleEntry_t2655376168, ___Selection_1)); }
	inline String_t* get_Selection_1() const { return ___Selection_1; }
	inline String_t** get_address_of_Selection_1() { return &___Selection_1; }
	inline void set_Selection_1(String_t* value)
	{
		___Selection_1 = value;
		Il2CppCodeGenWriteBarrier(&___Selection_1, value);
	}

	inline static int32_t get_offset_of_UrlResolver_2() { return static_cast<int32_t>(offsetof(StyleEntry_t2655376168, ___UrlResolver_2)); }
	inline Il2CppObject * get_UrlResolver_2() const { return ___UrlResolver_2; }
	inline Il2CppObject ** get_address_of_UrlResolver_2() { return &___UrlResolver_2; }
	inline void set_UrlResolver_2(Il2CppObject * value)
	{
		___UrlResolver_2 = value;
		Il2CppCodeGenWriteBarrier(&___UrlResolver_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
