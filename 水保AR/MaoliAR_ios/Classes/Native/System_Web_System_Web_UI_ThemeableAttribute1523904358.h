﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"

// System.Web.UI.ThemeableAttribute
struct ThemeableAttribute_t1523904358;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.ThemeableAttribute
struct  ThemeableAttribute_t1523904358  : public Attribute_t861562559
{
public:
	// System.Boolean System.Web.UI.ThemeableAttribute::themeable
	bool ___themeable_0;

public:
	inline static int32_t get_offset_of_themeable_0() { return static_cast<int32_t>(offsetof(ThemeableAttribute_t1523904358, ___themeable_0)); }
	inline bool get_themeable_0() const { return ___themeable_0; }
	inline bool* get_address_of_themeable_0() { return &___themeable_0; }
	inline void set_themeable_0(bool value)
	{
		___themeable_0 = value;
	}
};

struct ThemeableAttribute_t1523904358_StaticFields
{
public:
	// System.Web.UI.ThemeableAttribute System.Web.UI.ThemeableAttribute::Default
	ThemeableAttribute_t1523904358 * ___Default_1;
	// System.Web.UI.ThemeableAttribute System.Web.UI.ThemeableAttribute::No
	ThemeableAttribute_t1523904358 * ___No_2;
	// System.Web.UI.ThemeableAttribute System.Web.UI.ThemeableAttribute::Yes
	ThemeableAttribute_t1523904358 * ___Yes_3;

public:
	inline static int32_t get_offset_of_Default_1() { return static_cast<int32_t>(offsetof(ThemeableAttribute_t1523904358_StaticFields, ___Default_1)); }
	inline ThemeableAttribute_t1523904358 * get_Default_1() const { return ___Default_1; }
	inline ThemeableAttribute_t1523904358 ** get_address_of_Default_1() { return &___Default_1; }
	inline void set_Default_1(ThemeableAttribute_t1523904358 * value)
	{
		___Default_1 = value;
		Il2CppCodeGenWriteBarrier(&___Default_1, value);
	}

	inline static int32_t get_offset_of_No_2() { return static_cast<int32_t>(offsetof(ThemeableAttribute_t1523904358_StaticFields, ___No_2)); }
	inline ThemeableAttribute_t1523904358 * get_No_2() const { return ___No_2; }
	inline ThemeableAttribute_t1523904358 ** get_address_of_No_2() { return &___No_2; }
	inline void set_No_2(ThemeableAttribute_t1523904358 * value)
	{
		___No_2 = value;
		Il2CppCodeGenWriteBarrier(&___No_2, value);
	}

	inline static int32_t get_offset_of_Yes_3() { return static_cast<int32_t>(offsetof(ThemeableAttribute_t1523904358_StaticFields, ___Yes_3)); }
	inline ThemeableAttribute_t1523904358 * get_Yes_3() const { return ___Yes_3; }
	inline ThemeableAttribute_t1523904358 ** get_address_of_Yes_3() { return &___Yes_3; }
	inline void set_Yes_3(ThemeableAttribute_t1523904358 * value)
	{
		___Yes_3 = value;
		Il2CppCodeGenWriteBarrier(&___Yes_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
