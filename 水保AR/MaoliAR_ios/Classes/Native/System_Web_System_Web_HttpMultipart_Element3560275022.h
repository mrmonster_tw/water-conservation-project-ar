﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpMultipart/Element
struct  Element_t3560275022  : public Il2CppObject
{
public:
	// System.String System.Web.HttpMultipart/Element::ContentType
	String_t* ___ContentType_0;
	// System.String System.Web.HttpMultipart/Element::Name
	String_t* ___Name_1;
	// System.String System.Web.HttpMultipart/Element::Filename
	String_t* ___Filename_2;
	// System.Int64 System.Web.HttpMultipart/Element::Start
	int64_t ___Start_3;
	// System.Int64 System.Web.HttpMultipart/Element::Length
	int64_t ___Length_4;

public:
	inline static int32_t get_offset_of_ContentType_0() { return static_cast<int32_t>(offsetof(Element_t3560275022, ___ContentType_0)); }
	inline String_t* get_ContentType_0() const { return ___ContentType_0; }
	inline String_t** get_address_of_ContentType_0() { return &___ContentType_0; }
	inline void set_ContentType_0(String_t* value)
	{
		___ContentType_0 = value;
		Il2CppCodeGenWriteBarrier(&___ContentType_0, value);
	}

	inline static int32_t get_offset_of_Name_1() { return static_cast<int32_t>(offsetof(Element_t3560275022, ___Name_1)); }
	inline String_t* get_Name_1() const { return ___Name_1; }
	inline String_t** get_address_of_Name_1() { return &___Name_1; }
	inline void set_Name_1(String_t* value)
	{
		___Name_1 = value;
		Il2CppCodeGenWriteBarrier(&___Name_1, value);
	}

	inline static int32_t get_offset_of_Filename_2() { return static_cast<int32_t>(offsetof(Element_t3560275022, ___Filename_2)); }
	inline String_t* get_Filename_2() const { return ___Filename_2; }
	inline String_t** get_address_of_Filename_2() { return &___Filename_2; }
	inline void set_Filename_2(String_t* value)
	{
		___Filename_2 = value;
		Il2CppCodeGenWriteBarrier(&___Filename_2, value);
	}

	inline static int32_t get_offset_of_Start_3() { return static_cast<int32_t>(offsetof(Element_t3560275022, ___Start_3)); }
	inline int64_t get_Start_3() const { return ___Start_3; }
	inline int64_t* get_address_of_Start_3() { return &___Start_3; }
	inline void set_Start_3(int64_t value)
	{
		___Start_3 = value;
	}

	inline static int32_t get_offset_of_Length_4() { return static_cast<int32_t>(offsetof(Element_t3560275022, ___Length_4)); }
	inline int64_t get_Length_4() const { return ___Length_4; }
	inline int64_t* get_address_of_Length_4() { return &___Length_4; }
	inline void set_Length_4(int64_t value)
	{
		___Length_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
