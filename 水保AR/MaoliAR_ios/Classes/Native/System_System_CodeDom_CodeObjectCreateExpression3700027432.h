﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeExpression2166265795.h"

// System.CodeDom.CodeTypeReference
struct CodeTypeReference_t3809997434;
// System.CodeDom.CodeExpressionCollection
struct CodeExpressionCollection_t2370433003;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeObjectCreateExpression
struct  CodeObjectCreateExpression_t3700027432  : public CodeExpression_t2166265795
{
public:
	// System.CodeDom.CodeTypeReference System.CodeDom.CodeObjectCreateExpression::createType
	CodeTypeReference_t3809997434 * ___createType_1;
	// System.CodeDom.CodeExpressionCollection System.CodeDom.CodeObjectCreateExpression::parameters
	CodeExpressionCollection_t2370433003 * ___parameters_2;

public:
	inline static int32_t get_offset_of_createType_1() { return static_cast<int32_t>(offsetof(CodeObjectCreateExpression_t3700027432, ___createType_1)); }
	inline CodeTypeReference_t3809997434 * get_createType_1() const { return ___createType_1; }
	inline CodeTypeReference_t3809997434 ** get_address_of_createType_1() { return &___createType_1; }
	inline void set_createType_1(CodeTypeReference_t3809997434 * value)
	{
		___createType_1 = value;
		Il2CppCodeGenWriteBarrier(&___createType_1, value);
	}

	inline static int32_t get_offset_of_parameters_2() { return static_cast<int32_t>(offsetof(CodeObjectCreateExpression_t3700027432, ___parameters_2)); }
	inline CodeExpressionCollection_t2370433003 * get_parameters_2() const { return ___parameters_2; }
	inline CodeExpressionCollection_t2370433003 ** get_address_of_parameters_2() { return &___parameters_2; }
	inline void set_parameters_2(CodeExpressionCollection_t2370433003 * value)
	{
		___parameters_2 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
