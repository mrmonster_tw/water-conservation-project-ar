﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Generic.IList`1<System.ServiceModel.PeerNodeAddress>
struct IList_1_t3913347155;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.PeerResolvers.ResolveResponseInfoDC
struct  ResolveResponseInfoDC_t2470312020  : public Il2CppObject
{
public:
	// System.Collections.Generic.IList`1<System.ServiceModel.PeerNodeAddress> System.ServiceModel.PeerResolvers.ResolveResponseInfoDC::addresses
	Il2CppObject* ___addresses_0;

public:
	inline static int32_t get_offset_of_addresses_0() { return static_cast<int32_t>(offsetof(ResolveResponseInfoDC_t2470312020, ___addresses_0)); }
	inline Il2CppObject* get_addresses_0() const { return ___addresses_0; }
	inline Il2CppObject** get_address_of_addresses_0() { return &___addresses_0; }
	inline void set_addresses_0(Il2CppObject* value)
	{
		___addresses_0 = value;
		Il2CppCodeGenWriteBarrier(&___addresses_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
