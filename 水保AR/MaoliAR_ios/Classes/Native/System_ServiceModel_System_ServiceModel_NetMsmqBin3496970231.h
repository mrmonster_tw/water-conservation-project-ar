﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_MsmqBindin2036657955.h"
#include "System_ServiceModel_System_ServiceModel_QueueTrans3996023584.h"

// System.ServiceModel.NetMsmqSecurity
struct NetMsmqSecurity_t1571703333;
// System.Xml.XmlDictionaryReaderQuotas
struct XmlDictionaryReaderQuotas_t173030297;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.NetMsmqBinding
struct  NetMsmqBinding_t3496970231  : public MsmqBindingBase_t2036657955
{
public:
	// System.ServiceModel.NetMsmqSecurity System.ServiceModel.NetMsmqBinding::security
	NetMsmqSecurity_t1571703333 * ___security_18;
	// System.Boolean System.ServiceModel.NetMsmqBinding::use_ad
	bool ___use_ad_19;
	// System.Int64 System.ServiceModel.NetMsmqBinding::max_buffer_pool_size
	int64_t ___max_buffer_pool_size_20;
	// System.ServiceModel.QueueTransferProtocol System.ServiceModel.NetMsmqBinding::queue_tr_protocol
	int32_t ___queue_tr_protocol_21;
	// System.Xml.XmlDictionaryReaderQuotas System.ServiceModel.NetMsmqBinding::quotas
	XmlDictionaryReaderQuotas_t173030297 * ___quotas_22;

public:
	inline static int32_t get_offset_of_security_18() { return static_cast<int32_t>(offsetof(NetMsmqBinding_t3496970231, ___security_18)); }
	inline NetMsmqSecurity_t1571703333 * get_security_18() const { return ___security_18; }
	inline NetMsmqSecurity_t1571703333 ** get_address_of_security_18() { return &___security_18; }
	inline void set_security_18(NetMsmqSecurity_t1571703333 * value)
	{
		___security_18 = value;
		Il2CppCodeGenWriteBarrier(&___security_18, value);
	}

	inline static int32_t get_offset_of_use_ad_19() { return static_cast<int32_t>(offsetof(NetMsmqBinding_t3496970231, ___use_ad_19)); }
	inline bool get_use_ad_19() const { return ___use_ad_19; }
	inline bool* get_address_of_use_ad_19() { return &___use_ad_19; }
	inline void set_use_ad_19(bool value)
	{
		___use_ad_19 = value;
	}

	inline static int32_t get_offset_of_max_buffer_pool_size_20() { return static_cast<int32_t>(offsetof(NetMsmqBinding_t3496970231, ___max_buffer_pool_size_20)); }
	inline int64_t get_max_buffer_pool_size_20() const { return ___max_buffer_pool_size_20; }
	inline int64_t* get_address_of_max_buffer_pool_size_20() { return &___max_buffer_pool_size_20; }
	inline void set_max_buffer_pool_size_20(int64_t value)
	{
		___max_buffer_pool_size_20 = value;
	}

	inline static int32_t get_offset_of_queue_tr_protocol_21() { return static_cast<int32_t>(offsetof(NetMsmqBinding_t3496970231, ___queue_tr_protocol_21)); }
	inline int32_t get_queue_tr_protocol_21() const { return ___queue_tr_protocol_21; }
	inline int32_t* get_address_of_queue_tr_protocol_21() { return &___queue_tr_protocol_21; }
	inline void set_queue_tr_protocol_21(int32_t value)
	{
		___queue_tr_protocol_21 = value;
	}

	inline static int32_t get_offset_of_quotas_22() { return static_cast<int32_t>(offsetof(NetMsmqBinding_t3496970231, ___quotas_22)); }
	inline XmlDictionaryReaderQuotas_t173030297 * get_quotas_22() const { return ___quotas_22; }
	inline XmlDictionaryReaderQuotas_t173030297 ** get_address_of_quotas_22() { return &___quotas_22; }
	inline void set_quotas_22(XmlDictionaryReaderQuotas_t173030297 * value)
	{
		___quotas_22 = value;
		Il2CppCodeGenWriteBarrier(&___quotas_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
