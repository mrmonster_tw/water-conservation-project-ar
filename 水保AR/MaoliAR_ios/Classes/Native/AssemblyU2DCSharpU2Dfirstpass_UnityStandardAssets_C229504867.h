﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_4012421388.h"

// UnityStandardAssets.CinematicEffects.AmbientOcclusion/Settings
struct Settings_t1594920509;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t2206337031;
// UnityEngine.Mesh
struct Mesh_t3648964284;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.AmbientOcclusion
struct  AmbientOcclusion_t229504867  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.CinematicEffects.AmbientOcclusion/Settings UnityStandardAssets.CinematicEffects.AmbientOcclusion::settings
	Settings_t1594920509 * ___settings_2;
	// UnityEngine.Shader UnityStandardAssets.CinematicEffects.AmbientOcclusion::_aoShader
	Shader_t4151988712 * ____aoShader_3;
	// UnityEngine.Material UnityStandardAssets.CinematicEffects.AmbientOcclusion::_aoMaterial
	Material_t340375123 * ____aoMaterial_4;
	// UnityEngine.Rendering.CommandBuffer UnityStandardAssets.CinematicEffects.AmbientOcclusion::_aoCommands
	CommandBuffer_t2206337031 * ____aoCommands_5;
	// UnityStandardAssets.CinematicEffects.AmbientOcclusion/PropertyObserver UnityStandardAssets.CinematicEffects.AmbientOcclusion::<propertyObserver>k__BackingField
	PropertyObserver_t4012421388  ___U3CpropertyObserverU3Ek__BackingField_6;
	// UnityEngine.Mesh UnityStandardAssets.CinematicEffects.AmbientOcclusion::_quadMesh
	Mesh_t3648964284 * ____quadMesh_7;

public:
	inline static int32_t get_offset_of_settings_2() { return static_cast<int32_t>(offsetof(AmbientOcclusion_t229504867, ___settings_2)); }
	inline Settings_t1594920509 * get_settings_2() const { return ___settings_2; }
	inline Settings_t1594920509 ** get_address_of_settings_2() { return &___settings_2; }
	inline void set_settings_2(Settings_t1594920509 * value)
	{
		___settings_2 = value;
		Il2CppCodeGenWriteBarrier(&___settings_2, value);
	}

	inline static int32_t get_offset_of__aoShader_3() { return static_cast<int32_t>(offsetof(AmbientOcclusion_t229504867, ____aoShader_3)); }
	inline Shader_t4151988712 * get__aoShader_3() const { return ____aoShader_3; }
	inline Shader_t4151988712 ** get_address_of__aoShader_3() { return &____aoShader_3; }
	inline void set__aoShader_3(Shader_t4151988712 * value)
	{
		____aoShader_3 = value;
		Il2CppCodeGenWriteBarrier(&____aoShader_3, value);
	}

	inline static int32_t get_offset_of__aoMaterial_4() { return static_cast<int32_t>(offsetof(AmbientOcclusion_t229504867, ____aoMaterial_4)); }
	inline Material_t340375123 * get__aoMaterial_4() const { return ____aoMaterial_4; }
	inline Material_t340375123 ** get_address_of__aoMaterial_4() { return &____aoMaterial_4; }
	inline void set__aoMaterial_4(Material_t340375123 * value)
	{
		____aoMaterial_4 = value;
		Il2CppCodeGenWriteBarrier(&____aoMaterial_4, value);
	}

	inline static int32_t get_offset_of__aoCommands_5() { return static_cast<int32_t>(offsetof(AmbientOcclusion_t229504867, ____aoCommands_5)); }
	inline CommandBuffer_t2206337031 * get__aoCommands_5() const { return ____aoCommands_5; }
	inline CommandBuffer_t2206337031 ** get_address_of__aoCommands_5() { return &____aoCommands_5; }
	inline void set__aoCommands_5(CommandBuffer_t2206337031 * value)
	{
		____aoCommands_5 = value;
		Il2CppCodeGenWriteBarrier(&____aoCommands_5, value);
	}

	inline static int32_t get_offset_of_U3CpropertyObserverU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AmbientOcclusion_t229504867, ___U3CpropertyObserverU3Ek__BackingField_6)); }
	inline PropertyObserver_t4012421388  get_U3CpropertyObserverU3Ek__BackingField_6() const { return ___U3CpropertyObserverU3Ek__BackingField_6; }
	inline PropertyObserver_t4012421388 * get_address_of_U3CpropertyObserverU3Ek__BackingField_6() { return &___U3CpropertyObserverU3Ek__BackingField_6; }
	inline void set_U3CpropertyObserverU3Ek__BackingField_6(PropertyObserver_t4012421388  value)
	{
		___U3CpropertyObserverU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of__quadMesh_7() { return static_cast<int32_t>(offsetof(AmbientOcclusion_t229504867, ____quadMesh_7)); }
	inline Mesh_t3648964284 * get__quadMesh_7() const { return ____quadMesh_7; }
	inline Mesh_t3648964284 ** get_address_of__quadMesh_7() { return &____quadMesh_7; }
	inline void set__quadMesh_7(Mesh_t3648964284 * value)
	{
		____quadMesh_7 = value;
		Il2CppCodeGenWriteBarrier(&____quadMesh_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
