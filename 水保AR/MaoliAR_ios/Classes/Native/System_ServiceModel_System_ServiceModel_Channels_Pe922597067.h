﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_P1753925450.h"
#include "System_ServiceModel_System_ServiceModel_PeerResolv1552258726.h"

// System.ServiceModel.PeerResolvers.PeerCustomResolverSettings
struct PeerCustomResolverSettings_t3246124786;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.PeerCustomResolverBindingElement
struct  PeerCustomResolverBindingElement_t922597067  : public PeerResolverBindingElement_t1753925450
{
public:
	// System.ServiceModel.PeerResolvers.PeerCustomResolverSettings System.ServiceModel.Channels.PeerCustomResolverBindingElement::settings
	PeerCustomResolverSettings_t3246124786 * ___settings_0;
	// System.ServiceModel.PeerResolvers.PeerReferralPolicy System.ServiceModel.Channels.PeerCustomResolverBindingElement::<ReferralPolicy>k__BackingField
	int32_t ___U3CReferralPolicyU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_settings_0() { return static_cast<int32_t>(offsetof(PeerCustomResolverBindingElement_t922597067, ___settings_0)); }
	inline PeerCustomResolverSettings_t3246124786 * get_settings_0() const { return ___settings_0; }
	inline PeerCustomResolverSettings_t3246124786 ** get_address_of_settings_0() { return &___settings_0; }
	inline void set_settings_0(PeerCustomResolverSettings_t3246124786 * value)
	{
		___settings_0 = value;
		Il2CppCodeGenWriteBarrier(&___settings_0, value);
	}

	inline static int32_t get_offset_of_U3CReferralPolicyU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PeerCustomResolverBindingElement_t922597067, ___U3CReferralPolicyU3Ek__BackingField_1)); }
	inline int32_t get_U3CReferralPolicyU3Ek__BackingField_1() const { return ___U3CReferralPolicyU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CReferralPolicyU3Ek__BackingField_1() { return &___U3CReferralPolicyU3Ek__BackingField_1; }
	inline void set_U3CReferralPolicyU3Ek__BackingField_1(int32_t value)
	{
		___U3CReferralPolicyU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
