﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3549286319.h"
#include "System_Core_U3CModuleU3E692745525.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E3057255361.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U243907531057.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U244289081651.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U241950429485.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U241929481982.h"
#include "System_Core_Locale4128636107.h"
#include "mscorlib_System_String1847450689.h"
#include "mscorlib_System_Object3080106164.h"
#include "System_Core_Microsoft_Win32_SafeHandles_SafePipeHa1989113880.h"
#include "mscorlib_System_IntPtr840150181.h"
#include "mscorlib_System_Void1185182177.h"
#include "mscorlib_System_Boolean97287965.h"
#include "mscorlib_Microsoft_Win32_SafeHandles_SafeHandleZer1182193648.h"
#include "mscorlib_System_Runtime_InteropServices_SafeHandle3273388951.h"
#include "mscorlib_System_ArgumentException132251570.h"
#include "System_Core_Mono_Security_Cryptography_KeyBuilder2049230354.h"
#include "mscorlib_System_Security_Cryptography_RandomNumberG386037858.h"
#include "mscorlib_System_Byte1134296376.h"
#include "mscorlib_System_Int322950945753.h"
#include "System_Core_Mono_Security_Cryptography_SymmetricTr3802591842.h"
#include "mscorlib_System_Security_Cryptography_SymmetricAlg4254223087.h"
#include "mscorlib_System_Security_Cryptography_Cryptographic248831461.h"
#include "mscorlib_System_Security_Cryptography_CipherMode84635067.h"
#include "mscorlib_System_Enum4135868527.h"
#include "mscorlib_System_NotImplementedException3489357830.h"
#include "mscorlib_System_ArgumentNullException1615371798.h"
#include "mscorlib_System_ArgumentOutOfRangeException777629997.h"
#include "mscorlib_System_ObjectDisposedException21392786.h"
#include "mscorlib_System_Security_Cryptography_PaddingMode2546806710.h"
#include "System_Core_System_Action1264377477.h"
#include "mscorlib_System_AsyncCallback3962456242.h"
#include "System_Core_System_IO_HandleInheritability208753816.h"
#include "System_Core_System_IO_Pipes_NamedPipeServerStream125618231.h"
#include "System_Core_System_IO_Pipes_PipeDirection2244332536.h"
#include "System_Core_System_IO_Pipes_PipeTransmissionMode3285869980.h"
#include "System_Core_System_IO_Pipes_PipeOptions3458221681.h"
#include "System_Core_System_IO_Pipes_PipeSecurity3265965420.h"
#include "System_Core_System_IO_Pipes_PipeAccessRights4039352439.h"
#include "System_Core_System_IO_Pipes_PipeStream871053121.h"
#include "mscorlib_System_Exception1436737249.h"
#include "System_Core_System_IO_Pipes_Win32NamedPipeServer774293249.h"
#include "System_Core_System_IO_Pipes_UnixNamedPipeServer2839746932.h"
#include "mscorlib_System_IO_Stream1273022909.h"
#include "mscorlib_System_IO_FileAccess1659085276.h"
#include "mscorlib_System_InvalidOperationException56020091.h"
#include "mscorlib_System_IO_FileStream4292183065.h"
#include "mscorlib_System_NotSupportedException1314879016.h"
#include "mscorlib_System_Int643736567304.h"
#include "mscorlib_System_IO_SeekOrigin1441174344.h"
#include "System_Core_System_Func_4_gen752956844.h"
#include "System_Core_System_Action_3_gen1284011258.h"
#include "System_Core_System_IO_Pipes_SecurityAttributesHack2445122734.h"
#include "System_Core_System_IO_Pipes_UnixNamedPipe2806256776.h"
#include "Mono_Posix_Mono_Unix_Native_FilePermissions1971995193.h"
#include "mscorlib_System_UInt644134040092.h"
#include "mscorlib_System_IO_IOException4088381929.h"
#include "mscorlib_System_IO_FileMode1183438340.h"
#include "mscorlib_System_IO_FileShare3553318550.h"
#include "System_Core_System_IO_Pipes_Win32Marshal3620203595.h"
#include "mscorlib_System_PlatformID897822290.h"
#include "mscorlib_System_OperatingSystem3730783609.h"
#include "mscorlib_System_UInt322560061978.h"
#include "System_Core_System_IO_Pipes_Win32NamedPipe1933459707.h"
#include "System_System_ComponentModel_Win32Exception3234146298.h"
#include "System_Core_System_Linq_Check192468399.h"
#include "System_Core_System_Linq_Enumerable538148348.h"
#include "System_Core_System_Linq_Enumerable_Fallback3495999270.h"
#include "System_Core_System_MonoNotSupportedAttribute2563528020.h"
#include "System_Core_System_MonoTODOAttribute4131080581.h"
#include "mscorlib_System_Attribute861562559.h"
#include "System_Core_System_Runtime_CompilerServices_Extens1723066603.h"
#include "System_Core_System_Security_Cryptography_Aes1218282760.h"
#include "mscorlib_System_Security_Cryptography_KeySizes85027896.h"
#include "System_Core_System_Security_Cryptography_AesCryptoS345478893.h"
#include "System_Core_System_Security_Cryptography_AesTransf2957123611.h"
#include "System_Core_System_Security_Cryptography_AesManage1129950597.h"
#include "mscorlib_System_RuntimeFieldHandle1871169219.h"
#include "System_Core_System_Threading_LockRecursionExceptio2413788283.h"
#include "mscorlib_System_Runtime_Serialization_Serialization950877179.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon3711869237.h"
#include "System_Core_System_Threading_ReaderWriterLockSlim1938317233.h"
#include "System_Core_System_Threading_ReaderWriterLockSlim_2068764019.h"
#include "mscorlib_System_Threading_EventWaitHandle777845177.h"
#include "mscorlib_System_Threading_Thread2300836069.h"
#include "mscorlib_System_Threading_SynchronizationLockExcept841761767.h"
#include "mscorlib_System_ApplicationException2339761290.h"
#include "mscorlib_System_Threading_AutoResetEvent1333520283.h"
#include "mscorlib_System_Threading_ManualResetEvent451242010.h"
#include "mscorlib_System_Threading_WaitHandle1743403487.h"

// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Object
struct Il2CppObject;
// Microsoft.Win32.SafeHandles.SafePipeHandle
struct SafePipeHandle_t1989113880;
// Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid
struct SafeHandleZeroOrMinusOneIsInvalid_t1182193648;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t386037858;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// Mono.Security.Cryptography.SymmetricTransform
struct SymmetricTransform_t3802591843;
// System.Security.Cryptography.SymmetricAlgorithm
struct SymmetricAlgorithm_t4254223087;
// System.Array
struct Il2CppArray;
// System.Security.Cryptography.CryptographicException
struct CryptographicException_t248831461;
// System.NotImplementedException
struct NotImplementedException_t3489357830;
// System.ArgumentNullException
struct ArgumentNullException_t1615371798;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_t777629997;
// System.ArgumentException
struct ArgumentException_t132251570;
// System.ObjectDisposedException
struct ObjectDisposedException_t21392786;
// System.Action
struct Action_t1264377477;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.IO.Pipes.NamedPipeServerStream
struct NamedPipeServerStream_t125618231;
// System.IO.Pipes.PipeSecurity
struct PipeSecurity_t3265965420;
// System.IO.Pipes.PipeStream
struct PipeStream_t871053121;
// System.Exception
struct Exception_t1436737249;
// System.IO.Pipes.Win32NamedPipeServer
struct Win32NamedPipeServer_t774293249;
// System.IO.Pipes.UnixNamedPipeServer
struct UnixNamedPipeServer_t2839746932;
// System.IO.Stream
struct Stream_t1273022909;
// System.InvalidOperationException
struct InvalidOperationException_t56020091;
// System.Runtime.InteropServices.SafeHandle
struct SafeHandle_t3273388951;
// System.IO.FileStream
struct FileStream_t4292183065;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// System.Func`4<System.Byte[],System.Int32,System.Int32,System.Int32>
struct Func_4_t752956844;
// System.Func`4<System.Object,System.Int32,System.Int32,System.Int32>
struct Func_4_t1364532589;
// System.Action`3<System.Byte[],System.Int32,System.Int32>
struct Action_3_t1284011258;
// System.Action`3<System.Object,System.Int32,System.Int32>
struct Action_3_t1044701581;
// System.IO.Pipes.UnixNamedPipe
struct UnixNamedPipe_t2806256776;
// System.IO.IOException
struct IOException_t4088381929;
// System.OperatingSystem
struct OperatingSystem_t3730783609;
// System.IO.Pipes.SecurityAttributesHack
struct SecurityAttributesHack_t2445122734;
// System.IO.Pipes.Win32NamedPipe
struct Win32NamedPipe_t1933459707;
// System.ComponentModel.Win32Exception
struct Win32Exception_t3234146298;
// System.MonoNotSupportedAttribute
struct MonoNotSupportedAttribute_t2563528020;
// System.MonoTODOAttribute
struct MonoTODOAttribute_t4131080587;
// System.Attribute
struct Attribute_t861562559;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t1723066603;
// System.Security.Cryptography.Aes
struct Aes_t1218282760;
// System.Security.Cryptography.KeySizes
struct KeySizes_t85027896;
// System.Security.Cryptography.AesCryptoServiceProvider
struct AesCryptoServiceProvider_t345478893;
// System.Security.Cryptography.ICryptoTransform
struct ICryptoTransform_t2733259762;
// System.Security.Cryptography.AesTransform
struct AesTransform_t2957123611;
// System.Security.Cryptography.AesManaged
struct AesManaged_t1129950597;
// System.UInt32[]
struct UInt32U5BU5D_t2770800703;
// System.Threading.LockRecursionException
struct LockRecursionException_t2413788283;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.Threading.ReaderWriterLockSlim
struct ReaderWriterLockSlim_t1938317233;
// System.Threading.ReaderWriterLockSlim/LockDetails
struct LockDetails_t2068764019;
// System.Threading.Thread
struct Thread_t2300836069;
// System.Threading.EventWaitHandle
struct EventWaitHandle_t777845177;
// System.Threading.SynchronizationLockException
struct SynchronizationLockException_t841761767;
// System.ApplicationException
struct ApplicationException_t2339761290;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t1333520283;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t451242010;
// System.Threading.ReaderWriterLockSlim/LockDetails[]
struct LockDetailsU5BU5D_t1708220258;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Locale_GetText_m2427493201_MetadataUsageId;
extern Il2CppClass* Marshal_t1757017490_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t132251570_il2cpp_TypeInfo_var;
extern const uint32_t SafePipeHandle_ReleaseHandle_m3011790388_MetadataUsageId;
extern Il2CppClass* KeyBuilder_t2049230356_il2cpp_TypeInfo_var;
extern const uint32_t KeyBuilder_get_Rng_m3373220441_MetadataUsageId;
extern Il2CppClass* ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var;
extern const uint32_t KeyBuilder_Key_m2503211157_MetadataUsageId;
extern const uint32_t KeyBuilder_IV_m3340234014_MetadataUsageId;
extern Il2CppClass* ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern Il2CppClass* CryptographicException_t248831461_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2387040967;
extern const uint32_t SymmetricTransform__ctor_m2693628991_MetadataUsageId;
extern Il2CppClass* CipherMode_t84635067_il2cpp_TypeInfo_var;
extern Il2CppClass* NotImplementedException_t3489357830_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2609825863;
extern const uint32_t SymmetricTransform_Transform_m1683494363_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3478177746;
extern const uint32_t SymmetricTransform_OFB_m3690147804_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3430552138;
extern const uint32_t SymmetricTransform_CTS_m764800021_MetadataUsageId;
extern Il2CppClass* ArgumentNullException_t1615371798_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3152468735;
extern Il2CppCodeGenString* _stringLiteral2167393519;
extern Il2CppCodeGenString* _stringLiteral3073595182;
extern Il2CppCodeGenString* _stringLiteral438779933;
extern Il2CppCodeGenString* _stringLiteral251636811;
extern const uint32_t SymmetricTransform_CheckInput_m2092289040_MetadataUsageId;
extern Il2CppClass* ObjectDisposedException_t21392786_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral389898510;
extern Il2CppCodeGenString* _stringLiteral2053830539;
extern Il2CppCodeGenString* _stringLiteral1561769044;
extern const uint32_t SymmetricTransform_TransformBlock_m851059707_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3823085299;
extern const uint32_t SymmetricTransform_InternalTransformBlock_m1743612142_MetadataUsageId;
extern const uint32_t SymmetricTransform_Random_m3740038270_MetadataUsageId;
extern Il2CppClass* PaddingMode_t2546806710_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2101785501;
extern Il2CppCodeGenString* _stringLiteral289204851;
extern Il2CppCodeGenString* _stringLiteral4613441;
extern const uint32_t SymmetricTransform_ThrowBadPaddingException_m2898061954_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3246833729;
extern const uint32_t SymmetricTransform_FinalEncrypt_m748885414_MetadataUsageId;
extern const uint32_t SymmetricTransform_FinalDecrypt_m764004682_MetadataUsageId;
extern const uint32_t SymmetricTransform_TransformFinalBlock_m1030888689_MetadataUsageId;
extern Il2CppClass* Win32NamedPipeServer_t774293249_il2cpp_TypeInfo_var;
extern Il2CppClass* UnixNamedPipeServer_t2839746932_il2cpp_TypeInfo_var;
extern Il2CppClass* IPipe_t4211097707_il2cpp_TypeInfo_var;
extern const uint32_t NamedPipeServerStream__ctor_m2421472725_MetadataUsageId;
extern Il2CppClass* INamedPipeServer_t304076007_il2cpp_TypeInfo_var;
extern const uint32_t NamedPipeServerStream_WaitForConnection_m3070979034_MetadataUsageId;
extern Il2CppClass* Stream_t1273022909_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4274914650;
extern const uint32_t PipeStream__ctor_m3222674958_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1997704143;
extern const uint32_t PipeStream_ThrowACLException_m4220209091_MetadataUsageId;
extern const uint32_t PipeStream_ToAccessRights_m3500803338_MetadataUsageId;
extern Il2CppClass* InvalidOperationException_t56020091_il2cpp_TypeInfo_var;
extern Il2CppClass* FileStream_t4292183065_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral508036669;
extern const uint32_t PipeStream_get_Stream_m2042017473_MetadataUsageId;
extern Il2CppClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1491677019;
extern const uint32_t PipeStream_CheckReadOperations_m2917579478_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1061946625;
extern Il2CppCodeGenString* _stringLiteral2707246090;
extern const uint32_t PipeStream_CheckWriteOperations_m2156193142_MetadataUsageId;
extern const uint32_t PipeStream_get_Length_m2006706043_MetadataUsageId;
extern const uint32_t PipeStream_set_Position_m2639358827_MetadataUsageId;
extern const uint32_t PipeStream_SetLength_m1130662416_MetadataUsageId;
extern const uint32_t PipeStream_Seek_m3944388383_MetadataUsageId;
extern Il2CppClass* Func_4_t752956844_il2cpp_TypeInfo_var;
extern const MethodInfo* Func_4__ctor_m2755641909_MethodInfo_var;
extern const MethodInfo* Func_4_BeginInvoke_m1913839697_MethodInfo_var;
extern const uint32_t PipeStream_BeginRead_m1326353978_MetadataUsageId;
extern Il2CppClass* Action_3_t1284011258_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_3__ctor_m3554724437_MethodInfo_var;
extern const MethodInfo* Action_3_BeginInvoke_m392137494_MethodInfo_var;
extern const uint32_t PipeStream_BeginWrite_m1039560251_MetadataUsageId;
extern const MethodInfo* Func_4_EndInvoke_m3367563957_MethodInfo_var;
extern const uint32_t PipeStream_EndRead_m2516431970_MetadataUsageId;
extern const MethodInfo* Action_3_EndInvoke_m605455061_MethodInfo_var;
extern const uint32_t PipeStream_EndWrite_m1919876269_MetadataUsageId;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t SecurityAttributesHack__ctor_m2908506643_MetadataUsageId;
extern Il2CppClass* Syscall_t2623000133_il2cpp_TypeInfo_var;
extern Il2CppClass* IOException_t4088381929_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3576745504;
extern const uint32_t UnixNamedPipe_EnsureTargetFile_m962365236_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral312402182;
extern Il2CppCodeGenString* _stringLiteral2189701218;
extern Il2CppCodeGenString* _stringLiteral4120285786;
extern const uint32_t UnixNamedPipe_ValidateOptions_m3897423952_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3450845134;
extern Il2CppCodeGenString* _stringLiteral3452614606;
extern Il2CppCodeGenString* _stringLiteral3452614601;
extern Il2CppCodeGenString* _stringLiteral327211410;
extern const uint32_t UnixNamedPipe_RightsToAccess_m3895475835_MetadataUsageId;
extern const uint32_t UnixNamedPipe_RightsToFileAccess_m3755372448_MetadataUsageId;
extern Il2CppClass* Path_t1605229823_il2cpp_TypeInfo_var;
extern Il2CppClass* SafePipeHandle_t1989113880_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral641456133;
extern const uint32_t UnixNamedPipeServer__ctor_m1228519622_MetadataUsageId;
struct SecurityAttributesHack_t2445122734_marshaled_pinvoke;
struct SecurityAttributesHack_t2445122734;;
struct SecurityAttributesHack_t2445122734_marshaled_pinvoke;;
extern Il2CppClass* Win32Exception_t3234146298_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1672346004;
extern const uint32_t Win32NamedPipeServer__ctor_m442731582_MetadataUsageId;
extern const uint32_t Win32NamedPipeServer_WaitForConnection_m2578077009_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4294193667;
extern const uint32_t Check_Source_m4098695967_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3941128596;
extern const uint32_t Check_SourceAndPredicate_m2332465641_MetadataUsageId;
extern Il2CppClass* KeySizesU5BU5D_t722666473_il2cpp_TypeInfo_var;
extern Il2CppClass* KeySizes_t85027896_il2cpp_TypeInfo_var;
extern const uint32_t Aes__ctor_m178909601_MetadataUsageId;
extern Il2CppClass* AesTransform_t2957123611_il2cpp_TypeInfo_var;
extern const uint32_t AesCryptoServiceProvider_CreateDecryptor_m1328793350_MetadataUsageId;
extern const uint32_t AesCryptoServiceProvider_CreateEncryptor_m1407541527_MetadataUsageId;
extern const uint32_t AesManaged_CreateDecryptor_m692040246_MetadataUsageId;
extern const uint32_t AesManaged_CreateEncryptor_m2294080233_MetadataUsageId;
extern Il2CppClass* UInt32U5BU5D_t2770800703_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2153550409;
extern Il2CppCodeGenString* _stringLiteral2136391555;
extern Il2CppCodeGenString* _stringLiteral2585275424;
extern const uint32_t AesTransform__ctor_m3143546745_MetadataUsageId;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3057255366____U24U24fieldU2D1_1_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3057255366____U24U24fieldU2D2_2_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3057255366____U24U24fieldU2D3_3_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3057255366____U24U24fieldU2D4_4_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3057255366____U24U24fieldU2D5_5_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3057255366____U24U24fieldU2D6_6_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3057255366____U24U24fieldU2D7_7_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3057255366____U24U24fieldU2D8_8_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3057255366____U24U24fieldU2D9_9_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3057255366____U24U24fieldU2D10_10_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3057255366____U24U24fieldU2D11_11_FieldInfo_var;
extern const uint32_t AesTransform__cctor_m2567644034_MetadataUsageId;
extern const uint32_t AesTransform_SubByte_m3350159546_MetadataUsageId;
extern const uint32_t AesTransform_Encrypt128_m424393011_MetadataUsageId;
extern const uint32_t AesTransform_Decrypt128_m3018534522_MetadataUsageId;
extern Il2CppClass* LockDetailsU5BU5D_t1708220258_il2cpp_TypeInfo_var;
extern const uint32_t ReaderWriterLockSlim__ctor_m557461640_MetadataUsageId;
extern Il2CppClass* ReaderWriterLockSlim_t1938317233_il2cpp_TypeInfo_var;
extern const uint32_t ReaderWriterLockSlim__cctor_m2534870346_MetadataUsageId;
extern Il2CppClass* Thread_t2300836069_il2cpp_TypeInfo_var;
extern Il2CppClass* LockRecursionException_t2413788283_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3648076086;
extern Il2CppCodeGenString* _stringLiteral4187398550;
extern Il2CppCodeGenString* _stringLiteral2920290401;
extern const uint32_t ReaderWriterLockSlim_TryEnterReadLock_m580519220_MetadataUsageId;
extern Il2CppClass* SynchronizationLockException_t841761767_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4023828656;
extern const uint32_t ReaderWriterLockSlim_ExitReadLock_m336606214_MetadataUsageId;
extern Il2CppClass* ApplicationException_t2339761290_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258496512;
extern Il2CppCodeGenString* _stringLiteral3388452033;
extern const uint32_t ReaderWriterLockSlim_TryEnterWriteLock_m4237072678_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral990473242;
extern const uint32_t ReaderWriterLockSlim_ExitWriteLock_m2211074167_MetadataUsageId;
extern const uint32_t ReaderWriterLockSlim_TryEnterUpgradeableReadLock_m1427746323_MetadataUsageId;
extern const uint32_t ReaderWriterLockSlim_get_RecursiveUpgradeCount_m1354455429_MetadataUsageId;
extern const uint32_t ReaderWriterLockSlim_get_RecursiveWriteCount_m3156784678_MetadataUsageId;
extern const uint32_t ReaderWriterLockSlim_EnterMyLockSpin_m1525802727_MetadataUsageId;
extern Il2CppClass* AutoResetEvent_t1333520283_il2cpp_TypeInfo_var;
extern Il2CppClass* ManualResetEvent_t451242010_il2cpp_TypeInfo_var;
extern const uint32_t ReaderWriterLockSlim_LazyCreateEvent_m3565271370_MetadataUsageId;
extern Il2CppClass* LockDetails_t2068764019_il2cpp_TypeInfo_var;
extern const MethodInfo* Array_Resize_TisLockDetails_t2068764019_m2614325894_MethodInfo_var;
extern const uint32_t ReaderWriterLockSlim_GetReadLockDetails_m468803701_MetadataUsageId;

// System.Object[]
struct ObjectU5BU5D_t2843939325  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Byte[]
struct ByteU5BU5D_t4116647657  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Security.Cryptography.KeySizes[]
struct KeySizesU5BU5D_t722666473  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeySizes_t85027896 * m_Items[1];

public:
	inline KeySizes_t85027896 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeySizes_t85027896 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeySizes_t85027896 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline KeySizes_t85027896 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeySizes_t85027896 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeySizes_t85027896 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.UInt32[]
struct UInt32U5BU5D_t2770800703  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint32_t m_Items[1];

public:
	inline uint32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Threading.ReaderWriterLockSlim/LockDetails[]
struct LockDetailsU5BU5D_t1708220258  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LockDetails_t2068764019 * m_Items[1];

public:
	inline LockDetails_t2068764019 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline LockDetails_t2068764019 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, LockDetails_t2068764019 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline LockDetails_t2068764019 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline LockDetails_t2068764019 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, LockDetails_t2068764019 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};

extern "C" void SecurityAttributesHack_t2445122734_marshal_pinvoke(const SecurityAttributesHack_t2445122734& unmarshaled, SecurityAttributesHack_t2445122734_marshaled_pinvoke& marshaled);
extern "C" void SecurityAttributesHack_t2445122734_marshal_pinvoke_back(const SecurityAttributesHack_t2445122734_marshaled_pinvoke& marshaled, SecurityAttributesHack_t2445122734& unmarshaled);
extern "C" void SecurityAttributesHack_t2445122734_marshal_pinvoke_cleanup(SecurityAttributesHack_t2445122734_marshaled_pinvoke& marshaled);

// System.Void System.Func`4<System.Object,System.Int32,System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_4__ctor_m1729090094_gshared (Func_4_t1364532589 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method);
// System.IAsyncResult System.Func`4<System.Object,System.Int32,System.Int32,System.Int32>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_4_BeginInvoke_m4056288775_gshared (Func_4_t1364532589 * __this, Il2CppObject * p0, int32_t p1, int32_t p2, AsyncCallback_t3962456242 * p3, Il2CppObject * p4, const MethodInfo* method);
// System.Void System.Action`3<System.Object,System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m2926296862_gshared (Action_3_t1044701581 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method);
// System.IAsyncResult System.Action`3<System.Object,System.Int32,System.Int32>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_3_BeginInvoke_m3106524866_gshared (Action_3_t1044701581 * __this, Il2CppObject * p0, int32_t p1, int32_t p2, AsyncCallback_t3962456242 * p3, Il2CppObject * p4, const MethodInfo* method);
// TResult System.Func`4<System.Object,System.Int32,System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Func_4_EndInvoke_m3643279516_gshared (Func_4_t1364532589 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Action`3<System.Object,System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_3_EndInvoke_m3092081663_gshared (Action_3_t1044701581 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Array::Resize<System.Object>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisIl2CppObject_m2104685202_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t2843939325** p0, int32_t p1, const MethodInfo* method);

// System.String System.String::Format(System.String,System.Object[])
extern "C"  String_t* String_Format_m630303134 (Il2CppObject * __this /* static, unused */, String_t* p0, ObjectU5BU5D_t2843939325* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid::.ctor(System.Boolean)
extern "C"  void SafeHandleZeroOrMinusOneIsInvalid__ctor_m2667299826 (SafeHandleZeroOrMinusOneIsInvalid_t1182193648 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::FreeHGlobal(System.IntPtr)
extern "C"  void Marshal_FreeHGlobal_m1757369653 (Il2CppObject * __this /* static, unused */, IntPtr_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RandomNumberGenerator System.Security.Cryptography.RandomNumberGenerator::Create()
extern "C"  RandomNumberGenerator_t386037858 * RandomNumberGenerator_Create_m4162970280 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RandomNumberGenerator Mono.Security.Cryptography.KeyBuilder::get_Rng()
extern "C"  RandomNumberGenerator_t386037858 * KeyBuilder_get_Rng_m3373220441 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.KeyBuilder::IV(System.Int32)
extern "C"  ByteU5BU5D_t4116647657* KeyBuilder_IV_m3340234014 (Il2CppObject * __this /* static, unused */, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array::Clone()
extern "C"  Il2CppObject * Array_Clone_m2672907798 (Il2CppArray * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Locale::GetText(System.String,System.Object[])
extern "C"  String_t* Locale_GetText_m2427493201 (Il2CppObject * __this /* static, unused */, String_t* ___fmt0, ObjectU5BU5D_t2843939325* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CryptographicException::.ctor(System.String)
extern "C"  void CryptographicException__ctor_m503735289 (CryptographicException_t248831461 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Min(System.Int32,System.Int32)
extern "C"  int32_t Math_Min_m3468062251 (Il2CppObject * __this /* static, unused */, int32_t p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Buffer::BlockCopy(System.Array,System.Int32,System.Array,System.Int32,System.Int32)
extern "C"  void Buffer_BlockCopy_m2884209081 (Il2CppObject * __this /* static, unused */, Il2CppArray * p0, int32_t p1, Il2CppArray * p2, int32_t p3, int32_t p4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::SuppressFinalize(System.Object)
extern "C"  void GC_SuppressFinalize_m1177400158 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::Finalize()
extern "C"  void Object_Finalize_m3076187857 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::Clear(System.Array,System.Int32,System.Int32)
extern "C"  void Array_Clear_m2231608178 (Il2CppObject * __this /* static, unused */, Il2CppArray * p0, int32_t p1, int32_t p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m3937257545 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotImplementedException::.ctor(System.String)
extern "C"  void NotImplementedException__ctor_m3095902440 (NotImplementedException_t3489357830 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C"  void ArgumentNullException__ctor_m1170824041 (ArgumentNullException_t1615371798 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String,System.String)
extern "C"  void ArgumentOutOfRangeException__ctor_m282481429 (ArgumentOutOfRangeException_t777629997 * __this, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Locale::GetText(System.String)
extern "C"  String_t* Locale_GetText_m1626635120 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String,System.String)
extern "C"  void ArgumentException__ctor_m1216717135 (ArgumentException_t132251570 * __this, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ObjectDisposedException::.ctor(System.String)
extern "C"  void ObjectDisposedException__ctor_m3603759869 (ObjectDisposedException_t21392786 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.SymmetricTransform::CheckInput(System.Byte[],System.Int32,System.Int32)
extern "C"  void SymmetricTransform_CheckInput_m2092289040 (SymmetricTransform_t3802591843 * __this, ByteU5BU5D_t4116647657* ___inputBuffer0, int32_t ___inputOffset1, int32_t ___inputCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CryptographicException::.ctor(System.String,System.String)
extern "C"  void CryptographicException__ctor_m3803155940 (CryptographicException_t248831461 * __this, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Cryptography.SymmetricTransform::get_KeepLastBlock()
extern "C"  bool SymmetricTransform_get_KeepLastBlock_m2492071306 (SymmetricTransform_t3802591843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.Cryptography.SymmetricTransform::InternalTransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern "C"  int32_t SymmetricTransform_InternalTransformBlock_m1743612142 (SymmetricTransform_t3802591843 * __this, ByteU5BU5D_t4116647657* ___inputBuffer0, int32_t ___inputOffset1, int32_t ___inputCount2, ByteU5BU5D_t4116647657* ___outputBuffer3, int32_t ___outputOffset4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object)
extern "C"  String_t* String_Format_m2844511972 (Il2CppObject * __this /* static, unused */, String_t* p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.SymmetricTransform::Random(System.Byte[],System.Int32,System.Int32)
extern "C"  void SymmetricTransform_Random_m3740038270 (SymmetricTransform_t3802591843 * __this, ByteU5BU5D_t4116647657* ___buffer0, int32_t ___start1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.SymmetricTransform::ThrowBadPaddingException(System.Security.Cryptography.PaddingMode,System.Int32,System.Int32)
extern "C"  void SymmetricTransform_ThrowBadPaddingException_m2898061954 (SymmetricTransform_t3802591843 * __this, int32_t ___padding0, int32_t ___length1, int32_t ___position2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.SymmetricTransform::FinalEncrypt(System.Byte[],System.Int32,System.Int32)
extern "C"  ByteU5BU5D_t4116647657* SymmetricTransform_FinalEncrypt_m748885414 (SymmetricTransform_t3802591843 * __this, ByteU5BU5D_t4116647657* ___inputBuffer0, int32_t ___inputOffset1, int32_t ___inputCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.SymmetricTransform::FinalDecrypt(System.Byte[],System.Int32,System.Int32)
extern "C"  ByteU5BU5D_t4116647657* SymmetricTransform_FinalDecrypt_m764004682 (SymmetricTransform_t3802591843 * __this, ByteU5BU5D_t4116647657* ___inputBuffer0, int32_t ___inputOffset1, int32_t ___inputCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action::Invoke()
extern "C"  void Action_Invoke_m937035532 (Action_t1264377477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Pipes.NamedPipeServerStream::.ctor(System.String,System.IO.Pipes.PipeDirection,System.Int32)
extern "C"  void NamedPipeServerStream__ctor_m3566243057 (NamedPipeServerStream_t125618231 * __this, String_t* ___pipeName0, int32_t ___direction1, int32_t ___maxNumberOfServerInstances2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Pipes.NamedPipeServerStream::.ctor(System.String,System.IO.Pipes.PipeDirection,System.Int32,System.IO.Pipes.PipeTransmissionMode)
extern "C"  void NamedPipeServerStream__ctor_m2947084898 (NamedPipeServerStream_t125618231 * __this, String_t* ___pipeName0, int32_t ___direction1, int32_t ___maxNumberOfServerInstances2, int32_t ___transmissionMode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Pipes.NamedPipeServerStream::.ctor(System.String,System.IO.Pipes.PipeDirection,System.Int32,System.IO.Pipes.PipeTransmissionMode,System.IO.Pipes.PipeOptions)
extern "C"  void NamedPipeServerStream__ctor_m1922895012 (NamedPipeServerStream_t125618231 * __this, String_t* ___pipeName0, int32_t ___direction1, int32_t ___maxNumberOfServerInstances2, int32_t ___transmissionMode3, int32_t ___options4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Pipes.NamedPipeServerStream::.ctor(System.String,System.IO.Pipes.PipeDirection,System.Int32,System.IO.Pipes.PipeTransmissionMode,System.IO.Pipes.PipeOptions,System.Int32,System.Int32)
extern "C"  void NamedPipeServerStream__ctor_m3695614284 (NamedPipeServerStream_t125618231 * __this, String_t* ___pipeName0, int32_t ___direction1, int32_t ___maxNumberOfServerInstances2, int32_t ___transmissionMode3, int32_t ___options4, int32_t ___inBufferSize5, int32_t ___outBufferSize6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Pipes.NamedPipeServerStream::.ctor(System.String,System.IO.Pipes.PipeDirection,System.Int32,System.IO.Pipes.PipeTransmissionMode,System.IO.Pipes.PipeOptions,System.Int32,System.Int32,System.IO.Pipes.PipeSecurity)
extern "C"  void NamedPipeServerStream__ctor_m2847058098 (NamedPipeServerStream_t125618231 * __this, String_t* ___pipeName0, int32_t ___direction1, int32_t ___maxNumberOfServerInstances2, int32_t ___transmissionMode3, int32_t ___options4, int32_t ___inBufferSize5, int32_t ___outBufferSize6, PipeSecurity_t3265965420 * ___pipeSecurity7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Pipes.NamedPipeServerStream::.ctor(System.String,System.IO.Pipes.PipeDirection,System.Int32,System.IO.Pipes.PipeTransmissionMode,System.IO.Pipes.PipeOptions,System.Int32,System.Int32,System.IO.Pipes.PipeSecurity,System.IO.HandleInheritability)
extern "C"  void NamedPipeServerStream__ctor_m1022744216 (NamedPipeServerStream_t125618231 * __this, String_t* ___pipeName0, int32_t ___direction1, int32_t ___maxNumberOfServerInstances2, int32_t ___transmissionMode3, int32_t ___options4, int32_t ___inBufferSize5, int32_t ___outBufferSize6, PipeSecurity_t3265965420 * ___pipeSecurity7, int32_t ___inheritability8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Pipes.NamedPipeServerStream::.ctor(System.String,System.IO.Pipes.PipeDirection,System.Int32,System.IO.Pipes.PipeTransmissionMode,System.IO.Pipes.PipeOptions,System.Int32,System.Int32,System.IO.Pipes.PipeSecurity,System.IO.HandleInheritability,System.IO.Pipes.PipeAccessRights)
extern "C"  void NamedPipeServerStream__ctor_m2421472725 (NamedPipeServerStream_t125618231 * __this, String_t* ___pipeName0, int32_t ___direction1, int32_t ___maxNumberOfServerInstances2, int32_t ___transmissionMode3, int32_t ___options4, int32_t ___inBufferSize5, int32_t ___outBufferSize6, PipeSecurity_t3265965420 * ___pipeSecurity7, int32_t ___inheritability8, int32_t ___additionalAccessRights9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Pipes.PipeStream::.ctor(System.IO.Pipes.PipeDirection,System.IO.Pipes.PipeTransmissionMode,System.Int32)
extern "C"  void PipeStream__ctor_m3222674958 (PipeStream_t871053121 * __this, int32_t ___direction0, int32_t ___transmissionMode1, int32_t ___outBufferSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.IO.Pipes.PipeStream::ThrowACLException()
extern "C"  Exception_t1436737249 * PipeStream_ThrowACLException_m4220209091 (PipeStream_t871053121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Pipes.PipeAccessRights System.IO.Pipes.PipeStream::ToAccessRights(System.IO.Pipes.PipeDirection)
extern "C"  int32_t PipeStream_ToAccessRights_m3500803338 (Il2CppObject * __this /* static, unused */, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.Pipes.PipeStream::get_IsWindows()
extern "C"  bool PipeStream_get_IsWindows_m2836627059 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Pipes.Win32NamedPipeServer::.ctor(System.IO.Pipes.NamedPipeServerStream,System.String,System.Int32,System.IO.Pipes.PipeTransmissionMode,System.IO.Pipes.PipeAccessRights,System.IO.Pipes.PipeOptions,System.Int32,System.Int32,System.IO.HandleInheritability)
extern "C"  void Win32NamedPipeServer__ctor_m442731582 (Win32NamedPipeServer_t774293249 * __this, NamedPipeServerStream_t125618231 * ___owner0, String_t* ___pipeName1, int32_t ___maxNumberOfServerInstances2, int32_t ___transmissionMode3, int32_t ___rights4, int32_t ___options5, int32_t ___inBufferSize6, int32_t ___outBufferSize7, int32_t ___inheritability8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Pipes.UnixNamedPipeServer::.ctor(System.IO.Pipes.NamedPipeServerStream,System.String,System.Int32,System.IO.Pipes.PipeTransmissionMode,System.IO.Pipes.PipeAccessRights,System.IO.Pipes.PipeOptions,System.Int32,System.Int32,System.IO.HandleInheritability)
extern "C"  void UnixNamedPipeServer__ctor_m1228519622 (UnixNamedPipeServer_t2839746932 * __this, NamedPipeServerStream_t125618231 * ___owner0, String_t* ___pipeName1, int32_t ___maxNumberOfServerInstances2, int32_t ___transmissionMode3, int32_t ___rights4, int32_t ___options5, int32_t ___inBufferSize6, int32_t ___outBufferSize7, int32_t ___inheritability8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Pipes.PipeStream::InitializeHandle(Microsoft.Win32.SafeHandles.SafePipeHandle,System.Boolean,System.Boolean)
extern "C"  void PipeStream_InitializeHandle_m2111621070 (PipeStream_t871053121 * __this, SafePipeHandle_t1989113880 * ___handle0, bool ___isExposed1, bool ___isAsync2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Pipes.PipeStream::set_IsConnected(System.Boolean)
extern "C"  void PipeStream_set_IsConnected_m1407048896 (PipeStream_t871053121 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Stream::.ctor()
extern "C"  void Stream__ctor_m3881936881 (Stream_t1273022909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String)
extern "C"  void ArgumentOutOfRangeException__ctor_m3628145864 (ArgumentOutOfRangeException_t777629997 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.Pipes.Win32Marshal::get_IsWindows()
extern "C"  bool Win32Marshal_get_IsWindows_m4212098367 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentOutOfRangeException::.ctor()
extern "C"  void ArgumentOutOfRangeException__ctor_m2047740448 (ArgumentOutOfRangeException_t777629997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.Pipes.PipeStream::get_IsConnected()
extern "C"  bool PipeStream_get_IsConnected_m1046775879 (PipeStream_t871053121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.InvalidOperationException::.ctor(System.String)
extern "C"  void InvalidOperationException__ctor_m237278729 (InvalidOperationException_t56020091 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.SafeHandle::DangerousGetHandle()
extern "C"  IntPtr_t SafeHandle_DangerousGetHandle_m3697436134 (SafeHandle_t3273388951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.Pipes.PipeStream::get_IsAsync()
extern "C"  bool PipeStream_get_IsAsync_m1359144229 (PipeStream_t871053121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileStream::.ctor(System.IntPtr,System.IO.FileAccess,System.Boolean,System.Int32,System.Boolean)
extern "C"  void FileStream__ctor_m637495152 (FileStream_t4292183065 * __this, IntPtr_t p0, int32_t p1, bool p2, int32_t p3, bool p4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor(System.String)
extern "C"  void NotSupportedException__ctor_m2494070935 (NotSupportedException_t1314879016 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Pipes.PipeStream::set_IsHandleExposed(System.Boolean)
extern "C"  void PipeStream_set_IsHandleExposed_m4119048121 (PipeStream_t871053121 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Pipes.PipeStream::set_IsAsync(System.Boolean)
extern "C"  void PipeStream_set_IsAsync_m3945863678 (PipeStream_t871053121 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.SafeHandle::Dispose()
extern "C"  void SafeHandle_Dispose_m817995135 (SafeHandle_t3273388951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m2730133172 (NotSupportedException_t1314879016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Pipes.PipeStream::CheckReadOperations()
extern "C"  void PipeStream_CheckReadOperations_m2917579478 (PipeStream_t871053121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.IO.Pipes.PipeStream::get_Stream()
extern "C"  Stream_t1273022909 * PipeStream_get_Stream_m2042017473 (PipeStream_t871053121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Pipes.PipeStream::CheckWriteOperations()
extern "C"  void PipeStream_CheckWriteOperations_m2156193142 (PipeStream_t871053121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Func`4<System.Byte[],System.Int32,System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
#define Func_4__ctor_m2755641909(__this, p0, p1, method) ((  void (*) (Func_4_t752956844 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_4__ctor_m1729090094_gshared)(__this, p0, p1, method)
// System.IAsyncResult System.Func`4<System.Byte[],System.Int32,System.Int32,System.Int32>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
#define Func_4_BeginInvoke_m1913839697(__this, p0, p1, p2, p3, p4, method) ((  Il2CppObject * (*) (Func_4_t752956844 *, ByteU5BU5D_t4116647657*, int32_t, int32_t, AsyncCallback_t3962456242 *, Il2CppObject *, const MethodInfo*))Func_4_BeginInvoke_m4056288775_gshared)(__this, p0, p1, p2, p3, p4, method)
// System.Void System.Action`3<System.Byte[],System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
#define Action_3__ctor_m3554724437(__this, p0, p1, method) ((  void (*) (Action_3_t1284011258 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_3__ctor_m2926296862_gshared)(__this, p0, p1, method)
// System.IAsyncResult System.Action`3<System.Byte[],System.Int32,System.Int32>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
#define Action_3_BeginInvoke_m392137494(__this, p0, p1, p2, p3, p4, method) ((  Il2CppObject * (*) (Action_3_t1284011258 *, ByteU5BU5D_t4116647657*, int32_t, int32_t, AsyncCallback_t3962456242 *, Il2CppObject *, const MethodInfo*))Action_3_BeginInvoke_m3106524866_gshared)(__this, p0, p1, p2, p3, p4, method)
// TResult System.Func`4<System.Byte[],System.Int32,System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
#define Func_4_EndInvoke_m3367563957(__this, p0, method) ((  int32_t (*) (Func_4_t752956844 *, Il2CppObject *, const MethodInfo*))Func_4_EndInvoke_m3643279516_gshared)(__this, p0, method)
// System.Void System.Action`3<System.Byte[],System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
#define Action_3_EndInvoke_m605455061(__this, p0, method) ((  void (*) (Action_3_t1284011258 *, Il2CppObject *, const MethodInfo*))Action_3_EndInvoke_m3092081663_gshared)(__this, p0, method)
// System.Void System.IO.Pipes.SecurityAttributesHack::.ctor(System.Boolean)
extern "C"  void SecurityAttributesHack__ctor_m2908506643 (SecurityAttributesHack_t2445122734 * __this, bool ___inheritable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.File::Exists(System.String)
extern "C"  bool File_Exists_m3943585060 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Unix.Native.Syscall::mknod(System.String,Mono.Unix.Native.FilePermissions,System.UInt64)
extern "C"  int32_t Syscall_mknod_m3463737514 (Il2CppObject * __this /* static, unused */, String_t* p0, uint32_t p1, uint64_t p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IOException::.ctor(System.String)
extern "C"  void IOException__ctor_m3662782713 (IOException_t4088381929 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Pipes.UnixNamedPipe::.ctor()
extern "C"  void UnixNamedPipe__ctor_m4176079624 (UnixNamedPipe_t2806256776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.Path::Combine(System.String,System.String)
extern "C"  String_t* Path_Combine_m3389272516 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Pipes.UnixNamedPipe::EnsureTargetFile(System.String)
extern "C"  void UnixNamedPipe_EnsureTargetFile_m962365236 (UnixNamedPipe_t2806256776 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.Pipes.UnixNamedPipe::RightsToAccess(System.IO.Pipes.PipeAccessRights)
extern "C"  String_t* UnixNamedPipe_RightsToAccess_m3895475835 (UnixNamedPipe_t2806256776 * __this, int32_t ___rights0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Pipes.UnixNamedPipe::ValidateOptions(System.IO.Pipes.PipeOptions,System.IO.Pipes.PipeTransmissionMode)
extern "C"  void UnixNamedPipe_ValidateOptions_m3897423952 (UnixNamedPipe_t2806256776 * __this, int32_t ___options0, int32_t ___mode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileAccess System.IO.Pipes.UnixNamedPipe::RightsToFileAccess(System.IO.Pipes.PipeAccessRights)
extern "C"  int32_t UnixNamedPipe_RightsToFileAccess_m3755372448 (UnixNamedPipe_t2806256776 * __this, int32_t ___rights0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileStream::.ctor(System.String,System.IO.FileMode,System.IO.FileAccess,System.IO.FileShare)
extern "C"  void FileStream__ctor_m2889718780 (FileStream_t4292183065 * __this, String_t* p0, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.SafeHandles.SafePipeHandle::.ctor(System.IntPtr,System.Boolean)
extern "C"  void SafePipeHandle__ctor_m396738268 (SafePipeHandle_t1989113880 * __this, IntPtr_t ___preexistingHandle0, bool ___ownsHandle1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Pipes.PipeStream::set_Stream(System.IO.Stream)
extern "C"  void PipeStream_set_Stream_m1574685589 (PipeStream_t871053121 * __this, Stream_t1273022909 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.OperatingSystem System.Environment::get_OSVersion()
extern "C"  OperatingSystem_t3730783609 * Environment_get_OSVersion_m961136977 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.PlatformID System.OperatingSystem::get_Platform()
extern "C"  int32_t OperatingSystem_get_Platform_m2793423729 (OperatingSystem_t3730783609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.SafeHandle::DangerousAddRef(System.Boolean&)
extern "C"  void SafeHandle_DangerousAddRef_m614714386 (SafeHandle_t3273388951 * __this, bool* ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.SafeHandle::DangerousRelease()
extern "C"  void SafeHandle_DangerousRelease_m190326290 (SafeHandle_t3273388951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Pipes.Win32NamedPipe::.ctor()
extern "C"  void Win32NamedPipe__ctor_m671208207 (Win32NamedPipe_t1933459707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.IO.Pipes.Win32Marshal::CreateNamedPipe(System.String,System.UInt32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.IO.Pipes.SecurityAttributesHack&,System.IntPtr)
extern "C"  IntPtr_t Win32Marshal_CreateNamedPipe_m1976185420 (Il2CppObject * __this /* static, unused */, String_t* ___name0, uint32_t ___openMode1, int32_t ___pipeMode2, int32_t ___maxInstances3, int32_t ___outBufferSize4, int32_t ___inBufferSize5, int32_t ___defaultTimeout6, SecurityAttributesHack_t2445122734 * ___securityAttributes7, IntPtr_t ___atts8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IntPtr::.ctor(System.Int64)
extern "C"  void IntPtr__ctor_m987476171 (IntPtr_t* __this, int64_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Equality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Equality_m408849716 (Il2CppObject * __this /* static, unused */, IntPtr_t p0, IntPtr_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::GetLastWin32Error()
extern "C"  int32_t Marshal_GetLastWin32Error_m1272610344 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.Win32Exception::.ctor(System.Int32)
extern "C"  void Win32Exception__ctor_m3118723333 (Win32Exception_t3234146298 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.Pipes.Win32Marshal::ConnectNamedPipe(Microsoft.Win32.SafeHandles.SafePipeHandle,System.IntPtr)
extern "C"  bool Win32Marshal_ConnectNamedPipe_m4021705803 (Il2CppObject * __this /* static, unused */, SafePipeHandle_t1989113880 * ___handle0, IntPtr_t ___overlapped1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.MonoTODOAttribute::.ctor(System.String)
extern "C"  void MonoTODOAttribute__ctor_m4112301096 (MonoTODOAttribute_t4131080587 * __this, String_t* ___comment0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Attribute::.ctor()
extern "C"  void Attribute__ctor_m1529526131 (Attribute_t861562559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SymmetricAlgorithm::.ctor()
extern "C"  void SymmetricAlgorithm__ctor_m467277132 (SymmetricAlgorithm_t4254223087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.KeySizes::.ctor(System.Int32,System.Int32,System.Int32)
extern "C"  void KeySizes__ctor_m3113946058 (KeySizes_t85027896 * __this, int32_t p0, int32_t p1, int32_t p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Aes::.ctor()
extern "C"  void Aes__ctor_m178909601 (Aes_t1218282760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.KeyBuilder::Key(System.Int32)
extern "C"  ByteU5BU5D_t4116647657* KeyBuilder_Key_m2503211157 (Il2CppObject * __this /* static, unused */, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.AesTransform::.ctor(System.Security.Cryptography.Aes,System.Boolean,System.Byte[],System.Byte[])
extern "C"  void AesTransform__ctor_m3143546745 (AesTransform_t2957123611 * __this, Aes_t1218282760 * ___algo0, bool ___encryption1, ByteU5BU5D_t4116647657* ___key2, ByteU5BU5D_t4116647657* ___iv3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.SymmetricAlgorithm::get_IV()
extern "C"  ByteU5BU5D_t4116647657* SymmetricAlgorithm_get_IV_m1875559108 (SymmetricAlgorithm_t4254223087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SymmetricAlgorithm::set_IV(System.Byte[])
extern "C"  void SymmetricAlgorithm_set_IV_m3196220308 (SymmetricAlgorithm_t4254223087 * __this, ByteU5BU5D_t4116647657* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.SymmetricAlgorithm::get_Key()
extern "C"  ByteU5BU5D_t4116647657* SymmetricAlgorithm_get_Key_m3241860519 (SymmetricAlgorithm_t4254223087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SymmetricAlgorithm::set_Key(System.Byte[])
extern "C"  void SymmetricAlgorithm_set_Key_m1775642191 (SymmetricAlgorithm_t4254223087 * __this, ByteU5BU5D_t4116647657* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::get_KeySize()
extern "C"  int32_t SymmetricAlgorithm_get_KeySize_m4185004893 (SymmetricAlgorithm_t4254223087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SymmetricAlgorithm::set_KeySize(System.Int32)
extern "C"  void SymmetricAlgorithm_set_KeySize_m3805756466 (SymmetricAlgorithm_t4254223087 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_Key()
extern "C"  ByteU5BU5D_t4116647657* AesCryptoServiceProvider_get_Key_m249672229 (AesCryptoServiceProvider_t345478893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_IV()
extern "C"  ByteU5BU5D_t4116647657* AesCryptoServiceProvider_get_IV_m2323542127 (AesCryptoServiceProvider_t345478893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern "C"  Il2CppObject * AesCryptoServiceProvider_CreateDecryptor_m1328793350 (AesCryptoServiceProvider_t345478893 * __this, ByteU5BU5D_t4116647657* ___rgbKey0, ByteU5BU5D_t4116647657* ___rgbIV1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern "C"  Il2CppObject * AesCryptoServiceProvider_CreateEncryptor_m1407541527 (AesCryptoServiceProvider_t345478893 * __this, ByteU5BU5D_t4116647657* ___rgbKey0, ByteU5BU5D_t4116647657* ___rgbIV1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SymmetricAlgorithm::Dispose(System.Boolean)
extern "C"  void SymmetricAlgorithm_Dispose_m1120980942 (SymmetricAlgorithm_t4254223087 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.AesManaged::get_Key()
extern "C"  ByteU5BU5D_t4116647657* AesManaged_get_Key_m538801386 (AesManaged_t1129950597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.AesManaged::get_IV()
extern "C"  ByteU5BU5D_t4116647657* AesManaged_get_IV_m118095902 (AesManaged_t1129950597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern "C"  Il2CppObject * AesManaged_CreateDecryptor_m692040246 (AesManaged_t1129950597 * __this, ByteU5BU5D_t4116647657* ___rgbKey0, ByteU5BU5D_t4116647657* ___rgbIV1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern "C"  Il2CppObject * AesManaged_CreateEncryptor_m2294080233 (AesManaged_t1129950597 * __this, ByteU5BU5D_t4116647657* ___rgbKey0, ByteU5BU5D_t4116647657* ___rgbIV1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.SymmetricTransform::.ctor(System.Security.Cryptography.SymmetricAlgorithm,System.Boolean,System.Byte[])
extern "C"  void SymmetricTransform__ctor_m2693628991 (SymmetricTransform_t3802591843 * __this, SymmetricAlgorithm_t4254223087 * ___symmAlgo0, bool ___encryption1, ByteU5BU5D_t4116647657* ___rgbIV2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Security.Cryptography.AesTransform::SubByte(System.UInt32)
extern "C"  uint32_t AesTransform_SubByte_m3350159546 (AesTransform_t2957123611 * __this, uint32_t ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
extern "C"  void RuntimeHelpers_InitializeArray_m3117905507 (Il2CppObject * __this /* static, unused */, Il2CppArray * p0, RuntimeFieldHandle_t1871169219  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.AesTransform::Encrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern "C"  void AesTransform_Encrypt128_m424393011 (AesTransform_t2957123611 * __this, ByteU5BU5D_t4116647657* ___indata0, ByteU5BU5D_t4116647657* ___outdata1, UInt32U5BU5D_t2770800703* ___ekey2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.AesTransform::Decrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern "C"  void AesTransform_Decrypt128_m3018534522 (AesTransform_t2957123611 * __this, ByteU5BU5D_t4116647657* ___indata0, ByteU5BU5D_t4116647657* ___outdata1, UInt32U5BU5D_t2770800703* ___ekey2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::.ctor()
extern "C"  void Exception__ctor_m213470898 (Exception_t1436737249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::.ctor(System.String)
extern "C"  void Exception__ctor_m1152696503 (Exception_t1436737249 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Exception__ctor_m2499432361 (Exception_t1436737249 * __this, SerializationInfo_t950877179 * p0, StreamingContext_t3711869237  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Environment::get_ProcessorCount()
extern "C"  int32_t Environment_get_ProcessorCount_m3616251798 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.ReaderWriterLockSlim::TryEnterReadLock(System.Int32)
extern "C"  bool ReaderWriterLockSlim_TryEnterReadLock_m580519220 (ReaderWriterLockSlim_t1938317233 * __this, int32_t ___millisecondsTimeout0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.Thread System.Threading.Thread::get_CurrentThread()
extern "C"  Thread_t2300836069 * Thread_get_CurrentThread_m4142136012 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.LockRecursionException::.ctor(System.String)
extern "C"  void LockRecursionException__ctor_m1065084584 (LockRecursionException_t2413788283 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ReaderWriterLockSlim::EnterMyLock()
extern "C"  void ReaderWriterLockSlim_EnterMyLock_m3975449768 (ReaderWriterLockSlim_t1938317233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Thread::get_ManagedThreadId()
extern "C"  int32_t Thread_get_ManagedThreadId_m1068113671 (Thread_t2300836069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.ReaderWriterLockSlim/LockDetails System.Threading.ReaderWriterLockSlim::GetReadLockDetails(System.Int32,System.Boolean)
extern "C"  LockDetails_t2068764019 * ReaderWriterLockSlim_GetReadLockDetails_m468803701 (ReaderWriterLockSlim_t1938317233 * __this, int32_t ___threadId0, bool ___create1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ReaderWriterLockSlim::ExitMyLock()
extern "C"  void ReaderWriterLockSlim_ExitMyLock_m2519986703 (ReaderWriterLockSlim_t1938317233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ReaderWriterLockSlim::LazyCreateEvent(System.Threading.EventWaitHandle&,System.Boolean)
extern "C"  void ReaderWriterLockSlim_LazyCreateEvent_m3565271370 (ReaderWriterLockSlim_t1938317233 * __this, EventWaitHandle_t777845177 ** ___waitEvent0, bool ___makeAutoResetEvent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.ReaderWriterLockSlim::WaitOnEvent(System.Threading.EventWaitHandle,System.UInt32&,System.Int32)
extern "C"  bool ReaderWriterLockSlim_WaitOnEvent_m2536988665 (ReaderWriterLockSlim_t1938317233 * __this, EventWaitHandle_t777845177 * ___waitEvent0, uint32_t* ___numWaiters1, int32_t ___millisecondsTimeout2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.SynchronizationLockException::.ctor(System.String)
extern "C"  void SynchronizationLockException__ctor_m3407855920 (SynchronizationLockException_t841761767 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ReaderWriterLockSlim::ExitAndWakeUpAppropriateWaiters()
extern "C"  void ReaderWriterLockSlim_ExitAndWakeUpAppropriateWaiters_m3940164111 (ReaderWriterLockSlim_t1938317233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.ReaderWriterLockSlim::TryEnterWriteLock(System.Int32)
extern "C"  bool ReaderWriterLockSlim_TryEnterWriteLock_m4237072678 (ReaderWriterLockSlim_t1938317233 * __this, int32_t ___millisecondsTimeout0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.ReaderWriterLockSlim::get_IsWriteLockHeld()
extern "C"  bool ReaderWriterLockSlim_get_IsWriteLockHeld_m3969852364 (ReaderWriterLockSlim_t1938317233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.LockRecursionException::.ctor()
extern "C"  void LockRecursionException__ctor_m338981005 (LockRecursionException_t2413788283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ApplicationException::.ctor(System.String)
extern "C"  void ApplicationException__ctor_m2517758450 (ApplicationException_t2339761290 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.ReaderWriterLockSlim::TryEnterUpgradeableReadLock(System.Int32)
extern "C"  bool ReaderWriterLockSlim_TryEnterUpgradeableReadLock_m1427746323 (ReaderWriterLockSlim_t1938317233 * __this, int32_t ___millisecondsTimeout0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.ReaderWriterLockSlim::get_IsUpgradeableReadLockHeld()
extern "C"  bool ReaderWriterLockSlim_get_IsUpgradeableReadLockHeld_m1642772518 (ReaderWriterLockSlim_t1938317233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.ReaderWriterLockSlim::get_RecursiveWriteCount()
extern "C"  int32_t ReaderWriterLockSlim_get_RecursiveWriteCount_m3156784678 (ReaderWriterLockSlim_t1938317233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.ReaderWriterLockSlim::get_RecursiveUpgradeCount()
extern "C"  int32_t ReaderWriterLockSlim_get_RecursiveUpgradeCount_m1354455429 (ReaderWriterLockSlim_t1938317233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Interlocked::CompareExchange(System.Int32&,System.Int32,System.Int32)
extern "C"  int32_t Interlocked_CompareExchange_m3023855514 (Il2CppObject * __this /* static, unused */, int32_t* p0, int32_t p1, int32_t p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ReaderWriterLockSlim::EnterMyLockSpin()
extern "C"  void ReaderWriterLockSlim_EnterMyLockSpin_m1525802727 (ReaderWriterLockSlim_t1938317233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::SpinWait(System.Int32)
extern "C"  void Thread_SpinWait_m3968465979 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::Sleep(System.Int32)
extern "C"  void Thread_Sleep_m483098292 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.EventWaitHandle::Set()
extern "C"  bool EventWaitHandle_Set_m2445193251 (EventWaitHandle_t777845177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.AutoResetEvent::.ctor(System.Boolean)
extern "C"  void AutoResetEvent__ctor_m3710433672 (AutoResetEvent_t1333520283 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ManualResetEvent::.ctor(System.Boolean)
extern "C"  void ManualResetEvent__ctor_m4010886457 (ManualResetEvent_t451242010 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.EventWaitHandle::Reset()
extern "C"  bool EventWaitHandle_Reset_m3348053200 (EventWaitHandle_t777845177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::Resize<System.Threading.ReaderWriterLockSlim/LockDetails>(!!0[]&,System.Int32)
#define Array_Resize_TisLockDetails_t2068764019_m2614325894(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, LockDetailsU5BU5D_t1708220258**, int32_t, const MethodInfo*))Array_Resize_TisIl2CppObject_m2104685202_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void System.Threading.ReaderWriterLockSlim/LockDetails::.ctor()
extern "C"  void LockDetails__ctor_m1021033989 (LockDetails_t2068764019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Locale::GetText(System.String)
extern "C"  String_t* Locale_GetText_m1626635120 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___msg0;
		return L_0;
	}
}
// System.String Locale::GetText(System.String,System.Object[])
extern "C"  String_t* Locale_GetText_m2427493201 (Il2CppObject * __this /* static, unused */, String_t* ___fmt0, ObjectU5BU5D_t2843939325* ___args1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Locale_GetText_m2427493201_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___fmt0;
		ObjectU5BU5D_t2843939325* L_1 = ___args1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m630303134(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void Microsoft.Win32.SafeHandles.SafePipeHandle::.ctor(System.IntPtr,System.Boolean)
extern "C"  void SafePipeHandle__ctor_m396738268 (SafePipeHandle_t1989113880 * __this, IntPtr_t ___preexistingHandle0, bool ___ownsHandle1, const MethodInfo* method)
{
	{
		bool L_0 = ___ownsHandle1;
		SafeHandleZeroOrMinusOneIsInvalid__ctor_m2667299826(__this, L_0, /*hidden argument*/NULL);
		IntPtr_t L_1 = ___preexistingHandle0;
		((SafeHandle_t3273388951 *)__this)->set_handle_0(L_1);
		return;
	}
}
// System.Boolean Microsoft.Win32.SafeHandles.SafePipeHandle::ReleaseHandle()
extern "C"  bool SafePipeHandle_ReleaseHandle_m3011790388 (SafePipeHandle_t1989113880 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SafePipeHandle_ReleaseHandle_m3011790388_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ((SafeHandle_t3273388951 *)__this)->get_handle_0();
			IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1757017490_il2cpp_TypeInfo_var);
			Marshal_FreeHGlobal_m1757369653(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = (bool)1;
			goto IL_0024;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0024
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (ArgumentException_t132251570_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0017;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.ArgumentException)
		{
			V_0 = (bool)0;
			goto IL_0024;
		}

IL_001f:
		{
			; // IL_001f: leave IL_0024
		}
	} // end catch (depth: 1)

IL_0024:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Security.Cryptography.RandomNumberGenerator Mono.Security.Cryptography.KeyBuilder::get_Rng()
extern "C"  RandomNumberGenerator_t386037858 * KeyBuilder_get_Rng_m3373220441 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyBuilder_get_Rng_m3373220441_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RandomNumberGenerator_t386037858 * L_0 = ((KeyBuilder_t2049230356_StaticFields*)KeyBuilder_t2049230356_il2cpp_TypeInfo_var->static_fields)->get_rng_0();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		RandomNumberGenerator_t386037858 * L_1 = RandomNumberGenerator_Create_m4162970280(NULL /*static, unused*/, /*hidden argument*/NULL);
		((KeyBuilder_t2049230356_StaticFields*)KeyBuilder_t2049230356_il2cpp_TypeInfo_var->static_fields)->set_rng_0(L_1);
	}

IL_0014:
	{
		RandomNumberGenerator_t386037858 * L_2 = ((KeyBuilder_t2049230356_StaticFields*)KeyBuilder_t2049230356_il2cpp_TypeInfo_var->static_fields)->get_rng_0();
		return L_2;
	}
}
// System.Byte[] Mono.Security.Cryptography.KeyBuilder::Key(System.Int32)
extern "C"  ByteU5BU5D_t4116647657* KeyBuilder_Key_m2503211157 (Il2CppObject * __this /* static, unused */, int32_t ___size0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyBuilder_Key_m2503211157_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t4116647657* V_0 = NULL;
	{
		int32_t L_0 = ___size0;
		V_0 = ((ByteU5BU5D_t4116647657*)SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, (uint32_t)L_0));
		RandomNumberGenerator_t386037858 * L_1 = KeyBuilder_get_Rng_m3373220441(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_2 = V_0;
		NullCheck(L_1);
		VirtActionInvoker1< ByteU5BU5D_t4116647657* >::Invoke(4 /* System.Void System.Security.Cryptography.RandomNumberGenerator::GetBytes(System.Byte[]) */, L_1, L_2);
		ByteU5BU5D_t4116647657* L_3 = V_0;
		return L_3;
	}
}
// System.Byte[] Mono.Security.Cryptography.KeyBuilder::IV(System.Int32)
extern "C"  ByteU5BU5D_t4116647657* KeyBuilder_IV_m3340234014 (Il2CppObject * __this /* static, unused */, int32_t ___size0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyBuilder_IV_m3340234014_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t4116647657* V_0 = NULL;
	{
		int32_t L_0 = ___size0;
		V_0 = ((ByteU5BU5D_t4116647657*)SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, (uint32_t)L_0));
		RandomNumberGenerator_t386037858 * L_1 = KeyBuilder_get_Rng_m3373220441(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_2 = V_0;
		NullCheck(L_1);
		VirtActionInvoker1< ByteU5BU5D_t4116647657* >::Invoke(4 /* System.Void System.Security.Cryptography.RandomNumberGenerator::GetBytes(System.Byte[]) */, L_1, L_2);
		ByteU5BU5D_t4116647657* L_3 = V_0;
		return L_3;
	}
}
// System.Void Mono.Security.Cryptography.SymmetricTransform::.ctor(System.Security.Cryptography.SymmetricAlgorithm,System.Boolean,System.Byte[])
extern "C"  void SymmetricTransform__ctor_m2693628991 (SymmetricTransform_t3802591843 * __this, SymmetricAlgorithm_t4254223087 * ___symmAlgo0, bool ___encryption1, ByteU5BU5D_t4116647657* ___rgbIV2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SymmetricTransform__ctor_m2693628991_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		SymmetricAlgorithm_t4254223087 * L_0 = ___symmAlgo0;
		__this->set_algo_0(L_0);
		bool L_1 = ___encryption1;
		__this->set_encrypt_1(L_1);
		SymmetricAlgorithm_t4254223087 * L_2 = __this->get_algo_0();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Security.Cryptography.SymmetricAlgorithm::get_BlockSize() */, L_2);
		__this->set_BlockSizeByte_2(((int32_t)((int32_t)L_3>>(int32_t)3)));
		ByteU5BU5D_t4116647657* L_4 = ___rgbIV2;
		if (L_4)
		{
			goto IL_003f;
		}
	}
	{
		int32_t L_5 = __this->get_BlockSizeByte_2();
		ByteU5BU5D_t4116647657* L_6 = KeyBuilder_IV_m3340234014(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		___rgbIV2 = L_6;
		goto IL_004c;
	}

IL_003f:
	{
		ByteU5BU5D_t4116647657* L_7 = ___rgbIV2;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_7);
		Il2CppObject * L_8 = Array_Clone_m2672907798((Il2CppArray *)(Il2CppArray *)L_7, /*hidden argument*/NULL);
		___rgbIV2 = ((ByteU5BU5D_t4116647657*)Castclass(L_8, ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var));
	}

IL_004c:
	{
		ByteU5BU5D_t4116647657* L_9 = ___rgbIV2;
		NullCheck(L_9);
		int32_t L_10 = __this->get_BlockSizeByte_2();
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))) >= ((int32_t)L_10)))
		{
			goto IL_008b;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_11 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)2));
		ByteU5BU5D_t4116647657* L_12 = ___rgbIV2;
		NullCheck(L_12);
		int32_t L_13 = (((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length))));
		Il2CppObject * L_14 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_14);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_14);
		ObjectU5BU5D_t2843939325* L_15 = L_11;
		int32_t L_16 = __this->get_BlockSizeByte_2();
		int32_t L_17 = L_16;
		Il2CppObject * L_18 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_17);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_18);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_18);
		String_t* L_19 = Locale_GetText_m2427493201(NULL /*static, unused*/, _stringLiteral2387040967, L_15, /*hidden argument*/NULL);
		V_0 = L_19;
		String_t* L_20 = V_0;
		CryptographicException_t248831461 * L_21 = (CryptographicException_t248831461 *)il2cpp_codegen_object_new(CryptographicException_t248831461_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m503735289(L_21, L_20, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}

IL_008b:
	{
		int32_t L_22 = __this->get_BlockSizeByte_2();
		__this->set_temp_3(((ByteU5BU5D_t4116647657*)SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, (uint32_t)L_22)));
		ByteU5BU5D_t4116647657* L_23 = ___rgbIV2;
		ByteU5BU5D_t4116647657* L_24 = __this->get_temp_3();
		int32_t L_25 = __this->get_BlockSizeByte_2();
		ByteU5BU5D_t4116647657* L_26 = ___rgbIV2;
		NullCheck(L_26);
		int32_t L_27 = Math_Min_m3468062251(NULL /*static, unused*/, L_25, (((int32_t)((int32_t)(((Il2CppArray *)L_26)->max_length)))), /*hidden argument*/NULL);
		Buffer_BlockCopy_m2884209081(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_23, 0, (Il2CppArray *)(Il2CppArray *)L_24, 0, L_27, /*hidden argument*/NULL);
		int32_t L_28 = __this->get_BlockSizeByte_2();
		__this->set_temp2_4(((ByteU5BU5D_t4116647657*)SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, (uint32_t)L_28)));
		SymmetricAlgorithm_t4254223087 * L_29 = __this->get_algo_0();
		NullCheck(L_29);
		int32_t L_30 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Security.Cryptography.SymmetricAlgorithm::get_FeedbackSize() */, L_29);
		__this->set_FeedBackByte_7(((int32_t)((int32_t)L_30>>(int32_t)3)));
		int32_t L_31 = __this->get_FeedBackByte_7();
		if (!L_31)
		{
			goto IL_00fa;
		}
	}
	{
		int32_t L_32 = __this->get_BlockSizeByte_2();
		int32_t L_33 = __this->get_FeedBackByte_7();
		__this->set_FeedBackIter_8(((int32_t)((int32_t)L_32/(int32_t)L_33)));
	}

IL_00fa:
	{
		int32_t L_34 = __this->get_BlockSizeByte_2();
		__this->set_workBuff_5(((ByteU5BU5D_t4116647657*)SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, (uint32_t)L_34)));
		int32_t L_35 = __this->get_BlockSizeByte_2();
		__this->set_workout_6(((ByteU5BU5D_t4116647657*)SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, (uint32_t)L_35)));
		return;
	}
}
// System.Void Mono.Security.Cryptography.SymmetricTransform::System.IDisposable.Dispose()
extern "C"  void SymmetricTransform_System_IDisposable_Dispose_m3657987482 (SymmetricTransform_t3802591843 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker1< bool >::Invoke(11 /* System.Void Mono.Security.Cryptography.SymmetricTransform::Dispose(System.Boolean) */, __this, (bool)1);
		GC_SuppressFinalize_m1177400158(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Security.Cryptography.SymmetricTransform::Finalize()
extern "C"  void SymmetricTransform_Finalize_m4129642865 (SymmetricTransform_t3802591843 * __this, const MethodInfo* method)
{
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		VirtActionInvoker1< bool >::Invoke(11 /* System.Void Mono.Security.Cryptography.SymmetricTransform::Dispose(System.Boolean) */, __this, (bool)0);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m3076187857(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void Mono.Security.Cryptography.SymmetricTransform::Dispose(System.Boolean)
extern "C"  void SymmetricTransform_Dispose_m375394407 (SymmetricTransform_t3802591843 * __this, bool ___disposing0, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_disposed_9();
		if (L_0)
		{
			goto IL_004a;
		}
	}
	{
		bool L_1 = ___disposing0;
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		ByteU5BU5D_t4116647657* L_2 = __this->get_temp_3();
		int32_t L_3 = __this->get_BlockSizeByte_2();
		Array_Clear_m2231608178(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_2, 0, L_3, /*hidden argument*/NULL);
		__this->set_temp_3((ByteU5BU5D_t4116647657*)NULL);
		ByteU5BU5D_t4116647657* L_4 = __this->get_temp2_4();
		int32_t L_5 = __this->get_BlockSizeByte_2();
		Array_Clear_m2231608178(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_4, 0, L_5, /*hidden argument*/NULL);
		__this->set_temp2_4((ByteU5BU5D_t4116647657*)NULL);
	}

IL_0043:
	{
		__this->set_m_disposed_9((bool)1);
	}

IL_004a:
	{
		return;
	}
}
// System.Boolean Mono.Security.Cryptography.SymmetricTransform::get_CanTransformMultipleBlocks()
extern "C"  bool SymmetricTransform_get_CanTransformMultipleBlocks_m2172207851 (SymmetricTransform_t3802591843 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean Mono.Security.Cryptography.SymmetricTransform::get_CanReuseTransform()
extern "C"  bool SymmetricTransform_get_CanReuseTransform_m3495714228 (SymmetricTransform_t3802591843 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Int32 Mono.Security.Cryptography.SymmetricTransform::get_InputBlockSize()
extern "C"  int32_t SymmetricTransform_get_InputBlockSize_m48243441 (SymmetricTransform_t3802591843 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_BlockSizeByte_2();
		return L_0;
	}
}
// System.Int32 Mono.Security.Cryptography.SymmetricTransform::get_OutputBlockSize()
extern "C"  int32_t SymmetricTransform_get_OutputBlockSize_m3395837183 (SymmetricTransform_t3802591843 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_BlockSizeByte_2();
		return L_0;
	}
}
// System.Void Mono.Security.Cryptography.SymmetricTransform::Transform(System.Byte[],System.Byte[])
extern "C"  void SymmetricTransform_Transform_m1683494363 (SymmetricTransform_t3802591843 * __this, ByteU5BU5D_t4116647657* ___input0, ByteU5BU5D_t4116647657* ___output1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SymmetricTransform_Transform_m1683494363_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		SymmetricAlgorithm_t4254223087 * L_0 = __this->get_algo_0();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(16 /* System.Security.Cryptography.CipherMode System.Security.Cryptography.SymmetricAlgorithm::get_Mode() */, L_0);
		V_0 = L_1;
		int32_t L_2 = V_0;
		switch (((int32_t)((int32_t)L_2-(int32_t)1)))
		{
			case 0:
			{
				goto IL_003a;
			}
			case 1:
			{
				goto IL_002d;
			}
			case 2:
			{
				goto IL_0054;
			}
			case 3:
			{
				goto IL_0047;
			}
			case 4:
			{
				goto IL_0061;
			}
		}
	}
	{
		goto IL_006e;
	}

IL_002d:
	{
		ByteU5BU5D_t4116647657* L_3 = ___input0;
		ByteU5BU5D_t4116647657* L_4 = ___output1;
		VirtActionInvoker2< ByteU5BU5D_t4116647657*, ByteU5BU5D_t4116647657* >::Invoke(17 /* System.Void Mono.Security.Cryptography.SymmetricTransform::ECB(System.Byte[],System.Byte[]) */, __this, L_3, L_4);
		goto IL_0093;
	}

IL_003a:
	{
		ByteU5BU5D_t4116647657* L_5 = ___input0;
		ByteU5BU5D_t4116647657* L_6 = ___output1;
		VirtActionInvoker2< ByteU5BU5D_t4116647657*, ByteU5BU5D_t4116647657* >::Invoke(18 /* System.Void Mono.Security.Cryptography.SymmetricTransform::CBC(System.Byte[],System.Byte[]) */, __this, L_5, L_6);
		goto IL_0093;
	}

IL_0047:
	{
		ByteU5BU5D_t4116647657* L_7 = ___input0;
		ByteU5BU5D_t4116647657* L_8 = ___output1;
		VirtActionInvoker2< ByteU5BU5D_t4116647657*, ByteU5BU5D_t4116647657* >::Invoke(19 /* System.Void Mono.Security.Cryptography.SymmetricTransform::CFB(System.Byte[],System.Byte[]) */, __this, L_7, L_8);
		goto IL_0093;
	}

IL_0054:
	{
		ByteU5BU5D_t4116647657* L_9 = ___input0;
		ByteU5BU5D_t4116647657* L_10 = ___output1;
		VirtActionInvoker2< ByteU5BU5D_t4116647657*, ByteU5BU5D_t4116647657* >::Invoke(20 /* System.Void Mono.Security.Cryptography.SymmetricTransform::OFB(System.Byte[],System.Byte[]) */, __this, L_9, L_10);
		goto IL_0093;
	}

IL_0061:
	{
		ByteU5BU5D_t4116647657* L_11 = ___input0;
		ByteU5BU5D_t4116647657* L_12 = ___output1;
		VirtActionInvoker2< ByteU5BU5D_t4116647657*, ByteU5BU5D_t4116647657* >::Invoke(21 /* System.Void Mono.Security.Cryptography.SymmetricTransform::CTS(System.Byte[],System.Byte[]) */, __this, L_11, L_12);
		goto IL_0093;
	}

IL_006e:
	{
		SymmetricAlgorithm_t4254223087 * L_13 = __this->get_algo_0();
		NullCheck(L_13);
		int32_t L_14 = VirtFuncInvoker0< int32_t >::Invoke(16 /* System.Security.Cryptography.CipherMode System.Security.Cryptography.SymmetricAlgorithm::get_Mode() */, L_13);
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(CipherMode_t84635067_il2cpp_TypeInfo_var, &L_15);
		NullCheck((Enum_t4135868527 *)L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t4135868527 *)L_16);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2609825863, L_17, /*hidden argument*/NULL);
		NotImplementedException_t3489357830 * L_19 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3095902440(L_19, L_18, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
	}

IL_0093:
	{
		return;
	}
}
// System.Void Mono.Security.Cryptography.SymmetricTransform::CBC(System.Byte[],System.Byte[])
extern "C"  void SymmetricTransform_CBC_m3648398454 (SymmetricTransform_t3802591843 * __this, ByteU5BU5D_t4116647657* ___input0, ByteU5BU5D_t4116647657* ___output1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		bool L_0 = __this->get_encrypt_1();
		if (!L_0)
		{
			goto IL_005c;
		}
	}
	{
		V_0 = 0;
		goto IL_002a;
	}

IL_0012:
	{
		ByteU5BU5D_t4116647657* L_1 = __this->get_temp_3();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		uint8_t* L_3 = ((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2)));
		ByteU5BU5D_t4116647657* L_4 = ___input0;
		int32_t L_5 = V_0;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		uint8_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		*((int8_t*)(L_3)) = (int8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)(*((uint8_t*)L_3))^(int32_t)L_7)))));
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = __this->get_BlockSizeByte_2();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0012;
		}
	}
	{
		ByteU5BU5D_t4116647657* L_11 = __this->get_temp_3();
		ByteU5BU5D_t4116647657* L_12 = ___output1;
		VirtActionInvoker2< ByteU5BU5D_t4116647657*, ByteU5BU5D_t4116647657* >::Invoke(17 /* System.Void Mono.Security.Cryptography.SymmetricTransform::ECB(System.Byte[],System.Byte[]) */, __this, L_11, L_12);
		ByteU5BU5D_t4116647657* L_13 = ___output1;
		ByteU5BU5D_t4116647657* L_14 = __this->get_temp_3();
		int32_t L_15 = __this->get_BlockSizeByte_2();
		Buffer_BlockCopy_m2884209081(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_13, 0, (Il2CppArray *)(Il2CppArray *)L_14, 0, L_15, /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_005c:
	{
		ByteU5BU5D_t4116647657* L_16 = ___input0;
		ByteU5BU5D_t4116647657* L_17 = __this->get_temp2_4();
		int32_t L_18 = __this->get_BlockSizeByte_2();
		Buffer_BlockCopy_m2884209081(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_16, 0, (Il2CppArray *)(Il2CppArray *)L_17, 0, L_18, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_19 = ___input0;
		ByteU5BU5D_t4116647657* L_20 = ___output1;
		VirtActionInvoker2< ByteU5BU5D_t4116647657*, ByteU5BU5D_t4116647657* >::Invoke(17 /* System.Void Mono.Security.Cryptography.SymmetricTransform::ECB(System.Byte[],System.Byte[]) */, __this, L_19, L_20);
		V_1 = 0;
		goto IL_0097;
	}

IL_007f:
	{
		ByteU5BU5D_t4116647657* L_21 = ___output1;
		int32_t L_22 = V_1;
		NullCheck(L_21);
		uint8_t* L_23 = ((L_21)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_22)));
		ByteU5BU5D_t4116647657* L_24 = __this->get_temp_3();
		int32_t L_25 = V_1;
		NullCheck(L_24);
		int32_t L_26 = L_25;
		uint8_t L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		*((int8_t*)(L_23)) = (int8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)(*((uint8_t*)L_23))^(int32_t)L_27)))));
		int32_t L_28 = V_1;
		V_1 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_0097:
	{
		int32_t L_29 = V_1;
		int32_t L_30 = __this->get_BlockSizeByte_2();
		if ((((int32_t)L_29) < ((int32_t)L_30)))
		{
			goto IL_007f;
		}
	}
	{
		ByteU5BU5D_t4116647657* L_31 = __this->get_temp2_4();
		ByteU5BU5D_t4116647657* L_32 = __this->get_temp_3();
		int32_t L_33 = __this->get_BlockSizeByte_2();
		Buffer_BlockCopy_m2884209081(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_31, 0, (Il2CppArray *)(Il2CppArray *)L_32, 0, L_33, /*hidden argument*/NULL);
	}

IL_00bc:
	{
		return;
	}
}
// System.Void Mono.Security.Cryptography.SymmetricTransform::CFB(System.Byte[],System.Byte[])
extern "C"  void SymmetricTransform_CFB_m1755507252 (SymmetricTransform_t3802591843 * __this, ByteU5BU5D_t4116647657* ___input0, ByteU5BU5D_t4116647657* ___output1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		bool L_0 = __this->get_encrypt_1();
		if (!L_0)
		{
			goto IL_00a9;
		}
	}
	{
		V_0 = 0;
		goto IL_0098;
	}

IL_0012:
	{
		ByteU5BU5D_t4116647657* L_1 = __this->get_temp_3();
		ByteU5BU5D_t4116647657* L_2 = __this->get_temp2_4();
		VirtActionInvoker2< ByteU5BU5D_t4116647657*, ByteU5BU5D_t4116647657* >::Invoke(17 /* System.Void Mono.Security.Cryptography.SymmetricTransform::ECB(System.Byte[],System.Byte[]) */, __this, L_1, L_2);
		V_1 = 0;
		goto IL_0043;
	}

IL_002b:
	{
		ByteU5BU5D_t4116647657* L_3 = ___output1;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		ByteU5BU5D_t4116647657* L_6 = __this->get_temp2_4();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		uint8_t L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		ByteU5BU5D_t4116647657* L_10 = ___input0;
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		NullCheck(L_10);
		int32_t L_13 = ((int32_t)((int32_t)L_11+(int32_t)L_12));
		uint8_t L_14 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_4+(int32_t)L_5))), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_9^(int32_t)L_14))))));
		int32_t L_15 = V_1;
		V_1 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0043:
	{
		int32_t L_16 = V_1;
		int32_t L_17 = __this->get_FeedBackByte_7();
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_002b;
		}
	}
	{
		ByteU5BU5D_t4116647657* L_18 = __this->get_temp_3();
		int32_t L_19 = __this->get_FeedBackByte_7();
		ByteU5BU5D_t4116647657* L_20 = __this->get_temp_3();
		int32_t L_21 = __this->get_BlockSizeByte_2();
		int32_t L_22 = __this->get_FeedBackByte_7();
		Buffer_BlockCopy_m2884209081(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_18, L_19, (Il2CppArray *)(Il2CppArray *)L_20, 0, ((int32_t)((int32_t)L_21-(int32_t)L_22)), /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_23 = ___output1;
		int32_t L_24 = V_0;
		ByteU5BU5D_t4116647657* L_25 = __this->get_temp_3();
		int32_t L_26 = __this->get_BlockSizeByte_2();
		int32_t L_27 = __this->get_FeedBackByte_7();
		int32_t L_28 = __this->get_FeedBackByte_7();
		Buffer_BlockCopy_m2884209081(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_23, L_24, (Il2CppArray *)(Il2CppArray *)L_25, ((int32_t)((int32_t)L_26-(int32_t)L_27)), L_28, /*hidden argument*/NULL);
		int32_t L_29 = V_0;
		V_0 = ((int32_t)((int32_t)L_29+(int32_t)1));
	}

IL_0098:
	{
		int32_t L_30 = V_0;
		int32_t L_31 = __this->get_FeedBackIter_8();
		if ((((int32_t)L_30) < ((int32_t)L_31)))
		{
			goto IL_0012;
		}
	}
	{
		goto IL_0150;
	}

IL_00a9:
	{
		V_2 = 0;
		goto IL_0144;
	}

IL_00b0:
	{
		__this->set_encrypt_1((bool)1);
		ByteU5BU5D_t4116647657* L_32 = __this->get_temp_3();
		ByteU5BU5D_t4116647657* L_33 = __this->get_temp2_4();
		VirtActionInvoker2< ByteU5BU5D_t4116647657*, ByteU5BU5D_t4116647657* >::Invoke(17 /* System.Void Mono.Security.Cryptography.SymmetricTransform::ECB(System.Byte[],System.Byte[]) */, __this, L_32, L_33);
		__this->set_encrypt_1((bool)0);
		ByteU5BU5D_t4116647657* L_34 = __this->get_temp_3();
		int32_t L_35 = __this->get_FeedBackByte_7();
		ByteU5BU5D_t4116647657* L_36 = __this->get_temp_3();
		int32_t L_37 = __this->get_BlockSizeByte_2();
		int32_t L_38 = __this->get_FeedBackByte_7();
		Buffer_BlockCopy_m2884209081(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_34, L_35, (Il2CppArray *)(Il2CppArray *)L_36, 0, ((int32_t)((int32_t)L_37-(int32_t)L_38)), /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_39 = ___input0;
		int32_t L_40 = V_2;
		ByteU5BU5D_t4116647657* L_41 = __this->get_temp_3();
		int32_t L_42 = __this->get_BlockSizeByte_2();
		int32_t L_43 = __this->get_FeedBackByte_7();
		int32_t L_44 = __this->get_FeedBackByte_7();
		Buffer_BlockCopy_m2884209081(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_39, L_40, (Il2CppArray *)(Il2CppArray *)L_41, ((int32_t)((int32_t)L_42-(int32_t)L_43)), L_44, /*hidden argument*/NULL);
		V_3 = 0;
		goto IL_0134;
	}

IL_011c:
	{
		ByteU5BU5D_t4116647657* L_45 = ___output1;
		int32_t L_46 = V_3;
		int32_t L_47 = V_2;
		ByteU5BU5D_t4116647657* L_48 = __this->get_temp2_4();
		int32_t L_49 = V_3;
		NullCheck(L_48);
		int32_t L_50 = L_49;
		uint8_t L_51 = (L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_50));
		ByteU5BU5D_t4116647657* L_52 = ___input0;
		int32_t L_53 = V_3;
		int32_t L_54 = V_2;
		NullCheck(L_52);
		int32_t L_55 = ((int32_t)((int32_t)L_53+(int32_t)L_54));
		uint8_t L_56 = (L_52)->GetAt(static_cast<il2cpp_array_size_t>(L_55));
		NullCheck(L_45);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_46+(int32_t)L_47))), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_51^(int32_t)L_56))))));
		int32_t L_57 = V_3;
		V_3 = ((int32_t)((int32_t)L_57+(int32_t)1));
	}

IL_0134:
	{
		int32_t L_58 = V_3;
		int32_t L_59 = __this->get_FeedBackByte_7();
		if ((((int32_t)L_58) < ((int32_t)L_59)))
		{
			goto IL_011c;
		}
	}
	{
		int32_t L_60 = V_2;
		V_2 = ((int32_t)((int32_t)L_60+(int32_t)1));
	}

IL_0144:
	{
		int32_t L_61 = V_2;
		int32_t L_62 = __this->get_FeedBackIter_8();
		if ((((int32_t)L_61) < ((int32_t)L_62)))
		{
			goto IL_00b0;
		}
	}

IL_0150:
	{
		return;
	}
}
// System.Void Mono.Security.Cryptography.SymmetricTransform::OFB(System.Byte[],System.Byte[])
extern "C"  void SymmetricTransform_OFB_m3690147804 (SymmetricTransform_t3802591843 * __this, ByteU5BU5D_t4116647657* ___input0, ByteU5BU5D_t4116647657* ___output1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SymmetricTransform_OFB_m3690147804_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CryptographicException_t248831461 * L_0 = (CryptographicException_t248831461 *)il2cpp_codegen_object_new(CryptographicException_t248831461_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m503735289(L_0, _stringLiteral3478177746, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Mono.Security.Cryptography.SymmetricTransform::CTS(System.Byte[],System.Byte[])
extern "C"  void SymmetricTransform_CTS_m764800021 (SymmetricTransform_t3802591843 * __this, ByteU5BU5D_t4116647657* ___input0, ByteU5BU5D_t4116647657* ___output1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SymmetricTransform_CTS_m764800021_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CryptographicException_t248831461 * L_0 = (CryptographicException_t248831461 *)il2cpp_codegen_object_new(CryptographicException_t248831461_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m503735289(L_0, _stringLiteral3430552138, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Mono.Security.Cryptography.SymmetricTransform::CheckInput(System.Byte[],System.Int32,System.Int32)
extern "C"  void SymmetricTransform_CheckInput_m2092289040 (SymmetricTransform_t3802591843 * __this, ByteU5BU5D_t4116647657* ___inputBuffer0, int32_t ___inputOffset1, int32_t ___inputCount2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SymmetricTransform_CheckInput_m2092289040_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t4116647657* L_0 = ___inputBuffer0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral3152468735, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___inputOffset1;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0028;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_3 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m282481429(L_3, _stringLiteral2167393519, _stringLiteral3073595182, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0028:
	{
		int32_t L_4 = ___inputCount2;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_003f;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_5 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m282481429(L_5, _stringLiteral438779933, _stringLiteral3073595182, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_003f:
	{
		int32_t L_6 = ___inputOffset1;
		ByteU5BU5D_t4116647657* L_7 = ___inputBuffer0;
		NullCheck(L_7);
		int32_t L_8 = ___inputCount2;
		if ((((int32_t)L_6) <= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))-(int32_t)L_8)))))
		{
			goto IL_005f;
		}
	}
	{
		String_t* L_9 = Locale_GetText_m1626635120(NULL /*static, unused*/, _stringLiteral251636811, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_10 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1216717135(L_10, _stringLiteral3152468735, L_9, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_005f:
	{
		return;
	}
}
// System.Int32 Mono.Security.Cryptography.SymmetricTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern "C"  int32_t SymmetricTransform_TransformBlock_m851059707 (SymmetricTransform_t3802591843 * __this, ByteU5BU5D_t4116647657* ___inputBuffer0, int32_t ___inputOffset1, int32_t ___inputCount2, ByteU5BU5D_t4116647657* ___outputBuffer3, int32_t ___outputOffset4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SymmetricTransform_TransformBlock_m851059707_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_m_disposed_9();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ObjectDisposedException_t21392786 * L_1 = (ObjectDisposedException_t21392786 *)il2cpp_codegen_object_new(ObjectDisposedException_t21392786_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3603759869(L_1, _stringLiteral389898510, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		ByteU5BU5D_t4116647657* L_2 = ___inputBuffer0;
		int32_t L_3 = ___inputOffset1;
		int32_t L_4 = ___inputCount2;
		SymmetricTransform_CheckInput_m2092289040(__this, L_2, L_3, L_4, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_5 = ___outputBuffer3;
		if (L_5)
		{
			goto IL_0031;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_6 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_6, _stringLiteral2053830539, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0031:
	{
		int32_t L_7 = ___outputOffset4;
		if ((((int32_t)L_7) >= ((int32_t)0)))
		{
			goto IL_0049;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_8 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m282481429(L_8, _stringLiteral1561769044, _stringLiteral3073595182, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0049:
	{
		ByteU5BU5D_t4116647657* L_9 = ___outputBuffer3;
		NullCheck(L_9);
		int32_t L_10 = ___inputCount2;
		int32_t L_11 = ___outputOffset4;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))-(int32_t)L_10))-(int32_t)L_11));
		bool L_12 = __this->get_encrypt_1();
		if (L_12)
		{
			goto IL_009c;
		}
	}
	{
		int32_t L_13 = V_0;
		if ((((int32_t)0) <= ((int32_t)L_13)))
		{
			goto IL_009c;
		}
	}
	{
		SymmetricAlgorithm_t4254223087 * L_14 = __this->get_algo_0();
		NullCheck(L_14);
		int32_t L_15 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::get_Padding() */, L_14);
		if ((((int32_t)L_15) == ((int32_t)1)))
		{
			goto IL_0087;
		}
	}
	{
		SymmetricAlgorithm_t4254223087 * L_16 = __this->get_algo_0();
		NullCheck(L_16);
		int32_t L_17 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::get_Padding() */, L_16);
		if ((!(((uint32_t)L_17) == ((uint32_t)3))))
		{
			goto IL_009c;
		}
	}

IL_0087:
	{
		String_t* L_18 = Locale_GetText_m1626635120(NULL /*static, unused*/, _stringLiteral251636811, /*hidden argument*/NULL);
		CryptographicException_t248831461 * L_19 = (CryptographicException_t248831461 *)il2cpp_codegen_object_new(CryptographicException_t248831461_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m3803155940(L_19, _stringLiteral2053830539, L_18, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
	}

IL_009c:
	{
		bool L_20 = SymmetricTransform_get_KeepLastBlock_m2492071306(__this, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00cf;
		}
	}
	{
		int32_t L_21 = V_0;
		int32_t L_22 = __this->get_BlockSizeByte_2();
		if ((((int32_t)0) <= ((int32_t)((int32_t)((int32_t)L_21+(int32_t)L_22)))))
		{
			goto IL_00ca;
		}
	}
	{
		String_t* L_23 = Locale_GetText_m1626635120(NULL /*static, unused*/, _stringLiteral251636811, /*hidden argument*/NULL);
		CryptographicException_t248831461 * L_24 = (CryptographicException_t248831461 *)il2cpp_codegen_object_new(CryptographicException_t248831461_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m3803155940(L_24, _stringLiteral2053830539, L_23, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_24);
	}

IL_00ca:
	{
		goto IL_010e;
	}

IL_00cf:
	{
		int32_t L_25 = V_0;
		if ((((int32_t)0) <= ((int32_t)L_25)))
		{
			goto IL_010e;
		}
	}
	{
		ByteU5BU5D_t4116647657* L_26 = ___inputBuffer0;
		NullCheck(L_26);
		int32_t L_27 = ___inputOffset1;
		ByteU5BU5D_t4116647657* L_28 = ___outputBuffer3;
		NullCheck(L_28);
		int32_t L_29 = __this->get_BlockSizeByte_2();
		if ((!(((uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_26)->max_length))))-(int32_t)L_27))-(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_28)->max_length))))))) == ((uint32_t)L_29))))
		{
			goto IL_00f9;
		}
	}
	{
		ByteU5BU5D_t4116647657* L_30 = ___outputBuffer3;
		NullCheck(L_30);
		int32_t L_31 = ___outputOffset4;
		___inputCount2 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length))))-(int32_t)L_31));
		goto IL_010e;
	}

IL_00f9:
	{
		String_t* L_32 = Locale_GetText_m1626635120(NULL /*static, unused*/, _stringLiteral251636811, /*hidden argument*/NULL);
		CryptographicException_t248831461 * L_33 = (CryptographicException_t248831461 *)il2cpp_codegen_object_new(CryptographicException_t248831461_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m3803155940(L_33, _stringLiteral2053830539, L_32, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_33);
	}

IL_010e:
	{
		ByteU5BU5D_t4116647657* L_34 = ___inputBuffer0;
		int32_t L_35 = ___inputOffset1;
		int32_t L_36 = ___inputCount2;
		ByteU5BU5D_t4116647657* L_37 = ___outputBuffer3;
		int32_t L_38 = ___outputOffset4;
		int32_t L_39 = SymmetricTransform_InternalTransformBlock_m1743612142(__this, L_34, L_35, L_36, L_37, L_38, /*hidden argument*/NULL);
		return L_39;
	}
}
// System.Boolean Mono.Security.Cryptography.SymmetricTransform::get_KeepLastBlock()
extern "C"  bool SymmetricTransform_get_KeepLastBlock_m2492071306 (SymmetricTransform_t3802591843 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		bool L_0 = __this->get_encrypt_1();
		if (L_0)
		{
			goto IL_002f;
		}
	}
	{
		SymmetricAlgorithm_t4254223087 * L_1 = __this->get_algo_0();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::get_Padding() */, L_1);
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_002f;
		}
	}
	{
		SymmetricAlgorithm_t4254223087 * L_3 = __this->get_algo_0();
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::get_Padding() */, L_3);
		G_B4_0 = ((((int32_t)((((int32_t)L_4) == ((int32_t)3))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0030;
	}

IL_002f:
	{
		G_B4_0 = 0;
	}

IL_0030:
	{
		return (bool)G_B4_0;
	}
}
// System.Int32 Mono.Security.Cryptography.SymmetricTransform::InternalTransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern "C"  int32_t SymmetricTransform_InternalTransformBlock_m1743612142 (SymmetricTransform_t3802591843 * __this, ByteU5BU5D_t4116647657* ___inputBuffer0, int32_t ___inputOffset1, int32_t ___inputCount2, ByteU5BU5D_t4116647657* ___outputBuffer3, int32_t ___outputOffset4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SymmetricTransform_InternalTransformBlock_m1743612142_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		int32_t L_0 = ___inputOffset1;
		V_0 = L_0;
		int32_t L_1 = ___inputCount2;
		int32_t L_2 = __this->get_BlockSizeByte_2();
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_3 = ___inputCount2;
		int32_t L_4 = __this->get_BlockSizeByte_2();
		if (!((int32_t)((int32_t)L_3%(int32_t)L_4)))
		{
			goto IL_0026;
		}
	}
	{
		CryptographicException_t248831461 * L_5 = (CryptographicException_t248831461 *)il2cpp_codegen_object_new(CryptographicException_t248831461_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m503735289(L_5, _stringLiteral3823085299, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = ___inputCount2;
		int32_t L_7 = __this->get_BlockSizeByte_2();
		V_1 = ((int32_t)((int32_t)L_6/(int32_t)L_7));
		goto IL_0036;
	}

IL_0034:
	{
		V_1 = 1;
	}

IL_0036:
	{
		bool L_8 = SymmetricTransform_get_KeepLastBlock_m2492071306(__this, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0045;
		}
	}
	{
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_0045:
	{
		V_2 = 0;
		bool L_10 = __this->get_lastBlock_10();
		if (!L_10)
		{
			goto IL_0095;
		}
	}
	{
		ByteU5BU5D_t4116647657* L_11 = __this->get_workBuff_5();
		ByteU5BU5D_t4116647657* L_12 = __this->get_workout_6();
		VirtActionInvoker2< ByteU5BU5D_t4116647657*, ByteU5BU5D_t4116647657* >::Invoke(16 /* System.Void Mono.Security.Cryptography.SymmetricTransform::Transform(System.Byte[],System.Byte[]) */, __this, L_11, L_12);
		ByteU5BU5D_t4116647657* L_13 = __this->get_workout_6();
		ByteU5BU5D_t4116647657* L_14 = ___outputBuffer3;
		int32_t L_15 = ___outputOffset4;
		int32_t L_16 = __this->get_BlockSizeByte_2();
		Buffer_BlockCopy_m2884209081(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_13, 0, (Il2CppArray *)(Il2CppArray *)L_14, L_15, L_16, /*hidden argument*/NULL);
		int32_t L_17 = ___outputOffset4;
		int32_t L_18 = __this->get_BlockSizeByte_2();
		___outputOffset4 = ((int32_t)((int32_t)L_17+(int32_t)L_18));
		int32_t L_19 = V_2;
		int32_t L_20 = __this->get_BlockSizeByte_2();
		V_2 = ((int32_t)((int32_t)L_19+(int32_t)L_20));
		__this->set_lastBlock_10((bool)0);
	}

IL_0095:
	{
		V_3 = 0;
		goto IL_00f9;
	}

IL_009c:
	{
		ByteU5BU5D_t4116647657* L_21 = ___inputBuffer0;
		int32_t L_22 = V_0;
		ByteU5BU5D_t4116647657* L_23 = __this->get_workBuff_5();
		int32_t L_24 = __this->get_BlockSizeByte_2();
		Buffer_BlockCopy_m2884209081(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_21, L_22, (Il2CppArray *)(Il2CppArray *)L_23, 0, L_24, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_25 = __this->get_workBuff_5();
		ByteU5BU5D_t4116647657* L_26 = __this->get_workout_6();
		VirtActionInvoker2< ByteU5BU5D_t4116647657*, ByteU5BU5D_t4116647657* >::Invoke(16 /* System.Void Mono.Security.Cryptography.SymmetricTransform::Transform(System.Byte[],System.Byte[]) */, __this, L_25, L_26);
		ByteU5BU5D_t4116647657* L_27 = __this->get_workout_6();
		ByteU5BU5D_t4116647657* L_28 = ___outputBuffer3;
		int32_t L_29 = ___outputOffset4;
		int32_t L_30 = __this->get_BlockSizeByte_2();
		Buffer_BlockCopy_m2884209081(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_27, 0, (Il2CppArray *)(Il2CppArray *)L_28, L_29, L_30, /*hidden argument*/NULL);
		int32_t L_31 = V_0;
		int32_t L_32 = __this->get_BlockSizeByte_2();
		V_0 = ((int32_t)((int32_t)L_31+(int32_t)L_32));
		int32_t L_33 = ___outputOffset4;
		int32_t L_34 = __this->get_BlockSizeByte_2();
		___outputOffset4 = ((int32_t)((int32_t)L_33+(int32_t)L_34));
		int32_t L_35 = V_2;
		int32_t L_36 = __this->get_BlockSizeByte_2();
		V_2 = ((int32_t)((int32_t)L_35+(int32_t)L_36));
		int32_t L_37 = V_3;
		V_3 = ((int32_t)((int32_t)L_37+(int32_t)1));
	}

IL_00f9:
	{
		int32_t L_38 = V_3;
		int32_t L_39 = V_1;
		if ((((int32_t)L_38) < ((int32_t)L_39)))
		{
			goto IL_009c;
		}
	}
	{
		bool L_40 = SymmetricTransform_get_KeepLastBlock_m2492071306(__this, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_0126;
		}
	}
	{
		ByteU5BU5D_t4116647657* L_41 = ___inputBuffer0;
		int32_t L_42 = V_0;
		ByteU5BU5D_t4116647657* L_43 = __this->get_workBuff_5();
		int32_t L_44 = __this->get_BlockSizeByte_2();
		Buffer_BlockCopy_m2884209081(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_41, L_42, (Il2CppArray *)(Il2CppArray *)L_43, 0, L_44, /*hidden argument*/NULL);
		__this->set_lastBlock_10((bool)1);
	}

IL_0126:
	{
		int32_t L_45 = V_2;
		return L_45;
	}
}
// System.Void Mono.Security.Cryptography.SymmetricTransform::Random(System.Byte[],System.Int32,System.Int32)
extern "C"  void SymmetricTransform_Random_m3740038270 (SymmetricTransform_t3802591843 * __this, ByteU5BU5D_t4116647657* ___buffer0, int32_t ___start1, int32_t ___length2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SymmetricTransform_Random_m3740038270_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t4116647657* V_0 = NULL;
	{
		RandomNumberGenerator_t386037858 * L_0 = __this->get__rng_11();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		RandomNumberGenerator_t386037858 * L_1 = RandomNumberGenerator_Create_m4162970280(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__rng_11(L_1);
	}

IL_0016:
	{
		int32_t L_2 = ___length2;
		V_0 = ((ByteU5BU5D_t4116647657*)SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, (uint32_t)L_2));
		RandomNumberGenerator_t386037858 * L_3 = __this->get__rng_11();
		ByteU5BU5D_t4116647657* L_4 = V_0;
		NullCheck(L_3);
		VirtActionInvoker1< ByteU5BU5D_t4116647657* >::Invoke(4 /* System.Void System.Security.Cryptography.RandomNumberGenerator::GetBytes(System.Byte[]) */, L_3, L_4);
		ByteU5BU5D_t4116647657* L_5 = V_0;
		ByteU5BU5D_t4116647657* L_6 = ___buffer0;
		int32_t L_7 = ___start1;
		int32_t L_8 = ___length2;
		Buffer_BlockCopy_m2884209081(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, 0, (Il2CppArray *)(Il2CppArray *)L_6, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Security.Cryptography.SymmetricTransform::ThrowBadPaddingException(System.Security.Cryptography.PaddingMode,System.Int32,System.Int32)
extern "C"  void SymmetricTransform_ThrowBadPaddingException_m2898061954 (SymmetricTransform_t3802591843 * __this, int32_t ___padding0, int32_t ___length1, int32_t ___position2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SymmetricTransform_ThrowBadPaddingException_m2898061954_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = Locale_GetText_m1626635120(NULL /*static, unused*/, _stringLiteral2101785501, /*hidden argument*/NULL);
		int32_t L_1 = ___padding0;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(PaddingMode_t2546806710_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m2844511972(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = ___length1;
		if ((((int32_t)L_5) < ((int32_t)0)))
		{
			goto IL_0039;
		}
	}
	{
		String_t* L_6 = V_0;
		String_t* L_7 = Locale_GetText_m1626635120(NULL /*static, unused*/, _stringLiteral289204851, /*hidden argument*/NULL);
		int32_t L_8 = ___length1;
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_9);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Format_m2844511972(NULL /*static, unused*/, L_7, L_10, /*hidden argument*/NULL);
		String_t* L_12 = String_Concat_m3937257545(NULL /*static, unused*/, L_6, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
	}

IL_0039:
	{
		int32_t L_13 = ___position2;
		if ((((int32_t)L_13) < ((int32_t)0)))
		{
			goto IL_005c;
		}
	}
	{
		String_t* L_14 = V_0;
		String_t* L_15 = Locale_GetText_m1626635120(NULL /*static, unused*/, _stringLiteral4613441, /*hidden argument*/NULL);
		int32_t L_16 = ___position2;
		int32_t L_17 = L_16;
		Il2CppObject * L_18 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_17);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Format_m2844511972(NULL /*static, unused*/, L_15, L_18, /*hidden argument*/NULL);
		String_t* L_20 = String_Concat_m3937257545(NULL /*static, unused*/, L_14, L_19, /*hidden argument*/NULL);
		V_0 = L_20;
	}

IL_005c:
	{
		String_t* L_21 = V_0;
		CryptographicException_t248831461 * L_22 = (CryptographicException_t248831461 *)il2cpp_codegen_object_new(CryptographicException_t248831461_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m503735289(L_22, L_21, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22);
	}
}
// System.Byte[] Mono.Security.Cryptography.SymmetricTransform::FinalEncrypt(System.Byte[],System.Int32,System.Int32)
extern "C"  ByteU5BU5D_t4116647657* SymmetricTransform_FinalEncrypt_m748885414 (SymmetricTransform_t3802591843 * __this, ByteU5BU5D_t4116647657* ___inputBuffer0, int32_t ___inputOffset1, int32_t ___inputCount2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SymmetricTransform_FinalEncrypt_m748885414_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	ByteU5BU5D_t4116647657* V_3 = NULL;
	ByteU5BU5D_t4116647657* V_4 = NULL;
	int32_t V_5 = 0;
	uint8_t V_6 = 0x0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	{
		int32_t L_0 = ___inputCount2;
		int32_t L_1 = __this->get_BlockSizeByte_2();
		int32_t L_2 = __this->get_BlockSizeByte_2();
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_0/(int32_t)L_1))*(int32_t)L_2));
		int32_t L_3 = ___inputCount2;
		int32_t L_4 = V_0;
		V_1 = ((int32_t)((int32_t)L_3-(int32_t)L_4));
		int32_t L_5 = V_0;
		V_2 = L_5;
		SymmetricAlgorithm_t4254223087 * L_6 = __this->get_algo_0();
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::get_Padding() */, L_6);
		V_8 = L_7;
		int32_t L_8 = V_8;
		switch (((int32_t)((int32_t)L_8-(int32_t)2)))
		{
			case 0:
			{
				goto IL_0041;
			}
			case 1:
			{
				goto IL_004f;
			}
			case 2:
			{
				goto IL_0041;
			}
			case 3:
			{
				goto IL_0041;
			}
		}
	}
	{
		goto IL_004f;
	}

IL_0041:
	{
		int32_t L_9 = V_2;
		int32_t L_10 = __this->get_BlockSizeByte_2();
		V_2 = ((int32_t)((int32_t)L_9+(int32_t)L_10));
		goto IL_00a8;
	}

IL_004f:
	{
		int32_t L_11 = ___inputCount2;
		if (L_11)
		{
			goto IL_005c;
		}
	}
	{
		return ((ByteU5BU5D_t4116647657*)SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_005c:
	{
		int32_t L_12 = V_1;
		if (!L_12)
		{
			goto IL_00a3;
		}
	}
	{
		SymmetricAlgorithm_t4254223087 * L_13 = __this->get_algo_0();
		NullCheck(L_13);
		int32_t L_14 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::get_Padding() */, L_13);
		if ((!(((uint32_t)L_14) == ((uint32_t)1))))
		{
			goto IL_007e;
		}
	}
	{
		CryptographicException_t248831461 * L_15 = (CryptographicException_t248831461 *)il2cpp_codegen_object_new(CryptographicException_t248831461_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m503735289(L_15, _stringLiteral3246833729, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}

IL_007e:
	{
		int32_t L_16 = V_0;
		int32_t L_17 = __this->get_BlockSizeByte_2();
		V_3 = ((ByteU5BU5D_t4116647657*)SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_16+(int32_t)L_17))));
		ByteU5BU5D_t4116647657* L_18 = ___inputBuffer0;
		int32_t L_19 = ___inputOffset1;
		ByteU5BU5D_t4116647657* L_20 = V_3;
		int32_t L_21 = ___inputCount2;
		Buffer_BlockCopy_m2884209081(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_18, L_19, (Il2CppArray *)(Il2CppArray *)L_20, 0, L_21, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_22 = V_3;
		___inputBuffer0 = L_22;
		___inputOffset1 = 0;
		ByteU5BU5D_t4116647657* L_23 = V_3;
		NullCheck(L_23);
		___inputCount2 = (((int32_t)((int32_t)(((Il2CppArray *)L_23)->max_length))));
		int32_t L_24 = ___inputCount2;
		V_2 = L_24;
	}

IL_00a3:
	{
		goto IL_00a8;
	}

IL_00a8:
	{
		int32_t L_25 = V_2;
		V_4 = ((ByteU5BU5D_t4116647657*)SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, (uint32_t)L_25));
		V_5 = 0;
		goto IL_00e9;
	}

IL_00b8:
	{
		ByteU5BU5D_t4116647657* L_26 = ___inputBuffer0;
		int32_t L_27 = ___inputOffset1;
		int32_t L_28 = __this->get_BlockSizeByte_2();
		ByteU5BU5D_t4116647657* L_29 = V_4;
		int32_t L_30 = V_5;
		SymmetricTransform_InternalTransformBlock_m1743612142(__this, L_26, L_27, L_28, L_29, L_30, /*hidden argument*/NULL);
		int32_t L_31 = ___inputOffset1;
		int32_t L_32 = __this->get_BlockSizeByte_2();
		___inputOffset1 = ((int32_t)((int32_t)L_31+(int32_t)L_32));
		int32_t L_33 = V_5;
		int32_t L_34 = __this->get_BlockSizeByte_2();
		V_5 = ((int32_t)((int32_t)L_33+(int32_t)L_34));
		int32_t L_35 = V_2;
		int32_t L_36 = __this->get_BlockSizeByte_2();
		V_2 = ((int32_t)((int32_t)L_35-(int32_t)L_36));
	}

IL_00e9:
	{
		int32_t L_37 = V_2;
		int32_t L_38 = __this->get_BlockSizeByte_2();
		if ((((int32_t)L_37) > ((int32_t)L_38)))
		{
			goto IL_00b8;
		}
	}
	{
		int32_t L_39 = __this->get_BlockSizeByte_2();
		int32_t L_40 = V_1;
		V_6 = (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_39-(int32_t)L_40)))));
		SymmetricAlgorithm_t4254223087 * L_41 = __this->get_algo_0();
		NullCheck(L_41);
		int32_t L_42 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::get_Padding() */, L_41);
		V_8 = L_42;
		int32_t L_43 = V_8;
		switch (((int32_t)((int32_t)L_43-(int32_t)2)))
		{
			case 0:
			{
				goto IL_019a;
			}
			case 1:
			{
				goto IL_01e2;
			}
			case 2:
			{
				goto IL_012b;
			}
			case 3:
			{
				goto IL_0159;
			}
		}
	}
	{
		goto IL_01e2;
	}

IL_012b:
	{
		ByteU5BU5D_t4116647657* L_44 = V_4;
		ByteU5BU5D_t4116647657* L_45 = V_4;
		NullCheck(L_45);
		uint8_t L_46 = V_6;
		NullCheck(L_44);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_45)->max_length))))-(int32_t)1))), (uint8_t)L_46);
		ByteU5BU5D_t4116647657* L_47 = ___inputBuffer0;
		int32_t L_48 = ___inputOffset1;
		ByteU5BU5D_t4116647657* L_49 = V_4;
		int32_t L_50 = V_0;
		int32_t L_51 = V_1;
		Buffer_BlockCopy_m2884209081(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_47, L_48, (Il2CppArray *)(Il2CppArray *)L_49, L_50, L_51, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_52 = V_4;
		int32_t L_53 = V_0;
		int32_t L_54 = __this->get_BlockSizeByte_2();
		ByteU5BU5D_t4116647657* L_55 = V_4;
		int32_t L_56 = V_0;
		SymmetricTransform_InternalTransformBlock_m1743612142(__this, L_52, L_53, L_54, L_55, L_56, /*hidden argument*/NULL);
		goto IL_01fa;
	}

IL_0159:
	{
		ByteU5BU5D_t4116647657* L_57 = V_4;
		ByteU5BU5D_t4116647657* L_58 = V_4;
		NullCheck(L_58);
		uint8_t L_59 = V_6;
		uint8_t L_60 = V_6;
		SymmetricTransform_Random_m3740038270(__this, L_57, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_58)->max_length))))-(int32_t)L_59)), ((int32_t)((int32_t)L_60-(int32_t)1)), /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_61 = V_4;
		ByteU5BU5D_t4116647657* L_62 = V_4;
		NullCheck(L_62);
		uint8_t L_63 = V_6;
		NullCheck(L_61);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_62)->max_length))))-(int32_t)1))), (uint8_t)L_63);
		ByteU5BU5D_t4116647657* L_64 = ___inputBuffer0;
		int32_t L_65 = ___inputOffset1;
		ByteU5BU5D_t4116647657* L_66 = V_4;
		int32_t L_67 = V_0;
		int32_t L_68 = V_1;
		Buffer_BlockCopy_m2884209081(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_64, L_65, (Il2CppArray *)(Il2CppArray *)L_66, L_67, L_68, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_69 = V_4;
		int32_t L_70 = V_0;
		int32_t L_71 = __this->get_BlockSizeByte_2();
		ByteU5BU5D_t4116647657* L_72 = V_4;
		int32_t L_73 = V_0;
		SymmetricTransform_InternalTransformBlock_m1743612142(__this, L_69, L_70, L_71, L_72, L_73, /*hidden argument*/NULL);
		goto IL_01fa;
	}

IL_019a:
	{
		ByteU5BU5D_t4116647657* L_74 = V_4;
		NullCheck(L_74);
		V_7 = (((int32_t)((int32_t)(((Il2CppArray *)L_74)->max_length))));
		goto IL_01ac;
	}

IL_01a5:
	{
		ByteU5BU5D_t4116647657* L_75 = V_4;
		int32_t L_76 = V_7;
		uint8_t L_77 = V_6;
		NullCheck(L_75);
		(L_75)->SetAt(static_cast<il2cpp_array_size_t>(L_76), (uint8_t)L_77);
	}

IL_01ac:
	{
		int32_t L_78 = V_7;
		int32_t L_79 = ((int32_t)((int32_t)L_78-(int32_t)1));
		V_7 = L_79;
		ByteU5BU5D_t4116647657* L_80 = V_4;
		NullCheck(L_80);
		uint8_t L_81 = V_6;
		if ((((int32_t)L_79) >= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_80)->max_length))))-(int32_t)L_81)))))
		{
			goto IL_01a5;
		}
	}
	{
		ByteU5BU5D_t4116647657* L_82 = ___inputBuffer0;
		int32_t L_83 = ___inputOffset1;
		ByteU5BU5D_t4116647657* L_84 = V_4;
		int32_t L_85 = V_0;
		int32_t L_86 = V_1;
		Buffer_BlockCopy_m2884209081(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_82, L_83, (Il2CppArray *)(Il2CppArray *)L_84, L_85, L_86, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_87 = V_4;
		int32_t L_88 = V_0;
		int32_t L_89 = __this->get_BlockSizeByte_2();
		ByteU5BU5D_t4116647657* L_90 = V_4;
		int32_t L_91 = V_0;
		SymmetricTransform_InternalTransformBlock_m1743612142(__this, L_87, L_88, L_89, L_90, L_91, /*hidden argument*/NULL);
		goto IL_01fa;
	}

IL_01e2:
	{
		ByteU5BU5D_t4116647657* L_92 = ___inputBuffer0;
		int32_t L_93 = ___inputOffset1;
		int32_t L_94 = __this->get_BlockSizeByte_2();
		ByteU5BU5D_t4116647657* L_95 = V_4;
		int32_t L_96 = V_5;
		SymmetricTransform_InternalTransformBlock_m1743612142(__this, L_92, L_93, L_94, L_95, L_96, /*hidden argument*/NULL);
		goto IL_01fa;
	}

IL_01fa:
	{
		ByteU5BU5D_t4116647657* L_97 = V_4;
		return L_97;
	}
}
// System.Byte[] Mono.Security.Cryptography.SymmetricTransform::FinalDecrypt(System.Byte[],System.Int32,System.Int32)
extern "C"  ByteU5BU5D_t4116647657* SymmetricTransform_FinalDecrypt_m764004682 (SymmetricTransform_t3802591843 * __this, ByteU5BU5D_t4116647657* ___inputBuffer0, int32_t ___inputOffset1, int32_t ___inputCount2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SymmetricTransform_FinalDecrypt_m764004682_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ByteU5BU5D_t4116647657* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	uint8_t V_4 = 0x0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	ByteU5BU5D_t4116647657* V_7 = NULL;
	int32_t V_8 = 0;
	int32_t G_B12_0 = 0;
	{
		int32_t L_0 = ___inputCount2;
		int32_t L_1 = __this->get_BlockSizeByte_2();
		if ((((int32_t)((int32_t)((int32_t)L_0%(int32_t)L_1))) <= ((int32_t)0)))
		{
			goto IL_0019;
		}
	}
	{
		CryptographicException_t248831461 * L_2 = (CryptographicException_t248831461 *)il2cpp_codegen_object_new(CryptographicException_t248831461_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m503735289(L_2, _stringLiteral3823085299, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		int32_t L_3 = ___inputCount2;
		V_0 = L_3;
		bool L_4 = __this->get_lastBlock_10();
		if (!L_4)
		{
			goto IL_002f;
		}
	}
	{
		int32_t L_5 = V_0;
		int32_t L_6 = __this->get_BlockSizeByte_2();
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)L_6));
	}

IL_002f:
	{
		int32_t L_7 = V_0;
		V_1 = ((ByteU5BU5D_t4116647657*)SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, (uint32_t)L_7));
		V_2 = 0;
		goto IL_0066;
	}

IL_003d:
	{
		ByteU5BU5D_t4116647657* L_8 = ___inputBuffer0;
		int32_t L_9 = ___inputOffset1;
		int32_t L_10 = __this->get_BlockSizeByte_2();
		ByteU5BU5D_t4116647657* L_11 = V_1;
		int32_t L_12 = V_2;
		int32_t L_13 = SymmetricTransform_InternalTransformBlock_m1743612142(__this, L_8, L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		int32_t L_14 = ___inputOffset1;
		int32_t L_15 = __this->get_BlockSizeByte_2();
		___inputOffset1 = ((int32_t)((int32_t)L_14+(int32_t)L_15));
		int32_t L_16 = V_2;
		int32_t L_17 = V_3;
		V_2 = ((int32_t)((int32_t)L_16+(int32_t)L_17));
		int32_t L_18 = ___inputCount2;
		int32_t L_19 = __this->get_BlockSizeByte_2();
		___inputCount2 = ((int32_t)((int32_t)L_18-(int32_t)L_19));
	}

IL_0066:
	{
		int32_t L_20 = ___inputCount2;
		if ((((int32_t)L_20) > ((int32_t)0)))
		{
			goto IL_003d;
		}
	}
	{
		bool L_21 = __this->get_lastBlock_10();
		if (!L_21)
		{
			goto IL_00ae;
		}
	}
	{
		ByteU5BU5D_t4116647657* L_22 = __this->get_workBuff_5();
		ByteU5BU5D_t4116647657* L_23 = __this->get_workout_6();
		VirtActionInvoker2< ByteU5BU5D_t4116647657*, ByteU5BU5D_t4116647657* >::Invoke(16 /* System.Void Mono.Security.Cryptography.SymmetricTransform::Transform(System.Byte[],System.Byte[]) */, __this, L_22, L_23);
		ByteU5BU5D_t4116647657* L_24 = __this->get_workout_6();
		ByteU5BU5D_t4116647657* L_25 = V_1;
		int32_t L_26 = V_2;
		int32_t L_27 = __this->get_BlockSizeByte_2();
		Buffer_BlockCopy_m2884209081(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_24, 0, (Il2CppArray *)(Il2CppArray *)L_25, L_26, L_27, /*hidden argument*/NULL);
		int32_t L_28 = V_2;
		int32_t L_29 = __this->get_BlockSizeByte_2();
		V_2 = ((int32_t)((int32_t)L_28+(int32_t)L_29));
		__this->set_lastBlock_10((bool)0);
	}

IL_00ae:
	{
		int32_t L_30 = V_0;
		if ((((int32_t)L_30) <= ((int32_t)0)))
		{
			goto IL_00bf;
		}
	}
	{
		ByteU5BU5D_t4116647657* L_31 = V_1;
		int32_t L_32 = V_0;
		NullCheck(L_31);
		int32_t L_33 = ((int32_t)((int32_t)L_32-(int32_t)1));
		uint8_t L_34 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		G_B12_0 = ((int32_t)(L_34));
		goto IL_00c0;
	}

IL_00bf:
	{
		G_B12_0 = 0;
	}

IL_00c0:
	{
		V_4 = (uint8_t)G_B12_0;
		SymmetricAlgorithm_t4254223087 * L_35 = __this->get_algo_0();
		NullCheck(L_35);
		int32_t L_36 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::get_Padding() */, L_35);
		V_8 = L_36;
		int32_t L_37 = V_8;
		switch (((int32_t)((int32_t)L_37-(int32_t)1)))
		{
			case 0:
			{
				goto IL_01fd;
			}
			case 1:
			{
				goto IL_018f;
			}
			case 2:
			{
				goto IL_01fd;
			}
			case 3:
			{
				goto IL_00f1;
			}
			case 4:
			{
				goto IL_015d;
			}
		}
	}
	{
		goto IL_0202;
	}

IL_00f1:
	{
		uint8_t L_38 = V_4;
		if (!L_38)
		{
			goto IL_0105;
		}
	}
	{
		uint8_t L_39 = V_4;
		int32_t L_40 = __this->get_BlockSizeByte_2();
		if ((((int32_t)L_39) <= ((int32_t)L_40)))
		{
			goto IL_0119;
		}
	}

IL_0105:
	{
		SymmetricAlgorithm_t4254223087 * L_41 = __this->get_algo_0();
		NullCheck(L_41);
		int32_t L_42 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::get_Padding() */, L_41);
		uint8_t L_43 = V_4;
		SymmetricTransform_ThrowBadPaddingException_m2898061954(__this, L_42, L_43, (-1), /*hidden argument*/NULL);
	}

IL_0119:
	{
		uint8_t L_44 = V_4;
		V_5 = ((int32_t)((int32_t)L_44-(int32_t)1));
		goto IL_014b;
	}

IL_0124:
	{
		ByteU5BU5D_t4116647657* L_45 = V_1;
		int32_t L_46 = V_0;
		int32_t L_47 = V_5;
		NullCheck(L_45);
		int32_t L_48 = ((int32_t)((int32_t)((int32_t)((int32_t)L_46-(int32_t)1))-(int32_t)L_47));
		uint8_t L_49 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		if (!L_49)
		{
			goto IL_0145;
		}
	}
	{
		SymmetricAlgorithm_t4254223087 * L_50 = __this->get_algo_0();
		NullCheck(L_50);
		int32_t L_51 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::get_Padding() */, L_50);
		int32_t L_52 = V_5;
		SymmetricTransform_ThrowBadPaddingException_m2898061954(__this, L_51, (-1), L_52, /*hidden argument*/NULL);
	}

IL_0145:
	{
		int32_t L_53 = V_5;
		V_5 = ((int32_t)((int32_t)L_53-(int32_t)1));
	}

IL_014b:
	{
		int32_t L_54 = V_5;
		if ((((int32_t)L_54) > ((int32_t)0)))
		{
			goto IL_0124;
		}
	}
	{
		int32_t L_55 = V_0;
		uint8_t L_56 = V_4;
		V_0 = ((int32_t)((int32_t)L_55-(int32_t)L_56));
		goto IL_0202;
	}

IL_015d:
	{
		uint8_t L_57 = V_4;
		if (!L_57)
		{
			goto IL_0171;
		}
	}
	{
		uint8_t L_58 = V_4;
		int32_t L_59 = __this->get_BlockSizeByte_2();
		if ((((int32_t)L_58) <= ((int32_t)L_59)))
		{
			goto IL_0185;
		}
	}

IL_0171:
	{
		SymmetricAlgorithm_t4254223087 * L_60 = __this->get_algo_0();
		NullCheck(L_60);
		int32_t L_61 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::get_Padding() */, L_60);
		uint8_t L_62 = V_4;
		SymmetricTransform_ThrowBadPaddingException_m2898061954(__this, L_61, L_62, (-1), /*hidden argument*/NULL);
	}

IL_0185:
	{
		int32_t L_63 = V_0;
		uint8_t L_64 = V_4;
		V_0 = ((int32_t)((int32_t)L_63-(int32_t)L_64));
		goto IL_0202;
	}

IL_018f:
	{
		uint8_t L_65 = V_4;
		if (!L_65)
		{
			goto IL_01a3;
		}
	}
	{
		uint8_t L_66 = V_4;
		int32_t L_67 = __this->get_BlockSizeByte_2();
		if ((((int32_t)L_66) <= ((int32_t)L_67)))
		{
			goto IL_01b7;
		}
	}

IL_01a3:
	{
		SymmetricAlgorithm_t4254223087 * L_68 = __this->get_algo_0();
		NullCheck(L_68);
		int32_t L_69 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::get_Padding() */, L_68);
		uint8_t L_70 = V_4;
		SymmetricTransform_ThrowBadPaddingException_m2898061954(__this, L_69, L_70, (-1), /*hidden argument*/NULL);
	}

IL_01b7:
	{
		uint8_t L_71 = V_4;
		V_6 = ((int32_t)((int32_t)L_71-(int32_t)1));
		goto IL_01eb;
	}

IL_01c2:
	{
		ByteU5BU5D_t4116647657* L_72 = V_1;
		int32_t L_73 = V_0;
		int32_t L_74 = V_6;
		NullCheck(L_72);
		int32_t L_75 = ((int32_t)((int32_t)((int32_t)((int32_t)L_73-(int32_t)1))-(int32_t)L_74));
		uint8_t L_76 = (L_72)->GetAt(static_cast<il2cpp_array_size_t>(L_75));
		uint8_t L_77 = V_4;
		if ((((int32_t)L_76) == ((int32_t)L_77)))
		{
			goto IL_01e5;
		}
	}
	{
		SymmetricAlgorithm_t4254223087 * L_78 = __this->get_algo_0();
		NullCheck(L_78);
		int32_t L_79 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::get_Padding() */, L_78);
		int32_t L_80 = V_6;
		SymmetricTransform_ThrowBadPaddingException_m2898061954(__this, L_79, (-1), L_80, /*hidden argument*/NULL);
	}

IL_01e5:
	{
		int32_t L_81 = V_6;
		V_6 = ((int32_t)((int32_t)L_81-(int32_t)1));
	}

IL_01eb:
	{
		int32_t L_82 = V_6;
		if ((((int32_t)L_82) > ((int32_t)0)))
		{
			goto IL_01c2;
		}
	}
	{
		int32_t L_83 = V_0;
		uint8_t L_84 = V_4;
		V_0 = ((int32_t)((int32_t)L_83-(int32_t)L_84));
		goto IL_0202;
	}

IL_01fd:
	{
		goto IL_0202;
	}

IL_0202:
	{
		int32_t L_85 = V_0;
		if ((((int32_t)L_85) <= ((int32_t)0)))
		{
			goto IL_0229;
		}
	}
	{
		int32_t L_86 = V_0;
		V_7 = ((ByteU5BU5D_t4116647657*)SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, (uint32_t)L_86));
		ByteU5BU5D_t4116647657* L_87 = V_1;
		ByteU5BU5D_t4116647657* L_88 = V_7;
		int32_t L_89 = V_0;
		Buffer_BlockCopy_m2884209081(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_87, 0, (Il2CppArray *)(Il2CppArray *)L_88, 0, L_89, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_90 = V_1;
		ByteU5BU5D_t4116647657* L_91 = V_1;
		NullCheck(L_91);
		Array_Clear_m2231608178(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_90, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_91)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_92 = V_7;
		return L_92;
	}

IL_0229:
	{
		return ((ByteU5BU5D_t4116647657*)SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, (uint32_t)0));
	}
}
// System.Byte[] Mono.Security.Cryptography.SymmetricTransform::TransformFinalBlock(System.Byte[],System.Int32,System.Int32)
extern "C"  ByteU5BU5D_t4116647657* SymmetricTransform_TransformFinalBlock_m1030888689 (SymmetricTransform_t3802591843 * __this, ByteU5BU5D_t4116647657* ___inputBuffer0, int32_t ___inputOffset1, int32_t ___inputCount2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SymmetricTransform_TransformFinalBlock_m1030888689_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_m_disposed_9();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ObjectDisposedException_t21392786 * L_1 = (ObjectDisposedException_t21392786 *)il2cpp_codegen_object_new(ObjectDisposedException_t21392786_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3603759869(L_1, _stringLiteral389898510, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		ByteU5BU5D_t4116647657* L_2 = ___inputBuffer0;
		int32_t L_3 = ___inputOffset1;
		int32_t L_4 = ___inputCount2;
		SymmetricTransform_CheckInput_m2092289040(__this, L_2, L_3, L_4, /*hidden argument*/NULL);
		bool L_5 = __this->get_encrypt_1();
		if (!L_5)
		{
			goto IL_0034;
		}
	}
	{
		ByteU5BU5D_t4116647657* L_6 = ___inputBuffer0;
		int32_t L_7 = ___inputOffset1;
		int32_t L_8 = ___inputCount2;
		ByteU5BU5D_t4116647657* L_9 = SymmetricTransform_FinalEncrypt_m748885414(__this, L_6, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_0034:
	{
		ByteU5BU5D_t4116647657* L_10 = ___inputBuffer0;
		int32_t L_11 = ___inputOffset1;
		int32_t L_12 = ___inputCount2;
		ByteU5BU5D_t4116647657* L_13 = SymmetricTransform_FinalDecrypt_m764004682(__this, L_10, L_11, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  void DelegatePInvokeWrapper_Action_t1264377477 (Action_t1264377477 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
extern "C"  void Action__ctor_m2994342681 (Action_t1264377477 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action::Invoke()
extern "C"  void Action_Invoke_m937035532 (Action_t1264377477 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_Invoke_m937035532((Action_t1264377477 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_BeginInvoke_m2907948038 (Action_t1264377477 * __this, AsyncCallback_t3962456242 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void System.Action::EndInvoke(System.IAsyncResult)
extern "C"  void Action_EndInvoke_m1690492879 (Action_t1264377477 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.IO.Pipes.NamedPipeServerStream::.ctor(System.String,System.IO.Pipes.PipeDirection)
extern "C"  void NamedPipeServerStream__ctor_m2626719760 (NamedPipeServerStream_t125618231 * __this, String_t* ___pipeName0, int32_t ___direction1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___pipeName0;
		int32_t L_1 = ___direction1;
		NamedPipeServerStream__ctor_m3566243057(__this, L_0, L_1, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.IO.Pipes.NamedPipeServerStream::.ctor(System.String,System.IO.Pipes.PipeDirection,System.Int32)
extern "C"  void NamedPipeServerStream__ctor_m3566243057 (NamedPipeServerStream_t125618231 * __this, String_t* ___pipeName0, int32_t ___direction1, int32_t ___maxNumberOfServerInstances2, const MethodInfo* method)
{
	{
		String_t* L_0 = ___pipeName0;
		int32_t L_1 = ___direction1;
		int32_t L_2 = ___maxNumberOfServerInstances2;
		NamedPipeServerStream__ctor_m2947084898(__this, L_0, L_1, L_2, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.IO.Pipes.NamedPipeServerStream::.ctor(System.String,System.IO.Pipes.PipeDirection,System.Int32,System.IO.Pipes.PipeTransmissionMode)
extern "C"  void NamedPipeServerStream__ctor_m2947084898 (NamedPipeServerStream_t125618231 * __this, String_t* ___pipeName0, int32_t ___direction1, int32_t ___maxNumberOfServerInstances2, int32_t ___transmissionMode3, const MethodInfo* method)
{
	{
		String_t* L_0 = ___pipeName0;
		int32_t L_1 = ___direction1;
		int32_t L_2 = ___maxNumberOfServerInstances2;
		int32_t L_3 = ___transmissionMode3;
		NamedPipeServerStream__ctor_m1922895012(__this, L_0, L_1, L_2, L_3, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.IO.Pipes.NamedPipeServerStream::.ctor(System.String,System.IO.Pipes.PipeDirection,System.Int32,System.IO.Pipes.PipeTransmissionMode,System.IO.Pipes.PipeOptions)
extern "C"  void NamedPipeServerStream__ctor_m1922895012 (NamedPipeServerStream_t125618231 * __this, String_t* ___pipeName0, int32_t ___direction1, int32_t ___maxNumberOfServerInstances2, int32_t ___transmissionMode3, int32_t ___options4, const MethodInfo* method)
{
	{
		String_t* L_0 = ___pipeName0;
		int32_t L_1 = ___direction1;
		int32_t L_2 = ___maxNumberOfServerInstances2;
		int32_t L_3 = ___transmissionMode3;
		int32_t L_4 = ___options4;
		NamedPipeServerStream__ctor_m3695614284(__this, L_0, L_1, L_2, L_3, L_4, ((int32_t)1024), ((int32_t)1024), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.IO.Pipes.NamedPipeServerStream::.ctor(System.String,System.IO.Pipes.PipeDirection,System.Int32,System.IO.Pipes.PipeTransmissionMode,System.IO.Pipes.PipeOptions,System.Int32,System.Int32)
extern "C"  void NamedPipeServerStream__ctor_m3695614284 (NamedPipeServerStream_t125618231 * __this, String_t* ___pipeName0, int32_t ___direction1, int32_t ___maxNumberOfServerInstances2, int32_t ___transmissionMode3, int32_t ___options4, int32_t ___inBufferSize5, int32_t ___outBufferSize6, const MethodInfo* method)
{
	{
		String_t* L_0 = ___pipeName0;
		int32_t L_1 = ___direction1;
		int32_t L_2 = ___maxNumberOfServerInstances2;
		int32_t L_3 = ___transmissionMode3;
		int32_t L_4 = ___options4;
		int32_t L_5 = ___inBufferSize5;
		int32_t L_6 = ___outBufferSize6;
		NamedPipeServerStream__ctor_m2847058098(__this, L_0, L_1, L_2, L_3, L_4, L_5, L_6, (PipeSecurity_t3265965420 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.IO.Pipes.NamedPipeServerStream::.ctor(System.String,System.IO.Pipes.PipeDirection,System.Int32,System.IO.Pipes.PipeTransmissionMode,System.IO.Pipes.PipeOptions,System.Int32,System.Int32,System.IO.Pipes.PipeSecurity)
extern "C"  void NamedPipeServerStream__ctor_m2847058098 (NamedPipeServerStream_t125618231 * __this, String_t* ___pipeName0, int32_t ___direction1, int32_t ___maxNumberOfServerInstances2, int32_t ___transmissionMode3, int32_t ___options4, int32_t ___inBufferSize5, int32_t ___outBufferSize6, PipeSecurity_t3265965420 * ___pipeSecurity7, const MethodInfo* method)
{
	{
		String_t* L_0 = ___pipeName0;
		int32_t L_1 = ___direction1;
		int32_t L_2 = ___maxNumberOfServerInstances2;
		int32_t L_3 = ___transmissionMode3;
		int32_t L_4 = ___options4;
		int32_t L_5 = ___inBufferSize5;
		int32_t L_6 = ___outBufferSize6;
		PipeSecurity_t3265965420 * L_7 = ___pipeSecurity7;
		NamedPipeServerStream__ctor_m1022744216(__this, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.IO.Pipes.NamedPipeServerStream::.ctor(System.String,System.IO.Pipes.PipeDirection,System.Int32,System.IO.Pipes.PipeTransmissionMode,System.IO.Pipes.PipeOptions,System.Int32,System.Int32,System.IO.Pipes.PipeSecurity,System.IO.HandleInheritability)
extern "C"  void NamedPipeServerStream__ctor_m1022744216 (NamedPipeServerStream_t125618231 * __this, String_t* ___pipeName0, int32_t ___direction1, int32_t ___maxNumberOfServerInstances2, int32_t ___transmissionMode3, int32_t ___options4, int32_t ___inBufferSize5, int32_t ___outBufferSize6, PipeSecurity_t3265965420 * ___pipeSecurity7, int32_t ___inheritability8, const MethodInfo* method)
{
	{
		String_t* L_0 = ___pipeName0;
		int32_t L_1 = ___direction1;
		int32_t L_2 = ___maxNumberOfServerInstances2;
		int32_t L_3 = ___transmissionMode3;
		int32_t L_4 = ___options4;
		int32_t L_5 = ___inBufferSize5;
		int32_t L_6 = ___outBufferSize6;
		PipeSecurity_t3265965420 * L_7 = ___pipeSecurity7;
		int32_t L_8 = ___inheritability8;
		NamedPipeServerStream__ctor_m2421472725(__this, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, L_8, 3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.IO.Pipes.NamedPipeServerStream::.ctor(System.String,System.IO.Pipes.PipeDirection,System.Int32,System.IO.Pipes.PipeTransmissionMode,System.IO.Pipes.PipeOptions,System.Int32,System.Int32,System.IO.Pipes.PipeSecurity,System.IO.HandleInheritability,System.IO.Pipes.PipeAccessRights)
extern "C"  void NamedPipeServerStream__ctor_m2421472725 (NamedPipeServerStream_t125618231 * __this, String_t* ___pipeName0, int32_t ___direction1, int32_t ___maxNumberOfServerInstances2, int32_t ___transmissionMode3, int32_t ___options4, int32_t ___inBufferSize5, int32_t ___outBufferSize6, PipeSecurity_t3265965420 * ___pipeSecurity7, int32_t ___inheritability8, int32_t ___additionalAccessRights9, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NamedPipeServerStream__ctor_m2421472725_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___direction1;
		int32_t L_1 = ___transmissionMode3;
		int32_t L_2 = ___outBufferSize6;
		PipeStream__ctor_m3222674958(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		PipeSecurity_t3265965420 * L_3 = ___pipeSecurity7;
		if (!L_3)
		{
			goto IL_0019;
		}
	}
	{
		Exception_t1436737249 * L_4 = PipeStream_ThrowACLException_m4220209091(__this, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0019:
	{
		int32_t L_5 = ___direction1;
		int32_t L_6 = PipeStream_ToAccessRights_m3500803338(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		int32_t L_7 = ___additionalAccessRights9;
		V_0 = ((int32_t)((int32_t)L_6|(int32_t)L_7));
		bool L_8 = PipeStream_get_IsWindows_m2836627059(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004b;
		}
	}
	{
		String_t* L_9 = ___pipeName0;
		int32_t L_10 = ___maxNumberOfServerInstances2;
		int32_t L_11 = ___transmissionMode3;
		int32_t L_12 = V_0;
		int32_t L_13 = ___options4;
		int32_t L_14 = ___inBufferSize5;
		int32_t L_15 = ___outBufferSize6;
		int32_t L_16 = ___inheritability8;
		Win32NamedPipeServer_t774293249 * L_17 = (Win32NamedPipeServer_t774293249 *)il2cpp_codegen_object_new(Win32NamedPipeServer_t774293249_il2cpp_TypeInfo_var);
		Win32NamedPipeServer__ctor_m442731582(L_17, __this, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, /*hidden argument*/NULL);
		__this->set_impl_13(L_17);
		goto IL_0064;
	}

IL_004b:
	{
		String_t* L_18 = ___pipeName0;
		int32_t L_19 = ___maxNumberOfServerInstances2;
		int32_t L_20 = ___transmissionMode3;
		int32_t L_21 = V_0;
		int32_t L_22 = ___options4;
		int32_t L_23 = ___inBufferSize5;
		int32_t L_24 = ___outBufferSize6;
		int32_t L_25 = ___inheritability8;
		UnixNamedPipeServer_t2839746932 * L_26 = (UnixNamedPipeServer_t2839746932 *)il2cpp_codegen_object_new(UnixNamedPipeServer_t2839746932_il2cpp_TypeInfo_var);
		UnixNamedPipeServer__ctor_m1228519622(L_26, __this, L_18, L_19, L_20, L_21, L_22, L_23, L_24, L_25, /*hidden argument*/NULL);
		__this->set_impl_13(L_26);
	}

IL_0064:
	{
		Il2CppObject * L_27 = __this->get_impl_13();
		NullCheck(L_27);
		SafePipeHandle_t1989113880 * L_28 = InterfaceFuncInvoker0< SafePipeHandle_t1989113880 * >::Invoke(0 /* Microsoft.Win32.SafeHandles.SafePipeHandle System.IO.Pipes.IPipe::get_Handle() */, IPipe_t4211097707_il2cpp_TypeInfo_var, L_27);
		int32_t L_29 = ___options4;
		PipeStream_InitializeHandle_m2111621070(__this, L_28, (bool)0, (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_29&(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.IO.Pipes.NamedPipeServerStream::WaitForConnection()
extern "C"  void NamedPipeServerStream_WaitForConnection_m3070979034 (NamedPipeServerStream_t125618231 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NamedPipeServerStream_WaitForConnection_m3070979034_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_impl_13();
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IO.Pipes.INamedPipeServer::WaitForConnection() */, INamedPipeServer_t304076007_il2cpp_TypeInfo_var, L_0);
		PipeStream_set_IsConnected_m1407048896(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.IO.Pipes.PipeStream::.ctor(System.IO.Pipes.PipeDirection,System.IO.Pipes.PipeTransmissionMode,System.Int32)
extern "C"  void PipeStream__ctor_m3222674958 (PipeStream_t871053121 * __this, int32_t ___direction0, int32_t ___transmissionMode1, int32_t ___outBufferSize2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PipeStream__ctor_m3222674958_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Stream_t1273022909_il2cpp_TypeInfo_var);
		Stream__ctor_m3881936881(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___direction0;
		__this->set_direction_2(L_0);
		int32_t L_1 = ___transmissionMode1;
		__this->set_transmission_mode_3(L_1);
		int32_t L_2 = ___transmissionMode1;
		__this->set_read_trans_mode_4(L_2);
		int32_t L_3 = ___outBufferSize2;
		if ((((int32_t)L_3) > ((int32_t)0)))
		{
			goto IL_002d;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_4 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_4, _stringLiteral4274914650, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___outBufferSize2;
		__this->set_buffer_size_5(L_5);
		return;
	}
}
// System.Boolean System.IO.Pipes.PipeStream::get_IsWindows()
extern "C"  bool PipeStream_get_IsWindows_m2836627059 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		bool L_0 = Win32Marshal_get_IsWindows_m4212098367(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Exception System.IO.Pipes.PipeStream::ThrowACLException()
extern "C"  Exception_t1436737249 * PipeStream_ThrowACLException_m4220209091 (PipeStream_t871053121 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PipeStream_ThrowACLException_m4220209091_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t3489357830 * L_0 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3095902440(L_0, _stringLiteral1997704143, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.IO.Pipes.PipeAccessRights System.IO.Pipes.PipeStream::ToAccessRights(System.IO.Pipes.PipeDirection)
extern "C"  int32_t PipeStream_ToAccessRights_m3500803338 (Il2CppObject * __this /* static, unused */, int32_t ___direction0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PipeStream_ToAccessRights_m3500803338_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___direction0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (((int32_t)((int32_t)L_1-(int32_t)1)))
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_001d;
			}
			case 2:
			{
				goto IL_001f;
			}
		}
	}
	{
		goto IL_0021;
	}

IL_001b:
	{
		return (int32_t)(1);
	}

IL_001d:
	{
		return (int32_t)(2);
	}

IL_001f:
	{
		return (int32_t)(3);
	}

IL_0021:
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2047740448(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}
}
// System.Boolean System.IO.Pipes.PipeStream::get_CanRead()
extern "C"  bool PipeStream_get_CanRead_m4288404083 (PipeStream_t871053121 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_direction_2();
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)1))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean System.IO.Pipes.PipeStream::get_CanSeek()
extern "C"  bool PipeStream_get_CanSeek_m1296939946 (PipeStream_t871053121 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.IO.Pipes.PipeStream::get_CanWrite()
extern "C"  bool PipeStream_get_CanWrite_m883926572 (PipeStream_t871053121 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_direction_2();
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean System.IO.Pipes.PipeStream::get_IsAsync()
extern "C"  bool PipeStream_get_IsAsync_m1359144229 (PipeStream_t871053121 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CIsAsyncU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void System.IO.Pipes.PipeStream::set_IsAsync(System.Boolean)
extern "C"  void PipeStream_set_IsAsync_m3945863678 (PipeStream_t871053121 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CIsAsyncU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.Boolean System.IO.Pipes.PipeStream::get_IsConnected()
extern "C"  bool PipeStream_get_IsConnected_m1046775879 (PipeStream_t871053121 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CIsConnectedU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void System.IO.Pipes.PipeStream::set_IsConnected(System.Boolean)
extern "C"  void PipeStream_set_IsConnected_m1407048896 (PipeStream_t871053121 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CIsConnectedU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.IO.Stream System.IO.Pipes.PipeStream::get_Stream()
extern "C"  Stream_t1273022909 * PipeStream_get_Stream_m2042017473 (PipeStream_t871053121 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PipeStream_get_Stream_m2042017473_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IntPtr_t G_B8_0;
	memset(&G_B8_0, 0, sizeof(G_B8_0));
	PipeStream_t871053121 * G_B8_1 = NULL;
	IntPtr_t G_B4_0;
	memset(&G_B4_0, 0, sizeof(G_B4_0));
	PipeStream_t871053121 * G_B4_1 = NULL;
	IntPtr_t G_B6_0;
	memset(&G_B6_0, 0, sizeof(G_B6_0));
	PipeStream_t871053121 * G_B6_1 = NULL;
	IntPtr_t G_B5_0;
	memset(&G_B5_0, 0, sizeof(G_B5_0));
	PipeStream_t871053121 * G_B5_1 = NULL;
	int32_t G_B7_0 = 0;
	IntPtr_t G_B7_1;
	memset(&G_B7_1, 0, sizeof(G_B7_1));
	PipeStream_t871053121 * G_B7_2 = NULL;
	int32_t G_B9_0 = 0;
	IntPtr_t G_B9_1;
	memset(&G_B9_1, 0, sizeof(G_B9_1));
	PipeStream_t871053121 * G_B9_2 = NULL;
	{
		bool L_0 = PipeStream_get_IsConnected_m1046775879(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, _stringLiteral508036669, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Stream_t1273022909 * L_2 = __this->get_stream_7();
		if (L_2)
		{
			goto IL_0067;
		}
	}
	{
		SafePipeHandle_t1989113880 * L_3 = __this->get_handle_6();
		NullCheck(L_3);
		IntPtr_t L_4 = SafeHandle_DangerousGetHandle_m3697436134(L_3, /*hidden argument*/NULL);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.IO.Pipes.PipeStream::get_CanRead() */, __this);
		G_B4_0 = L_4;
		G_B4_1 = __this;
		if (!L_5)
		{
			G_B8_0 = L_4;
			G_B8_1 = __this;
			goto IL_004f;
		}
	}
	{
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean System.IO.Pipes.PipeStream::get_CanWrite() */, __this);
		G_B5_0 = G_B4_0;
		G_B5_1 = G_B4_1;
		if (!L_6)
		{
			G_B6_0 = G_B4_0;
			G_B6_1 = G_B4_1;
			goto IL_0049;
		}
	}
	{
		G_B7_0 = 3;
		G_B7_1 = G_B5_0;
		G_B7_2 = G_B5_1;
		goto IL_004a;
	}

IL_0049:
	{
		G_B7_0 = 1;
		G_B7_1 = G_B6_0;
		G_B7_2 = G_B6_1;
	}

IL_004a:
	{
		G_B9_0 = G_B7_0;
		G_B9_1 = G_B7_1;
		G_B9_2 = G_B7_2;
		goto IL_0050;
	}

IL_004f:
	{
		G_B9_0 = 2;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
	}

IL_0050:
	{
		int32_t L_7 = __this->get_buffer_size_5();
		bool L_8 = PipeStream_get_IsAsync_m1359144229(__this, /*hidden argument*/NULL);
		FileStream_t4292183065 * L_9 = (FileStream_t4292183065 *)il2cpp_codegen_object_new(FileStream_t4292183065_il2cpp_TypeInfo_var);
		FileStream__ctor_m637495152(L_9, G_B9_1, G_B9_0, (bool)1, L_7, L_8, /*hidden argument*/NULL);
		NullCheck(G_B9_2);
		G_B9_2->set_stream_7(L_9);
	}

IL_0067:
	{
		Stream_t1273022909 * L_10 = __this->get_stream_7();
		return L_10;
	}
}
// System.Void System.IO.Pipes.PipeStream::set_Stream(System.IO.Stream)
extern "C"  void PipeStream_set_Stream_m1574685589 (PipeStream_t871053121 * __this, Stream_t1273022909 * ___value0, const MethodInfo* method)
{
	{
		Stream_t1273022909 * L_0 = ___value0;
		__this->set_stream_7(L_0);
		return;
	}
}
// System.Void System.IO.Pipes.PipeStream::set_IsHandleExposed(System.Boolean)
extern "C"  void PipeStream_set_IsHandleExposed_m4119048121 (PipeStream_t871053121 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CIsHandleExposedU3Ek__BackingField_12(L_0);
		return;
	}
}
// System.IO.Pipes.PipeTransmissionMode System.IO.Pipes.PipeStream::get_TransmissionMode()
extern "C"  int32_t PipeStream_get_TransmissionMode_m1552493529 (PipeStream_t871053121 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(29 /* System.Void System.IO.Pipes.PipeStream::CheckPipePropertyOperations() */, __this);
		int32_t L_0 = __this->get_transmission_mode_3();
		return L_0;
	}
}
// System.Void System.IO.Pipes.PipeStream::CheckPipePropertyOperations()
extern "C"  void PipeStream_CheckPipePropertyOperations_m439803104 (PipeStream_t871053121 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void System.IO.Pipes.PipeStream::CheckReadOperations()
extern "C"  void PipeStream_CheckReadOperations_m2917579478 (PipeStream_t871053121 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PipeStream_CheckReadOperations_m2917579478_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = PipeStream_get_IsConnected_m1046775879(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, _stringLiteral508036669, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.IO.Pipes.PipeStream::get_CanRead() */, __this);
		if (L_2)
		{
			goto IL_002c;
		}
	}
	{
		NotSupportedException_t1314879016 * L_3 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2494070935(L_3, _stringLiteral1491677019, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002c:
	{
		return;
	}
}
// System.Void System.IO.Pipes.PipeStream::CheckWriteOperations()
extern "C"  void PipeStream_CheckWriteOperations_m2156193142 (PipeStream_t871053121 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PipeStream_CheckWriteOperations_m2156193142_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = PipeStream_get_IsConnected_m1046775879(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_1, _stringLiteral1061946625, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean System.IO.Pipes.PipeStream::get_CanWrite() */, __this);
		if (L_2)
		{
			goto IL_002c;
		}
	}
	{
		NotSupportedException_t1314879016 * L_3 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2494070935(L_3, _stringLiteral2707246090, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002c:
	{
		return;
	}
}
// System.Void System.IO.Pipes.PipeStream::InitializeHandle(Microsoft.Win32.SafeHandles.SafePipeHandle,System.Boolean,System.Boolean)
extern "C"  void PipeStream_InitializeHandle_m2111621070 (PipeStream_t871053121 * __this, SafePipeHandle_t1989113880 * ___handle0, bool ___isExposed1, bool ___isAsync2, const MethodInfo* method)
{
	{
		SafePipeHandle_t1989113880 * L_0 = ___handle0;
		__this->set_handle_6(L_0);
		bool L_1 = ___isExposed1;
		PipeStream_set_IsHandleExposed_m4119048121(__this, L_1, /*hidden argument*/NULL);
		bool L_2 = ___isAsync2;
		PipeStream_set_IsAsync_m3945863678(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.IO.Pipes.PipeStream::Dispose(System.Boolean)
extern "C"  void PipeStream_Dispose_m4189814804 (PipeStream_t871053121 * __this, bool ___disposing0, const MethodInfo* method)
{
	{
		SafePipeHandle_t1989113880 * L_0 = __this->get_handle_6();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		bool L_1 = ___disposing0;
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		SafePipeHandle_t1989113880 * L_2 = __this->get_handle_6();
		NullCheck(L_2);
		SafeHandle_Dispose_m817995135(L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Int64 System.IO.Pipes.PipeStream::get_Length()
extern "C"  int64_t PipeStream_get_Length_m2006706043 (PipeStream_t871053121 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PipeStream_get_Length_m2006706043_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int64 System.IO.Pipes.PipeStream::get_Position()
extern "C"  int64_t PipeStream_get_Position_m2156426846 (PipeStream_t871053121 * __this, const MethodInfo* method)
{
	{
		return (((int64_t)((int64_t)0)));
	}
}
// System.Void System.IO.Pipes.PipeStream::set_Position(System.Int64)
extern "C"  void PipeStream_set_Position_m2639358827 (PipeStream_t871053121 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PipeStream_set_Position_m2639358827_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.IO.Pipes.PipeStream::SetLength(System.Int64)
extern "C"  void PipeStream_SetLength_m1130662416 (PipeStream_t871053121 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PipeStream_SetLength_m1130662416_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int64 System.IO.Pipes.PipeStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C"  int64_t PipeStream_Seek_m3944388383 (PipeStream_t871053121 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PipeStream_Seek_m3944388383_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.IO.Pipes.PipeStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t PipeStream_Read_m2095920545 (PipeStream_t871053121 * __this, ByteU5BU5D_t4116647657* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	{
		PipeStream_CheckReadOperations_m2917579478(__this, /*hidden argument*/NULL);
		Stream_t1273022909 * L_0 = PipeStream_get_Stream_m2042017473(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_1 = ___buffer0;
		int32_t L_2 = ___offset1;
		int32_t L_3 = ___count2;
		NullCheck(L_0);
		int32_t L_4 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t4116647657*, int32_t, int32_t >::Invoke(18 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_0, L_1, L_2, L_3);
		return L_4;
	}
}
// System.Int32 System.IO.Pipes.PipeStream::ReadByte()
extern "C"  int32_t PipeStream_ReadByte_m1726760648 (PipeStream_t871053121 * __this, const MethodInfo* method)
{
	{
		PipeStream_CheckReadOperations_m2917579478(__this, /*hidden argument*/NULL);
		Stream_t1273022909 * L_0 = PipeStream_get_Stream_m2042017473(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 System.IO.Stream::ReadByte() */, L_0);
		return L_1;
	}
}
// System.Void System.IO.Pipes.PipeStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void PipeStream_Write_m215988007 (PipeStream_t871053121 * __this, ByteU5BU5D_t4116647657* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	{
		PipeStream_CheckWriteOperations_m2156193142(__this, /*hidden argument*/NULL);
		Stream_t1273022909 * L_0 = PipeStream_get_Stream_m2042017473(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_1 = ___buffer0;
		int32_t L_2 = ___offset1;
		int32_t L_3 = ___count2;
		NullCheck(L_0);
		VirtActionInvoker3< ByteU5BU5D_t4116647657*, int32_t, int32_t >::Invoke(22 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_0, L_1, L_2, L_3);
		return;
	}
}
// System.Void System.IO.Pipes.PipeStream::WriteByte(System.Byte)
extern "C"  void PipeStream_WriteByte_m453919135 (PipeStream_t871053121 * __this, uint8_t ___value0, const MethodInfo* method)
{
	{
		PipeStream_CheckWriteOperations_m2156193142(__this, /*hidden argument*/NULL);
		Stream_t1273022909 * L_0 = PipeStream_get_Stream_m2042017473(__this, /*hidden argument*/NULL);
		uint8_t L_1 = ___value0;
		NullCheck(L_0);
		VirtActionInvoker1< uint8_t >::Invoke(23 /* System.Void System.IO.Stream::WriteByte(System.Byte) */, L_0, L_1);
		return;
	}
}
// System.Void System.IO.Pipes.PipeStream::Flush()
extern "C"  void PipeStream_Flush_m1031396102 (PipeStream_t871053121 * __this, const MethodInfo* method)
{
	{
		PipeStream_CheckWriteOperations_m2156193142(__this, /*hidden argument*/NULL);
		Stream_t1273022909 * L_0 = PipeStream_get_Stream_m2042017473(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(17 /* System.Void System.IO.Stream::Flush() */, L_0);
		return;
	}
}
// System.IAsyncResult System.IO.Pipes.PipeStream::BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * PipeStream_BeginRead_m1326353978 (PipeStream_t871053121 * __this, ByteU5BU5D_t4116647657* ___buffer0, int32_t ___offset1, int32_t ___count2, AsyncCallback_t3962456242 * ___callback3, Il2CppObject * ___state4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PipeStream_BeginRead_m1326353978_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Func_4_t752956844 * L_0 = __this->get_read_delegate_8();
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)GetVirtualMethodInfo(__this, 18));
		Func_4_t752956844 * L_2 = (Func_4_t752956844 *)il2cpp_codegen_object_new(Func_4_t752956844_il2cpp_TypeInfo_var);
		Func_4__ctor_m2755641909(L_2, __this, L_1, /*hidden argument*/Func_4__ctor_m2755641909_MethodInfo_var);
		__this->set_read_delegate_8(L_2);
	}

IL_001e:
	{
		Func_4_t752956844 * L_3 = __this->get_read_delegate_8();
		ByteU5BU5D_t4116647657* L_4 = ___buffer0;
		int32_t L_5 = ___offset1;
		int32_t L_6 = ___count2;
		AsyncCallback_t3962456242 * L_7 = ___callback3;
		Il2CppObject * L_8 = ___state4;
		NullCheck(L_3);
		Il2CppObject * L_9 = Func_4_BeginInvoke_m1913839697(L_3, L_4, L_5, L_6, L_7, L_8, /*hidden argument*/Func_4_BeginInvoke_m1913839697_MethodInfo_var);
		return L_9;
	}
}
// System.IAsyncResult System.IO.Pipes.PipeStream::BeginWrite(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * PipeStream_BeginWrite_m1039560251 (PipeStream_t871053121 * __this, ByteU5BU5D_t4116647657* ___buffer0, int32_t ___offset1, int32_t ___count2, AsyncCallback_t3962456242 * ___callback3, Il2CppObject * ___state4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PipeStream_BeginWrite_m1039560251_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_3_t1284011258 * L_0 = __this->get_write_delegate_9();
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)GetVirtualMethodInfo(__this, 22));
		Action_3_t1284011258 * L_2 = (Action_3_t1284011258 *)il2cpp_codegen_object_new(Action_3_t1284011258_il2cpp_TypeInfo_var);
		Action_3__ctor_m3554724437(L_2, __this, L_1, /*hidden argument*/Action_3__ctor_m3554724437_MethodInfo_var);
		__this->set_write_delegate_9(L_2);
	}

IL_001e:
	{
		Action_3_t1284011258 * L_3 = __this->get_write_delegate_9();
		ByteU5BU5D_t4116647657* L_4 = ___buffer0;
		int32_t L_5 = ___offset1;
		int32_t L_6 = ___count2;
		AsyncCallback_t3962456242 * L_7 = ___callback3;
		Il2CppObject * L_8 = ___state4;
		NullCheck(L_3);
		Il2CppObject * L_9 = Action_3_BeginInvoke_m392137494(L_3, L_4, L_5, L_6, L_7, L_8, /*hidden argument*/Action_3_BeginInvoke_m392137494_MethodInfo_var);
		return L_9;
	}
}
// System.Int32 System.IO.Pipes.PipeStream::EndRead(System.IAsyncResult)
extern "C"  int32_t PipeStream_EndRead_m2516431970 (PipeStream_t871053121 * __this, Il2CppObject * ___asyncResult0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PipeStream_EndRead_m2516431970_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Func_4_t752956844 * L_0 = __this->get_read_delegate_8();
		Il2CppObject * L_1 = ___asyncResult0;
		NullCheck(L_0);
		int32_t L_2 = Func_4_EndInvoke_m3367563957(L_0, L_1, /*hidden argument*/Func_4_EndInvoke_m3367563957_MethodInfo_var);
		return L_2;
	}
}
// System.Void System.IO.Pipes.PipeStream::EndWrite(System.IAsyncResult)
extern "C"  void PipeStream_EndWrite_m1919876269 (PipeStream_t871053121 * __this, Il2CppObject * ___asyncResult0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PipeStream_EndWrite_m1919876269_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_3_t1284011258 * L_0 = __this->get_write_delegate_9();
		Il2CppObject * L_1 = ___asyncResult0;
		NullCheck(L_0);
		Action_3_EndInvoke_m605455061(L_0, L_1, /*hidden argument*/Action_3_EndInvoke_m605455061_MethodInfo_var);
		return;
	}
}
// Conversion methods for marshalling of: System.IO.Pipes.SecurityAttributesHack
extern "C" void SecurityAttributesHack_t2445122734_marshal_pinvoke(const SecurityAttributesHack_t2445122734& unmarshaled, SecurityAttributesHack_t2445122734_marshaled_pinvoke& marshaled)
{
	marshaled.___Length_0 = unmarshaled.get_Length_0();
	marshaled.___SecurityDescriptor_1 = reinterpret_cast<intptr_t>((unmarshaled.get_SecurityDescriptor_1()).get_m_value_0());
	marshaled.___Inheritable_2 = static_cast<int32_t>(unmarshaled.get_Inheritable_2());
}
extern "C" void SecurityAttributesHack_t2445122734_marshal_pinvoke_back(const SecurityAttributesHack_t2445122734_marshaled_pinvoke& marshaled, SecurityAttributesHack_t2445122734& unmarshaled)
{
	int32_t unmarshaled_Length_temp_0 = 0;
	unmarshaled_Length_temp_0 = marshaled.___Length_0;
	unmarshaled.set_Length_0(unmarshaled_Length_temp_0);
	IntPtr_t unmarshaled_SecurityDescriptor_temp_1;
	memset(&unmarshaled_SecurityDescriptor_temp_1, 0, sizeof(unmarshaled_SecurityDescriptor_temp_1));
	IntPtr_t unmarshaled_SecurityDescriptor_temp_1_temp;
	unmarshaled_SecurityDescriptor_temp_1_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___SecurityDescriptor_1)));
	unmarshaled_SecurityDescriptor_temp_1 = unmarshaled_SecurityDescriptor_temp_1_temp;
	unmarshaled.set_SecurityDescriptor_1(unmarshaled_SecurityDescriptor_temp_1);
	bool unmarshaled_Inheritable_temp_2 = false;
	unmarshaled_Inheritable_temp_2 = static_cast<bool>(marshaled.___Inheritable_2);
	unmarshaled.set_Inheritable_2(unmarshaled_Inheritable_temp_2);
}
// Conversion method for clean up from marshalling of: System.IO.Pipes.SecurityAttributesHack
extern "C" void SecurityAttributesHack_t2445122734_marshal_pinvoke_cleanup(SecurityAttributesHack_t2445122734_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: System.IO.Pipes.SecurityAttributesHack
extern "C" void SecurityAttributesHack_t2445122734_marshal_com(const SecurityAttributesHack_t2445122734& unmarshaled, SecurityAttributesHack_t2445122734_marshaled_com& marshaled)
{
	marshaled.___Length_0 = unmarshaled.get_Length_0();
	marshaled.___SecurityDescriptor_1 = reinterpret_cast<intptr_t>((unmarshaled.get_SecurityDescriptor_1()).get_m_value_0());
	marshaled.___Inheritable_2 = static_cast<int32_t>(unmarshaled.get_Inheritable_2());
}
extern "C" void SecurityAttributesHack_t2445122734_marshal_com_back(const SecurityAttributesHack_t2445122734_marshaled_com& marshaled, SecurityAttributesHack_t2445122734& unmarshaled)
{
	int32_t unmarshaled_Length_temp_0 = 0;
	unmarshaled_Length_temp_0 = marshaled.___Length_0;
	unmarshaled.set_Length_0(unmarshaled_Length_temp_0);
	IntPtr_t unmarshaled_SecurityDescriptor_temp_1;
	memset(&unmarshaled_SecurityDescriptor_temp_1, 0, sizeof(unmarshaled_SecurityDescriptor_temp_1));
	IntPtr_t unmarshaled_SecurityDescriptor_temp_1_temp;
	unmarshaled_SecurityDescriptor_temp_1_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___SecurityDescriptor_1)));
	unmarshaled_SecurityDescriptor_temp_1 = unmarshaled_SecurityDescriptor_temp_1_temp;
	unmarshaled.set_SecurityDescriptor_1(unmarshaled_SecurityDescriptor_temp_1);
	bool unmarshaled_Inheritable_temp_2 = false;
	unmarshaled_Inheritable_temp_2 = static_cast<bool>(marshaled.___Inheritable_2);
	unmarshaled.set_Inheritable_2(unmarshaled_Inheritable_temp_2);
}
// Conversion method for clean up from marshalling of: System.IO.Pipes.SecurityAttributesHack
extern "C" void SecurityAttributesHack_t2445122734_marshal_com_cleanup(SecurityAttributesHack_t2445122734_marshaled_com& marshaled)
{
}
// System.Void System.IO.Pipes.SecurityAttributesHack::.ctor(System.Boolean)
extern "C"  void SecurityAttributesHack__ctor_m2908506643 (SecurityAttributesHack_t2445122734 * __this, bool ___inheritable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SecurityAttributesHack__ctor_m2908506643_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_Length_0(0);
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		__this->set_SecurityDescriptor_1(L_0);
		bool L_1 = ___inheritable0;
		__this->set_Inheritable_2(L_1);
		return;
	}
}
extern "C"  void SecurityAttributesHack__ctor_m2908506643_AdjustorThunk (Il2CppObject * __this, bool ___inheritable0, const MethodInfo* method)
{
	SecurityAttributesHack_t2445122734 * _thisAdjusted = reinterpret_cast<SecurityAttributesHack_t2445122734 *>(__this + 1);
	SecurityAttributesHack__ctor_m2908506643(_thisAdjusted, ___inheritable0, method);
}
// System.Void System.IO.Pipes.UnixNamedPipe::.ctor()
extern "C"  void UnixNamedPipe__ctor_m4176079624 (UnixNamedPipe_t2806256776 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.IO.Pipes.UnixNamedPipe::EnsureTargetFile(System.String)
extern "C"  void UnixNamedPipe_EnsureTargetFile_m962365236 (UnixNamedPipe_t2806256776 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnixNamedPipe_EnsureTargetFile_m962365236_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___name0;
		bool L_1 = File_Exists_m3943585060(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0035;
		}
	}
	{
		String_t* L_2 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(Syscall_t2623000133_il2cpp_TypeInfo_var);
		int32_t L_3 = Syscall_mknod_m3463737514(NULL /*static, unused*/, L_2, ((int32_t)8191), (((int64_t)((int64_t)0))), /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if (!L_4)
		{
			goto IL_0035;
		}
	}
	{
		int32_t L_5 = V_0;
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral3576745504, L_7, /*hidden argument*/NULL);
		IOException_t4088381929 * L_9 = (IOException_t4088381929 *)il2cpp_codegen_object_new(IOException_t4088381929_il2cpp_TypeInfo_var);
		IOException__ctor_m3662782713(L_9, L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.IO.Pipes.UnixNamedPipe::ValidateOptions(System.IO.Pipes.PipeOptions,System.IO.Pipes.PipeTransmissionMode)
extern "C"  void UnixNamedPipe_ValidateOptions_m3897423952 (UnixNamedPipe_t2806256776 * __this, int32_t ___options0, int32_t ___mode1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnixNamedPipe_ValidateOptions_m3897423952_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___options0;
		if (!((int32_t)((int32_t)L_0&(int32_t)1)))
		{
			goto IL_0013;
		}
	}
	{
		NotImplementedException_t3489357830 * L_1 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3095902440(L_1, _stringLiteral312402182, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0013:
	{
		int32_t L_2 = ___mode1;
		if (!((int32_t)((int32_t)L_2&(int32_t)1)))
		{
			goto IL_0026;
		}
	}
	{
		NotImplementedException_t3489357830 * L_3 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3095902440(L_3, _stringLiteral2189701218, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0026:
	{
		int32_t L_4 = ___options0;
		if (!((int32_t)((int32_t)L_4&(int32_t)2)))
		{
			goto IL_0039;
		}
	}
	{
		NotImplementedException_t3489357830 * L_5 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3095902440(L_5, _stringLiteral4120285786, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0039:
	{
		return;
	}
}
// System.String System.IO.Pipes.UnixNamedPipe::RightsToAccess(System.IO.Pipes.PipeAccessRights)
extern "C"  String_t* UnixNamedPipe_RightsToAccess_m3895475835 (UnixNamedPipe_t2806256776 * __this, int32_t ___rights0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnixNamedPipe_RightsToAccess_m3895475835_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		V_0 = (String_t*)NULL;
		int32_t L_0 = ___rights0;
		if (!((int32_t)((int32_t)L_0&(int32_t)1)))
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_1 = ___rights0;
		if (!((int32_t)((int32_t)L_1&(int32_t)2)))
		{
			goto IL_001d;
		}
	}
	{
		V_0 = _stringLiteral3450845134;
		goto IL_0023;
	}

IL_001d:
	{
		V_0 = _stringLiteral3452614606;
	}

IL_0023:
	{
		goto IL_0046;
	}

IL_0028:
	{
		int32_t L_2 = ___rights0;
		if (!((int32_t)((int32_t)L_2&(int32_t)2)))
		{
			goto IL_003b;
		}
	}
	{
		V_0 = _stringLiteral3452614601;
		goto IL_0046;
	}

IL_003b:
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, _stringLiteral327211410, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0046:
	{
		String_t* L_4 = V_0;
		return L_4;
	}
}
// System.IO.FileAccess System.IO.Pipes.UnixNamedPipe::RightsToFileAccess(System.IO.Pipes.PipeAccessRights)
extern "C"  int32_t UnixNamedPipe_RightsToFileAccess_m3755372448 (UnixNamedPipe_t2806256776 * __this, int32_t ___rights0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnixNamedPipe_RightsToFileAccess_m3755372448_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		V_0 = (String_t*)NULL;
		int32_t L_0 = ___rights0;
		if (!((int32_t)((int32_t)L_0&(int32_t)1)))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = ___rights0;
		if (!((int32_t)((int32_t)L_1&(int32_t)2)))
		{
			goto IL_0014;
		}
	}
	{
		return (int32_t)(3);
	}

IL_0014:
	{
		return (int32_t)(1);
	}

IL_0016:
	{
		int32_t L_2 = ___rights0;
		if (!((int32_t)((int32_t)L_2&(int32_t)2)))
		{
			goto IL_0020;
		}
	}
	{
		return (int32_t)(2);
	}

IL_0020:
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_3, _stringLiteral327211410, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}
}
// System.Void System.IO.Pipes.UnixNamedPipeServer::.ctor(System.IO.Pipes.NamedPipeServerStream,System.String,System.Int32,System.IO.Pipes.PipeTransmissionMode,System.IO.Pipes.PipeAccessRights,System.IO.Pipes.PipeOptions,System.Int32,System.Int32,System.IO.HandleInheritability)
extern "C"  void UnixNamedPipeServer__ctor_m1228519622 (UnixNamedPipeServer_t2839746932 * __this, NamedPipeServerStream_t125618231 * ___owner0, String_t* ___pipeName1, int32_t ___maxNumberOfServerInstances2, int32_t ___transmissionMode3, int32_t ___rights4, int32_t ___options5, int32_t ___inBufferSize6, int32_t ___outBufferSize7, int32_t ___inheritability8, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnixNamedPipeServer__ctor_m1228519622_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	FileStream_t4292183065 * V_2 = NULL;
	{
		UnixNamedPipe__ctor_m4176079624(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___pipeName1;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_1 = Path_Combine_m3389272516(NULL /*static, unused*/, _stringLiteral641456133, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		UnixNamedPipe_EnsureTargetFile_m962365236(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___rights4;
		String_t* L_4 = UnixNamedPipe_RightsToAccess_m3895475835(__this, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		int32_t L_5 = ___options5;
		NamedPipeServerStream_t125618231 * L_6 = ___owner0;
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(28 /* System.IO.Pipes.PipeTransmissionMode System.IO.Pipes.PipeStream::get_TransmissionMode() */, L_6);
		UnixNamedPipe_ValidateOptions_m3897423952(__this, L_5, L_7, /*hidden argument*/NULL);
		String_t* L_8 = V_0;
		int32_t L_9 = ___rights4;
		int32_t L_10 = UnixNamedPipe_RightsToFileAccess_m3755372448(__this, L_9, /*hidden argument*/NULL);
		FileStream_t4292183065 * L_11 = (FileStream_t4292183065 *)il2cpp_codegen_object_new(FileStream_t4292183065_il2cpp_TypeInfo_var);
		FileStream__ctor_m2889718780(L_11, L_8, 3, L_10, 3, /*hidden argument*/NULL);
		V_2 = L_11;
		FileStream_t4292183065 * L_12 = V_2;
		NullCheck(L_12);
		IntPtr_t L_13 = VirtFuncInvoker0< IntPtr_t >::Invoke(28 /* System.IntPtr System.IO.FileStream::get_Handle() */, L_12);
		SafePipeHandle_t1989113880 * L_14 = (SafePipeHandle_t1989113880 *)il2cpp_codegen_object_new(SafePipeHandle_t1989113880_il2cpp_TypeInfo_var);
		SafePipeHandle__ctor_m396738268(L_14, L_13, (bool)0, /*hidden argument*/NULL);
		__this->set_handle_0(L_14);
		NamedPipeServerStream_t125618231 * L_15 = ___owner0;
		FileStream_t4292183065 * L_16 = V_2;
		NullCheck(L_15);
		PipeStream_set_Stream_m1574685589(L_15, L_16, /*hidden argument*/NULL);
		__this->set_should_close_handle_1((bool)1);
		return;
	}
}
// Microsoft.Win32.SafeHandles.SafePipeHandle System.IO.Pipes.UnixNamedPipeServer::get_Handle()
extern "C"  SafePipeHandle_t1989113880 * UnixNamedPipeServer_get_Handle_m2435177372 (UnixNamedPipeServer_t2839746932 * __this, const MethodInfo* method)
{
	{
		SafePipeHandle_t1989113880 * L_0 = __this->get_handle_0();
		return L_0;
	}
}
// System.Void System.IO.Pipes.UnixNamedPipeServer::WaitForConnection()
extern "C"  void UnixNamedPipeServer_WaitForConnection_m3204344745 (UnixNamedPipeServer_t2839746932 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean System.IO.Pipes.Win32Marshal::get_IsWindows()
extern "C"  bool Win32Marshal_get_IsWindows_m4212098367 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		OperatingSystem_t3730783609 * L_0 = Environment_get_OSVersion_m961136977(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = OperatingSystem_get_Platform_m2793423729(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0026;
			}
			case 1:
			{
				goto IL_0026;
			}
			case 2:
			{
				goto IL_0026;
			}
			case 3:
			{
				goto IL_0026;
			}
		}
	}
	{
		goto IL_0028;
	}

IL_0026:
	{
		return (bool)1;
	}

IL_0028:
	{
		return (bool)0;
	}
}
// System.IntPtr System.IO.Pipes.Win32Marshal::CreateNamedPipe(System.String,System.UInt32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.IO.Pipes.SecurityAttributesHack&,System.IntPtr)
extern "C"  IntPtr_t Win32Marshal_CreateNamedPipe_m1976185420 (Il2CppObject * __this /* static, unused */, String_t* ___name0, uint32_t ___openMode1, int32_t ___pipeMode2, int32_t ___maxInstances3, int32_t ___outBufferSize4, int32_t ___inBufferSize5, int32_t ___defaultTimeout6, SecurityAttributesHack_t2445122734 * ___securityAttributes7, IntPtr_t ___atts8, const MethodInfo* method)
{


	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (char*, uint32_t, int32_t, int32_t, int32_t, int32_t, int32_t, SecurityAttributesHack_t2445122734_marshaled_pinvoke*, intptr_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(char*) + sizeof(uint32_t) + sizeof(int32_t) + sizeof(int32_t) + sizeof(int32_t) + sizeof(int32_t) + sizeof(int32_t) + sizeof(SecurityAttributesHack_t2445122734_marshaled_pinvoke*) + sizeof(intptr_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("kernel32"), "CreateNamedPipe", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'CreateNamedPipe'"));
		}
	}

	// Marshaling of parameter '___name0' to native representation
	char* ____name0_marshaled = NULL;
	____name0_marshaled = il2cpp_codegen_marshal_string(___name0);

	// Marshaling of parameter '___securityAttributes7' to native representation
	SecurityAttributesHack_t2445122734_marshaled_pinvoke* ____securityAttributes7_marshaled = NULL;
	SecurityAttributesHack_t2445122734_marshaled_pinvoke ____securityAttributes7_marshaled_dereferenced = {};
	SecurityAttributesHack_t2445122734_marshal_pinvoke(*___securityAttributes7, ____securityAttributes7_marshaled_dereferenced);
	____securityAttributes7_marshaled = &____securityAttributes7_marshaled_dereferenced;

	// Native function invocation
	intptr_t returnValue = il2cppPInvokeFunc(____name0_marshaled, ___openMode1, ___pipeMode2, ___maxInstances3, ___outBufferSize4, ___inBufferSize5, ___defaultTimeout6, ____securityAttributes7_marshaled, reinterpret_cast<intptr_t>((___atts8).get_m_value_0()));

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	// Marshaling cleanup of parameter '___name0' native representation
	il2cpp_codegen_marshal_free(____name0_marshaled);
	____name0_marshaled = NULL;

	// Marshaling of parameter '___securityAttributes7' back from native representation
	SecurityAttributesHack_t2445122734  _____securityAttributes7_marshaled_unmarshaled_dereferenced;
	memset(&_____securityAttributes7_marshaled_unmarshaled_dereferenced, 0, sizeof(_____securityAttributes7_marshaled_unmarshaled_dereferenced));
	SecurityAttributesHack_t2445122734_marshal_pinvoke_back(*____securityAttributes7_marshaled, _____securityAttributes7_marshaled_unmarshaled_dereferenced);
	*___securityAttributes7 = _____securityAttributes7_marshaled_unmarshaled_dereferenced;

	// Marshaling cleanup of parameter '___securityAttributes7' native representation
	SecurityAttributesHack_t2445122734_marshal_pinvoke_cleanup(*____securityAttributes7_marshaled);

	return _returnValue_unmarshaled;
}
// System.Boolean System.IO.Pipes.Win32Marshal::ConnectNamedPipe(Microsoft.Win32.SafeHandles.SafePipeHandle,System.IntPtr)
extern "C"  bool Win32Marshal_ConnectNamedPipe_m4021705803 (Il2CppObject * __this /* static, unused */, SafePipeHandle_t1989113880 * ___handle0, IntPtr_t ___overlapped1, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(void*) + sizeof(intptr_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("kernel32"), "ConnectNamedPipe", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ConnectNamedPipe'"));
		}
	}

	// Marshaling of parameter '___handle0' to native representation
	void* ____handle0_marshaled = NULL;
	if (___handle0 == NULL) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("handle"));
	bool ___safeHandle_reference_incremented_for____handle0 = false;
	SafeHandle_DangerousAddRef_m614714386(___handle0, &___safeHandle_reference_incremented_for____handle0, NULL);
	____handle0_marshaled = (___handle0)->get_handle_0().get_m_value_0();

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(____handle0_marshaled, reinterpret_cast<intptr_t>((___overlapped1).get_m_value_0()));

	// Marshaling cleanup of parameter '___handle0' native representation
	if (___safeHandle_reference_incremented_for____handle0)
	{
		SafeHandle_DangerousRelease_m190326290(___handle0, NULL);
	}

	return static_cast<bool>(returnValue);
}
// System.Void System.IO.Pipes.Win32NamedPipe::.ctor()
extern "C"  void Win32NamedPipe__ctor_m671208207 (Win32NamedPipe_t1933459707 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.IO.Pipes.Win32NamedPipeServer::.ctor(System.IO.Pipes.NamedPipeServerStream,System.String,System.Int32,System.IO.Pipes.PipeTransmissionMode,System.IO.Pipes.PipeAccessRights,System.IO.Pipes.PipeOptions,System.Int32,System.Int32,System.IO.HandleInheritability)
extern "C"  void Win32NamedPipeServer__ctor_m442731582 (Win32NamedPipeServer_t774293249 * __this, NamedPipeServerStream_t125618231 * ___owner0, String_t* ___pipeName1, int32_t ___maxNumberOfServerInstances2, int32_t ___transmissionMode3, int32_t ___rights4, int32_t ___options5, int32_t ___inBufferSize6, int32_t ___outBufferSize7, int32_t ___inheritability8, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Win32NamedPipeServer__ctor_m442731582_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	uint32_t V_1 = 0;
	int32_t V_2 = 0;
	SecurityAttributesHack_t2445122734  V_3;
	memset(&V_3, 0, sizeof(V_3));
	IntPtr_t V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Win32NamedPipe__ctor_m671208207(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___pipeName1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral1672346004, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		V_1 = 0;
		int32_t L_2 = ___rights4;
		if (!((int32_t)((int32_t)L_2&(int32_t)1)))
		{
			goto IL_0021;
		}
	}
	{
		uint32_t L_3 = V_1;
		V_1 = ((int32_t)((int32_t)L_3|(int32_t)1));
	}

IL_0021:
	{
		int32_t L_4 = ___rights4;
		if (!((int32_t)((int32_t)L_4&(int32_t)2)))
		{
			goto IL_002e;
		}
	}
	{
		uint32_t L_5 = V_1;
		V_1 = ((int32_t)((int32_t)L_5|(int32_t)2));
	}

IL_002e:
	{
		int32_t L_6 = ___options5;
		if (!((int32_t)((int32_t)L_6&(int32_t)1)))
		{
			goto IL_003f;
		}
	}
	{
		uint32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7|(int32_t)((int32_t)-2147483648LL)));
	}

IL_003f:
	{
		V_2 = 0;
		NamedPipeServerStream_t125618231 * L_8 = ___owner0;
		NullCheck(L_8);
		int32_t L_9 = VirtFuncInvoker0< int32_t >::Invoke(28 /* System.IO.Pipes.PipeTransmissionMode System.IO.Pipes.PipeStream::get_TransmissionMode() */, L_8);
		if (!((int32_t)((int32_t)L_9&(int32_t)1)))
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_10 = V_2;
		V_2 = ((int32_t)((int32_t)L_10|(int32_t)4));
	}

IL_0052:
	{
		int32_t L_11 = ___options5;
		if (!((int32_t)((int32_t)L_11&(int32_t)2)))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_12 = V_2;
		V_2 = ((int32_t)((int32_t)L_12|(int32_t)1));
	}

IL_005f:
	{
		int32_t L_13 = ___inheritability8;
		SecurityAttributesHack__ctor_m2908506643((&V_3), (bool)((((int32_t)L_13) == ((int32_t)1))? 1 : 0), /*hidden argument*/NULL);
		String_t* L_14 = V_0;
		uint32_t L_15 = V_1;
		int32_t L_16 = V_2;
		int32_t L_17 = ___maxNumberOfServerInstances2;
		int32_t L_18 = ___outBufferSize7;
		int32_t L_19 = ___inBufferSize6;
		IntPtr_t L_20 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		IntPtr_t L_21 = Win32Marshal_CreateNamedPipe_m1976185420(NULL /*static, unused*/, L_14, L_15, L_16, L_17, L_18, L_19, 0, (&V_3), L_20, /*hidden argument*/NULL);
		V_4 = L_21;
		IntPtr_t L_22 = V_4;
		IntPtr_t L_23;
		memset(&L_23, 0, sizeof(L_23));
		IntPtr__ctor_m987476171(&L_23, (((int64_t)((int64_t)(-1)))), /*hidden argument*/NULL);
		bool L_24 = IntPtr_op_Equality_m408849716(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00a0;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1757017490_il2cpp_TypeInfo_var);
		int32_t L_25 = Marshal_GetLastWin32Error_m1272610344(NULL /*static, unused*/, /*hidden argument*/NULL);
		Win32Exception_t3234146298 * L_26 = (Win32Exception_t3234146298 *)il2cpp_codegen_object_new(Win32Exception_t3234146298_il2cpp_TypeInfo_var);
		Win32Exception__ctor_m3118723333(L_26, L_25, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_26);
	}

IL_00a0:
	{
		IntPtr_t L_27 = V_4;
		SafePipeHandle_t1989113880 * L_28 = (SafePipeHandle_t1989113880 *)il2cpp_codegen_object_new(SafePipeHandle_t1989113880_il2cpp_TypeInfo_var);
		SafePipeHandle__ctor_m396738268(L_28, L_27, (bool)1, /*hidden argument*/NULL);
		__this->set_handle_0(L_28);
		return;
	}
}
// Microsoft.Win32.SafeHandles.SafePipeHandle System.IO.Pipes.Win32NamedPipeServer::get_Handle()
extern "C"  SafePipeHandle_t1989113880 * Win32NamedPipeServer_get_Handle_m802589821 (Win32NamedPipeServer_t774293249 * __this, const MethodInfo* method)
{
	{
		SafePipeHandle_t1989113880 * L_0 = __this->get_handle_0();
		return L_0;
	}
}
// System.Void System.IO.Pipes.Win32NamedPipeServer::WaitForConnection()
extern "C"  void Win32NamedPipeServer_WaitForConnection_m2578077009 (Win32NamedPipeServer_t774293249 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Win32NamedPipeServer_WaitForConnection_m2578077009_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SafePipeHandle_t1989113880 * L_0 = VirtFuncInvoker0< SafePipeHandle_t1989113880 * >::Invoke(5 /* Microsoft.Win32.SafeHandles.SafePipeHandle System.IO.Pipes.Win32NamedPipeServer::get_Handle() */, __this);
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = Win32Marshal_ConnectNamedPipe_m4021705803(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1757017490_il2cpp_TypeInfo_var);
		int32_t L_3 = Marshal_GetLastWin32Error_m1272610344(NULL /*static, unused*/, /*hidden argument*/NULL);
		Win32Exception_t3234146298 * L_4 = (Win32Exception_t3234146298 *)il2cpp_codegen_object_new(Win32Exception_t3234146298_il2cpp_TypeInfo_var);
		Win32Exception__ctor_m3118723333(L_4, L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0020:
	{
		return;
	}
}
// System.Void System.Linq.Check::Source(System.Object)
extern "C"  void Check_Source_m4098695967 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Check_Source_m4098695967_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___source0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral4294193667, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Void System.Linq.Check::SourceAndPredicate(System.Object,System.Object)
extern "C"  void Check_SourceAndPredicate_m2332465641 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___source0, Il2CppObject * ___predicate1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Check_SourceAndPredicate_m2332465641_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___source0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral4294193667, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___predicate1;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_3 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_3, _stringLiteral3941128596, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		return;
	}
}
// System.Void System.MonoNotSupportedAttribute::.ctor(System.String)
extern "C"  void MonoNotSupportedAttribute__ctor_m3242113599 (MonoNotSupportedAttribute_t2563528020 * __this, String_t* ___comment0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___comment0;
		MonoTODOAttribute__ctor_m4112301096(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.MonoTODOAttribute::.ctor()
extern "C"  void MonoTODOAttribute__ctor_m2619396706 (MonoTODOAttribute_t4131080587 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.MonoTODOAttribute::.ctor(System.String)
extern "C"  void MonoTODOAttribute__ctor_m4112301096 (MonoTODOAttribute_t4131080587 * __this, String_t* ___comment0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___comment0;
		__this->set_comment_0(L_0);
		return;
	}
}
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
extern "C"  void ExtensionAttribute__ctor_m1708143005 (ExtensionAttribute_t1723066603 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.Aes::.ctor()
extern "C"  void Aes__ctor_m178909601 (Aes_t1218282760 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Aes__ctor_m178909601_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SymmetricAlgorithm__ctor_m467277132(__this, /*hidden argument*/NULL);
		((SymmetricAlgorithm_t4254223087 *)__this)->set_KeySizeValue_2(((int32_t)256));
		((SymmetricAlgorithm_t4254223087 *)__this)->set_BlockSizeValue_0(((int32_t)128));
		((SymmetricAlgorithm_t4254223087 *)__this)->set_FeedbackSizeValue_6(((int32_t)128));
		((SymmetricAlgorithm_t4254223087 *)__this)->set_LegalKeySizesValue_5(((KeySizesU5BU5D_t722666473*)SZArrayNew(KeySizesU5BU5D_t722666473_il2cpp_TypeInfo_var, (uint32_t)1)));
		KeySizesU5BU5D_t722666473* L_0 = ((SymmetricAlgorithm_t4254223087 *)__this)->get_LegalKeySizesValue_5();
		KeySizes_t85027896 * L_1 = (KeySizes_t85027896 *)il2cpp_codegen_object_new(KeySizes_t85027896_il2cpp_TypeInfo_var);
		KeySizes__ctor_m3113946058(L_1, ((int32_t)128), ((int32_t)256), ((int32_t)64), /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (KeySizes_t85027896 *)L_1);
		((SymmetricAlgorithm_t4254223087 *)__this)->set_LegalBlockSizesValue_4(((KeySizesU5BU5D_t722666473*)SZArrayNew(KeySizesU5BU5D_t722666473_il2cpp_TypeInfo_var, (uint32_t)1)));
		KeySizesU5BU5D_t722666473* L_2 = ((SymmetricAlgorithm_t4254223087 *)__this)->get_LegalBlockSizesValue_4();
		KeySizes_t85027896 * L_3 = (KeySizes_t85027896 *)il2cpp_codegen_object_new(KeySizes_t85027896_il2cpp_TypeInfo_var);
		KeySizes__ctor_m3113946058(L_3, ((int32_t)128), ((int32_t)128), 0, /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (KeySizes_t85027896 *)L_3);
		return;
	}
}
// System.Void System.Security.Cryptography.AesCryptoServiceProvider::.ctor()
extern "C"  void AesCryptoServiceProvider__ctor_m3527218652 (AesCryptoServiceProvider_t345478893 * __this, const MethodInfo* method)
{
	{
		Aes__ctor_m178909601(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateIV()
extern "C"  void AesCryptoServiceProvider_GenerateIV_m2862162833 (AesCryptoServiceProvider_t345478893 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((SymmetricAlgorithm_t4254223087 *)__this)->get_BlockSizeValue_0();
		ByteU5BU5D_t4116647657* L_1 = KeyBuilder_IV_m3340234014(NULL /*static, unused*/, ((int32_t)((int32_t)L_0>>(int32_t)3)), /*hidden argument*/NULL);
		((SymmetricAlgorithm_t4254223087 *)__this)->set_IVValue_1(L_1);
		return;
	}
}
// System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateKey()
extern "C"  void AesCryptoServiceProvider_GenerateKey_m2038339615 (AesCryptoServiceProvider_t345478893 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((SymmetricAlgorithm_t4254223087 *)__this)->get_KeySizeValue_2();
		ByteU5BU5D_t4116647657* L_1 = KeyBuilder_Key_m2503211157(NULL /*static, unused*/, ((int32_t)((int32_t)L_0>>(int32_t)3)), /*hidden argument*/NULL);
		((SymmetricAlgorithm_t4254223087 *)__this)->set_KeyValue_3(L_1);
		return;
	}
}
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern "C"  Il2CppObject * AesCryptoServiceProvider_CreateDecryptor_m1328793350 (AesCryptoServiceProvider_t345478893 * __this, ByteU5BU5D_t4116647657* ___rgbKey0, ByteU5BU5D_t4116647657* ___rgbIV1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AesCryptoServiceProvider_CreateDecryptor_m1328793350_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t4116647657* L_0 = ___rgbKey0;
		ByteU5BU5D_t4116647657* L_1 = ___rgbIV1;
		AesTransform_t2957123611 * L_2 = (AesTransform_t2957123611 *)il2cpp_codegen_object_new(AesTransform_t2957123611_il2cpp_TypeInfo_var);
		AesTransform__ctor_m3143546745(L_2, __this, (bool)0, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern "C"  Il2CppObject * AesCryptoServiceProvider_CreateEncryptor_m1407541527 (AesCryptoServiceProvider_t345478893 * __this, ByteU5BU5D_t4116647657* ___rgbKey0, ByteU5BU5D_t4116647657* ___rgbIV1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AesCryptoServiceProvider_CreateEncryptor_m1407541527_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t4116647657* L_0 = ___rgbKey0;
		ByteU5BU5D_t4116647657* L_1 = ___rgbIV1;
		AesTransform_t2957123611 * L_2 = (AesTransform_t2957123611 *)il2cpp_codegen_object_new(AesTransform_t2957123611_il2cpp_TypeInfo_var);
		AesTransform__ctor_m3143546745(L_2, __this, (bool)1, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_IV()
extern "C"  ByteU5BU5D_t4116647657* AesCryptoServiceProvider_get_IV_m2323542127 (AesCryptoServiceProvider_t345478893 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t4116647657* L_0 = SymmetricAlgorithm_get_IV_m1875559108(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_IV(System.Byte[])
extern "C"  void AesCryptoServiceProvider_set_IV_m1872168943 (AesCryptoServiceProvider_t345478893 * __this, ByteU5BU5D_t4116647657* ___value0, const MethodInfo* method)
{
	{
		ByteU5BU5D_t4116647657* L_0 = ___value0;
		SymmetricAlgorithm_set_IV_m3196220308(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_Key()
extern "C"  ByteU5BU5D_t4116647657* AesCryptoServiceProvider_get_Key_m249672229 (AesCryptoServiceProvider_t345478893 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t4116647657* L_0 = SymmetricAlgorithm_get_Key_m3241860519(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Key(System.Byte[])
extern "C"  void AesCryptoServiceProvider_set_Key_m1299633668 (AesCryptoServiceProvider_t345478893 * __this, ByteU5BU5D_t4116647657* ___value0, const MethodInfo* method)
{
	{
		ByteU5BU5D_t4116647657* L_0 = ___value0;
		SymmetricAlgorithm_set_Key_m1775642191(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_KeySize()
extern "C"  int32_t AesCryptoServiceProvider_get_KeySize_m1957235978 (AesCryptoServiceProvider_t345478893 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = SymmetricAlgorithm_get_KeySize_m4185004893(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_KeySize(System.Int32)
extern "C"  void AesCryptoServiceProvider_set_KeySize_m1700177054 (AesCryptoServiceProvider_t345478893 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		SymmetricAlgorithm_set_KeySize_m3805756466(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor()
extern "C"  Il2CppObject * AesCryptoServiceProvider_CreateDecryptor_m3603952599 (AesCryptoServiceProvider_t345478893 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t4116647657* L_0 = AesCryptoServiceProvider_get_Key_m249672229(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_1 = AesCryptoServiceProvider_get_IV_m2323542127(__this, /*hidden argument*/NULL);
		Il2CppObject * L_2 = AesCryptoServiceProvider_CreateDecryptor_m1328793350(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor()
extern "C"  Il2CppObject * AesCryptoServiceProvider_CreateEncryptor_m732637922 (AesCryptoServiceProvider_t345478893 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t4116647657* L_0 = AesCryptoServiceProvider_get_Key_m249672229(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_1 = AesCryptoServiceProvider_get_IV_m2323542127(__this, /*hidden argument*/NULL);
		Il2CppObject * L_2 = AesCryptoServiceProvider_CreateEncryptor_m1407541527(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Security.Cryptography.AesCryptoServiceProvider::Dispose(System.Boolean)
extern "C"  void AesCryptoServiceProvider_Dispose_m156197556 (AesCryptoServiceProvider_t345478893 * __this, bool ___disposing0, const MethodInfo* method)
{
	{
		bool L_0 = ___disposing0;
		SymmetricAlgorithm_Dispose_m1120980942(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.AesManaged::.ctor()
extern "C"  void AesManaged__ctor_m1349486362 (AesManaged_t1129950597 * __this, const MethodInfo* method)
{
	{
		Aes__ctor_m178909601(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.AesManaged::GenerateIV()
extern "C"  void AesManaged_GenerateIV_m1368817386 (AesManaged_t1129950597 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((SymmetricAlgorithm_t4254223087 *)__this)->get_BlockSizeValue_0();
		ByteU5BU5D_t4116647657* L_1 = KeyBuilder_IV_m3340234014(NULL /*static, unused*/, ((int32_t)((int32_t)L_0>>(int32_t)3)), /*hidden argument*/NULL);
		((SymmetricAlgorithm_t4254223087 *)__this)->set_IVValue_1(L_1);
		return;
	}
}
// System.Void System.Security.Cryptography.AesManaged::GenerateKey()
extern "C"  void AesManaged_GenerateKey_m2004209814 (AesManaged_t1129950597 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((SymmetricAlgorithm_t4254223087 *)__this)->get_KeySizeValue_2();
		ByteU5BU5D_t4116647657* L_1 = KeyBuilder_Key_m2503211157(NULL /*static, unused*/, ((int32_t)((int32_t)L_0>>(int32_t)3)), /*hidden argument*/NULL);
		((SymmetricAlgorithm_t4254223087 *)__this)->set_KeyValue_3(L_1);
		return;
	}
}
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern "C"  Il2CppObject * AesManaged_CreateDecryptor_m692040246 (AesManaged_t1129950597 * __this, ByteU5BU5D_t4116647657* ___rgbKey0, ByteU5BU5D_t4116647657* ___rgbIV1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AesManaged_CreateDecryptor_m692040246_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t4116647657* L_0 = ___rgbKey0;
		ByteU5BU5D_t4116647657* L_1 = ___rgbIV1;
		AesTransform_t2957123611 * L_2 = (AesTransform_t2957123611 *)il2cpp_codegen_object_new(AesTransform_t2957123611_il2cpp_TypeInfo_var);
		AesTransform__ctor_m3143546745(L_2, __this, (bool)0, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern "C"  Il2CppObject * AesManaged_CreateEncryptor_m2294080233 (AesManaged_t1129950597 * __this, ByteU5BU5D_t4116647657* ___rgbKey0, ByteU5BU5D_t4116647657* ___rgbIV1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AesManaged_CreateEncryptor_m2294080233_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t4116647657* L_0 = ___rgbKey0;
		ByteU5BU5D_t4116647657* L_1 = ___rgbIV1;
		AesTransform_t2957123611 * L_2 = (AesTransform_t2957123611 *)il2cpp_codegen_object_new(AesTransform_t2957123611_il2cpp_TypeInfo_var);
		AesTransform__ctor_m3143546745(L_2, __this, (bool)1, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Byte[] System.Security.Cryptography.AesManaged::get_IV()
extern "C"  ByteU5BU5D_t4116647657* AesManaged_get_IV_m118095902 (AesManaged_t1129950597 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t4116647657* L_0 = SymmetricAlgorithm_get_IV_m1875559108(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Security.Cryptography.AesManaged::set_IV(System.Byte[])
extern "C"  void AesManaged_set_IV_m3705704588 (AesManaged_t1129950597 * __this, ByteU5BU5D_t4116647657* ___value0, const MethodInfo* method)
{
	{
		ByteU5BU5D_t4116647657* L_0 = ___value0;
		SymmetricAlgorithm_set_IV_m3196220308(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Byte[] System.Security.Cryptography.AesManaged::get_Key()
extern "C"  ByteU5BU5D_t4116647657* AesManaged_get_Key_m538801386 (AesManaged_t1129950597 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t4116647657* L_0 = SymmetricAlgorithm_get_Key_m3241860519(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Security.Cryptography.AesManaged::set_Key(System.Byte[])
extern "C"  void AesManaged_set_Key_m767972181 (AesManaged_t1129950597 * __this, ByteU5BU5D_t4116647657* ___value0, const MethodInfo* method)
{
	{
		ByteU5BU5D_t4116647657* L_0 = ___value0;
		SymmetricAlgorithm_set_Key_m1775642191(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 System.Security.Cryptography.AesManaged::get_KeySize()
extern "C"  int32_t AesManaged_get_KeySize_m1181452829 (AesManaged_t1129950597 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = SymmetricAlgorithm_get_KeySize_m4185004893(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Security.Cryptography.AesManaged::set_KeySize(System.Int32)
extern "C"  void AesManaged_set_KeySize_m1150692274 (AesManaged_t1129950597 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		SymmetricAlgorithm_set_KeySize_m3805756466(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor()
extern "C"  Il2CppObject * AesManaged_CreateDecryptor_m752875210 (AesManaged_t1129950597 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t4116647657* L_0 = AesManaged_get_Key_m538801386(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_1 = AesManaged_get_IV_m118095902(__this, /*hidden argument*/NULL);
		Il2CppObject * L_2 = AesManaged_CreateDecryptor_m692040246(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor()
extern "C"  Il2CppObject * AesManaged_CreateEncryptor_m1611897367 (AesManaged_t1129950597 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t4116647657* L_0 = AesManaged_get_Key_m538801386(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_1 = AesManaged_get_IV_m118095902(__this, /*hidden argument*/NULL);
		Il2CppObject * L_2 = AesManaged_CreateEncryptor_m2294080233(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Security.Cryptography.AesManaged::Dispose(System.Boolean)
extern "C"  void AesManaged_Dispose_m615303088 (AesManaged_t1129950597 * __this, bool ___disposing0, const MethodInfo* method)
{
	{
		bool L_0 = ___disposing0;
		SymmetricAlgorithm_Dispose_m1120980942(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.AesTransform::.ctor(System.Security.Cryptography.Aes,System.Boolean,System.Byte[],System.Byte[])
extern "C"  void AesTransform__ctor_m3143546745 (AesTransform_t2957123611 * __this, Aes_t1218282760 * ___algo0, bool ___encryption1, ByteU5BU5D_t4116647657* ___key2, ByteU5BU5D_t4116647657* ___iv3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AesTransform__ctor_m3143546745_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	int32_t V_3 = 0;
	UInt32U5BU5D_t2770800703* V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	uint32_t V_7 = 0;
	int32_t V_8 = 0;
	uint32_t V_9 = 0;
	uint32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	uint32_t V_14 = 0;
	int32_t V_15 = 0;
	{
		Aes_t1218282760 * L_0 = ___algo0;
		bool L_1 = ___encryption1;
		ByteU5BU5D_t4116647657* L_2 = ___iv3;
		SymmetricTransform__ctor_m2693628991(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_3 = ___key2;
		if (L_3)
		{
			goto IL_001b;
		}
	}
	{
		CryptographicException_t248831461 * L_4 = (CryptographicException_t248831461 *)il2cpp_codegen_object_new(CryptographicException_t248831461_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m503735289(L_4, _stringLiteral2153550409, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_001b:
	{
		ByteU5BU5D_t4116647657* L_5 = ___iv3;
		if (!L_5)
		{
			goto IL_0067;
		}
	}
	{
		ByteU5BU5D_t4116647657* L_6 = ___iv3;
		NullCheck(L_6);
		Aes_t1218282760 * L_7 = ___algo0;
		NullCheck(L_7);
		int32_t L_8 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Security.Cryptography.SymmetricAlgorithm::get_BlockSize() */, L_7);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((int32_t)((int32_t)((int32_t)L_8>>(int32_t)3)))))
		{
			goto IL_0067;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_9 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)2));
		ByteU5BU5D_t4116647657* L_10 = ___iv3;
		NullCheck(L_10);
		int32_t L_11 = (((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length))));
		Il2CppObject * L_12 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_12);
		ObjectU5BU5D_t2843939325* L_13 = L_9;
		Aes_t1218282760 * L_14 = ___algo0;
		NullCheck(L_14);
		int32_t L_15 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Security.Cryptography.SymmetricAlgorithm::get_BlockSize() */, L_14);
		int32_t L_16 = ((int32_t)((int32_t)L_15>>(int32_t)3));
		Il2CppObject * L_17 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_17);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_17);
		String_t* L_18 = Locale_GetText_m2427493201(NULL /*static, unused*/, _stringLiteral2136391555, L_13, /*hidden argument*/NULL);
		V_0 = L_18;
		String_t* L_19 = V_0;
		CryptographicException_t248831461 * L_20 = (CryptographicException_t248831461 *)il2cpp_codegen_object_new(CryptographicException_t248831461_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m503735289(L_20, L_19, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20);
	}

IL_0067:
	{
		ByteU5BU5D_t4116647657* L_21 = ___key2;
		NullCheck(L_21);
		V_1 = (((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length))));
		int32_t L_22 = V_1;
		if ((((int32_t)L_22) == ((int32_t)((int32_t)16))))
		{
			goto IL_00c2;
		}
	}
	{
		int32_t L_23 = V_1;
		if ((((int32_t)L_23) == ((int32_t)((int32_t)24))))
		{
			goto IL_00c2;
		}
	}
	{
		int32_t L_24 = V_1;
		if ((((int32_t)L_24) == ((int32_t)((int32_t)32))))
		{
			goto IL_00c2;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_25 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4));
		int32_t L_26 = V_1;
		int32_t L_27 = L_26;
		Il2CppObject * L_28 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_27);
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, L_28);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_28);
		ObjectU5BU5D_t2843939325* L_29 = L_25;
		int32_t L_30 = ((int32_t)16);
		Il2CppObject * L_31 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_30);
		NullCheck(L_29);
		ArrayElementTypeCheck (L_29, L_31);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_31);
		ObjectU5BU5D_t2843939325* L_32 = L_29;
		int32_t L_33 = ((int32_t)24);
		Il2CppObject * L_34 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_33);
		NullCheck(L_32);
		ArrayElementTypeCheck (L_32, L_34);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_34);
		ObjectU5BU5D_t2843939325* L_35 = L_32;
		int32_t L_36 = ((int32_t)32);
		Il2CppObject * L_37 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_36);
		NullCheck(L_35);
		ArrayElementTypeCheck (L_35, L_37);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_37);
		String_t* L_38 = Locale_GetText_m2427493201(NULL /*static, unused*/, _stringLiteral2585275424, L_35, /*hidden argument*/NULL);
		V_2 = L_38;
		String_t* L_39 = V_2;
		CryptographicException_t248831461 * L_40 = (CryptographicException_t248831461 *)il2cpp_codegen_object_new(CryptographicException_t248831461_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m503735289(L_40, L_39, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_40);
	}

IL_00c2:
	{
		int32_t L_41 = V_1;
		V_1 = ((int32_t)((int32_t)L_41<<(int32_t)3));
		int32_t L_42 = V_1;
		__this->set_Nk_13(((int32_t)((int32_t)L_42>>(int32_t)5)));
		int32_t L_43 = __this->get_Nk_13();
		if ((!(((uint32_t)L_43) == ((uint32_t)8))))
		{
			goto IL_00e8;
		}
	}
	{
		__this->set_Nr_14(((int32_t)14));
		goto IL_0109;
	}

IL_00e8:
	{
		int32_t L_44 = __this->get_Nk_13();
		if ((!(((uint32_t)L_44) == ((uint32_t)6))))
		{
			goto IL_0101;
		}
	}
	{
		__this->set_Nr_14(((int32_t)12));
		goto IL_0109;
	}

IL_0101:
	{
		__this->set_Nr_14(((int32_t)10));
	}

IL_0109:
	{
		int32_t L_45 = __this->get_Nr_14();
		V_3 = ((int32_t)((int32_t)4*(int32_t)((int32_t)((int32_t)L_45+(int32_t)1))));
		int32_t L_46 = V_3;
		V_4 = ((UInt32U5BU5D_t2770800703*)SZArrayNew(UInt32U5BU5D_t2770800703_il2cpp_TypeInfo_var, (uint32_t)L_46));
		V_5 = 0;
		V_6 = 0;
		goto IL_0171;
	}

IL_0127:
	{
		ByteU5BU5D_t4116647657* L_47 = ___key2;
		int32_t L_48 = V_5;
		int32_t L_49 = L_48;
		V_5 = ((int32_t)((int32_t)L_49+(int32_t)1));
		NullCheck(L_47);
		int32_t L_50 = L_49;
		uint8_t L_51 = (L_47)->GetAt(static_cast<il2cpp_array_size_t>(L_50));
		V_7 = ((int32_t)((int32_t)L_51<<(int32_t)((int32_t)24)));
		uint32_t L_52 = V_7;
		ByteU5BU5D_t4116647657* L_53 = ___key2;
		int32_t L_54 = V_5;
		int32_t L_55 = L_54;
		V_5 = ((int32_t)((int32_t)L_55+(int32_t)1));
		NullCheck(L_53);
		int32_t L_56 = L_55;
		uint8_t L_57 = (L_53)->GetAt(static_cast<il2cpp_array_size_t>(L_56));
		V_7 = ((int32_t)((int32_t)L_52|(int32_t)((int32_t)((int32_t)L_57<<(int32_t)((int32_t)16)))));
		uint32_t L_58 = V_7;
		ByteU5BU5D_t4116647657* L_59 = ___key2;
		int32_t L_60 = V_5;
		int32_t L_61 = L_60;
		V_5 = ((int32_t)((int32_t)L_61+(int32_t)1));
		NullCheck(L_59);
		int32_t L_62 = L_61;
		uint8_t L_63 = (L_59)->GetAt(static_cast<il2cpp_array_size_t>(L_62));
		V_7 = ((int32_t)((int32_t)L_58|(int32_t)((int32_t)((int32_t)L_63<<(int32_t)8))));
		uint32_t L_64 = V_7;
		ByteU5BU5D_t4116647657* L_65 = ___key2;
		int32_t L_66 = V_5;
		int32_t L_67 = L_66;
		V_5 = ((int32_t)((int32_t)L_67+(int32_t)1));
		NullCheck(L_65);
		int32_t L_68 = L_67;
		uint8_t L_69 = (L_65)->GetAt(static_cast<il2cpp_array_size_t>(L_68));
		V_7 = ((int32_t)((int32_t)L_64|(int32_t)L_69));
		UInt32U5BU5D_t2770800703* L_70 = V_4;
		int32_t L_71 = V_6;
		uint32_t L_72 = V_7;
		NullCheck(L_70);
		(L_70)->SetAt(static_cast<il2cpp_array_size_t>(L_71), (uint32_t)L_72);
		int32_t L_73 = V_6;
		V_6 = ((int32_t)((int32_t)L_73+(int32_t)1));
	}

IL_0171:
	{
		int32_t L_74 = V_6;
		int32_t L_75 = __this->get_Nk_13();
		if ((((int32_t)L_74) < ((int32_t)L_75)))
		{
			goto IL_0127;
		}
	}
	{
		int32_t L_76 = __this->get_Nk_13();
		V_8 = L_76;
		goto IL_0212;
	}

IL_018b:
	{
		UInt32U5BU5D_t2770800703* L_77 = V_4;
		int32_t L_78 = V_8;
		NullCheck(L_77);
		int32_t L_79 = ((int32_t)((int32_t)L_78-(int32_t)1));
		uint32_t L_80 = (L_77)->GetAt(static_cast<il2cpp_array_size_t>(L_79));
		V_9 = L_80;
		int32_t L_81 = V_8;
		int32_t L_82 = __this->get_Nk_13();
		if (((int32_t)((int32_t)L_81%(int32_t)L_82)))
		{
			goto IL_01d3;
		}
	}
	{
		uint32_t L_83 = V_9;
		uint32_t L_84 = V_9;
		V_10 = ((int32_t)((int32_t)((int32_t)((int32_t)L_83<<(int32_t)8))|(int32_t)((int32_t)((int32_t)((int32_t)((uint32_t)L_84>>((int32_t)24)))&(int32_t)((int32_t)255)))));
		uint32_t L_85 = V_10;
		uint32_t L_86 = AesTransform_SubByte_m3350159546(__this, L_85, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AesTransform_t2957123611_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t2770800703* L_87 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_Rcon_15();
		int32_t L_88 = V_8;
		int32_t L_89 = __this->get_Nk_13();
		NullCheck(L_87);
		int32_t L_90 = ((int32_t)((int32_t)L_88/(int32_t)L_89));
		uint32_t L_91 = (L_87)->GetAt(static_cast<il2cpp_array_size_t>(L_90));
		V_9 = ((int32_t)((int32_t)L_86^(int32_t)L_91));
		goto IL_01f8;
	}

IL_01d3:
	{
		int32_t L_92 = __this->get_Nk_13();
		if ((((int32_t)L_92) <= ((int32_t)6)))
		{
			goto IL_01f8;
		}
	}
	{
		int32_t L_93 = V_8;
		int32_t L_94 = __this->get_Nk_13();
		if ((!(((uint32_t)((int32_t)((int32_t)L_93%(int32_t)L_94))) == ((uint32_t)4))))
		{
			goto IL_01f8;
		}
	}
	{
		uint32_t L_95 = V_9;
		uint32_t L_96 = AesTransform_SubByte_m3350159546(__this, L_95, /*hidden argument*/NULL);
		V_9 = L_96;
	}

IL_01f8:
	{
		UInt32U5BU5D_t2770800703* L_97 = V_4;
		int32_t L_98 = V_8;
		UInt32U5BU5D_t2770800703* L_99 = V_4;
		int32_t L_100 = V_8;
		int32_t L_101 = __this->get_Nk_13();
		NullCheck(L_99);
		int32_t L_102 = ((int32_t)((int32_t)L_100-(int32_t)L_101));
		uint32_t L_103 = (L_99)->GetAt(static_cast<il2cpp_array_size_t>(L_102));
		uint32_t L_104 = V_9;
		NullCheck(L_97);
		(L_97)->SetAt(static_cast<il2cpp_array_size_t>(L_98), (uint32_t)((int32_t)((int32_t)L_103^(int32_t)L_104)));
		int32_t L_105 = V_8;
		V_8 = ((int32_t)((int32_t)L_105+(int32_t)1));
	}

IL_0212:
	{
		int32_t L_106 = V_8;
		int32_t L_107 = V_3;
		if ((((int32_t)L_106) < ((int32_t)L_107)))
		{
			goto IL_018b;
		}
	}
	{
		bool L_108 = ___encryption1;
		if (L_108)
		{
			goto IL_0307;
		}
	}
	{
		Aes_t1218282760 * L_109 = ___algo0;
		NullCheck(L_109);
		int32_t L_110 = VirtFuncInvoker0< int32_t >::Invoke(16 /* System.Security.Cryptography.CipherMode System.Security.Cryptography.SymmetricAlgorithm::get_Mode() */, L_109);
		if ((((int32_t)L_110) == ((int32_t)2)))
		{
			goto IL_0238;
		}
	}
	{
		Aes_t1218282760 * L_111 = ___algo0;
		NullCheck(L_111);
		int32_t L_112 = VirtFuncInvoker0< int32_t >::Invoke(16 /* System.Security.Cryptography.CipherMode System.Security.Cryptography.SymmetricAlgorithm::get_Mode() */, L_111);
		if ((!(((uint32_t)L_112) == ((uint32_t)1))))
		{
			goto IL_0307;
		}
	}

IL_0238:
	{
		V_11 = 0;
		int32_t L_113 = V_3;
		V_12 = ((int32_t)((int32_t)L_113-(int32_t)4));
		goto IL_028b;
	}

IL_0245:
	{
		V_13 = 0;
		goto IL_0277;
	}

IL_024d:
	{
		UInt32U5BU5D_t2770800703* L_114 = V_4;
		int32_t L_115 = V_11;
		int32_t L_116 = V_13;
		NullCheck(L_114);
		int32_t L_117 = ((int32_t)((int32_t)L_115+(int32_t)L_116));
		uint32_t L_118 = (L_114)->GetAt(static_cast<il2cpp_array_size_t>(L_117));
		V_14 = L_118;
		UInt32U5BU5D_t2770800703* L_119 = V_4;
		int32_t L_120 = V_11;
		int32_t L_121 = V_13;
		UInt32U5BU5D_t2770800703* L_122 = V_4;
		int32_t L_123 = V_12;
		int32_t L_124 = V_13;
		NullCheck(L_122);
		int32_t L_125 = ((int32_t)((int32_t)L_123+(int32_t)L_124));
		uint32_t L_126 = (L_122)->GetAt(static_cast<il2cpp_array_size_t>(L_125));
		NullCheck(L_119);
		(L_119)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_120+(int32_t)L_121))), (uint32_t)L_126);
		UInt32U5BU5D_t2770800703* L_127 = V_4;
		int32_t L_128 = V_12;
		int32_t L_129 = V_13;
		uint32_t L_130 = V_14;
		NullCheck(L_127);
		(L_127)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_128+(int32_t)L_129))), (uint32_t)L_130);
		int32_t L_131 = V_13;
		V_13 = ((int32_t)((int32_t)L_131+(int32_t)1));
	}

IL_0277:
	{
		int32_t L_132 = V_13;
		if ((((int32_t)L_132) < ((int32_t)4)))
		{
			goto IL_024d;
		}
	}
	{
		int32_t L_133 = V_11;
		V_11 = ((int32_t)((int32_t)L_133+(int32_t)4));
		int32_t L_134 = V_12;
		V_12 = ((int32_t)((int32_t)L_134-(int32_t)4));
	}

IL_028b:
	{
		int32_t L_135 = V_11;
		int32_t L_136 = V_12;
		if ((((int32_t)L_135) < ((int32_t)L_136)))
		{
			goto IL_0245;
		}
	}
	{
		V_15 = 4;
		goto IL_02fa;
	}

IL_029c:
	{
		UInt32U5BU5D_t2770800703* L_137 = V_4;
		int32_t L_138 = V_15;
		IL2CPP_RUNTIME_CLASS_INIT(AesTransform_t2957123611_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t2770800703* L_139 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		ByteU5BU5D_t4116647657* L_140 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		UInt32U5BU5D_t2770800703* L_141 = V_4;
		int32_t L_142 = V_15;
		NullCheck(L_141);
		int32_t L_143 = L_142;
		uint32_t L_144 = (L_141)->GetAt(static_cast<il2cpp_array_size_t>(L_143));
		NullCheck(L_140);
		uintptr_t L_145 = (((uintptr_t)((int32_t)((uint32_t)L_144>>((int32_t)24)))));
		uint8_t L_146 = (L_140)->GetAt(static_cast<il2cpp_array_size_t>(L_145));
		NullCheck(L_139);
		uint8_t L_147 = L_146;
		uint32_t L_148 = (L_139)->GetAt(static_cast<il2cpp_array_size_t>(L_147));
		UInt32U5BU5D_t2770800703* L_149 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		ByteU5BU5D_t4116647657* L_150 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		UInt32U5BU5D_t2770800703* L_151 = V_4;
		int32_t L_152 = V_15;
		NullCheck(L_151);
		int32_t L_153 = L_152;
		uint32_t L_154 = (L_151)->GetAt(static_cast<il2cpp_array_size_t>(L_153));
		NullCheck(L_150);
		int32_t L_155 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_154>>((int32_t)16))))));
		uint8_t L_156 = (L_150)->GetAt(static_cast<il2cpp_array_size_t>(L_155));
		NullCheck(L_149);
		uint8_t L_157 = L_156;
		uint32_t L_158 = (L_149)->GetAt(static_cast<il2cpp_array_size_t>(L_157));
		UInt32U5BU5D_t2770800703* L_159 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		ByteU5BU5D_t4116647657* L_160 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		UInt32U5BU5D_t2770800703* L_161 = V_4;
		int32_t L_162 = V_15;
		NullCheck(L_161);
		int32_t L_163 = L_162;
		uint32_t L_164 = (L_161)->GetAt(static_cast<il2cpp_array_size_t>(L_163));
		NullCheck(L_160);
		int32_t L_165 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_164>>8)))));
		uint8_t L_166 = (L_160)->GetAt(static_cast<il2cpp_array_size_t>(L_165));
		NullCheck(L_159);
		uint8_t L_167 = L_166;
		uint32_t L_168 = (L_159)->GetAt(static_cast<il2cpp_array_size_t>(L_167));
		UInt32U5BU5D_t2770800703* L_169 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		ByteU5BU5D_t4116647657* L_170 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		UInt32U5BU5D_t2770800703* L_171 = V_4;
		int32_t L_172 = V_15;
		NullCheck(L_171);
		int32_t L_173 = L_172;
		uint32_t L_174 = (L_171)->GetAt(static_cast<il2cpp_array_size_t>(L_173));
		NullCheck(L_170);
		int32_t L_175 = (((int32_t)((uint8_t)L_174)));
		uint8_t L_176 = (L_170)->GetAt(static_cast<il2cpp_array_size_t>(L_175));
		NullCheck(L_169);
		uint8_t L_177 = L_176;
		uint32_t L_178 = (L_169)->GetAt(static_cast<il2cpp_array_size_t>(L_177));
		NullCheck(L_137);
		(L_137)->SetAt(static_cast<il2cpp_array_size_t>(L_138), (uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_148^(int32_t)L_158))^(int32_t)L_168))^(int32_t)L_178)));
		int32_t L_179 = V_15;
		V_15 = ((int32_t)((int32_t)L_179+(int32_t)1));
	}

IL_02fa:
	{
		int32_t L_180 = V_15;
		UInt32U5BU5D_t2770800703* L_181 = V_4;
		NullCheck(L_181);
		if ((((int32_t)L_180) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_181)->max_length))))-(int32_t)4)))))
		{
			goto IL_029c;
		}
	}

IL_0307:
	{
		UInt32U5BU5D_t2770800703* L_182 = V_4;
		__this->set_expandedKey_12(L_182);
		return;
	}
}
// System.Void System.Security.Cryptography.AesTransform::.cctor()
extern "C"  void AesTransform__cctor_m2567644034 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AesTransform__cctor_m2567644034_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UInt32U5BU5D_t2770800703* L_0 = ((UInt32U5BU5D_t2770800703*)SZArrayNew(UInt32U5BU5D_t2770800703_il2cpp_TypeInfo_var, (uint32_t)((int32_t)30)));
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3057255366____U24U24fieldU2D1_1_FieldInfo_var), /*hidden argument*/NULL);
		((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->set_Rcon_15(L_0);
		ByteU5BU5D_t4116647657* L_1 = ((ByteU5BU5D_t4116647657*)SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256)));
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3057255366____U24U24fieldU2D2_2_FieldInfo_var), /*hidden argument*/NULL);
		((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->set_SBox_16(L_1);
		ByteU5BU5D_t4116647657* L_2 = ((ByteU5BU5D_t4116647657*)SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256)));
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_2, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3057255366____U24U24fieldU2D3_3_FieldInfo_var), /*hidden argument*/NULL);
		((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->set_iSBox_17(L_2);
		UInt32U5BU5D_t2770800703* L_3 = ((UInt32U5BU5D_t2770800703*)SZArrayNew(UInt32U5BU5D_t2770800703_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256)));
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_3, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3057255366____U24U24fieldU2D4_4_FieldInfo_var), /*hidden argument*/NULL);
		((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->set_T0_18(L_3);
		UInt32U5BU5D_t2770800703* L_4 = ((UInt32U5BU5D_t2770800703*)SZArrayNew(UInt32U5BU5D_t2770800703_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256)));
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_4, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3057255366____U24U24fieldU2D5_5_FieldInfo_var), /*hidden argument*/NULL);
		((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->set_T1_19(L_4);
		UInt32U5BU5D_t2770800703* L_5 = ((UInt32U5BU5D_t2770800703*)SZArrayNew(UInt32U5BU5D_t2770800703_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256)));
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3057255366____U24U24fieldU2D6_6_FieldInfo_var), /*hidden argument*/NULL);
		((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->set_T2_20(L_5);
		UInt32U5BU5D_t2770800703* L_6 = ((UInt32U5BU5D_t2770800703*)SZArrayNew(UInt32U5BU5D_t2770800703_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256)));
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_6, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3057255366____U24U24fieldU2D7_7_FieldInfo_var), /*hidden argument*/NULL);
		((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->set_T3_21(L_6);
		UInt32U5BU5D_t2770800703* L_7 = ((UInt32U5BU5D_t2770800703*)SZArrayNew(UInt32U5BU5D_t2770800703_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256)));
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_7, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3057255366____U24U24fieldU2D8_8_FieldInfo_var), /*hidden argument*/NULL);
		((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->set_iT0_22(L_7);
		UInt32U5BU5D_t2770800703* L_8 = ((UInt32U5BU5D_t2770800703*)SZArrayNew(UInt32U5BU5D_t2770800703_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256)));
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_8, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3057255366____U24U24fieldU2D9_9_FieldInfo_var), /*hidden argument*/NULL);
		((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->set_iT1_23(L_8);
		UInt32U5BU5D_t2770800703* L_9 = ((UInt32U5BU5D_t2770800703*)SZArrayNew(UInt32U5BU5D_t2770800703_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256)));
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_9, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3057255366____U24U24fieldU2D10_10_FieldInfo_var), /*hidden argument*/NULL);
		((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->set_iT2_24(L_9);
		UInt32U5BU5D_t2770800703* L_10 = ((UInt32U5BU5D_t2770800703*)SZArrayNew(UInt32U5BU5D_t2770800703_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256)));
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_10, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3057255366____U24U24fieldU2D11_11_FieldInfo_var), /*hidden argument*/NULL);
		((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->set_iT3_25(L_10);
		return;
	}
}
// System.Void System.Security.Cryptography.AesTransform::ECB(System.Byte[],System.Byte[])
extern "C"  void AesTransform_ECB_m240244807 (AesTransform_t2957123611 * __this, ByteU5BU5D_t4116647657* ___input0, ByteU5BU5D_t4116647657* ___output1, const MethodInfo* method)
{
	{
		bool L_0 = ((SymmetricTransform_t3802591843 *)__this)->get_encrypt_1();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		ByteU5BU5D_t4116647657* L_1 = ___input0;
		ByteU5BU5D_t4116647657* L_2 = ___output1;
		UInt32U5BU5D_t2770800703* L_3 = __this->get_expandedKey_12();
		AesTransform_Encrypt128_m424393011(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		goto IL_002c;
	}

IL_001e:
	{
		ByteU5BU5D_t4116647657* L_4 = ___input0;
		ByteU5BU5D_t4116647657* L_5 = ___output1;
		UInt32U5BU5D_t2770800703* L_6 = __this->get_expandedKey_12();
		AesTransform_Decrypt128_m3018534522(__this, L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// System.UInt32 System.Security.Cryptography.AesTransform::SubByte(System.UInt32)
extern "C"  uint32_t AesTransform_SubByte_m3350159546 (AesTransform_t2957123611 * __this, uint32_t ___a0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AesTransform_SubByte_m3350159546_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	uint32_t V_1 = 0;
	{
		uint32_t L_0 = ___a0;
		V_0 = ((int32_t)((int32_t)((int32_t)255)&(int32_t)L_0));
		IL2CPP_RUNTIME_CLASS_INIT(AesTransform_t2957123611_il2cpp_TypeInfo_var);
		ByteU5BU5D_t4116647657* L_1 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_2 = V_0;
		NullCheck(L_1);
		uintptr_t L_3 = (((uintptr_t)L_2));
		uint8_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_1 = L_4;
		uint32_t L_5 = ___a0;
		V_0 = ((int32_t)((int32_t)((int32_t)255)&(int32_t)((int32_t)((uint32_t)L_5>>8))));
		uint32_t L_6 = V_1;
		ByteU5BU5D_t4116647657* L_7 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_8 = V_0;
		NullCheck(L_7);
		uintptr_t L_9 = (((uintptr_t)L_8));
		uint8_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_1 = ((int32_t)((int32_t)L_6|(int32_t)((int32_t)((int32_t)L_10<<(int32_t)8))));
		uint32_t L_11 = ___a0;
		V_0 = ((int32_t)((int32_t)((int32_t)255)&(int32_t)((int32_t)((uint32_t)L_11>>((int32_t)16)))));
		uint32_t L_12 = V_1;
		ByteU5BU5D_t4116647657* L_13 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_14 = V_0;
		NullCheck(L_13);
		uintptr_t L_15 = (((uintptr_t)L_14));
		uint8_t L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_1 = ((int32_t)((int32_t)L_12|(int32_t)((int32_t)((int32_t)L_16<<(int32_t)((int32_t)16)))));
		uint32_t L_17 = ___a0;
		V_0 = ((int32_t)((int32_t)((int32_t)255)&(int32_t)((int32_t)((uint32_t)L_17>>((int32_t)24)))));
		uint32_t L_18 = V_1;
		ByteU5BU5D_t4116647657* L_19 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_20 = V_0;
		NullCheck(L_19);
		uintptr_t L_21 = (((uintptr_t)L_20));
		uint8_t L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		return ((int32_t)((int32_t)L_18|(int32_t)((int32_t)((int32_t)L_22<<(int32_t)((int32_t)24)))));
	}
}
// System.Void System.Security.Cryptography.AesTransform::Encrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern "C"  void AesTransform_Encrypt128_m424393011 (AesTransform_t2957123611 * __this, ByteU5BU5D_t4116647657* ___indata0, ByteU5BU5D_t4116647657* ___outdata1, UInt32U5BU5D_t2770800703* ___ekey2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AesTransform_Encrypt128_m424393011_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	uint32_t V_1 = 0;
	uint32_t V_2 = 0;
	uint32_t V_3 = 0;
	uint32_t V_4 = 0;
	uint32_t V_5 = 0;
	uint32_t V_6 = 0;
	uint32_t V_7 = 0;
	int32_t V_8 = 0;
	{
		V_8 = ((int32_t)40);
		ByteU5BU5D_t4116647657* L_0 = ___indata0;
		NullCheck(L_0);
		int32_t L_1 = 0;
		uint8_t L_2 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1));
		ByteU5BU5D_t4116647657* L_3 = ___indata0;
		NullCheck(L_3);
		int32_t L_4 = 1;
		uint8_t L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		ByteU5BU5D_t4116647657* L_6 = ___indata0;
		NullCheck(L_6);
		int32_t L_7 = 2;
		uint8_t L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		ByteU5BU5D_t4116647657* L_9 = ___indata0;
		NullCheck(L_9);
		int32_t L_10 = 3;
		uint8_t L_11 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		UInt32U5BU5D_t2770800703* L_12 = ___ekey2;
		NullCheck(L_12);
		int32_t L_13 = 0;
		uint32_t L_14 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_2<<(int32_t)((int32_t)24)))|(int32_t)((int32_t)((int32_t)L_5<<(int32_t)((int32_t)16)))))|(int32_t)((int32_t)((int32_t)L_8<<(int32_t)8))))|(int32_t)L_11))^(int32_t)L_14));
		ByteU5BU5D_t4116647657* L_15 = ___indata0;
		NullCheck(L_15);
		int32_t L_16 = 4;
		uint8_t L_17 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		ByteU5BU5D_t4116647657* L_18 = ___indata0;
		NullCheck(L_18);
		int32_t L_19 = 5;
		uint8_t L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		ByteU5BU5D_t4116647657* L_21 = ___indata0;
		NullCheck(L_21);
		int32_t L_22 = 6;
		uint8_t L_23 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		ByteU5BU5D_t4116647657* L_24 = ___indata0;
		NullCheck(L_24);
		int32_t L_25 = 7;
		uint8_t L_26 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		UInt32U5BU5D_t2770800703* L_27 = ___ekey2;
		NullCheck(L_27);
		int32_t L_28 = 1;
		uint32_t L_29 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_17<<(int32_t)((int32_t)24)))|(int32_t)((int32_t)((int32_t)L_20<<(int32_t)((int32_t)16)))))|(int32_t)((int32_t)((int32_t)L_23<<(int32_t)8))))|(int32_t)L_26))^(int32_t)L_29));
		ByteU5BU5D_t4116647657* L_30 = ___indata0;
		NullCheck(L_30);
		int32_t L_31 = 8;
		uint8_t L_32 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		ByteU5BU5D_t4116647657* L_33 = ___indata0;
		NullCheck(L_33);
		int32_t L_34 = ((int32_t)9);
		uint8_t L_35 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
		ByteU5BU5D_t4116647657* L_36 = ___indata0;
		NullCheck(L_36);
		int32_t L_37 = ((int32_t)10);
		uint8_t L_38 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		ByteU5BU5D_t4116647657* L_39 = ___indata0;
		NullCheck(L_39);
		int32_t L_40 = ((int32_t)11);
		uint8_t L_41 = (L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
		UInt32U5BU5D_t2770800703* L_42 = ___ekey2;
		NullCheck(L_42);
		int32_t L_43 = 2;
		uint32_t L_44 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_32<<(int32_t)((int32_t)24)))|(int32_t)((int32_t)((int32_t)L_35<<(int32_t)((int32_t)16)))))|(int32_t)((int32_t)((int32_t)L_38<<(int32_t)8))))|(int32_t)L_41))^(int32_t)L_44));
		ByteU5BU5D_t4116647657* L_45 = ___indata0;
		NullCheck(L_45);
		int32_t L_46 = ((int32_t)12);
		uint8_t L_47 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_46));
		ByteU5BU5D_t4116647657* L_48 = ___indata0;
		NullCheck(L_48);
		int32_t L_49 = ((int32_t)13);
		uint8_t L_50 = (L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_49));
		ByteU5BU5D_t4116647657* L_51 = ___indata0;
		NullCheck(L_51);
		int32_t L_52 = ((int32_t)14);
		uint8_t L_53 = (L_51)->GetAt(static_cast<il2cpp_array_size_t>(L_52));
		ByteU5BU5D_t4116647657* L_54 = ___indata0;
		NullCheck(L_54);
		int32_t L_55 = ((int32_t)15);
		uint8_t L_56 = (L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_55));
		UInt32U5BU5D_t2770800703* L_57 = ___ekey2;
		NullCheck(L_57);
		int32_t L_58 = 3;
		uint32_t L_59 = (L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_58));
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_47<<(int32_t)((int32_t)24)))|(int32_t)((int32_t)((int32_t)L_50<<(int32_t)((int32_t)16)))))|(int32_t)((int32_t)((int32_t)L_53<<(int32_t)8))))|(int32_t)L_56))^(int32_t)L_59));
		IL2CPP_RUNTIME_CLASS_INIT(AesTransform_t2957123611_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t2770800703* L_60 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_61 = V_0;
		NullCheck(L_60);
		uintptr_t L_62 = (((uintptr_t)((int32_t)((uint32_t)L_61>>((int32_t)24)))));
		uint32_t L_63 = (L_60)->GetAt(static_cast<il2cpp_array_size_t>(L_62));
		UInt32U5BU5D_t2770800703* L_64 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_65 = V_1;
		NullCheck(L_64);
		int32_t L_66 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_65>>((int32_t)16))))));
		uint32_t L_67 = (L_64)->GetAt(static_cast<il2cpp_array_size_t>(L_66));
		UInt32U5BU5D_t2770800703* L_68 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_69 = V_2;
		NullCheck(L_68);
		int32_t L_70 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_69>>8)))));
		uint32_t L_71 = (L_68)->GetAt(static_cast<il2cpp_array_size_t>(L_70));
		UInt32U5BU5D_t2770800703* L_72 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_73 = V_3;
		NullCheck(L_72);
		int32_t L_74 = (((int32_t)((uint8_t)L_73)));
		uint32_t L_75 = (L_72)->GetAt(static_cast<il2cpp_array_size_t>(L_74));
		UInt32U5BU5D_t2770800703* L_76 = ___ekey2;
		NullCheck(L_76);
		int32_t L_77 = 4;
		uint32_t L_78 = (L_76)->GetAt(static_cast<il2cpp_array_size_t>(L_77));
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_63^(int32_t)L_67))^(int32_t)L_71))^(int32_t)L_75))^(int32_t)L_78));
		UInt32U5BU5D_t2770800703* L_79 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_80 = V_1;
		NullCheck(L_79);
		uintptr_t L_81 = (((uintptr_t)((int32_t)((uint32_t)L_80>>((int32_t)24)))));
		uint32_t L_82 = (L_79)->GetAt(static_cast<il2cpp_array_size_t>(L_81));
		UInt32U5BU5D_t2770800703* L_83 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_84 = V_2;
		NullCheck(L_83);
		int32_t L_85 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_84>>((int32_t)16))))));
		uint32_t L_86 = (L_83)->GetAt(static_cast<il2cpp_array_size_t>(L_85));
		UInt32U5BU5D_t2770800703* L_87 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_88 = V_3;
		NullCheck(L_87);
		int32_t L_89 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_88>>8)))));
		uint32_t L_90 = (L_87)->GetAt(static_cast<il2cpp_array_size_t>(L_89));
		UInt32U5BU5D_t2770800703* L_91 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_92 = V_0;
		NullCheck(L_91);
		int32_t L_93 = (((int32_t)((uint8_t)L_92)));
		uint32_t L_94 = (L_91)->GetAt(static_cast<il2cpp_array_size_t>(L_93));
		UInt32U5BU5D_t2770800703* L_95 = ___ekey2;
		NullCheck(L_95);
		int32_t L_96 = 5;
		uint32_t L_97 = (L_95)->GetAt(static_cast<il2cpp_array_size_t>(L_96));
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_82^(int32_t)L_86))^(int32_t)L_90))^(int32_t)L_94))^(int32_t)L_97));
		UInt32U5BU5D_t2770800703* L_98 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_99 = V_2;
		NullCheck(L_98);
		uintptr_t L_100 = (((uintptr_t)((int32_t)((uint32_t)L_99>>((int32_t)24)))));
		uint32_t L_101 = (L_98)->GetAt(static_cast<il2cpp_array_size_t>(L_100));
		UInt32U5BU5D_t2770800703* L_102 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_103 = V_3;
		NullCheck(L_102);
		int32_t L_104 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_103>>((int32_t)16))))));
		uint32_t L_105 = (L_102)->GetAt(static_cast<il2cpp_array_size_t>(L_104));
		UInt32U5BU5D_t2770800703* L_106 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_107 = V_0;
		NullCheck(L_106);
		int32_t L_108 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_107>>8)))));
		uint32_t L_109 = (L_106)->GetAt(static_cast<il2cpp_array_size_t>(L_108));
		UInt32U5BU5D_t2770800703* L_110 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_111 = V_1;
		NullCheck(L_110);
		int32_t L_112 = (((int32_t)((uint8_t)L_111)));
		uint32_t L_113 = (L_110)->GetAt(static_cast<il2cpp_array_size_t>(L_112));
		UInt32U5BU5D_t2770800703* L_114 = ___ekey2;
		NullCheck(L_114);
		int32_t L_115 = 6;
		uint32_t L_116 = (L_114)->GetAt(static_cast<il2cpp_array_size_t>(L_115));
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_101^(int32_t)L_105))^(int32_t)L_109))^(int32_t)L_113))^(int32_t)L_116));
		UInt32U5BU5D_t2770800703* L_117 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_118 = V_3;
		NullCheck(L_117);
		uintptr_t L_119 = (((uintptr_t)((int32_t)((uint32_t)L_118>>((int32_t)24)))));
		uint32_t L_120 = (L_117)->GetAt(static_cast<il2cpp_array_size_t>(L_119));
		UInt32U5BU5D_t2770800703* L_121 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_122 = V_0;
		NullCheck(L_121);
		int32_t L_123 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_122>>((int32_t)16))))));
		uint32_t L_124 = (L_121)->GetAt(static_cast<il2cpp_array_size_t>(L_123));
		UInt32U5BU5D_t2770800703* L_125 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_126 = V_1;
		NullCheck(L_125);
		int32_t L_127 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_126>>8)))));
		uint32_t L_128 = (L_125)->GetAt(static_cast<il2cpp_array_size_t>(L_127));
		UInt32U5BU5D_t2770800703* L_129 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_130 = V_2;
		NullCheck(L_129);
		int32_t L_131 = (((int32_t)((uint8_t)L_130)));
		uint32_t L_132 = (L_129)->GetAt(static_cast<il2cpp_array_size_t>(L_131));
		UInt32U5BU5D_t2770800703* L_133 = ___ekey2;
		NullCheck(L_133);
		int32_t L_134 = 7;
		uint32_t L_135 = (L_133)->GetAt(static_cast<il2cpp_array_size_t>(L_134));
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_120^(int32_t)L_124))^(int32_t)L_128))^(int32_t)L_132))^(int32_t)L_135));
		UInt32U5BU5D_t2770800703* L_136 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_137 = V_4;
		NullCheck(L_136);
		uintptr_t L_138 = (((uintptr_t)((int32_t)((uint32_t)L_137>>((int32_t)24)))));
		uint32_t L_139 = (L_136)->GetAt(static_cast<il2cpp_array_size_t>(L_138));
		UInt32U5BU5D_t2770800703* L_140 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_141 = V_5;
		NullCheck(L_140);
		int32_t L_142 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_141>>((int32_t)16))))));
		uint32_t L_143 = (L_140)->GetAt(static_cast<il2cpp_array_size_t>(L_142));
		UInt32U5BU5D_t2770800703* L_144 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_145 = V_6;
		NullCheck(L_144);
		int32_t L_146 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_145>>8)))));
		uint32_t L_147 = (L_144)->GetAt(static_cast<il2cpp_array_size_t>(L_146));
		UInt32U5BU5D_t2770800703* L_148 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_149 = V_7;
		NullCheck(L_148);
		int32_t L_150 = (((int32_t)((uint8_t)L_149)));
		uint32_t L_151 = (L_148)->GetAt(static_cast<il2cpp_array_size_t>(L_150));
		UInt32U5BU5D_t2770800703* L_152 = ___ekey2;
		NullCheck(L_152);
		int32_t L_153 = 8;
		uint32_t L_154 = (L_152)->GetAt(static_cast<il2cpp_array_size_t>(L_153));
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_139^(int32_t)L_143))^(int32_t)L_147))^(int32_t)L_151))^(int32_t)L_154));
		UInt32U5BU5D_t2770800703* L_155 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_156 = V_5;
		NullCheck(L_155);
		uintptr_t L_157 = (((uintptr_t)((int32_t)((uint32_t)L_156>>((int32_t)24)))));
		uint32_t L_158 = (L_155)->GetAt(static_cast<il2cpp_array_size_t>(L_157));
		UInt32U5BU5D_t2770800703* L_159 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_160 = V_6;
		NullCheck(L_159);
		int32_t L_161 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_160>>((int32_t)16))))));
		uint32_t L_162 = (L_159)->GetAt(static_cast<il2cpp_array_size_t>(L_161));
		UInt32U5BU5D_t2770800703* L_163 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_164 = V_7;
		NullCheck(L_163);
		int32_t L_165 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_164>>8)))));
		uint32_t L_166 = (L_163)->GetAt(static_cast<il2cpp_array_size_t>(L_165));
		UInt32U5BU5D_t2770800703* L_167 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_168 = V_4;
		NullCheck(L_167);
		int32_t L_169 = (((int32_t)((uint8_t)L_168)));
		uint32_t L_170 = (L_167)->GetAt(static_cast<il2cpp_array_size_t>(L_169));
		UInt32U5BU5D_t2770800703* L_171 = ___ekey2;
		NullCheck(L_171);
		int32_t L_172 = ((int32_t)9);
		uint32_t L_173 = (L_171)->GetAt(static_cast<il2cpp_array_size_t>(L_172));
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_158^(int32_t)L_162))^(int32_t)L_166))^(int32_t)L_170))^(int32_t)L_173));
		UInt32U5BU5D_t2770800703* L_174 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_175 = V_6;
		NullCheck(L_174);
		uintptr_t L_176 = (((uintptr_t)((int32_t)((uint32_t)L_175>>((int32_t)24)))));
		uint32_t L_177 = (L_174)->GetAt(static_cast<il2cpp_array_size_t>(L_176));
		UInt32U5BU5D_t2770800703* L_178 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_179 = V_7;
		NullCheck(L_178);
		int32_t L_180 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_179>>((int32_t)16))))));
		uint32_t L_181 = (L_178)->GetAt(static_cast<il2cpp_array_size_t>(L_180));
		UInt32U5BU5D_t2770800703* L_182 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_183 = V_4;
		NullCheck(L_182);
		int32_t L_184 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_183>>8)))));
		uint32_t L_185 = (L_182)->GetAt(static_cast<il2cpp_array_size_t>(L_184));
		UInt32U5BU5D_t2770800703* L_186 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_187 = V_5;
		NullCheck(L_186);
		int32_t L_188 = (((int32_t)((uint8_t)L_187)));
		uint32_t L_189 = (L_186)->GetAt(static_cast<il2cpp_array_size_t>(L_188));
		UInt32U5BU5D_t2770800703* L_190 = ___ekey2;
		NullCheck(L_190);
		int32_t L_191 = ((int32_t)10);
		uint32_t L_192 = (L_190)->GetAt(static_cast<il2cpp_array_size_t>(L_191));
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_177^(int32_t)L_181))^(int32_t)L_185))^(int32_t)L_189))^(int32_t)L_192));
		UInt32U5BU5D_t2770800703* L_193 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_194 = V_7;
		NullCheck(L_193);
		uintptr_t L_195 = (((uintptr_t)((int32_t)((uint32_t)L_194>>((int32_t)24)))));
		uint32_t L_196 = (L_193)->GetAt(static_cast<il2cpp_array_size_t>(L_195));
		UInt32U5BU5D_t2770800703* L_197 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_198 = V_4;
		NullCheck(L_197);
		int32_t L_199 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_198>>((int32_t)16))))));
		uint32_t L_200 = (L_197)->GetAt(static_cast<il2cpp_array_size_t>(L_199));
		UInt32U5BU5D_t2770800703* L_201 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_202 = V_5;
		NullCheck(L_201);
		int32_t L_203 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_202>>8)))));
		uint32_t L_204 = (L_201)->GetAt(static_cast<il2cpp_array_size_t>(L_203));
		UInt32U5BU5D_t2770800703* L_205 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_206 = V_6;
		NullCheck(L_205);
		int32_t L_207 = (((int32_t)((uint8_t)L_206)));
		uint32_t L_208 = (L_205)->GetAt(static_cast<il2cpp_array_size_t>(L_207));
		UInt32U5BU5D_t2770800703* L_209 = ___ekey2;
		NullCheck(L_209);
		int32_t L_210 = ((int32_t)11);
		uint32_t L_211 = (L_209)->GetAt(static_cast<il2cpp_array_size_t>(L_210));
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_196^(int32_t)L_200))^(int32_t)L_204))^(int32_t)L_208))^(int32_t)L_211));
		UInt32U5BU5D_t2770800703* L_212 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_213 = V_0;
		NullCheck(L_212);
		uintptr_t L_214 = (((uintptr_t)((int32_t)((uint32_t)L_213>>((int32_t)24)))));
		uint32_t L_215 = (L_212)->GetAt(static_cast<il2cpp_array_size_t>(L_214));
		UInt32U5BU5D_t2770800703* L_216 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_217 = V_1;
		NullCheck(L_216);
		int32_t L_218 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_217>>((int32_t)16))))));
		uint32_t L_219 = (L_216)->GetAt(static_cast<il2cpp_array_size_t>(L_218));
		UInt32U5BU5D_t2770800703* L_220 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_221 = V_2;
		NullCheck(L_220);
		int32_t L_222 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_221>>8)))));
		uint32_t L_223 = (L_220)->GetAt(static_cast<il2cpp_array_size_t>(L_222));
		UInt32U5BU5D_t2770800703* L_224 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_225 = V_3;
		NullCheck(L_224);
		int32_t L_226 = (((int32_t)((uint8_t)L_225)));
		uint32_t L_227 = (L_224)->GetAt(static_cast<il2cpp_array_size_t>(L_226));
		UInt32U5BU5D_t2770800703* L_228 = ___ekey2;
		NullCheck(L_228);
		int32_t L_229 = ((int32_t)12);
		uint32_t L_230 = (L_228)->GetAt(static_cast<il2cpp_array_size_t>(L_229));
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_215^(int32_t)L_219))^(int32_t)L_223))^(int32_t)L_227))^(int32_t)L_230));
		UInt32U5BU5D_t2770800703* L_231 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_232 = V_1;
		NullCheck(L_231);
		uintptr_t L_233 = (((uintptr_t)((int32_t)((uint32_t)L_232>>((int32_t)24)))));
		uint32_t L_234 = (L_231)->GetAt(static_cast<il2cpp_array_size_t>(L_233));
		UInt32U5BU5D_t2770800703* L_235 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_236 = V_2;
		NullCheck(L_235);
		int32_t L_237 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_236>>((int32_t)16))))));
		uint32_t L_238 = (L_235)->GetAt(static_cast<il2cpp_array_size_t>(L_237));
		UInt32U5BU5D_t2770800703* L_239 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_240 = V_3;
		NullCheck(L_239);
		int32_t L_241 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_240>>8)))));
		uint32_t L_242 = (L_239)->GetAt(static_cast<il2cpp_array_size_t>(L_241));
		UInt32U5BU5D_t2770800703* L_243 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_244 = V_0;
		NullCheck(L_243);
		int32_t L_245 = (((int32_t)((uint8_t)L_244)));
		uint32_t L_246 = (L_243)->GetAt(static_cast<il2cpp_array_size_t>(L_245));
		UInt32U5BU5D_t2770800703* L_247 = ___ekey2;
		NullCheck(L_247);
		int32_t L_248 = ((int32_t)13);
		uint32_t L_249 = (L_247)->GetAt(static_cast<il2cpp_array_size_t>(L_248));
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_234^(int32_t)L_238))^(int32_t)L_242))^(int32_t)L_246))^(int32_t)L_249));
		UInt32U5BU5D_t2770800703* L_250 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_251 = V_2;
		NullCheck(L_250);
		uintptr_t L_252 = (((uintptr_t)((int32_t)((uint32_t)L_251>>((int32_t)24)))));
		uint32_t L_253 = (L_250)->GetAt(static_cast<il2cpp_array_size_t>(L_252));
		UInt32U5BU5D_t2770800703* L_254 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_255 = V_3;
		NullCheck(L_254);
		int32_t L_256 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_255>>((int32_t)16))))));
		uint32_t L_257 = (L_254)->GetAt(static_cast<il2cpp_array_size_t>(L_256));
		UInt32U5BU5D_t2770800703* L_258 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_259 = V_0;
		NullCheck(L_258);
		int32_t L_260 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_259>>8)))));
		uint32_t L_261 = (L_258)->GetAt(static_cast<il2cpp_array_size_t>(L_260));
		UInt32U5BU5D_t2770800703* L_262 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_263 = V_1;
		NullCheck(L_262);
		int32_t L_264 = (((int32_t)((uint8_t)L_263)));
		uint32_t L_265 = (L_262)->GetAt(static_cast<il2cpp_array_size_t>(L_264));
		UInt32U5BU5D_t2770800703* L_266 = ___ekey2;
		NullCheck(L_266);
		int32_t L_267 = ((int32_t)14);
		uint32_t L_268 = (L_266)->GetAt(static_cast<il2cpp_array_size_t>(L_267));
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_253^(int32_t)L_257))^(int32_t)L_261))^(int32_t)L_265))^(int32_t)L_268));
		UInt32U5BU5D_t2770800703* L_269 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_270 = V_3;
		NullCheck(L_269);
		uintptr_t L_271 = (((uintptr_t)((int32_t)((uint32_t)L_270>>((int32_t)24)))));
		uint32_t L_272 = (L_269)->GetAt(static_cast<il2cpp_array_size_t>(L_271));
		UInt32U5BU5D_t2770800703* L_273 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_274 = V_0;
		NullCheck(L_273);
		int32_t L_275 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_274>>((int32_t)16))))));
		uint32_t L_276 = (L_273)->GetAt(static_cast<il2cpp_array_size_t>(L_275));
		UInt32U5BU5D_t2770800703* L_277 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_278 = V_1;
		NullCheck(L_277);
		int32_t L_279 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_278>>8)))));
		uint32_t L_280 = (L_277)->GetAt(static_cast<il2cpp_array_size_t>(L_279));
		UInt32U5BU5D_t2770800703* L_281 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_282 = V_2;
		NullCheck(L_281);
		int32_t L_283 = (((int32_t)((uint8_t)L_282)));
		uint32_t L_284 = (L_281)->GetAt(static_cast<il2cpp_array_size_t>(L_283));
		UInt32U5BU5D_t2770800703* L_285 = ___ekey2;
		NullCheck(L_285);
		int32_t L_286 = ((int32_t)15);
		uint32_t L_287 = (L_285)->GetAt(static_cast<il2cpp_array_size_t>(L_286));
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_272^(int32_t)L_276))^(int32_t)L_280))^(int32_t)L_284))^(int32_t)L_287));
		UInt32U5BU5D_t2770800703* L_288 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_289 = V_4;
		NullCheck(L_288);
		uintptr_t L_290 = (((uintptr_t)((int32_t)((uint32_t)L_289>>((int32_t)24)))));
		uint32_t L_291 = (L_288)->GetAt(static_cast<il2cpp_array_size_t>(L_290));
		UInt32U5BU5D_t2770800703* L_292 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_293 = V_5;
		NullCheck(L_292);
		int32_t L_294 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_293>>((int32_t)16))))));
		uint32_t L_295 = (L_292)->GetAt(static_cast<il2cpp_array_size_t>(L_294));
		UInt32U5BU5D_t2770800703* L_296 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_297 = V_6;
		NullCheck(L_296);
		int32_t L_298 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_297>>8)))));
		uint32_t L_299 = (L_296)->GetAt(static_cast<il2cpp_array_size_t>(L_298));
		UInt32U5BU5D_t2770800703* L_300 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_301 = V_7;
		NullCheck(L_300);
		int32_t L_302 = (((int32_t)((uint8_t)L_301)));
		uint32_t L_303 = (L_300)->GetAt(static_cast<il2cpp_array_size_t>(L_302));
		UInt32U5BU5D_t2770800703* L_304 = ___ekey2;
		NullCheck(L_304);
		int32_t L_305 = ((int32_t)16);
		uint32_t L_306 = (L_304)->GetAt(static_cast<il2cpp_array_size_t>(L_305));
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_291^(int32_t)L_295))^(int32_t)L_299))^(int32_t)L_303))^(int32_t)L_306));
		UInt32U5BU5D_t2770800703* L_307 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_308 = V_5;
		NullCheck(L_307);
		uintptr_t L_309 = (((uintptr_t)((int32_t)((uint32_t)L_308>>((int32_t)24)))));
		uint32_t L_310 = (L_307)->GetAt(static_cast<il2cpp_array_size_t>(L_309));
		UInt32U5BU5D_t2770800703* L_311 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_312 = V_6;
		NullCheck(L_311);
		int32_t L_313 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_312>>((int32_t)16))))));
		uint32_t L_314 = (L_311)->GetAt(static_cast<il2cpp_array_size_t>(L_313));
		UInt32U5BU5D_t2770800703* L_315 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_316 = V_7;
		NullCheck(L_315);
		int32_t L_317 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_316>>8)))));
		uint32_t L_318 = (L_315)->GetAt(static_cast<il2cpp_array_size_t>(L_317));
		UInt32U5BU5D_t2770800703* L_319 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_320 = V_4;
		NullCheck(L_319);
		int32_t L_321 = (((int32_t)((uint8_t)L_320)));
		uint32_t L_322 = (L_319)->GetAt(static_cast<il2cpp_array_size_t>(L_321));
		UInt32U5BU5D_t2770800703* L_323 = ___ekey2;
		NullCheck(L_323);
		int32_t L_324 = ((int32_t)17);
		uint32_t L_325 = (L_323)->GetAt(static_cast<il2cpp_array_size_t>(L_324));
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_310^(int32_t)L_314))^(int32_t)L_318))^(int32_t)L_322))^(int32_t)L_325));
		UInt32U5BU5D_t2770800703* L_326 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_327 = V_6;
		NullCheck(L_326);
		uintptr_t L_328 = (((uintptr_t)((int32_t)((uint32_t)L_327>>((int32_t)24)))));
		uint32_t L_329 = (L_326)->GetAt(static_cast<il2cpp_array_size_t>(L_328));
		UInt32U5BU5D_t2770800703* L_330 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_331 = V_7;
		NullCheck(L_330);
		int32_t L_332 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_331>>((int32_t)16))))));
		uint32_t L_333 = (L_330)->GetAt(static_cast<il2cpp_array_size_t>(L_332));
		UInt32U5BU5D_t2770800703* L_334 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_335 = V_4;
		NullCheck(L_334);
		int32_t L_336 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_335>>8)))));
		uint32_t L_337 = (L_334)->GetAt(static_cast<il2cpp_array_size_t>(L_336));
		UInt32U5BU5D_t2770800703* L_338 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_339 = V_5;
		NullCheck(L_338);
		int32_t L_340 = (((int32_t)((uint8_t)L_339)));
		uint32_t L_341 = (L_338)->GetAt(static_cast<il2cpp_array_size_t>(L_340));
		UInt32U5BU5D_t2770800703* L_342 = ___ekey2;
		NullCheck(L_342);
		int32_t L_343 = ((int32_t)18);
		uint32_t L_344 = (L_342)->GetAt(static_cast<il2cpp_array_size_t>(L_343));
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_329^(int32_t)L_333))^(int32_t)L_337))^(int32_t)L_341))^(int32_t)L_344));
		UInt32U5BU5D_t2770800703* L_345 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_346 = V_7;
		NullCheck(L_345);
		uintptr_t L_347 = (((uintptr_t)((int32_t)((uint32_t)L_346>>((int32_t)24)))));
		uint32_t L_348 = (L_345)->GetAt(static_cast<il2cpp_array_size_t>(L_347));
		UInt32U5BU5D_t2770800703* L_349 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_350 = V_4;
		NullCheck(L_349);
		int32_t L_351 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_350>>((int32_t)16))))));
		uint32_t L_352 = (L_349)->GetAt(static_cast<il2cpp_array_size_t>(L_351));
		UInt32U5BU5D_t2770800703* L_353 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_354 = V_5;
		NullCheck(L_353);
		int32_t L_355 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_354>>8)))));
		uint32_t L_356 = (L_353)->GetAt(static_cast<il2cpp_array_size_t>(L_355));
		UInt32U5BU5D_t2770800703* L_357 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_358 = V_6;
		NullCheck(L_357);
		int32_t L_359 = (((int32_t)((uint8_t)L_358)));
		uint32_t L_360 = (L_357)->GetAt(static_cast<il2cpp_array_size_t>(L_359));
		UInt32U5BU5D_t2770800703* L_361 = ___ekey2;
		NullCheck(L_361);
		int32_t L_362 = ((int32_t)19);
		uint32_t L_363 = (L_361)->GetAt(static_cast<il2cpp_array_size_t>(L_362));
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_348^(int32_t)L_352))^(int32_t)L_356))^(int32_t)L_360))^(int32_t)L_363));
		UInt32U5BU5D_t2770800703* L_364 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_365 = V_0;
		NullCheck(L_364);
		uintptr_t L_366 = (((uintptr_t)((int32_t)((uint32_t)L_365>>((int32_t)24)))));
		uint32_t L_367 = (L_364)->GetAt(static_cast<il2cpp_array_size_t>(L_366));
		UInt32U5BU5D_t2770800703* L_368 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_369 = V_1;
		NullCheck(L_368);
		int32_t L_370 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_369>>((int32_t)16))))));
		uint32_t L_371 = (L_368)->GetAt(static_cast<il2cpp_array_size_t>(L_370));
		UInt32U5BU5D_t2770800703* L_372 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_373 = V_2;
		NullCheck(L_372);
		int32_t L_374 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_373>>8)))));
		uint32_t L_375 = (L_372)->GetAt(static_cast<il2cpp_array_size_t>(L_374));
		UInt32U5BU5D_t2770800703* L_376 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_377 = V_3;
		NullCheck(L_376);
		int32_t L_378 = (((int32_t)((uint8_t)L_377)));
		uint32_t L_379 = (L_376)->GetAt(static_cast<il2cpp_array_size_t>(L_378));
		UInt32U5BU5D_t2770800703* L_380 = ___ekey2;
		NullCheck(L_380);
		int32_t L_381 = ((int32_t)20);
		uint32_t L_382 = (L_380)->GetAt(static_cast<il2cpp_array_size_t>(L_381));
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_367^(int32_t)L_371))^(int32_t)L_375))^(int32_t)L_379))^(int32_t)L_382));
		UInt32U5BU5D_t2770800703* L_383 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_384 = V_1;
		NullCheck(L_383);
		uintptr_t L_385 = (((uintptr_t)((int32_t)((uint32_t)L_384>>((int32_t)24)))));
		uint32_t L_386 = (L_383)->GetAt(static_cast<il2cpp_array_size_t>(L_385));
		UInt32U5BU5D_t2770800703* L_387 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_388 = V_2;
		NullCheck(L_387);
		int32_t L_389 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_388>>((int32_t)16))))));
		uint32_t L_390 = (L_387)->GetAt(static_cast<il2cpp_array_size_t>(L_389));
		UInt32U5BU5D_t2770800703* L_391 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_392 = V_3;
		NullCheck(L_391);
		int32_t L_393 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_392>>8)))));
		uint32_t L_394 = (L_391)->GetAt(static_cast<il2cpp_array_size_t>(L_393));
		UInt32U5BU5D_t2770800703* L_395 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_396 = V_0;
		NullCheck(L_395);
		int32_t L_397 = (((int32_t)((uint8_t)L_396)));
		uint32_t L_398 = (L_395)->GetAt(static_cast<il2cpp_array_size_t>(L_397));
		UInt32U5BU5D_t2770800703* L_399 = ___ekey2;
		NullCheck(L_399);
		int32_t L_400 = ((int32_t)21);
		uint32_t L_401 = (L_399)->GetAt(static_cast<il2cpp_array_size_t>(L_400));
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_386^(int32_t)L_390))^(int32_t)L_394))^(int32_t)L_398))^(int32_t)L_401));
		UInt32U5BU5D_t2770800703* L_402 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_403 = V_2;
		NullCheck(L_402);
		uintptr_t L_404 = (((uintptr_t)((int32_t)((uint32_t)L_403>>((int32_t)24)))));
		uint32_t L_405 = (L_402)->GetAt(static_cast<il2cpp_array_size_t>(L_404));
		UInt32U5BU5D_t2770800703* L_406 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_407 = V_3;
		NullCheck(L_406);
		int32_t L_408 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_407>>((int32_t)16))))));
		uint32_t L_409 = (L_406)->GetAt(static_cast<il2cpp_array_size_t>(L_408));
		UInt32U5BU5D_t2770800703* L_410 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_411 = V_0;
		NullCheck(L_410);
		int32_t L_412 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_411>>8)))));
		uint32_t L_413 = (L_410)->GetAt(static_cast<il2cpp_array_size_t>(L_412));
		UInt32U5BU5D_t2770800703* L_414 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_415 = V_1;
		NullCheck(L_414);
		int32_t L_416 = (((int32_t)((uint8_t)L_415)));
		uint32_t L_417 = (L_414)->GetAt(static_cast<il2cpp_array_size_t>(L_416));
		UInt32U5BU5D_t2770800703* L_418 = ___ekey2;
		NullCheck(L_418);
		int32_t L_419 = ((int32_t)22);
		uint32_t L_420 = (L_418)->GetAt(static_cast<il2cpp_array_size_t>(L_419));
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_405^(int32_t)L_409))^(int32_t)L_413))^(int32_t)L_417))^(int32_t)L_420));
		UInt32U5BU5D_t2770800703* L_421 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_422 = V_3;
		NullCheck(L_421);
		uintptr_t L_423 = (((uintptr_t)((int32_t)((uint32_t)L_422>>((int32_t)24)))));
		uint32_t L_424 = (L_421)->GetAt(static_cast<il2cpp_array_size_t>(L_423));
		UInt32U5BU5D_t2770800703* L_425 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_426 = V_0;
		NullCheck(L_425);
		int32_t L_427 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_426>>((int32_t)16))))));
		uint32_t L_428 = (L_425)->GetAt(static_cast<il2cpp_array_size_t>(L_427));
		UInt32U5BU5D_t2770800703* L_429 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_430 = V_1;
		NullCheck(L_429);
		int32_t L_431 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_430>>8)))));
		uint32_t L_432 = (L_429)->GetAt(static_cast<il2cpp_array_size_t>(L_431));
		UInt32U5BU5D_t2770800703* L_433 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_434 = V_2;
		NullCheck(L_433);
		int32_t L_435 = (((int32_t)((uint8_t)L_434)));
		uint32_t L_436 = (L_433)->GetAt(static_cast<il2cpp_array_size_t>(L_435));
		UInt32U5BU5D_t2770800703* L_437 = ___ekey2;
		NullCheck(L_437);
		int32_t L_438 = ((int32_t)23);
		uint32_t L_439 = (L_437)->GetAt(static_cast<il2cpp_array_size_t>(L_438));
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_424^(int32_t)L_428))^(int32_t)L_432))^(int32_t)L_436))^(int32_t)L_439));
		UInt32U5BU5D_t2770800703* L_440 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_441 = V_4;
		NullCheck(L_440);
		uintptr_t L_442 = (((uintptr_t)((int32_t)((uint32_t)L_441>>((int32_t)24)))));
		uint32_t L_443 = (L_440)->GetAt(static_cast<il2cpp_array_size_t>(L_442));
		UInt32U5BU5D_t2770800703* L_444 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_445 = V_5;
		NullCheck(L_444);
		int32_t L_446 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_445>>((int32_t)16))))));
		uint32_t L_447 = (L_444)->GetAt(static_cast<il2cpp_array_size_t>(L_446));
		UInt32U5BU5D_t2770800703* L_448 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_449 = V_6;
		NullCheck(L_448);
		int32_t L_450 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_449>>8)))));
		uint32_t L_451 = (L_448)->GetAt(static_cast<il2cpp_array_size_t>(L_450));
		UInt32U5BU5D_t2770800703* L_452 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_453 = V_7;
		NullCheck(L_452);
		int32_t L_454 = (((int32_t)((uint8_t)L_453)));
		uint32_t L_455 = (L_452)->GetAt(static_cast<il2cpp_array_size_t>(L_454));
		UInt32U5BU5D_t2770800703* L_456 = ___ekey2;
		NullCheck(L_456);
		int32_t L_457 = ((int32_t)24);
		uint32_t L_458 = (L_456)->GetAt(static_cast<il2cpp_array_size_t>(L_457));
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_443^(int32_t)L_447))^(int32_t)L_451))^(int32_t)L_455))^(int32_t)L_458));
		UInt32U5BU5D_t2770800703* L_459 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_460 = V_5;
		NullCheck(L_459);
		uintptr_t L_461 = (((uintptr_t)((int32_t)((uint32_t)L_460>>((int32_t)24)))));
		uint32_t L_462 = (L_459)->GetAt(static_cast<il2cpp_array_size_t>(L_461));
		UInt32U5BU5D_t2770800703* L_463 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_464 = V_6;
		NullCheck(L_463);
		int32_t L_465 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_464>>((int32_t)16))))));
		uint32_t L_466 = (L_463)->GetAt(static_cast<il2cpp_array_size_t>(L_465));
		UInt32U5BU5D_t2770800703* L_467 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_468 = V_7;
		NullCheck(L_467);
		int32_t L_469 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_468>>8)))));
		uint32_t L_470 = (L_467)->GetAt(static_cast<il2cpp_array_size_t>(L_469));
		UInt32U5BU5D_t2770800703* L_471 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_472 = V_4;
		NullCheck(L_471);
		int32_t L_473 = (((int32_t)((uint8_t)L_472)));
		uint32_t L_474 = (L_471)->GetAt(static_cast<il2cpp_array_size_t>(L_473));
		UInt32U5BU5D_t2770800703* L_475 = ___ekey2;
		NullCheck(L_475);
		int32_t L_476 = ((int32_t)25);
		uint32_t L_477 = (L_475)->GetAt(static_cast<il2cpp_array_size_t>(L_476));
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_462^(int32_t)L_466))^(int32_t)L_470))^(int32_t)L_474))^(int32_t)L_477));
		UInt32U5BU5D_t2770800703* L_478 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_479 = V_6;
		NullCheck(L_478);
		uintptr_t L_480 = (((uintptr_t)((int32_t)((uint32_t)L_479>>((int32_t)24)))));
		uint32_t L_481 = (L_478)->GetAt(static_cast<il2cpp_array_size_t>(L_480));
		UInt32U5BU5D_t2770800703* L_482 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_483 = V_7;
		NullCheck(L_482);
		int32_t L_484 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_483>>((int32_t)16))))));
		uint32_t L_485 = (L_482)->GetAt(static_cast<il2cpp_array_size_t>(L_484));
		UInt32U5BU5D_t2770800703* L_486 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_487 = V_4;
		NullCheck(L_486);
		int32_t L_488 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_487>>8)))));
		uint32_t L_489 = (L_486)->GetAt(static_cast<il2cpp_array_size_t>(L_488));
		UInt32U5BU5D_t2770800703* L_490 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_491 = V_5;
		NullCheck(L_490);
		int32_t L_492 = (((int32_t)((uint8_t)L_491)));
		uint32_t L_493 = (L_490)->GetAt(static_cast<il2cpp_array_size_t>(L_492));
		UInt32U5BU5D_t2770800703* L_494 = ___ekey2;
		NullCheck(L_494);
		int32_t L_495 = ((int32_t)26);
		uint32_t L_496 = (L_494)->GetAt(static_cast<il2cpp_array_size_t>(L_495));
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_481^(int32_t)L_485))^(int32_t)L_489))^(int32_t)L_493))^(int32_t)L_496));
		UInt32U5BU5D_t2770800703* L_497 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_498 = V_7;
		NullCheck(L_497);
		uintptr_t L_499 = (((uintptr_t)((int32_t)((uint32_t)L_498>>((int32_t)24)))));
		uint32_t L_500 = (L_497)->GetAt(static_cast<il2cpp_array_size_t>(L_499));
		UInt32U5BU5D_t2770800703* L_501 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_502 = V_4;
		NullCheck(L_501);
		int32_t L_503 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_502>>((int32_t)16))))));
		uint32_t L_504 = (L_501)->GetAt(static_cast<il2cpp_array_size_t>(L_503));
		UInt32U5BU5D_t2770800703* L_505 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_506 = V_5;
		NullCheck(L_505);
		int32_t L_507 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_506>>8)))));
		uint32_t L_508 = (L_505)->GetAt(static_cast<il2cpp_array_size_t>(L_507));
		UInt32U5BU5D_t2770800703* L_509 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_510 = V_6;
		NullCheck(L_509);
		int32_t L_511 = (((int32_t)((uint8_t)L_510)));
		uint32_t L_512 = (L_509)->GetAt(static_cast<il2cpp_array_size_t>(L_511));
		UInt32U5BU5D_t2770800703* L_513 = ___ekey2;
		NullCheck(L_513);
		int32_t L_514 = ((int32_t)27);
		uint32_t L_515 = (L_513)->GetAt(static_cast<il2cpp_array_size_t>(L_514));
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_500^(int32_t)L_504))^(int32_t)L_508))^(int32_t)L_512))^(int32_t)L_515));
		UInt32U5BU5D_t2770800703* L_516 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_517 = V_0;
		NullCheck(L_516);
		uintptr_t L_518 = (((uintptr_t)((int32_t)((uint32_t)L_517>>((int32_t)24)))));
		uint32_t L_519 = (L_516)->GetAt(static_cast<il2cpp_array_size_t>(L_518));
		UInt32U5BU5D_t2770800703* L_520 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_521 = V_1;
		NullCheck(L_520);
		int32_t L_522 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_521>>((int32_t)16))))));
		uint32_t L_523 = (L_520)->GetAt(static_cast<il2cpp_array_size_t>(L_522));
		UInt32U5BU5D_t2770800703* L_524 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_525 = V_2;
		NullCheck(L_524);
		int32_t L_526 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_525>>8)))));
		uint32_t L_527 = (L_524)->GetAt(static_cast<il2cpp_array_size_t>(L_526));
		UInt32U5BU5D_t2770800703* L_528 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_529 = V_3;
		NullCheck(L_528);
		int32_t L_530 = (((int32_t)((uint8_t)L_529)));
		uint32_t L_531 = (L_528)->GetAt(static_cast<il2cpp_array_size_t>(L_530));
		UInt32U5BU5D_t2770800703* L_532 = ___ekey2;
		NullCheck(L_532);
		int32_t L_533 = ((int32_t)28);
		uint32_t L_534 = (L_532)->GetAt(static_cast<il2cpp_array_size_t>(L_533));
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_519^(int32_t)L_523))^(int32_t)L_527))^(int32_t)L_531))^(int32_t)L_534));
		UInt32U5BU5D_t2770800703* L_535 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_536 = V_1;
		NullCheck(L_535);
		uintptr_t L_537 = (((uintptr_t)((int32_t)((uint32_t)L_536>>((int32_t)24)))));
		uint32_t L_538 = (L_535)->GetAt(static_cast<il2cpp_array_size_t>(L_537));
		UInt32U5BU5D_t2770800703* L_539 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_540 = V_2;
		NullCheck(L_539);
		int32_t L_541 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_540>>((int32_t)16))))));
		uint32_t L_542 = (L_539)->GetAt(static_cast<il2cpp_array_size_t>(L_541));
		UInt32U5BU5D_t2770800703* L_543 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_544 = V_3;
		NullCheck(L_543);
		int32_t L_545 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_544>>8)))));
		uint32_t L_546 = (L_543)->GetAt(static_cast<il2cpp_array_size_t>(L_545));
		UInt32U5BU5D_t2770800703* L_547 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_548 = V_0;
		NullCheck(L_547);
		int32_t L_549 = (((int32_t)((uint8_t)L_548)));
		uint32_t L_550 = (L_547)->GetAt(static_cast<il2cpp_array_size_t>(L_549));
		UInt32U5BU5D_t2770800703* L_551 = ___ekey2;
		NullCheck(L_551);
		int32_t L_552 = ((int32_t)29);
		uint32_t L_553 = (L_551)->GetAt(static_cast<il2cpp_array_size_t>(L_552));
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_538^(int32_t)L_542))^(int32_t)L_546))^(int32_t)L_550))^(int32_t)L_553));
		UInt32U5BU5D_t2770800703* L_554 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_555 = V_2;
		NullCheck(L_554);
		uintptr_t L_556 = (((uintptr_t)((int32_t)((uint32_t)L_555>>((int32_t)24)))));
		uint32_t L_557 = (L_554)->GetAt(static_cast<il2cpp_array_size_t>(L_556));
		UInt32U5BU5D_t2770800703* L_558 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_559 = V_3;
		NullCheck(L_558);
		int32_t L_560 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_559>>((int32_t)16))))));
		uint32_t L_561 = (L_558)->GetAt(static_cast<il2cpp_array_size_t>(L_560));
		UInt32U5BU5D_t2770800703* L_562 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_563 = V_0;
		NullCheck(L_562);
		int32_t L_564 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_563>>8)))));
		uint32_t L_565 = (L_562)->GetAt(static_cast<il2cpp_array_size_t>(L_564));
		UInt32U5BU5D_t2770800703* L_566 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_567 = V_1;
		NullCheck(L_566);
		int32_t L_568 = (((int32_t)((uint8_t)L_567)));
		uint32_t L_569 = (L_566)->GetAt(static_cast<il2cpp_array_size_t>(L_568));
		UInt32U5BU5D_t2770800703* L_570 = ___ekey2;
		NullCheck(L_570);
		int32_t L_571 = ((int32_t)30);
		uint32_t L_572 = (L_570)->GetAt(static_cast<il2cpp_array_size_t>(L_571));
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_557^(int32_t)L_561))^(int32_t)L_565))^(int32_t)L_569))^(int32_t)L_572));
		UInt32U5BU5D_t2770800703* L_573 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_574 = V_3;
		NullCheck(L_573);
		uintptr_t L_575 = (((uintptr_t)((int32_t)((uint32_t)L_574>>((int32_t)24)))));
		uint32_t L_576 = (L_573)->GetAt(static_cast<il2cpp_array_size_t>(L_575));
		UInt32U5BU5D_t2770800703* L_577 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_578 = V_0;
		NullCheck(L_577);
		int32_t L_579 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_578>>((int32_t)16))))));
		uint32_t L_580 = (L_577)->GetAt(static_cast<il2cpp_array_size_t>(L_579));
		UInt32U5BU5D_t2770800703* L_581 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_582 = V_1;
		NullCheck(L_581);
		int32_t L_583 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_582>>8)))));
		uint32_t L_584 = (L_581)->GetAt(static_cast<il2cpp_array_size_t>(L_583));
		UInt32U5BU5D_t2770800703* L_585 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_586 = V_2;
		NullCheck(L_585);
		int32_t L_587 = (((int32_t)((uint8_t)L_586)));
		uint32_t L_588 = (L_585)->GetAt(static_cast<il2cpp_array_size_t>(L_587));
		UInt32U5BU5D_t2770800703* L_589 = ___ekey2;
		NullCheck(L_589);
		int32_t L_590 = ((int32_t)31);
		uint32_t L_591 = (L_589)->GetAt(static_cast<il2cpp_array_size_t>(L_590));
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_576^(int32_t)L_580))^(int32_t)L_584))^(int32_t)L_588))^(int32_t)L_591));
		UInt32U5BU5D_t2770800703* L_592 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_593 = V_4;
		NullCheck(L_592);
		uintptr_t L_594 = (((uintptr_t)((int32_t)((uint32_t)L_593>>((int32_t)24)))));
		uint32_t L_595 = (L_592)->GetAt(static_cast<il2cpp_array_size_t>(L_594));
		UInt32U5BU5D_t2770800703* L_596 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_597 = V_5;
		NullCheck(L_596);
		int32_t L_598 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_597>>((int32_t)16))))));
		uint32_t L_599 = (L_596)->GetAt(static_cast<il2cpp_array_size_t>(L_598));
		UInt32U5BU5D_t2770800703* L_600 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_601 = V_6;
		NullCheck(L_600);
		int32_t L_602 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_601>>8)))));
		uint32_t L_603 = (L_600)->GetAt(static_cast<il2cpp_array_size_t>(L_602));
		UInt32U5BU5D_t2770800703* L_604 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_605 = V_7;
		NullCheck(L_604);
		int32_t L_606 = (((int32_t)((uint8_t)L_605)));
		uint32_t L_607 = (L_604)->GetAt(static_cast<il2cpp_array_size_t>(L_606));
		UInt32U5BU5D_t2770800703* L_608 = ___ekey2;
		NullCheck(L_608);
		int32_t L_609 = ((int32_t)32);
		uint32_t L_610 = (L_608)->GetAt(static_cast<il2cpp_array_size_t>(L_609));
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_595^(int32_t)L_599))^(int32_t)L_603))^(int32_t)L_607))^(int32_t)L_610));
		UInt32U5BU5D_t2770800703* L_611 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_612 = V_5;
		NullCheck(L_611);
		uintptr_t L_613 = (((uintptr_t)((int32_t)((uint32_t)L_612>>((int32_t)24)))));
		uint32_t L_614 = (L_611)->GetAt(static_cast<il2cpp_array_size_t>(L_613));
		UInt32U5BU5D_t2770800703* L_615 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_616 = V_6;
		NullCheck(L_615);
		int32_t L_617 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_616>>((int32_t)16))))));
		uint32_t L_618 = (L_615)->GetAt(static_cast<il2cpp_array_size_t>(L_617));
		UInt32U5BU5D_t2770800703* L_619 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_620 = V_7;
		NullCheck(L_619);
		int32_t L_621 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_620>>8)))));
		uint32_t L_622 = (L_619)->GetAt(static_cast<il2cpp_array_size_t>(L_621));
		UInt32U5BU5D_t2770800703* L_623 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_624 = V_4;
		NullCheck(L_623);
		int32_t L_625 = (((int32_t)((uint8_t)L_624)));
		uint32_t L_626 = (L_623)->GetAt(static_cast<il2cpp_array_size_t>(L_625));
		UInt32U5BU5D_t2770800703* L_627 = ___ekey2;
		NullCheck(L_627);
		int32_t L_628 = ((int32_t)33);
		uint32_t L_629 = (L_627)->GetAt(static_cast<il2cpp_array_size_t>(L_628));
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_614^(int32_t)L_618))^(int32_t)L_622))^(int32_t)L_626))^(int32_t)L_629));
		UInt32U5BU5D_t2770800703* L_630 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_631 = V_6;
		NullCheck(L_630);
		uintptr_t L_632 = (((uintptr_t)((int32_t)((uint32_t)L_631>>((int32_t)24)))));
		uint32_t L_633 = (L_630)->GetAt(static_cast<il2cpp_array_size_t>(L_632));
		UInt32U5BU5D_t2770800703* L_634 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_635 = V_7;
		NullCheck(L_634);
		int32_t L_636 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_635>>((int32_t)16))))));
		uint32_t L_637 = (L_634)->GetAt(static_cast<il2cpp_array_size_t>(L_636));
		UInt32U5BU5D_t2770800703* L_638 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_639 = V_4;
		NullCheck(L_638);
		int32_t L_640 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_639>>8)))));
		uint32_t L_641 = (L_638)->GetAt(static_cast<il2cpp_array_size_t>(L_640));
		UInt32U5BU5D_t2770800703* L_642 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_643 = V_5;
		NullCheck(L_642);
		int32_t L_644 = (((int32_t)((uint8_t)L_643)));
		uint32_t L_645 = (L_642)->GetAt(static_cast<il2cpp_array_size_t>(L_644));
		UInt32U5BU5D_t2770800703* L_646 = ___ekey2;
		NullCheck(L_646);
		int32_t L_647 = ((int32_t)34);
		uint32_t L_648 = (L_646)->GetAt(static_cast<il2cpp_array_size_t>(L_647));
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_633^(int32_t)L_637))^(int32_t)L_641))^(int32_t)L_645))^(int32_t)L_648));
		UInt32U5BU5D_t2770800703* L_649 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_650 = V_7;
		NullCheck(L_649);
		uintptr_t L_651 = (((uintptr_t)((int32_t)((uint32_t)L_650>>((int32_t)24)))));
		uint32_t L_652 = (L_649)->GetAt(static_cast<il2cpp_array_size_t>(L_651));
		UInt32U5BU5D_t2770800703* L_653 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_654 = V_4;
		NullCheck(L_653);
		int32_t L_655 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_654>>((int32_t)16))))));
		uint32_t L_656 = (L_653)->GetAt(static_cast<il2cpp_array_size_t>(L_655));
		UInt32U5BU5D_t2770800703* L_657 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_658 = V_5;
		NullCheck(L_657);
		int32_t L_659 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_658>>8)))));
		uint32_t L_660 = (L_657)->GetAt(static_cast<il2cpp_array_size_t>(L_659));
		UInt32U5BU5D_t2770800703* L_661 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_662 = V_6;
		NullCheck(L_661);
		int32_t L_663 = (((int32_t)((uint8_t)L_662)));
		uint32_t L_664 = (L_661)->GetAt(static_cast<il2cpp_array_size_t>(L_663));
		UInt32U5BU5D_t2770800703* L_665 = ___ekey2;
		NullCheck(L_665);
		int32_t L_666 = ((int32_t)35);
		uint32_t L_667 = (L_665)->GetAt(static_cast<il2cpp_array_size_t>(L_666));
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_652^(int32_t)L_656))^(int32_t)L_660))^(int32_t)L_664))^(int32_t)L_667));
		UInt32U5BU5D_t2770800703* L_668 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_669 = V_0;
		NullCheck(L_668);
		uintptr_t L_670 = (((uintptr_t)((int32_t)((uint32_t)L_669>>((int32_t)24)))));
		uint32_t L_671 = (L_668)->GetAt(static_cast<il2cpp_array_size_t>(L_670));
		UInt32U5BU5D_t2770800703* L_672 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_673 = V_1;
		NullCheck(L_672);
		int32_t L_674 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_673>>((int32_t)16))))));
		uint32_t L_675 = (L_672)->GetAt(static_cast<il2cpp_array_size_t>(L_674));
		UInt32U5BU5D_t2770800703* L_676 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_677 = V_2;
		NullCheck(L_676);
		int32_t L_678 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_677>>8)))));
		uint32_t L_679 = (L_676)->GetAt(static_cast<il2cpp_array_size_t>(L_678));
		UInt32U5BU5D_t2770800703* L_680 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_681 = V_3;
		NullCheck(L_680);
		int32_t L_682 = (((int32_t)((uint8_t)L_681)));
		uint32_t L_683 = (L_680)->GetAt(static_cast<il2cpp_array_size_t>(L_682));
		UInt32U5BU5D_t2770800703* L_684 = ___ekey2;
		NullCheck(L_684);
		int32_t L_685 = ((int32_t)36);
		uint32_t L_686 = (L_684)->GetAt(static_cast<il2cpp_array_size_t>(L_685));
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_671^(int32_t)L_675))^(int32_t)L_679))^(int32_t)L_683))^(int32_t)L_686));
		UInt32U5BU5D_t2770800703* L_687 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_688 = V_1;
		NullCheck(L_687);
		uintptr_t L_689 = (((uintptr_t)((int32_t)((uint32_t)L_688>>((int32_t)24)))));
		uint32_t L_690 = (L_687)->GetAt(static_cast<il2cpp_array_size_t>(L_689));
		UInt32U5BU5D_t2770800703* L_691 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_692 = V_2;
		NullCheck(L_691);
		int32_t L_693 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_692>>((int32_t)16))))));
		uint32_t L_694 = (L_691)->GetAt(static_cast<il2cpp_array_size_t>(L_693));
		UInt32U5BU5D_t2770800703* L_695 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_696 = V_3;
		NullCheck(L_695);
		int32_t L_697 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_696>>8)))));
		uint32_t L_698 = (L_695)->GetAt(static_cast<il2cpp_array_size_t>(L_697));
		UInt32U5BU5D_t2770800703* L_699 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_700 = V_0;
		NullCheck(L_699);
		int32_t L_701 = (((int32_t)((uint8_t)L_700)));
		uint32_t L_702 = (L_699)->GetAt(static_cast<il2cpp_array_size_t>(L_701));
		UInt32U5BU5D_t2770800703* L_703 = ___ekey2;
		NullCheck(L_703);
		int32_t L_704 = ((int32_t)37);
		uint32_t L_705 = (L_703)->GetAt(static_cast<il2cpp_array_size_t>(L_704));
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_690^(int32_t)L_694))^(int32_t)L_698))^(int32_t)L_702))^(int32_t)L_705));
		UInt32U5BU5D_t2770800703* L_706 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_707 = V_2;
		NullCheck(L_706);
		uintptr_t L_708 = (((uintptr_t)((int32_t)((uint32_t)L_707>>((int32_t)24)))));
		uint32_t L_709 = (L_706)->GetAt(static_cast<il2cpp_array_size_t>(L_708));
		UInt32U5BU5D_t2770800703* L_710 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_711 = V_3;
		NullCheck(L_710);
		int32_t L_712 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_711>>((int32_t)16))))));
		uint32_t L_713 = (L_710)->GetAt(static_cast<il2cpp_array_size_t>(L_712));
		UInt32U5BU5D_t2770800703* L_714 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_715 = V_0;
		NullCheck(L_714);
		int32_t L_716 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_715>>8)))));
		uint32_t L_717 = (L_714)->GetAt(static_cast<il2cpp_array_size_t>(L_716));
		UInt32U5BU5D_t2770800703* L_718 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_719 = V_1;
		NullCheck(L_718);
		int32_t L_720 = (((int32_t)((uint8_t)L_719)));
		uint32_t L_721 = (L_718)->GetAt(static_cast<il2cpp_array_size_t>(L_720));
		UInt32U5BU5D_t2770800703* L_722 = ___ekey2;
		NullCheck(L_722);
		int32_t L_723 = ((int32_t)38);
		uint32_t L_724 = (L_722)->GetAt(static_cast<il2cpp_array_size_t>(L_723));
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_709^(int32_t)L_713))^(int32_t)L_717))^(int32_t)L_721))^(int32_t)L_724));
		UInt32U5BU5D_t2770800703* L_725 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_726 = V_3;
		NullCheck(L_725);
		uintptr_t L_727 = (((uintptr_t)((int32_t)((uint32_t)L_726>>((int32_t)24)))));
		uint32_t L_728 = (L_725)->GetAt(static_cast<il2cpp_array_size_t>(L_727));
		UInt32U5BU5D_t2770800703* L_729 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_730 = V_0;
		NullCheck(L_729);
		int32_t L_731 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_730>>((int32_t)16))))));
		uint32_t L_732 = (L_729)->GetAt(static_cast<il2cpp_array_size_t>(L_731));
		UInt32U5BU5D_t2770800703* L_733 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_734 = V_1;
		NullCheck(L_733);
		int32_t L_735 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_734>>8)))));
		uint32_t L_736 = (L_733)->GetAt(static_cast<il2cpp_array_size_t>(L_735));
		UInt32U5BU5D_t2770800703* L_737 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_738 = V_2;
		NullCheck(L_737);
		int32_t L_739 = (((int32_t)((uint8_t)L_738)));
		uint32_t L_740 = (L_737)->GetAt(static_cast<il2cpp_array_size_t>(L_739));
		UInt32U5BU5D_t2770800703* L_741 = ___ekey2;
		NullCheck(L_741);
		int32_t L_742 = ((int32_t)39);
		uint32_t L_743 = (L_741)->GetAt(static_cast<il2cpp_array_size_t>(L_742));
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_728^(int32_t)L_732))^(int32_t)L_736))^(int32_t)L_740))^(int32_t)L_743));
		int32_t L_744 = __this->get_Nr_14();
		if ((((int32_t)L_744) <= ((int32_t)((int32_t)10))))
		{
			goto IL_0b08;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AesTransform_t2957123611_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t2770800703* L_745 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_746 = V_4;
		NullCheck(L_745);
		uintptr_t L_747 = (((uintptr_t)((int32_t)((uint32_t)L_746>>((int32_t)24)))));
		uint32_t L_748 = (L_745)->GetAt(static_cast<il2cpp_array_size_t>(L_747));
		UInt32U5BU5D_t2770800703* L_749 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_750 = V_5;
		NullCheck(L_749);
		int32_t L_751 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_750>>((int32_t)16))))));
		uint32_t L_752 = (L_749)->GetAt(static_cast<il2cpp_array_size_t>(L_751));
		UInt32U5BU5D_t2770800703* L_753 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_754 = V_6;
		NullCheck(L_753);
		int32_t L_755 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_754>>8)))));
		uint32_t L_756 = (L_753)->GetAt(static_cast<il2cpp_array_size_t>(L_755));
		UInt32U5BU5D_t2770800703* L_757 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_758 = V_7;
		NullCheck(L_757);
		int32_t L_759 = (((int32_t)((uint8_t)L_758)));
		uint32_t L_760 = (L_757)->GetAt(static_cast<il2cpp_array_size_t>(L_759));
		UInt32U5BU5D_t2770800703* L_761 = ___ekey2;
		NullCheck(L_761);
		int32_t L_762 = ((int32_t)40);
		uint32_t L_763 = (L_761)->GetAt(static_cast<il2cpp_array_size_t>(L_762));
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_748^(int32_t)L_752))^(int32_t)L_756))^(int32_t)L_760))^(int32_t)L_763));
		UInt32U5BU5D_t2770800703* L_764 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_765 = V_5;
		NullCheck(L_764);
		uintptr_t L_766 = (((uintptr_t)((int32_t)((uint32_t)L_765>>((int32_t)24)))));
		uint32_t L_767 = (L_764)->GetAt(static_cast<il2cpp_array_size_t>(L_766));
		UInt32U5BU5D_t2770800703* L_768 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_769 = V_6;
		NullCheck(L_768);
		int32_t L_770 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_769>>((int32_t)16))))));
		uint32_t L_771 = (L_768)->GetAt(static_cast<il2cpp_array_size_t>(L_770));
		UInt32U5BU5D_t2770800703* L_772 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_773 = V_7;
		NullCheck(L_772);
		int32_t L_774 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_773>>8)))));
		uint32_t L_775 = (L_772)->GetAt(static_cast<il2cpp_array_size_t>(L_774));
		UInt32U5BU5D_t2770800703* L_776 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_777 = V_4;
		NullCheck(L_776);
		int32_t L_778 = (((int32_t)((uint8_t)L_777)));
		uint32_t L_779 = (L_776)->GetAt(static_cast<il2cpp_array_size_t>(L_778));
		UInt32U5BU5D_t2770800703* L_780 = ___ekey2;
		NullCheck(L_780);
		int32_t L_781 = ((int32_t)41);
		uint32_t L_782 = (L_780)->GetAt(static_cast<il2cpp_array_size_t>(L_781));
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_767^(int32_t)L_771))^(int32_t)L_775))^(int32_t)L_779))^(int32_t)L_782));
		UInt32U5BU5D_t2770800703* L_783 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_784 = V_6;
		NullCheck(L_783);
		uintptr_t L_785 = (((uintptr_t)((int32_t)((uint32_t)L_784>>((int32_t)24)))));
		uint32_t L_786 = (L_783)->GetAt(static_cast<il2cpp_array_size_t>(L_785));
		UInt32U5BU5D_t2770800703* L_787 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_788 = V_7;
		NullCheck(L_787);
		int32_t L_789 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_788>>((int32_t)16))))));
		uint32_t L_790 = (L_787)->GetAt(static_cast<il2cpp_array_size_t>(L_789));
		UInt32U5BU5D_t2770800703* L_791 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_792 = V_4;
		NullCheck(L_791);
		int32_t L_793 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_792>>8)))));
		uint32_t L_794 = (L_791)->GetAt(static_cast<il2cpp_array_size_t>(L_793));
		UInt32U5BU5D_t2770800703* L_795 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_796 = V_5;
		NullCheck(L_795);
		int32_t L_797 = (((int32_t)((uint8_t)L_796)));
		uint32_t L_798 = (L_795)->GetAt(static_cast<il2cpp_array_size_t>(L_797));
		UInt32U5BU5D_t2770800703* L_799 = ___ekey2;
		NullCheck(L_799);
		int32_t L_800 = ((int32_t)42);
		uint32_t L_801 = (L_799)->GetAt(static_cast<il2cpp_array_size_t>(L_800));
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_786^(int32_t)L_790))^(int32_t)L_794))^(int32_t)L_798))^(int32_t)L_801));
		UInt32U5BU5D_t2770800703* L_802 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_803 = V_7;
		NullCheck(L_802);
		uintptr_t L_804 = (((uintptr_t)((int32_t)((uint32_t)L_803>>((int32_t)24)))));
		uint32_t L_805 = (L_802)->GetAt(static_cast<il2cpp_array_size_t>(L_804));
		UInt32U5BU5D_t2770800703* L_806 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_807 = V_4;
		NullCheck(L_806);
		int32_t L_808 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_807>>((int32_t)16))))));
		uint32_t L_809 = (L_806)->GetAt(static_cast<il2cpp_array_size_t>(L_808));
		UInt32U5BU5D_t2770800703* L_810 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_811 = V_5;
		NullCheck(L_810);
		int32_t L_812 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_811>>8)))));
		uint32_t L_813 = (L_810)->GetAt(static_cast<il2cpp_array_size_t>(L_812));
		UInt32U5BU5D_t2770800703* L_814 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_815 = V_6;
		NullCheck(L_814);
		int32_t L_816 = (((int32_t)((uint8_t)L_815)));
		uint32_t L_817 = (L_814)->GetAt(static_cast<il2cpp_array_size_t>(L_816));
		UInt32U5BU5D_t2770800703* L_818 = ___ekey2;
		NullCheck(L_818);
		int32_t L_819 = ((int32_t)43);
		uint32_t L_820 = (L_818)->GetAt(static_cast<il2cpp_array_size_t>(L_819));
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_805^(int32_t)L_809))^(int32_t)L_813))^(int32_t)L_817))^(int32_t)L_820));
		UInt32U5BU5D_t2770800703* L_821 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_822 = V_0;
		NullCheck(L_821);
		uintptr_t L_823 = (((uintptr_t)((int32_t)((uint32_t)L_822>>((int32_t)24)))));
		uint32_t L_824 = (L_821)->GetAt(static_cast<il2cpp_array_size_t>(L_823));
		UInt32U5BU5D_t2770800703* L_825 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_826 = V_1;
		NullCheck(L_825);
		int32_t L_827 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_826>>((int32_t)16))))));
		uint32_t L_828 = (L_825)->GetAt(static_cast<il2cpp_array_size_t>(L_827));
		UInt32U5BU5D_t2770800703* L_829 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_830 = V_2;
		NullCheck(L_829);
		int32_t L_831 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_830>>8)))));
		uint32_t L_832 = (L_829)->GetAt(static_cast<il2cpp_array_size_t>(L_831));
		UInt32U5BU5D_t2770800703* L_833 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_834 = V_3;
		NullCheck(L_833);
		int32_t L_835 = (((int32_t)((uint8_t)L_834)));
		uint32_t L_836 = (L_833)->GetAt(static_cast<il2cpp_array_size_t>(L_835));
		UInt32U5BU5D_t2770800703* L_837 = ___ekey2;
		NullCheck(L_837);
		int32_t L_838 = ((int32_t)44);
		uint32_t L_839 = (L_837)->GetAt(static_cast<il2cpp_array_size_t>(L_838));
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_824^(int32_t)L_828))^(int32_t)L_832))^(int32_t)L_836))^(int32_t)L_839));
		UInt32U5BU5D_t2770800703* L_840 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_841 = V_1;
		NullCheck(L_840);
		uintptr_t L_842 = (((uintptr_t)((int32_t)((uint32_t)L_841>>((int32_t)24)))));
		uint32_t L_843 = (L_840)->GetAt(static_cast<il2cpp_array_size_t>(L_842));
		UInt32U5BU5D_t2770800703* L_844 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_845 = V_2;
		NullCheck(L_844);
		int32_t L_846 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_845>>((int32_t)16))))));
		uint32_t L_847 = (L_844)->GetAt(static_cast<il2cpp_array_size_t>(L_846));
		UInt32U5BU5D_t2770800703* L_848 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_849 = V_3;
		NullCheck(L_848);
		int32_t L_850 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_849>>8)))));
		uint32_t L_851 = (L_848)->GetAt(static_cast<il2cpp_array_size_t>(L_850));
		UInt32U5BU5D_t2770800703* L_852 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_853 = V_0;
		NullCheck(L_852);
		int32_t L_854 = (((int32_t)((uint8_t)L_853)));
		uint32_t L_855 = (L_852)->GetAt(static_cast<il2cpp_array_size_t>(L_854));
		UInt32U5BU5D_t2770800703* L_856 = ___ekey2;
		NullCheck(L_856);
		int32_t L_857 = ((int32_t)45);
		uint32_t L_858 = (L_856)->GetAt(static_cast<il2cpp_array_size_t>(L_857));
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_843^(int32_t)L_847))^(int32_t)L_851))^(int32_t)L_855))^(int32_t)L_858));
		UInt32U5BU5D_t2770800703* L_859 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_860 = V_2;
		NullCheck(L_859);
		uintptr_t L_861 = (((uintptr_t)((int32_t)((uint32_t)L_860>>((int32_t)24)))));
		uint32_t L_862 = (L_859)->GetAt(static_cast<il2cpp_array_size_t>(L_861));
		UInt32U5BU5D_t2770800703* L_863 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_864 = V_3;
		NullCheck(L_863);
		int32_t L_865 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_864>>((int32_t)16))))));
		uint32_t L_866 = (L_863)->GetAt(static_cast<il2cpp_array_size_t>(L_865));
		UInt32U5BU5D_t2770800703* L_867 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_868 = V_0;
		NullCheck(L_867);
		int32_t L_869 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_868>>8)))));
		uint32_t L_870 = (L_867)->GetAt(static_cast<il2cpp_array_size_t>(L_869));
		UInt32U5BU5D_t2770800703* L_871 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_872 = V_1;
		NullCheck(L_871);
		int32_t L_873 = (((int32_t)((uint8_t)L_872)));
		uint32_t L_874 = (L_871)->GetAt(static_cast<il2cpp_array_size_t>(L_873));
		UInt32U5BU5D_t2770800703* L_875 = ___ekey2;
		NullCheck(L_875);
		int32_t L_876 = ((int32_t)46);
		uint32_t L_877 = (L_875)->GetAt(static_cast<il2cpp_array_size_t>(L_876));
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_862^(int32_t)L_866))^(int32_t)L_870))^(int32_t)L_874))^(int32_t)L_877));
		UInt32U5BU5D_t2770800703* L_878 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_879 = V_3;
		NullCheck(L_878);
		uintptr_t L_880 = (((uintptr_t)((int32_t)((uint32_t)L_879>>((int32_t)24)))));
		uint32_t L_881 = (L_878)->GetAt(static_cast<il2cpp_array_size_t>(L_880));
		UInt32U5BU5D_t2770800703* L_882 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_883 = V_0;
		NullCheck(L_882);
		int32_t L_884 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_883>>((int32_t)16))))));
		uint32_t L_885 = (L_882)->GetAt(static_cast<il2cpp_array_size_t>(L_884));
		UInt32U5BU5D_t2770800703* L_886 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_887 = V_1;
		NullCheck(L_886);
		int32_t L_888 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_887>>8)))));
		uint32_t L_889 = (L_886)->GetAt(static_cast<il2cpp_array_size_t>(L_888));
		UInt32U5BU5D_t2770800703* L_890 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_891 = V_2;
		NullCheck(L_890);
		int32_t L_892 = (((int32_t)((uint8_t)L_891)));
		uint32_t L_893 = (L_890)->GetAt(static_cast<il2cpp_array_size_t>(L_892));
		UInt32U5BU5D_t2770800703* L_894 = ___ekey2;
		NullCheck(L_894);
		int32_t L_895 = ((int32_t)47);
		uint32_t L_896 = (L_894)->GetAt(static_cast<il2cpp_array_size_t>(L_895));
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_881^(int32_t)L_885))^(int32_t)L_889))^(int32_t)L_893))^(int32_t)L_896));
		V_8 = ((int32_t)48);
		int32_t L_897 = __this->get_Nr_14();
		if ((((int32_t)L_897) <= ((int32_t)((int32_t)12))))
		{
			goto IL_0b08;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AesTransform_t2957123611_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t2770800703* L_898 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_899 = V_4;
		NullCheck(L_898);
		uintptr_t L_900 = (((uintptr_t)((int32_t)((uint32_t)L_899>>((int32_t)24)))));
		uint32_t L_901 = (L_898)->GetAt(static_cast<il2cpp_array_size_t>(L_900));
		UInt32U5BU5D_t2770800703* L_902 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_903 = V_5;
		NullCheck(L_902);
		int32_t L_904 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_903>>((int32_t)16))))));
		uint32_t L_905 = (L_902)->GetAt(static_cast<il2cpp_array_size_t>(L_904));
		UInt32U5BU5D_t2770800703* L_906 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_907 = V_6;
		NullCheck(L_906);
		int32_t L_908 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_907>>8)))));
		uint32_t L_909 = (L_906)->GetAt(static_cast<il2cpp_array_size_t>(L_908));
		UInt32U5BU5D_t2770800703* L_910 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_911 = V_7;
		NullCheck(L_910);
		int32_t L_912 = (((int32_t)((uint8_t)L_911)));
		uint32_t L_913 = (L_910)->GetAt(static_cast<il2cpp_array_size_t>(L_912));
		UInt32U5BU5D_t2770800703* L_914 = ___ekey2;
		NullCheck(L_914);
		int32_t L_915 = ((int32_t)48);
		uint32_t L_916 = (L_914)->GetAt(static_cast<il2cpp_array_size_t>(L_915));
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_901^(int32_t)L_905))^(int32_t)L_909))^(int32_t)L_913))^(int32_t)L_916));
		UInt32U5BU5D_t2770800703* L_917 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_918 = V_5;
		NullCheck(L_917);
		uintptr_t L_919 = (((uintptr_t)((int32_t)((uint32_t)L_918>>((int32_t)24)))));
		uint32_t L_920 = (L_917)->GetAt(static_cast<il2cpp_array_size_t>(L_919));
		UInt32U5BU5D_t2770800703* L_921 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_922 = V_6;
		NullCheck(L_921);
		int32_t L_923 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_922>>((int32_t)16))))));
		uint32_t L_924 = (L_921)->GetAt(static_cast<il2cpp_array_size_t>(L_923));
		UInt32U5BU5D_t2770800703* L_925 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_926 = V_7;
		NullCheck(L_925);
		int32_t L_927 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_926>>8)))));
		uint32_t L_928 = (L_925)->GetAt(static_cast<il2cpp_array_size_t>(L_927));
		UInt32U5BU5D_t2770800703* L_929 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_930 = V_4;
		NullCheck(L_929);
		int32_t L_931 = (((int32_t)((uint8_t)L_930)));
		uint32_t L_932 = (L_929)->GetAt(static_cast<il2cpp_array_size_t>(L_931));
		UInt32U5BU5D_t2770800703* L_933 = ___ekey2;
		NullCheck(L_933);
		int32_t L_934 = ((int32_t)49);
		uint32_t L_935 = (L_933)->GetAt(static_cast<il2cpp_array_size_t>(L_934));
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_920^(int32_t)L_924))^(int32_t)L_928))^(int32_t)L_932))^(int32_t)L_935));
		UInt32U5BU5D_t2770800703* L_936 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_937 = V_6;
		NullCheck(L_936);
		uintptr_t L_938 = (((uintptr_t)((int32_t)((uint32_t)L_937>>((int32_t)24)))));
		uint32_t L_939 = (L_936)->GetAt(static_cast<il2cpp_array_size_t>(L_938));
		UInt32U5BU5D_t2770800703* L_940 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_941 = V_7;
		NullCheck(L_940);
		int32_t L_942 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_941>>((int32_t)16))))));
		uint32_t L_943 = (L_940)->GetAt(static_cast<il2cpp_array_size_t>(L_942));
		UInt32U5BU5D_t2770800703* L_944 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_945 = V_4;
		NullCheck(L_944);
		int32_t L_946 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_945>>8)))));
		uint32_t L_947 = (L_944)->GetAt(static_cast<il2cpp_array_size_t>(L_946));
		UInt32U5BU5D_t2770800703* L_948 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_949 = V_5;
		NullCheck(L_948);
		int32_t L_950 = (((int32_t)((uint8_t)L_949)));
		uint32_t L_951 = (L_948)->GetAt(static_cast<il2cpp_array_size_t>(L_950));
		UInt32U5BU5D_t2770800703* L_952 = ___ekey2;
		NullCheck(L_952);
		int32_t L_953 = ((int32_t)50);
		uint32_t L_954 = (L_952)->GetAt(static_cast<il2cpp_array_size_t>(L_953));
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_939^(int32_t)L_943))^(int32_t)L_947))^(int32_t)L_951))^(int32_t)L_954));
		UInt32U5BU5D_t2770800703* L_955 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_956 = V_7;
		NullCheck(L_955);
		uintptr_t L_957 = (((uintptr_t)((int32_t)((uint32_t)L_956>>((int32_t)24)))));
		uint32_t L_958 = (L_955)->GetAt(static_cast<il2cpp_array_size_t>(L_957));
		UInt32U5BU5D_t2770800703* L_959 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_960 = V_4;
		NullCheck(L_959);
		int32_t L_961 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_960>>((int32_t)16))))));
		uint32_t L_962 = (L_959)->GetAt(static_cast<il2cpp_array_size_t>(L_961));
		UInt32U5BU5D_t2770800703* L_963 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_964 = V_5;
		NullCheck(L_963);
		int32_t L_965 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_964>>8)))));
		uint32_t L_966 = (L_963)->GetAt(static_cast<il2cpp_array_size_t>(L_965));
		UInt32U5BU5D_t2770800703* L_967 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_968 = V_6;
		NullCheck(L_967);
		int32_t L_969 = (((int32_t)((uint8_t)L_968)));
		uint32_t L_970 = (L_967)->GetAt(static_cast<il2cpp_array_size_t>(L_969));
		UInt32U5BU5D_t2770800703* L_971 = ___ekey2;
		NullCheck(L_971);
		int32_t L_972 = ((int32_t)51);
		uint32_t L_973 = (L_971)->GetAt(static_cast<il2cpp_array_size_t>(L_972));
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_958^(int32_t)L_962))^(int32_t)L_966))^(int32_t)L_970))^(int32_t)L_973));
		UInt32U5BU5D_t2770800703* L_974 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_975 = V_0;
		NullCheck(L_974);
		uintptr_t L_976 = (((uintptr_t)((int32_t)((uint32_t)L_975>>((int32_t)24)))));
		uint32_t L_977 = (L_974)->GetAt(static_cast<il2cpp_array_size_t>(L_976));
		UInt32U5BU5D_t2770800703* L_978 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_979 = V_1;
		NullCheck(L_978);
		int32_t L_980 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_979>>((int32_t)16))))));
		uint32_t L_981 = (L_978)->GetAt(static_cast<il2cpp_array_size_t>(L_980));
		UInt32U5BU5D_t2770800703* L_982 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_983 = V_2;
		NullCheck(L_982);
		int32_t L_984 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_983>>8)))));
		uint32_t L_985 = (L_982)->GetAt(static_cast<il2cpp_array_size_t>(L_984));
		UInt32U5BU5D_t2770800703* L_986 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_987 = V_3;
		NullCheck(L_986);
		int32_t L_988 = (((int32_t)((uint8_t)L_987)));
		uint32_t L_989 = (L_986)->GetAt(static_cast<il2cpp_array_size_t>(L_988));
		UInt32U5BU5D_t2770800703* L_990 = ___ekey2;
		NullCheck(L_990);
		int32_t L_991 = ((int32_t)52);
		uint32_t L_992 = (L_990)->GetAt(static_cast<il2cpp_array_size_t>(L_991));
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_977^(int32_t)L_981))^(int32_t)L_985))^(int32_t)L_989))^(int32_t)L_992));
		UInt32U5BU5D_t2770800703* L_993 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_994 = V_1;
		NullCheck(L_993);
		uintptr_t L_995 = (((uintptr_t)((int32_t)((uint32_t)L_994>>((int32_t)24)))));
		uint32_t L_996 = (L_993)->GetAt(static_cast<il2cpp_array_size_t>(L_995));
		UInt32U5BU5D_t2770800703* L_997 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_998 = V_2;
		NullCheck(L_997);
		int32_t L_999 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_998>>((int32_t)16))))));
		uint32_t L_1000 = (L_997)->GetAt(static_cast<il2cpp_array_size_t>(L_999));
		UInt32U5BU5D_t2770800703* L_1001 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_1002 = V_3;
		NullCheck(L_1001);
		int32_t L_1003 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_1002>>8)))));
		uint32_t L_1004 = (L_1001)->GetAt(static_cast<il2cpp_array_size_t>(L_1003));
		UInt32U5BU5D_t2770800703* L_1005 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_1006 = V_0;
		NullCheck(L_1005);
		int32_t L_1007 = (((int32_t)((uint8_t)L_1006)));
		uint32_t L_1008 = (L_1005)->GetAt(static_cast<il2cpp_array_size_t>(L_1007));
		UInt32U5BU5D_t2770800703* L_1009 = ___ekey2;
		NullCheck(L_1009);
		int32_t L_1010 = ((int32_t)53);
		uint32_t L_1011 = (L_1009)->GetAt(static_cast<il2cpp_array_size_t>(L_1010));
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_996^(int32_t)L_1000))^(int32_t)L_1004))^(int32_t)L_1008))^(int32_t)L_1011));
		UInt32U5BU5D_t2770800703* L_1012 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_1013 = V_2;
		NullCheck(L_1012);
		uintptr_t L_1014 = (((uintptr_t)((int32_t)((uint32_t)L_1013>>((int32_t)24)))));
		uint32_t L_1015 = (L_1012)->GetAt(static_cast<il2cpp_array_size_t>(L_1014));
		UInt32U5BU5D_t2770800703* L_1016 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_1017 = V_3;
		NullCheck(L_1016);
		int32_t L_1018 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_1017>>((int32_t)16))))));
		uint32_t L_1019 = (L_1016)->GetAt(static_cast<il2cpp_array_size_t>(L_1018));
		UInt32U5BU5D_t2770800703* L_1020 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_1021 = V_0;
		NullCheck(L_1020);
		int32_t L_1022 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_1021>>8)))));
		uint32_t L_1023 = (L_1020)->GetAt(static_cast<il2cpp_array_size_t>(L_1022));
		UInt32U5BU5D_t2770800703* L_1024 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_1025 = V_1;
		NullCheck(L_1024);
		int32_t L_1026 = (((int32_t)((uint8_t)L_1025)));
		uint32_t L_1027 = (L_1024)->GetAt(static_cast<il2cpp_array_size_t>(L_1026));
		UInt32U5BU5D_t2770800703* L_1028 = ___ekey2;
		NullCheck(L_1028);
		int32_t L_1029 = ((int32_t)54);
		uint32_t L_1030 = (L_1028)->GetAt(static_cast<il2cpp_array_size_t>(L_1029));
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1015^(int32_t)L_1019))^(int32_t)L_1023))^(int32_t)L_1027))^(int32_t)L_1030));
		UInt32U5BU5D_t2770800703* L_1031 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_1032 = V_3;
		NullCheck(L_1031);
		uintptr_t L_1033 = (((uintptr_t)((int32_t)((uint32_t)L_1032>>((int32_t)24)))));
		uint32_t L_1034 = (L_1031)->GetAt(static_cast<il2cpp_array_size_t>(L_1033));
		UInt32U5BU5D_t2770800703* L_1035 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_1036 = V_0;
		NullCheck(L_1035);
		int32_t L_1037 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_1036>>((int32_t)16))))));
		uint32_t L_1038 = (L_1035)->GetAt(static_cast<il2cpp_array_size_t>(L_1037));
		UInt32U5BU5D_t2770800703* L_1039 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_1040 = V_1;
		NullCheck(L_1039);
		int32_t L_1041 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_1040>>8)))));
		uint32_t L_1042 = (L_1039)->GetAt(static_cast<il2cpp_array_size_t>(L_1041));
		UInt32U5BU5D_t2770800703* L_1043 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_1044 = V_2;
		NullCheck(L_1043);
		int32_t L_1045 = (((int32_t)((uint8_t)L_1044)));
		uint32_t L_1046 = (L_1043)->GetAt(static_cast<il2cpp_array_size_t>(L_1045));
		UInt32U5BU5D_t2770800703* L_1047 = ___ekey2;
		NullCheck(L_1047);
		int32_t L_1048 = ((int32_t)55);
		uint32_t L_1049 = (L_1047)->GetAt(static_cast<il2cpp_array_size_t>(L_1048));
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1034^(int32_t)L_1038))^(int32_t)L_1042))^(int32_t)L_1046))^(int32_t)L_1049));
		V_8 = ((int32_t)56);
	}

IL_0b08:
	{
		ByteU5BU5D_t4116647657* L_1050 = ___outdata1;
		IL2CPP_RUNTIME_CLASS_INIT(AesTransform_t2957123611_il2cpp_TypeInfo_var);
		ByteU5BU5D_t4116647657* L_1051 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_1052 = V_4;
		NullCheck(L_1051);
		uintptr_t L_1053 = (((uintptr_t)((int32_t)((uint32_t)L_1052>>((int32_t)24)))));
		uint8_t L_1054 = (L_1051)->GetAt(static_cast<il2cpp_array_size_t>(L_1053));
		UInt32U5BU5D_t2770800703* L_1055 = ___ekey2;
		int32_t L_1056 = V_8;
		NullCheck(L_1055);
		int32_t L_1057 = L_1056;
		uint32_t L_1058 = (L_1055)->GetAt(static_cast<il2cpp_array_size_t>(L_1057));
		NullCheck(L_1050);
		(L_1050)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1054^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_1058>>((int32_t)24))))))))))));
		ByteU5BU5D_t4116647657* L_1059 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1060 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_1061 = V_5;
		NullCheck(L_1060);
		int32_t L_1062 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_1061>>((int32_t)16))))));
		uint8_t L_1063 = (L_1060)->GetAt(static_cast<il2cpp_array_size_t>(L_1062));
		UInt32U5BU5D_t2770800703* L_1064 = ___ekey2;
		int32_t L_1065 = V_8;
		NullCheck(L_1064);
		int32_t L_1066 = L_1065;
		uint32_t L_1067 = (L_1064)->GetAt(static_cast<il2cpp_array_size_t>(L_1066));
		NullCheck(L_1059);
		(L_1059)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1063^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_1067>>((int32_t)16))))))))))));
		ByteU5BU5D_t4116647657* L_1068 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1069 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_1070 = V_6;
		NullCheck(L_1069);
		int32_t L_1071 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_1070>>8)))));
		uint8_t L_1072 = (L_1069)->GetAt(static_cast<il2cpp_array_size_t>(L_1071));
		UInt32U5BU5D_t2770800703* L_1073 = ___ekey2;
		int32_t L_1074 = V_8;
		NullCheck(L_1073);
		int32_t L_1075 = L_1074;
		uint32_t L_1076 = (L_1073)->GetAt(static_cast<il2cpp_array_size_t>(L_1075));
		NullCheck(L_1068);
		(L_1068)->SetAt(static_cast<il2cpp_array_size_t>(2), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1072^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_1076>>8)))))))))));
		ByteU5BU5D_t4116647657* L_1077 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1078 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_1079 = V_7;
		NullCheck(L_1078);
		int32_t L_1080 = (((int32_t)((uint8_t)L_1079)));
		uint8_t L_1081 = (L_1078)->GetAt(static_cast<il2cpp_array_size_t>(L_1080));
		UInt32U5BU5D_t2770800703* L_1082 = ___ekey2;
		int32_t L_1083 = V_8;
		int32_t L_1084 = L_1083;
		V_8 = ((int32_t)((int32_t)L_1084+(int32_t)1));
		NullCheck(L_1082);
		int32_t L_1085 = L_1084;
		uint32_t L_1086 = (L_1082)->GetAt(static_cast<il2cpp_array_size_t>(L_1085));
		NullCheck(L_1077);
		(L_1077)->SetAt(static_cast<il2cpp_array_size_t>(3), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1081^(int32_t)(((int32_t)((uint8_t)L_1086)))))))));
		ByteU5BU5D_t4116647657* L_1087 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1088 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_1089 = V_5;
		NullCheck(L_1088);
		uintptr_t L_1090 = (((uintptr_t)((int32_t)((uint32_t)L_1089>>((int32_t)24)))));
		uint8_t L_1091 = (L_1088)->GetAt(static_cast<il2cpp_array_size_t>(L_1090));
		UInt32U5BU5D_t2770800703* L_1092 = ___ekey2;
		int32_t L_1093 = V_8;
		NullCheck(L_1092);
		int32_t L_1094 = L_1093;
		uint32_t L_1095 = (L_1092)->GetAt(static_cast<il2cpp_array_size_t>(L_1094));
		NullCheck(L_1087);
		(L_1087)->SetAt(static_cast<il2cpp_array_size_t>(4), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1091^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_1095>>((int32_t)24))))))))))));
		ByteU5BU5D_t4116647657* L_1096 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1097 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_1098 = V_6;
		NullCheck(L_1097);
		int32_t L_1099 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_1098>>((int32_t)16))))));
		uint8_t L_1100 = (L_1097)->GetAt(static_cast<il2cpp_array_size_t>(L_1099));
		UInt32U5BU5D_t2770800703* L_1101 = ___ekey2;
		int32_t L_1102 = V_8;
		NullCheck(L_1101);
		int32_t L_1103 = L_1102;
		uint32_t L_1104 = (L_1101)->GetAt(static_cast<il2cpp_array_size_t>(L_1103));
		NullCheck(L_1096);
		(L_1096)->SetAt(static_cast<il2cpp_array_size_t>(5), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1100^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_1104>>((int32_t)16))))))))))));
		ByteU5BU5D_t4116647657* L_1105 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1106 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_1107 = V_7;
		NullCheck(L_1106);
		int32_t L_1108 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_1107>>8)))));
		uint8_t L_1109 = (L_1106)->GetAt(static_cast<il2cpp_array_size_t>(L_1108));
		UInt32U5BU5D_t2770800703* L_1110 = ___ekey2;
		int32_t L_1111 = V_8;
		NullCheck(L_1110);
		int32_t L_1112 = L_1111;
		uint32_t L_1113 = (L_1110)->GetAt(static_cast<il2cpp_array_size_t>(L_1112));
		NullCheck(L_1105);
		(L_1105)->SetAt(static_cast<il2cpp_array_size_t>(6), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1109^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_1113>>8)))))))))));
		ByteU5BU5D_t4116647657* L_1114 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1115 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_1116 = V_4;
		NullCheck(L_1115);
		int32_t L_1117 = (((int32_t)((uint8_t)L_1116)));
		uint8_t L_1118 = (L_1115)->GetAt(static_cast<il2cpp_array_size_t>(L_1117));
		UInt32U5BU5D_t2770800703* L_1119 = ___ekey2;
		int32_t L_1120 = V_8;
		int32_t L_1121 = L_1120;
		V_8 = ((int32_t)((int32_t)L_1121+(int32_t)1));
		NullCheck(L_1119);
		int32_t L_1122 = L_1121;
		uint32_t L_1123 = (L_1119)->GetAt(static_cast<il2cpp_array_size_t>(L_1122));
		NullCheck(L_1114);
		(L_1114)->SetAt(static_cast<il2cpp_array_size_t>(7), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1118^(int32_t)(((int32_t)((uint8_t)L_1123)))))))));
		ByteU5BU5D_t4116647657* L_1124 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1125 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_1126 = V_6;
		NullCheck(L_1125);
		uintptr_t L_1127 = (((uintptr_t)((int32_t)((uint32_t)L_1126>>((int32_t)24)))));
		uint8_t L_1128 = (L_1125)->GetAt(static_cast<il2cpp_array_size_t>(L_1127));
		UInt32U5BU5D_t2770800703* L_1129 = ___ekey2;
		int32_t L_1130 = V_8;
		NullCheck(L_1129);
		int32_t L_1131 = L_1130;
		uint32_t L_1132 = (L_1129)->GetAt(static_cast<il2cpp_array_size_t>(L_1131));
		NullCheck(L_1124);
		(L_1124)->SetAt(static_cast<il2cpp_array_size_t>(8), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1128^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_1132>>((int32_t)24))))))))))));
		ByteU5BU5D_t4116647657* L_1133 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1134 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_1135 = V_7;
		NullCheck(L_1134);
		int32_t L_1136 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_1135>>((int32_t)16))))));
		uint8_t L_1137 = (L_1134)->GetAt(static_cast<il2cpp_array_size_t>(L_1136));
		UInt32U5BU5D_t2770800703* L_1138 = ___ekey2;
		int32_t L_1139 = V_8;
		NullCheck(L_1138);
		int32_t L_1140 = L_1139;
		uint32_t L_1141 = (L_1138)->GetAt(static_cast<il2cpp_array_size_t>(L_1140));
		NullCheck(L_1133);
		(L_1133)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1137^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_1141>>((int32_t)16))))))))))));
		ByteU5BU5D_t4116647657* L_1142 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1143 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_1144 = V_4;
		NullCheck(L_1143);
		int32_t L_1145 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_1144>>8)))));
		uint8_t L_1146 = (L_1143)->GetAt(static_cast<il2cpp_array_size_t>(L_1145));
		UInt32U5BU5D_t2770800703* L_1147 = ___ekey2;
		int32_t L_1148 = V_8;
		NullCheck(L_1147);
		int32_t L_1149 = L_1148;
		uint32_t L_1150 = (L_1147)->GetAt(static_cast<il2cpp_array_size_t>(L_1149));
		NullCheck(L_1142);
		(L_1142)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1146^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_1150>>8)))))))))));
		ByteU5BU5D_t4116647657* L_1151 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1152 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_1153 = V_5;
		NullCheck(L_1152);
		int32_t L_1154 = (((int32_t)((uint8_t)L_1153)));
		uint8_t L_1155 = (L_1152)->GetAt(static_cast<il2cpp_array_size_t>(L_1154));
		UInt32U5BU5D_t2770800703* L_1156 = ___ekey2;
		int32_t L_1157 = V_8;
		int32_t L_1158 = L_1157;
		V_8 = ((int32_t)((int32_t)L_1158+(int32_t)1));
		NullCheck(L_1156);
		int32_t L_1159 = L_1158;
		uint32_t L_1160 = (L_1156)->GetAt(static_cast<il2cpp_array_size_t>(L_1159));
		NullCheck(L_1151);
		(L_1151)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1155^(int32_t)(((int32_t)((uint8_t)L_1160)))))))));
		ByteU5BU5D_t4116647657* L_1161 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1162 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_1163 = V_7;
		NullCheck(L_1162);
		uintptr_t L_1164 = (((uintptr_t)((int32_t)((uint32_t)L_1163>>((int32_t)24)))));
		uint8_t L_1165 = (L_1162)->GetAt(static_cast<il2cpp_array_size_t>(L_1164));
		UInt32U5BU5D_t2770800703* L_1166 = ___ekey2;
		int32_t L_1167 = V_8;
		NullCheck(L_1166);
		int32_t L_1168 = L_1167;
		uint32_t L_1169 = (L_1166)->GetAt(static_cast<il2cpp_array_size_t>(L_1168));
		NullCheck(L_1161);
		(L_1161)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1165^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_1169>>((int32_t)24))))))))))));
		ByteU5BU5D_t4116647657* L_1170 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1171 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_1172 = V_4;
		NullCheck(L_1171);
		int32_t L_1173 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_1172>>((int32_t)16))))));
		uint8_t L_1174 = (L_1171)->GetAt(static_cast<il2cpp_array_size_t>(L_1173));
		UInt32U5BU5D_t2770800703* L_1175 = ___ekey2;
		int32_t L_1176 = V_8;
		NullCheck(L_1175);
		int32_t L_1177 = L_1176;
		uint32_t L_1178 = (L_1175)->GetAt(static_cast<il2cpp_array_size_t>(L_1177));
		NullCheck(L_1170);
		(L_1170)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1174^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_1178>>((int32_t)16))))))))))));
		ByteU5BU5D_t4116647657* L_1179 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1180 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_1181 = V_5;
		NullCheck(L_1180);
		int32_t L_1182 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_1181>>8)))));
		uint8_t L_1183 = (L_1180)->GetAt(static_cast<il2cpp_array_size_t>(L_1182));
		UInt32U5BU5D_t2770800703* L_1184 = ___ekey2;
		int32_t L_1185 = V_8;
		NullCheck(L_1184);
		int32_t L_1186 = L_1185;
		uint32_t L_1187 = (L_1184)->GetAt(static_cast<il2cpp_array_size_t>(L_1186));
		NullCheck(L_1179);
		(L_1179)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1183^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_1187>>8)))))))))));
		ByteU5BU5D_t4116647657* L_1188 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1189 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_1190 = V_6;
		NullCheck(L_1189);
		int32_t L_1191 = (((int32_t)((uint8_t)L_1190)));
		uint8_t L_1192 = (L_1189)->GetAt(static_cast<il2cpp_array_size_t>(L_1191));
		UInt32U5BU5D_t2770800703* L_1193 = ___ekey2;
		int32_t L_1194 = V_8;
		int32_t L_1195 = L_1194;
		V_8 = ((int32_t)((int32_t)L_1195+(int32_t)1));
		NullCheck(L_1193);
		int32_t L_1196 = L_1195;
		uint32_t L_1197 = (L_1193)->GetAt(static_cast<il2cpp_array_size_t>(L_1196));
		NullCheck(L_1188);
		(L_1188)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1192^(int32_t)(((int32_t)((uint8_t)L_1197)))))))));
		return;
	}
}
// System.Void System.Security.Cryptography.AesTransform::Decrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern "C"  void AesTransform_Decrypt128_m3018534522 (AesTransform_t2957123611 * __this, ByteU5BU5D_t4116647657* ___indata0, ByteU5BU5D_t4116647657* ___outdata1, UInt32U5BU5D_t2770800703* ___ekey2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AesTransform_Decrypt128_m3018534522_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	uint32_t V_1 = 0;
	uint32_t V_2 = 0;
	uint32_t V_3 = 0;
	uint32_t V_4 = 0;
	uint32_t V_5 = 0;
	uint32_t V_6 = 0;
	uint32_t V_7 = 0;
	int32_t V_8 = 0;
	{
		V_8 = ((int32_t)40);
		ByteU5BU5D_t4116647657* L_0 = ___indata0;
		NullCheck(L_0);
		int32_t L_1 = 0;
		uint8_t L_2 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1));
		ByteU5BU5D_t4116647657* L_3 = ___indata0;
		NullCheck(L_3);
		int32_t L_4 = 1;
		uint8_t L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		ByteU5BU5D_t4116647657* L_6 = ___indata0;
		NullCheck(L_6);
		int32_t L_7 = 2;
		uint8_t L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		ByteU5BU5D_t4116647657* L_9 = ___indata0;
		NullCheck(L_9);
		int32_t L_10 = 3;
		uint8_t L_11 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		UInt32U5BU5D_t2770800703* L_12 = ___ekey2;
		NullCheck(L_12);
		int32_t L_13 = 0;
		uint32_t L_14 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_2<<(int32_t)((int32_t)24)))|(int32_t)((int32_t)((int32_t)L_5<<(int32_t)((int32_t)16)))))|(int32_t)((int32_t)((int32_t)L_8<<(int32_t)8))))|(int32_t)L_11))^(int32_t)L_14));
		ByteU5BU5D_t4116647657* L_15 = ___indata0;
		NullCheck(L_15);
		int32_t L_16 = 4;
		uint8_t L_17 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		ByteU5BU5D_t4116647657* L_18 = ___indata0;
		NullCheck(L_18);
		int32_t L_19 = 5;
		uint8_t L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		ByteU5BU5D_t4116647657* L_21 = ___indata0;
		NullCheck(L_21);
		int32_t L_22 = 6;
		uint8_t L_23 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		ByteU5BU5D_t4116647657* L_24 = ___indata0;
		NullCheck(L_24);
		int32_t L_25 = 7;
		uint8_t L_26 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		UInt32U5BU5D_t2770800703* L_27 = ___ekey2;
		NullCheck(L_27);
		int32_t L_28 = 1;
		uint32_t L_29 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_17<<(int32_t)((int32_t)24)))|(int32_t)((int32_t)((int32_t)L_20<<(int32_t)((int32_t)16)))))|(int32_t)((int32_t)((int32_t)L_23<<(int32_t)8))))|(int32_t)L_26))^(int32_t)L_29));
		ByteU5BU5D_t4116647657* L_30 = ___indata0;
		NullCheck(L_30);
		int32_t L_31 = 8;
		uint8_t L_32 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		ByteU5BU5D_t4116647657* L_33 = ___indata0;
		NullCheck(L_33);
		int32_t L_34 = ((int32_t)9);
		uint8_t L_35 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
		ByteU5BU5D_t4116647657* L_36 = ___indata0;
		NullCheck(L_36);
		int32_t L_37 = ((int32_t)10);
		uint8_t L_38 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		ByteU5BU5D_t4116647657* L_39 = ___indata0;
		NullCheck(L_39);
		int32_t L_40 = ((int32_t)11);
		uint8_t L_41 = (L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
		UInt32U5BU5D_t2770800703* L_42 = ___ekey2;
		NullCheck(L_42);
		int32_t L_43 = 2;
		uint32_t L_44 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_32<<(int32_t)((int32_t)24)))|(int32_t)((int32_t)((int32_t)L_35<<(int32_t)((int32_t)16)))))|(int32_t)((int32_t)((int32_t)L_38<<(int32_t)8))))|(int32_t)L_41))^(int32_t)L_44));
		ByteU5BU5D_t4116647657* L_45 = ___indata0;
		NullCheck(L_45);
		int32_t L_46 = ((int32_t)12);
		uint8_t L_47 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_46));
		ByteU5BU5D_t4116647657* L_48 = ___indata0;
		NullCheck(L_48);
		int32_t L_49 = ((int32_t)13);
		uint8_t L_50 = (L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_49));
		ByteU5BU5D_t4116647657* L_51 = ___indata0;
		NullCheck(L_51);
		int32_t L_52 = ((int32_t)14);
		uint8_t L_53 = (L_51)->GetAt(static_cast<il2cpp_array_size_t>(L_52));
		ByteU5BU5D_t4116647657* L_54 = ___indata0;
		NullCheck(L_54);
		int32_t L_55 = ((int32_t)15);
		uint8_t L_56 = (L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_55));
		UInt32U5BU5D_t2770800703* L_57 = ___ekey2;
		NullCheck(L_57);
		int32_t L_58 = 3;
		uint32_t L_59 = (L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_58));
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_47<<(int32_t)((int32_t)24)))|(int32_t)((int32_t)((int32_t)L_50<<(int32_t)((int32_t)16)))))|(int32_t)((int32_t)((int32_t)L_53<<(int32_t)8))))|(int32_t)L_56))^(int32_t)L_59));
		IL2CPP_RUNTIME_CLASS_INIT(AesTransform_t2957123611_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t2770800703* L_60 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_61 = V_0;
		NullCheck(L_60);
		uintptr_t L_62 = (((uintptr_t)((int32_t)((uint32_t)L_61>>((int32_t)24)))));
		uint32_t L_63 = (L_60)->GetAt(static_cast<il2cpp_array_size_t>(L_62));
		UInt32U5BU5D_t2770800703* L_64 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_65 = V_3;
		NullCheck(L_64);
		int32_t L_66 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_65>>((int32_t)16))))));
		uint32_t L_67 = (L_64)->GetAt(static_cast<il2cpp_array_size_t>(L_66));
		UInt32U5BU5D_t2770800703* L_68 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_69 = V_2;
		NullCheck(L_68);
		int32_t L_70 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_69>>8)))));
		uint32_t L_71 = (L_68)->GetAt(static_cast<il2cpp_array_size_t>(L_70));
		UInt32U5BU5D_t2770800703* L_72 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_73 = V_1;
		NullCheck(L_72);
		int32_t L_74 = (((int32_t)((uint8_t)L_73)));
		uint32_t L_75 = (L_72)->GetAt(static_cast<il2cpp_array_size_t>(L_74));
		UInt32U5BU5D_t2770800703* L_76 = ___ekey2;
		NullCheck(L_76);
		int32_t L_77 = 4;
		uint32_t L_78 = (L_76)->GetAt(static_cast<il2cpp_array_size_t>(L_77));
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_63^(int32_t)L_67))^(int32_t)L_71))^(int32_t)L_75))^(int32_t)L_78));
		UInt32U5BU5D_t2770800703* L_79 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_80 = V_1;
		NullCheck(L_79);
		uintptr_t L_81 = (((uintptr_t)((int32_t)((uint32_t)L_80>>((int32_t)24)))));
		uint32_t L_82 = (L_79)->GetAt(static_cast<il2cpp_array_size_t>(L_81));
		UInt32U5BU5D_t2770800703* L_83 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_84 = V_0;
		NullCheck(L_83);
		int32_t L_85 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_84>>((int32_t)16))))));
		uint32_t L_86 = (L_83)->GetAt(static_cast<il2cpp_array_size_t>(L_85));
		UInt32U5BU5D_t2770800703* L_87 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_88 = V_3;
		NullCheck(L_87);
		int32_t L_89 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_88>>8)))));
		uint32_t L_90 = (L_87)->GetAt(static_cast<il2cpp_array_size_t>(L_89));
		UInt32U5BU5D_t2770800703* L_91 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_92 = V_2;
		NullCheck(L_91);
		int32_t L_93 = (((int32_t)((uint8_t)L_92)));
		uint32_t L_94 = (L_91)->GetAt(static_cast<il2cpp_array_size_t>(L_93));
		UInt32U5BU5D_t2770800703* L_95 = ___ekey2;
		NullCheck(L_95);
		int32_t L_96 = 5;
		uint32_t L_97 = (L_95)->GetAt(static_cast<il2cpp_array_size_t>(L_96));
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_82^(int32_t)L_86))^(int32_t)L_90))^(int32_t)L_94))^(int32_t)L_97));
		UInt32U5BU5D_t2770800703* L_98 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_99 = V_2;
		NullCheck(L_98);
		uintptr_t L_100 = (((uintptr_t)((int32_t)((uint32_t)L_99>>((int32_t)24)))));
		uint32_t L_101 = (L_98)->GetAt(static_cast<il2cpp_array_size_t>(L_100));
		UInt32U5BU5D_t2770800703* L_102 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_103 = V_1;
		NullCheck(L_102);
		int32_t L_104 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_103>>((int32_t)16))))));
		uint32_t L_105 = (L_102)->GetAt(static_cast<il2cpp_array_size_t>(L_104));
		UInt32U5BU5D_t2770800703* L_106 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_107 = V_0;
		NullCheck(L_106);
		int32_t L_108 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_107>>8)))));
		uint32_t L_109 = (L_106)->GetAt(static_cast<il2cpp_array_size_t>(L_108));
		UInt32U5BU5D_t2770800703* L_110 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_111 = V_3;
		NullCheck(L_110);
		int32_t L_112 = (((int32_t)((uint8_t)L_111)));
		uint32_t L_113 = (L_110)->GetAt(static_cast<il2cpp_array_size_t>(L_112));
		UInt32U5BU5D_t2770800703* L_114 = ___ekey2;
		NullCheck(L_114);
		int32_t L_115 = 6;
		uint32_t L_116 = (L_114)->GetAt(static_cast<il2cpp_array_size_t>(L_115));
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_101^(int32_t)L_105))^(int32_t)L_109))^(int32_t)L_113))^(int32_t)L_116));
		UInt32U5BU5D_t2770800703* L_117 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_118 = V_3;
		NullCheck(L_117);
		uintptr_t L_119 = (((uintptr_t)((int32_t)((uint32_t)L_118>>((int32_t)24)))));
		uint32_t L_120 = (L_117)->GetAt(static_cast<il2cpp_array_size_t>(L_119));
		UInt32U5BU5D_t2770800703* L_121 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_122 = V_2;
		NullCheck(L_121);
		int32_t L_123 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_122>>((int32_t)16))))));
		uint32_t L_124 = (L_121)->GetAt(static_cast<il2cpp_array_size_t>(L_123));
		UInt32U5BU5D_t2770800703* L_125 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_126 = V_1;
		NullCheck(L_125);
		int32_t L_127 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_126>>8)))));
		uint32_t L_128 = (L_125)->GetAt(static_cast<il2cpp_array_size_t>(L_127));
		UInt32U5BU5D_t2770800703* L_129 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_130 = V_0;
		NullCheck(L_129);
		int32_t L_131 = (((int32_t)((uint8_t)L_130)));
		uint32_t L_132 = (L_129)->GetAt(static_cast<il2cpp_array_size_t>(L_131));
		UInt32U5BU5D_t2770800703* L_133 = ___ekey2;
		NullCheck(L_133);
		int32_t L_134 = 7;
		uint32_t L_135 = (L_133)->GetAt(static_cast<il2cpp_array_size_t>(L_134));
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_120^(int32_t)L_124))^(int32_t)L_128))^(int32_t)L_132))^(int32_t)L_135));
		UInt32U5BU5D_t2770800703* L_136 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_137 = V_4;
		NullCheck(L_136);
		uintptr_t L_138 = (((uintptr_t)((int32_t)((uint32_t)L_137>>((int32_t)24)))));
		uint32_t L_139 = (L_136)->GetAt(static_cast<il2cpp_array_size_t>(L_138));
		UInt32U5BU5D_t2770800703* L_140 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_141 = V_7;
		NullCheck(L_140);
		int32_t L_142 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_141>>((int32_t)16))))));
		uint32_t L_143 = (L_140)->GetAt(static_cast<il2cpp_array_size_t>(L_142));
		UInt32U5BU5D_t2770800703* L_144 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_145 = V_6;
		NullCheck(L_144);
		int32_t L_146 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_145>>8)))));
		uint32_t L_147 = (L_144)->GetAt(static_cast<il2cpp_array_size_t>(L_146));
		UInt32U5BU5D_t2770800703* L_148 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_149 = V_5;
		NullCheck(L_148);
		int32_t L_150 = (((int32_t)((uint8_t)L_149)));
		uint32_t L_151 = (L_148)->GetAt(static_cast<il2cpp_array_size_t>(L_150));
		UInt32U5BU5D_t2770800703* L_152 = ___ekey2;
		NullCheck(L_152);
		int32_t L_153 = 8;
		uint32_t L_154 = (L_152)->GetAt(static_cast<il2cpp_array_size_t>(L_153));
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_139^(int32_t)L_143))^(int32_t)L_147))^(int32_t)L_151))^(int32_t)L_154));
		UInt32U5BU5D_t2770800703* L_155 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_156 = V_5;
		NullCheck(L_155);
		uintptr_t L_157 = (((uintptr_t)((int32_t)((uint32_t)L_156>>((int32_t)24)))));
		uint32_t L_158 = (L_155)->GetAt(static_cast<il2cpp_array_size_t>(L_157));
		UInt32U5BU5D_t2770800703* L_159 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_160 = V_4;
		NullCheck(L_159);
		int32_t L_161 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_160>>((int32_t)16))))));
		uint32_t L_162 = (L_159)->GetAt(static_cast<il2cpp_array_size_t>(L_161));
		UInt32U5BU5D_t2770800703* L_163 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_164 = V_7;
		NullCheck(L_163);
		int32_t L_165 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_164>>8)))));
		uint32_t L_166 = (L_163)->GetAt(static_cast<il2cpp_array_size_t>(L_165));
		UInt32U5BU5D_t2770800703* L_167 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_168 = V_6;
		NullCheck(L_167);
		int32_t L_169 = (((int32_t)((uint8_t)L_168)));
		uint32_t L_170 = (L_167)->GetAt(static_cast<il2cpp_array_size_t>(L_169));
		UInt32U5BU5D_t2770800703* L_171 = ___ekey2;
		NullCheck(L_171);
		int32_t L_172 = ((int32_t)9);
		uint32_t L_173 = (L_171)->GetAt(static_cast<il2cpp_array_size_t>(L_172));
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_158^(int32_t)L_162))^(int32_t)L_166))^(int32_t)L_170))^(int32_t)L_173));
		UInt32U5BU5D_t2770800703* L_174 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_175 = V_6;
		NullCheck(L_174);
		uintptr_t L_176 = (((uintptr_t)((int32_t)((uint32_t)L_175>>((int32_t)24)))));
		uint32_t L_177 = (L_174)->GetAt(static_cast<il2cpp_array_size_t>(L_176));
		UInt32U5BU5D_t2770800703* L_178 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_179 = V_5;
		NullCheck(L_178);
		int32_t L_180 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_179>>((int32_t)16))))));
		uint32_t L_181 = (L_178)->GetAt(static_cast<il2cpp_array_size_t>(L_180));
		UInt32U5BU5D_t2770800703* L_182 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_183 = V_4;
		NullCheck(L_182);
		int32_t L_184 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_183>>8)))));
		uint32_t L_185 = (L_182)->GetAt(static_cast<il2cpp_array_size_t>(L_184));
		UInt32U5BU5D_t2770800703* L_186 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_187 = V_7;
		NullCheck(L_186);
		int32_t L_188 = (((int32_t)((uint8_t)L_187)));
		uint32_t L_189 = (L_186)->GetAt(static_cast<il2cpp_array_size_t>(L_188));
		UInt32U5BU5D_t2770800703* L_190 = ___ekey2;
		NullCheck(L_190);
		int32_t L_191 = ((int32_t)10);
		uint32_t L_192 = (L_190)->GetAt(static_cast<il2cpp_array_size_t>(L_191));
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_177^(int32_t)L_181))^(int32_t)L_185))^(int32_t)L_189))^(int32_t)L_192));
		UInt32U5BU5D_t2770800703* L_193 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_194 = V_7;
		NullCheck(L_193);
		uintptr_t L_195 = (((uintptr_t)((int32_t)((uint32_t)L_194>>((int32_t)24)))));
		uint32_t L_196 = (L_193)->GetAt(static_cast<il2cpp_array_size_t>(L_195));
		UInt32U5BU5D_t2770800703* L_197 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_198 = V_6;
		NullCheck(L_197);
		int32_t L_199 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_198>>((int32_t)16))))));
		uint32_t L_200 = (L_197)->GetAt(static_cast<il2cpp_array_size_t>(L_199));
		UInt32U5BU5D_t2770800703* L_201 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_202 = V_5;
		NullCheck(L_201);
		int32_t L_203 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_202>>8)))));
		uint32_t L_204 = (L_201)->GetAt(static_cast<il2cpp_array_size_t>(L_203));
		UInt32U5BU5D_t2770800703* L_205 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_206 = V_4;
		NullCheck(L_205);
		int32_t L_207 = (((int32_t)((uint8_t)L_206)));
		uint32_t L_208 = (L_205)->GetAt(static_cast<il2cpp_array_size_t>(L_207));
		UInt32U5BU5D_t2770800703* L_209 = ___ekey2;
		NullCheck(L_209);
		int32_t L_210 = ((int32_t)11);
		uint32_t L_211 = (L_209)->GetAt(static_cast<il2cpp_array_size_t>(L_210));
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_196^(int32_t)L_200))^(int32_t)L_204))^(int32_t)L_208))^(int32_t)L_211));
		UInt32U5BU5D_t2770800703* L_212 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_213 = V_0;
		NullCheck(L_212);
		uintptr_t L_214 = (((uintptr_t)((int32_t)((uint32_t)L_213>>((int32_t)24)))));
		uint32_t L_215 = (L_212)->GetAt(static_cast<il2cpp_array_size_t>(L_214));
		UInt32U5BU5D_t2770800703* L_216 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_217 = V_3;
		NullCheck(L_216);
		int32_t L_218 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_217>>((int32_t)16))))));
		uint32_t L_219 = (L_216)->GetAt(static_cast<il2cpp_array_size_t>(L_218));
		UInt32U5BU5D_t2770800703* L_220 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_221 = V_2;
		NullCheck(L_220);
		int32_t L_222 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_221>>8)))));
		uint32_t L_223 = (L_220)->GetAt(static_cast<il2cpp_array_size_t>(L_222));
		UInt32U5BU5D_t2770800703* L_224 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_225 = V_1;
		NullCheck(L_224);
		int32_t L_226 = (((int32_t)((uint8_t)L_225)));
		uint32_t L_227 = (L_224)->GetAt(static_cast<il2cpp_array_size_t>(L_226));
		UInt32U5BU5D_t2770800703* L_228 = ___ekey2;
		NullCheck(L_228);
		int32_t L_229 = ((int32_t)12);
		uint32_t L_230 = (L_228)->GetAt(static_cast<il2cpp_array_size_t>(L_229));
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_215^(int32_t)L_219))^(int32_t)L_223))^(int32_t)L_227))^(int32_t)L_230));
		UInt32U5BU5D_t2770800703* L_231 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_232 = V_1;
		NullCheck(L_231);
		uintptr_t L_233 = (((uintptr_t)((int32_t)((uint32_t)L_232>>((int32_t)24)))));
		uint32_t L_234 = (L_231)->GetAt(static_cast<il2cpp_array_size_t>(L_233));
		UInt32U5BU5D_t2770800703* L_235 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_236 = V_0;
		NullCheck(L_235);
		int32_t L_237 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_236>>((int32_t)16))))));
		uint32_t L_238 = (L_235)->GetAt(static_cast<il2cpp_array_size_t>(L_237));
		UInt32U5BU5D_t2770800703* L_239 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_240 = V_3;
		NullCheck(L_239);
		int32_t L_241 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_240>>8)))));
		uint32_t L_242 = (L_239)->GetAt(static_cast<il2cpp_array_size_t>(L_241));
		UInt32U5BU5D_t2770800703* L_243 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_244 = V_2;
		NullCheck(L_243);
		int32_t L_245 = (((int32_t)((uint8_t)L_244)));
		uint32_t L_246 = (L_243)->GetAt(static_cast<il2cpp_array_size_t>(L_245));
		UInt32U5BU5D_t2770800703* L_247 = ___ekey2;
		NullCheck(L_247);
		int32_t L_248 = ((int32_t)13);
		uint32_t L_249 = (L_247)->GetAt(static_cast<il2cpp_array_size_t>(L_248));
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_234^(int32_t)L_238))^(int32_t)L_242))^(int32_t)L_246))^(int32_t)L_249));
		UInt32U5BU5D_t2770800703* L_250 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_251 = V_2;
		NullCheck(L_250);
		uintptr_t L_252 = (((uintptr_t)((int32_t)((uint32_t)L_251>>((int32_t)24)))));
		uint32_t L_253 = (L_250)->GetAt(static_cast<il2cpp_array_size_t>(L_252));
		UInt32U5BU5D_t2770800703* L_254 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_255 = V_1;
		NullCheck(L_254);
		int32_t L_256 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_255>>((int32_t)16))))));
		uint32_t L_257 = (L_254)->GetAt(static_cast<il2cpp_array_size_t>(L_256));
		UInt32U5BU5D_t2770800703* L_258 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_259 = V_0;
		NullCheck(L_258);
		int32_t L_260 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_259>>8)))));
		uint32_t L_261 = (L_258)->GetAt(static_cast<il2cpp_array_size_t>(L_260));
		UInt32U5BU5D_t2770800703* L_262 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_263 = V_3;
		NullCheck(L_262);
		int32_t L_264 = (((int32_t)((uint8_t)L_263)));
		uint32_t L_265 = (L_262)->GetAt(static_cast<il2cpp_array_size_t>(L_264));
		UInt32U5BU5D_t2770800703* L_266 = ___ekey2;
		NullCheck(L_266);
		int32_t L_267 = ((int32_t)14);
		uint32_t L_268 = (L_266)->GetAt(static_cast<il2cpp_array_size_t>(L_267));
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_253^(int32_t)L_257))^(int32_t)L_261))^(int32_t)L_265))^(int32_t)L_268));
		UInt32U5BU5D_t2770800703* L_269 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_270 = V_3;
		NullCheck(L_269);
		uintptr_t L_271 = (((uintptr_t)((int32_t)((uint32_t)L_270>>((int32_t)24)))));
		uint32_t L_272 = (L_269)->GetAt(static_cast<il2cpp_array_size_t>(L_271));
		UInt32U5BU5D_t2770800703* L_273 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_274 = V_2;
		NullCheck(L_273);
		int32_t L_275 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_274>>((int32_t)16))))));
		uint32_t L_276 = (L_273)->GetAt(static_cast<il2cpp_array_size_t>(L_275));
		UInt32U5BU5D_t2770800703* L_277 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_278 = V_1;
		NullCheck(L_277);
		int32_t L_279 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_278>>8)))));
		uint32_t L_280 = (L_277)->GetAt(static_cast<il2cpp_array_size_t>(L_279));
		UInt32U5BU5D_t2770800703* L_281 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_282 = V_0;
		NullCheck(L_281);
		int32_t L_283 = (((int32_t)((uint8_t)L_282)));
		uint32_t L_284 = (L_281)->GetAt(static_cast<il2cpp_array_size_t>(L_283));
		UInt32U5BU5D_t2770800703* L_285 = ___ekey2;
		NullCheck(L_285);
		int32_t L_286 = ((int32_t)15);
		uint32_t L_287 = (L_285)->GetAt(static_cast<il2cpp_array_size_t>(L_286));
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_272^(int32_t)L_276))^(int32_t)L_280))^(int32_t)L_284))^(int32_t)L_287));
		UInt32U5BU5D_t2770800703* L_288 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_289 = V_4;
		NullCheck(L_288);
		uintptr_t L_290 = (((uintptr_t)((int32_t)((uint32_t)L_289>>((int32_t)24)))));
		uint32_t L_291 = (L_288)->GetAt(static_cast<il2cpp_array_size_t>(L_290));
		UInt32U5BU5D_t2770800703* L_292 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_293 = V_7;
		NullCheck(L_292);
		int32_t L_294 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_293>>((int32_t)16))))));
		uint32_t L_295 = (L_292)->GetAt(static_cast<il2cpp_array_size_t>(L_294));
		UInt32U5BU5D_t2770800703* L_296 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_297 = V_6;
		NullCheck(L_296);
		int32_t L_298 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_297>>8)))));
		uint32_t L_299 = (L_296)->GetAt(static_cast<il2cpp_array_size_t>(L_298));
		UInt32U5BU5D_t2770800703* L_300 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_301 = V_5;
		NullCheck(L_300);
		int32_t L_302 = (((int32_t)((uint8_t)L_301)));
		uint32_t L_303 = (L_300)->GetAt(static_cast<il2cpp_array_size_t>(L_302));
		UInt32U5BU5D_t2770800703* L_304 = ___ekey2;
		NullCheck(L_304);
		int32_t L_305 = ((int32_t)16);
		uint32_t L_306 = (L_304)->GetAt(static_cast<il2cpp_array_size_t>(L_305));
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_291^(int32_t)L_295))^(int32_t)L_299))^(int32_t)L_303))^(int32_t)L_306));
		UInt32U5BU5D_t2770800703* L_307 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_308 = V_5;
		NullCheck(L_307);
		uintptr_t L_309 = (((uintptr_t)((int32_t)((uint32_t)L_308>>((int32_t)24)))));
		uint32_t L_310 = (L_307)->GetAt(static_cast<il2cpp_array_size_t>(L_309));
		UInt32U5BU5D_t2770800703* L_311 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_312 = V_4;
		NullCheck(L_311);
		int32_t L_313 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_312>>((int32_t)16))))));
		uint32_t L_314 = (L_311)->GetAt(static_cast<il2cpp_array_size_t>(L_313));
		UInt32U5BU5D_t2770800703* L_315 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_316 = V_7;
		NullCheck(L_315);
		int32_t L_317 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_316>>8)))));
		uint32_t L_318 = (L_315)->GetAt(static_cast<il2cpp_array_size_t>(L_317));
		UInt32U5BU5D_t2770800703* L_319 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_320 = V_6;
		NullCheck(L_319);
		int32_t L_321 = (((int32_t)((uint8_t)L_320)));
		uint32_t L_322 = (L_319)->GetAt(static_cast<il2cpp_array_size_t>(L_321));
		UInt32U5BU5D_t2770800703* L_323 = ___ekey2;
		NullCheck(L_323);
		int32_t L_324 = ((int32_t)17);
		uint32_t L_325 = (L_323)->GetAt(static_cast<il2cpp_array_size_t>(L_324));
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_310^(int32_t)L_314))^(int32_t)L_318))^(int32_t)L_322))^(int32_t)L_325));
		UInt32U5BU5D_t2770800703* L_326 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_327 = V_6;
		NullCheck(L_326);
		uintptr_t L_328 = (((uintptr_t)((int32_t)((uint32_t)L_327>>((int32_t)24)))));
		uint32_t L_329 = (L_326)->GetAt(static_cast<il2cpp_array_size_t>(L_328));
		UInt32U5BU5D_t2770800703* L_330 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_331 = V_5;
		NullCheck(L_330);
		int32_t L_332 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_331>>((int32_t)16))))));
		uint32_t L_333 = (L_330)->GetAt(static_cast<il2cpp_array_size_t>(L_332));
		UInt32U5BU5D_t2770800703* L_334 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_335 = V_4;
		NullCheck(L_334);
		int32_t L_336 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_335>>8)))));
		uint32_t L_337 = (L_334)->GetAt(static_cast<il2cpp_array_size_t>(L_336));
		UInt32U5BU5D_t2770800703* L_338 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_339 = V_7;
		NullCheck(L_338);
		int32_t L_340 = (((int32_t)((uint8_t)L_339)));
		uint32_t L_341 = (L_338)->GetAt(static_cast<il2cpp_array_size_t>(L_340));
		UInt32U5BU5D_t2770800703* L_342 = ___ekey2;
		NullCheck(L_342);
		int32_t L_343 = ((int32_t)18);
		uint32_t L_344 = (L_342)->GetAt(static_cast<il2cpp_array_size_t>(L_343));
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_329^(int32_t)L_333))^(int32_t)L_337))^(int32_t)L_341))^(int32_t)L_344));
		UInt32U5BU5D_t2770800703* L_345 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_346 = V_7;
		NullCheck(L_345);
		uintptr_t L_347 = (((uintptr_t)((int32_t)((uint32_t)L_346>>((int32_t)24)))));
		uint32_t L_348 = (L_345)->GetAt(static_cast<il2cpp_array_size_t>(L_347));
		UInt32U5BU5D_t2770800703* L_349 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_350 = V_6;
		NullCheck(L_349);
		int32_t L_351 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_350>>((int32_t)16))))));
		uint32_t L_352 = (L_349)->GetAt(static_cast<il2cpp_array_size_t>(L_351));
		UInt32U5BU5D_t2770800703* L_353 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_354 = V_5;
		NullCheck(L_353);
		int32_t L_355 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_354>>8)))));
		uint32_t L_356 = (L_353)->GetAt(static_cast<il2cpp_array_size_t>(L_355));
		UInt32U5BU5D_t2770800703* L_357 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_358 = V_4;
		NullCheck(L_357);
		int32_t L_359 = (((int32_t)((uint8_t)L_358)));
		uint32_t L_360 = (L_357)->GetAt(static_cast<il2cpp_array_size_t>(L_359));
		UInt32U5BU5D_t2770800703* L_361 = ___ekey2;
		NullCheck(L_361);
		int32_t L_362 = ((int32_t)19);
		uint32_t L_363 = (L_361)->GetAt(static_cast<il2cpp_array_size_t>(L_362));
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_348^(int32_t)L_352))^(int32_t)L_356))^(int32_t)L_360))^(int32_t)L_363));
		UInt32U5BU5D_t2770800703* L_364 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_365 = V_0;
		NullCheck(L_364);
		uintptr_t L_366 = (((uintptr_t)((int32_t)((uint32_t)L_365>>((int32_t)24)))));
		uint32_t L_367 = (L_364)->GetAt(static_cast<il2cpp_array_size_t>(L_366));
		UInt32U5BU5D_t2770800703* L_368 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_369 = V_3;
		NullCheck(L_368);
		int32_t L_370 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_369>>((int32_t)16))))));
		uint32_t L_371 = (L_368)->GetAt(static_cast<il2cpp_array_size_t>(L_370));
		UInt32U5BU5D_t2770800703* L_372 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_373 = V_2;
		NullCheck(L_372);
		int32_t L_374 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_373>>8)))));
		uint32_t L_375 = (L_372)->GetAt(static_cast<il2cpp_array_size_t>(L_374));
		UInt32U5BU5D_t2770800703* L_376 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_377 = V_1;
		NullCheck(L_376);
		int32_t L_378 = (((int32_t)((uint8_t)L_377)));
		uint32_t L_379 = (L_376)->GetAt(static_cast<il2cpp_array_size_t>(L_378));
		UInt32U5BU5D_t2770800703* L_380 = ___ekey2;
		NullCheck(L_380);
		int32_t L_381 = ((int32_t)20);
		uint32_t L_382 = (L_380)->GetAt(static_cast<il2cpp_array_size_t>(L_381));
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_367^(int32_t)L_371))^(int32_t)L_375))^(int32_t)L_379))^(int32_t)L_382));
		UInt32U5BU5D_t2770800703* L_383 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_384 = V_1;
		NullCheck(L_383);
		uintptr_t L_385 = (((uintptr_t)((int32_t)((uint32_t)L_384>>((int32_t)24)))));
		uint32_t L_386 = (L_383)->GetAt(static_cast<il2cpp_array_size_t>(L_385));
		UInt32U5BU5D_t2770800703* L_387 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_388 = V_0;
		NullCheck(L_387);
		int32_t L_389 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_388>>((int32_t)16))))));
		uint32_t L_390 = (L_387)->GetAt(static_cast<il2cpp_array_size_t>(L_389));
		UInt32U5BU5D_t2770800703* L_391 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_392 = V_3;
		NullCheck(L_391);
		int32_t L_393 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_392>>8)))));
		uint32_t L_394 = (L_391)->GetAt(static_cast<il2cpp_array_size_t>(L_393));
		UInt32U5BU5D_t2770800703* L_395 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_396 = V_2;
		NullCheck(L_395);
		int32_t L_397 = (((int32_t)((uint8_t)L_396)));
		uint32_t L_398 = (L_395)->GetAt(static_cast<il2cpp_array_size_t>(L_397));
		UInt32U5BU5D_t2770800703* L_399 = ___ekey2;
		NullCheck(L_399);
		int32_t L_400 = ((int32_t)21);
		uint32_t L_401 = (L_399)->GetAt(static_cast<il2cpp_array_size_t>(L_400));
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_386^(int32_t)L_390))^(int32_t)L_394))^(int32_t)L_398))^(int32_t)L_401));
		UInt32U5BU5D_t2770800703* L_402 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_403 = V_2;
		NullCheck(L_402);
		uintptr_t L_404 = (((uintptr_t)((int32_t)((uint32_t)L_403>>((int32_t)24)))));
		uint32_t L_405 = (L_402)->GetAt(static_cast<il2cpp_array_size_t>(L_404));
		UInt32U5BU5D_t2770800703* L_406 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_407 = V_1;
		NullCheck(L_406);
		int32_t L_408 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_407>>((int32_t)16))))));
		uint32_t L_409 = (L_406)->GetAt(static_cast<il2cpp_array_size_t>(L_408));
		UInt32U5BU5D_t2770800703* L_410 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_411 = V_0;
		NullCheck(L_410);
		int32_t L_412 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_411>>8)))));
		uint32_t L_413 = (L_410)->GetAt(static_cast<il2cpp_array_size_t>(L_412));
		UInt32U5BU5D_t2770800703* L_414 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_415 = V_3;
		NullCheck(L_414);
		int32_t L_416 = (((int32_t)((uint8_t)L_415)));
		uint32_t L_417 = (L_414)->GetAt(static_cast<il2cpp_array_size_t>(L_416));
		UInt32U5BU5D_t2770800703* L_418 = ___ekey2;
		NullCheck(L_418);
		int32_t L_419 = ((int32_t)22);
		uint32_t L_420 = (L_418)->GetAt(static_cast<il2cpp_array_size_t>(L_419));
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_405^(int32_t)L_409))^(int32_t)L_413))^(int32_t)L_417))^(int32_t)L_420));
		UInt32U5BU5D_t2770800703* L_421 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_422 = V_3;
		NullCheck(L_421);
		uintptr_t L_423 = (((uintptr_t)((int32_t)((uint32_t)L_422>>((int32_t)24)))));
		uint32_t L_424 = (L_421)->GetAt(static_cast<il2cpp_array_size_t>(L_423));
		UInt32U5BU5D_t2770800703* L_425 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_426 = V_2;
		NullCheck(L_425);
		int32_t L_427 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_426>>((int32_t)16))))));
		uint32_t L_428 = (L_425)->GetAt(static_cast<il2cpp_array_size_t>(L_427));
		UInt32U5BU5D_t2770800703* L_429 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_430 = V_1;
		NullCheck(L_429);
		int32_t L_431 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_430>>8)))));
		uint32_t L_432 = (L_429)->GetAt(static_cast<il2cpp_array_size_t>(L_431));
		UInt32U5BU5D_t2770800703* L_433 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_434 = V_0;
		NullCheck(L_433);
		int32_t L_435 = (((int32_t)((uint8_t)L_434)));
		uint32_t L_436 = (L_433)->GetAt(static_cast<il2cpp_array_size_t>(L_435));
		UInt32U5BU5D_t2770800703* L_437 = ___ekey2;
		NullCheck(L_437);
		int32_t L_438 = ((int32_t)23);
		uint32_t L_439 = (L_437)->GetAt(static_cast<il2cpp_array_size_t>(L_438));
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_424^(int32_t)L_428))^(int32_t)L_432))^(int32_t)L_436))^(int32_t)L_439));
		UInt32U5BU5D_t2770800703* L_440 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_441 = V_4;
		NullCheck(L_440);
		uintptr_t L_442 = (((uintptr_t)((int32_t)((uint32_t)L_441>>((int32_t)24)))));
		uint32_t L_443 = (L_440)->GetAt(static_cast<il2cpp_array_size_t>(L_442));
		UInt32U5BU5D_t2770800703* L_444 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_445 = V_7;
		NullCheck(L_444);
		int32_t L_446 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_445>>((int32_t)16))))));
		uint32_t L_447 = (L_444)->GetAt(static_cast<il2cpp_array_size_t>(L_446));
		UInt32U5BU5D_t2770800703* L_448 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_449 = V_6;
		NullCheck(L_448);
		int32_t L_450 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_449>>8)))));
		uint32_t L_451 = (L_448)->GetAt(static_cast<il2cpp_array_size_t>(L_450));
		UInt32U5BU5D_t2770800703* L_452 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_453 = V_5;
		NullCheck(L_452);
		int32_t L_454 = (((int32_t)((uint8_t)L_453)));
		uint32_t L_455 = (L_452)->GetAt(static_cast<il2cpp_array_size_t>(L_454));
		UInt32U5BU5D_t2770800703* L_456 = ___ekey2;
		NullCheck(L_456);
		int32_t L_457 = ((int32_t)24);
		uint32_t L_458 = (L_456)->GetAt(static_cast<il2cpp_array_size_t>(L_457));
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_443^(int32_t)L_447))^(int32_t)L_451))^(int32_t)L_455))^(int32_t)L_458));
		UInt32U5BU5D_t2770800703* L_459 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_460 = V_5;
		NullCheck(L_459);
		uintptr_t L_461 = (((uintptr_t)((int32_t)((uint32_t)L_460>>((int32_t)24)))));
		uint32_t L_462 = (L_459)->GetAt(static_cast<il2cpp_array_size_t>(L_461));
		UInt32U5BU5D_t2770800703* L_463 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_464 = V_4;
		NullCheck(L_463);
		int32_t L_465 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_464>>((int32_t)16))))));
		uint32_t L_466 = (L_463)->GetAt(static_cast<il2cpp_array_size_t>(L_465));
		UInt32U5BU5D_t2770800703* L_467 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_468 = V_7;
		NullCheck(L_467);
		int32_t L_469 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_468>>8)))));
		uint32_t L_470 = (L_467)->GetAt(static_cast<il2cpp_array_size_t>(L_469));
		UInt32U5BU5D_t2770800703* L_471 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_472 = V_6;
		NullCheck(L_471);
		int32_t L_473 = (((int32_t)((uint8_t)L_472)));
		uint32_t L_474 = (L_471)->GetAt(static_cast<il2cpp_array_size_t>(L_473));
		UInt32U5BU5D_t2770800703* L_475 = ___ekey2;
		NullCheck(L_475);
		int32_t L_476 = ((int32_t)25);
		uint32_t L_477 = (L_475)->GetAt(static_cast<il2cpp_array_size_t>(L_476));
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_462^(int32_t)L_466))^(int32_t)L_470))^(int32_t)L_474))^(int32_t)L_477));
		UInt32U5BU5D_t2770800703* L_478 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_479 = V_6;
		NullCheck(L_478);
		uintptr_t L_480 = (((uintptr_t)((int32_t)((uint32_t)L_479>>((int32_t)24)))));
		uint32_t L_481 = (L_478)->GetAt(static_cast<il2cpp_array_size_t>(L_480));
		UInt32U5BU5D_t2770800703* L_482 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_483 = V_5;
		NullCheck(L_482);
		int32_t L_484 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_483>>((int32_t)16))))));
		uint32_t L_485 = (L_482)->GetAt(static_cast<il2cpp_array_size_t>(L_484));
		UInt32U5BU5D_t2770800703* L_486 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_487 = V_4;
		NullCheck(L_486);
		int32_t L_488 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_487>>8)))));
		uint32_t L_489 = (L_486)->GetAt(static_cast<il2cpp_array_size_t>(L_488));
		UInt32U5BU5D_t2770800703* L_490 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_491 = V_7;
		NullCheck(L_490);
		int32_t L_492 = (((int32_t)((uint8_t)L_491)));
		uint32_t L_493 = (L_490)->GetAt(static_cast<il2cpp_array_size_t>(L_492));
		UInt32U5BU5D_t2770800703* L_494 = ___ekey2;
		NullCheck(L_494);
		int32_t L_495 = ((int32_t)26);
		uint32_t L_496 = (L_494)->GetAt(static_cast<il2cpp_array_size_t>(L_495));
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_481^(int32_t)L_485))^(int32_t)L_489))^(int32_t)L_493))^(int32_t)L_496));
		UInt32U5BU5D_t2770800703* L_497 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_498 = V_7;
		NullCheck(L_497);
		uintptr_t L_499 = (((uintptr_t)((int32_t)((uint32_t)L_498>>((int32_t)24)))));
		uint32_t L_500 = (L_497)->GetAt(static_cast<il2cpp_array_size_t>(L_499));
		UInt32U5BU5D_t2770800703* L_501 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_502 = V_6;
		NullCheck(L_501);
		int32_t L_503 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_502>>((int32_t)16))))));
		uint32_t L_504 = (L_501)->GetAt(static_cast<il2cpp_array_size_t>(L_503));
		UInt32U5BU5D_t2770800703* L_505 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_506 = V_5;
		NullCheck(L_505);
		int32_t L_507 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_506>>8)))));
		uint32_t L_508 = (L_505)->GetAt(static_cast<il2cpp_array_size_t>(L_507));
		UInt32U5BU5D_t2770800703* L_509 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_510 = V_4;
		NullCheck(L_509);
		int32_t L_511 = (((int32_t)((uint8_t)L_510)));
		uint32_t L_512 = (L_509)->GetAt(static_cast<il2cpp_array_size_t>(L_511));
		UInt32U5BU5D_t2770800703* L_513 = ___ekey2;
		NullCheck(L_513);
		int32_t L_514 = ((int32_t)27);
		uint32_t L_515 = (L_513)->GetAt(static_cast<il2cpp_array_size_t>(L_514));
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_500^(int32_t)L_504))^(int32_t)L_508))^(int32_t)L_512))^(int32_t)L_515));
		UInt32U5BU5D_t2770800703* L_516 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_517 = V_0;
		NullCheck(L_516);
		uintptr_t L_518 = (((uintptr_t)((int32_t)((uint32_t)L_517>>((int32_t)24)))));
		uint32_t L_519 = (L_516)->GetAt(static_cast<il2cpp_array_size_t>(L_518));
		UInt32U5BU5D_t2770800703* L_520 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_521 = V_3;
		NullCheck(L_520);
		int32_t L_522 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_521>>((int32_t)16))))));
		uint32_t L_523 = (L_520)->GetAt(static_cast<il2cpp_array_size_t>(L_522));
		UInt32U5BU5D_t2770800703* L_524 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_525 = V_2;
		NullCheck(L_524);
		int32_t L_526 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_525>>8)))));
		uint32_t L_527 = (L_524)->GetAt(static_cast<il2cpp_array_size_t>(L_526));
		UInt32U5BU5D_t2770800703* L_528 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_529 = V_1;
		NullCheck(L_528);
		int32_t L_530 = (((int32_t)((uint8_t)L_529)));
		uint32_t L_531 = (L_528)->GetAt(static_cast<il2cpp_array_size_t>(L_530));
		UInt32U5BU5D_t2770800703* L_532 = ___ekey2;
		NullCheck(L_532);
		int32_t L_533 = ((int32_t)28);
		uint32_t L_534 = (L_532)->GetAt(static_cast<il2cpp_array_size_t>(L_533));
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_519^(int32_t)L_523))^(int32_t)L_527))^(int32_t)L_531))^(int32_t)L_534));
		UInt32U5BU5D_t2770800703* L_535 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_536 = V_1;
		NullCheck(L_535);
		uintptr_t L_537 = (((uintptr_t)((int32_t)((uint32_t)L_536>>((int32_t)24)))));
		uint32_t L_538 = (L_535)->GetAt(static_cast<il2cpp_array_size_t>(L_537));
		UInt32U5BU5D_t2770800703* L_539 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_540 = V_0;
		NullCheck(L_539);
		int32_t L_541 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_540>>((int32_t)16))))));
		uint32_t L_542 = (L_539)->GetAt(static_cast<il2cpp_array_size_t>(L_541));
		UInt32U5BU5D_t2770800703* L_543 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_544 = V_3;
		NullCheck(L_543);
		int32_t L_545 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_544>>8)))));
		uint32_t L_546 = (L_543)->GetAt(static_cast<il2cpp_array_size_t>(L_545));
		UInt32U5BU5D_t2770800703* L_547 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_548 = V_2;
		NullCheck(L_547);
		int32_t L_549 = (((int32_t)((uint8_t)L_548)));
		uint32_t L_550 = (L_547)->GetAt(static_cast<il2cpp_array_size_t>(L_549));
		UInt32U5BU5D_t2770800703* L_551 = ___ekey2;
		NullCheck(L_551);
		int32_t L_552 = ((int32_t)29);
		uint32_t L_553 = (L_551)->GetAt(static_cast<il2cpp_array_size_t>(L_552));
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_538^(int32_t)L_542))^(int32_t)L_546))^(int32_t)L_550))^(int32_t)L_553));
		UInt32U5BU5D_t2770800703* L_554 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_555 = V_2;
		NullCheck(L_554);
		uintptr_t L_556 = (((uintptr_t)((int32_t)((uint32_t)L_555>>((int32_t)24)))));
		uint32_t L_557 = (L_554)->GetAt(static_cast<il2cpp_array_size_t>(L_556));
		UInt32U5BU5D_t2770800703* L_558 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_559 = V_1;
		NullCheck(L_558);
		int32_t L_560 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_559>>((int32_t)16))))));
		uint32_t L_561 = (L_558)->GetAt(static_cast<il2cpp_array_size_t>(L_560));
		UInt32U5BU5D_t2770800703* L_562 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_563 = V_0;
		NullCheck(L_562);
		int32_t L_564 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_563>>8)))));
		uint32_t L_565 = (L_562)->GetAt(static_cast<il2cpp_array_size_t>(L_564));
		UInt32U5BU5D_t2770800703* L_566 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_567 = V_3;
		NullCheck(L_566);
		int32_t L_568 = (((int32_t)((uint8_t)L_567)));
		uint32_t L_569 = (L_566)->GetAt(static_cast<il2cpp_array_size_t>(L_568));
		UInt32U5BU5D_t2770800703* L_570 = ___ekey2;
		NullCheck(L_570);
		int32_t L_571 = ((int32_t)30);
		uint32_t L_572 = (L_570)->GetAt(static_cast<il2cpp_array_size_t>(L_571));
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_557^(int32_t)L_561))^(int32_t)L_565))^(int32_t)L_569))^(int32_t)L_572));
		UInt32U5BU5D_t2770800703* L_573 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_574 = V_3;
		NullCheck(L_573);
		uintptr_t L_575 = (((uintptr_t)((int32_t)((uint32_t)L_574>>((int32_t)24)))));
		uint32_t L_576 = (L_573)->GetAt(static_cast<il2cpp_array_size_t>(L_575));
		UInt32U5BU5D_t2770800703* L_577 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_578 = V_2;
		NullCheck(L_577);
		int32_t L_579 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_578>>((int32_t)16))))));
		uint32_t L_580 = (L_577)->GetAt(static_cast<il2cpp_array_size_t>(L_579));
		UInt32U5BU5D_t2770800703* L_581 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_582 = V_1;
		NullCheck(L_581);
		int32_t L_583 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_582>>8)))));
		uint32_t L_584 = (L_581)->GetAt(static_cast<il2cpp_array_size_t>(L_583));
		UInt32U5BU5D_t2770800703* L_585 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_586 = V_0;
		NullCheck(L_585);
		int32_t L_587 = (((int32_t)((uint8_t)L_586)));
		uint32_t L_588 = (L_585)->GetAt(static_cast<il2cpp_array_size_t>(L_587));
		UInt32U5BU5D_t2770800703* L_589 = ___ekey2;
		NullCheck(L_589);
		int32_t L_590 = ((int32_t)31);
		uint32_t L_591 = (L_589)->GetAt(static_cast<il2cpp_array_size_t>(L_590));
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_576^(int32_t)L_580))^(int32_t)L_584))^(int32_t)L_588))^(int32_t)L_591));
		UInt32U5BU5D_t2770800703* L_592 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_593 = V_4;
		NullCheck(L_592);
		uintptr_t L_594 = (((uintptr_t)((int32_t)((uint32_t)L_593>>((int32_t)24)))));
		uint32_t L_595 = (L_592)->GetAt(static_cast<il2cpp_array_size_t>(L_594));
		UInt32U5BU5D_t2770800703* L_596 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_597 = V_7;
		NullCheck(L_596);
		int32_t L_598 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_597>>((int32_t)16))))));
		uint32_t L_599 = (L_596)->GetAt(static_cast<il2cpp_array_size_t>(L_598));
		UInt32U5BU5D_t2770800703* L_600 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_601 = V_6;
		NullCheck(L_600);
		int32_t L_602 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_601>>8)))));
		uint32_t L_603 = (L_600)->GetAt(static_cast<il2cpp_array_size_t>(L_602));
		UInt32U5BU5D_t2770800703* L_604 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_605 = V_5;
		NullCheck(L_604);
		int32_t L_606 = (((int32_t)((uint8_t)L_605)));
		uint32_t L_607 = (L_604)->GetAt(static_cast<il2cpp_array_size_t>(L_606));
		UInt32U5BU5D_t2770800703* L_608 = ___ekey2;
		NullCheck(L_608);
		int32_t L_609 = ((int32_t)32);
		uint32_t L_610 = (L_608)->GetAt(static_cast<il2cpp_array_size_t>(L_609));
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_595^(int32_t)L_599))^(int32_t)L_603))^(int32_t)L_607))^(int32_t)L_610));
		UInt32U5BU5D_t2770800703* L_611 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_612 = V_5;
		NullCheck(L_611);
		uintptr_t L_613 = (((uintptr_t)((int32_t)((uint32_t)L_612>>((int32_t)24)))));
		uint32_t L_614 = (L_611)->GetAt(static_cast<il2cpp_array_size_t>(L_613));
		UInt32U5BU5D_t2770800703* L_615 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_616 = V_4;
		NullCheck(L_615);
		int32_t L_617 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_616>>((int32_t)16))))));
		uint32_t L_618 = (L_615)->GetAt(static_cast<il2cpp_array_size_t>(L_617));
		UInt32U5BU5D_t2770800703* L_619 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_620 = V_7;
		NullCheck(L_619);
		int32_t L_621 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_620>>8)))));
		uint32_t L_622 = (L_619)->GetAt(static_cast<il2cpp_array_size_t>(L_621));
		UInt32U5BU5D_t2770800703* L_623 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_624 = V_6;
		NullCheck(L_623);
		int32_t L_625 = (((int32_t)((uint8_t)L_624)));
		uint32_t L_626 = (L_623)->GetAt(static_cast<il2cpp_array_size_t>(L_625));
		UInt32U5BU5D_t2770800703* L_627 = ___ekey2;
		NullCheck(L_627);
		int32_t L_628 = ((int32_t)33);
		uint32_t L_629 = (L_627)->GetAt(static_cast<il2cpp_array_size_t>(L_628));
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_614^(int32_t)L_618))^(int32_t)L_622))^(int32_t)L_626))^(int32_t)L_629));
		UInt32U5BU5D_t2770800703* L_630 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_631 = V_6;
		NullCheck(L_630);
		uintptr_t L_632 = (((uintptr_t)((int32_t)((uint32_t)L_631>>((int32_t)24)))));
		uint32_t L_633 = (L_630)->GetAt(static_cast<il2cpp_array_size_t>(L_632));
		UInt32U5BU5D_t2770800703* L_634 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_635 = V_5;
		NullCheck(L_634);
		int32_t L_636 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_635>>((int32_t)16))))));
		uint32_t L_637 = (L_634)->GetAt(static_cast<il2cpp_array_size_t>(L_636));
		UInt32U5BU5D_t2770800703* L_638 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_639 = V_4;
		NullCheck(L_638);
		int32_t L_640 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_639>>8)))));
		uint32_t L_641 = (L_638)->GetAt(static_cast<il2cpp_array_size_t>(L_640));
		UInt32U5BU5D_t2770800703* L_642 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_643 = V_7;
		NullCheck(L_642);
		int32_t L_644 = (((int32_t)((uint8_t)L_643)));
		uint32_t L_645 = (L_642)->GetAt(static_cast<il2cpp_array_size_t>(L_644));
		UInt32U5BU5D_t2770800703* L_646 = ___ekey2;
		NullCheck(L_646);
		int32_t L_647 = ((int32_t)34);
		uint32_t L_648 = (L_646)->GetAt(static_cast<il2cpp_array_size_t>(L_647));
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_633^(int32_t)L_637))^(int32_t)L_641))^(int32_t)L_645))^(int32_t)L_648));
		UInt32U5BU5D_t2770800703* L_649 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_650 = V_7;
		NullCheck(L_649);
		uintptr_t L_651 = (((uintptr_t)((int32_t)((uint32_t)L_650>>((int32_t)24)))));
		uint32_t L_652 = (L_649)->GetAt(static_cast<il2cpp_array_size_t>(L_651));
		UInt32U5BU5D_t2770800703* L_653 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_654 = V_6;
		NullCheck(L_653);
		int32_t L_655 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_654>>((int32_t)16))))));
		uint32_t L_656 = (L_653)->GetAt(static_cast<il2cpp_array_size_t>(L_655));
		UInt32U5BU5D_t2770800703* L_657 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_658 = V_5;
		NullCheck(L_657);
		int32_t L_659 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_658>>8)))));
		uint32_t L_660 = (L_657)->GetAt(static_cast<il2cpp_array_size_t>(L_659));
		UInt32U5BU5D_t2770800703* L_661 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_662 = V_4;
		NullCheck(L_661);
		int32_t L_663 = (((int32_t)((uint8_t)L_662)));
		uint32_t L_664 = (L_661)->GetAt(static_cast<il2cpp_array_size_t>(L_663));
		UInt32U5BU5D_t2770800703* L_665 = ___ekey2;
		NullCheck(L_665);
		int32_t L_666 = ((int32_t)35);
		uint32_t L_667 = (L_665)->GetAt(static_cast<il2cpp_array_size_t>(L_666));
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_652^(int32_t)L_656))^(int32_t)L_660))^(int32_t)L_664))^(int32_t)L_667));
		UInt32U5BU5D_t2770800703* L_668 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_669 = V_0;
		NullCheck(L_668);
		uintptr_t L_670 = (((uintptr_t)((int32_t)((uint32_t)L_669>>((int32_t)24)))));
		uint32_t L_671 = (L_668)->GetAt(static_cast<il2cpp_array_size_t>(L_670));
		UInt32U5BU5D_t2770800703* L_672 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_673 = V_3;
		NullCheck(L_672);
		int32_t L_674 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_673>>((int32_t)16))))));
		uint32_t L_675 = (L_672)->GetAt(static_cast<il2cpp_array_size_t>(L_674));
		UInt32U5BU5D_t2770800703* L_676 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_677 = V_2;
		NullCheck(L_676);
		int32_t L_678 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_677>>8)))));
		uint32_t L_679 = (L_676)->GetAt(static_cast<il2cpp_array_size_t>(L_678));
		UInt32U5BU5D_t2770800703* L_680 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_681 = V_1;
		NullCheck(L_680);
		int32_t L_682 = (((int32_t)((uint8_t)L_681)));
		uint32_t L_683 = (L_680)->GetAt(static_cast<il2cpp_array_size_t>(L_682));
		UInt32U5BU5D_t2770800703* L_684 = ___ekey2;
		NullCheck(L_684);
		int32_t L_685 = ((int32_t)36);
		uint32_t L_686 = (L_684)->GetAt(static_cast<il2cpp_array_size_t>(L_685));
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_671^(int32_t)L_675))^(int32_t)L_679))^(int32_t)L_683))^(int32_t)L_686));
		UInt32U5BU5D_t2770800703* L_687 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_688 = V_1;
		NullCheck(L_687);
		uintptr_t L_689 = (((uintptr_t)((int32_t)((uint32_t)L_688>>((int32_t)24)))));
		uint32_t L_690 = (L_687)->GetAt(static_cast<il2cpp_array_size_t>(L_689));
		UInt32U5BU5D_t2770800703* L_691 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_692 = V_0;
		NullCheck(L_691);
		int32_t L_693 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_692>>((int32_t)16))))));
		uint32_t L_694 = (L_691)->GetAt(static_cast<il2cpp_array_size_t>(L_693));
		UInt32U5BU5D_t2770800703* L_695 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_696 = V_3;
		NullCheck(L_695);
		int32_t L_697 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_696>>8)))));
		uint32_t L_698 = (L_695)->GetAt(static_cast<il2cpp_array_size_t>(L_697));
		UInt32U5BU5D_t2770800703* L_699 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_700 = V_2;
		NullCheck(L_699);
		int32_t L_701 = (((int32_t)((uint8_t)L_700)));
		uint32_t L_702 = (L_699)->GetAt(static_cast<il2cpp_array_size_t>(L_701));
		UInt32U5BU5D_t2770800703* L_703 = ___ekey2;
		NullCheck(L_703);
		int32_t L_704 = ((int32_t)37);
		uint32_t L_705 = (L_703)->GetAt(static_cast<il2cpp_array_size_t>(L_704));
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_690^(int32_t)L_694))^(int32_t)L_698))^(int32_t)L_702))^(int32_t)L_705));
		UInt32U5BU5D_t2770800703* L_706 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_707 = V_2;
		NullCheck(L_706);
		uintptr_t L_708 = (((uintptr_t)((int32_t)((uint32_t)L_707>>((int32_t)24)))));
		uint32_t L_709 = (L_706)->GetAt(static_cast<il2cpp_array_size_t>(L_708));
		UInt32U5BU5D_t2770800703* L_710 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_711 = V_1;
		NullCheck(L_710);
		int32_t L_712 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_711>>((int32_t)16))))));
		uint32_t L_713 = (L_710)->GetAt(static_cast<il2cpp_array_size_t>(L_712));
		UInt32U5BU5D_t2770800703* L_714 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_715 = V_0;
		NullCheck(L_714);
		int32_t L_716 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_715>>8)))));
		uint32_t L_717 = (L_714)->GetAt(static_cast<il2cpp_array_size_t>(L_716));
		UInt32U5BU5D_t2770800703* L_718 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_719 = V_3;
		NullCheck(L_718);
		int32_t L_720 = (((int32_t)((uint8_t)L_719)));
		uint32_t L_721 = (L_718)->GetAt(static_cast<il2cpp_array_size_t>(L_720));
		UInt32U5BU5D_t2770800703* L_722 = ___ekey2;
		NullCheck(L_722);
		int32_t L_723 = ((int32_t)38);
		uint32_t L_724 = (L_722)->GetAt(static_cast<il2cpp_array_size_t>(L_723));
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_709^(int32_t)L_713))^(int32_t)L_717))^(int32_t)L_721))^(int32_t)L_724));
		UInt32U5BU5D_t2770800703* L_725 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_726 = V_3;
		NullCheck(L_725);
		uintptr_t L_727 = (((uintptr_t)((int32_t)((uint32_t)L_726>>((int32_t)24)))));
		uint32_t L_728 = (L_725)->GetAt(static_cast<il2cpp_array_size_t>(L_727));
		UInt32U5BU5D_t2770800703* L_729 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_730 = V_2;
		NullCheck(L_729);
		int32_t L_731 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_730>>((int32_t)16))))));
		uint32_t L_732 = (L_729)->GetAt(static_cast<il2cpp_array_size_t>(L_731));
		UInt32U5BU5D_t2770800703* L_733 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_734 = V_1;
		NullCheck(L_733);
		int32_t L_735 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_734>>8)))));
		uint32_t L_736 = (L_733)->GetAt(static_cast<il2cpp_array_size_t>(L_735));
		UInt32U5BU5D_t2770800703* L_737 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_738 = V_0;
		NullCheck(L_737);
		int32_t L_739 = (((int32_t)((uint8_t)L_738)));
		uint32_t L_740 = (L_737)->GetAt(static_cast<il2cpp_array_size_t>(L_739));
		UInt32U5BU5D_t2770800703* L_741 = ___ekey2;
		NullCheck(L_741);
		int32_t L_742 = ((int32_t)39);
		uint32_t L_743 = (L_741)->GetAt(static_cast<il2cpp_array_size_t>(L_742));
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_728^(int32_t)L_732))^(int32_t)L_736))^(int32_t)L_740))^(int32_t)L_743));
		int32_t L_744 = __this->get_Nr_14();
		if ((((int32_t)L_744) <= ((int32_t)((int32_t)10))))
		{
			goto IL_0b08;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AesTransform_t2957123611_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t2770800703* L_745 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_746 = V_4;
		NullCheck(L_745);
		uintptr_t L_747 = (((uintptr_t)((int32_t)((uint32_t)L_746>>((int32_t)24)))));
		uint32_t L_748 = (L_745)->GetAt(static_cast<il2cpp_array_size_t>(L_747));
		UInt32U5BU5D_t2770800703* L_749 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_750 = V_7;
		NullCheck(L_749);
		int32_t L_751 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_750>>((int32_t)16))))));
		uint32_t L_752 = (L_749)->GetAt(static_cast<il2cpp_array_size_t>(L_751));
		UInt32U5BU5D_t2770800703* L_753 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_754 = V_6;
		NullCheck(L_753);
		int32_t L_755 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_754>>8)))));
		uint32_t L_756 = (L_753)->GetAt(static_cast<il2cpp_array_size_t>(L_755));
		UInt32U5BU5D_t2770800703* L_757 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_758 = V_5;
		NullCheck(L_757);
		int32_t L_759 = (((int32_t)((uint8_t)L_758)));
		uint32_t L_760 = (L_757)->GetAt(static_cast<il2cpp_array_size_t>(L_759));
		UInt32U5BU5D_t2770800703* L_761 = ___ekey2;
		NullCheck(L_761);
		int32_t L_762 = ((int32_t)40);
		uint32_t L_763 = (L_761)->GetAt(static_cast<il2cpp_array_size_t>(L_762));
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_748^(int32_t)L_752))^(int32_t)L_756))^(int32_t)L_760))^(int32_t)L_763));
		UInt32U5BU5D_t2770800703* L_764 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_765 = V_5;
		NullCheck(L_764);
		uintptr_t L_766 = (((uintptr_t)((int32_t)((uint32_t)L_765>>((int32_t)24)))));
		uint32_t L_767 = (L_764)->GetAt(static_cast<il2cpp_array_size_t>(L_766));
		UInt32U5BU5D_t2770800703* L_768 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_769 = V_4;
		NullCheck(L_768);
		int32_t L_770 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_769>>((int32_t)16))))));
		uint32_t L_771 = (L_768)->GetAt(static_cast<il2cpp_array_size_t>(L_770));
		UInt32U5BU5D_t2770800703* L_772 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_773 = V_7;
		NullCheck(L_772);
		int32_t L_774 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_773>>8)))));
		uint32_t L_775 = (L_772)->GetAt(static_cast<il2cpp_array_size_t>(L_774));
		UInt32U5BU5D_t2770800703* L_776 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_777 = V_6;
		NullCheck(L_776);
		int32_t L_778 = (((int32_t)((uint8_t)L_777)));
		uint32_t L_779 = (L_776)->GetAt(static_cast<il2cpp_array_size_t>(L_778));
		UInt32U5BU5D_t2770800703* L_780 = ___ekey2;
		NullCheck(L_780);
		int32_t L_781 = ((int32_t)41);
		uint32_t L_782 = (L_780)->GetAt(static_cast<il2cpp_array_size_t>(L_781));
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_767^(int32_t)L_771))^(int32_t)L_775))^(int32_t)L_779))^(int32_t)L_782));
		UInt32U5BU5D_t2770800703* L_783 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_784 = V_6;
		NullCheck(L_783);
		uintptr_t L_785 = (((uintptr_t)((int32_t)((uint32_t)L_784>>((int32_t)24)))));
		uint32_t L_786 = (L_783)->GetAt(static_cast<il2cpp_array_size_t>(L_785));
		UInt32U5BU5D_t2770800703* L_787 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_788 = V_5;
		NullCheck(L_787);
		int32_t L_789 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_788>>((int32_t)16))))));
		uint32_t L_790 = (L_787)->GetAt(static_cast<il2cpp_array_size_t>(L_789));
		UInt32U5BU5D_t2770800703* L_791 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_792 = V_4;
		NullCheck(L_791);
		int32_t L_793 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_792>>8)))));
		uint32_t L_794 = (L_791)->GetAt(static_cast<il2cpp_array_size_t>(L_793));
		UInt32U5BU5D_t2770800703* L_795 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_796 = V_7;
		NullCheck(L_795);
		int32_t L_797 = (((int32_t)((uint8_t)L_796)));
		uint32_t L_798 = (L_795)->GetAt(static_cast<il2cpp_array_size_t>(L_797));
		UInt32U5BU5D_t2770800703* L_799 = ___ekey2;
		NullCheck(L_799);
		int32_t L_800 = ((int32_t)42);
		uint32_t L_801 = (L_799)->GetAt(static_cast<il2cpp_array_size_t>(L_800));
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_786^(int32_t)L_790))^(int32_t)L_794))^(int32_t)L_798))^(int32_t)L_801));
		UInt32U5BU5D_t2770800703* L_802 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_803 = V_7;
		NullCheck(L_802);
		uintptr_t L_804 = (((uintptr_t)((int32_t)((uint32_t)L_803>>((int32_t)24)))));
		uint32_t L_805 = (L_802)->GetAt(static_cast<il2cpp_array_size_t>(L_804));
		UInt32U5BU5D_t2770800703* L_806 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_807 = V_6;
		NullCheck(L_806);
		int32_t L_808 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_807>>((int32_t)16))))));
		uint32_t L_809 = (L_806)->GetAt(static_cast<il2cpp_array_size_t>(L_808));
		UInt32U5BU5D_t2770800703* L_810 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_811 = V_5;
		NullCheck(L_810);
		int32_t L_812 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_811>>8)))));
		uint32_t L_813 = (L_810)->GetAt(static_cast<il2cpp_array_size_t>(L_812));
		UInt32U5BU5D_t2770800703* L_814 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_815 = V_4;
		NullCheck(L_814);
		int32_t L_816 = (((int32_t)((uint8_t)L_815)));
		uint32_t L_817 = (L_814)->GetAt(static_cast<il2cpp_array_size_t>(L_816));
		UInt32U5BU5D_t2770800703* L_818 = ___ekey2;
		NullCheck(L_818);
		int32_t L_819 = ((int32_t)43);
		uint32_t L_820 = (L_818)->GetAt(static_cast<il2cpp_array_size_t>(L_819));
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_805^(int32_t)L_809))^(int32_t)L_813))^(int32_t)L_817))^(int32_t)L_820));
		UInt32U5BU5D_t2770800703* L_821 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_822 = V_0;
		NullCheck(L_821);
		uintptr_t L_823 = (((uintptr_t)((int32_t)((uint32_t)L_822>>((int32_t)24)))));
		uint32_t L_824 = (L_821)->GetAt(static_cast<il2cpp_array_size_t>(L_823));
		UInt32U5BU5D_t2770800703* L_825 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_826 = V_3;
		NullCheck(L_825);
		int32_t L_827 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_826>>((int32_t)16))))));
		uint32_t L_828 = (L_825)->GetAt(static_cast<il2cpp_array_size_t>(L_827));
		UInt32U5BU5D_t2770800703* L_829 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_830 = V_2;
		NullCheck(L_829);
		int32_t L_831 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_830>>8)))));
		uint32_t L_832 = (L_829)->GetAt(static_cast<il2cpp_array_size_t>(L_831));
		UInt32U5BU5D_t2770800703* L_833 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_834 = V_1;
		NullCheck(L_833);
		int32_t L_835 = (((int32_t)((uint8_t)L_834)));
		uint32_t L_836 = (L_833)->GetAt(static_cast<il2cpp_array_size_t>(L_835));
		UInt32U5BU5D_t2770800703* L_837 = ___ekey2;
		NullCheck(L_837);
		int32_t L_838 = ((int32_t)44);
		uint32_t L_839 = (L_837)->GetAt(static_cast<il2cpp_array_size_t>(L_838));
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_824^(int32_t)L_828))^(int32_t)L_832))^(int32_t)L_836))^(int32_t)L_839));
		UInt32U5BU5D_t2770800703* L_840 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_841 = V_1;
		NullCheck(L_840);
		uintptr_t L_842 = (((uintptr_t)((int32_t)((uint32_t)L_841>>((int32_t)24)))));
		uint32_t L_843 = (L_840)->GetAt(static_cast<il2cpp_array_size_t>(L_842));
		UInt32U5BU5D_t2770800703* L_844 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_845 = V_0;
		NullCheck(L_844);
		int32_t L_846 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_845>>((int32_t)16))))));
		uint32_t L_847 = (L_844)->GetAt(static_cast<il2cpp_array_size_t>(L_846));
		UInt32U5BU5D_t2770800703* L_848 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_849 = V_3;
		NullCheck(L_848);
		int32_t L_850 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_849>>8)))));
		uint32_t L_851 = (L_848)->GetAt(static_cast<il2cpp_array_size_t>(L_850));
		UInt32U5BU5D_t2770800703* L_852 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_853 = V_2;
		NullCheck(L_852);
		int32_t L_854 = (((int32_t)((uint8_t)L_853)));
		uint32_t L_855 = (L_852)->GetAt(static_cast<il2cpp_array_size_t>(L_854));
		UInt32U5BU5D_t2770800703* L_856 = ___ekey2;
		NullCheck(L_856);
		int32_t L_857 = ((int32_t)45);
		uint32_t L_858 = (L_856)->GetAt(static_cast<il2cpp_array_size_t>(L_857));
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_843^(int32_t)L_847))^(int32_t)L_851))^(int32_t)L_855))^(int32_t)L_858));
		UInt32U5BU5D_t2770800703* L_859 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_860 = V_2;
		NullCheck(L_859);
		uintptr_t L_861 = (((uintptr_t)((int32_t)((uint32_t)L_860>>((int32_t)24)))));
		uint32_t L_862 = (L_859)->GetAt(static_cast<il2cpp_array_size_t>(L_861));
		UInt32U5BU5D_t2770800703* L_863 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_864 = V_1;
		NullCheck(L_863);
		int32_t L_865 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_864>>((int32_t)16))))));
		uint32_t L_866 = (L_863)->GetAt(static_cast<il2cpp_array_size_t>(L_865));
		UInt32U5BU5D_t2770800703* L_867 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_868 = V_0;
		NullCheck(L_867);
		int32_t L_869 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_868>>8)))));
		uint32_t L_870 = (L_867)->GetAt(static_cast<il2cpp_array_size_t>(L_869));
		UInt32U5BU5D_t2770800703* L_871 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_872 = V_3;
		NullCheck(L_871);
		int32_t L_873 = (((int32_t)((uint8_t)L_872)));
		uint32_t L_874 = (L_871)->GetAt(static_cast<il2cpp_array_size_t>(L_873));
		UInt32U5BU5D_t2770800703* L_875 = ___ekey2;
		NullCheck(L_875);
		int32_t L_876 = ((int32_t)46);
		uint32_t L_877 = (L_875)->GetAt(static_cast<il2cpp_array_size_t>(L_876));
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_862^(int32_t)L_866))^(int32_t)L_870))^(int32_t)L_874))^(int32_t)L_877));
		UInt32U5BU5D_t2770800703* L_878 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_879 = V_3;
		NullCheck(L_878);
		uintptr_t L_880 = (((uintptr_t)((int32_t)((uint32_t)L_879>>((int32_t)24)))));
		uint32_t L_881 = (L_878)->GetAt(static_cast<il2cpp_array_size_t>(L_880));
		UInt32U5BU5D_t2770800703* L_882 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_883 = V_2;
		NullCheck(L_882);
		int32_t L_884 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_883>>((int32_t)16))))));
		uint32_t L_885 = (L_882)->GetAt(static_cast<il2cpp_array_size_t>(L_884));
		UInt32U5BU5D_t2770800703* L_886 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_887 = V_1;
		NullCheck(L_886);
		int32_t L_888 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_887>>8)))));
		uint32_t L_889 = (L_886)->GetAt(static_cast<il2cpp_array_size_t>(L_888));
		UInt32U5BU5D_t2770800703* L_890 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_891 = V_0;
		NullCheck(L_890);
		int32_t L_892 = (((int32_t)((uint8_t)L_891)));
		uint32_t L_893 = (L_890)->GetAt(static_cast<il2cpp_array_size_t>(L_892));
		UInt32U5BU5D_t2770800703* L_894 = ___ekey2;
		NullCheck(L_894);
		int32_t L_895 = ((int32_t)47);
		uint32_t L_896 = (L_894)->GetAt(static_cast<il2cpp_array_size_t>(L_895));
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_881^(int32_t)L_885))^(int32_t)L_889))^(int32_t)L_893))^(int32_t)L_896));
		V_8 = ((int32_t)48);
		int32_t L_897 = __this->get_Nr_14();
		if ((((int32_t)L_897) <= ((int32_t)((int32_t)12))))
		{
			goto IL_0b08;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AesTransform_t2957123611_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t2770800703* L_898 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_899 = V_4;
		NullCheck(L_898);
		uintptr_t L_900 = (((uintptr_t)((int32_t)((uint32_t)L_899>>((int32_t)24)))));
		uint32_t L_901 = (L_898)->GetAt(static_cast<il2cpp_array_size_t>(L_900));
		UInt32U5BU5D_t2770800703* L_902 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_903 = V_7;
		NullCheck(L_902);
		int32_t L_904 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_903>>((int32_t)16))))));
		uint32_t L_905 = (L_902)->GetAt(static_cast<il2cpp_array_size_t>(L_904));
		UInt32U5BU5D_t2770800703* L_906 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_907 = V_6;
		NullCheck(L_906);
		int32_t L_908 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_907>>8)))));
		uint32_t L_909 = (L_906)->GetAt(static_cast<il2cpp_array_size_t>(L_908));
		UInt32U5BU5D_t2770800703* L_910 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_911 = V_5;
		NullCheck(L_910);
		int32_t L_912 = (((int32_t)((uint8_t)L_911)));
		uint32_t L_913 = (L_910)->GetAt(static_cast<il2cpp_array_size_t>(L_912));
		UInt32U5BU5D_t2770800703* L_914 = ___ekey2;
		NullCheck(L_914);
		int32_t L_915 = ((int32_t)48);
		uint32_t L_916 = (L_914)->GetAt(static_cast<il2cpp_array_size_t>(L_915));
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_901^(int32_t)L_905))^(int32_t)L_909))^(int32_t)L_913))^(int32_t)L_916));
		UInt32U5BU5D_t2770800703* L_917 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_918 = V_5;
		NullCheck(L_917);
		uintptr_t L_919 = (((uintptr_t)((int32_t)((uint32_t)L_918>>((int32_t)24)))));
		uint32_t L_920 = (L_917)->GetAt(static_cast<il2cpp_array_size_t>(L_919));
		UInt32U5BU5D_t2770800703* L_921 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_922 = V_4;
		NullCheck(L_921);
		int32_t L_923 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_922>>((int32_t)16))))));
		uint32_t L_924 = (L_921)->GetAt(static_cast<il2cpp_array_size_t>(L_923));
		UInt32U5BU5D_t2770800703* L_925 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_926 = V_7;
		NullCheck(L_925);
		int32_t L_927 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_926>>8)))));
		uint32_t L_928 = (L_925)->GetAt(static_cast<il2cpp_array_size_t>(L_927));
		UInt32U5BU5D_t2770800703* L_929 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_930 = V_6;
		NullCheck(L_929);
		int32_t L_931 = (((int32_t)((uint8_t)L_930)));
		uint32_t L_932 = (L_929)->GetAt(static_cast<il2cpp_array_size_t>(L_931));
		UInt32U5BU5D_t2770800703* L_933 = ___ekey2;
		NullCheck(L_933);
		int32_t L_934 = ((int32_t)49);
		uint32_t L_935 = (L_933)->GetAt(static_cast<il2cpp_array_size_t>(L_934));
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_920^(int32_t)L_924))^(int32_t)L_928))^(int32_t)L_932))^(int32_t)L_935));
		UInt32U5BU5D_t2770800703* L_936 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_937 = V_6;
		NullCheck(L_936);
		uintptr_t L_938 = (((uintptr_t)((int32_t)((uint32_t)L_937>>((int32_t)24)))));
		uint32_t L_939 = (L_936)->GetAt(static_cast<il2cpp_array_size_t>(L_938));
		UInt32U5BU5D_t2770800703* L_940 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_941 = V_5;
		NullCheck(L_940);
		int32_t L_942 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_941>>((int32_t)16))))));
		uint32_t L_943 = (L_940)->GetAt(static_cast<il2cpp_array_size_t>(L_942));
		UInt32U5BU5D_t2770800703* L_944 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_945 = V_4;
		NullCheck(L_944);
		int32_t L_946 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_945>>8)))));
		uint32_t L_947 = (L_944)->GetAt(static_cast<il2cpp_array_size_t>(L_946));
		UInt32U5BU5D_t2770800703* L_948 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_949 = V_7;
		NullCheck(L_948);
		int32_t L_950 = (((int32_t)((uint8_t)L_949)));
		uint32_t L_951 = (L_948)->GetAt(static_cast<il2cpp_array_size_t>(L_950));
		UInt32U5BU5D_t2770800703* L_952 = ___ekey2;
		NullCheck(L_952);
		int32_t L_953 = ((int32_t)50);
		uint32_t L_954 = (L_952)->GetAt(static_cast<il2cpp_array_size_t>(L_953));
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_939^(int32_t)L_943))^(int32_t)L_947))^(int32_t)L_951))^(int32_t)L_954));
		UInt32U5BU5D_t2770800703* L_955 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_956 = V_7;
		NullCheck(L_955);
		uintptr_t L_957 = (((uintptr_t)((int32_t)((uint32_t)L_956>>((int32_t)24)))));
		uint32_t L_958 = (L_955)->GetAt(static_cast<il2cpp_array_size_t>(L_957));
		UInt32U5BU5D_t2770800703* L_959 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_960 = V_6;
		NullCheck(L_959);
		int32_t L_961 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_960>>((int32_t)16))))));
		uint32_t L_962 = (L_959)->GetAt(static_cast<il2cpp_array_size_t>(L_961));
		UInt32U5BU5D_t2770800703* L_963 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_964 = V_5;
		NullCheck(L_963);
		int32_t L_965 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_964>>8)))));
		uint32_t L_966 = (L_963)->GetAt(static_cast<il2cpp_array_size_t>(L_965));
		UInt32U5BU5D_t2770800703* L_967 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_968 = V_4;
		NullCheck(L_967);
		int32_t L_969 = (((int32_t)((uint8_t)L_968)));
		uint32_t L_970 = (L_967)->GetAt(static_cast<il2cpp_array_size_t>(L_969));
		UInt32U5BU5D_t2770800703* L_971 = ___ekey2;
		NullCheck(L_971);
		int32_t L_972 = ((int32_t)51);
		uint32_t L_973 = (L_971)->GetAt(static_cast<il2cpp_array_size_t>(L_972));
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_958^(int32_t)L_962))^(int32_t)L_966))^(int32_t)L_970))^(int32_t)L_973));
		UInt32U5BU5D_t2770800703* L_974 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_975 = V_0;
		NullCheck(L_974);
		uintptr_t L_976 = (((uintptr_t)((int32_t)((uint32_t)L_975>>((int32_t)24)))));
		uint32_t L_977 = (L_974)->GetAt(static_cast<il2cpp_array_size_t>(L_976));
		UInt32U5BU5D_t2770800703* L_978 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_979 = V_3;
		NullCheck(L_978);
		int32_t L_980 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_979>>((int32_t)16))))));
		uint32_t L_981 = (L_978)->GetAt(static_cast<il2cpp_array_size_t>(L_980));
		UInt32U5BU5D_t2770800703* L_982 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_983 = V_2;
		NullCheck(L_982);
		int32_t L_984 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_983>>8)))));
		uint32_t L_985 = (L_982)->GetAt(static_cast<il2cpp_array_size_t>(L_984));
		UInt32U5BU5D_t2770800703* L_986 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_987 = V_1;
		NullCheck(L_986);
		int32_t L_988 = (((int32_t)((uint8_t)L_987)));
		uint32_t L_989 = (L_986)->GetAt(static_cast<il2cpp_array_size_t>(L_988));
		UInt32U5BU5D_t2770800703* L_990 = ___ekey2;
		NullCheck(L_990);
		int32_t L_991 = ((int32_t)52);
		uint32_t L_992 = (L_990)->GetAt(static_cast<il2cpp_array_size_t>(L_991));
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_977^(int32_t)L_981))^(int32_t)L_985))^(int32_t)L_989))^(int32_t)L_992));
		UInt32U5BU5D_t2770800703* L_993 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_994 = V_1;
		NullCheck(L_993);
		uintptr_t L_995 = (((uintptr_t)((int32_t)((uint32_t)L_994>>((int32_t)24)))));
		uint32_t L_996 = (L_993)->GetAt(static_cast<il2cpp_array_size_t>(L_995));
		UInt32U5BU5D_t2770800703* L_997 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_998 = V_0;
		NullCheck(L_997);
		int32_t L_999 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_998>>((int32_t)16))))));
		uint32_t L_1000 = (L_997)->GetAt(static_cast<il2cpp_array_size_t>(L_999));
		UInt32U5BU5D_t2770800703* L_1001 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_1002 = V_3;
		NullCheck(L_1001);
		int32_t L_1003 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_1002>>8)))));
		uint32_t L_1004 = (L_1001)->GetAt(static_cast<il2cpp_array_size_t>(L_1003));
		UInt32U5BU5D_t2770800703* L_1005 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_1006 = V_2;
		NullCheck(L_1005);
		int32_t L_1007 = (((int32_t)((uint8_t)L_1006)));
		uint32_t L_1008 = (L_1005)->GetAt(static_cast<il2cpp_array_size_t>(L_1007));
		UInt32U5BU5D_t2770800703* L_1009 = ___ekey2;
		NullCheck(L_1009);
		int32_t L_1010 = ((int32_t)53);
		uint32_t L_1011 = (L_1009)->GetAt(static_cast<il2cpp_array_size_t>(L_1010));
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_996^(int32_t)L_1000))^(int32_t)L_1004))^(int32_t)L_1008))^(int32_t)L_1011));
		UInt32U5BU5D_t2770800703* L_1012 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_1013 = V_2;
		NullCheck(L_1012);
		uintptr_t L_1014 = (((uintptr_t)((int32_t)((uint32_t)L_1013>>((int32_t)24)))));
		uint32_t L_1015 = (L_1012)->GetAt(static_cast<il2cpp_array_size_t>(L_1014));
		UInt32U5BU5D_t2770800703* L_1016 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_1017 = V_1;
		NullCheck(L_1016);
		int32_t L_1018 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_1017>>((int32_t)16))))));
		uint32_t L_1019 = (L_1016)->GetAt(static_cast<il2cpp_array_size_t>(L_1018));
		UInt32U5BU5D_t2770800703* L_1020 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_1021 = V_0;
		NullCheck(L_1020);
		int32_t L_1022 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_1021>>8)))));
		uint32_t L_1023 = (L_1020)->GetAt(static_cast<il2cpp_array_size_t>(L_1022));
		UInt32U5BU5D_t2770800703* L_1024 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_1025 = V_3;
		NullCheck(L_1024);
		int32_t L_1026 = (((int32_t)((uint8_t)L_1025)));
		uint32_t L_1027 = (L_1024)->GetAt(static_cast<il2cpp_array_size_t>(L_1026));
		UInt32U5BU5D_t2770800703* L_1028 = ___ekey2;
		NullCheck(L_1028);
		int32_t L_1029 = ((int32_t)54);
		uint32_t L_1030 = (L_1028)->GetAt(static_cast<il2cpp_array_size_t>(L_1029));
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1015^(int32_t)L_1019))^(int32_t)L_1023))^(int32_t)L_1027))^(int32_t)L_1030));
		UInt32U5BU5D_t2770800703* L_1031 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_1032 = V_3;
		NullCheck(L_1031);
		uintptr_t L_1033 = (((uintptr_t)((int32_t)((uint32_t)L_1032>>((int32_t)24)))));
		uint32_t L_1034 = (L_1031)->GetAt(static_cast<il2cpp_array_size_t>(L_1033));
		UInt32U5BU5D_t2770800703* L_1035 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_1036 = V_2;
		NullCheck(L_1035);
		int32_t L_1037 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_1036>>((int32_t)16))))));
		uint32_t L_1038 = (L_1035)->GetAt(static_cast<il2cpp_array_size_t>(L_1037));
		UInt32U5BU5D_t2770800703* L_1039 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_1040 = V_1;
		NullCheck(L_1039);
		int32_t L_1041 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_1040>>8)))));
		uint32_t L_1042 = (L_1039)->GetAt(static_cast<il2cpp_array_size_t>(L_1041));
		UInt32U5BU5D_t2770800703* L_1043 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_1044 = V_0;
		NullCheck(L_1043);
		int32_t L_1045 = (((int32_t)((uint8_t)L_1044)));
		uint32_t L_1046 = (L_1043)->GetAt(static_cast<il2cpp_array_size_t>(L_1045));
		UInt32U5BU5D_t2770800703* L_1047 = ___ekey2;
		NullCheck(L_1047);
		int32_t L_1048 = ((int32_t)55);
		uint32_t L_1049 = (L_1047)->GetAt(static_cast<il2cpp_array_size_t>(L_1048));
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1034^(int32_t)L_1038))^(int32_t)L_1042))^(int32_t)L_1046))^(int32_t)L_1049));
		V_8 = ((int32_t)56);
	}

IL_0b08:
	{
		ByteU5BU5D_t4116647657* L_1050 = ___outdata1;
		IL2CPP_RUNTIME_CLASS_INIT(AesTransform_t2957123611_il2cpp_TypeInfo_var);
		ByteU5BU5D_t4116647657* L_1051 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_1052 = V_4;
		NullCheck(L_1051);
		uintptr_t L_1053 = (((uintptr_t)((int32_t)((uint32_t)L_1052>>((int32_t)24)))));
		uint8_t L_1054 = (L_1051)->GetAt(static_cast<il2cpp_array_size_t>(L_1053));
		UInt32U5BU5D_t2770800703* L_1055 = ___ekey2;
		int32_t L_1056 = V_8;
		NullCheck(L_1055);
		int32_t L_1057 = L_1056;
		uint32_t L_1058 = (L_1055)->GetAt(static_cast<il2cpp_array_size_t>(L_1057));
		NullCheck(L_1050);
		(L_1050)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1054^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_1058>>((int32_t)24))))))))))));
		ByteU5BU5D_t4116647657* L_1059 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1060 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_1061 = V_7;
		NullCheck(L_1060);
		int32_t L_1062 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_1061>>((int32_t)16))))));
		uint8_t L_1063 = (L_1060)->GetAt(static_cast<il2cpp_array_size_t>(L_1062));
		UInt32U5BU5D_t2770800703* L_1064 = ___ekey2;
		int32_t L_1065 = V_8;
		NullCheck(L_1064);
		int32_t L_1066 = L_1065;
		uint32_t L_1067 = (L_1064)->GetAt(static_cast<il2cpp_array_size_t>(L_1066));
		NullCheck(L_1059);
		(L_1059)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1063^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_1067>>((int32_t)16))))))))))));
		ByteU5BU5D_t4116647657* L_1068 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1069 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_1070 = V_6;
		NullCheck(L_1069);
		int32_t L_1071 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_1070>>8)))));
		uint8_t L_1072 = (L_1069)->GetAt(static_cast<il2cpp_array_size_t>(L_1071));
		UInt32U5BU5D_t2770800703* L_1073 = ___ekey2;
		int32_t L_1074 = V_8;
		NullCheck(L_1073);
		int32_t L_1075 = L_1074;
		uint32_t L_1076 = (L_1073)->GetAt(static_cast<il2cpp_array_size_t>(L_1075));
		NullCheck(L_1068);
		(L_1068)->SetAt(static_cast<il2cpp_array_size_t>(2), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1072^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_1076>>8)))))))))));
		ByteU5BU5D_t4116647657* L_1077 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1078 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_1079 = V_5;
		NullCheck(L_1078);
		int32_t L_1080 = (((int32_t)((uint8_t)L_1079)));
		uint8_t L_1081 = (L_1078)->GetAt(static_cast<il2cpp_array_size_t>(L_1080));
		UInt32U5BU5D_t2770800703* L_1082 = ___ekey2;
		int32_t L_1083 = V_8;
		int32_t L_1084 = L_1083;
		V_8 = ((int32_t)((int32_t)L_1084+(int32_t)1));
		NullCheck(L_1082);
		int32_t L_1085 = L_1084;
		uint32_t L_1086 = (L_1082)->GetAt(static_cast<il2cpp_array_size_t>(L_1085));
		NullCheck(L_1077);
		(L_1077)->SetAt(static_cast<il2cpp_array_size_t>(3), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1081^(int32_t)(((int32_t)((uint8_t)L_1086)))))))));
		ByteU5BU5D_t4116647657* L_1087 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1088 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_1089 = V_5;
		NullCheck(L_1088);
		uintptr_t L_1090 = (((uintptr_t)((int32_t)((uint32_t)L_1089>>((int32_t)24)))));
		uint8_t L_1091 = (L_1088)->GetAt(static_cast<il2cpp_array_size_t>(L_1090));
		UInt32U5BU5D_t2770800703* L_1092 = ___ekey2;
		int32_t L_1093 = V_8;
		NullCheck(L_1092);
		int32_t L_1094 = L_1093;
		uint32_t L_1095 = (L_1092)->GetAt(static_cast<il2cpp_array_size_t>(L_1094));
		NullCheck(L_1087);
		(L_1087)->SetAt(static_cast<il2cpp_array_size_t>(4), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1091^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_1095>>((int32_t)24))))))))))));
		ByteU5BU5D_t4116647657* L_1096 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1097 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_1098 = V_4;
		NullCheck(L_1097);
		int32_t L_1099 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_1098>>((int32_t)16))))));
		uint8_t L_1100 = (L_1097)->GetAt(static_cast<il2cpp_array_size_t>(L_1099));
		UInt32U5BU5D_t2770800703* L_1101 = ___ekey2;
		int32_t L_1102 = V_8;
		NullCheck(L_1101);
		int32_t L_1103 = L_1102;
		uint32_t L_1104 = (L_1101)->GetAt(static_cast<il2cpp_array_size_t>(L_1103));
		NullCheck(L_1096);
		(L_1096)->SetAt(static_cast<il2cpp_array_size_t>(5), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1100^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_1104>>((int32_t)16))))))))))));
		ByteU5BU5D_t4116647657* L_1105 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1106 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_1107 = V_7;
		NullCheck(L_1106);
		int32_t L_1108 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_1107>>8)))));
		uint8_t L_1109 = (L_1106)->GetAt(static_cast<il2cpp_array_size_t>(L_1108));
		UInt32U5BU5D_t2770800703* L_1110 = ___ekey2;
		int32_t L_1111 = V_8;
		NullCheck(L_1110);
		int32_t L_1112 = L_1111;
		uint32_t L_1113 = (L_1110)->GetAt(static_cast<il2cpp_array_size_t>(L_1112));
		NullCheck(L_1105);
		(L_1105)->SetAt(static_cast<il2cpp_array_size_t>(6), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1109^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_1113>>8)))))))))));
		ByteU5BU5D_t4116647657* L_1114 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1115 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_1116 = V_6;
		NullCheck(L_1115);
		int32_t L_1117 = (((int32_t)((uint8_t)L_1116)));
		uint8_t L_1118 = (L_1115)->GetAt(static_cast<il2cpp_array_size_t>(L_1117));
		UInt32U5BU5D_t2770800703* L_1119 = ___ekey2;
		int32_t L_1120 = V_8;
		int32_t L_1121 = L_1120;
		V_8 = ((int32_t)((int32_t)L_1121+(int32_t)1));
		NullCheck(L_1119);
		int32_t L_1122 = L_1121;
		uint32_t L_1123 = (L_1119)->GetAt(static_cast<il2cpp_array_size_t>(L_1122));
		NullCheck(L_1114);
		(L_1114)->SetAt(static_cast<il2cpp_array_size_t>(7), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1118^(int32_t)(((int32_t)((uint8_t)L_1123)))))))));
		ByteU5BU5D_t4116647657* L_1124 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1125 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_1126 = V_6;
		NullCheck(L_1125);
		uintptr_t L_1127 = (((uintptr_t)((int32_t)((uint32_t)L_1126>>((int32_t)24)))));
		uint8_t L_1128 = (L_1125)->GetAt(static_cast<il2cpp_array_size_t>(L_1127));
		UInt32U5BU5D_t2770800703* L_1129 = ___ekey2;
		int32_t L_1130 = V_8;
		NullCheck(L_1129);
		int32_t L_1131 = L_1130;
		uint32_t L_1132 = (L_1129)->GetAt(static_cast<il2cpp_array_size_t>(L_1131));
		NullCheck(L_1124);
		(L_1124)->SetAt(static_cast<il2cpp_array_size_t>(8), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1128^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_1132>>((int32_t)24))))))))))));
		ByteU5BU5D_t4116647657* L_1133 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1134 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_1135 = V_5;
		NullCheck(L_1134);
		int32_t L_1136 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_1135>>((int32_t)16))))));
		uint8_t L_1137 = (L_1134)->GetAt(static_cast<il2cpp_array_size_t>(L_1136));
		UInt32U5BU5D_t2770800703* L_1138 = ___ekey2;
		int32_t L_1139 = V_8;
		NullCheck(L_1138);
		int32_t L_1140 = L_1139;
		uint32_t L_1141 = (L_1138)->GetAt(static_cast<il2cpp_array_size_t>(L_1140));
		NullCheck(L_1133);
		(L_1133)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1137^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_1141>>((int32_t)16))))))))))));
		ByteU5BU5D_t4116647657* L_1142 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1143 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_1144 = V_4;
		NullCheck(L_1143);
		int32_t L_1145 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_1144>>8)))));
		uint8_t L_1146 = (L_1143)->GetAt(static_cast<il2cpp_array_size_t>(L_1145));
		UInt32U5BU5D_t2770800703* L_1147 = ___ekey2;
		int32_t L_1148 = V_8;
		NullCheck(L_1147);
		int32_t L_1149 = L_1148;
		uint32_t L_1150 = (L_1147)->GetAt(static_cast<il2cpp_array_size_t>(L_1149));
		NullCheck(L_1142);
		(L_1142)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1146^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_1150>>8)))))))))));
		ByteU5BU5D_t4116647657* L_1151 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1152 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_1153 = V_7;
		NullCheck(L_1152);
		int32_t L_1154 = (((int32_t)((uint8_t)L_1153)));
		uint8_t L_1155 = (L_1152)->GetAt(static_cast<il2cpp_array_size_t>(L_1154));
		UInt32U5BU5D_t2770800703* L_1156 = ___ekey2;
		int32_t L_1157 = V_8;
		int32_t L_1158 = L_1157;
		V_8 = ((int32_t)((int32_t)L_1158+(int32_t)1));
		NullCheck(L_1156);
		int32_t L_1159 = L_1158;
		uint32_t L_1160 = (L_1156)->GetAt(static_cast<il2cpp_array_size_t>(L_1159));
		NullCheck(L_1151);
		(L_1151)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1155^(int32_t)(((int32_t)((uint8_t)L_1160)))))))));
		ByteU5BU5D_t4116647657* L_1161 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1162 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_1163 = V_7;
		NullCheck(L_1162);
		uintptr_t L_1164 = (((uintptr_t)((int32_t)((uint32_t)L_1163>>((int32_t)24)))));
		uint8_t L_1165 = (L_1162)->GetAt(static_cast<il2cpp_array_size_t>(L_1164));
		UInt32U5BU5D_t2770800703* L_1166 = ___ekey2;
		int32_t L_1167 = V_8;
		NullCheck(L_1166);
		int32_t L_1168 = L_1167;
		uint32_t L_1169 = (L_1166)->GetAt(static_cast<il2cpp_array_size_t>(L_1168));
		NullCheck(L_1161);
		(L_1161)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1165^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_1169>>((int32_t)24))))))))))));
		ByteU5BU5D_t4116647657* L_1170 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1171 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_1172 = V_6;
		NullCheck(L_1171);
		int32_t L_1173 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_1172>>((int32_t)16))))));
		uint8_t L_1174 = (L_1171)->GetAt(static_cast<il2cpp_array_size_t>(L_1173));
		UInt32U5BU5D_t2770800703* L_1175 = ___ekey2;
		int32_t L_1176 = V_8;
		NullCheck(L_1175);
		int32_t L_1177 = L_1176;
		uint32_t L_1178 = (L_1175)->GetAt(static_cast<il2cpp_array_size_t>(L_1177));
		NullCheck(L_1170);
		(L_1170)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1174^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_1178>>((int32_t)16))))))))))));
		ByteU5BU5D_t4116647657* L_1179 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1180 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_1181 = V_5;
		NullCheck(L_1180);
		int32_t L_1182 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_1181>>8)))));
		uint8_t L_1183 = (L_1180)->GetAt(static_cast<il2cpp_array_size_t>(L_1182));
		UInt32U5BU5D_t2770800703* L_1184 = ___ekey2;
		int32_t L_1185 = V_8;
		NullCheck(L_1184);
		int32_t L_1186 = L_1185;
		uint32_t L_1187 = (L_1184)->GetAt(static_cast<il2cpp_array_size_t>(L_1186));
		NullCheck(L_1179);
		(L_1179)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1183^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_1187>>8)))))))))));
		ByteU5BU5D_t4116647657* L_1188 = ___outdata1;
		ByteU5BU5D_t4116647657* L_1189 = ((AesTransform_t2957123611_StaticFields*)AesTransform_t2957123611_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_1190 = V_4;
		NullCheck(L_1189);
		int32_t L_1191 = (((int32_t)((uint8_t)L_1190)));
		uint8_t L_1192 = (L_1189)->GetAt(static_cast<il2cpp_array_size_t>(L_1191));
		UInt32U5BU5D_t2770800703* L_1193 = ___ekey2;
		int32_t L_1194 = V_8;
		int32_t L_1195 = L_1194;
		V_8 = ((int32_t)((int32_t)L_1195+(int32_t)1));
		NullCheck(L_1193);
		int32_t L_1196 = L_1195;
		uint32_t L_1197 = (L_1193)->GetAt(static_cast<il2cpp_array_size_t>(L_1196));
		NullCheck(L_1188);
		(L_1188)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1192^(int32_t)(((int32_t)((uint8_t)L_1197)))))))));
		return;
	}
}
// System.Void System.Threading.LockRecursionException::.ctor()
extern "C"  void LockRecursionException__ctor_m338981005 (LockRecursionException_t2413788283 * __this, const MethodInfo* method)
{
	{
		Exception__ctor_m213470898(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.LockRecursionException::.ctor(System.String)
extern "C"  void LockRecursionException__ctor_m1065084584 (LockRecursionException_t2413788283 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception__ctor_m1152696503(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.LockRecursionException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void LockRecursionException__ctor_m1257962800 (LockRecursionException_t2413788283 * __this, SerializationInfo_t950877179 * ___info0, StreamingContext_t3711869237  ___sc1, const MethodInfo* method)
{
	{
		SerializationInfo_t950877179 * L_0 = ___info0;
		StreamingContext_t3711869237  L_1 = ___sc1;
		Exception__ctor_m2499432361(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.ReaderWriterLockSlim::.ctor()
extern "C"  void ReaderWriterLockSlim__ctor_m557461640 (ReaderWriterLockSlim_t1938317233 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReaderWriterLockSlim__ctor_m557461640_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_read_locks_11(((LockDetailsU5BU5D_t1708220258*)SZArrayNew(LockDetailsU5BU5D_t1708220258_il2cpp_TypeInfo_var, (uint32_t)8)));
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.ReaderWriterLockSlim::.cctor()
extern "C"  void ReaderWriterLockSlim__cctor_m2534870346 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReaderWriterLockSlim__cctor_m2534870346_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = Environment_get_ProcessorCount_m3616251798(NULL /*static, unused*/, /*hidden argument*/NULL);
		((ReaderWriterLockSlim_t1938317233_StaticFields*)ReaderWriterLockSlim_t1938317233_il2cpp_TypeInfo_var->static_fields)->set_smp_0((bool)((((int32_t)L_0) > ((int32_t)1))? 1 : 0));
		return;
	}
}
// System.Void System.Threading.ReaderWriterLockSlim::EnterReadLock()
extern "C"  void ReaderWriterLockSlim_EnterReadLock_m1572181045 (ReaderWriterLockSlim_t1938317233 * __this, const MethodInfo* method)
{
	{
		ReaderWriterLockSlim_TryEnterReadLock_m580519220(__this, (-1), /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Threading.ReaderWriterLockSlim::TryEnterReadLock(System.Int32)
extern "C"  bool ReaderWriterLockSlim_TryEnterReadLock_m580519220 (ReaderWriterLockSlim_t1938317233 * __this, int32_t ___millisecondsTimeout0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReaderWriterLockSlim_TryEnterReadLock_m580519220_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LockDetails_t2068764019 * V_0 = NULL;
	{
		int32_t L_0 = ___millisecondsTimeout0;
		if ((((int32_t)L_0) >= ((int32_t)(-1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_1 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_1, _stringLiteral3648076086, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		LockDetailsU5BU5D_t1708220258* L_2 = __this->get_read_locks_11();
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		ObjectDisposedException_t21392786 * L_3 = (ObjectDisposedException_t21392786 *)il2cpp_codegen_object_new(ObjectDisposedException_t21392786_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3603759869(L_3, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t2300836069_il2cpp_TypeInfo_var);
		Thread_t2300836069 * L_4 = Thread_get_CurrentThread_m4142136012(NULL /*static, unused*/, /*hidden argument*/NULL);
		Thread_t2300836069 * L_5 = __this->get_write_thread_4();
		if ((!(((Il2CppObject*)(Thread_t2300836069 *)L_4) == ((Il2CppObject*)(Thread_t2300836069 *)L_5))))
		{
			goto IL_003f;
		}
	}
	{
		LockRecursionException_t2413788283 * L_6 = (LockRecursionException_t2413788283 *)il2cpp_codegen_object_new(LockRecursionException_t2413788283_il2cpp_TypeInfo_var);
		LockRecursionException__ctor_m1065084584(L_6, _stringLiteral4187398550, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_003f:
	{
		ReaderWriterLockSlim_EnterMyLock_m3975449768(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t2300836069_il2cpp_TypeInfo_var);
		Thread_t2300836069 * L_7 = Thread_get_CurrentThread_m4142136012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_8 = Thread_get_ManagedThreadId_m1068113671(L_7, /*hidden argument*/NULL);
		LockDetails_t2068764019 * L_9 = ReaderWriterLockSlim_GetReadLockDetails_m468803701(__this, L_8, (bool)1, /*hidden argument*/NULL);
		V_0 = L_9;
		LockDetails_t2068764019 * L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = L_10->get_ReadLocks_1();
		if (!L_11)
		{
			goto IL_0073;
		}
	}
	{
		ReaderWriterLockSlim_ExitMyLock_m2519986703(__this, /*hidden argument*/NULL);
		LockRecursionException_t2413788283 * L_12 = (LockRecursionException_t2413788283 *)il2cpp_codegen_object_new(LockRecursionException_t2413788283_il2cpp_TypeInfo_var);
		LockRecursionException__ctor_m1065084584(L_12, _stringLiteral2920290401, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_0073:
	{
		LockDetails_t2068764019 * L_13 = V_0;
		LockDetails_t2068764019 * L_14 = L_13;
		NullCheck(L_14);
		int32_t L_15 = L_14->get_ReadLocks_1();
		NullCheck(L_14);
		L_14->set_ReadLocks_1(((int32_t)((int32_t)L_15+(int32_t)1)));
	}

IL_0081:
	{
		int32_t L_16 = __this->get_owners_2();
		if ((((int32_t)L_16) < ((int32_t)0)))
		{
			goto IL_00ab;
		}
	}
	{
		uint32_t L_17 = __this->get_numWriteWaiters_5();
		if (L_17)
		{
			goto IL_00ab;
		}
	}
	{
		int32_t L_18 = __this->get_owners_2();
		__this->set_owners_2(((int32_t)((int32_t)L_18+(int32_t)1)));
		goto IL_00f5;
	}

IL_00ab:
	{
		int32_t L_19 = ___millisecondsTimeout0;
		if (L_19)
		{
			goto IL_00b9;
		}
	}
	{
		ReaderWriterLockSlim_ExitMyLock_m2519986703(__this, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_00b9:
	{
		EventWaitHandle_t777845177 * L_20 = __this->get_readEvent_9();
		if (L_20)
		{
			goto IL_00d6;
		}
	}
	{
		EventWaitHandle_t777845177 ** L_21 = __this->get_address_of_readEvent_9();
		ReaderWriterLockSlim_LazyCreateEvent_m3565271370(__this, L_21, (bool)0, /*hidden argument*/NULL);
		goto IL_0081;
	}

IL_00d6:
	{
		EventWaitHandle_t777845177 * L_22 = __this->get_readEvent_9();
		uint32_t* L_23 = __this->get_address_of_numReadWaiters_6();
		int32_t L_24 = ___millisecondsTimeout0;
		bool L_25 = ReaderWriterLockSlim_WaitOnEvent_m2536988665(__this, L_22, L_23, L_24, /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_00f0;
		}
	}
	{
		return (bool)0;
	}

IL_00f0:
	{
		goto IL_0081;
	}

IL_00f5:
	{
		ReaderWriterLockSlim_ExitMyLock_m2519986703(__this, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Void System.Threading.ReaderWriterLockSlim::ExitReadLock()
extern "C"  void ReaderWriterLockSlim_ExitReadLock_m336606214 (ReaderWriterLockSlim_t1938317233 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReaderWriterLockSlim_ExitReadLock_m336606214_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ReaderWriterLockSlim_EnterMyLock_m3975449768(__this, /*hidden argument*/NULL);
		int32_t L_0 = __this->get_owners_2();
		if ((((int32_t)L_0) >= ((int32_t)1)))
		{
			goto IL_0023;
		}
	}
	{
		ReaderWriterLockSlim_ExitMyLock_m2519986703(__this, /*hidden argument*/NULL);
		SynchronizationLockException_t841761767 * L_1 = (SynchronizationLockException_t841761767 *)il2cpp_codegen_object_new(SynchronizationLockException_t841761767_il2cpp_TypeInfo_var);
		SynchronizationLockException__ctor_m3407855920(L_1, _stringLiteral4023828656, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0023:
	{
		int32_t L_2 = __this->get_owners_2();
		__this->set_owners_2(((int32_t)((int32_t)L_2-(int32_t)1)));
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t2300836069_il2cpp_TypeInfo_var);
		Thread_t2300836069 * L_3 = Thread_get_CurrentThread_m4142136012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = Thread_get_ManagedThreadId_m1068113671(L_3, /*hidden argument*/NULL);
		LockDetails_t2068764019 * L_5 = ReaderWriterLockSlim_GetReadLockDetails_m468803701(__this, L_4, (bool)0, /*hidden argument*/NULL);
		LockDetails_t2068764019 * L_6 = L_5;
		NullCheck(L_6);
		int32_t L_7 = L_6->get_ReadLocks_1();
		NullCheck(L_6);
		L_6->set_ReadLocks_1(((int32_t)((int32_t)L_7-(int32_t)1)));
		ReaderWriterLockSlim_ExitAndWakeUpAppropriateWaiters_m3940164111(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.ReaderWriterLockSlim::EnterWriteLock()
extern "C"  void ReaderWriterLockSlim_EnterWriteLock_m1169059049 (ReaderWriterLockSlim_t1938317233 * __this, const MethodInfo* method)
{
	{
		ReaderWriterLockSlim_TryEnterWriteLock_m4237072678(__this, (-1), /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Threading.ReaderWriterLockSlim::TryEnterWriteLock(System.Int32)
extern "C"  bool ReaderWriterLockSlim_TryEnterWriteLock_m4237072678 (ReaderWriterLockSlim_t1938317233 * __this, int32_t ___millisecondsTimeout0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReaderWriterLockSlim_TryEnterWriteLock_m4237072678_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LockDetails_t2068764019 * V_0 = NULL;
	{
		int32_t L_0 = ___millisecondsTimeout0;
		if ((((int32_t)L_0) >= ((int32_t)(-1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_1 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_1, _stringLiteral3648076086, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		LockDetailsU5BU5D_t1708220258* L_2 = __this->get_read_locks_11();
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		ObjectDisposedException_t21392786 * L_3 = (ObjectDisposedException_t21392786 *)il2cpp_codegen_object_new(ObjectDisposedException_t21392786_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3603759869(L_3, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0024:
	{
		bool L_4 = ReaderWriterLockSlim_get_IsWriteLockHeld_m3969852364(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0035;
		}
	}
	{
		LockRecursionException_t2413788283 * L_5 = (LockRecursionException_t2413788283 *)il2cpp_codegen_object_new(LockRecursionException_t2413788283_il2cpp_TypeInfo_var);
		LockRecursionException__ctor_m338981005(L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0035:
	{
		ReaderWriterLockSlim_EnterMyLock_m3975449768(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t2300836069_il2cpp_TypeInfo_var);
		Thread_t2300836069 * L_6 = Thread_get_CurrentThread_m4142136012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = Thread_get_ManagedThreadId_m1068113671(L_6, /*hidden argument*/NULL);
		LockDetails_t2068764019 * L_8 = ReaderWriterLockSlim_GetReadLockDetails_m468803701(__this, L_7, (bool)0, /*hidden argument*/NULL);
		V_0 = L_8;
		LockDetails_t2068764019 * L_9 = V_0;
		if (!L_9)
		{
			goto IL_0070;
		}
	}
	{
		LockDetails_t2068764019 * L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = L_10->get_ReadLocks_1();
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		ReaderWriterLockSlim_ExitMyLock_m2519986703(__this, /*hidden argument*/NULL);
		LockRecursionException_t2413788283 * L_12 = (LockRecursionException_t2413788283 *)il2cpp_codegen_object_new(LockRecursionException_t2413788283_il2cpp_TypeInfo_var);
		LockRecursionException__ctor_m1065084584(L_12, _stringLiteral1258496512, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_0070:
	{
		int32_t L_13 = __this->get_owners_2();
		if (L_13)
		{
			goto IL_0092;
		}
	}
	{
		__this->set_owners_2((-1));
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t2300836069_il2cpp_TypeInfo_var);
		Thread_t2300836069 * L_14 = Thread_get_CurrentThread_m4142136012(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_write_thread_4(L_14);
		goto IL_0178;
	}

IL_0092:
	{
		int32_t L_15 = __this->get_owners_2();
		if ((!(((uint32_t)L_15) == ((uint32_t)1))))
		{
			goto IL_00c5;
		}
	}
	{
		Thread_t2300836069 * L_16 = __this->get_upgradable_thread_3();
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t2300836069_il2cpp_TypeInfo_var);
		Thread_t2300836069 * L_17 = Thread_get_CurrentThread_m4142136012(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Thread_t2300836069 *)L_16) == ((Il2CppObject*)(Thread_t2300836069 *)L_17))))
		{
			goto IL_00c5;
		}
	}
	{
		__this->set_owners_2((-1));
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t2300836069_il2cpp_TypeInfo_var);
		Thread_t2300836069 * L_18 = Thread_get_CurrentThread_m4142136012(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_write_thread_4(L_18);
		goto IL_0178;
	}

IL_00c5:
	{
		int32_t L_19 = ___millisecondsTimeout0;
		if (L_19)
		{
			goto IL_00d3;
		}
	}
	{
		ReaderWriterLockSlim_ExitMyLock_m2519986703(__this, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_00d3:
	{
		Thread_t2300836069 * L_20 = __this->get_upgradable_thread_3();
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t2300836069_il2cpp_TypeInfo_var);
		Thread_t2300836069 * L_21 = Thread_get_CurrentThread_m4142136012(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Thread_t2300836069 *)L_20) == ((Il2CppObject*)(Thread_t2300836069 *)L_21))))
		{
			goto IL_013c;
		}
	}
	{
		EventWaitHandle_t777845177 * L_22 = __this->get_upgradeEvent_10();
		if (L_22)
		{
			goto IL_0100;
		}
	}
	{
		EventWaitHandle_t777845177 ** L_23 = __this->get_address_of_upgradeEvent_10();
		ReaderWriterLockSlim_LazyCreateEvent_m3565271370(__this, L_23, (bool)0, /*hidden argument*/NULL);
		goto IL_0070;
	}

IL_0100:
	{
		uint32_t L_24 = __this->get_numUpgradeWaiters_7();
		if ((!(((uint32_t)L_24) > ((uint32_t)0))))
		{
			goto IL_011d;
		}
	}
	{
		ReaderWriterLockSlim_ExitMyLock_m2519986703(__this, /*hidden argument*/NULL);
		ApplicationException_t2339761290 * L_25 = (ApplicationException_t2339761290 *)il2cpp_codegen_object_new(ApplicationException_t2339761290_il2cpp_TypeInfo_var);
		ApplicationException__ctor_m2517758450(L_25, _stringLiteral3388452033, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_25);
	}

IL_011d:
	{
		EventWaitHandle_t777845177 * L_26 = __this->get_upgradeEvent_10();
		uint32_t* L_27 = __this->get_address_of_numUpgradeWaiters_7();
		int32_t L_28 = ___millisecondsTimeout0;
		bool L_29 = ReaderWriterLockSlim_WaitOnEvent_m2536988665(__this, L_26, L_27, L_28, /*hidden argument*/NULL);
		if (L_29)
		{
			goto IL_0137;
		}
	}
	{
		return (bool)0;
	}

IL_0137:
	{
		goto IL_0173;
	}

IL_013c:
	{
		EventWaitHandle_t777845177 * L_30 = __this->get_writeEvent_8();
		if (L_30)
		{
			goto IL_0159;
		}
	}
	{
		EventWaitHandle_t777845177 ** L_31 = __this->get_address_of_writeEvent_8();
		ReaderWriterLockSlim_LazyCreateEvent_m3565271370(__this, L_31, (bool)1, /*hidden argument*/NULL);
		goto IL_0070;
	}

IL_0159:
	{
		EventWaitHandle_t777845177 * L_32 = __this->get_writeEvent_8();
		uint32_t* L_33 = __this->get_address_of_numWriteWaiters_5();
		int32_t L_34 = ___millisecondsTimeout0;
		bool L_35 = ReaderWriterLockSlim_WaitOnEvent_m2536988665(__this, L_32, L_33, L_34, /*hidden argument*/NULL);
		if (L_35)
		{
			goto IL_0173;
		}
	}
	{
		return (bool)0;
	}

IL_0173:
	{
		goto IL_0070;
	}

IL_0178:
	{
		ReaderWriterLockSlim_ExitMyLock_m2519986703(__this, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Void System.Threading.ReaderWriterLockSlim::ExitWriteLock()
extern "C"  void ReaderWriterLockSlim_ExitWriteLock_m2211074167 (ReaderWriterLockSlim_t1938317233 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReaderWriterLockSlim_ExitWriteLock_m2211074167_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ReaderWriterLockSlim_EnterMyLock_m3975449768(__this, /*hidden argument*/NULL);
		int32_t L_0 = __this->get_owners_2();
		if ((((int32_t)L_0) == ((int32_t)(-1))))
		{
			goto IL_0023;
		}
	}
	{
		ReaderWriterLockSlim_ExitMyLock_m2519986703(__this, /*hidden argument*/NULL);
		SynchronizationLockException_t841761767 * L_1 = (SynchronizationLockException_t841761767 *)il2cpp_codegen_object_new(SynchronizationLockException_t841761767_il2cpp_TypeInfo_var);
		SynchronizationLockException__ctor_m3407855920(L_1, _stringLiteral990473242, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0023:
	{
		Thread_t2300836069 * L_2 = __this->get_upgradable_thread_3();
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t2300836069_il2cpp_TypeInfo_var);
		Thread_t2300836069 * L_3 = Thread_get_CurrentThread_m4142136012(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Thread_t2300836069 *)L_2) == ((Il2CppObject*)(Thread_t2300836069 *)L_3))))
		{
			goto IL_003f;
		}
	}
	{
		__this->set_owners_2(1);
		goto IL_0046;
	}

IL_003f:
	{
		__this->set_owners_2(0);
	}

IL_0046:
	{
		__this->set_write_thread_4((Thread_t2300836069 *)NULL);
		ReaderWriterLockSlim_ExitAndWakeUpAppropriateWaiters_m3940164111(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.ReaderWriterLockSlim::EnterUpgradeableReadLock()
extern "C"  void ReaderWriterLockSlim_EnterUpgradeableReadLock_m4153689444 (ReaderWriterLockSlim_t1938317233 * __this, const MethodInfo* method)
{
	{
		ReaderWriterLockSlim_TryEnterUpgradeableReadLock_m1427746323(__this, (-1), /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Threading.ReaderWriterLockSlim::TryEnterUpgradeableReadLock(System.Int32)
extern "C"  bool ReaderWriterLockSlim_TryEnterUpgradeableReadLock_m1427746323 (ReaderWriterLockSlim_t1938317233 * __this, int32_t ___millisecondsTimeout0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReaderWriterLockSlim_TryEnterUpgradeableReadLock_m1427746323_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___millisecondsTimeout0;
		if ((((int32_t)L_0) >= ((int32_t)(-1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_1 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_1, _stringLiteral3648076086, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		LockDetailsU5BU5D_t1708220258* L_2 = __this->get_read_locks_11();
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		ObjectDisposedException_t21392786 * L_3 = (ObjectDisposedException_t21392786 *)il2cpp_codegen_object_new(ObjectDisposedException_t21392786_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3603759869(L_3, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0024:
	{
		bool L_4 = ReaderWriterLockSlim_get_IsUpgradeableReadLockHeld_m1642772518(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0035;
		}
	}
	{
		LockRecursionException_t2413788283 * L_5 = (LockRecursionException_t2413788283 *)il2cpp_codegen_object_new(LockRecursionException_t2413788283_il2cpp_TypeInfo_var);
		LockRecursionException__ctor_m338981005(L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0035:
	{
		bool L_6 = ReaderWriterLockSlim_get_IsWriteLockHeld_m3969852364(__this, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0046;
		}
	}
	{
		LockRecursionException_t2413788283 * L_7 = (LockRecursionException_t2413788283 *)il2cpp_codegen_object_new(LockRecursionException_t2413788283_il2cpp_TypeInfo_var);
		LockRecursionException__ctor_m338981005(L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0046:
	{
		ReaderWriterLockSlim_EnterMyLock_m3975449768(__this, /*hidden argument*/NULL);
	}

IL_004c:
	{
		int32_t L_8 = __this->get_owners_2();
		if (L_8)
		{
			goto IL_008b;
		}
	}
	{
		uint32_t L_9 = __this->get_numWriteWaiters_5();
		if (L_9)
		{
			goto IL_008b;
		}
	}
	{
		Thread_t2300836069 * L_10 = __this->get_upgradable_thread_3();
		if (L_10)
		{
			goto IL_008b;
		}
	}
	{
		int32_t L_11 = __this->get_owners_2();
		__this->set_owners_2(((int32_t)((int32_t)L_11+(int32_t)1)));
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t2300836069_il2cpp_TypeInfo_var);
		Thread_t2300836069 * L_12 = Thread_get_CurrentThread_m4142136012(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_upgradable_thread_3(L_12);
		goto IL_00d5;
	}

IL_008b:
	{
		int32_t L_13 = ___millisecondsTimeout0;
		if (L_13)
		{
			goto IL_0099;
		}
	}
	{
		ReaderWriterLockSlim_ExitMyLock_m2519986703(__this, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0099:
	{
		EventWaitHandle_t777845177 * L_14 = __this->get_readEvent_9();
		if (L_14)
		{
			goto IL_00b6;
		}
	}
	{
		EventWaitHandle_t777845177 ** L_15 = __this->get_address_of_readEvent_9();
		ReaderWriterLockSlim_LazyCreateEvent_m3565271370(__this, L_15, (bool)0, /*hidden argument*/NULL);
		goto IL_004c;
	}

IL_00b6:
	{
		EventWaitHandle_t777845177 * L_16 = __this->get_readEvent_9();
		uint32_t* L_17 = __this->get_address_of_numReadWaiters_6();
		int32_t L_18 = ___millisecondsTimeout0;
		bool L_19 = ReaderWriterLockSlim_WaitOnEvent_m2536988665(__this, L_16, L_17, L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_00d0;
		}
	}
	{
		return (bool)0;
	}

IL_00d0:
	{
		goto IL_004c;
	}

IL_00d5:
	{
		ReaderWriterLockSlim_ExitMyLock_m2519986703(__this, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Void System.Threading.ReaderWriterLockSlim::ExitUpgradeableReadLock()
extern "C"  void ReaderWriterLockSlim_ExitUpgradeableReadLock_m1485987527 (ReaderWriterLockSlim_t1938317233 * __this, const MethodInfo* method)
{
	{
		ReaderWriterLockSlim_EnterMyLock_m3975449768(__this, /*hidden argument*/NULL);
		int32_t L_0 = __this->get_owners_2();
		__this->set_owners_2(((int32_t)((int32_t)L_0-(int32_t)1)));
		__this->set_upgradable_thread_3((Thread_t2300836069 *)NULL);
		ReaderWriterLockSlim_ExitAndWakeUpAppropriateWaiters_m3940164111(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.ReaderWriterLockSlim::Dispose()
extern "C"  void ReaderWriterLockSlim_Dispose_m966716435 (ReaderWriterLockSlim_t1938317233 * __this, const MethodInfo* method)
{
	{
		__this->set_read_locks_11((LockDetailsU5BU5D_t1708220258*)NULL);
		return;
	}
}
// System.Boolean System.Threading.ReaderWriterLockSlim::get_IsWriteLockHeld()
extern "C"  bool ReaderWriterLockSlim_get_IsWriteLockHeld_m3969852364 (ReaderWriterLockSlim_t1938317233 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ReaderWriterLockSlim_get_RecursiveWriteCount_m3156784678(__this, /*hidden argument*/NULL);
		return (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean System.Threading.ReaderWriterLockSlim::get_IsUpgradeableReadLockHeld()
extern "C"  bool ReaderWriterLockSlim_get_IsUpgradeableReadLockHeld_m1642772518 (ReaderWriterLockSlim_t1938317233 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ReaderWriterLockSlim_get_RecursiveUpgradeCount_m1354455429(__this, /*hidden argument*/NULL);
		return (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Threading.ReaderWriterLockSlim::get_RecursiveUpgradeCount()
extern "C"  int32_t ReaderWriterLockSlim_get_RecursiveUpgradeCount_m1354455429 (ReaderWriterLockSlim_t1938317233 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReaderWriterLockSlim_get_RecursiveUpgradeCount_m1354455429_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		Thread_t2300836069 * L_0 = __this->get_upgradable_thread_3();
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t2300836069_il2cpp_TypeInfo_var);
		Thread_t2300836069 * L_1 = Thread_get_CurrentThread_m4142136012(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Thread_t2300836069 *)L_0) == ((Il2CppObject*)(Thread_t2300836069 *)L_1))))
		{
			goto IL_0016;
		}
	}
	{
		G_B3_0 = 1;
		goto IL_0017;
	}

IL_0016:
	{
		G_B3_0 = 0;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
// System.Int32 System.Threading.ReaderWriterLockSlim::get_RecursiveWriteCount()
extern "C"  int32_t ReaderWriterLockSlim_get_RecursiveWriteCount_m3156784678 (ReaderWriterLockSlim_t1938317233 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReaderWriterLockSlim_get_RecursiveWriteCount_m3156784678_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		Thread_t2300836069 * L_0 = __this->get_write_thread_4();
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t2300836069_il2cpp_TypeInfo_var);
		Thread_t2300836069 * L_1 = Thread_get_CurrentThread_m4142136012(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Thread_t2300836069 *)L_0) == ((Il2CppObject*)(Thread_t2300836069 *)L_1))))
		{
			goto IL_0016;
		}
	}
	{
		G_B3_0 = 1;
		goto IL_0017;
	}

IL_0016:
	{
		G_B3_0 = 0;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
// System.Void System.Threading.ReaderWriterLockSlim::EnterMyLock()
extern "C"  void ReaderWriterLockSlim_EnterMyLock_m3975449768 (ReaderWriterLockSlim_t1938317233 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = __this->get_address_of_myLock_1();
		int32_t L_1 = Interlocked_CompareExchange_m3023855514(NULL /*static, unused*/, L_0, 1, 0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		ReaderWriterLockSlim_EnterMyLockSpin_m1525802727(__this, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void System.Threading.ReaderWriterLockSlim::EnterMyLockSpin()
extern "C"  void ReaderWriterLockSlim_EnterMyLockSpin_m1525802727 (ReaderWriterLockSlim_t1938317233 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReaderWriterLockSlim_EnterMyLockSpin_m1525802727_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0041;
	}

IL_0007:
	{
		int32_t L_0 = V_0;
		if ((((int32_t)L_0) >= ((int32_t)3)))
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ReaderWriterLockSlim_t1938317233_il2cpp_TypeInfo_var);
		bool L_1 = ((ReaderWriterLockSlim_t1938317233_StaticFields*)ReaderWriterLockSlim_t1938317233_il2cpp_TypeInfo_var->static_fields)->get_smp_0();
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t2300836069_il2cpp_TypeInfo_var);
		Thread_SpinWait_m3968465979(NULL /*static, unused*/, ((int32_t)20), /*hidden argument*/NULL);
		goto IL_002a;
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t2300836069_il2cpp_TypeInfo_var);
		Thread_Sleep_m483098292(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_002a:
	{
		int32_t* L_2 = __this->get_address_of_myLock_1();
		int32_t L_3 = Interlocked_CompareExchange_m3023855514(NULL /*static, unused*/, L_2, 1, 0, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003d;
		}
	}
	{
		return;
	}

IL_003d:
	{
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0041:
	{
		goto IL_0007;
	}
}
// System.Void System.Threading.ReaderWriterLockSlim::ExitMyLock()
extern "C"  void ReaderWriterLockSlim_ExitMyLock_m2519986703 (ReaderWriterLockSlim_t1938317233 * __this, const MethodInfo* method)
{
	{
		__this->set_myLock_1(0);
		return;
	}
}
// System.Void System.Threading.ReaderWriterLockSlim::ExitAndWakeUpAppropriateWaiters()
extern "C"  void ReaderWriterLockSlim_ExitAndWakeUpAppropriateWaiters_m3940164111 (ReaderWriterLockSlim_t1938317233 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_owners_2();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_002e;
		}
	}
	{
		uint32_t L_1 = __this->get_numUpgradeWaiters_7();
		if (!L_1)
		{
			goto IL_002e;
		}
	}
	{
		ReaderWriterLockSlim_ExitMyLock_m2519986703(__this, /*hidden argument*/NULL);
		EventWaitHandle_t777845177 * L_2 = __this->get_upgradeEvent_10();
		NullCheck(L_2);
		EventWaitHandle_Set_m2445193251(L_2, /*hidden argument*/NULL);
		goto IL_0090;
	}

IL_002e:
	{
		int32_t L_3 = __this->get_owners_2();
		if (L_3)
		{
			goto IL_005c;
		}
	}
	{
		uint32_t L_4 = __this->get_numWriteWaiters_5();
		if ((!(((uint32_t)L_4) > ((uint32_t)0))))
		{
			goto IL_005c;
		}
	}
	{
		ReaderWriterLockSlim_ExitMyLock_m2519986703(__this, /*hidden argument*/NULL);
		EventWaitHandle_t777845177 * L_5 = __this->get_writeEvent_8();
		NullCheck(L_5);
		EventWaitHandle_Set_m2445193251(L_5, /*hidden argument*/NULL);
		goto IL_0090;
	}

IL_005c:
	{
		int32_t L_6 = __this->get_owners_2();
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_008a;
		}
	}
	{
		uint32_t L_7 = __this->get_numReadWaiters_6();
		if (!L_7)
		{
			goto IL_008a;
		}
	}
	{
		ReaderWriterLockSlim_ExitMyLock_m2519986703(__this, /*hidden argument*/NULL);
		EventWaitHandle_t777845177 * L_8 = __this->get_readEvent_9();
		NullCheck(L_8);
		EventWaitHandle_Set_m2445193251(L_8, /*hidden argument*/NULL);
		goto IL_0090;
	}

IL_008a:
	{
		ReaderWriterLockSlim_ExitMyLock_m2519986703(__this, /*hidden argument*/NULL);
	}

IL_0090:
	{
		return;
	}
}
// System.Void System.Threading.ReaderWriterLockSlim::LazyCreateEvent(System.Threading.EventWaitHandle&,System.Boolean)
extern "C"  void ReaderWriterLockSlim_LazyCreateEvent_m3565271370 (ReaderWriterLockSlim_t1938317233 * __this, EventWaitHandle_t777845177 ** ___waitEvent0, bool ___makeAutoResetEvent1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReaderWriterLockSlim_LazyCreateEvent_m3565271370_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventWaitHandle_t777845177 * V_0 = NULL;
	{
		ReaderWriterLockSlim_ExitMyLock_m2519986703(__this, /*hidden argument*/NULL);
		bool L_0 = ___makeAutoResetEvent1;
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		AutoResetEvent_t1333520283 * L_1 = (AutoResetEvent_t1333520283 *)il2cpp_codegen_object_new(AutoResetEvent_t1333520283_il2cpp_TypeInfo_var);
		AutoResetEvent__ctor_m3710433672(L_1, (bool)0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_001f;
	}

IL_0018:
	{
		ManualResetEvent_t451242010 * L_2 = (ManualResetEvent_t451242010 *)il2cpp_codegen_object_new(ManualResetEvent_t451242010_il2cpp_TypeInfo_var);
		ManualResetEvent__ctor_m4010886457(L_2, (bool)0, /*hidden argument*/NULL);
		V_0 = L_2;
	}

IL_001f:
	{
		ReaderWriterLockSlim_EnterMyLock_m3975449768(__this, /*hidden argument*/NULL);
		EventWaitHandle_t777845177 ** L_3 = ___waitEvent0;
		if ((*((EventWaitHandle_t777845177 **)L_3)))
		{
			goto IL_002f;
		}
	}
	{
		EventWaitHandle_t777845177 ** L_4 = ___waitEvent0;
		EventWaitHandle_t777845177 * L_5 = V_0;
		*((Il2CppObject **)(L_4)) = (Il2CppObject *)L_5;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_4), (Il2CppObject *)L_5);
	}

IL_002f:
	{
		return;
	}
}
// System.Boolean System.Threading.ReaderWriterLockSlim::WaitOnEvent(System.Threading.EventWaitHandle,System.UInt32&,System.Int32)
extern "C"  bool ReaderWriterLockSlim_WaitOnEvent_m2536988665 (ReaderWriterLockSlim_t1938317233 * __this, EventWaitHandle_t777845177 * ___waitEvent0, uint32_t* ___numWaiters1, int32_t ___millisecondsTimeout2, const MethodInfo* method)
{
	bool V_0 = false;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		EventWaitHandle_t777845177 * L_0 = ___waitEvent0;
		NullCheck(L_0);
		EventWaitHandle_Reset_m3348053200(L_0, /*hidden argument*/NULL);
		uint32_t* L_1 = ___numWaiters1;
		uint32_t* L_2 = ___numWaiters1;
		*((int32_t*)(L_1)) = (int32_t)((int32_t)((int32_t)(*((uint32_t*)L_2))+(int32_t)1));
		V_0 = (bool)0;
		ReaderWriterLockSlim_ExitMyLock_m2519986703(__this, /*hidden argument*/NULL);
	}

IL_0015:
	try
	{ // begin try (depth: 1)
		EventWaitHandle_t777845177 * L_3 = ___waitEvent0;
		int32_t L_4 = ___millisecondsTimeout2;
		NullCheck(L_3);
		bool L_5 = VirtFuncInvoker2< bool, int32_t, bool >::Invoke(12 /* System.Boolean System.Threading.WaitHandle::WaitOne(System.Int32,System.Boolean) */, L_3, L_4, (bool)0);
		V_0 = L_5;
		IL2CPP_LEAVE(0x3C, FINALLY_0023);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0023;
	}

FINALLY_0023:
	{ // begin finally (depth: 1)
		{
			ReaderWriterLockSlim_EnterMyLock_m3975449768(__this, /*hidden argument*/NULL);
			uint32_t* L_6 = ___numWaiters1;
			uint32_t* L_7 = ___numWaiters1;
			*((int32_t*)(L_6)) = (int32_t)((int32_t)((int32_t)(*((uint32_t*)L_7))-(int32_t)1));
			bool L_8 = V_0;
			if (L_8)
			{
				goto IL_003b;
			}
		}

IL_0035:
		{
			ReaderWriterLockSlim_ExitMyLock_m2519986703(__this, /*hidden argument*/NULL);
		}

IL_003b:
		{
			IL2CPP_END_FINALLY(35)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(35)
	{
		IL2CPP_JUMP_TBL(0x3C, IL_003c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_003c:
	{
		bool L_9 = V_0;
		return L_9;
	}
}
// System.Threading.ReaderWriterLockSlim/LockDetails System.Threading.ReaderWriterLockSlim::GetReadLockDetails(System.Int32,System.Boolean)
extern "C"  LockDetails_t2068764019 * ReaderWriterLockSlim_GetReadLockDetails_m468803701 (ReaderWriterLockSlim_t1938317233 * __this, int32_t ___threadId0, bool ___create1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReaderWriterLockSlim_GetReadLockDetails_m468803701_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	LockDetails_t2068764019 * V_1 = NULL;
	LockDetails_t2068764019 * V_2 = NULL;
	{
		V_0 = 0;
		goto IL_002d;
	}

IL_0007:
	{
		LockDetailsU5BU5D_t1708220258* L_0 = __this->get_read_locks_11();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		LockDetails_t2068764019 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		LockDetails_t2068764019 * L_4 = V_1;
		if (L_4)
		{
			goto IL_001b;
		}
	}
	{
		goto IL_003b;
	}

IL_001b:
	{
		LockDetails_t2068764019 * L_5 = V_1;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_ThreadId_0();
		int32_t L_7 = ___threadId0;
		if ((!(((uint32_t)L_6) == ((uint32_t)L_7))))
		{
			goto IL_0029;
		}
	}
	{
		LockDetails_t2068764019 * L_8 = V_1;
		return L_8;
	}

IL_0029:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002d:
	{
		int32_t L_10 = V_0;
		LockDetailsU5BU5D_t1708220258* L_11 = __this->get_read_locks_11();
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0007;
		}
	}

IL_003b:
	{
		bool L_12 = ___create1;
		if (L_12)
		{
			goto IL_0043;
		}
	}
	{
		return (LockDetails_t2068764019 *)NULL;
	}

IL_0043:
	{
		int32_t L_13 = V_0;
		LockDetailsU5BU5D_t1708220258* L_14 = __this->get_read_locks_11();
		NullCheck(L_14);
		if ((!(((uint32_t)L_13) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length))))))))
		{
			goto IL_0066;
		}
	}
	{
		LockDetailsU5BU5D_t1708220258** L_15 = __this->get_address_of_read_locks_11();
		LockDetailsU5BU5D_t1708220258* L_16 = __this->get_read_locks_11();
		NullCheck(L_16);
		Array_Resize_TisLockDetails_t2068764019_m2614325894(NULL /*static, unused*/, L_15, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length))))*(int32_t)2)), /*hidden argument*/Array_Resize_TisLockDetails_t2068764019_m2614325894_MethodInfo_var);
	}

IL_0066:
	{
		LockDetailsU5BU5D_t1708220258* L_17 = __this->get_read_locks_11();
		int32_t L_18 = V_0;
		LockDetails_t2068764019 * L_19 = (LockDetails_t2068764019 *)il2cpp_codegen_object_new(LockDetails_t2068764019_il2cpp_TypeInfo_var);
		LockDetails__ctor_m1021033989(L_19, /*hidden argument*/NULL);
		LockDetails_t2068764019 * L_20 = L_19;
		V_2 = L_20;
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_20);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (LockDetails_t2068764019 *)L_20);
		LockDetails_t2068764019 * L_21 = V_2;
		V_1 = L_21;
		LockDetails_t2068764019 * L_22 = V_1;
		int32_t L_23 = ___threadId0;
		NullCheck(L_22);
		L_22->set_ThreadId_0(L_23);
		LockDetails_t2068764019 * L_24 = V_1;
		return L_24;
	}
}
// System.Void System.Threading.ReaderWriterLockSlim/LockDetails::.ctor()
extern "C"  void LockDetails__ctor_m1021033989 (LockDetails_t2068764019 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
