﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeExpression2166265795.h"

// System.CodeDom.CodeTypeReference
struct CodeTypeReference_t3809997434;
// System.String
struct String_t;
// System.CodeDom.CodeExpression
struct CodeExpression_t2166265795;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeDelegateCreateExpression
struct  CodeDelegateCreateExpression_t2744709072  : public CodeExpression_t2166265795
{
public:
	// System.CodeDom.CodeTypeReference System.CodeDom.CodeDelegateCreateExpression::delegateType
	CodeTypeReference_t3809997434 * ___delegateType_1;
	// System.String System.CodeDom.CodeDelegateCreateExpression::methodName
	String_t* ___methodName_2;
	// System.CodeDom.CodeExpression System.CodeDom.CodeDelegateCreateExpression::targetObject
	CodeExpression_t2166265795 * ___targetObject_3;

public:
	inline static int32_t get_offset_of_delegateType_1() { return static_cast<int32_t>(offsetof(CodeDelegateCreateExpression_t2744709072, ___delegateType_1)); }
	inline CodeTypeReference_t3809997434 * get_delegateType_1() const { return ___delegateType_1; }
	inline CodeTypeReference_t3809997434 ** get_address_of_delegateType_1() { return &___delegateType_1; }
	inline void set_delegateType_1(CodeTypeReference_t3809997434 * value)
	{
		___delegateType_1 = value;
		Il2CppCodeGenWriteBarrier(&___delegateType_1, value);
	}

	inline static int32_t get_offset_of_methodName_2() { return static_cast<int32_t>(offsetof(CodeDelegateCreateExpression_t2744709072, ___methodName_2)); }
	inline String_t* get_methodName_2() const { return ___methodName_2; }
	inline String_t** get_address_of_methodName_2() { return &___methodName_2; }
	inline void set_methodName_2(String_t* value)
	{
		___methodName_2 = value;
		Il2CppCodeGenWriteBarrier(&___methodName_2, value);
	}

	inline static int32_t get_offset_of_targetObject_3() { return static_cast<int32_t>(offsetof(CodeDelegateCreateExpression_t2744709072, ___targetObject_3)); }
	inline CodeExpression_t2166265795 * get_targetObject_3() const { return ___targetObject_3; }
	inline CodeExpression_t2166265795 ** get_address_of_targetObject_3() { return &___targetObject_3; }
	inline void set_targetObject_3(CodeExpression_t2166265795 * value)
	{
		___targetObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___targetObject_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
