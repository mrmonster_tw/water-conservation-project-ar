﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// System.String[]
struct StringU5BU5D_t1281789340;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneLoader
struct  SceneLoader_t4130533360  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 SceneLoader::ox
	int32_t ___ox_3;
	// System.Int32 SceneLoader::oy
	int32_t ___oy_4;
	// System.Int32 SceneLoader::h
	int32_t ___h_5;

public:
	inline static int32_t get_offset_of_ox_3() { return static_cast<int32_t>(offsetof(SceneLoader_t4130533360, ___ox_3)); }
	inline int32_t get_ox_3() const { return ___ox_3; }
	inline int32_t* get_address_of_ox_3() { return &___ox_3; }
	inline void set_ox_3(int32_t value)
	{
		___ox_3 = value;
	}

	inline static int32_t get_offset_of_oy_4() { return static_cast<int32_t>(offsetof(SceneLoader_t4130533360, ___oy_4)); }
	inline int32_t get_oy_4() const { return ___oy_4; }
	inline int32_t* get_address_of_oy_4() { return &___oy_4; }
	inline void set_oy_4(int32_t value)
	{
		___oy_4 = value;
	}

	inline static int32_t get_offset_of_h_5() { return static_cast<int32_t>(offsetof(SceneLoader_t4130533360, ___h_5)); }
	inline int32_t get_h_5() const { return ___h_5; }
	inline int32_t* get_address_of_h_5() { return &___h_5; }
	inline void set_h_5(int32_t value)
	{
		___h_5 = value;
	}
};

struct SceneLoader_t4130533360_StaticFields
{
public:
	// System.String[] SceneLoader::sceneNames
	StringU5BU5D_t1281789340* ___sceneNames_2;

public:
	inline static int32_t get_offset_of_sceneNames_2() { return static_cast<int32_t>(offsetof(SceneLoader_t4130533360_StaticFields, ___sceneNames_2)); }
	inline StringU5BU5D_t1281789340* get_sceneNames_2() const { return ___sceneNames_2; }
	inline StringU5BU5D_t1281789340** get_address_of_sceneNames_2() { return &___sceneNames_2; }
	inline void set_sceneNames_2(StringU5BU5D_t1281789340* value)
	{
		___sceneNames_2 = value;
		Il2CppCodeGenWriteBarrier(&___sceneNames_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
