﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.TcpConnectionPoolSettingsElement
struct  TcpConnectionPoolSettingsElement_t1603906250  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct TcpConnectionPoolSettingsElement_t1603906250_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.TcpConnectionPoolSettingsElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.TcpConnectionPoolSettingsElement::group_name
	ConfigurationProperty_t3590861854 * ___group_name_14;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.TcpConnectionPoolSettingsElement::idle_timeout
	ConfigurationProperty_t3590861854 * ___idle_timeout_15;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.TcpConnectionPoolSettingsElement::lease_timeout
	ConfigurationProperty_t3590861854 * ___lease_timeout_16;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.TcpConnectionPoolSettingsElement::max_outbound_connections_per_endpoint
	ConfigurationProperty_t3590861854 * ___max_outbound_connections_per_endpoint_17;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(TcpConnectionPoolSettingsElement_t1603906250_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier(&___properties_13, value);
	}

	inline static int32_t get_offset_of_group_name_14() { return static_cast<int32_t>(offsetof(TcpConnectionPoolSettingsElement_t1603906250_StaticFields, ___group_name_14)); }
	inline ConfigurationProperty_t3590861854 * get_group_name_14() const { return ___group_name_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_group_name_14() { return &___group_name_14; }
	inline void set_group_name_14(ConfigurationProperty_t3590861854 * value)
	{
		___group_name_14 = value;
		Il2CppCodeGenWriteBarrier(&___group_name_14, value);
	}

	inline static int32_t get_offset_of_idle_timeout_15() { return static_cast<int32_t>(offsetof(TcpConnectionPoolSettingsElement_t1603906250_StaticFields, ___idle_timeout_15)); }
	inline ConfigurationProperty_t3590861854 * get_idle_timeout_15() const { return ___idle_timeout_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_idle_timeout_15() { return &___idle_timeout_15; }
	inline void set_idle_timeout_15(ConfigurationProperty_t3590861854 * value)
	{
		___idle_timeout_15 = value;
		Il2CppCodeGenWriteBarrier(&___idle_timeout_15, value);
	}

	inline static int32_t get_offset_of_lease_timeout_16() { return static_cast<int32_t>(offsetof(TcpConnectionPoolSettingsElement_t1603906250_StaticFields, ___lease_timeout_16)); }
	inline ConfigurationProperty_t3590861854 * get_lease_timeout_16() const { return ___lease_timeout_16; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_lease_timeout_16() { return &___lease_timeout_16; }
	inline void set_lease_timeout_16(ConfigurationProperty_t3590861854 * value)
	{
		___lease_timeout_16 = value;
		Il2CppCodeGenWriteBarrier(&___lease_timeout_16, value);
	}

	inline static int32_t get_offset_of_max_outbound_connections_per_endpoint_17() { return static_cast<int32_t>(offsetof(TcpConnectionPoolSettingsElement_t1603906250_StaticFields, ___max_outbound_connections_per_endpoint_17)); }
	inline ConfigurationProperty_t3590861854 * get_max_outbound_connections_per_endpoint_17() const { return ___max_outbound_connections_per_endpoint_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_max_outbound_connections_per_endpoint_17() { return &___max_outbound_connections_per_endpoint_17; }
	inline void set_max_outbound_connections_per_endpoint_17(ConfigurationProperty_t3590861854 * value)
	{
		___max_outbound_connections_per_endpoint_17 = value;
		Il2CppCodeGenWriteBarrier(&___max_outbound_connections_per_endpoint_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
