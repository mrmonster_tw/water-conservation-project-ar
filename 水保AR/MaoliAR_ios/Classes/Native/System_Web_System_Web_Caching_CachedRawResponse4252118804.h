﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t407452768;
// System.Collections.Generic.List`1<System.Web.Caching.CachedRawResponse/DataItem>
struct List_1_t2647236735;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Caching.CachedRawResponse
struct  CachedRawResponse_t4252118804  : public Il2CppObject
{
public:
	// System.Collections.Specialized.NameValueCollection System.Web.Caching.CachedRawResponse::headers
	NameValueCollection_t407452768 * ___headers_0;
	// System.Collections.Generic.List`1<System.Web.Caching.CachedRawResponse/DataItem> System.Web.Caching.CachedRawResponse::data
	List_1_t2647236735 * ___data_1;

public:
	inline static int32_t get_offset_of_headers_0() { return static_cast<int32_t>(offsetof(CachedRawResponse_t4252118804, ___headers_0)); }
	inline NameValueCollection_t407452768 * get_headers_0() const { return ___headers_0; }
	inline NameValueCollection_t407452768 ** get_address_of_headers_0() { return &___headers_0; }
	inline void set_headers_0(NameValueCollection_t407452768 * value)
	{
		___headers_0 = value;
		Il2CppCodeGenWriteBarrier(&___headers_0, value);
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(CachedRawResponse_t4252118804, ___data_1)); }
	inline List_1_t2647236735 * get_data_1() const { return ___data_1; }
	inline List_1_t2647236735 ** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(List_1_t2647236735 * value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier(&___data_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
