﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Tokens_S1271873540.h"
#include "mscorlib_System_DateTime3738529785.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Tokens.UserNameSecurityToken
struct  UserNameSecurityToken_t2268299345  : public SecurityToken_t1271873540
{
public:
	// System.DateTime System.IdentityModel.Tokens.UserNameSecurityToken::from
	DateTime_t3738529785  ___from_0;
	// System.String System.IdentityModel.Tokens.UserNameSecurityToken::username
	String_t* ___username_1;
	// System.String System.IdentityModel.Tokens.UserNameSecurityToken::password
	String_t* ___password_2;
	// System.String System.IdentityModel.Tokens.UserNameSecurityToken::id
	String_t* ___id_3;

public:
	inline static int32_t get_offset_of_from_0() { return static_cast<int32_t>(offsetof(UserNameSecurityToken_t2268299345, ___from_0)); }
	inline DateTime_t3738529785  get_from_0() const { return ___from_0; }
	inline DateTime_t3738529785 * get_address_of_from_0() { return &___from_0; }
	inline void set_from_0(DateTime_t3738529785  value)
	{
		___from_0 = value;
	}

	inline static int32_t get_offset_of_username_1() { return static_cast<int32_t>(offsetof(UserNameSecurityToken_t2268299345, ___username_1)); }
	inline String_t* get_username_1() const { return ___username_1; }
	inline String_t** get_address_of_username_1() { return &___username_1; }
	inline void set_username_1(String_t* value)
	{
		___username_1 = value;
		Il2CppCodeGenWriteBarrier(&___username_1, value);
	}

	inline static int32_t get_offset_of_password_2() { return static_cast<int32_t>(offsetof(UserNameSecurityToken_t2268299345, ___password_2)); }
	inline String_t* get_password_2() const { return ___password_2; }
	inline String_t** get_address_of_password_2() { return &___password_2; }
	inline void set_password_2(String_t* value)
	{
		___password_2 = value;
		Il2CppCodeGenWriteBarrier(&___password_2, value);
	}

	inline static int32_t get_offset_of_id_3() { return static_cast<int32_t>(offsetof(UserNameSecurityToken_t2268299345, ___id_3)); }
	inline String_t* get_id_3() const { return ___id_3; }
	inline String_t** get_address_of_id_3() { return &___id_3; }
	inline void set_id_3(String_t* value)
	{
		___id_3 = value;
		Il2CppCodeGenWriteBarrier(&___id_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
