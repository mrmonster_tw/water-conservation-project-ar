﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_ChannelFact628250694.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.ChannelFactory`1<System.Object>
struct  ChannelFactory_1_t585335377  : public ChannelFactory_t628250694
{
public:
	// System.Object System.ServiceModel.ChannelFactory`1::<OwnerClientBase>k__BackingField
	Il2CppObject * ___U3COwnerClientBaseU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3COwnerClientBaseU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ChannelFactory_1_t585335377, ___U3COwnerClientBaseU3Ek__BackingField_11)); }
	inline Il2CppObject * get_U3COwnerClientBaseU3Ek__BackingField_11() const { return ___U3COwnerClientBaseU3Ek__BackingField_11; }
	inline Il2CppObject ** get_address_of_U3COwnerClientBaseU3Ek__BackingField_11() { return &___U3COwnerClientBaseU3Ek__BackingField_11; }
	inline void set_U3COwnerClientBaseU3Ek__BackingField_11(Il2CppObject * value)
	{
		___U3COwnerClientBaseU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3COwnerClientBaseU3Ek__BackingField_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
