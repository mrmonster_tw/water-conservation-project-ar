﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Xml.XmlDocument
struct XmlDocument_t2837193595;
// System.Security.Cryptography.Xml.XmlDsigExcC14NTransform
struct XmlDsigExcC14NTransform_t586418029;
// System.IO.MemoryStream
struct MemoryStream_t94973147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.SslCommunicationObject/TlsnegoClientSessionContext
struct  TlsnegoClientSessionContext_t3168857361  : public Il2CppObject
{
public:
	// System.Xml.XmlDocument System.ServiceModel.Security.Tokens.SslCommunicationObject/TlsnegoClientSessionContext::doc
	XmlDocument_t2837193595 * ___doc_0;
	// System.Security.Cryptography.Xml.XmlDsigExcC14NTransform System.ServiceModel.Security.Tokens.SslCommunicationObject/TlsnegoClientSessionContext::t
	XmlDsigExcC14NTransform_t586418029 * ___t_1;
	// System.IO.MemoryStream System.ServiceModel.Security.Tokens.SslCommunicationObject/TlsnegoClientSessionContext::stream
	MemoryStream_t94973147 * ___stream_2;

public:
	inline static int32_t get_offset_of_doc_0() { return static_cast<int32_t>(offsetof(TlsnegoClientSessionContext_t3168857361, ___doc_0)); }
	inline XmlDocument_t2837193595 * get_doc_0() const { return ___doc_0; }
	inline XmlDocument_t2837193595 ** get_address_of_doc_0() { return &___doc_0; }
	inline void set_doc_0(XmlDocument_t2837193595 * value)
	{
		___doc_0 = value;
		Il2CppCodeGenWriteBarrier(&___doc_0, value);
	}

	inline static int32_t get_offset_of_t_1() { return static_cast<int32_t>(offsetof(TlsnegoClientSessionContext_t3168857361, ___t_1)); }
	inline XmlDsigExcC14NTransform_t586418029 * get_t_1() const { return ___t_1; }
	inline XmlDsigExcC14NTransform_t586418029 ** get_address_of_t_1() { return &___t_1; }
	inline void set_t_1(XmlDsigExcC14NTransform_t586418029 * value)
	{
		___t_1 = value;
		Il2CppCodeGenWriteBarrier(&___t_1, value);
	}

	inline static int32_t get_offset_of_stream_2() { return static_cast<int32_t>(offsetof(TlsnegoClientSessionContext_t3168857361, ___stream_2)); }
	inline MemoryStream_t94973147 * get_stream_2() const { return ___stream_2; }
	inline MemoryStream_t94973147 ** get_address_of_stream_2() { return &___stream_2; }
	inline void set_stream_2(MemoryStream_t94973147 * value)
	{
		___stream_2 = value;
		Il2CppCodeGenWriteBarrier(&___stream_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
