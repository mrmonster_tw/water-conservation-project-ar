﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_MarshalByRefObject2760389100.h"
#include "mscorlib_System_Boolean97287965.h"

// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Object
struct Il2CppObject;
// System.Collections.ICollection
struct ICollection_t3904884886;
// System.Char[]
struct CharU5BU5D_t3528271667;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.CapabilitiesLoader
struct  CapabilitiesLoader_t211893129  : public MarshalByRefObject_t2760389100
{
public:

public:
};

struct CapabilitiesLoader_t211893129_StaticFields
{
public:
	// System.Collections.Hashtable System.Web.CapabilitiesLoader::defaultCaps
	Hashtable_t1853889766 * ___defaultCaps_1;
	// System.Object System.Web.CapabilitiesLoader::lockobj
	Il2CppObject * ___lockobj_2;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Web.CapabilitiesLoader::loaded
	bool ___loaded_3;
	// System.Collections.ICollection System.Web.CapabilitiesLoader::alldata
	Il2CppObject * ___alldata_4;
	// System.Collections.Hashtable System.Web.CapabilitiesLoader::userAgentsCache
	Hashtable_t1853889766 * ___userAgentsCache_5;
	// System.Char[] System.Web.CapabilitiesLoader::eq
	CharU5BU5D_t3528271667* ___eq_6;

public:
	inline static int32_t get_offset_of_defaultCaps_1() { return static_cast<int32_t>(offsetof(CapabilitiesLoader_t211893129_StaticFields, ___defaultCaps_1)); }
	inline Hashtable_t1853889766 * get_defaultCaps_1() const { return ___defaultCaps_1; }
	inline Hashtable_t1853889766 ** get_address_of_defaultCaps_1() { return &___defaultCaps_1; }
	inline void set_defaultCaps_1(Hashtable_t1853889766 * value)
	{
		___defaultCaps_1 = value;
		Il2CppCodeGenWriteBarrier(&___defaultCaps_1, value);
	}

	inline static int32_t get_offset_of_lockobj_2() { return static_cast<int32_t>(offsetof(CapabilitiesLoader_t211893129_StaticFields, ___lockobj_2)); }
	inline Il2CppObject * get_lockobj_2() const { return ___lockobj_2; }
	inline Il2CppObject ** get_address_of_lockobj_2() { return &___lockobj_2; }
	inline void set_lockobj_2(Il2CppObject * value)
	{
		___lockobj_2 = value;
		Il2CppCodeGenWriteBarrier(&___lockobj_2, value);
	}

	inline static int32_t get_offset_of_loaded_3() { return static_cast<int32_t>(offsetof(CapabilitiesLoader_t211893129_StaticFields, ___loaded_3)); }
	inline bool get_loaded_3() const { return ___loaded_3; }
	inline bool* get_address_of_loaded_3() { return &___loaded_3; }
	inline void set_loaded_3(bool value)
	{
		___loaded_3 = value;
	}

	inline static int32_t get_offset_of_alldata_4() { return static_cast<int32_t>(offsetof(CapabilitiesLoader_t211893129_StaticFields, ___alldata_4)); }
	inline Il2CppObject * get_alldata_4() const { return ___alldata_4; }
	inline Il2CppObject ** get_address_of_alldata_4() { return &___alldata_4; }
	inline void set_alldata_4(Il2CppObject * value)
	{
		___alldata_4 = value;
		Il2CppCodeGenWriteBarrier(&___alldata_4, value);
	}

	inline static int32_t get_offset_of_userAgentsCache_5() { return static_cast<int32_t>(offsetof(CapabilitiesLoader_t211893129_StaticFields, ___userAgentsCache_5)); }
	inline Hashtable_t1853889766 * get_userAgentsCache_5() const { return ___userAgentsCache_5; }
	inline Hashtable_t1853889766 ** get_address_of_userAgentsCache_5() { return &___userAgentsCache_5; }
	inline void set_userAgentsCache_5(Hashtable_t1853889766 * value)
	{
		___userAgentsCache_5 = value;
		Il2CppCodeGenWriteBarrier(&___userAgentsCache_5, value);
	}

	inline static int32_t get_offset_of_eq_6() { return static_cast<int32_t>(offsetof(CapabilitiesLoader_t211893129_StaticFields, ___eq_6)); }
	inline CharU5BU5D_t3528271667* get_eq_6() const { return ___eq_6; }
	inline CharU5BU5D_t3528271667** get_address_of_eq_6() { return &___eq_6; }
	inline void set_eq_6(CharU5BU5D_t3528271667* value)
	{
		___eq_6 = value;
		Il2CppCodeGenWriteBarrier(&___eq_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
