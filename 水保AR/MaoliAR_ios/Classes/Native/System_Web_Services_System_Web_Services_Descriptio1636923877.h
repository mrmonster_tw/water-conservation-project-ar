﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_Services_System_Web_Services_Descriptio3177955060.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.HttpBinding
struct  HttpBinding_t1636923877  : public ServiceDescriptionFormatExtension_t3177955060
{
public:
	// System.String System.Web.Services.Description.HttpBinding::verb
	String_t* ___verb_1;

public:
	inline static int32_t get_offset_of_verb_1() { return static_cast<int32_t>(offsetof(HttpBinding_t1636923877, ___verb_1)); }
	inline String_t* get_verb_1() const { return ___verb_1; }
	inline String_t** get_address_of_verb_1() { return &___verb_1; }
	inline void set_verb_1(String_t* value)
	{
		___verb_1 = value;
		Il2CppCodeGenWriteBarrier(&___verb_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
