﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpStaticObjectsCollection/StaticItem
struct  StaticItem_t3669534551  : public Il2CppObject
{
public:
	// System.Object System.Web.HttpStaticObjectsCollection/StaticItem::this_lock
	Il2CppObject * ___this_lock_0;
	// System.Type System.Web.HttpStaticObjectsCollection/StaticItem::type
	Type_t * ___type_1;

public:
	inline static int32_t get_offset_of_this_lock_0() { return static_cast<int32_t>(offsetof(StaticItem_t3669534551, ___this_lock_0)); }
	inline Il2CppObject * get_this_lock_0() const { return ___this_lock_0; }
	inline Il2CppObject ** get_address_of_this_lock_0() { return &___this_lock_0; }
	inline void set_this_lock_0(Il2CppObject * value)
	{
		___this_lock_0 = value;
		Il2CppCodeGenWriteBarrier(&___this_lock_0, value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(StaticItem_t3669534551, ___type_1)); }
	inline Type_t * get_type_1() const { return ___type_1; }
	inline Type_t ** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(Type_t * value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier(&___type_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
