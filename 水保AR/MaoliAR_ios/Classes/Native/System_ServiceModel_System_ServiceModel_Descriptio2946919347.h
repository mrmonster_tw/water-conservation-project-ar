﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Description.ServiceEndpointCollection
struct ServiceEndpointCollection_t3548009095;
// System.Collections.Generic.KeyedByTypeCollection`1<System.ServiceModel.Description.IServiceBehavior>
struct KeyedByTypeCollection_1_t2687594759;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.ServiceDescription
struct  ServiceDescription_t2946919347  : public Il2CppObject
{
public:
	// System.ServiceModel.Description.ServiceEndpointCollection System.ServiceModel.Description.ServiceDescription::endpoints
	ServiceEndpointCollection_t3548009095 * ___endpoints_0;
	// System.Collections.Generic.KeyedByTypeCollection`1<System.ServiceModel.Description.IServiceBehavior> System.ServiceModel.Description.ServiceDescription::behaviors
	KeyedByTypeCollection_1_t2687594759 * ___behaviors_1;
	// System.Type System.ServiceModel.Description.ServiceDescription::service_type
	Type_t * ___service_type_2;

public:
	inline static int32_t get_offset_of_endpoints_0() { return static_cast<int32_t>(offsetof(ServiceDescription_t2946919347, ___endpoints_0)); }
	inline ServiceEndpointCollection_t3548009095 * get_endpoints_0() const { return ___endpoints_0; }
	inline ServiceEndpointCollection_t3548009095 ** get_address_of_endpoints_0() { return &___endpoints_0; }
	inline void set_endpoints_0(ServiceEndpointCollection_t3548009095 * value)
	{
		___endpoints_0 = value;
		Il2CppCodeGenWriteBarrier(&___endpoints_0, value);
	}

	inline static int32_t get_offset_of_behaviors_1() { return static_cast<int32_t>(offsetof(ServiceDescription_t2946919347, ___behaviors_1)); }
	inline KeyedByTypeCollection_1_t2687594759 * get_behaviors_1() const { return ___behaviors_1; }
	inline KeyedByTypeCollection_1_t2687594759 ** get_address_of_behaviors_1() { return &___behaviors_1; }
	inline void set_behaviors_1(KeyedByTypeCollection_1_t2687594759 * value)
	{
		___behaviors_1 = value;
		Il2CppCodeGenWriteBarrier(&___behaviors_1, value);
	}

	inline static int32_t get_offset_of_service_type_2() { return static_cast<int32_t>(offsetof(ServiceDescription_t2946919347, ___service_type_2)); }
	inline Type_t * get_service_type_2() const { return ___service_type_2; }
	inline Type_t ** get_address_of_service_type_2() { return &___service_type_2; }
	inline void set_service_type_2(Type_t * value)
	{
		___service_type_2 = value;
		Il2CppCodeGenWriteBarrier(&___service_type_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
