﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Object
struct Il2CppObject;
// System.Web.HttpApplicationFactory
struct HttpApplicationFactory_t4230706105;
// System.Type
struct Type_t;
// System.Web.HttpApplicationState
struct HttpApplicationState_t2618076950;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Collections.Stack
struct Stack_t2329662280;
// System.Web.Configuration.nBrowser.Build
struct Build_t486885285;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpApplicationFactory
struct  HttpApplicationFactory_t4230706105  : public Il2CppObject
{
public:
	// System.Object System.Web.HttpApplicationFactory::this_lock
	Il2CppObject * ___this_lock_0;
	// System.Object System.Web.HttpApplicationFactory::session_end
	Il2CppObject * ___session_end_2;
	// System.Boolean System.Web.HttpApplicationFactory::needs_init
	bool ___needs_init_3;
	// System.Boolean System.Web.HttpApplicationFactory::app_start_needed
	bool ___app_start_needed_4;
	// System.Boolean System.Web.HttpApplicationFactory::have_app_events
	bool ___have_app_events_5;
	// System.Type System.Web.HttpApplicationFactory::app_type
	Type_t * ___app_type_6;
	// System.Web.HttpApplicationState System.Web.HttpApplicationFactory::app_state
	HttpApplicationState_t2618076950 * ___app_state_7;
	// System.Collections.Hashtable System.Web.HttpApplicationFactory::app_event_handlers
	Hashtable_t1853889766 * ___app_event_handlers_8;
	// System.Collections.Stack System.Web.HttpApplicationFactory::available
	Stack_t2329662280 * ___available_16;
	// System.Object System.Web.HttpApplicationFactory::next_free
	Il2CppObject * ___next_free_17;
	// System.Collections.Stack System.Web.HttpApplicationFactory::available_for_end
	Stack_t2329662280 * ___available_for_end_18;

public:
	inline static int32_t get_offset_of_this_lock_0() { return static_cast<int32_t>(offsetof(HttpApplicationFactory_t4230706105, ___this_lock_0)); }
	inline Il2CppObject * get_this_lock_0() const { return ___this_lock_0; }
	inline Il2CppObject ** get_address_of_this_lock_0() { return &___this_lock_0; }
	inline void set_this_lock_0(Il2CppObject * value)
	{
		___this_lock_0 = value;
		Il2CppCodeGenWriteBarrier(&___this_lock_0, value);
	}

	inline static int32_t get_offset_of_session_end_2() { return static_cast<int32_t>(offsetof(HttpApplicationFactory_t4230706105, ___session_end_2)); }
	inline Il2CppObject * get_session_end_2() const { return ___session_end_2; }
	inline Il2CppObject ** get_address_of_session_end_2() { return &___session_end_2; }
	inline void set_session_end_2(Il2CppObject * value)
	{
		___session_end_2 = value;
		Il2CppCodeGenWriteBarrier(&___session_end_2, value);
	}

	inline static int32_t get_offset_of_needs_init_3() { return static_cast<int32_t>(offsetof(HttpApplicationFactory_t4230706105, ___needs_init_3)); }
	inline bool get_needs_init_3() const { return ___needs_init_3; }
	inline bool* get_address_of_needs_init_3() { return &___needs_init_3; }
	inline void set_needs_init_3(bool value)
	{
		___needs_init_3 = value;
	}

	inline static int32_t get_offset_of_app_start_needed_4() { return static_cast<int32_t>(offsetof(HttpApplicationFactory_t4230706105, ___app_start_needed_4)); }
	inline bool get_app_start_needed_4() const { return ___app_start_needed_4; }
	inline bool* get_address_of_app_start_needed_4() { return &___app_start_needed_4; }
	inline void set_app_start_needed_4(bool value)
	{
		___app_start_needed_4 = value;
	}

	inline static int32_t get_offset_of_have_app_events_5() { return static_cast<int32_t>(offsetof(HttpApplicationFactory_t4230706105, ___have_app_events_5)); }
	inline bool get_have_app_events_5() const { return ___have_app_events_5; }
	inline bool* get_address_of_have_app_events_5() { return &___have_app_events_5; }
	inline void set_have_app_events_5(bool value)
	{
		___have_app_events_5 = value;
	}

	inline static int32_t get_offset_of_app_type_6() { return static_cast<int32_t>(offsetof(HttpApplicationFactory_t4230706105, ___app_type_6)); }
	inline Type_t * get_app_type_6() const { return ___app_type_6; }
	inline Type_t ** get_address_of_app_type_6() { return &___app_type_6; }
	inline void set_app_type_6(Type_t * value)
	{
		___app_type_6 = value;
		Il2CppCodeGenWriteBarrier(&___app_type_6, value);
	}

	inline static int32_t get_offset_of_app_state_7() { return static_cast<int32_t>(offsetof(HttpApplicationFactory_t4230706105, ___app_state_7)); }
	inline HttpApplicationState_t2618076950 * get_app_state_7() const { return ___app_state_7; }
	inline HttpApplicationState_t2618076950 ** get_address_of_app_state_7() { return &___app_state_7; }
	inline void set_app_state_7(HttpApplicationState_t2618076950 * value)
	{
		___app_state_7 = value;
		Il2CppCodeGenWriteBarrier(&___app_state_7, value);
	}

	inline static int32_t get_offset_of_app_event_handlers_8() { return static_cast<int32_t>(offsetof(HttpApplicationFactory_t4230706105, ___app_event_handlers_8)); }
	inline Hashtable_t1853889766 * get_app_event_handlers_8() const { return ___app_event_handlers_8; }
	inline Hashtable_t1853889766 ** get_address_of_app_event_handlers_8() { return &___app_event_handlers_8; }
	inline void set_app_event_handlers_8(Hashtable_t1853889766 * value)
	{
		___app_event_handlers_8 = value;
		Il2CppCodeGenWriteBarrier(&___app_event_handlers_8, value);
	}

	inline static int32_t get_offset_of_available_16() { return static_cast<int32_t>(offsetof(HttpApplicationFactory_t4230706105, ___available_16)); }
	inline Stack_t2329662280 * get_available_16() const { return ___available_16; }
	inline Stack_t2329662280 ** get_address_of_available_16() { return &___available_16; }
	inline void set_available_16(Stack_t2329662280 * value)
	{
		___available_16 = value;
		Il2CppCodeGenWriteBarrier(&___available_16, value);
	}

	inline static int32_t get_offset_of_next_free_17() { return static_cast<int32_t>(offsetof(HttpApplicationFactory_t4230706105, ___next_free_17)); }
	inline Il2CppObject * get_next_free_17() const { return ___next_free_17; }
	inline Il2CppObject ** get_address_of_next_free_17() { return &___next_free_17; }
	inline void set_next_free_17(Il2CppObject * value)
	{
		___next_free_17 = value;
		Il2CppCodeGenWriteBarrier(&___next_free_17, value);
	}

	inline static int32_t get_offset_of_available_for_end_18() { return static_cast<int32_t>(offsetof(HttpApplicationFactory_t4230706105, ___available_for_end_18)); }
	inline Stack_t2329662280 * get_available_for_end_18() const { return ___available_for_end_18; }
	inline Stack_t2329662280 ** get_address_of_available_for_end_18() { return &___available_for_end_18; }
	inline void set_available_for_end_18(Stack_t2329662280 * value)
	{
		___available_for_end_18 = value;
		Il2CppCodeGenWriteBarrier(&___available_for_end_18, value);
	}
};

struct HttpApplicationFactory_t4230706105_StaticFields
{
public:
	// System.Web.HttpApplicationFactory System.Web.HttpApplicationFactory::theFactory
	HttpApplicationFactory_t4230706105 * ___theFactory_1;
	// System.Collections.ArrayList System.Web.HttpApplicationFactory::watchers
	ArrayList_t2718874744 * ___watchers_9;
	// System.Object System.Web.HttpApplicationFactory::watchers_lock
	Il2CppObject * ___watchers_lock_10;
	// System.Boolean System.Web.HttpApplicationFactory::app_shutdown
	bool ___app_shutdown_11;
	// System.Boolean System.Web.HttpApplicationFactory::app_disabled
	bool ___app_disabled_12;
	// System.String[] System.Web.HttpApplicationFactory::app_browsers_files
	StringU5BU5D_t1281789340* ___app_browsers_files_13;
	// System.String[] System.Web.HttpApplicationFactory::default_machine_browsers_files
	StringU5BU5D_t1281789340* ___default_machine_browsers_files_14;
	// System.String[] System.Web.HttpApplicationFactory::app_mono_machine_browsers_files
	StringU5BU5D_t1281789340* ___app_mono_machine_browsers_files_15;
	// System.Web.Configuration.nBrowser.Build System.Web.HttpApplicationFactory::capabilities_processor
	Build_t486885285 * ___capabilities_processor_19;
	// System.Object System.Web.HttpApplicationFactory::capabilities_processor_lock
	Il2CppObject * ___capabilities_processor_lock_20;

public:
	inline static int32_t get_offset_of_theFactory_1() { return static_cast<int32_t>(offsetof(HttpApplicationFactory_t4230706105_StaticFields, ___theFactory_1)); }
	inline HttpApplicationFactory_t4230706105 * get_theFactory_1() const { return ___theFactory_1; }
	inline HttpApplicationFactory_t4230706105 ** get_address_of_theFactory_1() { return &___theFactory_1; }
	inline void set_theFactory_1(HttpApplicationFactory_t4230706105 * value)
	{
		___theFactory_1 = value;
		Il2CppCodeGenWriteBarrier(&___theFactory_1, value);
	}

	inline static int32_t get_offset_of_watchers_9() { return static_cast<int32_t>(offsetof(HttpApplicationFactory_t4230706105_StaticFields, ___watchers_9)); }
	inline ArrayList_t2718874744 * get_watchers_9() const { return ___watchers_9; }
	inline ArrayList_t2718874744 ** get_address_of_watchers_9() { return &___watchers_9; }
	inline void set_watchers_9(ArrayList_t2718874744 * value)
	{
		___watchers_9 = value;
		Il2CppCodeGenWriteBarrier(&___watchers_9, value);
	}

	inline static int32_t get_offset_of_watchers_lock_10() { return static_cast<int32_t>(offsetof(HttpApplicationFactory_t4230706105_StaticFields, ___watchers_lock_10)); }
	inline Il2CppObject * get_watchers_lock_10() const { return ___watchers_lock_10; }
	inline Il2CppObject ** get_address_of_watchers_lock_10() { return &___watchers_lock_10; }
	inline void set_watchers_lock_10(Il2CppObject * value)
	{
		___watchers_lock_10 = value;
		Il2CppCodeGenWriteBarrier(&___watchers_lock_10, value);
	}

	inline static int32_t get_offset_of_app_shutdown_11() { return static_cast<int32_t>(offsetof(HttpApplicationFactory_t4230706105_StaticFields, ___app_shutdown_11)); }
	inline bool get_app_shutdown_11() const { return ___app_shutdown_11; }
	inline bool* get_address_of_app_shutdown_11() { return &___app_shutdown_11; }
	inline void set_app_shutdown_11(bool value)
	{
		___app_shutdown_11 = value;
	}

	inline static int32_t get_offset_of_app_disabled_12() { return static_cast<int32_t>(offsetof(HttpApplicationFactory_t4230706105_StaticFields, ___app_disabled_12)); }
	inline bool get_app_disabled_12() const { return ___app_disabled_12; }
	inline bool* get_address_of_app_disabled_12() { return &___app_disabled_12; }
	inline void set_app_disabled_12(bool value)
	{
		___app_disabled_12 = value;
	}

	inline static int32_t get_offset_of_app_browsers_files_13() { return static_cast<int32_t>(offsetof(HttpApplicationFactory_t4230706105_StaticFields, ___app_browsers_files_13)); }
	inline StringU5BU5D_t1281789340* get_app_browsers_files_13() const { return ___app_browsers_files_13; }
	inline StringU5BU5D_t1281789340** get_address_of_app_browsers_files_13() { return &___app_browsers_files_13; }
	inline void set_app_browsers_files_13(StringU5BU5D_t1281789340* value)
	{
		___app_browsers_files_13 = value;
		Il2CppCodeGenWriteBarrier(&___app_browsers_files_13, value);
	}

	inline static int32_t get_offset_of_default_machine_browsers_files_14() { return static_cast<int32_t>(offsetof(HttpApplicationFactory_t4230706105_StaticFields, ___default_machine_browsers_files_14)); }
	inline StringU5BU5D_t1281789340* get_default_machine_browsers_files_14() const { return ___default_machine_browsers_files_14; }
	inline StringU5BU5D_t1281789340** get_address_of_default_machine_browsers_files_14() { return &___default_machine_browsers_files_14; }
	inline void set_default_machine_browsers_files_14(StringU5BU5D_t1281789340* value)
	{
		___default_machine_browsers_files_14 = value;
		Il2CppCodeGenWriteBarrier(&___default_machine_browsers_files_14, value);
	}

	inline static int32_t get_offset_of_app_mono_machine_browsers_files_15() { return static_cast<int32_t>(offsetof(HttpApplicationFactory_t4230706105_StaticFields, ___app_mono_machine_browsers_files_15)); }
	inline StringU5BU5D_t1281789340* get_app_mono_machine_browsers_files_15() const { return ___app_mono_machine_browsers_files_15; }
	inline StringU5BU5D_t1281789340** get_address_of_app_mono_machine_browsers_files_15() { return &___app_mono_machine_browsers_files_15; }
	inline void set_app_mono_machine_browsers_files_15(StringU5BU5D_t1281789340* value)
	{
		___app_mono_machine_browsers_files_15 = value;
		Il2CppCodeGenWriteBarrier(&___app_mono_machine_browsers_files_15, value);
	}

	inline static int32_t get_offset_of_capabilities_processor_19() { return static_cast<int32_t>(offsetof(HttpApplicationFactory_t4230706105_StaticFields, ___capabilities_processor_19)); }
	inline Build_t486885285 * get_capabilities_processor_19() const { return ___capabilities_processor_19; }
	inline Build_t486885285 ** get_address_of_capabilities_processor_19() { return &___capabilities_processor_19; }
	inline void set_capabilities_processor_19(Build_t486885285 * value)
	{
		___capabilities_processor_19 = value;
		Il2CppCodeGenWriteBarrier(&___capabilities_processor_19, value);
	}

	inline static int32_t get_offset_of_capabilities_processor_lock_20() { return static_cast<int32_t>(offsetof(HttpApplicationFactory_t4230706105_StaticFields, ___capabilities_processor_lock_20)); }
	inline Il2CppObject * get_capabilities_processor_lock_20() const { return ___capabilities_processor_lock_20; }
	inline Il2CppObject ** get_address_of_capabilities_processor_lock_20() { return &___capabilities_processor_lock_20; }
	inline void set_capabilities_processor_lock_20(Il2CppObject * value)
	{
		___capabilities_processor_lock_20 = value;
		Il2CppCodeGenWriteBarrier(&___capabilities_processor_lock_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
