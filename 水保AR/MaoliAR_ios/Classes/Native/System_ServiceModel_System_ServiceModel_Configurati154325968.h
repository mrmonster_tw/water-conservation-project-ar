﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Configurati254180867.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.WSFederationHttpBindingElement
struct  WSFederationHttpBindingElement_t154325968  : public WSHttpBindingBaseElement_t254180867
{
public:

public:
};

struct WSFederationHttpBindingElement_t154325968_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.WSFederationHttpBindingElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_15;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.WSFederationHttpBindingElement::binding_element_type
	ConfigurationProperty_t3590861854 * ___binding_element_type_16;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.WSFederationHttpBindingElement::privacy_notice_at
	ConfigurationProperty_t3590861854 * ___privacy_notice_at_17;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.WSFederationHttpBindingElement::privacy_notice_version
	ConfigurationProperty_t3590861854 * ___privacy_notice_version_18;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.WSFederationHttpBindingElement::security
	ConfigurationProperty_t3590861854 * ___security_19;

public:
	inline static int32_t get_offset_of_properties_15() { return static_cast<int32_t>(offsetof(WSFederationHttpBindingElement_t154325968_StaticFields, ___properties_15)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_15() const { return ___properties_15; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_15() { return &___properties_15; }
	inline void set_properties_15(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_15 = value;
		Il2CppCodeGenWriteBarrier(&___properties_15, value);
	}

	inline static int32_t get_offset_of_binding_element_type_16() { return static_cast<int32_t>(offsetof(WSFederationHttpBindingElement_t154325968_StaticFields, ___binding_element_type_16)); }
	inline ConfigurationProperty_t3590861854 * get_binding_element_type_16() const { return ___binding_element_type_16; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_binding_element_type_16() { return &___binding_element_type_16; }
	inline void set_binding_element_type_16(ConfigurationProperty_t3590861854 * value)
	{
		___binding_element_type_16 = value;
		Il2CppCodeGenWriteBarrier(&___binding_element_type_16, value);
	}

	inline static int32_t get_offset_of_privacy_notice_at_17() { return static_cast<int32_t>(offsetof(WSFederationHttpBindingElement_t154325968_StaticFields, ___privacy_notice_at_17)); }
	inline ConfigurationProperty_t3590861854 * get_privacy_notice_at_17() const { return ___privacy_notice_at_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_privacy_notice_at_17() { return &___privacy_notice_at_17; }
	inline void set_privacy_notice_at_17(ConfigurationProperty_t3590861854 * value)
	{
		___privacy_notice_at_17 = value;
		Il2CppCodeGenWriteBarrier(&___privacy_notice_at_17, value);
	}

	inline static int32_t get_offset_of_privacy_notice_version_18() { return static_cast<int32_t>(offsetof(WSFederationHttpBindingElement_t154325968_StaticFields, ___privacy_notice_version_18)); }
	inline ConfigurationProperty_t3590861854 * get_privacy_notice_version_18() const { return ___privacy_notice_version_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_privacy_notice_version_18() { return &___privacy_notice_version_18; }
	inline void set_privacy_notice_version_18(ConfigurationProperty_t3590861854 * value)
	{
		___privacy_notice_version_18 = value;
		Il2CppCodeGenWriteBarrier(&___privacy_notice_version_18, value);
	}

	inline static int32_t get_offset_of_security_19() { return static_cast<int32_t>(offsetof(WSFederationHttpBindingElement_t154325968_StaticFields, ___security_19)); }
	inline ConfigurationProperty_t3590861854 * get_security_19() const { return ___security_19; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_security_19() { return &___security_19; }
	inline void set_security_19(ConfigurationProperty_t3590861854 * value)
	{
		___security_19 = value;
		Il2CppCodeGenWriteBarrier(&___security_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
