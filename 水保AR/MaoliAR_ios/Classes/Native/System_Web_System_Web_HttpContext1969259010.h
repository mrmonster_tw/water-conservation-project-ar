﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_DateTime3738529785.h"

// System.Web.HttpWorkerRequest
struct HttpWorkerRequest_t998871775;
// System.Web.HttpApplication
struct HttpApplication_t3359645917;
// System.Web.HttpRequest
struct HttpRequest_t809700260;
// System.Web.HttpResponse
struct HttpResponse_t1542361753;
// System.Web.SessionState.HttpSessionState
struct HttpSessionState_t3146594024;
// System.Web.HttpServerUtility
struct HttpServerUtility_t3850680178;
// System.Web.TraceContext
struct TraceContext_t3886895271;
// System.Web.IHttpHandler
struct IHttpHandler_t2624218893;
// System.String
struct String_t;
// System.Security.Principal.IPrincipal
struct IPrincipal_t2343618843;
// System.Object
struct Il2CppObject;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Threading.Timer
struct Timer_t716671026;
// System.Threading.Thread
struct Thread_t2300836069;
// System.Web.Compilation.ResourceProviderFactory
struct ResourceProviderFactory_t2982880633;
// System.Web.Compilation.DefaultResourceProviderFactory
struct DefaultResourceProviderFactory_t3181543722;
// System.Collections.Generic.Dictionary`2<System.String,System.Web.Compilation.IResourceProvider>
struct Dictionary_2_t17864734;
// System.Reflection.Assembly
struct Assembly_t4102432799;
// System.Collections.Generic.LinkedList`1<System.Web.IHttpHandler>
struct LinkedList_1_t1463864902;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpContext
struct  HttpContext_t1969259010  : public Il2CppObject
{
public:
	// System.Web.HttpWorkerRequest System.Web.HttpContext::WorkerRequest
	HttpWorkerRequest_t998871775 * ___WorkerRequest_0;
	// System.Web.HttpApplication System.Web.HttpContext::app_instance
	HttpApplication_t3359645917 * ___app_instance_1;
	// System.Web.HttpRequest System.Web.HttpContext::request
	HttpRequest_t809700260 * ___request_2;
	// System.Web.HttpResponse System.Web.HttpContext::response
	HttpResponse_t1542361753 * ___response_3;
	// System.Web.SessionState.HttpSessionState System.Web.HttpContext::session_state
	HttpSessionState_t3146594024 * ___session_state_4;
	// System.Web.HttpServerUtility System.Web.HttpContext::server
	HttpServerUtility_t3850680178 * ___server_5;
	// System.Web.TraceContext System.Web.HttpContext::trace_context
	TraceContext_t3886895271 * ___trace_context_6;
	// System.Web.IHttpHandler System.Web.HttpContext::handler
	Il2CppObject * ___handler_7;
	// System.String System.Web.HttpContext::error_page
	String_t* ___error_page_8;
	// System.Security.Principal.IPrincipal System.Web.HttpContext::user
	Il2CppObject * ___user_9;
	// System.Object System.Web.HttpContext::errors
	Il2CppObject * ___errors_10;
	// System.Collections.Hashtable System.Web.HttpContext::items
	Hashtable_t1853889766 * ___items_11;
	// System.Object System.Web.HttpContext::config_timeout
	Il2CppObject * ___config_timeout_12;
	// System.Int32 System.Web.HttpContext::timeout_possible
	int32_t ___timeout_possible_13;
	// System.DateTime System.Web.HttpContext::time_stamp
	DateTime_t3738529785  ___time_stamp_14;
	// System.Threading.Timer System.Web.HttpContext::timer
	Timer_t716671026 * ___timer_15;
	// System.Threading.Thread System.Web.HttpContext::thread
	Thread_t2300836069 * ___thread_16;
	// System.Boolean System.Web.HttpContext::_isProcessingInclude
	bool ____isProcessingInclude_17;
	// System.Collections.Generic.LinkedList`1<System.Web.IHttpHandler> System.Web.HttpContext::handlers
	LinkedList_1_t1463864902 * ___handlers_22;
	// System.Boolean System.Web.HttpContext::<MapRequestHandlerDone>k__BackingField
	bool ___U3CMapRequestHandlerDoneU3Ek__BackingField_23;

public:
	inline static int32_t get_offset_of_WorkerRequest_0() { return static_cast<int32_t>(offsetof(HttpContext_t1969259010, ___WorkerRequest_0)); }
	inline HttpWorkerRequest_t998871775 * get_WorkerRequest_0() const { return ___WorkerRequest_0; }
	inline HttpWorkerRequest_t998871775 ** get_address_of_WorkerRequest_0() { return &___WorkerRequest_0; }
	inline void set_WorkerRequest_0(HttpWorkerRequest_t998871775 * value)
	{
		___WorkerRequest_0 = value;
		Il2CppCodeGenWriteBarrier(&___WorkerRequest_0, value);
	}

	inline static int32_t get_offset_of_app_instance_1() { return static_cast<int32_t>(offsetof(HttpContext_t1969259010, ___app_instance_1)); }
	inline HttpApplication_t3359645917 * get_app_instance_1() const { return ___app_instance_1; }
	inline HttpApplication_t3359645917 ** get_address_of_app_instance_1() { return &___app_instance_1; }
	inline void set_app_instance_1(HttpApplication_t3359645917 * value)
	{
		___app_instance_1 = value;
		Il2CppCodeGenWriteBarrier(&___app_instance_1, value);
	}

	inline static int32_t get_offset_of_request_2() { return static_cast<int32_t>(offsetof(HttpContext_t1969259010, ___request_2)); }
	inline HttpRequest_t809700260 * get_request_2() const { return ___request_2; }
	inline HttpRequest_t809700260 ** get_address_of_request_2() { return &___request_2; }
	inline void set_request_2(HttpRequest_t809700260 * value)
	{
		___request_2 = value;
		Il2CppCodeGenWriteBarrier(&___request_2, value);
	}

	inline static int32_t get_offset_of_response_3() { return static_cast<int32_t>(offsetof(HttpContext_t1969259010, ___response_3)); }
	inline HttpResponse_t1542361753 * get_response_3() const { return ___response_3; }
	inline HttpResponse_t1542361753 ** get_address_of_response_3() { return &___response_3; }
	inline void set_response_3(HttpResponse_t1542361753 * value)
	{
		___response_3 = value;
		Il2CppCodeGenWriteBarrier(&___response_3, value);
	}

	inline static int32_t get_offset_of_session_state_4() { return static_cast<int32_t>(offsetof(HttpContext_t1969259010, ___session_state_4)); }
	inline HttpSessionState_t3146594024 * get_session_state_4() const { return ___session_state_4; }
	inline HttpSessionState_t3146594024 ** get_address_of_session_state_4() { return &___session_state_4; }
	inline void set_session_state_4(HttpSessionState_t3146594024 * value)
	{
		___session_state_4 = value;
		Il2CppCodeGenWriteBarrier(&___session_state_4, value);
	}

	inline static int32_t get_offset_of_server_5() { return static_cast<int32_t>(offsetof(HttpContext_t1969259010, ___server_5)); }
	inline HttpServerUtility_t3850680178 * get_server_5() const { return ___server_5; }
	inline HttpServerUtility_t3850680178 ** get_address_of_server_5() { return &___server_5; }
	inline void set_server_5(HttpServerUtility_t3850680178 * value)
	{
		___server_5 = value;
		Il2CppCodeGenWriteBarrier(&___server_5, value);
	}

	inline static int32_t get_offset_of_trace_context_6() { return static_cast<int32_t>(offsetof(HttpContext_t1969259010, ___trace_context_6)); }
	inline TraceContext_t3886895271 * get_trace_context_6() const { return ___trace_context_6; }
	inline TraceContext_t3886895271 ** get_address_of_trace_context_6() { return &___trace_context_6; }
	inline void set_trace_context_6(TraceContext_t3886895271 * value)
	{
		___trace_context_6 = value;
		Il2CppCodeGenWriteBarrier(&___trace_context_6, value);
	}

	inline static int32_t get_offset_of_handler_7() { return static_cast<int32_t>(offsetof(HttpContext_t1969259010, ___handler_7)); }
	inline Il2CppObject * get_handler_7() const { return ___handler_7; }
	inline Il2CppObject ** get_address_of_handler_7() { return &___handler_7; }
	inline void set_handler_7(Il2CppObject * value)
	{
		___handler_7 = value;
		Il2CppCodeGenWriteBarrier(&___handler_7, value);
	}

	inline static int32_t get_offset_of_error_page_8() { return static_cast<int32_t>(offsetof(HttpContext_t1969259010, ___error_page_8)); }
	inline String_t* get_error_page_8() const { return ___error_page_8; }
	inline String_t** get_address_of_error_page_8() { return &___error_page_8; }
	inline void set_error_page_8(String_t* value)
	{
		___error_page_8 = value;
		Il2CppCodeGenWriteBarrier(&___error_page_8, value);
	}

	inline static int32_t get_offset_of_user_9() { return static_cast<int32_t>(offsetof(HttpContext_t1969259010, ___user_9)); }
	inline Il2CppObject * get_user_9() const { return ___user_9; }
	inline Il2CppObject ** get_address_of_user_9() { return &___user_9; }
	inline void set_user_9(Il2CppObject * value)
	{
		___user_9 = value;
		Il2CppCodeGenWriteBarrier(&___user_9, value);
	}

	inline static int32_t get_offset_of_errors_10() { return static_cast<int32_t>(offsetof(HttpContext_t1969259010, ___errors_10)); }
	inline Il2CppObject * get_errors_10() const { return ___errors_10; }
	inline Il2CppObject ** get_address_of_errors_10() { return &___errors_10; }
	inline void set_errors_10(Il2CppObject * value)
	{
		___errors_10 = value;
		Il2CppCodeGenWriteBarrier(&___errors_10, value);
	}

	inline static int32_t get_offset_of_items_11() { return static_cast<int32_t>(offsetof(HttpContext_t1969259010, ___items_11)); }
	inline Hashtable_t1853889766 * get_items_11() const { return ___items_11; }
	inline Hashtable_t1853889766 ** get_address_of_items_11() { return &___items_11; }
	inline void set_items_11(Hashtable_t1853889766 * value)
	{
		___items_11 = value;
		Il2CppCodeGenWriteBarrier(&___items_11, value);
	}

	inline static int32_t get_offset_of_config_timeout_12() { return static_cast<int32_t>(offsetof(HttpContext_t1969259010, ___config_timeout_12)); }
	inline Il2CppObject * get_config_timeout_12() const { return ___config_timeout_12; }
	inline Il2CppObject ** get_address_of_config_timeout_12() { return &___config_timeout_12; }
	inline void set_config_timeout_12(Il2CppObject * value)
	{
		___config_timeout_12 = value;
		Il2CppCodeGenWriteBarrier(&___config_timeout_12, value);
	}

	inline static int32_t get_offset_of_timeout_possible_13() { return static_cast<int32_t>(offsetof(HttpContext_t1969259010, ___timeout_possible_13)); }
	inline int32_t get_timeout_possible_13() const { return ___timeout_possible_13; }
	inline int32_t* get_address_of_timeout_possible_13() { return &___timeout_possible_13; }
	inline void set_timeout_possible_13(int32_t value)
	{
		___timeout_possible_13 = value;
	}

	inline static int32_t get_offset_of_time_stamp_14() { return static_cast<int32_t>(offsetof(HttpContext_t1969259010, ___time_stamp_14)); }
	inline DateTime_t3738529785  get_time_stamp_14() const { return ___time_stamp_14; }
	inline DateTime_t3738529785 * get_address_of_time_stamp_14() { return &___time_stamp_14; }
	inline void set_time_stamp_14(DateTime_t3738529785  value)
	{
		___time_stamp_14 = value;
	}

	inline static int32_t get_offset_of_timer_15() { return static_cast<int32_t>(offsetof(HttpContext_t1969259010, ___timer_15)); }
	inline Timer_t716671026 * get_timer_15() const { return ___timer_15; }
	inline Timer_t716671026 ** get_address_of_timer_15() { return &___timer_15; }
	inline void set_timer_15(Timer_t716671026 * value)
	{
		___timer_15 = value;
		Il2CppCodeGenWriteBarrier(&___timer_15, value);
	}

	inline static int32_t get_offset_of_thread_16() { return static_cast<int32_t>(offsetof(HttpContext_t1969259010, ___thread_16)); }
	inline Thread_t2300836069 * get_thread_16() const { return ___thread_16; }
	inline Thread_t2300836069 ** get_address_of_thread_16() { return &___thread_16; }
	inline void set_thread_16(Thread_t2300836069 * value)
	{
		___thread_16 = value;
		Il2CppCodeGenWriteBarrier(&___thread_16, value);
	}

	inline static int32_t get_offset_of__isProcessingInclude_17() { return static_cast<int32_t>(offsetof(HttpContext_t1969259010, ____isProcessingInclude_17)); }
	inline bool get__isProcessingInclude_17() const { return ____isProcessingInclude_17; }
	inline bool* get_address_of__isProcessingInclude_17() { return &____isProcessingInclude_17; }
	inline void set__isProcessingInclude_17(bool value)
	{
		____isProcessingInclude_17 = value;
	}

	inline static int32_t get_offset_of_handlers_22() { return static_cast<int32_t>(offsetof(HttpContext_t1969259010, ___handlers_22)); }
	inline LinkedList_1_t1463864902 * get_handlers_22() const { return ___handlers_22; }
	inline LinkedList_1_t1463864902 ** get_address_of_handlers_22() { return &___handlers_22; }
	inline void set_handlers_22(LinkedList_1_t1463864902 * value)
	{
		___handlers_22 = value;
		Il2CppCodeGenWriteBarrier(&___handlers_22, value);
	}

	inline static int32_t get_offset_of_U3CMapRequestHandlerDoneU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(HttpContext_t1969259010, ___U3CMapRequestHandlerDoneU3Ek__BackingField_23)); }
	inline bool get_U3CMapRequestHandlerDoneU3Ek__BackingField_23() const { return ___U3CMapRequestHandlerDoneU3Ek__BackingField_23; }
	inline bool* get_address_of_U3CMapRequestHandlerDoneU3Ek__BackingField_23() { return &___U3CMapRequestHandlerDoneU3Ek__BackingField_23; }
	inline void set_U3CMapRequestHandlerDoneU3Ek__BackingField_23(bool value)
	{
		___U3CMapRequestHandlerDoneU3Ek__BackingField_23 = value;
	}
};

struct HttpContext_t1969259010_StaticFields
{
public:
	// System.Reflection.Assembly System.Web.HttpContext::AppGlobalResourcesAssembly
	Assembly_t4102432799 * ___AppGlobalResourcesAssembly_21;

public:
	inline static int32_t get_offset_of_AppGlobalResourcesAssembly_21() { return static_cast<int32_t>(offsetof(HttpContext_t1969259010_StaticFields, ___AppGlobalResourcesAssembly_21)); }
	inline Assembly_t4102432799 * get_AppGlobalResourcesAssembly_21() const { return ___AppGlobalResourcesAssembly_21; }
	inline Assembly_t4102432799 ** get_address_of_AppGlobalResourcesAssembly_21() { return &___AppGlobalResourcesAssembly_21; }
	inline void set_AppGlobalResourcesAssembly_21(Assembly_t4102432799 * value)
	{
		___AppGlobalResourcesAssembly_21 = value;
		Il2CppCodeGenWriteBarrier(&___AppGlobalResourcesAssembly_21, value);
	}
};

struct HttpContext_t1969259010_ThreadStaticFields
{
public:
	// System.Web.Compilation.ResourceProviderFactory System.Web.HttpContext::provider_factory
	ResourceProviderFactory_t2982880633 * ___provider_factory_18;
	// System.Web.Compilation.DefaultResourceProviderFactory System.Web.HttpContext::default_provider_factory
	DefaultResourceProviderFactory_t3181543722 * ___default_provider_factory_19;
	// System.Collections.Generic.Dictionary`2<System.String,System.Web.Compilation.IResourceProvider> System.Web.HttpContext::resource_providers
	Dictionary_2_t17864734 * ___resource_providers_20;

public:
	inline static int32_t get_offset_of_provider_factory_18() { return static_cast<int32_t>(offsetof(HttpContext_t1969259010_ThreadStaticFields, ___provider_factory_18)); }
	inline ResourceProviderFactory_t2982880633 * get_provider_factory_18() const { return ___provider_factory_18; }
	inline ResourceProviderFactory_t2982880633 ** get_address_of_provider_factory_18() { return &___provider_factory_18; }
	inline void set_provider_factory_18(ResourceProviderFactory_t2982880633 * value)
	{
		___provider_factory_18 = value;
		Il2CppCodeGenWriteBarrier(&___provider_factory_18, value);
	}

	inline static int32_t get_offset_of_default_provider_factory_19() { return static_cast<int32_t>(offsetof(HttpContext_t1969259010_ThreadStaticFields, ___default_provider_factory_19)); }
	inline DefaultResourceProviderFactory_t3181543722 * get_default_provider_factory_19() const { return ___default_provider_factory_19; }
	inline DefaultResourceProviderFactory_t3181543722 ** get_address_of_default_provider_factory_19() { return &___default_provider_factory_19; }
	inline void set_default_provider_factory_19(DefaultResourceProviderFactory_t3181543722 * value)
	{
		___default_provider_factory_19 = value;
		Il2CppCodeGenWriteBarrier(&___default_provider_factory_19, value);
	}

	inline static int32_t get_offset_of_resource_providers_20() { return static_cast<int32_t>(offsetof(HttpContext_t1969259010_ThreadStaticFields, ___resource_providers_20)); }
	inline Dictionary_2_t17864734 * get_resource_providers_20() const { return ___resource_providers_20; }
	inline Dictionary_2_t17864734 ** get_address_of_resource_providers_20() { return &___resource_providers_20; }
	inline void set_resource_providers_20(Dictionary_2_t17864734 * value)
	{
		___resource_providers_20 = value;
		Il2CppCodeGenWriteBarrier(&___resource_providers_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
