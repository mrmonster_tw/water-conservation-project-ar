﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.Web.QueueManager
struct QueueManager_t3850794979;
// System.Web.TraceManager
struct TraceManager_t157182125;
// System.Web.Caching.Cache
struct Cache_t4020976765;
// System.Threading.WaitCallback
struct WaitCallback_t2448485498;
// System.Exception
struct Exception_t1436737249;
// System.Object
struct Il2CppObject;
// System.String[]
struct StringU5BU5D_t1281789340;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpRuntime
struct  HttpRuntime_t1480753741  : public Il2CppObject
{
public:

public:
};

struct HttpRuntime_t1480753741_StaticFields
{
public:
	// System.Boolean System.Web.HttpRuntime::caseInsensitive
	bool ___caseInsensitive_0;
	// System.Boolean System.Web.HttpRuntime::runningOnWindows
	bool ___runningOnWindows_1;
	// System.Boolean System.Web.HttpRuntime::isunc
	bool ___isunc_2;
	// System.String System.Web.HttpRuntime::monoVersion
	String_t* ___monoVersion_3;
	// System.Boolean System.Web.HttpRuntime::domainUnloading
	bool ___domainUnloading_4;
	// System.Web.QueueManager System.Web.HttpRuntime::queue_manager
	QueueManager_t3850794979 * ___queue_manager_5;
	// System.Web.TraceManager System.Web.HttpRuntime::trace_manager
	TraceManager_t157182125 * ___trace_manager_6;
	// System.Web.Caching.Cache System.Web.HttpRuntime::cache
	Cache_t4020976765 * ___cache_7;
	// System.Web.Caching.Cache System.Web.HttpRuntime::internalCache
	Cache_t4020976765 * ___internalCache_8;
	// System.Threading.WaitCallback System.Web.HttpRuntime::do_RealProcessRequest
	WaitCallback_t2448485498 * ___do_RealProcessRequest_9;
	// System.Exception System.Web.HttpRuntime::initialException
	Exception_t1436737249 * ___initialException_10;
	// System.Boolean System.Web.HttpRuntime::firstRun
	bool ___firstRun_11;
	// System.Boolean System.Web.HttpRuntime::assemblyMappingEnabled
	bool ___assemblyMappingEnabled_12;
	// System.Object System.Web.HttpRuntime::assemblyMappingLock
	Il2CppObject * ___assemblyMappingLock_13;
	// System.Object System.Web.HttpRuntime::appOfflineLock
	Il2CppObject * ___appOfflineLock_14;
	// System.String System.Web.HttpRuntime::_actual_bin_directory
	String_t* ____actual_bin_directory_15;
	// System.String[] System.Web.HttpRuntime::app_offline_files
	StringU5BU5D_t1281789340* ___app_offline_files_16;
	// System.String System.Web.HttpRuntime::app_offline_file
	String_t* ___app_offline_file_17;
	// System.String System.Web.HttpRuntime::content503
	String_t* ___content503_18;

public:
	inline static int32_t get_offset_of_caseInsensitive_0() { return static_cast<int32_t>(offsetof(HttpRuntime_t1480753741_StaticFields, ___caseInsensitive_0)); }
	inline bool get_caseInsensitive_0() const { return ___caseInsensitive_0; }
	inline bool* get_address_of_caseInsensitive_0() { return &___caseInsensitive_0; }
	inline void set_caseInsensitive_0(bool value)
	{
		___caseInsensitive_0 = value;
	}

	inline static int32_t get_offset_of_runningOnWindows_1() { return static_cast<int32_t>(offsetof(HttpRuntime_t1480753741_StaticFields, ___runningOnWindows_1)); }
	inline bool get_runningOnWindows_1() const { return ___runningOnWindows_1; }
	inline bool* get_address_of_runningOnWindows_1() { return &___runningOnWindows_1; }
	inline void set_runningOnWindows_1(bool value)
	{
		___runningOnWindows_1 = value;
	}

	inline static int32_t get_offset_of_isunc_2() { return static_cast<int32_t>(offsetof(HttpRuntime_t1480753741_StaticFields, ___isunc_2)); }
	inline bool get_isunc_2() const { return ___isunc_2; }
	inline bool* get_address_of_isunc_2() { return &___isunc_2; }
	inline void set_isunc_2(bool value)
	{
		___isunc_2 = value;
	}

	inline static int32_t get_offset_of_monoVersion_3() { return static_cast<int32_t>(offsetof(HttpRuntime_t1480753741_StaticFields, ___monoVersion_3)); }
	inline String_t* get_monoVersion_3() const { return ___monoVersion_3; }
	inline String_t** get_address_of_monoVersion_3() { return &___monoVersion_3; }
	inline void set_monoVersion_3(String_t* value)
	{
		___monoVersion_3 = value;
		Il2CppCodeGenWriteBarrier(&___monoVersion_3, value);
	}

	inline static int32_t get_offset_of_domainUnloading_4() { return static_cast<int32_t>(offsetof(HttpRuntime_t1480753741_StaticFields, ___domainUnloading_4)); }
	inline bool get_domainUnloading_4() const { return ___domainUnloading_4; }
	inline bool* get_address_of_domainUnloading_4() { return &___domainUnloading_4; }
	inline void set_domainUnloading_4(bool value)
	{
		___domainUnloading_4 = value;
	}

	inline static int32_t get_offset_of_queue_manager_5() { return static_cast<int32_t>(offsetof(HttpRuntime_t1480753741_StaticFields, ___queue_manager_5)); }
	inline QueueManager_t3850794979 * get_queue_manager_5() const { return ___queue_manager_5; }
	inline QueueManager_t3850794979 ** get_address_of_queue_manager_5() { return &___queue_manager_5; }
	inline void set_queue_manager_5(QueueManager_t3850794979 * value)
	{
		___queue_manager_5 = value;
		Il2CppCodeGenWriteBarrier(&___queue_manager_5, value);
	}

	inline static int32_t get_offset_of_trace_manager_6() { return static_cast<int32_t>(offsetof(HttpRuntime_t1480753741_StaticFields, ___trace_manager_6)); }
	inline TraceManager_t157182125 * get_trace_manager_6() const { return ___trace_manager_6; }
	inline TraceManager_t157182125 ** get_address_of_trace_manager_6() { return &___trace_manager_6; }
	inline void set_trace_manager_6(TraceManager_t157182125 * value)
	{
		___trace_manager_6 = value;
		Il2CppCodeGenWriteBarrier(&___trace_manager_6, value);
	}

	inline static int32_t get_offset_of_cache_7() { return static_cast<int32_t>(offsetof(HttpRuntime_t1480753741_StaticFields, ___cache_7)); }
	inline Cache_t4020976765 * get_cache_7() const { return ___cache_7; }
	inline Cache_t4020976765 ** get_address_of_cache_7() { return &___cache_7; }
	inline void set_cache_7(Cache_t4020976765 * value)
	{
		___cache_7 = value;
		Il2CppCodeGenWriteBarrier(&___cache_7, value);
	}

	inline static int32_t get_offset_of_internalCache_8() { return static_cast<int32_t>(offsetof(HttpRuntime_t1480753741_StaticFields, ___internalCache_8)); }
	inline Cache_t4020976765 * get_internalCache_8() const { return ___internalCache_8; }
	inline Cache_t4020976765 ** get_address_of_internalCache_8() { return &___internalCache_8; }
	inline void set_internalCache_8(Cache_t4020976765 * value)
	{
		___internalCache_8 = value;
		Il2CppCodeGenWriteBarrier(&___internalCache_8, value);
	}

	inline static int32_t get_offset_of_do_RealProcessRequest_9() { return static_cast<int32_t>(offsetof(HttpRuntime_t1480753741_StaticFields, ___do_RealProcessRequest_9)); }
	inline WaitCallback_t2448485498 * get_do_RealProcessRequest_9() const { return ___do_RealProcessRequest_9; }
	inline WaitCallback_t2448485498 ** get_address_of_do_RealProcessRequest_9() { return &___do_RealProcessRequest_9; }
	inline void set_do_RealProcessRequest_9(WaitCallback_t2448485498 * value)
	{
		___do_RealProcessRequest_9 = value;
		Il2CppCodeGenWriteBarrier(&___do_RealProcessRequest_9, value);
	}

	inline static int32_t get_offset_of_initialException_10() { return static_cast<int32_t>(offsetof(HttpRuntime_t1480753741_StaticFields, ___initialException_10)); }
	inline Exception_t1436737249 * get_initialException_10() const { return ___initialException_10; }
	inline Exception_t1436737249 ** get_address_of_initialException_10() { return &___initialException_10; }
	inline void set_initialException_10(Exception_t1436737249 * value)
	{
		___initialException_10 = value;
		Il2CppCodeGenWriteBarrier(&___initialException_10, value);
	}

	inline static int32_t get_offset_of_firstRun_11() { return static_cast<int32_t>(offsetof(HttpRuntime_t1480753741_StaticFields, ___firstRun_11)); }
	inline bool get_firstRun_11() const { return ___firstRun_11; }
	inline bool* get_address_of_firstRun_11() { return &___firstRun_11; }
	inline void set_firstRun_11(bool value)
	{
		___firstRun_11 = value;
	}

	inline static int32_t get_offset_of_assemblyMappingEnabled_12() { return static_cast<int32_t>(offsetof(HttpRuntime_t1480753741_StaticFields, ___assemblyMappingEnabled_12)); }
	inline bool get_assemblyMappingEnabled_12() const { return ___assemblyMappingEnabled_12; }
	inline bool* get_address_of_assemblyMappingEnabled_12() { return &___assemblyMappingEnabled_12; }
	inline void set_assemblyMappingEnabled_12(bool value)
	{
		___assemblyMappingEnabled_12 = value;
	}

	inline static int32_t get_offset_of_assemblyMappingLock_13() { return static_cast<int32_t>(offsetof(HttpRuntime_t1480753741_StaticFields, ___assemblyMappingLock_13)); }
	inline Il2CppObject * get_assemblyMappingLock_13() const { return ___assemblyMappingLock_13; }
	inline Il2CppObject ** get_address_of_assemblyMappingLock_13() { return &___assemblyMappingLock_13; }
	inline void set_assemblyMappingLock_13(Il2CppObject * value)
	{
		___assemblyMappingLock_13 = value;
		Il2CppCodeGenWriteBarrier(&___assemblyMappingLock_13, value);
	}

	inline static int32_t get_offset_of_appOfflineLock_14() { return static_cast<int32_t>(offsetof(HttpRuntime_t1480753741_StaticFields, ___appOfflineLock_14)); }
	inline Il2CppObject * get_appOfflineLock_14() const { return ___appOfflineLock_14; }
	inline Il2CppObject ** get_address_of_appOfflineLock_14() { return &___appOfflineLock_14; }
	inline void set_appOfflineLock_14(Il2CppObject * value)
	{
		___appOfflineLock_14 = value;
		Il2CppCodeGenWriteBarrier(&___appOfflineLock_14, value);
	}

	inline static int32_t get_offset_of__actual_bin_directory_15() { return static_cast<int32_t>(offsetof(HttpRuntime_t1480753741_StaticFields, ____actual_bin_directory_15)); }
	inline String_t* get__actual_bin_directory_15() const { return ____actual_bin_directory_15; }
	inline String_t** get_address_of__actual_bin_directory_15() { return &____actual_bin_directory_15; }
	inline void set__actual_bin_directory_15(String_t* value)
	{
		____actual_bin_directory_15 = value;
		Il2CppCodeGenWriteBarrier(&____actual_bin_directory_15, value);
	}

	inline static int32_t get_offset_of_app_offline_files_16() { return static_cast<int32_t>(offsetof(HttpRuntime_t1480753741_StaticFields, ___app_offline_files_16)); }
	inline StringU5BU5D_t1281789340* get_app_offline_files_16() const { return ___app_offline_files_16; }
	inline StringU5BU5D_t1281789340** get_address_of_app_offline_files_16() { return &___app_offline_files_16; }
	inline void set_app_offline_files_16(StringU5BU5D_t1281789340* value)
	{
		___app_offline_files_16 = value;
		Il2CppCodeGenWriteBarrier(&___app_offline_files_16, value);
	}

	inline static int32_t get_offset_of_app_offline_file_17() { return static_cast<int32_t>(offsetof(HttpRuntime_t1480753741_StaticFields, ___app_offline_file_17)); }
	inline String_t* get_app_offline_file_17() const { return ___app_offline_file_17; }
	inline String_t** get_address_of_app_offline_file_17() { return &___app_offline_file_17; }
	inline void set_app_offline_file_17(String_t* value)
	{
		___app_offline_file_17 = value;
		Il2CppCodeGenWriteBarrier(&___app_offline_file_17, value);
	}

	inline static int32_t get_offset_of_content503_18() { return static_cast<int32_t>(offsetof(HttpRuntime_t1480753741_StaticFields, ___content503_18)); }
	inline String_t* get_content503_18() const { return ___content503_18; }
	inline String_t** get_address_of_content503_18() { return &___content503_18; }
	inline void set_content503_18(String_t* value)
	{
		___content503_18 = value;
		Il2CppCodeGenWriteBarrier(&___content503_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
