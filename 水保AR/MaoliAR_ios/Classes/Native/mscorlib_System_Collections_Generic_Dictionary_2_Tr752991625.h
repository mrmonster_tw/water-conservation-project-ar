﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_MulticastDelegate157516450.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24262468432.h"

// System.String
struct String_t;
// System.ServiceModel.MessageSecurityVersion
struct MessageSecurityVersion_t2079539966;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.ServiceModel.MessageSecurityVersion,System.Collections.Generic.KeyValuePair`2<System.String,System.ServiceModel.MessageSecurityVersion>>
struct  Transform_1_t752991625  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
