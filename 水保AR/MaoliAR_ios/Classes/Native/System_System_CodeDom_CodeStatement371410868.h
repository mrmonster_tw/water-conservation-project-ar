﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeObject3927604602.h"

// System.CodeDom.CodeLinePragma
struct CodeLinePragma_t3085002198;
// System.CodeDom.CodeDirectiveCollection
struct CodeDirectiveCollection_t1153190035;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeStatement
struct  CodeStatement_t371410868  : public CodeObject_t3927604602
{
public:
	// System.CodeDom.CodeLinePragma System.CodeDom.CodeStatement::linePragma
	CodeLinePragma_t3085002198 * ___linePragma_1;
	// System.CodeDom.CodeDirectiveCollection System.CodeDom.CodeStatement::endDirectives
	CodeDirectiveCollection_t1153190035 * ___endDirectives_2;
	// System.CodeDom.CodeDirectiveCollection System.CodeDom.CodeStatement::startDirectives
	CodeDirectiveCollection_t1153190035 * ___startDirectives_3;

public:
	inline static int32_t get_offset_of_linePragma_1() { return static_cast<int32_t>(offsetof(CodeStatement_t371410868, ___linePragma_1)); }
	inline CodeLinePragma_t3085002198 * get_linePragma_1() const { return ___linePragma_1; }
	inline CodeLinePragma_t3085002198 ** get_address_of_linePragma_1() { return &___linePragma_1; }
	inline void set_linePragma_1(CodeLinePragma_t3085002198 * value)
	{
		___linePragma_1 = value;
		Il2CppCodeGenWriteBarrier(&___linePragma_1, value);
	}

	inline static int32_t get_offset_of_endDirectives_2() { return static_cast<int32_t>(offsetof(CodeStatement_t371410868, ___endDirectives_2)); }
	inline CodeDirectiveCollection_t1153190035 * get_endDirectives_2() const { return ___endDirectives_2; }
	inline CodeDirectiveCollection_t1153190035 ** get_address_of_endDirectives_2() { return &___endDirectives_2; }
	inline void set_endDirectives_2(CodeDirectiveCollection_t1153190035 * value)
	{
		___endDirectives_2 = value;
		Il2CppCodeGenWriteBarrier(&___endDirectives_2, value);
	}

	inline static int32_t get_offset_of_startDirectives_3() { return static_cast<int32_t>(offsetof(CodeStatement_t371410868, ___startDirectives_3)); }
	inline CodeDirectiveCollection_t1153190035 * get_startDirectives_3() const { return ___startDirectives_3; }
	inline CodeDirectiveCollection_t1153190035 ** get_address_of_startDirectives_3() { return &___startDirectives_3; }
	inline void set_startDirectives_3(CodeDirectiveCollection_t1153190035 * value)
	{
		___startDirectives_3 = value;
		Il2CppCodeGenWriteBarrier(&___startDirectives_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
