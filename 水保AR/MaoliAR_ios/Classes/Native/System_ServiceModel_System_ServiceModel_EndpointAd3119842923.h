﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Uri
struct Uri_t100236324;
// System.ServiceModel.Channels.AddressHeaderCollection
struct AddressHeaderCollection_t331929339;
// System.ServiceModel.EndpointIdentity
struct EndpointIdentity_t3899230012;
// System.Xml.XmlDictionaryReader
struct XmlDictionaryReader_t1044334689;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.EndpointAddress
struct  EndpointAddress_t3119842923  : public Il2CppObject
{
public:
	// System.Uri System.ServiceModel.EndpointAddress::address
	Uri_t100236324 * ___address_3;
	// System.ServiceModel.Channels.AddressHeaderCollection System.ServiceModel.EndpointAddress::headers
	AddressHeaderCollection_t331929339 * ___headers_4;
	// System.ServiceModel.EndpointIdentity System.ServiceModel.EndpointAddress::identity
	EndpointIdentity_t3899230012 * ___identity_5;
	// System.Xml.XmlDictionaryReader System.ServiceModel.EndpointAddress::metadata_reader
	XmlDictionaryReader_t1044334689 * ___metadata_reader_6;
	// System.Xml.XmlDictionaryReader System.ServiceModel.EndpointAddress::extension_reader
	XmlDictionaryReader_t1044334689 * ___extension_reader_7;

public:
	inline static int32_t get_offset_of_address_3() { return static_cast<int32_t>(offsetof(EndpointAddress_t3119842923, ___address_3)); }
	inline Uri_t100236324 * get_address_3() const { return ___address_3; }
	inline Uri_t100236324 ** get_address_of_address_3() { return &___address_3; }
	inline void set_address_3(Uri_t100236324 * value)
	{
		___address_3 = value;
		Il2CppCodeGenWriteBarrier(&___address_3, value);
	}

	inline static int32_t get_offset_of_headers_4() { return static_cast<int32_t>(offsetof(EndpointAddress_t3119842923, ___headers_4)); }
	inline AddressHeaderCollection_t331929339 * get_headers_4() const { return ___headers_4; }
	inline AddressHeaderCollection_t331929339 ** get_address_of_headers_4() { return &___headers_4; }
	inline void set_headers_4(AddressHeaderCollection_t331929339 * value)
	{
		___headers_4 = value;
		Il2CppCodeGenWriteBarrier(&___headers_4, value);
	}

	inline static int32_t get_offset_of_identity_5() { return static_cast<int32_t>(offsetof(EndpointAddress_t3119842923, ___identity_5)); }
	inline EndpointIdentity_t3899230012 * get_identity_5() const { return ___identity_5; }
	inline EndpointIdentity_t3899230012 ** get_address_of_identity_5() { return &___identity_5; }
	inline void set_identity_5(EndpointIdentity_t3899230012 * value)
	{
		___identity_5 = value;
		Il2CppCodeGenWriteBarrier(&___identity_5, value);
	}

	inline static int32_t get_offset_of_metadata_reader_6() { return static_cast<int32_t>(offsetof(EndpointAddress_t3119842923, ___metadata_reader_6)); }
	inline XmlDictionaryReader_t1044334689 * get_metadata_reader_6() const { return ___metadata_reader_6; }
	inline XmlDictionaryReader_t1044334689 ** get_address_of_metadata_reader_6() { return &___metadata_reader_6; }
	inline void set_metadata_reader_6(XmlDictionaryReader_t1044334689 * value)
	{
		___metadata_reader_6 = value;
		Il2CppCodeGenWriteBarrier(&___metadata_reader_6, value);
	}

	inline static int32_t get_offset_of_extension_reader_7() { return static_cast<int32_t>(offsetof(EndpointAddress_t3119842923, ___extension_reader_7)); }
	inline XmlDictionaryReader_t1044334689 * get_extension_reader_7() const { return ___extension_reader_7; }
	inline XmlDictionaryReader_t1044334689 ** get_address_of_extension_reader_7() { return &___extension_reader_7; }
	inline void set_extension_reader_7(XmlDictionaryReader_t1044334689 * value)
	{
		___extension_reader_7 = value;
		Il2CppCodeGenWriteBarrier(&___extension_reader_7, value);
	}
};

struct EndpointAddress_t3119842923_StaticFields
{
public:
	// System.Uri System.ServiceModel.EndpointAddress::w3c_anonymous
	Uri_t100236324 * ___w3c_anonymous_0;
	// System.Uri System.ServiceModel.EndpointAddress::anonymous_role
	Uri_t100236324 * ___anonymous_role_1;
	// System.Uri System.ServiceModel.EndpointAddress::none_role
	Uri_t100236324 * ___none_role_2;

public:
	inline static int32_t get_offset_of_w3c_anonymous_0() { return static_cast<int32_t>(offsetof(EndpointAddress_t3119842923_StaticFields, ___w3c_anonymous_0)); }
	inline Uri_t100236324 * get_w3c_anonymous_0() const { return ___w3c_anonymous_0; }
	inline Uri_t100236324 ** get_address_of_w3c_anonymous_0() { return &___w3c_anonymous_0; }
	inline void set_w3c_anonymous_0(Uri_t100236324 * value)
	{
		___w3c_anonymous_0 = value;
		Il2CppCodeGenWriteBarrier(&___w3c_anonymous_0, value);
	}

	inline static int32_t get_offset_of_anonymous_role_1() { return static_cast<int32_t>(offsetof(EndpointAddress_t3119842923_StaticFields, ___anonymous_role_1)); }
	inline Uri_t100236324 * get_anonymous_role_1() const { return ___anonymous_role_1; }
	inline Uri_t100236324 ** get_address_of_anonymous_role_1() { return &___anonymous_role_1; }
	inline void set_anonymous_role_1(Uri_t100236324 * value)
	{
		___anonymous_role_1 = value;
		Il2CppCodeGenWriteBarrier(&___anonymous_role_1, value);
	}

	inline static int32_t get_offset_of_none_role_2() { return static_cast<int32_t>(offsetof(EndpointAddress_t3119842923_StaticFields, ___none_role_2)); }
	inline Uri_t100236324 * get_none_role_2() const { return ___none_role_2; }
	inline Uri_t100236324 ** get_address_of_none_role_2() { return &___none_role_2; }
	inline void set_none_role_2(Uri_t100236324 * value)
	{
		___none_role_2 = value;
		Il2CppCodeGenWriteBarrier(&___none_role_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
