﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.WsaEndpointReference
struct  WsaEndpointReference_t4192482636  : public Il2CppObject
{
public:
	// System.String System.ServiceModel.Description.WsaEndpointReference::Address
	String_t* ___Address_0;

public:
	inline static int32_t get_offset_of_Address_0() { return static_cast<int32_t>(offsetof(WsaEndpointReference_t4192482636, ___Address_0)); }
	inline String_t* get_Address_0() const { return ___Address_0; }
	inline String_t** get_address_of_Address_0() { return &___Address_0; }
	inline void set_Address_0(String_t* value)
	{
		___Address_0 = value;
		Il2CppCodeGenWriteBarrier(&___Address_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
