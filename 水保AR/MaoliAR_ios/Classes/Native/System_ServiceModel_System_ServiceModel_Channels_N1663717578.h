﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.IO.Pipes.NamedPipeServerStream
struct NamedPipeServerStream_t125618231;
// System.ServiceModel.Channels.NamedPipeChannelListener`1<System.Object>
struct NamedPipeChannelListener_1_t2232250219;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.NamedPipeChannelListener`1/<OnAcceptChannel>c__AnonStoreyE<System.Object>
struct  U3COnAcceptChannelU3Ec__AnonStoreyE_t1663717578  : public Il2CppObject
{
public:
	// System.IO.Pipes.NamedPipeServerStream System.ServiceModel.Channels.NamedPipeChannelListener`1/<OnAcceptChannel>c__AnonStoreyE::server
	NamedPipeServerStream_t125618231 * ___server_0;
	// System.ServiceModel.Channels.NamedPipeChannelListener`1<TChannel> System.ServiceModel.Channels.NamedPipeChannelListener`1/<OnAcceptChannel>c__AnonStoreyE::<>f__this
	NamedPipeChannelListener_1_t2232250219 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_server_0() { return static_cast<int32_t>(offsetof(U3COnAcceptChannelU3Ec__AnonStoreyE_t1663717578, ___server_0)); }
	inline NamedPipeServerStream_t125618231 * get_server_0() const { return ___server_0; }
	inline NamedPipeServerStream_t125618231 ** get_address_of_server_0() { return &___server_0; }
	inline void set_server_0(NamedPipeServerStream_t125618231 * value)
	{
		___server_0 = value;
		Il2CppCodeGenWriteBarrier(&___server_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3COnAcceptChannelU3Ec__AnonStoreyE_t1663717578, ___U3CU3Ef__this_1)); }
	inline NamedPipeChannelListener_1_t2232250219 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline NamedPipeChannelListener_1_t2232250219 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(NamedPipeChannelListener_1_t2232250219 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
