﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.ProfilePropertySettings
struct  ProfilePropertySettings_t3170835156  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct ProfilePropertySettings_t3170835156_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Web.Configuration.ProfilePropertySettings::allowAnonymousProp
	ConfigurationProperty_t3590861854 * ___allowAnonymousProp_13;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.ProfilePropertySettings::customProviderDataProp
	ConfigurationProperty_t3590861854 * ___customProviderDataProp_14;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.ProfilePropertySettings::defaultValueProp
	ConfigurationProperty_t3590861854 * ___defaultValueProp_15;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.ProfilePropertySettings::nameProp
	ConfigurationProperty_t3590861854 * ___nameProp_16;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.ProfilePropertySettings::providerProp
	ConfigurationProperty_t3590861854 * ___providerProp_17;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.ProfilePropertySettings::readOnlyProp
	ConfigurationProperty_t3590861854 * ___readOnlyProp_18;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.ProfilePropertySettings::serializeAsProp
	ConfigurationProperty_t3590861854 * ___serializeAsProp_19;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.ProfilePropertySettings::typeProp
	ConfigurationProperty_t3590861854 * ___typeProp_20;
	// System.Configuration.ConfigurationPropertyCollection System.Web.Configuration.ProfilePropertySettings::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_21;

public:
	inline static int32_t get_offset_of_allowAnonymousProp_13() { return static_cast<int32_t>(offsetof(ProfilePropertySettings_t3170835156_StaticFields, ___allowAnonymousProp_13)); }
	inline ConfigurationProperty_t3590861854 * get_allowAnonymousProp_13() const { return ___allowAnonymousProp_13; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_allowAnonymousProp_13() { return &___allowAnonymousProp_13; }
	inline void set_allowAnonymousProp_13(ConfigurationProperty_t3590861854 * value)
	{
		___allowAnonymousProp_13 = value;
		Il2CppCodeGenWriteBarrier(&___allowAnonymousProp_13, value);
	}

	inline static int32_t get_offset_of_customProviderDataProp_14() { return static_cast<int32_t>(offsetof(ProfilePropertySettings_t3170835156_StaticFields, ___customProviderDataProp_14)); }
	inline ConfigurationProperty_t3590861854 * get_customProviderDataProp_14() const { return ___customProviderDataProp_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_customProviderDataProp_14() { return &___customProviderDataProp_14; }
	inline void set_customProviderDataProp_14(ConfigurationProperty_t3590861854 * value)
	{
		___customProviderDataProp_14 = value;
		Il2CppCodeGenWriteBarrier(&___customProviderDataProp_14, value);
	}

	inline static int32_t get_offset_of_defaultValueProp_15() { return static_cast<int32_t>(offsetof(ProfilePropertySettings_t3170835156_StaticFields, ___defaultValueProp_15)); }
	inline ConfigurationProperty_t3590861854 * get_defaultValueProp_15() const { return ___defaultValueProp_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_defaultValueProp_15() { return &___defaultValueProp_15; }
	inline void set_defaultValueProp_15(ConfigurationProperty_t3590861854 * value)
	{
		___defaultValueProp_15 = value;
		Il2CppCodeGenWriteBarrier(&___defaultValueProp_15, value);
	}

	inline static int32_t get_offset_of_nameProp_16() { return static_cast<int32_t>(offsetof(ProfilePropertySettings_t3170835156_StaticFields, ___nameProp_16)); }
	inline ConfigurationProperty_t3590861854 * get_nameProp_16() const { return ___nameProp_16; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_nameProp_16() { return &___nameProp_16; }
	inline void set_nameProp_16(ConfigurationProperty_t3590861854 * value)
	{
		___nameProp_16 = value;
		Il2CppCodeGenWriteBarrier(&___nameProp_16, value);
	}

	inline static int32_t get_offset_of_providerProp_17() { return static_cast<int32_t>(offsetof(ProfilePropertySettings_t3170835156_StaticFields, ___providerProp_17)); }
	inline ConfigurationProperty_t3590861854 * get_providerProp_17() const { return ___providerProp_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_providerProp_17() { return &___providerProp_17; }
	inline void set_providerProp_17(ConfigurationProperty_t3590861854 * value)
	{
		___providerProp_17 = value;
		Il2CppCodeGenWriteBarrier(&___providerProp_17, value);
	}

	inline static int32_t get_offset_of_readOnlyProp_18() { return static_cast<int32_t>(offsetof(ProfilePropertySettings_t3170835156_StaticFields, ___readOnlyProp_18)); }
	inline ConfigurationProperty_t3590861854 * get_readOnlyProp_18() const { return ___readOnlyProp_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_readOnlyProp_18() { return &___readOnlyProp_18; }
	inline void set_readOnlyProp_18(ConfigurationProperty_t3590861854 * value)
	{
		___readOnlyProp_18 = value;
		Il2CppCodeGenWriteBarrier(&___readOnlyProp_18, value);
	}

	inline static int32_t get_offset_of_serializeAsProp_19() { return static_cast<int32_t>(offsetof(ProfilePropertySettings_t3170835156_StaticFields, ___serializeAsProp_19)); }
	inline ConfigurationProperty_t3590861854 * get_serializeAsProp_19() const { return ___serializeAsProp_19; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_serializeAsProp_19() { return &___serializeAsProp_19; }
	inline void set_serializeAsProp_19(ConfigurationProperty_t3590861854 * value)
	{
		___serializeAsProp_19 = value;
		Il2CppCodeGenWriteBarrier(&___serializeAsProp_19, value);
	}

	inline static int32_t get_offset_of_typeProp_20() { return static_cast<int32_t>(offsetof(ProfilePropertySettings_t3170835156_StaticFields, ___typeProp_20)); }
	inline ConfigurationProperty_t3590861854 * get_typeProp_20() const { return ___typeProp_20; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_typeProp_20() { return &___typeProp_20; }
	inline void set_typeProp_20(ConfigurationProperty_t3590861854 * value)
	{
		___typeProp_20 = value;
		Il2CppCodeGenWriteBarrier(&___typeProp_20, value);
	}

	inline static int32_t get_offset_of_properties_21() { return static_cast<int32_t>(offsetof(ProfilePropertySettings_t3170835156_StaticFields, ___properties_21)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_21() const { return ___properties_21; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_21() { return &___properties_21; }
	inline void set_properties_21(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_21 = value;
		Il2CppCodeGenWriteBarrier(&___properties_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
