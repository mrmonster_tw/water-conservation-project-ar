﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.UI.ControlSkinDelegate
struct ControlSkinDelegate_t1165826654;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.ControlSkin
struct  ControlSkin_t212484848  : public Il2CppObject
{
public:
	// System.Web.UI.ControlSkinDelegate System.Web.UI.ControlSkin::themeDelegate
	ControlSkinDelegate_t1165826654 * ___themeDelegate_0;

public:
	inline static int32_t get_offset_of_themeDelegate_0() { return static_cast<int32_t>(offsetof(ControlSkin_t212484848, ___themeDelegate_0)); }
	inline ControlSkinDelegate_t1165826654 * get_themeDelegate_0() const { return ___themeDelegate_0; }
	inline ControlSkinDelegate_t1165826654 ** get_address_of_themeDelegate_0() { return &___themeDelegate_0; }
	inline void set_themeDelegate_0(ControlSkinDelegate_t1165826654 * value)
	{
		___themeDelegate_0 = value;
		Il2CppCodeGenWriteBarrier(&___themeDelegate_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
