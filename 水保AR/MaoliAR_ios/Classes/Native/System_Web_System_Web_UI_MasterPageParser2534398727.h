﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_UserControlParser2039383077.h"

// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.MasterPageParser
struct  MasterPageParser_t2534398727  : public UserControlParser_t2039383077
{
public:
	// System.Type System.Web.UI.MasterPageParser::masterType
	Type_t * ___masterType_62;
	// System.String System.Web.UI.MasterPageParser::masterTypeVirtualPath
	String_t* ___masterTypeVirtualPath_63;
	// System.Collections.Generic.List`1<System.String> System.Web.UI.MasterPageParser::contentPlaceHolderIds
	List_1_t3319525431 * ___contentPlaceHolderIds_64;
	// System.String System.Web.UI.MasterPageParser::cacheEntryName
	String_t* ___cacheEntryName_65;

public:
	inline static int32_t get_offset_of_masterType_62() { return static_cast<int32_t>(offsetof(MasterPageParser_t2534398727, ___masterType_62)); }
	inline Type_t * get_masterType_62() const { return ___masterType_62; }
	inline Type_t ** get_address_of_masterType_62() { return &___masterType_62; }
	inline void set_masterType_62(Type_t * value)
	{
		___masterType_62 = value;
		Il2CppCodeGenWriteBarrier(&___masterType_62, value);
	}

	inline static int32_t get_offset_of_masterTypeVirtualPath_63() { return static_cast<int32_t>(offsetof(MasterPageParser_t2534398727, ___masterTypeVirtualPath_63)); }
	inline String_t* get_masterTypeVirtualPath_63() const { return ___masterTypeVirtualPath_63; }
	inline String_t** get_address_of_masterTypeVirtualPath_63() { return &___masterTypeVirtualPath_63; }
	inline void set_masterTypeVirtualPath_63(String_t* value)
	{
		___masterTypeVirtualPath_63 = value;
		Il2CppCodeGenWriteBarrier(&___masterTypeVirtualPath_63, value);
	}

	inline static int32_t get_offset_of_contentPlaceHolderIds_64() { return static_cast<int32_t>(offsetof(MasterPageParser_t2534398727, ___contentPlaceHolderIds_64)); }
	inline List_1_t3319525431 * get_contentPlaceHolderIds_64() const { return ___contentPlaceHolderIds_64; }
	inline List_1_t3319525431 ** get_address_of_contentPlaceHolderIds_64() { return &___contentPlaceHolderIds_64; }
	inline void set_contentPlaceHolderIds_64(List_1_t3319525431 * value)
	{
		___contentPlaceHolderIds_64 = value;
		Il2CppCodeGenWriteBarrier(&___contentPlaceHolderIds_64, value);
	}

	inline static int32_t get_offset_of_cacheEntryName_65() { return static_cast<int32_t>(offsetof(MasterPageParser_t2534398727, ___cacheEntryName_65)); }
	inline String_t* get_cacheEntryName_65() const { return ___cacheEntryName_65; }
	inline String_t** get_address_of_cacheEntryName_65() { return &___cacheEntryName_65; }
	inline void set_cacheEntryName_65(String_t* value)
	{
		___cacheEntryName_65 = value;
		Il2CppCodeGenWriteBarrier(&___cacheEntryName_65, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
