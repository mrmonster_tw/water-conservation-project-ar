﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Generic.List`1<System.Reflection.Assembly>
struct List_1_t1279540245;
// System.Collections.Generic.Dictionary`2<System.String,System.Type>
struct Dictionary_2_t2269201059;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.AppResourcesCompiler/TypeResolutionService
struct  TypeResolutionService_t365108461  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.Reflection.Assembly> System.Web.Compilation.AppResourcesCompiler/TypeResolutionService::referencedAssemblies
	List_1_t1279540245 * ___referencedAssemblies_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Type> System.Web.Compilation.AppResourcesCompiler/TypeResolutionService::mappedTypes
	Dictionary_2_t2269201059 * ___mappedTypes_1;

public:
	inline static int32_t get_offset_of_referencedAssemblies_0() { return static_cast<int32_t>(offsetof(TypeResolutionService_t365108461, ___referencedAssemblies_0)); }
	inline List_1_t1279540245 * get_referencedAssemblies_0() const { return ___referencedAssemblies_0; }
	inline List_1_t1279540245 ** get_address_of_referencedAssemblies_0() { return &___referencedAssemblies_0; }
	inline void set_referencedAssemblies_0(List_1_t1279540245 * value)
	{
		___referencedAssemblies_0 = value;
		Il2CppCodeGenWriteBarrier(&___referencedAssemblies_0, value);
	}

	inline static int32_t get_offset_of_mappedTypes_1() { return static_cast<int32_t>(offsetof(TypeResolutionService_t365108461, ___mappedTypes_1)); }
	inline Dictionary_2_t2269201059 * get_mappedTypes_1() const { return ___mappedTypes_1; }
	inline Dictionary_2_t2269201059 ** get_address_of_mappedTypes_1() { return &___mappedTypes_1; }
	inline void set_mappedTypes_1(Dictionary_2_t2269201059 * value)
	{
		___mappedTypes_1 = value;
		Il2CppCodeGenWriteBarrier(&___mappedTypes_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
