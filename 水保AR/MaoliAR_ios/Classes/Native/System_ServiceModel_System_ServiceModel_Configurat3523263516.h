﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Configurat3049031238.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.MexBindingBindingCollectionElement`2<System.ServiceModel.WSHttpBinding,System.ServiceModel.Configuration.MexHttpBindingElement>
struct  MexBindingBindingCollectionElement_2_t3523263516  : public StandardBindingCollectionElement_2_t3049031238
{
public:

public:
};

struct MexBindingBindingCollectionElement_2_t3523263516_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.MexBindingBindingCollectionElement`2::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_14;

public:
	inline static int32_t get_offset_of_properties_14() { return static_cast<int32_t>(offsetof(MexBindingBindingCollectionElement_2_t3523263516_StaticFields, ___properties_14)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_14() const { return ___properties_14; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_14() { return &___properties_14; }
	inline void set_properties_14(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_14 = value;
		Il2CppCodeGenWriteBarrier(&___properties_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
