﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_Services_System_Web_Services_Descriptio3850760970.h"

// System.Web.Services.Description.SoapBinding
struct SoapBinding_t1418434649;
// System.Xml.Serialization.SoapCodeExporter
struct SoapCodeExporter_t3304897474;
// System.Xml.Serialization.SoapSchemaImporter
struct SoapSchemaImporter_t1895423347;
// System.Xml.Serialization.XmlCodeExporter
struct XmlCodeExporter_t4164579413;
// System.Xml.Serialization.XmlSchemaImporter
struct XmlSchemaImporter_t2852143304;
// System.Xml.Serialization.CodeIdentifiers
struct CodeIdentifiers_t4095039290;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Char[]
struct CharU5BU5D_t3528271667;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.SoapProtocolImporter
struct  SoapProtocolImporter_t721795827  : public ProtocolImporter_t3850760970
{
public:
	// System.Web.Services.Description.SoapBinding System.Web.Services.Description.SoapProtocolImporter::soapBinding
	SoapBinding_t1418434649 * ___soapBinding_19;
	// System.Xml.Serialization.SoapCodeExporter System.Web.Services.Description.SoapProtocolImporter::soapExporter
	SoapCodeExporter_t3304897474 * ___soapExporter_20;
	// System.Xml.Serialization.SoapSchemaImporter System.Web.Services.Description.SoapProtocolImporter::soapImporter
	SoapSchemaImporter_t1895423347 * ___soapImporter_21;
	// System.Xml.Serialization.XmlCodeExporter System.Web.Services.Description.SoapProtocolImporter::xmlExporter
	XmlCodeExporter_t4164579413 * ___xmlExporter_22;
	// System.Xml.Serialization.XmlSchemaImporter System.Web.Services.Description.SoapProtocolImporter::xmlImporter
	XmlSchemaImporter_t2852143304 * ___xmlImporter_23;
	// System.Xml.Serialization.CodeIdentifiers System.Web.Services.Description.SoapProtocolImporter::memberIds
	CodeIdentifiers_t4095039290 * ___memberIds_24;
	// System.Collections.ArrayList System.Web.Services.Description.SoapProtocolImporter::extensionImporters
	ArrayList_t2718874744 * ___extensionImporters_25;
	// System.Collections.Hashtable System.Web.Services.Description.SoapProtocolImporter::headerVariables
	Hashtable_t1853889766 * ___headerVariables_26;

public:
	inline static int32_t get_offset_of_soapBinding_19() { return static_cast<int32_t>(offsetof(SoapProtocolImporter_t721795827, ___soapBinding_19)); }
	inline SoapBinding_t1418434649 * get_soapBinding_19() const { return ___soapBinding_19; }
	inline SoapBinding_t1418434649 ** get_address_of_soapBinding_19() { return &___soapBinding_19; }
	inline void set_soapBinding_19(SoapBinding_t1418434649 * value)
	{
		___soapBinding_19 = value;
		Il2CppCodeGenWriteBarrier(&___soapBinding_19, value);
	}

	inline static int32_t get_offset_of_soapExporter_20() { return static_cast<int32_t>(offsetof(SoapProtocolImporter_t721795827, ___soapExporter_20)); }
	inline SoapCodeExporter_t3304897474 * get_soapExporter_20() const { return ___soapExporter_20; }
	inline SoapCodeExporter_t3304897474 ** get_address_of_soapExporter_20() { return &___soapExporter_20; }
	inline void set_soapExporter_20(SoapCodeExporter_t3304897474 * value)
	{
		___soapExporter_20 = value;
		Il2CppCodeGenWriteBarrier(&___soapExporter_20, value);
	}

	inline static int32_t get_offset_of_soapImporter_21() { return static_cast<int32_t>(offsetof(SoapProtocolImporter_t721795827, ___soapImporter_21)); }
	inline SoapSchemaImporter_t1895423347 * get_soapImporter_21() const { return ___soapImporter_21; }
	inline SoapSchemaImporter_t1895423347 ** get_address_of_soapImporter_21() { return &___soapImporter_21; }
	inline void set_soapImporter_21(SoapSchemaImporter_t1895423347 * value)
	{
		___soapImporter_21 = value;
		Il2CppCodeGenWriteBarrier(&___soapImporter_21, value);
	}

	inline static int32_t get_offset_of_xmlExporter_22() { return static_cast<int32_t>(offsetof(SoapProtocolImporter_t721795827, ___xmlExporter_22)); }
	inline XmlCodeExporter_t4164579413 * get_xmlExporter_22() const { return ___xmlExporter_22; }
	inline XmlCodeExporter_t4164579413 ** get_address_of_xmlExporter_22() { return &___xmlExporter_22; }
	inline void set_xmlExporter_22(XmlCodeExporter_t4164579413 * value)
	{
		___xmlExporter_22 = value;
		Il2CppCodeGenWriteBarrier(&___xmlExporter_22, value);
	}

	inline static int32_t get_offset_of_xmlImporter_23() { return static_cast<int32_t>(offsetof(SoapProtocolImporter_t721795827, ___xmlImporter_23)); }
	inline XmlSchemaImporter_t2852143304 * get_xmlImporter_23() const { return ___xmlImporter_23; }
	inline XmlSchemaImporter_t2852143304 ** get_address_of_xmlImporter_23() { return &___xmlImporter_23; }
	inline void set_xmlImporter_23(XmlSchemaImporter_t2852143304 * value)
	{
		___xmlImporter_23 = value;
		Il2CppCodeGenWriteBarrier(&___xmlImporter_23, value);
	}

	inline static int32_t get_offset_of_memberIds_24() { return static_cast<int32_t>(offsetof(SoapProtocolImporter_t721795827, ___memberIds_24)); }
	inline CodeIdentifiers_t4095039290 * get_memberIds_24() const { return ___memberIds_24; }
	inline CodeIdentifiers_t4095039290 ** get_address_of_memberIds_24() { return &___memberIds_24; }
	inline void set_memberIds_24(CodeIdentifiers_t4095039290 * value)
	{
		___memberIds_24 = value;
		Il2CppCodeGenWriteBarrier(&___memberIds_24, value);
	}

	inline static int32_t get_offset_of_extensionImporters_25() { return static_cast<int32_t>(offsetof(SoapProtocolImporter_t721795827, ___extensionImporters_25)); }
	inline ArrayList_t2718874744 * get_extensionImporters_25() const { return ___extensionImporters_25; }
	inline ArrayList_t2718874744 ** get_address_of_extensionImporters_25() { return &___extensionImporters_25; }
	inline void set_extensionImporters_25(ArrayList_t2718874744 * value)
	{
		___extensionImporters_25 = value;
		Il2CppCodeGenWriteBarrier(&___extensionImporters_25, value);
	}

	inline static int32_t get_offset_of_headerVariables_26() { return static_cast<int32_t>(offsetof(SoapProtocolImporter_t721795827, ___headerVariables_26)); }
	inline Hashtable_t1853889766 * get_headerVariables_26() const { return ___headerVariables_26; }
	inline Hashtable_t1853889766 ** get_address_of_headerVariables_26() { return &___headerVariables_26; }
	inline void set_headerVariables_26(Hashtable_t1853889766 * value)
	{
		___headerVariables_26 = value;
		Il2CppCodeGenWriteBarrier(&___headerVariables_26, value);
	}
};

struct SoapProtocolImporter_t721795827_StaticFields
{
public:
	// System.Char[] System.Web.Services.Description.SoapProtocolImporter::whitespaces
	CharU5BU5D_t3528271667* ___whitespaces_27;

public:
	inline static int32_t get_offset_of_whitespaces_27() { return static_cast<int32_t>(offsetof(SoapProtocolImporter_t721795827_StaticFields, ___whitespaces_27)); }
	inline CharU5BU5D_t3528271667* get_whitespaces_27() const { return ___whitespaces_27; }
	inline CharU5BU5D_t3528271667** get_address_of_whitespaces_27() { return &___whitespaces_27; }
	inline void set_whitespaces_27(CharU5BU5D_t3528271667* value)
	{
		___whitespaces_27 = value;
		Il2CppCodeGenWriteBarrier(&___whitespaces_27, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
