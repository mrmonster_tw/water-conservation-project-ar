﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Xml_System_Xml_Serialization_CodeGeneration2228734478.h"

// System.CodeDom.CodeNamespace
struct CodeNamespace_t2165007136;
// System.CodeDom.CodeCompileUnit
struct CodeCompileUnit_t2527618915;
// System.CodeDom.CodeAttributeDeclarationCollection
struct CodeAttributeDeclarationCollection_t3890917538;
// System.Xml.Serialization.XmlTypeMapping
struct XmlTypeMapping_t3915382669;
// System.CodeDom.Compiler.CodeDomProvider
struct CodeDomProvider_t110349836;
// System.Xml.Serialization.CodeIdentifiers
struct CodeIdentifiers_t4095039290;
// System.Collections.Hashtable
struct Hashtable_t1853889766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.MapCodeGenerator
struct  MapCodeGenerator_t1608106344  : public Il2CppObject
{
public:
	// System.CodeDom.CodeNamespace System.Xml.Serialization.MapCodeGenerator::codeNamespace
	CodeNamespace_t2165007136 * ___codeNamespace_0;
	// System.CodeDom.CodeCompileUnit System.Xml.Serialization.MapCodeGenerator::codeCompileUnit
	CodeCompileUnit_t2527618915 * ___codeCompileUnit_1;
	// System.CodeDom.CodeAttributeDeclarationCollection System.Xml.Serialization.MapCodeGenerator::includeMetadata
	CodeAttributeDeclarationCollection_t3890917538 * ___includeMetadata_2;
	// System.Xml.Serialization.XmlTypeMapping System.Xml.Serialization.MapCodeGenerator::exportedAnyType
	XmlTypeMapping_t3915382669 * ___exportedAnyType_3;
	// System.Boolean System.Xml.Serialization.MapCodeGenerator::includeArrayTypes
	bool ___includeArrayTypes_4;
	// System.CodeDom.Compiler.CodeDomProvider System.Xml.Serialization.MapCodeGenerator::codeProvider
	CodeDomProvider_t110349836 * ___codeProvider_5;
	// System.Xml.Serialization.CodeGenerationOptions System.Xml.Serialization.MapCodeGenerator::options
	int32_t ___options_6;
	// System.Xml.Serialization.CodeIdentifiers System.Xml.Serialization.MapCodeGenerator::identifiers
	CodeIdentifiers_t4095039290 * ___identifiers_7;
	// System.Collections.Hashtable System.Xml.Serialization.MapCodeGenerator::exportedMaps
	Hashtable_t1853889766 * ___exportedMaps_8;
	// System.Collections.Hashtable System.Xml.Serialization.MapCodeGenerator::includeMaps
	Hashtable_t1853889766 * ___includeMaps_9;

public:
	inline static int32_t get_offset_of_codeNamespace_0() { return static_cast<int32_t>(offsetof(MapCodeGenerator_t1608106344, ___codeNamespace_0)); }
	inline CodeNamespace_t2165007136 * get_codeNamespace_0() const { return ___codeNamespace_0; }
	inline CodeNamespace_t2165007136 ** get_address_of_codeNamespace_0() { return &___codeNamespace_0; }
	inline void set_codeNamespace_0(CodeNamespace_t2165007136 * value)
	{
		___codeNamespace_0 = value;
		Il2CppCodeGenWriteBarrier(&___codeNamespace_0, value);
	}

	inline static int32_t get_offset_of_codeCompileUnit_1() { return static_cast<int32_t>(offsetof(MapCodeGenerator_t1608106344, ___codeCompileUnit_1)); }
	inline CodeCompileUnit_t2527618915 * get_codeCompileUnit_1() const { return ___codeCompileUnit_1; }
	inline CodeCompileUnit_t2527618915 ** get_address_of_codeCompileUnit_1() { return &___codeCompileUnit_1; }
	inline void set_codeCompileUnit_1(CodeCompileUnit_t2527618915 * value)
	{
		___codeCompileUnit_1 = value;
		Il2CppCodeGenWriteBarrier(&___codeCompileUnit_1, value);
	}

	inline static int32_t get_offset_of_includeMetadata_2() { return static_cast<int32_t>(offsetof(MapCodeGenerator_t1608106344, ___includeMetadata_2)); }
	inline CodeAttributeDeclarationCollection_t3890917538 * get_includeMetadata_2() const { return ___includeMetadata_2; }
	inline CodeAttributeDeclarationCollection_t3890917538 ** get_address_of_includeMetadata_2() { return &___includeMetadata_2; }
	inline void set_includeMetadata_2(CodeAttributeDeclarationCollection_t3890917538 * value)
	{
		___includeMetadata_2 = value;
		Il2CppCodeGenWriteBarrier(&___includeMetadata_2, value);
	}

	inline static int32_t get_offset_of_exportedAnyType_3() { return static_cast<int32_t>(offsetof(MapCodeGenerator_t1608106344, ___exportedAnyType_3)); }
	inline XmlTypeMapping_t3915382669 * get_exportedAnyType_3() const { return ___exportedAnyType_3; }
	inline XmlTypeMapping_t3915382669 ** get_address_of_exportedAnyType_3() { return &___exportedAnyType_3; }
	inline void set_exportedAnyType_3(XmlTypeMapping_t3915382669 * value)
	{
		___exportedAnyType_3 = value;
		Il2CppCodeGenWriteBarrier(&___exportedAnyType_3, value);
	}

	inline static int32_t get_offset_of_includeArrayTypes_4() { return static_cast<int32_t>(offsetof(MapCodeGenerator_t1608106344, ___includeArrayTypes_4)); }
	inline bool get_includeArrayTypes_4() const { return ___includeArrayTypes_4; }
	inline bool* get_address_of_includeArrayTypes_4() { return &___includeArrayTypes_4; }
	inline void set_includeArrayTypes_4(bool value)
	{
		___includeArrayTypes_4 = value;
	}

	inline static int32_t get_offset_of_codeProvider_5() { return static_cast<int32_t>(offsetof(MapCodeGenerator_t1608106344, ___codeProvider_5)); }
	inline CodeDomProvider_t110349836 * get_codeProvider_5() const { return ___codeProvider_5; }
	inline CodeDomProvider_t110349836 ** get_address_of_codeProvider_5() { return &___codeProvider_5; }
	inline void set_codeProvider_5(CodeDomProvider_t110349836 * value)
	{
		___codeProvider_5 = value;
		Il2CppCodeGenWriteBarrier(&___codeProvider_5, value);
	}

	inline static int32_t get_offset_of_options_6() { return static_cast<int32_t>(offsetof(MapCodeGenerator_t1608106344, ___options_6)); }
	inline int32_t get_options_6() const { return ___options_6; }
	inline int32_t* get_address_of_options_6() { return &___options_6; }
	inline void set_options_6(int32_t value)
	{
		___options_6 = value;
	}

	inline static int32_t get_offset_of_identifiers_7() { return static_cast<int32_t>(offsetof(MapCodeGenerator_t1608106344, ___identifiers_7)); }
	inline CodeIdentifiers_t4095039290 * get_identifiers_7() const { return ___identifiers_7; }
	inline CodeIdentifiers_t4095039290 ** get_address_of_identifiers_7() { return &___identifiers_7; }
	inline void set_identifiers_7(CodeIdentifiers_t4095039290 * value)
	{
		___identifiers_7 = value;
		Il2CppCodeGenWriteBarrier(&___identifiers_7, value);
	}

	inline static int32_t get_offset_of_exportedMaps_8() { return static_cast<int32_t>(offsetof(MapCodeGenerator_t1608106344, ___exportedMaps_8)); }
	inline Hashtable_t1853889766 * get_exportedMaps_8() const { return ___exportedMaps_8; }
	inline Hashtable_t1853889766 ** get_address_of_exportedMaps_8() { return &___exportedMaps_8; }
	inline void set_exportedMaps_8(Hashtable_t1853889766 * value)
	{
		___exportedMaps_8 = value;
		Il2CppCodeGenWriteBarrier(&___exportedMaps_8, value);
	}

	inline static int32_t get_offset_of_includeMaps_9() { return static_cast<int32_t>(offsetof(MapCodeGenerator_t1608106344, ___includeMaps_9)); }
	inline Hashtable_t1853889766 * get_includeMaps_9() const { return ___includeMaps_9; }
	inline Hashtable_t1853889766 ** get_address_of_includeMaps_9() { return &___includeMaps_9; }
	inline void set_includeMaps_9(Hashtable_t1853889766 * value)
	{
		___includeMaps_9 = value;
		Il2CppCodeGenWriteBarrier(&___includeMaps_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
