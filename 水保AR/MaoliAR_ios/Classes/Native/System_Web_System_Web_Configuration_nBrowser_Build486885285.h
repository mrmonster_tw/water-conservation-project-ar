﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_Configuration_CapabilitiesBu1108460537.h"

// System.Collections.Generic.Dictionary`2<System.String,System.Web.Configuration.nBrowser.File>
struct Dictionary_2_t2888899401;
// System.Collections.Generic.List`1<System.Web.Configuration.nBrowser.File>
struct List_1_t280750548;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Object
struct Il2CppObject;
// System.Web.Configuration.nBrowser.Node
struct Node_t2365075726;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.nBrowser.Build
struct  Build_t486885285  : public CapabilitiesBuild_t1108460537
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Web.Configuration.nBrowser.File> System.Web.Configuration.nBrowser.Build::Browserfiles
	Dictionary_2_t2888899401 * ___Browserfiles_0;
	// System.Collections.Generic.List`1<System.Web.Configuration.nBrowser.File> System.Web.Configuration.nBrowser.Build::nbrowserfiles
	List_1_t280750548 * ___nbrowserfiles_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> System.Web.Configuration.nBrowser.Build::DefaultKeys
	Dictionary_2_t1632706988 * ___DefaultKeys_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> System.Web.Configuration.nBrowser.Build::BrowserKeys
	Dictionary_2_t1632706988 * ___BrowserKeys_3;
	// System.Object System.Web.Configuration.nBrowser.Build::browserSyncRoot
	Il2CppObject * ___browserSyncRoot_4;
	// System.Web.Configuration.nBrowser.Node System.Web.Configuration.nBrowser.Build::browser
	Node_t2365075726 * ___browser_5;

public:
	inline static int32_t get_offset_of_Browserfiles_0() { return static_cast<int32_t>(offsetof(Build_t486885285, ___Browserfiles_0)); }
	inline Dictionary_2_t2888899401 * get_Browserfiles_0() const { return ___Browserfiles_0; }
	inline Dictionary_2_t2888899401 ** get_address_of_Browserfiles_0() { return &___Browserfiles_0; }
	inline void set_Browserfiles_0(Dictionary_2_t2888899401 * value)
	{
		___Browserfiles_0 = value;
		Il2CppCodeGenWriteBarrier(&___Browserfiles_0, value);
	}

	inline static int32_t get_offset_of_nbrowserfiles_1() { return static_cast<int32_t>(offsetof(Build_t486885285, ___nbrowserfiles_1)); }
	inline List_1_t280750548 * get_nbrowserfiles_1() const { return ___nbrowserfiles_1; }
	inline List_1_t280750548 ** get_address_of_nbrowserfiles_1() { return &___nbrowserfiles_1; }
	inline void set_nbrowserfiles_1(List_1_t280750548 * value)
	{
		___nbrowserfiles_1 = value;
		Il2CppCodeGenWriteBarrier(&___nbrowserfiles_1, value);
	}

	inline static int32_t get_offset_of_DefaultKeys_2() { return static_cast<int32_t>(offsetof(Build_t486885285, ___DefaultKeys_2)); }
	inline Dictionary_2_t1632706988 * get_DefaultKeys_2() const { return ___DefaultKeys_2; }
	inline Dictionary_2_t1632706988 ** get_address_of_DefaultKeys_2() { return &___DefaultKeys_2; }
	inline void set_DefaultKeys_2(Dictionary_2_t1632706988 * value)
	{
		___DefaultKeys_2 = value;
		Il2CppCodeGenWriteBarrier(&___DefaultKeys_2, value);
	}

	inline static int32_t get_offset_of_BrowserKeys_3() { return static_cast<int32_t>(offsetof(Build_t486885285, ___BrowserKeys_3)); }
	inline Dictionary_2_t1632706988 * get_BrowserKeys_3() const { return ___BrowserKeys_3; }
	inline Dictionary_2_t1632706988 ** get_address_of_BrowserKeys_3() { return &___BrowserKeys_3; }
	inline void set_BrowserKeys_3(Dictionary_2_t1632706988 * value)
	{
		___BrowserKeys_3 = value;
		Il2CppCodeGenWriteBarrier(&___BrowserKeys_3, value);
	}

	inline static int32_t get_offset_of_browserSyncRoot_4() { return static_cast<int32_t>(offsetof(Build_t486885285, ___browserSyncRoot_4)); }
	inline Il2CppObject * get_browserSyncRoot_4() const { return ___browserSyncRoot_4; }
	inline Il2CppObject ** get_address_of_browserSyncRoot_4() { return &___browserSyncRoot_4; }
	inline void set_browserSyncRoot_4(Il2CppObject * value)
	{
		___browserSyncRoot_4 = value;
		Il2CppCodeGenWriteBarrier(&___browserSyncRoot_4, value);
	}

	inline static int32_t get_offset_of_browser_5() { return static_cast<int32_t>(offsetof(Build_t486885285, ___browser_5)); }
	inline Node_t2365075726 * get_browser_5() const { return ___browser_5; }
	inline Node_t2365075726 ** get_address_of_browser_5() { return &___browser_5; }
	inline void set_browser_5(Node_t2365075726 * value)
	{
		___browser_5 = value;
		Il2CppCodeGenWriteBarrier(&___browser_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
