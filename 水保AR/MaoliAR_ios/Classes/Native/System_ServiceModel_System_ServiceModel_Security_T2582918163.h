﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Selector1397312864.h"

// System.ServiceModel.ClientCredentialsSecurityTokenManager
struct ClientCredentialsSecurityTokenManager_t1905807029;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.SspiClientSecurityTokenAuthenticator
struct  SspiClientSecurityTokenAuthenticator_t2582918163  : public SecurityTokenAuthenticator_t1397312864
{
public:
	// System.ServiceModel.ClientCredentialsSecurityTokenManager System.ServiceModel.Security.Tokens.SspiClientSecurityTokenAuthenticator::manager
	ClientCredentialsSecurityTokenManager_t1905807029 * ___manager_0;

public:
	inline static int32_t get_offset_of_manager_0() { return static_cast<int32_t>(offsetof(SspiClientSecurityTokenAuthenticator_t2582918163, ___manager_0)); }
	inline ClientCredentialsSecurityTokenManager_t1905807029 * get_manager_0() const { return ___manager_0; }
	inline ClientCredentialsSecurityTokenManager_t1905807029 ** get_address_of_manager_0() { return &___manager_0; }
	inline void set_manager_0(ClientCredentialsSecurityTokenManager_t1905807029 * value)
	{
		___manager_0 = value;
		Il2CppCodeGenWriteBarrier(&___manager_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
