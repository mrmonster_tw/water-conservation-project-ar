﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Serialization_XmlTypeMapMemb3739392753.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaForm4264307319.h"

// System.String
struct String_t;
// System.Xml.Serialization.XmlTypeMapping
struct XmlTypeMapping_t3915382669;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeMapMemberAttribute
struct  XmlTypeMapMemberAttribute_t2452898273  : public XmlTypeMapMember_t3739392753
{
public:
	// System.String System.Xml.Serialization.XmlTypeMapMemberAttribute::_attributeName
	String_t* ____attributeName_9;
	// System.String System.Xml.Serialization.XmlTypeMapMemberAttribute::_namespace
	String_t* ____namespace_10;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Serialization.XmlTypeMapMemberAttribute::_form
	int32_t ____form_11;
	// System.Xml.Serialization.XmlTypeMapping System.Xml.Serialization.XmlTypeMapMemberAttribute::_mappedType
	XmlTypeMapping_t3915382669 * ____mappedType_12;

public:
	inline static int32_t get_offset_of__attributeName_9() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberAttribute_t2452898273, ____attributeName_9)); }
	inline String_t* get__attributeName_9() const { return ____attributeName_9; }
	inline String_t** get_address_of__attributeName_9() { return &____attributeName_9; }
	inline void set__attributeName_9(String_t* value)
	{
		____attributeName_9 = value;
		Il2CppCodeGenWriteBarrier(&____attributeName_9, value);
	}

	inline static int32_t get_offset_of__namespace_10() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberAttribute_t2452898273, ____namespace_10)); }
	inline String_t* get__namespace_10() const { return ____namespace_10; }
	inline String_t** get_address_of__namespace_10() { return &____namespace_10; }
	inline void set__namespace_10(String_t* value)
	{
		____namespace_10 = value;
		Il2CppCodeGenWriteBarrier(&____namespace_10, value);
	}

	inline static int32_t get_offset_of__form_11() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberAttribute_t2452898273, ____form_11)); }
	inline int32_t get__form_11() const { return ____form_11; }
	inline int32_t* get_address_of__form_11() { return &____form_11; }
	inline void set__form_11(int32_t value)
	{
		____form_11 = value;
	}

	inline static int32_t get_offset_of__mappedType_12() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberAttribute_t2452898273, ____mappedType_12)); }
	inline XmlTypeMapping_t3915382669 * get__mappedType_12() const { return ____mappedType_12; }
	inline XmlTypeMapping_t3915382669 ** get_address_of__mappedType_12() { return &____mappedType_12; }
	inline void set__mappedType_12(XmlTypeMapping_t3915382669 * value)
	{
		____mappedType_12 = value;
		Il2CppCodeGenWriteBarrier(&____mappedType_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
