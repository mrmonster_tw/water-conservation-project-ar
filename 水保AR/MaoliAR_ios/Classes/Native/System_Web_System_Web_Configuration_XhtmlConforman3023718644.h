﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3156163955.h"

// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.XhtmlConformanceSection
struct  XhtmlConformanceSection_t3023718644  : public ConfigurationSection_t3156163955
{
public:

public:
};

struct XhtmlConformanceSection_t3023718644_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Web.Configuration.XhtmlConformanceSection::modeProp
	ConfigurationProperty_t3590861854 * ___modeProp_17;
	// System.Configuration.ConfigurationPropertyCollection System.Web.Configuration.XhtmlConformanceSection::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_18;

public:
	inline static int32_t get_offset_of_modeProp_17() { return static_cast<int32_t>(offsetof(XhtmlConformanceSection_t3023718644_StaticFields, ___modeProp_17)); }
	inline ConfigurationProperty_t3590861854 * get_modeProp_17() const { return ___modeProp_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_modeProp_17() { return &___modeProp_17; }
	inline void set_modeProp_17(ConfigurationProperty_t3590861854 * value)
	{
		___modeProp_17 = value;
		Il2CppCodeGenWriteBarrier(&___modeProp_17, value);
	}

	inline static int32_t get_offset_of_properties_18() { return static_cast<int32_t>(offsetof(XhtmlConformanceSection_t3023718644_StaticFields, ___properties_18)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_18() const { return ___properties_18; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_18() { return &___properties_18; }
	inline void set_properties_18(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_18 = value;
		Il2CppCodeGenWriteBarrier(&___properties_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
