﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3549286319.h"
#include "System_Web_Services_System_Web_Services_Description448004713.h"
#include "mscorlib_System_Void1185182177.h"
#include "mscorlib_System_Object3080106164.h"
#include "System_Web_Services_System_Web_Services_Description721795827.h"
#include "mscorlib_System_Collections_ArrayList2718874744.h"
#include "System_Web_Services_System_Web_Services_Descriptio1283733481.h"
#include "mscorlib_System_Int322950945753.h"
#include "mscorlib_System_String1847450689.h"
#include "mscorlib_System_Boolean97287965.h"
#include "System_Web_Services_System_Web_Services_Descriptio1892683773.h"
#include "System_Web_Services_System_Web_Services_Descriptio2630153888.h"
#include "System_Xml_System_Xml_Serialization_XmlSchemas3283371924.h"
#include "System_Web_Services_System_Web_Services_Descriptio2299867357.h"
#include "System_Web_Services_System_Web_Services_Discovery_1899304036.h"
#include "System_System_CodeDom_CodeNamespace2165007136.h"
#include "mscorlib_System_ArgumentNullException1615371798.h"
#include "System_Web_Services_System_Web_Services_Description_10559386.h"
#include "System_System_Collections_Specialized_StringCollect167406615.h"
#include "System_Web_Services_System_Web_Services_Description141982620.h"
#include "mscorlib_System_Collections_CollectionBase2727926298.h"
#include "System_Web_Services_System_Web_Services_Descriptio1872617518.h"
#include "System_Web_Services_System_Web_Services_Descriptio3203052595.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializerI3900572101.h"
#include "System_Xml_System_Xml_Serialization_CodeGeneration2228734478.h"
#include "System_Web_Services_System_Web_Services_Description992977419.h"
#include "System_Web_Services_System_Web_Services_Description112384656.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati1652069793.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializatio982275218.h"
#include "System_Web_Services_System_Web_Services_Descriptio3206095699.h"
#include "mscorlib_System_RuntimeFieldHandle1871169219.h"
#include "mscorlib_System_Int643736567304.h"
#include "System_Web_Services_U3CPrivateImplementationDetail3057255361.h"
#include "System_Web_Services_U3CPrivateImplementationDetail2867270459.h"
#include "mscorlib_System_Collections_DictionaryBase3062914256.h"
#include "mscorlib_System_Collections_Hashtable1853889766.h"
#include "System_Web_Services_System_Web_Services_Protocols_H561465076.h"
#include "System_Web_Services_System_Web_Services_Protocols_1477195207.h"
#include "System_Web_Services_System_Web_Services_Protocols_2043200072.h"
#include "System_Web_Services_System_Web_Services_Protocols_M214809441.h"
#include "System_Web_Services_System_Web_Services_Protocols_3724139997.h"
#include "System_Web_Services_System_Web_Services_Protocols_1071160147.h"
#include "System_Web_Services_System_Web_Services_Protocols_1510473392.h"
#include "System_Web_Services_System_Web_Services_Protocols_2730831361.h"
#include "System_Web_Services_System_Web_Services_Protocols_2230442292.h"
#include "System_Web_Services_System_Web_Services_Protocols_1501641444.h"
#include "System_Web_Services_System_Web_Services_Protocols_1492964055.h"
#include "System_Web_Services_System_Web_Services_Protocols_2336331114.h"
#include "System_Web_Services_System_Web_Services_WsiProfile1986678150.h"

// System.Web.Services.Description.SoapTransportImporter
struct SoapTransportImporter_t448004713;
// System.Object
struct Il2CppObject;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Web.Services.Description.SoapHttpTransportImporter
struct SoapHttpTransportImporter_t1283733481;
// System.Web.Services.Description.SoapProtocolImporter
struct SoapProtocolImporter_t721795827;
// System.String
struct String_t;
// System.Web.Services.Description.Types
struct Types_t1892683773;
// System.Web.Services.Description.ServiceDescriptionFormatExtensionCollection
struct ServiceDescriptionFormatExtensionCollection_t2630153888;
// System.Xml.Serialization.XmlSchemas
struct XmlSchemas_t3283371924;
// System.Web.Services.Description.WebReference
struct WebReference_t2299867357;
// System.Web.Services.Discovery.DiscoveryClientDocumentCollection
struct DiscoveryClientDocumentCollection_t1899304036;
// System.CodeDom.CodeNamespace
struct CodeNamespace_t2165007136;
// System.ArgumentNullException
struct ArgumentNullException_t1615371798;
// System.Collections.Specialized.StringCollection
struct StringCollection_t167406615;
// System.Web.Services.Description.WebReferenceCollection
struct WebReferenceCollection_t141982620;
// System.Collections.CollectionBase
struct CollectionBase_t2727926298;
// System.Collections.IList
struct IList_t2094931216;
// System.Web.Services.Description.WebReferenceOptions
struct WebReferenceOptions_t1872617518;
// System.Web.Services.Description.WebReferenceOptionsSerializerImplementation
struct WebReferenceOptionsSerializerImplementation_t3203052595;
// System.Web.Services.Description.WebReferenceOptionsReader
struct WebReferenceOptionsReader_t112384656;
// System.Xml.Serialization.XmlSerializationReader
struct XmlSerializationReader_t1652069793;
// System.Xml.Serialization.XmlSerializerImplementation
struct XmlSerializerImplementation_t3900572101;
// System.Xml.Serialization.XmlSerializationWriter
struct XmlSerializationWriter_t982275218;
// System.Web.Services.Description.WebReferenceOptionsWriter
struct WebReferenceOptionsWriter_t3206095699;
// System.Array
struct Il2CppArray;
// System.Collections.DictionaryBase
struct DictionaryBase_t3062914256;
// System.Collections.ICollection
struct ICollection_t3904884886;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
extern Il2CppClass* ArrayList_t2718874744_il2cpp_TypeInfo_var;
extern Il2CppClass* SoapTransportImporter_t448004713_il2cpp_TypeInfo_var;
extern Il2CppClass* SoapHttpTransportImporter_t1283733481_il2cpp_TypeInfo_var;
extern const uint32_t SoapTransportImporter__cctor_m914484570_MetadataUsageId;
extern Il2CppClass* IEnumerator_t1853284238_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t3640265483_il2cpp_TypeInfo_var;
extern const uint32_t SoapTransportImporter_FindTransportImporter_m3803018425_MetadataUsageId;
extern Il2CppClass* ArgumentNullException_t1615371798_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3333185686;
extern Il2CppCodeGenString* _stringLiteral1294384764;
extern const uint32_t WebReference__ctor_m912545043_MetadataUsageId;
extern Il2CppClass* IList_t2094931216_il2cpp_TypeInfo_var;
extern const uint32_t WebReferenceCollection_Add_m3787867898_MetadataUsageId;
extern Il2CppClass* StringCollection_t167406615_il2cpp_TypeInfo_var;
extern const uint32_t WebReferenceOptions__ctor_m225687570_MetadataUsageId;
extern Il2CppClass* WebReferenceOptionsSerializerImplementation_t3203052595_il2cpp_TypeInfo_var;
extern Il2CppClass* WebReferenceOptions_t1872617518_il2cpp_TypeInfo_var;
extern const uint32_t WebReferenceOptions__cctor_m2163686482_MetadataUsageId;
extern Il2CppClass* WebReferenceOptionsReader_t112384656_il2cpp_TypeInfo_var;
extern const uint32_t WebReferenceOptionsSerializerImplementation_get_Reader_m888414159_MetadataUsageId;
extern Il2CppClass* WebReferenceOptionsWriter_t3206095699_il2cpp_TypeInfo_var;
extern const uint32_t WebReferenceOptionsSerializerImplementation_get_Writer_m2153650649_MetadataUsageId;
extern Il2CppClass* StringU5BU5D_t1281789340_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64U5BU5D_t2559172825_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3057255368____U24U24fieldU2D2_2_FieldInfo_var;
extern Il2CppCodeGenString* _stringLiteral1667624293;
extern Il2CppCodeGenString* _stringLiteral4035097588;
extern Il2CppCodeGenString* _stringLiteral3135217856;
extern Il2CppCodeGenString* _stringLiteral823592379;
extern Il2CppCodeGenString* _stringLiteral3801645664;
extern const uint32_t WebReferenceOptionsWriter__cctor_m257028589_MetadataUsageId;

// System.String[]
struct StringU5BU5D_t1281789340  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Int64[]
struct Int64U5BU5D_t2559172825  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int64_t m_Items[1];

public:
	inline int64_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int64_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int64_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int64_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int64_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int64_t value)
	{
		m_Items[index] = value;
	}
};



// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::.ctor()
extern "C"  void ArrayList__ctor_m4254721275 (ArrayList_t2718874744 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.Services.Description.SoapHttpTransportImporter::.ctor()
extern "C"  void SoapHttpTransportImporter__ctor_m2716372853 (SoapHttpTransportImporter_t1283733481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C"  void ArgumentNullException__ctor_m1170824041 (ArgumentNullException_t1615371798 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.CollectionBase::.ctor()
extern "C"  void CollectionBase__ctor_m3343513710 (CollectionBase_t2727926298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList System.Collections.CollectionBase::get_List()
extern "C"  Il2CppObject * CollectionBase_get_List_m490744407 (CollectionBase_t2727926298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.StringCollection::.ctor()
extern "C"  void StringCollection__ctor_m2508104136 (StringCollection_t167406615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.Services.Description.WebReferenceOptionsSerializerImplementation::.ctor()
extern "C"  void WebReferenceOptionsSerializerImplementation__ctor_m3081807271 (WebReferenceOptionsSerializerImplementation_t3203052595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.XmlSerializationReader::.ctor()
extern "C"  void XmlSerializationReader__ctor_m1264294723 (XmlSerializationReader_t1652069793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.XmlSerializerImplementation::.ctor()
extern "C"  void XmlSerializerImplementation__ctor_m2195430971 (XmlSerializerImplementation_t3900572101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.Services.Description.WebReferenceOptionsReader::.ctor()
extern "C"  void WebReferenceOptionsReader__ctor_m1549201320 (WebReferenceOptionsReader_t112384656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.Services.Description.WebReferenceOptionsWriter::.ctor()
extern "C"  void WebReferenceOptionsWriter__ctor_m4017459405 (WebReferenceOptionsWriter_t3206095699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Serialization.XmlSerializationWriter::.ctor()
extern "C"  void XmlSerializationWriter__ctor_m126065250 (XmlSerializationWriter_t982275218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
extern "C"  void RuntimeHelpers_InitializeArray_m3117905507 (Il2CppObject * __this /* static, unused */, Il2CppArray * p0, RuntimeFieldHandle_t1871169219  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.DictionaryBase::.ctor()
extern "C"  void DictionaryBase__ctor_m1430589782 (DictionaryBase_t3062914256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable System.Collections.DictionaryBase::get_InnerHashtable()
extern "C"  Hashtable_t1853889766 * DictionaryBase_get_InnerHashtable_m60442154 (DictionaryBase_t3062914256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Web.Services.Description.SoapTransportImporter::.ctor()
extern "C"  void SoapTransportImporter__ctor_m4266185467 (SoapTransportImporter_t448004713 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		__this->set_importContext_1((SoapProtocolImporter_t721795827 *)NULL);
		return;
	}
}
// System.Void System.Web.Services.Description.SoapTransportImporter::.cctor()
extern "C"  void SoapTransportImporter__cctor_m914484570 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoapTransportImporter__cctor_m914484570_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ArrayList_t2718874744 * L_0 = (ArrayList_t2718874744 *)il2cpp_codegen_object_new(ArrayList_t2718874744_il2cpp_TypeInfo_var);
		ArrayList__ctor_m4254721275(L_0, /*hidden argument*/NULL);
		((SoapTransportImporter_t448004713_StaticFields*)SoapTransportImporter_t448004713_il2cpp_TypeInfo_var->static_fields)->set_transportImporters_0(L_0);
		ArrayList_t2718874744 * L_1 = ((SoapTransportImporter_t448004713_StaticFields*)SoapTransportImporter_t448004713_il2cpp_TypeInfo_var->static_fields)->get_transportImporters_0();
		SoapHttpTransportImporter_t1283733481 * L_2 = (SoapHttpTransportImporter_t1283733481 *)il2cpp_codegen_object_new(SoapHttpTransportImporter_t1283733481_il2cpp_TypeInfo_var);
		SoapHttpTransportImporter__ctor_m2716372853(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_1, L_2);
		return;
	}
}
// System.Void System.Web.Services.Description.SoapTransportImporter::set_ImportContext(System.Web.Services.Description.SoapProtocolImporter)
extern "C"  void SoapTransportImporter_set_ImportContext_m3772809403 (SoapTransportImporter_t448004713 * __this, SoapProtocolImporter_t721795827 * ___value0, const MethodInfo* method)
{
	{
		SoapProtocolImporter_t721795827 * L_0 = ___value0;
		__this->set_importContext_1(L_0);
		return;
	}
}
// System.Web.Services.Description.SoapTransportImporter System.Web.Services.Description.SoapTransportImporter::FindTransportImporter(System.String)
extern "C"  SoapTransportImporter_t448004713 * SoapTransportImporter_FindTransportImporter_m3803018425 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoapTransportImporter_FindTransportImporter_m3803018425_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SoapHttpTransportImporter_t1283733481 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	SoapTransportImporter_t448004713 * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoapTransportImporter_t448004713_il2cpp_TypeInfo_var);
		ArrayList_t2718874744 * L_0 = ((SoapTransportImporter_t448004713_StaticFields*)SoapTransportImporter_t448004713_il2cpp_TypeInfo_var->static_fields)->get_transportImporters_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(45 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_0);
		V_1 = L_1;
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002f;
		}

IL_0010:
		{
			Il2CppObject * L_2 = V_1;
			NullCheck(L_2);
			Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_2);
			V_0 = ((SoapHttpTransportImporter_t1283733481 *)CastclassClass(L_3, SoapHttpTransportImporter_t1283733481_il2cpp_TypeInfo_var));
			SoapHttpTransportImporter_t1283733481 * L_4 = V_0;
			String_t* L_5 = ___uri0;
			NullCheck(L_4);
			bool L_6 = VirtFuncInvoker1< bool, String_t* >::Invoke(5 /* System.Boolean System.Web.Services.Description.SoapHttpTransportImporter::IsSupportedTransport(System.String) */, L_4, L_5);
			if (!L_6)
			{
				goto IL_002f;
			}
		}

IL_0028:
		{
			SoapHttpTransportImporter_t1283733481 * L_7 = V_0;
			V_2 = L_7;
			IL2CPP_LEAVE(0x53, FINALLY_003f);
		}

IL_002f:
		{
			Il2CppObject * L_8 = V_1;
			NullCheck(L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_8);
			if (L_9)
			{
				goto IL_0010;
			}
		}

IL_003a:
		{
			IL2CPP_LEAVE(0x51, FINALLY_003f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_003f;
	}

FINALLY_003f:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_10 = V_1;
			V_3 = ((Il2CppObject *)IsInst(L_10, IDisposable_t3640265483_il2cpp_TypeInfo_var));
			Il2CppObject * L_11 = V_3;
			if (L_11)
			{
				goto IL_004a;
			}
		}

IL_0049:
		{
			IL2CPP_END_FINALLY(63)
		}

IL_004a:
		{
			Il2CppObject * L_12 = V_3;
			NullCheck(L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_12);
			IL2CPP_END_FINALLY(63)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(63)
	{
		IL2CPP_JUMP_TBL(0x53, IL_0053)
		IL2CPP_JUMP_TBL(0x51, IL_0051)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0051:
	{
		return (SoapTransportImporter_t448004713 *)NULL;
	}

IL_0053:
	{
		SoapTransportImporter_t448004713 * L_13 = V_2;
		return L_13;
	}
}
// System.Web.Services.Description.ServiceDescriptionFormatExtensionCollection System.Web.Services.Description.Types::get_Extensions()
extern "C"  ServiceDescriptionFormatExtensionCollection_t2630153888 * Types_get_Extensions_m2642376216 (Types_t1892683773 * __this, const MethodInfo* method)
{
	{
		ServiceDescriptionFormatExtensionCollection_t2630153888 * L_0 = __this->get_extensions_3();
		return L_0;
	}
}
// System.Xml.Serialization.XmlSchemas System.Web.Services.Description.Types::get_Schemas()
extern "C"  XmlSchemas_t3283371924 * Types_get_Schemas_m2316746046 (Types_t1892683773 * __this, const MethodInfo* method)
{
	{
		XmlSchemas_t3283371924 * L_0 = __this->get_schemas_4();
		return L_0;
	}
}
// System.Void System.Web.Services.Description.WebReference::.ctor(System.Web.Services.Discovery.DiscoveryClientDocumentCollection,System.CodeDom.CodeNamespace)
extern "C"  void WebReference__ctor_m912545043 (WebReference_t2299867357 * __this, DiscoveryClientDocumentCollection_t1899304036 * ___documents0, CodeNamespace_t2165007136 * ___proxyCode1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebReference__ctor_m912545043_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		DiscoveryClientDocumentCollection_t1899304036 * L_0 = ___documents0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral3333185686, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		CodeNamespace_t2165007136 * L_2 = ___proxyCode1;
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_3 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_3, _stringLiteral1294384764, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0028:
	{
		DiscoveryClientDocumentCollection_t1899304036 * L_4 = ___documents0;
		__this->set__documents_0(L_4);
		CodeNamespace_t2165007136 * L_5 = ___proxyCode1;
		__this->set__proxyCode_1(L_5);
		return;
	}
}
// System.String System.Web.Services.Description.WebReference::get_AppSettingBaseUrl()
extern "C"  String_t* WebReference_get_AppSettingBaseUrl_m1646858525 (WebReference_t2299867357 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__appSettingBaseUrl_5();
		return L_0;
	}
}
// System.String System.Web.Services.Description.WebReference::get_AppSettingUrlKey()
extern "C"  String_t* WebReference_get_AppSettingUrlKey_m462927788 (WebReference_t2299867357 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__appSettingUrlKey_4();
		return L_0;
	}
}
// System.Web.Services.Discovery.DiscoveryClientDocumentCollection System.Web.Services.Description.WebReference::get_Documents()
extern "C"  DiscoveryClientDocumentCollection_t1899304036 * WebReference_get_Documents_m1206797760 (WebReference_t2299867357 * __this, const MethodInfo* method)
{
	{
		DiscoveryClientDocumentCollection_t1899304036 * L_0 = __this->get__documents_0();
		return L_0;
	}
}
// System.String System.Web.Services.Description.WebReference::get_ProtocolName()
extern "C"  String_t* WebReference_get_ProtocolName_m1565712313 (WebReference_t2299867357 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__protocolName_3();
		return L_0;
	}
}
// System.CodeDom.CodeNamespace System.Web.Services.Description.WebReference::get_ProxyCode()
extern "C"  CodeNamespace_t2165007136 * WebReference_get_ProxyCode_m3652947113 (WebReference_t2299867357 * __this, const MethodInfo* method)
{
	{
		CodeNamespace_t2165007136 * L_0 = __this->get__proxyCode_1();
		return L_0;
	}
}
// System.Void System.Web.Services.Description.WebReference::set_Warnings(System.Web.Services.Description.ServiceDescriptionImportWarnings)
extern "C"  void WebReference_set_Warnings_m4115407045 (WebReference_t2299867357 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set__warnings_2(L_0);
		return;
	}
}
// System.Void System.Web.Services.Description.WebReference::SetValidationWarnings(System.Collections.Specialized.StringCollection)
extern "C"  void WebReference_SetValidationWarnings_m3003901808 (WebReference_t2299867357 * __this, StringCollection_t167406615 * ___col0, const MethodInfo* method)
{
	{
		StringCollection_t167406615 * L_0 = ___col0;
		__this->set__validationWarnings_6(L_0);
		return;
	}
}
// System.Void System.Web.Services.Description.WebReferenceCollection::.ctor()
extern "C"  void WebReferenceCollection__ctor_m3081623393 (WebReferenceCollection_t141982620 * __this, const MethodInfo* method)
{
	{
		CollectionBase__ctor_m3343513710(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 System.Web.Services.Description.WebReferenceCollection::Add(System.Web.Services.Description.WebReference)
extern "C"  int32_t WebReferenceCollection_Add_m3787867898 (WebReferenceCollection_t141982620 * __this, WebReference_t2299867357 * ___webReference0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebReferenceCollection_Add_m3787867898_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = CollectionBase_get_List_m490744407(__this, /*hidden argument*/NULL);
		WebReference_t2299867357 * L_1 = ___webReference0;
		NullCheck(L_0);
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(4 /* System.Int32 System.Collections.IList::Add(System.Object) */, IList_t2094931216_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Void System.Web.Services.Description.WebReferenceOptions::.ctor()
extern "C"  void WebReferenceOptions__ctor_m225687570 (WebReferenceOptions_t1872617518 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebReferenceOptions__ctor_m225687570_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringCollection_t167406615 * L_0 = (StringCollection_t167406615 *)il2cpp_codegen_object_new(StringCollection_t167406615_il2cpp_TypeInfo_var);
		StringCollection__ctor_m2508104136(L_0, /*hidden argument*/NULL);
		__this->set_importer_extensions_2(L_0);
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Web.Services.Description.WebReferenceOptions::.cctor()
extern "C"  void WebReferenceOptions__cctor_m2163686482 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebReferenceOptions__cctor_m2163686482_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		WebReferenceOptionsSerializerImplementation_t3203052595 * L_0 = (WebReferenceOptionsSerializerImplementation_t3203052595 *)il2cpp_codegen_object_new(WebReferenceOptionsSerializerImplementation_t3203052595_il2cpp_TypeInfo_var);
		WebReferenceOptionsSerializerImplementation__ctor_m3081807271(L_0, /*hidden argument*/NULL);
		((WebReferenceOptions_t1872617518_StaticFields*)WebReferenceOptions_t1872617518_il2cpp_TypeInfo_var->static_fields)->set_implementation_0(L_0);
		return;
	}
}
// System.Xml.Serialization.CodeGenerationOptions System.Web.Services.Description.WebReferenceOptions::get_CodeGenerationOptions()
extern "C"  int32_t WebReferenceOptions_get_CodeGenerationOptions_m2658847051 (WebReferenceOptions_t1872617518 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_code_options_1();
		return L_0;
	}
}
// System.Web.Services.Description.ServiceDescriptionImportStyle System.Web.Services.Description.WebReferenceOptions::get_Style()
extern "C"  int32_t WebReferenceOptions_get_Style_m3249161283 (WebReferenceOptions_t1872617518 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_style_3();
		return L_0;
	}
}
// System.Void System.Web.Services.Description.WebReferenceOptions::set_Style(System.Web.Services.Description.ServiceDescriptionImportStyle)
extern "C"  void WebReferenceOptions_set_Style_m1404763763 (WebReferenceOptions_t1872617518 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_style_3(L_0);
		return;
	}
}
// System.Void System.Web.Services.Description.WebReferenceOptionsReader::.ctor()
extern "C"  void WebReferenceOptionsReader__ctor_m1549201320 (WebReferenceOptionsReader_t112384656 * __this, const MethodInfo* method)
{
	{
		XmlSerializationReader__ctor_m1264294723(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Web.Services.Description.WebReferenceOptionsReader::InitCallbacks()
extern "C"  void WebReferenceOptionsReader_InitCallbacks_m1744302481 (WebReferenceOptionsReader_t112384656 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void System.Web.Services.Description.WebReferenceOptionsReader::InitIDs()
extern "C"  void WebReferenceOptionsReader_InitIDs_m3804815766 (WebReferenceOptionsReader_t112384656 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void System.Web.Services.Description.WebReferenceOptionsSerializerImplementation::.ctor()
extern "C"  void WebReferenceOptionsSerializerImplementation__ctor_m3081807271 (WebReferenceOptionsSerializerImplementation_t3203052595 * __this, const MethodInfo* method)
{
	{
		XmlSerializerImplementation__ctor_m2195430971(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.Serialization.XmlSerializationReader System.Web.Services.Description.WebReferenceOptionsSerializerImplementation::get_Reader()
extern "C"  XmlSerializationReader_t1652069793 * WebReferenceOptionsSerializerImplementation_get_Reader_m888414159 (WebReferenceOptionsSerializerImplementation_t3203052595 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebReferenceOptionsSerializerImplementation_get_Reader_m888414159_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		WebReferenceOptionsReader_t112384656 * L_0 = (WebReferenceOptionsReader_t112384656 *)il2cpp_codegen_object_new(WebReferenceOptionsReader_t112384656_il2cpp_TypeInfo_var);
		WebReferenceOptionsReader__ctor_m1549201320(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Xml.Serialization.XmlSerializationWriter System.Web.Services.Description.WebReferenceOptionsSerializerImplementation::get_Writer()
extern "C"  XmlSerializationWriter_t982275218 * WebReferenceOptionsSerializerImplementation_get_Writer_m2153650649 (WebReferenceOptionsSerializerImplementation_t3203052595 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebReferenceOptionsSerializerImplementation_get_Writer_m2153650649_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		WebReferenceOptionsWriter_t3206095699 * L_0 = (WebReferenceOptionsWriter_t3206095699 *)il2cpp_codegen_object_new(WebReferenceOptionsWriter_t3206095699_il2cpp_TypeInfo_var);
		WebReferenceOptionsWriter__ctor_m4017459405(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Web.Services.Description.WebReferenceOptionsWriter::.ctor()
extern "C"  void WebReferenceOptionsWriter__ctor_m4017459405 (WebReferenceOptionsWriter_t3206095699 * __this, const MethodInfo* method)
{
	{
		XmlSerializationWriter__ctor_m126065250(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Web.Services.Description.WebReferenceOptionsWriter::.cctor()
extern "C"  void WebReferenceOptionsWriter__cctor_m257028589 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebReferenceOptionsWriter__cctor_m257028589_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t1281789340* L_0 = ((StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral1667624293);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1667624293);
		StringU5BU5D_t1281789340* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral4035097588);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral4035097588);
		StringU5BU5D_t1281789340* L_2 = L_1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral3135217856);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3135217856);
		StringU5BU5D_t1281789340* L_3 = L_2;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral823592379);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral823592379);
		StringU5BU5D_t1281789340* L_4 = L_3;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral3801645664);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3801645664);
		((WebReferenceOptionsWriter_t3206095699_StaticFields*)WebReferenceOptionsWriter_t3206095699_il2cpp_TypeInfo_var->static_fields)->set__xmlNamesCodeGenerationOptions_8(L_4);
		Int64U5BU5D_t2559172825* L_5 = ((Int64U5BU5D_t2559172825*)SZArrayNew(Int64U5BU5D_t2559172825_il2cpp_TypeInfo_var, (uint32_t)5));
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3057255368____U24U24fieldU2D2_2_FieldInfo_var), /*hidden argument*/NULL);
		((WebReferenceOptionsWriter_t3206095699_StaticFields*)WebReferenceOptionsWriter_t3206095699_il2cpp_TypeInfo_var->static_fields)->set__valuesCodeGenerationOptions_9(L_5);
		return;
	}
}
// System.Void System.Web.Services.Description.WebReferenceOptionsWriter::InitCallbacks()
extern "C"  void WebReferenceOptionsWriter_InitCallbacks_m3525092048 (WebReferenceOptionsWriter_t3206095699 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void System.Web.Services.Discovery.DiscoveryClientDocumentCollection::.ctor()
extern "C"  void DiscoveryClientDocumentCollection__ctor_m3584766153 (DiscoveryClientDocumentCollection_t1899304036 * __this, const MethodInfo* method)
{
	{
		DictionaryBase__ctor_m1430589782(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.ICollection System.Web.Services.Discovery.DiscoveryClientDocumentCollection::get_Values()
extern "C"  Il2CppObject * DiscoveryClientDocumentCollection_get_Values_m3683684304 (DiscoveryClientDocumentCollection_t1899304036 * __this, const MethodInfo* method)
{
	{
		Hashtable_t1853889766 * L_0 = DictionaryBase_get_InnerHashtable_m60442154(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(25 /* System.Collections.ICollection System.Collections.Hashtable::get_Values() */, L_0);
		return L_1;
	}
}
// System.Void System.Web.Services.Discovery.DiscoveryClientDocumentCollection::Add(System.String,System.Object)
extern "C"  void DiscoveryClientDocumentCollection_Add_m192011347 (DiscoveryClientDocumentCollection_t1899304036 * __this, String_t* ___url0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Hashtable_t1853889766 * L_0 = DictionaryBase_get_InnerHashtable_m60442154(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___url0;
		Il2CppObject * L_2 = ___value1;
		NullCheck(L_0);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(27 /* System.Void System.Collections.Hashtable::set_Item(System.Object,System.Object) */, L_0, L_1, L_2);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
