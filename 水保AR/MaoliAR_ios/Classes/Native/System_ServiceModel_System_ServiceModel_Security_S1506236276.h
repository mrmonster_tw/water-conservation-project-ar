﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.SspiSession
struct  SspiSession_t1506236276  : public Il2CppObject
{
public:
	// System.Int64 System.ServiceModel.Security.SspiSession::Challenge
	int64_t ___Challenge_1;
	// System.Int64 System.ServiceModel.Security.SspiSession::Context
	int64_t ___Context_2;
	// System.Int64 System.ServiceModel.Security.SspiSession::ServerOSVersion
	int64_t ___ServerOSVersion_3;
	// System.String System.ServiceModel.Security.SspiSession::ServerName
	String_t* ___ServerName_4;
	// System.String System.ServiceModel.Security.SspiSession::DomainName
	String_t* ___DomainName_5;
	// System.String System.ServiceModel.Security.SspiSession::DnsHostName
	String_t* ___DnsHostName_6;
	// System.String System.ServiceModel.Security.SspiSession::DnsDomainName
	String_t* ___DnsDomainName_7;

public:
	inline static int32_t get_offset_of_Challenge_1() { return static_cast<int32_t>(offsetof(SspiSession_t1506236276, ___Challenge_1)); }
	inline int64_t get_Challenge_1() const { return ___Challenge_1; }
	inline int64_t* get_address_of_Challenge_1() { return &___Challenge_1; }
	inline void set_Challenge_1(int64_t value)
	{
		___Challenge_1 = value;
	}

	inline static int32_t get_offset_of_Context_2() { return static_cast<int32_t>(offsetof(SspiSession_t1506236276, ___Context_2)); }
	inline int64_t get_Context_2() const { return ___Context_2; }
	inline int64_t* get_address_of_Context_2() { return &___Context_2; }
	inline void set_Context_2(int64_t value)
	{
		___Context_2 = value;
	}

	inline static int32_t get_offset_of_ServerOSVersion_3() { return static_cast<int32_t>(offsetof(SspiSession_t1506236276, ___ServerOSVersion_3)); }
	inline int64_t get_ServerOSVersion_3() const { return ___ServerOSVersion_3; }
	inline int64_t* get_address_of_ServerOSVersion_3() { return &___ServerOSVersion_3; }
	inline void set_ServerOSVersion_3(int64_t value)
	{
		___ServerOSVersion_3 = value;
	}

	inline static int32_t get_offset_of_ServerName_4() { return static_cast<int32_t>(offsetof(SspiSession_t1506236276, ___ServerName_4)); }
	inline String_t* get_ServerName_4() const { return ___ServerName_4; }
	inline String_t** get_address_of_ServerName_4() { return &___ServerName_4; }
	inline void set_ServerName_4(String_t* value)
	{
		___ServerName_4 = value;
		Il2CppCodeGenWriteBarrier(&___ServerName_4, value);
	}

	inline static int32_t get_offset_of_DomainName_5() { return static_cast<int32_t>(offsetof(SspiSession_t1506236276, ___DomainName_5)); }
	inline String_t* get_DomainName_5() const { return ___DomainName_5; }
	inline String_t** get_address_of_DomainName_5() { return &___DomainName_5; }
	inline void set_DomainName_5(String_t* value)
	{
		___DomainName_5 = value;
		Il2CppCodeGenWriteBarrier(&___DomainName_5, value);
	}

	inline static int32_t get_offset_of_DnsHostName_6() { return static_cast<int32_t>(offsetof(SspiSession_t1506236276, ___DnsHostName_6)); }
	inline String_t* get_DnsHostName_6() const { return ___DnsHostName_6; }
	inline String_t** get_address_of_DnsHostName_6() { return &___DnsHostName_6; }
	inline void set_DnsHostName_6(String_t* value)
	{
		___DnsHostName_6 = value;
		Il2CppCodeGenWriteBarrier(&___DnsHostName_6, value);
	}

	inline static int32_t get_offset_of_DnsDomainName_7() { return static_cast<int32_t>(offsetof(SspiSession_t1506236276, ___DnsDomainName_7)); }
	inline String_t* get_DnsDomainName_7() const { return ___DnsDomainName_7; }
	inline String_t** get_address_of_DnsDomainName_7() { return &___DnsDomainName_7; }
	inline void set_DnsDomainName_7(String_t* value)
	{
		___DnsDomainName_7 = value;
		Il2CppCodeGenWriteBarrier(&___DnsDomainName_7, value);
	}
};

struct SspiSession_t1506236276_StaticFields
{
public:
	// System.Byte[] System.ServiceModel.Security.SspiSession::NtlmSSP
	ByteU5BU5D_t4116647657* ___NtlmSSP_0;

public:
	inline static int32_t get_offset_of_NtlmSSP_0() { return static_cast<int32_t>(offsetof(SspiSession_t1506236276_StaticFields, ___NtlmSSP_0)); }
	inline ByteU5BU5D_t4116647657* get_NtlmSSP_0() const { return ___NtlmSSP_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_NtlmSSP_0() { return &___NtlmSSP_0; }
	inline void set_NtlmSSP_0(ByteU5BU5D_t4116647657* value)
	{
		___NtlmSSP_0 = value;
		Il2CppCodeGenWriteBarrier(&___NtlmSSP_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
