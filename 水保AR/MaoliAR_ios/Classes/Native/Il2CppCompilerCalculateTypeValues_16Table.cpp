﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_Mono_Xml_DTDElementDeclarationCollection222313714.h"
#include "System_Xml_Mono_Xml_DTDAttListDeclarationCollectio2220366188.h"
#include "System_Xml_Mono_Xml_DTDEntityDeclarationCollection2250844513.h"
#include "System_Xml_Mono_Xml_DTDNotationDeclarationCollectio959292105.h"
#include "System_Xml_Mono_Xml_DTDContentModel3264596611.h"
#include "System_Xml_Mono_Xml_DTDContentModelCollection2798820000.h"
#include "System_Xml_Mono_Xml_DTDNode858560093.h"
#include "System_Xml_Mono_Xml_DTDElementDeclaration1830540991.h"
#include "System_Xml_Mono_Xml_DTDAttributeDefinition3434905422.h"
#include "System_Xml_Mono_Xml_DTDAttListDeclaration3593159715.h"
#include "System_Xml_Mono_Xml_DTDEntityBase1228162861.h"
#include "System_Xml_Mono_Xml_DTDEntityDeclaration811637416.h"
#include "System_Xml_Mono_Xml_DTDNotationDeclaration3702682588.h"
#include "System_Xml_Mono_Xml_DTDParameterEntityDeclarationC2844734410.h"
#include "System_Xml_Mono_Xml_DTDParameterEntityDeclaration3796253422.h"
#include "System_Xml_Mono_Xml_DTDContentOrderType1195786655.h"
#include "System_Xml_Mono_Xml_DTDAttributeOccurenceType2323614041.h"
#include "System_Xml_Mono_Xml_DTDOccurence3140866896.h"
#include "System_Xml_System_Xml_DTDReader386081180.h"
#include "System_Xml_Mono_Xml_DTDValidatingReader3946379043.h"
#include "System_Xml_Mono_Xml_DTDValidatingReader_AttributeS3985135163.h"
#include "System_Xml_Mono_Xml_EntityResolvingXmlReader1267732406.h"
#include "System_Xml_System_Xml_EntityHandling1047276436.h"
#include "System_Xml_System_Xml_Formatting1232942836.h"
#include "System_Xml_System_Xml_MonoFIXAttribute630276366.h"
#include "System_Xml_System_Xml_NameTable3178203267.h"
#include "System_Xml_System_Xml_NameTable_Entry3052280359.h"
#include "System_Xml_System_Xml_NamespaceHandling4087553436.h"
#include "System_Xml_System_Xml_NewLineHandling850339274.h"
#include "System_Xml_System_Xml_ReadState944984020.h"
#include "System_Xml_System_Xml_ValidationType4049928607.h"
#include "System_Xml_System_Xml_WhitespaceHandling784045650.h"
#include "System_Xml_System_Xml_WriteState3983380671.h"
#include "System_Xml_System_Xml_XmlEntity3308518401.h"
#include "System_Xml_System_Xml_XmlAttribute1173852259.h"
#include "System_Xml_System_Xml_XmlAttributeCollection2316283784.h"
#include "System_Xml_System_Xml_XmlCDataSection3267478366.h"
#include "System_Xml_System_Xml_XmlChar3816087079.h"
#include "System_Xml_System_Xml_XmlCharacterData1167807131.h"
#include "System_Xml_System_Xml_XmlComment2476947920.h"
#include "System_Xml_System_Xml_XmlNotation1476580686.h"
#include "System_Xml_System_Xml_XmlDeclaration679870411.h"
#include "System_Xml_System_Xml_XmlDocument2837193595.h"
#include "System_Xml_System_Xml_XmlDocumentFragment1323348855.h"
#include "System_Xml_System_Xml_XmlDocumentNavigator512445268.h"
#include "System_Xml_System_Xml_XmlDocumentType4112370061.h"
#include "System_Xml_System_Xml_XmlElement561603118.h"
#include "System_Xml_System_Xml_XmlEntityReference1966808559.h"
#include "System_Xml_System_Xml_XmlException1761730631.h"
#include "System_Xml_System_Xml_XmlImplementation254178875.h"
#include "System_Xml_System_Xml_XmlConvert1981561327.h"
#include "System_Xml_System_Xml_XmlDateTimeSerializationMode1214355817.h"
#include "System_Xml_System_Xml_XmlLinkedNode1437094927.h"
#include "System_Xml_System_Xml_XmlNameEntry1073099671.h"
#include "System_Xml_System_Xml_XmlNameEntryCache2890546907.h"
#include "System_Xml_System_Xml_XmlNameTable71772148.h"
#include "System_Xml_System_Xml_XmlNamedNodeMap2821286253.h"
#include "System_Xml_System_Xml_XmlNamespaceManager418790500.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsDecl3938094415.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsScope3958624705.h"
#include "System_Xml_System_Xml_XmlNode3767805227.h"
#include "System_Xml_System_Xml_XmlNode_EmptyNodeList139615908.h"
#include "System_Xml_System_Xml_XmlNodeChangedAction3227731597.h"
#include "System_Xml_System_Xml_XmlNodeChangedEventArgs2486095928.h"
#include "System_Xml_System_Xml_XmlNodeList2551693786.h"
#include "System_Xml_System_Xml_XmlNodeListChildren1082692789.h"
#include "System_Xml_System_Xml_XmlNodeListChildren_Enumerator97922292.h"
#include "System_Xml_System_Xml_XmlNodeArrayList4092146157.h"
#include "System_Xml_System_Xml_XmlIteratorNodeList2218031790.h"
#include "System_Xml_System_Xml_XmlIteratorNodeList_XPathNod2379010936.h"
#include "System_Xml_System_Xml_XmlNodeOrder3385003529.h"
#include "System_Xml_System_Xml_XmlNodeReader4064889562.h"
#include "System_Xml_System_Xml_XmlNodeReaderImpl2501602067.h"
#include "System_Xml_System_Xml_XmlNodeType1672767151.h"
#include "System_Xml_System_Xml_XmlOutputMethod2185361861.h"
#include "System_Xml_System_Xml_XmlParserContext2544895291.h"
#include "System_Xml_System_Xml_XmlParserContext_ContextItem3112052795.h"
#include "System_Xml_System_Xml_XmlProcessingInstruction425688976.h"
#include "System_Xml_System_Xml_XmlQualifiedName2760654312.h"
#include "System_Xml_System_Xml_XmlReader3121518892.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport1809665003.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport_Comma1020432923.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport_CharG1703763694.h"
#include "System_Xml_System_Xml_XmlReaderSettings2186285234.h"
#include "System_Xml_System_Xml_XmlResolver626023767.h"
#include "System_Xml_System_Xml_XmlSecureResolver3504191023.h"
#include "System_Xml_System_Xml_XmlSignificantWhitespace1052520128.h"
#include "System_Xml_System_Xml_XmlSpace3324193251.h"
#include "System_Xml_System_Xml_XmlText2682211705.h"
#include "System_Xml_Mono_Xml2_XmlTextReader3455035481.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_XmlTokenInfo2519673037.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_XmlAttributeToke384315108.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_TagName2891256255.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_DtdInputState1766821130.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_DtdInputStateSta339956957.h"
#include "System_Xml_System_Xml_XmlTextReader4233384356.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1600 = { sizeof (DTDElementDeclarationCollection_t222313714), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1601 = { sizeof (DTDAttListDeclarationCollection_t2220366188), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1602 = { sizeof (DTDEntityDeclarationCollection_t2250844513), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1603 = { sizeof (DTDNotationDeclarationCollection_t959292105), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1604 = { sizeof (DTDContentModel_t3264596611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1604[7] = 
{
	DTDContentModel_t3264596611::get_offset_of_root_5(),
	DTDContentModel_t3264596611::get_offset_of_compiledAutomata_6(),
	DTDContentModel_t3264596611::get_offset_of_ownerElementName_7(),
	DTDContentModel_t3264596611::get_offset_of_elementName_8(),
	DTDContentModel_t3264596611::get_offset_of_orderType_9(),
	DTDContentModel_t3264596611::get_offset_of_childModels_10(),
	DTDContentModel_t3264596611::get_offset_of_occurence_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1605 = { sizeof (DTDContentModelCollection_t2798820000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1605[1] = 
{
	DTDContentModelCollection_t2798820000::get_offset_of_contentModel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1606 = { sizeof (DTDNode_t858560093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1606[5] = 
{
	DTDNode_t858560093::get_offset_of_root_0(),
	DTDNode_t858560093::get_offset_of_isInternalSubset_1(),
	DTDNode_t858560093::get_offset_of_baseURI_2(),
	DTDNode_t858560093::get_offset_of_lineNumber_3(),
	DTDNode_t858560093::get_offset_of_linePosition_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1607 = { sizeof (DTDElementDeclaration_t1830540991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1607[6] = 
{
	DTDElementDeclaration_t1830540991::get_offset_of_root_5(),
	DTDElementDeclaration_t1830540991::get_offset_of_contentModel_6(),
	DTDElementDeclaration_t1830540991::get_offset_of_name_7(),
	DTDElementDeclaration_t1830540991::get_offset_of_isEmpty_8(),
	DTDElementDeclaration_t1830540991::get_offset_of_isAny_9(),
	DTDElementDeclaration_t1830540991::get_offset_of_isMixedContent_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1608 = { sizeof (DTDAttributeDefinition_t3434905422), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1608[7] = 
{
	DTDAttributeDefinition_t3434905422::get_offset_of_name_5(),
	DTDAttributeDefinition_t3434905422::get_offset_of_datatype_6(),
	DTDAttributeDefinition_t3434905422::get_offset_of_enumeratedLiterals_7(),
	DTDAttributeDefinition_t3434905422::get_offset_of_unresolvedDefault_8(),
	DTDAttributeDefinition_t3434905422::get_offset_of_enumeratedNotations_9(),
	DTDAttributeDefinition_t3434905422::get_offset_of_occurenceType_10(),
	DTDAttributeDefinition_t3434905422::get_offset_of_resolvedDefaultValue_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1609 = { sizeof (DTDAttListDeclaration_t3593159715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1609[3] = 
{
	DTDAttListDeclaration_t3593159715::get_offset_of_name_5(),
	DTDAttListDeclaration_t3593159715::get_offset_of_attributeOrders_6(),
	DTDAttListDeclaration_t3593159715::get_offset_of_attributes_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1610 = { sizeof (DTDEntityBase_t1228162861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1610[10] = 
{
	DTDEntityBase_t1228162861::get_offset_of_name_5(),
	DTDEntityBase_t1228162861::get_offset_of_publicId_6(),
	DTDEntityBase_t1228162861::get_offset_of_systemId_7(),
	DTDEntityBase_t1228162861::get_offset_of_literalValue_8(),
	DTDEntityBase_t1228162861::get_offset_of_replacementText_9(),
	DTDEntityBase_t1228162861::get_offset_of_uriString_10(),
	DTDEntityBase_t1228162861::get_offset_of_absUri_11(),
	DTDEntityBase_t1228162861::get_offset_of_isInvalid_12(),
	DTDEntityBase_t1228162861::get_offset_of_loadFailed_13(),
	DTDEntityBase_t1228162861::get_offset_of_resolver_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1611 = { sizeof (DTDEntityDeclaration_t811637416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1611[6] = 
{
	DTDEntityDeclaration_t811637416::get_offset_of_entityValue_15(),
	DTDEntityDeclaration_t811637416::get_offset_of_notationName_16(),
	DTDEntityDeclaration_t811637416::get_offset_of_ReferencingEntities_17(),
	DTDEntityDeclaration_t811637416::get_offset_of_scanned_18(),
	DTDEntityDeclaration_t811637416::get_offset_of_recursed_19(),
	DTDEntityDeclaration_t811637416::get_offset_of_hasExternalReference_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1612 = { sizeof (DTDNotationDeclaration_t3702682588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1612[5] = 
{
	DTDNotationDeclaration_t3702682588::get_offset_of_name_5(),
	DTDNotationDeclaration_t3702682588::get_offset_of_localName_6(),
	DTDNotationDeclaration_t3702682588::get_offset_of_prefix_7(),
	DTDNotationDeclaration_t3702682588::get_offset_of_publicId_8(),
	DTDNotationDeclaration_t3702682588::get_offset_of_systemId_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1613 = { sizeof (DTDParameterEntityDeclarationCollection_t2844734410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1613[2] = 
{
	DTDParameterEntityDeclarationCollection_t2844734410::get_offset_of_peDecls_0(),
	DTDParameterEntityDeclarationCollection_t2844734410::get_offset_of_root_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1614 = { sizeof (DTDParameterEntityDeclaration_t3796253422), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1615 = { sizeof (DTDContentOrderType_t1195786655)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1615[4] = 
{
	DTDContentOrderType_t1195786655::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1616 = { sizeof (DTDAttributeOccurenceType_t2323614041)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1616[5] = 
{
	DTDAttributeOccurenceType_t2323614041::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1617 = { sizeof (DTDOccurence_t3140866896)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1617[5] = 
{
	DTDOccurence_t3140866896::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1618 = { sizeof (DTDReader_t386081180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1618[14] = 
{
	DTDReader_t386081180::get_offset_of_currentInput_0(),
	DTDReader_t386081180::get_offset_of_parserInputStack_1(),
	DTDReader_t386081180::get_offset_of_nameBuffer_2(),
	DTDReader_t386081180::get_offset_of_nameLength_3(),
	DTDReader_t386081180::get_offset_of_nameCapacity_4(),
	DTDReader_t386081180::get_offset_of_valueBuffer_5(),
	DTDReader_t386081180::get_offset_of_currentLinkedNodeLineNumber_6(),
	DTDReader_t386081180::get_offset_of_currentLinkedNodeLinePosition_7(),
	DTDReader_t386081180::get_offset_of_dtdIncludeSect_8(),
	DTDReader_t386081180::get_offset_of_normalization_9(),
	DTDReader_t386081180::get_offset_of_processingInternalSubset_10(),
	DTDReader_t386081180::get_offset_of_cachedPublicId_11(),
	DTDReader_t386081180::get_offset_of_cachedSystemId_12(),
	DTDReader_t386081180::get_offset_of_DTD_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1619 = { sizeof (DTDValidatingReader_t3946379043), -1, sizeof(DTDValidatingReader_t3946379043_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1619[29] = 
{
	DTDValidatingReader_t3946379043::get_offset_of_reader_3(),
	DTDValidatingReader_t3946379043::get_offset_of_sourceTextReader_4(),
	DTDValidatingReader_t3946379043::get_offset_of_validatingReader_5(),
	DTDValidatingReader_t3946379043::get_offset_of_dtd_6(),
	DTDValidatingReader_t3946379043::get_offset_of_resolver_7(),
	DTDValidatingReader_t3946379043::get_offset_of_currentElement_8(),
	DTDValidatingReader_t3946379043::get_offset_of_attributes_9(),
	DTDValidatingReader_t3946379043::get_offset_of_attributeCount_10(),
	DTDValidatingReader_t3946379043::get_offset_of_currentAttribute_11(),
	DTDValidatingReader_t3946379043::get_offset_of_consumedAttribute_12(),
	DTDValidatingReader_t3946379043::get_offset_of_elementStack_13(),
	DTDValidatingReader_t3946379043::get_offset_of_automataStack_14(),
	DTDValidatingReader_t3946379043::get_offset_of_popScope_15(),
	DTDValidatingReader_t3946379043::get_offset_of_isStandalone_16(),
	DTDValidatingReader_t3946379043::get_offset_of_currentAutomata_17(),
	DTDValidatingReader_t3946379043::get_offset_of_previousAutomata_18(),
	DTDValidatingReader_t3946379043::get_offset_of_idList_19(),
	DTDValidatingReader_t3946379043::get_offset_of_missingIDReferences_20(),
	DTDValidatingReader_t3946379043::get_offset_of_nsmgr_21(),
	DTDValidatingReader_t3946379043::get_offset_of_currentTextValue_22(),
	DTDValidatingReader_t3946379043::get_offset_of_constructingTextValue_23(),
	DTDValidatingReader_t3946379043::get_offset_of_shouldResetCurrentTextValue_24(),
	DTDValidatingReader_t3946379043::get_offset_of_isSignificantWhitespace_25(),
	DTDValidatingReader_t3946379043::get_offset_of_isWhitespace_26(),
	DTDValidatingReader_t3946379043::get_offset_of_isText_27(),
	DTDValidatingReader_t3946379043::get_offset_of_attributeValueEntityStack_28(),
	DTDValidatingReader_t3946379043::get_offset_of_valueBuilder_29(),
	DTDValidatingReader_t3946379043::get_offset_of_whitespaceChars_30(),
	DTDValidatingReader_t3946379043_StaticFields::get_offset_of_U3CU3Ef__switchU24map2A_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1620 = { sizeof (AttributeSlot_t3985135163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1620[6] = 
{
	AttributeSlot_t3985135163::get_offset_of_Name_0(),
	AttributeSlot_t3985135163::get_offset_of_LocalName_1(),
	AttributeSlot_t3985135163::get_offset_of_NS_2(),
	AttributeSlot_t3985135163::get_offset_of_Prefix_3(),
	AttributeSlot_t3985135163::get_offset_of_Value_4(),
	AttributeSlot_t3985135163::get_offset_of_IsDefault_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1621 = { sizeof (EntityResolvingXmlReader_t1267732406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1621[8] = 
{
	EntityResolvingXmlReader_t1267732406::get_offset_of_entity_3(),
	EntityResolvingXmlReader_t1267732406::get_offset_of_source_4(),
	EntityResolvingXmlReader_t1267732406::get_offset_of_context_5(),
	EntityResolvingXmlReader_t1267732406::get_offset_of_resolver_6(),
	EntityResolvingXmlReader_t1267732406::get_offset_of_entity_handling_7(),
	EntityResolvingXmlReader_t1267732406::get_offset_of_entity_inside_attr_8(),
	EntityResolvingXmlReader_t1267732406::get_offset_of_inside_attr_9(),
	EntityResolvingXmlReader_t1267732406::get_offset_of_do_resolve_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1622 = { sizeof (EntityHandling_t1047276436)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1622[3] = 
{
	EntityHandling_t1047276436::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1623 = { sizeof (Formatting_t1232942836)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1623[3] = 
{
	Formatting_t1232942836::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1624 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1625 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1626 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1627 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1628 = { sizeof (MonoFIXAttribute_t630276366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1628[1] = 
{
	MonoFIXAttribute_t630276366::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1629 = { sizeof (NameTable_t3178203267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1629[3] = 
{
	NameTable_t3178203267::get_offset_of_count_0(),
	NameTable_t3178203267::get_offset_of_buckets_1(),
	NameTable_t3178203267::get_offset_of_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1630 = { sizeof (Entry_t3052280359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1630[4] = 
{
	Entry_t3052280359::get_offset_of_str_0(),
	Entry_t3052280359::get_offset_of_hash_1(),
	Entry_t3052280359::get_offset_of_len_2(),
	Entry_t3052280359::get_offset_of_next_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1631 = { sizeof (NamespaceHandling_t4087553436)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1631[3] = 
{
	NamespaceHandling_t4087553436::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1632 = { sizeof (NewLineHandling_t850339274)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1632[4] = 
{
	NewLineHandling_t850339274::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1633 = { sizeof (ReadState_t944984020)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1633[6] = 
{
	ReadState_t944984020::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1634 = { sizeof (ValidationType_t4049928607)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1634[6] = 
{
	ValidationType_t4049928607::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1635 = { sizeof (WhitespaceHandling_t784045650)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1635[4] = 
{
	WhitespaceHandling_t784045650::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1636 = { sizeof (WriteState_t3983380671)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1636[8] = 
{
	WriteState_t3983380671::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1637 = { sizeof (XmlEntity_t3308518401), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1637[7] = 
{
	XmlEntity_t3308518401::get_offset_of_name_7(),
	XmlEntity_t3308518401::get_offset_of_NDATA_8(),
	XmlEntity_t3308518401::get_offset_of_publicId_9(),
	XmlEntity_t3308518401::get_offset_of_systemId_10(),
	XmlEntity_t3308518401::get_offset_of_baseUri_11(),
	XmlEntity_t3308518401::get_offset_of_lastLinkedChild_12(),
	XmlEntity_t3308518401::get_offset_of_contentAlreadySet_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1638 = { sizeof (XmlAttribute_t1173852259), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1638[4] = 
{
	XmlAttribute_t1173852259::get_offset_of_name_7(),
	XmlAttribute_t1173852259::get_offset_of_isDefault_8(),
	XmlAttribute_t1173852259::get_offset_of_lastLinkedChild_9(),
	XmlAttribute_t1173852259::get_offset_of_schemaInfo_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1639 = { sizeof (XmlAttributeCollection_t2316283784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1639[2] = 
{
	XmlAttributeCollection_t2316283784::get_offset_of_ownerElement_4(),
	XmlAttributeCollection_t2316283784::get_offset_of_ownerDocument_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1640 = { sizeof (XmlCDataSection_t3267478366), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1641 = { sizeof (XmlChar_t3816087079), -1, sizeof(XmlChar_t3816087079_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1641[5] = 
{
	XmlChar_t3816087079_StaticFields::get_offset_of_WhitespaceChars_0(),
	XmlChar_t3816087079_StaticFields::get_offset_of_firstNamePages_1(),
	XmlChar_t3816087079_StaticFields::get_offset_of_namePages_2(),
	XmlChar_t3816087079_StaticFields::get_offset_of_nameBitmap_3(),
	XmlChar_t3816087079_StaticFields::get_offset_of_U3CU3Ef__switchU24map2F_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1642 = { sizeof (XmlCharacterData_t1167807131), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1642[1] = 
{
	XmlCharacterData_t1167807131::get_offset_of_data_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1643 = { sizeof (XmlComment_t2476947920), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1644 = { sizeof (XmlNotation_t1476580686), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1644[4] = 
{
	XmlNotation_t1476580686::get_offset_of_localName_7(),
	XmlNotation_t1476580686::get_offset_of_publicId_8(),
	XmlNotation_t1476580686::get_offset_of_systemId_9(),
	XmlNotation_t1476580686::get_offset_of_prefix_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1645 = { sizeof (XmlDeclaration_t679870411), -1, sizeof(XmlDeclaration_t679870411_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1645[4] = 
{
	XmlDeclaration_t679870411::get_offset_of_encoding_8(),
	XmlDeclaration_t679870411::get_offset_of_standalone_9(),
	XmlDeclaration_t679870411::get_offset_of_version_10(),
	XmlDeclaration_t679870411_StaticFields::get_offset_of_U3CU3Ef__switchU24map30_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1646 = { sizeof (XmlDocument_t2837193595), -1, sizeof(XmlDocument_t2837193595_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1646[21] = 
{
	XmlDocument_t2837193595_StaticFields::get_offset_of_optimal_create_types_7(),
	XmlDocument_t2837193595::get_offset_of_optimal_create_element_8(),
	XmlDocument_t2837193595::get_offset_of_optimal_create_attribute_9(),
	XmlDocument_t2837193595::get_offset_of_nameTable_10(),
	XmlDocument_t2837193595::get_offset_of_baseURI_11(),
	XmlDocument_t2837193595::get_offset_of_implementation_12(),
	XmlDocument_t2837193595::get_offset_of_preserveWhitespace_13(),
	XmlDocument_t2837193595::get_offset_of_resolver_14(),
	XmlDocument_t2837193595::get_offset_of_idTable_15(),
	XmlDocument_t2837193595::get_offset_of_nameCache_16(),
	XmlDocument_t2837193595::get_offset_of_lastLinkedChild_17(),
	XmlDocument_t2837193595::get_offset_of_nsNodeXml_18(),
	XmlDocument_t2837193595::get_offset_of_schemas_19(),
	XmlDocument_t2837193595::get_offset_of_schemaInfo_20(),
	XmlDocument_t2837193595::get_offset_of_loadMode_21(),
	XmlDocument_t2837193595::get_offset_of_NodeChanged_22(),
	XmlDocument_t2837193595::get_offset_of_NodeChanging_23(),
	XmlDocument_t2837193595::get_offset_of_NodeInserted_24(),
	XmlDocument_t2837193595::get_offset_of_NodeInserting_25(),
	XmlDocument_t2837193595::get_offset_of_NodeRemoved_26(),
	XmlDocument_t2837193595::get_offset_of_NodeRemoving_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1647 = { sizeof (XmlDocumentFragment_t1323348855), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1647[1] = 
{
	XmlDocumentFragment_t1323348855::get_offset_of_lastLinkedChild_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1648 = { sizeof (XmlDocumentNavigator_t512445268), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1648[3] = 
{
	XmlDocumentNavigator_t512445268::get_offset_of_node_2(),
	XmlDocumentNavigator_t512445268::get_offset_of_nsNode_3(),
	XmlDocumentNavigator_t512445268::get_offset_of_iteratedNsNames_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1649 = { sizeof (XmlDocumentType_t4112370061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1649[3] = 
{
	XmlDocumentType_t4112370061::get_offset_of_entities_8(),
	XmlDocumentType_t4112370061::get_offset_of_notations_9(),
	XmlDocumentType_t4112370061::get_offset_of_dtd_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1650 = { sizeof (XmlElement_t561603118), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1650[5] = 
{
	XmlElement_t561603118::get_offset_of_attributes_8(),
	XmlElement_t561603118::get_offset_of_name_9(),
	XmlElement_t561603118::get_offset_of_lastLinkedChild_10(),
	XmlElement_t561603118::get_offset_of_isNotEmpty_11(),
	XmlElement_t561603118::get_offset_of_schemaInfo_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1651 = { sizeof (XmlEntityReference_t1966808559), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1651[2] = 
{
	XmlEntityReference_t1966808559::get_offset_of_entityName_8(),
	XmlEntityReference_t1966808559::get_offset_of_lastLinkedChild_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1652 = { sizeof (XmlException_t1761730631), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1652[5] = 
{
	XmlException_t1761730631::get_offset_of_lineNumber_11(),
	XmlException_t1761730631::get_offset_of_linePosition_12(),
	XmlException_t1761730631::get_offset_of_sourceUri_13(),
	XmlException_t1761730631::get_offset_of_res_14(),
	XmlException_t1761730631::get_offset_of_messages_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1653 = { sizeof (XmlImplementation_t254178875), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1653[1] = 
{
	XmlImplementation_t254178875::get_offset_of_InternalNameTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1654 = { sizeof (XmlConvert_t1981561327), -1, sizeof(XmlConvert_t1981561327_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1654[8] = 
{
	XmlConvert_t1981561327_StaticFields::get_offset_of_datetimeFormats_0(),
	XmlConvert_t1981561327_StaticFields::get_offset_of_defaultDateTimeFormats_1(),
	XmlConvert_t1981561327_StaticFields::get_offset_of_roundtripDateTimeFormats_2(),
	XmlConvert_t1981561327_StaticFields::get_offset_of_localDateTimeFormats_3(),
	XmlConvert_t1981561327_StaticFields::get_offset_of_utcDateTimeFormats_4(),
	XmlConvert_t1981561327_StaticFields::get_offset_of_unspecifiedDateTimeFormats_5(),
	XmlConvert_t1981561327_StaticFields::get_offset_of__defaultStyle_6(),
	XmlConvert_t1981561327_StaticFields::get_offset_of_U3CU3Ef__switchU24map33_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1655 = { sizeof (XmlDateTimeSerializationMode_t1214355817)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1655[5] = 
{
	XmlDateTimeSerializationMode_t1214355817::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1656 = { sizeof (XmlLinkedNode_t1437094927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1656[1] = 
{
	XmlLinkedNode_t1437094927::get_offset_of_nextSibling_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1657 = { sizeof (XmlNameEntry_t1073099671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1657[5] = 
{
	XmlNameEntry_t1073099671::get_offset_of_Prefix_0(),
	XmlNameEntry_t1073099671::get_offset_of_LocalName_1(),
	XmlNameEntry_t1073099671::get_offset_of_NS_2(),
	XmlNameEntry_t1073099671::get_offset_of_Hash_3(),
	XmlNameEntry_t1073099671::get_offset_of_prefixed_name_cache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1658 = { sizeof (XmlNameEntryCache_t2890546907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1658[4] = 
{
	XmlNameEntryCache_t2890546907::get_offset_of_table_0(),
	XmlNameEntryCache_t2890546907::get_offset_of_nameTable_1(),
	XmlNameEntryCache_t2890546907::get_offset_of_dummy_2(),
	XmlNameEntryCache_t2890546907::get_offset_of_cacheBuffer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1659 = { sizeof (XmlNameTable_t71772148), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1660 = { sizeof (XmlNamedNodeMap_t2821286253), -1, sizeof(XmlNamedNodeMap_t2821286253_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1660[4] = 
{
	XmlNamedNodeMap_t2821286253_StaticFields::get_offset_of_emptyEnumerator_0(),
	XmlNamedNodeMap_t2821286253::get_offset_of_parent_1(),
	XmlNamedNodeMap_t2821286253::get_offset_of_nodeList_2(),
	XmlNamedNodeMap_t2821286253::get_offset_of_readOnly_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1661 = { sizeof (XmlNamespaceManager_t418790500), -1, sizeof(XmlNamespaceManager_t418790500_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1661[9] = 
{
	XmlNamespaceManager_t418790500::get_offset_of_decls_0(),
	XmlNamespaceManager_t418790500::get_offset_of_declPos_1(),
	XmlNamespaceManager_t418790500::get_offset_of_scopes_2(),
	XmlNamespaceManager_t418790500::get_offset_of_scopePos_3(),
	XmlNamespaceManager_t418790500::get_offset_of_defaultNamespace_4(),
	XmlNamespaceManager_t418790500::get_offset_of_count_5(),
	XmlNamespaceManager_t418790500::get_offset_of_nameTable_6(),
	XmlNamespaceManager_t418790500::get_offset_of_internalAtomizedNames_7(),
	XmlNamespaceManager_t418790500_StaticFields::get_offset_of_U3CU3Ef__switchU24map25_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1662 = { sizeof (NsDecl_t3938094415)+ sizeof (Il2CppObject), sizeof(NsDecl_t3938094415_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1662[2] = 
{
	NsDecl_t3938094415::get_offset_of_Prefix_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NsDecl_t3938094415::get_offset_of_Uri_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1663 = { sizeof (NsScope_t3958624705)+ sizeof (Il2CppObject), sizeof(NsScope_t3958624705_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1663[2] = 
{
	NsScope_t3958624705::get_offset_of_DeclCount_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NsScope_t3958624705::get_offset_of_DefaultNamespace_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1664 = { sizeof (XmlNode_t3767805227), -1, sizeof(XmlNode_t3767805227_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1664[7] = 
{
	XmlNode_t3767805227_StaticFields::get_offset_of_emptyList_0(),
	XmlNode_t3767805227::get_offset_of_ownerDocument_1(),
	XmlNode_t3767805227::get_offset_of_parentNode_2(),
	XmlNode_t3767805227::get_offset_of_childNodes_3(),
	XmlNode_t3767805227_StaticFields::get_offset_of_U3CU3Ef__switchU24map2B_4(),
	XmlNode_t3767805227_StaticFields::get_offset_of_U3CU3Ef__switchU24map2C_5(),
	XmlNode_t3767805227_StaticFields::get_offset_of_U3CU3Ef__switchU24map2D_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1665 = { sizeof (EmptyNodeList_t139615908), -1, sizeof(EmptyNodeList_t139615908_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1665[1] = 
{
	EmptyNodeList_t139615908_StaticFields::get_offset_of_emptyEnumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1666 = { sizeof (XmlNodeChangedAction_t3227731597)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1666[4] = 
{
	XmlNodeChangedAction_t3227731597::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1667 = { sizeof (XmlNodeChangedEventArgs_t2486095928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1667[6] = 
{
	XmlNodeChangedEventArgs_t2486095928::get_offset_of__oldParent_1(),
	XmlNodeChangedEventArgs_t2486095928::get_offset_of__newParent_2(),
	XmlNodeChangedEventArgs_t2486095928::get_offset_of__action_3(),
	XmlNodeChangedEventArgs_t2486095928::get_offset_of__node_4(),
	XmlNodeChangedEventArgs_t2486095928::get_offset_of__oldValue_5(),
	XmlNodeChangedEventArgs_t2486095928::get_offset_of__newValue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1668 = { sizeof (XmlNodeList_t2551693786), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1669 = { sizeof (XmlNodeListChildren_t1082692789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1669[1] = 
{
	XmlNodeListChildren_t1082692789::get_offset_of_parent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1670 = { sizeof (Enumerator_t97922292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1670[3] = 
{
	Enumerator_t97922292::get_offset_of_parent_0(),
	Enumerator_t97922292::get_offset_of_currentChild_1(),
	Enumerator_t97922292::get_offset_of_passedLastNode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1671 = { sizeof (XmlNodeArrayList_t4092146157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1671[1] = 
{
	XmlNodeArrayList_t4092146157::get_offset_of__rgNodes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1672 = { sizeof (XmlIteratorNodeList_t2218031790), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1672[4] = 
{
	XmlIteratorNodeList_t2218031790::get_offset_of_source_0(),
	XmlIteratorNodeList_t2218031790::get_offset_of_iterator_1(),
	XmlIteratorNodeList_t2218031790::get_offset_of_list_2(),
	XmlIteratorNodeList_t2218031790::get_offset_of_finished_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1673 = { sizeof (XPathNodeIteratorNodeListIterator_t2379010936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1673[2] = 
{
	XPathNodeIteratorNodeListIterator_t2379010936::get_offset_of_iter_0(),
	XPathNodeIteratorNodeListIterator_t2379010936::get_offset_of_source_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1674 = { sizeof (XmlNodeOrder_t3385003529)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1674[5] = 
{
	XmlNodeOrder_t3385003529::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1675 = { sizeof (XmlNodeReader_t4064889562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1675[4] = 
{
	XmlNodeReader_t4064889562::get_offset_of_entity_3(),
	XmlNodeReader_t4064889562::get_offset_of_source_4(),
	XmlNodeReader_t4064889562::get_offset_of_entityInsideAttribute_5(),
	XmlNodeReader_t4064889562::get_offset_of_insideAttribute_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1676 = { sizeof (XmlNodeReaderImpl_t2501602067), -1, sizeof(XmlNodeReaderImpl_t2501602067_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1676[12] = 
{
	XmlNodeReaderImpl_t2501602067::get_offset_of_document_3(),
	XmlNodeReaderImpl_t2501602067::get_offset_of_startNode_4(),
	XmlNodeReaderImpl_t2501602067::get_offset_of_current_5(),
	XmlNodeReaderImpl_t2501602067::get_offset_of_ownerLinkedNode_6(),
	XmlNodeReaderImpl_t2501602067::get_offset_of_state_7(),
	XmlNodeReaderImpl_t2501602067::get_offset_of_depth_8(),
	XmlNodeReaderImpl_t2501602067::get_offset_of_isEndElement_9(),
	XmlNodeReaderImpl_t2501602067::get_offset_of_ignoreStartNode_10(),
	XmlNodeReaderImpl_t2501602067_StaticFields::get_offset_of_U3CU3Ef__switchU24map34_11(),
	XmlNodeReaderImpl_t2501602067_StaticFields::get_offset_of_U3CU3Ef__switchU24map35_12(),
	XmlNodeReaderImpl_t2501602067_StaticFields::get_offset_of_U3CU3Ef__switchU24map36_13(),
	XmlNodeReaderImpl_t2501602067_StaticFields::get_offset_of_U3CU3Ef__switchU24map37_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1677 = { sizeof (XmlNodeType_t1672767151)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1677[19] = 
{
	XmlNodeType_t1672767151::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1678 = { sizeof (XmlOutputMethod_t2185361861)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1678[5] = 
{
	XmlOutputMethod_t2185361861::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1679 = { sizeof (XmlParserContext_t2544895291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1679[13] = 
{
	XmlParserContext_t2544895291::get_offset_of_baseURI_0(),
	XmlParserContext_t2544895291::get_offset_of_docTypeName_1(),
	XmlParserContext_t2544895291::get_offset_of_encoding_2(),
	XmlParserContext_t2544895291::get_offset_of_internalSubset_3(),
	XmlParserContext_t2544895291::get_offset_of_namespaceManager_4(),
	XmlParserContext_t2544895291::get_offset_of_nameTable_5(),
	XmlParserContext_t2544895291::get_offset_of_publicID_6(),
	XmlParserContext_t2544895291::get_offset_of_systemID_7(),
	XmlParserContext_t2544895291::get_offset_of_xmlLang_8(),
	XmlParserContext_t2544895291::get_offset_of_xmlSpace_9(),
	XmlParserContext_t2544895291::get_offset_of_contextItems_10(),
	XmlParserContext_t2544895291::get_offset_of_contextItemCount_11(),
	XmlParserContext_t2544895291::get_offset_of_dtd_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1680 = { sizeof (ContextItem_t3112052795), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1680[3] = 
{
	ContextItem_t3112052795::get_offset_of_BaseURI_0(),
	ContextItem_t3112052795::get_offset_of_XmlLang_1(),
	ContextItem_t3112052795::get_offset_of_XmlSpace_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1681 = { sizeof (XmlProcessingInstruction_t425688976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1681[2] = 
{
	XmlProcessingInstruction_t425688976::get_offset_of_target_8(),
	XmlProcessingInstruction_t425688976::get_offset_of_data_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1682 = { sizeof (XmlQualifiedName_t2760654312), -1, sizeof(XmlQualifiedName_t2760654312_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1682[4] = 
{
	XmlQualifiedName_t2760654312_StaticFields::get_offset_of_Empty_0(),
	XmlQualifiedName_t2760654312::get_offset_of_name_1(),
	XmlQualifiedName_t2760654312::get_offset_of_ns_2(),
	XmlQualifiedName_t2760654312::get_offset_of_hash_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1683 = { sizeof (XmlReader_t3121518892), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1683[3] = 
{
	XmlReader_t3121518892::get_offset_of_readStringBuffer_0(),
	XmlReader_t3121518892::get_offset_of_binary_1(),
	XmlReader_t3121518892::get_offset_of_settings_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1684 = { sizeof (XmlReaderBinarySupport_t1809665003), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1684[8] = 
{
	XmlReaderBinarySupport_t1809665003::get_offset_of_reader_0(),
	XmlReaderBinarySupport_t1809665003::get_offset_of_getter_1(),
	XmlReaderBinarySupport_t1809665003::get_offset_of_base64Cache_2(),
	XmlReaderBinarySupport_t1809665003::get_offset_of_base64CacheStartsAt_3(),
	XmlReaderBinarySupport_t1809665003::get_offset_of_state_4(),
	XmlReaderBinarySupport_t1809665003::get_offset_of_textCache_5(),
	XmlReaderBinarySupport_t1809665003::get_offset_of_hasCache_6(),
	XmlReaderBinarySupport_t1809665003::get_offset_of_dontReset_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1685 = { sizeof (CommandState_t1020432923)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1685[6] = 
{
	CommandState_t1020432923::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1686 = { sizeof (CharGetter_t1703763694), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1687 = { sizeof (XmlReaderSettings_t2186285234), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1687[16] = 
{
	XmlReaderSettings_t2186285234::get_offset_of_checkCharacters_0(),
	XmlReaderSettings_t2186285234::get_offset_of_closeInput_1(),
	XmlReaderSettings_t2186285234::get_offset_of_conformance_2(),
	XmlReaderSettings_t2186285234::get_offset_of_ignoreComments_3(),
	XmlReaderSettings_t2186285234::get_offset_of_ignoreProcessingInstructions_4(),
	XmlReaderSettings_t2186285234::get_offset_of_ignoreWhitespace_5(),
	XmlReaderSettings_t2186285234::get_offset_of_lineNumberOffset_6(),
	XmlReaderSettings_t2186285234::get_offset_of_linePositionOffset_7(),
	XmlReaderSettings_t2186285234::get_offset_of_prohibitDtd_8(),
	XmlReaderSettings_t2186285234::get_offset_of_nameTable_9(),
	XmlReaderSettings_t2186285234::get_offset_of_schemas_10(),
	XmlReaderSettings_t2186285234::get_offset_of_schemasNeedsInitialization_11(),
	XmlReaderSettings_t2186285234::get_offset_of_validationFlags_12(),
	XmlReaderSettings_t2186285234::get_offset_of_validationType_13(),
	XmlReaderSettings_t2186285234::get_offset_of_xmlResolver_14(),
	XmlReaderSettings_t2186285234::get_offset_of_ValidationEventHandler_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1688 = { sizeof (XmlResolver_t626023767), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1689 = { sizeof (XmlSecureResolver_t3504191023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1689[2] = 
{
	XmlSecureResolver_t3504191023::get_offset_of_resolver_0(),
	XmlSecureResolver_t3504191023::get_offset_of_permissionSet_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1690 = { sizeof (XmlSignificantWhitespace_t1052520128), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1691 = { sizeof (XmlSpace_t3324193251)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1691[4] = 
{
	XmlSpace_t3324193251::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1692 = { sizeof (XmlText_t2682211705), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1693 = { sizeof (XmlTextReader_t3455035481), -1, sizeof(XmlTextReader_t3455035481_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1693[54] = 
{
	XmlTextReader_t3455035481::get_offset_of_cursorToken_3(),
	XmlTextReader_t3455035481::get_offset_of_currentToken_4(),
	XmlTextReader_t3455035481::get_offset_of_currentAttributeToken_5(),
	XmlTextReader_t3455035481::get_offset_of_currentAttributeValueToken_6(),
	XmlTextReader_t3455035481::get_offset_of_attributeTokens_7(),
	XmlTextReader_t3455035481::get_offset_of_attributeValueTokens_8(),
	XmlTextReader_t3455035481::get_offset_of_currentAttribute_9(),
	XmlTextReader_t3455035481::get_offset_of_currentAttributeValue_10(),
	XmlTextReader_t3455035481::get_offset_of_attributeCount_11(),
	XmlTextReader_t3455035481::get_offset_of_parserContext_12(),
	XmlTextReader_t3455035481::get_offset_of_nameTable_13(),
	XmlTextReader_t3455035481::get_offset_of_nsmgr_14(),
	XmlTextReader_t3455035481::get_offset_of_readState_15(),
	XmlTextReader_t3455035481::get_offset_of_disallowReset_16(),
	XmlTextReader_t3455035481::get_offset_of_depth_17(),
	XmlTextReader_t3455035481::get_offset_of_elementDepth_18(),
	XmlTextReader_t3455035481::get_offset_of_depthUp_19(),
	XmlTextReader_t3455035481::get_offset_of_popScope_20(),
	XmlTextReader_t3455035481::get_offset_of_elementNames_21(),
	XmlTextReader_t3455035481::get_offset_of_elementNameStackPos_22(),
	XmlTextReader_t3455035481::get_offset_of_allowMultipleRoot_23(),
	XmlTextReader_t3455035481::get_offset_of_isStandalone_24(),
	XmlTextReader_t3455035481::get_offset_of_returnEntityReference_25(),
	XmlTextReader_t3455035481::get_offset_of_entityReferenceName_26(),
	XmlTextReader_t3455035481::get_offset_of_valueBuffer_27(),
	XmlTextReader_t3455035481::get_offset_of_reader_28(),
	XmlTextReader_t3455035481::get_offset_of_peekChars_29(),
	XmlTextReader_t3455035481::get_offset_of_peekCharsIndex_30(),
	XmlTextReader_t3455035481::get_offset_of_peekCharsLength_31(),
	XmlTextReader_t3455035481::get_offset_of_curNodePeekIndex_32(),
	XmlTextReader_t3455035481::get_offset_of_preserveCurrentTag_33(),
	XmlTextReader_t3455035481::get_offset_of_line_34(),
	XmlTextReader_t3455035481::get_offset_of_column_35(),
	XmlTextReader_t3455035481::get_offset_of_currentLinkedNodeLineNumber_36(),
	XmlTextReader_t3455035481::get_offset_of_currentLinkedNodeLinePosition_37(),
	XmlTextReader_t3455035481::get_offset_of_useProceedingLineInfo_38(),
	XmlTextReader_t3455035481::get_offset_of_startNodeType_39(),
	XmlTextReader_t3455035481::get_offset_of_currentState_40(),
	XmlTextReader_t3455035481::get_offset_of_nestLevel_41(),
	XmlTextReader_t3455035481::get_offset_of_readCharsInProgress_42(),
	XmlTextReader_t3455035481::get_offset_of_binaryCharGetter_43(),
	XmlTextReader_t3455035481::get_offset_of_namespaces_44(),
	XmlTextReader_t3455035481::get_offset_of_whitespaceHandling_45(),
	XmlTextReader_t3455035481::get_offset_of_resolver_46(),
	XmlTextReader_t3455035481::get_offset_of_normalization_47(),
	XmlTextReader_t3455035481::get_offset_of_checkCharacters_48(),
	XmlTextReader_t3455035481::get_offset_of_prohibitDtd_49(),
	XmlTextReader_t3455035481::get_offset_of_closeInput_50(),
	XmlTextReader_t3455035481::get_offset_of_entityHandling_51(),
	XmlTextReader_t3455035481::get_offset_of_whitespacePool_52(),
	XmlTextReader_t3455035481::get_offset_of_whitespaceCache_53(),
	XmlTextReader_t3455035481::get_offset_of_stateStack_54(),
	XmlTextReader_t3455035481_StaticFields::get_offset_of_U3CU3Ef__switchU24map38_55(),
	XmlTextReader_t3455035481_StaticFields::get_offset_of_U3CU3Ef__switchU24map39_56(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1694 = { sizeof (XmlTokenInfo_t2519673037), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1694[13] = 
{
	XmlTokenInfo_t2519673037::get_offset_of_valueCache_0(),
	XmlTokenInfo_t2519673037::get_offset_of_Reader_1(),
	XmlTokenInfo_t2519673037::get_offset_of_Name_2(),
	XmlTokenInfo_t2519673037::get_offset_of_LocalName_3(),
	XmlTokenInfo_t2519673037::get_offset_of_Prefix_4(),
	XmlTokenInfo_t2519673037::get_offset_of_NamespaceURI_5(),
	XmlTokenInfo_t2519673037::get_offset_of_IsEmptyElement_6(),
	XmlTokenInfo_t2519673037::get_offset_of_QuoteChar_7(),
	XmlTokenInfo_t2519673037::get_offset_of_LineNumber_8(),
	XmlTokenInfo_t2519673037::get_offset_of_LinePosition_9(),
	XmlTokenInfo_t2519673037::get_offset_of_ValueBufferStart_10(),
	XmlTokenInfo_t2519673037::get_offset_of_ValueBufferEnd_11(),
	XmlTokenInfo_t2519673037::get_offset_of_NodeType_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1695 = { sizeof (XmlAttributeTokenInfo_t384315108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1695[4] = 
{
	XmlAttributeTokenInfo_t384315108::get_offset_of_ValueTokenStartIndex_13(),
	XmlAttributeTokenInfo_t384315108::get_offset_of_ValueTokenEndIndex_14(),
	XmlAttributeTokenInfo_t384315108::get_offset_of_valueCache_15(),
	XmlAttributeTokenInfo_t384315108::get_offset_of_tmpBuilder_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1696 = { sizeof (TagName_t2891256255)+ sizeof (Il2CppObject), sizeof(TagName_t2891256255_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1696[3] = 
{
	TagName_t2891256255::get_offset_of_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TagName_t2891256255::get_offset_of_LocalName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TagName_t2891256255::get_offset_of_Prefix_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1697 = { sizeof (DtdInputState_t1766821130)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1697[10] = 
{
	DtdInputState_t1766821130::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1698 = { sizeof (DtdInputStateStack_t339956957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1698[1] = 
{
	DtdInputStateStack_t339956957::get_offset_of_intern_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1699 = { sizeof (XmlTextReader_t4233384356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1699[5] = 
{
	XmlTextReader_t4233384356::get_offset_of_entity_3(),
	XmlTextReader_t4233384356::get_offset_of_source_4(),
	XmlTextReader_t4233384356::get_offset_of_entityInsideAttribute_5(),
	XmlTextReader_t4233384356::get_offset_of_insideAttribute_6(),
	XmlTextReader_t4233384356::get_offset_of_entityNameStack_7(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
