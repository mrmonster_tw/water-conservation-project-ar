﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Bi859993683.h"
#include "System_ServiceModel_System_ServiceModel_HostNameCo1351856580.h"
#include "System_ServiceModel_System_ServiceModel_WSMessageE2322128963.h"
#include "System_ServiceModel_System_ServiceModel_TransferMod436880017.h"

// System.Uri
struct Uri_t100236324;
// System.Xml.XmlDictionaryReaderQuotas
struct XmlDictionaryReaderQuotas_t173030297;
// System.ServiceModel.EnvelopeVersion
struct EnvelopeVersion_t2487833488;
// System.Text.Encoding
struct Encoding_t1523322056;
// System.ServiceModel.BasicHttpSecurity
struct BasicHttpSecurity_t548302097;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.BasicHttpBinding
struct  BasicHttpBinding_t4145755420  : public Binding_t859993683
{
public:
	// System.Boolean System.ServiceModel.BasicHttpBinding::allow_cookies
	bool ___allow_cookies_6;
	// System.Boolean System.ServiceModel.BasicHttpBinding::bypass_proxy_on_local
	bool ___bypass_proxy_on_local_7;
	// System.ServiceModel.HostNameComparisonMode System.ServiceModel.BasicHttpBinding::host_name_comparison_mode
	int32_t ___host_name_comparison_mode_8;
	// System.Int64 System.ServiceModel.BasicHttpBinding::max_buffer_pool_size
	int64_t ___max_buffer_pool_size_9;
	// System.Int32 System.ServiceModel.BasicHttpBinding::max_buffer_size
	int32_t ___max_buffer_size_10;
	// System.Int64 System.ServiceModel.BasicHttpBinding::max_recv_message_size
	int64_t ___max_recv_message_size_11;
	// System.ServiceModel.WSMessageEncoding System.ServiceModel.BasicHttpBinding::message_encoding
	int32_t ___message_encoding_12;
	// System.Uri System.ServiceModel.BasicHttpBinding::proxy_address
	Uri_t100236324 * ___proxy_address_13;
	// System.Xml.XmlDictionaryReaderQuotas System.ServiceModel.BasicHttpBinding::reader_quotas
	XmlDictionaryReaderQuotas_t173030297 * ___reader_quotas_14;
	// System.ServiceModel.EnvelopeVersion System.ServiceModel.BasicHttpBinding::env_version
	EnvelopeVersion_t2487833488 * ___env_version_15;
	// System.Text.Encoding System.ServiceModel.BasicHttpBinding::text_encoding
	Encoding_t1523322056 * ___text_encoding_16;
	// System.ServiceModel.TransferMode System.ServiceModel.BasicHttpBinding::transfer_mode
	int32_t ___transfer_mode_17;
	// System.Boolean System.ServiceModel.BasicHttpBinding::use_default_web_proxy
	bool ___use_default_web_proxy_18;
	// System.ServiceModel.BasicHttpSecurity System.ServiceModel.BasicHttpBinding::security
	BasicHttpSecurity_t548302097 * ___security_19;

public:
	inline static int32_t get_offset_of_allow_cookies_6() { return static_cast<int32_t>(offsetof(BasicHttpBinding_t4145755420, ___allow_cookies_6)); }
	inline bool get_allow_cookies_6() const { return ___allow_cookies_6; }
	inline bool* get_address_of_allow_cookies_6() { return &___allow_cookies_6; }
	inline void set_allow_cookies_6(bool value)
	{
		___allow_cookies_6 = value;
	}

	inline static int32_t get_offset_of_bypass_proxy_on_local_7() { return static_cast<int32_t>(offsetof(BasicHttpBinding_t4145755420, ___bypass_proxy_on_local_7)); }
	inline bool get_bypass_proxy_on_local_7() const { return ___bypass_proxy_on_local_7; }
	inline bool* get_address_of_bypass_proxy_on_local_7() { return &___bypass_proxy_on_local_7; }
	inline void set_bypass_proxy_on_local_7(bool value)
	{
		___bypass_proxy_on_local_7 = value;
	}

	inline static int32_t get_offset_of_host_name_comparison_mode_8() { return static_cast<int32_t>(offsetof(BasicHttpBinding_t4145755420, ___host_name_comparison_mode_8)); }
	inline int32_t get_host_name_comparison_mode_8() const { return ___host_name_comparison_mode_8; }
	inline int32_t* get_address_of_host_name_comparison_mode_8() { return &___host_name_comparison_mode_8; }
	inline void set_host_name_comparison_mode_8(int32_t value)
	{
		___host_name_comparison_mode_8 = value;
	}

	inline static int32_t get_offset_of_max_buffer_pool_size_9() { return static_cast<int32_t>(offsetof(BasicHttpBinding_t4145755420, ___max_buffer_pool_size_9)); }
	inline int64_t get_max_buffer_pool_size_9() const { return ___max_buffer_pool_size_9; }
	inline int64_t* get_address_of_max_buffer_pool_size_9() { return &___max_buffer_pool_size_9; }
	inline void set_max_buffer_pool_size_9(int64_t value)
	{
		___max_buffer_pool_size_9 = value;
	}

	inline static int32_t get_offset_of_max_buffer_size_10() { return static_cast<int32_t>(offsetof(BasicHttpBinding_t4145755420, ___max_buffer_size_10)); }
	inline int32_t get_max_buffer_size_10() const { return ___max_buffer_size_10; }
	inline int32_t* get_address_of_max_buffer_size_10() { return &___max_buffer_size_10; }
	inline void set_max_buffer_size_10(int32_t value)
	{
		___max_buffer_size_10 = value;
	}

	inline static int32_t get_offset_of_max_recv_message_size_11() { return static_cast<int32_t>(offsetof(BasicHttpBinding_t4145755420, ___max_recv_message_size_11)); }
	inline int64_t get_max_recv_message_size_11() const { return ___max_recv_message_size_11; }
	inline int64_t* get_address_of_max_recv_message_size_11() { return &___max_recv_message_size_11; }
	inline void set_max_recv_message_size_11(int64_t value)
	{
		___max_recv_message_size_11 = value;
	}

	inline static int32_t get_offset_of_message_encoding_12() { return static_cast<int32_t>(offsetof(BasicHttpBinding_t4145755420, ___message_encoding_12)); }
	inline int32_t get_message_encoding_12() const { return ___message_encoding_12; }
	inline int32_t* get_address_of_message_encoding_12() { return &___message_encoding_12; }
	inline void set_message_encoding_12(int32_t value)
	{
		___message_encoding_12 = value;
	}

	inline static int32_t get_offset_of_proxy_address_13() { return static_cast<int32_t>(offsetof(BasicHttpBinding_t4145755420, ___proxy_address_13)); }
	inline Uri_t100236324 * get_proxy_address_13() const { return ___proxy_address_13; }
	inline Uri_t100236324 ** get_address_of_proxy_address_13() { return &___proxy_address_13; }
	inline void set_proxy_address_13(Uri_t100236324 * value)
	{
		___proxy_address_13 = value;
		Il2CppCodeGenWriteBarrier(&___proxy_address_13, value);
	}

	inline static int32_t get_offset_of_reader_quotas_14() { return static_cast<int32_t>(offsetof(BasicHttpBinding_t4145755420, ___reader_quotas_14)); }
	inline XmlDictionaryReaderQuotas_t173030297 * get_reader_quotas_14() const { return ___reader_quotas_14; }
	inline XmlDictionaryReaderQuotas_t173030297 ** get_address_of_reader_quotas_14() { return &___reader_quotas_14; }
	inline void set_reader_quotas_14(XmlDictionaryReaderQuotas_t173030297 * value)
	{
		___reader_quotas_14 = value;
		Il2CppCodeGenWriteBarrier(&___reader_quotas_14, value);
	}

	inline static int32_t get_offset_of_env_version_15() { return static_cast<int32_t>(offsetof(BasicHttpBinding_t4145755420, ___env_version_15)); }
	inline EnvelopeVersion_t2487833488 * get_env_version_15() const { return ___env_version_15; }
	inline EnvelopeVersion_t2487833488 ** get_address_of_env_version_15() { return &___env_version_15; }
	inline void set_env_version_15(EnvelopeVersion_t2487833488 * value)
	{
		___env_version_15 = value;
		Il2CppCodeGenWriteBarrier(&___env_version_15, value);
	}

	inline static int32_t get_offset_of_text_encoding_16() { return static_cast<int32_t>(offsetof(BasicHttpBinding_t4145755420, ___text_encoding_16)); }
	inline Encoding_t1523322056 * get_text_encoding_16() const { return ___text_encoding_16; }
	inline Encoding_t1523322056 ** get_address_of_text_encoding_16() { return &___text_encoding_16; }
	inline void set_text_encoding_16(Encoding_t1523322056 * value)
	{
		___text_encoding_16 = value;
		Il2CppCodeGenWriteBarrier(&___text_encoding_16, value);
	}

	inline static int32_t get_offset_of_transfer_mode_17() { return static_cast<int32_t>(offsetof(BasicHttpBinding_t4145755420, ___transfer_mode_17)); }
	inline int32_t get_transfer_mode_17() const { return ___transfer_mode_17; }
	inline int32_t* get_address_of_transfer_mode_17() { return &___transfer_mode_17; }
	inline void set_transfer_mode_17(int32_t value)
	{
		___transfer_mode_17 = value;
	}

	inline static int32_t get_offset_of_use_default_web_proxy_18() { return static_cast<int32_t>(offsetof(BasicHttpBinding_t4145755420, ___use_default_web_proxy_18)); }
	inline bool get_use_default_web_proxy_18() const { return ___use_default_web_proxy_18; }
	inline bool* get_address_of_use_default_web_proxy_18() { return &___use_default_web_proxy_18; }
	inline void set_use_default_web_proxy_18(bool value)
	{
		___use_default_web_proxy_18 = value;
	}

	inline static int32_t get_offset_of_security_19() { return static_cast<int32_t>(offsetof(BasicHttpBinding_t4145755420, ___security_19)); }
	inline BasicHttpSecurity_t548302097 * get_security_19() const { return ___security_19; }
	inline BasicHttpSecurity_t548302097 ** get_address_of_security_19() { return &___security_19; }
	inline void set_security_19(BasicHttpSecurity_t548302097 * value)
	{
		___security_19 = value;
		Il2CppCodeGenWriteBarrier(&___security_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
