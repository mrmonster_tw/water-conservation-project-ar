﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Net.NetworkCredential
struct NetworkCredential_t3282608323;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.HttpDigestClientCredential
struct  HttpDigestClientCredential_t4244309379  : public Il2CppObject
{
public:
	// System.Net.NetworkCredential System.ServiceModel.Security.HttpDigestClientCredential::credential
	NetworkCredential_t3282608323 * ___credential_0;

public:
	inline static int32_t get_offset_of_credential_0() { return static_cast<int32_t>(offsetof(HttpDigestClientCredential_t4244309379, ___credential_0)); }
	inline NetworkCredential_t3282608323 * get_credential_0() const { return ___credential_0; }
	inline NetworkCredential_t3282608323 ** get_address_of_credential_0() { return &___credential_0; }
	inline void set_credential_0(NetworkCredential_t3282608323 * value)
	{
		___credential_0 = value;
		Il2CppCodeGenWriteBarrier(&___credential_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
