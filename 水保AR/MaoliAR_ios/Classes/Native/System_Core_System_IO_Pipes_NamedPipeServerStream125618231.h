﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Core_System_IO_Pipes_PipeStream871053121.h"

// System.IO.Pipes.INamedPipeServer
struct INamedPipeServer_t304076007;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Pipes.NamedPipeServerStream
struct  NamedPipeServerStream_t125618231  : public PipeStream_t871053121
{
public:
	// System.IO.Pipes.INamedPipeServer System.IO.Pipes.NamedPipeServerStream::impl
	Il2CppObject * ___impl_13;

public:
	inline static int32_t get_offset_of_impl_13() { return static_cast<int32_t>(offsetof(NamedPipeServerStream_t125618231, ___impl_13)); }
	inline Il2CppObject * get_impl_13() const { return ___impl_13; }
	inline Il2CppObject ** get_address_of_impl_13() { return &___impl_13; }
	inline void set_impl_13(Il2CppObject * value)
	{
		___impl_13 = value;
		Il2CppCodeGenWriteBarrier(&___impl_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
