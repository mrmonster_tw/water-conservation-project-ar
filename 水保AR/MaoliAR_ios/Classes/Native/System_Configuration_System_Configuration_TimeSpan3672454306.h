﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configura448955463.h"

// System.String
struct String_t;
// System.Configuration.ConfigurationValidatorBase
struct ConfigurationValidatorBase_t888490966;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.TimeSpanValidatorAttribute
struct  TimeSpanValidatorAttribute_t3672454306  : public ConfigurationValidatorAttribute_t448955463
{
public:
	// System.Boolean System.Configuration.TimeSpanValidatorAttribute::excludeRange
	bool ___excludeRange_2;
	// System.String System.Configuration.TimeSpanValidatorAttribute::maxValueString
	String_t* ___maxValueString_3;
	// System.String System.Configuration.TimeSpanValidatorAttribute::minValueString
	String_t* ___minValueString_4;
	// System.Configuration.ConfigurationValidatorBase System.Configuration.TimeSpanValidatorAttribute::instance
	ConfigurationValidatorBase_t888490966 * ___instance_5;

public:
	inline static int32_t get_offset_of_excludeRange_2() { return static_cast<int32_t>(offsetof(TimeSpanValidatorAttribute_t3672454306, ___excludeRange_2)); }
	inline bool get_excludeRange_2() const { return ___excludeRange_2; }
	inline bool* get_address_of_excludeRange_2() { return &___excludeRange_2; }
	inline void set_excludeRange_2(bool value)
	{
		___excludeRange_2 = value;
	}

	inline static int32_t get_offset_of_maxValueString_3() { return static_cast<int32_t>(offsetof(TimeSpanValidatorAttribute_t3672454306, ___maxValueString_3)); }
	inline String_t* get_maxValueString_3() const { return ___maxValueString_3; }
	inline String_t** get_address_of_maxValueString_3() { return &___maxValueString_3; }
	inline void set_maxValueString_3(String_t* value)
	{
		___maxValueString_3 = value;
		Il2CppCodeGenWriteBarrier(&___maxValueString_3, value);
	}

	inline static int32_t get_offset_of_minValueString_4() { return static_cast<int32_t>(offsetof(TimeSpanValidatorAttribute_t3672454306, ___minValueString_4)); }
	inline String_t* get_minValueString_4() const { return ___minValueString_4; }
	inline String_t** get_address_of_minValueString_4() { return &___minValueString_4; }
	inline void set_minValueString_4(String_t* value)
	{
		___minValueString_4 = value;
		Il2CppCodeGenWriteBarrier(&___minValueString_4, value);
	}

	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(TimeSpanValidatorAttribute_t3672454306, ___instance_5)); }
	inline ConfigurationValidatorBase_t888490966 * get_instance_5() const { return ___instance_5; }
	inline ConfigurationValidatorBase_t888490966 ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(ConfigurationValidatorBase_t888490966 * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier(&___instance_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
