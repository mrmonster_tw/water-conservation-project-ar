﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_Compilation_BaseCompiler69158820.h"

// System.Web.UI.ApplicationFileParser
struct ApplicationFileParser_t2181270929;
// System.Collections.ArrayList
struct ArrayList_t2718874744;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.GlobalAsaxCompiler
struct  GlobalAsaxCompiler_t2475863205  : public BaseCompiler_t69158820
{
public:
	// System.Web.UI.ApplicationFileParser System.Web.Compilation.GlobalAsaxCompiler::parser
	ApplicationFileParser_t2181270929 * ___parser_12;

public:
	inline static int32_t get_offset_of_parser_12() { return static_cast<int32_t>(offsetof(GlobalAsaxCompiler_t2475863205, ___parser_12)); }
	inline ApplicationFileParser_t2181270929 * get_parser_12() const { return ___parser_12; }
	inline ApplicationFileParser_t2181270929 ** get_address_of_parser_12() { return &___parser_12; }
	inline void set_parser_12(ApplicationFileParser_t2181270929 * value)
	{
		___parser_12 = value;
		Il2CppCodeGenWriteBarrier(&___parser_12, value);
	}
};

struct GlobalAsaxCompiler_t2475863205_StaticFields
{
public:
	// System.Collections.ArrayList System.Web.Compilation.GlobalAsaxCompiler::applicationObjectTags
	ArrayList_t2718874744 * ___applicationObjectTags_13;
	// System.Collections.ArrayList System.Web.Compilation.GlobalAsaxCompiler::sessionObjectTags
	ArrayList_t2718874744 * ___sessionObjectTags_14;

public:
	inline static int32_t get_offset_of_applicationObjectTags_13() { return static_cast<int32_t>(offsetof(GlobalAsaxCompiler_t2475863205_StaticFields, ___applicationObjectTags_13)); }
	inline ArrayList_t2718874744 * get_applicationObjectTags_13() const { return ___applicationObjectTags_13; }
	inline ArrayList_t2718874744 ** get_address_of_applicationObjectTags_13() { return &___applicationObjectTags_13; }
	inline void set_applicationObjectTags_13(ArrayList_t2718874744 * value)
	{
		___applicationObjectTags_13 = value;
		Il2CppCodeGenWriteBarrier(&___applicationObjectTags_13, value);
	}

	inline static int32_t get_offset_of_sessionObjectTags_14() { return static_cast<int32_t>(offsetof(GlobalAsaxCompiler_t2475863205_StaticFields, ___sessionObjectTags_14)); }
	inline ArrayList_t2718874744 * get_sessionObjectTags_14() const { return ___sessionObjectTags_14; }
	inline ArrayList_t2718874744 ** get_address_of_sessionObjectTags_14() { return &___sessionObjectTags_14; }
	inline void set_sessionObjectTags_14(ArrayList_t2718874744 * value)
	{
		___sessionObjectTags_14 = value;
		Il2CppCodeGenWriteBarrier(&___sessionObjectTags_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
