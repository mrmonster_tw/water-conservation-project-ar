﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Collections.Generic.IEnumerator`1<System.Type>
struct IEnumerator_1_t2916515228;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.ContractDescriptionGenerator/<GetAllInterfaceTypes>c__Iterator6
struct  U3CGetAllInterfaceTypesU3Ec__Iterator6_t2529581963  : public Il2CppObject
{
public:
	// System.Type System.ServiceModel.Description.ContractDescriptionGenerator/<GetAllInterfaceTypes>c__Iterator6::type
	Type_t * ___type_0;
	// System.Type[] System.ServiceModel.Description.ContractDescriptionGenerator/<GetAllInterfaceTypes>c__Iterator6::<$s_255>__0
	TypeU5BU5D_t3940880105* ___U3CU24s_255U3E__0_1;
	// System.Int32 System.ServiceModel.Description.ContractDescriptionGenerator/<GetAllInterfaceTypes>c__Iterator6::<$s_256>__1
	int32_t ___U3CU24s_256U3E__1_2;
	// System.Type System.ServiceModel.Description.ContractDescriptionGenerator/<GetAllInterfaceTypes>c__Iterator6::<t>__2
	Type_t * ___U3CtU3E__2_3;
	// System.Collections.Generic.IEnumerator`1<System.Type> System.ServiceModel.Description.ContractDescriptionGenerator/<GetAllInterfaceTypes>c__Iterator6::<$s_257>__3
	Il2CppObject* ___U3CU24s_257U3E__3_4;
	// System.Type System.ServiceModel.Description.ContractDescriptionGenerator/<GetAllInterfaceTypes>c__Iterator6::<tt>__4
	Type_t * ___U3CttU3E__4_5;
	// System.Int32 System.ServiceModel.Description.ContractDescriptionGenerator/<GetAllInterfaceTypes>c__Iterator6::$PC
	int32_t ___U24PC_6;
	// System.Type System.ServiceModel.Description.ContractDescriptionGenerator/<GetAllInterfaceTypes>c__Iterator6::$current
	Type_t * ___U24current_7;
	// System.Type System.ServiceModel.Description.ContractDescriptionGenerator/<GetAllInterfaceTypes>c__Iterator6::<$>type
	Type_t * ___U3CU24U3Etype_8;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(U3CGetAllInterfaceTypesU3Ec__Iterator6_t2529581963, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier(&___type_0, value);
	}

	inline static int32_t get_offset_of_U3CU24s_255U3E__0_1() { return static_cast<int32_t>(offsetof(U3CGetAllInterfaceTypesU3Ec__Iterator6_t2529581963, ___U3CU24s_255U3E__0_1)); }
	inline TypeU5BU5D_t3940880105* get_U3CU24s_255U3E__0_1() const { return ___U3CU24s_255U3E__0_1; }
	inline TypeU5BU5D_t3940880105** get_address_of_U3CU24s_255U3E__0_1() { return &___U3CU24s_255U3E__0_1; }
	inline void set_U3CU24s_255U3E__0_1(TypeU5BU5D_t3940880105* value)
	{
		___U3CU24s_255U3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_255U3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CU24s_256U3E__1_2() { return static_cast<int32_t>(offsetof(U3CGetAllInterfaceTypesU3Ec__Iterator6_t2529581963, ___U3CU24s_256U3E__1_2)); }
	inline int32_t get_U3CU24s_256U3E__1_2() const { return ___U3CU24s_256U3E__1_2; }
	inline int32_t* get_address_of_U3CU24s_256U3E__1_2() { return &___U3CU24s_256U3E__1_2; }
	inline void set_U3CU24s_256U3E__1_2(int32_t value)
	{
		___U3CU24s_256U3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E__2_3() { return static_cast<int32_t>(offsetof(U3CGetAllInterfaceTypesU3Ec__Iterator6_t2529581963, ___U3CtU3E__2_3)); }
	inline Type_t * get_U3CtU3E__2_3() const { return ___U3CtU3E__2_3; }
	inline Type_t ** get_address_of_U3CtU3E__2_3() { return &___U3CtU3E__2_3; }
	inline void set_U3CtU3E__2_3(Type_t * value)
	{
		___U3CtU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtU3E__2_3, value);
	}

	inline static int32_t get_offset_of_U3CU24s_257U3E__3_4() { return static_cast<int32_t>(offsetof(U3CGetAllInterfaceTypesU3Ec__Iterator6_t2529581963, ___U3CU24s_257U3E__3_4)); }
	inline Il2CppObject* get_U3CU24s_257U3E__3_4() const { return ___U3CU24s_257U3E__3_4; }
	inline Il2CppObject** get_address_of_U3CU24s_257U3E__3_4() { return &___U3CU24s_257U3E__3_4; }
	inline void set_U3CU24s_257U3E__3_4(Il2CppObject* value)
	{
		___U3CU24s_257U3E__3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_257U3E__3_4, value);
	}

	inline static int32_t get_offset_of_U3CttU3E__4_5() { return static_cast<int32_t>(offsetof(U3CGetAllInterfaceTypesU3Ec__Iterator6_t2529581963, ___U3CttU3E__4_5)); }
	inline Type_t * get_U3CttU3E__4_5() const { return ___U3CttU3E__4_5; }
	inline Type_t ** get_address_of_U3CttU3E__4_5() { return &___U3CttU3E__4_5; }
	inline void set_U3CttU3E__4_5(Type_t * value)
	{
		___U3CttU3E__4_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CttU3E__4_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CGetAllInterfaceTypesU3Ec__Iterator6_t2529581963, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CGetAllInterfaceTypesU3Ec__Iterator6_t2529581963, ___U24current_7)); }
	inline Type_t * get_U24current_7() const { return ___U24current_7; }
	inline Type_t ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Type_t * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Etype_8() { return static_cast<int32_t>(offsetof(U3CGetAllInterfaceTypesU3Ec__Iterator6_t2529581963, ___U3CU24U3Etype_8)); }
	inline Type_t * get_U3CU24U3Etype_8() const { return ___U3CU24U3Etype_8; }
	inline Type_t ** get_address_of_U3CU24U3Etype_8() { return &___U3CU24U3Etype_8; }
	inline void set_U3CU24U3Etype_8(Type_t * value)
	{
		___U3CU24U3Etype_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Etype_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
