﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"

// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestFulsClass.RestFulClass/RestFul/Datas
struct  Datas_t545811082 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Datas_t545811082__padding[1];
	};

public:
};

struct Datas_t545811082_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.String> RestFulsClass.RestFulClass/RestFul/Datas::RestFulString
	List_1_t3319525431 * ___RestFulString_0;

public:
	inline static int32_t get_offset_of_RestFulString_0() { return static_cast<int32_t>(offsetof(Datas_t545811082_StaticFields, ___RestFulString_0)); }
	inline List_1_t3319525431 * get_RestFulString_0() const { return ___RestFulString_0; }
	inline List_1_t3319525431 ** get_address_of_RestFulString_0() { return &___RestFulString_0; }
	inline void set_RestFulString_0(List_1_t3319525431 * value)
	{
		___RestFulString_0 = value;
		Il2CppCodeGenWriteBarrier(&___RestFulString_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
