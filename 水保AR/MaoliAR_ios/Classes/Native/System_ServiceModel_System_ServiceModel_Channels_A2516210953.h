﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_A3096310965.h"

// System.String
struct String_t;
// System.Runtime.Serialization.XmlObjectSerializer
struct XmlObjectSerializer_t3967301761;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.AddressHeader/DefaultAddressHeader
struct  DefaultAddressHeader_t2516210953  : public AddressHeader_t3096310965
{
public:
	// System.String System.ServiceModel.Channels.AddressHeader/DefaultAddressHeader::name
	String_t* ___name_0;
	// System.String System.ServiceModel.Channels.AddressHeader/DefaultAddressHeader::ns
	String_t* ___ns_1;
	// System.Runtime.Serialization.XmlObjectSerializer System.ServiceModel.Channels.AddressHeader/DefaultAddressHeader::formatter
	XmlObjectSerializer_t3967301761 * ___formatter_2;
	// System.Object System.ServiceModel.Channels.AddressHeader/DefaultAddressHeader::value
	Il2CppObject * ___value_3;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(DefaultAddressHeader_t2516210953, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_ns_1() { return static_cast<int32_t>(offsetof(DefaultAddressHeader_t2516210953, ___ns_1)); }
	inline String_t* get_ns_1() const { return ___ns_1; }
	inline String_t** get_address_of_ns_1() { return &___ns_1; }
	inline void set_ns_1(String_t* value)
	{
		___ns_1 = value;
		Il2CppCodeGenWriteBarrier(&___ns_1, value);
	}

	inline static int32_t get_offset_of_formatter_2() { return static_cast<int32_t>(offsetof(DefaultAddressHeader_t2516210953, ___formatter_2)); }
	inline XmlObjectSerializer_t3967301761 * get_formatter_2() const { return ___formatter_2; }
	inline XmlObjectSerializer_t3967301761 ** get_address_of_formatter_2() { return &___formatter_2; }
	inline void set_formatter_2(XmlObjectSerializer_t3967301761 * value)
	{
		___formatter_2 = value;
		Il2CppCodeGenWriteBarrier(&___formatter_2, value);
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(DefaultAddressHeader_t2516210953, ___value_3)); }
	inline Il2CppObject * get_value_3() const { return ___value_3; }
	inline Il2CppObject ** get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(Il2CppObject * value)
	{
		___value_3 = value;
		Il2CppCodeGenWriteBarrier(&___value_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
