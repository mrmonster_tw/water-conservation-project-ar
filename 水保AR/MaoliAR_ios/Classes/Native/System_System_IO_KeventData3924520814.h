﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_System_IO_kevent2406656960.h"

// System.IO.FileSystemWatcher
struct FileSystemWatcher_t416760199;
// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t1853889766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.KeventData
struct  KeventData_t3924520814  : public Il2CppObject
{
public:
	// System.IO.FileSystemWatcher System.IO.KeventData::FSW
	FileSystemWatcher_t416760199 * ___FSW_0;
	// System.String System.IO.KeventData::Directory
	String_t* ___Directory_1;
	// System.String System.IO.KeventData::FileMask
	String_t* ___FileMask_2;
	// System.Boolean System.IO.KeventData::IncludeSubdirs
	bool ___IncludeSubdirs_3;
	// System.Boolean System.IO.KeventData::Enabled
	bool ___Enabled_4;
	// System.Collections.Hashtable System.IO.KeventData::DirEntries
	Hashtable_t1853889766 * ___DirEntries_5;
	// System.IO.kevent System.IO.KeventData::ev
	kevent_t2406656960  ___ev_6;

public:
	inline static int32_t get_offset_of_FSW_0() { return static_cast<int32_t>(offsetof(KeventData_t3924520814, ___FSW_0)); }
	inline FileSystemWatcher_t416760199 * get_FSW_0() const { return ___FSW_0; }
	inline FileSystemWatcher_t416760199 ** get_address_of_FSW_0() { return &___FSW_0; }
	inline void set_FSW_0(FileSystemWatcher_t416760199 * value)
	{
		___FSW_0 = value;
		Il2CppCodeGenWriteBarrier(&___FSW_0, value);
	}

	inline static int32_t get_offset_of_Directory_1() { return static_cast<int32_t>(offsetof(KeventData_t3924520814, ___Directory_1)); }
	inline String_t* get_Directory_1() const { return ___Directory_1; }
	inline String_t** get_address_of_Directory_1() { return &___Directory_1; }
	inline void set_Directory_1(String_t* value)
	{
		___Directory_1 = value;
		Il2CppCodeGenWriteBarrier(&___Directory_1, value);
	}

	inline static int32_t get_offset_of_FileMask_2() { return static_cast<int32_t>(offsetof(KeventData_t3924520814, ___FileMask_2)); }
	inline String_t* get_FileMask_2() const { return ___FileMask_2; }
	inline String_t** get_address_of_FileMask_2() { return &___FileMask_2; }
	inline void set_FileMask_2(String_t* value)
	{
		___FileMask_2 = value;
		Il2CppCodeGenWriteBarrier(&___FileMask_2, value);
	}

	inline static int32_t get_offset_of_IncludeSubdirs_3() { return static_cast<int32_t>(offsetof(KeventData_t3924520814, ___IncludeSubdirs_3)); }
	inline bool get_IncludeSubdirs_3() const { return ___IncludeSubdirs_3; }
	inline bool* get_address_of_IncludeSubdirs_3() { return &___IncludeSubdirs_3; }
	inline void set_IncludeSubdirs_3(bool value)
	{
		___IncludeSubdirs_3 = value;
	}

	inline static int32_t get_offset_of_Enabled_4() { return static_cast<int32_t>(offsetof(KeventData_t3924520814, ___Enabled_4)); }
	inline bool get_Enabled_4() const { return ___Enabled_4; }
	inline bool* get_address_of_Enabled_4() { return &___Enabled_4; }
	inline void set_Enabled_4(bool value)
	{
		___Enabled_4 = value;
	}

	inline static int32_t get_offset_of_DirEntries_5() { return static_cast<int32_t>(offsetof(KeventData_t3924520814, ___DirEntries_5)); }
	inline Hashtable_t1853889766 * get_DirEntries_5() const { return ___DirEntries_5; }
	inline Hashtable_t1853889766 ** get_address_of_DirEntries_5() { return &___DirEntries_5; }
	inline void set_DirEntries_5(Hashtable_t1853889766 * value)
	{
		___DirEntries_5 = value;
		Il2CppCodeGenWriteBarrier(&___DirEntries_5, value);
	}

	inline static int32_t get_offset_of_ev_6() { return static_cast<int32_t>(offsetof(KeventData_t3924520814, ___ev_6)); }
	inline kevent_t2406656960  get_ev_6() const { return ___ev_6; }
	inline kevent_t2406656960 * get_address_of_ev_6() { return &___ev_6; }
	inline void set_ev_6(kevent_t2406656960  value)
	{
		___ev_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
