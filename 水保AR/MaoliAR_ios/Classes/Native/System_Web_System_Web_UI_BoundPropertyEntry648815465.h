﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_PropertyEntry4184962072.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.BoundPropertyEntry
struct  BoundPropertyEntry_t648815465  : public PropertyEntry_t4184962072
{
public:
	// System.String System.Web.UI.BoundPropertyEntry::expression
	String_t* ___expression_3;
	// System.String System.Web.UI.BoundPropertyEntry::expressionPrefix
	String_t* ___expressionPrefix_4;
	// System.Boolean System.Web.UI.BoundPropertyEntry::generated
	bool ___generated_5;
	// System.Boolean System.Web.UI.BoundPropertyEntry::useSetAttribute
	bool ___useSetAttribute_6;

public:
	inline static int32_t get_offset_of_expression_3() { return static_cast<int32_t>(offsetof(BoundPropertyEntry_t648815465, ___expression_3)); }
	inline String_t* get_expression_3() const { return ___expression_3; }
	inline String_t** get_address_of_expression_3() { return &___expression_3; }
	inline void set_expression_3(String_t* value)
	{
		___expression_3 = value;
		Il2CppCodeGenWriteBarrier(&___expression_3, value);
	}

	inline static int32_t get_offset_of_expressionPrefix_4() { return static_cast<int32_t>(offsetof(BoundPropertyEntry_t648815465, ___expressionPrefix_4)); }
	inline String_t* get_expressionPrefix_4() const { return ___expressionPrefix_4; }
	inline String_t** get_address_of_expressionPrefix_4() { return &___expressionPrefix_4; }
	inline void set_expressionPrefix_4(String_t* value)
	{
		___expressionPrefix_4 = value;
		Il2CppCodeGenWriteBarrier(&___expressionPrefix_4, value);
	}

	inline static int32_t get_offset_of_generated_5() { return static_cast<int32_t>(offsetof(BoundPropertyEntry_t648815465, ___generated_5)); }
	inline bool get_generated_5() const { return ___generated_5; }
	inline bool* get_address_of_generated_5() { return &___generated_5; }
	inline void set_generated_5(bool value)
	{
		___generated_5 = value;
	}

	inline static int32_t get_offset_of_useSetAttribute_6() { return static_cast<int32_t>(offsetof(BoundPropertyEntry_t648815465, ___useSetAttribute_6)); }
	inline bool get_useSetAttribute_6() const { return ___useSetAttribute_6; }
	inline bool* get_address_of_useSetAttribute_6() { return &___useSetAttribute_6; }
	inline void set_useSetAttribute_6(bool value)
	{
		___useSetAttribute_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
