﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Core_System_IO_Pipes_UnixNamedPipe2806256776.h"

// Microsoft.Win32.SafeHandles.SafePipeHandle
struct SafePipeHandle_t1989113880;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Pipes.UnixNamedPipeServer
struct  UnixNamedPipeServer_t2839746932  : public UnixNamedPipe_t2806256776
{
public:
	// Microsoft.Win32.SafeHandles.SafePipeHandle System.IO.Pipes.UnixNamedPipeServer::handle
	SafePipeHandle_t1989113880 * ___handle_0;
	// System.Boolean System.IO.Pipes.UnixNamedPipeServer::should_close_handle
	bool ___should_close_handle_1;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(UnixNamedPipeServer_t2839746932, ___handle_0)); }
	inline SafePipeHandle_t1989113880 * get_handle_0() const { return ___handle_0; }
	inline SafePipeHandle_t1989113880 ** get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(SafePipeHandle_t1989113880 * value)
	{
		___handle_0 = value;
		Il2CppCodeGenWriteBarrier(&___handle_0, value);
	}

	inline static int32_t get_offset_of_should_close_handle_1() { return static_cast<int32_t>(offsetof(UnixNamedPipeServer_t2839746932, ___should_close_handle_1)); }
	inline bool get_should_close_handle_1() const { return ___should_close_handle_1; }
	inline bool* get_address_of_should_close_handle_1() { return &___should_close_handle_1; }
	inline void set_should_close_handle_1(bool value)
	{
		___should_close_handle_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
