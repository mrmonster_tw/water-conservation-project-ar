﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.PeerNodeAddress
struct PeerNodeAddress_t2098027372;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.PeerResolvers.ConnectInfoDC
struct  ConnectInfoDC_t2606082360  : public Il2CppObject
{
public:
	// System.ServiceModel.PeerNodeAddress System.ServiceModel.PeerResolvers.ConnectInfoDC::<Address>k__BackingField
	PeerNodeAddress_t2098027372 * ___U3CAddressU3Ek__BackingField_0;
	// System.UInt64 System.ServiceModel.PeerResolvers.ConnectInfoDC::<NodeId>k__BackingField
	uint64_t ___U3CNodeIdU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CAddressU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ConnectInfoDC_t2606082360, ___U3CAddressU3Ek__BackingField_0)); }
	inline PeerNodeAddress_t2098027372 * get_U3CAddressU3Ek__BackingField_0() const { return ___U3CAddressU3Ek__BackingField_0; }
	inline PeerNodeAddress_t2098027372 ** get_address_of_U3CAddressU3Ek__BackingField_0() { return &___U3CAddressU3Ek__BackingField_0; }
	inline void set_U3CAddressU3Ek__BackingField_0(PeerNodeAddress_t2098027372 * value)
	{
		___U3CAddressU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAddressU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CNodeIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ConnectInfoDC_t2606082360, ___U3CNodeIdU3Ek__BackingField_1)); }
	inline uint64_t get_U3CNodeIdU3Ek__BackingField_1() const { return ___U3CNodeIdU3Ek__BackingField_1; }
	inline uint64_t* get_address_of_U3CNodeIdU3Ek__BackingField_1() { return &___U3CNodeIdU3Ek__BackingField_1; }
	inline void set_U3CNodeIdU3Ek__BackingField_1(uint64_t value)
	{
		___U3CNodeIdU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
