﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.Web.Compilation.ILocation
struct ILocation_t3961726794;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.ServerSideScript
struct  ServerSideScript_t2595428029  : public Il2CppObject
{
public:
	// System.String System.Web.UI.ServerSideScript::Script
	String_t* ___Script_0;
	// System.Web.Compilation.ILocation System.Web.UI.ServerSideScript::Location
	Il2CppObject * ___Location_1;

public:
	inline static int32_t get_offset_of_Script_0() { return static_cast<int32_t>(offsetof(ServerSideScript_t2595428029, ___Script_0)); }
	inline String_t* get_Script_0() const { return ___Script_0; }
	inline String_t** get_address_of_Script_0() { return &___Script_0; }
	inline void set_Script_0(String_t* value)
	{
		___Script_0 = value;
		Il2CppCodeGenWriteBarrier(&___Script_0, value);
	}

	inline static int32_t get_offset_of_Location_1() { return static_cast<int32_t>(offsetof(ServerSideScript_t2595428029, ___Location_1)); }
	inline Il2CppObject * get_Location_1() const { return ___Location_1; }
	inline Il2CppObject ** get_address_of_Location_1() { return &___Location_1; }
	inline void set_Location_1(Il2CppObject * value)
	{
		___Location_1 = value;
		Il2CppCodeGenWriteBarrier(&___Location_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
