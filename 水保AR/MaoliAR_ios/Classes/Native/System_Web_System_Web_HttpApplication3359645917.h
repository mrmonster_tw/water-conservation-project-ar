﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Object
struct Il2CppObject;
// System.Diagnostics.PerformanceCounter
struct PerformanceCounter_t1972251440;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Web.HttpContext
struct HttpContext_t1969259010;
// System.Web.SessionState.HttpSessionState
struct HttpSessionState_t3146594024;
// System.ComponentModel.ISite
struct ISite_t4006303512;
// System.Web.HttpModuleCollection
struct HttpModuleCollection_t38260146;
// System.String
struct String_t;
// System.Web.IHttpHandlerFactory
struct IHttpHandlerFactory_t4271213456;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t451242010;
// System.Web.AsyncRequestState
struct AsyncRequestState_t2506828551;
// System.Web.AsyncInvoker
struct AsyncInvoker_t1593358297;
// System.ComponentModel.EventHandlerList
struct EventHandlerList_t1108123056;
// System.Globalization.CultureInfo
struct CultureInfo_t4157843068;
// System.Security.Principal.IPrincipal
struct IPrincipal_t2343618843;
// System.Exception
struct Exception_t1436737249;
// System.EventHandler
struct EventHandler_t1348719766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpApplication
struct  HttpApplication_t3359645917  : public Il2CppObject
{
public:
	// System.Object System.Web.HttpApplication::this_lock
	Il2CppObject * ___this_lock_4;
	// System.Web.HttpContext System.Web.HttpApplication::context
	HttpContext_t1969259010 * ___context_5;
	// System.Web.SessionState.HttpSessionState System.Web.HttpApplication::session
	HttpSessionState_t3146594024 * ___session_6;
	// System.ComponentModel.ISite System.Web.HttpApplication::isite
	Il2CppObject * ___isite_7;
	// System.Web.HttpModuleCollection System.Web.HttpApplication::modcoll
	HttpModuleCollection_t38260146 * ___modcoll_8;
	// System.String System.Web.HttpApplication::assemblyLocation
	String_t* ___assemblyLocation_9;
	// System.Web.IHttpHandlerFactory System.Web.HttpApplication::factory
	Il2CppObject * ___factory_10;
	// System.Boolean System.Web.HttpApplication::autoCulture
	bool ___autoCulture_11;
	// System.Boolean System.Web.HttpApplication::autoUICulture
	bool ___autoUICulture_12;
	// System.Boolean System.Web.HttpApplication::stop_processing
	bool ___stop_processing_13;
	// System.Boolean System.Web.HttpApplication::in_application_start
	bool ___in_application_start_14;
	// System.Collections.IEnumerator System.Web.HttpApplication::pipeline
	Il2CppObject * ___pipeline_15;
	// System.Threading.ManualResetEvent System.Web.HttpApplication::done
	ManualResetEvent_t451242010 * ___done_16;
	// System.Web.AsyncRequestState System.Web.HttpApplication::begin_iar
	AsyncRequestState_t2506828551 * ___begin_iar_17;
	// System.Web.AsyncInvoker System.Web.HttpApplication::current_ai
	AsyncInvoker_t1593358297 * ___current_ai_18;
	// System.ComponentModel.EventHandlerList System.Web.HttpApplication::events
	EventHandlerList_t1108123056 * ___events_19;
	// System.ComponentModel.EventHandlerList System.Web.HttpApplication::nonApplicationEvents
	EventHandlerList_t1108123056 * ___nonApplicationEvents_20;
	// System.Globalization.CultureInfo System.Web.HttpApplication::app_culture
	CultureInfo_t4157843068 * ___app_culture_21;
	// System.Globalization.CultureInfo System.Web.HttpApplication::appui_culture
	CultureInfo_t4157843068 * ___appui_culture_22;
	// System.Globalization.CultureInfo System.Web.HttpApplication::prev_app_culture
	CultureInfo_t4157843068 * ___prev_app_culture_23;
	// System.Globalization.CultureInfo System.Web.HttpApplication::prev_appui_culture
	CultureInfo_t4157843068 * ___prev_appui_culture_24;
	// System.Security.Principal.IPrincipal System.Web.HttpApplication::prev_user
	Il2CppObject * ___prev_user_25;
	// System.Boolean System.Web.HttpApplication::removeConfigurationFromCache
	bool ___removeConfigurationFromCache_28;
	// System.Boolean System.Web.HttpApplication::fullInitComplete
	bool ___fullInitComplete_29;
	// System.Boolean System.Web.HttpApplication::must_yield
	bool ___must_yield_30;
	// System.Boolean System.Web.HttpApplication::in_begin
	bool ___in_begin_31;
	// System.EventHandler System.Web.HttpApplication::DefaultAuthentication
	EventHandler_t1348719766 * ___DefaultAuthentication_54;

public:
	inline static int32_t get_offset_of_this_lock_4() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917, ___this_lock_4)); }
	inline Il2CppObject * get_this_lock_4() const { return ___this_lock_4; }
	inline Il2CppObject ** get_address_of_this_lock_4() { return &___this_lock_4; }
	inline void set_this_lock_4(Il2CppObject * value)
	{
		___this_lock_4 = value;
		Il2CppCodeGenWriteBarrier(&___this_lock_4, value);
	}

	inline static int32_t get_offset_of_context_5() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917, ___context_5)); }
	inline HttpContext_t1969259010 * get_context_5() const { return ___context_5; }
	inline HttpContext_t1969259010 ** get_address_of_context_5() { return &___context_5; }
	inline void set_context_5(HttpContext_t1969259010 * value)
	{
		___context_5 = value;
		Il2CppCodeGenWriteBarrier(&___context_5, value);
	}

	inline static int32_t get_offset_of_session_6() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917, ___session_6)); }
	inline HttpSessionState_t3146594024 * get_session_6() const { return ___session_6; }
	inline HttpSessionState_t3146594024 ** get_address_of_session_6() { return &___session_6; }
	inline void set_session_6(HttpSessionState_t3146594024 * value)
	{
		___session_6 = value;
		Il2CppCodeGenWriteBarrier(&___session_6, value);
	}

	inline static int32_t get_offset_of_isite_7() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917, ___isite_7)); }
	inline Il2CppObject * get_isite_7() const { return ___isite_7; }
	inline Il2CppObject ** get_address_of_isite_7() { return &___isite_7; }
	inline void set_isite_7(Il2CppObject * value)
	{
		___isite_7 = value;
		Il2CppCodeGenWriteBarrier(&___isite_7, value);
	}

	inline static int32_t get_offset_of_modcoll_8() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917, ___modcoll_8)); }
	inline HttpModuleCollection_t38260146 * get_modcoll_8() const { return ___modcoll_8; }
	inline HttpModuleCollection_t38260146 ** get_address_of_modcoll_8() { return &___modcoll_8; }
	inline void set_modcoll_8(HttpModuleCollection_t38260146 * value)
	{
		___modcoll_8 = value;
		Il2CppCodeGenWriteBarrier(&___modcoll_8, value);
	}

	inline static int32_t get_offset_of_assemblyLocation_9() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917, ___assemblyLocation_9)); }
	inline String_t* get_assemblyLocation_9() const { return ___assemblyLocation_9; }
	inline String_t** get_address_of_assemblyLocation_9() { return &___assemblyLocation_9; }
	inline void set_assemblyLocation_9(String_t* value)
	{
		___assemblyLocation_9 = value;
		Il2CppCodeGenWriteBarrier(&___assemblyLocation_9, value);
	}

	inline static int32_t get_offset_of_factory_10() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917, ___factory_10)); }
	inline Il2CppObject * get_factory_10() const { return ___factory_10; }
	inline Il2CppObject ** get_address_of_factory_10() { return &___factory_10; }
	inline void set_factory_10(Il2CppObject * value)
	{
		___factory_10 = value;
		Il2CppCodeGenWriteBarrier(&___factory_10, value);
	}

	inline static int32_t get_offset_of_autoCulture_11() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917, ___autoCulture_11)); }
	inline bool get_autoCulture_11() const { return ___autoCulture_11; }
	inline bool* get_address_of_autoCulture_11() { return &___autoCulture_11; }
	inline void set_autoCulture_11(bool value)
	{
		___autoCulture_11 = value;
	}

	inline static int32_t get_offset_of_autoUICulture_12() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917, ___autoUICulture_12)); }
	inline bool get_autoUICulture_12() const { return ___autoUICulture_12; }
	inline bool* get_address_of_autoUICulture_12() { return &___autoUICulture_12; }
	inline void set_autoUICulture_12(bool value)
	{
		___autoUICulture_12 = value;
	}

	inline static int32_t get_offset_of_stop_processing_13() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917, ___stop_processing_13)); }
	inline bool get_stop_processing_13() const { return ___stop_processing_13; }
	inline bool* get_address_of_stop_processing_13() { return &___stop_processing_13; }
	inline void set_stop_processing_13(bool value)
	{
		___stop_processing_13 = value;
	}

	inline static int32_t get_offset_of_in_application_start_14() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917, ___in_application_start_14)); }
	inline bool get_in_application_start_14() const { return ___in_application_start_14; }
	inline bool* get_address_of_in_application_start_14() { return &___in_application_start_14; }
	inline void set_in_application_start_14(bool value)
	{
		___in_application_start_14 = value;
	}

	inline static int32_t get_offset_of_pipeline_15() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917, ___pipeline_15)); }
	inline Il2CppObject * get_pipeline_15() const { return ___pipeline_15; }
	inline Il2CppObject ** get_address_of_pipeline_15() { return &___pipeline_15; }
	inline void set_pipeline_15(Il2CppObject * value)
	{
		___pipeline_15 = value;
		Il2CppCodeGenWriteBarrier(&___pipeline_15, value);
	}

	inline static int32_t get_offset_of_done_16() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917, ___done_16)); }
	inline ManualResetEvent_t451242010 * get_done_16() const { return ___done_16; }
	inline ManualResetEvent_t451242010 ** get_address_of_done_16() { return &___done_16; }
	inline void set_done_16(ManualResetEvent_t451242010 * value)
	{
		___done_16 = value;
		Il2CppCodeGenWriteBarrier(&___done_16, value);
	}

	inline static int32_t get_offset_of_begin_iar_17() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917, ___begin_iar_17)); }
	inline AsyncRequestState_t2506828551 * get_begin_iar_17() const { return ___begin_iar_17; }
	inline AsyncRequestState_t2506828551 ** get_address_of_begin_iar_17() { return &___begin_iar_17; }
	inline void set_begin_iar_17(AsyncRequestState_t2506828551 * value)
	{
		___begin_iar_17 = value;
		Il2CppCodeGenWriteBarrier(&___begin_iar_17, value);
	}

	inline static int32_t get_offset_of_current_ai_18() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917, ___current_ai_18)); }
	inline AsyncInvoker_t1593358297 * get_current_ai_18() const { return ___current_ai_18; }
	inline AsyncInvoker_t1593358297 ** get_address_of_current_ai_18() { return &___current_ai_18; }
	inline void set_current_ai_18(AsyncInvoker_t1593358297 * value)
	{
		___current_ai_18 = value;
		Il2CppCodeGenWriteBarrier(&___current_ai_18, value);
	}

	inline static int32_t get_offset_of_events_19() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917, ___events_19)); }
	inline EventHandlerList_t1108123056 * get_events_19() const { return ___events_19; }
	inline EventHandlerList_t1108123056 ** get_address_of_events_19() { return &___events_19; }
	inline void set_events_19(EventHandlerList_t1108123056 * value)
	{
		___events_19 = value;
		Il2CppCodeGenWriteBarrier(&___events_19, value);
	}

	inline static int32_t get_offset_of_nonApplicationEvents_20() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917, ___nonApplicationEvents_20)); }
	inline EventHandlerList_t1108123056 * get_nonApplicationEvents_20() const { return ___nonApplicationEvents_20; }
	inline EventHandlerList_t1108123056 ** get_address_of_nonApplicationEvents_20() { return &___nonApplicationEvents_20; }
	inline void set_nonApplicationEvents_20(EventHandlerList_t1108123056 * value)
	{
		___nonApplicationEvents_20 = value;
		Il2CppCodeGenWriteBarrier(&___nonApplicationEvents_20, value);
	}

	inline static int32_t get_offset_of_app_culture_21() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917, ___app_culture_21)); }
	inline CultureInfo_t4157843068 * get_app_culture_21() const { return ___app_culture_21; }
	inline CultureInfo_t4157843068 ** get_address_of_app_culture_21() { return &___app_culture_21; }
	inline void set_app_culture_21(CultureInfo_t4157843068 * value)
	{
		___app_culture_21 = value;
		Il2CppCodeGenWriteBarrier(&___app_culture_21, value);
	}

	inline static int32_t get_offset_of_appui_culture_22() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917, ___appui_culture_22)); }
	inline CultureInfo_t4157843068 * get_appui_culture_22() const { return ___appui_culture_22; }
	inline CultureInfo_t4157843068 ** get_address_of_appui_culture_22() { return &___appui_culture_22; }
	inline void set_appui_culture_22(CultureInfo_t4157843068 * value)
	{
		___appui_culture_22 = value;
		Il2CppCodeGenWriteBarrier(&___appui_culture_22, value);
	}

	inline static int32_t get_offset_of_prev_app_culture_23() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917, ___prev_app_culture_23)); }
	inline CultureInfo_t4157843068 * get_prev_app_culture_23() const { return ___prev_app_culture_23; }
	inline CultureInfo_t4157843068 ** get_address_of_prev_app_culture_23() { return &___prev_app_culture_23; }
	inline void set_prev_app_culture_23(CultureInfo_t4157843068 * value)
	{
		___prev_app_culture_23 = value;
		Il2CppCodeGenWriteBarrier(&___prev_app_culture_23, value);
	}

	inline static int32_t get_offset_of_prev_appui_culture_24() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917, ___prev_appui_culture_24)); }
	inline CultureInfo_t4157843068 * get_prev_appui_culture_24() const { return ___prev_appui_culture_24; }
	inline CultureInfo_t4157843068 ** get_address_of_prev_appui_culture_24() { return &___prev_appui_culture_24; }
	inline void set_prev_appui_culture_24(CultureInfo_t4157843068 * value)
	{
		___prev_appui_culture_24 = value;
		Il2CppCodeGenWriteBarrier(&___prev_appui_culture_24, value);
	}

	inline static int32_t get_offset_of_prev_user_25() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917, ___prev_user_25)); }
	inline Il2CppObject * get_prev_user_25() const { return ___prev_user_25; }
	inline Il2CppObject ** get_address_of_prev_user_25() { return &___prev_user_25; }
	inline void set_prev_user_25(Il2CppObject * value)
	{
		___prev_user_25 = value;
		Il2CppCodeGenWriteBarrier(&___prev_user_25, value);
	}

	inline static int32_t get_offset_of_removeConfigurationFromCache_28() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917, ___removeConfigurationFromCache_28)); }
	inline bool get_removeConfigurationFromCache_28() const { return ___removeConfigurationFromCache_28; }
	inline bool* get_address_of_removeConfigurationFromCache_28() { return &___removeConfigurationFromCache_28; }
	inline void set_removeConfigurationFromCache_28(bool value)
	{
		___removeConfigurationFromCache_28 = value;
	}

	inline static int32_t get_offset_of_fullInitComplete_29() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917, ___fullInitComplete_29)); }
	inline bool get_fullInitComplete_29() const { return ___fullInitComplete_29; }
	inline bool* get_address_of_fullInitComplete_29() { return &___fullInitComplete_29; }
	inline void set_fullInitComplete_29(bool value)
	{
		___fullInitComplete_29 = value;
	}

	inline static int32_t get_offset_of_must_yield_30() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917, ___must_yield_30)); }
	inline bool get_must_yield_30() const { return ___must_yield_30; }
	inline bool* get_address_of_must_yield_30() { return &___must_yield_30; }
	inline void set_must_yield_30(bool value)
	{
		___must_yield_30 = value;
	}

	inline static int32_t get_offset_of_in_begin_31() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917, ___in_begin_31)); }
	inline bool get_in_begin_31() const { return ___in_begin_31; }
	inline bool* get_address_of_in_begin_31() { return &___in_begin_31; }
	inline void set_in_begin_31(bool value)
	{
		___in_begin_31 = value;
	}

	inline static int32_t get_offset_of_DefaultAuthentication_54() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917, ___DefaultAuthentication_54)); }
	inline EventHandler_t1348719766 * get_DefaultAuthentication_54() const { return ___DefaultAuthentication_54; }
	inline EventHandler_t1348719766 ** get_address_of_DefaultAuthentication_54() { return &___DefaultAuthentication_54; }
	inline void set_DefaultAuthentication_54(EventHandler_t1348719766 * value)
	{
		___DefaultAuthentication_54 = value;
		Il2CppCodeGenWriteBarrier(&___DefaultAuthentication_54, value);
	}
};

struct HttpApplication_t3359645917_StaticFields
{
public:
	// System.Object System.Web.HttpApplication::disposedEvent
	Il2CppObject * ___disposedEvent_0;
	// System.Object System.Web.HttpApplication::errorEvent
	Il2CppObject * ___errorEvent_1;
	// System.Diagnostics.PerformanceCounter System.Web.HttpApplication::requests_total_counter
	PerformanceCounter_t1972251440 * ___requests_total_counter_2;
	// System.String[] System.Web.HttpApplication::BinDirs
	StringU5BU5D_t1281789340* ___BinDirs_3;
	// System.String System.Web.HttpApplication::binDirectory
	String_t* ___binDirectory_26;
	// System.Exception System.Web.HttpApplication::initialization_exception
	Exception_t1436737249 * ___initialization_exception_27;
	// System.Object System.Web.HttpApplication::PreSendRequestHeadersEvent
	Il2CppObject * ___PreSendRequestHeadersEvent_32;
	// System.Object System.Web.HttpApplication::PreSendRequestContentEvent
	Il2CppObject * ___PreSendRequestContentEvent_33;
	// System.Object System.Web.HttpApplication::AcquireRequestStateEvent
	Il2CppObject * ___AcquireRequestStateEvent_34;
	// System.Object System.Web.HttpApplication::AuthenticateRequestEvent
	Il2CppObject * ___AuthenticateRequestEvent_35;
	// System.Object System.Web.HttpApplication::AuthorizeRequestEvent
	Il2CppObject * ___AuthorizeRequestEvent_36;
	// System.Object System.Web.HttpApplication::BeginRequestEvent
	Il2CppObject * ___BeginRequestEvent_37;
	// System.Object System.Web.HttpApplication::EndRequestEvent
	Il2CppObject * ___EndRequestEvent_38;
	// System.Object System.Web.HttpApplication::PostRequestHandlerExecuteEvent
	Il2CppObject * ___PostRequestHandlerExecuteEvent_39;
	// System.Object System.Web.HttpApplication::PreRequestHandlerExecuteEvent
	Il2CppObject * ___PreRequestHandlerExecuteEvent_40;
	// System.Object System.Web.HttpApplication::ReleaseRequestStateEvent
	Il2CppObject * ___ReleaseRequestStateEvent_41;
	// System.Object System.Web.HttpApplication::ResolveRequestCacheEvent
	Il2CppObject * ___ResolveRequestCacheEvent_42;
	// System.Object System.Web.HttpApplication::UpdateRequestCacheEvent
	Il2CppObject * ___UpdateRequestCacheEvent_43;
	// System.Object System.Web.HttpApplication::PostAuthenticateRequestEvent
	Il2CppObject * ___PostAuthenticateRequestEvent_44;
	// System.Object System.Web.HttpApplication::PostAuthorizeRequestEvent
	Il2CppObject * ___PostAuthorizeRequestEvent_45;
	// System.Object System.Web.HttpApplication::PostResolveRequestCacheEvent
	Il2CppObject * ___PostResolveRequestCacheEvent_46;
	// System.Object System.Web.HttpApplication::PostMapRequestHandlerEvent
	Il2CppObject * ___PostMapRequestHandlerEvent_47;
	// System.Object System.Web.HttpApplication::PostAcquireRequestStateEvent
	Il2CppObject * ___PostAcquireRequestStateEvent_48;
	// System.Object System.Web.HttpApplication::PostReleaseRequestStateEvent
	Il2CppObject * ___PostReleaseRequestStateEvent_49;
	// System.Object System.Web.HttpApplication::PostUpdateRequestCacheEvent
	Il2CppObject * ___PostUpdateRequestCacheEvent_50;
	// System.Object System.Web.HttpApplication::LogRequestEvent
	Il2CppObject * ___LogRequestEvent_51;
	// System.Object System.Web.HttpApplication::MapRequestHandlerEvent
	Il2CppObject * ___MapRequestHandlerEvent_52;
	// System.Object System.Web.HttpApplication::PostLogRequestEvent
	Il2CppObject * ___PostLogRequestEvent_53;

public:
	inline static int32_t get_offset_of_disposedEvent_0() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917_StaticFields, ___disposedEvent_0)); }
	inline Il2CppObject * get_disposedEvent_0() const { return ___disposedEvent_0; }
	inline Il2CppObject ** get_address_of_disposedEvent_0() { return &___disposedEvent_0; }
	inline void set_disposedEvent_0(Il2CppObject * value)
	{
		___disposedEvent_0 = value;
		Il2CppCodeGenWriteBarrier(&___disposedEvent_0, value);
	}

	inline static int32_t get_offset_of_errorEvent_1() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917_StaticFields, ___errorEvent_1)); }
	inline Il2CppObject * get_errorEvent_1() const { return ___errorEvent_1; }
	inline Il2CppObject ** get_address_of_errorEvent_1() { return &___errorEvent_1; }
	inline void set_errorEvent_1(Il2CppObject * value)
	{
		___errorEvent_1 = value;
		Il2CppCodeGenWriteBarrier(&___errorEvent_1, value);
	}

	inline static int32_t get_offset_of_requests_total_counter_2() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917_StaticFields, ___requests_total_counter_2)); }
	inline PerformanceCounter_t1972251440 * get_requests_total_counter_2() const { return ___requests_total_counter_2; }
	inline PerformanceCounter_t1972251440 ** get_address_of_requests_total_counter_2() { return &___requests_total_counter_2; }
	inline void set_requests_total_counter_2(PerformanceCounter_t1972251440 * value)
	{
		___requests_total_counter_2 = value;
		Il2CppCodeGenWriteBarrier(&___requests_total_counter_2, value);
	}

	inline static int32_t get_offset_of_BinDirs_3() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917_StaticFields, ___BinDirs_3)); }
	inline StringU5BU5D_t1281789340* get_BinDirs_3() const { return ___BinDirs_3; }
	inline StringU5BU5D_t1281789340** get_address_of_BinDirs_3() { return &___BinDirs_3; }
	inline void set_BinDirs_3(StringU5BU5D_t1281789340* value)
	{
		___BinDirs_3 = value;
		Il2CppCodeGenWriteBarrier(&___BinDirs_3, value);
	}

	inline static int32_t get_offset_of_binDirectory_26() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917_StaticFields, ___binDirectory_26)); }
	inline String_t* get_binDirectory_26() const { return ___binDirectory_26; }
	inline String_t** get_address_of_binDirectory_26() { return &___binDirectory_26; }
	inline void set_binDirectory_26(String_t* value)
	{
		___binDirectory_26 = value;
		Il2CppCodeGenWriteBarrier(&___binDirectory_26, value);
	}

	inline static int32_t get_offset_of_initialization_exception_27() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917_StaticFields, ___initialization_exception_27)); }
	inline Exception_t1436737249 * get_initialization_exception_27() const { return ___initialization_exception_27; }
	inline Exception_t1436737249 ** get_address_of_initialization_exception_27() { return &___initialization_exception_27; }
	inline void set_initialization_exception_27(Exception_t1436737249 * value)
	{
		___initialization_exception_27 = value;
		Il2CppCodeGenWriteBarrier(&___initialization_exception_27, value);
	}

	inline static int32_t get_offset_of_PreSendRequestHeadersEvent_32() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917_StaticFields, ___PreSendRequestHeadersEvent_32)); }
	inline Il2CppObject * get_PreSendRequestHeadersEvent_32() const { return ___PreSendRequestHeadersEvent_32; }
	inline Il2CppObject ** get_address_of_PreSendRequestHeadersEvent_32() { return &___PreSendRequestHeadersEvent_32; }
	inline void set_PreSendRequestHeadersEvent_32(Il2CppObject * value)
	{
		___PreSendRequestHeadersEvent_32 = value;
		Il2CppCodeGenWriteBarrier(&___PreSendRequestHeadersEvent_32, value);
	}

	inline static int32_t get_offset_of_PreSendRequestContentEvent_33() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917_StaticFields, ___PreSendRequestContentEvent_33)); }
	inline Il2CppObject * get_PreSendRequestContentEvent_33() const { return ___PreSendRequestContentEvent_33; }
	inline Il2CppObject ** get_address_of_PreSendRequestContentEvent_33() { return &___PreSendRequestContentEvent_33; }
	inline void set_PreSendRequestContentEvent_33(Il2CppObject * value)
	{
		___PreSendRequestContentEvent_33 = value;
		Il2CppCodeGenWriteBarrier(&___PreSendRequestContentEvent_33, value);
	}

	inline static int32_t get_offset_of_AcquireRequestStateEvent_34() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917_StaticFields, ___AcquireRequestStateEvent_34)); }
	inline Il2CppObject * get_AcquireRequestStateEvent_34() const { return ___AcquireRequestStateEvent_34; }
	inline Il2CppObject ** get_address_of_AcquireRequestStateEvent_34() { return &___AcquireRequestStateEvent_34; }
	inline void set_AcquireRequestStateEvent_34(Il2CppObject * value)
	{
		___AcquireRequestStateEvent_34 = value;
		Il2CppCodeGenWriteBarrier(&___AcquireRequestStateEvent_34, value);
	}

	inline static int32_t get_offset_of_AuthenticateRequestEvent_35() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917_StaticFields, ___AuthenticateRequestEvent_35)); }
	inline Il2CppObject * get_AuthenticateRequestEvent_35() const { return ___AuthenticateRequestEvent_35; }
	inline Il2CppObject ** get_address_of_AuthenticateRequestEvent_35() { return &___AuthenticateRequestEvent_35; }
	inline void set_AuthenticateRequestEvent_35(Il2CppObject * value)
	{
		___AuthenticateRequestEvent_35 = value;
		Il2CppCodeGenWriteBarrier(&___AuthenticateRequestEvent_35, value);
	}

	inline static int32_t get_offset_of_AuthorizeRequestEvent_36() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917_StaticFields, ___AuthorizeRequestEvent_36)); }
	inline Il2CppObject * get_AuthorizeRequestEvent_36() const { return ___AuthorizeRequestEvent_36; }
	inline Il2CppObject ** get_address_of_AuthorizeRequestEvent_36() { return &___AuthorizeRequestEvent_36; }
	inline void set_AuthorizeRequestEvent_36(Il2CppObject * value)
	{
		___AuthorizeRequestEvent_36 = value;
		Il2CppCodeGenWriteBarrier(&___AuthorizeRequestEvent_36, value);
	}

	inline static int32_t get_offset_of_BeginRequestEvent_37() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917_StaticFields, ___BeginRequestEvent_37)); }
	inline Il2CppObject * get_BeginRequestEvent_37() const { return ___BeginRequestEvent_37; }
	inline Il2CppObject ** get_address_of_BeginRequestEvent_37() { return &___BeginRequestEvent_37; }
	inline void set_BeginRequestEvent_37(Il2CppObject * value)
	{
		___BeginRequestEvent_37 = value;
		Il2CppCodeGenWriteBarrier(&___BeginRequestEvent_37, value);
	}

	inline static int32_t get_offset_of_EndRequestEvent_38() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917_StaticFields, ___EndRequestEvent_38)); }
	inline Il2CppObject * get_EndRequestEvent_38() const { return ___EndRequestEvent_38; }
	inline Il2CppObject ** get_address_of_EndRequestEvent_38() { return &___EndRequestEvent_38; }
	inline void set_EndRequestEvent_38(Il2CppObject * value)
	{
		___EndRequestEvent_38 = value;
		Il2CppCodeGenWriteBarrier(&___EndRequestEvent_38, value);
	}

	inline static int32_t get_offset_of_PostRequestHandlerExecuteEvent_39() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917_StaticFields, ___PostRequestHandlerExecuteEvent_39)); }
	inline Il2CppObject * get_PostRequestHandlerExecuteEvent_39() const { return ___PostRequestHandlerExecuteEvent_39; }
	inline Il2CppObject ** get_address_of_PostRequestHandlerExecuteEvent_39() { return &___PostRequestHandlerExecuteEvent_39; }
	inline void set_PostRequestHandlerExecuteEvent_39(Il2CppObject * value)
	{
		___PostRequestHandlerExecuteEvent_39 = value;
		Il2CppCodeGenWriteBarrier(&___PostRequestHandlerExecuteEvent_39, value);
	}

	inline static int32_t get_offset_of_PreRequestHandlerExecuteEvent_40() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917_StaticFields, ___PreRequestHandlerExecuteEvent_40)); }
	inline Il2CppObject * get_PreRequestHandlerExecuteEvent_40() const { return ___PreRequestHandlerExecuteEvent_40; }
	inline Il2CppObject ** get_address_of_PreRequestHandlerExecuteEvent_40() { return &___PreRequestHandlerExecuteEvent_40; }
	inline void set_PreRequestHandlerExecuteEvent_40(Il2CppObject * value)
	{
		___PreRequestHandlerExecuteEvent_40 = value;
		Il2CppCodeGenWriteBarrier(&___PreRequestHandlerExecuteEvent_40, value);
	}

	inline static int32_t get_offset_of_ReleaseRequestStateEvent_41() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917_StaticFields, ___ReleaseRequestStateEvent_41)); }
	inline Il2CppObject * get_ReleaseRequestStateEvent_41() const { return ___ReleaseRequestStateEvent_41; }
	inline Il2CppObject ** get_address_of_ReleaseRequestStateEvent_41() { return &___ReleaseRequestStateEvent_41; }
	inline void set_ReleaseRequestStateEvent_41(Il2CppObject * value)
	{
		___ReleaseRequestStateEvent_41 = value;
		Il2CppCodeGenWriteBarrier(&___ReleaseRequestStateEvent_41, value);
	}

	inline static int32_t get_offset_of_ResolveRequestCacheEvent_42() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917_StaticFields, ___ResolveRequestCacheEvent_42)); }
	inline Il2CppObject * get_ResolveRequestCacheEvent_42() const { return ___ResolveRequestCacheEvent_42; }
	inline Il2CppObject ** get_address_of_ResolveRequestCacheEvent_42() { return &___ResolveRequestCacheEvent_42; }
	inline void set_ResolveRequestCacheEvent_42(Il2CppObject * value)
	{
		___ResolveRequestCacheEvent_42 = value;
		Il2CppCodeGenWriteBarrier(&___ResolveRequestCacheEvent_42, value);
	}

	inline static int32_t get_offset_of_UpdateRequestCacheEvent_43() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917_StaticFields, ___UpdateRequestCacheEvent_43)); }
	inline Il2CppObject * get_UpdateRequestCacheEvent_43() const { return ___UpdateRequestCacheEvent_43; }
	inline Il2CppObject ** get_address_of_UpdateRequestCacheEvent_43() { return &___UpdateRequestCacheEvent_43; }
	inline void set_UpdateRequestCacheEvent_43(Il2CppObject * value)
	{
		___UpdateRequestCacheEvent_43 = value;
		Il2CppCodeGenWriteBarrier(&___UpdateRequestCacheEvent_43, value);
	}

	inline static int32_t get_offset_of_PostAuthenticateRequestEvent_44() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917_StaticFields, ___PostAuthenticateRequestEvent_44)); }
	inline Il2CppObject * get_PostAuthenticateRequestEvent_44() const { return ___PostAuthenticateRequestEvent_44; }
	inline Il2CppObject ** get_address_of_PostAuthenticateRequestEvent_44() { return &___PostAuthenticateRequestEvent_44; }
	inline void set_PostAuthenticateRequestEvent_44(Il2CppObject * value)
	{
		___PostAuthenticateRequestEvent_44 = value;
		Il2CppCodeGenWriteBarrier(&___PostAuthenticateRequestEvent_44, value);
	}

	inline static int32_t get_offset_of_PostAuthorizeRequestEvent_45() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917_StaticFields, ___PostAuthorizeRequestEvent_45)); }
	inline Il2CppObject * get_PostAuthorizeRequestEvent_45() const { return ___PostAuthorizeRequestEvent_45; }
	inline Il2CppObject ** get_address_of_PostAuthorizeRequestEvent_45() { return &___PostAuthorizeRequestEvent_45; }
	inline void set_PostAuthorizeRequestEvent_45(Il2CppObject * value)
	{
		___PostAuthorizeRequestEvent_45 = value;
		Il2CppCodeGenWriteBarrier(&___PostAuthorizeRequestEvent_45, value);
	}

	inline static int32_t get_offset_of_PostResolveRequestCacheEvent_46() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917_StaticFields, ___PostResolveRequestCacheEvent_46)); }
	inline Il2CppObject * get_PostResolveRequestCacheEvent_46() const { return ___PostResolveRequestCacheEvent_46; }
	inline Il2CppObject ** get_address_of_PostResolveRequestCacheEvent_46() { return &___PostResolveRequestCacheEvent_46; }
	inline void set_PostResolveRequestCacheEvent_46(Il2CppObject * value)
	{
		___PostResolveRequestCacheEvent_46 = value;
		Il2CppCodeGenWriteBarrier(&___PostResolveRequestCacheEvent_46, value);
	}

	inline static int32_t get_offset_of_PostMapRequestHandlerEvent_47() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917_StaticFields, ___PostMapRequestHandlerEvent_47)); }
	inline Il2CppObject * get_PostMapRequestHandlerEvent_47() const { return ___PostMapRequestHandlerEvent_47; }
	inline Il2CppObject ** get_address_of_PostMapRequestHandlerEvent_47() { return &___PostMapRequestHandlerEvent_47; }
	inline void set_PostMapRequestHandlerEvent_47(Il2CppObject * value)
	{
		___PostMapRequestHandlerEvent_47 = value;
		Il2CppCodeGenWriteBarrier(&___PostMapRequestHandlerEvent_47, value);
	}

	inline static int32_t get_offset_of_PostAcquireRequestStateEvent_48() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917_StaticFields, ___PostAcquireRequestStateEvent_48)); }
	inline Il2CppObject * get_PostAcquireRequestStateEvent_48() const { return ___PostAcquireRequestStateEvent_48; }
	inline Il2CppObject ** get_address_of_PostAcquireRequestStateEvent_48() { return &___PostAcquireRequestStateEvent_48; }
	inline void set_PostAcquireRequestStateEvent_48(Il2CppObject * value)
	{
		___PostAcquireRequestStateEvent_48 = value;
		Il2CppCodeGenWriteBarrier(&___PostAcquireRequestStateEvent_48, value);
	}

	inline static int32_t get_offset_of_PostReleaseRequestStateEvent_49() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917_StaticFields, ___PostReleaseRequestStateEvent_49)); }
	inline Il2CppObject * get_PostReleaseRequestStateEvent_49() const { return ___PostReleaseRequestStateEvent_49; }
	inline Il2CppObject ** get_address_of_PostReleaseRequestStateEvent_49() { return &___PostReleaseRequestStateEvent_49; }
	inline void set_PostReleaseRequestStateEvent_49(Il2CppObject * value)
	{
		___PostReleaseRequestStateEvent_49 = value;
		Il2CppCodeGenWriteBarrier(&___PostReleaseRequestStateEvent_49, value);
	}

	inline static int32_t get_offset_of_PostUpdateRequestCacheEvent_50() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917_StaticFields, ___PostUpdateRequestCacheEvent_50)); }
	inline Il2CppObject * get_PostUpdateRequestCacheEvent_50() const { return ___PostUpdateRequestCacheEvent_50; }
	inline Il2CppObject ** get_address_of_PostUpdateRequestCacheEvent_50() { return &___PostUpdateRequestCacheEvent_50; }
	inline void set_PostUpdateRequestCacheEvent_50(Il2CppObject * value)
	{
		___PostUpdateRequestCacheEvent_50 = value;
		Il2CppCodeGenWriteBarrier(&___PostUpdateRequestCacheEvent_50, value);
	}

	inline static int32_t get_offset_of_LogRequestEvent_51() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917_StaticFields, ___LogRequestEvent_51)); }
	inline Il2CppObject * get_LogRequestEvent_51() const { return ___LogRequestEvent_51; }
	inline Il2CppObject ** get_address_of_LogRequestEvent_51() { return &___LogRequestEvent_51; }
	inline void set_LogRequestEvent_51(Il2CppObject * value)
	{
		___LogRequestEvent_51 = value;
		Il2CppCodeGenWriteBarrier(&___LogRequestEvent_51, value);
	}

	inline static int32_t get_offset_of_MapRequestHandlerEvent_52() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917_StaticFields, ___MapRequestHandlerEvent_52)); }
	inline Il2CppObject * get_MapRequestHandlerEvent_52() const { return ___MapRequestHandlerEvent_52; }
	inline Il2CppObject ** get_address_of_MapRequestHandlerEvent_52() { return &___MapRequestHandlerEvent_52; }
	inline void set_MapRequestHandlerEvent_52(Il2CppObject * value)
	{
		___MapRequestHandlerEvent_52 = value;
		Il2CppCodeGenWriteBarrier(&___MapRequestHandlerEvent_52, value);
	}

	inline static int32_t get_offset_of_PostLogRequestEvent_53() { return static_cast<int32_t>(offsetof(HttpApplication_t3359645917_StaticFields, ___PostLogRequestEvent_53)); }
	inline Il2CppObject * get_PostLogRequestEvent_53() const { return ___PostLogRequestEvent_53; }
	inline Il2CppObject ** get_address_of_PostLogRequestEvent_53() { return &___PostLogRequestEvent_53; }
	inline void set_PostLogRequestEvent_53(Il2CppObject * value)
	{
		___PostLogRequestEvent_53 = value;
		Il2CppCodeGenWriteBarrier(&___PostLogRequestEvent_53, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
