﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Ch715356738.h"

// System.Collections.Generic.List`1<System.ServiceModel.Channels.IDuplexChannel>
struct List_1_t410891160;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.ChannelFactoryBase`1<System.ServiceModel.Channels.IDuplexChannel>
struct  ChannelFactoryBase_1_t173304910  : public ChannelFactoryBase_t715356738
{
public:
	// System.Collections.Generic.List`1<TChannel> System.ServiceModel.Channels.ChannelFactoryBase`1::channels
	List_1_t410891160 * ___channels_13;

public:
	inline static int32_t get_offset_of_channels_13() { return static_cast<int32_t>(offsetof(ChannelFactoryBase_1_t173304910, ___channels_13)); }
	inline List_1_t410891160 * get_channels_13() const { return ___channels_13; }
	inline List_1_t410891160 ** get_address_of_channels_13() { return &___channels_13; }
	inline void set_channels_13(List_1_t410891160 * value)
	{
		___channels_13 = value;
		Il2CppCodeGenWriteBarrier(&___channels_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
