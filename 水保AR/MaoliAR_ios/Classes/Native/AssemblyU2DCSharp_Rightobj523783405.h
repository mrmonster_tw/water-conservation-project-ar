﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// CubeScale
struct CubeScale_t43718014;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rightobj
struct  Rightobj_t523783405  : public MonoBehaviour_t3962482529
{
public:
	// CubeScale Rightobj::cs
	CubeScale_t43718014 * ___cs_2;

public:
	inline static int32_t get_offset_of_cs_2() { return static_cast<int32_t>(offsetof(Rightobj_t523783405, ___cs_2)); }
	inline CubeScale_t43718014 * get_cs_2() const { return ___cs_2; }
	inline CubeScale_t43718014 ** get_address_of_cs_2() { return &___cs_2; }
	inline void set_cs_2(CubeScale_t43718014 * value)
	{
		___cs_2 = value;
		Il2CppCodeGenWriteBarrier(&___cs_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
