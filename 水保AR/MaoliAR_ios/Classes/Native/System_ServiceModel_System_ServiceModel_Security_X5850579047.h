﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Security.X509ServiceCertificateAuthentication
struct X509ServiceCertificateAuthentication_t3406728790;
// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t714049126;
// System.Collections.Generic.Dictionary`2<System.Uri,System.Security.Cryptography.X509Certificates.X509Certificate2>
struct Dictionary_2_t4185929986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.X509CertificateRecipientClientCredential
struct  X509CertificateRecipientClientCredential_t850579047  : public Il2CppObject
{
public:
	// System.ServiceModel.Security.X509ServiceCertificateAuthentication System.ServiceModel.Security.X509CertificateRecipientClientCredential::auth
	X509ServiceCertificateAuthentication_t3406728790 * ___auth_0;
	// System.Security.Cryptography.X509Certificates.X509Certificate2 System.ServiceModel.Security.X509CertificateRecipientClientCredential::certificate
	X509Certificate2_t714049126 * ___certificate_1;
	// System.Collections.Generic.Dictionary`2<System.Uri,System.Security.Cryptography.X509Certificates.X509Certificate2> System.ServiceModel.Security.X509CertificateRecipientClientCredential::scoped
	Dictionary_2_t4185929986 * ___scoped_2;

public:
	inline static int32_t get_offset_of_auth_0() { return static_cast<int32_t>(offsetof(X509CertificateRecipientClientCredential_t850579047, ___auth_0)); }
	inline X509ServiceCertificateAuthentication_t3406728790 * get_auth_0() const { return ___auth_0; }
	inline X509ServiceCertificateAuthentication_t3406728790 ** get_address_of_auth_0() { return &___auth_0; }
	inline void set_auth_0(X509ServiceCertificateAuthentication_t3406728790 * value)
	{
		___auth_0 = value;
		Il2CppCodeGenWriteBarrier(&___auth_0, value);
	}

	inline static int32_t get_offset_of_certificate_1() { return static_cast<int32_t>(offsetof(X509CertificateRecipientClientCredential_t850579047, ___certificate_1)); }
	inline X509Certificate2_t714049126 * get_certificate_1() const { return ___certificate_1; }
	inline X509Certificate2_t714049126 ** get_address_of_certificate_1() { return &___certificate_1; }
	inline void set_certificate_1(X509Certificate2_t714049126 * value)
	{
		___certificate_1 = value;
		Il2CppCodeGenWriteBarrier(&___certificate_1, value);
	}

	inline static int32_t get_offset_of_scoped_2() { return static_cast<int32_t>(offsetof(X509CertificateRecipientClientCredential_t850579047, ___scoped_2)); }
	inline Dictionary_2_t4185929986 * get_scoped_2() const { return ___scoped_2; }
	inline Dictionary_2_t4185929986 ** get_address_of_scoped_2() { return &___scoped_2; }
	inline void set_scoped_2(Dictionary_2_t4185929986 * value)
	{
		___scoped_2 = value;
		Il2CppCodeGenWriteBarrier(&___scoped_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
