﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"

// System.Collections.Generic.SortedList`2<System.String,System.String>
struct SortedList_2_t2974774816;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.SortedList`2/KeyEnumerator<System.String,System.String>
struct  KeyEnumerator_t2432372467 
{
public:
	// System.Collections.Generic.SortedList`2<TKey,TValue> System.Collections.Generic.SortedList`2/KeyEnumerator::l
	SortedList_2_t2974774816 * ___l_0;
	// System.Int32 System.Collections.Generic.SortedList`2/KeyEnumerator::idx
	int32_t ___idx_1;
	// System.Int32 System.Collections.Generic.SortedList`2/KeyEnumerator::ver
	int32_t ___ver_2;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(KeyEnumerator_t2432372467, ___l_0)); }
	inline SortedList_2_t2974774816 * get_l_0() const { return ___l_0; }
	inline SortedList_2_t2974774816 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(SortedList_2_t2974774816 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier(&___l_0, value);
	}

	inline static int32_t get_offset_of_idx_1() { return static_cast<int32_t>(offsetof(KeyEnumerator_t2432372467, ___idx_1)); }
	inline int32_t get_idx_1() const { return ___idx_1; }
	inline int32_t* get_address_of_idx_1() { return &___idx_1; }
	inline void set_idx_1(int32_t value)
	{
		___idx_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(KeyEnumerator_t2432372467, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
