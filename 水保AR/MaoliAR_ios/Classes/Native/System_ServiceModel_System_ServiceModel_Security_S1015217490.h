﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Security_T3373317487.h"

// System.ServiceModel.Security.SpnegoSecurityTokenProvider
struct SpnegoSecurityTokenProvider_t3056146291;
// System.ServiceModel.Description.WSTrustSecurityTokenServiceProxy
struct WSTrustSecurityTokenServiceProxy_t2160987012;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.SpnegoCommunicationObject
struct  SpnegoCommunicationObject_t1015217490  : public ProviderCommunicationObject_t3373317487
{
public:
	// System.ServiceModel.Security.SpnegoSecurityTokenProvider System.ServiceModel.Security.SpnegoCommunicationObject::owner
	SpnegoSecurityTokenProvider_t3056146291 * ___owner_15;
	// System.ServiceModel.Description.WSTrustSecurityTokenServiceProxy System.ServiceModel.Security.SpnegoCommunicationObject::proxy
	WSTrustSecurityTokenServiceProxy_t2160987012 * ___proxy_16;

public:
	inline static int32_t get_offset_of_owner_15() { return static_cast<int32_t>(offsetof(SpnegoCommunicationObject_t1015217490, ___owner_15)); }
	inline SpnegoSecurityTokenProvider_t3056146291 * get_owner_15() const { return ___owner_15; }
	inline SpnegoSecurityTokenProvider_t3056146291 ** get_address_of_owner_15() { return &___owner_15; }
	inline void set_owner_15(SpnegoSecurityTokenProvider_t3056146291 * value)
	{
		___owner_15 = value;
		Il2CppCodeGenWriteBarrier(&___owner_15, value);
	}

	inline static int32_t get_offset_of_proxy_16() { return static_cast<int32_t>(offsetof(SpnegoCommunicationObject_t1015217490, ___proxy_16)); }
	inline WSTrustSecurityTokenServiceProxy_t2160987012 * get_proxy_16() const { return ___proxy_16; }
	inline WSTrustSecurityTokenServiceProxy_t2160987012 ** get_address_of_proxy_16() { return &___proxy_16; }
	inline void set_proxy_16(WSTrustSecurityTokenServiceProxy_t2160987012 * value)
	{
		___proxy_16 = value;
		Il2CppCodeGenWriteBarrier(&___proxy_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
