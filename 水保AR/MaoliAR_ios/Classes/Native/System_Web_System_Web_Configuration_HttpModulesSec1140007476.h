﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3156163955.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.HttpModulesSection
struct  HttpModulesSection_t1140007476  : public ConfigurationSection_t3156163955
{
public:

public:
};

struct HttpModulesSection_t1140007476_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Web.Configuration.HttpModulesSection::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_17;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.HttpModulesSection::modulesProp
	ConfigurationProperty_t3590861854 * ___modulesProp_18;

public:
	inline static int32_t get_offset_of_properties_17() { return static_cast<int32_t>(offsetof(HttpModulesSection_t1140007476_StaticFields, ___properties_17)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_17() const { return ___properties_17; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_17() { return &___properties_17; }
	inline void set_properties_17(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_17 = value;
		Il2CppCodeGenWriteBarrier(&___properties_17, value);
	}

	inline static int32_t get_offset_of_modulesProp_18() { return static_cast<int32_t>(offsetof(HttpModulesSection_t1140007476_StaticFields, ___modulesProp_18)); }
	inline ConfigurationProperty_t3590861854 * get_modulesProp_18() const { return ___modulesProp_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_modulesProp_18() { return &___modulesProp_18; }
	inline void set_modulesProp_18(ConfigurationProperty_t3590861854 * value)
	{
		___modulesProp_18 = value;
		Il2CppCodeGenWriteBarrier(&___modulesProp_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
