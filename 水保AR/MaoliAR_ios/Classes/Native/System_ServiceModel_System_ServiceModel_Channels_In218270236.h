﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Threading.ManualResetEvent
struct ManualResetEvent_t451242010;
// System.ServiceModel.Channels.InternalChannelListenerBase`1<System.Object>
struct InternalChannelListenerBase_1_t1532512053;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.InternalChannelListenerBase`1/<OnBeginAcceptChannel>c__AnonStorey8<System.Object>
struct  U3COnBeginAcceptChannelU3Ec__AnonStorey8_t218270236  : public Il2CppObject
{
public:
	// System.Threading.ManualResetEvent System.ServiceModel.Channels.InternalChannelListenerBase`1/<OnBeginAcceptChannel>c__AnonStorey8::wait
	ManualResetEvent_t451242010 * ___wait_0;
	// System.ServiceModel.Channels.InternalChannelListenerBase`1<TChannel> System.ServiceModel.Channels.InternalChannelListenerBase`1/<OnBeginAcceptChannel>c__AnonStorey8::<>f__this
	InternalChannelListenerBase_1_t1532512053 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_wait_0() { return static_cast<int32_t>(offsetof(U3COnBeginAcceptChannelU3Ec__AnonStorey8_t218270236, ___wait_0)); }
	inline ManualResetEvent_t451242010 * get_wait_0() const { return ___wait_0; }
	inline ManualResetEvent_t451242010 ** get_address_of_wait_0() { return &___wait_0; }
	inline void set_wait_0(ManualResetEvent_t451242010 * value)
	{
		___wait_0 = value;
		Il2CppCodeGenWriteBarrier(&___wait_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3COnBeginAcceptChannelU3Ec__AnonStorey8_t218270236, ___U3CU3Ef__this_1)); }
	inline InternalChannelListenerBase_1_t1532512053 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline InternalChannelListenerBase_1_t1532512053 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(InternalChannelListenerBase_1_t1532512053 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
