﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TonemappingLut/SimplePolyFunc
struct  SimplePolyFunc_t3505133624 
{
public:
	// System.Single TonemappingLut/SimplePolyFunc::A
	float ___A_0;
	// System.Single TonemappingLut/SimplePolyFunc::B
	float ___B_1;
	// System.Single TonemappingLut/SimplePolyFunc::x0
	float ___x0_2;
	// System.Single TonemappingLut/SimplePolyFunc::y0
	float ___y0_3;
	// System.Single TonemappingLut/SimplePolyFunc::signX
	float ___signX_4;
	// System.Single TonemappingLut/SimplePolyFunc::signY
	float ___signY_5;
	// System.Single TonemappingLut/SimplePolyFunc::logA
	float ___logA_6;

public:
	inline static int32_t get_offset_of_A_0() { return static_cast<int32_t>(offsetof(SimplePolyFunc_t3505133624, ___A_0)); }
	inline float get_A_0() const { return ___A_0; }
	inline float* get_address_of_A_0() { return &___A_0; }
	inline void set_A_0(float value)
	{
		___A_0 = value;
	}

	inline static int32_t get_offset_of_B_1() { return static_cast<int32_t>(offsetof(SimplePolyFunc_t3505133624, ___B_1)); }
	inline float get_B_1() const { return ___B_1; }
	inline float* get_address_of_B_1() { return &___B_1; }
	inline void set_B_1(float value)
	{
		___B_1 = value;
	}

	inline static int32_t get_offset_of_x0_2() { return static_cast<int32_t>(offsetof(SimplePolyFunc_t3505133624, ___x0_2)); }
	inline float get_x0_2() const { return ___x0_2; }
	inline float* get_address_of_x0_2() { return &___x0_2; }
	inline void set_x0_2(float value)
	{
		___x0_2 = value;
	}

	inline static int32_t get_offset_of_y0_3() { return static_cast<int32_t>(offsetof(SimplePolyFunc_t3505133624, ___y0_3)); }
	inline float get_y0_3() const { return ___y0_3; }
	inline float* get_address_of_y0_3() { return &___y0_3; }
	inline void set_y0_3(float value)
	{
		___y0_3 = value;
	}

	inline static int32_t get_offset_of_signX_4() { return static_cast<int32_t>(offsetof(SimplePolyFunc_t3505133624, ___signX_4)); }
	inline float get_signX_4() const { return ___signX_4; }
	inline float* get_address_of_signX_4() { return &___signX_4; }
	inline void set_signX_4(float value)
	{
		___signX_4 = value;
	}

	inline static int32_t get_offset_of_signY_5() { return static_cast<int32_t>(offsetof(SimplePolyFunc_t3505133624, ___signY_5)); }
	inline float get_signY_5() const { return ___signY_5; }
	inline float* get_address_of_signY_5() { return &___signY_5; }
	inline void set_signY_5(float value)
	{
		___signY_5 = value;
	}

	inline static int32_t get_offset_of_logA_6() { return static_cast<int32_t>(offsetof(SimplePolyFunc_t3505133624, ___logA_6)); }
	inline float get_logA_6() const { return ___logA_6; }
	inline float* get_address_of_logA_6() { return &___logA_6; }
	inline void set_logA_6(float value)
	{
		___logA_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
