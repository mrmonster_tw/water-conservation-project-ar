﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Security.Cryptography.Xml.TransformChain
struct TransformChain_t1669092815;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Xml.XmlElement
struct XmlElement_t561603118;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.Xml.Reference
struct  Reference_t1944003019  : public Il2CppObject
{
public:
	// System.Security.Cryptography.Xml.TransformChain System.Security.Cryptography.Xml.Reference::chain
	TransformChain_t1669092815 * ___chain_0;
	// System.String System.Security.Cryptography.Xml.Reference::digestMethod
	String_t* ___digestMethod_1;
	// System.Byte[] System.Security.Cryptography.Xml.Reference::digestValue
	ByteU5BU5D_t4116647657* ___digestValue_2;
	// System.String System.Security.Cryptography.Xml.Reference::id
	String_t* ___id_3;
	// System.String System.Security.Cryptography.Xml.Reference::uri
	String_t* ___uri_4;
	// System.String System.Security.Cryptography.Xml.Reference::type
	String_t* ___type_5;
	// System.Xml.XmlElement System.Security.Cryptography.Xml.Reference::element
	XmlElement_t561603118 * ___element_6;

public:
	inline static int32_t get_offset_of_chain_0() { return static_cast<int32_t>(offsetof(Reference_t1944003019, ___chain_0)); }
	inline TransformChain_t1669092815 * get_chain_0() const { return ___chain_0; }
	inline TransformChain_t1669092815 ** get_address_of_chain_0() { return &___chain_0; }
	inline void set_chain_0(TransformChain_t1669092815 * value)
	{
		___chain_0 = value;
		Il2CppCodeGenWriteBarrier(&___chain_0, value);
	}

	inline static int32_t get_offset_of_digestMethod_1() { return static_cast<int32_t>(offsetof(Reference_t1944003019, ___digestMethod_1)); }
	inline String_t* get_digestMethod_1() const { return ___digestMethod_1; }
	inline String_t** get_address_of_digestMethod_1() { return &___digestMethod_1; }
	inline void set_digestMethod_1(String_t* value)
	{
		___digestMethod_1 = value;
		Il2CppCodeGenWriteBarrier(&___digestMethod_1, value);
	}

	inline static int32_t get_offset_of_digestValue_2() { return static_cast<int32_t>(offsetof(Reference_t1944003019, ___digestValue_2)); }
	inline ByteU5BU5D_t4116647657* get_digestValue_2() const { return ___digestValue_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_digestValue_2() { return &___digestValue_2; }
	inline void set_digestValue_2(ByteU5BU5D_t4116647657* value)
	{
		___digestValue_2 = value;
		Il2CppCodeGenWriteBarrier(&___digestValue_2, value);
	}

	inline static int32_t get_offset_of_id_3() { return static_cast<int32_t>(offsetof(Reference_t1944003019, ___id_3)); }
	inline String_t* get_id_3() const { return ___id_3; }
	inline String_t** get_address_of_id_3() { return &___id_3; }
	inline void set_id_3(String_t* value)
	{
		___id_3 = value;
		Il2CppCodeGenWriteBarrier(&___id_3, value);
	}

	inline static int32_t get_offset_of_uri_4() { return static_cast<int32_t>(offsetof(Reference_t1944003019, ___uri_4)); }
	inline String_t* get_uri_4() const { return ___uri_4; }
	inline String_t** get_address_of_uri_4() { return &___uri_4; }
	inline void set_uri_4(String_t* value)
	{
		___uri_4 = value;
		Il2CppCodeGenWriteBarrier(&___uri_4, value);
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(Reference_t1944003019, ___type_5)); }
	inline String_t* get_type_5() const { return ___type_5; }
	inline String_t** get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(String_t* value)
	{
		___type_5 = value;
		Il2CppCodeGenWriteBarrier(&___type_5, value);
	}

	inline static int32_t get_offset_of_element_6() { return static_cast<int32_t>(offsetof(Reference_t1944003019, ___element_6)); }
	inline XmlElement_t561603118 * get_element_6() const { return ___element_6; }
	inline XmlElement_t561603118 ** get_address_of_element_6() { return &___element_6; }
	inline void set_element_6(XmlElement_t561603118 * value)
	{
		___element_6 = value;
		Il2CppCodeGenWriteBarrier(&___element_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
