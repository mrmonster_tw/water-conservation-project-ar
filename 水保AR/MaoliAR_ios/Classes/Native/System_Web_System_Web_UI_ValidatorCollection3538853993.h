﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.ArrayList
struct ArrayList_t2718874744;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.ValidatorCollection
struct  ValidatorCollection_t3538853993  : public Il2CppObject
{
public:
	// System.Collections.ArrayList System.Web.UI.ValidatorCollection::_validators
	ArrayList_t2718874744 * ____validators_0;

public:
	inline static int32_t get_offset_of__validators_0() { return static_cast<int32_t>(offsetof(ValidatorCollection_t3538853993, ____validators_0)); }
	inline ArrayList_t2718874744 * get__validators_0() const { return ____validators_0; }
	inline ArrayList_t2718874744 ** get_address_of__validators_0() { return &____validators_0; }
	inline void set__validators_0(ArrayList_t2718874744 * value)
	{
		____validators_0 = value;
		Il2CppCodeGenWriteBarrier(&____validators_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
