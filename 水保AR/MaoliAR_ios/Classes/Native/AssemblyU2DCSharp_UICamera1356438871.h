﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "AssemblyU2DCSharp_UICamera_EventType561105572.h"
#include "UnityEngine_UnityEngine_LayerMask3493934918.h"
#include "UnityEngine_UnityEngine_KeyCode2599294277.h"
#include "UnityEngine_UnityEngine_Vector22156229523.h"
#include "UnityEngine_UnityEngine_Vector33722313464.h"
#include "UnityEngine_UnityEngine_RaycastHit1056001966.h"
#include "AssemblyU2DCSharp_UICamera_DepthEntry628749918.h"
#include "UnityEngine_UnityEngine_Plane1000493321.h"

// BetterList`1<UICamera>
struct BetterList_1_t511459189;
// UICamera/GetKeyStateFunc
struct GetKeyStateFunc_t2810275146;
// UICamera/GetAxisFunc
struct GetAxisFunc_t2592608932;
// UICamera/GetAnyKeyFunc
struct GetAnyKeyFunc_t1761480072;
// UICamera/OnScreenResize
struct OnScreenResize_t2279991692;
// System.String
struct String_t;
// UICamera/OnCustomInput
struct OnCustomInput_t3508588789;
// UICamera
struct UICamera_t1356438871;
// UnityEngine.Camera
struct Camera_t4157153871;
// UICamera/OnSchemeChange
struct OnSchemeChange_t1701155603;
// UICamera/MouseOrTouch
struct MouseOrTouch_t3052596533;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UICamera/VoidDelegate
struct VoidDelegate_t3100799918;
// UICamera/BoolDelegate
struct BoolDelegate_t3825226153;
// UICamera/FloatDelegate
struct FloatDelegate_t906524069;
// UICamera/VectorDelegate
struct VectorDelegate_t435795517;
// UICamera/ObjectDelegate
struct ObjectDelegate_t2041570719;
// UICamera/KeyCodeDelegate
struct KeyCodeDelegate_t3064672302;
// UICamera/MoveDelegate
struct MoveDelegate_t16019400;
// UICamera/MouseOrTouch[]
struct MouseOrTouchU5BU5D_t3534648920;
// System.Collections.Generic.List`1<UICamera/MouseOrTouch>
struct List_1_t229703979;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// BetterList`1<UICamera/DepthEntry>
struct BetterList_1_t4078737532;
// UICamera/GetTouchCountCallback
struct GetTouchCountCallback_t3185863032;
// UICamera/GetTouchCallback
struct GetTouchCallback_t97678626;
// BetterList`1/CompareFunc<UICamera/DepthEntry>
struct CompareFunc_t2967399990;
// BetterList`1/CompareFunc<UICamera>
struct CompareFunc_t3695088943;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera
struct  UICamera_t1356438871  : public MonoBehaviour_t3962482529
{
public:
	// UICamera/EventType UICamera::eventType
	int32_t ___eventType_9;
	// System.Boolean UICamera::eventsGoToColliders
	bool ___eventsGoToColliders_10;
	// UnityEngine.LayerMask UICamera::eventReceiverMask
	LayerMask_t3493934918  ___eventReceiverMask_11;
	// System.Boolean UICamera::debug
	bool ___debug_12;
	// System.Boolean UICamera::useMouse
	bool ___useMouse_13;
	// System.Boolean UICamera::useTouch
	bool ___useTouch_14;
	// System.Boolean UICamera::allowMultiTouch
	bool ___allowMultiTouch_15;
	// System.Boolean UICamera::useKeyboard
	bool ___useKeyboard_16;
	// System.Boolean UICamera::useController
	bool ___useController_17;
	// System.Boolean UICamera::stickyTooltip
	bool ___stickyTooltip_18;
	// System.Single UICamera::tooltipDelay
	float ___tooltipDelay_19;
	// System.Boolean UICamera::longPressTooltip
	bool ___longPressTooltip_20;
	// System.Single UICamera::mouseDragThreshold
	float ___mouseDragThreshold_21;
	// System.Single UICamera::mouseClickThreshold
	float ___mouseClickThreshold_22;
	// System.Single UICamera::touchDragThreshold
	float ___touchDragThreshold_23;
	// System.Single UICamera::touchClickThreshold
	float ___touchClickThreshold_24;
	// System.Single UICamera::rangeDistance
	float ___rangeDistance_25;
	// System.String UICamera::horizontalAxisName
	String_t* ___horizontalAxisName_26;
	// System.String UICamera::verticalAxisName
	String_t* ___verticalAxisName_27;
	// System.String UICamera::horizontalPanAxisName
	String_t* ___horizontalPanAxisName_28;
	// System.String UICamera::verticalPanAxisName
	String_t* ___verticalPanAxisName_29;
	// System.String UICamera::scrollAxisName
	String_t* ___scrollAxisName_30;
	// System.Boolean UICamera::commandClick
	bool ___commandClick_31;
	// UnityEngine.KeyCode UICamera::submitKey0
	int32_t ___submitKey0_32;
	// UnityEngine.KeyCode UICamera::submitKey1
	int32_t ___submitKey1_33;
	// UnityEngine.KeyCode UICamera::cancelKey0
	int32_t ___cancelKey0_34;
	// UnityEngine.KeyCode UICamera::cancelKey1
	int32_t ___cancelKey1_35;
	// UnityEngine.Camera UICamera::mCam
	Camera_t4157153871 * ___mCam_75;
	// System.Single UICamera::mNextRaycast
	float ___mNextRaycast_77;

public:
	inline static int32_t get_offset_of_eventType_9() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___eventType_9)); }
	inline int32_t get_eventType_9() const { return ___eventType_9; }
	inline int32_t* get_address_of_eventType_9() { return &___eventType_9; }
	inline void set_eventType_9(int32_t value)
	{
		___eventType_9 = value;
	}

	inline static int32_t get_offset_of_eventsGoToColliders_10() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___eventsGoToColliders_10)); }
	inline bool get_eventsGoToColliders_10() const { return ___eventsGoToColliders_10; }
	inline bool* get_address_of_eventsGoToColliders_10() { return &___eventsGoToColliders_10; }
	inline void set_eventsGoToColliders_10(bool value)
	{
		___eventsGoToColliders_10 = value;
	}

	inline static int32_t get_offset_of_eventReceiverMask_11() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___eventReceiverMask_11)); }
	inline LayerMask_t3493934918  get_eventReceiverMask_11() const { return ___eventReceiverMask_11; }
	inline LayerMask_t3493934918 * get_address_of_eventReceiverMask_11() { return &___eventReceiverMask_11; }
	inline void set_eventReceiverMask_11(LayerMask_t3493934918  value)
	{
		___eventReceiverMask_11 = value;
	}

	inline static int32_t get_offset_of_debug_12() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___debug_12)); }
	inline bool get_debug_12() const { return ___debug_12; }
	inline bool* get_address_of_debug_12() { return &___debug_12; }
	inline void set_debug_12(bool value)
	{
		___debug_12 = value;
	}

	inline static int32_t get_offset_of_useMouse_13() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___useMouse_13)); }
	inline bool get_useMouse_13() const { return ___useMouse_13; }
	inline bool* get_address_of_useMouse_13() { return &___useMouse_13; }
	inline void set_useMouse_13(bool value)
	{
		___useMouse_13 = value;
	}

	inline static int32_t get_offset_of_useTouch_14() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___useTouch_14)); }
	inline bool get_useTouch_14() const { return ___useTouch_14; }
	inline bool* get_address_of_useTouch_14() { return &___useTouch_14; }
	inline void set_useTouch_14(bool value)
	{
		___useTouch_14 = value;
	}

	inline static int32_t get_offset_of_allowMultiTouch_15() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___allowMultiTouch_15)); }
	inline bool get_allowMultiTouch_15() const { return ___allowMultiTouch_15; }
	inline bool* get_address_of_allowMultiTouch_15() { return &___allowMultiTouch_15; }
	inline void set_allowMultiTouch_15(bool value)
	{
		___allowMultiTouch_15 = value;
	}

	inline static int32_t get_offset_of_useKeyboard_16() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___useKeyboard_16)); }
	inline bool get_useKeyboard_16() const { return ___useKeyboard_16; }
	inline bool* get_address_of_useKeyboard_16() { return &___useKeyboard_16; }
	inline void set_useKeyboard_16(bool value)
	{
		___useKeyboard_16 = value;
	}

	inline static int32_t get_offset_of_useController_17() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___useController_17)); }
	inline bool get_useController_17() const { return ___useController_17; }
	inline bool* get_address_of_useController_17() { return &___useController_17; }
	inline void set_useController_17(bool value)
	{
		___useController_17 = value;
	}

	inline static int32_t get_offset_of_stickyTooltip_18() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___stickyTooltip_18)); }
	inline bool get_stickyTooltip_18() const { return ___stickyTooltip_18; }
	inline bool* get_address_of_stickyTooltip_18() { return &___stickyTooltip_18; }
	inline void set_stickyTooltip_18(bool value)
	{
		___stickyTooltip_18 = value;
	}

	inline static int32_t get_offset_of_tooltipDelay_19() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___tooltipDelay_19)); }
	inline float get_tooltipDelay_19() const { return ___tooltipDelay_19; }
	inline float* get_address_of_tooltipDelay_19() { return &___tooltipDelay_19; }
	inline void set_tooltipDelay_19(float value)
	{
		___tooltipDelay_19 = value;
	}

	inline static int32_t get_offset_of_longPressTooltip_20() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___longPressTooltip_20)); }
	inline bool get_longPressTooltip_20() const { return ___longPressTooltip_20; }
	inline bool* get_address_of_longPressTooltip_20() { return &___longPressTooltip_20; }
	inline void set_longPressTooltip_20(bool value)
	{
		___longPressTooltip_20 = value;
	}

	inline static int32_t get_offset_of_mouseDragThreshold_21() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___mouseDragThreshold_21)); }
	inline float get_mouseDragThreshold_21() const { return ___mouseDragThreshold_21; }
	inline float* get_address_of_mouseDragThreshold_21() { return &___mouseDragThreshold_21; }
	inline void set_mouseDragThreshold_21(float value)
	{
		___mouseDragThreshold_21 = value;
	}

	inline static int32_t get_offset_of_mouseClickThreshold_22() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___mouseClickThreshold_22)); }
	inline float get_mouseClickThreshold_22() const { return ___mouseClickThreshold_22; }
	inline float* get_address_of_mouseClickThreshold_22() { return &___mouseClickThreshold_22; }
	inline void set_mouseClickThreshold_22(float value)
	{
		___mouseClickThreshold_22 = value;
	}

	inline static int32_t get_offset_of_touchDragThreshold_23() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___touchDragThreshold_23)); }
	inline float get_touchDragThreshold_23() const { return ___touchDragThreshold_23; }
	inline float* get_address_of_touchDragThreshold_23() { return &___touchDragThreshold_23; }
	inline void set_touchDragThreshold_23(float value)
	{
		___touchDragThreshold_23 = value;
	}

	inline static int32_t get_offset_of_touchClickThreshold_24() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___touchClickThreshold_24)); }
	inline float get_touchClickThreshold_24() const { return ___touchClickThreshold_24; }
	inline float* get_address_of_touchClickThreshold_24() { return &___touchClickThreshold_24; }
	inline void set_touchClickThreshold_24(float value)
	{
		___touchClickThreshold_24 = value;
	}

	inline static int32_t get_offset_of_rangeDistance_25() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___rangeDistance_25)); }
	inline float get_rangeDistance_25() const { return ___rangeDistance_25; }
	inline float* get_address_of_rangeDistance_25() { return &___rangeDistance_25; }
	inline void set_rangeDistance_25(float value)
	{
		___rangeDistance_25 = value;
	}

	inline static int32_t get_offset_of_horizontalAxisName_26() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___horizontalAxisName_26)); }
	inline String_t* get_horizontalAxisName_26() const { return ___horizontalAxisName_26; }
	inline String_t** get_address_of_horizontalAxisName_26() { return &___horizontalAxisName_26; }
	inline void set_horizontalAxisName_26(String_t* value)
	{
		___horizontalAxisName_26 = value;
		Il2CppCodeGenWriteBarrier(&___horizontalAxisName_26, value);
	}

	inline static int32_t get_offset_of_verticalAxisName_27() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___verticalAxisName_27)); }
	inline String_t* get_verticalAxisName_27() const { return ___verticalAxisName_27; }
	inline String_t** get_address_of_verticalAxisName_27() { return &___verticalAxisName_27; }
	inline void set_verticalAxisName_27(String_t* value)
	{
		___verticalAxisName_27 = value;
		Il2CppCodeGenWriteBarrier(&___verticalAxisName_27, value);
	}

	inline static int32_t get_offset_of_horizontalPanAxisName_28() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___horizontalPanAxisName_28)); }
	inline String_t* get_horizontalPanAxisName_28() const { return ___horizontalPanAxisName_28; }
	inline String_t** get_address_of_horizontalPanAxisName_28() { return &___horizontalPanAxisName_28; }
	inline void set_horizontalPanAxisName_28(String_t* value)
	{
		___horizontalPanAxisName_28 = value;
		Il2CppCodeGenWriteBarrier(&___horizontalPanAxisName_28, value);
	}

	inline static int32_t get_offset_of_verticalPanAxisName_29() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___verticalPanAxisName_29)); }
	inline String_t* get_verticalPanAxisName_29() const { return ___verticalPanAxisName_29; }
	inline String_t** get_address_of_verticalPanAxisName_29() { return &___verticalPanAxisName_29; }
	inline void set_verticalPanAxisName_29(String_t* value)
	{
		___verticalPanAxisName_29 = value;
		Il2CppCodeGenWriteBarrier(&___verticalPanAxisName_29, value);
	}

	inline static int32_t get_offset_of_scrollAxisName_30() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___scrollAxisName_30)); }
	inline String_t* get_scrollAxisName_30() const { return ___scrollAxisName_30; }
	inline String_t** get_address_of_scrollAxisName_30() { return &___scrollAxisName_30; }
	inline void set_scrollAxisName_30(String_t* value)
	{
		___scrollAxisName_30 = value;
		Il2CppCodeGenWriteBarrier(&___scrollAxisName_30, value);
	}

	inline static int32_t get_offset_of_commandClick_31() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___commandClick_31)); }
	inline bool get_commandClick_31() const { return ___commandClick_31; }
	inline bool* get_address_of_commandClick_31() { return &___commandClick_31; }
	inline void set_commandClick_31(bool value)
	{
		___commandClick_31 = value;
	}

	inline static int32_t get_offset_of_submitKey0_32() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___submitKey0_32)); }
	inline int32_t get_submitKey0_32() const { return ___submitKey0_32; }
	inline int32_t* get_address_of_submitKey0_32() { return &___submitKey0_32; }
	inline void set_submitKey0_32(int32_t value)
	{
		___submitKey0_32 = value;
	}

	inline static int32_t get_offset_of_submitKey1_33() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___submitKey1_33)); }
	inline int32_t get_submitKey1_33() const { return ___submitKey1_33; }
	inline int32_t* get_address_of_submitKey1_33() { return &___submitKey1_33; }
	inline void set_submitKey1_33(int32_t value)
	{
		___submitKey1_33 = value;
	}

	inline static int32_t get_offset_of_cancelKey0_34() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___cancelKey0_34)); }
	inline int32_t get_cancelKey0_34() const { return ___cancelKey0_34; }
	inline int32_t* get_address_of_cancelKey0_34() { return &___cancelKey0_34; }
	inline void set_cancelKey0_34(int32_t value)
	{
		___cancelKey0_34 = value;
	}

	inline static int32_t get_offset_of_cancelKey1_35() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___cancelKey1_35)); }
	inline int32_t get_cancelKey1_35() const { return ___cancelKey1_35; }
	inline int32_t* get_address_of_cancelKey1_35() { return &___cancelKey1_35; }
	inline void set_cancelKey1_35(int32_t value)
	{
		___cancelKey1_35 = value;
	}

	inline static int32_t get_offset_of_mCam_75() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___mCam_75)); }
	inline Camera_t4157153871 * get_mCam_75() const { return ___mCam_75; }
	inline Camera_t4157153871 ** get_address_of_mCam_75() { return &___mCam_75; }
	inline void set_mCam_75(Camera_t4157153871 * value)
	{
		___mCam_75 = value;
		Il2CppCodeGenWriteBarrier(&___mCam_75, value);
	}

	inline static int32_t get_offset_of_mNextRaycast_77() { return static_cast<int32_t>(offsetof(UICamera_t1356438871, ___mNextRaycast_77)); }
	inline float get_mNextRaycast_77() const { return ___mNextRaycast_77; }
	inline float* get_address_of_mNextRaycast_77() { return &___mNextRaycast_77; }
	inline void set_mNextRaycast_77(float value)
	{
		___mNextRaycast_77 = value;
	}
};

struct UICamera_t1356438871_StaticFields
{
public:
	// BetterList`1<UICamera> UICamera::list
	BetterList_1_t511459189 * ___list_2;
	// UICamera/GetKeyStateFunc UICamera::GetKeyDown
	GetKeyStateFunc_t2810275146 * ___GetKeyDown_3;
	// UICamera/GetKeyStateFunc UICamera::GetKeyUp
	GetKeyStateFunc_t2810275146 * ___GetKeyUp_4;
	// UICamera/GetKeyStateFunc UICamera::GetKey
	GetKeyStateFunc_t2810275146 * ___GetKey_5;
	// UICamera/GetAxisFunc UICamera::GetAxis
	GetAxisFunc_t2592608932 * ___GetAxis_6;
	// UICamera/GetAnyKeyFunc UICamera::GetAnyKeyDown
	GetAnyKeyFunc_t1761480072 * ___GetAnyKeyDown_7;
	// UICamera/OnScreenResize UICamera::onScreenResize
	OnScreenResize_t2279991692 * ___onScreenResize_8;
	// UICamera/OnCustomInput UICamera::onCustomInput
	OnCustomInput_t3508588789 * ___onCustomInput_36;
	// System.Boolean UICamera::showTooltips
	bool ___showTooltips_37;
	// System.Boolean UICamera::mDisableController
	bool ___mDisableController_38;
	// UnityEngine.Vector2 UICamera::mLastPos
	Vector2_t2156229523  ___mLastPos_39;
	// UnityEngine.Vector3 UICamera::lastWorldPosition
	Vector3_t3722313464  ___lastWorldPosition_40;
	// UnityEngine.RaycastHit UICamera::lastHit
	RaycastHit_t1056001966  ___lastHit_41;
	// UICamera UICamera::current
	UICamera_t1356438871 * ___current_42;
	// UnityEngine.Camera UICamera::currentCamera
	Camera_t4157153871 * ___currentCamera_43;
	// UICamera/OnSchemeChange UICamera::onSchemeChange
	OnSchemeChange_t1701155603 * ___onSchemeChange_44;
	// System.Int32 UICamera::currentTouchID
	int32_t ___currentTouchID_45;
	// UnityEngine.KeyCode UICamera::mCurrentKey
	int32_t ___mCurrentKey_46;
	// UICamera/MouseOrTouch UICamera::currentTouch
	MouseOrTouch_t3052596533 * ___currentTouch_47;
	// System.Boolean UICamera::mInputFocus
	bool ___mInputFocus_48;
	// UnityEngine.GameObject UICamera::mGenericHandler
	GameObject_t1113636619 * ___mGenericHandler_49;
	// UnityEngine.GameObject UICamera::fallThrough
	GameObject_t1113636619 * ___fallThrough_50;
	// UICamera/VoidDelegate UICamera::onClick
	VoidDelegate_t3100799918 * ___onClick_51;
	// UICamera/VoidDelegate UICamera::onDoubleClick
	VoidDelegate_t3100799918 * ___onDoubleClick_52;
	// UICamera/BoolDelegate UICamera::onHover
	BoolDelegate_t3825226153 * ___onHover_53;
	// UICamera/BoolDelegate UICamera::onPress
	BoolDelegate_t3825226153 * ___onPress_54;
	// UICamera/BoolDelegate UICamera::onSelect
	BoolDelegate_t3825226153 * ___onSelect_55;
	// UICamera/FloatDelegate UICamera::onScroll
	FloatDelegate_t906524069 * ___onScroll_56;
	// UICamera/VectorDelegate UICamera::onDrag
	VectorDelegate_t435795517 * ___onDrag_57;
	// UICamera/VoidDelegate UICamera::onDragStart
	VoidDelegate_t3100799918 * ___onDragStart_58;
	// UICamera/ObjectDelegate UICamera::onDragOver
	ObjectDelegate_t2041570719 * ___onDragOver_59;
	// UICamera/ObjectDelegate UICamera::onDragOut
	ObjectDelegate_t2041570719 * ___onDragOut_60;
	// UICamera/VoidDelegate UICamera::onDragEnd
	VoidDelegate_t3100799918 * ___onDragEnd_61;
	// UICamera/ObjectDelegate UICamera::onDrop
	ObjectDelegate_t2041570719 * ___onDrop_62;
	// UICamera/KeyCodeDelegate UICamera::onKey
	KeyCodeDelegate_t3064672302 * ___onKey_63;
	// UICamera/KeyCodeDelegate UICamera::onNavigate
	KeyCodeDelegate_t3064672302 * ___onNavigate_64;
	// UICamera/VectorDelegate UICamera::onPan
	VectorDelegate_t435795517 * ___onPan_65;
	// UICamera/BoolDelegate UICamera::onTooltip
	BoolDelegate_t3825226153 * ___onTooltip_66;
	// UICamera/MoveDelegate UICamera::onMouseMove
	MoveDelegate_t16019400 * ___onMouseMove_67;
	// UICamera/MouseOrTouch[] UICamera::mMouse
	MouseOrTouchU5BU5D_t3534648920* ___mMouse_68;
	// UICamera/MouseOrTouch UICamera::controller
	MouseOrTouch_t3052596533 * ___controller_69;
	// System.Collections.Generic.List`1<UICamera/MouseOrTouch> UICamera::activeTouches
	List_1_t229703979 * ___activeTouches_70;
	// System.Collections.Generic.List`1<System.Int32> UICamera::mTouchIDs
	List_1_t128053199 * ___mTouchIDs_71;
	// System.Int32 UICamera::mWidth
	int32_t ___mWidth_72;
	// System.Int32 UICamera::mHeight
	int32_t ___mHeight_73;
	// UnityEngine.GameObject UICamera::mTooltip
	GameObject_t1113636619 * ___mTooltip_74;
	// System.Single UICamera::mTooltipTime
	float ___mTooltipTime_76;
	// System.Boolean UICamera::isDragging
	bool ___isDragging_78;
	// UnityEngine.GameObject UICamera::mRayHitObject
	GameObject_t1113636619 * ___mRayHitObject_79;
	// UnityEngine.GameObject UICamera::mHover
	GameObject_t1113636619 * ___mHover_80;
	// UnityEngine.GameObject UICamera::mSelected
	GameObject_t1113636619 * ___mSelected_81;
	// UICamera/DepthEntry UICamera::mHit
	DepthEntry_t628749918  ___mHit_82;
	// BetterList`1<UICamera/DepthEntry> UICamera::mHits
	BetterList_1_t4078737532 * ___mHits_83;
	// UnityEngine.Plane UICamera::m2DPlane
	Plane_t1000493321  ___m2DPlane_84;
	// System.Single UICamera::mNextEvent
	float ___mNextEvent_85;
	// System.Int32 UICamera::mNotifying
	int32_t ___mNotifying_86;
	// System.Boolean UICamera::mUsingTouchEvents
	bool ___mUsingTouchEvents_87;
	// UICamera/GetTouchCountCallback UICamera::GetInputTouchCount
	GetTouchCountCallback_t3185863032 * ___GetInputTouchCount_88;
	// UICamera/GetTouchCallback UICamera::GetInputTouch
	GetTouchCallback_t97678626 * ___GetInputTouch_89;
	// BetterList`1/CompareFunc<UICamera/DepthEntry> UICamera::<>f__am$cache0
	CompareFunc_t2967399990 * ___U3CU3Ef__amU24cache0_90;
	// BetterList`1/CompareFunc<UICamera/DepthEntry> UICamera::<>f__am$cache1
	CompareFunc_t2967399990 * ___U3CU3Ef__amU24cache1_91;
	// BetterList`1/CompareFunc<UICamera> UICamera::<>f__mg$cache0
	CompareFunc_t3695088943 * ___U3CU3Ef__mgU24cache0_92;
	// UICamera/GetKeyStateFunc UICamera::<>f__mg$cache1
	GetKeyStateFunc_t2810275146 * ___U3CU3Ef__mgU24cache1_93;
	// UICamera/GetKeyStateFunc UICamera::<>f__mg$cache2
	GetKeyStateFunc_t2810275146 * ___U3CU3Ef__mgU24cache2_94;
	// UICamera/GetKeyStateFunc UICamera::<>f__mg$cache3
	GetKeyStateFunc_t2810275146 * ___U3CU3Ef__mgU24cache3_95;
	// UICamera/GetAxisFunc UICamera::<>f__mg$cache4
	GetAxisFunc_t2592608932 * ___U3CU3Ef__mgU24cache4_96;

public:
	inline static int32_t get_offset_of_list_2() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___list_2)); }
	inline BetterList_1_t511459189 * get_list_2() const { return ___list_2; }
	inline BetterList_1_t511459189 ** get_address_of_list_2() { return &___list_2; }
	inline void set_list_2(BetterList_1_t511459189 * value)
	{
		___list_2 = value;
		Il2CppCodeGenWriteBarrier(&___list_2, value);
	}

	inline static int32_t get_offset_of_GetKeyDown_3() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___GetKeyDown_3)); }
	inline GetKeyStateFunc_t2810275146 * get_GetKeyDown_3() const { return ___GetKeyDown_3; }
	inline GetKeyStateFunc_t2810275146 ** get_address_of_GetKeyDown_3() { return &___GetKeyDown_3; }
	inline void set_GetKeyDown_3(GetKeyStateFunc_t2810275146 * value)
	{
		___GetKeyDown_3 = value;
		Il2CppCodeGenWriteBarrier(&___GetKeyDown_3, value);
	}

	inline static int32_t get_offset_of_GetKeyUp_4() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___GetKeyUp_4)); }
	inline GetKeyStateFunc_t2810275146 * get_GetKeyUp_4() const { return ___GetKeyUp_4; }
	inline GetKeyStateFunc_t2810275146 ** get_address_of_GetKeyUp_4() { return &___GetKeyUp_4; }
	inline void set_GetKeyUp_4(GetKeyStateFunc_t2810275146 * value)
	{
		___GetKeyUp_4 = value;
		Il2CppCodeGenWriteBarrier(&___GetKeyUp_4, value);
	}

	inline static int32_t get_offset_of_GetKey_5() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___GetKey_5)); }
	inline GetKeyStateFunc_t2810275146 * get_GetKey_5() const { return ___GetKey_5; }
	inline GetKeyStateFunc_t2810275146 ** get_address_of_GetKey_5() { return &___GetKey_5; }
	inline void set_GetKey_5(GetKeyStateFunc_t2810275146 * value)
	{
		___GetKey_5 = value;
		Il2CppCodeGenWriteBarrier(&___GetKey_5, value);
	}

	inline static int32_t get_offset_of_GetAxis_6() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___GetAxis_6)); }
	inline GetAxisFunc_t2592608932 * get_GetAxis_6() const { return ___GetAxis_6; }
	inline GetAxisFunc_t2592608932 ** get_address_of_GetAxis_6() { return &___GetAxis_6; }
	inline void set_GetAxis_6(GetAxisFunc_t2592608932 * value)
	{
		___GetAxis_6 = value;
		Il2CppCodeGenWriteBarrier(&___GetAxis_6, value);
	}

	inline static int32_t get_offset_of_GetAnyKeyDown_7() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___GetAnyKeyDown_7)); }
	inline GetAnyKeyFunc_t1761480072 * get_GetAnyKeyDown_7() const { return ___GetAnyKeyDown_7; }
	inline GetAnyKeyFunc_t1761480072 ** get_address_of_GetAnyKeyDown_7() { return &___GetAnyKeyDown_7; }
	inline void set_GetAnyKeyDown_7(GetAnyKeyFunc_t1761480072 * value)
	{
		___GetAnyKeyDown_7 = value;
		Il2CppCodeGenWriteBarrier(&___GetAnyKeyDown_7, value);
	}

	inline static int32_t get_offset_of_onScreenResize_8() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onScreenResize_8)); }
	inline OnScreenResize_t2279991692 * get_onScreenResize_8() const { return ___onScreenResize_8; }
	inline OnScreenResize_t2279991692 ** get_address_of_onScreenResize_8() { return &___onScreenResize_8; }
	inline void set_onScreenResize_8(OnScreenResize_t2279991692 * value)
	{
		___onScreenResize_8 = value;
		Il2CppCodeGenWriteBarrier(&___onScreenResize_8, value);
	}

	inline static int32_t get_offset_of_onCustomInput_36() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onCustomInput_36)); }
	inline OnCustomInput_t3508588789 * get_onCustomInput_36() const { return ___onCustomInput_36; }
	inline OnCustomInput_t3508588789 ** get_address_of_onCustomInput_36() { return &___onCustomInput_36; }
	inline void set_onCustomInput_36(OnCustomInput_t3508588789 * value)
	{
		___onCustomInput_36 = value;
		Il2CppCodeGenWriteBarrier(&___onCustomInput_36, value);
	}

	inline static int32_t get_offset_of_showTooltips_37() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___showTooltips_37)); }
	inline bool get_showTooltips_37() const { return ___showTooltips_37; }
	inline bool* get_address_of_showTooltips_37() { return &___showTooltips_37; }
	inline void set_showTooltips_37(bool value)
	{
		___showTooltips_37 = value;
	}

	inline static int32_t get_offset_of_mDisableController_38() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mDisableController_38)); }
	inline bool get_mDisableController_38() const { return ___mDisableController_38; }
	inline bool* get_address_of_mDisableController_38() { return &___mDisableController_38; }
	inline void set_mDisableController_38(bool value)
	{
		___mDisableController_38 = value;
	}

	inline static int32_t get_offset_of_mLastPos_39() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mLastPos_39)); }
	inline Vector2_t2156229523  get_mLastPos_39() const { return ___mLastPos_39; }
	inline Vector2_t2156229523 * get_address_of_mLastPos_39() { return &___mLastPos_39; }
	inline void set_mLastPos_39(Vector2_t2156229523  value)
	{
		___mLastPos_39 = value;
	}

	inline static int32_t get_offset_of_lastWorldPosition_40() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___lastWorldPosition_40)); }
	inline Vector3_t3722313464  get_lastWorldPosition_40() const { return ___lastWorldPosition_40; }
	inline Vector3_t3722313464 * get_address_of_lastWorldPosition_40() { return &___lastWorldPosition_40; }
	inline void set_lastWorldPosition_40(Vector3_t3722313464  value)
	{
		___lastWorldPosition_40 = value;
	}

	inline static int32_t get_offset_of_lastHit_41() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___lastHit_41)); }
	inline RaycastHit_t1056001966  get_lastHit_41() const { return ___lastHit_41; }
	inline RaycastHit_t1056001966 * get_address_of_lastHit_41() { return &___lastHit_41; }
	inline void set_lastHit_41(RaycastHit_t1056001966  value)
	{
		___lastHit_41 = value;
	}

	inline static int32_t get_offset_of_current_42() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___current_42)); }
	inline UICamera_t1356438871 * get_current_42() const { return ___current_42; }
	inline UICamera_t1356438871 ** get_address_of_current_42() { return &___current_42; }
	inline void set_current_42(UICamera_t1356438871 * value)
	{
		___current_42 = value;
		Il2CppCodeGenWriteBarrier(&___current_42, value);
	}

	inline static int32_t get_offset_of_currentCamera_43() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___currentCamera_43)); }
	inline Camera_t4157153871 * get_currentCamera_43() const { return ___currentCamera_43; }
	inline Camera_t4157153871 ** get_address_of_currentCamera_43() { return &___currentCamera_43; }
	inline void set_currentCamera_43(Camera_t4157153871 * value)
	{
		___currentCamera_43 = value;
		Il2CppCodeGenWriteBarrier(&___currentCamera_43, value);
	}

	inline static int32_t get_offset_of_onSchemeChange_44() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onSchemeChange_44)); }
	inline OnSchemeChange_t1701155603 * get_onSchemeChange_44() const { return ___onSchemeChange_44; }
	inline OnSchemeChange_t1701155603 ** get_address_of_onSchemeChange_44() { return &___onSchemeChange_44; }
	inline void set_onSchemeChange_44(OnSchemeChange_t1701155603 * value)
	{
		___onSchemeChange_44 = value;
		Il2CppCodeGenWriteBarrier(&___onSchemeChange_44, value);
	}

	inline static int32_t get_offset_of_currentTouchID_45() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___currentTouchID_45)); }
	inline int32_t get_currentTouchID_45() const { return ___currentTouchID_45; }
	inline int32_t* get_address_of_currentTouchID_45() { return &___currentTouchID_45; }
	inline void set_currentTouchID_45(int32_t value)
	{
		___currentTouchID_45 = value;
	}

	inline static int32_t get_offset_of_mCurrentKey_46() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mCurrentKey_46)); }
	inline int32_t get_mCurrentKey_46() const { return ___mCurrentKey_46; }
	inline int32_t* get_address_of_mCurrentKey_46() { return &___mCurrentKey_46; }
	inline void set_mCurrentKey_46(int32_t value)
	{
		___mCurrentKey_46 = value;
	}

	inline static int32_t get_offset_of_currentTouch_47() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___currentTouch_47)); }
	inline MouseOrTouch_t3052596533 * get_currentTouch_47() const { return ___currentTouch_47; }
	inline MouseOrTouch_t3052596533 ** get_address_of_currentTouch_47() { return &___currentTouch_47; }
	inline void set_currentTouch_47(MouseOrTouch_t3052596533 * value)
	{
		___currentTouch_47 = value;
		Il2CppCodeGenWriteBarrier(&___currentTouch_47, value);
	}

	inline static int32_t get_offset_of_mInputFocus_48() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mInputFocus_48)); }
	inline bool get_mInputFocus_48() const { return ___mInputFocus_48; }
	inline bool* get_address_of_mInputFocus_48() { return &___mInputFocus_48; }
	inline void set_mInputFocus_48(bool value)
	{
		___mInputFocus_48 = value;
	}

	inline static int32_t get_offset_of_mGenericHandler_49() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mGenericHandler_49)); }
	inline GameObject_t1113636619 * get_mGenericHandler_49() const { return ___mGenericHandler_49; }
	inline GameObject_t1113636619 ** get_address_of_mGenericHandler_49() { return &___mGenericHandler_49; }
	inline void set_mGenericHandler_49(GameObject_t1113636619 * value)
	{
		___mGenericHandler_49 = value;
		Il2CppCodeGenWriteBarrier(&___mGenericHandler_49, value);
	}

	inline static int32_t get_offset_of_fallThrough_50() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___fallThrough_50)); }
	inline GameObject_t1113636619 * get_fallThrough_50() const { return ___fallThrough_50; }
	inline GameObject_t1113636619 ** get_address_of_fallThrough_50() { return &___fallThrough_50; }
	inline void set_fallThrough_50(GameObject_t1113636619 * value)
	{
		___fallThrough_50 = value;
		Il2CppCodeGenWriteBarrier(&___fallThrough_50, value);
	}

	inline static int32_t get_offset_of_onClick_51() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onClick_51)); }
	inline VoidDelegate_t3100799918 * get_onClick_51() const { return ___onClick_51; }
	inline VoidDelegate_t3100799918 ** get_address_of_onClick_51() { return &___onClick_51; }
	inline void set_onClick_51(VoidDelegate_t3100799918 * value)
	{
		___onClick_51 = value;
		Il2CppCodeGenWriteBarrier(&___onClick_51, value);
	}

	inline static int32_t get_offset_of_onDoubleClick_52() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onDoubleClick_52)); }
	inline VoidDelegate_t3100799918 * get_onDoubleClick_52() const { return ___onDoubleClick_52; }
	inline VoidDelegate_t3100799918 ** get_address_of_onDoubleClick_52() { return &___onDoubleClick_52; }
	inline void set_onDoubleClick_52(VoidDelegate_t3100799918 * value)
	{
		___onDoubleClick_52 = value;
		Il2CppCodeGenWriteBarrier(&___onDoubleClick_52, value);
	}

	inline static int32_t get_offset_of_onHover_53() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onHover_53)); }
	inline BoolDelegate_t3825226153 * get_onHover_53() const { return ___onHover_53; }
	inline BoolDelegate_t3825226153 ** get_address_of_onHover_53() { return &___onHover_53; }
	inline void set_onHover_53(BoolDelegate_t3825226153 * value)
	{
		___onHover_53 = value;
		Il2CppCodeGenWriteBarrier(&___onHover_53, value);
	}

	inline static int32_t get_offset_of_onPress_54() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onPress_54)); }
	inline BoolDelegate_t3825226153 * get_onPress_54() const { return ___onPress_54; }
	inline BoolDelegate_t3825226153 ** get_address_of_onPress_54() { return &___onPress_54; }
	inline void set_onPress_54(BoolDelegate_t3825226153 * value)
	{
		___onPress_54 = value;
		Il2CppCodeGenWriteBarrier(&___onPress_54, value);
	}

	inline static int32_t get_offset_of_onSelect_55() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onSelect_55)); }
	inline BoolDelegate_t3825226153 * get_onSelect_55() const { return ___onSelect_55; }
	inline BoolDelegate_t3825226153 ** get_address_of_onSelect_55() { return &___onSelect_55; }
	inline void set_onSelect_55(BoolDelegate_t3825226153 * value)
	{
		___onSelect_55 = value;
		Il2CppCodeGenWriteBarrier(&___onSelect_55, value);
	}

	inline static int32_t get_offset_of_onScroll_56() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onScroll_56)); }
	inline FloatDelegate_t906524069 * get_onScroll_56() const { return ___onScroll_56; }
	inline FloatDelegate_t906524069 ** get_address_of_onScroll_56() { return &___onScroll_56; }
	inline void set_onScroll_56(FloatDelegate_t906524069 * value)
	{
		___onScroll_56 = value;
		Il2CppCodeGenWriteBarrier(&___onScroll_56, value);
	}

	inline static int32_t get_offset_of_onDrag_57() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onDrag_57)); }
	inline VectorDelegate_t435795517 * get_onDrag_57() const { return ___onDrag_57; }
	inline VectorDelegate_t435795517 ** get_address_of_onDrag_57() { return &___onDrag_57; }
	inline void set_onDrag_57(VectorDelegate_t435795517 * value)
	{
		___onDrag_57 = value;
		Il2CppCodeGenWriteBarrier(&___onDrag_57, value);
	}

	inline static int32_t get_offset_of_onDragStart_58() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onDragStart_58)); }
	inline VoidDelegate_t3100799918 * get_onDragStart_58() const { return ___onDragStart_58; }
	inline VoidDelegate_t3100799918 ** get_address_of_onDragStart_58() { return &___onDragStart_58; }
	inline void set_onDragStart_58(VoidDelegate_t3100799918 * value)
	{
		___onDragStart_58 = value;
		Il2CppCodeGenWriteBarrier(&___onDragStart_58, value);
	}

	inline static int32_t get_offset_of_onDragOver_59() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onDragOver_59)); }
	inline ObjectDelegate_t2041570719 * get_onDragOver_59() const { return ___onDragOver_59; }
	inline ObjectDelegate_t2041570719 ** get_address_of_onDragOver_59() { return &___onDragOver_59; }
	inline void set_onDragOver_59(ObjectDelegate_t2041570719 * value)
	{
		___onDragOver_59 = value;
		Il2CppCodeGenWriteBarrier(&___onDragOver_59, value);
	}

	inline static int32_t get_offset_of_onDragOut_60() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onDragOut_60)); }
	inline ObjectDelegate_t2041570719 * get_onDragOut_60() const { return ___onDragOut_60; }
	inline ObjectDelegate_t2041570719 ** get_address_of_onDragOut_60() { return &___onDragOut_60; }
	inline void set_onDragOut_60(ObjectDelegate_t2041570719 * value)
	{
		___onDragOut_60 = value;
		Il2CppCodeGenWriteBarrier(&___onDragOut_60, value);
	}

	inline static int32_t get_offset_of_onDragEnd_61() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onDragEnd_61)); }
	inline VoidDelegate_t3100799918 * get_onDragEnd_61() const { return ___onDragEnd_61; }
	inline VoidDelegate_t3100799918 ** get_address_of_onDragEnd_61() { return &___onDragEnd_61; }
	inline void set_onDragEnd_61(VoidDelegate_t3100799918 * value)
	{
		___onDragEnd_61 = value;
		Il2CppCodeGenWriteBarrier(&___onDragEnd_61, value);
	}

	inline static int32_t get_offset_of_onDrop_62() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onDrop_62)); }
	inline ObjectDelegate_t2041570719 * get_onDrop_62() const { return ___onDrop_62; }
	inline ObjectDelegate_t2041570719 ** get_address_of_onDrop_62() { return &___onDrop_62; }
	inline void set_onDrop_62(ObjectDelegate_t2041570719 * value)
	{
		___onDrop_62 = value;
		Il2CppCodeGenWriteBarrier(&___onDrop_62, value);
	}

	inline static int32_t get_offset_of_onKey_63() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onKey_63)); }
	inline KeyCodeDelegate_t3064672302 * get_onKey_63() const { return ___onKey_63; }
	inline KeyCodeDelegate_t3064672302 ** get_address_of_onKey_63() { return &___onKey_63; }
	inline void set_onKey_63(KeyCodeDelegate_t3064672302 * value)
	{
		___onKey_63 = value;
		Il2CppCodeGenWriteBarrier(&___onKey_63, value);
	}

	inline static int32_t get_offset_of_onNavigate_64() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onNavigate_64)); }
	inline KeyCodeDelegate_t3064672302 * get_onNavigate_64() const { return ___onNavigate_64; }
	inline KeyCodeDelegate_t3064672302 ** get_address_of_onNavigate_64() { return &___onNavigate_64; }
	inline void set_onNavigate_64(KeyCodeDelegate_t3064672302 * value)
	{
		___onNavigate_64 = value;
		Il2CppCodeGenWriteBarrier(&___onNavigate_64, value);
	}

	inline static int32_t get_offset_of_onPan_65() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onPan_65)); }
	inline VectorDelegate_t435795517 * get_onPan_65() const { return ___onPan_65; }
	inline VectorDelegate_t435795517 ** get_address_of_onPan_65() { return &___onPan_65; }
	inline void set_onPan_65(VectorDelegate_t435795517 * value)
	{
		___onPan_65 = value;
		Il2CppCodeGenWriteBarrier(&___onPan_65, value);
	}

	inline static int32_t get_offset_of_onTooltip_66() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onTooltip_66)); }
	inline BoolDelegate_t3825226153 * get_onTooltip_66() const { return ___onTooltip_66; }
	inline BoolDelegate_t3825226153 ** get_address_of_onTooltip_66() { return &___onTooltip_66; }
	inline void set_onTooltip_66(BoolDelegate_t3825226153 * value)
	{
		___onTooltip_66 = value;
		Il2CppCodeGenWriteBarrier(&___onTooltip_66, value);
	}

	inline static int32_t get_offset_of_onMouseMove_67() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___onMouseMove_67)); }
	inline MoveDelegate_t16019400 * get_onMouseMove_67() const { return ___onMouseMove_67; }
	inline MoveDelegate_t16019400 ** get_address_of_onMouseMove_67() { return &___onMouseMove_67; }
	inline void set_onMouseMove_67(MoveDelegate_t16019400 * value)
	{
		___onMouseMove_67 = value;
		Il2CppCodeGenWriteBarrier(&___onMouseMove_67, value);
	}

	inline static int32_t get_offset_of_mMouse_68() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mMouse_68)); }
	inline MouseOrTouchU5BU5D_t3534648920* get_mMouse_68() const { return ___mMouse_68; }
	inline MouseOrTouchU5BU5D_t3534648920** get_address_of_mMouse_68() { return &___mMouse_68; }
	inline void set_mMouse_68(MouseOrTouchU5BU5D_t3534648920* value)
	{
		___mMouse_68 = value;
		Il2CppCodeGenWriteBarrier(&___mMouse_68, value);
	}

	inline static int32_t get_offset_of_controller_69() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___controller_69)); }
	inline MouseOrTouch_t3052596533 * get_controller_69() const { return ___controller_69; }
	inline MouseOrTouch_t3052596533 ** get_address_of_controller_69() { return &___controller_69; }
	inline void set_controller_69(MouseOrTouch_t3052596533 * value)
	{
		___controller_69 = value;
		Il2CppCodeGenWriteBarrier(&___controller_69, value);
	}

	inline static int32_t get_offset_of_activeTouches_70() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___activeTouches_70)); }
	inline List_1_t229703979 * get_activeTouches_70() const { return ___activeTouches_70; }
	inline List_1_t229703979 ** get_address_of_activeTouches_70() { return &___activeTouches_70; }
	inline void set_activeTouches_70(List_1_t229703979 * value)
	{
		___activeTouches_70 = value;
		Il2CppCodeGenWriteBarrier(&___activeTouches_70, value);
	}

	inline static int32_t get_offset_of_mTouchIDs_71() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mTouchIDs_71)); }
	inline List_1_t128053199 * get_mTouchIDs_71() const { return ___mTouchIDs_71; }
	inline List_1_t128053199 ** get_address_of_mTouchIDs_71() { return &___mTouchIDs_71; }
	inline void set_mTouchIDs_71(List_1_t128053199 * value)
	{
		___mTouchIDs_71 = value;
		Il2CppCodeGenWriteBarrier(&___mTouchIDs_71, value);
	}

	inline static int32_t get_offset_of_mWidth_72() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mWidth_72)); }
	inline int32_t get_mWidth_72() const { return ___mWidth_72; }
	inline int32_t* get_address_of_mWidth_72() { return &___mWidth_72; }
	inline void set_mWidth_72(int32_t value)
	{
		___mWidth_72 = value;
	}

	inline static int32_t get_offset_of_mHeight_73() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mHeight_73)); }
	inline int32_t get_mHeight_73() const { return ___mHeight_73; }
	inline int32_t* get_address_of_mHeight_73() { return &___mHeight_73; }
	inline void set_mHeight_73(int32_t value)
	{
		___mHeight_73 = value;
	}

	inline static int32_t get_offset_of_mTooltip_74() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mTooltip_74)); }
	inline GameObject_t1113636619 * get_mTooltip_74() const { return ___mTooltip_74; }
	inline GameObject_t1113636619 ** get_address_of_mTooltip_74() { return &___mTooltip_74; }
	inline void set_mTooltip_74(GameObject_t1113636619 * value)
	{
		___mTooltip_74 = value;
		Il2CppCodeGenWriteBarrier(&___mTooltip_74, value);
	}

	inline static int32_t get_offset_of_mTooltipTime_76() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mTooltipTime_76)); }
	inline float get_mTooltipTime_76() const { return ___mTooltipTime_76; }
	inline float* get_address_of_mTooltipTime_76() { return &___mTooltipTime_76; }
	inline void set_mTooltipTime_76(float value)
	{
		___mTooltipTime_76 = value;
	}

	inline static int32_t get_offset_of_isDragging_78() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___isDragging_78)); }
	inline bool get_isDragging_78() const { return ___isDragging_78; }
	inline bool* get_address_of_isDragging_78() { return &___isDragging_78; }
	inline void set_isDragging_78(bool value)
	{
		___isDragging_78 = value;
	}

	inline static int32_t get_offset_of_mRayHitObject_79() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mRayHitObject_79)); }
	inline GameObject_t1113636619 * get_mRayHitObject_79() const { return ___mRayHitObject_79; }
	inline GameObject_t1113636619 ** get_address_of_mRayHitObject_79() { return &___mRayHitObject_79; }
	inline void set_mRayHitObject_79(GameObject_t1113636619 * value)
	{
		___mRayHitObject_79 = value;
		Il2CppCodeGenWriteBarrier(&___mRayHitObject_79, value);
	}

	inline static int32_t get_offset_of_mHover_80() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mHover_80)); }
	inline GameObject_t1113636619 * get_mHover_80() const { return ___mHover_80; }
	inline GameObject_t1113636619 ** get_address_of_mHover_80() { return &___mHover_80; }
	inline void set_mHover_80(GameObject_t1113636619 * value)
	{
		___mHover_80 = value;
		Il2CppCodeGenWriteBarrier(&___mHover_80, value);
	}

	inline static int32_t get_offset_of_mSelected_81() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mSelected_81)); }
	inline GameObject_t1113636619 * get_mSelected_81() const { return ___mSelected_81; }
	inline GameObject_t1113636619 ** get_address_of_mSelected_81() { return &___mSelected_81; }
	inline void set_mSelected_81(GameObject_t1113636619 * value)
	{
		___mSelected_81 = value;
		Il2CppCodeGenWriteBarrier(&___mSelected_81, value);
	}

	inline static int32_t get_offset_of_mHit_82() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mHit_82)); }
	inline DepthEntry_t628749918  get_mHit_82() const { return ___mHit_82; }
	inline DepthEntry_t628749918 * get_address_of_mHit_82() { return &___mHit_82; }
	inline void set_mHit_82(DepthEntry_t628749918  value)
	{
		___mHit_82 = value;
	}

	inline static int32_t get_offset_of_mHits_83() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mHits_83)); }
	inline BetterList_1_t4078737532 * get_mHits_83() const { return ___mHits_83; }
	inline BetterList_1_t4078737532 ** get_address_of_mHits_83() { return &___mHits_83; }
	inline void set_mHits_83(BetterList_1_t4078737532 * value)
	{
		___mHits_83 = value;
		Il2CppCodeGenWriteBarrier(&___mHits_83, value);
	}

	inline static int32_t get_offset_of_m2DPlane_84() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___m2DPlane_84)); }
	inline Plane_t1000493321  get_m2DPlane_84() const { return ___m2DPlane_84; }
	inline Plane_t1000493321 * get_address_of_m2DPlane_84() { return &___m2DPlane_84; }
	inline void set_m2DPlane_84(Plane_t1000493321  value)
	{
		___m2DPlane_84 = value;
	}

	inline static int32_t get_offset_of_mNextEvent_85() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mNextEvent_85)); }
	inline float get_mNextEvent_85() const { return ___mNextEvent_85; }
	inline float* get_address_of_mNextEvent_85() { return &___mNextEvent_85; }
	inline void set_mNextEvent_85(float value)
	{
		___mNextEvent_85 = value;
	}

	inline static int32_t get_offset_of_mNotifying_86() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mNotifying_86)); }
	inline int32_t get_mNotifying_86() const { return ___mNotifying_86; }
	inline int32_t* get_address_of_mNotifying_86() { return &___mNotifying_86; }
	inline void set_mNotifying_86(int32_t value)
	{
		___mNotifying_86 = value;
	}

	inline static int32_t get_offset_of_mUsingTouchEvents_87() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___mUsingTouchEvents_87)); }
	inline bool get_mUsingTouchEvents_87() const { return ___mUsingTouchEvents_87; }
	inline bool* get_address_of_mUsingTouchEvents_87() { return &___mUsingTouchEvents_87; }
	inline void set_mUsingTouchEvents_87(bool value)
	{
		___mUsingTouchEvents_87 = value;
	}

	inline static int32_t get_offset_of_GetInputTouchCount_88() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___GetInputTouchCount_88)); }
	inline GetTouchCountCallback_t3185863032 * get_GetInputTouchCount_88() const { return ___GetInputTouchCount_88; }
	inline GetTouchCountCallback_t3185863032 ** get_address_of_GetInputTouchCount_88() { return &___GetInputTouchCount_88; }
	inline void set_GetInputTouchCount_88(GetTouchCountCallback_t3185863032 * value)
	{
		___GetInputTouchCount_88 = value;
		Il2CppCodeGenWriteBarrier(&___GetInputTouchCount_88, value);
	}

	inline static int32_t get_offset_of_GetInputTouch_89() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___GetInputTouch_89)); }
	inline GetTouchCallback_t97678626 * get_GetInputTouch_89() const { return ___GetInputTouch_89; }
	inline GetTouchCallback_t97678626 ** get_address_of_GetInputTouch_89() { return &___GetInputTouch_89; }
	inline void set_GetInputTouch_89(GetTouchCallback_t97678626 * value)
	{
		___GetInputTouch_89 = value;
		Il2CppCodeGenWriteBarrier(&___GetInputTouch_89, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_90() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___U3CU3Ef__amU24cache0_90)); }
	inline CompareFunc_t2967399990 * get_U3CU3Ef__amU24cache0_90() const { return ___U3CU3Ef__amU24cache0_90; }
	inline CompareFunc_t2967399990 ** get_address_of_U3CU3Ef__amU24cache0_90() { return &___U3CU3Ef__amU24cache0_90; }
	inline void set_U3CU3Ef__amU24cache0_90(CompareFunc_t2967399990 * value)
	{
		___U3CU3Ef__amU24cache0_90 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_90, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_91() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___U3CU3Ef__amU24cache1_91)); }
	inline CompareFunc_t2967399990 * get_U3CU3Ef__amU24cache1_91() const { return ___U3CU3Ef__amU24cache1_91; }
	inline CompareFunc_t2967399990 ** get_address_of_U3CU3Ef__amU24cache1_91() { return &___U3CU3Ef__amU24cache1_91; }
	inline void set_U3CU3Ef__amU24cache1_91(CompareFunc_t2967399990 * value)
	{
		___U3CU3Ef__amU24cache1_91 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_91, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_92() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___U3CU3Ef__mgU24cache0_92)); }
	inline CompareFunc_t3695088943 * get_U3CU3Ef__mgU24cache0_92() const { return ___U3CU3Ef__mgU24cache0_92; }
	inline CompareFunc_t3695088943 ** get_address_of_U3CU3Ef__mgU24cache0_92() { return &___U3CU3Ef__mgU24cache0_92; }
	inline void set_U3CU3Ef__mgU24cache0_92(CompareFunc_t3695088943 * value)
	{
		___U3CU3Ef__mgU24cache0_92 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_92, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_93() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___U3CU3Ef__mgU24cache1_93)); }
	inline GetKeyStateFunc_t2810275146 * get_U3CU3Ef__mgU24cache1_93() const { return ___U3CU3Ef__mgU24cache1_93; }
	inline GetKeyStateFunc_t2810275146 ** get_address_of_U3CU3Ef__mgU24cache1_93() { return &___U3CU3Ef__mgU24cache1_93; }
	inline void set_U3CU3Ef__mgU24cache1_93(GetKeyStateFunc_t2810275146 * value)
	{
		___U3CU3Ef__mgU24cache1_93 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache1_93, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_94() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___U3CU3Ef__mgU24cache2_94)); }
	inline GetKeyStateFunc_t2810275146 * get_U3CU3Ef__mgU24cache2_94() const { return ___U3CU3Ef__mgU24cache2_94; }
	inline GetKeyStateFunc_t2810275146 ** get_address_of_U3CU3Ef__mgU24cache2_94() { return &___U3CU3Ef__mgU24cache2_94; }
	inline void set_U3CU3Ef__mgU24cache2_94(GetKeyStateFunc_t2810275146 * value)
	{
		___U3CU3Ef__mgU24cache2_94 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache2_94, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_95() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___U3CU3Ef__mgU24cache3_95)); }
	inline GetKeyStateFunc_t2810275146 * get_U3CU3Ef__mgU24cache3_95() const { return ___U3CU3Ef__mgU24cache3_95; }
	inline GetKeyStateFunc_t2810275146 ** get_address_of_U3CU3Ef__mgU24cache3_95() { return &___U3CU3Ef__mgU24cache3_95; }
	inline void set_U3CU3Ef__mgU24cache3_95(GetKeyStateFunc_t2810275146 * value)
	{
		___U3CU3Ef__mgU24cache3_95 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache3_95, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_96() { return static_cast<int32_t>(offsetof(UICamera_t1356438871_StaticFields, ___U3CU3Ef__mgU24cache4_96)); }
	inline GetAxisFunc_t2592608932 * get_U3CU3Ef__mgU24cache4_96() const { return ___U3CU3Ef__mgU24cache4_96; }
	inline GetAxisFunc_t2592608932 ** get_address_of_U3CU3Ef__mgU24cache4_96() { return &___U3CU3Ef__mgU24cache4_96; }
	inline void set_U3CU3Ef__mgU24cache4_96(GetAxisFunc_t2592608932 * value)
	{
		___U3CU3Ef__mgU24cache4_96 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache4_96, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
