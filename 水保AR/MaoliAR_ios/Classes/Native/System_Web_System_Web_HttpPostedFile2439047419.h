﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.IO.Stream
struct Stream_t1273022909;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpPostedFile
struct  HttpPostedFile_t2439047419  : public Il2CppObject
{
public:
	// System.String System.Web.HttpPostedFile::name
	String_t* ___name_0;
	// System.String System.Web.HttpPostedFile::content_type
	String_t* ___content_type_1;
	// System.IO.Stream System.Web.HttpPostedFile::stream
	Stream_t1273022909 * ___stream_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(HttpPostedFile_t2439047419, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_content_type_1() { return static_cast<int32_t>(offsetof(HttpPostedFile_t2439047419, ___content_type_1)); }
	inline String_t* get_content_type_1() const { return ___content_type_1; }
	inline String_t** get_address_of_content_type_1() { return &___content_type_1; }
	inline void set_content_type_1(String_t* value)
	{
		___content_type_1 = value;
		Il2CppCodeGenWriteBarrier(&___content_type_1, value);
	}

	inline static int32_t get_offset_of_stream_2() { return static_cast<int32_t>(offsetof(HttpPostedFile_t2439047419, ___stream_2)); }
	inline Stream_t1273022909 * get_stream_2() const { return ___stream_2; }
	inline Stream_t1273022909 ** get_address_of_stream_2() { return &___stream_2; }
	inline void set_stream_2(Stream_t1273022909 * value)
	{
		___stream_2 = value;
		Il2CppCodeGenWriteBarrier(&___stream_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
