﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Operations_XslCompiledElemen50593777.h"

// Mono.Xml.Xsl.Operations.XslOperation
struct XslOperation_t2153241355;
// System.String
struct String_t;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Xml.XmlQualifiedName[]
struct XmlQualifiedNameU5BU5D_t1471530361;
// System.Collections.Hashtable
struct Hashtable_t1853889766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslLiteralElement
struct  XslLiteralElement_t1817810350  : public XslCompiledElement_t50593777
{
public:
	// Mono.Xml.Xsl.Operations.XslOperation Mono.Xml.Xsl.Operations.XslLiteralElement::children
	XslOperation_t2153241355 * ___children_3;
	// System.String Mono.Xml.Xsl.Operations.XslLiteralElement::localname
	String_t* ___localname_4;
	// System.String Mono.Xml.Xsl.Operations.XslLiteralElement::prefix
	String_t* ___prefix_5;
	// System.String Mono.Xml.Xsl.Operations.XslLiteralElement::nsUri
	String_t* ___nsUri_6;
	// System.Boolean Mono.Xml.Xsl.Operations.XslLiteralElement::isEmptyElement
	bool ___isEmptyElement_7;
	// System.Collections.ArrayList Mono.Xml.Xsl.Operations.XslLiteralElement::attrs
	ArrayList_t2718874744 * ___attrs_8;
	// System.Xml.XmlQualifiedName[] Mono.Xml.Xsl.Operations.XslLiteralElement::useAttributeSets
	XmlQualifiedNameU5BU5D_t1471530361* ___useAttributeSets_9;
	// System.Collections.Hashtable Mono.Xml.Xsl.Operations.XslLiteralElement::nsDecls
	Hashtable_t1853889766 * ___nsDecls_10;

public:
	inline static int32_t get_offset_of_children_3() { return static_cast<int32_t>(offsetof(XslLiteralElement_t1817810350, ___children_3)); }
	inline XslOperation_t2153241355 * get_children_3() const { return ___children_3; }
	inline XslOperation_t2153241355 ** get_address_of_children_3() { return &___children_3; }
	inline void set_children_3(XslOperation_t2153241355 * value)
	{
		___children_3 = value;
		Il2CppCodeGenWriteBarrier(&___children_3, value);
	}

	inline static int32_t get_offset_of_localname_4() { return static_cast<int32_t>(offsetof(XslLiteralElement_t1817810350, ___localname_4)); }
	inline String_t* get_localname_4() const { return ___localname_4; }
	inline String_t** get_address_of_localname_4() { return &___localname_4; }
	inline void set_localname_4(String_t* value)
	{
		___localname_4 = value;
		Il2CppCodeGenWriteBarrier(&___localname_4, value);
	}

	inline static int32_t get_offset_of_prefix_5() { return static_cast<int32_t>(offsetof(XslLiteralElement_t1817810350, ___prefix_5)); }
	inline String_t* get_prefix_5() const { return ___prefix_5; }
	inline String_t** get_address_of_prefix_5() { return &___prefix_5; }
	inline void set_prefix_5(String_t* value)
	{
		___prefix_5 = value;
		Il2CppCodeGenWriteBarrier(&___prefix_5, value);
	}

	inline static int32_t get_offset_of_nsUri_6() { return static_cast<int32_t>(offsetof(XslLiteralElement_t1817810350, ___nsUri_6)); }
	inline String_t* get_nsUri_6() const { return ___nsUri_6; }
	inline String_t** get_address_of_nsUri_6() { return &___nsUri_6; }
	inline void set_nsUri_6(String_t* value)
	{
		___nsUri_6 = value;
		Il2CppCodeGenWriteBarrier(&___nsUri_6, value);
	}

	inline static int32_t get_offset_of_isEmptyElement_7() { return static_cast<int32_t>(offsetof(XslLiteralElement_t1817810350, ___isEmptyElement_7)); }
	inline bool get_isEmptyElement_7() const { return ___isEmptyElement_7; }
	inline bool* get_address_of_isEmptyElement_7() { return &___isEmptyElement_7; }
	inline void set_isEmptyElement_7(bool value)
	{
		___isEmptyElement_7 = value;
	}

	inline static int32_t get_offset_of_attrs_8() { return static_cast<int32_t>(offsetof(XslLiteralElement_t1817810350, ___attrs_8)); }
	inline ArrayList_t2718874744 * get_attrs_8() const { return ___attrs_8; }
	inline ArrayList_t2718874744 ** get_address_of_attrs_8() { return &___attrs_8; }
	inline void set_attrs_8(ArrayList_t2718874744 * value)
	{
		___attrs_8 = value;
		Il2CppCodeGenWriteBarrier(&___attrs_8, value);
	}

	inline static int32_t get_offset_of_useAttributeSets_9() { return static_cast<int32_t>(offsetof(XslLiteralElement_t1817810350, ___useAttributeSets_9)); }
	inline XmlQualifiedNameU5BU5D_t1471530361* get_useAttributeSets_9() const { return ___useAttributeSets_9; }
	inline XmlQualifiedNameU5BU5D_t1471530361** get_address_of_useAttributeSets_9() { return &___useAttributeSets_9; }
	inline void set_useAttributeSets_9(XmlQualifiedNameU5BU5D_t1471530361* value)
	{
		___useAttributeSets_9 = value;
		Il2CppCodeGenWriteBarrier(&___useAttributeSets_9, value);
	}

	inline static int32_t get_offset_of_nsDecls_10() { return static_cast<int32_t>(offsetof(XslLiteralElement_t1817810350, ___nsDecls_10)); }
	inline Hashtable_t1853889766 * get_nsDecls_10() const { return ___nsDecls_10; }
	inline Hashtable_t1853889766 ** get_address_of_nsDecls_10() { return &___nsDecls_10; }
	inline void set_nsDecls_10(Hashtable_t1853889766 * value)
	{
		___nsDecls_10 = value;
		Il2CppCodeGenWriteBarrier(&___nsDecls_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
