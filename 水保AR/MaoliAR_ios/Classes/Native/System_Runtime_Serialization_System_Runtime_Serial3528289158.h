﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Runtime_Serialization_System_Runtime_Serial4088073813.h"

// System.Type
struct Type_t;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2760654312;
// System.Reflection.MethodInfo
struct MethodInfo_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.CollectionTypeMap
struct  CollectionTypeMap_t3528289158  : public SerializationMap_t4088073813
{
public:
	// System.Type System.Runtime.Serialization.CollectionTypeMap::element_type
	Type_t * ___element_type_6;
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.CollectionTypeMap::element_qname
	XmlQualifiedName_t2760654312 * ___element_qname_7;
	// System.Reflection.MethodInfo System.Runtime.Serialization.CollectionTypeMap::add_method
	MethodInfo_t * ___add_method_8;

public:
	inline static int32_t get_offset_of_element_type_6() { return static_cast<int32_t>(offsetof(CollectionTypeMap_t3528289158, ___element_type_6)); }
	inline Type_t * get_element_type_6() const { return ___element_type_6; }
	inline Type_t ** get_address_of_element_type_6() { return &___element_type_6; }
	inline void set_element_type_6(Type_t * value)
	{
		___element_type_6 = value;
		Il2CppCodeGenWriteBarrier(&___element_type_6, value);
	}

	inline static int32_t get_offset_of_element_qname_7() { return static_cast<int32_t>(offsetof(CollectionTypeMap_t3528289158, ___element_qname_7)); }
	inline XmlQualifiedName_t2760654312 * get_element_qname_7() const { return ___element_qname_7; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_element_qname_7() { return &___element_qname_7; }
	inline void set_element_qname_7(XmlQualifiedName_t2760654312 * value)
	{
		___element_qname_7 = value;
		Il2CppCodeGenWriteBarrier(&___element_qname_7, value);
	}

	inline static int32_t get_offset_of_add_method_8() { return static_cast<int32_t>(offsetof(CollectionTypeMap_t3528289158, ___add_method_8)); }
	inline MethodInfo_t * get_add_method_8() const { return ___add_method_8; }
	inline MethodInfo_t ** get_address_of_add_method_8() { return &___add_method_8; }
	inline void set_add_method_8(MethodInfo_t * value)
	{
		___add_method_8 = value;
		Il2CppCodeGenWriteBarrier(&___add_method_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
