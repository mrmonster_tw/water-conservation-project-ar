﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Security_S2009957818.h"

// System.Byte[]
struct ByteU5BU5D_t4116647657;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.DataProtectionSecurityStateEncoder
struct  DataProtectionSecurityStateEncoder_t2922702212  : public SecurityStateEncoder_t2009957818
{
public:
	// System.Boolean System.ServiceModel.Security.DataProtectionSecurityStateEncoder::user
	bool ___user_0;
	// System.Byte[] System.ServiceModel.Security.DataProtectionSecurityStateEncoder::entropy
	ByteU5BU5D_t4116647657* ___entropy_1;

public:
	inline static int32_t get_offset_of_user_0() { return static_cast<int32_t>(offsetof(DataProtectionSecurityStateEncoder_t2922702212, ___user_0)); }
	inline bool get_user_0() const { return ___user_0; }
	inline bool* get_address_of_user_0() { return &___user_0; }
	inline void set_user_0(bool value)
	{
		___user_0 = value;
	}

	inline static int32_t get_offset_of_entropy_1() { return static_cast<int32_t>(offsetof(DataProtectionSecurityStateEncoder_t2922702212, ___entropy_1)); }
	inline ByteU5BU5D_t4116647657* get_entropy_1() const { return ___entropy_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_entropy_1() { return &___entropy_1; }
	inline void set_entropy_1(ByteU5BU5D_t4116647657* value)
	{
		___entropy_1 = value;
		Il2CppCodeGenWriteBarrier(&___entropy_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
