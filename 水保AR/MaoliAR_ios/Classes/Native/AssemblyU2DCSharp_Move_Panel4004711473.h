﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.Transform
struct Transform_t3600365921;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Move_Panel
struct  Move_Panel_t4004711473  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform Move_Panel::pos_show
	Transform_t3600365921 * ___pos_show_2;
	// UnityEngine.Transform Move_Panel::pos_unshow
	Transform_t3600365921 * ___pos_unshow_3;
	// System.Single Move_Panel::_Time
	float ____Time_4;

public:
	inline static int32_t get_offset_of_pos_show_2() { return static_cast<int32_t>(offsetof(Move_Panel_t4004711473, ___pos_show_2)); }
	inline Transform_t3600365921 * get_pos_show_2() const { return ___pos_show_2; }
	inline Transform_t3600365921 ** get_address_of_pos_show_2() { return &___pos_show_2; }
	inline void set_pos_show_2(Transform_t3600365921 * value)
	{
		___pos_show_2 = value;
		Il2CppCodeGenWriteBarrier(&___pos_show_2, value);
	}

	inline static int32_t get_offset_of_pos_unshow_3() { return static_cast<int32_t>(offsetof(Move_Panel_t4004711473, ___pos_unshow_3)); }
	inline Transform_t3600365921 * get_pos_unshow_3() const { return ___pos_unshow_3; }
	inline Transform_t3600365921 ** get_address_of_pos_unshow_3() { return &___pos_unshow_3; }
	inline void set_pos_unshow_3(Transform_t3600365921 * value)
	{
		___pos_unshow_3 = value;
		Il2CppCodeGenWriteBarrier(&___pos_unshow_3, value);
	}

	inline static int32_t get_offset_of__Time_4() { return static_cast<int32_t>(offsetof(Move_Panel_t4004711473, ____Time_4)); }
	inline float get__Time_4() const { return ____Time_4; }
	inline float* get_address_of__Time_4() { return &____Time_4; }
	inline void set__Time_4(float value)
	{
		____Time_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
