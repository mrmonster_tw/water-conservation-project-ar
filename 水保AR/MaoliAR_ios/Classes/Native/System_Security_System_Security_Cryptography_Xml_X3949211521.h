﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Security_System_Security_Cryptography_Xml_T1105379765.h"

// Mono.Xml.XmlCanonicalizer
struct XmlCanonicalizer_t3076776375;
// System.IO.Stream
struct Stream_t1273022909;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.Xml.XmlDsigC14NTransform
struct  XmlDsigC14NTransform_t3949211521  : public Transform_t1105379765
{
public:
	// Mono.Xml.XmlCanonicalizer System.Security.Cryptography.Xml.XmlDsigC14NTransform::canonicalizer
	XmlCanonicalizer_t3076776375 * ___canonicalizer_3;
	// System.IO.Stream System.Security.Cryptography.Xml.XmlDsigC14NTransform::s
	Stream_t1273022909 * ___s_4;

public:
	inline static int32_t get_offset_of_canonicalizer_3() { return static_cast<int32_t>(offsetof(XmlDsigC14NTransform_t3949211521, ___canonicalizer_3)); }
	inline XmlCanonicalizer_t3076776375 * get_canonicalizer_3() const { return ___canonicalizer_3; }
	inline XmlCanonicalizer_t3076776375 ** get_address_of_canonicalizer_3() { return &___canonicalizer_3; }
	inline void set_canonicalizer_3(XmlCanonicalizer_t3076776375 * value)
	{
		___canonicalizer_3 = value;
		Il2CppCodeGenWriteBarrier(&___canonicalizer_3, value);
	}

	inline static int32_t get_offset_of_s_4() { return static_cast<int32_t>(offsetof(XmlDsigC14NTransform_t3949211521, ___s_4)); }
	inline Stream_t1273022909 * get_s_4() const { return ___s_4; }
	inline Stream_t1273022909 ** get_address_of_s_4() { return &___s_4; }
	inline void set_s_4(Stream_t1273022909 * value)
	{
		___s_4 = value;
		Il2CppCodeGenWriteBarrier(&___s_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
