﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.Transform
struct Transform_t3600365921;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SchoolToWordPos
struct  SchoolToWordPos_t3829711464  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform SchoolToWordPos::SchoolFwTarget
	Transform_t3600365921 * ___SchoolFwTarget_2;
	// System.Single SchoolToWordPos::BaseScale
	float ___BaseScale_3;

public:
	inline static int32_t get_offset_of_SchoolFwTarget_2() { return static_cast<int32_t>(offsetof(SchoolToWordPos_t3829711464, ___SchoolFwTarget_2)); }
	inline Transform_t3600365921 * get_SchoolFwTarget_2() const { return ___SchoolFwTarget_2; }
	inline Transform_t3600365921 ** get_address_of_SchoolFwTarget_2() { return &___SchoolFwTarget_2; }
	inline void set_SchoolFwTarget_2(Transform_t3600365921 * value)
	{
		___SchoolFwTarget_2 = value;
		Il2CppCodeGenWriteBarrier(&___SchoolFwTarget_2, value);
	}

	inline static int32_t get_offset_of_BaseScale_3() { return static_cast<int32_t>(offsetof(SchoolToWordPos_t3829711464, ___BaseScale_3)); }
	inline float get_BaseScale_3() const { return ___BaseScale_3; }
	inline float* get_address_of_BaseScale_3() { return &___BaseScale_3; }
	inline void set_BaseScale_3(float value)
	{
		___BaseScale_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
