﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Ht555124713.h"

// System.ServiceModel.Channels.SvcHttpHandler
struct SvcHttpHandler_t1718683511;
// System.Func`2<System.ServiceModel.Channels.IChannelListener,System.Web.HttpContext>
struct Func_2_t1648433686;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.AspNetListenerManager
struct  AspNetListenerManager_t71798493  : public HttpListenerManager_t555124713
{
public:
	// System.ServiceModel.Channels.SvcHttpHandler System.ServiceModel.Channels.AspNetListenerManager::http_handler
	SvcHttpHandler_t1718683511 * ___http_handler_7;
	// System.Func`2<System.ServiceModel.Channels.IChannelListener,System.Web.HttpContext> System.ServiceModel.Channels.AspNetListenerManager::wait_delegate
	Func_2_t1648433686 * ___wait_delegate_8;

public:
	inline static int32_t get_offset_of_http_handler_7() { return static_cast<int32_t>(offsetof(AspNetListenerManager_t71798493, ___http_handler_7)); }
	inline SvcHttpHandler_t1718683511 * get_http_handler_7() const { return ___http_handler_7; }
	inline SvcHttpHandler_t1718683511 ** get_address_of_http_handler_7() { return &___http_handler_7; }
	inline void set_http_handler_7(SvcHttpHandler_t1718683511 * value)
	{
		___http_handler_7 = value;
		Il2CppCodeGenWriteBarrier(&___http_handler_7, value);
	}

	inline static int32_t get_offset_of_wait_delegate_8() { return static_cast<int32_t>(offsetof(AspNetListenerManager_t71798493, ___wait_delegate_8)); }
	inline Func_2_t1648433686 * get_wait_delegate_8() const { return ___wait_delegate_8; }
	inline Func_2_t1648433686 ** get_address_of_wait_delegate_8() { return &___wait_delegate_8; }
	inline void set_wait_delegate_8(Func_2_t1648433686 * value)
	{
		___wait_delegate_8 = value;
		Il2CppCodeGenWriteBarrier(&___wait_delegate_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
