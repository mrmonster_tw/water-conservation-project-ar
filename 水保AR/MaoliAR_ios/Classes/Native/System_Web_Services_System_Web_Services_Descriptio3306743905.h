﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_Services_System_Web_Services_Descriptio3177955060.h"
#include "System_Web_Services_System_Web_Services_Descriptio2844024356.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.SoapOperationBinding
struct  SoapOperationBinding_t3306743905  : public ServiceDescriptionFormatExtension_t3177955060
{
public:
	// System.String System.Web.Services.Description.SoapOperationBinding::soapAction
	String_t* ___soapAction_1;
	// System.Web.Services.Description.SoapBindingStyle System.Web.Services.Description.SoapOperationBinding::style
	int32_t ___style_2;

public:
	inline static int32_t get_offset_of_soapAction_1() { return static_cast<int32_t>(offsetof(SoapOperationBinding_t3306743905, ___soapAction_1)); }
	inline String_t* get_soapAction_1() const { return ___soapAction_1; }
	inline String_t** get_address_of_soapAction_1() { return &___soapAction_1; }
	inline void set_soapAction_1(String_t* value)
	{
		___soapAction_1 = value;
		Il2CppCodeGenWriteBarrier(&___soapAction_1, value);
	}

	inline static int32_t get_offset_of_style_2() { return static_cast<int32_t>(offsetof(SoapOperationBinding_t3306743905, ___style_2)); }
	inline int32_t get_style_2() const { return ___style_2; }
	inline int32_t* get_address_of_style_2() { return &___style_2; }
	inline void set_style_2(int32_t value)
	{
		___style_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
