﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Emitter942443340.h"

// System.IO.TextWriter
struct TextWriter_t3478189236;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.TextEmitter
struct  TextEmitter_t2742298062  : public Emitter_t942443340
{
public:
	// System.IO.TextWriter Mono.Xml.Xsl.TextEmitter::writer
	TextWriter_t3478189236 * ___writer_0;

public:
	inline static int32_t get_offset_of_writer_0() { return static_cast<int32_t>(offsetof(TextEmitter_t2742298062, ___writer_0)); }
	inline TextWriter_t3478189236 * get_writer_0() const { return ___writer_0; }
	inline TextWriter_t3478189236 ** get_address_of_writer_0() { return &___writer_0; }
	inline void set_writer_0(TextWriter_t3478189236 * value)
	{
		___writer_0 = value;
		Il2CppCodeGenWriteBarrier(&___writer_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
