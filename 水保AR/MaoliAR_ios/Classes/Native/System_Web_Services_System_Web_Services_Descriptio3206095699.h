﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Serialization_XmlSerializatio982275218.h"

// System.String[]
struct StringU5BU5D_t1281789340;
// System.Int64[]
struct Int64U5BU5D_t2559172825;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.WebReferenceOptionsWriter
struct  WebReferenceOptionsWriter_t3206095699  : public XmlSerializationWriter_t982275218
{
public:

public:
};

struct WebReferenceOptionsWriter_t3206095699_StaticFields
{
public:
	// System.String[] System.Web.Services.Description.WebReferenceOptionsWriter::_xmlNamesCodeGenerationOptions
	StringU5BU5D_t1281789340* ____xmlNamesCodeGenerationOptions_8;
	// System.Int64[] System.Web.Services.Description.WebReferenceOptionsWriter::_valuesCodeGenerationOptions
	Int64U5BU5D_t2559172825* ____valuesCodeGenerationOptions_9;

public:
	inline static int32_t get_offset_of__xmlNamesCodeGenerationOptions_8() { return static_cast<int32_t>(offsetof(WebReferenceOptionsWriter_t3206095699_StaticFields, ____xmlNamesCodeGenerationOptions_8)); }
	inline StringU5BU5D_t1281789340* get__xmlNamesCodeGenerationOptions_8() const { return ____xmlNamesCodeGenerationOptions_8; }
	inline StringU5BU5D_t1281789340** get_address_of__xmlNamesCodeGenerationOptions_8() { return &____xmlNamesCodeGenerationOptions_8; }
	inline void set__xmlNamesCodeGenerationOptions_8(StringU5BU5D_t1281789340* value)
	{
		____xmlNamesCodeGenerationOptions_8 = value;
		Il2CppCodeGenWriteBarrier(&____xmlNamesCodeGenerationOptions_8, value);
	}

	inline static int32_t get_offset_of__valuesCodeGenerationOptions_9() { return static_cast<int32_t>(offsetof(WebReferenceOptionsWriter_t3206095699_StaticFields, ____valuesCodeGenerationOptions_9)); }
	inline Int64U5BU5D_t2559172825* get__valuesCodeGenerationOptions_9() const { return ____valuesCodeGenerationOptions_9; }
	inline Int64U5BU5D_t2559172825** get_address_of__valuesCodeGenerationOptions_9() { return &____valuesCodeGenerationOptions_9; }
	inline void set__valuesCodeGenerationOptions_9(Int64U5BU5D_t2559172825* value)
	{
		____valuesCodeGenerationOptions_9 = value;
		Il2CppCodeGenWriteBarrier(&____valuesCodeGenerationOptions_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
