﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// Level_Controller
struct Level_Controller_t1433348427;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// Vuforia.ImageTargetBehaviour
struct ImageTargetBehaviour_t2200418350;
// System.Predicate`1<UnityEngine.UI.Text>
struct Predicate_1_t2727176838;
// System.Predicate`1<UnityEngine.UI.Button>
struct Predicate_1_t585359297;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Level_Controller
struct  Level_Controller_t1433348427  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text Level_Controller::請點擊任意處以繼續遊戲
	Text_t1901882714 * ___U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_3;
	// System.Boolean Level_Controller::isPostLevel
	bool ___isPostLevel_4;
	// System.Boolean Level_Controller::completed
	bool ___completed_5;
	// System.Boolean Level_Controller::leveling
	bool ___leveling_6;
	// UnityEngine.UI.Button Level_Controller::QUIT_Button
	Button_t4055032469 * ___QUIT_Button_7;
	// UnityEngine.GameObject[] Level_Controller::All_Images
	GameObjectU5BU5D_t3328599146* ___All_Images_8;
	// UnityEngine.GameObject Level_Controller::gray
	GameObject_t1113636619 * ___gray_9;
	// UnityEngine.GameObject Level_Controller::bt_close
	GameObject_t1113636619 * ___bt_close_10;
	// Vuforia.ImageTargetBehaviour Level_Controller::levelMeshsTarget
	ImageTargetBehaviour_t2200418350 * ___levelMeshsTarget_11;

public:
	inline static int32_t get_offset_of_U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_3() { return static_cast<int32_t>(offsetof(Level_Controller_t1433348427, ___U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_3)); }
	inline Text_t1901882714 * get_U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_3() const { return ___U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_3; }
	inline Text_t1901882714 ** get_address_of_U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_3() { return &___U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_3; }
	inline void set_U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_3(Text_t1901882714 * value)
	{
		___U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_3 = value;
		Il2CppCodeGenWriteBarrier(&___U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_3, value);
	}

	inline static int32_t get_offset_of_isPostLevel_4() { return static_cast<int32_t>(offsetof(Level_Controller_t1433348427, ___isPostLevel_4)); }
	inline bool get_isPostLevel_4() const { return ___isPostLevel_4; }
	inline bool* get_address_of_isPostLevel_4() { return &___isPostLevel_4; }
	inline void set_isPostLevel_4(bool value)
	{
		___isPostLevel_4 = value;
	}

	inline static int32_t get_offset_of_completed_5() { return static_cast<int32_t>(offsetof(Level_Controller_t1433348427, ___completed_5)); }
	inline bool get_completed_5() const { return ___completed_5; }
	inline bool* get_address_of_completed_5() { return &___completed_5; }
	inline void set_completed_5(bool value)
	{
		___completed_5 = value;
	}

	inline static int32_t get_offset_of_leveling_6() { return static_cast<int32_t>(offsetof(Level_Controller_t1433348427, ___leveling_6)); }
	inline bool get_leveling_6() const { return ___leveling_6; }
	inline bool* get_address_of_leveling_6() { return &___leveling_6; }
	inline void set_leveling_6(bool value)
	{
		___leveling_6 = value;
	}

	inline static int32_t get_offset_of_QUIT_Button_7() { return static_cast<int32_t>(offsetof(Level_Controller_t1433348427, ___QUIT_Button_7)); }
	inline Button_t4055032469 * get_QUIT_Button_7() const { return ___QUIT_Button_7; }
	inline Button_t4055032469 ** get_address_of_QUIT_Button_7() { return &___QUIT_Button_7; }
	inline void set_QUIT_Button_7(Button_t4055032469 * value)
	{
		___QUIT_Button_7 = value;
		Il2CppCodeGenWriteBarrier(&___QUIT_Button_7, value);
	}

	inline static int32_t get_offset_of_All_Images_8() { return static_cast<int32_t>(offsetof(Level_Controller_t1433348427, ___All_Images_8)); }
	inline GameObjectU5BU5D_t3328599146* get_All_Images_8() const { return ___All_Images_8; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_All_Images_8() { return &___All_Images_8; }
	inline void set_All_Images_8(GameObjectU5BU5D_t3328599146* value)
	{
		___All_Images_8 = value;
		Il2CppCodeGenWriteBarrier(&___All_Images_8, value);
	}

	inline static int32_t get_offset_of_gray_9() { return static_cast<int32_t>(offsetof(Level_Controller_t1433348427, ___gray_9)); }
	inline GameObject_t1113636619 * get_gray_9() const { return ___gray_9; }
	inline GameObject_t1113636619 ** get_address_of_gray_9() { return &___gray_9; }
	inline void set_gray_9(GameObject_t1113636619 * value)
	{
		___gray_9 = value;
		Il2CppCodeGenWriteBarrier(&___gray_9, value);
	}

	inline static int32_t get_offset_of_bt_close_10() { return static_cast<int32_t>(offsetof(Level_Controller_t1433348427, ___bt_close_10)); }
	inline GameObject_t1113636619 * get_bt_close_10() const { return ___bt_close_10; }
	inline GameObject_t1113636619 ** get_address_of_bt_close_10() { return &___bt_close_10; }
	inline void set_bt_close_10(GameObject_t1113636619 * value)
	{
		___bt_close_10 = value;
		Il2CppCodeGenWriteBarrier(&___bt_close_10, value);
	}

	inline static int32_t get_offset_of_levelMeshsTarget_11() { return static_cast<int32_t>(offsetof(Level_Controller_t1433348427, ___levelMeshsTarget_11)); }
	inline ImageTargetBehaviour_t2200418350 * get_levelMeshsTarget_11() const { return ___levelMeshsTarget_11; }
	inline ImageTargetBehaviour_t2200418350 ** get_address_of_levelMeshsTarget_11() { return &___levelMeshsTarget_11; }
	inline void set_levelMeshsTarget_11(ImageTargetBehaviour_t2200418350 * value)
	{
		___levelMeshsTarget_11 = value;
		Il2CppCodeGenWriteBarrier(&___levelMeshsTarget_11, value);
	}
};

struct Level_Controller_t1433348427_StaticFields
{
public:
	// Level_Controller Level_Controller::This
	Level_Controller_t1433348427 * ___This_2;
	// System.Predicate`1<UnityEngine.UI.Text> Level_Controller::<>f__am$cache0
	Predicate_1_t2727176838 * ___U3CU3Ef__amU24cache0_12;
	// System.Predicate`1<UnityEngine.UI.Button> Level_Controller::<>f__am$cache1
	Predicate_1_t585359297 * ___U3CU3Ef__amU24cache1_13;
	// System.Predicate`1<UnityEngine.UI.Text> Level_Controller::<>f__am$cache2
	Predicate_1_t2727176838 * ___U3CU3Ef__amU24cache2_14;

public:
	inline static int32_t get_offset_of_This_2() { return static_cast<int32_t>(offsetof(Level_Controller_t1433348427_StaticFields, ___This_2)); }
	inline Level_Controller_t1433348427 * get_This_2() const { return ___This_2; }
	inline Level_Controller_t1433348427 ** get_address_of_This_2() { return &___This_2; }
	inline void set_This_2(Level_Controller_t1433348427 * value)
	{
		___This_2 = value;
		Il2CppCodeGenWriteBarrier(&___This_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_12() { return static_cast<int32_t>(offsetof(Level_Controller_t1433348427_StaticFields, ___U3CU3Ef__amU24cache0_12)); }
	inline Predicate_1_t2727176838 * get_U3CU3Ef__amU24cache0_12() const { return ___U3CU3Ef__amU24cache0_12; }
	inline Predicate_1_t2727176838 ** get_address_of_U3CU3Ef__amU24cache0_12() { return &___U3CU3Ef__amU24cache0_12; }
	inline void set_U3CU3Ef__amU24cache0_12(Predicate_1_t2727176838 * value)
	{
		___U3CU3Ef__amU24cache0_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_13() { return static_cast<int32_t>(offsetof(Level_Controller_t1433348427_StaticFields, ___U3CU3Ef__amU24cache1_13)); }
	inline Predicate_1_t585359297 * get_U3CU3Ef__amU24cache1_13() const { return ___U3CU3Ef__amU24cache1_13; }
	inline Predicate_1_t585359297 ** get_address_of_U3CU3Ef__amU24cache1_13() { return &___U3CU3Ef__amU24cache1_13; }
	inline void set_U3CU3Ef__amU24cache1_13(Predicate_1_t585359297 * value)
	{
		___U3CU3Ef__amU24cache1_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_14() { return static_cast<int32_t>(offsetof(Level_Controller_t1433348427_StaticFields, ___U3CU3Ef__amU24cache2_14)); }
	inline Predicate_1_t2727176838 * get_U3CU3Ef__amU24cache2_14() const { return ___U3CU3Ef__amU24cache2_14; }
	inline Predicate_1_t2727176838 ** get_address_of_U3CU3Ef__amU24cache2_14() { return &___U3CU3Ef__amU24cache2_14; }
	inline void set_U3CU3Ef__amU24cache2_14(Predicate_1_t2727176838 * value)
	{
		___U3CU3Ef__amU24cache2_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
