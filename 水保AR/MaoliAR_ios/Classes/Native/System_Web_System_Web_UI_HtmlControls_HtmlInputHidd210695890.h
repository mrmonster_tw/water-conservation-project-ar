﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_HtmlControls_HtmlInputCon1839583126.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.HtmlControls.HtmlInputHidden
struct  HtmlInputHidden_t210695890  : public HtmlInputControl_t1839583126
{
public:

public:
};

struct HtmlInputHidden_t210695890_StaticFields
{
public:
	// System.Object System.Web.UI.HtmlControls.HtmlInputHidden::ServerChangeEvent
	Il2CppObject * ___ServerChangeEvent_30;

public:
	inline static int32_t get_offset_of_ServerChangeEvent_30() { return static_cast<int32_t>(offsetof(HtmlInputHidden_t210695890_StaticFields, ___ServerChangeEvent_30)); }
	inline Il2CppObject * get_ServerChangeEvent_30() const { return ___ServerChangeEvent_30; }
	inline Il2CppObject ** get_address_of_ServerChangeEvent_30() { return &___ServerChangeEvent_30; }
	inline void set_ServerChangeEvent_30(Il2CppObject * value)
	{
		___ServerChangeEvent_30 = value;
		Il2CppCodeGenWriteBarrier(&___ServerChangeEvent_30, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
