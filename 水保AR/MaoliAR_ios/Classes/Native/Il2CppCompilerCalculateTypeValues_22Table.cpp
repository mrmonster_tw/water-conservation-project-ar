﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_CodeDom_CodeTypeDeclaration2359234283.h"
#include "System_System_CodeDom_CodeTypeDelegate2896700083.h"
#include "System_System_CodeDom_CodeTypeMemberCollection1296205520.h"
#include "System_System_CodeDom_CodeTypeMember1555525554.h"
#include "System_System_CodeDom_CodeTypeOfExpression2266700964.h"
#include "System_System_CodeDom_CodeTypeParameterCollection1882362546.h"
#include "System_System_CodeDom_CodeTypeParameter3345688804.h"
#include "System_System_CodeDom_CodeTypeReferenceCollection3857551471.h"
#include "System_System_CodeDom_CodeTypeReference3809997434.h"
#include "System_System_CodeDom_CodeTypeReferenceExpression793901702.h"
#include "System_System_CodeDom_CodeTypeReferenceOptions661023944.h"
#include "System_System_CodeDom_CodeVariableDeclarationState1840202122.h"
#include "System_System_CodeDom_CodeVariableReferenceExpress2242020292.h"
#include "System_System_CodeDom_Compiler_CodeDomConfiguratio2657043416.h"
#include "System_System_CodeDom_Compiler_CodeDomProvider110349836.h"
#include "System_System_CodeDom_Compiler_CodeGenerator1606279797.h"
#include "System_System_CodeDom_Compiler_CodeGenerator_Visit4039484693.h"
#include "System_System_CodeDom_Compiler_CodeGeneratorOption1601321324.h"
#include "System_System_CodeDom_Compiler_Compiler2467443999.h"
#include "System_System_CodeDom_Compiler_CompilerCollection2531538163.h"
#include "System_System_CodeDom_Compiler_CompilerErrorCollec2383307460.h"
#include "System_System_CodeDom_Compiler_CompilerError2283741458.h"
#include "System_System_CodeDom_Compiler_CompilerInfo947794800.h"
#include "System_System_CodeDom_Compiler_CompilerParameters2325000967.h"
#include "System_System_CodeDom_Compiler_CompilerProviderOpt2418514121.h"
#include "System_System_CodeDom_Compiler_CompilerProviderOpt3548446607.h"
#include "System_System_CodeDom_Compiler_CompilerResults1736487784.h"
#include "System_System_CodeDom_Compiler_GeneratedCodeAttrib1133842240.h"
#include "System_System_CodeDom_Compiler_GeneratorSupport1922728367.h"
#include "System_System_CodeDom_Compiler_IndentedTextWriter2995409896.h"
#include "System_System_CodeDom_Compiler_LanguageOptions700284153.h"
#include "System_System_CodeDom_Compiler_TempFileCollection2060973068.h"
#include "System_System_CodeDom_FieldDirection1550335646.h"
#include "System_System_CodeDom_MemberAttributes1258384723.h"
#include "System_System_Collections_Generic_RBTree4095273678.h"
#include "System_System_Collections_Generic_RBTree_Node2057664286.h"
#include "System_System_Collections_Generic_RBTree_NodeEnume2183475207.h"
#include "System_System_Collections_Specialized_HybridDictio4070033136.h"
#include "System_System_Collections_Specialized_ListDictiona1624492310.h"
#include "System_System_Collections_Specialized_ListDictionar417719465.h"
#include "System_System_Collections_Specialized_ListDictiona1673829610.h"
#include "System_System_Collections_Specialized_ListDictionar819283804.h"
#include "System_System_Collections_Specialized_ListDictiona2863182637.h"
#include "System_System_Collections_Specialized_NameObjectCo2091847364.h"
#include "System_System_Collections_Specialized_NameObjectCo2272350267.h"
#include "System_System_Collections_Specialized_NameObjectCo4246666432.h"
#include "System_System_Collections_Specialized_NameObjectCo1318642398.h"
#include "System_System_Collections_Specialized_NameValueColl407452768.h"
#include "System_System_Collections_Specialized_OrderedDicti2617496293.h"
#include "System_System_Collections_Specialized_OrderedDicti1171499853.h"
#include "System_System_Collections_Specialized_OrderedDicti2804082187.h"
#include "System_System_Collections_Specialized_OrderedDicti3799789192.h"
#include "System_System_Collections_Specialized_ProcessStrin2107791454.h"
#include "System_System_Collections_Specialized_StringCollect167406615.h"
#include "System_System_Collections_Specialized_StringDiction120437468.h"
#include "System_System_Collections_Specialized_StringEnumer3934291441.h"
#include "System_System_ComponentModel_ArrayConverter1750795769.h"
#include "System_System_ComponentModel_ArrayConverter_ArrayPr157990669.h"
#include "System_System_ComponentModel_AsyncCompletedEventAr1863481821.h"
#include "System_System_ComponentModel_AttributeCollection4221220734.h"
#include "System_System_ComponentModel_BaseNumberConverter312147029.h"
#include "System_System_ComponentModel_BindableAttribute3255577478.h"
#include "System_System_ComponentModel_BindableSupport3743993600.h"
#include "System_System_ComponentModel_BindingDirection2741897897.h"
#include "System_System_ComponentModel_BooleanConverter941538927.h"
#include "System_System_ComponentModel_BrowsableAttribute3407396667.h"
#include "System_System_ComponentModel_ByteConverter1408362843.h"
#include "System_System_ComponentModel_CategoryAttribute39585132.h"
#include "System_System_ComponentModel_CharConverter747842913.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (CodeTypeDeclaration_t2359234283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2200[9] = 
{
	CodeTypeDeclaration_t2359234283::get_offset_of_baseTypes_8(),
	CodeTypeDeclaration_t2359234283::get_offset_of_members_9(),
	CodeTypeDeclaration_t2359234283::get_offset_of_attributes_10(),
	CodeTypeDeclaration_t2359234283::get_offset_of_isEnum_11(),
	CodeTypeDeclaration_t2359234283::get_offset_of_isStruct_12(),
	CodeTypeDeclaration_t2359234283::get_offset_of_isPartial_13(),
	CodeTypeDeclaration_t2359234283::get_offset_of_typeParameters_14(),
	CodeTypeDeclaration_t2359234283::get_offset_of_PopulateBaseTypes_15(),
	CodeTypeDeclaration_t2359234283::get_offset_of_PopulateMembers_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (CodeTypeDelegate_t2896700083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2201[2] = 
{
	CodeTypeDelegate_t2896700083::get_offset_of_parameters_17(),
	CodeTypeDelegate_t2896700083::get_offset_of_returnType_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (CodeTypeMemberCollection_t1296205520), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (CodeTypeMember_t1555525554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2203[7] = 
{
	CodeTypeMember_t1555525554::get_offset_of_name_1(),
	CodeTypeMember_t1555525554::get_offset_of_attributes_2(),
	CodeTypeMember_t1555525554::get_offset_of_comments_3(),
	CodeTypeMember_t1555525554::get_offset_of_customAttributes_4(),
	CodeTypeMember_t1555525554::get_offset_of_linePragma_5(),
	CodeTypeMember_t1555525554::get_offset_of_endDirectives_6(),
	CodeTypeMember_t1555525554::get_offset_of_startDirectives_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (CodeTypeOfExpression_t2266700964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2204[1] = 
{
	CodeTypeOfExpression_t2266700964::get_offset_of_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (CodeTypeParameterCollection_t1882362546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (CodeTypeParameter_t3345688804), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2206[3] = 
{
	CodeTypeParameter_t3345688804::get_offset_of_constraints_1(),
	CodeTypeParameter_t3345688804::get_offset_of_hasConstructorConstraint_2(),
	CodeTypeParameter_t3345688804::get_offset_of_name_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (CodeTypeReferenceCollection_t3857551471), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (CodeTypeReference_t3809997434), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2208[6] = 
{
	CodeTypeReference_t3809997434::get_offset_of_baseType_1(),
	CodeTypeReference_t3809997434::get_offset_of_arrayElementType_2(),
	CodeTypeReference_t3809997434::get_offset_of_arrayRank_3(),
	CodeTypeReference_t3809997434::get_offset_of_isInterface_4(),
	CodeTypeReference_t3809997434::get_offset_of_typeArguments_5(),
	CodeTypeReference_t3809997434::get_offset_of_referenceOptions_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (CodeTypeReferenceExpression_t793901702), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2209[1] = 
{
	CodeTypeReferenceExpression_t793901702::get_offset_of_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (CodeTypeReferenceOptions_t661023944)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2210[3] = 
{
	CodeTypeReferenceOptions_t661023944::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (CodeVariableDeclarationStatement_t1840202122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2211[3] = 
{
	CodeVariableDeclarationStatement_t1840202122::get_offset_of_initExpression_4(),
	CodeVariableDeclarationStatement_t1840202122::get_offset_of_type_5(),
	CodeVariableDeclarationStatement_t1840202122::get_offset_of_name_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (CodeVariableReferenceExpression_t2242020292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2212[1] = 
{
	CodeVariableReferenceExpression_t2242020292::get_offset_of_variableName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (CodeDomConfigurationHandler_t2657043416), -1, sizeof(CodeDomConfigurationHandler_t2657043416_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2214[3] = 
{
	CodeDomConfigurationHandler_t2657043416_StaticFields::get_offset_of_properties_17(),
	CodeDomConfigurationHandler_t2657043416_StaticFields::get_offset_of_compilersProp_18(),
	CodeDomConfigurationHandler_t2657043416_StaticFields::get_offset_of_default_compilers_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (CodeDomProvider_t110349836), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (CodeGenerator_t1606279797), -1, sizeof(CodeGenerator_t1606279797_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2216[6] = 
{
	CodeGenerator_t1606279797::get_offset_of_output_0(),
	CodeGenerator_t1606279797::get_offset_of_options_1(),
	CodeGenerator_t1606279797::get_offset_of_currentMember_2(),
	CodeGenerator_t1606279797::get_offset_of_currentType_3(),
	CodeGenerator_t1606279797::get_offset_of_visitor_4(),
	CodeGenerator_t1606279797_StaticFields::get_offset_of_memberTypes_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (Visitor_t4039484693), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2217[1] = 
{
	Visitor_t4039484693::get_offset_of_g_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (CodeGeneratorOptions_t1601321324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2218[1] = 
{
	CodeGeneratorOptions_t1601321324::get_offset_of_properties_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (Compiler_t2467443999), -1, sizeof(Compiler_t2467443999_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2219[7] = 
{
	Compiler_t2467443999_StaticFields::get_offset_of_compilerOptionsProp_13(),
	Compiler_t2467443999_StaticFields::get_offset_of_extensionProp_14(),
	Compiler_t2467443999_StaticFields::get_offset_of_languageProp_15(),
	Compiler_t2467443999_StaticFields::get_offset_of_typeProp_16(),
	Compiler_t2467443999_StaticFields::get_offset_of_warningLevelProp_17(),
	Compiler_t2467443999_StaticFields::get_offset_of_providerOptionsProp_18(),
	Compiler_t2467443999_StaticFields::get_offset_of_properties_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (CompilerCollection_t2531538163), -1, sizeof(CompilerCollection_t2531538163_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2220[4] = 
{
	CompilerCollection_t2531538163_StaticFields::get_offset_of_properties_23(),
	CompilerCollection_t2531538163_StaticFields::get_offset_of_compiler_infos_24(),
	CompilerCollection_t2531538163_StaticFields::get_offset_of_compiler_languages_25(),
	CompilerCollection_t2531538163_StaticFields::get_offset_of_compiler_extensions_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (CompilerErrorCollection_t2383307460), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (CompilerError_t2283741458), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2222[6] = 
{
	CompilerError_t2283741458::get_offset_of_fileName_0(),
	CompilerError_t2283741458::get_offset_of_line_1(),
	CompilerError_t2283741458::get_offset_of_column_2(),
	CompilerError_t2283741458::get_offset_of_errorNumber_3(),
	CompilerError_t2283741458::get_offset_of_errorText_4(),
	CompilerError_t2283741458::get_offset_of_isWarning_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (CompilerInfo_t947794800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2223[8] = 
{
	CompilerInfo_t947794800::get_offset_of_Languages_0(),
	CompilerInfo_t947794800::get_offset_of_Extensions_1(),
	CompilerInfo_t947794800::get_offset_of_TypeName_2(),
	CompilerInfo_t947794800::get_offset_of_WarningLevel_3(),
	CompilerInfo_t947794800::get_offset_of_CompilerOptions_4(),
	CompilerInfo_t947794800::get_offset_of_ProviderOptions_5(),
	CompilerInfo_t947794800::get_offset_of_inited_6(),
	CompilerInfo_t947794800::get_offset_of_type_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (CompilerParameters_t2325000967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2224[14] = 
{
	CompilerParameters_t2325000967::get_offset_of_compilerOptions_0(),
	CompilerParameters_t2325000967::get_offset_of_evidence_1(),
	CompilerParameters_t2325000967::get_offset_of_generateExecutable_2(),
	CompilerParameters_t2325000967::get_offset_of_generateInMemory_3(),
	CompilerParameters_t2325000967::get_offset_of_includeDebugInformation_4(),
	CompilerParameters_t2325000967::get_offset_of_outputAssembly_5(),
	CompilerParameters_t2325000967::get_offset_of_referencedAssemblies_6(),
	CompilerParameters_t2325000967::get_offset_of_tempFiles_7(),
	CompilerParameters_t2325000967::get_offset_of_treatWarningsAsErrors_8(),
	CompilerParameters_t2325000967::get_offset_of_userToken_9(),
	CompilerParameters_t2325000967::get_offset_of_warningLevel_10(),
	CompilerParameters_t2325000967::get_offset_of_win32Resource_11(),
	CompilerParameters_t2325000967::get_offset_of_embedded_resources_12(),
	CompilerParameters_t2325000967::get_offset_of_linked_resources_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (CompilerProviderOption_t2418514121), -1, sizeof(CompilerProviderOption_t2418514121_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2225[3] = 
{
	CompilerProviderOption_t2418514121_StaticFields::get_offset_of_nameProp_13(),
	CompilerProviderOption_t2418514121_StaticFields::get_offset_of_valueProp_14(),
	CompilerProviderOption_t2418514121_StaticFields::get_offset_of_properties_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (CompilerProviderOptionsCollection_t3548446607), -1, sizeof(CompilerProviderOptionsCollection_t3548446607_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2226[1] = 
{
	CompilerProviderOptionsCollection_t3548446607_StaticFields::get_offset_of_properties_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (CompilerResults_t1736487784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2227[6] = 
{
	CompilerResults_t1736487784::get_offset_of_compiledAssembly_0(),
	CompilerResults_t1736487784::get_offset_of_errors_1(),
	CompilerResults_t1736487784::get_offset_of_nativeCompilerReturnValue_2(),
	CompilerResults_t1736487784::get_offset_of_output_3(),
	CompilerResults_t1736487784::get_offset_of_pathToAssembly_4(),
	CompilerResults_t1736487784::get_offset_of_tempFiles_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (GeneratedCodeAttribute_t1133842240), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2228[2] = 
{
	GeneratedCodeAttribute_t1133842240::get_offset_of_tool_0(),
	GeneratedCodeAttribute_t1133842240::get_offset_of_version_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (GeneratorSupport_t1922728367)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2229[27] = 
{
	GeneratorSupport_t1922728367::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (IndentedTextWriter_t2995409896), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2232[4] = 
{
	IndentedTextWriter_t2995409896::get_offset_of_writer_4(),
	IndentedTextWriter_t2995409896::get_offset_of_tabString_5(),
	IndentedTextWriter_t2995409896::get_offset_of_indent_6(),
	IndentedTextWriter_t2995409896::get_offset_of_newline_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (LanguageOptions_t700284153)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2233[3] = 
{
	LanguageOptions_t700284153::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (TempFileCollection_t2060973068), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2234[6] = 
{
	TempFileCollection_t2060973068::get_offset_of_filehash_0(),
	TempFileCollection_t2060973068::get_offset_of_tempdir_1(),
	TempFileCollection_t2060973068::get_offset_of_keepfiles_2(),
	TempFileCollection_t2060973068::get_offset_of_basepath_3(),
	TempFileCollection_t2060973068::get_offset_of_rnd_4(),
	TempFileCollection_t2060973068::get_offset_of_ownTempDir_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (FieldDirection_t1550335646)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2235[4] = 
{
	FieldDirection_t1550335646::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (MemberAttributes_t1258384723)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2236[17] = 
{
	MemberAttributes_t1258384723::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2237[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2238[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2239[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (RBTree_t4095273678), -1, 0, sizeof(RBTree_t4095273678_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable2240[4] = 
{
	RBTree_t4095273678::get_offset_of_root_0(),
	RBTree_t4095273678::get_offset_of_hlp_1(),
	RBTree_t4095273678::get_offset_of_version_2(),
	THREAD_STATIC_FIELD_OFFSET,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (Node_t2057664286), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2242[3] = 
{
	Node_t2057664286::get_offset_of_left_0(),
	Node_t2057664286::get_offset_of_right_1(),
	Node_t2057664286::get_offset_of_size_black_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (NodeEnumerator_t2183475207)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2243[3] = 
{
	NodeEnumerator_t2183475207::get_offset_of_tree_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NodeEnumerator_t2183475207::get_offset_of_version_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NodeEnumerator_t2183475207::get_offset_of_pennants_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2244[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2245[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2246[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2247[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2248[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2249[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2250[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2251[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2252[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2253[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2254[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2255[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2256[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2257[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2258[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2259[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2260[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2261[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2262[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2263[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2264[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2265[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2266[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (HybridDictionary_t4070033136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2267[3] = 
{
	HybridDictionary_t4070033136::get_offset_of_caseInsensitive_0(),
	HybridDictionary_t4070033136::get_offset_of_hashtable_1(),
	HybridDictionary_t4070033136::get_offset_of_list_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (ListDictionary_t1624492310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2269[4] = 
{
	ListDictionary_t1624492310::get_offset_of_count_0(),
	ListDictionary_t1624492310::get_offset_of_version_1(),
	ListDictionary_t1624492310::get_offset_of_head_2(),
	ListDictionary_t1624492310::get_offset_of_comparer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (DictionaryNode_t417719465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2270[3] = 
{
	DictionaryNode_t417719465::get_offset_of_key_0(),
	DictionaryNode_t417719465::get_offset_of_value_1(),
	DictionaryNode_t417719465::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (DictionaryNodeEnumerator_t1673829610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2271[4] = 
{
	DictionaryNodeEnumerator_t1673829610::get_offset_of_dict_0(),
	DictionaryNodeEnumerator_t1673829610::get_offset_of_isAtStart_1(),
	DictionaryNodeEnumerator_t1673829610::get_offset_of_current_2(),
	DictionaryNodeEnumerator_t1673829610::get_offset_of_version_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (DictionaryNodeCollection_t819283804), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2272[2] = 
{
	DictionaryNodeCollection_t819283804::get_offset_of_dict_0(),
	DictionaryNodeCollection_t819283804::get_offset_of_isKeyList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (DictionaryNodeCollectionEnumerator_t2863182637), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2273[2] = 
{
	DictionaryNodeCollectionEnumerator_t2863182637::get_offset_of_inner_0(),
	DictionaryNodeCollectionEnumerator_t2863182637::get_offset_of_isKeyList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (NameObjectCollectionBase_t2091847364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2274[10] = 
{
	NameObjectCollectionBase_t2091847364::get_offset_of_m_ItemsContainer_0(),
	NameObjectCollectionBase_t2091847364::get_offset_of_m_NullKeyItem_1(),
	NameObjectCollectionBase_t2091847364::get_offset_of_m_ItemsArray_2(),
	NameObjectCollectionBase_t2091847364::get_offset_of_m_hashprovider_3(),
	NameObjectCollectionBase_t2091847364::get_offset_of_m_comparer_4(),
	NameObjectCollectionBase_t2091847364::get_offset_of_m_defCapacity_5(),
	NameObjectCollectionBase_t2091847364::get_offset_of_m_readonly_6(),
	NameObjectCollectionBase_t2091847364::get_offset_of_infoCopy_7(),
	NameObjectCollectionBase_t2091847364::get_offset_of_keyscoll_8(),
	NameObjectCollectionBase_t2091847364::get_offset_of_equality_comparer_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (_Item_t2272350267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2275[2] = 
{
	_Item_t2272350267::get_offset_of_key_0(),
	_Item_t2272350267::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (_KeysEnumerator_t4246666432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2276[2] = 
{
	_KeysEnumerator_t4246666432::get_offset_of_m_collection_0(),
	_KeysEnumerator_t4246666432::get_offset_of_m_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (KeysCollection_t1318642398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2277[1] = 
{
	KeysCollection_t1318642398::get_offset_of_m_collection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (NameValueCollection_t407452768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2278[2] = 
{
	NameValueCollection_t407452768::get_offset_of_cachedAllKeys_10(),
	NameValueCollection_t407452768::get_offset_of_cachedAll_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (OrderedDictionary_t2617496293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2279[6] = 
{
	OrderedDictionary_t2617496293::get_offset_of_list_0(),
	OrderedDictionary_t2617496293::get_offset_of_hash_1(),
	OrderedDictionary_t2617496293::get_offset_of_readOnly_2(),
	OrderedDictionary_t2617496293::get_offset_of_initialCapacity_3(),
	OrderedDictionary_t2617496293::get_offset_of_serializationInfo_4(),
	OrderedDictionary_t2617496293::get_offset_of_comparer_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (OrderedEntryCollectionEnumerator_t1171499853), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2280[1] = 
{
	OrderedEntryCollectionEnumerator_t1171499853::get_offset_of_listEnumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (OrderedCollection_t2804082187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2281[2] = 
{
	OrderedCollection_t2804082187::get_offset_of_list_0(),
	OrderedCollection_t2804082187::get_offset_of_isKeyList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (OrderedCollectionEnumerator_t3799789192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2282[2] = 
{
	OrderedCollectionEnumerator_t3799789192::get_offset_of_isKeyList_0(),
	OrderedCollectionEnumerator_t3799789192::get_offset_of_listEnumerator_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (ProcessStringDictionary_t2107791454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2283[1] = 
{
	ProcessStringDictionary_t2107791454::get_offset_of_table_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (StringCollection_t167406615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2284[1] = 
{
	StringCollection_t167406615::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (StringDictionary_t120437468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2285[1] = 
{
	StringDictionary_t120437468::get_offset_of_contents_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (StringEnumerator_t3934291441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2286[1] = 
{
	StringEnumerator_t3934291441::get_offset_of_enumerable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (ArrayConverter_t1750795769), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (ArrayPropertyDescriptor_t157990669), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2288[2] = 
{
	ArrayPropertyDescriptor_t157990669::get_offset_of_index_4(),
	ArrayPropertyDescriptor_t157990669::get_offset_of_array_type_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (AsyncCompletedEventArgs_t1863481821), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (AttributeCollection_t4221220734), -1, sizeof(AttributeCollection_t4221220734_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2290[2] = 
{
	AttributeCollection_t4221220734::get_offset_of_attrList_0(),
	AttributeCollection_t4221220734_StaticFields::get_offset_of_Empty_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (BaseNumberConverter_t312147029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2291[1] = 
{
	BaseNumberConverter_t312147029::get_offset_of_InnerType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (BindableAttribute_t3255577478), -1, sizeof(BindableAttribute_t3255577478_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2292[4] = 
{
	BindableAttribute_t3255577478::get_offset_of_bindable_0(),
	BindableAttribute_t3255577478_StaticFields::get_offset_of_No_1(),
	BindableAttribute_t3255577478_StaticFields::get_offset_of_Yes_2(),
	BindableAttribute_t3255577478_StaticFields::get_offset_of_Default_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (BindableSupport_t3743993600)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2293[4] = 
{
	BindableSupport_t3743993600::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (BindingDirection_t2741897897)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2294[3] = 
{
	BindingDirection_t2741897897::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (BooleanConverter_t941538927), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (BrowsableAttribute_t3407396667), -1, sizeof(BrowsableAttribute_t3407396667_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2296[4] = 
{
	BrowsableAttribute_t3407396667::get_offset_of_browsable_0(),
	BrowsableAttribute_t3407396667_StaticFields::get_offset_of_Default_1(),
	BrowsableAttribute_t3407396667_StaticFields::get_offset_of_No_2(),
	BrowsableAttribute_t3407396667_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (ByteConverter_t1408362843), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (CategoryAttribute_t39585132), -1, sizeof(CategoryAttribute_t39585132_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2298[4] = 
{
	CategoryAttribute_t39585132::get_offset_of_category_0(),
	CategoryAttribute_t39585132::get_offset_of_IsLocalized_1(),
	CategoryAttribute_t39585132_StaticFields::get_offset_of_def_2(),
	CategoryAttribute_t39585132_StaticFields::get_offset_of_lockobj_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (CharConverter_t747842913), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
