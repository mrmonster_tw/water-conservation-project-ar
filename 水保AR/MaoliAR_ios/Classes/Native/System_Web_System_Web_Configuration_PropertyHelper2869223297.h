﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Configuration.WhiteSpaceTrimStringConverter
struct WhiteSpaceTrimStringConverter_t4027530507;
// System.Configuration.InfiniteTimeSpanConverter
struct InfiniteTimeSpanConverter_t2957091789;
// System.Configuration.InfiniteIntConverter
struct InfiniteIntConverter_t2860508531;
// System.Configuration.TimeSpanMinutesConverter
struct TimeSpanMinutesConverter_t730211177;
// System.Configuration.TimeSpanSecondsOrInfiniteConverter
struct TimeSpanSecondsOrInfiniteConverter_t2792648515;
// System.Configuration.TimeSpanSecondsConverter
struct TimeSpanSecondsConverter_t1574829041;
// System.Configuration.CommaDelimitedStringCollectionConverter
struct CommaDelimitedStringCollectionConverter_t2027889819;
// System.Configuration.DefaultValidator
struct DefaultValidator_t1354184753;
// System.Configuration.NullableStringValidator
struct NullableStringValidator_t388666549;
// System.Configuration.PositiveTimeSpanValidator
struct PositiveTimeSpanValidator_t714847674;
// System.Configuration.TimeSpanMinutesOrInfiniteConverter
struct TimeSpanMinutesOrInfiniteConverter_t2683336721;
// System.Configuration.IntegerValidator
struct IntegerValidator_t780581956;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.PropertyHelper
struct  PropertyHelper_t2869223297  : public Il2CppObject
{
public:

public:
};

struct PropertyHelper_t2869223297_StaticFields
{
public:
	// System.Configuration.WhiteSpaceTrimStringConverter System.Web.Configuration.PropertyHelper::WhiteSpaceTrimStringConverter
	WhiteSpaceTrimStringConverter_t4027530507 * ___WhiteSpaceTrimStringConverter_0;
	// System.Configuration.InfiniteTimeSpanConverter System.Web.Configuration.PropertyHelper::InfiniteTimeSpanConverter
	InfiniteTimeSpanConverter_t2957091789 * ___InfiniteTimeSpanConverter_1;
	// System.Configuration.InfiniteIntConverter System.Web.Configuration.PropertyHelper::InfiniteIntConverter
	InfiniteIntConverter_t2860508531 * ___InfiniteIntConverter_2;
	// System.Configuration.TimeSpanMinutesConverter System.Web.Configuration.PropertyHelper::TimeSpanMinutesConverter
	TimeSpanMinutesConverter_t730211177 * ___TimeSpanMinutesConverter_3;
	// System.Configuration.TimeSpanSecondsOrInfiniteConverter System.Web.Configuration.PropertyHelper::TimeSpanSecondsOrInfiniteConverter
	TimeSpanSecondsOrInfiniteConverter_t2792648515 * ___TimeSpanSecondsOrInfiniteConverter_4;
	// System.Configuration.TimeSpanSecondsConverter System.Web.Configuration.PropertyHelper::TimeSpanSecondsConverter
	TimeSpanSecondsConverter_t1574829041 * ___TimeSpanSecondsConverter_5;
	// System.Configuration.CommaDelimitedStringCollectionConverter System.Web.Configuration.PropertyHelper::CommaDelimitedStringCollectionConverter
	CommaDelimitedStringCollectionConverter_t2027889819 * ___CommaDelimitedStringCollectionConverter_6;
	// System.Configuration.DefaultValidator System.Web.Configuration.PropertyHelper::DefaultValidator
	DefaultValidator_t1354184753 * ___DefaultValidator_7;
	// System.Configuration.NullableStringValidator System.Web.Configuration.PropertyHelper::NonEmptyStringValidator
	NullableStringValidator_t388666549 * ___NonEmptyStringValidator_8;
	// System.Configuration.PositiveTimeSpanValidator System.Web.Configuration.PropertyHelper::PositiveTimeSpanValidator
	PositiveTimeSpanValidator_t714847674 * ___PositiveTimeSpanValidator_9;
	// System.Configuration.TimeSpanMinutesOrInfiniteConverter System.Web.Configuration.PropertyHelper::TimeSpanMinutesOrInfiniteConverter
	TimeSpanMinutesOrInfiniteConverter_t2683336721 * ___TimeSpanMinutesOrInfiniteConverter_10;
	// System.Configuration.IntegerValidator System.Web.Configuration.PropertyHelper::IntFromZeroToMaxValidator
	IntegerValidator_t780581956 * ___IntFromZeroToMaxValidator_11;
	// System.Configuration.IntegerValidator System.Web.Configuration.PropertyHelper::IntFromOneToMax_1Validator
	IntegerValidator_t780581956 * ___IntFromOneToMax_1Validator_12;

public:
	inline static int32_t get_offset_of_WhiteSpaceTrimStringConverter_0() { return static_cast<int32_t>(offsetof(PropertyHelper_t2869223297_StaticFields, ___WhiteSpaceTrimStringConverter_0)); }
	inline WhiteSpaceTrimStringConverter_t4027530507 * get_WhiteSpaceTrimStringConverter_0() const { return ___WhiteSpaceTrimStringConverter_0; }
	inline WhiteSpaceTrimStringConverter_t4027530507 ** get_address_of_WhiteSpaceTrimStringConverter_0() { return &___WhiteSpaceTrimStringConverter_0; }
	inline void set_WhiteSpaceTrimStringConverter_0(WhiteSpaceTrimStringConverter_t4027530507 * value)
	{
		___WhiteSpaceTrimStringConverter_0 = value;
		Il2CppCodeGenWriteBarrier(&___WhiteSpaceTrimStringConverter_0, value);
	}

	inline static int32_t get_offset_of_InfiniteTimeSpanConverter_1() { return static_cast<int32_t>(offsetof(PropertyHelper_t2869223297_StaticFields, ___InfiniteTimeSpanConverter_1)); }
	inline InfiniteTimeSpanConverter_t2957091789 * get_InfiniteTimeSpanConverter_1() const { return ___InfiniteTimeSpanConverter_1; }
	inline InfiniteTimeSpanConverter_t2957091789 ** get_address_of_InfiniteTimeSpanConverter_1() { return &___InfiniteTimeSpanConverter_1; }
	inline void set_InfiniteTimeSpanConverter_1(InfiniteTimeSpanConverter_t2957091789 * value)
	{
		___InfiniteTimeSpanConverter_1 = value;
		Il2CppCodeGenWriteBarrier(&___InfiniteTimeSpanConverter_1, value);
	}

	inline static int32_t get_offset_of_InfiniteIntConverter_2() { return static_cast<int32_t>(offsetof(PropertyHelper_t2869223297_StaticFields, ___InfiniteIntConverter_2)); }
	inline InfiniteIntConverter_t2860508531 * get_InfiniteIntConverter_2() const { return ___InfiniteIntConverter_2; }
	inline InfiniteIntConverter_t2860508531 ** get_address_of_InfiniteIntConverter_2() { return &___InfiniteIntConverter_2; }
	inline void set_InfiniteIntConverter_2(InfiniteIntConverter_t2860508531 * value)
	{
		___InfiniteIntConverter_2 = value;
		Il2CppCodeGenWriteBarrier(&___InfiniteIntConverter_2, value);
	}

	inline static int32_t get_offset_of_TimeSpanMinutesConverter_3() { return static_cast<int32_t>(offsetof(PropertyHelper_t2869223297_StaticFields, ___TimeSpanMinutesConverter_3)); }
	inline TimeSpanMinutesConverter_t730211177 * get_TimeSpanMinutesConverter_3() const { return ___TimeSpanMinutesConverter_3; }
	inline TimeSpanMinutesConverter_t730211177 ** get_address_of_TimeSpanMinutesConverter_3() { return &___TimeSpanMinutesConverter_3; }
	inline void set_TimeSpanMinutesConverter_3(TimeSpanMinutesConverter_t730211177 * value)
	{
		___TimeSpanMinutesConverter_3 = value;
		Il2CppCodeGenWriteBarrier(&___TimeSpanMinutesConverter_3, value);
	}

	inline static int32_t get_offset_of_TimeSpanSecondsOrInfiniteConverter_4() { return static_cast<int32_t>(offsetof(PropertyHelper_t2869223297_StaticFields, ___TimeSpanSecondsOrInfiniteConverter_4)); }
	inline TimeSpanSecondsOrInfiniteConverter_t2792648515 * get_TimeSpanSecondsOrInfiniteConverter_4() const { return ___TimeSpanSecondsOrInfiniteConverter_4; }
	inline TimeSpanSecondsOrInfiniteConverter_t2792648515 ** get_address_of_TimeSpanSecondsOrInfiniteConverter_4() { return &___TimeSpanSecondsOrInfiniteConverter_4; }
	inline void set_TimeSpanSecondsOrInfiniteConverter_4(TimeSpanSecondsOrInfiniteConverter_t2792648515 * value)
	{
		___TimeSpanSecondsOrInfiniteConverter_4 = value;
		Il2CppCodeGenWriteBarrier(&___TimeSpanSecondsOrInfiniteConverter_4, value);
	}

	inline static int32_t get_offset_of_TimeSpanSecondsConverter_5() { return static_cast<int32_t>(offsetof(PropertyHelper_t2869223297_StaticFields, ___TimeSpanSecondsConverter_5)); }
	inline TimeSpanSecondsConverter_t1574829041 * get_TimeSpanSecondsConverter_5() const { return ___TimeSpanSecondsConverter_5; }
	inline TimeSpanSecondsConverter_t1574829041 ** get_address_of_TimeSpanSecondsConverter_5() { return &___TimeSpanSecondsConverter_5; }
	inline void set_TimeSpanSecondsConverter_5(TimeSpanSecondsConverter_t1574829041 * value)
	{
		___TimeSpanSecondsConverter_5 = value;
		Il2CppCodeGenWriteBarrier(&___TimeSpanSecondsConverter_5, value);
	}

	inline static int32_t get_offset_of_CommaDelimitedStringCollectionConverter_6() { return static_cast<int32_t>(offsetof(PropertyHelper_t2869223297_StaticFields, ___CommaDelimitedStringCollectionConverter_6)); }
	inline CommaDelimitedStringCollectionConverter_t2027889819 * get_CommaDelimitedStringCollectionConverter_6() const { return ___CommaDelimitedStringCollectionConverter_6; }
	inline CommaDelimitedStringCollectionConverter_t2027889819 ** get_address_of_CommaDelimitedStringCollectionConverter_6() { return &___CommaDelimitedStringCollectionConverter_6; }
	inline void set_CommaDelimitedStringCollectionConverter_6(CommaDelimitedStringCollectionConverter_t2027889819 * value)
	{
		___CommaDelimitedStringCollectionConverter_6 = value;
		Il2CppCodeGenWriteBarrier(&___CommaDelimitedStringCollectionConverter_6, value);
	}

	inline static int32_t get_offset_of_DefaultValidator_7() { return static_cast<int32_t>(offsetof(PropertyHelper_t2869223297_StaticFields, ___DefaultValidator_7)); }
	inline DefaultValidator_t1354184753 * get_DefaultValidator_7() const { return ___DefaultValidator_7; }
	inline DefaultValidator_t1354184753 ** get_address_of_DefaultValidator_7() { return &___DefaultValidator_7; }
	inline void set_DefaultValidator_7(DefaultValidator_t1354184753 * value)
	{
		___DefaultValidator_7 = value;
		Il2CppCodeGenWriteBarrier(&___DefaultValidator_7, value);
	}

	inline static int32_t get_offset_of_NonEmptyStringValidator_8() { return static_cast<int32_t>(offsetof(PropertyHelper_t2869223297_StaticFields, ___NonEmptyStringValidator_8)); }
	inline NullableStringValidator_t388666549 * get_NonEmptyStringValidator_8() const { return ___NonEmptyStringValidator_8; }
	inline NullableStringValidator_t388666549 ** get_address_of_NonEmptyStringValidator_8() { return &___NonEmptyStringValidator_8; }
	inline void set_NonEmptyStringValidator_8(NullableStringValidator_t388666549 * value)
	{
		___NonEmptyStringValidator_8 = value;
		Il2CppCodeGenWriteBarrier(&___NonEmptyStringValidator_8, value);
	}

	inline static int32_t get_offset_of_PositiveTimeSpanValidator_9() { return static_cast<int32_t>(offsetof(PropertyHelper_t2869223297_StaticFields, ___PositiveTimeSpanValidator_9)); }
	inline PositiveTimeSpanValidator_t714847674 * get_PositiveTimeSpanValidator_9() const { return ___PositiveTimeSpanValidator_9; }
	inline PositiveTimeSpanValidator_t714847674 ** get_address_of_PositiveTimeSpanValidator_9() { return &___PositiveTimeSpanValidator_9; }
	inline void set_PositiveTimeSpanValidator_9(PositiveTimeSpanValidator_t714847674 * value)
	{
		___PositiveTimeSpanValidator_9 = value;
		Il2CppCodeGenWriteBarrier(&___PositiveTimeSpanValidator_9, value);
	}

	inline static int32_t get_offset_of_TimeSpanMinutesOrInfiniteConverter_10() { return static_cast<int32_t>(offsetof(PropertyHelper_t2869223297_StaticFields, ___TimeSpanMinutesOrInfiniteConverter_10)); }
	inline TimeSpanMinutesOrInfiniteConverter_t2683336721 * get_TimeSpanMinutesOrInfiniteConverter_10() const { return ___TimeSpanMinutesOrInfiniteConverter_10; }
	inline TimeSpanMinutesOrInfiniteConverter_t2683336721 ** get_address_of_TimeSpanMinutesOrInfiniteConverter_10() { return &___TimeSpanMinutesOrInfiniteConverter_10; }
	inline void set_TimeSpanMinutesOrInfiniteConverter_10(TimeSpanMinutesOrInfiniteConverter_t2683336721 * value)
	{
		___TimeSpanMinutesOrInfiniteConverter_10 = value;
		Il2CppCodeGenWriteBarrier(&___TimeSpanMinutesOrInfiniteConverter_10, value);
	}

	inline static int32_t get_offset_of_IntFromZeroToMaxValidator_11() { return static_cast<int32_t>(offsetof(PropertyHelper_t2869223297_StaticFields, ___IntFromZeroToMaxValidator_11)); }
	inline IntegerValidator_t780581956 * get_IntFromZeroToMaxValidator_11() const { return ___IntFromZeroToMaxValidator_11; }
	inline IntegerValidator_t780581956 ** get_address_of_IntFromZeroToMaxValidator_11() { return &___IntFromZeroToMaxValidator_11; }
	inline void set_IntFromZeroToMaxValidator_11(IntegerValidator_t780581956 * value)
	{
		___IntFromZeroToMaxValidator_11 = value;
		Il2CppCodeGenWriteBarrier(&___IntFromZeroToMaxValidator_11, value);
	}

	inline static int32_t get_offset_of_IntFromOneToMax_1Validator_12() { return static_cast<int32_t>(offsetof(PropertyHelper_t2869223297_StaticFields, ___IntFromOneToMax_1Validator_12)); }
	inline IntegerValidator_t780581956 * get_IntFromOneToMax_1Validator_12() const { return ___IntFromOneToMax_1Validator_12; }
	inline IntegerValidator_t780581956 ** get_address_of_IntFromOneToMax_1Validator_12() { return &___IntFromOneToMax_1Validator_12; }
	inline void set_IntFromOneToMax_1Validator_12(IntegerValidator_t780581956 * value)
	{
		___IntFromOneToMax_1Validator_12 = value;
		Il2CppCodeGenWriteBarrier(&___IntFromOneToMax_1Validator_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
