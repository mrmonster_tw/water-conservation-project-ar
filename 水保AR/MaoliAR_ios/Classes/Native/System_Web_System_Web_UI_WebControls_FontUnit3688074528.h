﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"
#include "System_Web_System_Web_UI_WebControls_FontSize1746433605.h"
#include "System_Web_System_Web_UI_WebControls_Unit3186727900.h"
#include "System_Web_System_Web_UI_WebControls_FontUnit3688074528.h"

// System.String[]
struct StringU5BU5D_t1281789340;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.WebControls.FontUnit
struct  FontUnit_t3688074528 
{
public:
	// System.Web.UI.WebControls.FontSize System.Web.UI.WebControls.FontUnit::type
	int32_t ___type_0;
	// System.Web.UI.WebControls.Unit System.Web.UI.WebControls.FontUnit::unit
	Unit_t3186727900  ___unit_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(FontUnit_t3688074528, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_unit_1() { return static_cast<int32_t>(offsetof(FontUnit_t3688074528, ___unit_1)); }
	inline Unit_t3186727900  get_unit_1() const { return ___unit_1; }
	inline Unit_t3186727900 * get_address_of_unit_1() { return &___unit_1; }
	inline void set_unit_1(Unit_t3186727900  value)
	{
		___unit_1 = value;
	}
};

struct FontUnit_t3688074528_StaticFields
{
public:
	// System.Web.UI.WebControls.FontUnit System.Web.UI.WebControls.FontUnit::Empty
	FontUnit_t3688074528  ___Empty_2;
	// System.Web.UI.WebControls.FontUnit System.Web.UI.WebControls.FontUnit::Smaller
	FontUnit_t3688074528  ___Smaller_3;
	// System.Web.UI.WebControls.FontUnit System.Web.UI.WebControls.FontUnit::Larger
	FontUnit_t3688074528  ___Larger_4;
	// System.Web.UI.WebControls.FontUnit System.Web.UI.WebControls.FontUnit::XXSmall
	FontUnit_t3688074528  ___XXSmall_5;
	// System.Web.UI.WebControls.FontUnit System.Web.UI.WebControls.FontUnit::XSmall
	FontUnit_t3688074528  ___XSmall_6;
	// System.Web.UI.WebControls.FontUnit System.Web.UI.WebControls.FontUnit::Small
	FontUnit_t3688074528  ___Small_7;
	// System.Web.UI.WebControls.FontUnit System.Web.UI.WebControls.FontUnit::Medium
	FontUnit_t3688074528  ___Medium_8;
	// System.Web.UI.WebControls.FontUnit System.Web.UI.WebControls.FontUnit::Large
	FontUnit_t3688074528  ___Large_9;
	// System.Web.UI.WebControls.FontUnit System.Web.UI.WebControls.FontUnit::XLarge
	FontUnit_t3688074528  ___XLarge_10;
	// System.Web.UI.WebControls.FontUnit System.Web.UI.WebControls.FontUnit::XXLarge
	FontUnit_t3688074528  ___XXLarge_11;
	// System.String[] System.Web.UI.WebControls.FontUnit::font_size_names
	StringU5BU5D_t1281789340* ___font_size_names_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Web.UI.WebControls.FontUnit::<>f__switch$map15
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map15_13;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(FontUnit_t3688074528_StaticFields, ___Empty_2)); }
	inline FontUnit_t3688074528  get_Empty_2() const { return ___Empty_2; }
	inline FontUnit_t3688074528 * get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(FontUnit_t3688074528  value)
	{
		___Empty_2 = value;
	}

	inline static int32_t get_offset_of_Smaller_3() { return static_cast<int32_t>(offsetof(FontUnit_t3688074528_StaticFields, ___Smaller_3)); }
	inline FontUnit_t3688074528  get_Smaller_3() const { return ___Smaller_3; }
	inline FontUnit_t3688074528 * get_address_of_Smaller_3() { return &___Smaller_3; }
	inline void set_Smaller_3(FontUnit_t3688074528  value)
	{
		___Smaller_3 = value;
	}

	inline static int32_t get_offset_of_Larger_4() { return static_cast<int32_t>(offsetof(FontUnit_t3688074528_StaticFields, ___Larger_4)); }
	inline FontUnit_t3688074528  get_Larger_4() const { return ___Larger_4; }
	inline FontUnit_t3688074528 * get_address_of_Larger_4() { return &___Larger_4; }
	inline void set_Larger_4(FontUnit_t3688074528  value)
	{
		___Larger_4 = value;
	}

	inline static int32_t get_offset_of_XXSmall_5() { return static_cast<int32_t>(offsetof(FontUnit_t3688074528_StaticFields, ___XXSmall_5)); }
	inline FontUnit_t3688074528  get_XXSmall_5() const { return ___XXSmall_5; }
	inline FontUnit_t3688074528 * get_address_of_XXSmall_5() { return &___XXSmall_5; }
	inline void set_XXSmall_5(FontUnit_t3688074528  value)
	{
		___XXSmall_5 = value;
	}

	inline static int32_t get_offset_of_XSmall_6() { return static_cast<int32_t>(offsetof(FontUnit_t3688074528_StaticFields, ___XSmall_6)); }
	inline FontUnit_t3688074528  get_XSmall_6() const { return ___XSmall_6; }
	inline FontUnit_t3688074528 * get_address_of_XSmall_6() { return &___XSmall_6; }
	inline void set_XSmall_6(FontUnit_t3688074528  value)
	{
		___XSmall_6 = value;
	}

	inline static int32_t get_offset_of_Small_7() { return static_cast<int32_t>(offsetof(FontUnit_t3688074528_StaticFields, ___Small_7)); }
	inline FontUnit_t3688074528  get_Small_7() const { return ___Small_7; }
	inline FontUnit_t3688074528 * get_address_of_Small_7() { return &___Small_7; }
	inline void set_Small_7(FontUnit_t3688074528  value)
	{
		___Small_7 = value;
	}

	inline static int32_t get_offset_of_Medium_8() { return static_cast<int32_t>(offsetof(FontUnit_t3688074528_StaticFields, ___Medium_8)); }
	inline FontUnit_t3688074528  get_Medium_8() const { return ___Medium_8; }
	inline FontUnit_t3688074528 * get_address_of_Medium_8() { return &___Medium_8; }
	inline void set_Medium_8(FontUnit_t3688074528  value)
	{
		___Medium_8 = value;
	}

	inline static int32_t get_offset_of_Large_9() { return static_cast<int32_t>(offsetof(FontUnit_t3688074528_StaticFields, ___Large_9)); }
	inline FontUnit_t3688074528  get_Large_9() const { return ___Large_9; }
	inline FontUnit_t3688074528 * get_address_of_Large_9() { return &___Large_9; }
	inline void set_Large_9(FontUnit_t3688074528  value)
	{
		___Large_9 = value;
	}

	inline static int32_t get_offset_of_XLarge_10() { return static_cast<int32_t>(offsetof(FontUnit_t3688074528_StaticFields, ___XLarge_10)); }
	inline FontUnit_t3688074528  get_XLarge_10() const { return ___XLarge_10; }
	inline FontUnit_t3688074528 * get_address_of_XLarge_10() { return &___XLarge_10; }
	inline void set_XLarge_10(FontUnit_t3688074528  value)
	{
		___XLarge_10 = value;
	}

	inline static int32_t get_offset_of_XXLarge_11() { return static_cast<int32_t>(offsetof(FontUnit_t3688074528_StaticFields, ___XXLarge_11)); }
	inline FontUnit_t3688074528  get_XXLarge_11() const { return ___XXLarge_11; }
	inline FontUnit_t3688074528 * get_address_of_XXLarge_11() { return &___XXLarge_11; }
	inline void set_XXLarge_11(FontUnit_t3688074528  value)
	{
		___XXLarge_11 = value;
	}

	inline static int32_t get_offset_of_font_size_names_12() { return static_cast<int32_t>(offsetof(FontUnit_t3688074528_StaticFields, ___font_size_names_12)); }
	inline StringU5BU5D_t1281789340* get_font_size_names_12() const { return ___font_size_names_12; }
	inline StringU5BU5D_t1281789340** get_address_of_font_size_names_12() { return &___font_size_names_12; }
	inline void set_font_size_names_12(StringU5BU5D_t1281789340* value)
	{
		___font_size_names_12 = value;
		Il2CppCodeGenWriteBarrier(&___font_size_names_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map15_13() { return static_cast<int32_t>(offsetof(FontUnit_t3688074528_StaticFields, ___U3CU3Ef__switchU24map15_13)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map15_13() const { return ___U3CU3Ef__switchU24map15_13; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map15_13() { return &___U3CU3Ef__switchU24map15_13; }
	inline void set_U3CU3Ef__switchU24map15_13(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map15_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map15_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Web.UI.WebControls.FontUnit
struct FontUnit_t3688074528_marshaled_pinvoke
{
	int32_t ___type_0;
	Unit_t3186727900_marshaled_pinvoke ___unit_1;
};
// Native definition for COM marshalling of System.Web.UI.WebControls.FontUnit
struct FontUnit_t3688074528_marshaled_com
{
	int32_t ___type_0;
	Unit_t3186727900_marshaled_com ___unit_1;
};
