﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3156163955.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.MonoSettingsSection
struct  MonoSettingsSection_t2949572421  : public ConfigurationSection_t3156163955
{
public:

public:
};

struct MonoSettingsSection_t2949572421_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Web.Configuration.MonoSettingsSection::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_17;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.MonoSettingsSection::compilersCompatibilityProp
	ConfigurationProperty_t3590861854 * ___compilersCompatibilityProp_18;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.MonoSettingsSection::useCompilersCompatibilityProp
	ConfigurationProperty_t3590861854 * ___useCompilersCompatibilityProp_19;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.MonoSettingsSection::verificationCompatibilityProp
	ConfigurationProperty_t3590861854 * ___verificationCompatibilityProp_20;

public:
	inline static int32_t get_offset_of_properties_17() { return static_cast<int32_t>(offsetof(MonoSettingsSection_t2949572421_StaticFields, ___properties_17)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_17() const { return ___properties_17; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_17() { return &___properties_17; }
	inline void set_properties_17(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_17 = value;
		Il2CppCodeGenWriteBarrier(&___properties_17, value);
	}

	inline static int32_t get_offset_of_compilersCompatibilityProp_18() { return static_cast<int32_t>(offsetof(MonoSettingsSection_t2949572421_StaticFields, ___compilersCompatibilityProp_18)); }
	inline ConfigurationProperty_t3590861854 * get_compilersCompatibilityProp_18() const { return ___compilersCompatibilityProp_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_compilersCompatibilityProp_18() { return &___compilersCompatibilityProp_18; }
	inline void set_compilersCompatibilityProp_18(ConfigurationProperty_t3590861854 * value)
	{
		___compilersCompatibilityProp_18 = value;
		Il2CppCodeGenWriteBarrier(&___compilersCompatibilityProp_18, value);
	}

	inline static int32_t get_offset_of_useCompilersCompatibilityProp_19() { return static_cast<int32_t>(offsetof(MonoSettingsSection_t2949572421_StaticFields, ___useCompilersCompatibilityProp_19)); }
	inline ConfigurationProperty_t3590861854 * get_useCompilersCompatibilityProp_19() const { return ___useCompilersCompatibilityProp_19; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_useCompilersCompatibilityProp_19() { return &___useCompilersCompatibilityProp_19; }
	inline void set_useCompilersCompatibilityProp_19(ConfigurationProperty_t3590861854 * value)
	{
		___useCompilersCompatibilityProp_19 = value;
		Il2CppCodeGenWriteBarrier(&___useCompilersCompatibilityProp_19, value);
	}

	inline static int32_t get_offset_of_verificationCompatibilityProp_20() { return static_cast<int32_t>(offsetof(MonoSettingsSection_t2949572421_StaticFields, ___verificationCompatibilityProp_20)); }
	inline ConfigurationProperty_t3590861854 * get_verificationCompatibilityProp_20() const { return ___verificationCompatibilityProp_20; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_verificationCompatibilityProp_20() { return &___verificationCompatibilityProp_20; }
	inline void set_verificationCompatibilityProp_20(ConfigurationProperty_t3590861854 * value)
	{
		___verificationCompatibilityProp_20 = value;
		Il2CppCodeGenWriteBarrier(&___verificationCompatibilityProp_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
