﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Selector1453713750.h"

// System.IdentityModel.Selectors.UserNamePasswordValidator
struct UserNamePasswordValidator_t4240724900;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Selectors.CustomUserNameSecurityTokenAuthenticator
struct  CustomUserNameSecurityTokenAuthenticator_t1007898913  : public UserNameSecurityTokenAuthenticator_t1453713750
{
public:
	// System.IdentityModel.Selectors.UserNamePasswordValidator System.IdentityModel.Selectors.CustomUserNameSecurityTokenAuthenticator::validator
	UserNamePasswordValidator_t4240724900 * ___validator_0;

public:
	inline static int32_t get_offset_of_validator_0() { return static_cast<int32_t>(offsetof(CustomUserNameSecurityTokenAuthenticator_t1007898913, ___validator_0)); }
	inline UserNamePasswordValidator_t4240724900 * get_validator_0() const { return ___validator_0; }
	inline UserNamePasswordValidator_t4240724900 ** get_address_of_validator_0() { return &___validator_0; }
	inline void set_validator_0(UserNamePasswordValidator_t4240724900 * value)
	{
		___validator_0 = value;
		Il2CppCodeGenWriteBarrier(&___validator_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
