﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// UnityEngine.Sprite
struct Sprite_t280657092;
// Drag_Controller
struct Drag_Controller_t1925480236;
// System.Object
struct Il2CppObject;
// System.Predicate`1<UnityEngine.UI.Image>
struct Predicate_1_t3495563775;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Drag_Controller/<點錯了>c__Iterator1
struct  U3CU9EDEU932FU4E86U3Ec__Iterator1_t2446876263  : public Il2CppObject
{
public:
	// UnityEngine.Sprite Drag_Controller/<點錯了>c__Iterator1::<s>__0
	Sprite_t280657092 * ___U3CsU3E__0_0;
	// Drag_Controller Drag_Controller/<點錯了>c__Iterator1::$this
	Drag_Controller_t1925480236 * ___U24this_1;
	// System.Object Drag_Controller/<點錯了>c__Iterator1::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean Drag_Controller/<點錯了>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 Drag_Controller/<點錯了>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CU9EDEU932FU4E86U3Ec__Iterator1_t2446876263, ___U3CsU3E__0_0)); }
	inline Sprite_t280657092 * get_U3CsU3E__0_0() const { return ___U3CsU3E__0_0; }
	inline Sprite_t280657092 ** get_address_of_U3CsU3E__0_0() { return &___U3CsU3E__0_0; }
	inline void set_U3CsU3E__0_0(Sprite_t280657092 * value)
	{
		___U3CsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CU9EDEU932FU4E86U3Ec__Iterator1_t2446876263, ___U24this_1)); }
	inline Drag_Controller_t1925480236 * get_U24this_1() const { return ___U24this_1; }
	inline Drag_Controller_t1925480236 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Drag_Controller_t1925480236 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CU9EDEU932FU4E86U3Ec__Iterator1_t2446876263, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CU9EDEU932FU4E86U3Ec__Iterator1_t2446876263, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CU9EDEU932FU4E86U3Ec__Iterator1_t2446876263, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

struct U3CU9EDEU932FU4E86U3Ec__Iterator1_t2446876263_StaticFields
{
public:
	// System.Predicate`1<UnityEngine.UI.Image> Drag_Controller/<點錯了>c__Iterator1::<>f__am$cache0
	Predicate_1_t3495563775 * ___U3CU3Ef__amU24cache0_5;
	// System.Predicate`1<UnityEngine.UI.Image> Drag_Controller/<點錯了>c__Iterator1::<>f__am$cache1
	Predicate_1_t3495563775 * ___U3CU3Ef__amU24cache1_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(U3CU9EDEU932FU4E86U3Ec__Iterator1_t2446876263_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Predicate_1_t3495563775 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Predicate_1_t3495563775 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Predicate_1_t3495563775 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_6() { return static_cast<int32_t>(offsetof(U3CU9EDEU932FU4E86U3Ec__Iterator1_t2446876263_StaticFields, ___U3CU3Ef__amU24cache1_6)); }
	inline Predicate_1_t3495563775 * get_U3CU3Ef__amU24cache1_6() const { return ___U3CU3Ef__amU24cache1_6; }
	inline Predicate_1_t3495563775 ** get_address_of_U3CU3Ef__amU24cache1_6() { return &___U3CU3Ef__amU24cache1_6; }
	inline void set_U3CU3Ef__amU24cache1_6(Predicate_1_t3495563775 * value)
	{
		___U3CU3Ef__amU24cache1_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
