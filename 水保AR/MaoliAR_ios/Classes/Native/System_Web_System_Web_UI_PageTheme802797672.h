﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.UI.Page
struct Page_t770747966;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.PageTheme
struct  PageTheme_t802797672  : public Il2CppObject
{
public:
	// System.Web.UI.Page System.Web.UI.PageTheme::_page
	Page_t770747966 * ____page_0;

public:
	inline static int32_t get_offset_of__page_0() { return static_cast<int32_t>(offsetof(PageTheme_t802797672, ____page_0)); }
	inline Page_t770747966 * get__page_0() const { return ____page_0; }
	inline Page_t770747966 ** get_address_of__page_0() { return &____page_0; }
	inline void set__page_0(Page_t770747966 * value)
	{
		____page_0 = value;
		Il2CppCodeGenWriteBarrier(&____page_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
