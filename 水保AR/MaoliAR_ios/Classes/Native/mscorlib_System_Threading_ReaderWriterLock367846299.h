﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Runtime_ConstrainedExecution_Critic701527852.h"

// System.Threading.LockQueue
struct LockQueue_t2679652224;
// System.Collections.Hashtable
struct Hashtable_t1853889766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.ReaderWriterLock
struct  ReaderWriterLock_t367846299  : public CriticalFinalizerObject_t701527852
{
public:
	// System.Int32 System.Threading.ReaderWriterLock::seq_num
	int32_t ___seq_num_0;
	// System.Int32 System.Threading.ReaderWriterLock::state
	int32_t ___state_1;
	// System.Int32 System.Threading.ReaderWriterLock::readers
	int32_t ___readers_2;
	// System.Threading.LockQueue System.Threading.ReaderWriterLock::writer_queue
	LockQueue_t2679652224 * ___writer_queue_3;
	// System.Collections.Hashtable System.Threading.ReaderWriterLock::reader_locks
	Hashtable_t1853889766 * ___reader_locks_4;
	// System.Int32 System.Threading.ReaderWriterLock::writer_lock_owner
	int32_t ___writer_lock_owner_5;

public:
	inline static int32_t get_offset_of_seq_num_0() { return static_cast<int32_t>(offsetof(ReaderWriterLock_t367846299, ___seq_num_0)); }
	inline int32_t get_seq_num_0() const { return ___seq_num_0; }
	inline int32_t* get_address_of_seq_num_0() { return &___seq_num_0; }
	inline void set_seq_num_0(int32_t value)
	{
		___seq_num_0 = value;
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(ReaderWriterLock_t367846299, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_readers_2() { return static_cast<int32_t>(offsetof(ReaderWriterLock_t367846299, ___readers_2)); }
	inline int32_t get_readers_2() const { return ___readers_2; }
	inline int32_t* get_address_of_readers_2() { return &___readers_2; }
	inline void set_readers_2(int32_t value)
	{
		___readers_2 = value;
	}

	inline static int32_t get_offset_of_writer_queue_3() { return static_cast<int32_t>(offsetof(ReaderWriterLock_t367846299, ___writer_queue_3)); }
	inline LockQueue_t2679652224 * get_writer_queue_3() const { return ___writer_queue_3; }
	inline LockQueue_t2679652224 ** get_address_of_writer_queue_3() { return &___writer_queue_3; }
	inline void set_writer_queue_3(LockQueue_t2679652224 * value)
	{
		___writer_queue_3 = value;
		Il2CppCodeGenWriteBarrier(&___writer_queue_3, value);
	}

	inline static int32_t get_offset_of_reader_locks_4() { return static_cast<int32_t>(offsetof(ReaderWriterLock_t367846299, ___reader_locks_4)); }
	inline Hashtable_t1853889766 * get_reader_locks_4() const { return ___reader_locks_4; }
	inline Hashtable_t1853889766 ** get_address_of_reader_locks_4() { return &___reader_locks_4; }
	inline void set_reader_locks_4(Hashtable_t1853889766 * value)
	{
		___reader_locks_4 = value;
		Il2CppCodeGenWriteBarrier(&___reader_locks_4, value);
	}

	inline static int32_t get_offset_of_writer_lock_owner_5() { return static_cast<int32_t>(offsetof(ReaderWriterLock_t367846299, ___writer_lock_owner_5)); }
	inline int32_t get_writer_lock_owner_5() const { return ___writer_lock_owner_5; }
	inline int32_t* get_address_of_writer_lock_owner_5() { return &___writer_lock_owner_5; }
	inline void set_writer_lock_owner_5(int32_t value)
	{
		___writer_lock_owner_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
