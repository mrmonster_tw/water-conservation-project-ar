﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinderImpl_Int50334038.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableSourceImp2456210702.h"
#include "Vuforia_UnityExtensions_Vuforia_TextureRenderer3278815845.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableImpl3595316917.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackerManagerImpl3998245004.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButtonImpl4195991701.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamImpl124212097.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamProfile4166408645.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamProfile_Prof3519391925.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi901995765.h"
#include "Vuforia_UnityExtensions_Vuforia_Image745056343.h"
#include "Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT3209881435.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageTargetAbstrac3577513769.h"
#include "Vuforia_UnityExtensions_Vuforia_ObjectTracker4177997237.h"
#include "Vuforia_UnityExtensions_Vuforia_MaskOutAbstractBeh1543528878.h"
#include "Vuforia_UnityExtensions_Vuforia_MultiTargetAbstrac1865684713.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity1788908542.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity_InitE3420749710.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity_Vufori545805519.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity_Storag857810839.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaARControlle1876945237.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaARControlle3672819471.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaMacros2044285728.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManager1653423889.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManager_Tra4227350457.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer3433045970.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Fp2906034572.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vid736962841.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vid994527297.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Ve3527036565.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vi1805965052.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Ren402009282.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRuntimeUtili399660591.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRuntimeUtil3274999204.h"
#include "Vuforia_UnityExtensions_Vuforia_SurfaceUtilities1841955943.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder2439332195.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_InitSt538152685.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Updat1279515537.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Filte1400485161.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Targe3441982613.h"
#include "Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBeh802660870.h"
#include "Vuforia_UnityExtensions_Vuforia_TextTracker3950053289.h"
#include "Vuforia_UnityExtensions_Vuforia_SimpleTargetData4194873257.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour1113559212.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour1100905814.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour4035406609.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableSource2567074243.h"
#include "Vuforia_UnityExtensions_Vuforia_Tracker2709586299.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackerManager1703337244.h"
#include "Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBeha303815290.h"
#include "Vuforia_UnityExtensions_Vuforia_UserDefinedTargetB2875328746.h"
#include "Vuforia_UnityExtensions_Vuforia_VideoBackgroundAbst293578642.h"
#include "Vuforia_UnityExtensions_Vuforia_VideoBackgroundMan2198727358.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButton386166510.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButton_Sens3045829715.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstr2408702468.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamARController3718642882.h"
#include "Vuforia_UnityExtensions_Vuforia_WordManager3100853168.h"
#include "Vuforia_UnityExtensions_Vuforia_WordResult3640773802.h"
#include "Vuforia_UnityExtensions_Vuforia_WordTemplateMode435619688.h"
#include "Vuforia_UnityExtensions_Vuforia_WordAbstractBehavi3502498754.h"
#include "Vuforia_UnityExtensions_Vuforia_WordFilterMode1058214526.h"
#include "Vuforia_UnityExtensions_Vuforia_WordList3693642253.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearCalibrationP947793426.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearUserCalibra2926839199.h"
#include "Vuforia_UnityExtensions_U3CPrivateImplementationDe3057255361.h"
#include "Vuforia_UnityExtensions_U3CPrivateImplementationDe3517759979.h"
#include "Boo_Lang_U3CModuleU3E692745525.h"
#include "Boo_Lang_Boo_Lang_Builtins286914603.h"
#include "Boo_Lang_Boo_Lang_Runtime_DynamicDispatching_Dispa1010326087.h"
#include "Boo_Lang_Boo_Lang_Runtime_DynamicDispatching_Dispa1014155341.h"
#include "Boo_Lang_Boo_Lang_Runtime_DynamicDispatching_Dispa2110064572.h"
#include "Boo_Lang_Boo_Lang_Runtime_DynamicDispatching_Dispat965810167.h"
#include "Boo_Lang_Boo_Lang_Runtime_DynamicDispatching_Numer3533651679.h"
#include "Boo_Lang_Boo_Lang_Runtime_ExtensionRegistry2424660641.h"
#include "Boo_Lang_Boo_Lang_Runtime_RuntimeServices2098243569.h"
#include "Boo_Lang_Boo_Lang_Runtime_RuntimeServices_U3CGetEx3368530435.h"
#include "Boo_Lang_Boo_Lang_Runtime_RuntimeServices_U3CCoerce572148199.h"
#include "Boo_Lang_Boo_Lang_Runtime_RuntimeServices_U3CEmitI4009522197.h"
#include "Boo_Lang_Boo_Lang_Runtime_DynamicDispatching_Dispat684365006.h"
#include "System_ServiceModel_U3CModuleU3E692745525.h"
#include "System_ServiceModel_Locale4128636107.h"
#include "System_ServiceModel_System_MonoTODOAttribute4131080581.h"
#include "System_ServiceModel_System_MonoLimitationAttribute3672514598.h"
#include "System_ServiceModel_Mono_CodeGeneration_CodeArgume1456663465.h"
#include "System_ServiceModel_Mono_CodeGeneration_CodeArrayI2712318199.h"
#include "System_ServiceModel_Mono_CodeGeneration_CodeAssignm720477174.h"
#include "System_ServiceModel_Mono_CodeGeneration_CodeBlock2718634139.h"
#include "System_ServiceModel_Mono_CodeGeneration_CodeBuilde3190018006.h"
#include "System_ServiceModel_Mono_CodeGeneration_CodeCast3030498946.h"
#include "System_ServiceModel_Mono_CodeGeneration_CodeClass2288932064.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4400 = { sizeof (InternalTargetSearchResult_t50334038)+ sizeof (Il2CppObject), sizeof(InternalTargetSearchResult_t50334038 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4400[6] = 
{
	InternalTargetSearchResult_t50334038::get_offset_of_TargetNamePtr_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t50334038::get_offset_of_UniqueTargetIdPtr_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t50334038::get_offset_of_MetaDataPtr_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t50334038::get_offset_of_TargetSearchResultPtr_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t50334038::get_offset_of_TargetSize_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t50334038::get_offset_of_TrackingRating_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4401 = { sizeof (TrackableSourceImpl_t2456210702), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4401[1] = 
{
	TrackableSourceImpl_t2456210702::get_offset_of_U3CTrackableSourcePtrU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4402 = { sizeof (TextureRenderer_t3278815845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4402[3] = 
{
	TextureRenderer_t3278815845::get_offset_of_mTextureBufferCamera_0(),
	TextureRenderer_t3278815845::get_offset_of_mTextureWidth_1(),
	TextureRenderer_t3278815845::get_offset_of_mTextureHeight_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4403 = { sizeof (TrackableImpl_t3595316917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4403[2] = 
{
	TrackableImpl_t3595316917::get_offset_of_U3CNameU3Ek__BackingField_0(),
	TrackableImpl_t3595316917::get_offset_of_U3CIDU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4404 = { sizeof (TrackerManagerImpl_t3998245004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4404[5] = 
{
	TrackerManagerImpl_t3998245004::get_offset_of_mObjectTracker_1(),
	TrackerManagerImpl_t3998245004::get_offset_of_mTextTracker_2(),
	TrackerManagerImpl_t3998245004::get_offset_of_mSmartTerrainTracker_3(),
	TrackerManagerImpl_t3998245004::get_offset_of_mDeviceTracker_4(),
	TrackerManagerImpl_t3998245004::get_offset_of_mStateManager_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4405 = { sizeof (VirtualButtonImpl_t4195991701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4405[6] = 
{
	VirtualButtonImpl_t4195991701::get_offset_of_mName_1(),
	VirtualButtonImpl_t4195991701::get_offset_of_mID_2(),
	VirtualButtonImpl_t4195991701::get_offset_of_mArea_3(),
	VirtualButtonImpl_t4195991701::get_offset_of_mIsEnabled_4(),
	VirtualButtonImpl_t4195991701::get_offset_of_mParentImageTarget_5(),
	VirtualButtonImpl_t4195991701::get_offset_of_mParentDataSet_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4406 = { sizeof (WebCamImpl_t124212097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4406[15] = 
{
	WebCamImpl_t124212097::get_offset_of_mARCameras_0(),
	WebCamImpl_t124212097::get_offset_of_mOriginalCameraCullMask_1(),
	WebCamImpl_t124212097::get_offset_of_mWebCamTexture_2(),
	WebCamImpl_t124212097::get_offset_of_mVideoModeData_3(),
	WebCamImpl_t124212097::get_offset_of_mVideoTextureInfo_4(),
	WebCamImpl_t124212097::get_offset_of_mTextureRenderer_5(),
	WebCamImpl_t124212097::get_offset_of_mBufferReadTexture_6(),
	WebCamImpl_t124212097::get_offset_of_mReadPixelsRect_7(),
	WebCamImpl_t124212097::get_offset_of_mWebCamProfile_8(),
	WebCamImpl_t124212097::get_offset_of_mFlipHorizontally_9(),
	WebCamImpl_t124212097::get_offset_of_mIsDirty_10(),
	WebCamImpl_t124212097::get_offset_of_mLastFrameIdx_11(),
	WebCamImpl_t124212097::get_offset_of_mRenderTextureLayer_12(),
	WebCamImpl_t124212097::get_offset_of_mWebcamPlaying_13(),
	WebCamImpl_t124212097::get_offset_of_U3CIsTextureSizeAvailableU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4407 = { sizeof (WebCamProfile_t4166408645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4407[1] = 
{
	WebCamProfile_t4166408645::get_offset_of_mProfileCollection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4408 = { sizeof (ProfileData_t3519391925)+ sizeof (Il2CppObject), sizeof(ProfileData_t3519391925 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4408[3] = 
{
	ProfileData_t3519391925::get_offset_of_RequestedTextureSize_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ProfileData_t3519391925::get_offset_of_ResampledTextureSize_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ProfileData_t3519391925::get_offset_of_RequestedFPS_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4409 = { sizeof (ProfileCollection_t901995765)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4409[2] = 
{
	ProfileCollection_t901995765::get_offset_of_DefaultProfile_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ProfileCollection_t901995765::get_offset_of_Profiles_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4410 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4411 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4412 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4413 = { sizeof (Image_t745056343), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4414 = { sizeof (PIXEL_FORMAT_t3209881435)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4414[7] = 
{
	PIXEL_FORMAT_t3209881435::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4415 = { sizeof (ImageTargetAbstractBehaviour_t3577513769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4415[8] = 
{
	ImageTargetAbstractBehaviour_t3577513769::get_offset_of_mAspectRatio_20(),
	ImageTargetAbstractBehaviour_t3577513769::get_offset_of_mImageTargetType_21(),
	ImageTargetAbstractBehaviour_t3577513769::get_offset_of_mWidth_22(),
	ImageTargetAbstractBehaviour_t3577513769::get_offset_of_mHeight_23(),
	ImageTargetAbstractBehaviour_t3577513769::get_offset_of_mImageTarget_24(),
	ImageTargetAbstractBehaviour_t3577513769::get_offset_of_mVirtualButtonBehaviours_25(),
	ImageTargetAbstractBehaviour_t3577513769::get_offset_of_mLastTransformScale_26(),
	ImageTargetAbstractBehaviour_t3577513769::get_offset_of_mLastSize_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4416 = { sizeof (ObjectTracker_t4177997237), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4417 = { sizeof (MaskOutAbstractBehaviour_t1543528878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4417[1] = 
{
	MaskOutAbstractBehaviour_t1543528878::get_offset_of_maskMaterial_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4418 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4419 = { sizeof (MultiTargetAbstractBehaviour_t1865684713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4419[1] = 
{
	MultiTargetAbstractBehaviour_t1865684713::get_offset_of_mMultiTarget_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4420 = { sizeof (VuforiaUnity_t1788908542), -1, sizeof(VuforiaUnity_t1788908542_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4420[1] = 
{
	VuforiaUnity_t1788908542_StaticFields::get_offset_of_mHoloLensApiAbstraction_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4421 = { sizeof (InitError_t3420749710)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4421[12] = 
{
	InitError_t3420749710::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4422 = { sizeof (VuforiaHint_t545805519)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4422[4] = 
{
	VuforiaHint_t545805519::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4423 = { sizeof (StorageType_t857810839)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4423[4] = 
{
	StorageType_t857810839::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4424 = { sizeof (VuforiaARController_t1876945237), -1, sizeof(VuforiaARController_t1876945237_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4424[39] = 
{
	VuforiaARController_t1876945237::get_offset_of_CameraDeviceModeSetting_1(),
	VuforiaARController_t1876945237::get_offset_of_MaxSimultaneousImageTargets_2(),
	VuforiaARController_t1876945237::get_offset_of_MaxSimultaneousObjectTargets_3(),
	VuforiaARController_t1876945237::get_offset_of_UseDelayedLoadingObjectTargets_4(),
	VuforiaARController_t1876945237::get_offset_of_CameraDirection_5(),
	VuforiaARController_t1876945237::get_offset_of_MirrorVideoBackground_6(),
	VuforiaARController_t1876945237::get_offset_of_mWorldCenterMode_7(),
	VuforiaARController_t1876945237::get_offset_of_mWorldCenter_8(),
	VuforiaARController_t1876945237::get_offset_of_mTrackerEventHandlers_9(),
	VuforiaARController_t1876945237::get_offset_of_mVideoBgEventHandlers_10(),
	VuforiaARController_t1876945237::get_offset_of_mOnVuforiaInitialized_11(),
	VuforiaARController_t1876945237::get_offset_of_mOnVuforiaStarted_12(),
	VuforiaARController_t1876945237::get_offset_of_mOnVuforiaDeinitialized_13(),
	VuforiaARController_t1876945237::get_offset_of_mOnTrackablesUpdated_14(),
	VuforiaARController_t1876945237::get_offset_of_mRenderOnUpdate_15(),
	VuforiaARController_t1876945237::get_offset_of_mOnPause_16(),
	VuforiaARController_t1876945237::get_offset_of_mPaused_17(),
	VuforiaARController_t1876945237::get_offset_of_mOnBackgroundTextureChanged_18(),
	VuforiaARController_t1876945237::get_offset_of_mStartHasBeenInvoked_19(),
	VuforiaARController_t1876945237::get_offset_of_mHasStarted_20(),
	VuforiaARController_t1876945237::get_offset_of_mBackgroundTextureHasChanged_21(),
	VuforiaARController_t1876945237::get_offset_of_mCameraConfiguration_22(),
	VuforiaARController_t1876945237::get_offset_of_mEyewearBehaviour_23(),
	VuforiaARController_t1876945237::get_offset_of_mVideoBackgroundMgr_24(),
	VuforiaARController_t1876945237::get_offset_of_mCheckStopCamera_25(),
	VuforiaARController_t1876945237::get_offset_of_mClearMaterial_26(),
	VuforiaARController_t1876945237::get_offset_of_mMetalRendering_27(),
	VuforiaARController_t1876945237::get_offset_of_mHasStartedOnce_28(),
	VuforiaARController_t1876945237::get_offset_of_mWasEnabledBeforePause_29(),
	VuforiaARController_t1876945237::get_offset_of_mObjectTrackerWasActiveBeforePause_30(),
	VuforiaARController_t1876945237::get_offset_of_mObjectTrackerWasActiveBeforeDisabling_31(),
	VuforiaARController_t1876945237::get_offset_of_mLastUpdatedFrame_32(),
	VuforiaARController_t1876945237::get_offset_of_mTrackersRequestedToDeinit_33(),
	VuforiaARController_t1876945237::get_offset_of_mMissedToApplyLeftProjectionMatrix_34(),
	VuforiaARController_t1876945237::get_offset_of_mMissedToApplyRightProjectionMatrix_35(),
	VuforiaARController_t1876945237::get_offset_of_mLeftProjectMatrixToApply_36(),
	VuforiaARController_t1876945237::get_offset_of_mRightProjectMatrixToApply_37(),
	VuforiaARController_t1876945237_StaticFields::get_offset_of_mInstance_38(),
	VuforiaARController_t1876945237_StaticFields::get_offset_of_mPadlock_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4425 = { sizeof (WorldCenterMode_t3672819471)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4425[5] = 
{
	WorldCenterMode_t3672819471::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4426 = { sizeof (VuforiaMacros_t2044285728)+ sizeof (Il2CppObject), sizeof(VuforiaMacros_t2044285728 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4426[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4427 = { sizeof (VuforiaManager_t1653423889), -1, sizeof(VuforiaManager_t1653423889_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4427[1] = 
{
	VuforiaManager_t1653423889_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4428 = { sizeof (TrackableIdPair_t4227350457)+ sizeof (Il2CppObject), sizeof(TrackableIdPair_t4227350457 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4428[2] = 
{
	TrackableIdPair_t4227350457::get_offset_of_TrackableId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TrackableIdPair_t4227350457::get_offset_of_ResultId_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4429 = { sizeof (VuforiaRenderer_t3433045970), -1, sizeof(VuforiaRenderer_t3433045970_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4429[1] = 
{
	VuforiaRenderer_t3433045970_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4430 = { sizeof (FpsHint_t2906034572)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4430[5] = 
{
	FpsHint_t2906034572::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4431 = { sizeof (VideoBackgroundReflection_t736962841)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4431[4] = 
{
	VideoBackgroundReflection_t736962841::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4432 = { sizeof (VideoBGCfgData_t994527297)+ sizeof (Il2CppObject), sizeof(VideoBGCfgData_t994527297 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4432[4] = 
{
	VideoBGCfgData_t994527297::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoBGCfgData_t994527297::get_offset_of_size_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoBGCfgData_t994527297::get_offset_of_enabled_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoBGCfgData_t994527297::get_offset_of_reflectionInteger_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4433 = { sizeof (Vec2I_t3527036565)+ sizeof (Il2CppObject), sizeof(Vec2I_t3527036565 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4433[2] = 
{
	Vec2I_t3527036565::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vec2I_t3527036565::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4434 = { sizeof (VideoTextureInfo_t1805965052)+ sizeof (Il2CppObject), sizeof(VideoTextureInfo_t1805965052 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4434[2] = 
{
	VideoTextureInfo_t1805965052::get_offset_of_textureSize_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoTextureInfo_t1805965052::get_offset_of_imageSize_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4435 = { sizeof (RendererAPI_t402009282)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4435[5] = 
{
	RendererAPI_t402009282::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4436 = { sizeof (VuforiaRuntimeUtilities_t399660591), -1, sizeof(VuforiaRuntimeUtilities_t399660591_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4436[2] = 
{
	VuforiaRuntimeUtilities_t399660591_StaticFields::get_offset_of_sWebCamUsed_0(),
	VuforiaRuntimeUtilities_t399660591_StaticFields::get_offset_of_sNativePluginSupport_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4437 = { sizeof (InitializableBool_t3274999204)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4437[4] = 
{
	InitializableBool_t3274999204::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4438 = { sizeof (SurfaceUtilities_t1841955943), -1, sizeof(SurfaceUtilities_t1841955943_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4438[1] = 
{
	SurfaceUtilities_t1841955943_StaticFields::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4439 = { sizeof (TargetFinder_t2439332195), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4440 = { sizeof (InitState_t538152685)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4440[6] = 
{
	InitState_t538152685::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4441 = { sizeof (UpdateState_t1279515537)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4441[12] = 
{
	UpdateState_t1279515537::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4442 = { sizeof (FilterMode_t1400485161)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4442[3] = 
{
	FilterMode_t1400485161::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4443 = { sizeof (TargetSearchResult_t3441982613)+ sizeof (Il2CppObject), sizeof(TargetSearchResult_t3441982613_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4443[6] = 
{
	TargetSearchResult_t3441982613::get_offset_of_TargetName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t3441982613::get_offset_of_UniqueTargetId_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t3441982613::get_offset_of_TargetSize_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t3441982613::get_offset_of_MetaData_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t3441982613::get_offset_of_TrackingRating_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t3441982613::get_offset_of_TargetSearchResultPtr_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4444 = { sizeof (TextRecoAbstractBehaviour_t802660870), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4444[12] = 
{
	TextRecoAbstractBehaviour_t802660870::get_offset_of_mHasInitialized_2(),
	TextRecoAbstractBehaviour_t802660870::get_offset_of_mTrackerWasActiveBeforePause_3(),
	TextRecoAbstractBehaviour_t802660870::get_offset_of_mTrackerWasActiveBeforeDisabling_4(),
	TextRecoAbstractBehaviour_t802660870::get_offset_of_mWordListFile_5(),
	TextRecoAbstractBehaviour_t802660870::get_offset_of_mCustomWordListFile_6(),
	TextRecoAbstractBehaviour_t802660870::get_offset_of_mAdditionalCustomWords_7(),
	TextRecoAbstractBehaviour_t802660870::get_offset_of_mFilterMode_8(),
	TextRecoAbstractBehaviour_t802660870::get_offset_of_mFilterListFile_9(),
	TextRecoAbstractBehaviour_t802660870::get_offset_of_mAdditionalFilterWords_10(),
	TextRecoAbstractBehaviour_t802660870::get_offset_of_mWordPrefabCreationMode_11(),
	TextRecoAbstractBehaviour_t802660870::get_offset_of_mMaximumWordInstances_12(),
	TextRecoAbstractBehaviour_t802660870::get_offset_of_mTextRecoEventHandlers_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4445 = { sizeof (TextTracker_t3950053289), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4446 = { sizeof (SimpleTargetData_t4194873257)+ sizeof (Il2CppObject), sizeof(SimpleTargetData_t4194873257 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4446[2] = 
{
	SimpleTargetData_t4194873257::get_offset_of_id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SimpleTargetData_t4194873257::get_offset_of_unused_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4447 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4448 = { sizeof (TrackableBehaviour_t1113559212), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4448[8] = 
{
	TrackableBehaviour_t1113559212::get_offset_of_U3CTimeStampU3Ek__BackingField_2(),
	TrackableBehaviour_t1113559212::get_offset_of_mTrackableName_3(),
	TrackableBehaviour_t1113559212::get_offset_of_mPreserveChildSize_4(),
	TrackableBehaviour_t1113559212::get_offset_of_mInitializedInEditor_5(),
	TrackableBehaviour_t1113559212::get_offset_of_mPreviousScale_6(),
	TrackableBehaviour_t1113559212::get_offset_of_mStatus_7(),
	TrackableBehaviour_t1113559212::get_offset_of_mTrackable_8(),
	TrackableBehaviour_t1113559212::get_offset_of_mTrackableEventHandlers_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4449 = { sizeof (Status_t1100905814)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4449[7] = 
{
	Status_t1100905814::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4450 = { sizeof (CoordinateSystem_t4035406609)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4450[4] = 
{
	CoordinateSystem_t4035406609::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4451 = { sizeof (TrackableSource_t2567074243), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4452 = { sizeof (Tracker_t2709586299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4452[1] = 
{
	Tracker_t2709586299::get_offset_of_U3CIsActiveU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4453 = { sizeof (TrackerManager_t1703337244), -1, sizeof(TrackerManager_t1703337244_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4453[1] = 
{
	TrackerManager_t1703337244_StaticFields::get_offset_of_mInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4454 = { sizeof (TurnOffAbstractBehaviour_t303815290), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4455 = { sizeof (UserDefinedTargetBuildingAbstractBehaviour_t2875328746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4455[11] = 
{
	UserDefinedTargetBuildingAbstractBehaviour_t2875328746::get_offset_of_mObjectTracker_2(),
	UserDefinedTargetBuildingAbstractBehaviour_t2875328746::get_offset_of_mLastFrameQuality_3(),
	UserDefinedTargetBuildingAbstractBehaviour_t2875328746::get_offset_of_mCurrentlyScanning_4(),
	UserDefinedTargetBuildingAbstractBehaviour_t2875328746::get_offset_of_mWasScanningBeforeDisable_5(),
	UserDefinedTargetBuildingAbstractBehaviour_t2875328746::get_offset_of_mCurrentlyBuilding_6(),
	UserDefinedTargetBuildingAbstractBehaviour_t2875328746::get_offset_of_mWasBuildingBeforeDisable_7(),
	UserDefinedTargetBuildingAbstractBehaviour_t2875328746::get_offset_of_mOnInitializedCalled_8(),
	UserDefinedTargetBuildingAbstractBehaviour_t2875328746::get_offset_of_mHandlers_9(),
	UserDefinedTargetBuildingAbstractBehaviour_t2875328746::get_offset_of_StopTrackerWhileScanning_10(),
	UserDefinedTargetBuildingAbstractBehaviour_t2875328746::get_offset_of_StartScanningAutomatically_11(),
	UserDefinedTargetBuildingAbstractBehaviour_t2875328746::get_offset_of_StopScanningWhenFinshedBuilding_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4456 = { sizeof (VideoBackgroundAbstractBehaviour_t293578642), -1, sizeof(VideoBackgroundAbstractBehaviour_t293578642_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4456[12] = 
{
	VideoBackgroundAbstractBehaviour_t293578642::get_offset_of_mClearBuffers_2(),
	VideoBackgroundAbstractBehaviour_t293578642::get_offset_of_mSkipStateUpdates_3(),
	VideoBackgroundAbstractBehaviour_t293578642::get_offset_of_mVuforiaARController_4(),
	VideoBackgroundAbstractBehaviour_t293578642::get_offset_of_mCamera_5(),
	VideoBackgroundAbstractBehaviour_t293578642::get_offset_of_mBackgroundBehaviour_6(),
	VideoBackgroundAbstractBehaviour_t293578642::get_offset_of_mStereoDepth_7(),
	VideoBackgroundAbstractBehaviour_t293578642_StaticFields::get_offset_of_mFrameCounter_8(),
	VideoBackgroundAbstractBehaviour_t293578642_StaticFields::get_offset_of_mRenderCounter_9(),
	VideoBackgroundAbstractBehaviour_t293578642::get_offset_of_mResetMatrix_10(),
	VideoBackgroundAbstractBehaviour_t293578642::get_offset_of_mVuforiaFrustumSkew_11(),
	VideoBackgroundAbstractBehaviour_t293578642::get_offset_of_mCenterToEyeAxis_12(),
	VideoBackgroundAbstractBehaviour_t293578642::get_offset_of_mDisabledMeshRenderers_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4457 = { sizeof (VideoBackgroundManager_t2198727358), -1, sizeof(VideoBackgroundManager_t2198727358_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4457[8] = 
{
	VideoBackgroundManager_t2198727358::get_offset_of_mClippingMode_1(),
	VideoBackgroundManager_t2198727358::get_offset_of_mMatteShader_2(),
	VideoBackgroundManager_t2198727358::get_offset_of_mVideoBackgroundEnabled_3(),
	VideoBackgroundManager_t2198727358::get_offset_of_mTexture_4(),
	VideoBackgroundManager_t2198727358::get_offset_of_mVideoBgConfigChanged_5(),
	VideoBackgroundManager_t2198727358::get_offset_of_mNativeTexturePtr_6(),
	VideoBackgroundManager_t2198727358_StaticFields::get_offset_of_mInstance_7(),
	VideoBackgroundManager_t2198727358_StaticFields::get_offset_of_mPadlock_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4458 = { sizeof (VirtualButton_t386166510), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4458[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4459 = { sizeof (Sensitivity_t3045829715)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4459[4] = 
{
	Sensitivity_t3045829715::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4460 = { sizeof (VirtualButtonAbstractBehaviour_t2408702468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4460[15] = 
{
	0,
	VirtualButtonAbstractBehaviour_t2408702468::get_offset_of_mName_3(),
	VirtualButtonAbstractBehaviour_t2408702468::get_offset_of_mSensitivity_4(),
	VirtualButtonAbstractBehaviour_t2408702468::get_offset_of_mHasUpdatedPose_5(),
	VirtualButtonAbstractBehaviour_t2408702468::get_offset_of_mPrevTransform_6(),
	VirtualButtonAbstractBehaviour_t2408702468::get_offset_of_mPrevParent_7(),
	VirtualButtonAbstractBehaviour_t2408702468::get_offset_of_mSensitivityDirty_8(),
	VirtualButtonAbstractBehaviour_t2408702468::get_offset_of_mPreviousSensitivity_9(),
	VirtualButtonAbstractBehaviour_t2408702468::get_offset_of_mPreviouslyEnabled_10(),
	VirtualButtonAbstractBehaviour_t2408702468::get_offset_of_mPressed_11(),
	VirtualButtonAbstractBehaviour_t2408702468::get_offset_of_mHandlers_12(),
	VirtualButtonAbstractBehaviour_t2408702468::get_offset_of_mLeftTop_13(),
	VirtualButtonAbstractBehaviour_t2408702468::get_offset_of_mRightBottom_14(),
	VirtualButtonAbstractBehaviour_t2408702468::get_offset_of_mUnregisterOnDestroy_15(),
	VirtualButtonAbstractBehaviour_t2408702468::get_offset_of_mVirtualButton_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4461 = { sizeof (WebCamARController_t3718642882), -1, sizeof(WebCamARController_t3718642882_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4461[6] = 
{
	WebCamARController_t3718642882::get_offset_of_RenderTextureLayer_1(),
	WebCamARController_t3718642882::get_offset_of_mDeviceNameSetInEditor_2(),
	WebCamARController_t3718642882::get_offset_of_mFlipHorizontally_3(),
	WebCamARController_t3718642882::get_offset_of_mWebCamImpl_4(),
	WebCamARController_t3718642882_StaticFields::get_offset_of_mInstance_5(),
	WebCamARController_t3718642882_StaticFields::get_offset_of_mPadlock_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4462 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4463 = { sizeof (WordManager_t3100853168), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4464 = { sizeof (WordResult_t3640773802), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4465 = { sizeof (WordTemplateMode_t435619688)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4465[3] = 
{
	WordTemplateMode_t435619688::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4466 = { sizeof (WordAbstractBehaviour_t3502498754), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4466[3] = 
{
	WordAbstractBehaviour_t3502498754::get_offset_of_mMode_10(),
	WordAbstractBehaviour_t3502498754::get_offset_of_mSpecificWord_11(),
	WordAbstractBehaviour_t3502498754::get_offset_of_mWord_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4467 = { sizeof (WordFilterMode_t1058214526)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4467[4] = 
{
	WordFilterMode_t1058214526::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4468 = { sizeof (WordList_t3693642253), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4469 = { sizeof (EyewearCalibrationProfileManager_t947793426), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4470 = { sizeof (EyewearUserCalibrator_t2926839199), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4471 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255371), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4471[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4472 = { sizeof (__StaticArrayInitTypeSizeU3D24_t3517759979)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D24_t3517759979 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4473 = { sizeof (U3CModuleU3E_t692745553), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4474 = { sizeof (Builtins_t286914603), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4475 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4475[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4476 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4476[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4477 = { sizeof (DispatcherCache_t1010326087), -1, sizeof(DispatcherCache_t1010326087_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4477[1] = 
{
	DispatcherCache_t1010326087_StaticFields::get_offset_of__cache_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4478 = { sizeof (DispatcherFactory_t1014155341), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4479 = { sizeof (DispatcherKey_t2110064572), -1, sizeof(DispatcherKey_t2110064572_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4479[4] = 
{
	DispatcherKey_t2110064572_StaticFields::get_offset_of_EqualityComparer_0(),
	DispatcherKey_t2110064572::get_offset_of__type_1(),
	DispatcherKey_t2110064572::get_offset_of__name_2(),
	DispatcherKey_t2110064572::get_offset_of__arguments_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4480 = { sizeof (_EqualityComparer_t965810167), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4481 = { sizeof (NumericPromotions_t3533651679), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4482 = { sizeof (ExtensionRegistry_t2424660641), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4482[2] = 
{
	ExtensionRegistry_t2424660641::get_offset_of__extensions_0(),
	ExtensionRegistry_t2424660641::get_offset_of__classLock_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4483 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4484 = { sizeof (RuntimeServices_t2098243569), -1, sizeof(RuntimeServices_t2098243569_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4484[5] = 
{
	RuntimeServices_t2098243569_StaticFields::get_offset_of_NoArguments_0(),
	RuntimeServices_t2098243569_StaticFields::get_offset_of_RuntimeServicesType_1(),
	RuntimeServices_t2098243569_StaticFields::get_offset_of__cache_2(),
	RuntimeServices_t2098243569_StaticFields::get_offset_of__extensions_3(),
	RuntimeServices_t2098243569_StaticFields::get_offset_of_True_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4485 = { sizeof (U3CGetExtensionMethodsU3Ec__IteratorC_t3368530435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4485[4] = 
{
	U3CGetExtensionMethodsU3Ec__IteratorC_t3368530435::get_offset_of_U3CU24s_49U3E__0_0(),
	U3CGetExtensionMethodsU3Ec__IteratorC_t3368530435::get_offset_of_U3CmemberU3E__1_1(),
	U3CGetExtensionMethodsU3Ec__IteratorC_t3368530435::get_offset_of_U24PC_2(),
	U3CGetExtensionMethodsU3Ec__IteratorC_t3368530435::get_offset_of_U24current_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4486 = { sizeof (U3CCoerceU3Ec__AnonStorey1D_t572148199), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4486[2] = 
{
	U3CCoerceU3Ec__AnonStorey1D_t572148199::get_offset_of_value_0(),
	U3CCoerceU3Ec__AnonStorey1D_t572148199::get_offset_of_toType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4487 = { sizeof (U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t4009522197), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4487[1] = 
{
	U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t4009522197::get_offset_of_method_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4488 = { sizeof (Dispatcher_t684365006), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4489 = { sizeof (U3CModuleU3E_t692745554), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4490 = { sizeof (Locale_t4128636115), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4491 = { sizeof (MonoTODOAttribute_t4131080593), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4491[1] = 
{
	MonoTODOAttribute_t4131080593::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4492 = { sizeof (MonoLimitationAttribute_t3672514600), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4493 = { sizeof (CodeArgumentReference_t1456663465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4493[3] = 
{
	CodeArgumentReference_t1456663465::get_offset_of_type_0(),
	CodeArgumentReference_t1456663465::get_offset_of_argNum_1(),
	CodeArgumentReference_t1456663465::get_offset_of_name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4494 = { sizeof (CodeArrayItem_t2712318199), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4494[2] = 
{
	CodeArrayItem_t2712318199::get_offset_of_array_0(),
	CodeArrayItem_t2712318199::get_offset_of_index_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4495 = { sizeof (CodeAssignment_t720477174), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4495[2] = 
{
	CodeAssignment_t720477174::get_offset_of_var_0(),
	CodeAssignment_t720477174::get_offset_of_exp_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4496 = { sizeof (CodeBlock_t2718634139), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4496[1] = 
{
	CodeBlock_t2718634139::get_offset_of_statements_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4497 = { sizeof (CodeBuilder_t3190018006), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4497[7] = 
{
	CodeBuilder_t3190018006::get_offset_of_mainBlock_0(),
	CodeBuilder_t3190018006::get_offset_of_currentBlock_1(),
	CodeBuilder_t3190018006::get_offset_of_blockStack_2(),
	CodeBuilder_t3190018006::get_offset_of_returnLabel_3(),
	CodeBuilder_t3190018006::get_offset_of_nestedIfs_4(),
	CodeBuilder_t3190018006::get_offset_of_currentIfSerie_5(),
	CodeBuilder_t3190018006::get_offset_of_codeClass_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4498 = { sizeof (CodeCast_t3030498946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4498[2] = 
{
	CodeCast_t3030498946::get_offset_of_type_0(),
	CodeCast_t3030498946::get_offset_of_exp_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4499 = { sizeof (CodeClass_t2288932064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4499[10] = 
{
	CodeClass_t2288932064::get_offset_of_typeBuilder_0(),
	CodeClass_t2288932064::get_offset_of_customAttributes_1(),
	CodeClass_t2288932064::get_offset_of_methods_2(),
	CodeClass_t2288932064::get_offset_of_properties_3(),
	CodeClass_t2288932064::get_offset_of_fields_4(),
	CodeClass_t2288932064::get_offset_of_fieldAttributes_5(),
	CodeClass_t2288932064::get_offset_of_baseType_6(),
	CodeClass_t2288932064::get_offset_of_interfaces_7(),
	CodeClass_t2288932064::get_offset_of_ctor_8(),
	CodeClass_t2288932064::get_offset_of_instanceInit_9(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
