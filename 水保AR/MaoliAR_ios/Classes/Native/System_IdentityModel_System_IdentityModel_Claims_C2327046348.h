﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.IdentityModel.Claims.Claim/ClaimComparer
struct ClaimComparer_t870884883;
// System.IdentityModel.Claims.Claim
struct Claim_t2327046348;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Claims.Claim
struct  Claim_t2327046348  : public Il2CppObject
{
public:
	// System.String System.IdentityModel.Claims.Claim::claim_type
	String_t* ___claim_type_2;
	// System.Object System.IdentityModel.Claims.Claim::resource
	Il2CppObject * ___resource_3;
	// System.String System.IdentityModel.Claims.Claim::right
	String_t* ___right_4;

public:
	inline static int32_t get_offset_of_claim_type_2() { return static_cast<int32_t>(offsetof(Claim_t2327046348, ___claim_type_2)); }
	inline String_t* get_claim_type_2() const { return ___claim_type_2; }
	inline String_t** get_address_of_claim_type_2() { return &___claim_type_2; }
	inline void set_claim_type_2(String_t* value)
	{
		___claim_type_2 = value;
		Il2CppCodeGenWriteBarrier(&___claim_type_2, value);
	}

	inline static int32_t get_offset_of_resource_3() { return static_cast<int32_t>(offsetof(Claim_t2327046348, ___resource_3)); }
	inline Il2CppObject * get_resource_3() const { return ___resource_3; }
	inline Il2CppObject ** get_address_of_resource_3() { return &___resource_3; }
	inline void set_resource_3(Il2CppObject * value)
	{
		___resource_3 = value;
		Il2CppCodeGenWriteBarrier(&___resource_3, value);
	}

	inline static int32_t get_offset_of_right_4() { return static_cast<int32_t>(offsetof(Claim_t2327046348, ___right_4)); }
	inline String_t* get_right_4() const { return ___right_4; }
	inline String_t** get_address_of_right_4() { return &___right_4; }
	inline void set_right_4(String_t* value)
	{
		___right_4 = value;
		Il2CppCodeGenWriteBarrier(&___right_4, value);
	}
};

struct Claim_t2327046348_StaticFields
{
public:
	// System.IdentityModel.Claims.Claim/ClaimComparer System.IdentityModel.Claims.Claim::default_comparer
	ClaimComparer_t870884883 * ___default_comparer_0;
	// System.IdentityModel.Claims.Claim System.IdentityModel.Claims.Claim::system
	Claim_t2327046348 * ___system_1;

public:
	inline static int32_t get_offset_of_default_comparer_0() { return static_cast<int32_t>(offsetof(Claim_t2327046348_StaticFields, ___default_comparer_0)); }
	inline ClaimComparer_t870884883 * get_default_comparer_0() const { return ___default_comparer_0; }
	inline ClaimComparer_t870884883 ** get_address_of_default_comparer_0() { return &___default_comparer_0; }
	inline void set_default_comparer_0(ClaimComparer_t870884883 * value)
	{
		___default_comparer_0 = value;
		Il2CppCodeGenWriteBarrier(&___default_comparer_0, value);
	}

	inline static int32_t get_offset_of_system_1() { return static_cast<int32_t>(offsetof(Claim_t2327046348_StaticFields, ___system_1)); }
	inline Claim_t2327046348 * get_system_1() const { return ___system_1; }
	inline Claim_t2327046348 ** get_address_of_system_1() { return &___system_1; }
	inline void set_system_1(Claim_t2327046348 * value)
	{
		___system_1 = value;
		Il2CppCodeGenWriteBarrier(&___system_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
