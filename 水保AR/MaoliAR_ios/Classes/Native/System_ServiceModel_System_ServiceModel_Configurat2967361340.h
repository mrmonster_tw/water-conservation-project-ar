﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_ComponentModel_TypeConverter2249118273.h"

// System.ServiceModel.Configuration.EncodingConverter
struct EncodingConverter_t2967361340;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.EncodingConverter
struct  EncodingConverter_t2967361340  : public TypeConverter_t2249118273
{
public:

public:
};

struct EncodingConverter_t2967361340_StaticFields
{
public:
	// System.ServiceModel.Configuration.EncodingConverter System.ServiceModel.Configuration.EncodingConverter::_instance
	EncodingConverter_t2967361340 * ____instance_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Configuration.EncodingConverter::<>f__switch$mapF
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24mapF_1;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(EncodingConverter_t2967361340_StaticFields, ____instance_0)); }
	inline EncodingConverter_t2967361340 * get__instance_0() const { return ____instance_0; }
	inline EncodingConverter_t2967361340 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(EncodingConverter_t2967361340 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapF_1() { return static_cast<int32_t>(offsetof(EncodingConverter_t2967361340_StaticFields, ___U3CU3Ef__switchU24mapF_1)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24mapF_1() const { return ___U3CU3Ef__switchU24mapF_1; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24mapF_1() { return &___U3CU3Ef__switchU24mapF_1; }
	inline void set_U3CU3Ef__switchU24mapF_1(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24mapF_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapF_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
