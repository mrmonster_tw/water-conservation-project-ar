﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Operations_XslCompiledElemen50593777.h"

// System.IO.TextWriter
struct TextWriter_t3478189236;
// Mono.Xml.Xsl.Operations.XslOperation
struct XslOperation_t2153241355;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslMessage
struct  XslMessage_t3346364917  : public XslCompiledElement_t50593777
{
public:
	// System.Boolean Mono.Xml.Xsl.Operations.XslMessage::terminate
	bool ___terminate_4;
	// Mono.Xml.Xsl.Operations.XslOperation Mono.Xml.Xsl.Operations.XslMessage::children
	XslOperation_t2153241355 * ___children_5;

public:
	inline static int32_t get_offset_of_terminate_4() { return static_cast<int32_t>(offsetof(XslMessage_t3346364917, ___terminate_4)); }
	inline bool get_terminate_4() const { return ___terminate_4; }
	inline bool* get_address_of_terminate_4() { return &___terminate_4; }
	inline void set_terminate_4(bool value)
	{
		___terminate_4 = value;
	}

	inline static int32_t get_offset_of_children_5() { return static_cast<int32_t>(offsetof(XslMessage_t3346364917, ___children_5)); }
	inline XslOperation_t2153241355 * get_children_5() const { return ___children_5; }
	inline XslOperation_t2153241355 ** get_address_of_children_5() { return &___children_5; }
	inline void set_children_5(XslOperation_t2153241355 * value)
	{
		___children_5 = value;
		Il2CppCodeGenWriteBarrier(&___children_5, value);
	}
};

struct XslMessage_t3346364917_StaticFields
{
public:
	// System.IO.TextWriter Mono.Xml.Xsl.Operations.XslMessage::output
	TextWriter_t3478189236 * ___output_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.Operations.XslMessage::<>f__switch$mapA
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24mapA_6;

public:
	inline static int32_t get_offset_of_output_3() { return static_cast<int32_t>(offsetof(XslMessage_t3346364917_StaticFields, ___output_3)); }
	inline TextWriter_t3478189236 * get_output_3() const { return ___output_3; }
	inline TextWriter_t3478189236 ** get_address_of_output_3() { return &___output_3; }
	inline void set_output_3(TextWriter_t3478189236 * value)
	{
		___output_3 = value;
		Il2CppCodeGenWriteBarrier(&___output_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapA_6() { return static_cast<int32_t>(offsetof(XslMessage_t3346364917_StaticFields, ___U3CU3Ef__switchU24mapA_6)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24mapA_6() const { return ___U3CU3Ef__switchU24mapA_6; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24mapA_6() { return &___U3CU3Ef__switchU24mapA_6; }
	inline void set_U3CU3Ef__switchU24mapA_6(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24mapA_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapA_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
