﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Security.TrustVersion
struct TrustVersion_t3017348835;
// System.Xml.XmlDictionaryString
struct XmlDictionaryString_t3504120266;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.TrustVersion
struct  TrustVersion_t3017348835  : public Il2CppObject
{
public:
	// System.Xml.XmlDictionaryString System.ServiceModel.Security.TrustVersion::<Namespace>k__BackingField
	XmlDictionaryString_t3504120266 * ___U3CNamespaceU3Ek__BackingField_3;
	// System.Xml.XmlDictionaryString System.ServiceModel.Security.TrustVersion::<Prefix>k__BackingField
	XmlDictionaryString_t3504120266 * ___U3CPrefixU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CNamespaceU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TrustVersion_t3017348835, ___U3CNamespaceU3Ek__BackingField_3)); }
	inline XmlDictionaryString_t3504120266 * get_U3CNamespaceU3Ek__BackingField_3() const { return ___U3CNamespaceU3Ek__BackingField_3; }
	inline XmlDictionaryString_t3504120266 ** get_address_of_U3CNamespaceU3Ek__BackingField_3() { return &___U3CNamespaceU3Ek__BackingField_3; }
	inline void set_U3CNamespaceU3Ek__BackingField_3(XmlDictionaryString_t3504120266 * value)
	{
		___U3CNamespaceU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CNamespaceU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CPrefixU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TrustVersion_t3017348835, ___U3CPrefixU3Ek__BackingField_4)); }
	inline XmlDictionaryString_t3504120266 * get_U3CPrefixU3Ek__BackingField_4() const { return ___U3CPrefixU3Ek__BackingField_4; }
	inline XmlDictionaryString_t3504120266 ** get_address_of_U3CPrefixU3Ek__BackingField_4() { return &___U3CPrefixU3Ek__BackingField_4; }
	inline void set_U3CPrefixU3Ek__BackingField_4(XmlDictionaryString_t3504120266 * value)
	{
		___U3CPrefixU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPrefixU3Ek__BackingField_4, value);
	}
};

struct TrustVersion_t3017348835_StaticFields
{
public:
	// System.ServiceModel.Security.TrustVersion System.ServiceModel.Security.TrustVersion::<Default>k__BackingField
	TrustVersion_t3017348835 * ___U3CDefaultU3Ek__BackingField_0;
	// System.ServiceModel.Security.TrustVersion System.ServiceModel.Security.TrustVersion::<WSTrust13>k__BackingField
	TrustVersion_t3017348835 * ___U3CWSTrust13U3Ek__BackingField_1;
	// System.ServiceModel.Security.TrustVersion System.ServiceModel.Security.TrustVersion::<WSTrustFeb2005>k__BackingField
	TrustVersion_t3017348835 * ___U3CWSTrustFeb2005U3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CDefaultU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TrustVersion_t3017348835_StaticFields, ___U3CDefaultU3Ek__BackingField_0)); }
	inline TrustVersion_t3017348835 * get_U3CDefaultU3Ek__BackingField_0() const { return ___U3CDefaultU3Ek__BackingField_0; }
	inline TrustVersion_t3017348835 ** get_address_of_U3CDefaultU3Ek__BackingField_0() { return &___U3CDefaultU3Ek__BackingField_0; }
	inline void set_U3CDefaultU3Ek__BackingField_0(TrustVersion_t3017348835 * value)
	{
		___U3CDefaultU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDefaultU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CWSTrust13U3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TrustVersion_t3017348835_StaticFields, ___U3CWSTrust13U3Ek__BackingField_1)); }
	inline TrustVersion_t3017348835 * get_U3CWSTrust13U3Ek__BackingField_1() const { return ___U3CWSTrust13U3Ek__BackingField_1; }
	inline TrustVersion_t3017348835 ** get_address_of_U3CWSTrust13U3Ek__BackingField_1() { return &___U3CWSTrust13U3Ek__BackingField_1; }
	inline void set_U3CWSTrust13U3Ek__BackingField_1(TrustVersion_t3017348835 * value)
	{
		___U3CWSTrust13U3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CWSTrust13U3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CWSTrustFeb2005U3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TrustVersion_t3017348835_StaticFields, ___U3CWSTrustFeb2005U3Ek__BackingField_2)); }
	inline TrustVersion_t3017348835 * get_U3CWSTrustFeb2005U3Ek__BackingField_2() const { return ___U3CWSTrustFeb2005U3Ek__BackingField_2; }
	inline TrustVersion_t3017348835 ** get_address_of_U3CWSTrustFeb2005U3Ek__BackingField_2() { return &___U3CWSTrustFeb2005U3Ek__BackingField_2; }
	inline void set_U3CWSTrustFeb2005U3Ek__BackingField_2(TrustVersion_t3017348835 * value)
	{
		___U3CWSTrustFeb2005U3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CWSTrustFeb2005U3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
