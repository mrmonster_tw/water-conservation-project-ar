﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Policy_A2521326227.h"

// System.IdentityModel.Policy.DefaultEvaluationContext
struct DefaultEvaluationContext_t426032594;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Policy.AuthorizationContext/DefaultAuthorizationContext
struct  DefaultAuthorizationContext_t3136051856  : public AuthorizationContext_t2521326227
{
public:
	// System.IdentityModel.Policy.DefaultEvaluationContext System.IdentityModel.Policy.AuthorizationContext/DefaultAuthorizationContext::ctx
	DefaultEvaluationContext_t426032594 * ___ctx_0;
	// System.String System.IdentityModel.Policy.AuthorizationContext/DefaultAuthorizationContext::id
	String_t* ___id_1;

public:
	inline static int32_t get_offset_of_ctx_0() { return static_cast<int32_t>(offsetof(DefaultAuthorizationContext_t3136051856, ___ctx_0)); }
	inline DefaultEvaluationContext_t426032594 * get_ctx_0() const { return ___ctx_0; }
	inline DefaultEvaluationContext_t426032594 ** get_address_of_ctx_0() { return &___ctx_0; }
	inline void set_ctx_0(DefaultEvaluationContext_t426032594 * value)
	{
		___ctx_0 = value;
		Il2CppCodeGenWriteBarrier(&___ctx_0, value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(DefaultAuthorizationContext_t3136051856, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier(&___id_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
