﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Security_Se807295854.h"

// System.ServiceModel.Security.UserNamePasswordClientCredential
struct UserNamePasswordClientCredential_t4222120774;
// System.ServiceModel.Security.IssuedTokenClientCredential
struct IssuedTokenClientCredential_t2331807164;
// System.ServiceModel.Security.HttpDigestClientCredential
struct HttpDigestClientCredential_t4244309379;
// System.ServiceModel.Security.X509CertificateInitiatorClientCredential
struct X509CertificateInitiatorClientCredential_t679231977;
// System.ServiceModel.Security.X509CertificateRecipientClientCredential
struct X509CertificateRecipientClientCredential_t850579047;
// System.ServiceModel.Security.WindowsClientCredential
struct WindowsClientCredential_t2902646674;
// System.ServiceModel.Security.PeerCredential
struct PeerCredential_t4076012648;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.ClientCredentials
struct  ClientCredentials_t1418047401  : public SecurityCredentialsManager_t807295854
{
public:
	// System.ServiceModel.Security.UserNamePasswordClientCredential System.ServiceModel.Description.ClientCredentials::userpass
	UserNamePasswordClientCredential_t4222120774 * ___userpass_0;
	// System.ServiceModel.Security.IssuedTokenClientCredential System.ServiceModel.Description.ClientCredentials::issued_token
	IssuedTokenClientCredential_t2331807164 * ___issued_token_1;
	// System.ServiceModel.Security.HttpDigestClientCredential System.ServiceModel.Description.ClientCredentials::digest
	HttpDigestClientCredential_t4244309379 * ___digest_2;
	// System.ServiceModel.Security.X509CertificateInitiatorClientCredential System.ServiceModel.Description.ClientCredentials::initiator
	X509CertificateInitiatorClientCredential_t679231977 * ___initiator_3;
	// System.ServiceModel.Security.X509CertificateRecipientClientCredential System.ServiceModel.Description.ClientCredentials::recipient
	X509CertificateRecipientClientCredential_t850579047 * ___recipient_4;
	// System.ServiceModel.Security.WindowsClientCredential System.ServiceModel.Description.ClientCredentials::windows
	WindowsClientCredential_t2902646674 * ___windows_5;
	// System.ServiceModel.Security.PeerCredential System.ServiceModel.Description.ClientCredentials::peer
	PeerCredential_t4076012648 * ___peer_6;
	// System.Boolean System.ServiceModel.Description.ClientCredentials::support_interactive
	bool ___support_interactive_7;

public:
	inline static int32_t get_offset_of_userpass_0() { return static_cast<int32_t>(offsetof(ClientCredentials_t1418047401, ___userpass_0)); }
	inline UserNamePasswordClientCredential_t4222120774 * get_userpass_0() const { return ___userpass_0; }
	inline UserNamePasswordClientCredential_t4222120774 ** get_address_of_userpass_0() { return &___userpass_0; }
	inline void set_userpass_0(UserNamePasswordClientCredential_t4222120774 * value)
	{
		___userpass_0 = value;
		Il2CppCodeGenWriteBarrier(&___userpass_0, value);
	}

	inline static int32_t get_offset_of_issued_token_1() { return static_cast<int32_t>(offsetof(ClientCredentials_t1418047401, ___issued_token_1)); }
	inline IssuedTokenClientCredential_t2331807164 * get_issued_token_1() const { return ___issued_token_1; }
	inline IssuedTokenClientCredential_t2331807164 ** get_address_of_issued_token_1() { return &___issued_token_1; }
	inline void set_issued_token_1(IssuedTokenClientCredential_t2331807164 * value)
	{
		___issued_token_1 = value;
		Il2CppCodeGenWriteBarrier(&___issued_token_1, value);
	}

	inline static int32_t get_offset_of_digest_2() { return static_cast<int32_t>(offsetof(ClientCredentials_t1418047401, ___digest_2)); }
	inline HttpDigestClientCredential_t4244309379 * get_digest_2() const { return ___digest_2; }
	inline HttpDigestClientCredential_t4244309379 ** get_address_of_digest_2() { return &___digest_2; }
	inline void set_digest_2(HttpDigestClientCredential_t4244309379 * value)
	{
		___digest_2 = value;
		Il2CppCodeGenWriteBarrier(&___digest_2, value);
	}

	inline static int32_t get_offset_of_initiator_3() { return static_cast<int32_t>(offsetof(ClientCredentials_t1418047401, ___initiator_3)); }
	inline X509CertificateInitiatorClientCredential_t679231977 * get_initiator_3() const { return ___initiator_3; }
	inline X509CertificateInitiatorClientCredential_t679231977 ** get_address_of_initiator_3() { return &___initiator_3; }
	inline void set_initiator_3(X509CertificateInitiatorClientCredential_t679231977 * value)
	{
		___initiator_3 = value;
		Il2CppCodeGenWriteBarrier(&___initiator_3, value);
	}

	inline static int32_t get_offset_of_recipient_4() { return static_cast<int32_t>(offsetof(ClientCredentials_t1418047401, ___recipient_4)); }
	inline X509CertificateRecipientClientCredential_t850579047 * get_recipient_4() const { return ___recipient_4; }
	inline X509CertificateRecipientClientCredential_t850579047 ** get_address_of_recipient_4() { return &___recipient_4; }
	inline void set_recipient_4(X509CertificateRecipientClientCredential_t850579047 * value)
	{
		___recipient_4 = value;
		Il2CppCodeGenWriteBarrier(&___recipient_4, value);
	}

	inline static int32_t get_offset_of_windows_5() { return static_cast<int32_t>(offsetof(ClientCredentials_t1418047401, ___windows_5)); }
	inline WindowsClientCredential_t2902646674 * get_windows_5() const { return ___windows_5; }
	inline WindowsClientCredential_t2902646674 ** get_address_of_windows_5() { return &___windows_5; }
	inline void set_windows_5(WindowsClientCredential_t2902646674 * value)
	{
		___windows_5 = value;
		Il2CppCodeGenWriteBarrier(&___windows_5, value);
	}

	inline static int32_t get_offset_of_peer_6() { return static_cast<int32_t>(offsetof(ClientCredentials_t1418047401, ___peer_6)); }
	inline PeerCredential_t4076012648 * get_peer_6() const { return ___peer_6; }
	inline PeerCredential_t4076012648 ** get_address_of_peer_6() { return &___peer_6; }
	inline void set_peer_6(PeerCredential_t4076012648 * value)
	{
		___peer_6 = value;
		Il2CppCodeGenWriteBarrier(&___peer_6, value);
	}

	inline static int32_t get_offset_of_support_interactive_7() { return static_cast<int32_t>(offsetof(ClientCredentials_t1418047401, ___support_interactive_7)); }
	inline bool get_support_interactive_7() const { return ___support_interactive_7; }
	inline bool* get_address_of_support_interactive_7() { return &___support_interactive_7; }
	inline void set_support_interactive_7(bool value)
	{
		___support_interactive_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
