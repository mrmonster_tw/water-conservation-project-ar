﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Operations_XslCompiledElemen50593777.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslNumberingLev1604989502.h"

// Mono.Xml.XPath.Pattern
struct Pattern_t1136864796;
// System.Xml.XPath.XPathExpression
struct XPathExpression_t1723793351;
// Mono.Xml.Xsl.Operations.XslAvt
struct XslAvt_t1645109359;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslNumber
struct  XslNumber_t2577579437  : public XslCompiledElement_t50593777
{
public:
	// Mono.Xml.Xsl.Operations.XslNumberingLevel Mono.Xml.Xsl.Operations.XslNumber::level
	int32_t ___level_3;
	// Mono.Xml.XPath.Pattern Mono.Xml.Xsl.Operations.XslNumber::count
	Pattern_t1136864796 * ___count_4;
	// Mono.Xml.XPath.Pattern Mono.Xml.Xsl.Operations.XslNumber::from
	Pattern_t1136864796 * ___from_5;
	// System.Xml.XPath.XPathExpression Mono.Xml.Xsl.Operations.XslNumber::value
	XPathExpression_t1723793351 * ___value_6;
	// Mono.Xml.Xsl.Operations.XslAvt Mono.Xml.Xsl.Operations.XslNumber::format
	XslAvt_t1645109359 * ___format_7;
	// Mono.Xml.Xsl.Operations.XslAvt Mono.Xml.Xsl.Operations.XslNumber::lang
	XslAvt_t1645109359 * ___lang_8;
	// Mono.Xml.Xsl.Operations.XslAvt Mono.Xml.Xsl.Operations.XslNumber::letterValue
	XslAvt_t1645109359 * ___letterValue_9;
	// Mono.Xml.Xsl.Operations.XslAvt Mono.Xml.Xsl.Operations.XslNumber::groupingSeparator
	XslAvt_t1645109359 * ___groupingSeparator_10;
	// Mono.Xml.Xsl.Operations.XslAvt Mono.Xml.Xsl.Operations.XslNumber::groupingSize
	XslAvt_t1645109359 * ___groupingSize_11;

public:
	inline static int32_t get_offset_of_level_3() { return static_cast<int32_t>(offsetof(XslNumber_t2577579437, ___level_3)); }
	inline int32_t get_level_3() const { return ___level_3; }
	inline int32_t* get_address_of_level_3() { return &___level_3; }
	inline void set_level_3(int32_t value)
	{
		___level_3 = value;
	}

	inline static int32_t get_offset_of_count_4() { return static_cast<int32_t>(offsetof(XslNumber_t2577579437, ___count_4)); }
	inline Pattern_t1136864796 * get_count_4() const { return ___count_4; }
	inline Pattern_t1136864796 ** get_address_of_count_4() { return &___count_4; }
	inline void set_count_4(Pattern_t1136864796 * value)
	{
		___count_4 = value;
		Il2CppCodeGenWriteBarrier(&___count_4, value);
	}

	inline static int32_t get_offset_of_from_5() { return static_cast<int32_t>(offsetof(XslNumber_t2577579437, ___from_5)); }
	inline Pattern_t1136864796 * get_from_5() const { return ___from_5; }
	inline Pattern_t1136864796 ** get_address_of_from_5() { return &___from_5; }
	inline void set_from_5(Pattern_t1136864796 * value)
	{
		___from_5 = value;
		Il2CppCodeGenWriteBarrier(&___from_5, value);
	}

	inline static int32_t get_offset_of_value_6() { return static_cast<int32_t>(offsetof(XslNumber_t2577579437, ___value_6)); }
	inline XPathExpression_t1723793351 * get_value_6() const { return ___value_6; }
	inline XPathExpression_t1723793351 ** get_address_of_value_6() { return &___value_6; }
	inline void set_value_6(XPathExpression_t1723793351 * value)
	{
		___value_6 = value;
		Il2CppCodeGenWriteBarrier(&___value_6, value);
	}

	inline static int32_t get_offset_of_format_7() { return static_cast<int32_t>(offsetof(XslNumber_t2577579437, ___format_7)); }
	inline XslAvt_t1645109359 * get_format_7() const { return ___format_7; }
	inline XslAvt_t1645109359 ** get_address_of_format_7() { return &___format_7; }
	inline void set_format_7(XslAvt_t1645109359 * value)
	{
		___format_7 = value;
		Il2CppCodeGenWriteBarrier(&___format_7, value);
	}

	inline static int32_t get_offset_of_lang_8() { return static_cast<int32_t>(offsetof(XslNumber_t2577579437, ___lang_8)); }
	inline XslAvt_t1645109359 * get_lang_8() const { return ___lang_8; }
	inline XslAvt_t1645109359 ** get_address_of_lang_8() { return &___lang_8; }
	inline void set_lang_8(XslAvt_t1645109359 * value)
	{
		___lang_8 = value;
		Il2CppCodeGenWriteBarrier(&___lang_8, value);
	}

	inline static int32_t get_offset_of_letterValue_9() { return static_cast<int32_t>(offsetof(XslNumber_t2577579437, ___letterValue_9)); }
	inline XslAvt_t1645109359 * get_letterValue_9() const { return ___letterValue_9; }
	inline XslAvt_t1645109359 ** get_address_of_letterValue_9() { return &___letterValue_9; }
	inline void set_letterValue_9(XslAvt_t1645109359 * value)
	{
		___letterValue_9 = value;
		Il2CppCodeGenWriteBarrier(&___letterValue_9, value);
	}

	inline static int32_t get_offset_of_groupingSeparator_10() { return static_cast<int32_t>(offsetof(XslNumber_t2577579437, ___groupingSeparator_10)); }
	inline XslAvt_t1645109359 * get_groupingSeparator_10() const { return ___groupingSeparator_10; }
	inline XslAvt_t1645109359 ** get_address_of_groupingSeparator_10() { return &___groupingSeparator_10; }
	inline void set_groupingSeparator_10(XslAvt_t1645109359 * value)
	{
		___groupingSeparator_10 = value;
		Il2CppCodeGenWriteBarrier(&___groupingSeparator_10, value);
	}

	inline static int32_t get_offset_of_groupingSize_11() { return static_cast<int32_t>(offsetof(XslNumber_t2577579437, ___groupingSize_11)); }
	inline XslAvt_t1645109359 * get_groupingSize_11() const { return ___groupingSize_11; }
	inline XslAvt_t1645109359 ** get_address_of_groupingSize_11() { return &___groupingSize_11; }
	inline void set_groupingSize_11(XslAvt_t1645109359 * value)
	{
		___groupingSize_11 = value;
		Il2CppCodeGenWriteBarrier(&___groupingSize_11, value);
	}
};

struct XslNumber_t2577579437_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.Operations.XslNumber::<>f__switch$mapB
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24mapB_12;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapB_12() { return static_cast<int32_t>(offsetof(XslNumber_t2577579437_StaticFields, ___U3CU3Ef__switchU24mapB_12)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24mapB_12() const { return ___U3CU3Ef__switchU24mapB_12; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24mapB_12() { return &___U3CU3Ef__switchU24mapB_12; }
	inline void set_U3CU3Ef__switchU24mapB_12(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24mapB_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapB_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
