﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.ServiceSecurityContext
struct ServiceSecurityContext_t3919255606;
// System.IdentityModel.Policy.AuthorizationContext
struct AuthorizationContext_t2521326227;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IdentityModel.Policy.IAuthorizationPolicy>
struct ReadOnlyCollection_1_t778487864;
// System.Security.Principal.IIdentity
struct IIdentity_t2948385546;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.ServiceSecurityContext
struct  ServiceSecurityContext_t3919255606  : public Il2CppObject
{
public:
	// System.IdentityModel.Policy.AuthorizationContext System.ServiceModel.ServiceSecurityContext::context
	AuthorizationContext_t2521326227 * ___context_1;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IdentityModel.Policy.IAuthorizationPolicy> System.ServiceModel.ServiceSecurityContext::policies
	ReadOnlyCollection_1_t778487864 * ___policies_2;
	// System.Security.Principal.IIdentity System.ServiceModel.ServiceSecurityContext::primary_identity
	Il2CppObject * ___primary_identity_3;

public:
	inline static int32_t get_offset_of_context_1() { return static_cast<int32_t>(offsetof(ServiceSecurityContext_t3919255606, ___context_1)); }
	inline AuthorizationContext_t2521326227 * get_context_1() const { return ___context_1; }
	inline AuthorizationContext_t2521326227 ** get_address_of_context_1() { return &___context_1; }
	inline void set_context_1(AuthorizationContext_t2521326227 * value)
	{
		___context_1 = value;
		Il2CppCodeGenWriteBarrier(&___context_1, value);
	}

	inline static int32_t get_offset_of_policies_2() { return static_cast<int32_t>(offsetof(ServiceSecurityContext_t3919255606, ___policies_2)); }
	inline ReadOnlyCollection_1_t778487864 * get_policies_2() const { return ___policies_2; }
	inline ReadOnlyCollection_1_t778487864 ** get_address_of_policies_2() { return &___policies_2; }
	inline void set_policies_2(ReadOnlyCollection_1_t778487864 * value)
	{
		___policies_2 = value;
		Il2CppCodeGenWriteBarrier(&___policies_2, value);
	}

	inline static int32_t get_offset_of_primary_identity_3() { return static_cast<int32_t>(offsetof(ServiceSecurityContext_t3919255606, ___primary_identity_3)); }
	inline Il2CppObject * get_primary_identity_3() const { return ___primary_identity_3; }
	inline Il2CppObject ** get_address_of_primary_identity_3() { return &___primary_identity_3; }
	inline void set_primary_identity_3(Il2CppObject * value)
	{
		___primary_identity_3 = value;
		Il2CppCodeGenWriteBarrier(&___primary_identity_3, value);
	}
};

struct ServiceSecurityContext_t3919255606_StaticFields
{
public:
	// System.ServiceModel.ServiceSecurityContext System.ServiceModel.ServiceSecurityContext::anonymous
	ServiceSecurityContext_t3919255606 * ___anonymous_0;

public:
	inline static int32_t get_offset_of_anonymous_0() { return static_cast<int32_t>(offsetof(ServiceSecurityContext_t3919255606_StaticFields, ___anonymous_0)); }
	inline ServiceSecurityContext_t3919255606 * get_anonymous_0() const { return ___anonymous_0; }
	inline ServiceSecurityContext_t3919255606 ** get_address_of_anonymous_0() { return &___anonymous_0; }
	inline void set_anonymous_0(ServiceSecurityContext_t3919255606 * value)
	{
		___anonymous_0 = value;
		Il2CppCodeGenWriteBarrier(&___anonymous_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
