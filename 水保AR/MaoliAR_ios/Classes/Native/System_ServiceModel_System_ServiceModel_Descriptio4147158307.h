﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_DateTime3738529785.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.WstLifetime
struct  WstLifetime_t4147158307  : public Il2CppObject
{
public:
	// System.DateTime System.ServiceModel.Description.WstLifetime::Created
	DateTime_t3738529785  ___Created_0;
	// System.DateTime System.ServiceModel.Description.WstLifetime::Expires
	DateTime_t3738529785  ___Expires_1;

public:
	inline static int32_t get_offset_of_Created_0() { return static_cast<int32_t>(offsetof(WstLifetime_t4147158307, ___Created_0)); }
	inline DateTime_t3738529785  get_Created_0() const { return ___Created_0; }
	inline DateTime_t3738529785 * get_address_of_Created_0() { return &___Created_0; }
	inline void set_Created_0(DateTime_t3738529785  value)
	{
		___Created_0 = value;
	}

	inline static int32_t get_offset_of_Expires_1() { return static_cast<int32_t>(offsetof(WstLifetime_t4147158307, ___Expires_1)); }
	inline DateTime_t3738529785  get_Expires_1() const { return ___Expires_1; }
	inline DateTime_t3738529785 * get_address_of_Expires_1() { return &___Expires_1; }
	inline void set_Expires_1(DateTime_t3738529785  value)
	{
		___Expires_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
