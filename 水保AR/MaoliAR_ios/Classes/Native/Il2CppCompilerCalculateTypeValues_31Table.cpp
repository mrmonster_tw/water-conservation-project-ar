﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Runtime_Serialization_System_Xml_XmlBinaryR3909229952.h"
#include "System_Runtime_Serialization_System_Xml_XmlBinaryW1730638232.h"
#include "System_Runtime_Serialization_System_Xml_XmlDictiona238028028.h"
#include "System_Runtime_Serialization_System_Xml_XmlDiction1467978489.h"
#include "System_Runtime_Serialization_System_Xml_XmlDiction1044334689.h"
#include "System_Runtime_Serialization_System_Xml_XmlDictiona173030297.h"
#include "System_Runtime_Serialization_System_Xml_XmlDiction3504120266.h"
#include "System_Runtime_Serialization_System_Xml_XmlDiction1958650860.h"
#include "System_Runtime_Serialization_System_Xml_XmlMtomDic3282849429.h"
#include "System_Runtime_Serialization_System_Xml_NonInterac4263140638.h"
#include "System_Runtime_Serialization_System_Xml_MultiParte1435767550.h"
#include "System_Runtime_Serialization_System_Xml_DummyState3823067943.h"
#include "System_Runtime_Serialization_System_Xml_MimeEncoded622371820.h"
#include "System_Runtime_Serialization_System_Xml_XmlMtomDict545583934.h"
#include "System_Runtime_Serialization_System_Xml_XmlSimpleD1404946835.h"
#include "System_Runtime_Serialization_System_Xml_XmlSimpleD1777131310.h"
#include "System_Runtime_Serialization_System_Xml_OnXmlDicti3234185550.h"
#include "System_Runtime_Serialization_U3CPrivateImplementat3057255361.h"
#include "System_Runtime_Serialization_U3CPrivateImplementat3244137463.h"
#include "System_Web_U3CModuleU3E692745525.h"
#include "System_Web_System_Web_UplevelHelper1754666578.h"
#include "System_Web_Locale4128636107.h"
#include "System_Web_System_MonoTODOAttribute4131080581.h"
#include "System_Web_System_Resources_ResXResourceReader2189107174.h"
#include "System_Web_System_Resources_ResXResourceReader_Res3067387026.h"
#include "System_Web_System_Resources_ResXResourceWriter1119841949.h"
#include "System_Web_System_Resources_ResXFileRef4219341967.h"
#include "System_Web_System_Resources_ResXFileRef_Converter4050297939.h"
#include "System_Web_System_Resources_ResXDataNode2895009953.h"
#include "System_Web_System_Resources_ResXNullRef2896826674.h"
#include "System_Web_System_Web_Configuration_CapabilitiesBu1108460537.h"
#include "System_Web_System_Web_Configuration_CapabilitiesResul2339653.h"
#include "System_Web_System_Web_Configuration_nBrowser_Resul2266581459.h"
#include "System_Web_System_Web_Configuration_nBrowser_NodeTy169286799.h"
#include "System_Web_System_Web_Configuration_nBrowser_Identi455147138.h"
#include "System_Web_System_Web_Configuration_nBrowser_File3103643102.h"
#include "System_Web_System_Web_Configuration_nBrowser_Build486885285.h"
#include "System_Web_System_Web_Configuration_nBrowser_Node2365075726.h"
#include "System_Web_System_Web_Configuration_nBrowser_Excep1057686439.h"
#include "System_Web_System_Web_Caching_Cache4020976765.h"
#include "System_Web_System_Web_Caching_CacheItem2069182237.h"
#include "System_Web_System_Web_Caching_CacheItemEnumerator1234489326.h"
#include "System_Web_System_Web_Caching_CacheDependency311635210.h"
#include "System_Web_System_Web_Caching_CacheItemPriority3871792821.h"
#include "System_Web_System_Web_Caching_CacheItemRemovedReaso692128244.h"
#include "System_Web_System_Web_Caching_CachedRawResponse4252118804.h"
#include "System_Web_System_Web_Caching_CachedRawResponse_Da1175161993.h"
#include "System_Web_System_Web_BrowserData4272670656.h"
#include "System_Web_System_Web_CapabilitiesLoader211893129.h"
#include "System_Web_System_Web_Configuration_HttpCapabiliti3453542846.h"
#include "System_Web_System_Web_Compilation_ApplicationFileB2076493029.h"
#include "System_Web_System_Web_Compilation_AspComponent590775874.h"
#include "System_Web_System_Web_Compilation_AspComponentFoun3087970636.h"
#include "System_Web_System_Web_Compilation_AspComponentFoun1085528248.h"
#include "System_Web_System_Web_Compilation_AspComponentFoun3663392959.h"
#include "System_Web_System_Web_Compilation_AspComponentFoun1662405986.h"
#include "System_Web_System_Web_Compilation_AspComponentFound667948795.h"
#include "System_Web_System_Web_Compilation_AssemblyPathReso3631113943.h"
#include "System_Web_System_Web_Compilation_AppCodeAssembly1261384177.h"
#include "System_Web_System_Web_Compilation_AppCodeCompiler848602058.h"
#include "System_Web_System_Web_Compilation_AppResourceFileI3599744125.h"
#include "System_Web_System_Web_Compilation_AppResourceFileK1276421563.h"
#include "System_Web_System_Web_Compilation_AppResourceFiles3801859039.h"
#include "System_Web_System_Web_Compilation_AppResourcesAsse2366511295.h"
#include "System_Web_System_Web_Compilation_AppResourcesAsse3646760641.h"
#include "System_Web_System_Web_Compilation_AppResourcesComp3512896086.h"
#include "System_Web_System_Web_Compilation_AppResourcesCompi365108461.h"
#include "System_Web_System_Web_Compilation_AppWebReferencesC328258392.h"
#include "System_Web_System_Web_Compilation_BuilderLocation1517769213.h"
#include "System_Web_System_Web_Compilation_BuilderLocationS2907033206.h"
#include "System_Web_System_Web_Compilation_ParserStack2907135086.h"
#include "System_Web_System_Web_Compilation_TextBlockType2260672758.h"
#include "System_Web_System_Web_Compilation_TextBlock3735829980.h"
#include "System_Web_System_Web_Compilation_AspGenerator2810561191.h"
#include "System_Web_System_Web_Compilation_AspGenerator_Code694444359.h"
#include "System_Web_System_Web_Compilation_AspGenerator_Che1373994268.h"
#include "System_Web_System_Web_Compilation_AspParser4001025751.h"
#include "System_Web_System_Web_Compilation_AspTokenizer992095833.h"
#include "System_Web_System_Web_Compilation_AspTokenizer_PutB147579746.h"
#include "System_Web_System_Web_Compilation_CompileUnitParti3538146433.h"
#include "System_Web_System_Web_Compilation_AssemblyBuilder187066735.h"
#include "System_Web_System_Web_Compilation_AssemblyBuilder_1519973372.h"
#include "System_Web_System_Web_Compilation_AssemblyBuilder_3189098197.h"
#include "System_Web_System_Web_Compilation_AssemblyBuilder_1914127224.h"
#include "System_Web_System_Web_Compilation_BaseCompiler69158820.h"
#include "System_Web_System_Web_Compilation_BuildManager3594736268.h"
#include "System_Web_System_Web_Compilation_BuildManager_Pre3220258209.h"
#include "System_Web_System_Web_Compilation_BuildManagerCach1643815903.h"
#include "System_Web_System_Web_Compilation_BuildManagerDire1749203966.h"
#include "System_Web_System_Web_Compilation_BuildManagerRemov439168315.h"
#include "System_Web_System_Web_Compilation_BuildProviderApp3516266216.h"
#include "System_Web_System_Web_Compilation_BuildProviderAppl131801534.h"
#include "System_Web_System_Web_Compilation_BuildProviderGrou619564070.h"
#include "System_Web_System_Web_Compilation_BuildProvider3736381005.h"
#include "System_Web_System_Web_Compilation_CachingCompiler3794161503.h"
#include "System_Web_System_Web_Compilation_CompilationExcep3125132074.h"
#include "System_Web_System_Web_Compilation_CompilerType2533482247.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3100 = { sizeof (XmlBinaryReaderSession_t3909229952), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3100[2] = 
{
	XmlBinaryReaderSession_t3909229952::get_offset_of_dic_0(),
	XmlBinaryReaderSession_t3909229952::get_offset_of_store_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3101 = { sizeof (XmlBinaryWriterSession_t1730638232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3101[1] = 
{
	XmlBinaryWriterSession_t1730638232::get_offset_of_dic_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3102 = { sizeof (XmlDictionary_t238028028), -1, sizeof(XmlDictionary_t238028028_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3102[4] = 
{
	XmlDictionary_t238028028_StaticFields::get_offset_of_empty_0(),
	XmlDictionary_t238028028::get_offset_of_is_readonly_1(),
	XmlDictionary_t238028028::get_offset_of_dict_2(),
	XmlDictionary_t238028028::get_offset_of_list_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3103 = { sizeof (EmptyDictionary_t1467978489), -1, sizeof(EmptyDictionary_t1467978489_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3103[1] = 
{
	EmptyDictionary_t1467978489_StaticFields::get_offset_of_Instance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3104 = { sizeof (XmlDictionaryReader_t1044334689), -1, sizeof(XmlDictionaryReader_t1044334689_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3104[3] = 
{
	XmlDictionaryReader_t1044334689::get_offset_of_quotas_3(),
	XmlDictionaryReader_t1044334689::get_offset_of_xmlconv_from_bin_hex_4(),
	XmlDictionaryReader_t1044334689_StaticFields::get_offset_of_wsChars_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3105 = { sizeof (XmlDictionaryReaderQuotas_t173030297), -1, sizeof(XmlDictionaryReaderQuotas_t173030297_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3105[7] = 
{
	XmlDictionaryReaderQuotas_t173030297_StaticFields::get_offset_of_max_0(),
	XmlDictionaryReaderQuotas_t173030297::get_offset_of_is_readonly_1(),
	XmlDictionaryReaderQuotas_t173030297::get_offset_of_array_len_2(),
	XmlDictionaryReaderQuotas_t173030297::get_offset_of_bytes_3(),
	XmlDictionaryReaderQuotas_t173030297::get_offset_of_depth_4(),
	XmlDictionaryReaderQuotas_t173030297::get_offset_of_nt_chars_5(),
	XmlDictionaryReaderQuotas_t173030297::get_offset_of_text_len_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3106 = { sizeof (XmlDictionaryString_t3504120266), -1, sizeof(XmlDictionaryString_t3504120266_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3106[4] = 
{
	XmlDictionaryString_t3504120266_StaticFields::get_offset_of_empty_0(),
	XmlDictionaryString_t3504120266::get_offset_of_dict_1(),
	XmlDictionaryString_t3504120266::get_offset_of_value_2(),
	XmlDictionaryString_t3504120266::get_offset_of_key_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3107 = { sizeof (XmlDictionaryWriter_t1958650860), -1, sizeof(XmlDictionaryWriter_t1958650860_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3107[2] = 
{
	XmlDictionaryWriter_t1958650860_StaticFields::get_offset_of_utf8_unmarked_1(),
	XmlDictionaryWriter_t1958650860::get_offset_of_depth_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3108 = { sizeof (XmlMtomDictionaryReader_t3282849429), -1, sizeof(XmlMtomDictionaryReader_t3282849429_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3108[17] = 
{
	XmlMtomDictionaryReader_t3282849429::get_offset_of_stream_6(),
	XmlMtomDictionaryReader_t3282849429::get_offset_of_encoding_7(),
	XmlMtomDictionaryReader_t3282849429::get_offset_of_encodings_8(),
	XmlMtomDictionaryReader_t3282849429::get_offset_of_content_type_9(),
	XmlMtomDictionaryReader_t3282849429::get_offset_of_quotas_10(),
	XmlMtomDictionaryReader_t3282849429::get_offset_of_on_close_11(),
	XmlMtomDictionaryReader_t3282849429::get_offset_of_readers_12(),
	XmlMtomDictionaryReader_t3282849429::get_offset_of_xml_reader_13(),
	XmlMtomDictionaryReader_t3282849429::get_offset_of_initial_reader_14(),
	XmlMtomDictionaryReader_t3282849429::get_offset_of_eof_reader_15(),
	XmlMtomDictionaryReader_t3282849429::get_offset_of_part_reader_16(),
	XmlMtomDictionaryReader_t3282849429::get_offset_of_buffer_17(),
	XmlMtomDictionaryReader_t3282849429::get_offset_of_peek_char_18(),
	XmlMtomDictionaryReader_t3282849429::get_offset_of_current_content_type_19(),
	XmlMtomDictionaryReader_t3282849429::get_offset_of_current_content_id_20(),
	XmlMtomDictionaryReader_t3282849429::get_offset_of_current_content_encoding_21(),
	XmlMtomDictionaryReader_t3282849429_StaticFields::get_offset_of_U3CU3Ef__switchU24map8_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3109 = { sizeof (NonInteractiveStateXmlReader_t4263140638), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3110 = { sizeof (MultiPartedXmlReader_t1435767550), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3110[2] = 
{
	MultiPartedXmlReader_t1435767550::get_offset_of_owner_6(),
	MultiPartedXmlReader_t1435767550::get_offset_of_value_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3111 = { sizeof (DummyStateXmlReader_t3823067943), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3111[3] = 
{
	DummyStateXmlReader_t3823067943::get_offset_of_base_uri_3(),
	DummyStateXmlReader_t3823067943::get_offset_of_name_table_4(),
	DummyStateXmlReader_t3823067943::get_offset_of_read_state_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3112 = { sizeof (MimeEncodedStream_t622371820), -1, sizeof(MimeEncodedStream_t622371820_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3112[4] = 
{
	MimeEncodedStream_t622371820::get_offset_of_U3CIdU3Ek__BackingField_0(),
	MimeEncodedStream_t622371820::get_offset_of_U3CContentEncodingU3Ek__BackingField_1(),
	MimeEncodedStream_t622371820::get_offset_of_U3CEncodedStringU3Ek__BackingField_2(),
	MimeEncodedStream_t622371820_StaticFields::get_offset_of_U3CU3Ef__switchU24map9_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3113 = { sizeof (XmlMtomDictionaryWriter_t545583934), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3113[9] = 
{
	XmlMtomDictionaryWriter_t545583934::get_offset_of_writer_3(),
	XmlMtomDictionaryWriter_t545583934::get_offset_of_xml_writer_settings_4(),
	XmlMtomDictionaryWriter_t545583934::get_offset_of_max_bytes_5(),
	XmlMtomDictionaryWriter_t545583934::get_offset_of_write_headers_6(),
	XmlMtomDictionaryWriter_t545583934::get_offset_of_owns_stream_7(),
	XmlMtomDictionaryWriter_t545583934::get_offset_of_content_type_8(),
	XmlMtomDictionaryWriter_t545583934::get_offset_of_w_9(),
	XmlMtomDictionaryWriter_t545583934::get_offset_of_depth_10(),
	XmlMtomDictionaryWriter_t545583934::get_offset_of_section_count_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3114 = { sizeof (XmlSimpleDictionaryReader_t1404946835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3114[5] = 
{
	XmlSimpleDictionaryReader_t1404946835::get_offset_of_dict_6(),
	XmlSimpleDictionaryReader_t1404946835::get_offset_of_reader_7(),
	XmlSimpleDictionaryReader_t1404946835::get_offset_of_as_dict_reader_8(),
	XmlSimpleDictionaryReader_t1404946835::get_offset_of_as_line_info_9(),
	XmlSimpleDictionaryReader_t1404946835::get_offset_of_onClose_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3115 = { sizeof (XmlSimpleDictionaryWriter_t1777131310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3115[1] = 
{
	XmlSimpleDictionaryWriter_t1777131310::get_offset_of_writer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3116 = { sizeof (OnXmlDictionaryReaderClose_t3234185550), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3117 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255369), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3117[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3118 = { sizeof (U24ArrayTypeU248_t3244137467)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU248_t3244137467 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3119 = { sizeof (U3CModuleU3E_t692745544), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3120 = { sizeof (UplevelHelper_t1754666578), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3121 = { sizeof (Locale_t4128636114), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3122 = { sizeof (MonoTODOAttribute_t4131080591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3122[1] = 
{
	MonoTODOAttribute_t4131080591::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3123 = { sizeof (ResXResourceReader_t2189107174), -1, sizeof(ResXResourceReader_t2189107174_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3123[11] = 
{
	ResXResourceReader_t2189107174::get_offset_of_fileName_0(),
	ResXResourceReader_t2189107174::get_offset_of_stream_1(),
	ResXResourceReader_t2189107174::get_offset_of_reader_2(),
	ResXResourceReader_t2189107174::get_offset_of_hasht_3(),
	ResXResourceReader_t2189107174::get_offset_of_typeresolver_4(),
	ResXResourceReader_t2189107174::get_offset_of_xmlReader_5(),
	ResXResourceReader_t2189107174::get_offset_of_basepath_6(),
	ResXResourceReader_t2189107174::get_offset_of_useResXDataNodes_7(),
	ResXResourceReader_t2189107174::get_offset_of_assemblyNames_8(),
	ResXResourceReader_t2189107174::get_offset_of_hashtm_9(),
	ResXResourceReader_t2189107174_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3124 = { sizeof (ResXHeader_t3067387026), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3124[4] = 
{
	ResXHeader_t3067387026::get_offset_of_resMimeType_0(),
	ResXHeader_t3067387026::get_offset_of_reader_1(),
	ResXHeader_t3067387026::get_offset_of_version_2(),
	ResXHeader_t3067387026::get_offset_of_writer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3125 = { sizeof (ResXResourceWriter_t1119841949), -1, sizeof(ResXResourceWriter_t1119841949_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3125[13] = 
{
	ResXResourceWriter_t1119841949::get_offset_of_filename_0(),
	ResXResourceWriter_t1119841949::get_offset_of_stream_1(),
	ResXResourceWriter_t1119841949::get_offset_of_textwriter_2(),
	ResXResourceWriter_t1119841949::get_offset_of_writer_3(),
	ResXResourceWriter_t1119841949::get_offset_of_written_4(),
	ResXResourceWriter_t1119841949_StaticFields::get_offset_of_BinSerializedObjectMimeType_5(),
	ResXResourceWriter_t1119841949_StaticFields::get_offset_of_ByteArraySerializedObjectMimeType_6(),
	ResXResourceWriter_t1119841949_StaticFields::get_offset_of_DefaultSerializedObjectMimeType_7(),
	ResXResourceWriter_t1119841949_StaticFields::get_offset_of_ResMimeType_8(),
	ResXResourceWriter_t1119841949_StaticFields::get_offset_of_ResourceSchema_9(),
	ResXResourceWriter_t1119841949_StaticFields::get_offset_of_SoapSerializedObjectMimeType_10(),
	ResXResourceWriter_t1119841949_StaticFields::get_offset_of_Version_11(),
	ResXResourceWriter_t1119841949_StaticFields::get_offset_of_schema_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3126 = { sizeof (ResXFileRef_t4219341967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3126[3] = 
{
	ResXFileRef_t4219341967::get_offset_of_filename_0(),
	ResXFileRef_t4219341967::get_offset_of_typename_1(),
	ResXFileRef_t4219341967::get_offset_of_textFileEncoding_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3127 = { sizeof (Converter_t4050297939), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3128 = { sizeof (ResXDataNode_t2895009953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3128[5] = 
{
	ResXDataNode_t2895009953::get_offset_of_name_0(),
	ResXDataNode_t2895009953::get_offset_of_value_1(),
	ResXDataNode_t2895009953::get_offset_of_type_2(),
	ResXDataNode_t2895009953::get_offset_of_comment_3(),
	ResXDataNode_t2895009953::get_offset_of_pos_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3129 = { sizeof (ResXNullRef_t2896826674), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3130 = { sizeof (CapabilitiesBuild_t1108460537), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3131 = { sizeof (CapabilitiesResult_t2339653), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3132 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3133 = { sizeof (Result_t2266581459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3133[3] = 
{
	Result_t2266581459::get_offset_of_AdapterTypeMap_6(),
	Result_t2266581459::get_offset_of_Track_7(),
	Result_t2266581459::get_offset_of_MarkupTextWriter_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3134 = { sizeof (NodeType_t169286799)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3134[5] = 
{
	NodeType_t169286799::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3135 = { sizeof (Identification_t455147138), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3135[5] = 
{
	Identification_t455147138::get_offset_of_MatchType_0(),
	Identification_t455147138::get_offset_of_MatchName_1(),
	Identification_t455147138::get_offset_of_MatchGroup_2(),
	Identification_t455147138::get_offset_of_MatchPattern_3(),
	Identification_t455147138::get_offset_of_RegexPattern_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3136 = { sizeof (File_t3103643102), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3136[6] = 
{
	File_t3103643102::get_offset_of_BrowserFile_0(),
	File_t3103643102::get_offset_of_Nodes_1(),
	File_t3103643102::get_offset_of_Lookup_2(),
	File_t3103643102::get_offset_of_DefaultLookup_3(),
	File_t3103643102::get_offset_of_RefNodes_4(),
	File_t3103643102::get_offset_of_pFileName_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3137 = { sizeof (Build_t486885285), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3137[6] = 
{
	Build_t486885285::get_offset_of_Browserfiles_0(),
	Build_t486885285::get_offset_of_nbrowserfiles_1(),
	Build_t486885285::get_offset_of_DefaultKeys_2(),
	Build_t486885285::get_offset_of_BrowserKeys_3(),
	Build_t486885285::get_offset_of_browserSyncRoot_4(),
	Build_t486885285::get_offset_of_browser_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3138 = { sizeof (Node_t2365075726), -1, sizeof(Node_t2365075726_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3138[23] = 
{
	Node_t2365075726::get_offset_of_pName_0(),
	Node_t2365075726::get_offset_of_pId_1(),
	Node_t2365075726::get_offset_of_pParentID_2(),
	Node_t2365075726::get_offset_of_pRefID_3(),
	Node_t2365075726::get_offset_of_pMarkupTextWriterType_4(),
	Node_t2365075726::get_offset_of_pFileName_5(),
	Node_t2365075726::get_offset_of_xmlNode_6(),
	Node_t2365075726::get_offset_of_Identification_7(),
	Node_t2365075726::get_offset_of_Capture_8(),
	Node_t2365075726::get_offset_of_Capabilities_9(),
	Node_t2365075726::get_offset_of_Adapter_10(),
	Node_t2365075726::get_offset_of_AdapterControlTypes_11(),
	Node_t2365075726::get_offset_of_AdapterTypes_12(),
	Node_t2365075726::get_offset_of_ChildrenKeys_13(),
	Node_t2365075726::get_offset_of_DefaultChildrenKeys_14(),
	Node_t2365075726::get_offset_of_Children_15(),
	Node_t2365075726::get_offset_of_DefaultChildren_16(),
	Node_t2365075726::get_offset_of_sampleHeaders_17(),
	Node_t2365075726::get_offset_of_HaveAdapterTypes_18(),
	Node_t2365075726::get_offset_of_LookupAdapterTypesLock_19(),
	Node_t2365075726_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_20(),
	Node_t2365075726_StaticFields::get_offset_of_U3CU3Ef__switchU24map2_21(),
	Node_t2365075726_StaticFields::get_offset_of_U3CU3Ef__switchU24map3_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3139 = { sizeof (Exception_t1057686439), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3140 = { sizeof (Cache_t4020976765), -1, sizeof(Cache_t4020976765_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3140[4] = 
{
	Cache_t4020976765::get_offset_of_cache_0(),
	Cache_t4020976765::get_offset_of_dependencyCache_1(),
	Cache_t4020976765_StaticFields::get_offset_of_NoAbsoluteExpiration_2(),
	Cache_t4020976765_StaticFields::get_offset_of_NoSlidingExpiration_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3141 = { sizeof (CacheItem_t2069182237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3141[9] = 
{
	CacheItem_t2069182237::get_offset_of_Value_0(),
	CacheItem_t2069182237::get_offset_of_Key_1(),
	CacheItem_t2069182237::get_offset_of_Dependency_2(),
	CacheItem_t2069182237::get_offset_of_AbsoluteExpiration_3(),
	CacheItem_t2069182237::get_offset_of_SlidingExpiration_4(),
	CacheItem_t2069182237::get_offset_of_Priority_5(),
	CacheItem_t2069182237::get_offset_of_OnRemoveCallback_6(),
	CacheItem_t2069182237::get_offset_of_LastChange_7(),
	CacheItem_t2069182237::get_offset_of_Timer_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3142 = { sizeof (CacheItemEnumerator_t1234489326), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3142[2] = 
{
	CacheItemEnumerator_t1234489326::get_offset_of_list_0(),
	CacheItemEnumerator_t1234489326::get_offset_of_pos_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3143 = { sizeof (CacheDependency_t311635210), -1, sizeof(CacheDependency_t311635210_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3143[11] = 
{
	CacheDependency_t311635210_StaticFields::get_offset_of_dependencyChangedEvent_0(),
	CacheDependency_t311635210::get_offset_of_cachekeys_1(),
	CacheDependency_t311635210::get_offset_of_dependency_2(),
	CacheDependency_t311635210::get_offset_of_start_3(),
	CacheDependency_t311635210::get_offset_of_cache_4(),
	CacheDependency_t311635210::get_offset_of_watchers_5(),
	CacheDependency_t311635210::get_offset_of_hasChanged_6(),
	CacheDependency_t311635210::get_offset_of_used_7(),
	CacheDependency_t311635210::get_offset_of_utcLastModified_8(),
	CacheDependency_t311635210::get_offset_of_locker_9(),
	CacheDependency_t311635210::get_offset_of_events_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3144 = { sizeof (CacheItemPriority_t3871792821)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3144[8] = 
{
	CacheItemPriority_t3871792821::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3145 = { sizeof (CacheItemRemovedReason_t692128244)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3145[5] = 
{
	CacheItemRemovedReason_t692128244::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3146 = { sizeof (CachedRawResponse_t4252118804), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3146[2] = 
{
	CachedRawResponse_t4252118804::get_offset_of_headers_0(),
	CachedRawResponse_t4252118804::get_offset_of_data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3147 = { sizeof (DataItem_t1175161993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3147[2] = 
{
	DataItem_t1175161993::get_offset_of_Buffer_0(),
	DataItem_t1175161993::get_offset_of_Length_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3148 = { sizeof (BrowserData_t4272670656), -1, sizeof(BrowserData_t4272670656_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3148[7] = 
{
	BrowserData_t4272670656_StaticFields::get_offset_of_wildchars_0(),
	BrowserData_t4272670656::get_offset_of_this_lock_1(),
	BrowserData_t4272670656::get_offset_of_parent_2(),
	BrowserData_t4272670656::get_offset_of_text_3(),
	BrowserData_t4272670656::get_offset_of_pattern_4(),
	BrowserData_t4272670656::get_offset_of_regex_5(),
	BrowserData_t4272670656::get_offset_of_data_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3149 = { sizeof (CapabilitiesLoader_t211893129), -1, sizeof(CapabilitiesLoader_t211893129_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3149[6] = 
{
	CapabilitiesLoader_t211893129_StaticFields::get_offset_of_defaultCaps_1(),
	CapabilitiesLoader_t211893129_StaticFields::get_offset_of_lockobj_2(),
	CapabilitiesLoader_t211893129_StaticFields::get_offset_of_loaded_3(),
	CapabilitiesLoader_t211893129_StaticFields::get_offset_of_alldata_4(),
	CapabilitiesLoader_t211893129_StaticFields::get_offset_of_userAgentsCache_5(),
	CapabilitiesLoader_t211893129_StaticFields::get_offset_of_eq_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3150 = { sizeof (HttpCapabilitiesBase_t3453542846), -1, sizeof(HttpCapabilitiesBase_t3453542846_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3150[6] = 
{
	HttpCapabilitiesBase_t3453542846::get_offset_of_flags_0(),
	HttpCapabilitiesBase_t3453542846::get_offset_of_tagWriter_1(),
	HttpCapabilitiesBase_t3453542846::get_offset_of_useragent_2(),
	HttpCapabilitiesBase_t3453542846::get_offset_of_capabilities_3(),
	HttpCapabilitiesBase_t3453542846_StaticFields::get_offset_of_GetConfigCapabilities_called_4(),
	HttpCapabilitiesBase_t3453542846::get_offset_of_adapters_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3151 = { sizeof (ApplicationFileBuildProvider_t2076493029), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3152 = { sizeof (AspComponent_t590775874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3152[5] = 
{
	AspComponent_t590775874::get_offset_of_Type_0(),
	AspComponent_t590775874::get_offset_of_Prefix_1(),
	AspComponent_t590775874::get_offset_of_Source_2(),
	AspComponent_t590775874::get_offset_of_FromConfig_3(),
	AspComponent_t590775874::get_offset_of_Namespace_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3153 = { sizeof (AspComponentFoundry_t3087970636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3153[2] = 
{
	AspComponentFoundry_t3087970636::get_offset_of_foundries_0(),
	AspComponentFoundry_t3087970636::get_offset_of_components_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3154 = { sizeof (Foundry_t1085528248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3154[1] = 
{
	Foundry_t1085528248::get_offset_of__fromConfig_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3155 = { sizeof (TagNameFoundry_t3663392959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3155[3] = 
{
	TagNameFoundry_t3663392959::get_offset_of_tagName_1(),
	TagNameFoundry_t3663392959::get_offset_of_type_2(),
	TagNameFoundry_t3663392959::get_offset_of_source_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3156 = { sizeof (AssemblyFoundry_t1662405986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3156[4] = 
{
	AssemblyFoundry_t1662405986::get_offset_of_nameSpace_1(),
	AssemblyFoundry_t1662405986::get_offset_of_assembly_2(),
	AssemblyFoundry_t1662405986::get_offset_of_assemblyName_3(),
	AssemblyFoundry_t1662405986::get_offset_of_assemblyCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3157 = { sizeof (CompoundFoundry_t667948795), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3157[3] = 
{
	CompoundFoundry_t667948795::get_offset_of_assemblyFoundry_1(),
	CompoundFoundry_t667948795::get_offset_of_tagnames_2(),
	CompoundFoundry_t667948795::get_offset_of_tagPrefix_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3158 = { sizeof (AssemblyPathResolver_t3631113943), -1, sizeof(AssemblyPathResolver_t3631113943_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3158[1] = 
{
	AssemblyPathResolver_t3631113943_StaticFields::get_offset_of_assemblyCache_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3159 = { sizeof (AppCodeAssembly_t1261384177), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3159[6] = 
{
	AppCodeAssembly_t1261384177::get_offset_of_files_0(),
	AppCodeAssembly_t1261384177::get_offset_of_units_1(),
	AppCodeAssembly_t1261384177::get_offset_of_name_2(),
	AppCodeAssembly_t1261384177::get_offset_of_path_3(),
	AppCodeAssembly_t1261384177::get_offset_of_validAssembly_4(),
	AppCodeAssembly_t1261384177::get_offset_of_outputAssemblyName_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3160 = { sizeof (AppCodeCompiler_t848602058), -1, sizeof(AppCodeCompiler_t848602058_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3160[4] = 
{
	AppCodeCompiler_t848602058_StaticFields::get_offset_of__alreadyCompiled_0(),
	AppCodeCompiler_t848602058_StaticFields::get_offset_of_DefaultAppCodeAssemblyName_1(),
	AppCodeCompiler_t848602058::get_offset_of_assemblies_2(),
	AppCodeCompiler_t848602058::get_offset_of_providerTypeName_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3161 = { sizeof (AppResourceFileInfo_t3599744125), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3161[5] = 
{
	AppResourceFileInfo_t3599744125::get_offset_of_Embeddable_0(),
	AppResourceFileInfo_t3599744125::get_offset_of_Compilable_1(),
	AppResourceFileInfo_t3599744125::get_offset_of_Info_2(),
	AppResourceFileInfo_t3599744125::get_offset_of_Kind_3(),
	AppResourceFileInfo_t3599744125::get_offset_of_Seen_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3162 = { sizeof (AppResourceFileKind_t1276421563)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3162[5] = 
{
	AppResourceFileKind_t1276421563::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3163 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3164 = { sizeof (AppResourceFilesCollection_t3801859039), -1, sizeof(AppResourceFilesCollection_t3801859039_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3164[4] = 
{
	AppResourceFilesCollection_t3801859039::get_offset_of_files_0(),
	AppResourceFilesCollection_t3801859039::get_offset_of_isGlobal_1(),
	AppResourceFilesCollection_t3801859039::get_offset_of_sourceDir_2(),
	AppResourceFilesCollection_t3801859039_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3165 = { sizeof (AppResourcesAssemblyBuilder_t2366511295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3165[8] = 
{
	AppResourcesAssemblyBuilder_t2366511295::get_offset_of_config_0(),
	AppResourcesAssemblyBuilder_t2366511295::get_offset_of_ci_1(),
	AppResourcesAssemblyBuilder_t2366511295::get_offset_of__provider_2(),
	AppResourcesAssemblyBuilder_t2366511295::get_offset_of_baseAssemblyPath_3(),
	AppResourcesAssemblyBuilder_t2366511295::get_offset_of_baseAssemblyDirectory_4(),
	AppResourcesAssemblyBuilder_t2366511295::get_offset_of_canonicAssemblyName_5(),
	AppResourcesAssemblyBuilder_t2366511295::get_offset_of_mainAssembly_6(),
	AppResourcesAssemblyBuilder_t2366511295::get_offset_of_appResourcesCompiler_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3166 = { sizeof (U3CBuildSatelliteAssemblyU3Ec__AnonStorey6_t3646760641), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3166[2] = 
{
	U3CBuildSatelliteAssemblyU3Ec__AnonStorey6_t3646760641::get_offset_of_alMutex_0(),
	U3CBuildSatelliteAssemblyU3Ec__AnonStorey6_t3646760641::get_offset_of_alOutput_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3167 = { sizeof (AppResourcesCompiler_t3512896086), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3167[6] = 
{
	AppResourcesCompiler_t3512896086::get_offset_of_isGlobal_0(),
	AppResourcesCompiler_t3512896086::get_offset_of_files_1(),
	AppResourcesCompiler_t3512896086::get_offset_of_tempDirectory_2(),
	AppResourcesCompiler_t3512896086::get_offset_of_virtualPath_3(),
	AppResourcesCompiler_t3512896086::get_offset_of_cultureFiles_4(),
	AppResourcesCompiler_t3512896086::get_offset_of_defaultCultureFiles_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3168 = { sizeof (TypeResolutionService_t365108461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3168[2] = 
{
	TypeResolutionService_t365108461::get_offset_of_referencedAssemblies_0(),
	TypeResolutionService_t365108461::get_offset_of_mappedTypes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3169 = { sizeof (AppWebReferencesCompiler_t328258392), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3170 = { sizeof (BuilderLocation_t1517769213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3170[2] = 
{
	BuilderLocation_t1517769213::get_offset_of_Builder_0(),
	BuilderLocation_t1517769213::get_offset_of_Location_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3171 = { sizeof (BuilderLocationStack_t2907033206), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3172 = { sizeof (ParserStack_t2907135086), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3172[3] = 
{
	ParserStack_t2907135086::get_offset_of_files_0(),
	ParserStack_t2907135086::get_offset_of_parsers_1(),
	ParserStack_t2907135086::get_offset_of_current_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3173 = { sizeof (TextBlockType_t2260672758)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3173[5] = 
{
	TextBlockType_t2260672758::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3174 = { sizeof (TextBlock_t3735829980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3174[3] = 
{
	TextBlock_t3735829980::get_offset_of_Content_0(),
	TextBlock_t3735829980::get_offset_of_Type_1(),
	TextBlock_t3735829980::get_offset_of_Length_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3175 = { sizeof (AspGenerator_t2810561191), -1, sizeof(AspGenerator_t2810561191_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3175[22] = 
{
	AspGenerator_t2810561191_StaticFields::get_offset_of_DirectiveRegex_0(),
	AspGenerator_t2810561191_StaticFields::get_offset_of_runatServer_1(),
	AspGenerator_t2810561191_StaticFields::get_offset_of_endOfTag_2(),
	AspGenerator_t2810561191_StaticFields::get_offset_of_expressionRegex_3(),
	AspGenerator_t2810561191_StaticFields::get_offset_of_clientCommentRegex_4(),
	AspGenerator_t2810561191::get_offset_of_pstack_5(),
	AspGenerator_t2810561191::get_offset_of_stack_6(),
	AspGenerator_t2810561191::get_offset_of_tparser_7(),
	AspGenerator_t2810561191::get_offset_of_text_8(),
	AspGenerator_t2810561191::get_offset_of_rootBuilder_9(),
	AspGenerator_t2810561191::get_offset_of_inScript_10(),
	AspGenerator_t2810561191::get_offset_of_javascript_11(),
	AspGenerator_t2810561191::get_offset_of_ignore_text_12(),
	AspGenerator_t2810561191::get_offset_of_location_13(),
	AspGenerator_t2810561191::get_offset_of_isApplication_14(),
	AspGenerator_t2810561191::get_offset_of_tagInnerText_15(),
	AspGenerator_t2810561191_StaticFields::get_offset_of_emptyHash_16(),
	AspGenerator_t2810561191::get_offset_of_inForm_17(),
	AspGenerator_t2810561191::get_offset_of_useOtherTags_18(),
	AspGenerator_t2810561191::get_offset_of_lastTag_19(),
	AspGenerator_t2810561191::get_offset_of_componentFoundry_20(),
	AspGenerator_t2810561191::get_offset_of_inputStream_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3176 = { sizeof (CodeRenderParser_t694444359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3176[4] = 
{
	CodeRenderParser_t694444359::get_offset_of_str_0(),
	CodeRenderParser_t694444359::get_offset_of_builder_1(),
	CodeRenderParser_t694444359::get_offset_of_generator_2(),
	CodeRenderParser_t694444359::get_offset_of_location_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3177 = { sizeof (CheckBlockEnd_t1373994268), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3178 = { sizeof (AspParser_t4001025751), -1, sizeof(AspParser_t4001025751_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3178[21] = 
{
	AspParser_t4001025751_StaticFields::get_offset_of_errorEvent_0(),
	AspParser_t4001025751_StaticFields::get_offset_of_tagParsedEvent_1(),
	AspParser_t4001025751_StaticFields::get_offset_of_textParsedEvent_2(),
	AspParser_t4001025751_StaticFields::get_offset_of_parsingCompleteEvent_3(),
	AspParser_t4001025751::get_offset_of_checksum_4(),
	AspParser_t4001025751::get_offset_of_tokenizer_5(),
	AspParser_t4001025751::get_offset_of_beginLine_6(),
	AspParser_t4001025751::get_offset_of_endLine_7(),
	AspParser_t4001025751::get_offset_of_beginColumn_8(),
	AspParser_t4001025751::get_offset_of_endColumn_9(),
	AspParser_t4001025751::get_offset_of_beginPosition_10(),
	AspParser_t4001025751::get_offset_of_endPosition_11(),
	AspParser_t4001025751::get_offset_of_filename_12(),
	AspParser_t4001025751::get_offset_of_verbatimID_13(),
	AspParser_t4001025751::get_offset_of_fileText_14(),
	AspParser_t4001025751::get_offset_of_fileReader_15(),
	AspParser_t4001025751::get_offset_of__internal_16(),
	AspParser_t4001025751::get_offset_of__internalLineOffset_17(),
	AspParser_t4001025751::get_offset_of__internalPositionOffset_18(),
	AspParser_t4001025751::get_offset_of_outer_19(),
	AspParser_t4001025751::get_offset_of_events_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3179 = { sizeof (AspTokenizer_t992095833), -1, sizeof(AspTokenizer_t992095833_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3179[23] = 
{
	AspTokenizer_t992095833_StaticFields::get_offset_of_lfcr_0(),
	AspTokenizer_t992095833::get_offset_of_sr_1(),
	AspTokenizer_t992095833::get_offset_of_current_token_2(),
	AspTokenizer_t992095833::get_offset_of_sb_3(),
	AspTokenizer_t992095833::get_offset_of_odds_4(),
	AspTokenizer_t992095833::get_offset_of_col_5(),
	AspTokenizer_t992095833::get_offset_of_line_6(),
	AspTokenizer_t992095833::get_offset_of_begcol_7(),
	AspTokenizer_t992095833::get_offset_of_begline_8(),
	AspTokenizer_t992095833::get_offset_of_position_9(),
	AspTokenizer_t992095833::get_offset_of_inTag_10(),
	AspTokenizer_t992095833::get_offset_of_expectAttrValue_11(),
	AspTokenizer_t992095833::get_offset_of_alternatingQuotes_12(),
	AspTokenizer_t992095833::get_offset_of_hasPutBack_13(),
	AspTokenizer_t992095833::get_offset_of_verbatim_14(),
	AspTokenizer_t992095833::get_offset_of_have_value_15(),
	AspTokenizer_t992095833::get_offset_of_have_unget_16(),
	AspTokenizer_t992095833::get_offset_of_unget_value_17(),
	AspTokenizer_t992095833::get_offset_of_val_18(),
	AspTokenizer_t992095833::get_offset_of_putBackBuffer_19(),
	AspTokenizer_t992095833::get_offset_of_checksum_20(),
	AspTokenizer_t992095833::get_offset_of_checksum_buf_21(),
	AspTokenizer_t992095833::get_offset_of_checksum_buf_pos_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3180 = { sizeof (PutBackItem_t147579746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3180[4] = 
{
	PutBackItem_t147579746::get_offset_of_Value_0(),
	PutBackItem_t147579746::get_offset_of_Position_1(),
	PutBackItem_t147579746::get_offset_of_CurrentToken_2(),
	PutBackItem_t147579746::get_offset_of_InTag_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3181 = { sizeof (CompileUnitPartialType_t3538146433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3181[4] = 
{
	CompileUnitPartialType_t3538146433::get_offset_of_Unit_0(),
	CompileUnitPartialType_t3538146433::get_offset_of_ParentNamespace_1(),
	CompileUnitPartialType_t3538146433::get_offset_of_PartialType_2(),
	CompileUnitPartialType_t3538146433::get_offset_of_typeName_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3182 = { sizeof (AssemblyBuilder_t187066735), -1, sizeof(AssemblyBuilder_t187066735_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3182[15] = 
{
	AssemblyBuilder_t187066735_StaticFields::get_offset_of_KeepFiles_0(),
	AssemblyBuilder_t187066735::get_offset_of_provider_1(),
	AssemblyBuilder_t187066735::get_offset_of_parameters_2(),
	AssemblyBuilder_t187066735::get_offset_of_code_files_3(),
	AssemblyBuilder_t187066735::get_offset_of_partial_types_4(),
	AssemblyBuilder_t187066735::get_offset_of_path_to_buildprovider_5(),
	AssemblyBuilder_t187066735::get_offset_of_units_6(),
	AssemblyBuilder_t187066735::get_offset_of_source_files_7(),
	AssemblyBuilder_t187066735::get_offset_of_referenced_assemblies_8(),
	AssemblyBuilder_t187066735::get_offset_of_resource_files_9(),
	AssemblyBuilder_t187066735::get_offset_of_temp_files_10(),
	AssemblyBuilder_t187066735::get_offset_of_outputFilesPrefix_11(),
	AssemblyBuilder_t187066735::get_offset_of_outputAssemblyPrefix_12(),
	AssemblyBuilder_t187066735::get_offset_of_outputAssemblyName_13(),
	AssemblyBuilder_t187066735_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3183 = { sizeof (CodeUnit_t1519973372)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3183[2] = 
{
	CodeUnit_t1519973372::get_offset_of_BuildProvider_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CodeUnit_t1519973372::get_offset_of_Unit_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3184 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3185 = { sizeof (CSharpCodePragmaGenerator_t3189098197), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3186 = { sizeof (VBCodePragmaGenerator_t1914127224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3187 = { sizeof (BaseCompiler_t69158820), -1, sizeof(BaseCompiler_t69158820_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3187[12] = 
{
	BaseCompiler_t69158820_StaticFields::get_offset_of_HashMD5_0(),
	BaseCompiler_t69158820_StaticFields::get_offset_of_replaceableFlags_1(),
	BaseCompiler_t69158820::get_offset_of_parser_2(),
	BaseCompiler_t69158820::get_offset_of_unit_3(),
	BaseCompiler_t69158820::get_offset_of_mainNS_4(),
	BaseCompiler_t69158820::get_offset_of_partialNameOverride_5(),
	BaseCompiler_t69158820::get_offset_of_partialClass_6(),
	BaseCompiler_t69158820::get_offset_of_partialClassExpr_7(),
	BaseCompiler_t69158820::get_offset_of_mainClass_8(),
	BaseCompiler_t69158820::get_offset_of_mainClassExpr_9(),
	BaseCompiler_t69158820_StaticFields::get_offset_of_thisRef_10(),
	BaseCompiler_t69158820::get_offset_of_inputVirtualPath_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3188 = { sizeof (BuildManager_t3594736268), -1, sizeof(BuildManager_t3594736268_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3188[22] = 
{
	BuildManager_t3594736268_StaticFields::get_offset_of_BUILD_MANAGER_VIRTUAL_PATH_CACHE_PREFIX_LENGTH_0(),
	BuildManager_t3594736268_StaticFields::get_offset_of_bigCompilationLock_1(),
	BuildManager_t3594736268_StaticFields::get_offset_of_virtualPathsToIgnoreSplitChars_2(),
	BuildManager_t3594736268_StaticFields::get_offset_of_events_3(),
	BuildManager_t3594736268_StaticFields::get_offset_of_buildManagerRemoveEntryEvent_4(),
	BuildManager_t3594736268_StaticFields::get_offset_of_hosted_5(),
	BuildManager_t3594736268_StaticFields::get_offset_of_comparer_6(),
	BuildManager_t3594736268_StaticFields::get_offset_of_stringComparer_7(),
	BuildManager_t3594736268_StaticFields::get_offset_of_virtualPathsToIgnore_8(),
	BuildManager_t3594736268_StaticFields::get_offset_of_haveVirtualPathsToIgnore_9(),
	BuildManager_t3594736268_StaticFields::get_offset_of_AppCode_Assemblies_10(),
	BuildManager_t3594736268_StaticFields::get_offset_of_TopLevel_Assemblies_11(),
	BuildManager_t3594736268_StaticFields::get_offset_of_codeDomProviders_12(),
	BuildManager_t3594736268_StaticFields::get_offset_of_buildCache_13(),
	BuildManager_t3594736268_StaticFields::get_offset_of_referencedAssemblies_14(),
	BuildManager_t3594736268_StaticFields::get_offset_of_buildCount_15(),
	BuildManager_t3594736268_StaticFields::get_offset_of_is_precompiled_16(),
	BuildManager_t3594736268_StaticFields::get_offset_of_precompiled_17(),
	BuildManager_t3594736268_StaticFields::get_offset_of_suppressDebugModeMessages_18(),
	BuildManager_t3594736268_StaticFields::get_offset_of_buildCacheLock_19(),
	BuildManager_t3594736268_StaticFields::get_offset_of_recursionDepth_20(),
	BuildManager_t3594736268_StaticFields::get_offset_of_U3CHaveResourcesU3Ek__BackingField_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3189 = { sizeof (PreCompilationData_t3220258209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3189[4] = 
{
	PreCompilationData_t3220258209::get_offset_of_VirtualPath_0(),
	PreCompilationData_t3220258209::get_offset_of_AssemblyFileName_1(),
	PreCompilationData_t3220258209::get_offset_of_TypeName_2(),
	PreCompilationData_t3220258209::get_offset_of_Type_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3190 = { sizeof (BuildManagerCacheItem_t1643815903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3190[4] = 
{
	BuildManagerCacheItem_t1643815903::get_offset_of_CompiledCustomString_0(),
	BuildManagerCacheItem_t1643815903::get_offset_of_BuiltAssembly_1(),
	BuildManagerCacheItem_t1643815903::get_offset_of_VirtualPath_2(),
	BuildManagerCacheItem_t1643815903::get_offset_of_Type_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3191 = { sizeof (BuildManagerDirectoryBuilder_t1749203966), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3191[7] = 
{
	BuildManagerDirectoryBuilder_t1749203966::get_offset_of_virtualPath_0(),
	BuildManagerDirectoryBuilder_t1749203966::get_offset_of_virtualPathDirectory_1(),
	BuildManagerDirectoryBuilder_t1749203966::get_offset_of_compilationSection_2(),
	BuildManagerDirectoryBuilder_t1749203966::get_offset_of_buildProviders_3(),
	BuildManagerDirectoryBuilder_t1749203966::get_offset_of_dictionaryComparer_4(),
	BuildManagerDirectoryBuilder_t1749203966::get_offset_of_stringComparer_5(),
	BuildManagerDirectoryBuilder_t1749203966::get_offset_of_vpp_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3192 = { sizeof (BuildManagerRemoveEntryEventArgs_t439168315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3192[2] = 
{
	BuildManagerRemoveEntryEventArgs_t439168315::get_offset_of_U3CEntryNameU3Ek__BackingField_1(),
	BuildManagerRemoveEntryEventArgs_t439168315::get_offset_of_U3CContextU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3193 = { sizeof (BuildProviderAppliesToAttribute_t3516266216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3193[1] = 
{
	BuildProviderAppliesToAttribute_t3516266216::get_offset_of_appliesTo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3194 = { sizeof (BuildProviderAppliesTo_t131801534)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3194[5] = 
{
	BuildProviderAppliesTo_t131801534::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3195 = { sizeof (BuildProviderGroup_t619564070), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3195[5] = 
{
	BuildProviderGroup_t619564070::get_offset_of_U3CNamePrefixU3Ek__BackingField_5(),
	BuildProviderGroup_t619564070::get_offset_of_U3CStandaloneU3Ek__BackingField_6(),
	BuildProviderGroup_t619564070::get_offset_of_U3CApplicationU3Ek__BackingField_7(),
	BuildProviderGroup_t619564070::get_offset_of_U3CMasterU3Ek__BackingField_8(),
	BuildProviderGroup_t619564070::get_offset_of_U3CCompilerTypeU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3196 = { sizeof (BuildProvider_t3736381005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3196[3] = 
{
	BuildProvider_t3736381005::get_offset_of_ref_assemblies_0(),
	BuildProvider_t3736381005::get_offset_of_compilationSection_1(),
	BuildProvider_t3736381005::get_offset_of_vpath_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3197 = { sizeof (CachingCompiler_t3794161503), -1, sizeof(CachingCompiler_t3794161503_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3197[3] = 
{
	CachingCompiler_t3794161503_StaticFields::get_offset_of_dynamicBase_0(),
	CachingCompiler_t3794161503_StaticFields::get_offset_of_compilationTickets_1(),
	CachingCompiler_t3794161503_StaticFields::get_offset_of_assemblyCache_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3198 = { sizeof (CompilationException_t3125132074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3198[6] = 
{
	CompilationException_t3125132074::get_offset_of_filename_14(),
	CompilationException_t3125132074::get_offset_of_errors_15(),
	CompilationException_t3125132074::get_offset_of_results_16(),
	CompilationException_t3125132074::get_offset_of_fileText_17(),
	CompilationException_t3125132074::get_offset_of_errmsg_18(),
	CompilationException_t3125132074::get_offset_of_errorLines_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3199 = { sizeof (CompilerType_t2533482247), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3199[2] = 
{
	CompilerType_t2533482247::get_offset_of_type_0(),
	CompilerType_t2533482247::get_offset_of_parameters_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
