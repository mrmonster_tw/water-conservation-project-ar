﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XPath_XPathFunction857746608.h"

// System.Xml.XPath.Expression
struct Expression_t1452783009;
// System.Xml.XPath.XPathNavigator
struct XPathNavigator_t787956054;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.XsltDocument
struct  XsltDocument_t841609188  : public XPathFunction_t857746608
{
public:
	// System.Xml.XPath.Expression Mono.Xml.Xsl.XsltDocument::arg0
	Expression_t1452783009 * ___arg0_0;
	// System.Xml.XPath.Expression Mono.Xml.Xsl.XsltDocument::arg1
	Expression_t1452783009 * ___arg1_1;
	// System.Xml.XPath.XPathNavigator Mono.Xml.Xsl.XsltDocument::doc
	XPathNavigator_t787956054 * ___doc_2;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XsltDocument_t841609188, ___arg0_0)); }
	inline Expression_t1452783009 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1452783009 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1452783009 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier(&___arg0_0, value);
	}

	inline static int32_t get_offset_of_arg1_1() { return static_cast<int32_t>(offsetof(XsltDocument_t841609188, ___arg1_1)); }
	inline Expression_t1452783009 * get_arg1_1() const { return ___arg1_1; }
	inline Expression_t1452783009 ** get_address_of_arg1_1() { return &___arg1_1; }
	inline void set_arg1_1(Expression_t1452783009 * value)
	{
		___arg1_1 = value;
		Il2CppCodeGenWriteBarrier(&___arg1_1, value);
	}

	inline static int32_t get_offset_of_doc_2() { return static_cast<int32_t>(offsetof(XsltDocument_t841609188, ___doc_2)); }
	inline XPathNavigator_t787956054 * get_doc_2() const { return ___doc_2; }
	inline XPathNavigator_t787956054 ** get_address_of_doc_2() { return &___doc_2; }
	inline void set_doc_2(XPathNavigator_t787956054 * value)
	{
		___doc_2 = value;
		Il2CppCodeGenWriteBarrier(&___doc_2, value);
	}
};

struct XsltDocument_t841609188_StaticFields
{
public:
	// System.String Mono.Xml.Xsl.XsltDocument::VoidBaseUriFlag
	String_t* ___VoidBaseUriFlag_3;

public:
	inline static int32_t get_offset_of_VoidBaseUriFlag_3() { return static_cast<int32_t>(offsetof(XsltDocument_t841609188_StaticFields, ___VoidBaseUriFlag_3)); }
	inline String_t* get_VoidBaseUriFlag_3() const { return ___VoidBaseUriFlag_3; }
	inline String_t** get_address_of_VoidBaseUriFlag_3() { return &___VoidBaseUriFlag_3; }
	inline void set_VoidBaseUriFlag_3(String_t* value)
	{
		___VoidBaseUriFlag_3 = value;
		Il2CppCodeGenWriteBarrier(&___VoidBaseUriFlag_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
