﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"

// UnityEngine.RectTransform
struct RectTransform_t3704657025;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuestionedComponets/Questions
struct  Questions_t2798307503 
{
public:
	// UnityEngine.RectTransform QuestionedComponets/Questions::satisfaction
	RectTransform_t3704657025 * ___satisfaction_0;
	// UnityEngine.RectTransform QuestionedComponets/Questions::Sex_chose
	RectTransform_t3704657025 * ___Sex_chose_1;
	// UnityEngine.RectTransform QuestionedComponets/Questions::Age_b
	RectTransform_t3704657025 * ___Age_b_2;
	// UnityEngine.RectTransform QuestionedComponets/Questions::Opinion
	RectTransform_t3704657025 * ___Opinion_3;

public:
	inline static int32_t get_offset_of_satisfaction_0() { return static_cast<int32_t>(offsetof(Questions_t2798307503, ___satisfaction_0)); }
	inline RectTransform_t3704657025 * get_satisfaction_0() const { return ___satisfaction_0; }
	inline RectTransform_t3704657025 ** get_address_of_satisfaction_0() { return &___satisfaction_0; }
	inline void set_satisfaction_0(RectTransform_t3704657025 * value)
	{
		___satisfaction_0 = value;
		Il2CppCodeGenWriteBarrier(&___satisfaction_0, value);
	}

	inline static int32_t get_offset_of_Sex_chose_1() { return static_cast<int32_t>(offsetof(Questions_t2798307503, ___Sex_chose_1)); }
	inline RectTransform_t3704657025 * get_Sex_chose_1() const { return ___Sex_chose_1; }
	inline RectTransform_t3704657025 ** get_address_of_Sex_chose_1() { return &___Sex_chose_1; }
	inline void set_Sex_chose_1(RectTransform_t3704657025 * value)
	{
		___Sex_chose_1 = value;
		Il2CppCodeGenWriteBarrier(&___Sex_chose_1, value);
	}

	inline static int32_t get_offset_of_Age_b_2() { return static_cast<int32_t>(offsetof(Questions_t2798307503, ___Age_b_2)); }
	inline RectTransform_t3704657025 * get_Age_b_2() const { return ___Age_b_2; }
	inline RectTransform_t3704657025 ** get_address_of_Age_b_2() { return &___Age_b_2; }
	inline void set_Age_b_2(RectTransform_t3704657025 * value)
	{
		___Age_b_2 = value;
		Il2CppCodeGenWriteBarrier(&___Age_b_2, value);
	}

	inline static int32_t get_offset_of_Opinion_3() { return static_cast<int32_t>(offsetof(Questions_t2798307503, ___Opinion_3)); }
	inline RectTransform_t3704657025 * get_Opinion_3() const { return ___Opinion_3; }
	inline RectTransform_t3704657025 ** get_address_of_Opinion_3() { return &___Opinion_3; }
	inline void set_Opinion_3(RectTransform_t3704657025 * value)
	{
		___Opinion_3 = value;
		Il2CppCodeGenWriteBarrier(&___Opinion_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of QuestionedComponets/Questions
struct Questions_t2798307503_marshaled_pinvoke
{
	RectTransform_t3704657025 * ___satisfaction_0;
	RectTransform_t3704657025 * ___Sex_chose_1;
	RectTransform_t3704657025 * ___Age_b_2;
	RectTransform_t3704657025 * ___Opinion_3;
};
// Native definition for COM marshalling of QuestionedComponets/Questions
struct Questions_t2798307503_marshaled_com
{
	RectTransform_t3704657025 * ___satisfaction_0;
	RectTransform_t3704657025 * ___Sex_chose_1;
	RectTransform_t3704657025 * ___Age_b_2;
	RectTransform_t3704657025 * ___Opinion_3;
};
