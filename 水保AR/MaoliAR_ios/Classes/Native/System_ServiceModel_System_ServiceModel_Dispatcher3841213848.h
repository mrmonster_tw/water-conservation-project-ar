﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.ServiceThrottle
struct  ServiceThrottle_t3841213848  : public Il2CppObject
{
public:
	// System.Int32 System.ServiceModel.Dispatcher.ServiceThrottle::max_call
	int32_t ___max_call_0;
	// System.Int32 System.ServiceModel.Dispatcher.ServiceThrottle::max_session
	int32_t ___max_session_1;
	// System.Int32 System.ServiceModel.Dispatcher.ServiceThrottle::max_instance
	int32_t ___max_instance_2;

public:
	inline static int32_t get_offset_of_max_call_0() { return static_cast<int32_t>(offsetof(ServiceThrottle_t3841213848, ___max_call_0)); }
	inline int32_t get_max_call_0() const { return ___max_call_0; }
	inline int32_t* get_address_of_max_call_0() { return &___max_call_0; }
	inline void set_max_call_0(int32_t value)
	{
		___max_call_0 = value;
	}

	inline static int32_t get_offset_of_max_session_1() { return static_cast<int32_t>(offsetof(ServiceThrottle_t3841213848, ___max_session_1)); }
	inline int32_t get_max_session_1() const { return ___max_session_1; }
	inline int32_t* get_address_of_max_session_1() { return &___max_session_1; }
	inline void set_max_session_1(int32_t value)
	{
		___max_session_1 = value;
	}

	inline static int32_t get_offset_of_max_instance_2() { return static_cast<int32_t>(offsetof(ServiceThrottle_t3841213848, ___max_instance_2)); }
	inline int32_t get_max_instance_2() const { return ___max_instance_2; }
	inline int32_t* get_address_of_max_instance_2() { return &___max_instance_2; }
	inline void set_max_instance_2(int32_t value)
	{
		___max_instance_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
