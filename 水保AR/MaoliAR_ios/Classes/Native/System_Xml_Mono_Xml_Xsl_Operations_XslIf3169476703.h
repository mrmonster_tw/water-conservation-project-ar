﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Operations_XslCompiledElemen50593777.h"

// System.Xml.XPath.CompiledExpression
struct CompiledExpression_t4018285681;
// Mono.Xml.Xsl.Operations.XslOperation
struct XslOperation_t2153241355;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslIf
struct  XslIf_t3169476703  : public XslCompiledElement_t50593777
{
public:
	// System.Xml.XPath.CompiledExpression Mono.Xml.Xsl.Operations.XslIf::test
	CompiledExpression_t4018285681 * ___test_3;
	// Mono.Xml.Xsl.Operations.XslOperation Mono.Xml.Xsl.Operations.XslIf::children
	XslOperation_t2153241355 * ___children_4;

public:
	inline static int32_t get_offset_of_test_3() { return static_cast<int32_t>(offsetof(XslIf_t3169476703, ___test_3)); }
	inline CompiledExpression_t4018285681 * get_test_3() const { return ___test_3; }
	inline CompiledExpression_t4018285681 ** get_address_of_test_3() { return &___test_3; }
	inline void set_test_3(CompiledExpression_t4018285681 * value)
	{
		___test_3 = value;
		Il2CppCodeGenWriteBarrier(&___test_3, value);
	}

	inline static int32_t get_offset_of_children_4() { return static_cast<int32_t>(offsetof(XslIf_t3169476703, ___children_4)); }
	inline XslOperation_t2153241355 * get_children_4() const { return ___children_4; }
	inline XslOperation_t2153241355 ** get_address_of_children_4() { return &___children_4; }
	inline void set_children_4(XslOperation_t2153241355 * value)
	{
		___children_4 = value;
		Il2CppCodeGenWriteBarrier(&___children_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
