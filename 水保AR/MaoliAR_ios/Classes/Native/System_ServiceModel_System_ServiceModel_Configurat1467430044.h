﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Configurati254180867.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.WSHttpBindingElement
struct  WSHttpBindingElement_t1467430044  : public WSHttpBindingBaseElement_t254180867
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.WSHttpBindingElement::_properties
	ConfigurationPropertyCollection_t2852175726 * ____properties_15;

public:
	inline static int32_t get_offset_of__properties_15() { return static_cast<int32_t>(offsetof(WSHttpBindingElement_t1467430044, ____properties_15)); }
	inline ConfigurationPropertyCollection_t2852175726 * get__properties_15() const { return ____properties_15; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of__properties_15() { return &____properties_15; }
	inline void set__properties_15(ConfigurationPropertyCollection_t2852175726 * value)
	{
		____properties_15 = value;
		Il2CppCodeGenWriteBarrier(&____properties_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
