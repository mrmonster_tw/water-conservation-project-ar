﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_HttpResponseStream_Bucket1453203164.h"

// System.Web.HttpResponseStream/BlockManager
struct BlockManager_t141966076;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpResponseStream/ByteBucket
struct  ByteBucket_t3684624432  : public Bucket_t1453203164
{
public:
	// System.Int32 System.Web.HttpResponseStream/ByteBucket::start
	int32_t ___start_1;
	// System.Int32 System.Web.HttpResponseStream/ByteBucket::length
	int32_t ___length_2;
	// System.Web.HttpResponseStream/BlockManager System.Web.HttpResponseStream/ByteBucket::blocks
	BlockManager_t141966076 * ___blocks_3;
	// System.Boolean System.Web.HttpResponseStream/ByteBucket::Expandable
	bool ___Expandable_4;

public:
	inline static int32_t get_offset_of_start_1() { return static_cast<int32_t>(offsetof(ByteBucket_t3684624432, ___start_1)); }
	inline int32_t get_start_1() const { return ___start_1; }
	inline int32_t* get_address_of_start_1() { return &___start_1; }
	inline void set_start_1(int32_t value)
	{
		___start_1 = value;
	}

	inline static int32_t get_offset_of_length_2() { return static_cast<int32_t>(offsetof(ByteBucket_t3684624432, ___length_2)); }
	inline int32_t get_length_2() const { return ___length_2; }
	inline int32_t* get_address_of_length_2() { return &___length_2; }
	inline void set_length_2(int32_t value)
	{
		___length_2 = value;
	}

	inline static int32_t get_offset_of_blocks_3() { return static_cast<int32_t>(offsetof(ByteBucket_t3684624432, ___blocks_3)); }
	inline BlockManager_t141966076 * get_blocks_3() const { return ___blocks_3; }
	inline BlockManager_t141966076 ** get_address_of_blocks_3() { return &___blocks_3; }
	inline void set_blocks_3(BlockManager_t141966076 * value)
	{
		___blocks_3 = value;
		Il2CppCodeGenWriteBarrier(&___blocks_3, value);
	}

	inline static int32_t get_offset_of_Expandable_4() { return static_cast<int32_t>(offsetof(ByteBucket_t3684624432, ___Expandable_4)); }
	inline bool get_Expandable_4() const { return ___Expandable_4; }
	inline bool* get_address_of_Expandable_4() { return &___Expandable_4; }
	inline void set_Expandable_4(bool value)
	{
		___Expandable_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
