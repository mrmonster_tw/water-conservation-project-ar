﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Me778472114.h"

// System.ServiceModel.Channels.MessageHeaders
struct MessageHeaders_t4050072634;
// System.ServiceModel.Channels.MessageProperties
struct MessageProperties_t4101341573;
// System.ServiceModel.Channels.BodyWriter
struct BodyWriter_t3441673553;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.DefaultMessageBuffer
struct  DefaultMessageBuffer_t1331859589  : public MessageBuffer_t778472114
{
public:
	// System.ServiceModel.Channels.MessageHeaders System.ServiceModel.Channels.DefaultMessageBuffer::headers
	MessageHeaders_t4050072634 * ___headers_1;
	// System.ServiceModel.Channels.MessageProperties System.ServiceModel.Channels.DefaultMessageBuffer::properties
	MessageProperties_t4101341573 * ___properties_2;
	// System.ServiceModel.Channels.BodyWriter System.ServiceModel.Channels.DefaultMessageBuffer::body
	BodyWriter_t3441673553 * ___body_3;
	// System.Boolean System.ServiceModel.Channels.DefaultMessageBuffer::closed
	bool ___closed_4;
	// System.Boolean System.ServiceModel.Channels.DefaultMessageBuffer::is_fault
	bool ___is_fault_5;
	// System.Int32 System.ServiceModel.Channels.DefaultMessageBuffer::max_buffer_size
	int32_t ___max_buffer_size_6;

public:
	inline static int32_t get_offset_of_headers_1() { return static_cast<int32_t>(offsetof(DefaultMessageBuffer_t1331859589, ___headers_1)); }
	inline MessageHeaders_t4050072634 * get_headers_1() const { return ___headers_1; }
	inline MessageHeaders_t4050072634 ** get_address_of_headers_1() { return &___headers_1; }
	inline void set_headers_1(MessageHeaders_t4050072634 * value)
	{
		___headers_1 = value;
		Il2CppCodeGenWriteBarrier(&___headers_1, value);
	}

	inline static int32_t get_offset_of_properties_2() { return static_cast<int32_t>(offsetof(DefaultMessageBuffer_t1331859589, ___properties_2)); }
	inline MessageProperties_t4101341573 * get_properties_2() const { return ___properties_2; }
	inline MessageProperties_t4101341573 ** get_address_of_properties_2() { return &___properties_2; }
	inline void set_properties_2(MessageProperties_t4101341573 * value)
	{
		___properties_2 = value;
		Il2CppCodeGenWriteBarrier(&___properties_2, value);
	}

	inline static int32_t get_offset_of_body_3() { return static_cast<int32_t>(offsetof(DefaultMessageBuffer_t1331859589, ___body_3)); }
	inline BodyWriter_t3441673553 * get_body_3() const { return ___body_3; }
	inline BodyWriter_t3441673553 ** get_address_of_body_3() { return &___body_3; }
	inline void set_body_3(BodyWriter_t3441673553 * value)
	{
		___body_3 = value;
		Il2CppCodeGenWriteBarrier(&___body_3, value);
	}

	inline static int32_t get_offset_of_closed_4() { return static_cast<int32_t>(offsetof(DefaultMessageBuffer_t1331859589, ___closed_4)); }
	inline bool get_closed_4() const { return ___closed_4; }
	inline bool* get_address_of_closed_4() { return &___closed_4; }
	inline void set_closed_4(bool value)
	{
		___closed_4 = value;
	}

	inline static int32_t get_offset_of_is_fault_5() { return static_cast<int32_t>(offsetof(DefaultMessageBuffer_t1331859589, ___is_fault_5)); }
	inline bool get_is_fault_5() const { return ___is_fault_5; }
	inline bool* get_address_of_is_fault_5() { return &___is_fault_5; }
	inline void set_is_fault_5(bool value)
	{
		___is_fault_5 = value;
	}

	inline static int32_t get_offset_of_max_buffer_size_6() { return static_cast<int32_t>(offsetof(DefaultMessageBuffer_t1331859589, ___max_buffer_size_6)); }
	inline int32_t get_max_buffer_size_6() const { return ___max_buffer_size_6; }
	inline int32_t* get_address_of_max_buffer_size_6() { return &___max_buffer_size_6; }
	inline void set_max_buffer_size_6(int32_t value)
	{
		___max_buffer_size_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
