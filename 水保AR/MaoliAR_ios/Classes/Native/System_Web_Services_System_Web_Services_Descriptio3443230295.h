﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Collections_CollectionBase2727926298.h"

// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.ServiceDescriptionBaseCollection
struct  ServiceDescriptionBaseCollection_t3443230295  : public CollectionBase_t2727926298
{
public:
	// System.Collections.Hashtable System.Web.Services.Description.ServiceDescriptionBaseCollection::table
	Hashtable_t1853889766 * ___table_1;
	// System.Object System.Web.Services.Description.ServiceDescriptionBaseCollection::parent
	Il2CppObject * ___parent_2;

public:
	inline static int32_t get_offset_of_table_1() { return static_cast<int32_t>(offsetof(ServiceDescriptionBaseCollection_t3443230295, ___table_1)); }
	inline Hashtable_t1853889766 * get_table_1() const { return ___table_1; }
	inline Hashtable_t1853889766 ** get_address_of_table_1() { return &___table_1; }
	inline void set_table_1(Hashtable_t1853889766 * value)
	{
		___table_1 = value;
		Il2CppCodeGenWriteBarrier(&___table_1, value);
	}

	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(ServiceDescriptionBaseCollection_t3443230295, ___parent_2)); }
	inline Il2CppObject * get_parent_2() const { return ___parent_2; }
	inline Il2CppObject ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(Il2CppObject * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
