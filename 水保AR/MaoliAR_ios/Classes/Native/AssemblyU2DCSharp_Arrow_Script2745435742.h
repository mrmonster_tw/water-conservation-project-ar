﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Vector33722313464.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Arrow_Script
struct  Arrow_Script_t2745435742  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Arrow_Script::speed
	float ___speed_2;
	// UnityEngine.Vector3 Arrow_Script::org
	Vector3_t3722313464  ___org_3;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(Arrow_Script_t2745435742, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_org_3() { return static_cast<int32_t>(offsetof(Arrow_Script_t2745435742, ___org_3)); }
	inline Vector3_t3722313464  get_org_3() const { return ___org_3; }
	inline Vector3_t3722313464 * get_address_of_org_3() { return &___org_3; }
	inline void set_org_3(Vector3_t3722313464  value)
	{
		___org_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
