﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.TagPrefixAttribute
struct  TagPrefixAttribute_t1587840331  : public Attribute_t861562559
{
public:
	// System.String System.Web.UI.TagPrefixAttribute::namespaceName
	String_t* ___namespaceName_0;
	// System.String System.Web.UI.TagPrefixAttribute::tagPrefix
	String_t* ___tagPrefix_1;

public:
	inline static int32_t get_offset_of_namespaceName_0() { return static_cast<int32_t>(offsetof(TagPrefixAttribute_t1587840331, ___namespaceName_0)); }
	inline String_t* get_namespaceName_0() const { return ___namespaceName_0; }
	inline String_t** get_address_of_namespaceName_0() { return &___namespaceName_0; }
	inline void set_namespaceName_0(String_t* value)
	{
		___namespaceName_0 = value;
		Il2CppCodeGenWriteBarrier(&___namespaceName_0, value);
	}

	inline static int32_t get_offset_of_tagPrefix_1() { return static_cast<int32_t>(offsetof(TagPrefixAttribute_t1587840331, ___tagPrefix_1)); }
	inline String_t* get_tagPrefix_1() const { return ___tagPrefix_1; }
	inline String_t** get_address_of_tagPrefix_1() { return &___tagPrefix_1; }
	inline void set_tagPrefix_1(String_t* value)
	{
		___tagPrefix_1 = value;
		Il2CppCodeGenWriteBarrier(&___tagPrefix_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
