﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Char[]
struct CharU5BU5D_t3528271667;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.VirtualPathUtility
struct  VirtualPathUtility_t3131300229  : public Il2CppObject
{
public:

public:
};

struct VirtualPathUtility_t3131300229_StaticFields
{
public:
	// System.Boolean System.Web.VirtualPathUtility::monoSettingsVerifyCompatibility
	bool ___monoSettingsVerifyCompatibility_0;
	// System.Boolean System.Web.VirtualPathUtility::runningOnWindows
	bool ___runningOnWindows_1;
	// System.Char[] System.Web.VirtualPathUtility::path_sep
	CharU5BU5D_t3528271667* ___path_sep_2;
	// System.Char[] System.Web.VirtualPathUtility::invalidVirtualPathChars
	CharU5BU5D_t3528271667* ___invalidVirtualPathChars_3;
	// System.String System.Web.VirtualPathUtility::aspNetVerificationKey
	String_t* ___aspNetVerificationKey_4;

public:
	inline static int32_t get_offset_of_monoSettingsVerifyCompatibility_0() { return static_cast<int32_t>(offsetof(VirtualPathUtility_t3131300229_StaticFields, ___monoSettingsVerifyCompatibility_0)); }
	inline bool get_monoSettingsVerifyCompatibility_0() const { return ___monoSettingsVerifyCompatibility_0; }
	inline bool* get_address_of_monoSettingsVerifyCompatibility_0() { return &___monoSettingsVerifyCompatibility_0; }
	inline void set_monoSettingsVerifyCompatibility_0(bool value)
	{
		___monoSettingsVerifyCompatibility_0 = value;
	}

	inline static int32_t get_offset_of_runningOnWindows_1() { return static_cast<int32_t>(offsetof(VirtualPathUtility_t3131300229_StaticFields, ___runningOnWindows_1)); }
	inline bool get_runningOnWindows_1() const { return ___runningOnWindows_1; }
	inline bool* get_address_of_runningOnWindows_1() { return &___runningOnWindows_1; }
	inline void set_runningOnWindows_1(bool value)
	{
		___runningOnWindows_1 = value;
	}

	inline static int32_t get_offset_of_path_sep_2() { return static_cast<int32_t>(offsetof(VirtualPathUtility_t3131300229_StaticFields, ___path_sep_2)); }
	inline CharU5BU5D_t3528271667* get_path_sep_2() const { return ___path_sep_2; }
	inline CharU5BU5D_t3528271667** get_address_of_path_sep_2() { return &___path_sep_2; }
	inline void set_path_sep_2(CharU5BU5D_t3528271667* value)
	{
		___path_sep_2 = value;
		Il2CppCodeGenWriteBarrier(&___path_sep_2, value);
	}

	inline static int32_t get_offset_of_invalidVirtualPathChars_3() { return static_cast<int32_t>(offsetof(VirtualPathUtility_t3131300229_StaticFields, ___invalidVirtualPathChars_3)); }
	inline CharU5BU5D_t3528271667* get_invalidVirtualPathChars_3() const { return ___invalidVirtualPathChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_invalidVirtualPathChars_3() { return &___invalidVirtualPathChars_3; }
	inline void set_invalidVirtualPathChars_3(CharU5BU5D_t3528271667* value)
	{
		___invalidVirtualPathChars_3 = value;
		Il2CppCodeGenWriteBarrier(&___invalidVirtualPathChars_3, value);
	}

	inline static int32_t get_offset_of_aspNetVerificationKey_4() { return static_cast<int32_t>(offsetof(VirtualPathUtility_t3131300229_StaticFields, ___aspNetVerificationKey_4)); }
	inline String_t* get_aspNetVerificationKey_4() const { return ___aspNetVerificationKey_4; }
	inline String_t** get_address_of_aspNetVerificationKey_4() { return &___aspNetVerificationKey_4; }
	inline void set_aspNetVerificationKey_4(String_t* value)
	{
		___aspNetVerificationKey_4 = value;
		Il2CppCodeGenWriteBarrier(&___aspNetVerificationKey_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
