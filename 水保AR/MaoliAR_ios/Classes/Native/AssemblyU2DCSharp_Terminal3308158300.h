﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Vector22156229523.h"
#include "UnityEngine_UnityEngine_Rect2360479859.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Terminal
struct  Terminal_t3308158300  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector2 Terminal::scrollPosition
	Vector2_t2156229523  ___scrollPosition_2;
	// System.String Terminal::messageToMC
	String_t* ___messageToMC_3;
	// System.String Terminal::messageFromMC
	String_t* ___messageFromMC_4;
	// System.String Terminal::controlData
	String_t* ___controlData_5;
	// System.Collections.Generic.List`1<System.String> Terminal::messages
	List_1_t3319525431 * ___messages_6;
	// System.Int32 Terminal::labelHeight
	int32_t ___labelHeight_7;
	// System.Int32 Terminal::height
	int32_t ___height_8;
	// UnityEngine.Rect Terminal::gRect
	Rect_t2360479859  ___gRect_9;

public:
	inline static int32_t get_offset_of_scrollPosition_2() { return static_cast<int32_t>(offsetof(Terminal_t3308158300, ___scrollPosition_2)); }
	inline Vector2_t2156229523  get_scrollPosition_2() const { return ___scrollPosition_2; }
	inline Vector2_t2156229523 * get_address_of_scrollPosition_2() { return &___scrollPosition_2; }
	inline void set_scrollPosition_2(Vector2_t2156229523  value)
	{
		___scrollPosition_2 = value;
	}

	inline static int32_t get_offset_of_messageToMC_3() { return static_cast<int32_t>(offsetof(Terminal_t3308158300, ___messageToMC_3)); }
	inline String_t* get_messageToMC_3() const { return ___messageToMC_3; }
	inline String_t** get_address_of_messageToMC_3() { return &___messageToMC_3; }
	inline void set_messageToMC_3(String_t* value)
	{
		___messageToMC_3 = value;
		Il2CppCodeGenWriteBarrier(&___messageToMC_3, value);
	}

	inline static int32_t get_offset_of_messageFromMC_4() { return static_cast<int32_t>(offsetof(Terminal_t3308158300, ___messageFromMC_4)); }
	inline String_t* get_messageFromMC_4() const { return ___messageFromMC_4; }
	inline String_t** get_address_of_messageFromMC_4() { return &___messageFromMC_4; }
	inline void set_messageFromMC_4(String_t* value)
	{
		___messageFromMC_4 = value;
		Il2CppCodeGenWriteBarrier(&___messageFromMC_4, value);
	}

	inline static int32_t get_offset_of_controlData_5() { return static_cast<int32_t>(offsetof(Terminal_t3308158300, ___controlData_5)); }
	inline String_t* get_controlData_5() const { return ___controlData_5; }
	inline String_t** get_address_of_controlData_5() { return &___controlData_5; }
	inline void set_controlData_5(String_t* value)
	{
		___controlData_5 = value;
		Il2CppCodeGenWriteBarrier(&___controlData_5, value);
	}

	inline static int32_t get_offset_of_messages_6() { return static_cast<int32_t>(offsetof(Terminal_t3308158300, ___messages_6)); }
	inline List_1_t3319525431 * get_messages_6() const { return ___messages_6; }
	inline List_1_t3319525431 ** get_address_of_messages_6() { return &___messages_6; }
	inline void set_messages_6(List_1_t3319525431 * value)
	{
		___messages_6 = value;
		Il2CppCodeGenWriteBarrier(&___messages_6, value);
	}

	inline static int32_t get_offset_of_labelHeight_7() { return static_cast<int32_t>(offsetof(Terminal_t3308158300, ___labelHeight_7)); }
	inline int32_t get_labelHeight_7() const { return ___labelHeight_7; }
	inline int32_t* get_address_of_labelHeight_7() { return &___labelHeight_7; }
	inline void set_labelHeight_7(int32_t value)
	{
		___labelHeight_7 = value;
	}

	inline static int32_t get_offset_of_height_8() { return static_cast<int32_t>(offsetof(Terminal_t3308158300, ___height_8)); }
	inline int32_t get_height_8() const { return ___height_8; }
	inline int32_t* get_address_of_height_8() { return &___height_8; }
	inline void set_height_8(int32_t value)
	{
		___height_8 = value;
	}

	inline static int32_t get_offset_of_gRect_9() { return static_cast<int32_t>(offsetof(Terminal_t3308158300, ___gRect_9)); }
	inline Rect_t2360479859  get_gRect_9() const { return ___gRect_9; }
	inline Rect_t2360479859 * get_address_of_gRect_9() { return &___gRect_9; }
	inline void set_gRect_9(Rect_t2360479859  value)
	{
		___gRect_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
