﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XmlWriter127905479.h"
#include "System_Xml_System_Xml_WriteState3983380671.h"

// System.Xml.XmlNode
struct XmlNode_t3767805227;
// System.Xml.XmlAttribute
struct XmlAttribute_t1173852259;
// Mono.Xml.XPath.XmlWriterClosedEventHandler
struct XmlWriterClosedEventHandler_t712903533;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.XPath.XmlDocumentInsertionWriter
struct  XmlDocumentInsertionWriter_t2537499277  : public XmlWriter_t127905479
{
public:
	// System.Xml.XmlNode Mono.Xml.XPath.XmlDocumentInsertionWriter::parent
	XmlNode_t3767805227 * ___parent_1;
	// System.Xml.XmlNode Mono.Xml.XPath.XmlDocumentInsertionWriter::current
	XmlNode_t3767805227 * ___current_2;
	// System.Xml.XmlNode Mono.Xml.XPath.XmlDocumentInsertionWriter::nextSibling
	XmlNode_t3767805227 * ___nextSibling_3;
	// System.Xml.WriteState Mono.Xml.XPath.XmlDocumentInsertionWriter::state
	int32_t ___state_4;
	// System.Xml.XmlAttribute Mono.Xml.XPath.XmlDocumentInsertionWriter::attribute
	XmlAttribute_t1173852259 * ___attribute_5;
	// Mono.Xml.XPath.XmlWriterClosedEventHandler Mono.Xml.XPath.XmlDocumentInsertionWriter::Closed
	XmlWriterClosedEventHandler_t712903533 * ___Closed_6;

public:
	inline static int32_t get_offset_of_parent_1() { return static_cast<int32_t>(offsetof(XmlDocumentInsertionWriter_t2537499277, ___parent_1)); }
	inline XmlNode_t3767805227 * get_parent_1() const { return ___parent_1; }
	inline XmlNode_t3767805227 ** get_address_of_parent_1() { return &___parent_1; }
	inline void set_parent_1(XmlNode_t3767805227 * value)
	{
		___parent_1 = value;
		Il2CppCodeGenWriteBarrier(&___parent_1, value);
	}

	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(XmlDocumentInsertionWriter_t2537499277, ___current_2)); }
	inline XmlNode_t3767805227 * get_current_2() const { return ___current_2; }
	inline XmlNode_t3767805227 ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(XmlNode_t3767805227 * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier(&___current_2, value);
	}

	inline static int32_t get_offset_of_nextSibling_3() { return static_cast<int32_t>(offsetof(XmlDocumentInsertionWriter_t2537499277, ___nextSibling_3)); }
	inline XmlNode_t3767805227 * get_nextSibling_3() const { return ___nextSibling_3; }
	inline XmlNode_t3767805227 ** get_address_of_nextSibling_3() { return &___nextSibling_3; }
	inline void set_nextSibling_3(XmlNode_t3767805227 * value)
	{
		___nextSibling_3 = value;
		Il2CppCodeGenWriteBarrier(&___nextSibling_3, value);
	}

	inline static int32_t get_offset_of_state_4() { return static_cast<int32_t>(offsetof(XmlDocumentInsertionWriter_t2537499277, ___state_4)); }
	inline int32_t get_state_4() const { return ___state_4; }
	inline int32_t* get_address_of_state_4() { return &___state_4; }
	inline void set_state_4(int32_t value)
	{
		___state_4 = value;
	}

	inline static int32_t get_offset_of_attribute_5() { return static_cast<int32_t>(offsetof(XmlDocumentInsertionWriter_t2537499277, ___attribute_5)); }
	inline XmlAttribute_t1173852259 * get_attribute_5() const { return ___attribute_5; }
	inline XmlAttribute_t1173852259 ** get_address_of_attribute_5() { return &___attribute_5; }
	inline void set_attribute_5(XmlAttribute_t1173852259 * value)
	{
		___attribute_5 = value;
		Il2CppCodeGenWriteBarrier(&___attribute_5, value);
	}

	inline static int32_t get_offset_of_Closed_6() { return static_cast<int32_t>(offsetof(XmlDocumentInsertionWriter_t2537499277, ___Closed_6)); }
	inline XmlWriterClosedEventHandler_t712903533 * get_Closed_6() const { return ___Closed_6; }
	inline XmlWriterClosedEventHandler_t712903533 ** get_address_of_Closed_6() { return &___Closed_6; }
	inline void set_Closed_6(XmlWriterClosedEventHandler_t712903533 * value)
	{
		___Closed_6 = value;
		Il2CppCodeGenWriteBarrier(&___Closed_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
