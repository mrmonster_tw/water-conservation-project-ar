﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HighlighterController3493614325.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConstantHighlighting
struct  ConstantHighlighting_t1154505092  : public HighlighterController_t3493614325
{
public:
	// UnityEngine.Color ConstantHighlighting::color
	Color_t2555686324  ___color_5;

public:
	inline static int32_t get_offset_of_color_5() { return static_cast<int32_t>(offsetof(ConstantHighlighting_t1154505092, ___color_5)); }
	inline Color_t2555686324  get_color_5() const { return ___color_5; }
	inline Color_t2555686324 * get_address_of_color_5() { return &___color_5; }
	inline void set_color_5(Color_t2555686324  value)
	{
		___color_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
