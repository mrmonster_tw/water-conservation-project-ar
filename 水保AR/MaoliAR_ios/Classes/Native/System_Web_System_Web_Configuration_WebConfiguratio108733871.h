﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.Configuration.WebConfigurationFileMap
struct WebConfigurationFileMap_t225230154;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.WebConfigurationHost
struct  WebConfigurationHost_t108733871  : public Il2CppObject
{
public:
	// System.Web.Configuration.WebConfigurationFileMap System.Web.Configuration.WebConfigurationHost::map
	WebConfigurationFileMap_t225230154 * ___map_0;

public:
	inline static int32_t get_offset_of_map_0() { return static_cast<int32_t>(offsetof(WebConfigurationHost_t108733871, ___map_0)); }
	inline WebConfigurationFileMap_t225230154 * get_map_0() const { return ___map_0; }
	inline WebConfigurationFileMap_t225230154 ** get_address_of_map_0() { return &___map_0; }
	inline void set_map_0(WebConfigurationFileMap_t225230154 * value)
	{
		___map_0 = value;
		Il2CppCodeGenWriteBarrier(&___map_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
