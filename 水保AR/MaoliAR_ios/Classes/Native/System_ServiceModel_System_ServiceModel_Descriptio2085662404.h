﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Description.HttpGetWsdl
struct HttpGetWsdl_t3793260870;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.MetadataPublishingInfo
struct  MetadataPublishingInfo_t2085662404  : public Il2CppObject
{
public:
	// System.Boolean System.ServiceModel.Description.MetadataPublishingInfo::<SupportsMex>k__BackingField
	bool ___U3CSupportsMexU3Ek__BackingField_0;
	// System.Boolean System.ServiceModel.Description.MetadataPublishingInfo::<SupportsHelp>k__BackingField
	bool ___U3CSupportsHelpU3Ek__BackingField_1;
	// System.ServiceModel.Description.HttpGetWsdl System.ServiceModel.Description.MetadataPublishingInfo::<Instance>k__BackingField
	HttpGetWsdl_t3793260870 * ___U3CInstanceU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CSupportsMexU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MetadataPublishingInfo_t2085662404, ___U3CSupportsMexU3Ek__BackingField_0)); }
	inline bool get_U3CSupportsMexU3Ek__BackingField_0() const { return ___U3CSupportsMexU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CSupportsMexU3Ek__BackingField_0() { return &___U3CSupportsMexU3Ek__BackingField_0; }
	inline void set_U3CSupportsMexU3Ek__BackingField_0(bool value)
	{
		___U3CSupportsMexU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CSupportsHelpU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MetadataPublishingInfo_t2085662404, ___U3CSupportsHelpU3Ek__BackingField_1)); }
	inline bool get_U3CSupportsHelpU3Ek__BackingField_1() const { return ___U3CSupportsHelpU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CSupportsHelpU3Ek__BackingField_1() { return &___U3CSupportsHelpU3Ek__BackingField_1; }
	inline void set_U3CSupportsHelpU3Ek__BackingField_1(bool value)
	{
		___U3CSupportsHelpU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MetadataPublishingInfo_t2085662404, ___U3CInstanceU3Ek__BackingField_2)); }
	inline HttpGetWsdl_t3793260870 * get_U3CInstanceU3Ek__BackingField_2() const { return ___U3CInstanceU3Ek__BackingField_2; }
	inline HttpGetWsdl_t3793260870 ** get_address_of_U3CInstanceU3Ek__BackingField_2() { return &___U3CInstanceU3Ek__BackingField_2; }
	inline void set_U3CInstanceU3Ek__BackingField_2(HttpGetWsdl_t3793260870 * value)
	{
		___U3CInstanceU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CInstanceU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
