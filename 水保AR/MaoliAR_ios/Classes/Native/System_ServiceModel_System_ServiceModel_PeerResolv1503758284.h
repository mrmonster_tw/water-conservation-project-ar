﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_Guid3193532887.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.PeerResolvers.RegisterResponseInfoDC
struct  RegisterResponseInfoDC_t1503758284  : public Il2CppObject
{
public:
	// System.Guid System.ServiceModel.PeerResolvers.RegisterResponseInfoDC::registration_id
	Guid_t  ___registration_id_0;

public:
	inline static int32_t get_offset_of_registration_id_0() { return static_cast<int32_t>(offsetof(RegisterResponseInfoDC_t1503758284, ___registration_id_0)); }
	inline Guid_t  get_registration_id_0() const { return ___registration_id_0; }
	inline Guid_t * get_address_of_registration_id_0() { return &___registration_id_0; }
	inline void set_registration_id_0(Guid_t  value)
	{
		___registration_id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
