﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_B3441673553.h"

// System.Xml.Serialization.XmlSerializer
struct XmlSerializer_t1117804635;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.XmlMessagesFormatter/XmlBodyWriter
struct  XmlBodyWriter_t648809924  : public BodyWriter_t3441673553
{
public:
	// System.Xml.Serialization.XmlSerializer System.ServiceModel.Dispatcher.XmlMessagesFormatter/XmlBodyWriter::serializer
	XmlSerializer_t1117804635 * ___serializer_1;
	// System.Object System.ServiceModel.Dispatcher.XmlMessagesFormatter/XmlBodyWriter::body
	Il2CppObject * ___body_2;

public:
	inline static int32_t get_offset_of_serializer_1() { return static_cast<int32_t>(offsetof(XmlBodyWriter_t648809924, ___serializer_1)); }
	inline XmlSerializer_t1117804635 * get_serializer_1() const { return ___serializer_1; }
	inline XmlSerializer_t1117804635 ** get_address_of_serializer_1() { return &___serializer_1; }
	inline void set_serializer_1(XmlSerializer_t1117804635 * value)
	{
		___serializer_1 = value;
		Il2CppCodeGenWriteBarrier(&___serializer_1, value);
	}

	inline static int32_t get_offset_of_body_2() { return static_cast<int32_t>(offsetof(XmlBodyWriter_t648809924, ___body_2)); }
	inline Il2CppObject * get_body_2() const { return ___body_2; }
	inline Il2CppObject ** get_address_of_body_2() { return &___body_2; }
	inline void set_body_2(Il2CppObject * value)
	{
		___body_2 = value;
		Il2CppCodeGenWriteBarrier(&___body_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
