﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.IssuedTokenClientElement
struct  IssuedTokenClientElement_t4053539559  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct IssuedTokenClientElement_t4053539559_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.IssuedTokenClientElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.IssuedTokenClientElement::cache_issued_tokens
	ConfigurationProperty_t3590861854 * ___cache_issued_tokens_14;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.IssuedTokenClientElement::default_key_entropy_mode
	ConfigurationProperty_t3590861854 * ___default_key_entropy_mode_15;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.IssuedTokenClientElement::issued_token_renewal_threshold_percentage
	ConfigurationProperty_t3590861854 * ___issued_token_renewal_threshold_percentage_16;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.IssuedTokenClientElement::issuer_channel_behaviors
	ConfigurationProperty_t3590861854 * ___issuer_channel_behaviors_17;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.IssuedTokenClientElement::local_issuer
	ConfigurationProperty_t3590861854 * ___local_issuer_18;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.IssuedTokenClientElement::local_issuer_channel_behaviors
	ConfigurationProperty_t3590861854 * ___local_issuer_channel_behaviors_19;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.IssuedTokenClientElement::max_issued_token_caching_time
	ConfigurationProperty_t3590861854 * ___max_issued_token_caching_time_20;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(IssuedTokenClientElement_t4053539559_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier(&___properties_13, value);
	}

	inline static int32_t get_offset_of_cache_issued_tokens_14() { return static_cast<int32_t>(offsetof(IssuedTokenClientElement_t4053539559_StaticFields, ___cache_issued_tokens_14)); }
	inline ConfigurationProperty_t3590861854 * get_cache_issued_tokens_14() const { return ___cache_issued_tokens_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_cache_issued_tokens_14() { return &___cache_issued_tokens_14; }
	inline void set_cache_issued_tokens_14(ConfigurationProperty_t3590861854 * value)
	{
		___cache_issued_tokens_14 = value;
		Il2CppCodeGenWriteBarrier(&___cache_issued_tokens_14, value);
	}

	inline static int32_t get_offset_of_default_key_entropy_mode_15() { return static_cast<int32_t>(offsetof(IssuedTokenClientElement_t4053539559_StaticFields, ___default_key_entropy_mode_15)); }
	inline ConfigurationProperty_t3590861854 * get_default_key_entropy_mode_15() const { return ___default_key_entropy_mode_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_default_key_entropy_mode_15() { return &___default_key_entropy_mode_15; }
	inline void set_default_key_entropy_mode_15(ConfigurationProperty_t3590861854 * value)
	{
		___default_key_entropy_mode_15 = value;
		Il2CppCodeGenWriteBarrier(&___default_key_entropy_mode_15, value);
	}

	inline static int32_t get_offset_of_issued_token_renewal_threshold_percentage_16() { return static_cast<int32_t>(offsetof(IssuedTokenClientElement_t4053539559_StaticFields, ___issued_token_renewal_threshold_percentage_16)); }
	inline ConfigurationProperty_t3590861854 * get_issued_token_renewal_threshold_percentage_16() const { return ___issued_token_renewal_threshold_percentage_16; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_issued_token_renewal_threshold_percentage_16() { return &___issued_token_renewal_threshold_percentage_16; }
	inline void set_issued_token_renewal_threshold_percentage_16(ConfigurationProperty_t3590861854 * value)
	{
		___issued_token_renewal_threshold_percentage_16 = value;
		Il2CppCodeGenWriteBarrier(&___issued_token_renewal_threshold_percentage_16, value);
	}

	inline static int32_t get_offset_of_issuer_channel_behaviors_17() { return static_cast<int32_t>(offsetof(IssuedTokenClientElement_t4053539559_StaticFields, ___issuer_channel_behaviors_17)); }
	inline ConfigurationProperty_t3590861854 * get_issuer_channel_behaviors_17() const { return ___issuer_channel_behaviors_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_issuer_channel_behaviors_17() { return &___issuer_channel_behaviors_17; }
	inline void set_issuer_channel_behaviors_17(ConfigurationProperty_t3590861854 * value)
	{
		___issuer_channel_behaviors_17 = value;
		Il2CppCodeGenWriteBarrier(&___issuer_channel_behaviors_17, value);
	}

	inline static int32_t get_offset_of_local_issuer_18() { return static_cast<int32_t>(offsetof(IssuedTokenClientElement_t4053539559_StaticFields, ___local_issuer_18)); }
	inline ConfigurationProperty_t3590861854 * get_local_issuer_18() const { return ___local_issuer_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_local_issuer_18() { return &___local_issuer_18; }
	inline void set_local_issuer_18(ConfigurationProperty_t3590861854 * value)
	{
		___local_issuer_18 = value;
		Il2CppCodeGenWriteBarrier(&___local_issuer_18, value);
	}

	inline static int32_t get_offset_of_local_issuer_channel_behaviors_19() { return static_cast<int32_t>(offsetof(IssuedTokenClientElement_t4053539559_StaticFields, ___local_issuer_channel_behaviors_19)); }
	inline ConfigurationProperty_t3590861854 * get_local_issuer_channel_behaviors_19() const { return ___local_issuer_channel_behaviors_19; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_local_issuer_channel_behaviors_19() { return &___local_issuer_channel_behaviors_19; }
	inline void set_local_issuer_channel_behaviors_19(ConfigurationProperty_t3590861854 * value)
	{
		___local_issuer_channel_behaviors_19 = value;
		Il2CppCodeGenWriteBarrier(&___local_issuer_channel_behaviors_19, value);
	}

	inline static int32_t get_offset_of_max_issued_token_caching_time_20() { return static_cast<int32_t>(offsetof(IssuedTokenClientElement_t4053539559_StaticFields, ___max_issued_token_caching_time_20)); }
	inline ConfigurationProperty_t3590861854 * get_max_issued_token_caching_time_20() const { return ___max_issued_token_caching_time_20; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_max_issued_token_caching_time_20() { return &___max_issued_token_caching_time_20; }
	inline void set_max_issued_token_caching_time_20(ConfigurationProperty_t3590861854 * value)
	{
		___max_issued_token_caching_time_20 = value;
		Il2CppCodeGenWriteBarrier(&___max_issued_token_caching_time_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
