﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Hashtable
struct Hashtable_t1853889766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.MSXslScriptManager
struct  MSXslScriptManager_t4018040365  : public Il2CppObject
{
public:
	// System.Collections.Hashtable Mono.Xml.Xsl.MSXslScriptManager::scripts
	Hashtable_t1853889766 * ___scripts_0;

public:
	inline static int32_t get_offset_of_scripts_0() { return static_cast<int32_t>(offsetof(MSXslScriptManager_t4018040365, ___scripts_0)); }
	inline Hashtable_t1853889766 * get_scripts_0() const { return ___scripts_0; }
	inline Hashtable_t1853889766 ** get_address_of_scripts_0() { return &___scripts_0; }
	inline void set_scripts_0(Hashtable_t1853889766 * value)
	{
		___scripts_0 = value;
		Il2CppCodeGenWriteBarrier(&___scripts_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
