﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2404315739.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat962350765.h"

// UnityEngine.Texture3D
struct Texture3D_t1884131049;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// System.String
struct String_t;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t1068524471;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TonemappingLut
struct  TonemappingLut_t4032363238  : public PostEffectsBase_t2404315739
{
public:
	// System.Boolean TonemappingLut::enableAdaptive
	bool ___enableAdaptive_5;
	// System.Boolean TonemappingLut::enableFilmicCurve
	bool ___enableFilmicCurve_6;
	// System.Boolean TonemappingLut::enableUserLut
	bool ___enableUserLut_7;
	// System.Boolean TonemappingLut::enableColorGrading
	bool ___enableColorGrading_8;
	// System.Boolean TonemappingLut::enableColorCurve
	bool ___enableColorCurve_9;
	// System.Boolean TonemappingLut::debugClamp
	bool ___debugClamp_10;
	// UnityEngine.Texture3D TonemappingLut::lutTex
	Texture3D_t1884131049 * ___lutTex_11;
	// UnityEngine.Texture2D TonemappingLut::lutCurveTex1D
	Texture2D_t3840446185 * ___lutCurveTex1D_12;
	// System.Single TonemappingLut::middleGrey
	float ___middleGrey_13;
	// System.Single TonemappingLut::adaptionSpeed
	float ___adaptionSpeed_14;
	// System.Single TonemappingLut::adaptiveMin
	float ___adaptiveMin_15;
	// System.Single TonemappingLut::adaptiveMax
	float ___adaptiveMax_16;
	// System.Boolean TonemappingLut::adaptiveDebug
	bool ___adaptiveDebug_17;
	// System.Single TonemappingLut::lutExposureBias
	float ___lutExposureBias_18;
	// UnityEngine.Color TonemappingLut::lutWhiteBalance
	Color_t2555686324  ___lutWhiteBalance_19;
	// System.Single TonemappingLut::lutContrast
	float ___lutContrast_20;
	// System.Single TonemappingLut::lutSaturation
	float ___lutSaturation_21;
	// System.Single TonemappingLut::lutGamma
	float ___lutGamma_22;
	// System.Single TonemappingLut::lutToe
	float ___lutToe_23;
	// System.Single TonemappingLut::lutShoulder
	float ___lutShoulder_24;
	// UnityEngine.AnimationCurve TonemappingLut::remapCurve
	AnimationCurve_t3046754366 * ___remapCurve_25;
	// UnityEngine.Texture2D TonemappingLut::userLutTex
	Texture2D_t3840446185 * ___userLutTex_26;
	// System.String TonemappingLut::userLutName
	String_t* ___userLutName_27;
	// UnityEngine.Color TonemappingLut::lutShadows
	Color_t2555686324  ___lutShadows_28;
	// UnityEngine.Color TonemappingLut::lutMidtones
	Color_t2555686324  ___lutMidtones_29;
	// UnityEngine.Color TonemappingLut::lutHighlights
	Color_t2555686324  ___lutHighlights_30;
	// System.Single TonemappingLut::cache_lutContrast
	float ___cache_lutContrast_31;
	// System.Single TonemappingLut::cache_lutSaturation
	float ___cache_lutSaturation_32;
	// System.Single TonemappingLut::cache_lutGamma
	float ___cache_lutGamma_33;
	// System.Single TonemappingLut::cache_lutLowY
	float ___cache_lutLowY_34;
	// System.Single TonemappingLut::cache_lutHighY
	float ___cache_lutHighY_35;
	// System.Boolean TonemappingLut::cache_enableAdaptive
	bool ___cache_enableAdaptive_36;
	// System.Boolean TonemappingLut::cache_enableFilmicCurve
	bool ___cache_enableFilmicCurve_37;
	// System.Boolean TonemappingLut::cache_enableColorGrading
	bool ___cache_enableColorGrading_38;
	// System.Boolean TonemappingLut::cache_enableUserLut
	bool ___cache_enableUserLut_39;
	// System.Boolean TonemappingLut::cache_enableColorCurve
	bool ___cache_enableColorCurve_40;
	// UnityEngine.Color TonemappingLut::cache_lutWhiteBalance
	Color_t2555686324  ___cache_lutWhiteBalance_41;
	// UnityEngine.Color TonemappingLut::cache_lutHighlights
	Color_t2555686324  ___cache_lutHighlights_42;
	// UnityEngine.Color TonemappingLut::cache_lutMidtones
	Color_t2555686324  ___cache_lutMidtones_43;
	// UnityEngine.Color TonemappingLut::cache_lutShadows
	Color_t2555686324  ___cache_lutShadows_44;
	// UnityEngine.Keyframe[] TonemappingLut::cache_remapCurve
	KeyframeU5BU5D_t1068524471* ___cache_remapCurve_45;
	// System.String TonemappingLut::cache_userLutName
	String_t* ___cache_userLutName_46;
	// UnityEngine.Shader TonemappingLut::tonemapperLut
	Shader_t4151988712 * ___tonemapperLut_47;
	// System.Boolean TonemappingLut::validRenderTextureFormat
	bool ___validRenderTextureFormat_48;
	// UnityEngine.Material TonemappingLut::tonemapMaterial
	Material_t340375123 * ___tonemapMaterial_49;
	// UnityEngine.RenderTexture TonemappingLut::rt
	RenderTexture_t2108887433 * ___rt_50;
	// UnityEngine.RenderTextureFormat TonemappingLut::rtFormat
	int32_t ___rtFormat_51;
	// System.Int32 TonemappingLut::curveLen
	int32_t ___curveLen_52;
	// System.Single[] TonemappingLut::curveData
	SingleU5BU5D_t1444911251* ___curveData_53;
	// System.Int32 TonemappingLut::userLutDim
	int32_t ___userLutDim_54;
	// UnityEngine.Color[] TonemappingLut::userLutData
	ColorU5BU5D_t941916413* ___userLutData_55;

public:
	inline static int32_t get_offset_of_enableAdaptive_5() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___enableAdaptive_5)); }
	inline bool get_enableAdaptive_5() const { return ___enableAdaptive_5; }
	inline bool* get_address_of_enableAdaptive_5() { return &___enableAdaptive_5; }
	inline void set_enableAdaptive_5(bool value)
	{
		___enableAdaptive_5 = value;
	}

	inline static int32_t get_offset_of_enableFilmicCurve_6() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___enableFilmicCurve_6)); }
	inline bool get_enableFilmicCurve_6() const { return ___enableFilmicCurve_6; }
	inline bool* get_address_of_enableFilmicCurve_6() { return &___enableFilmicCurve_6; }
	inline void set_enableFilmicCurve_6(bool value)
	{
		___enableFilmicCurve_6 = value;
	}

	inline static int32_t get_offset_of_enableUserLut_7() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___enableUserLut_7)); }
	inline bool get_enableUserLut_7() const { return ___enableUserLut_7; }
	inline bool* get_address_of_enableUserLut_7() { return &___enableUserLut_7; }
	inline void set_enableUserLut_7(bool value)
	{
		___enableUserLut_7 = value;
	}

	inline static int32_t get_offset_of_enableColorGrading_8() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___enableColorGrading_8)); }
	inline bool get_enableColorGrading_8() const { return ___enableColorGrading_8; }
	inline bool* get_address_of_enableColorGrading_8() { return &___enableColorGrading_8; }
	inline void set_enableColorGrading_8(bool value)
	{
		___enableColorGrading_8 = value;
	}

	inline static int32_t get_offset_of_enableColorCurve_9() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___enableColorCurve_9)); }
	inline bool get_enableColorCurve_9() const { return ___enableColorCurve_9; }
	inline bool* get_address_of_enableColorCurve_9() { return &___enableColorCurve_9; }
	inline void set_enableColorCurve_9(bool value)
	{
		___enableColorCurve_9 = value;
	}

	inline static int32_t get_offset_of_debugClamp_10() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___debugClamp_10)); }
	inline bool get_debugClamp_10() const { return ___debugClamp_10; }
	inline bool* get_address_of_debugClamp_10() { return &___debugClamp_10; }
	inline void set_debugClamp_10(bool value)
	{
		___debugClamp_10 = value;
	}

	inline static int32_t get_offset_of_lutTex_11() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___lutTex_11)); }
	inline Texture3D_t1884131049 * get_lutTex_11() const { return ___lutTex_11; }
	inline Texture3D_t1884131049 ** get_address_of_lutTex_11() { return &___lutTex_11; }
	inline void set_lutTex_11(Texture3D_t1884131049 * value)
	{
		___lutTex_11 = value;
		Il2CppCodeGenWriteBarrier(&___lutTex_11, value);
	}

	inline static int32_t get_offset_of_lutCurveTex1D_12() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___lutCurveTex1D_12)); }
	inline Texture2D_t3840446185 * get_lutCurveTex1D_12() const { return ___lutCurveTex1D_12; }
	inline Texture2D_t3840446185 ** get_address_of_lutCurveTex1D_12() { return &___lutCurveTex1D_12; }
	inline void set_lutCurveTex1D_12(Texture2D_t3840446185 * value)
	{
		___lutCurveTex1D_12 = value;
		Il2CppCodeGenWriteBarrier(&___lutCurveTex1D_12, value);
	}

	inline static int32_t get_offset_of_middleGrey_13() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___middleGrey_13)); }
	inline float get_middleGrey_13() const { return ___middleGrey_13; }
	inline float* get_address_of_middleGrey_13() { return &___middleGrey_13; }
	inline void set_middleGrey_13(float value)
	{
		___middleGrey_13 = value;
	}

	inline static int32_t get_offset_of_adaptionSpeed_14() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___adaptionSpeed_14)); }
	inline float get_adaptionSpeed_14() const { return ___adaptionSpeed_14; }
	inline float* get_address_of_adaptionSpeed_14() { return &___adaptionSpeed_14; }
	inline void set_adaptionSpeed_14(float value)
	{
		___adaptionSpeed_14 = value;
	}

	inline static int32_t get_offset_of_adaptiveMin_15() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___adaptiveMin_15)); }
	inline float get_adaptiveMin_15() const { return ___adaptiveMin_15; }
	inline float* get_address_of_adaptiveMin_15() { return &___adaptiveMin_15; }
	inline void set_adaptiveMin_15(float value)
	{
		___adaptiveMin_15 = value;
	}

	inline static int32_t get_offset_of_adaptiveMax_16() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___adaptiveMax_16)); }
	inline float get_adaptiveMax_16() const { return ___adaptiveMax_16; }
	inline float* get_address_of_adaptiveMax_16() { return &___adaptiveMax_16; }
	inline void set_adaptiveMax_16(float value)
	{
		___adaptiveMax_16 = value;
	}

	inline static int32_t get_offset_of_adaptiveDebug_17() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___adaptiveDebug_17)); }
	inline bool get_adaptiveDebug_17() const { return ___adaptiveDebug_17; }
	inline bool* get_address_of_adaptiveDebug_17() { return &___adaptiveDebug_17; }
	inline void set_adaptiveDebug_17(bool value)
	{
		___adaptiveDebug_17 = value;
	}

	inline static int32_t get_offset_of_lutExposureBias_18() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___lutExposureBias_18)); }
	inline float get_lutExposureBias_18() const { return ___lutExposureBias_18; }
	inline float* get_address_of_lutExposureBias_18() { return &___lutExposureBias_18; }
	inline void set_lutExposureBias_18(float value)
	{
		___lutExposureBias_18 = value;
	}

	inline static int32_t get_offset_of_lutWhiteBalance_19() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___lutWhiteBalance_19)); }
	inline Color_t2555686324  get_lutWhiteBalance_19() const { return ___lutWhiteBalance_19; }
	inline Color_t2555686324 * get_address_of_lutWhiteBalance_19() { return &___lutWhiteBalance_19; }
	inline void set_lutWhiteBalance_19(Color_t2555686324  value)
	{
		___lutWhiteBalance_19 = value;
	}

	inline static int32_t get_offset_of_lutContrast_20() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___lutContrast_20)); }
	inline float get_lutContrast_20() const { return ___lutContrast_20; }
	inline float* get_address_of_lutContrast_20() { return &___lutContrast_20; }
	inline void set_lutContrast_20(float value)
	{
		___lutContrast_20 = value;
	}

	inline static int32_t get_offset_of_lutSaturation_21() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___lutSaturation_21)); }
	inline float get_lutSaturation_21() const { return ___lutSaturation_21; }
	inline float* get_address_of_lutSaturation_21() { return &___lutSaturation_21; }
	inline void set_lutSaturation_21(float value)
	{
		___lutSaturation_21 = value;
	}

	inline static int32_t get_offset_of_lutGamma_22() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___lutGamma_22)); }
	inline float get_lutGamma_22() const { return ___lutGamma_22; }
	inline float* get_address_of_lutGamma_22() { return &___lutGamma_22; }
	inline void set_lutGamma_22(float value)
	{
		___lutGamma_22 = value;
	}

	inline static int32_t get_offset_of_lutToe_23() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___lutToe_23)); }
	inline float get_lutToe_23() const { return ___lutToe_23; }
	inline float* get_address_of_lutToe_23() { return &___lutToe_23; }
	inline void set_lutToe_23(float value)
	{
		___lutToe_23 = value;
	}

	inline static int32_t get_offset_of_lutShoulder_24() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___lutShoulder_24)); }
	inline float get_lutShoulder_24() const { return ___lutShoulder_24; }
	inline float* get_address_of_lutShoulder_24() { return &___lutShoulder_24; }
	inline void set_lutShoulder_24(float value)
	{
		___lutShoulder_24 = value;
	}

	inline static int32_t get_offset_of_remapCurve_25() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___remapCurve_25)); }
	inline AnimationCurve_t3046754366 * get_remapCurve_25() const { return ___remapCurve_25; }
	inline AnimationCurve_t3046754366 ** get_address_of_remapCurve_25() { return &___remapCurve_25; }
	inline void set_remapCurve_25(AnimationCurve_t3046754366 * value)
	{
		___remapCurve_25 = value;
		Il2CppCodeGenWriteBarrier(&___remapCurve_25, value);
	}

	inline static int32_t get_offset_of_userLutTex_26() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___userLutTex_26)); }
	inline Texture2D_t3840446185 * get_userLutTex_26() const { return ___userLutTex_26; }
	inline Texture2D_t3840446185 ** get_address_of_userLutTex_26() { return &___userLutTex_26; }
	inline void set_userLutTex_26(Texture2D_t3840446185 * value)
	{
		___userLutTex_26 = value;
		Il2CppCodeGenWriteBarrier(&___userLutTex_26, value);
	}

	inline static int32_t get_offset_of_userLutName_27() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___userLutName_27)); }
	inline String_t* get_userLutName_27() const { return ___userLutName_27; }
	inline String_t** get_address_of_userLutName_27() { return &___userLutName_27; }
	inline void set_userLutName_27(String_t* value)
	{
		___userLutName_27 = value;
		Il2CppCodeGenWriteBarrier(&___userLutName_27, value);
	}

	inline static int32_t get_offset_of_lutShadows_28() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___lutShadows_28)); }
	inline Color_t2555686324  get_lutShadows_28() const { return ___lutShadows_28; }
	inline Color_t2555686324 * get_address_of_lutShadows_28() { return &___lutShadows_28; }
	inline void set_lutShadows_28(Color_t2555686324  value)
	{
		___lutShadows_28 = value;
	}

	inline static int32_t get_offset_of_lutMidtones_29() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___lutMidtones_29)); }
	inline Color_t2555686324  get_lutMidtones_29() const { return ___lutMidtones_29; }
	inline Color_t2555686324 * get_address_of_lutMidtones_29() { return &___lutMidtones_29; }
	inline void set_lutMidtones_29(Color_t2555686324  value)
	{
		___lutMidtones_29 = value;
	}

	inline static int32_t get_offset_of_lutHighlights_30() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___lutHighlights_30)); }
	inline Color_t2555686324  get_lutHighlights_30() const { return ___lutHighlights_30; }
	inline Color_t2555686324 * get_address_of_lutHighlights_30() { return &___lutHighlights_30; }
	inline void set_lutHighlights_30(Color_t2555686324  value)
	{
		___lutHighlights_30 = value;
	}

	inline static int32_t get_offset_of_cache_lutContrast_31() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___cache_lutContrast_31)); }
	inline float get_cache_lutContrast_31() const { return ___cache_lutContrast_31; }
	inline float* get_address_of_cache_lutContrast_31() { return &___cache_lutContrast_31; }
	inline void set_cache_lutContrast_31(float value)
	{
		___cache_lutContrast_31 = value;
	}

	inline static int32_t get_offset_of_cache_lutSaturation_32() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___cache_lutSaturation_32)); }
	inline float get_cache_lutSaturation_32() const { return ___cache_lutSaturation_32; }
	inline float* get_address_of_cache_lutSaturation_32() { return &___cache_lutSaturation_32; }
	inline void set_cache_lutSaturation_32(float value)
	{
		___cache_lutSaturation_32 = value;
	}

	inline static int32_t get_offset_of_cache_lutGamma_33() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___cache_lutGamma_33)); }
	inline float get_cache_lutGamma_33() const { return ___cache_lutGamma_33; }
	inline float* get_address_of_cache_lutGamma_33() { return &___cache_lutGamma_33; }
	inline void set_cache_lutGamma_33(float value)
	{
		___cache_lutGamma_33 = value;
	}

	inline static int32_t get_offset_of_cache_lutLowY_34() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___cache_lutLowY_34)); }
	inline float get_cache_lutLowY_34() const { return ___cache_lutLowY_34; }
	inline float* get_address_of_cache_lutLowY_34() { return &___cache_lutLowY_34; }
	inline void set_cache_lutLowY_34(float value)
	{
		___cache_lutLowY_34 = value;
	}

	inline static int32_t get_offset_of_cache_lutHighY_35() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___cache_lutHighY_35)); }
	inline float get_cache_lutHighY_35() const { return ___cache_lutHighY_35; }
	inline float* get_address_of_cache_lutHighY_35() { return &___cache_lutHighY_35; }
	inline void set_cache_lutHighY_35(float value)
	{
		___cache_lutHighY_35 = value;
	}

	inline static int32_t get_offset_of_cache_enableAdaptive_36() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___cache_enableAdaptive_36)); }
	inline bool get_cache_enableAdaptive_36() const { return ___cache_enableAdaptive_36; }
	inline bool* get_address_of_cache_enableAdaptive_36() { return &___cache_enableAdaptive_36; }
	inline void set_cache_enableAdaptive_36(bool value)
	{
		___cache_enableAdaptive_36 = value;
	}

	inline static int32_t get_offset_of_cache_enableFilmicCurve_37() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___cache_enableFilmicCurve_37)); }
	inline bool get_cache_enableFilmicCurve_37() const { return ___cache_enableFilmicCurve_37; }
	inline bool* get_address_of_cache_enableFilmicCurve_37() { return &___cache_enableFilmicCurve_37; }
	inline void set_cache_enableFilmicCurve_37(bool value)
	{
		___cache_enableFilmicCurve_37 = value;
	}

	inline static int32_t get_offset_of_cache_enableColorGrading_38() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___cache_enableColorGrading_38)); }
	inline bool get_cache_enableColorGrading_38() const { return ___cache_enableColorGrading_38; }
	inline bool* get_address_of_cache_enableColorGrading_38() { return &___cache_enableColorGrading_38; }
	inline void set_cache_enableColorGrading_38(bool value)
	{
		___cache_enableColorGrading_38 = value;
	}

	inline static int32_t get_offset_of_cache_enableUserLut_39() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___cache_enableUserLut_39)); }
	inline bool get_cache_enableUserLut_39() const { return ___cache_enableUserLut_39; }
	inline bool* get_address_of_cache_enableUserLut_39() { return &___cache_enableUserLut_39; }
	inline void set_cache_enableUserLut_39(bool value)
	{
		___cache_enableUserLut_39 = value;
	}

	inline static int32_t get_offset_of_cache_enableColorCurve_40() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___cache_enableColorCurve_40)); }
	inline bool get_cache_enableColorCurve_40() const { return ___cache_enableColorCurve_40; }
	inline bool* get_address_of_cache_enableColorCurve_40() { return &___cache_enableColorCurve_40; }
	inline void set_cache_enableColorCurve_40(bool value)
	{
		___cache_enableColorCurve_40 = value;
	}

	inline static int32_t get_offset_of_cache_lutWhiteBalance_41() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___cache_lutWhiteBalance_41)); }
	inline Color_t2555686324  get_cache_lutWhiteBalance_41() const { return ___cache_lutWhiteBalance_41; }
	inline Color_t2555686324 * get_address_of_cache_lutWhiteBalance_41() { return &___cache_lutWhiteBalance_41; }
	inline void set_cache_lutWhiteBalance_41(Color_t2555686324  value)
	{
		___cache_lutWhiteBalance_41 = value;
	}

	inline static int32_t get_offset_of_cache_lutHighlights_42() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___cache_lutHighlights_42)); }
	inline Color_t2555686324  get_cache_lutHighlights_42() const { return ___cache_lutHighlights_42; }
	inline Color_t2555686324 * get_address_of_cache_lutHighlights_42() { return &___cache_lutHighlights_42; }
	inline void set_cache_lutHighlights_42(Color_t2555686324  value)
	{
		___cache_lutHighlights_42 = value;
	}

	inline static int32_t get_offset_of_cache_lutMidtones_43() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___cache_lutMidtones_43)); }
	inline Color_t2555686324  get_cache_lutMidtones_43() const { return ___cache_lutMidtones_43; }
	inline Color_t2555686324 * get_address_of_cache_lutMidtones_43() { return &___cache_lutMidtones_43; }
	inline void set_cache_lutMidtones_43(Color_t2555686324  value)
	{
		___cache_lutMidtones_43 = value;
	}

	inline static int32_t get_offset_of_cache_lutShadows_44() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___cache_lutShadows_44)); }
	inline Color_t2555686324  get_cache_lutShadows_44() const { return ___cache_lutShadows_44; }
	inline Color_t2555686324 * get_address_of_cache_lutShadows_44() { return &___cache_lutShadows_44; }
	inline void set_cache_lutShadows_44(Color_t2555686324  value)
	{
		___cache_lutShadows_44 = value;
	}

	inline static int32_t get_offset_of_cache_remapCurve_45() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___cache_remapCurve_45)); }
	inline KeyframeU5BU5D_t1068524471* get_cache_remapCurve_45() const { return ___cache_remapCurve_45; }
	inline KeyframeU5BU5D_t1068524471** get_address_of_cache_remapCurve_45() { return &___cache_remapCurve_45; }
	inline void set_cache_remapCurve_45(KeyframeU5BU5D_t1068524471* value)
	{
		___cache_remapCurve_45 = value;
		Il2CppCodeGenWriteBarrier(&___cache_remapCurve_45, value);
	}

	inline static int32_t get_offset_of_cache_userLutName_46() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___cache_userLutName_46)); }
	inline String_t* get_cache_userLutName_46() const { return ___cache_userLutName_46; }
	inline String_t** get_address_of_cache_userLutName_46() { return &___cache_userLutName_46; }
	inline void set_cache_userLutName_46(String_t* value)
	{
		___cache_userLutName_46 = value;
		Il2CppCodeGenWriteBarrier(&___cache_userLutName_46, value);
	}

	inline static int32_t get_offset_of_tonemapperLut_47() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___tonemapperLut_47)); }
	inline Shader_t4151988712 * get_tonemapperLut_47() const { return ___tonemapperLut_47; }
	inline Shader_t4151988712 ** get_address_of_tonemapperLut_47() { return &___tonemapperLut_47; }
	inline void set_tonemapperLut_47(Shader_t4151988712 * value)
	{
		___tonemapperLut_47 = value;
		Il2CppCodeGenWriteBarrier(&___tonemapperLut_47, value);
	}

	inline static int32_t get_offset_of_validRenderTextureFormat_48() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___validRenderTextureFormat_48)); }
	inline bool get_validRenderTextureFormat_48() const { return ___validRenderTextureFormat_48; }
	inline bool* get_address_of_validRenderTextureFormat_48() { return &___validRenderTextureFormat_48; }
	inline void set_validRenderTextureFormat_48(bool value)
	{
		___validRenderTextureFormat_48 = value;
	}

	inline static int32_t get_offset_of_tonemapMaterial_49() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___tonemapMaterial_49)); }
	inline Material_t340375123 * get_tonemapMaterial_49() const { return ___tonemapMaterial_49; }
	inline Material_t340375123 ** get_address_of_tonemapMaterial_49() { return &___tonemapMaterial_49; }
	inline void set_tonemapMaterial_49(Material_t340375123 * value)
	{
		___tonemapMaterial_49 = value;
		Il2CppCodeGenWriteBarrier(&___tonemapMaterial_49, value);
	}

	inline static int32_t get_offset_of_rt_50() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___rt_50)); }
	inline RenderTexture_t2108887433 * get_rt_50() const { return ___rt_50; }
	inline RenderTexture_t2108887433 ** get_address_of_rt_50() { return &___rt_50; }
	inline void set_rt_50(RenderTexture_t2108887433 * value)
	{
		___rt_50 = value;
		Il2CppCodeGenWriteBarrier(&___rt_50, value);
	}

	inline static int32_t get_offset_of_rtFormat_51() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___rtFormat_51)); }
	inline int32_t get_rtFormat_51() const { return ___rtFormat_51; }
	inline int32_t* get_address_of_rtFormat_51() { return &___rtFormat_51; }
	inline void set_rtFormat_51(int32_t value)
	{
		___rtFormat_51 = value;
	}

	inline static int32_t get_offset_of_curveLen_52() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___curveLen_52)); }
	inline int32_t get_curveLen_52() const { return ___curveLen_52; }
	inline int32_t* get_address_of_curveLen_52() { return &___curveLen_52; }
	inline void set_curveLen_52(int32_t value)
	{
		___curveLen_52 = value;
	}

	inline static int32_t get_offset_of_curveData_53() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___curveData_53)); }
	inline SingleU5BU5D_t1444911251* get_curveData_53() const { return ___curveData_53; }
	inline SingleU5BU5D_t1444911251** get_address_of_curveData_53() { return &___curveData_53; }
	inline void set_curveData_53(SingleU5BU5D_t1444911251* value)
	{
		___curveData_53 = value;
		Il2CppCodeGenWriteBarrier(&___curveData_53, value);
	}

	inline static int32_t get_offset_of_userLutDim_54() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___userLutDim_54)); }
	inline int32_t get_userLutDim_54() const { return ___userLutDim_54; }
	inline int32_t* get_address_of_userLutDim_54() { return &___userLutDim_54; }
	inline void set_userLutDim_54(int32_t value)
	{
		___userLutDim_54 = value;
	}

	inline static int32_t get_offset_of_userLutData_55() { return static_cast<int32_t>(offsetof(TonemappingLut_t4032363238, ___userLutData_55)); }
	inline ColorU5BU5D_t941916413* get_userLutData_55() const { return ___userLutData_55; }
	inline ColorU5BU5D_t941916413** get_address_of_userLutData_55() { return &___userLutData_55; }
	inline void set_userLutData_55(ColorU5BU5D_t941916413* value)
	{
		___userLutData_55 = value;
		Il2CppCodeGenWriteBarrier(&___userLutData_55, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
