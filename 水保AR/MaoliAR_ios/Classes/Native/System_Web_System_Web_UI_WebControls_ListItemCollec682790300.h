﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.ArrayList
struct ArrayList_t2718874744;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.WebControls.ListItemCollection
struct  ListItemCollection_t682790300  : public Il2CppObject
{
public:
	// System.Collections.ArrayList System.Web.UI.WebControls.ListItemCollection::items
	ArrayList_t2718874744 * ___items_0;
	// System.Boolean System.Web.UI.WebControls.ListItemCollection::tracking
	bool ___tracking_1;
	// System.Boolean System.Web.UI.WebControls.ListItemCollection::dirty
	bool ___dirty_2;
	// System.Int32 System.Web.UI.WebControls.ListItemCollection::lastDirty
	int32_t ___lastDirty_3;

public:
	inline static int32_t get_offset_of_items_0() { return static_cast<int32_t>(offsetof(ListItemCollection_t682790300, ___items_0)); }
	inline ArrayList_t2718874744 * get_items_0() const { return ___items_0; }
	inline ArrayList_t2718874744 ** get_address_of_items_0() { return &___items_0; }
	inline void set_items_0(ArrayList_t2718874744 * value)
	{
		___items_0 = value;
		Il2CppCodeGenWriteBarrier(&___items_0, value);
	}

	inline static int32_t get_offset_of_tracking_1() { return static_cast<int32_t>(offsetof(ListItemCollection_t682790300, ___tracking_1)); }
	inline bool get_tracking_1() const { return ___tracking_1; }
	inline bool* get_address_of_tracking_1() { return &___tracking_1; }
	inline void set_tracking_1(bool value)
	{
		___tracking_1 = value;
	}

	inline static int32_t get_offset_of_dirty_2() { return static_cast<int32_t>(offsetof(ListItemCollection_t682790300, ___dirty_2)); }
	inline bool get_dirty_2() const { return ___dirty_2; }
	inline bool* get_address_of_dirty_2() { return &___dirty_2; }
	inline void set_dirty_2(bool value)
	{
		___dirty_2 = value;
	}

	inline static int32_t get_offset_of_lastDirty_3() { return static_cast<int32_t>(offsetof(ListItemCollection_t682790300, ___lastDirty_3)); }
	inline int32_t get_lastDirty_3() const { return ___lastDirty_3; }
	inline int32_t* get_address_of_lastDirty_3() { return &___lastDirty_3; }
	inline void set_lastDirty_3(int32_t value)
	{
		___lastDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
