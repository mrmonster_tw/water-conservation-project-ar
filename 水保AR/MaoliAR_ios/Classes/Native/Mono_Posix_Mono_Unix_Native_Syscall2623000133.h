﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Mono_Posix_Mono_Unix_Native_Stdlib3308777473.h"
#include "mscorlib_System_IntPtr840150181.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Unix.Native.Syscall
struct  Syscall_t2623000133  : public Stdlib_t3308777473
{
public:

public:
};

struct Syscall_t2623000133_StaticFields
{
public:
	// System.Object Mono.Unix.Native.Syscall::readdir_lock
	Il2CppObject * ___readdir_lock_25;
	// System.Object Mono.Unix.Native.Syscall::fstab_lock
	Il2CppObject * ___fstab_lock_26;
	// System.Object Mono.Unix.Native.Syscall::grp_lock
	Il2CppObject * ___grp_lock_27;
	// System.Object Mono.Unix.Native.Syscall::pwd_lock
	Il2CppObject * ___pwd_lock_28;
	// System.Object Mono.Unix.Native.Syscall::signal_lock
	Il2CppObject * ___signal_lock_29;
	// System.Int32 Mono.Unix.Native.Syscall::L_ctermid
	int32_t ___L_ctermid_30;
	// System.Int32 Mono.Unix.Native.Syscall::L_cuserid
	int32_t ___L_cuserid_31;
	// System.Object Mono.Unix.Native.Syscall::getlogin_lock
	Il2CppObject * ___getlogin_lock_32;
	// System.IntPtr Mono.Unix.Native.Syscall::MAP_FAILED
	IntPtr_t ___MAP_FAILED_33;
	// System.Object Mono.Unix.Native.Syscall::tty_lock
	Il2CppObject * ___tty_lock_34;
	// System.Object Mono.Unix.Native.Syscall::usershell_lock
	Il2CppObject * ___usershell_lock_35;

public:
	inline static int32_t get_offset_of_readdir_lock_25() { return static_cast<int32_t>(offsetof(Syscall_t2623000133_StaticFields, ___readdir_lock_25)); }
	inline Il2CppObject * get_readdir_lock_25() const { return ___readdir_lock_25; }
	inline Il2CppObject ** get_address_of_readdir_lock_25() { return &___readdir_lock_25; }
	inline void set_readdir_lock_25(Il2CppObject * value)
	{
		___readdir_lock_25 = value;
		Il2CppCodeGenWriteBarrier(&___readdir_lock_25, value);
	}

	inline static int32_t get_offset_of_fstab_lock_26() { return static_cast<int32_t>(offsetof(Syscall_t2623000133_StaticFields, ___fstab_lock_26)); }
	inline Il2CppObject * get_fstab_lock_26() const { return ___fstab_lock_26; }
	inline Il2CppObject ** get_address_of_fstab_lock_26() { return &___fstab_lock_26; }
	inline void set_fstab_lock_26(Il2CppObject * value)
	{
		___fstab_lock_26 = value;
		Il2CppCodeGenWriteBarrier(&___fstab_lock_26, value);
	}

	inline static int32_t get_offset_of_grp_lock_27() { return static_cast<int32_t>(offsetof(Syscall_t2623000133_StaticFields, ___grp_lock_27)); }
	inline Il2CppObject * get_grp_lock_27() const { return ___grp_lock_27; }
	inline Il2CppObject ** get_address_of_grp_lock_27() { return &___grp_lock_27; }
	inline void set_grp_lock_27(Il2CppObject * value)
	{
		___grp_lock_27 = value;
		Il2CppCodeGenWriteBarrier(&___grp_lock_27, value);
	}

	inline static int32_t get_offset_of_pwd_lock_28() { return static_cast<int32_t>(offsetof(Syscall_t2623000133_StaticFields, ___pwd_lock_28)); }
	inline Il2CppObject * get_pwd_lock_28() const { return ___pwd_lock_28; }
	inline Il2CppObject ** get_address_of_pwd_lock_28() { return &___pwd_lock_28; }
	inline void set_pwd_lock_28(Il2CppObject * value)
	{
		___pwd_lock_28 = value;
		Il2CppCodeGenWriteBarrier(&___pwd_lock_28, value);
	}

	inline static int32_t get_offset_of_signal_lock_29() { return static_cast<int32_t>(offsetof(Syscall_t2623000133_StaticFields, ___signal_lock_29)); }
	inline Il2CppObject * get_signal_lock_29() const { return ___signal_lock_29; }
	inline Il2CppObject ** get_address_of_signal_lock_29() { return &___signal_lock_29; }
	inline void set_signal_lock_29(Il2CppObject * value)
	{
		___signal_lock_29 = value;
		Il2CppCodeGenWriteBarrier(&___signal_lock_29, value);
	}

	inline static int32_t get_offset_of_L_ctermid_30() { return static_cast<int32_t>(offsetof(Syscall_t2623000133_StaticFields, ___L_ctermid_30)); }
	inline int32_t get_L_ctermid_30() const { return ___L_ctermid_30; }
	inline int32_t* get_address_of_L_ctermid_30() { return &___L_ctermid_30; }
	inline void set_L_ctermid_30(int32_t value)
	{
		___L_ctermid_30 = value;
	}

	inline static int32_t get_offset_of_L_cuserid_31() { return static_cast<int32_t>(offsetof(Syscall_t2623000133_StaticFields, ___L_cuserid_31)); }
	inline int32_t get_L_cuserid_31() const { return ___L_cuserid_31; }
	inline int32_t* get_address_of_L_cuserid_31() { return &___L_cuserid_31; }
	inline void set_L_cuserid_31(int32_t value)
	{
		___L_cuserid_31 = value;
	}

	inline static int32_t get_offset_of_getlogin_lock_32() { return static_cast<int32_t>(offsetof(Syscall_t2623000133_StaticFields, ___getlogin_lock_32)); }
	inline Il2CppObject * get_getlogin_lock_32() const { return ___getlogin_lock_32; }
	inline Il2CppObject ** get_address_of_getlogin_lock_32() { return &___getlogin_lock_32; }
	inline void set_getlogin_lock_32(Il2CppObject * value)
	{
		___getlogin_lock_32 = value;
		Il2CppCodeGenWriteBarrier(&___getlogin_lock_32, value);
	}

	inline static int32_t get_offset_of_MAP_FAILED_33() { return static_cast<int32_t>(offsetof(Syscall_t2623000133_StaticFields, ___MAP_FAILED_33)); }
	inline IntPtr_t get_MAP_FAILED_33() const { return ___MAP_FAILED_33; }
	inline IntPtr_t* get_address_of_MAP_FAILED_33() { return &___MAP_FAILED_33; }
	inline void set_MAP_FAILED_33(IntPtr_t value)
	{
		___MAP_FAILED_33 = value;
	}

	inline static int32_t get_offset_of_tty_lock_34() { return static_cast<int32_t>(offsetof(Syscall_t2623000133_StaticFields, ___tty_lock_34)); }
	inline Il2CppObject * get_tty_lock_34() const { return ___tty_lock_34; }
	inline Il2CppObject ** get_address_of_tty_lock_34() { return &___tty_lock_34; }
	inline void set_tty_lock_34(Il2CppObject * value)
	{
		___tty_lock_34 = value;
		Il2CppCodeGenWriteBarrier(&___tty_lock_34, value);
	}

	inline static int32_t get_offset_of_usershell_lock_35() { return static_cast<int32_t>(offsetof(Syscall_t2623000133_StaticFields, ___usershell_lock_35)); }
	inline Il2CppObject * get_usershell_lock_35() const { return ___usershell_lock_35; }
	inline Il2CppObject ** get_address_of_usershell_lock_35() { return &___usershell_lock_35; }
	inline void set_usershell_lock_35(Il2CppObject * value)
	{
		___usershell_lock_35 = value;
		Il2CppCodeGenWriteBarrier(&___usershell_lock_35, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
