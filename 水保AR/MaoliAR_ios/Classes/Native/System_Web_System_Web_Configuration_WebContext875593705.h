﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Web_System_Web_Configuration_WebApplication1989904319.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.WebContext
struct  WebContext_t875593705  : public Il2CppObject
{
public:
	// System.Web.Configuration.WebApplicationLevel System.Web.Configuration.WebContext::pathLevel
	int32_t ___pathLevel_0;
	// System.String System.Web.Configuration.WebContext::site
	String_t* ___site_1;
	// System.String System.Web.Configuration.WebContext::applicationPath
	String_t* ___applicationPath_2;
	// System.String System.Web.Configuration.WebContext::path
	String_t* ___path_3;
	// System.String System.Web.Configuration.WebContext::locationSubPath
	String_t* ___locationSubPath_4;

public:
	inline static int32_t get_offset_of_pathLevel_0() { return static_cast<int32_t>(offsetof(WebContext_t875593705, ___pathLevel_0)); }
	inline int32_t get_pathLevel_0() const { return ___pathLevel_0; }
	inline int32_t* get_address_of_pathLevel_0() { return &___pathLevel_0; }
	inline void set_pathLevel_0(int32_t value)
	{
		___pathLevel_0 = value;
	}

	inline static int32_t get_offset_of_site_1() { return static_cast<int32_t>(offsetof(WebContext_t875593705, ___site_1)); }
	inline String_t* get_site_1() const { return ___site_1; }
	inline String_t** get_address_of_site_1() { return &___site_1; }
	inline void set_site_1(String_t* value)
	{
		___site_1 = value;
		Il2CppCodeGenWriteBarrier(&___site_1, value);
	}

	inline static int32_t get_offset_of_applicationPath_2() { return static_cast<int32_t>(offsetof(WebContext_t875593705, ___applicationPath_2)); }
	inline String_t* get_applicationPath_2() const { return ___applicationPath_2; }
	inline String_t** get_address_of_applicationPath_2() { return &___applicationPath_2; }
	inline void set_applicationPath_2(String_t* value)
	{
		___applicationPath_2 = value;
		Il2CppCodeGenWriteBarrier(&___applicationPath_2, value);
	}

	inline static int32_t get_offset_of_path_3() { return static_cast<int32_t>(offsetof(WebContext_t875593705, ___path_3)); }
	inline String_t* get_path_3() const { return ___path_3; }
	inline String_t** get_address_of_path_3() { return &___path_3; }
	inline void set_path_3(String_t* value)
	{
		___path_3 = value;
		Il2CppCodeGenWriteBarrier(&___path_3, value);
	}

	inline static int32_t get_offset_of_locationSubPath_4() { return static_cast<int32_t>(offsetof(WebContext_t875593705, ___locationSubPath_4)); }
	inline String_t* get_locationSubPath_4() const { return ___locationSubPath_4; }
	inline String_t** get_address_of_locationSubPath_4() { return &___locationSubPath_4; }
	inline void set_locationSubPath_4(String_t* value)
	{
		___locationSubPath_4 = value;
		Il2CppCodeGenWriteBarrier(&___locationSubPath_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
