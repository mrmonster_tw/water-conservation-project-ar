﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Tokens_B3487952943.h"

// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t714049126;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Tokens.X509RawDataKeyIdentifierClause
struct  X509RawDataKeyIdentifierClause_t329336726  : public BinaryKeyIdentifierClause_t3487952943
{
public:
	// System.Security.Cryptography.X509Certificates.X509Certificate2 System.IdentityModel.Tokens.X509RawDataKeyIdentifierClause::cert
	X509Certificate2_t714049126 * ___cert_4;

public:
	inline static int32_t get_offset_of_cert_4() { return static_cast<int32_t>(offsetof(X509RawDataKeyIdentifierClause_t329336726, ___cert_4)); }
	inline X509Certificate2_t714049126 * get_cert_4() const { return ___cert_4; }
	inline X509Certificate2_t714049126 ** get_address_of_cert_4() { return &___cert_4; }
	inline void set_cert_4(X509Certificate2_t714049126 * value)
	{
		___cert_4 = value;
		Il2CppCodeGenWriteBarrier(&___cert_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
