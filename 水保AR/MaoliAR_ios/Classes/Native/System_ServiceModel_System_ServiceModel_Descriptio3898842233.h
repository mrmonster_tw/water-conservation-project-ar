﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_B3441673553.h"

// System.ServiceModel.Description.WstRequestSecurityToken
struct WstRequestSecurityToken_t4025507634;
// System.IdentityModel.Selectors.SecurityTokenSerializer
struct SecurityTokenSerializer_t972969391;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.WstRequestSecurityTokenWriter
struct  WstRequestSecurityTokenWriter_t3898842233  : public BodyWriter_t3441673553
{
public:
	// System.ServiceModel.Description.WstRequestSecurityToken System.ServiceModel.Description.WstRequestSecurityTokenWriter::value
	WstRequestSecurityToken_t4025507634 * ___value_1;
	// System.IdentityModel.Selectors.SecurityTokenSerializer System.ServiceModel.Description.WstRequestSecurityTokenWriter::serializer
	SecurityTokenSerializer_t972969391 * ___serializer_2;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(WstRequestSecurityTokenWriter_t3898842233, ___value_1)); }
	inline WstRequestSecurityToken_t4025507634 * get_value_1() const { return ___value_1; }
	inline WstRequestSecurityToken_t4025507634 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(WstRequestSecurityToken_t4025507634 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}

	inline static int32_t get_offset_of_serializer_2() { return static_cast<int32_t>(offsetof(WstRequestSecurityTokenWriter_t3898842233, ___serializer_2)); }
	inline SecurityTokenSerializer_t972969391 * get_serializer_2() const { return ___serializer_2; }
	inline SecurityTokenSerializer_t972969391 ** get_address_of_serializer_2() { return &___serializer_2; }
	inline void set_serializer_2(SecurityTokenSerializer_t972969391 * value)
	{
		___serializer_2 = value;
		Il2CppCodeGenWriteBarrier(&___serializer_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
