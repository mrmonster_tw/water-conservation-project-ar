﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Security_To589097440.h"

// System.IdentityModel.Selectors.SecurityTokenRequirement
struct SecurityTokenRequirement_t1975057878;
// System.ServiceModel.Security.Tokens.SecurityContextSecurityTokenAuthenticator
struct SecurityContextSecurityTokenAuthenticator_t4052758026;
// System.ServiceModel.Security.Tokens.SecurityContextSecurityTokenResolver
struct SecurityContextSecurityTokenResolver_t1523964452;
// System.ServiceModel.Security.Tokens.WsscAuthenticatorCommunicationObject
struct WsscAuthenticatorCommunicationObject_t2840861221;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.SecureConversationSecurityTokenAuthenticator
struct  SecureConversationSecurityTokenAuthenticator_t2082245521  : public CommunicationSecurityTokenAuthenticator_t589097440
{
public:
	// System.IdentityModel.Selectors.SecurityTokenRequirement System.ServiceModel.Security.Tokens.SecureConversationSecurityTokenAuthenticator::req
	SecurityTokenRequirement_t1975057878 * ___req_1;
	// System.ServiceModel.Security.Tokens.SecurityContextSecurityTokenAuthenticator System.ServiceModel.Security.Tokens.SecureConversationSecurityTokenAuthenticator::sc_auth
	SecurityContextSecurityTokenAuthenticator_t4052758026 * ___sc_auth_2;
	// System.ServiceModel.Security.Tokens.SecurityContextSecurityTokenResolver System.ServiceModel.Security.Tokens.SecureConversationSecurityTokenAuthenticator::sc_res
	SecurityContextSecurityTokenResolver_t1523964452 * ___sc_res_3;
	// System.ServiceModel.Security.Tokens.WsscAuthenticatorCommunicationObject System.ServiceModel.Security.Tokens.SecureConversationSecurityTokenAuthenticator::comm
	WsscAuthenticatorCommunicationObject_t2840861221 * ___comm_4;

public:
	inline static int32_t get_offset_of_req_1() { return static_cast<int32_t>(offsetof(SecureConversationSecurityTokenAuthenticator_t2082245521, ___req_1)); }
	inline SecurityTokenRequirement_t1975057878 * get_req_1() const { return ___req_1; }
	inline SecurityTokenRequirement_t1975057878 ** get_address_of_req_1() { return &___req_1; }
	inline void set_req_1(SecurityTokenRequirement_t1975057878 * value)
	{
		___req_1 = value;
		Il2CppCodeGenWriteBarrier(&___req_1, value);
	}

	inline static int32_t get_offset_of_sc_auth_2() { return static_cast<int32_t>(offsetof(SecureConversationSecurityTokenAuthenticator_t2082245521, ___sc_auth_2)); }
	inline SecurityContextSecurityTokenAuthenticator_t4052758026 * get_sc_auth_2() const { return ___sc_auth_2; }
	inline SecurityContextSecurityTokenAuthenticator_t4052758026 ** get_address_of_sc_auth_2() { return &___sc_auth_2; }
	inline void set_sc_auth_2(SecurityContextSecurityTokenAuthenticator_t4052758026 * value)
	{
		___sc_auth_2 = value;
		Il2CppCodeGenWriteBarrier(&___sc_auth_2, value);
	}

	inline static int32_t get_offset_of_sc_res_3() { return static_cast<int32_t>(offsetof(SecureConversationSecurityTokenAuthenticator_t2082245521, ___sc_res_3)); }
	inline SecurityContextSecurityTokenResolver_t1523964452 * get_sc_res_3() const { return ___sc_res_3; }
	inline SecurityContextSecurityTokenResolver_t1523964452 ** get_address_of_sc_res_3() { return &___sc_res_3; }
	inline void set_sc_res_3(SecurityContextSecurityTokenResolver_t1523964452 * value)
	{
		___sc_res_3 = value;
		Il2CppCodeGenWriteBarrier(&___sc_res_3, value);
	}

	inline static int32_t get_offset_of_comm_4() { return static_cast<int32_t>(offsetof(SecureConversationSecurityTokenAuthenticator_t2082245521, ___comm_4)); }
	inline WsscAuthenticatorCommunicationObject_t2840861221 * get_comm_4() const { return ___comm_4; }
	inline WsscAuthenticatorCommunicationObject_t2840861221 ** get_address_of_comm_4() { return &___comm_4; }
	inline void set_comm_4(WsscAuthenticatorCommunicationObject_t2840861221 * value)
	{
		___comm_4 = value;
		Il2CppCodeGenWriteBarrier(&___comm_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
