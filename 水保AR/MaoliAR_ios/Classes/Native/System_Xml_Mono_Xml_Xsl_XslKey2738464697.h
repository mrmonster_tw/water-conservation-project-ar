﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2760654312;
// System.Xml.XPath.CompiledExpression
struct CompiledExpression_t4018285681;
// Mono.Xml.XPath.Pattern
struct Pattern_t1136864796;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.XslKey
struct  XslKey_t2738464697  : public Il2CppObject
{
public:
	// System.Xml.XmlQualifiedName Mono.Xml.Xsl.XslKey::name
	XmlQualifiedName_t2760654312 * ___name_0;
	// System.Xml.XPath.CompiledExpression Mono.Xml.Xsl.XslKey::useExpr
	CompiledExpression_t4018285681 * ___useExpr_1;
	// Mono.Xml.XPath.Pattern Mono.Xml.Xsl.XslKey::matchPattern
	Pattern_t1136864796 * ___matchPattern_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(XslKey_t2738464697, ___name_0)); }
	inline XmlQualifiedName_t2760654312 * get_name_0() const { return ___name_0; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(XmlQualifiedName_t2760654312 * value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_useExpr_1() { return static_cast<int32_t>(offsetof(XslKey_t2738464697, ___useExpr_1)); }
	inline CompiledExpression_t4018285681 * get_useExpr_1() const { return ___useExpr_1; }
	inline CompiledExpression_t4018285681 ** get_address_of_useExpr_1() { return &___useExpr_1; }
	inline void set_useExpr_1(CompiledExpression_t4018285681 * value)
	{
		___useExpr_1 = value;
		Il2CppCodeGenWriteBarrier(&___useExpr_1, value);
	}

	inline static int32_t get_offset_of_matchPattern_2() { return static_cast<int32_t>(offsetof(XslKey_t2738464697, ___matchPattern_2)); }
	inline Pattern_t1136864796 * get_matchPattern_2() const { return ___matchPattern_2; }
	inline Pattern_t1136864796 ** get_address_of_matchPattern_2() { return &___matchPattern_2; }
	inline void set_matchPattern_2(Pattern_t1136864796 * value)
	{
		___matchPattern_2 = value;
		Il2CppCodeGenWriteBarrier(&___matchPattern_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
