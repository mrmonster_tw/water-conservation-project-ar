﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_B3643137745.h"

// System.ServiceModel.TransactionProtocol
struct TransactionProtocol_t3972232485;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.TransactionFlowBindingElement
struct  TransactionFlowBindingElement_t881905444  : public BindingElement_t3643137745
{
public:
	// System.ServiceModel.TransactionProtocol System.ServiceModel.Channels.TransactionFlowBindingElement::protocol
	TransactionProtocol_t3972232485 * ___protocol_0;

public:
	inline static int32_t get_offset_of_protocol_0() { return static_cast<int32_t>(offsetof(TransactionFlowBindingElement_t881905444, ___protocol_0)); }
	inline TransactionProtocol_t3972232485 * get_protocol_0() const { return ___protocol_0; }
	inline TransactionProtocol_t3972232485 ** get_address_of_protocol_0() { return &___protocol_0; }
	inline void set_protocol_0(TransactionProtocol_t3972232485 * value)
	{
		___protocol_0 = value;
		Il2CppCodeGenWriteBarrier(&___protocol_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
