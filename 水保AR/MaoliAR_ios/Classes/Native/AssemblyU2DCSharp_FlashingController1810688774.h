﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HighlighterController3493614325.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlashingController
struct  FlashingController_t1810688774  : public HighlighterController_t3493614325
{
public:
	// UnityEngine.Color FlashingController::flashingStartColor
	Color_t2555686324  ___flashingStartColor_5;
	// UnityEngine.Color FlashingController::flashingEndColor
	Color_t2555686324  ___flashingEndColor_6;
	// System.Single FlashingController::flashingDelay
	float ___flashingDelay_7;
	// System.Single FlashingController::flashingFrequency
	float ___flashingFrequency_8;

public:
	inline static int32_t get_offset_of_flashingStartColor_5() { return static_cast<int32_t>(offsetof(FlashingController_t1810688774, ___flashingStartColor_5)); }
	inline Color_t2555686324  get_flashingStartColor_5() const { return ___flashingStartColor_5; }
	inline Color_t2555686324 * get_address_of_flashingStartColor_5() { return &___flashingStartColor_5; }
	inline void set_flashingStartColor_5(Color_t2555686324  value)
	{
		___flashingStartColor_5 = value;
	}

	inline static int32_t get_offset_of_flashingEndColor_6() { return static_cast<int32_t>(offsetof(FlashingController_t1810688774, ___flashingEndColor_6)); }
	inline Color_t2555686324  get_flashingEndColor_6() const { return ___flashingEndColor_6; }
	inline Color_t2555686324 * get_address_of_flashingEndColor_6() { return &___flashingEndColor_6; }
	inline void set_flashingEndColor_6(Color_t2555686324  value)
	{
		___flashingEndColor_6 = value;
	}

	inline static int32_t get_offset_of_flashingDelay_7() { return static_cast<int32_t>(offsetof(FlashingController_t1810688774, ___flashingDelay_7)); }
	inline float get_flashingDelay_7() const { return ___flashingDelay_7; }
	inline float* get_address_of_flashingDelay_7() { return &___flashingDelay_7; }
	inline void set_flashingDelay_7(float value)
	{
		___flashingDelay_7 = value;
	}

	inline static int32_t get_offset_of_flashingFrequency_8() { return static_cast<int32_t>(offsetof(FlashingController_t1810688774, ___flashingFrequency_8)); }
	inline float get_flashingFrequency_8() const { return ___flashingFrequency_8; }
	inline float* get_address_of_flashingFrequency_8() { return &___flashingFrequency_8; }
	inline void set_flashingFrequency_8(float value)
	{
		___flashingFrequency_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
