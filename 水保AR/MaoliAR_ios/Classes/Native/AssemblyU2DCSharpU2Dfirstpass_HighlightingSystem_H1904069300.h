﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// System.Collections.Generic.List`1<HighlightingSystem.Highlighter/RendererCache/Data>
struct List_1_t3060799844;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HighlightingSystem.Highlighter/RendererCache
struct  RendererCache_t1904069300  : public Il2CppObject
{
public:
	// System.Boolean HighlightingSystem.Highlighter/RendererCache::<visible>k__BackingField
	bool ___U3CvisibleU3Ek__BackingField_7;
	// UnityEngine.GameObject HighlightingSystem.Highlighter/RendererCache::go
	GameObject_t1113636619 * ___go_8;
	// UnityEngine.Renderer HighlightingSystem.Highlighter/RendererCache::renderer
	Renderer_t2627027031 * ___renderer_9;
	// System.Collections.Generic.List`1<HighlightingSystem.Highlighter/RendererCache/Data> HighlightingSystem.Highlighter/RendererCache::data
	List_1_t3060799844 * ___data_10;

public:
	inline static int32_t get_offset_of_U3CvisibleU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(RendererCache_t1904069300, ___U3CvisibleU3Ek__BackingField_7)); }
	inline bool get_U3CvisibleU3Ek__BackingField_7() const { return ___U3CvisibleU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CvisibleU3Ek__BackingField_7() { return &___U3CvisibleU3Ek__BackingField_7; }
	inline void set_U3CvisibleU3Ek__BackingField_7(bool value)
	{
		___U3CvisibleU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_go_8() { return static_cast<int32_t>(offsetof(RendererCache_t1904069300, ___go_8)); }
	inline GameObject_t1113636619 * get_go_8() const { return ___go_8; }
	inline GameObject_t1113636619 ** get_address_of_go_8() { return &___go_8; }
	inline void set_go_8(GameObject_t1113636619 * value)
	{
		___go_8 = value;
		Il2CppCodeGenWriteBarrier(&___go_8, value);
	}

	inline static int32_t get_offset_of_renderer_9() { return static_cast<int32_t>(offsetof(RendererCache_t1904069300, ___renderer_9)); }
	inline Renderer_t2627027031 * get_renderer_9() const { return ___renderer_9; }
	inline Renderer_t2627027031 ** get_address_of_renderer_9() { return &___renderer_9; }
	inline void set_renderer_9(Renderer_t2627027031 * value)
	{
		___renderer_9 = value;
		Il2CppCodeGenWriteBarrier(&___renderer_9, value);
	}

	inline static int32_t get_offset_of_data_10() { return static_cast<int32_t>(offsetof(RendererCache_t1904069300, ___data_10)); }
	inline List_1_t3060799844 * get_data_10() const { return ___data_10; }
	inline List_1_t3060799844 ** get_address_of_data_10() { return &___data_10; }
	inline void set_data_10(List_1_t3060799844 * value)
	{
		___data_10 = value;
		Il2CppCodeGenWriteBarrier(&___data_10, value);
	}
};

struct RendererCache_t1904069300_StaticFields
{
public:
	// System.String HighlightingSystem.Highlighter/RendererCache::sRenderType
	String_t* ___sRenderType_0;
	// System.String HighlightingSystem.Highlighter/RendererCache::sOpaque
	String_t* ___sOpaque_1;
	// System.String HighlightingSystem.Highlighter/RendererCache::sTransparent
	String_t* ___sTransparent_2;
	// System.String HighlightingSystem.Highlighter/RendererCache::sTransparentCutout
	String_t* ___sTransparentCutout_3;
	// System.String HighlightingSystem.Highlighter/RendererCache::sMainTex
	String_t* ___sMainTex_4;

public:
	inline static int32_t get_offset_of_sRenderType_0() { return static_cast<int32_t>(offsetof(RendererCache_t1904069300_StaticFields, ___sRenderType_0)); }
	inline String_t* get_sRenderType_0() const { return ___sRenderType_0; }
	inline String_t** get_address_of_sRenderType_0() { return &___sRenderType_0; }
	inline void set_sRenderType_0(String_t* value)
	{
		___sRenderType_0 = value;
		Il2CppCodeGenWriteBarrier(&___sRenderType_0, value);
	}

	inline static int32_t get_offset_of_sOpaque_1() { return static_cast<int32_t>(offsetof(RendererCache_t1904069300_StaticFields, ___sOpaque_1)); }
	inline String_t* get_sOpaque_1() const { return ___sOpaque_1; }
	inline String_t** get_address_of_sOpaque_1() { return &___sOpaque_1; }
	inline void set_sOpaque_1(String_t* value)
	{
		___sOpaque_1 = value;
		Il2CppCodeGenWriteBarrier(&___sOpaque_1, value);
	}

	inline static int32_t get_offset_of_sTransparent_2() { return static_cast<int32_t>(offsetof(RendererCache_t1904069300_StaticFields, ___sTransparent_2)); }
	inline String_t* get_sTransparent_2() const { return ___sTransparent_2; }
	inline String_t** get_address_of_sTransparent_2() { return &___sTransparent_2; }
	inline void set_sTransparent_2(String_t* value)
	{
		___sTransparent_2 = value;
		Il2CppCodeGenWriteBarrier(&___sTransparent_2, value);
	}

	inline static int32_t get_offset_of_sTransparentCutout_3() { return static_cast<int32_t>(offsetof(RendererCache_t1904069300_StaticFields, ___sTransparentCutout_3)); }
	inline String_t* get_sTransparentCutout_3() const { return ___sTransparentCutout_3; }
	inline String_t** get_address_of_sTransparentCutout_3() { return &___sTransparentCutout_3; }
	inline void set_sTransparentCutout_3(String_t* value)
	{
		___sTransparentCutout_3 = value;
		Il2CppCodeGenWriteBarrier(&___sTransparentCutout_3, value);
	}

	inline static int32_t get_offset_of_sMainTex_4() { return static_cast<int32_t>(offsetof(RendererCache_t1904069300_StaticFields, ___sMainTex_4)); }
	inline String_t* get_sMainTex_4() const { return ___sMainTex_4; }
	inline String_t** get_address_of_sMainTex_4() { return &___sMainTex_4; }
	inline void set_sMainTex_4(String_t* value)
	{
		___sMainTex_4 = value;
		Il2CppCodeGenWriteBarrier(&___sMainTex_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
