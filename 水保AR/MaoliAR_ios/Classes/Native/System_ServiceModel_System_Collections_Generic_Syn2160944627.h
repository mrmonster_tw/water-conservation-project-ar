﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_Collections_Generic_Syn2124650601.h"

// System.Collections.Generic.Dictionary`2<System.String,System.ServiceModel.Dispatcher.DispatchOperation>
struct Dictionary_2_t1595562916;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.SynchronizedKeyedCollection`2<System.String,System.ServiceModel.Dispatcher.DispatchOperation>
struct  SynchronizedKeyedCollection_2_t2160944627  : public SynchronizedCollection_1_t2124650601
{
public:
	// System.Collections.Generic.Dictionary`2<K,T> System.Collections.Generic.SynchronizedKeyedCollection`2::dict
	Dictionary_2_t1595562916 * ___dict_2;

public:
	inline static int32_t get_offset_of_dict_2() { return static_cast<int32_t>(offsetof(SynchronizedKeyedCollection_2_t2160944627, ___dict_2)); }
	inline Dictionary_2_t1595562916 * get_dict_2() const { return ___dict_2; }
	inline Dictionary_2_t1595562916 ** get_address_of_dict_2() { return &___dict_2; }
	inline void set_dict_2(Dictionary_2_t1595562916 * value)
	{
		___dict_2 = value;
		Il2CppCodeGenWriteBarrier(&___dict_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
