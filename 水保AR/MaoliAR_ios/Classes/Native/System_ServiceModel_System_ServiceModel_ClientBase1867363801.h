﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.InstanceContext
struct InstanceContext_t3593205954;
// System.ServiceModel.ChannelFactory`1<System.ServiceModel.Description.IWSTrustSecurityTokenService>
struct ChannelFactory_1_t1890714201;
// System.ServiceModel.IClientChannel
struct IClientChannel_t714275133;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.ClientBase`1<System.ServiceModel.Description.IWSTrustSecurityTokenService>
struct  ClientBase_1_t1867363801  : public Il2CppObject
{
public:
	// System.ServiceModel.ChannelFactory`1<TChannel> System.ServiceModel.ClientBase`1::factory
	ChannelFactory_1_t1890714201 * ___factory_1;
	// System.ServiceModel.IClientChannel System.ServiceModel.ClientBase`1::inner_channel
	Il2CppObject * ___inner_channel_2;

public:
	inline static int32_t get_offset_of_factory_1() { return static_cast<int32_t>(offsetof(ClientBase_1_t1867363801, ___factory_1)); }
	inline ChannelFactory_1_t1890714201 * get_factory_1() const { return ___factory_1; }
	inline ChannelFactory_1_t1890714201 ** get_address_of_factory_1() { return &___factory_1; }
	inline void set_factory_1(ChannelFactory_1_t1890714201 * value)
	{
		___factory_1 = value;
		Il2CppCodeGenWriteBarrier(&___factory_1, value);
	}

	inline static int32_t get_offset_of_inner_channel_2() { return static_cast<int32_t>(offsetof(ClientBase_1_t1867363801, ___inner_channel_2)); }
	inline Il2CppObject * get_inner_channel_2() const { return ___inner_channel_2; }
	inline Il2CppObject ** get_address_of_inner_channel_2() { return &___inner_channel_2; }
	inline void set_inner_channel_2(Il2CppObject * value)
	{
		___inner_channel_2 = value;
		Il2CppCodeGenWriteBarrier(&___inner_channel_2, value);
	}
};

struct ClientBase_1_t1867363801_StaticFields
{
public:
	// System.ServiceModel.InstanceContext System.ServiceModel.ClientBase`1::initialContxt
	InstanceContext_t3593205954 * ___initialContxt_0;

public:
	inline static int32_t get_offset_of_initialContxt_0() { return static_cast<int32_t>(offsetof(ClientBase_1_t1867363801_StaticFields, ___initialContxt_0)); }
	inline InstanceContext_t3593205954 * get_initialContxt_0() const { return ___initialContxt_0; }
	inline InstanceContext_t3593205954 ** get_address_of_initialContxt_0() { return &___initialContxt_0; }
	inline void set_initialContxt_0(InstanceContext_t3593205954 * value)
	{
		___initialContxt_0 = value;
		Il2CppCodeGenWriteBarrier(&___initialContxt_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
