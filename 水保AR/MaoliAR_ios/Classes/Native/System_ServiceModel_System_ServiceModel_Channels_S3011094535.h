﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_System_ServiceModel_Security_S2144301020.h"

// System.IdentityModel.Tokens.SecurityToken
struct SecurityToken_t1271873540;
// System.Security.Cryptography.Xml.EncryptedData
struct EncryptedData_t3129875747;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.SupportingTokenInfo
struct  SupportingTokenInfo_t3011094535  : public Il2CppObject
{
public:
	// System.IdentityModel.Tokens.SecurityToken System.ServiceModel.Channels.SupportingTokenInfo::Token
	SecurityToken_t1271873540 * ___Token_0;
	// System.ServiceModel.Security.SecurityTokenAttachmentMode System.ServiceModel.Channels.SupportingTokenInfo::Mode
	int32_t ___Mode_1;
	// System.Boolean System.ServiceModel.Channels.SupportingTokenInfo::IsOptional
	bool ___IsOptional_2;
	// System.Security.Cryptography.Xml.EncryptedData System.ServiceModel.Channels.SupportingTokenInfo::Encrypted
	EncryptedData_t3129875747 * ___Encrypted_3;

public:
	inline static int32_t get_offset_of_Token_0() { return static_cast<int32_t>(offsetof(SupportingTokenInfo_t3011094535, ___Token_0)); }
	inline SecurityToken_t1271873540 * get_Token_0() const { return ___Token_0; }
	inline SecurityToken_t1271873540 ** get_address_of_Token_0() { return &___Token_0; }
	inline void set_Token_0(SecurityToken_t1271873540 * value)
	{
		___Token_0 = value;
		Il2CppCodeGenWriteBarrier(&___Token_0, value);
	}

	inline static int32_t get_offset_of_Mode_1() { return static_cast<int32_t>(offsetof(SupportingTokenInfo_t3011094535, ___Mode_1)); }
	inline int32_t get_Mode_1() const { return ___Mode_1; }
	inline int32_t* get_address_of_Mode_1() { return &___Mode_1; }
	inline void set_Mode_1(int32_t value)
	{
		___Mode_1 = value;
	}

	inline static int32_t get_offset_of_IsOptional_2() { return static_cast<int32_t>(offsetof(SupportingTokenInfo_t3011094535, ___IsOptional_2)); }
	inline bool get_IsOptional_2() const { return ___IsOptional_2; }
	inline bool* get_address_of_IsOptional_2() { return &___IsOptional_2; }
	inline void set_IsOptional_2(bool value)
	{
		___IsOptional_2 = value;
	}

	inline static int32_t get_offset_of_Encrypted_3() { return static_cast<int32_t>(offsetof(SupportingTokenInfo_t3011094535, ___Encrypted_3)); }
	inline EncryptedData_t3129875747 * get_Encrypted_3() const { return ___Encrypted_3; }
	inline EncryptedData_t3129875747 ** get_address_of_Encrypted_3() { return &___Encrypted_3; }
	inline void set_Encrypted_3(EncryptedData_t3129875747 * value)
	{
		___Encrypted_3 = value;
		Il2CppCodeGenWriteBarrier(&___Encrypted_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
