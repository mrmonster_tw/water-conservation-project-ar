﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Collections_ObjectModel_Collection_333665643.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.OperationDescriptionCollection
struct  OperationDescriptionCollection_t4198923421  : public Collection_1_t333665643
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
