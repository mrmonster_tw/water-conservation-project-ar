﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_System_ServiceModel_MsmqAuthen2816778259.h"
#include "System_ServiceModel_System_ServiceModel_MsmqEncrypt395927193.h"
#include "System_ServiceModel_System_ServiceModel_MsmqSecure3964215293.h"
#include "System_System_Net_Security_ProtectionLevel2559578148.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.MsmqTransportSecurity
struct  MsmqTransportSecurity_t2605141420  : public Il2CppObject
{
public:
	// System.ServiceModel.MsmqAuthenticationMode System.ServiceModel.MsmqTransportSecurity::auth
	int32_t ___auth_0;
	// System.ServiceModel.MsmqEncryptionAlgorithm System.ServiceModel.MsmqTransportSecurity::enc
	int32_t ___enc_1;
	// System.ServiceModel.MsmqSecureHashAlgorithm System.ServiceModel.MsmqTransportSecurity::hash
	int32_t ___hash_2;
	// System.Net.Security.ProtectionLevel System.ServiceModel.MsmqTransportSecurity::protection_level
	int32_t ___protection_level_3;

public:
	inline static int32_t get_offset_of_auth_0() { return static_cast<int32_t>(offsetof(MsmqTransportSecurity_t2605141420, ___auth_0)); }
	inline int32_t get_auth_0() const { return ___auth_0; }
	inline int32_t* get_address_of_auth_0() { return &___auth_0; }
	inline void set_auth_0(int32_t value)
	{
		___auth_0 = value;
	}

	inline static int32_t get_offset_of_enc_1() { return static_cast<int32_t>(offsetof(MsmqTransportSecurity_t2605141420, ___enc_1)); }
	inline int32_t get_enc_1() const { return ___enc_1; }
	inline int32_t* get_address_of_enc_1() { return &___enc_1; }
	inline void set_enc_1(int32_t value)
	{
		___enc_1 = value;
	}

	inline static int32_t get_offset_of_hash_2() { return static_cast<int32_t>(offsetof(MsmqTransportSecurity_t2605141420, ___hash_2)); }
	inline int32_t get_hash_2() const { return ___hash_2; }
	inline int32_t* get_address_of_hash_2() { return &___hash_2; }
	inline void set_hash_2(int32_t value)
	{
		___hash_2 = value;
	}

	inline static int32_t get_offset_of_protection_level_3() { return static_cast<int32_t>(offsetof(MsmqTransportSecurity_t2605141420, ___protection_level_3)); }
	inline int32_t get_protection_level_3() const { return ___protection_level_3; }
	inline int32_t* get_address_of_protection_level_3() { return &___protection_level_3; }
	inline void set_protection_level_3(int32_t value)
	{
		___protection_level_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
