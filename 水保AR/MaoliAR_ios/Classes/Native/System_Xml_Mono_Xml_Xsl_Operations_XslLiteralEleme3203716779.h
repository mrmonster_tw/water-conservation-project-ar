﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// Mono.Xml.Xsl.Operations.XslAvt
struct XslAvt_t1645109359;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslLiteralElement/XslLiteralAttribute
struct  XslLiteralAttribute_t3203716779  : public Il2CppObject
{
public:
	// System.String Mono.Xml.Xsl.Operations.XslLiteralElement/XslLiteralAttribute::localname
	String_t* ___localname_0;
	// System.String Mono.Xml.Xsl.Operations.XslLiteralElement/XslLiteralAttribute::prefix
	String_t* ___prefix_1;
	// System.String Mono.Xml.Xsl.Operations.XslLiteralElement/XslLiteralAttribute::nsUri
	String_t* ___nsUri_2;
	// Mono.Xml.Xsl.Operations.XslAvt Mono.Xml.Xsl.Operations.XslLiteralElement/XslLiteralAttribute::val
	XslAvt_t1645109359 * ___val_3;

public:
	inline static int32_t get_offset_of_localname_0() { return static_cast<int32_t>(offsetof(XslLiteralAttribute_t3203716779, ___localname_0)); }
	inline String_t* get_localname_0() const { return ___localname_0; }
	inline String_t** get_address_of_localname_0() { return &___localname_0; }
	inline void set_localname_0(String_t* value)
	{
		___localname_0 = value;
		Il2CppCodeGenWriteBarrier(&___localname_0, value);
	}

	inline static int32_t get_offset_of_prefix_1() { return static_cast<int32_t>(offsetof(XslLiteralAttribute_t3203716779, ___prefix_1)); }
	inline String_t* get_prefix_1() const { return ___prefix_1; }
	inline String_t** get_address_of_prefix_1() { return &___prefix_1; }
	inline void set_prefix_1(String_t* value)
	{
		___prefix_1 = value;
		Il2CppCodeGenWriteBarrier(&___prefix_1, value);
	}

	inline static int32_t get_offset_of_nsUri_2() { return static_cast<int32_t>(offsetof(XslLiteralAttribute_t3203716779, ___nsUri_2)); }
	inline String_t* get_nsUri_2() const { return ___nsUri_2; }
	inline String_t** get_address_of_nsUri_2() { return &___nsUri_2; }
	inline void set_nsUri_2(String_t* value)
	{
		___nsUri_2 = value;
		Il2CppCodeGenWriteBarrier(&___nsUri_2, value);
	}

	inline static int32_t get_offset_of_val_3() { return static_cast<int32_t>(offsetof(XslLiteralAttribute_t3203716779, ___val_3)); }
	inline XslAvt_t1645109359 * get_val_3() const { return ___val_3; }
	inline XslAvt_t1645109359 ** get_address_of_val_3() { return &___val_3; }
	inline void set_val_3(XslAvt_t1645109359 * value)
	{
		___val_3 = value;
		Il2CppCodeGenWriteBarrier(&___val_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
