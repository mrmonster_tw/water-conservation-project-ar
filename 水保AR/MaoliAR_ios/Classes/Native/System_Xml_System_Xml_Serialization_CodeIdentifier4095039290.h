﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Hashtable
struct Hashtable_t1853889766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.CodeIdentifiers
struct  CodeIdentifiers_t4095039290  : public Il2CppObject
{
public:
	// System.Boolean System.Xml.Serialization.CodeIdentifiers::useCamelCasing
	bool ___useCamelCasing_0;
	// System.Collections.Hashtable System.Xml.Serialization.CodeIdentifiers::table
	Hashtable_t1853889766 * ___table_1;
	// System.Collections.Hashtable System.Xml.Serialization.CodeIdentifiers::reserved
	Hashtable_t1853889766 * ___reserved_2;

public:
	inline static int32_t get_offset_of_useCamelCasing_0() { return static_cast<int32_t>(offsetof(CodeIdentifiers_t4095039290, ___useCamelCasing_0)); }
	inline bool get_useCamelCasing_0() const { return ___useCamelCasing_0; }
	inline bool* get_address_of_useCamelCasing_0() { return &___useCamelCasing_0; }
	inline void set_useCamelCasing_0(bool value)
	{
		___useCamelCasing_0 = value;
	}

	inline static int32_t get_offset_of_table_1() { return static_cast<int32_t>(offsetof(CodeIdentifiers_t4095039290, ___table_1)); }
	inline Hashtable_t1853889766 * get_table_1() const { return ___table_1; }
	inline Hashtable_t1853889766 ** get_address_of_table_1() { return &___table_1; }
	inline void set_table_1(Hashtable_t1853889766 * value)
	{
		___table_1 = value;
		Il2CppCodeGenWriteBarrier(&___table_1, value);
	}

	inline static int32_t get_offset_of_reserved_2() { return static_cast<int32_t>(offsetof(CodeIdentifiers_t4095039290, ___reserved_2)); }
	inline Hashtable_t1853889766 * get_reserved_2() const { return ___reserved_2; }
	inline Hashtable_t1853889766 ** get_address_of_reserved_2() { return &___reserved_2; }
	inline void set_reserved_2(Hashtable_t1853889766 * value)
	{
		___reserved_2 = value;
		Il2CppCodeGenWriteBarrier(&___reserved_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
