﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_System_Net_HttpStatusCode3035121829.h"

// System.Net.WebHeaderCollection
struct WebHeaderCollection_t1942268960;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.HttpResponseMessageProperty
struct  HttpResponseMessageProperty_t2019380601  : public Il2CppObject
{
public:
	// System.Net.WebHeaderCollection System.ServiceModel.Channels.HttpResponseMessageProperty::headers
	WebHeaderCollection_t1942268960 * ___headers_0;
	// System.String System.ServiceModel.Channels.HttpResponseMessageProperty::status_desc
	String_t* ___status_desc_1;
	// System.Net.HttpStatusCode System.ServiceModel.Channels.HttpResponseMessageProperty::status_code
	int32_t ___status_code_2;
	// System.Boolean System.ServiceModel.Channels.HttpResponseMessageProperty::suppress_entity
	bool ___suppress_entity_3;

public:
	inline static int32_t get_offset_of_headers_0() { return static_cast<int32_t>(offsetof(HttpResponseMessageProperty_t2019380601, ___headers_0)); }
	inline WebHeaderCollection_t1942268960 * get_headers_0() const { return ___headers_0; }
	inline WebHeaderCollection_t1942268960 ** get_address_of_headers_0() { return &___headers_0; }
	inline void set_headers_0(WebHeaderCollection_t1942268960 * value)
	{
		___headers_0 = value;
		Il2CppCodeGenWriteBarrier(&___headers_0, value);
	}

	inline static int32_t get_offset_of_status_desc_1() { return static_cast<int32_t>(offsetof(HttpResponseMessageProperty_t2019380601, ___status_desc_1)); }
	inline String_t* get_status_desc_1() const { return ___status_desc_1; }
	inline String_t** get_address_of_status_desc_1() { return &___status_desc_1; }
	inline void set_status_desc_1(String_t* value)
	{
		___status_desc_1 = value;
		Il2CppCodeGenWriteBarrier(&___status_desc_1, value);
	}

	inline static int32_t get_offset_of_status_code_2() { return static_cast<int32_t>(offsetof(HttpResponseMessageProperty_t2019380601, ___status_code_2)); }
	inline int32_t get_status_code_2() const { return ___status_code_2; }
	inline int32_t* get_address_of_status_code_2() { return &___status_code_2; }
	inline void set_status_code_2(int32_t value)
	{
		___status_code_2 = value;
	}

	inline static int32_t get_offset_of_suppress_entity_3() { return static_cast<int32_t>(offsetof(HttpResponseMessageProperty_t2019380601, ___suppress_entity_3)); }
	inline bool get_suppress_entity_3() const { return ___suppress_entity_3; }
	inline bool* get_address_of_suppress_entity_3() { return &___suppress_entity_3; }
	inline void set_suppress_entity_3(bool value)
	{
		___suppress_entity_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
