﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Net.Sockets.TcpClient
struct TcpClient_t822906377;
// System.ServiceModel.Channels.TcpChannelListener`1<System.Object>
struct TcpChannelListener_1_t2218842867;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.TcpChannelListener`1/<AcceptTcpClient>c__AnonStorey13<System.Object>
struct  U3CAcceptTcpClientU3Ec__AnonStorey13_t2602765713  : public Il2CppObject
{
public:
	// System.Net.Sockets.TcpClient System.ServiceModel.Channels.TcpChannelListener`1/<AcceptTcpClient>c__AnonStorey13::client
	TcpClient_t822906377 * ___client_0;
	// System.ServiceModel.Channels.TcpChannelListener`1<TChannel> System.ServiceModel.Channels.TcpChannelListener`1/<AcceptTcpClient>c__AnonStorey13::<>f__this
	TcpChannelListener_1_t2218842867 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_client_0() { return static_cast<int32_t>(offsetof(U3CAcceptTcpClientU3Ec__AnonStorey13_t2602765713, ___client_0)); }
	inline TcpClient_t822906377 * get_client_0() const { return ___client_0; }
	inline TcpClient_t822906377 ** get_address_of_client_0() { return &___client_0; }
	inline void set_client_0(TcpClient_t822906377 * value)
	{
		___client_0 = value;
		Il2CppCodeGenWriteBarrier(&___client_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CAcceptTcpClientU3Ec__AnonStorey13_t2602765713, ___U3CU3Ef__this_1)); }
	inline TcpChannelListener_1_t2218842867 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline TcpChannelListener_1_t2218842867 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(TcpChannelListener_1_t2218842867 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
