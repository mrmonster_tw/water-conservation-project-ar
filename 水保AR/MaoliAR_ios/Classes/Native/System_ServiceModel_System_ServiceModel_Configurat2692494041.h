﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Configuration.BehaviorExtensionElement
struct BehaviorExtensionElement_t4188976535;
// System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1<System.ServiceModel.Configuration.BehaviorExtensionElement>
struct ServiceModelExtensionCollectionElement_1_t3653747872;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1/<GetEnumerator>c__Iterator5<System.ServiceModel.Configuration.BehaviorExtensionElement>
struct  U3CGetEnumeratorU3Ec__Iterator5_t2692494041  : public Il2CppObject
{
public:
	// System.Int32 System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1/<GetEnumerator>c__Iterator5::<i>__0
	int32_t ___U3CiU3E__0_0;
	// System.Int32 System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1/<GetEnumerator>c__Iterator5::$PC
	int32_t ___U24PC_1;
	// TServiceModelExtensionElement System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1/<GetEnumerator>c__Iterator5::$current
	BehaviorExtensionElement_t4188976535 * ___U24current_2;
	// System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1<TServiceModelExtensionElement> System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1/<GetEnumerator>c__Iterator5::<>f__this
	ServiceModelExtensionCollectionElement_1_t3653747872 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_U3CiU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator5_t2692494041, ___U3CiU3E__0_0)); }
	inline int32_t get_U3CiU3E__0_0() const { return ___U3CiU3E__0_0; }
	inline int32_t* get_address_of_U3CiU3E__0_0() { return &___U3CiU3E__0_0; }
	inline void set_U3CiU3E__0_0(int32_t value)
	{
		___U3CiU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator5_t2692494041, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator5_t2692494041, ___U24current_2)); }
	inline BehaviorExtensionElement_t4188976535 * get_U24current_2() const { return ___U24current_2; }
	inline BehaviorExtensionElement_t4188976535 ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(BehaviorExtensionElement_t4188976535 * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator5_t2692494041, ___U3CU3Ef__this_3)); }
	inline ServiceModelExtensionCollectionElement_1_t3653747872 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline ServiceModelExtensionCollectionElement_1_t3653747872 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(ServiceModelExtensionCollectionElement_1_t3653747872 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
