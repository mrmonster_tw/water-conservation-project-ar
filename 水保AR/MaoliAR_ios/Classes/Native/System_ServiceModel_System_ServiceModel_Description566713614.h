﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Uri
struct Uri_t100236324;
// System.ServiceModel.Channels.Binding
struct Binding_t859993683;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.ServiceMetadataBehavior
struct  ServiceMetadataBehavior_t566713614  : public Il2CppObject
{
public:
	// System.Boolean System.ServiceModel.Description.ServiceMetadataBehavior::<HttpGetEnabled>k__BackingField
	bool ___U3CHttpGetEnabledU3Ek__BackingField_0;
	// System.Boolean System.ServiceModel.Description.ServiceMetadataBehavior::<HttpsGetEnabled>k__BackingField
	bool ___U3CHttpsGetEnabledU3Ek__BackingField_1;
	// System.Uri System.ServiceModel.Description.ServiceMetadataBehavior::<HttpGetUrl>k__BackingField
	Uri_t100236324 * ___U3CHttpGetUrlU3Ek__BackingField_2;
	// System.Uri System.ServiceModel.Description.ServiceMetadataBehavior::<HttpsGetUrl>k__BackingField
	Uri_t100236324 * ___U3CHttpsGetUrlU3Ek__BackingField_3;
	// System.ServiceModel.Channels.Binding System.ServiceModel.Description.ServiceMetadataBehavior::<HttpGetBinding>k__BackingField
	Binding_t859993683 * ___U3CHttpGetBindingU3Ek__BackingField_4;
	// System.ServiceModel.Channels.Binding System.ServiceModel.Description.ServiceMetadataBehavior::<HttpsGetBinding>k__BackingField
	Binding_t859993683 * ___U3CHttpsGetBindingU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CHttpGetEnabledU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ServiceMetadataBehavior_t566713614, ___U3CHttpGetEnabledU3Ek__BackingField_0)); }
	inline bool get_U3CHttpGetEnabledU3Ek__BackingField_0() const { return ___U3CHttpGetEnabledU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CHttpGetEnabledU3Ek__BackingField_0() { return &___U3CHttpGetEnabledU3Ek__BackingField_0; }
	inline void set_U3CHttpGetEnabledU3Ek__BackingField_0(bool value)
	{
		___U3CHttpGetEnabledU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CHttpsGetEnabledU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ServiceMetadataBehavior_t566713614, ___U3CHttpsGetEnabledU3Ek__BackingField_1)); }
	inline bool get_U3CHttpsGetEnabledU3Ek__BackingField_1() const { return ___U3CHttpsGetEnabledU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CHttpsGetEnabledU3Ek__BackingField_1() { return &___U3CHttpsGetEnabledU3Ek__BackingField_1; }
	inline void set_U3CHttpsGetEnabledU3Ek__BackingField_1(bool value)
	{
		___U3CHttpsGetEnabledU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CHttpGetUrlU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ServiceMetadataBehavior_t566713614, ___U3CHttpGetUrlU3Ek__BackingField_2)); }
	inline Uri_t100236324 * get_U3CHttpGetUrlU3Ek__BackingField_2() const { return ___U3CHttpGetUrlU3Ek__BackingField_2; }
	inline Uri_t100236324 ** get_address_of_U3CHttpGetUrlU3Ek__BackingField_2() { return &___U3CHttpGetUrlU3Ek__BackingField_2; }
	inline void set_U3CHttpGetUrlU3Ek__BackingField_2(Uri_t100236324 * value)
	{
		___U3CHttpGetUrlU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHttpGetUrlU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CHttpsGetUrlU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ServiceMetadataBehavior_t566713614, ___U3CHttpsGetUrlU3Ek__BackingField_3)); }
	inline Uri_t100236324 * get_U3CHttpsGetUrlU3Ek__BackingField_3() const { return ___U3CHttpsGetUrlU3Ek__BackingField_3; }
	inline Uri_t100236324 ** get_address_of_U3CHttpsGetUrlU3Ek__BackingField_3() { return &___U3CHttpsGetUrlU3Ek__BackingField_3; }
	inline void set_U3CHttpsGetUrlU3Ek__BackingField_3(Uri_t100236324 * value)
	{
		___U3CHttpsGetUrlU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHttpsGetUrlU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CHttpGetBindingU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ServiceMetadataBehavior_t566713614, ___U3CHttpGetBindingU3Ek__BackingField_4)); }
	inline Binding_t859993683 * get_U3CHttpGetBindingU3Ek__BackingField_4() const { return ___U3CHttpGetBindingU3Ek__BackingField_4; }
	inline Binding_t859993683 ** get_address_of_U3CHttpGetBindingU3Ek__BackingField_4() { return &___U3CHttpGetBindingU3Ek__BackingField_4; }
	inline void set_U3CHttpGetBindingU3Ek__BackingField_4(Binding_t859993683 * value)
	{
		___U3CHttpGetBindingU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHttpGetBindingU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CHttpsGetBindingU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ServiceMetadataBehavior_t566713614, ___U3CHttpsGetBindingU3Ek__BackingField_5)); }
	inline Binding_t859993683 * get_U3CHttpsGetBindingU3Ek__BackingField_5() const { return ___U3CHttpsGetBindingU3Ek__BackingField_5; }
	inline Binding_t859993683 ** get_address_of_U3CHttpsGetBindingU3Ek__BackingField_5() { return &___U3CHttpsGetBindingU3Ek__BackingField_5; }
	inline void set_U3CHttpsGetBindingU3Ek__BackingField_5(Binding_t859993683 * value)
	{
		___U3CHttpsGetBindingU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHttpsGetBindingU3Ek__BackingField_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
