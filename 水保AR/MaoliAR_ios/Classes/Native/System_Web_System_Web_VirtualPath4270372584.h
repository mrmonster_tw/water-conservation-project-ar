﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.VirtualPath
struct  VirtualPath_t4270372584  : public Il2CppObject
{
public:
	// System.String System.Web.VirtualPath::_absolute
	String_t* ____absolute_0;
	// System.String System.Web.VirtualPath::_appRelative
	String_t* ____appRelative_1;
	// System.String System.Web.VirtualPath::_appRelativeNotRooted
	String_t* ____appRelativeNotRooted_2;
	// System.String System.Web.VirtualPath::_extension
	String_t* ____extension_3;
	// System.String System.Web.VirtualPath::_directory
	String_t* ____directory_4;
	// System.String System.Web.VirtualPath::_directoryNoNormalize
	String_t* ____directoryNoNormalize_5;
	// System.String System.Web.VirtualPath::_currentRequestDirectory
	String_t* ____currentRequestDirectory_6;
	// System.String System.Web.VirtualPath::_physicalPath
	String_t* ____physicalPath_7;
	// System.Boolean System.Web.VirtualPath::<IsAbsolute>k__BackingField
	bool ___U3CIsAbsoluteU3Ek__BackingField_8;
	// System.Boolean System.Web.VirtualPath::<IsFake>k__BackingField
	bool ___U3CIsFakeU3Ek__BackingField_9;
	// System.Boolean System.Web.VirtualPath::<IsRooted>k__BackingField
	bool ___U3CIsRootedU3Ek__BackingField_10;
	// System.Boolean System.Web.VirtualPath::<IsAppRelative>k__BackingField
	bool ___U3CIsAppRelativeU3Ek__BackingField_11;
	// System.String System.Web.VirtualPath::<Original>k__BackingField
	String_t* ___U3COriginalU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of__absolute_0() { return static_cast<int32_t>(offsetof(VirtualPath_t4270372584, ____absolute_0)); }
	inline String_t* get__absolute_0() const { return ____absolute_0; }
	inline String_t** get_address_of__absolute_0() { return &____absolute_0; }
	inline void set__absolute_0(String_t* value)
	{
		____absolute_0 = value;
		Il2CppCodeGenWriteBarrier(&____absolute_0, value);
	}

	inline static int32_t get_offset_of__appRelative_1() { return static_cast<int32_t>(offsetof(VirtualPath_t4270372584, ____appRelative_1)); }
	inline String_t* get__appRelative_1() const { return ____appRelative_1; }
	inline String_t** get_address_of__appRelative_1() { return &____appRelative_1; }
	inline void set__appRelative_1(String_t* value)
	{
		____appRelative_1 = value;
		Il2CppCodeGenWriteBarrier(&____appRelative_1, value);
	}

	inline static int32_t get_offset_of__appRelativeNotRooted_2() { return static_cast<int32_t>(offsetof(VirtualPath_t4270372584, ____appRelativeNotRooted_2)); }
	inline String_t* get__appRelativeNotRooted_2() const { return ____appRelativeNotRooted_2; }
	inline String_t** get_address_of__appRelativeNotRooted_2() { return &____appRelativeNotRooted_2; }
	inline void set__appRelativeNotRooted_2(String_t* value)
	{
		____appRelativeNotRooted_2 = value;
		Il2CppCodeGenWriteBarrier(&____appRelativeNotRooted_2, value);
	}

	inline static int32_t get_offset_of__extension_3() { return static_cast<int32_t>(offsetof(VirtualPath_t4270372584, ____extension_3)); }
	inline String_t* get__extension_3() const { return ____extension_3; }
	inline String_t** get_address_of__extension_3() { return &____extension_3; }
	inline void set__extension_3(String_t* value)
	{
		____extension_3 = value;
		Il2CppCodeGenWriteBarrier(&____extension_3, value);
	}

	inline static int32_t get_offset_of__directory_4() { return static_cast<int32_t>(offsetof(VirtualPath_t4270372584, ____directory_4)); }
	inline String_t* get__directory_4() const { return ____directory_4; }
	inline String_t** get_address_of__directory_4() { return &____directory_4; }
	inline void set__directory_4(String_t* value)
	{
		____directory_4 = value;
		Il2CppCodeGenWriteBarrier(&____directory_4, value);
	}

	inline static int32_t get_offset_of__directoryNoNormalize_5() { return static_cast<int32_t>(offsetof(VirtualPath_t4270372584, ____directoryNoNormalize_5)); }
	inline String_t* get__directoryNoNormalize_5() const { return ____directoryNoNormalize_5; }
	inline String_t** get_address_of__directoryNoNormalize_5() { return &____directoryNoNormalize_5; }
	inline void set__directoryNoNormalize_5(String_t* value)
	{
		____directoryNoNormalize_5 = value;
		Il2CppCodeGenWriteBarrier(&____directoryNoNormalize_5, value);
	}

	inline static int32_t get_offset_of__currentRequestDirectory_6() { return static_cast<int32_t>(offsetof(VirtualPath_t4270372584, ____currentRequestDirectory_6)); }
	inline String_t* get__currentRequestDirectory_6() const { return ____currentRequestDirectory_6; }
	inline String_t** get_address_of__currentRequestDirectory_6() { return &____currentRequestDirectory_6; }
	inline void set__currentRequestDirectory_6(String_t* value)
	{
		____currentRequestDirectory_6 = value;
		Il2CppCodeGenWriteBarrier(&____currentRequestDirectory_6, value);
	}

	inline static int32_t get_offset_of__physicalPath_7() { return static_cast<int32_t>(offsetof(VirtualPath_t4270372584, ____physicalPath_7)); }
	inline String_t* get__physicalPath_7() const { return ____physicalPath_7; }
	inline String_t** get_address_of__physicalPath_7() { return &____physicalPath_7; }
	inline void set__physicalPath_7(String_t* value)
	{
		____physicalPath_7 = value;
		Il2CppCodeGenWriteBarrier(&____physicalPath_7, value);
	}

	inline static int32_t get_offset_of_U3CIsAbsoluteU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(VirtualPath_t4270372584, ___U3CIsAbsoluteU3Ek__BackingField_8)); }
	inline bool get_U3CIsAbsoluteU3Ek__BackingField_8() const { return ___U3CIsAbsoluteU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIsAbsoluteU3Ek__BackingField_8() { return &___U3CIsAbsoluteU3Ek__BackingField_8; }
	inline void set_U3CIsAbsoluteU3Ek__BackingField_8(bool value)
	{
		___U3CIsAbsoluteU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CIsFakeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(VirtualPath_t4270372584, ___U3CIsFakeU3Ek__BackingField_9)); }
	inline bool get_U3CIsFakeU3Ek__BackingField_9() const { return ___U3CIsFakeU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CIsFakeU3Ek__BackingField_9() { return &___U3CIsFakeU3Ek__BackingField_9; }
	inline void set_U3CIsFakeU3Ek__BackingField_9(bool value)
	{
		___U3CIsFakeU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CIsRootedU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(VirtualPath_t4270372584, ___U3CIsRootedU3Ek__BackingField_10)); }
	inline bool get_U3CIsRootedU3Ek__BackingField_10() const { return ___U3CIsRootedU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CIsRootedU3Ek__BackingField_10() { return &___U3CIsRootedU3Ek__BackingField_10; }
	inline void set_U3CIsRootedU3Ek__BackingField_10(bool value)
	{
		___U3CIsRootedU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CIsAppRelativeU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(VirtualPath_t4270372584, ___U3CIsAppRelativeU3Ek__BackingField_11)); }
	inline bool get_U3CIsAppRelativeU3Ek__BackingField_11() const { return ___U3CIsAppRelativeU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CIsAppRelativeU3Ek__BackingField_11() { return &___U3CIsAppRelativeU3Ek__BackingField_11; }
	inline void set_U3CIsAppRelativeU3Ek__BackingField_11(bool value)
	{
		___U3CIsAppRelativeU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3COriginalU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(VirtualPath_t4270372584, ___U3COriginalU3Ek__BackingField_12)); }
	inline String_t* get_U3COriginalU3Ek__BackingField_12() const { return ___U3COriginalU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3COriginalU3Ek__BackingField_12() { return &___U3COriginalU3Ek__BackingField_12; }
	inline void set_U3COriginalU3Ek__BackingField_12(String_t* value)
	{
		___U3COriginalU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3COriginalU3Ek__BackingField_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
