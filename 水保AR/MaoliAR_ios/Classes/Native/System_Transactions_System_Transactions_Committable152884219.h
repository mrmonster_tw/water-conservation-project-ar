﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Transactions_System_Transactions_Transactio3472000926.h"
#include "System_Transactions_System_Transactions_Transactio2865697824.h"

// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t767004451;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Transactions.CommittableTransaction
struct  CommittableTransaction_t152884219  : public Transaction_t3472000926
{
public:
	// System.Transactions.TransactionOptions System.Transactions.CommittableTransaction::options
	TransactionOptions_t2865697824  ___options_11;
	// System.Object System.Transactions.CommittableTransaction::user_defined_state
	Il2CppObject * ___user_defined_state_12;
	// System.IAsyncResult System.Transactions.CommittableTransaction::asyncResult
	Il2CppObject * ___asyncResult_13;

public:
	inline static int32_t get_offset_of_options_11() { return static_cast<int32_t>(offsetof(CommittableTransaction_t152884219, ___options_11)); }
	inline TransactionOptions_t2865697824  get_options_11() const { return ___options_11; }
	inline TransactionOptions_t2865697824 * get_address_of_options_11() { return &___options_11; }
	inline void set_options_11(TransactionOptions_t2865697824  value)
	{
		___options_11 = value;
	}

	inline static int32_t get_offset_of_user_defined_state_12() { return static_cast<int32_t>(offsetof(CommittableTransaction_t152884219, ___user_defined_state_12)); }
	inline Il2CppObject * get_user_defined_state_12() const { return ___user_defined_state_12; }
	inline Il2CppObject ** get_address_of_user_defined_state_12() { return &___user_defined_state_12; }
	inline void set_user_defined_state_12(Il2CppObject * value)
	{
		___user_defined_state_12 = value;
		Il2CppCodeGenWriteBarrier(&___user_defined_state_12, value);
	}

	inline static int32_t get_offset_of_asyncResult_13() { return static_cast<int32_t>(offsetof(CommittableTransaction_t152884219, ___asyncResult_13)); }
	inline Il2CppObject * get_asyncResult_13() const { return ___asyncResult_13; }
	inline Il2CppObject ** get_address_of_asyncResult_13() { return &___asyncResult_13; }
	inline void set_asyncResult_13(Il2CppObject * value)
	{
		___asyncResult_13 = value;
		Il2CppCodeGenWriteBarrier(&___asyncResult_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
