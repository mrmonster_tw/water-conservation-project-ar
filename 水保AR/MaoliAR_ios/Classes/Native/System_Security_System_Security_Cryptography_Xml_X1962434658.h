﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Security_System_Security_Cryptography_Xml_T1105379765.h"

// System.Xml.XmlNodeList
struct XmlNodeList_t2551693786;
// System.Xml.XmlDocument
struct XmlDocument_t2837193595;
// System.Xml.Xsl.XsltContext
struct XsltContext_t2039362735;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.Xml.XmlDsigXPathTransform
struct  XmlDsigXPathTransform_t1962434658  : public Transform_t1105379765
{
public:
	// System.Xml.XmlNodeList System.Security.Cryptography.Xml.XmlDsigXPathTransform::xpath
	XmlNodeList_t2551693786 * ___xpath_3;
	// System.Xml.XmlDocument System.Security.Cryptography.Xml.XmlDsigXPathTransform::doc
	XmlDocument_t2837193595 * ___doc_4;
	// System.Xml.Xsl.XsltContext System.Security.Cryptography.Xml.XmlDsigXPathTransform::ctx
	XsltContext_t2039362735 * ___ctx_5;

public:
	inline static int32_t get_offset_of_xpath_3() { return static_cast<int32_t>(offsetof(XmlDsigXPathTransform_t1962434658, ___xpath_3)); }
	inline XmlNodeList_t2551693786 * get_xpath_3() const { return ___xpath_3; }
	inline XmlNodeList_t2551693786 ** get_address_of_xpath_3() { return &___xpath_3; }
	inline void set_xpath_3(XmlNodeList_t2551693786 * value)
	{
		___xpath_3 = value;
		Il2CppCodeGenWriteBarrier(&___xpath_3, value);
	}

	inline static int32_t get_offset_of_doc_4() { return static_cast<int32_t>(offsetof(XmlDsigXPathTransform_t1962434658, ___doc_4)); }
	inline XmlDocument_t2837193595 * get_doc_4() const { return ___doc_4; }
	inline XmlDocument_t2837193595 ** get_address_of_doc_4() { return &___doc_4; }
	inline void set_doc_4(XmlDocument_t2837193595 * value)
	{
		___doc_4 = value;
		Il2CppCodeGenWriteBarrier(&___doc_4, value);
	}

	inline static int32_t get_offset_of_ctx_5() { return static_cast<int32_t>(offsetof(XmlDsigXPathTransform_t1962434658, ___ctx_5)); }
	inline XsltContext_t2039362735 * get_ctx_5() const { return ___ctx_5; }
	inline XsltContext_t2039362735 ** get_address_of_ctx_5() { return &___ctx_5; }
	inline void set_ctx_5(XsltContext_t2039362735 * value)
	{
		___ctx_5 = value;
		Il2CppCodeGenWriteBarrier(&___ctx_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
