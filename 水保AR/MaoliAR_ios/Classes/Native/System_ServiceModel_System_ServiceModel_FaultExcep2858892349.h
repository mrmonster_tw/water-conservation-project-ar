﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Communicati147767313.h"

// System.ServiceModel.Channels.MessageFault
struct MessageFault_t929740066;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.FaultException
struct  FaultException_t2858892349  : public CommunicationException_t147767313
{
public:
	// System.ServiceModel.Channels.MessageFault System.ServiceModel.FaultException::fault
	MessageFault_t929740066 * ___fault_11;
	// System.String System.ServiceModel.FaultException::action
	String_t* ___action_12;

public:
	inline static int32_t get_offset_of_fault_11() { return static_cast<int32_t>(offsetof(FaultException_t2858892349, ___fault_11)); }
	inline MessageFault_t929740066 * get_fault_11() const { return ___fault_11; }
	inline MessageFault_t929740066 ** get_address_of_fault_11() { return &___fault_11; }
	inline void set_fault_11(MessageFault_t929740066 * value)
	{
		___fault_11 = value;
		Il2CppCodeGenWriteBarrier(&___fault_11, value);
	}

	inline static int32_t get_offset_of_action_12() { return static_cast<int32_t>(offsetof(FaultException_t2858892349, ___action_12)); }
	inline String_t* get_action_12() const { return ___action_12; }
	inline String_t** get_address_of_action_12() { return &___action_12; }
	inline void set_action_12(String_t* value)
	{
		___action_12 = value;
		Il2CppCodeGenWriteBarrier(&___action_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
