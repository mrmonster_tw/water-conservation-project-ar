﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_System_Net_HttpConnection_InputState4051538957.h"
#include "System_System_Net_HttpConnection_LineState768597569.h"

// System.Net.Sockets.Socket
struct Socket_t1119025450;
// System.IO.Stream
struct Stream_t1273022909;
// System.Net.EndPointListener
struct EndPointListener_t2984434924;
// System.IO.MemoryStream
struct MemoryStream_t94973147;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Net.HttpListenerContext
struct HttpListenerContext_t424880822;
// System.Text.StringBuilder
struct StringBuilder_t1712802186;
// System.Net.ListenerPrefix
struct ListenerPrefix_t3570496559;
// System.Net.RequestStream
struct RequestStream_t762880582;
// System.Net.ResponseStream
struct ResponseStream_t3810703494;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t932037087;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpConnection
struct  HttpConnection_t269576101  : public Il2CppObject
{
public:
	// System.Net.Sockets.Socket System.Net.HttpConnection::sock
	Socket_t1119025450 * ___sock_0;
	// System.IO.Stream System.Net.HttpConnection::stream
	Stream_t1273022909 * ___stream_1;
	// System.Net.EndPointListener System.Net.HttpConnection::epl
	EndPointListener_t2984434924 * ___epl_2;
	// System.IO.MemoryStream System.Net.HttpConnection::ms
	MemoryStream_t94973147 * ___ms_3;
	// System.Byte[] System.Net.HttpConnection::buffer
	ByteU5BU5D_t4116647657* ___buffer_4;
	// System.Net.HttpListenerContext System.Net.HttpConnection::context
	HttpListenerContext_t424880822 * ___context_5;
	// System.Text.StringBuilder System.Net.HttpConnection::current_line
	StringBuilder_t1712802186 * ___current_line_6;
	// System.Net.ListenerPrefix System.Net.HttpConnection::prefix
	ListenerPrefix_t3570496559 * ___prefix_7;
	// System.Net.RequestStream System.Net.HttpConnection::i_stream
	RequestStream_t762880582 * ___i_stream_8;
	// System.Net.ResponseStream System.Net.HttpConnection::o_stream
	ResponseStream_t3810703494 * ___o_stream_9;
	// System.Boolean System.Net.HttpConnection::chunked
	bool ___chunked_10;
	// System.Int32 System.Net.HttpConnection::chunked_uses
	int32_t ___chunked_uses_11;
	// System.Boolean System.Net.HttpConnection::context_bound
	bool ___context_bound_12;
	// System.Boolean System.Net.HttpConnection::secure
	bool ___secure_13;
	// System.Security.Cryptography.AsymmetricAlgorithm System.Net.HttpConnection::key
	AsymmetricAlgorithm_t932037087 * ___key_14;
	// System.Net.HttpConnection/InputState System.Net.HttpConnection::input_state
	int32_t ___input_state_15;
	// System.Net.HttpConnection/LineState System.Net.HttpConnection::line_state
	int32_t ___line_state_16;
	// System.Int32 System.Net.HttpConnection::position
	int32_t ___position_17;

public:
	inline static int32_t get_offset_of_sock_0() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___sock_0)); }
	inline Socket_t1119025450 * get_sock_0() const { return ___sock_0; }
	inline Socket_t1119025450 ** get_address_of_sock_0() { return &___sock_0; }
	inline void set_sock_0(Socket_t1119025450 * value)
	{
		___sock_0 = value;
		Il2CppCodeGenWriteBarrier(&___sock_0, value);
	}

	inline static int32_t get_offset_of_stream_1() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___stream_1)); }
	inline Stream_t1273022909 * get_stream_1() const { return ___stream_1; }
	inline Stream_t1273022909 ** get_address_of_stream_1() { return &___stream_1; }
	inline void set_stream_1(Stream_t1273022909 * value)
	{
		___stream_1 = value;
		Il2CppCodeGenWriteBarrier(&___stream_1, value);
	}

	inline static int32_t get_offset_of_epl_2() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___epl_2)); }
	inline EndPointListener_t2984434924 * get_epl_2() const { return ___epl_2; }
	inline EndPointListener_t2984434924 ** get_address_of_epl_2() { return &___epl_2; }
	inline void set_epl_2(EndPointListener_t2984434924 * value)
	{
		___epl_2 = value;
		Il2CppCodeGenWriteBarrier(&___epl_2, value);
	}

	inline static int32_t get_offset_of_ms_3() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___ms_3)); }
	inline MemoryStream_t94973147 * get_ms_3() const { return ___ms_3; }
	inline MemoryStream_t94973147 ** get_address_of_ms_3() { return &___ms_3; }
	inline void set_ms_3(MemoryStream_t94973147 * value)
	{
		___ms_3 = value;
		Il2CppCodeGenWriteBarrier(&___ms_3, value);
	}

	inline static int32_t get_offset_of_buffer_4() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___buffer_4)); }
	inline ByteU5BU5D_t4116647657* get_buffer_4() const { return ___buffer_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_buffer_4() { return &___buffer_4; }
	inline void set_buffer_4(ByteU5BU5D_t4116647657* value)
	{
		___buffer_4 = value;
		Il2CppCodeGenWriteBarrier(&___buffer_4, value);
	}

	inline static int32_t get_offset_of_context_5() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___context_5)); }
	inline HttpListenerContext_t424880822 * get_context_5() const { return ___context_5; }
	inline HttpListenerContext_t424880822 ** get_address_of_context_5() { return &___context_5; }
	inline void set_context_5(HttpListenerContext_t424880822 * value)
	{
		___context_5 = value;
		Il2CppCodeGenWriteBarrier(&___context_5, value);
	}

	inline static int32_t get_offset_of_current_line_6() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___current_line_6)); }
	inline StringBuilder_t1712802186 * get_current_line_6() const { return ___current_line_6; }
	inline StringBuilder_t1712802186 ** get_address_of_current_line_6() { return &___current_line_6; }
	inline void set_current_line_6(StringBuilder_t1712802186 * value)
	{
		___current_line_6 = value;
		Il2CppCodeGenWriteBarrier(&___current_line_6, value);
	}

	inline static int32_t get_offset_of_prefix_7() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___prefix_7)); }
	inline ListenerPrefix_t3570496559 * get_prefix_7() const { return ___prefix_7; }
	inline ListenerPrefix_t3570496559 ** get_address_of_prefix_7() { return &___prefix_7; }
	inline void set_prefix_7(ListenerPrefix_t3570496559 * value)
	{
		___prefix_7 = value;
		Il2CppCodeGenWriteBarrier(&___prefix_7, value);
	}

	inline static int32_t get_offset_of_i_stream_8() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___i_stream_8)); }
	inline RequestStream_t762880582 * get_i_stream_8() const { return ___i_stream_8; }
	inline RequestStream_t762880582 ** get_address_of_i_stream_8() { return &___i_stream_8; }
	inline void set_i_stream_8(RequestStream_t762880582 * value)
	{
		___i_stream_8 = value;
		Il2CppCodeGenWriteBarrier(&___i_stream_8, value);
	}

	inline static int32_t get_offset_of_o_stream_9() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___o_stream_9)); }
	inline ResponseStream_t3810703494 * get_o_stream_9() const { return ___o_stream_9; }
	inline ResponseStream_t3810703494 ** get_address_of_o_stream_9() { return &___o_stream_9; }
	inline void set_o_stream_9(ResponseStream_t3810703494 * value)
	{
		___o_stream_9 = value;
		Il2CppCodeGenWriteBarrier(&___o_stream_9, value);
	}

	inline static int32_t get_offset_of_chunked_10() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___chunked_10)); }
	inline bool get_chunked_10() const { return ___chunked_10; }
	inline bool* get_address_of_chunked_10() { return &___chunked_10; }
	inline void set_chunked_10(bool value)
	{
		___chunked_10 = value;
	}

	inline static int32_t get_offset_of_chunked_uses_11() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___chunked_uses_11)); }
	inline int32_t get_chunked_uses_11() const { return ___chunked_uses_11; }
	inline int32_t* get_address_of_chunked_uses_11() { return &___chunked_uses_11; }
	inline void set_chunked_uses_11(int32_t value)
	{
		___chunked_uses_11 = value;
	}

	inline static int32_t get_offset_of_context_bound_12() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___context_bound_12)); }
	inline bool get_context_bound_12() const { return ___context_bound_12; }
	inline bool* get_address_of_context_bound_12() { return &___context_bound_12; }
	inline void set_context_bound_12(bool value)
	{
		___context_bound_12 = value;
	}

	inline static int32_t get_offset_of_secure_13() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___secure_13)); }
	inline bool get_secure_13() const { return ___secure_13; }
	inline bool* get_address_of_secure_13() { return &___secure_13; }
	inline void set_secure_13(bool value)
	{
		___secure_13 = value;
	}

	inline static int32_t get_offset_of_key_14() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___key_14)); }
	inline AsymmetricAlgorithm_t932037087 * get_key_14() const { return ___key_14; }
	inline AsymmetricAlgorithm_t932037087 ** get_address_of_key_14() { return &___key_14; }
	inline void set_key_14(AsymmetricAlgorithm_t932037087 * value)
	{
		___key_14 = value;
		Il2CppCodeGenWriteBarrier(&___key_14, value);
	}

	inline static int32_t get_offset_of_input_state_15() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___input_state_15)); }
	inline int32_t get_input_state_15() const { return ___input_state_15; }
	inline int32_t* get_address_of_input_state_15() { return &___input_state_15; }
	inline void set_input_state_15(int32_t value)
	{
		___input_state_15 = value;
	}

	inline static int32_t get_offset_of_line_state_16() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___line_state_16)); }
	inline int32_t get_line_state_16() const { return ___line_state_16; }
	inline int32_t* get_address_of_line_state_16() { return &___line_state_16; }
	inline void set_line_state_16(int32_t value)
	{
		___line_state_16 = value;
	}

	inline static int32_t get_offset_of_position_17() { return static_cast<int32_t>(offsetof(HttpConnection_t269576101, ___position_17)); }
	inline int32_t get_position_17() const { return ___position_17; }
	inline int32_t* get_address_of_position_17() { return &___position_17; }
	inline void set_position_17(int32_t value)
	{
		___position_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
