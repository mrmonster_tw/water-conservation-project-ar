﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.ObjectModel.Collection`1<System.ServiceModel.Security.Tokens.SecurityTokenParameters>
struct Collection_1_t1813314702;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.SupportingTokenParameters
struct  SupportingTokenParameters_t907883796  : public Il2CppObject
{
public:
	// System.Collections.ObjectModel.Collection`1<System.ServiceModel.Security.Tokens.SecurityTokenParameters> System.ServiceModel.Security.Tokens.SupportingTokenParameters::endorsing
	Collection_1_t1813314702 * ___endorsing_0;
	// System.Collections.ObjectModel.Collection`1<System.ServiceModel.Security.Tokens.SecurityTokenParameters> System.ServiceModel.Security.Tokens.SupportingTokenParameters::signed
	Collection_1_t1813314702 * ___signed_1;
	// System.Collections.ObjectModel.Collection`1<System.ServiceModel.Security.Tokens.SecurityTokenParameters> System.ServiceModel.Security.Tokens.SupportingTokenParameters::signed_encrypted
	Collection_1_t1813314702 * ___signed_encrypted_2;
	// System.Collections.ObjectModel.Collection`1<System.ServiceModel.Security.Tokens.SecurityTokenParameters> System.ServiceModel.Security.Tokens.SupportingTokenParameters::signed_endorsing
	Collection_1_t1813314702 * ___signed_endorsing_3;

public:
	inline static int32_t get_offset_of_endorsing_0() { return static_cast<int32_t>(offsetof(SupportingTokenParameters_t907883796, ___endorsing_0)); }
	inline Collection_1_t1813314702 * get_endorsing_0() const { return ___endorsing_0; }
	inline Collection_1_t1813314702 ** get_address_of_endorsing_0() { return &___endorsing_0; }
	inline void set_endorsing_0(Collection_1_t1813314702 * value)
	{
		___endorsing_0 = value;
		Il2CppCodeGenWriteBarrier(&___endorsing_0, value);
	}

	inline static int32_t get_offset_of_signed_1() { return static_cast<int32_t>(offsetof(SupportingTokenParameters_t907883796, ___signed_1)); }
	inline Collection_1_t1813314702 * get_signed_1() const { return ___signed_1; }
	inline Collection_1_t1813314702 ** get_address_of_signed_1() { return &___signed_1; }
	inline void set_signed_1(Collection_1_t1813314702 * value)
	{
		___signed_1 = value;
		Il2CppCodeGenWriteBarrier(&___signed_1, value);
	}

	inline static int32_t get_offset_of_signed_encrypted_2() { return static_cast<int32_t>(offsetof(SupportingTokenParameters_t907883796, ___signed_encrypted_2)); }
	inline Collection_1_t1813314702 * get_signed_encrypted_2() const { return ___signed_encrypted_2; }
	inline Collection_1_t1813314702 ** get_address_of_signed_encrypted_2() { return &___signed_encrypted_2; }
	inline void set_signed_encrypted_2(Collection_1_t1813314702 * value)
	{
		___signed_encrypted_2 = value;
		Il2CppCodeGenWriteBarrier(&___signed_encrypted_2, value);
	}

	inline static int32_t get_offset_of_signed_endorsing_3() { return static_cast<int32_t>(offsetof(SupportingTokenParameters_t907883796, ___signed_endorsing_3)); }
	inline Collection_1_t1813314702 * get_signed_endorsing_3() const { return ___signed_endorsing_3; }
	inline Collection_1_t1813314702 ** get_address_of_signed_endorsing_3() { return &___signed_endorsing_3; }
	inline void set_signed_endorsing_3(Collection_1_t1813314702 * value)
	{
		___signed_endorsing_3 = value;
		Il2CppCodeGenWriteBarrier(&___signed_endorsing_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
