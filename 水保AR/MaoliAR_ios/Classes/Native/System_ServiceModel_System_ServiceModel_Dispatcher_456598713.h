﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Dispatcher.HandlersChain
struct HandlersChain_t2728349147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.BaseRequestProcessor
struct  BaseRequestProcessor_t456598713  : public Il2CppObject
{
public:
	// System.ServiceModel.Dispatcher.HandlersChain System.ServiceModel.Dispatcher.BaseRequestProcessor::initialize_handlers_chain
	HandlersChain_t2728349147 * ___initialize_handlers_chain_0;
	// System.ServiceModel.Dispatcher.HandlersChain System.ServiceModel.Dispatcher.BaseRequestProcessor::process_handlers_chain
	HandlersChain_t2728349147 * ___process_handlers_chain_1;
	// System.ServiceModel.Dispatcher.HandlersChain System.ServiceModel.Dispatcher.BaseRequestProcessor::error_handlers_chain
	HandlersChain_t2728349147 * ___error_handlers_chain_2;
	// System.ServiceModel.Dispatcher.HandlersChain System.ServiceModel.Dispatcher.BaseRequestProcessor::finalize_handlers_chain
	HandlersChain_t2728349147 * ___finalize_handlers_chain_3;

public:
	inline static int32_t get_offset_of_initialize_handlers_chain_0() { return static_cast<int32_t>(offsetof(BaseRequestProcessor_t456598713, ___initialize_handlers_chain_0)); }
	inline HandlersChain_t2728349147 * get_initialize_handlers_chain_0() const { return ___initialize_handlers_chain_0; }
	inline HandlersChain_t2728349147 ** get_address_of_initialize_handlers_chain_0() { return &___initialize_handlers_chain_0; }
	inline void set_initialize_handlers_chain_0(HandlersChain_t2728349147 * value)
	{
		___initialize_handlers_chain_0 = value;
		Il2CppCodeGenWriteBarrier(&___initialize_handlers_chain_0, value);
	}

	inline static int32_t get_offset_of_process_handlers_chain_1() { return static_cast<int32_t>(offsetof(BaseRequestProcessor_t456598713, ___process_handlers_chain_1)); }
	inline HandlersChain_t2728349147 * get_process_handlers_chain_1() const { return ___process_handlers_chain_1; }
	inline HandlersChain_t2728349147 ** get_address_of_process_handlers_chain_1() { return &___process_handlers_chain_1; }
	inline void set_process_handlers_chain_1(HandlersChain_t2728349147 * value)
	{
		___process_handlers_chain_1 = value;
		Il2CppCodeGenWriteBarrier(&___process_handlers_chain_1, value);
	}

	inline static int32_t get_offset_of_error_handlers_chain_2() { return static_cast<int32_t>(offsetof(BaseRequestProcessor_t456598713, ___error_handlers_chain_2)); }
	inline HandlersChain_t2728349147 * get_error_handlers_chain_2() const { return ___error_handlers_chain_2; }
	inline HandlersChain_t2728349147 ** get_address_of_error_handlers_chain_2() { return &___error_handlers_chain_2; }
	inline void set_error_handlers_chain_2(HandlersChain_t2728349147 * value)
	{
		___error_handlers_chain_2 = value;
		Il2CppCodeGenWriteBarrier(&___error_handlers_chain_2, value);
	}

	inline static int32_t get_offset_of_finalize_handlers_chain_3() { return static_cast<int32_t>(offsetof(BaseRequestProcessor_t456598713, ___finalize_handlers_chain_3)); }
	inline HandlersChain_t2728349147 * get_finalize_handlers_chain_3() const { return ___finalize_handlers_chain_3; }
	inline HandlersChain_t2728349147 ** get_address_of_finalize_handlers_chain_3() { return &___finalize_handlers_chain_3; }
	inline void set_finalize_handlers_chain_3(HandlersChain_t2728349147 * value)
	{
		___finalize_handlers_chain_3 = value;
		Il2CppCodeGenWriteBarrier(&___finalize_handlers_chain_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
