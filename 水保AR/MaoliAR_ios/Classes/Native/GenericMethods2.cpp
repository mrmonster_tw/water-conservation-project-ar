﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1444694249.h"
#include "mscorlib_System_Array3549286319.h"
#include "mscorlib_System_Void1185182177.h"
#include "mscorlib_System_Int322950945753.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4218749037.h"
#include "mscorlib_System_RuntimeTypeHandle3027515415.h"
#include "mscorlib_System_String1847450689.h"
#include "mscorlib_System_Exception1436737249.h"
#include "mscorlib_System_Type2483944760.h"
#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_Boolean97287965.h"
#include "mscorlib_System_ArgumentException132251570.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge790968773.h"
#include "mscorlib_System_Collections_DictionaryEntry3123975638.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2549852611.h"
#include "mscorlib_System_Collections_Generic_Link544317964.h"
#include "mscorlib_System_DateTime3738529785.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23188640940.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2614517913.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3164406758.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2505983137.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen3384741.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1750446691.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22401056908.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1027527961.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1577416806.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1706577217.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge132545152.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4209139644.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22530217319.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3615381325.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4165270170.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3525131242.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr396552554.h"
#include "mscorlib_System_UInt162177724958.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21627836113.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3195380325.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr352683080.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3745269170.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3274888882.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr140567170.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp1927482598.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21377593753.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2689152581.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tra96697696.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3239041426.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge571830913.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1727736471.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamProfile_Prof3519391925.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22969503080.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1573263913.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1683866997.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2123152758.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1670703453.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr991236299.h"
#include "Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT3209881435.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24068375620.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1935636281.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr947366825.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1077142096.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1160396953.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2007148831.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManager_Tra4227350457.h"
#include "Vuforia_UnityExtensions_Vuforia_HoloLensExtendedTr2181165958.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23558069120.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2441242313.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1064339151.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3110523650.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge591960174.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3241055830.h"
#include "Vuforia_UnityExtensions_Vuforia_HoloLensExtendedTr1612729179.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22989632341.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3106712533.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1729809371.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tra49463353.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen80136809.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3336711695.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour1100905814.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22477808976.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2690545033.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1313641871.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr145119218.h"
#include "UnityEngine_UnityEngine_Component1923634451.h"
#include "mscorlib_System_Collections_Generic_List_1_gen257213610.h"
#include "UnityEngine_UnityEngine_GameObject1113636619.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall2703961024.h"
#include "mscorlib_System_Single1397266774.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"
#include "UnityEngine_UnityEngine_Vector22156229523.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve3484638744.h"
#include "UnityEngine_UnityEngine_Object631007953.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3395709193.h"
#include "UnityEngine_UnityEngine_Mesh3648964284.h"
#include "UnityEngine_UnityEngine_Mesh_InternalShaderChannel300897861.h"
#include "UnityEngine_UnityEngine_Mesh_InternalVertexChannelT299736786.h"
#include "UnityEngine_UnityEngine_Color322600501292.h"
#include "UnityEngine_UnityEngine_Vector33722313464.h"
#include "UnityEngine_UnityEngine_Vector43319028937.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4072576034.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3628304265.h"
#include "mscorlib_System_Collections_Generic_List_1_gen899420910.h"
#include "mscorlib_System_Collections_Generic_List_1_gen496136383.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup2436138090.h"
#include "UnityEngine_UnityEngine_TextAnchor2035777396.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis3613393006.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Const814224393.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corn1493259673.h"
#include "Vuforia_UnityExtensions_Vuforia_DelegateHelper3231076514.h"
#include "mscorlib_System_Action_1_gen269755560.h"
#include "mscorlib_System_Delegate1188392813.h"
#include "System_Core_System_Action_2_gen2470008838.h"
#include "mscorlib_System_Action_1_gen3252573759.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainInitia1789741059.h"
#include "mscorlib_System_Action_1_gen1962208654.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity_InitE3420749710.h"
#include "mscorlib_System_Action_1_gen3593217305.h"
#include "AssemblyU2DCSharp_NGUITools1206951095.h"
#include "AssemblyU2DCSharp_UIWidget3538521925.h"
#include "UnityEngine_UnityEngine_Transform3600365921.h"
#include "mscorlib_System_Activator1841325713.h"
#include "mscorlib_System_Predicate_1_gen3905400288.h"
#include "mscorlib_System_ArgumentNullException1615371798.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H1588725102.h"
#include "mscorlib_System_ArgumentOutOfRangeException777629997.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndex3332867892.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake1004704908.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Han1004704908.h"
#include "mscorlib_Mono_Security_Uri_UriScheme2867806342.h"
#include "System_Xml_Mono_Xml_XPath_DTMXPathAttributeNode23707096872.h"
#include "System_ServiceModel_Mono_Xml_XPath_DTMXPathAttribu3707096872.h"
#include "System_Xml_Mono_Xml_XPath_DTMXPathLinkedNode23353097823.h"
#include "System_ServiceModel_Mono_Xml_XPath_DTMXPathLinkedN3353097823.h"
#include "System_Xml_Mono_Xml_XPath_DTMXPathNamespaceNode21119120712.h"
#include "System_ServiceModel_Mono_Xml_XPath_DTMXPathNamespa1119120712.h"
#include "System_Xml_Mono_Xml_Xsl_Attribute270895445.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_TagName2891256255.h"
#include "AssemblyU2DCSharp_PresetSelector_Preset2972107632.h"
#include "AssemblyU2DCSharp_SetAnimArray_SetAnimInfo2127528194.h"
#include "mscorlib_System_ArraySegment_1_gen283560987.h"
#include "mscorlib_System_Byte1134296376.h"
#include "mscorlib_System_Char3634460470.h"
#include "System_Core_System_Collections_Generic_HashSet_1_L3080106562.h"
#include "System_Core_System_Collections_Generic_HashSet_1_L3209266973.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24237331251.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g71524366.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23568047141.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22387291312.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24188143612.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23842366416.h"
#include "mscorlib_System_Collections_Hashtable_Slot3975888750.h"
#include "mscorlib_System_Collections_SortedList_Slot384495010.h"
#include "mscorlib_System_Decimal2948259380.h"
#include "mscorlib_System_Double594665363.h"
#include "System_Drawing_System_Drawing_Color1869934208.h"
#include "System_Drawing_System_Drawing_Icon_IconDirEntry3003987536.h"
#include "System_Drawing_System_Drawing_Icon_IconImage1835934191.h"
#include "mscorlib_System_Int162552820387.h"
#include "mscorlib_System_Int643736567304.h"
#include "mscorlib_System_IntPtr840150181.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgu287865710.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg2723150157.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelDa360167391.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFi858502054.h"
#include "mscorlib_System_Reflection_Emit_ILTokenInfo2325775114.h"
#include "mscorlib_System_Reflection_Emit_Label2281661643.h"
#include "mscorlib_System_Reflection_Emit_MonoResource4103430009.h"
#include "mscorlib_System_Reflection_Emit_RefEmitPermissionSe484390987.h"
#include "mscorlib_System_Reflection_ParameterModifier1461694466.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceCac51292791.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceI2872965302.h"
#include "System_Runtime_Serialization_System_Runtime_Serial3072296154.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3541821701.h"
#include "mscorlib_System_SByte1669577662.h"
#include "System_System_Security_Cryptography_X509Certificate133602714.h"
#include "mscorlib_System_TermInfoStrings290279955.h"
#include "System_System_Text_RegularExpressions_Mark3471605523.h"
#include "mscorlib_System_TimeSpan881159249.h"
#include "mscorlib_System_TypeCode2987224087.h"
#include "mscorlib_System_UInt322560061978.h"
#include "mscorlib_System_UInt644134040092.h"
#include "System_System_Uri_UriScheme722425697.h"
#include "System_Web_System_Web_Compilation_AssemblyBuilder_1519973372.h"
#include "System_Web_System_Web_UI_HtmlTextWriter_AddedAttr2359971688.h"
#include "System_Web_System_Web_UI_HtmlTextWriter_AddedStyle611321135.h"
#include "System_Web_System_Web_UI_HtmlTextWriter_AddedTag1198678936.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsDecl3938094415.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsScope3958624705.h"
#include "System_Xml_System_Xml_XmlSpace3324193251.h"
#include "System_Xml_System_Xml_XPath_XPathResultType2828988488.h"
#include "AssemblyU2DCSharp_TypewriterEffect_FadeEntry639421133.h"
#include "AssemblyU2DCSharp_UICamera_DepthEntry628749918.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo3156717155.h"
#include "UnityEngine_UnityEngine_Bounds2266837910.h"
#include "UnityEngine_UnityEngine_ContactPoint3758755253.h"
#include "UnityEngine_UnityEngine_ContactPoint2D3390240644.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastRes3360306849.h"
#include "UnityEngine_UnityEngine_jvalue1372148875.h"
#include "UnityEngine_UnityEngine_KeyCode2599294277.h"
#include "UnityEngine_UnityEngine_Keyframe4206410242.h"
#include "UnityEngine_UnityEngine_Plane1000493321.h"
#include "UnityEngine_UnityEngine_RaycastHit1056001966.h"
#include "UnityEngine_UnityEngine_RaycastHit2D2279581989.h"
#include "UnityEngine_UnityEngine_Rect2360479859.h"
#include "UnityEngine_UnityEngine_RenderBuffer586150500.h"
#include "UnityEngine_UnityEngine_Rendering_RenderTargetIden2079184500.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat962350765.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo3229609740.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_675222246.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2125309831.h"
#include "UnityEngine_UnityEngine_Touch1921856868.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentTy1787303396.h"
#include "UnityEngine_UnityEngine_UICharInfo75501106.h"
#include "UnityEngine_UnityEngine_UILineInfo4195266810.h"
#include "UnityEngine_UnityEngine_UIVertex4057497605.h"
#include "UnityEngine_UnityEngine_WebCamDevice1322781432.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1379802392.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_C736195574.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3511111948.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1869387270.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice_Camer1483002240.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearDevice_Eyewe664929988.h"
#include "Vuforia_UnityExtensions_Vuforia_RectangleData1039179782.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Targe3441982613.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl_489181770.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl2744445717.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1300926610.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3800049328.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl2901758114.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3925829072.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1414459591.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl2883825721.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1835118957.h"
#include "System_ServiceModel_System_Collections_Generic_Key1429017627.h"
#include "mscorlib_System_Collections_ObjectModel_Collection2024462082.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_Ge395812252.h"
#include "mscorlib_System_NotImplementedException3489357830.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_K1189367464.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_R2003951325.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_S1930669850.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_S1271873540.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_X1311826287.h"
#include "System_System_Security_Cryptography_X509Certificate714049126.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_X1082272133.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_X3285382384.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_X5329336726.h"
#include "mscorlib_System_NotSupportedException1314879016.h"
#include "System_Runtime_Serialization_System_Runtime_Serial3509697551.h"
#include "mscorlib_System_Reflection_MemberInfo3380001741.h"
#include "System_Runtime_Serialization_System_Runtime_Serial2695450699.h"
#include "System_ServiceModel_System_ServiceModel_Channels_A2673164393.h"
#include "System_ServiceModel_System_ServiceModel_Channels_B2842489830.h"
#include "System_ServiceModel_System_ServiceModel_Channels_As888703423.h"
#include "System_ServiceModel_System_ServiceModel_Channels_B4154157131.h"
#include "System_ServiceModel_System_ServiceModel_Channels_M1958933598.h"
#include "System_ServiceModel_System_ServiceModel_Channels_B3643137745.h"
#include "System_ServiceModel_System_ServiceModel_Channels_B2456870521.h"
#include "mscorlib_System_Collections_ObjectModel_Collection2587493663.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Ch990592377.h"
#include "System_ServiceModel_System_ServiceModel_Channels_H2392894562.h"
#include "System_ServiceModel_System_ServiceModel_TransferMod436880017.h"
#include "System_ServiceModel_System_ServiceModel_Channels_H3982995109.h"
#include "System_ServiceModel_System_ServiceModel_Channels_T2144904149.h"
#include "System_ServiceModel_System_ServiceModel_Channels_M4050072634.h"
#include "System_Runtime_Serialization_System_Xml_XmlDiction1044334689.h"
#include "System_ServiceModel_System_ServiceModel_Channels_M3682274810.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1936922331.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Me464847589.h"
#include "System_Xml_System_Xml_XmlReader3121518892.h"
#include "System_Xml_System_Xml_XmlNodeType1672767151.h"
#include "System_ServiceModel_System_ServiceModel_EndpointAd3119842923.h"
#include "System_Runtime_Serialization_System_Runtime_Serial3967301761.h"
#include "System_ServiceModel_System_ServiceModel_Channels_M3589678829.h"
#include "System_ServiceModel_System_ServiceModel_Channels_M3584114940.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Na654821915.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Pe922597067.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Pe261693216.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Pnr85468628.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Se569455982.h"
#include "System_ServiceModel_System_ServiceModel_Channels_R1155974079.h"
#include "System_ServiceModel_System_ServiceModel_Channels_S3733530694.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Sy506905939.h"
#include "System_ServiceModel_System_ServiceModel_Channels_T1965552937.h"
#include "System_ServiceModel_System_ServiceModel_Channels_T2133327734.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Tr881905444.h"
#include "System_Runtime_Serialization_System_Xml_XmlDictiona173030297.h"
#include "System_ServiceModel_System_ServiceModel_Security_C2141883287.h"
#include "System_ServiceModel_System_ServiceModel_Channels_T3016354099.h"
#include "System_ServiceModel_System_ServiceModel_Channels_WSS97321761.h"
#include "System_ServiceModel_System_ServiceModel_OperationC2829644788.h"
#include "System_ServiceModel_System_ServiceModel_Security_T3624779732.h"
#include "System_Runtime_Serialization_System_Xml_UniqueId1383576913.h"
#include "System_ServiceModel_System_ServiceModel_Security_Se698951267.h"
#include "System_ServiceModel_System_ServiceModel_Security_To738218815.h"
#include "mscorlib_System_Threading_Interlocked2273387594.h"
#include "AssemblyU2DCSharp_UITweener260334902.h"
#include "AssemblyU2DCSharp_UITweener_Style3120619385.h"
#include "UnityEngine_UnityEngine_AnimationCurve3046754366.h"
#include "UnityEngine_UnityEngine_Behaviour1437897464.h"
#include "UnityEngine_UnityEngine_AttributeHelperEngine2735742303.h"
#include "UnityEngine_UnityEngine_CastHelper_1_gen2613165452.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD3903027533.h"
#include "UnityEngine_UnityEngine_Quaternion2301928331.h"
#include "UnityEngine_UnityEngine_Resources2942265397.h"
#include "UnityEngine_UnityEngine_ScriptableObject2528358522.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown2274391225.h"
#include "Vuforia_UnityExtensions_Vuforia_PremiumObjectFacto3495665550.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainBuilde1777433883.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainBuilde1656443109.h"
#include "Vuforia_UnityExtensions_Vuforia_ReconstructionFromT676137874.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackerManager1703337244.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackerManagerImpl3998245004.h"
#include "Vuforia_UnityExtensions_Vuforia_ObjectTracker4177997237.h"
#include "Vuforia_UnityExtensions_Vuforia_TextTracker3950053289.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainTracke1238706968.h"
#include "Vuforia_UnityExtensions_Vuforia_DeviceTracker2315692373.h"
#include "Vuforia_UnityExtensions_Vuforia_ObjectTrackerImpl4028644236.h"
#include "Vuforia_UnityExtensions_Vuforia_TextTrackerImpl1410587152.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainTracker651952228.h"
#include "Vuforia_UnityExtensions_Vuforia_RotationalPlayMode4048275232.h"
#include "Vuforia_UnityExtensions_Vuforia_RotationalDeviceTr1573407597.h"
#include "mscorlib_System_Reflection_CustomAttributeData1084486650.h"
#include "System_ServiceModel_System_ServiceModel_ServiceHos1894294968.h"
#include "mscorlib_System_Converter_2_gen2442480487.h"
#include "System_Web_System_Web_UI_BaseParser4057001241.h"
#include "System_Web_System_Web_VirtualPath4270372584.h"
#include "System_Core_System_Linq_Enumerable538148348.h"
#include "mscorlib_System_InvalidOperationException56020091.h"
#include "System_Core_System_Func_2_gen3759279471.h"
#include "System_Core_System_Linq_Enumerable_Fallback3495999270.h"
#include "System_Core_System_Func_2_gen1033609360.h"
#include "mscorlib_System_Collections_Generic_List_1_gen128053199.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1404457903.h"
#include "System_IdentityModel_System_IdentityModel_Selector1975057878.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve1764640198.h"
#include "mscorlib_System_Collections_Generic_List_1_gen777473367.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t1444694249;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Object>
struct Transform_1_t4218749037;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1436737249;
// System.ArgumentException
struct ArgumentException_t132251570;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.DateTime>
struct Dictionary_2_t790968773;
// System.Collections.DictionaryEntry[]
struct DictionaryEntryU5BU5D_t4217117203;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.DateTime,System.Collections.DictionaryEntry>
struct Transform_1_t2549852611;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.DateTime>[]
struct KeyValuePair_2U5BU5D_t2753949477;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.DateTime,System.Collections.Generic.KeyValuePair`2<System.Object,System.DateTime>>
struct Transform_1_t2614517913;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.DateTime[]
struct DateTimeU5BU5D_t1184652292;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.DateTime,System.DateTime>
struct Transform_1_t3164406758;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.DateTime,System.Object>
struct Transform_1_t2505983137;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t3384741;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.DictionaryEntry>
struct Transform_1_t1750446691;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct KeyValuePair_2U5BU5D_t1954543557;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct Transform_1_t1027527961;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Int32>
struct Transform_1_t1577416806;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Object>
struct Transform_1_t1706577217;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t132545152;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t4209139644;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t118269214;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Transform_1_t3615381325;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Object>
struct Transform_1_t4165270170;
// System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>
struct Dictionary_2_t3525131242;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.DictionaryEntry>
struct Transform_1_t396552554;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>[]
struct KeyValuePair_2U5BU5D_t600649420;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>
struct Transform_1_t3195380325;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Object>
struct Transform_1_t352683080;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.UInt16>
struct Transform_1_t3745269170;
// System.UInt16[]
struct UInt16U5BU5D_t3326319531;
// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t3274888882;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.DictionaryEntry>
struct Transform_1_t140567170;
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>[]
struct KeyValuePair_2U5BU5D_t344664036;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>
struct Transform_1_t2689152581;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Object>
struct Transform_1_t96697696;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,UnityEngine.TextEditor/TextEditOp>
struct Transform_1_t3239041426;
// UnityEngine.TextEditor/TextEditOp[]
struct TextEditOpU5BU5D_t3070334147;
// System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t571830913;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>
struct Transform_1_t1727736471;
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>[]
struct KeyValuePair_2U5BU5D_t1931833337;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>
struct Transform_1_t1573263913;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Object>
struct Transform_1_t1683866997;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,Vuforia.WebCamProfile/ProfileData>
struct Transform_1_t2123152758;
// Vuforia.WebCamProfile/ProfileData[]
struct ProfileDataU5BU5D_t362536152;
// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>
struct Dictionary_2_t1670703453;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t991236299;
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>[]
struct KeyValuePair_2U5BU5D_t1195333165;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>
struct Transform_1_t1935636281;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,System.Object>
struct Transform_1_t947366825;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,Vuforia.Image/PIXEL_FORMAT>
struct Transform_1_t1077142096;
// Vuforia.Image/PIXEL_FORMAT[]
struct PIXEL_FORMATU5BU5D_t87130330;
// System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>
struct Dictionary_2_t1160396953;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,System.Collections.DictionaryEntry>
struct Transform_1_t2007148831;
// System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>[]
struct KeyValuePair_2U5BU5D_t2211245697;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>>
struct Transform_1_t2441242313;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>
struct Transform_1_t1064339151;
// Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry[]
struct PoseAgeEntryU5BU5D_t3970047907;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,Vuforia.VuforiaManager/TrackableIdPair>
struct Transform_1_t3110523650;
// Vuforia.VuforiaManager/TrackableIdPair[]
struct TrackableIdPairU5BU5D_t475764036;
// System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>
struct Dictionary_2_t591960174;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,System.Collections.DictionaryEntry>
struct Transform_1_t3241055830;
// System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>[]
struct KeyValuePair_2U5BU5D_t3445152696;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>>
struct Transform_1_t3106712533;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>
struct Transform_1_t1729809371;
// Vuforia.HoloLensExtendedTrackingManager/PoseInfo[]
struct PoseInfoU5BU5D_t908987610;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,Vuforia.VuforiaManager/TrackableIdPair>
struct Transform_1_t49463353;
// System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>
struct Dictionary_2_t80136809;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,System.Collections.DictionaryEntry>
struct Transform_1_t3336711695;
// System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>[]
struct KeyValuePair_2U5BU5D_t3540808561;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>>
struct Transform_1_t2690545033;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status>
struct Transform_1_t1313641871;
// Vuforia.TrackableBehaviour/Status[]
struct StatusU5BU5D_t1004643475;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,Vuforia.VuforiaManager/TrackableIdPair>
struct Transform_1_t145119218;
// UnityEngine.Component
struct Component_t1923634451;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Collections.Generic.IList`1<UnityEngine.EventSystems.IEventSystemHandler>
struct IList_1_t875036337;
// UnityEngine.Object
struct Object_t631007953;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t3395709193;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// UnityEngine.Color32[]
struct Color32U5BU5D_t3850468773;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t934056436;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t4072576034;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t496136383;
// UnityEngine.UI.LayoutGroup
struct LayoutGroup_t2436138090;
// System.Action`1<System.Boolean>
struct Action_1_t269755560;
// System.Delegate
struct Delegate_t1188392813;
// System.Action`2<System.Object,System.Object>
struct Action_2_t2470008838;
// System.Action`1<System.Object>
struct Action_1_t3252573759;
// System.Action`1<Vuforia.SmartTerrainInitializationInfo>
struct Action_1_t1962208654;
// System.Action`1<Vuforia.VuforiaUnity/InitError>
struct Action_1_t3593217305;
// UIWidget
struct UIWidget_t3538521925;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.Predicate`1<System.Object>
struct Predicate_1_t3905400288;
// System.ArgumentNullException
struct ArgumentNullException_t1615371798;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_t777629997;
// System.Collections.Generic.KeyedByTypeCollection`1<System.Object>
struct KeyedByTypeCollection_1_t1429017627;
// System.IdentityModel.Tokens.GenericXmlSecurityToken
struct GenericXmlSecurityToken_t395812252;
// System.NotImplementedException
struct NotImplementedException_t3489357830;
// System.IdentityModel.Tokens.KerberosRequestorSecurityToken
struct KerberosRequestorSecurityToken_t1189367464;
// System.IdentityModel.Tokens.RsaSecurityToken
struct RsaSecurityToken_t2003951325;
// System.IdentityModel.Tokens.SamlSecurityToken
struct SamlSecurityToken_t1930669850;
// System.IdentityModel.Tokens.SecurityToken
struct SecurityToken_t1271873540;
// System.IdentityModel.Tokens.X509SecurityToken
struct X509SecurityToken_t1311826287;
// System.IdentityModel.Tokens.X509ThumbprintKeyIdentifierClause
struct X509ThumbprintKeyIdentifierClause_t1082272133;
// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t714049126;
// System.IdentityModel.Tokens.X509IssuerSerialKeyIdentifierClause
struct X509IssuerSerialKeyIdentifierClause_t3285382384;
// System.IdentityModel.Tokens.X509RawDataKeyIdentifierClause
struct X509RawDataKeyIdentifierClause_t329336726;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// System.Runtime.Serialization.KnownTypeCollection
struct KnownTypeCollection_t3509697551;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.ServiceModel.Channels.AsymmetricSecurityBindingElement
struct AsymmetricSecurityBindingElement_t2673164393;
// System.ServiceModel.Channels.BindingContext
struct BindingContext_t2842489830;
// System.ServiceModel.Channels.AsymmetricSecurityCapabilities
struct AsymmetricSecurityCapabilities_t888703423;
// System.ServiceModel.Channels.BinaryMessageEncodingBindingElement
struct BinaryMessageEncodingBindingElement_t4154157131;
// System.ServiceModel.Channels.MessageVersion
struct MessageVersion_t1958933598;
// System.ServiceModel.Channels.BindingElement
struct BindingElement_t3643137745;
// System.ServiceModel.Channels.BindingElementCollection
struct BindingElementCollection_t2456870521;
// System.Collections.ObjectModel.Collection`1<System.ServiceModel.Channels.BindingElement>
struct Collection_1_t2587493663;
// System.Collections.Generic.IEnumerator`1<System.ServiceModel.Channels.BindingElement>
struct IEnumerator_1_t4075708213;
// System.Collections.ObjectModel.Collection`1<System.Object>
struct Collection_1_t2024462082;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t3512676632;
// System.ServiceModel.Channels.ChannelListenerBase
struct ChannelListenerBase_t990592377;
// System.ServiceModel.Channels.HttpTransportBindingElement
struct HttpTransportBindingElement_t2392894562;
// System.ServiceModel.Channels.HttpTransportBindingElement/HttpBindingProperties
struct HttpBindingProperties_t3982995109;
// System.ServiceModel.Channels.MessageHeaders
struct MessageHeaders_t4050072634;
// System.Collections.Generic.List`1<System.ServiceModel.Channels.MessageHeaderInfo>
struct List_1_t1936922331;
// System.ServiceModel.Channels.MessageHeaderInfo
struct MessageHeaderInfo_t464847589;
// System.ServiceModel.Channels.MessageHeader/DefaultMessageHeader
struct DefaultMessageHeader_t3682274810;
// System.Xml.XmlDictionaryReader
struct XmlDictionaryReader_t1044334689;
// System.ServiceModel.EndpointAddress
struct EndpointAddress_t3119842923;
// System.Runtime.Serialization.XmlObjectSerializer
struct XmlObjectSerializer_t3967301761;
// System.ServiceModel.Channels.MsmqBindingElementBase
struct MsmqBindingElementBase_t3589678829;
// System.ServiceModel.Channels.MtomMessageEncodingBindingElement
struct MtomMessageEncodingBindingElement_t3584114940;
// System.ServiceModel.Channels.NamedPipeTransportBindingElement
struct NamedPipeTransportBindingElement_t654821915;
// System.ServiceModel.Channels.PeerCustomResolverBindingElement
struct PeerCustomResolverBindingElement_t922597067;
// System.ServiceModel.Channels.PeerTransportBindingElement
struct PeerTransportBindingElement_t261693216;
// System.ServiceModel.Channels.PnrpPeerResolverBindingElement
struct PnrpPeerResolverBindingElement_t85468628;
// System.ServiceModel.Channels.SecurityChannelListener`1<System.Object>
struct SecurityChannelListener_1_t569455982;
// System.ServiceModel.Channels.SymmetricSecurityBindingElement
struct SymmetricSecurityBindingElement_t3733530694;
// System.ServiceModel.Channels.SymmetricSecurityCapabilities
struct SymmetricSecurityCapabilities_t506905939;
// System.ServiceModel.Channels.TcpTransportBindingElement
struct TcpTransportBindingElement_t1965552937;
// System.ServiceModel.Channels.TextMessageEncodingBindingElement
struct TextMessageEncodingBindingElement_t2133327734;
// System.ServiceModel.Channels.TransactionFlowBindingElement
struct TransactionFlowBindingElement_t881905444;
// System.ServiceModel.Channels.TransportBindingElement
struct TransportBindingElement_t2144904149;
// System.Xml.XmlDictionaryReaderQuotas
struct XmlDictionaryReaderQuotas_t173030297;
// System.ServiceModel.Security.ChannelProtectionRequirements
struct ChannelProtectionRequirements_t2141883287;
// System.ServiceModel.Channels.TransportSecurityBindingElement
struct TransportSecurityBindingElement_t3016354099;
// System.ServiceModel.Channels.WSSecurityMessageHeader
struct WSSecurityMessageHeader_t97321761;
// System.ServiceModel.OperationContext
struct OperationContext_t2829644788;
// System.ServiceModel.Security.Tokens.SecurityContextSecurityToken
struct SecurityContextSecurityToken_t3624779732;
// System.Xml.UniqueId
struct UniqueId_t1383576913;
// System.ServiceModel.Security.SecurityContextKeyIdentifierClause
struct SecurityContextKeyIdentifierClause_t698951267;
// System.ServiceModel.Security.Tokens.WrappedKeySecurityToken
struct WrappedKeySecurityToken_t738218815;
// UITweener
struct UITweener_t260334902;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t1068524471;
// UnityEngine.Behaviour
struct Behaviour_t1437897464;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t3903027533;
// UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522;
// Vuforia.PremiumObjectFactory/NullPremiumObjectFactory
struct NullPremiumObjectFactory_t3495665550;
// Vuforia.SmartTerrainBuilderImpl
struct SmartTerrainBuilderImpl_t1656443109;
// Vuforia.IVuforiaWrapper
struct IVuforiaWrapper_t2381307640;
// Vuforia.ReconstructionFromTargetImpl
struct ReconstructionFromTargetImpl_t676137874;
// Vuforia.IPremiumObjectFactory
struct IPremiumObjectFactory_t2078405174;
// Vuforia.TrackerManagerImpl
struct TrackerManagerImpl_t3998245004;
// Vuforia.ObjectTrackerImpl
struct ObjectTrackerImpl_t4028644236;
// Vuforia.TextTrackerImpl
struct TextTrackerImpl_t1410587152;
// Vuforia.SmartTerrainTrackerImpl
struct SmartTerrainTrackerImpl_t651952228;
// Vuforia.RotationalPlayModeDeviceTrackerImpl
struct RotationalPlayModeDeviceTrackerImpl_t4048275232;
// Vuforia.RotationalDeviceTrackerImpl
struct RotationalDeviceTrackerImpl_t1573407597;
// UnityEngine.Object[]
struct ObjectU5BU5D_t1417781964;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t3710464795;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1465843424;
// System.ServiceModel.ServiceHost
struct ServiceHost_t1894294968;
// System.Converter`2<System.Object,System.Object>
struct Converter_2_t2442480487;
// System.Web.UI.BaseParser
struct BaseParser_t4057001241;
// System.Web.VirtualPath
struct VirtualPath_t4270372584;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2059959053;
// System.InvalidOperationException
struct InvalidOperationException_t56020091;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t3759279471;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerable_1_t1510070208;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Boolean>
struct Func_2_t1033609360;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t1930798642;
// System.Collections.Generic.IEnumerable`1<Vuforia.VuforiaManager/TrackableIdPair>
struct IEnumerable_1_t3207203346;
// System.IdentityModel.Selectors.SecurityTokenRequirement
struct SecurityTokenRequirement_t1975057878;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>
struct EventFunction_1_t1764640198;
// System.Collections.Generic.IList`1<UnityEngine.Transform>
struct IList_1_t1120718408;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t777473367;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1436737249_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t132251570_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1492106003;
extern Il2CppCodeGenString* _stringLiteral4007973390;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m3695543300_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3188640940_m904145848_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisDateTime_t3738529785_m742909906_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m2559484862_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2401056908_m1169495264_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisInt32_t2950945753_m1134171305_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m1362949338_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2530217319_m1439704807_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m3942192587_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t1627836113_m1280446632_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m3278344529_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisUInt16_t2177724958_m902960032_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t1377593753_m4023145528_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m141062928_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisTextEditOp_t1927482598_m1340764829_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2969503080_m4178501938_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m1055602370_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisProfileData_t3519391925_m816695391_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t4068375620_m2332577487_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m4257328066_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisPIXEL_FORMAT_t3209881435_m3459464361_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3558069120_m3124385426_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisPoseAgeEntry_t2181165958_m2559446055_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisTrackableIdPair_t4227350457_m667701602_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2989632341_m644375200_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisPoseInfo_t1612729179_m82049209_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisTrackableIdPair_t4227350457_m2949278720_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2477808976_m1598207542_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisStatus_t1100905814_m812461599_MetadataUsageId;
extern const uint32_t Dictionary_2_Do_ICollectionCopyTo_TisTrackableIdPair_t4227350457_m3157615771_MetadataUsageId;
extern const uint32_t Component_GetComponents_TisIl2CppObject_m2416546752_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral273595399;
extern const uint32_t BaseInvokableCall_ThrowOnInvalidArg_TisBoolean_t97287965_m1128114209_MetadataUsageId;
extern const uint32_t BaseInvokableCall_ThrowOnInvalidArg_TisInt32_t2950945753_m3123282398_MetadataUsageId;
extern const uint32_t BaseInvokableCall_ThrowOnInvalidArg_TisIl2CppObject_m1037130369_MetadataUsageId;
extern const uint32_t BaseInvokableCall_ThrowOnInvalidArg_TisSingle_t1397266774_m182307123_MetadataUsageId;
extern const uint32_t BaseInvokableCall_ThrowOnInvalidArg_TisColor_t2555686324_m391225187_MetadataUsageId;
extern const uint32_t BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t2156229523_m3759800740_MetadataUsageId;
extern Il2CppClass* Object_t631007953_il2cpp_TypeInfo_var;
extern Il2CppClass* ListPool_1_t2953223642_il2cpp_TypeInfo_var;
extern Il2CppClass* ExecuteEvents_t3484638744_il2cpp_TypeInfo_var;
extern Il2CppClass* IEventSystemHandler_t3354683850_il2cpp_TypeInfo_var;
extern Il2CppClass* ICollection_1_t1887868788_il2cpp_TypeInfo_var;
extern const MethodInfo* ListPool_1_Get_m2062177143_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponents_TisComponent_t1923634451_m1648148377_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m3306164819_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m1294235957_MethodInfo_var;
extern const MethodInfo* ListPool_1_Release_m3920209327_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1261396091;
extern Il2CppCodeGenString* _stringLiteral2001578372;
extern const uint32_t ExecuteEvents_GetEventList_TisIl2CppObject_m3803188029_MetadataUsageId;
extern const uint32_t GameObject_GetComponents_TisIl2CppObject_m974145097_MetadataUsageId;
extern const uint32_t GameObject_GetComponentsInChildren_TisIl2CppObject_m4174741699_MetadataUsageId;
extern const uint32_t GameObject_GetComponentsInParent_TisIl2CppObject_m947018401_MetadataUsageId;
extern Il2CppClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2415889112;
extern const uint32_t Mesh_SetUvsImpl_TisIl2CppObject_m2518356015_MetadataUsageId;
extern const uint32_t Mesh_SetUvsImpl_TisVector2_t2156229523_m984389559_MetadataUsageId;
extern const uint32_t DelegateHelper_InvokeWithExceptionHandling_TisBoolean_t97287965_m1676209294_MetadataUsageId;
extern const uint32_t DelegateHelper_InvokeWithExceptionHandling_TisIl2CppObject_TisIl2CppObject_m3522526466_MetadataUsageId;
extern const uint32_t DelegateHelper_InvokeWithExceptionHandling_TisIl2CppObject_m3886612143_MetadataUsageId;
extern const uint32_t DelegateHelper_InvokeWithExceptionHandling_TisSmartTerrainInitializationInfo_t1789741059_m3284269701_MetadataUsageId;
extern const uint32_t DelegateHelper_InvokeWithExceptionHandling_TisInitError_t3420749710_m585457832_MetadataUsageId;
extern Il2CppClass* NGUITools_t1206951095_il2cpp_TypeInfo_var;
extern const uint32_t NGUITools_AddChild_TisIl2CppObject_m3339032197_MetadataUsageId;
extern const uint32_t NGUITools_AddChild_TisIl2CppObject_m1990647493_MetadataUsageId;
extern const uint32_t NGUITools_AddMissingComponent_TisIl2CppObject_m159845787_MetadataUsageId;
extern const uint32_t NGUITools_AddWidget_TisIl2CppObject_m1311540038_MetadataUsageId;
extern const uint32_t NGUITools_AddWidget_TisIl2CppObject_m797893513_MetadataUsageId;
extern const uint32_t NGUITools_FindInParents_TisIl2CppObject_m780518502_MetadataUsageId;
extern const uint32_t NGUITools_FindInParents_TisIl2CppObject_m3955890412_MetadataUsageId;
extern const uint32_t Activator_CreateInstance_TisIl2CppObject_m729575857_MetadataUsageId;
extern Il2CppClass* ArgumentNullException_t1615371798_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral461028519;
extern const uint32_t Array_Find_TisIl2CppObject_m2842000327_MetadataUsageId;
extern const uint32_t Array_FindLast_TisIl2CppObject_m1088586648_MetadataUsageId;
extern Il2CppClass* ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral797640427;
extern const uint32_t Array_InternalArray__get_Item_TisData_t1588725102_m1153206052_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisTableRange_t3332867892_m1483480711_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisClientCertificateType_t1004704908_m2297379651_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisClientCertificateType_t1004704909_m2297379652_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisUriScheme_t2867806342_m2246384759_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisDTMXPathAttributeNode2_t3707096872_m2076492393_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisDTMXPathAttributeNode2_t3707096873_m2076492394_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisDTMXPathLinkedNode2_t3353097823_m3898909110_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisDTMXPathLinkedNode2_t3353097824_m3898909111_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisDTMXPathNamespaceNode2_t1119120712_m318820268_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisDTMXPathNamespaceNode2_t1119120713_m318820269_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisAttribute_t270895445_m4280563089_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisTagName_t2891256255_m926239580_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisPreset_t2972107632_m2524355772_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisSetAnimInfo_t2127528194_m1640025244_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisArraySegment_1_t283560987_m1350465735_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisBoolean_t97287965_m1407010309_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisByte_t1134296376_m3566214066_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisChar_t3634460470_m324132692_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisDictionaryEntry_t3123975638_m479537688_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisLink_t3080106562_m1916144364_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisLink_t3209266973_m1574224299_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t4237331251_m1101612129_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t71524366_m252172060_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t3568047141_m192728881_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t2387291312_m2065480692_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t4188143612_m3177769408_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t3842366416_m3937535230_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t3188640940_m541164372_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t2401056908_m3647027688_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t2530217319_m2886833132_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t1627836113_m1686737554_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t1377593753_m3379274744_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t2969503080_m3564523187_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t4068375620_m428999009_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t3558069120_m1688560335_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t2989632341_m3852954896_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t2477808976_m2299162451_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisLink_t544317964_m1669566993_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisSlot_t3975888750_m905303097_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisSlot_t384495010_m2861978404_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisDateTime_t3738529785_m623181444_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisDecimal_t2948259380_m3511003792_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisDouble_t594665363_m850827605_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisColor_t1869934208_m1908199522_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisIconDirEntry_t3003987536_m2088850479_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisIconImage_t1835934191_m4181697865_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisInt16_t2552820387_m76930473_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisInt32_t2950945753_m714868479_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisInt64_t3736567304_m3562990826_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisIntPtr_t_m784054003_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisIl2CppObject_m3347010206_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t287865710_m2282658220_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t2723150157_m2639399822_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisLabelData_t360167391_m1054702781_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisLabelFixup_t858502054_m3276643490_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisILTokenInfo_t2325775114_m3110830457_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisLabel_t2281661643_m2294767982_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisMonoResource_t4103430009_m2937222811_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisRefEmitPermissionSet_t484390987_m1505876205_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisParameterModifier_t1461694466_m29553316_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisResourceCacheItem_t51292791_m1306056717_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisResourceInfo_t2872965302_m3865610257_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisEnumMemberInfo_t3072296154_m3120872482_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisTypeTag_t3541821701_m4208350471_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisSByte_t1669577662_m2349608172_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisX509ChainStatus_t133602714_m2237651489_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisSingle_t1397266774_m1672589487_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisTermInfoStrings_t290279955_m1174084225_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisMark_t3471605523_m3397473850_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisTimeSpan_t881159249_m1885583191_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisTypeCode_t2987224087_m3502052715_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisUInt16_t2177724958_m3601205466_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisUInt32_t2560061978_m1955195035_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisUInt64_t4134040092_m129291315_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisUriScheme_t722425697_m2816273040_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisCodeUnit_t1519973372_m4096517762_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisAddedAttr_t2359971688_m1570912593_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisAddedStyle_t611321135_m3480009420_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisAddedTag_t1198678936_m2674902006_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisNsDecl_t3938094415_m3184366930_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisNsScope_t3958624705_m3796816205_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisXmlSpace_t3324193251_m4247271528_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisXPathResultType_t2828988488_m305960836_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisFadeEntry_t639421133_m3938258723_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisDepthEntry_t628749918_m111472233_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisAnimatorClipInfo_t3156717155_m2815898241_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisBounds_t2266837910_m2942110546_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisColor_t2555686324_m2985413820_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisColor32_t2600501292_m1325986122_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisContactPoint_t3758755253_m2489897608_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisContactPoint2D_t3390240644_m1483798952_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisRaycastResult_t3360306849_m1872700081_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_Tisjvalue_t1372148875_m327974056_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisKeyCode_t2599294277_m2562599561_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisKeyframe_t4206410242_m27698365_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisPlane_t1000493321_m2362885170_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisRaycastHit_t1056001966_m3352067444_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisRaycastHit2D_t2279581989_m2440275162_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisRect_t2360479859_m1464655811_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisRenderBuffer_t586150500_m686729538_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisRenderTargetIdentifier_t2079184500_m4172643256_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisRenderTextureFormat_t962350765_m925159806_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisHitInfo_t3229609740_m2260995172_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisGcAchievementData_t675222246_m2680268485_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisGcScoreData_t2125309831_m174676143_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisTextEditOp_t1927482598_m730697523_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisTouch_t1921856868_m1955772797_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisContentType_t1787303396_m421427711_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisUICharInfo_t75501106_m1797321427_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisUILineInfo_t4195266810_m1305614921_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisUIVertex_t4057497605_m289307453_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisVector2_t2156229523_m2502961026_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisVector3_t3722313464_m2720091419_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisVector4_t3319028937_m1117939728_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisWebCamDevice_t1322781432_m2046557297_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisQualitySettings_t1379802392_m2691165501_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisPreset_t736195574_m203402203_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisFrame_t3511111948_m2708263693_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisQualitySettings_t1869387270_m3375905464_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisCameraField_t1483002240_m4229449498_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisEyewearCalibrationReading_t664929988_m608053677_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisPoseAgeEntry_t2181165958_m1672129510_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisPoseInfo_t1612729179_m3253150969_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisPIXEL_FORMAT_t3209881435_m1056154199_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisRectangleData_t1039179782_m3235823909_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisTargetSearchResult_t3441982613_m1391463760_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisStatus_t1100905814_m1160951035_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisTrackableIdPair_t4227350457_m281251552_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisPropData_t489181770_m770296646_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t2744445717_m4103853089_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisSurfaceData_t1300926610_m3000210253_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisTrackableResultData_t3800049328_m112364720_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisVirtualButtonData_t2901758114_m352267582_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisVuMarkTargetData_t3925829072_m4035236093_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisVuMarkTargetResultData_t1414459591_m3260416437_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisWordData_t2883825721_m4217774079_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisWordResultData_t1835118957_m2197241955_MetadataUsageId;
extern const uint32_t Array_InternalArray__get_Item_TisProfileData_t3519391925_m544952548_MetadataUsageId;
extern Il2CppClass* IEnumerator_t1853284238_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t3640265483_il2cpp_TypeInfo_var;
extern const uint32_t KeyedByTypeCollection_1_Find_TisIl2CppObject_m727987262_MetadataUsageId;
extern Il2CppClass* NotImplementedException_t3489357830_il2cpp_TypeInfo_var;
extern const uint32_t GenericXmlSecurityToken_CreateKeyIdentifierClause_TisIl2CppObject_m932525054_MetadataUsageId;
extern const uint32_t KerberosRequestorSecurityToken_CreateKeyIdentifierClause_TisIl2CppObject_m2465289282_MetadataUsageId;
extern const uint32_t RsaSecurityToken_CreateKeyIdentifierClause_TisIl2CppObject_m3998194118_MetadataUsageId;
extern const uint32_t SamlSecurityToken_CreateKeyIdentifierClause_TisIl2CppObject_m723444784_MetadataUsageId;
extern const uint32_t SecurityToken_CreateKeyIdentifierClause_TisIl2CppObject_m3879203870_MetadataUsageId;
extern const Il2CppType* X509ThumbprintKeyIdentifierClause_t1082272133_0_0_0_var;
extern const Il2CppType* X509IssuerSerialKeyIdentifierClause_t3285382384_0_0_0_var;
extern const Il2CppType* X509RawDataKeyIdentifierClause_t329336726_0_0_0_var;
extern Il2CppClass* X509ThumbprintKeyIdentifierClause_t1082272133_il2cpp_TypeInfo_var;
extern Il2CppClass* X509IssuerSerialKeyIdentifierClause_t3285382384_il2cpp_TypeInfo_var;
extern Il2CppClass* X509RawDataKeyIdentifierClause_t329336726_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral686316168;
extern const uint32_t X509SecurityToken_CreateKeyIdentifierClause_TisIl2CppObject_m332784898_MetadataUsageId;
extern const uint32_t KnownTypeCollection_GetAttribute_TisIl2CppObject_m197020807_MetadataUsageId;
extern const uint32_t TypeExtensions_GetCustomAttribute_TisIl2CppObject_m1592309407_MetadataUsageId;
extern const Il2CppType* ISecurityCapabilities_t4129699402_0_0_0_var;
extern const Il2CppType* IdentityVerifier_t614541315_0_0_0_var;
extern Il2CppCodeGenString* _stringLiteral1340338390;
extern const uint32_t AsymmetricSecurityBindingElement_GetProperty_TisIl2CppObject_m2388565458_MetadataUsageId;
extern const Il2CppType* MessageVersion_t1958933598_0_0_0_var;
extern const uint32_t BinaryMessageEncodingBindingElement_GetProperty_TisIl2CppObject_m3714584124_MetadataUsageId;
extern Il2CppClass* BindingContext_t2842489830_il2cpp_TypeInfo_var;
extern const uint32_t BindingContext_GetInnerProperty_TisIl2CppObject_m42862505_MetadataUsageId;
extern Il2CppClass* IEnumerator_1_t4075708213_il2cpp_TypeInfo_var;
extern const MethodInfo* Collection_1_GetEnumerator_m2650847642_MethodInfo_var;
extern const uint32_t BindingElementCollection_Find_TisIl2CppObject_m774808656_MetadataUsageId;
extern const Il2CppType* IBindingDeliveryCapabilities_t3355695844_0_0_0_var;
extern const Il2CppType* TransferMode_t436880017_0_0_0_var;
extern Il2CppClass* HttpBindingProperties_t3982995109_il2cpp_TypeInfo_var;
extern Il2CppClass* TransferMode_t436880017_il2cpp_TypeInfo_var;
extern const uint32_t HttpTransportBindingElement_GetProperty_TisIl2CppObject_m2685399948_MetadataUsageId;
extern const Il2CppType* EndpointAddress_t3119842923_0_0_0_var;
extern Il2CppClass* DefaultMessageHeader_t3682274810_il2cpp_TypeInfo_var;
extern Il2CppClass* EndpointAddress_t3119842923_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m14301231_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1657857521_MethodInfo_var;
extern const uint32_t MessageHeaders_GetHeader_TisIl2CppObject_m1623046356_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3975789767;
extern const uint32_t MessageHeaders_GetHeader_TisIl2CppObject_m1357331642_MetadataUsageId;
extern Il2CppClass* IBindingDeliveryCapabilities_t3355695844_il2cpp_TypeInfo_var;
extern Il2CppClass* ISecurityCapabilities_t4129699402_il2cpp_TypeInfo_var;
extern const uint32_t MsmqBindingElementBase_GetProperty_TisIl2CppObject_m4150449364_MetadataUsageId;
extern const uint32_t MtomMessageEncodingBindingElement_GetProperty_TisIl2CppObject_m3684534025_MetadataUsageId;
extern const uint32_t PeerCustomResolverBindingElement_GetProperty_TisIl2CppObject_m1644286882_MetadataUsageId;
extern const Il2CppType* IBindingMulticastCapabilities_t1459441759_0_0_0_var;
extern const uint32_t PeerTransportBindingElement_GetProperty_TisIl2CppObject_m695965485_MetadataUsageId;
extern const uint32_t PnrpPeerResolverBindingElement_GetProperty_TisIl2CppObject_m1493533106_MetadataUsageId;
extern const Il2CppType* MessageSecurityBindingSupport_t1904510157_0_0_0_var;
extern const uint32_t SecurityChannelListener_1_GetProperty_TisIl2CppObject_m2093574432_MetadataUsageId;
extern const uint32_t SymmetricSecurityBindingElement_GetProperty_TisIl2CppObject_m697793203_MetadataUsageId;
extern const uint32_t TransactionFlowBindingElement_GetProperty_TisIl2CppObject_m137456711_MetadataUsageId;
extern const Il2CppType* XmlDictionaryReaderQuotas_t173030297_0_0_0_var;
extern const Il2CppType* ChannelProtectionRequirements_t2141883287_0_0_0_var;
extern Il2CppClass* XmlDictionaryReaderQuotas_t173030297_il2cpp_TypeInfo_var;
extern Il2CppClass* ChannelProtectionRequirements_t2141883287_il2cpp_TypeInfo_var;
extern const uint32_t TransportBindingElement_GetProperty_TisIl2CppObject_m3683280528_MetadataUsageId;
extern const uint32_t TransportSecurityBindingElement_GetProperty_TisIl2CppObject_m2606895813_MetadataUsageId;
extern Il2CppClass* IEnumerator_1_t3512676632_il2cpp_TypeInfo_var;
extern const MethodInfo* Collection_1_GetEnumerator_m3456018579_MethodInfo_var;
extern const uint32_t WSSecurityMessageHeader_Find_TisIl2CppObject_m526510621_MetadataUsageId;
extern const Il2CppType* SecurityContextKeyIdentifierClause_t698951267_0_0_0_var;
extern Il2CppClass* SecurityContextKeyIdentifierClause_t698951267_il2cpp_TypeInfo_var;
extern const uint32_t SecurityContextSecurityToken_CreateKeyIdentifierClause_TisIl2CppObject_m359550936_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2654637398;
extern const uint32_t WrappedKeySecurityToken_CreateKeyIdentifierClause_TisIl2CppObject_m4062238421_MetadataUsageId;
extern Il2CppClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern Il2CppClass* KeyframeU5BU5D_t1068524471_il2cpp_TypeInfo_var;
extern Il2CppClass* AnimationCurve_t3046754366_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3466665877;
extern Il2CppCodeGenString* _stringLiteral684852591;
extern const uint32_t UITweener_Begin_TisIl2CppObject_m2692891208_MetadataUsageId;
extern const uint32_t AttributeHelperEngine_GetCustomAttributeOfType_TisIl2CppObject_m2601070632_MetadataUsageId;
extern Il2CppClass* CastHelper_1_t2613165452_il2cpp_TypeInfo_var;
extern const uint32_t Component_GetComponent_TisIl2CppObject_m2735705429_MetadataUsageId;
extern const uint32_t Component_GetComponentInChildren_TisIl2CppObject_m3600364750_MetadataUsageId;
extern const uint32_t Component_GetComponentInParent_TisIl2CppObject_m3491943679_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2964872255;
extern const uint32_t ExecuteEvents_ValidateEventData_TisIl2CppObject_m1594546529_MetadataUsageId;
extern const uint32_t GameObject_AddComponent_TisIl2CppObject_m3678101806_MetadataUsageId;
extern const uint32_t GameObject_GetComponent_TisIl2CppObject_m2385344436_MetadataUsageId;
extern const uint32_t GameObject_GetComponentInChildren_TisIl2CppObject_m1898919846_MetadataUsageId;
extern const uint32_t Object_FindObjectOfType_TisIl2CppObject_m1542987838_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2475671027;
extern const uint32_t Object_Instantiate_TisIl2CppObject_m3168325340_MetadataUsageId;
extern const uint32_t Object_Instantiate_TisIl2CppObject_m878221869_MetadataUsageId;
extern const uint32_t Object_Instantiate_TisIl2CppObject_m864675153_MetadataUsageId;
extern const uint32_t Object_Instantiate_TisIl2CppObject_m1135049463_MetadataUsageId;
extern const uint32_t Object_Instantiate_TisIl2CppObject_m3913942063_MetadataUsageId;
extern const uint32_t Resources_GetBuiltinResource_TisIl2CppObject_m3352626831_MetadataUsageId;
extern const uint32_t Resources_Load_TisIl2CppObject_m1502289511_MetadataUsageId;
extern const uint32_t ScriptableObject_CreateInstance_TisIl2CppObject_m3068602743_MetadataUsageId;
extern const uint32_t Dropdown_GetOrAddComponent_TisIl2CppObject_m769901662_MetadataUsageId;
extern const uint32_t NullPremiumObjectFactory_CreateReconstruction_TisIl2CppObject_m2685558895_MetadataUsageId;
extern const Il2CppType* ReconstructionFromTarget_t3809448279_0_0_0_var;
extern Il2CppClass* VuforiaWrapper_t2763746413_il2cpp_TypeInfo_var;
extern Il2CppClass* IVuforiaWrapper_t2381307640_il2cpp_TypeInfo_var;
extern Il2CppClass* ReconstructionFromTargetImpl_t676137874_il2cpp_TypeInfo_var;
extern const uint32_t SmartTerrainBuilderImpl_CreateReconstruction_TisIl2CppObject_m2589243573_MetadataUsageId;
extern const Il2CppType* ObjectTracker_t4177997237_0_0_0_var;
extern const Il2CppType* TextTracker_t3950053289_0_0_0_var;
extern const Il2CppType* SmartTerrainTracker_t1238706968_0_0_0_var;
extern const Il2CppType* DeviceTracker_t2315692373_0_0_0_var;
extern const Il2CppType* RotationalDeviceTracker_t2847210804_0_0_0_var;
extern Il2CppCodeGenString* _stringLiteral2521874635;
extern const uint32_t TrackerManagerImpl_GetTracker_TisIl2CppObject_m3003825392_MetadataUsageId;
extern Il2CppClass* VuforiaRuntimeUtilities_t399660591_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeMapping_t3272509632_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectTrackerImpl_t4028644236_il2cpp_TypeInfo_var;
extern Il2CppClass* TextTrackerImpl_t1410587152_il2cpp_TypeInfo_var;
extern Il2CppClass* SmartTerrainTrackerImpl_t651952228_il2cpp_TypeInfo_var;
extern Il2CppClass* RotationalPlayModeDeviceTrackerImpl_t4048275232_il2cpp_TypeInfo_var;
extern Il2CppClass* RotationalDeviceTrackerImpl_t1573407597_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3741637024;
extern Il2CppCodeGenString* _stringLiteral1345897578;
extern const uint32_t TrackerManagerImpl_InitTracker_TisIl2CppObject_m917366830_MetadataUsageId;
extern const uint32_t NGUITools_FindActive_TisIl2CppObject_m1941122218_MetadataUsageId;
extern const uint32_t Array_FindAll_TisIl2CppObject_m3566631088_MetadataUsageId;
extern const uint32_t GameObject_GetComponents_TisIl2CppObject_m1405838976_MetadataUsageId;
extern const uint32_t GameObject_GetComponentsInChildren_TisIl2CppObject_m836045_MetadataUsageId;
extern const uint32_t GameObject_GetComponentsInParent_TisIl2CppObject_m145281020_MetadataUsageId;
extern const uint32_t Object_FindObjectsOfType_TisIl2CppObject_m2647183545_MetadataUsageId;
extern const uint32_t ServiceHost_PopulateAttribute_TisIl2CppObject_m1706125618_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral879148213;
extern const uint32_t Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2417852296_MetadataUsageId;
extern Il2CppClass* WebConfigurationManager_t2451561378_il2cpp_TypeInfo_var;
extern const uint32_t BaseParser_GetConfigSection_TisIl2CppObject_m2542956124_MetadataUsageId;
extern Il2CppClass* InvalidOperationException_t56020091_il2cpp_TypeInfo_var;
extern const uint32_t Enumerable_First_TisIl2CppObject_m3835263318_MetadataUsageId;
extern const uint32_t Enumerable_First_TisIl2CppObject_m2925268555_MetadataUsageId;
extern Il2CppClass* KeyValuePair_2_t2530217319_il2cpp_TypeInfo_var;
extern const uint32_t Enumerable_Last_TisKeyValuePair_2_t2530217319_m2391423423_MetadataUsageId;
extern const uint32_t Enumerable_Last_TisIl2CppObject_m191782855_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3710528600;
extern const uint32_t SecurityTokenRequirement_GetProperty_TisIl2CppObject_m4175878597_MetadataUsageId;
extern const MethodInfo* List_1_get_Item_m3022113929_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3787308655_MethodInfo_var;
extern const uint32_t ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m3266560969_MetadataUsageId;
extern const uint32_t ExecuteEvents_GetEventHandler_TisIl2CppObject_m3687647312_MetadataUsageId;

// System.Object[]
struct ObjectU5BU5D_t2843939325  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.DictionaryEntry[]
struct DictionaryEntryU5BU5D_t4217117203  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DictionaryEntry_t3123975638  m_Items[1];

public:
	inline DictionaryEntry_t3123975638  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline DictionaryEntry_t3123975638 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, DictionaryEntry_t3123975638  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline DictionaryEntry_t3123975638  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline DictionaryEntry_t3123975638 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, DictionaryEntry_t3123975638  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t964245573  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t544317964  m_Items[1];

public:
	inline Link_t544317964  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Link_t544317964 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Link_t544317964  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Link_t544317964  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Link_t544317964 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Link_t544317964  value)
	{
		m_Items[index] = value;
	}
};
// System.DateTime[]
struct DateTimeU5BU5D_t1184652292  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DateTime_t3738529785  m_Items[1];

public:
	inline DateTime_t3738529785  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline DateTime_t3738529785 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, DateTime_t3738529785  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline DateTime_t3738529785  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline DateTime_t3738529785 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, DateTime_t3738529785  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.DateTime>[]
struct KeyValuePair_2U5BU5D_t2753949477  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3188640940  m_Items[1];

public:
	inline KeyValuePair_2_t3188640940  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t3188640940 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3188640940  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t3188640940  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t3188640940 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t3188640940  value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t385246372  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct KeyValuePair_2U5BU5D_t1954543557  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2401056908  m_Items[1];

public:
	inline KeyValuePair_2_t2401056908  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t2401056908 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2401056908  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t2401056908  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t2401056908 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t2401056908  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t118269214  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2530217319  m_Items[1];

public:
	inline KeyValuePair_2_t2530217319  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t2530217319 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2530217319  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t2530217319  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t2530217319 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t2530217319  value)
	{
		m_Items[index] = value;
	}
};
// System.UInt16[]
struct UInt16U5BU5D_t3326319531  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint16_t m_Items[1];

public:
	inline uint16_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint16_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint16_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint16_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint16_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint16_t value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>[]
struct KeyValuePair_2U5BU5D_t600649420  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1627836113  m_Items[1];

public:
	inline KeyValuePair_2_t1627836113  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t1627836113 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1627836113  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t1627836113  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t1627836113 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t1627836113  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.TextEditor/TextEditOp[]
struct TextEditOpU5BU5D_t3070334147  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>[]
struct KeyValuePair_2U5BU5D_t344664036  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1377593753  m_Items[1];

public:
	inline KeyValuePair_2_t1377593753  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t1377593753 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1377593753  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t1377593753  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t1377593753 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t1377593753  value)
	{
		m_Items[index] = value;
	}
};
// Vuforia.WebCamProfile/ProfileData[]
struct ProfileDataU5BU5D_t362536152  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ProfileData_t3519391925  m_Items[1];

public:
	inline ProfileData_t3519391925  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ProfileData_t3519391925 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ProfileData_t3519391925  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline ProfileData_t3519391925  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ProfileData_t3519391925 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ProfileData_t3519391925  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>[]
struct KeyValuePair_2U5BU5D_t1931833337  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2969503080  m_Items[1];

public:
	inline KeyValuePair_2_t2969503080  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t2969503080 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2969503080  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t2969503080  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t2969503080 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t2969503080  value)
	{
		m_Items[index] = value;
	}
};
// Vuforia.Image/PIXEL_FORMAT[]
struct PIXEL_FORMATU5BU5D_t87130330  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>[]
struct KeyValuePair_2U5BU5D_t1195333165  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t4068375620  m_Items[1];

public:
	inline KeyValuePair_2_t4068375620  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t4068375620 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t4068375620  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t4068375620  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t4068375620 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t4068375620  value)
	{
		m_Items[index] = value;
	}
};
// Vuforia.VuforiaManager/TrackableIdPair[]
struct TrackableIdPairU5BU5D_t475764036  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TrackableIdPair_t4227350457  m_Items[1];

public:
	inline TrackableIdPair_t4227350457  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline TrackableIdPair_t4227350457 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, TrackableIdPair_t4227350457  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline TrackableIdPair_t4227350457  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline TrackableIdPair_t4227350457 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, TrackableIdPair_t4227350457  value)
	{
		m_Items[index] = value;
	}
};
// Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry[]
struct PoseAgeEntryU5BU5D_t3970047907  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PoseAgeEntry_t2181165958  m_Items[1];

public:
	inline PoseAgeEntry_t2181165958  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline PoseAgeEntry_t2181165958 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, PoseAgeEntry_t2181165958  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline PoseAgeEntry_t2181165958  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline PoseAgeEntry_t2181165958 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, PoseAgeEntry_t2181165958  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>[]
struct KeyValuePair_2U5BU5D_t2211245697  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3558069120  m_Items[1];

public:
	inline KeyValuePair_2_t3558069120  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t3558069120 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3558069120  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t3558069120  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t3558069120 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t3558069120  value)
	{
		m_Items[index] = value;
	}
};
// Vuforia.HoloLensExtendedTrackingManager/PoseInfo[]
struct PoseInfoU5BU5D_t908987610  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PoseInfo_t1612729179  m_Items[1];

public:
	inline PoseInfo_t1612729179  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline PoseInfo_t1612729179 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, PoseInfo_t1612729179  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline PoseInfo_t1612729179  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline PoseInfo_t1612729179 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, PoseInfo_t1612729179  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>[]
struct KeyValuePair_2U5BU5D_t3445152696  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2989632341  m_Items[1];

public:
	inline KeyValuePair_2_t2989632341  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t2989632341 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2989632341  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t2989632341  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t2989632341 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t2989632341  value)
	{
		m_Items[index] = value;
	}
};
// Vuforia.TrackableBehaviour/Status[]
struct StatusU5BU5D_t1004643475  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>[]
struct KeyValuePair_2U5BU5D_t3540808561  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2477808976  m_Items[1];

public:
	inline KeyValuePair_2_t2477808976  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t2477808976 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2477808976  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t2477808976  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t2477808976 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t2477808976  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Color_t2555686324  m_Items[1];

public:
	inline Color_t2555686324  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color_t2555686324 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color_t2555686324  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color_t2555686324  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color_t2555686324 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color_t2555686324  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Color32[]
struct Color32U5BU5D_t3850468773  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Color32_t2600501292  m_Items[1];

public:
	inline Color32_t2600501292  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color32_t2600501292 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color32_t2600501292  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color32_t2600501292  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color32_t2600501292 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color32_t2600501292  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Vector2_t2156229523  m_Items[1];

public:
	inline Vector2_t2156229523  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2_t2156229523 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2_t2156229523  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2_t2156229523  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2_t2156229523 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2_t2156229523  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Vector3_t3722313464  m_Items[1];

public:
	inline Vector3_t3722313464  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t3722313464 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t3722313464  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t3722313464  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t3722313464 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t3722313464  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t934056436  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Vector4_t3319028937  m_Items[1];

public:
	inline Vector4_t3319028937  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector4_t3319028937 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector4_t3319028937  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector4_t3319028937  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector4_t3319028937 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector4_t3319028937  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t1068524471  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Keyframe_t4206410242  m_Items[1];

public:
	inline Keyframe_t4206410242  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Keyframe_t4206410242 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Keyframe_t4206410242  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Keyframe_t4206410242  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Keyframe_t4206410242 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Keyframe_t4206410242  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Object[]
struct ObjectU5BU5D_t1417781964  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Object_t631007953 * m_Items[1];

public:
	inline Object_t631007953 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Object_t631007953 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Object_t631007953 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Object_t631007953 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Object_t631007953 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Object_t631007953 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t3710464795  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CustomAttributeNamedArgument_t287865710  m_Items[1];

public:
	inline CustomAttributeNamedArgument_t287865710  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CustomAttributeNamedArgument_t287865710 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CustomAttributeNamedArgument_t287865710  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline CustomAttributeNamedArgument_t287865710  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CustomAttributeNamedArgument_t287865710 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CustomAttributeNamedArgument_t287865710  value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1465843424  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CustomAttributeTypedArgument_t2723150157  m_Items[1];

public:
	inline CustomAttributeTypedArgument_t2723150157  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CustomAttributeTypedArgument_t2723150157 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CustomAttributeTypedArgument_t2723150157  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline CustomAttributeTypedArgument_t2723150157  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CustomAttributeTypedArgument_t2723150157 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CustomAttributeTypedArgument_t2723150157  value)
	{
		m_Items[index] = value;
	}
};


// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<System.Object>::Get()
extern "C"  List_1_t257213610 * ListPool_1_Get_m1670010485_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
// System.Void UnityEngine.GameObject::GetComponents<System.Object>(System.Collections.Generic.List`1<!!0>)
extern "C"  void GameObject_GetComponents_TisIl2CppObject_m974145097_gshared (GameObject_t1113636619 * __this, List_1_t257213610 * p0, const MethodInfo* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_get_Item_m2287542950_gshared (List_1_t257213610 * __this, int32_t p0, const MethodInfo* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const MethodInfo* method);
// System.Void UnityEngine.UI.ListPool`1<System.Object>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m957266927_gshared (Il2CppObject * __this /* static, unused */, List_1_t257213610 * p0, const MethodInfo* method);
// System.Collections.Generic.IEnumerator`1<!0> System.Collections.ObjectModel.Collection`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m3456018579_gshared (Collection_1_t2024462082 * __this, const MethodInfo* method);

// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m1620074514 (Il2CppObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m88164663 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Type::get_IsPrimitive()
extern "C"  bool Type_get_IsPrimitive_m1114712797 (Type_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::.ctor()
extern "C"  void Exception__ctor_m213470898 (Exception_t1436737249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String,System.String,System.Exception)
extern "C"  void ArgumentException__ctor_m3761792013 (ArgumentException_t132251570 * __this, String_t* ___message0, String_t* ___paramName1, Exception_t1436737249 * ___innerException2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)
extern "C"  void Component_GetComponentsForListInternal_m1978301288 (Component_t1923634451 * __this, Type_t * ___searchType0, Il2CppObject * ___resultList1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1113636619 * Component_get_gameObject_m2648350745 (Component_t1923634451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
extern "C"  String_t* UnityString_Format_m3741272017 (Il2CppObject * __this /* static, unused */, String_t* ___fmt0, ObjectU5BU5D_t2843939325* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String)
extern "C"  void ArgumentException__ctor_m1312628991 (ArgumentException_t132251570 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String,System.String)
extern "C"  void ArgumentException__ctor_m1216717135 (ArgumentException_t132251570 * __this, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1454075600 (Il2CppObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
extern "C"  bool GameObject_get_activeInHierarchy_m921367020 (GameObject_t1113636619 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Component>::Get()
#define ListPool_1_Get_m2062177143(__this /* static, unused */, method) ((  List_1_t3395709193 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1_Get_m1670010485_gshared)(__this /* static, unused */, method)
// System.Void UnityEngine.GameObject::GetComponents<UnityEngine.Component>(System.Collections.Generic.List`1<!!0>)
#define GameObject_GetComponents_TisComponent_t1923634451_m1648148377(__this, p0, method) ((  void (*) (GameObject_t1113636619 *, List_1_t3395709193 *, const MethodInfo*))GameObject_GetComponents_TisIl2CppObject_m974145097_gshared)(__this, p0, method)
// !0 System.Collections.Generic.List`1<UnityEngine.Component>::get_Item(System.Int32)
#define List_1_get_Item_m3306164819(__this, p0, method) ((  Component_t1923634451 * (*) (List_1_t3395709193 *, int32_t, const MethodInfo*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count()
#define List_1_get_Count_m1294235957(__this, method) ((  int32_t (*) (List_1_t3395709193 *, const MethodInfo*))List_1_get_Count_m2934127733_gshared)(__this, method)
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Component>::Release(System.Collections.Generic.List`1<T>)
#define ListPool_1_Release_m3920209327(__this /* static, unused */, p0, method) ((  void (*) (Il2CppObject * /* static, unused */, List_1_t3395709193 *, const MethodInfo*))ListPool_1_Release_m957266927_gshared)(__this /* static, unused */, p0, method)
// System.Array UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)
extern "C"  Il2CppArray * GameObject_GetComponentsInternal_m525215727 (GameObject_t1113636619 * __this, Type_t * ___type0, bool ___useSearchTypeAsArrayReturnType1, bool ___recursive2, bool ___includeInactive3, bool ___reverse4, Il2CppObject * ___resultList5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mesh::DefaultDimensionForChannel(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  int32_t Mesh_DefaultDimensionForChannel_m4089233615 (Il2CppObject * __this /* static, unused */, int32_t ___channel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mesh::SafeLength(System.Array)
extern "C"  int32_t Mesh_SafeLength_m2456625434 (Mesh_t3648964284 * __this, Il2CppArray * ___values0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetSizedArrayForChannel(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,System.Array,System.Int32)
extern "C"  void Mesh_SetSizedArrayForChannel_m2433695052 (Mesh_t3648964284 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, Il2CppArray * ___values3, int32_t ___valuesCount4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Array UnityEngine.Mesh::ExtractArrayFromList(System.Object)
extern "C"  Il2CppArray * Mesh_ExtractArrayFromList_m3633351874 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m2059623341 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh/InternalShaderChannel UnityEngine.Mesh::GetUVChannel(System.Int32)
extern "C"  int32_t Mesh_GetUVChannel_m3402048366 (Mesh_t3648964284 * __this, int32_t ___uvIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Boolean::Equals(System.Object)
extern "C"  bool Boolean_Equals_m2410333903 (bool* __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutGroup::SetDirty()
extern "C"  void LayoutGroup_SetDirty_m957775107 (LayoutGroup_t2436138090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Int32::Equals(System.Object)
extern "C"  bool Int32_Equals_m3996243976 (int32_t* __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Single::Equals(System.Object)
extern "C"  bool Single_Equals_m438106747 (float* __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector2::Equals(System.Object)
extern "C"  bool Vector2_Equals_m1356052954 (Vector2_t2156229523 * __this, Il2CppObject * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DelegateHelper::InvokeDelegate(System.Delegate,System.Object[])
extern "C"  void DelegateHelper_InvokeDelegate_m431786658 (Il2CppObject * __this /* static, unused */, Delegate_t1188392813 * ___action0, ObjectU5BU5D_t2843939325* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject NGUITools::AddChild(UnityEngine.GameObject)
extern "C"  GameObject_t1113636619 * NGUITools_AddChild_m2967414470 (Il2CppObject * __this /* static, unused */, GameObject_t1113636619 * ___parent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::set_name(System.String)
extern "C"  void Object_set_name_m653319976 (Object_t631007953 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject NGUITools::AddChild(UnityEngine.GameObject,System.Boolean)
extern "C"  GameObject_t1113636619 * NGUITools_AddChild_m1975831075 (Il2CppObject * __this /* static, unused */, GameObject_t1113636619 * ___parent0, bool ___undo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NGUITools::CalculateNextDepth(UnityEngine.GameObject)
extern "C"  int32_t NGUITools_CalculateNextDepth_m3108909969 (Il2CppObject * __this /* static, unused */, GameObject_t1113636619 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::set_width(System.Int32)
extern "C"  void UIWidget_set_width_m181008468 (UIWidget_t3538521925 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::set_height(System.Int32)
extern "C"  void UIWidget_set_height_m878974275 (UIWidget_t3538521925 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::set_depth(System.Int32)
extern "C"  void UIWidget_set_depth_m2124062109 (UIWidget_t3538521925 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3600365921 * GameObject_get_transform_m393750976 (GameObject_t1113636619 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t3600365921 * Transform_get_parent_m1293647796 (Transform_t3600365921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m1920811489 (Il2CppObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Activator::CreateInstance(System.Type)
extern "C"  Il2CppObject * Activator_CreateInstance_m3631483688 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C"  void ArgumentNullException__ctor_m1170824041 (ArgumentNullException_t1615371798 * __this, String_t* ___paramName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::get_Length()
extern "C"  int32_t Array_get_Length_m21610649 (Il2CppArray * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String)
extern "C"  void ArgumentOutOfRangeException__ctor_m3628145864 (ArgumentOutOfRangeException_t777629997 * __this, String_t* ___paramName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotImplementedException::.ctor()
extern "C"  void NotImplementedException__ctor_m3058704252 (NotImplementedException_t3489357830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IdentityModel.Tokens.X509ThumbprintKeyIdentifierClause::.ctor(System.Security.Cryptography.X509Certificates.X509Certificate2)
extern "C"  void X509ThumbprintKeyIdentifierClause__ctor_m618984855 (X509ThumbprintKeyIdentifierClause_t1082272133 * __this, X509Certificate2_t714049126 * ___certificate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IdentityModel.Tokens.X509IssuerSerialKeyIdentifierClause::.ctor(System.Security.Cryptography.X509Certificates.X509Certificate2)
extern "C"  void X509IssuerSerialKeyIdentifierClause__ctor_m1675252971 (X509IssuerSerialKeyIdentifierClause_t3285382384 * __this, X509Certificate2_t714049126 * ___certificate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IdentityModel.Tokens.X509RawDataKeyIdentifierClause::.ctor(System.Security.Cryptography.X509Certificates.X509Certificate2)
extern "C"  void X509RawDataKeyIdentifierClause__ctor_m2770603042 (X509RawDataKeyIdentifierClause_t329336726 * __this, X509Certificate2_t714049126 * ___certificate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object)
extern "C"  String_t* String_Format_m2844511972 (Il2CppObject * __this /* static, unused */, String_t* p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor(System.String)
extern "C"  void NotSupportedException__ctor_m2494070935 (NotSupportedException_t1314879016 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ServiceModel.Channels.AsymmetricSecurityCapabilities System.ServiceModel.Channels.AsymmetricSecurityBindingElement::GetCapabilities()
extern "C"  AsymmetricSecurityCapabilities_t888703423 * AsymmetricSecurityBindingElement_GetCapabilities_m330455506 (AsymmetricSecurityBindingElement_t2673164393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ServiceModel.Channels.MessageVersion System.ServiceModel.Channels.BinaryMessageEncodingBindingElement::get_MessageVersion()
extern "C"  MessageVersion_t1958933598 * BinaryMessageEncodingBindingElement_get_MessageVersion_m3795418149 (BinaryMessageEncodingBindingElement_t4154157131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ServiceModel.Channels.BindingContext::PrepareElements()
extern "C"  bool BindingContext_PrepareElements_m1563199935 (BindingContext_t2842489830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ServiceModel.Channels.BindingElement System.ServiceModel.Channels.BindingContext::DequeueBindingElement(System.Boolean)
extern "C"  BindingElement_t3643137745 * BindingContext_DequeueBindingElement_m1422365611 (BindingContext_t2842489830 * __this, bool ___raiseError0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<!0> System.Collections.ObjectModel.Collection`1<System.ServiceModel.Channels.BindingElement>::GetEnumerator()
#define Collection_1_GetEnumerator_m2650847642(__this, method) ((  Il2CppObject* (*) (Collection_1_t2587493663 *, const MethodInfo*))Collection_1_GetEnumerator_m3456018579_gshared)(__this, method)
// System.Void System.ServiceModel.Channels.HttpTransportBindingElement/HttpBindingProperties::.ctor(System.ServiceModel.Channels.HttpTransportBindingElement)
extern "C"  void HttpBindingProperties__ctor_m3977539646 (HttpBindingProperties_t3982995109 * __this, HttpTransportBindingElement_t2392894562 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ServiceModel.TransferMode System.ServiceModel.Channels.HttpTransportBindingElement::get_TransferMode()
extern "C"  int32_t HttpTransportBindingElement_get_TransferMode_m2062667909 (HttpTransportBindingElement_t2392894562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<System.ServiceModel.Channels.MessageHeaderInfo>::get_Count()
#define List_1_get_Count_m14301231(__this, method) ((  int32_t (*) (List_1_t1936922331 *, const MethodInfo*))List_1_get_Count_m2934127733_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<System.ServiceModel.Channels.MessageHeaderInfo>::get_Item(System.Int32)
#define List_1_get_Item_m1657857521(__this, p0, method) ((  MessageHeaderInfo_t464847589 * (*) (List_1_t1936922331 *, int32_t, const MethodInfo*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Object System.ServiceModel.Channels.MessageHeader/DefaultMessageHeader::get_Value()
extern "C"  Il2CppObject * DefaultMessageHeader_get_Value_m164191447 (DefaultMessageHeader_t3682274810 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlDictionaryReader System.ServiceModel.Channels.MessageHeaders::GetReaderAtHeader(System.Int32)
extern "C"  XmlDictionaryReader_t1044334689 * MessageHeaders_GetReaderAtHeader_m2101511628 (MessageHeaders_t4050072634 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ServiceModel.EndpointAddress System.ServiceModel.EndpointAddress::ReadFrom(System.Xml.XmlDictionaryReader)
extern "C"  EndpointAddress_t3119842923 * EndpointAddress_ReadFrom_m2950182928 (Il2CppObject * __this /* static, unused */, XmlDictionaryReader_t1044334689 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ServiceModel.Channels.SymmetricSecurityCapabilities System.ServiceModel.Channels.SymmetricSecurityBindingElement::GetCapabilities()
extern "C"  SymmetricSecurityCapabilities_t506905939 * SymmetricSecurityBindingElement_GetCapabilities_m3874139475 (SymmetricSecurityBindingElement_t3733530694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlDictionaryReaderQuotas::.ctor()
extern "C"  void XmlDictionaryReaderQuotas__ctor_m1273539748 (XmlDictionaryReaderQuotas_t173030297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlDictionaryReaderQuotas::set_MaxStringContentLength(System.Int32)
extern "C"  void XmlDictionaryReaderQuotas_set_MaxStringContentLength_m2248955720 (XmlDictionaryReaderQuotas_t173030297 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ServiceModel.Security.ChannelProtectionRequirements::.ctor()
extern "C"  void ChannelProtectionRequirements__ctor_m4267701572 (ChannelProtectionRequirements_t2141883287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ServiceModel.Channels.MessageVersion System.ServiceModel.Channels.MessageVersion::get_Default()
extern "C"  MessageVersion_t1958933598 * MessageVersion_get_Default_m3338319686 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ObjectModel.Collection`1<System.Object> System.ServiceModel.Channels.WSSecurityMessageHeader::get_Contents()
extern "C"  Collection_1_t2024462082 * WSSecurityMessageHeader_get_Contents_m3609380460 (WSSecurityMessageHeader_t97321761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<!0> System.Collections.ObjectModel.Collection`1<System.Object>::GetEnumerator()
#define Collection_1_GetEnumerator_m3456018579(__this, method) ((  Il2CppObject* (*) (Collection_1_t2024462082 *, const MethodInfo*))Collection_1_GetEnumerator_m3456018579_gshared)(__this, method)
// System.Xml.UniqueId System.ServiceModel.Security.Tokens.SecurityContextSecurityToken::get_ContextId()
extern "C"  UniqueId_t1383576913 * SecurityContextSecurityToken_get_ContextId_m3447042347 (SecurityContextSecurityToken_t3624779732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.UniqueId System.ServiceModel.Security.Tokens.SecurityContextSecurityToken::get_KeyGeneration()
extern "C"  UniqueId_t1383576913 * SecurityContextSecurityToken_get_KeyGeneration_m1708319148 (SecurityContextSecurityToken_t3624779732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ServiceModel.Security.SecurityContextKeyIdentifierClause::.ctor(System.Xml.UniqueId,System.Xml.UniqueId)
extern "C"  void SecurityContextKeyIdentifierClause__ctor_m366404922 (SecurityContextKeyIdentifierClause_t698951267 * __this, UniqueId_t1383576913 * ___contextId0, UniqueId_t1383576913 * ___generation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String NGUITools::GetHierarchy(UnityEngine.GameObject)
extern "C"  String_t* NGUITools_GetHierarchy_m3080800227 (Il2CppObject * __this /* static, unused */, GameObject_t1113636619 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object[])
extern "C"  String_t* String_Concat_m2971454694 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t2843939325* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object,UnityEngine.Object)
extern "C"  void Debug_LogError_m3356949141 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Object_t631007953 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UITweener::get_amountPerDelta()
extern "C"  float UITweener_get_amountPerDelta_m1343789098 (UITweener_t260334902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Keyframe::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Keyframe__ctor_m2938821330 (Keyframe_t4206410242 * __this, float p0, float p1, float p2, float p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve__ctor_m1726807957 (AnimationCurve_t3046754366 * __this, KeyframeU5BU5D_t1068524471* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C"  void Behaviour_set_enabled_m3107225489 (Behaviour_t1437897464 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IntPtr::.ctor(System.Void*)
extern "C"  void IntPtr__ctor_m3384658186 (IntPtr_t* __this, void* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)
extern "C"  void Component_GetComponentFastPath_m476901382 (Component_t1923634451 * __this, Type_t * ___type0, IntPtr_t ___oneFurtherThanResultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.Component::GetComponentInChildren(System.Type,System.Boolean)
extern "C"  Component_t1923634451 * Component_GetComponentInChildren_m2139176229 (Component_t1923634451 * __this, Type_t * ___t0, bool ___includeInactive1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.Component::GetComponentInParent(System.Type)
extern "C"  Component_t1923634451 * Component_GetComponentInParent_m2023105846 (Component_t1923634451 * __this, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object,System.Object)
extern "C"  String_t* String_Format_m2556382932 (Il2CppObject * __this /* static, unused */, String_t* p0, Il2CppObject * p1, Il2CppObject * p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::AddComponent(System.Type)
extern "C"  Component_t1923634451 * GameObject_AddComponent_m2485158059 (GameObject_t1113636619 * __this, Type_t * ___componentType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::GetComponentFastPath(System.Type,System.IntPtr)
extern "C"  void GameObject_GetComponentFastPath_m730500428 (GameObject_t1113636619 * __this, Type_t * ___type0, IntPtr_t ___oneFurtherThanResultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::GetComponentInChildren(System.Type,System.Boolean)
extern "C"  Component_t1923634451 * GameObject_GetComponentInChildren_m230563287 (GameObject_t1113636619 * __this, Type_t * ___type0, bool ___includeInactive1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::FindObjectOfType(System.Type)
extern "C"  Object_t631007953 * Object_FindObjectOfType_m1736538631 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::CheckNullArgument(System.Object,System.String)
extern "C"  void Object_CheckNullArgument_m129989854 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
extern "C"  Object_t631007953 * Object_Internal_CloneSingle_m1527886285 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
extern "C"  Object_t631007953 * Object_Instantiate_m3931415074 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___original0, Transform_t3600365921 * ___parent1, bool ___instantiateInWorldSpace2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Object_t631007953 * Object_Instantiate_m2563497874 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___original0, Vector3_t3722313464  ___position1, Quaternion_t2301928331  ___rotation2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern "C"  Object_t631007953 * Object_Instantiate_m3242828326 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___original0, Vector3_t3722313464  ___position1, Quaternion_t2301928331  ___rotation2, Transform_t3600365921 * ___parent3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Resources::GetBuiltinResource(System.Type,System.String)
extern "C"  Object_t631007953 * Resources_GetBuiltinResource_m629864230 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, String_t* ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
extern "C"  Object_t631007953 * Resources_Load_m4202687077 (Il2CppObject * __this /* static, unused */, String_t* ___path0, Type_t * ___systemTypeInstance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.Type)
extern "C"  ScriptableObject_t2528358522 * ScriptableObject_CreateInstance_m2508048690 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m487959476 (Il2CppObject * __this /* static, unused */, Object_t631007953 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.IVuforiaWrapper Vuforia.VuforiaWrapper::get_Instance()
extern "C"  Il2CppObject * VuforiaWrapper_get_Instance_m2947582594 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetImpl::.ctor(System.IntPtr)
extern "C"  void ReconstructionFromTargetImpl__ctor_m1359819978 (ReconstructionFromTargetImpl_t676137874 * __this, IntPtr_t ___nativeReconstructionPtr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.IPremiumObjectFactory Vuforia.PremiumObjectFactory::get_Instance()
extern "C"  Il2CppObject * PremiumObjectFactory_get_Instance_m2847660535 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VuforiaRuntimeUtilities::IsVuforiaEnabled()
extern "C"  bool VuforiaRuntimeUtilities_IsVuforiaEnabled_m1152377305 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VuforiaRuntimeUtilities::IsPlayMode()
extern "C"  bool VuforiaRuntimeUtilities_IsPlayMode_m4165764373 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 Vuforia.TypeMapping::GetTypeID(System.Type)
extern "C"  uint16_t TypeMapping_GetTypeID_m2138532019 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ObjectTrackerImpl::.ctor()
extern "C"  void ObjectTrackerImpl__ctor_m236183719 (ObjectTrackerImpl_t4028644236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TextTrackerImpl::.ctor()
extern "C"  void TextTrackerImpl__ctor_m3064977494 (TextTrackerImpl_t1410587152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerImpl::.ctor()
extern "C"  void SmartTerrainTrackerImpl__ctor_m2510818320 (SmartTerrainTrackerImpl_t651952228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.RotationalPlayModeDeviceTrackerImpl::.ctor()
extern "C"  void RotationalPlayModeDeviceTrackerImpl__ctor_m3977961433 (RotationalPlayModeDeviceTrackerImpl_t4048275232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.RotationalDeviceTrackerImpl::.ctor()
extern "C"  void RotationalDeviceTrackerImpl__ctor_m134363731 (RotationalDeviceTrackerImpl_t1573407597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
extern "C"  ObjectU5BU5D_t1417781964* Object_FindObjectsOfType_m2898745631 (Il2CppObject * __this /* static, unused */, Type_t * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Mesh::get_canAccess()
extern "C"  bool Mesh_get_canAccess_m2982329792 (Mesh_t3648964284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Mesh::HasChannel(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  bool Mesh_HasChannel_m3577110678 (Mesh_t3648964284 * __this, int32_t ___channel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Array UnityEngine.Mesh::GetAllocArrayFromChannelImpl(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  Il2CppArray * Mesh_GetAllocArrayFromChannelImpl_m1881002877 (Mesh_t3648964284 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::PrintErrorCantAccessMesh(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  void Mesh_PrintErrorCantAccessMesh_m868675854 (Mesh_t3648964284 * __this, int32_t ___channel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Web.VirtualPath System.Web.UI.BaseParser::get_VirtualPath()
extern "C"  VirtualPath_t4270372584 * BaseParser_get_VirtualPath_m2391628584 (BaseParser_t4057001241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.VirtualPath::get_Absolute()
extern "C"  String_t* VirtualPath_get_Absolute_m3947569336 (VirtualPath_t4270372584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Web.Configuration.WebConfigurationManager::GetSection(System.String)
extern "C"  Il2CppObject * WebConfigurationManager_GetSection_m1176823268 (Il2CppObject * __this /* static, unused */, String_t* ___sectionName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Web.Configuration.WebConfigurationManager::GetSection(System.String,System.String)
extern "C"  Il2CppObject * WebConfigurationManager_GetSection_m285847730 (Il2CppObject * __this /* static, unused */, String_t* ___sectionName0, String_t* ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Check::Source(System.Object)
extern "C"  void Check_Source_m4098695967 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.InvalidOperationException::.ctor()
extern "C"  void InvalidOperationException__ctor_m2734335978 (InvalidOperationException_t56020091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Check::SourceAndPredicate(System.Object,System.Object)
extern "C"  void Check_SourceAndPredicate_m2332465641 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___source0, Il2CppObject * ___predicate1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::GetEventChain(UnityEngine.GameObject,System.Collections.Generic.IList`1<UnityEngine.Transform>)
extern "C"  void ExecuteEvents_GetEventChain_m2404658789 (Il2CppObject * __this /* static, unused */, GameObject_t1113636619 * ___root0, Il2CppObject* ___eventChain1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Item(System.Int32)
#define List_1_get_Item_m3022113929(__this, p0, method) ((  Transform_t3600365921 * (*) (List_1_t777473367 *, int32_t, const MethodInfo*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Count()
#define List_1_get_Count_m3787308655(__this, method) ((  int32_t (*) (List_1_t777473367 *, const MethodInfo*))List_1_get_Count_m2934127733_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m3695543300_gshared (Dictionary_2_t1444694249 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t4218749037 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m3695543300_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t4218749037 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t1444694249 *)__this);
			((  void (*) (Dictionary_2_t1444694249 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t4218749037 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t1444694249 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t4218749037 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.DateTime>::Do_CopyTo<System.Collections.DictionaryEntry,System.Collections.DictionaryEntry>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t3123975638_TisDictionaryEntry_t3123975638_m3172029790_gshared (Dictionary_2_t790968773 * __this, DictionaryEntryU5BU5D_t4217117203* ___array0, int32_t ___index1, Transform_1_t2549852611 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		DictionaryEntryU5BU5D_t4217117203* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t2549852611 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		DateTimeU5BU5D_t1184652292* L_11 = (DateTimeU5BU5D_t1184652292*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		DateTime_t3738529785  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t2549852611 *)L_6);
		DictionaryEntry_t3123975638  L_15 = ((  DictionaryEntry_t3123975638  (*) (Transform_1_t2549852611 *, Il2CppObject *, DateTime_t3738529785 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t2549852611 *)L_6, (Il2CppObject *)L_10, (DateTime_t3738529785 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		DictionaryEntry_t3123975638  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (DictionaryEntry_t3123975638 )((*(DictionaryEntry_t3123975638 *)((DictionaryEntry_t3123975638 *)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.DateTime>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<System.Object,System.DateTime>,System.Collections.Generic.KeyValuePair`2<System.Object,System.DateTime>>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3188640940_TisKeyValuePair_2_t3188640940_m452765418_gshared (Dictionary_2_t790968773 * __this, KeyValuePair_2U5BU5D_t2753949477* ___array0, int32_t ___index1, Transform_1_t2614517913 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		KeyValuePair_2U5BU5D_t2753949477* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t2614517913 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		DateTimeU5BU5D_t1184652292* L_11 = (DateTimeU5BU5D_t1184652292*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		DateTime_t3738529785  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t2614517913 *)L_6);
		KeyValuePair_2_t3188640940  L_15 = ((  KeyValuePair_2_t3188640940  (*) (Transform_1_t2614517913 *, Il2CppObject *, DateTime_t3738529785 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t2614517913 *)L_6, (Il2CppObject *)L_10, (DateTime_t3738529785 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		KeyValuePair_2_t3188640940  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (KeyValuePair_2_t3188640940 )((*(KeyValuePair_2_t3188640940 *)((KeyValuePair_2_t3188640940 *)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.DateTime>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<System.Object,System.DateTime>,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3188640940_TisIl2CppObject_m435373975_gshared (Dictionary_2_t790968773 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t2614517913 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t2614517913 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		DateTimeU5BU5D_t1184652292* L_11 = (DateTimeU5BU5D_t1184652292*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		DateTime_t3738529785  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t2614517913 *)L_6);
		KeyValuePair_2_t3188640940  L_15 = ((  KeyValuePair_2_t3188640940  (*) (Transform_1_t2614517913 *, Il2CppObject *, DateTime_t3738529785 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t2614517913 *)L_6, (Il2CppObject *)L_10, (DateTime_t3738529785 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		KeyValuePair_2_t3188640940  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.DateTime>::Do_CopyTo<System.DateTime,System.DateTime>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisDateTime_t3738529785_TisDateTime_t3738529785_m993747734_gshared (Dictionary_2_t790968773 * __this, DateTimeU5BU5D_t1184652292* ___array0, int32_t ___index1, Transform_1_t3164406758 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		DateTimeU5BU5D_t1184652292* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t3164406758 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		DateTimeU5BU5D_t1184652292* L_11 = (DateTimeU5BU5D_t1184652292*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		DateTime_t3738529785  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t3164406758 *)L_6);
		DateTime_t3738529785  L_15 = ((  DateTime_t3738529785  (*) (Transform_1_t3164406758 *, Il2CppObject *, DateTime_t3738529785 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t3164406758 *)L_6, (Il2CppObject *)L_10, (DateTime_t3738529785 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		DateTime_t3738529785  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (DateTime_t3738529785 )((*(DateTime_t3738529785 *)((DateTime_t3738529785 *)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.DateTime>::Do_CopyTo<System.DateTime,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisDateTime_t3738529785_TisIl2CppObject_m2196239800_gshared (Dictionary_2_t790968773 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t3164406758 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t3164406758 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		DateTimeU5BU5D_t1184652292* L_11 = (DateTimeU5BU5D_t1184652292*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		DateTime_t3738529785  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t3164406758 *)L_6);
		DateTime_t3738529785  L_15 = ((  DateTime_t3738529785  (*) (Transform_1_t3164406758 *, Il2CppObject *, DateTime_t3738529785 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t3164406758 *)L_6, (Il2CppObject *)L_10, (DateTime_t3738529785 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		DateTime_t3738529785  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.DateTime>::Do_CopyTo<System.Object,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m1344156113_gshared (Dictionary_2_t790968773 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t2505983137 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t2505983137 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		DateTimeU5BU5D_t1184652292* L_11 = (DateTimeU5BU5D_t1184652292*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		DateTime_t3738529785  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t2505983137 *)L_6);
		Il2CppObject * L_15 = ((  Il2CppObject * (*) (Transform_1_t2505983137 *, Il2CppObject *, DateTime_t3738529785 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t2505983137 *)L_6, (Il2CppObject *)L_10, (DateTime_t3738529785 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_16 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_17 = V_0;
		int32_t L_18 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.DateTime>::Do_ICollectionCopyTo<System.Collections.Generic.KeyValuePair`2<System.Object,System.DateTime>>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3188640940_m904145848_gshared (Dictionary_2_t790968773 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t2614517913 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3188640940_m904145848_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t2614517913 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t790968773 *)__this);
			((  void (*) (Dictionary_2_t790968773 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t2614517913 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t790968773 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t2614517913 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.DateTime>::Do_ICollectionCopyTo<System.DateTime>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisDateTime_t3738529785_m742909906_gshared (Dictionary_2_t790968773 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t3164406758 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisDateTime_t3738529785_m742909906_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t3164406758 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t790968773 *)__this);
			((  void (*) (Dictionary_2_t790968773 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t3164406758 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t790968773 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t3164406758 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.DateTime>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m2559484862_gshared (Dictionary_2_t790968773 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t2505983137 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m2559484862_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t2505983137 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t790968773 *)__this);
			((  void (*) (Dictionary_2_t790968773 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t2505983137 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t790968773 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t2505983137 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Do_CopyTo<System.Collections.DictionaryEntry,System.Collections.DictionaryEntry>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t3123975638_TisDictionaryEntry_t3123975638_m3300127835_gshared (Dictionary_2_t3384741 * __this, DictionaryEntryU5BU5D_t4217117203* ___array0, int32_t ___index1, Transform_1_t1750446691 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		DictionaryEntryU5BU5D_t4217117203* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t1750446691 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		Int32U5BU5D_t385246372* L_11 = (Int32U5BU5D_t385246372*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t1750446691 *)L_6);
		DictionaryEntry_t3123975638  L_15 = ((  DictionaryEntry_t3123975638  (*) (Transform_1_t1750446691 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t1750446691 *)L_6, (Il2CppObject *)L_10, (int32_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		DictionaryEntry_t3123975638  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (DictionaryEntry_t3123975638 )((*(DictionaryEntry_t3123975638 *)((DictionaryEntry_t3123975638 *)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2401056908_TisKeyValuePair_2_t2401056908_m676905794_gshared (Dictionary_2_t3384741 * __this, KeyValuePair_2U5BU5D_t1954543557* ___array0, int32_t ___index1, Transform_1_t1027527961 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		KeyValuePair_2U5BU5D_t1954543557* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t1027527961 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		Int32U5BU5D_t385246372* L_11 = (Int32U5BU5D_t385246372*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t1027527961 *)L_6);
		KeyValuePair_2_t2401056908  L_15 = ((  KeyValuePair_2_t2401056908  (*) (Transform_1_t1027527961 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t1027527961 *)L_6, (Il2CppObject *)L_10, (int32_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		KeyValuePair_2_t2401056908  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (KeyValuePair_2_t2401056908 )((*(KeyValuePair_2_t2401056908 *)((KeyValuePair_2_t2401056908 *)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2401056908_TisIl2CppObject_m4084399341_gshared (Dictionary_2_t3384741 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t1027527961 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t1027527961 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		Int32U5BU5D_t385246372* L_11 = (Int32U5BU5D_t385246372*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t1027527961 *)L_6);
		KeyValuePair_2_t2401056908  L_15 = ((  KeyValuePair_2_t2401056908  (*) (Transform_1_t1027527961 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t1027527961 *)L_6, (Il2CppObject *)L_10, (int32_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		KeyValuePair_2_t2401056908  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Do_CopyTo<System.Int32,System.Int32>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisInt32_t2950945753_TisInt32_t2950945753_m3384108308_gshared (Dictionary_2_t3384741 * __this, Int32U5BU5D_t385246372* ___array0, int32_t ___index1, Transform_1_t1577416806 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		Int32U5BU5D_t385246372* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t1577416806 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		Int32U5BU5D_t385246372* L_11 = (Int32U5BU5D_t385246372*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t1577416806 *)L_6);
		int32_t L_15 = ((  int32_t (*) (Transform_1_t1577416806 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t1577416806 *)L_6, (Il2CppObject *)L_10, (int32_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Do_CopyTo<System.Int32,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisInt32_t2950945753_TisIl2CppObject_m3783191429_gshared (Dictionary_2_t3384741 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t1577416806 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t1577416806 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		Int32U5BU5D_t385246372* L_11 = (Int32U5BU5D_t385246372*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t1577416806 *)L_6);
		int32_t L_15 = ((  int32_t (*) (Transform_1_t1577416806 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t1577416806 *)L_6, (Il2CppObject *)L_10, (int32_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Do_CopyTo<System.Object,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m1607739207_gshared (Dictionary_2_t3384741 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t1706577217 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t1706577217 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		Int32U5BU5D_t385246372* L_11 = (Int32U5BU5D_t385246372*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t1706577217 *)L_6);
		Il2CppObject * L_15 = ((  Il2CppObject * (*) (Transform_1_t1706577217 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t1706577217 *)L_6, (Il2CppObject *)L_10, (int32_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_16 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_17 = V_0;
		int32_t L_18 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Do_ICollectionCopyTo<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2401056908_m1169495264_gshared (Dictionary_2_t3384741 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t1027527961 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2401056908_m1169495264_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t1027527961 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t3384741 *)__this);
			((  void (*) (Dictionary_2_t3384741 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t1027527961 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t3384741 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t1027527961 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Do_ICollectionCopyTo<System.Int32>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t2950945753_m1134171305_gshared (Dictionary_2_t3384741 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t1577416806 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisInt32_t2950945753_m1134171305_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t1577416806 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t3384741 *)__this);
			((  void (*) (Dictionary_2_t3384741 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t1577416806 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t3384741 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t1577416806 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m1362949338_gshared (Dictionary_2_t3384741 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t1706577217 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m1362949338_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t1706577217 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t3384741 *)__this);
			((  void (*) (Dictionary_2_t3384741 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t1706577217 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t3384741 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t1706577217 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Do_CopyTo<System.Collections.DictionaryEntry,System.Collections.DictionaryEntry>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t3123975638_TisDictionaryEntry_t3123975638_m3864993650_gshared (Dictionary_2_t132545152 * __this, DictionaryEntryU5BU5D_t4217117203* ___array0, int32_t ___index1, Transform_1_t4209139644 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		DictionaryEntryU5BU5D_t4217117203* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t4209139644 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		ObjectU5BU5D_t2843939325* L_11 = (ObjectU5BU5D_t2843939325*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t4209139644 *)L_6);
		DictionaryEntry_t3123975638  L_15 = ((  DictionaryEntry_t3123975638  (*) (Transform_1_t4209139644 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t4209139644 *)L_6, (Il2CppObject *)L_10, (Il2CppObject *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		DictionaryEntry_t3123975638  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (DictionaryEntry_t3123975638 )((*(DictionaryEntry_t3123975638 *)((DictionaryEntry_t3123975638 *)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2530217319_TisKeyValuePair_2_t2530217319_m985448706_gshared (Dictionary_2_t132545152 * __this, KeyValuePair_2U5BU5D_t118269214* ___array0, int32_t ___index1, Transform_1_t3615381325 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		KeyValuePair_2U5BU5D_t118269214* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t3615381325 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		ObjectU5BU5D_t2843939325* L_11 = (ObjectU5BU5D_t2843939325*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t3615381325 *)L_6);
		KeyValuePair_2_t2530217319  L_15 = ((  KeyValuePair_2_t2530217319  (*) (Transform_1_t3615381325 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t3615381325 *)L_6, (Il2CppObject *)L_10, (Il2CppObject *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		KeyValuePair_2_t2530217319  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (KeyValuePair_2_t2530217319 )((*(KeyValuePair_2_t2530217319 *)((KeyValuePair_2_t2530217319 *)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2530217319_TisIl2CppObject_m311023789_gshared (Dictionary_2_t132545152 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t3615381325 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t3615381325 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		ObjectU5BU5D_t2843939325* L_11 = (ObjectU5BU5D_t2843939325*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t3615381325 *)L_6);
		KeyValuePair_2_t2530217319  L_15 = ((  KeyValuePair_2_t2530217319  (*) (Transform_1_t3615381325 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t3615381325 *)L_6, (Il2CppObject *)L_10, (Il2CppObject *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		KeyValuePair_2_t2530217319  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Do_CopyTo<System.Object,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m976542334_gshared (Dictionary_2_t132545152 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t4165270170 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t4165270170 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		ObjectU5BU5D_t2843939325* L_11 = (ObjectU5BU5D_t2843939325*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t4165270170 *)L_6);
		Il2CppObject * L_15 = ((  Il2CppObject * (*) (Transform_1_t4165270170 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t4165270170 *)L_6, (Il2CppObject *)L_10, (Il2CppObject *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_16 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_17 = V_0;
		int32_t L_18 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Do_ICollectionCopyTo<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2530217319_m1439704807_gshared (Dictionary_2_t132545152 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t3615381325 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2530217319_m1439704807_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t3615381325 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t132545152 *)__this);
			((  void (*) (Dictionary_2_t132545152 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t3615381325 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t132545152 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t3615381325 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m3942192587_gshared (Dictionary_2_t132545152 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t4165270170 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m3942192587_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t4165270170 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t132545152 *)__this);
			((  void (*) (Dictionary_2_t132545152 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t4165270170 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t132545152 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t4165270170 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Do_CopyTo<System.Collections.DictionaryEntry,System.Collections.DictionaryEntry>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t3123975638_TisDictionaryEntry_t3123975638_m1593219116_gshared (Dictionary_2_t3525131242 * __this, DictionaryEntryU5BU5D_t4217117203* ___array0, int32_t ___index1, Transform_1_t396552554 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		DictionaryEntryU5BU5D_t4217117203* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t396552554 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		UInt16U5BU5D_t3326319531* L_11 = (UInt16U5BU5D_t3326319531*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		uint16_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t396552554 *)L_6);
		DictionaryEntry_t3123975638  L_15 = ((  DictionaryEntry_t3123975638  (*) (Transform_1_t396552554 *, Il2CppObject *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t396552554 *)L_6, (Il2CppObject *)L_10, (uint16_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		DictionaryEntry_t3123975638  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (DictionaryEntry_t3123975638 )((*(DictionaryEntry_t3123975638 *)((DictionaryEntry_t3123975638 *)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>,System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t1627836113_TisKeyValuePair_2_t1627836113_m2956160249_gshared (Dictionary_2_t3525131242 * __this, KeyValuePair_2U5BU5D_t600649420* ___array0, int32_t ___index1, Transform_1_t3195380325 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		KeyValuePair_2U5BU5D_t600649420* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t3195380325 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		UInt16U5BU5D_t3326319531* L_11 = (UInt16U5BU5D_t3326319531*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		uint16_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t3195380325 *)L_6);
		KeyValuePair_2_t1627836113  L_15 = ((  KeyValuePair_2_t1627836113  (*) (Transform_1_t3195380325 *, Il2CppObject *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t3195380325 *)L_6, (Il2CppObject *)L_10, (uint16_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		KeyValuePair_2_t1627836113  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (KeyValuePair_2_t1627836113 )((*(KeyValuePair_2_t1627836113 *)((KeyValuePair_2_t1627836113 *)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t1627836113_TisIl2CppObject_m3890572092_gshared (Dictionary_2_t3525131242 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t3195380325 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t3195380325 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		UInt16U5BU5D_t3326319531* L_11 = (UInt16U5BU5D_t3326319531*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		uint16_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t3195380325 *)L_6);
		KeyValuePair_2_t1627836113  L_15 = ((  KeyValuePair_2_t1627836113  (*) (Transform_1_t3195380325 *, Il2CppObject *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t3195380325 *)L_6, (Il2CppObject *)L_10, (uint16_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		KeyValuePair_2_t1627836113  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Do_CopyTo<System.Object,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m2542818225_gshared (Dictionary_2_t3525131242 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t352683080 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t352683080 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		UInt16U5BU5D_t3326319531* L_11 = (UInt16U5BU5D_t3326319531*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		uint16_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t352683080 *)L_6);
		Il2CppObject * L_15 = ((  Il2CppObject * (*) (Transform_1_t352683080 *, Il2CppObject *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t352683080 *)L_6, (Il2CppObject *)L_10, (uint16_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_16 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_17 = V_0;
		int32_t L_18 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Do_CopyTo<System.UInt16,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisUInt16_t2177724958_TisIl2CppObject_m4118182932_gshared (Dictionary_2_t3525131242 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t3745269170 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t3745269170 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		UInt16U5BU5D_t3326319531* L_11 = (UInt16U5BU5D_t3326319531*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		uint16_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t3745269170 *)L_6);
		uint16_t L_15 = ((  uint16_t (*) (Transform_1_t3745269170 *, Il2CppObject *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t3745269170 *)L_6, (Il2CppObject *)L_10, (uint16_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		uint16_t L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Do_CopyTo<System.UInt16,System.UInt16>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisUInt16_t2177724958_TisUInt16_t2177724958_m788912763_gshared (Dictionary_2_t3525131242 * __this, UInt16U5BU5D_t3326319531* ___array0, int32_t ___index1, Transform_1_t3745269170 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		UInt16U5BU5D_t3326319531* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t3745269170 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		UInt16U5BU5D_t3326319531* L_11 = (UInt16U5BU5D_t3326319531*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		uint16_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t3745269170 *)L_6);
		uint16_t L_15 = ((  uint16_t (*) (Transform_1_t3745269170 *, Il2CppObject *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t3745269170 *)L_6, (Il2CppObject *)L_10, (uint16_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		uint16_t L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (uint16_t)((*(uint16_t*)((uint16_t*)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Do_ICollectionCopyTo<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t1627836113_m1280446632_gshared (Dictionary_2_t3525131242 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t3195380325 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t1627836113_m1280446632_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t3195380325 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t3525131242 *)__this);
			((  void (*) (Dictionary_2_t3525131242 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t3195380325 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t3525131242 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t3195380325 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m3278344529_gshared (Dictionary_2_t3525131242 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t352683080 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m3278344529_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t352683080 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t3525131242 *)__this);
			((  void (*) (Dictionary_2_t3525131242 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t352683080 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t3525131242 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t352683080 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Do_ICollectionCopyTo<System.UInt16>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisUInt16_t2177724958_m902960032_gshared (Dictionary_2_t3525131242 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t3745269170 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisUInt16_t2177724958_m902960032_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t3745269170 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t3525131242 *)__this);
			((  void (*) (Dictionary_2_t3525131242 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t3745269170 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t3525131242 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t3745269170 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Do_CopyTo<System.Collections.DictionaryEntry,System.Collections.DictionaryEntry>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t3123975638_TisDictionaryEntry_t3123975638_m1880746376_gshared (Dictionary_2_t3274888882 * __this, DictionaryEntryU5BU5D_t4217117203* ___array0, int32_t ___index1, Transform_1_t140567170 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		DictionaryEntryU5BU5D_t4217117203* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t140567170 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		TextEditOpU5BU5D_t3070334147* L_11 = (TextEditOpU5BU5D_t3070334147*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t140567170 *)L_6);
		DictionaryEntry_t3123975638  L_15 = ((  DictionaryEntry_t3123975638  (*) (Transform_1_t140567170 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t140567170 *)L_6, (Il2CppObject *)L_10, (int32_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		DictionaryEntry_t3123975638  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (DictionaryEntry_t3123975638 )((*(DictionaryEntry_t3123975638 *)((DictionaryEntry_t3123975638 *)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>,System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t1377593753_TisKeyValuePair_2_t1377593753_m4179717964_gshared (Dictionary_2_t3274888882 * __this, KeyValuePair_2U5BU5D_t344664036* ___array0, int32_t ___index1, Transform_1_t2689152581 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		KeyValuePair_2U5BU5D_t344664036* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t2689152581 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		TextEditOpU5BU5D_t3070334147* L_11 = (TextEditOpU5BU5D_t3070334147*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t2689152581 *)L_6);
		KeyValuePair_2_t1377593753  L_15 = ((  KeyValuePair_2_t1377593753  (*) (Transform_1_t2689152581 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t2689152581 *)L_6, (Il2CppObject *)L_10, (int32_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		KeyValuePair_2_t1377593753  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (KeyValuePair_2_t1377593753 )((*(KeyValuePair_2_t1377593753 *)((KeyValuePair_2_t1377593753 *)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t1377593753_TisIl2CppObject_m2614136613_gshared (Dictionary_2_t3274888882 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t2689152581 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t2689152581 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		TextEditOpU5BU5D_t3070334147* L_11 = (TextEditOpU5BU5D_t3070334147*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t2689152581 *)L_6);
		KeyValuePair_2_t1377593753  L_15 = ((  KeyValuePair_2_t1377593753  (*) (Transform_1_t2689152581 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t2689152581 *)L_6, (Il2CppObject *)L_10, (int32_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		KeyValuePair_2_t1377593753  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Do_CopyTo<System.Object,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m967603034_gshared (Dictionary_2_t3274888882 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t96697696 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t96697696 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		TextEditOpU5BU5D_t3070334147* L_11 = (TextEditOpU5BU5D_t3070334147*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t96697696 *)L_6);
		Il2CppObject * L_15 = ((  Il2CppObject * (*) (Transform_1_t96697696 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t96697696 *)L_6, (Il2CppObject *)L_10, (int32_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_16 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_17 = V_0;
		int32_t L_18 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Do_CopyTo<UnityEngine.TextEditor/TextEditOp,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisTextEditOp_t1927482598_TisIl2CppObject_m2764429702_gshared (Dictionary_2_t3274888882 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t3239041426 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t3239041426 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		TextEditOpU5BU5D_t3070334147* L_11 = (TextEditOpU5BU5D_t3070334147*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t3239041426 *)L_6);
		int32_t L_15 = ((  int32_t (*) (Transform_1_t3239041426 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t3239041426 *)L_6, (Il2CppObject *)L_10, (int32_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Do_CopyTo<UnityEngine.TextEditor/TextEditOp,UnityEngine.TextEditor/TextEditOp>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisTextEditOp_t1927482598_TisTextEditOp_t1927482598_m2365000843_gshared (Dictionary_2_t3274888882 * __this, TextEditOpU5BU5D_t3070334147* ___array0, int32_t ___index1, Transform_1_t3239041426 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		TextEditOpU5BU5D_t3070334147* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t3239041426 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		TextEditOpU5BU5D_t3070334147* L_11 = (TextEditOpU5BU5D_t3070334147*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t3239041426 *)L_6);
		int32_t L_15 = ((  int32_t (*) (Transform_1_t3239041426 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t3239041426 *)L_6, (Il2CppObject *)L_10, (int32_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Do_ICollectionCopyTo<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t1377593753_m4023145528_gshared (Dictionary_2_t3274888882 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t2689152581 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t1377593753_m4023145528_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t2689152581 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t3274888882 *)__this);
			((  void (*) (Dictionary_2_t3274888882 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t2689152581 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t3274888882 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t2689152581 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m141062928_gshared (Dictionary_2_t3274888882 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t96697696 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m141062928_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t96697696 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t3274888882 *)__this);
			((  void (*) (Dictionary_2_t3274888882 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t96697696 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t3274888882 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t96697696 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Do_ICollectionCopyTo<UnityEngine.TextEditor/TextEditOp>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisTextEditOp_t1927482598_m1340764829_gshared (Dictionary_2_t3274888882 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t3239041426 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisTextEditOp_t1927482598_m1340764829_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t3239041426 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t3274888882 *)__this);
			((  void (*) (Dictionary_2_t3274888882 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t3239041426 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t3274888882 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t3239041426 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Do_CopyTo<System.Collections.DictionaryEntry,System.Collections.DictionaryEntry>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t3123975638_TisDictionaryEntry_t3123975638_m3966685699_gshared (Dictionary_2_t571830913 * __this, DictionaryEntryU5BU5D_t4217117203* ___array0, int32_t ___index1, Transform_1_t1727736471 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		DictionaryEntryU5BU5D_t4217117203* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t1727736471 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		ProfileDataU5BU5D_t362536152* L_11 = (ProfileDataU5BU5D_t362536152*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		ProfileData_t3519391925  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t1727736471 *)L_6);
		DictionaryEntry_t3123975638  L_15 = ((  DictionaryEntry_t3123975638  (*) (Transform_1_t1727736471 *, Il2CppObject *, ProfileData_t3519391925 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t1727736471 *)L_6, (Il2CppObject *)L_10, (ProfileData_t3519391925 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		DictionaryEntry_t3123975638  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (DictionaryEntry_t3123975638 )((*(DictionaryEntry_t3123975638 *)((DictionaryEntry_t3123975638 *)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2969503080_TisKeyValuePair_2_t2969503080_m2000208616_gshared (Dictionary_2_t571830913 * __this, KeyValuePair_2U5BU5D_t1931833337* ___array0, int32_t ___index1, Transform_1_t1573263913 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		KeyValuePair_2U5BU5D_t1931833337* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t1573263913 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		ProfileDataU5BU5D_t362536152* L_11 = (ProfileDataU5BU5D_t362536152*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		ProfileData_t3519391925  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t1573263913 *)L_6);
		KeyValuePair_2_t2969503080  L_15 = ((  KeyValuePair_2_t2969503080  (*) (Transform_1_t1573263913 *, Il2CppObject *, ProfileData_t3519391925 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t1573263913 *)L_6, (Il2CppObject *)L_10, (ProfileData_t3519391925 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		KeyValuePair_2_t2969503080  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (KeyValuePair_2_t2969503080 )((*(KeyValuePair_2_t2969503080 *)((KeyValuePair_2_t2969503080 *)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2969503080_TisIl2CppObject_m3373930730_gshared (Dictionary_2_t571830913 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t1573263913 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t1573263913 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		ProfileDataU5BU5D_t362536152* L_11 = (ProfileDataU5BU5D_t362536152*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		ProfileData_t3519391925  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t1573263913 *)L_6);
		KeyValuePair_2_t2969503080  L_15 = ((  KeyValuePair_2_t2969503080  (*) (Transform_1_t1573263913 *, Il2CppObject *, ProfileData_t3519391925 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t1573263913 *)L_6, (Il2CppObject *)L_10, (ProfileData_t3519391925 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		KeyValuePair_2_t2969503080  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Do_CopyTo<System.Object,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m2379530792_gshared (Dictionary_2_t571830913 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t1683866997 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t1683866997 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		ProfileDataU5BU5D_t362536152* L_11 = (ProfileDataU5BU5D_t362536152*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		ProfileData_t3519391925  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t1683866997 *)L_6);
		Il2CppObject * L_15 = ((  Il2CppObject * (*) (Transform_1_t1683866997 *, Il2CppObject *, ProfileData_t3519391925 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t1683866997 *)L_6, (Il2CppObject *)L_10, (ProfileData_t3519391925 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_16 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_17 = V_0;
		int32_t L_18 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Do_CopyTo<Vuforia.WebCamProfile/ProfileData,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisProfileData_t3519391925_TisIl2CppObject_m2798843146_gshared (Dictionary_2_t571830913 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t2123152758 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t2123152758 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		ProfileDataU5BU5D_t362536152* L_11 = (ProfileDataU5BU5D_t362536152*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		ProfileData_t3519391925  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t2123152758 *)L_6);
		ProfileData_t3519391925  L_15 = ((  ProfileData_t3519391925  (*) (Transform_1_t2123152758 *, Il2CppObject *, ProfileData_t3519391925 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t2123152758 *)L_6, (Il2CppObject *)L_10, (ProfileData_t3519391925 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		ProfileData_t3519391925  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Do_CopyTo<Vuforia.WebCamProfile/ProfileData,Vuforia.WebCamProfile/ProfileData>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisProfileData_t3519391925_TisProfileData_t3519391925_m4253905941_gshared (Dictionary_2_t571830913 * __this, ProfileDataU5BU5D_t362536152* ___array0, int32_t ___index1, Transform_1_t2123152758 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ProfileDataU5BU5D_t362536152* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t2123152758 * L_6 = ___transform2;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		ProfileDataU5BU5D_t362536152* L_11 = (ProfileDataU5BU5D_t362536152*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		ProfileData_t3519391925  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t2123152758 *)L_6);
		ProfileData_t3519391925  L_15 = ((  ProfileData_t3519391925  (*) (Transform_1_t2123152758 *, Il2CppObject *, ProfileData_t3519391925 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t2123152758 *)L_6, (Il2CppObject *)L_10, (ProfileData_t3519391925 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		ProfileData_t3519391925  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (ProfileData_t3519391925 )((*(ProfileData_t3519391925 *)((ProfileData_t3519391925 *)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Do_ICollectionCopyTo<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2969503080_m4178501938_gshared (Dictionary_2_t571830913 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t1573263913 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2969503080_m4178501938_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t1573263913 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t571830913 *)__this);
			((  void (*) (Dictionary_2_t571830913 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t1573263913 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t571830913 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t1573263913 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m1055602370_gshared (Dictionary_2_t571830913 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t1683866997 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m1055602370_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t1683866997 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t571830913 *)__this);
			((  void (*) (Dictionary_2_t571830913 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t1683866997 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t571830913 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t1683866997 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Do_ICollectionCopyTo<Vuforia.WebCamProfile/ProfileData>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisProfileData_t3519391925_m816695391_gshared (Dictionary_2_t571830913 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t2123152758 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisProfileData_t3519391925_m816695391_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t2123152758 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t571830913 *)__this);
			((  void (*) (Dictionary_2_t571830913 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t2123152758 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t571830913 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t2123152758 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::Do_CopyTo<System.Collections.DictionaryEntry,System.Collections.DictionaryEntry>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t3123975638_TisDictionaryEntry_t3123975638_m1005785837_gshared (Dictionary_2_t1670703453 * __this, DictionaryEntryU5BU5D_t4217117203* ___array0, int32_t ___index1, Transform_1_t991236299 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		DictionaryEntryU5BU5D_t4217117203* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t991236299 * L_6 = ___transform2;
		PIXEL_FORMATU5BU5D_t87130330* L_7 = (PIXEL_FORMATU5BU5D_t87130330*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		ObjectU5BU5D_t2843939325* L_11 = (ObjectU5BU5D_t2843939325*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t991236299 *)L_6);
		DictionaryEntry_t3123975638  L_15 = ((  DictionaryEntry_t3123975638  (*) (Transform_1_t991236299 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t991236299 *)L_6, (int32_t)L_10, (Il2CppObject *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		DictionaryEntry_t3123975638  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (DictionaryEntry_t3123975638 )((*(DictionaryEntry_t3123975638 *)((DictionaryEntry_t3123975638 *)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>,System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t4068375620_TisKeyValuePair_2_t4068375620_m178129099_gshared (Dictionary_2_t1670703453 * __this, KeyValuePair_2U5BU5D_t1195333165* ___array0, int32_t ___index1, Transform_1_t1935636281 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		KeyValuePair_2U5BU5D_t1195333165* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t1935636281 * L_6 = ___transform2;
		PIXEL_FORMATU5BU5D_t87130330* L_7 = (PIXEL_FORMATU5BU5D_t87130330*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		ObjectU5BU5D_t2843939325* L_11 = (ObjectU5BU5D_t2843939325*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t1935636281 *)L_6);
		KeyValuePair_2_t4068375620  L_15 = ((  KeyValuePair_2_t4068375620  (*) (Transform_1_t1935636281 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t1935636281 *)L_6, (int32_t)L_10, (Il2CppObject *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		KeyValuePair_2_t4068375620  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (KeyValuePair_2_t4068375620 )((*(KeyValuePair_2_t4068375620 *)((KeyValuePair_2_t4068375620 *)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t4068375620_TisIl2CppObject_m813293398_gshared (Dictionary_2_t1670703453 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t1935636281 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t1935636281 * L_6 = ___transform2;
		PIXEL_FORMATU5BU5D_t87130330* L_7 = (PIXEL_FORMATU5BU5D_t87130330*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		ObjectU5BU5D_t2843939325* L_11 = (ObjectU5BU5D_t2843939325*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t1935636281 *)L_6);
		KeyValuePair_2_t4068375620  L_15 = ((  KeyValuePair_2_t4068375620  (*) (Transform_1_t1935636281 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t1935636281 *)L_6, (int32_t)L_10, (Il2CppObject *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		KeyValuePair_2_t4068375620  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::Do_CopyTo<System.Object,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m1947892747_gshared (Dictionary_2_t1670703453 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t947366825 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t947366825 * L_6 = ___transform2;
		PIXEL_FORMATU5BU5D_t87130330* L_7 = (PIXEL_FORMATU5BU5D_t87130330*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		ObjectU5BU5D_t2843939325* L_11 = (ObjectU5BU5D_t2843939325*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t947366825 *)L_6);
		Il2CppObject * L_15 = ((  Il2CppObject * (*) (Transform_1_t947366825 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t947366825 *)L_6, (int32_t)L_10, (Il2CppObject *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_16 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_17 = V_0;
		int32_t L_18 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::Do_CopyTo<Vuforia.Image/PIXEL_FORMAT,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisPIXEL_FORMAT_t3209881435_TisIl2CppObject_m2519921458_gshared (Dictionary_2_t1670703453 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t1077142096 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t1077142096 * L_6 = ___transform2;
		PIXEL_FORMATU5BU5D_t87130330* L_7 = (PIXEL_FORMATU5BU5D_t87130330*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		ObjectU5BU5D_t2843939325* L_11 = (ObjectU5BU5D_t2843939325*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t1077142096 *)L_6);
		int32_t L_15 = ((  int32_t (*) (Transform_1_t1077142096 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t1077142096 *)L_6, (int32_t)L_10, (Il2CppObject *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::Do_CopyTo<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image/PIXEL_FORMAT>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisPIXEL_FORMAT_t3209881435_TisPIXEL_FORMAT_t3209881435_m4177618005_gshared (Dictionary_2_t1670703453 * __this, PIXEL_FORMATU5BU5D_t87130330* ___array0, int32_t ___index1, Transform_1_t1077142096 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		PIXEL_FORMATU5BU5D_t87130330* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t1077142096 * L_6 = ___transform2;
		PIXEL_FORMATU5BU5D_t87130330* L_7 = (PIXEL_FORMATU5BU5D_t87130330*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		ObjectU5BU5D_t2843939325* L_11 = (ObjectU5BU5D_t2843939325*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t1077142096 *)L_6);
		int32_t L_15 = ((  int32_t (*) (Transform_1_t1077142096 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t1077142096 *)L_6, (int32_t)L_10, (Il2CppObject *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::Do_ICollectionCopyTo<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t4068375620_m2332577487_gshared (Dictionary_2_t1670703453 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t1935636281 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t4068375620_m2332577487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t1935636281 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t1670703453 *)__this);
			((  void (*) (Dictionary_2_t1670703453 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t1935636281 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t1670703453 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t1935636281 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m4257328066_gshared (Dictionary_2_t1670703453 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t947366825 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m4257328066_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t947366825 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t1670703453 *)__this);
			((  void (*) (Dictionary_2_t1670703453 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t947366825 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t1670703453 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t947366825 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::Do_ICollectionCopyTo<Vuforia.Image/PIXEL_FORMAT>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisPIXEL_FORMAT_t3209881435_m3459464361_gshared (Dictionary_2_t1670703453 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t1077142096 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisPIXEL_FORMAT_t3209881435_m3459464361_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t1077142096 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t1670703453 *)__this);
			((  void (*) (Dictionary_2_t1670703453 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t1077142096 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t1670703453 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t1077142096 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::Do_CopyTo<System.Collections.DictionaryEntry,System.Collections.DictionaryEntry>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t3123975638_TisDictionaryEntry_t3123975638_m3881532964_gshared (Dictionary_2_t1160396953 * __this, DictionaryEntryU5BU5D_t4217117203* ___array0, int32_t ___index1, Transform_1_t2007148831 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		DictionaryEntryU5BU5D_t4217117203* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t2007148831 * L_6 = ___transform2;
		TrackableIdPairU5BU5D_t475764036* L_7 = (TrackableIdPairU5BU5D_t475764036*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		TrackableIdPair_t4227350457  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		PoseAgeEntryU5BU5D_t3970047907* L_11 = (PoseAgeEntryU5BU5D_t3970047907*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		PoseAgeEntry_t2181165958  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t2007148831 *)L_6);
		DictionaryEntry_t3123975638  L_15 = ((  DictionaryEntry_t3123975638  (*) (Transform_1_t2007148831 *, TrackableIdPair_t4227350457 , PoseAgeEntry_t2181165958 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t2007148831 *)L_6, (TrackableIdPair_t4227350457 )L_10, (PoseAgeEntry_t2181165958 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		DictionaryEntry_t3123975638  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (DictionaryEntry_t3123975638 )((*(DictionaryEntry_t3123975638 *)((DictionaryEntry_t3123975638 *)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>,System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3558069120_TisKeyValuePair_2_t3558069120_m533313562_gshared (Dictionary_2_t1160396953 * __this, KeyValuePair_2U5BU5D_t2211245697* ___array0, int32_t ___index1, Transform_1_t2441242313 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		KeyValuePair_2U5BU5D_t2211245697* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t2441242313 * L_6 = ___transform2;
		TrackableIdPairU5BU5D_t475764036* L_7 = (TrackableIdPairU5BU5D_t475764036*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		TrackableIdPair_t4227350457  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		PoseAgeEntryU5BU5D_t3970047907* L_11 = (PoseAgeEntryU5BU5D_t3970047907*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		PoseAgeEntry_t2181165958  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t2441242313 *)L_6);
		KeyValuePair_2_t3558069120  L_15 = ((  KeyValuePair_2_t3558069120  (*) (Transform_1_t2441242313 *, TrackableIdPair_t4227350457 , PoseAgeEntry_t2181165958 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t2441242313 *)L_6, (TrackableIdPair_t4227350457 )L_10, (PoseAgeEntry_t2181165958 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		KeyValuePair_2_t3558069120  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (KeyValuePair_2_t3558069120 )((*(KeyValuePair_2_t3558069120 *)((KeyValuePair_2_t3558069120 *)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3558069120_TisIl2CppObject_m1657117623_gshared (Dictionary_2_t1160396953 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t2441242313 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t2441242313 * L_6 = ___transform2;
		TrackableIdPairU5BU5D_t475764036* L_7 = (TrackableIdPairU5BU5D_t475764036*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		TrackableIdPair_t4227350457  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		PoseAgeEntryU5BU5D_t3970047907* L_11 = (PoseAgeEntryU5BU5D_t3970047907*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		PoseAgeEntry_t2181165958  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t2441242313 *)L_6);
		KeyValuePair_2_t3558069120  L_15 = ((  KeyValuePair_2_t3558069120  (*) (Transform_1_t2441242313 *, TrackableIdPair_t4227350457 , PoseAgeEntry_t2181165958 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t2441242313 *)L_6, (TrackableIdPair_t4227350457 )L_10, (PoseAgeEntry_t2181165958 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		KeyValuePair_2_t3558069120  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::Do_CopyTo<Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisPoseAgeEntry_t2181165958_TisIl2CppObject_m4150342050_gshared (Dictionary_2_t1160396953 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t1064339151 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t1064339151 * L_6 = ___transform2;
		TrackableIdPairU5BU5D_t475764036* L_7 = (TrackableIdPairU5BU5D_t475764036*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		TrackableIdPair_t4227350457  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		PoseAgeEntryU5BU5D_t3970047907* L_11 = (PoseAgeEntryU5BU5D_t3970047907*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		PoseAgeEntry_t2181165958  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t1064339151 *)L_6);
		PoseAgeEntry_t2181165958  L_15 = ((  PoseAgeEntry_t2181165958  (*) (Transform_1_t1064339151 *, TrackableIdPair_t4227350457 , PoseAgeEntry_t2181165958 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t1064339151 *)L_6, (TrackableIdPair_t4227350457 )L_10, (PoseAgeEntry_t2181165958 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		PoseAgeEntry_t2181165958  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::Do_CopyTo<Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisPoseAgeEntry_t2181165958_TisPoseAgeEntry_t2181165958_m44019566_gshared (Dictionary_2_t1160396953 * __this, PoseAgeEntryU5BU5D_t3970047907* ___array0, int32_t ___index1, Transform_1_t1064339151 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		PoseAgeEntryU5BU5D_t3970047907* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t1064339151 * L_6 = ___transform2;
		TrackableIdPairU5BU5D_t475764036* L_7 = (TrackableIdPairU5BU5D_t475764036*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		TrackableIdPair_t4227350457  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		PoseAgeEntryU5BU5D_t3970047907* L_11 = (PoseAgeEntryU5BU5D_t3970047907*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		PoseAgeEntry_t2181165958  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t1064339151 *)L_6);
		PoseAgeEntry_t2181165958  L_15 = ((  PoseAgeEntry_t2181165958  (*) (Transform_1_t1064339151 *, TrackableIdPair_t4227350457 , PoseAgeEntry_t2181165958 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t1064339151 *)L_6, (TrackableIdPair_t4227350457 )L_10, (PoseAgeEntry_t2181165958 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		PoseAgeEntry_t2181165958  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (PoseAgeEntry_t2181165958 )((*(PoseAgeEntry_t2181165958 *)((PoseAgeEntry_t2181165958 *)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::Do_CopyTo<Vuforia.VuforiaManager/TrackableIdPair,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisTrackableIdPair_t4227350457_TisIl2CppObject_m4041055443_gshared (Dictionary_2_t1160396953 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t3110523650 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t3110523650 * L_6 = ___transform2;
		TrackableIdPairU5BU5D_t475764036* L_7 = (TrackableIdPairU5BU5D_t475764036*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		TrackableIdPair_t4227350457  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		PoseAgeEntryU5BU5D_t3970047907* L_11 = (PoseAgeEntryU5BU5D_t3970047907*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		PoseAgeEntry_t2181165958  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t3110523650 *)L_6);
		TrackableIdPair_t4227350457  L_15 = ((  TrackableIdPair_t4227350457  (*) (Transform_1_t3110523650 *, TrackableIdPair_t4227350457 , PoseAgeEntry_t2181165958 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t3110523650 *)L_6, (TrackableIdPair_t4227350457 )L_10, (PoseAgeEntry_t2181165958 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		TrackableIdPair_t4227350457  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::Do_CopyTo<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.VuforiaManager/TrackableIdPair>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisTrackableIdPair_t4227350457_TisTrackableIdPair_t4227350457_m4183888959_gshared (Dictionary_2_t1160396953 * __this, TrackableIdPairU5BU5D_t475764036* ___array0, int32_t ___index1, Transform_1_t3110523650 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		TrackableIdPairU5BU5D_t475764036* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t3110523650 * L_6 = ___transform2;
		TrackableIdPairU5BU5D_t475764036* L_7 = (TrackableIdPairU5BU5D_t475764036*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		TrackableIdPair_t4227350457  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		PoseAgeEntryU5BU5D_t3970047907* L_11 = (PoseAgeEntryU5BU5D_t3970047907*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		PoseAgeEntry_t2181165958  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t3110523650 *)L_6);
		TrackableIdPair_t4227350457  L_15 = ((  TrackableIdPair_t4227350457  (*) (Transform_1_t3110523650 *, TrackableIdPair_t4227350457 , PoseAgeEntry_t2181165958 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t3110523650 *)L_6, (TrackableIdPair_t4227350457 )L_10, (PoseAgeEntry_t2181165958 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		TrackableIdPair_t4227350457  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (TrackableIdPair_t4227350457 )((*(TrackableIdPair_t4227350457 *)((TrackableIdPair_t4227350457 *)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::Do_ICollectionCopyTo<System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3558069120_m3124385426_gshared (Dictionary_2_t1160396953 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t2441242313 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3558069120_m3124385426_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t2441242313 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t1160396953 *)__this);
			((  void (*) (Dictionary_2_t1160396953 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t2441242313 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t1160396953 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t2441242313 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::Do_ICollectionCopyTo<Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisPoseAgeEntry_t2181165958_m2559446055_gshared (Dictionary_2_t1160396953 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t1064339151 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisPoseAgeEntry_t2181165958_m2559446055_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t1064339151 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t1160396953 *)__this);
			((  void (*) (Dictionary_2_t1160396953 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t1064339151 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t1160396953 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t1064339151 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::Do_ICollectionCopyTo<Vuforia.VuforiaManager/TrackableIdPair>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisTrackableIdPair_t4227350457_m667701602_gshared (Dictionary_2_t1160396953 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t3110523650 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisTrackableIdPair_t4227350457_m667701602_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t3110523650 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t1160396953 *)__this);
			((  void (*) (Dictionary_2_t1160396953 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t3110523650 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t1160396953 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t3110523650 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::Do_CopyTo<System.Collections.DictionaryEntry,System.Collections.DictionaryEntry>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t3123975638_TisDictionaryEntry_t3123975638_m1510739758_gshared (Dictionary_2_t591960174 * __this, DictionaryEntryU5BU5D_t4217117203* ___array0, int32_t ___index1, Transform_1_t3241055830 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		DictionaryEntryU5BU5D_t4217117203* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t3241055830 * L_6 = ___transform2;
		TrackableIdPairU5BU5D_t475764036* L_7 = (TrackableIdPairU5BU5D_t475764036*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		TrackableIdPair_t4227350457  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		PoseInfoU5BU5D_t908987610* L_11 = (PoseInfoU5BU5D_t908987610*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		PoseInfo_t1612729179  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t3241055830 *)L_6);
		DictionaryEntry_t3123975638  L_15 = ((  DictionaryEntry_t3123975638  (*) (Transform_1_t3241055830 *, TrackableIdPair_t4227350457 , PoseInfo_t1612729179 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t3241055830 *)L_6, (TrackableIdPair_t4227350457 )L_10, (PoseInfo_t1612729179 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		DictionaryEntry_t3123975638  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (DictionaryEntry_t3123975638 )((*(DictionaryEntry_t3123975638 *)((DictionaryEntry_t3123975638 *)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>,System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2989632341_TisKeyValuePair_2_t2989632341_m1492645954_gshared (Dictionary_2_t591960174 * __this, KeyValuePair_2U5BU5D_t3445152696* ___array0, int32_t ___index1, Transform_1_t3106712533 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		KeyValuePair_2U5BU5D_t3445152696* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t3106712533 * L_6 = ___transform2;
		TrackableIdPairU5BU5D_t475764036* L_7 = (TrackableIdPairU5BU5D_t475764036*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		TrackableIdPair_t4227350457  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		PoseInfoU5BU5D_t908987610* L_11 = (PoseInfoU5BU5D_t908987610*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		PoseInfo_t1612729179  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t3106712533 *)L_6);
		KeyValuePair_2_t2989632341  L_15 = ((  KeyValuePair_2_t2989632341  (*) (Transform_1_t3106712533 *, TrackableIdPair_t4227350457 , PoseInfo_t1612729179 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t3106712533 *)L_6, (TrackableIdPair_t4227350457 )L_10, (PoseInfo_t1612729179 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		KeyValuePair_2_t2989632341  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (KeyValuePair_2_t2989632341 )((*(KeyValuePair_2_t2989632341 *)((KeyValuePair_2_t2989632341 *)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2989632341_TisIl2CppObject_m4232779346_gshared (Dictionary_2_t591960174 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t3106712533 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t3106712533 * L_6 = ___transform2;
		TrackableIdPairU5BU5D_t475764036* L_7 = (TrackableIdPairU5BU5D_t475764036*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		TrackableIdPair_t4227350457  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		PoseInfoU5BU5D_t908987610* L_11 = (PoseInfoU5BU5D_t908987610*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		PoseInfo_t1612729179  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t3106712533 *)L_6);
		KeyValuePair_2_t2989632341  L_15 = ((  KeyValuePair_2_t2989632341  (*) (Transform_1_t3106712533 *, TrackableIdPair_t4227350457 , PoseInfo_t1612729179 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t3106712533 *)L_6, (TrackableIdPair_t4227350457 )L_10, (PoseInfo_t1612729179 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		KeyValuePair_2_t2989632341  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::Do_CopyTo<Vuforia.HoloLensExtendedTrackingManager/PoseInfo,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisPoseInfo_t1612729179_TisIl2CppObject_m448745051_gshared (Dictionary_2_t591960174 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t1729809371 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t1729809371 * L_6 = ___transform2;
		TrackableIdPairU5BU5D_t475764036* L_7 = (TrackableIdPairU5BU5D_t475764036*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		TrackableIdPair_t4227350457  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		PoseInfoU5BU5D_t908987610* L_11 = (PoseInfoU5BU5D_t908987610*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		PoseInfo_t1612729179  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t1729809371 *)L_6);
		PoseInfo_t1612729179  L_15 = ((  PoseInfo_t1612729179  (*) (Transform_1_t1729809371 *, TrackableIdPair_t4227350457 , PoseInfo_t1612729179 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t1729809371 *)L_6, (TrackableIdPair_t4227350457 )L_10, (PoseInfo_t1612729179 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		PoseInfo_t1612729179  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::Do_CopyTo<Vuforia.HoloLensExtendedTrackingManager/PoseInfo,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisPoseInfo_t1612729179_TisPoseInfo_t1612729179_m3397209373_gshared (Dictionary_2_t591960174 * __this, PoseInfoU5BU5D_t908987610* ___array0, int32_t ___index1, Transform_1_t1729809371 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		PoseInfoU5BU5D_t908987610* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t1729809371 * L_6 = ___transform2;
		TrackableIdPairU5BU5D_t475764036* L_7 = (TrackableIdPairU5BU5D_t475764036*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		TrackableIdPair_t4227350457  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		PoseInfoU5BU5D_t908987610* L_11 = (PoseInfoU5BU5D_t908987610*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		PoseInfo_t1612729179  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t1729809371 *)L_6);
		PoseInfo_t1612729179  L_15 = ((  PoseInfo_t1612729179  (*) (Transform_1_t1729809371 *, TrackableIdPair_t4227350457 , PoseInfo_t1612729179 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t1729809371 *)L_6, (TrackableIdPair_t4227350457 )L_10, (PoseInfo_t1612729179 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		PoseInfo_t1612729179  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (PoseInfo_t1612729179 )((*(PoseInfo_t1612729179 *)((PoseInfo_t1612729179 *)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::Do_CopyTo<Vuforia.VuforiaManager/TrackableIdPair,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisTrackableIdPair_t4227350457_TisIl2CppObject_m4235071121_gshared (Dictionary_2_t591960174 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t49463353 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t49463353 * L_6 = ___transform2;
		TrackableIdPairU5BU5D_t475764036* L_7 = (TrackableIdPairU5BU5D_t475764036*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		TrackableIdPair_t4227350457  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		PoseInfoU5BU5D_t908987610* L_11 = (PoseInfoU5BU5D_t908987610*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		PoseInfo_t1612729179  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t49463353 *)L_6);
		TrackableIdPair_t4227350457  L_15 = ((  TrackableIdPair_t4227350457  (*) (Transform_1_t49463353 *, TrackableIdPair_t4227350457 , PoseInfo_t1612729179 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t49463353 *)L_6, (TrackableIdPair_t4227350457 )L_10, (PoseInfo_t1612729179 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		TrackableIdPair_t4227350457  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::Do_CopyTo<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.VuforiaManager/TrackableIdPair>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisTrackableIdPair_t4227350457_TisTrackableIdPair_t4227350457_m1693902170_gshared (Dictionary_2_t591960174 * __this, TrackableIdPairU5BU5D_t475764036* ___array0, int32_t ___index1, Transform_1_t49463353 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		TrackableIdPairU5BU5D_t475764036* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t49463353 * L_6 = ___transform2;
		TrackableIdPairU5BU5D_t475764036* L_7 = (TrackableIdPairU5BU5D_t475764036*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		TrackableIdPair_t4227350457  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		PoseInfoU5BU5D_t908987610* L_11 = (PoseInfoU5BU5D_t908987610*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		PoseInfo_t1612729179  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t49463353 *)L_6);
		TrackableIdPair_t4227350457  L_15 = ((  TrackableIdPair_t4227350457  (*) (Transform_1_t49463353 *, TrackableIdPair_t4227350457 , PoseInfo_t1612729179 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t49463353 *)L_6, (TrackableIdPair_t4227350457 )L_10, (PoseInfo_t1612729179 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		TrackableIdPair_t4227350457  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (TrackableIdPair_t4227350457 )((*(TrackableIdPair_t4227350457 *)((TrackableIdPair_t4227350457 *)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::Do_ICollectionCopyTo<System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2989632341_m644375200_gshared (Dictionary_2_t591960174 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t3106712533 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2989632341_m644375200_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t3106712533 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t591960174 *)__this);
			((  void (*) (Dictionary_2_t591960174 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t3106712533 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t591960174 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t3106712533 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::Do_ICollectionCopyTo<Vuforia.HoloLensExtendedTrackingManager/PoseInfo>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisPoseInfo_t1612729179_m82049209_gshared (Dictionary_2_t591960174 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t1729809371 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisPoseInfo_t1612729179_m82049209_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t1729809371 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t591960174 *)__this);
			((  void (*) (Dictionary_2_t591960174 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t1729809371 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t591960174 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t1729809371 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::Do_ICollectionCopyTo<Vuforia.VuforiaManager/TrackableIdPair>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisTrackableIdPair_t4227350457_m2949278720_gshared (Dictionary_2_t591960174 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t49463353 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisTrackableIdPair_t4227350457_m2949278720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t49463353 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t591960174 *)__this);
			((  void (*) (Dictionary_2_t591960174 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t49463353 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t591960174 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t49463353 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::Do_CopyTo<System.Collections.DictionaryEntry,System.Collections.DictionaryEntry>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t3123975638_TisDictionaryEntry_t3123975638_m2416026738_gshared (Dictionary_2_t80136809 * __this, DictionaryEntryU5BU5D_t4217117203* ___array0, int32_t ___index1, Transform_1_t3336711695 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		DictionaryEntryU5BU5D_t4217117203* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t3336711695 * L_6 = ___transform2;
		TrackableIdPairU5BU5D_t475764036* L_7 = (TrackableIdPairU5BU5D_t475764036*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		TrackableIdPair_t4227350457  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		StatusU5BU5D_t1004643475* L_11 = (StatusU5BU5D_t1004643475*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t3336711695 *)L_6);
		DictionaryEntry_t3123975638  L_15 = ((  DictionaryEntry_t3123975638  (*) (Transform_1_t3336711695 *, TrackableIdPair_t4227350457 , int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t3336711695 *)L_6, (TrackableIdPair_t4227350457 )L_10, (int32_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		DictionaryEntry_t3123975638  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (DictionaryEntry_t3123975638 )((*(DictionaryEntry_t3123975638 *)((DictionaryEntry_t3123975638 *)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>,System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2477808976_TisKeyValuePair_2_t2477808976_m1277010750_gshared (Dictionary_2_t80136809 * __this, KeyValuePair_2U5BU5D_t3540808561* ___array0, int32_t ___index1, Transform_1_t2690545033 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		KeyValuePair_2U5BU5D_t3540808561* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t2690545033 * L_6 = ___transform2;
		TrackableIdPairU5BU5D_t475764036* L_7 = (TrackableIdPairU5BU5D_t475764036*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		TrackableIdPair_t4227350457  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		StatusU5BU5D_t1004643475* L_11 = (StatusU5BU5D_t1004643475*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t2690545033 *)L_6);
		KeyValuePair_2_t2477808976  L_15 = ((  KeyValuePair_2_t2477808976  (*) (Transform_1_t2690545033 *, TrackableIdPair_t4227350457 , int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t2690545033 *)L_6, (TrackableIdPair_t4227350457 )L_10, (int32_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		KeyValuePair_2_t2477808976  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (KeyValuePair_2_t2477808976 )((*(KeyValuePair_2_t2477808976 *)((KeyValuePair_2_t2477808976 *)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2477808976_TisIl2CppObject_m3920954144_gshared (Dictionary_2_t80136809 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t2690545033 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t2690545033 * L_6 = ___transform2;
		TrackableIdPairU5BU5D_t475764036* L_7 = (TrackableIdPairU5BU5D_t475764036*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		TrackableIdPair_t4227350457  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		StatusU5BU5D_t1004643475* L_11 = (StatusU5BU5D_t1004643475*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t2690545033 *)L_6);
		KeyValuePair_2_t2477808976  L_15 = ((  KeyValuePair_2_t2477808976  (*) (Transform_1_t2690545033 *, TrackableIdPair_t4227350457 , int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t2690545033 *)L_6, (TrackableIdPair_t4227350457 )L_10, (int32_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		KeyValuePair_2_t2477808976  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::Do_CopyTo<Vuforia.TrackableBehaviour/Status,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisStatus_t1100905814_TisIl2CppObject_m3660208997_gshared (Dictionary_2_t80136809 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t1313641871 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t1313641871 * L_6 = ___transform2;
		TrackableIdPairU5BU5D_t475764036* L_7 = (TrackableIdPairU5BU5D_t475764036*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		TrackableIdPair_t4227350457  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		StatusU5BU5D_t1004643475* L_11 = (StatusU5BU5D_t1004643475*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t1313641871 *)L_6);
		int32_t L_15 = ((  int32_t (*) (Transform_1_t1313641871 *, TrackableIdPair_t4227350457 , int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t1313641871 *)L_6, (TrackableIdPair_t4227350457 )L_10, (int32_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::Do_CopyTo<Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisStatus_t1100905814_TisStatus_t1100905814_m833427182_gshared (Dictionary_2_t80136809 * __this, StatusU5BU5D_t1004643475* ___array0, int32_t ___index1, Transform_1_t1313641871 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		StatusU5BU5D_t1004643475* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t1313641871 * L_6 = ___transform2;
		TrackableIdPairU5BU5D_t475764036* L_7 = (TrackableIdPairU5BU5D_t475764036*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		TrackableIdPair_t4227350457  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		StatusU5BU5D_t1004643475* L_11 = (StatusU5BU5D_t1004643475*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t1313641871 *)L_6);
		int32_t L_15 = ((  int32_t (*) (Transform_1_t1313641871 *, TrackableIdPair_t4227350457 , int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t1313641871 *)L_6, (TrackableIdPair_t4227350457 )L_10, (int32_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::Do_CopyTo<Vuforia.VuforiaManager/TrackableIdPair,System.Object>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisTrackableIdPair_t4227350457_TisIl2CppObject_m2712377032_gshared (Dictionary_2_t80136809 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, Transform_1_t145119218 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t145119218 * L_6 = ___transform2;
		TrackableIdPairU5BU5D_t475764036* L_7 = (TrackableIdPairU5BU5D_t475764036*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		TrackableIdPair_t4227350457  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		StatusU5BU5D_t1004643475* L_11 = (StatusU5BU5D_t1004643475*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t145119218 *)L_6);
		TrackableIdPair_t4227350457  L_15 = ((  TrackableIdPair_t4227350457  (*) (Transform_1_t145119218 *, TrackableIdPair_t4227350457 , int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t145119218 *)L_6, (TrackableIdPair_t4227350457 )L_10, (int32_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		TrackableIdPair_t4227350457  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((Il2CppObject *)Castclass(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::Do_CopyTo<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.VuforiaManager/TrackableIdPair>(TElem[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_CopyTo_TisTrackableIdPair_t4227350457_TisTrackableIdPair_t4227350457_m2076527386_gshared (Dictionary_2_t80136809 * __this, TrackableIdPairU5BU5D_t475764036* ___array0, int32_t ___index1, Transform_1_t145119218 * ___transform2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_005b;
	}

IL_0007:
	{
		LinkU5BU5D_t964245573* L_0 = (LinkU5BU5D_t964245573*)__this->get_linkSlots_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_0057;
		}
	}
	{
		TrackableIdPairU5BU5D_t475764036* L_3 = ___array0;
		int32_t L_4 = ___index1;
		int32_t L_5 = (int32_t)L_4;
		___index1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
		Transform_1_t145119218 * L_6 = ___transform2;
		TrackableIdPairU5BU5D_t475764036* L_7 = (TrackableIdPairU5BU5D_t475764036*)__this->get_keySlots_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		TrackableIdPair_t4227350457  L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		StatusU5BU5D_t1004643475* L_11 = (StatusU5BU5D_t1004643475*)__this->get_valueSlots_7();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((Transform_1_t145119218 *)L_6);
		TrackableIdPair_t4227350457  L_15 = ((  TrackableIdPair_t4227350457  (*) (Transform_1_t145119218 *, TrackableIdPair_t4227350457 , int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Transform_1_t145119218 *)L_6, (TrackableIdPair_t4227350457 )L_10, (int32_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		TrackableIdPair_t4227350457  L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_16);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (TrackableIdPair_t4227350457 )((*(TrackableIdPair_t4227350457 *)((TrackableIdPair_t4227350457 *)UnBox(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))));
	}

IL_0057:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)__this->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::Do_ICollectionCopyTo<System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2477808976_m1598207542_gshared (Dictionary_2_t80136809 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t2690545033 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2477808976_m1598207542_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t2690545033 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t80136809 *)__this);
			((  void (*) (Dictionary_2_t80136809 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t2690545033 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t80136809 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t2690545033 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::Do_ICollectionCopyTo<Vuforia.TrackableBehaviour/Status>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisStatus_t1100905814_m812461599_gshared (Dictionary_2_t80136809 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t1313641871 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisStatus_t1100905814_m812461599_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t1313641871 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t80136809 *)__this);
			((  void (*) (Dictionary_2_t80136809 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t1313641871 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t80136809 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t1313641871 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::Do_ICollectionCopyTo<Vuforia.VuforiaManager/TrackableIdPair>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,TRet>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisTrackableIdPair_t4227350457_m3157615771_gshared (Dictionary_2_t80136809 * __this, Il2CppArray * ___array0, int32_t ___index1, Transform_1_t145119218 * ___transform2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dictionary_2_Do_ICollectionCopyTo_TisTrackableIdPair_t4227350457_m3157615771_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppArray * L_1 = ___array0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(50 /* System.Type System.Type::GetElementType() */, (Type_t *)L_2);
		V_1 = (Type_t *)L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_4 = V_0;
			NullCheck((Type_t *)L_4);
			bool L_5 = Type_get_IsPrimitive_m1114712797((Type_t *)L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0022:
		{
			Type_t * L_6 = V_1;
			NullCheck((Type_t *)L_6);
			bool L_7 = Type_get_IsPrimitive_m1114712797((Type_t *)L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Type_t * L_8 = V_1;
			Type_t * L_9 = V_0;
			NullCheck((Type_t *)L_8);
			bool L_10 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_9);
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Exception_t1436737249 * L_11 = (Exception_t1436737249 *)il2cpp_codegen_object_new(Exception_t1436737249_il2cpp_TypeInfo_var);
			Exception__ctor_m213470898(L_11, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
		}

IL_003f:
		{
			Il2CppArray * L_12 = ___array0;
			int32_t L_13 = ___index1;
			Transform_1_t145119218 * L_14 = ___transform2;
			NullCheck((Dictionary_2_t80136809 *)__this);
			((  void (*) (Dictionary_2_t80136809 *, ObjectU5BU5D_t2843939325*, int32_t, Transform_1_t145119218 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Dictionary_2_t80136809 *)__this, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_12, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), (int32_t)L_13, (Transform_1_t145119218 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
			goto IL_0069;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0052;
		throw e;
	}

CATCH_0052:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_15 = V_2;
			ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3761792013(L_16, (String_t*)_stringLiteral1492106003, (String_t*)_stringLiteral4007973390, (Exception_t1436737249 *)L_15, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_0064:
		{
			goto IL_0069;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		return;
	}
}
// System.Void UnityEngine.Component::GetComponents<System.Object>(System.Collections.Generic.List`1<T>)
extern "C"  void Component_GetComponents_TisIl2CppObject_m2416546752_gshared (Component_t1923634451 * __this, List_1_t257213610 * ___results0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Component_GetComponents_TisIl2CppObject_m2416546752_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		List_1_t257213610 * L_1 = ___results0;
		NullCheck((Component_t1923634451 *)__this);
		Component_GetComponentsForListInternal_m1978301288((Component_t1923634451 *)__this, (Type_t *)L_0, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean,System.Collections.Generic.List`1<T>)
extern "C"  void Component_GetComponentsInChildren_TisIl2CppObject_m4185132698_gshared (Component_t1923634451 * __this, bool ___includeInactive0, List_1_t257213610 * ___result1, const MethodInfo* method)
{
	{
		NullCheck((Component_t1923634451 *)__this);
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m2648350745((Component_t1923634451 *)__this, /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		List_1_t257213610 * L_2 = ___result1;
		NullCheck((GameObject_t1113636619 *)L_0);
		((  void (*) (GameObject_t1113636619 *, bool, List_1_t257213610 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((GameObject_t1113636619 *)L_0, (bool)L_1, (List_1_t257213610 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return;
	}
}
// System.Void UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Collections.Generic.List`1<T>)
extern "C"  void Component_GetComponentsInChildren_TisIl2CppObject_m35549932_gshared (Component_t1923634451 * __this, List_1_t257213610 * ___results0, const MethodInfo* method)
{
	{
		List_1_t257213610 * L_0 = ___results0;
		NullCheck((Component_t1923634451 *)__this);
		((  void (*) (Component_t1923634451 *, bool, List_1_t257213610 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Component_t1923634451 *)__this, (bool)0, (List_1_t257213610 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return;
	}
}
// System.Void UnityEngine.Component::GetComponentsInParent<System.Object>(System.Boolean,System.Collections.Generic.List`1<T>)
extern "C"  void Component_GetComponentsInParent_TisIl2CppObject_m3603136339_gshared (Component_t1923634451 * __this, bool ___includeInactive0, List_1_t257213610 * ___results1, const MethodInfo* method)
{
	{
		NullCheck((Component_t1923634451 *)__this);
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m2648350745((Component_t1923634451 *)__this, /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		List_1_t257213610 * L_2 = ___results1;
		NullCheck((GameObject_t1113636619 *)L_0);
		((  void (*) (GameObject_t1113636619 *, bool, List_1_t257213610 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((GameObject_t1113636619 *)L_0, (bool)L_1, (List_1_t257213610 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return;
	}
}
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Boolean>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisBoolean_t97287965_m1128114209_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BaseInvokableCall_ThrowOnInvalidArg_TisBoolean_t97287965_m1128114209_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___arg0;
		if (!L_0)
		{
			goto IL_003e;
		}
	}
	{
		Il2CppObject * L_1 = ___arg0;
		if (((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0))))
		{
			goto IL_003e;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_2 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)2));
		Il2CppObject * L_3 = ___arg0;
		NullCheck((Il2CppObject *)L_3);
		Type_t * L_4 = Object_GetType_m88164663((Il2CppObject *)L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_4);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		ObjectU5BU5D_t2843939325* L_5 = (ObjectU5BU5D_t2843939325*)L_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_6);
		String_t* L_7 = UnityString_Format_m3741272017(NULL /*static, unused*/, (String_t*)_stringLiteral273595399, (ObjectU5BU5D_t2843939325*)L_5, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_8 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_8, (String_t*)L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_003e:
	{
		return;
	}
}
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Int32>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisInt32_t2950945753_m3123282398_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BaseInvokableCall_ThrowOnInvalidArg_TisInt32_t2950945753_m3123282398_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___arg0;
		if (!L_0)
		{
			goto IL_003e;
		}
	}
	{
		Il2CppObject * L_1 = ___arg0;
		if (((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0))))
		{
			goto IL_003e;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_2 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)2));
		Il2CppObject * L_3 = ___arg0;
		NullCheck((Il2CppObject *)L_3);
		Type_t * L_4 = Object_GetType_m88164663((Il2CppObject *)L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_4);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		ObjectU5BU5D_t2843939325* L_5 = (ObjectU5BU5D_t2843939325*)L_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_6);
		String_t* L_7 = UnityString_Format_m3741272017(NULL /*static, unused*/, (String_t*)_stringLiteral273595399, (ObjectU5BU5D_t2843939325*)L_5, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_8 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_8, (String_t*)L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_003e:
	{
		return;
	}
}
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisIl2CppObject_m1037130369_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BaseInvokableCall_ThrowOnInvalidArg_TisIl2CppObject_m1037130369_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___arg0;
		if (!L_0)
		{
			goto IL_003e;
		}
	}
	{
		Il2CppObject * L_1 = ___arg0;
		if (((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0))))
		{
			goto IL_003e;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_2 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)2));
		Il2CppObject * L_3 = ___arg0;
		NullCheck((Il2CppObject *)L_3);
		Type_t * L_4 = Object_GetType_m88164663((Il2CppObject *)L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_4);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		ObjectU5BU5D_t2843939325* L_5 = (ObjectU5BU5D_t2843939325*)L_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_6);
		String_t* L_7 = UnityString_Format_m3741272017(NULL /*static, unused*/, (String_t*)_stringLiteral273595399, (ObjectU5BU5D_t2843939325*)L_5, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_8 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_8, (String_t*)L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_003e:
	{
		return;
	}
}
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Single>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisSingle_t1397266774_m182307123_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BaseInvokableCall_ThrowOnInvalidArg_TisSingle_t1397266774_m182307123_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___arg0;
		if (!L_0)
		{
			goto IL_003e;
		}
	}
	{
		Il2CppObject * L_1 = ___arg0;
		if (((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0))))
		{
			goto IL_003e;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_2 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)2));
		Il2CppObject * L_3 = ___arg0;
		NullCheck((Il2CppObject *)L_3);
		Type_t * L_4 = Object_GetType_m88164663((Il2CppObject *)L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_4);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		ObjectU5BU5D_t2843939325* L_5 = (ObjectU5BU5D_t2843939325*)L_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_6);
		String_t* L_7 = UnityString_Format_m3741272017(NULL /*static, unused*/, (String_t*)_stringLiteral273595399, (ObjectU5BU5D_t2843939325*)L_5, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_8 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_8, (String_t*)L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_003e:
	{
		return;
	}
}
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Color>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisColor_t2555686324_m391225187_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BaseInvokableCall_ThrowOnInvalidArg_TisColor_t2555686324_m391225187_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___arg0;
		if (!L_0)
		{
			goto IL_003e;
		}
	}
	{
		Il2CppObject * L_1 = ___arg0;
		if (((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0))))
		{
			goto IL_003e;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_2 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)2));
		Il2CppObject * L_3 = ___arg0;
		NullCheck((Il2CppObject *)L_3);
		Type_t * L_4 = Object_GetType_m88164663((Il2CppObject *)L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_4);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		ObjectU5BU5D_t2843939325* L_5 = (ObjectU5BU5D_t2843939325*)L_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_6);
		String_t* L_7 = UnityString_Format_m3741272017(NULL /*static, unused*/, (String_t*)_stringLiteral273595399, (ObjectU5BU5D_t2843939325*)L_5, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_8 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_8, (String_t*)L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_003e:
	{
		return;
	}
}
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Vector2>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t2156229523_m3759800740_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t2156229523_m3759800740_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___arg0;
		if (!L_0)
		{
			goto IL_003e;
		}
	}
	{
		Il2CppObject * L_1 = ___arg0;
		if (((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0))))
		{
			goto IL_003e;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_2 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)2));
		Il2CppObject * L_3 = ___arg0;
		NullCheck((Il2CppObject *)L_3);
		Type_t * L_4 = Object_GetType_m88164663((Il2CppObject *)L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_4);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		ObjectU5BU5D_t2843939325* L_5 = (ObjectU5BU5D_t2843939325*)L_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_6);
		String_t* L_7 = UnityString_Format_m3741272017(NULL /*static, unused*/, (String_t*)_stringLiteral273595399, (ObjectU5BU5D_t2843939325*)L_5, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_8 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_8, (String_t*)L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_003e:
	{
		return;
	}
}
// System.Void UnityEngine.EventSystems.ExecuteEvents::GetEventList<System.Object>(UnityEngine.GameObject,System.Collections.Generic.IList`1<UnityEngine.EventSystems.IEventSystemHandler>)
extern "C"  void ExecuteEvents_GetEventList_TisIl2CppObject_m3803188029_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1113636619 * ___go0, Il2CppObject* ___results1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExecuteEvents_GetEventList_TisIl2CppObject_m3803188029_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t3395709193 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		Il2CppObject* L_0 = ___results1;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentException_t132251570 * L_1 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1216717135(L_1, (String_t*)_stringLiteral1261396091, (String_t*)_stringLiteral2001578372, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		GameObject_t1113636619 * L_2 = ___go0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m1454075600(NULL /*static, unused*/, (Object_t631007953 *)L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_002e;
		}
	}
	{
		GameObject_t1113636619 * L_4 = ___go0;
		NullCheck((GameObject_t1113636619 *)L_4);
		bool L_5 = GameObject_get_activeInHierarchy_m921367020((GameObject_t1113636619 *)L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0033;
		}
	}

IL_002e:
	{
		goto IL_0087;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t2953223642_il2cpp_TypeInfo_var);
		List_1_t3395709193 * L_6 = ListPool_1_Get_m2062177143(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m2062177143_MethodInfo_var);
		V_0 = (List_1_t3395709193 *)L_6;
		GameObject_t1113636619 * L_7 = ___go0;
		List_1_t3395709193 * L_8 = V_0;
		NullCheck((GameObject_t1113636619 *)L_7);
		GameObject_GetComponents_TisComponent_t1923634451_m1648148377((GameObject_t1113636619 *)L_7, (List_1_t3395709193 *)L_8, /*hidden argument*/GameObject_GetComponents_TisComponent_t1923634451_m1648148377_MethodInfo_var);
		V_1 = (int32_t)0;
		goto IL_0075;
	}

IL_0047:
	{
		List_1_t3395709193 * L_9 = V_0;
		int32_t L_10 = V_1;
		NullCheck((List_1_t3395709193 *)L_9);
		Component_t1923634451 * L_11 = List_1_get_Item_m3306164819((List_1_t3395709193 *)L_9, (int32_t)L_10, /*hidden argument*/List_1_get_Item_m3306164819_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t3484638744_il2cpp_TypeInfo_var);
		bool L_12 = ((  bool (*) (Il2CppObject * /* static, unused */, Component_t1923634451 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Component_t1923634451 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (L_12)
		{
			goto IL_005e;
		}
	}
	{
		goto IL_0071;
	}

IL_005e:
	{
		Il2CppObject* L_13 = ___results1;
		List_1_t3395709193 * L_14 = V_0;
		int32_t L_15 = V_1;
		NullCheck((List_1_t3395709193 *)L_14);
		Component_t1923634451 * L_16 = List_1_get_Item_m3306164819((List_1_t3395709193 *)L_14, (int32_t)L_15, /*hidden argument*/List_1_get_Item_m3306164819_MethodInfo_var);
		NullCheck((Il2CppObject*)L_13);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IEventSystemHandler>::Add(!0) */, ICollection_1_t1887868788_il2cpp_TypeInfo_var, (Il2CppObject*)L_13, (Il2CppObject *)((Il2CppObject *)IsInst(L_16, IEventSystemHandler_t3354683850_il2cpp_TypeInfo_var)));
	}

IL_0071:
	{
		int32_t L_17 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0075:
	{
		int32_t L_18 = V_1;
		List_1_t3395709193 * L_19 = V_0;
		NullCheck((List_1_t3395709193 *)L_19);
		int32_t L_20 = List_1_get_Count_m1294235957((List_1_t3395709193 *)L_19, /*hidden argument*/List_1_get_Count_m1294235957_MethodInfo_var);
		if ((((int32_t)L_18) < ((int32_t)L_20)))
		{
			goto IL_0047;
		}
	}
	{
		List_1_t3395709193 * L_21 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t2953223642_il2cpp_TypeInfo_var);
		ListPool_1_Release_m3920209327(NULL /*static, unused*/, (List_1_t3395709193 *)L_21, /*hidden argument*/ListPool_1_Release_m3920209327_MethodInfo_var);
	}

IL_0087:
	{
		return;
	}
}
// System.Void UnityEngine.GameObject::GetComponents<System.Object>(System.Collections.Generic.List`1<T>)
extern "C"  void GameObject_GetComponents_TisIl2CppObject_m974145097_gshared (GameObject_t1113636619 * __this, List_1_t257213610 * ___results0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponents_TisIl2CppObject_m974145097_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		List_1_t257213610 * L_1 = ___results0;
		NullCheck((GameObject_t1113636619 *)__this);
		GameObject_GetComponentsInternal_m525215727((GameObject_t1113636619 *)__this, (Type_t *)L_0, (bool)0, (bool)0, (bool)1, (bool)0, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GameObject::GetComponentsInChildren<System.Object>(System.Boolean,System.Collections.Generic.List`1<T>)
extern "C"  void GameObject_GetComponentsInChildren_TisIl2CppObject_m4174741699_gshared (GameObject_t1113636619 * __this, bool ___includeInactive0, List_1_t257213610 * ___results1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponentsInChildren_TisIl2CppObject_m4174741699_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		List_1_t257213610 * L_2 = ___results1;
		NullCheck((GameObject_t1113636619 *)__this);
		GameObject_GetComponentsInternal_m525215727((GameObject_t1113636619 *)__this, (Type_t *)L_0, (bool)1, (bool)1, (bool)L_1, (bool)0, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GameObject::GetComponentsInParent<System.Object>(System.Boolean,System.Collections.Generic.List`1<T>)
extern "C"  void GameObject_GetComponentsInParent_TisIl2CppObject_m947018401_gshared (GameObject_t1113636619 * __this, bool ___includeInactive0, List_1_t257213610 * ___results1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponentsInParent_TisIl2CppObject_m947018401_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		List_1_t257213610 * L_2 = ___results1;
		NullCheck((GameObject_t1113636619 *)__this);
		GameObject_GetComponentsInternal_m525215727((GameObject_t1113636619 *)__this, (Type_t *)L_0, (bool)1, (bool)1, (bool)L_1, (bool)1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetArrayForChannel<System.Object>(UnityEngine.Mesh/InternalShaderChannel,T[])
extern "C"  void Mesh_SetArrayForChannel_TisIl2CppObject_m2935277067_gshared (Mesh_t3648964284 * __this, int32_t ___channel0, ObjectU5BU5D_t2843939325* ___values1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___channel0;
		int32_t L_1 = ___channel0;
		int32_t L_2 = Mesh_DefaultDimensionForChannel_m4089233615(NULL /*static, unused*/, (int32_t)L_1, /*hidden argument*/NULL);
		ObjectU5BU5D_t2843939325* L_3 = ___values1;
		ObjectU5BU5D_t2843939325* L_4 = ___values1;
		NullCheck((Mesh_t3648964284 *)__this);
		int32_t L_5 = Mesh_SafeLength_m2456625434((Mesh_t3648964284 *)__this, (Il2CppArray *)(Il2CppArray *)L_4, /*hidden argument*/NULL);
		NullCheck((Mesh_t3648964284 *)__this);
		Mesh_SetSizedArrayForChannel_m2433695052((Mesh_t3648964284 *)__this, (int32_t)L_0, (int32_t)0, (int32_t)L_2, (Il2CppArray *)(Il2CppArray *)L_3, (int32_t)L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetArrayForChannel<System.Object>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,T[])
extern "C"  void Mesh_SetArrayForChannel_TisIl2CppObject_m1101287124_gshared (Mesh_t3648964284 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, ObjectU5BU5D_t2843939325* ___values3, const MethodInfo* method)
{
	{
		int32_t L_0 = ___channel0;
		int32_t L_1 = ___format1;
		int32_t L_2 = ___dim2;
		ObjectU5BU5D_t2843939325* L_3 = ___values3;
		ObjectU5BU5D_t2843939325* L_4 = ___values3;
		NullCheck((Mesh_t3648964284 *)__this);
		int32_t L_5 = Mesh_SafeLength_m2456625434((Mesh_t3648964284 *)__this, (Il2CppArray *)(Il2CppArray *)L_4, /*hidden argument*/NULL);
		NullCheck((Mesh_t3648964284 *)__this);
		Mesh_SetSizedArrayForChannel_m2433695052((Mesh_t3648964284 *)__this, (int32_t)L_0, (int32_t)L_1, (int32_t)L_2, (Il2CppArray *)(Il2CppArray *)L_3, (int32_t)L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetArrayForChannel<UnityEngine.Color>(UnityEngine.Mesh/InternalShaderChannel,T[])
extern "C"  void Mesh_SetArrayForChannel_TisColor_t2555686324_m800997319_gshared (Mesh_t3648964284 * __this, int32_t ___channel0, ColorU5BU5D_t941916413* ___values1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___channel0;
		int32_t L_1 = ___channel0;
		int32_t L_2 = Mesh_DefaultDimensionForChannel_m4089233615(NULL /*static, unused*/, (int32_t)L_1, /*hidden argument*/NULL);
		ColorU5BU5D_t941916413* L_3 = ___values1;
		ColorU5BU5D_t941916413* L_4 = ___values1;
		NullCheck((Mesh_t3648964284 *)__this);
		int32_t L_5 = Mesh_SafeLength_m2456625434((Mesh_t3648964284 *)__this, (Il2CppArray *)(Il2CppArray *)L_4, /*hidden argument*/NULL);
		NullCheck((Mesh_t3648964284 *)__this);
		Mesh_SetSizedArrayForChannel_m2433695052((Mesh_t3648964284 *)__this, (int32_t)L_0, (int32_t)0, (int32_t)L_2, (Il2CppArray *)(Il2CppArray *)L_3, (int32_t)L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetArrayForChannel<UnityEngine.Color32>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,T[])
extern "C"  void Mesh_SetArrayForChannel_TisColor32_t2600501292_m2796906708_gshared (Mesh_t3648964284 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, Color32U5BU5D_t3850468773* ___values3, const MethodInfo* method)
{
	{
		int32_t L_0 = ___channel0;
		int32_t L_1 = ___format1;
		int32_t L_2 = ___dim2;
		Color32U5BU5D_t3850468773* L_3 = ___values3;
		Color32U5BU5D_t3850468773* L_4 = ___values3;
		NullCheck((Mesh_t3648964284 *)__this);
		int32_t L_5 = Mesh_SafeLength_m2456625434((Mesh_t3648964284 *)__this, (Il2CppArray *)(Il2CppArray *)L_4, /*hidden argument*/NULL);
		NullCheck((Mesh_t3648964284 *)__this);
		Mesh_SetSizedArrayForChannel_m2433695052((Mesh_t3648964284 *)__this, (int32_t)L_0, (int32_t)L_1, (int32_t)L_2, (Il2CppArray *)(Il2CppArray *)L_3, (int32_t)L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetArrayForChannel<UnityEngine.Vector2>(UnityEngine.Mesh/InternalShaderChannel,T[])
extern "C"  void Mesh_SetArrayForChannel_TisVector2_t2156229523_m2178854011_gshared (Mesh_t3648964284 * __this, int32_t ___channel0, Vector2U5BU5D_t1457185986* ___values1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___channel0;
		int32_t L_1 = ___channel0;
		int32_t L_2 = Mesh_DefaultDimensionForChannel_m4089233615(NULL /*static, unused*/, (int32_t)L_1, /*hidden argument*/NULL);
		Vector2U5BU5D_t1457185986* L_3 = ___values1;
		Vector2U5BU5D_t1457185986* L_4 = ___values1;
		NullCheck((Mesh_t3648964284 *)__this);
		int32_t L_5 = Mesh_SafeLength_m2456625434((Mesh_t3648964284 *)__this, (Il2CppArray *)(Il2CppArray *)L_4, /*hidden argument*/NULL);
		NullCheck((Mesh_t3648964284 *)__this);
		Mesh_SetSizedArrayForChannel_m2433695052((Mesh_t3648964284 *)__this, (int32_t)L_0, (int32_t)0, (int32_t)L_2, (Il2CppArray *)(Il2CppArray *)L_3, (int32_t)L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetArrayForChannel<UnityEngine.Vector3>(UnityEngine.Mesh/InternalShaderChannel,T[])
extern "C"  void Mesh_SetArrayForChannel_TisVector3_t3722313464_m2152743030_gshared (Mesh_t3648964284 * __this, int32_t ___channel0, Vector3U5BU5D_t1718750761* ___values1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___channel0;
		int32_t L_1 = ___channel0;
		int32_t L_2 = Mesh_DefaultDimensionForChannel_m4089233615(NULL /*static, unused*/, (int32_t)L_1, /*hidden argument*/NULL);
		Vector3U5BU5D_t1718750761* L_3 = ___values1;
		Vector3U5BU5D_t1718750761* L_4 = ___values1;
		NullCheck((Mesh_t3648964284 *)__this);
		int32_t L_5 = Mesh_SafeLength_m2456625434((Mesh_t3648964284 *)__this, (Il2CppArray *)(Il2CppArray *)L_4, /*hidden argument*/NULL);
		NullCheck((Mesh_t3648964284 *)__this);
		Mesh_SetSizedArrayForChannel_m2433695052((Mesh_t3648964284 *)__this, (int32_t)L_0, (int32_t)0, (int32_t)L_2, (Il2CppArray *)(Il2CppArray *)L_3, (int32_t)L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetArrayForChannel<UnityEngine.Vector4>(UnityEngine.Mesh/InternalShaderChannel,T[])
extern "C"  void Mesh_SetArrayForChannel_TisVector4_t3319028937_m3133911601_gshared (Mesh_t3648964284 * __this, int32_t ___channel0, Vector4U5BU5D_t934056436* ___values1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___channel0;
		int32_t L_1 = ___channel0;
		int32_t L_2 = Mesh_DefaultDimensionForChannel_m4089233615(NULL /*static, unused*/, (int32_t)L_1, /*hidden argument*/NULL);
		Vector4U5BU5D_t934056436* L_3 = ___values1;
		Vector4U5BU5D_t934056436* L_4 = ___values1;
		NullCheck((Mesh_t3648964284 *)__this);
		int32_t L_5 = Mesh_SafeLength_m2456625434((Mesh_t3648964284 *)__this, (Il2CppArray *)(Il2CppArray *)L_4, /*hidden argument*/NULL);
		NullCheck((Mesh_t3648964284 *)__this);
		Mesh_SetSizedArrayForChannel_m2433695052((Mesh_t3648964284 *)__this, (int32_t)L_0, (int32_t)0, (int32_t)L_2, (Il2CppArray *)(Il2CppArray *)L_3, (int32_t)L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetListForChannel<System.Object>(UnityEngine.Mesh/InternalShaderChannel,System.Collections.Generic.List`1<T>)
extern "C"  void Mesh_SetListForChannel_TisIl2CppObject_m3931641919_gshared (Mesh_t3648964284 * __this, int32_t ___channel0, List_1_t257213610 * ___values1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___channel0;
		int32_t L_1 = ___channel0;
		int32_t L_2 = Mesh_DefaultDimensionForChannel_m4089233615(NULL /*static, unused*/, (int32_t)L_1, /*hidden argument*/NULL);
		List_1_t257213610 * L_3 = ___values1;
		Il2CppArray * L_4 = Mesh_ExtractArrayFromList_m3633351874(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		List_1_t257213610 * L_5 = ___values1;
		NullCheck((Mesh_t3648964284 *)__this);
		int32_t L_6 = ((  int32_t (*) (Mesh_t3648964284 *, List_1_t257213610 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Mesh_t3648964284 *)__this, (List_1_t257213610 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck((Mesh_t3648964284 *)__this);
		Mesh_SetSizedArrayForChannel_m2433695052((Mesh_t3648964284 *)__this, (int32_t)L_0, (int32_t)0, (int32_t)L_2, (Il2CppArray *)L_4, (int32_t)L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetListForChannel<System.Object>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,System.Collections.Generic.List`1<T>)
extern "C"  void Mesh_SetListForChannel_TisIl2CppObject_m2407984969_gshared (Mesh_t3648964284 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, List_1_t257213610 * ___values3, const MethodInfo* method)
{
	{
		int32_t L_0 = ___channel0;
		int32_t L_1 = ___format1;
		int32_t L_2 = ___dim2;
		List_1_t257213610 * L_3 = ___values3;
		Il2CppArray * L_4 = Mesh_ExtractArrayFromList_m3633351874(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		List_1_t257213610 * L_5 = ___values3;
		NullCheck((Mesh_t3648964284 *)__this);
		int32_t L_6 = ((  int32_t (*) (Mesh_t3648964284 *, List_1_t257213610 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Mesh_t3648964284 *)__this, (List_1_t257213610 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck((Mesh_t3648964284 *)__this);
		Mesh_SetSizedArrayForChannel_m2433695052((Mesh_t3648964284 *)__this, (int32_t)L_0, (int32_t)L_1, (int32_t)L_2, (Il2CppArray *)L_4, (int32_t)L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetListForChannel<UnityEngine.Color32>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,System.Collections.Generic.List`1<T>)
extern "C"  void Mesh_SetListForChannel_TisColor32_t2600501292_m1432330425_gshared (Mesh_t3648964284 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, List_1_t4072576034 * ___values3, const MethodInfo* method)
{
	{
		int32_t L_0 = ___channel0;
		int32_t L_1 = ___format1;
		int32_t L_2 = ___dim2;
		List_1_t4072576034 * L_3 = ___values3;
		Il2CppArray * L_4 = Mesh_ExtractArrayFromList_m3633351874(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		List_1_t4072576034 * L_5 = ___values3;
		NullCheck((Mesh_t3648964284 *)__this);
		int32_t L_6 = ((  int32_t (*) (Mesh_t3648964284 *, List_1_t4072576034 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Mesh_t3648964284 *)__this, (List_1_t4072576034 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck((Mesh_t3648964284 *)__this);
		Mesh_SetSizedArrayForChannel_m2433695052((Mesh_t3648964284 *)__this, (int32_t)L_0, (int32_t)L_1, (int32_t)L_2, (Il2CppArray *)L_4, (int32_t)L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetListForChannel<UnityEngine.Vector2>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,System.Collections.Generic.List`1<T>)
extern "C"  void Mesh_SetListForChannel_TisVector2_t2156229523_m627274542_gshared (Mesh_t3648964284 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, List_1_t3628304265 * ___values3, const MethodInfo* method)
{
	{
		int32_t L_0 = ___channel0;
		int32_t L_1 = ___format1;
		int32_t L_2 = ___dim2;
		List_1_t3628304265 * L_3 = ___values3;
		Il2CppArray * L_4 = Mesh_ExtractArrayFromList_m3633351874(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		List_1_t3628304265 * L_5 = ___values3;
		NullCheck((Mesh_t3648964284 *)__this);
		int32_t L_6 = ((  int32_t (*) (Mesh_t3648964284 *, List_1_t3628304265 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Mesh_t3648964284 *)__this, (List_1_t3628304265 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck((Mesh_t3648964284 *)__this);
		Mesh_SetSizedArrayForChannel_m2433695052((Mesh_t3648964284 *)__this, (int32_t)L_0, (int32_t)L_1, (int32_t)L_2, (Il2CppArray *)L_4, (int32_t)L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetListForChannel<UnityEngine.Vector3>(UnityEngine.Mesh/InternalShaderChannel,System.Collections.Generic.List`1<T>)
extern "C"  void Mesh_SetListForChannel_TisVector3_t3722313464_m594975523_gshared (Mesh_t3648964284 * __this, int32_t ___channel0, List_1_t899420910 * ___values1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___channel0;
		int32_t L_1 = ___channel0;
		int32_t L_2 = Mesh_DefaultDimensionForChannel_m4089233615(NULL /*static, unused*/, (int32_t)L_1, /*hidden argument*/NULL);
		List_1_t899420910 * L_3 = ___values1;
		Il2CppArray * L_4 = Mesh_ExtractArrayFromList_m3633351874(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		List_1_t899420910 * L_5 = ___values1;
		NullCheck((Mesh_t3648964284 *)__this);
		int32_t L_6 = ((  int32_t (*) (Mesh_t3648964284 *, List_1_t899420910 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Mesh_t3648964284 *)__this, (List_1_t899420910 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck((Mesh_t3648964284 *)__this);
		Mesh_SetSizedArrayForChannel_m2433695052((Mesh_t3648964284 *)__this, (int32_t)L_0, (int32_t)0, (int32_t)L_2, (Il2CppArray *)L_4, (int32_t)L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetListForChannel<UnityEngine.Vector4>(UnityEngine.Mesh/InternalShaderChannel,System.Collections.Generic.List`1<T>)
extern "C"  void Mesh_SetListForChannel_TisVector4_t3319028937_m503763262_gshared (Mesh_t3648964284 * __this, int32_t ___channel0, List_1_t496136383 * ___values1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___channel0;
		int32_t L_1 = ___channel0;
		int32_t L_2 = Mesh_DefaultDimensionForChannel_m4089233615(NULL /*static, unused*/, (int32_t)L_1, /*hidden argument*/NULL);
		List_1_t496136383 * L_3 = ___values1;
		Il2CppArray * L_4 = Mesh_ExtractArrayFromList_m3633351874(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		List_1_t496136383 * L_5 = ___values1;
		NullCheck((Mesh_t3648964284 *)__this);
		int32_t L_6 = ((  int32_t (*) (Mesh_t3648964284 *, List_1_t496136383 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Mesh_t3648964284 *)__this, (List_1_t496136383 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck((Mesh_t3648964284 *)__this);
		Mesh_SetSizedArrayForChannel_m2433695052((Mesh_t3648964284 *)__this, (int32_t)L_0, (int32_t)0, (int32_t)L_2, (Il2CppArray *)L_4, (int32_t)L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetUvsImpl<System.Object>(System.Int32,System.Int32,System.Collections.Generic.List`1<T>)
extern "C"  void Mesh_SetUvsImpl_TisIl2CppObject_m2518356015_gshared (Mesh_t3648964284 * __this, int32_t ___uvIndex0, int32_t ___dim1, List_1_t257213610 * ___uvs2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_SetUvsImpl_TisIl2CppObject_m2518356015_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___uvIndex0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_1 = ___uvIndex0;
		if ((((int32_t)L_1) <= ((int32_t)3)))
		{
			goto IL_001f;
		}
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2059623341(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2415889112, /*hidden argument*/NULL);
		goto IL_002f;
	}

IL_001f:
	{
		int32_t L_2 = ___uvIndex0;
		NullCheck((Mesh_t3648964284 *)__this);
		int32_t L_3 = Mesh_GetUVChannel_m3402048366((Mesh_t3648964284 *)__this, (int32_t)L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___dim1;
		List_1_t257213610 * L_5 = ___uvs2;
		NullCheck((Mesh_t3648964284 *)__this);
		((  void (*) (Mesh_t3648964284 *, int32_t, int32_t, int32_t, List_1_t257213610 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Mesh_t3648964284 *)__this, (int32_t)L_3, (int32_t)0, (int32_t)L_4, (List_1_t257213610 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
	}

IL_002f:
	{
		return;
	}
}
// System.Void UnityEngine.Mesh::SetUvsImpl<UnityEngine.Vector2>(System.Int32,System.Int32,System.Collections.Generic.List`1<T>)
extern "C"  void Mesh_SetUvsImpl_TisVector2_t2156229523_m984389559_gshared (Mesh_t3648964284 * __this, int32_t ___uvIndex0, int32_t ___dim1, List_1_t3628304265 * ___uvs2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_SetUvsImpl_TisVector2_t2156229523_m984389559_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___uvIndex0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_1 = ___uvIndex0;
		if ((((int32_t)L_1) <= ((int32_t)3)))
		{
			goto IL_001f;
		}
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2059623341(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2415889112, /*hidden argument*/NULL);
		goto IL_002f;
	}

IL_001f:
	{
		int32_t L_2 = ___uvIndex0;
		NullCheck((Mesh_t3648964284 *)__this);
		int32_t L_3 = Mesh_GetUVChannel_m3402048366((Mesh_t3648964284 *)__this, (int32_t)L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___dim1;
		List_1_t3628304265 * L_5 = ___uvs2;
		NullCheck((Mesh_t3648964284 *)__this);
		((  void (*) (Mesh_t3648964284 *, int32_t, int32_t, int32_t, List_1_t3628304265 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Mesh_t3648964284 *)__this, (int32_t)L_3, (int32_t)0, (int32_t)L_4, (List_1_t3628304265 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
	}

IL_002f:
	{
		return;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<System.Boolean>(T&,T)
extern "C"  void LayoutGroup_SetProperty_TisBoolean_t97287965_m3903959758_gshared (LayoutGroup_t2436138090 * __this, bool* ___currentValue0, bool ___newValue1, const MethodInfo* method)
{
	{
		goto IL_001c;
	}
	{
	}

IL_001c:
	{
	}
	{
		bool* L_3 = ___currentValue0;
		bool L_4 = ___newValue1;
		bool L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_5);
		bool L_7 = Boolean_Equals_m2410333903((bool*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0048;
		}
	}

IL_0043:
	{
		goto IL_0055;
	}

IL_0048:
	{
		bool* L_8 = ___currentValue0;
		bool L_9 = ___newValue1;
		(*(bool*)L_8) = L_9;
		NullCheck((LayoutGroup_t2436138090 *)__this);
		LayoutGroup_SetDirty_m957775107((LayoutGroup_t2436138090 *)__this, /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<System.Int32>(T&,T)
extern "C"  void LayoutGroup_SetProperty_TisInt32_t2950945753_m3911895589_gshared (LayoutGroup_t2436138090 * __this, int32_t* ___currentValue0, int32_t ___newValue1, const MethodInfo* method)
{
	{
		goto IL_001c;
	}
	{
	}

IL_001c:
	{
	}
	{
		int32_t* L_3 = ___currentValue0;
		int32_t L_4 = ___newValue1;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_5);
		bool L_7 = Int32_Equals_m3996243976((int32_t*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0048;
		}
	}

IL_0043:
	{
		goto IL_0055;
	}

IL_0048:
	{
		int32_t* L_8 = ___currentValue0;
		int32_t L_9 = ___newValue1;
		(*(int32_t*)L_8) = L_9;
		NullCheck((LayoutGroup_t2436138090 *)__this);
		LayoutGroup_SetDirty_m957775107((LayoutGroup_t2436138090 *)__this, /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<System.Object>(T&,T)
extern "C"  void LayoutGroup_SetProperty_TisIl2CppObject_m3460819731_gshared (LayoutGroup_t2436138090 * __this, Il2CppObject ** ___currentValue0, Il2CppObject * ___newValue1, const MethodInfo* method)
{
	{
		Il2CppObject ** L_0 = ___currentValue0;
		if ((*(Il2CppObject **)L_0))
		{
			goto IL_001c;
		}
	}
	{
		Il2CppObject * L_1 = ___newValue1;
		if (!L_1)
		{
			goto IL_0043;
		}
	}

IL_001c:
	{
		Il2CppObject ** L_2 = ___currentValue0;
		if (!(*(Il2CppObject **)L_2))
		{
			goto IL_0048;
		}
	}
	{
		Il2CppObject ** L_3 = ___currentValue0;
		Il2CppObject * L_4 = ___newValue1;
		NullCheck((Il2CppObject *)(*L_3));
		bool L_5 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*L_3), (Il2CppObject *)L_4);
		if (!L_5)
		{
			goto IL_0048;
		}
	}

IL_0043:
	{
		goto IL_0055;
	}

IL_0048:
	{
		Il2CppObject ** L_6 = ___currentValue0;
		Il2CppObject * L_7 = ___newValue1;
		(*(Il2CppObject **)L_6) = L_7;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)L_6, L_7);
		NullCheck((LayoutGroup_t2436138090 *)__this);
		LayoutGroup_SetDirty_m957775107((LayoutGroup_t2436138090 *)__this, /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<System.Single>(T&,T)
extern "C"  void LayoutGroup_SetProperty_TisSingle_t1397266774_m793506911_gshared (LayoutGroup_t2436138090 * __this, float* ___currentValue0, float ___newValue1, const MethodInfo* method)
{
	{
		goto IL_001c;
	}
	{
	}

IL_001c:
	{
	}
	{
		float* L_3 = ___currentValue0;
		float L_4 = ___newValue1;
		float L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_5);
		bool L_7 = Single_Equals_m438106747((float*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0048;
		}
	}

IL_0043:
	{
		goto IL_0055;
	}

IL_0048:
	{
		float* L_8 = ___currentValue0;
		float L_9 = ___newValue1;
		(*(float*)L_8) = L_9;
		NullCheck((LayoutGroup_t2436138090 *)__this);
		LayoutGroup_SetDirty_m957775107((LayoutGroup_t2436138090 *)__this, /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.TextAnchor>(T&,T)
extern "C"  void LayoutGroup_SetProperty_TisTextAnchor_t2035777396_m2990589179_gshared (LayoutGroup_t2436138090 * __this, int32_t* ___currentValue0, int32_t ___newValue1, const MethodInfo* method)
{
	{
		goto IL_001c;
	}
	{
	}

IL_001c:
	{
	}
	{
		int32_t* L_3 = ___currentValue0;
		int32_t L_4 = ___newValue1;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), L_3);
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		if (!L_8)
		{
			goto IL_0048;
		}
	}

IL_0043:
	{
		goto IL_0055;
	}

IL_0048:
	{
		int32_t* L_9 = ___currentValue0;
		int32_t L_10 = ___newValue1;
		(*(int32_t*)L_9) = L_10;
		NullCheck((LayoutGroup_t2436138090 *)__this);
		LayoutGroup_SetDirty_m957775107((LayoutGroup_t2436138090 *)__this, /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.UI.GridLayoutGroup/Axis>(T&,T)
extern "C"  void LayoutGroup_SetProperty_TisAxis_t3613393006_m3591044743_gshared (LayoutGroup_t2436138090 * __this, int32_t* ___currentValue0, int32_t ___newValue1, const MethodInfo* method)
{
	{
		goto IL_001c;
	}
	{
	}

IL_001c:
	{
	}
	{
		int32_t* L_3 = ___currentValue0;
		int32_t L_4 = ___newValue1;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), L_3);
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		if (!L_8)
		{
			goto IL_0048;
		}
	}

IL_0043:
	{
		goto IL_0055;
	}

IL_0048:
	{
		int32_t* L_9 = ___currentValue0;
		int32_t L_10 = ___newValue1;
		(*(int32_t*)L_9) = L_10;
		NullCheck((LayoutGroup_t2436138090 *)__this);
		LayoutGroup_SetDirty_m957775107((LayoutGroup_t2436138090 *)__this, /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.UI.GridLayoutGroup/Constraint>(T&,T)
extern "C"  void LayoutGroup_SetProperty_TisConstraint_t814224393_m1820208910_gshared (LayoutGroup_t2436138090 * __this, int32_t* ___currentValue0, int32_t ___newValue1, const MethodInfo* method)
{
	{
		goto IL_001c;
	}
	{
	}

IL_001c:
	{
	}
	{
		int32_t* L_3 = ___currentValue0;
		int32_t L_4 = ___newValue1;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), L_3);
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		if (!L_8)
		{
			goto IL_0048;
		}
	}

IL_0043:
	{
		goto IL_0055;
	}

IL_0048:
	{
		int32_t* L_9 = ___currentValue0;
		int32_t L_10 = ___newValue1;
		(*(int32_t*)L_9) = L_10;
		NullCheck((LayoutGroup_t2436138090 *)__this);
		LayoutGroup_SetDirty_m957775107((LayoutGroup_t2436138090 *)__this, /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.UI.GridLayoutGroup/Corner>(T&,T)
extern "C"  void LayoutGroup_SetProperty_TisCorner_t1493259673_m3558432704_gshared (LayoutGroup_t2436138090 * __this, int32_t* ___currentValue0, int32_t ___newValue1, const MethodInfo* method)
{
	{
		goto IL_001c;
	}
	{
	}

IL_001c:
	{
	}
	{
		int32_t* L_3 = ___currentValue0;
		int32_t L_4 = ___newValue1;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), L_3);
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		if (!L_8)
		{
			goto IL_0048;
		}
	}

IL_0043:
	{
		goto IL_0055;
	}

IL_0048:
	{
		int32_t* L_9 = ___currentValue0;
		int32_t L_10 = ___newValue1;
		(*(int32_t*)L_9) = L_10;
		NullCheck((LayoutGroup_t2436138090 *)__this);
		LayoutGroup_SetDirty_m957775107((LayoutGroup_t2436138090 *)__this, /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.Vector2>(T&,T)
extern "C"  void LayoutGroup_SetProperty_TisVector2_t2156229523_m2721164497_gshared (LayoutGroup_t2436138090 * __this, Vector2_t2156229523 * ___currentValue0, Vector2_t2156229523  ___newValue1, const MethodInfo* method)
{
	{
		goto IL_001c;
	}
	{
	}

IL_001c:
	{
	}
	{
		Vector2_t2156229523 * L_3 = ___currentValue0;
		Vector2_t2156229523  L_4 = ___newValue1;
		Vector2_t2156229523  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_5);
		bool L_7 = Vector2_Equals_m1356052954((Vector2_t2156229523 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0048;
		}
	}

IL_0043:
	{
		goto IL_0055;
	}

IL_0048:
	{
		Vector2_t2156229523 * L_8 = ___currentValue0;
		Vector2_t2156229523  L_9 = ___newValue1;
		(*(Vector2_t2156229523 *)L_8) = L_9;
		NullCheck((LayoutGroup_t2436138090 *)__this);
		LayoutGroup_SetDirty_m957775107((LayoutGroup_t2436138090 *)__this, /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// System.Void Vuforia.DelegateHelper::InvokeWithExceptionHandling<System.Boolean>(System.Action`1<T>,T)
extern "C"  void DelegateHelper_InvokeWithExceptionHandling_TisBoolean_t97287965_m1676209294_gshared (Il2CppObject * __this /* static, unused */, Action_1_t269755560 * ___action0, bool ___arg1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DelegateHelper_InvokeWithExceptionHandling_TisBoolean_t97287965_m1676209294_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t269755560 * L_0 = ___action0;
		ObjectU5BU5D_t2843939325* L_1 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		bool L_2 = ___arg1;
		bool L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		DelegateHelper_InvokeDelegate_m431786658(NULL /*static, unused*/, (Delegate_t1188392813 *)L_0, (ObjectU5BU5D_t2843939325*)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DelegateHelper::InvokeWithExceptionHandling<System.Object,System.Object>(System.Action`2<T1,T2>,T1,T2)
extern "C"  void DelegateHelper_InvokeWithExceptionHandling_TisIl2CppObject_TisIl2CppObject_m3522526466_gshared (Il2CppObject * __this /* static, unused */, Action_2_t2470008838 * ___action0, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DelegateHelper_InvokeWithExceptionHandling_TisIl2CppObject_TisIl2CppObject_m3522526466_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_2_t2470008838 * L_0 = ___action0;
		ObjectU5BU5D_t2843939325* L_1 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)2));
		Il2CppObject * L_2 = ___arg11;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
		ObjectU5BU5D_t2843939325* L_3 = (ObjectU5BU5D_t2843939325*)L_1;
		Il2CppObject * L_4 = ___arg22;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		DelegateHelper_InvokeDelegate_m431786658(NULL /*static, unused*/, (Delegate_t1188392813 *)L_0, (ObjectU5BU5D_t2843939325*)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DelegateHelper::InvokeWithExceptionHandling<System.Object>(System.Action`1<T>,T)
extern "C"  void DelegateHelper_InvokeWithExceptionHandling_TisIl2CppObject_m3886612143_gshared (Il2CppObject * __this /* static, unused */, Action_1_t3252573759 * ___action0, Il2CppObject * ___arg1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DelegateHelper_InvokeWithExceptionHandling_TisIl2CppObject_m3886612143_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t3252573759 * L_0 = ___action0;
		ObjectU5BU5D_t2843939325* L_1 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_2 = ___arg1;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
		DelegateHelper_InvokeDelegate_m431786658(NULL /*static, unused*/, (Delegate_t1188392813 *)L_0, (ObjectU5BU5D_t2843939325*)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DelegateHelper::InvokeWithExceptionHandling<Vuforia.SmartTerrainInitializationInfo>(System.Action`1<T>,T)
extern "C"  void DelegateHelper_InvokeWithExceptionHandling_TisSmartTerrainInitializationInfo_t1789741059_m3284269701_gshared (Il2CppObject * __this /* static, unused */, Action_1_t1962208654 * ___action0, SmartTerrainInitializationInfo_t1789741059  ___arg1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DelegateHelper_InvokeWithExceptionHandling_TisSmartTerrainInitializationInfo_t1789741059_m3284269701_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t1962208654 * L_0 = ___action0;
		ObjectU5BU5D_t2843939325* L_1 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		SmartTerrainInitializationInfo_t1789741059  L_2 = ___arg1;
		SmartTerrainInitializationInfo_t1789741059  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		DelegateHelper_InvokeDelegate_m431786658(NULL /*static, unused*/, (Delegate_t1188392813 *)L_0, (ObjectU5BU5D_t2843939325*)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DelegateHelper::InvokeWithExceptionHandling<Vuforia.VuforiaUnity/InitError>(System.Action`1<T>,T)
extern "C"  void DelegateHelper_InvokeWithExceptionHandling_TisInitError_t3420749710_m585457832_gshared (Il2CppObject * __this /* static, unused */, Action_1_t3593217305 * ___action0, int32_t ___arg1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DelegateHelper_InvokeWithExceptionHandling_TisInitError_t3420749710_m585457832_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t3593217305 * L_0 = ___action0;
		ObjectU5BU5D_t2843939325* L_1 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		int32_t L_2 = ___arg1;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		DelegateHelper_InvokeDelegate_m431786658(NULL /*static, unused*/, (Delegate_t1188392813 *)L_0, (ObjectU5BU5D_t2843939325*)L_1, /*hidden argument*/NULL);
		return;
	}
}
// T NGUITools::AddChild<System.Object>(UnityEngine.GameObject)
extern "C"  Il2CppObject * NGUITools_AddChild_TisIl2CppObject_m3339032197_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1113636619 * ___parent0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NGUITools_AddChild_TisIl2CppObject_m3339032197_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = ___parent0;
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t1206951095_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_1 = NGUITools_AddChild_m2967414470(NULL /*static, unused*/, (GameObject_t1113636619 *)L_0, /*hidden argument*/NULL);
		V_0 = (GameObject_t1113636619 *)L_1;
		GameObject_t1113636619 * L_2 = V_0;
		String_t* L_3 = ((  String_t* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck((Object_t631007953 *)L_2);
		Object_set_name_m653319976((Object_t631007953 *)L_2, (String_t*)L_3, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_4 = V_0;
		NullCheck((GameObject_t1113636619 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (GameObject_t1113636619 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((GameObject_t1113636619 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_5;
	}
}
// T NGUITools::AddChild<System.Object>(UnityEngine.GameObject,System.Boolean)
extern "C"  Il2CppObject * NGUITools_AddChild_TisIl2CppObject_m1990647493_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1113636619 * ___parent0, bool ___undo1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NGUITools_AddChild_TisIl2CppObject_m1990647493_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = ___parent0;
		bool L_1 = ___undo1;
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t1206951095_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_2 = NGUITools_AddChild_m1975831075(NULL /*static, unused*/, (GameObject_t1113636619 *)L_0, (bool)L_1, /*hidden argument*/NULL);
		V_0 = (GameObject_t1113636619 *)L_2;
		GameObject_t1113636619 * L_3 = V_0;
		String_t* L_4 = ((  String_t* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck((Object_t631007953 *)L_3);
		Object_set_name_m653319976((Object_t631007953 *)L_3, (String_t*)L_4, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_5 = V_0;
		NullCheck((GameObject_t1113636619 *)L_5);
		Il2CppObject * L_6 = ((  Il2CppObject * (*) (GameObject_t1113636619 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((GameObject_t1113636619 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_6;
	}
}
// T NGUITools::AddMissingComponent<System.Object>(UnityEngine.GameObject)
extern "C"  Il2CppObject * NGUITools_AddMissingComponent_TisIl2CppObject_m159845787_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1113636619 * ___go0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NGUITools_AddMissingComponent_TisIl2CppObject_m159845787_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = ___go0;
		NullCheck((GameObject_t1113636619 *)L_0);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (GameObject_t1113636619 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((GameObject_t1113636619 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m1454075600(NULL /*static, unused*/, (Object_t631007953 *)L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001f;
		}
	}
	{
		GameObject_t1113636619 * L_4 = ___go0;
		NullCheck((GameObject_t1113636619 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (GameObject_t1113636619 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((GameObject_t1113636619 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_5;
	}

IL_001f:
	{
		Il2CppObject * L_6 = V_0;
		return L_6;
	}
}
// T NGUITools::AddWidget<System.Object>(UnityEngine.GameObject)
extern "C"  Il2CppObject * NGUITools_AddWidget_TisIl2CppObject_m1311540038_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1113636619 * ___go0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NGUITools_AddWidget_TisIl2CppObject_m1311540038_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		GameObject_t1113636619 * L_0 = ___go0;
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t1206951095_il2cpp_TypeInfo_var);
		int32_t L_1 = NGUITools_CalculateNextDepth_m3108909969(NULL /*static, unused*/, (GameObject_t1113636619 *)L_0, /*hidden argument*/NULL);
		V_0 = (int32_t)L_1;
		GameObject_t1113636619 * L_2 = ___go0;
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, GameObject_t1113636619 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (GameObject_t1113636619 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_1 = (Il2CppObject *)L_3;
		NullCheck((UIWidget_t3538521925 *)(*(&V_1)));
		UIWidget_set_width_m181008468((UIWidget_t3538521925 *)(*(&V_1)), (int32_t)((int32_t)100), /*hidden argument*/NULL);
		NullCheck((UIWidget_t3538521925 *)(*(&V_1)));
		UIWidget_set_height_m878974275((UIWidget_t3538521925 *)(*(&V_1)), (int32_t)((int32_t)100), /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		NullCheck((UIWidget_t3538521925 *)(*(&V_1)));
		UIWidget_set_depth_m2124062109((UIWidget_t3538521925 *)(*(&V_1)), (int32_t)L_4, /*hidden argument*/NULL);
		Il2CppObject * L_5 = V_1;
		return L_5;
	}
}
// T NGUITools::AddWidget<System.Object>(UnityEngine.GameObject,System.Int32)
extern "C"  Il2CppObject * NGUITools_AddWidget_TisIl2CppObject_m797893513_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1113636619 * ___go0, int32_t ___depth1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NGUITools_AddWidget_TisIl2CppObject_m797893513_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = ___go0;
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t1206951095_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, GameObject_t1113636619 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (GameObject_t1113636619 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_1;
		NullCheck((UIWidget_t3538521925 *)(*(&V_0)));
		UIWidget_set_width_m181008468((UIWidget_t3538521925 *)(*(&V_0)), (int32_t)((int32_t)100), /*hidden argument*/NULL);
		NullCheck((UIWidget_t3538521925 *)(*(&V_0)));
		UIWidget_set_height_m878974275((UIWidget_t3538521925 *)(*(&V_0)), (int32_t)((int32_t)100), /*hidden argument*/NULL);
		int32_t L_2 = ___depth1;
		NullCheck((UIWidget_t3538521925 *)(*(&V_0)));
		UIWidget_set_depth_m2124062109((UIWidget_t3538521925 *)(*(&V_0)), (int32_t)L_2, /*hidden argument*/NULL);
		Il2CppObject * L_3 = V_0;
		return L_3;
	}
}
// T NGUITools::FindInParents<System.Object>(UnityEngine.GameObject)
extern "C"  Il2CppObject * NGUITools_FindInParents_TisIl2CppObject_m780518502_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1113636619 * ___go0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NGUITools_FindInParents_TisIl2CppObject_m780518502_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Transform_t3600365921 * V_1 = NULL;
	{
		GameObject_t1113636619 * L_0 = ___go0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1454075600(NULL /*static, unused*/, (Object_t631007953 *)L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		return ((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
	}

IL_0013:
	{
		GameObject_t1113636619 * L_2 = ___go0;
		NullCheck((GameObject_t1113636619 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (GameObject_t1113636619 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((GameObject_t1113636619 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (Il2CppObject *)L_3;
		Il2CppObject * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m1454075600(NULL /*static, unused*/, (Object_t631007953 *)L_4, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_006c;
		}
	}
	{
		GameObject_t1113636619 * L_6 = ___go0;
		NullCheck((GameObject_t1113636619 *)L_6);
		Transform_t3600365921 * L_7 = GameObject_get_transform_m393750976((GameObject_t1113636619 *)L_6, /*hidden argument*/NULL);
		NullCheck((Transform_t3600365921 *)L_7);
		Transform_t3600365921 * L_8 = Transform_get_parent_m1293647796((Transform_t3600365921 *)L_7, /*hidden argument*/NULL);
		V_1 = (Transform_t3600365921 *)L_8;
		goto IL_004f;
	}

IL_003c:
	{
		Transform_t3600365921 * L_9 = V_1;
		NullCheck((Component_t1923634451 *)L_9);
		GameObject_t1113636619 * L_10 = Component_get_gameObject_m2648350745((Component_t1923634451 *)L_9, /*hidden argument*/NULL);
		NullCheck((GameObject_t1113636619 *)L_10);
		Il2CppObject * L_11 = ((  Il2CppObject * (*) (GameObject_t1113636619 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((GameObject_t1113636619 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (Il2CppObject *)L_11;
		Transform_t3600365921 * L_12 = V_1;
		NullCheck((Transform_t3600365921 *)L_12);
		Transform_t3600365921 * L_13 = Transform_get_parent_m1293647796((Transform_t3600365921 *)L_12, /*hidden argument*/NULL);
		V_1 = (Transform_t3600365921 *)L_13;
	}

IL_004f:
	{
		Transform_t3600365921 * L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, (Object_t631007953 *)L_14, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_006c;
		}
	}
	{
		Il2CppObject * L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_17 = Object_op_Equality_m1454075600(NULL /*static, unused*/, (Object_t631007953 *)L_16, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_003c;
		}
	}

IL_006c:
	{
		Il2CppObject * L_18 = V_0;
		return L_18;
	}
}
// T NGUITools::FindInParents<System.Object>(UnityEngine.Transform)
extern "C"  Il2CppObject * NGUITools_FindInParents_TisIl2CppObject_m3955890412_gshared (Il2CppObject * __this /* static, unused */, Transform_t3600365921 * ___trans0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NGUITools_FindInParents_TisIl2CppObject_m3955890412_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3600365921 * L_0 = ___trans0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1454075600(NULL /*static, unused*/, (Object_t631007953 *)L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		return ((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
	}

IL_0013:
	{
		Transform_t3600365921 * L_2 = ___trans0;
		NullCheck((Component_t1923634451 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Component_t1923634451 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Component_t1923634451 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_3;
	}
}
// T System.Activator::CreateInstance<System.Object>()
extern "C"  Il2CppObject * Activator_CreateInstance_TisIl2CppObject_m729575857_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Activator_CreateInstance_TisIl2CppObject_m729575857_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_1 = Activator_CreateInstance_m3631483688(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T System.Array::Find<System.Object>(T[],System.Predicate`1<T>)
extern "C"  Il2CppObject * Array_Find_TisIl2CppObject_m2842000327_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t2843939325* ___array0, Predicate_1_t3905400288 * ___match1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_Find_TisIl2CppObject_m2842000327_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	ObjectU5BU5D_t2843939325* V_1 = NULL;
	int32_t V_2 = 0;
	Il2CppObject * V_3 = NULL;
	{
		ObjectU5BU5D_t2843939325* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, (String_t*)_stringLiteral4007973390, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Predicate_1_t3905400288 * L_2 = ___match1;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_3 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_3, (String_t*)_stringLiteral461028519, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		ObjectU5BU5D_t2843939325* L_4 = ___array0;
		V_1 = (ObjectU5BU5D_t2843939325*)L_4;
		V_2 = (int32_t)0;
		goto IL_0045;
	}

IL_002b:
	{
		ObjectU5BU5D_t2843939325* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		V_0 = (Il2CppObject *)L_8;
		Predicate_1_t3905400288 * L_9 = ___match1;
		Il2CppObject * L_10 = V_0;
		NullCheck((Predicate_1_t3905400288 *)L_9);
		bool L_11 = ((  bool (*) (Predicate_1_t3905400288 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Predicate_1_t3905400288 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_11)
		{
			goto IL_0041;
		}
	}
	{
		Il2CppObject * L_12 = V_0;
		return L_12;
	}

IL_0041:
	{
		int32_t L_13 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0045:
	{
		int32_t L_14 = V_2;
		ObjectU5BU5D_t2843939325* L_15 = V_1;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_002b;
		}
	}
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_3));
		Il2CppObject * L_16 = V_3;
		return L_16;
	}
}
// T System.Array::FindLast<System.Object>(T[],System.Predicate`1<T>)
extern "C"  Il2CppObject * Array_FindLast_TisIl2CppObject_m1088586648_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t2843939325* ___array0, Predicate_1_t3905400288 * ___match1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_FindLast_TisIl2CppObject_m1088586648_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		ObjectU5BU5D_t2843939325* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, (String_t*)_stringLiteral4007973390, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Predicate_1_t3905400288 * L_2 = ___match1;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_3 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_3, (String_t*)_stringLiteral461028519, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		ObjectU5BU5D_t2843939325* L_4 = ___array0;
		NullCheck(L_4);
		V_0 = (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))-(int32_t)1));
		goto IL_004b;
	}

IL_002d:
	{
		Predicate_1_t3905400288 * L_5 = ___match1;
		ObjectU5BU5D_t2843939325* L_6 = ___array0;
		int32_t L_7 = V_0;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck((Predicate_1_t3905400288 *)L_5);
		bool L_10 = ((  bool (*) (Predicate_1_t3905400288 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Predicate_1_t3905400288 *)L_5, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_10)
		{
			goto IL_0047;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_11 = ___array0;
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		return L_14;
	}

IL_0047:
	{
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_15-(int32_t)1));
	}

IL_004b:
	{
		int32_t L_16 = V_0;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_002d;
		}
	}
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_17 = V_1;
		return L_17;
	}
}
// T System.Array::InternalArray__get_Item<HighlightingSystem.Highlighter/RendererCache/Data>(System.Int32)
extern "C"  Data_t1588725102  Array_InternalArray__get_Item_TisData_t1588725102_m1153206052_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisData_t1588725102_m1153206052_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Data_t1588725102  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Data_t1588725102 *)(&V_0));
		Data_t1588725102  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32)
extern "C"  TableRange_t3332867892  Array_InternalArray__get_Item_TisTableRange_t3332867892_m1483480711_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTableRange_t3332867892_m1483480711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TableRange_t3332867892  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (TableRange_t3332867892 *)(&V_0));
		TableRange_t3332867892  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisClientCertificateType_t1004704908_m2297379651_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisClientCertificateType_t1004704908_m2297379651_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisClientCertificateType_t1004704909_m2297379652_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisClientCertificateType_t1004704909_m2297379652_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Mono.Security.Uri/UriScheme>(System.Int32)
extern "C"  UriScheme_t2867806342  Array_InternalArray__get_Item_TisUriScheme_t2867806342_m2246384759_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUriScheme_t2867806342_m2246384759_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UriScheme_t2867806342  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (UriScheme_t2867806342 *)(&V_0));
		UriScheme_t2867806342  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Mono.Xml.XPath.DTMXPathAttributeNode2>(System.Int32)
extern "C"  DTMXPathAttributeNode2_t3707096872  Array_InternalArray__get_Item_TisDTMXPathAttributeNode2_t3707096872_m2076492393_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisDTMXPathAttributeNode2_t3707096872_m2076492393_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DTMXPathAttributeNode2_t3707096872  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (DTMXPathAttributeNode2_t3707096872 *)(&V_0));
		DTMXPathAttributeNode2_t3707096872  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Mono.Xml.XPath.DTMXPathAttributeNode2>(System.Int32)
extern "C"  DTMXPathAttributeNode2_t3707096873  Array_InternalArray__get_Item_TisDTMXPathAttributeNode2_t3707096873_m2076492394_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisDTMXPathAttributeNode2_t3707096873_m2076492394_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DTMXPathAttributeNode2_t3707096873  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (DTMXPathAttributeNode2_t3707096873 *)(&V_0));
		DTMXPathAttributeNode2_t3707096873  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Mono.Xml.XPath.DTMXPathLinkedNode2>(System.Int32)
extern "C"  DTMXPathLinkedNode2_t3353097823  Array_InternalArray__get_Item_TisDTMXPathLinkedNode2_t3353097823_m3898909110_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisDTMXPathLinkedNode2_t3353097823_m3898909110_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DTMXPathLinkedNode2_t3353097823  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (DTMXPathLinkedNode2_t3353097823 *)(&V_0));
		DTMXPathLinkedNode2_t3353097823  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Mono.Xml.XPath.DTMXPathLinkedNode2>(System.Int32)
extern "C"  DTMXPathLinkedNode2_t3353097824  Array_InternalArray__get_Item_TisDTMXPathLinkedNode2_t3353097824_m3898909111_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisDTMXPathLinkedNode2_t3353097824_m3898909111_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DTMXPathLinkedNode2_t3353097824  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (DTMXPathLinkedNode2_t3353097824 *)(&V_0));
		DTMXPathLinkedNode2_t3353097824  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Mono.Xml.XPath.DTMXPathNamespaceNode2>(System.Int32)
extern "C"  DTMXPathNamespaceNode2_t1119120712  Array_InternalArray__get_Item_TisDTMXPathNamespaceNode2_t1119120712_m318820268_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisDTMXPathNamespaceNode2_t1119120712_m318820268_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DTMXPathNamespaceNode2_t1119120712  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (DTMXPathNamespaceNode2_t1119120712 *)(&V_0));
		DTMXPathNamespaceNode2_t1119120712  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Mono.Xml.XPath.DTMXPathNamespaceNode2>(System.Int32)
extern "C"  DTMXPathNamespaceNode2_t1119120713  Array_InternalArray__get_Item_TisDTMXPathNamespaceNode2_t1119120713_m318820269_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisDTMXPathNamespaceNode2_t1119120713_m318820269_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DTMXPathNamespaceNode2_t1119120713  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (DTMXPathNamespaceNode2_t1119120713 *)(&V_0));
		DTMXPathNamespaceNode2_t1119120713  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Mono.Xml.Xsl.Attribute>(System.Int32)
extern "C"  Attribute_t270895445  Array_InternalArray__get_Item_TisAttribute_t270895445_m4280563089_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisAttribute_t270895445_m4280563089_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Attribute_t270895445  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Attribute_t270895445 *)(&V_0));
		Attribute_t270895445  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Mono.Xml2.XmlTextReader/TagName>(System.Int32)
extern "C"  TagName_t2891256255  Array_InternalArray__get_Item_TisTagName_t2891256255_m926239580_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTagName_t2891256255_m926239580_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TagName_t2891256255  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (TagName_t2891256255 *)(&V_0));
		TagName_t2891256255  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<PresetSelector/Preset>(System.Int32)
extern "C"  Preset_t2972107632  Array_InternalArray__get_Item_TisPreset_t2972107632_m2524355772_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisPreset_t2972107632_m2524355772_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Preset_t2972107632  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Preset_t2972107632 *)(&V_0));
		Preset_t2972107632  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<SetAnimArray/SetAnimInfo>(System.Int32)
extern "C"  SetAnimInfo_t2127528194  Array_InternalArray__get_Item_TisSetAnimInfo_t2127528194_m1640025244_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisSetAnimInfo_t2127528194_m1640025244_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SetAnimInfo_t2127528194  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (SetAnimInfo_t2127528194 *)(&V_0));
		SetAnimInfo_t2127528194  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.ArraySegment`1<System.Byte>>(System.Int32)
extern "C"  ArraySegment_1_t283560987  Array_InternalArray__get_Item_TisArraySegment_1_t283560987_m1350465735_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisArraySegment_1_t283560987_m1350465735_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ArraySegment_1_t283560987  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ArraySegment_1_t283560987 *)(&V_0));
		ArraySegment_1_t283560987  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Boolean>(System.Int32)
extern "C"  bool Array_InternalArray__get_Item_TisBoolean_t97287965_m1407010309_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisBoolean_t97287965_m1407010309_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (bool*)(&V_0));
		bool L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Byte>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisByte_t1134296376_m3566214066_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisByte_t1134296376_m3566214066_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint8_t V_0 = 0x0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (uint8_t*)(&V_0));
		uint8_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Char>(System.Int32)
extern "C"  Il2CppChar Array_InternalArray__get_Item_TisChar_t3634460470_m324132692_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisChar_t3634460470_m324132692_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppChar V_0 = 0x0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Il2CppChar*)(&V_0));
		Il2CppChar L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.DictionaryEntry>(System.Int32)
extern "C"  DictionaryEntry_t3123975638  Array_InternalArray__get_Item_TisDictionaryEntry_t3123975638_m479537688_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisDictionaryEntry_t3123975638_m479537688_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DictionaryEntry_t3123975638  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (DictionaryEntry_t3123975638 *)(&V_0));
		DictionaryEntry_t3123975638  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.HashSet`1/Link<System.Int32>>(System.Int32)
extern "C"  Link_t3080106562  Array_InternalArray__get_Item_TisLink_t3080106562_m1916144364_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisLink_t3080106562_m1916144364_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Link_t3080106562  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Link_t3080106562 *)(&V_0));
		Link_t3080106562  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.HashSet`1/Link<System.Object>>(System.Int32)
extern "C"  Link_t3209266973  Array_InternalArray__get_Item_TisLink_t3209266973_m1574224299_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisLink_t3209266973_m1574224299_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Link_t3209266973  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Link_t3209266973 *)(&V_0));
		Link_t3209266973  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>(System.Int32)
extern "C"  KeyValuePair_2_t4237331251  Array_InternalArray__get_Item_TisKeyValuePair_2_t4237331251_m1101612129_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t4237331251_m1101612129_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t4237331251  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t4237331251 *)(&V_0));
		KeyValuePair_2_t4237331251  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t71524366  Array_InternalArray__get_Item_TisKeyValuePair_2_t71524366_m252172060_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t71524366_m252172060_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t71524366  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t71524366 *)(&V_0));
		KeyValuePair_2_t71524366  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>(System.Int32)
extern "C"  KeyValuePair_2_t3568047141  Array_InternalArray__get_Item_TisKeyValuePair_2_t3568047141_m192728881_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t3568047141_m192728881_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3568047141  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t3568047141 *)(&V_0));
		KeyValuePair_2_t3568047141  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>>(System.Int32)
extern "C"  KeyValuePair_2_t2387291312  Array_InternalArray__get_Item_TisKeyValuePair_2_t2387291312_m2065480692_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t2387291312_m2065480692_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t2387291312  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t2387291312 *)(&V_0));
		KeyValuePair_2_t2387291312  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>(System.Int32)
extern "C"  KeyValuePair_2_t4188143612  Array_InternalArray__get_Item_TisKeyValuePair_2_t4188143612_m3177769408_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t4188143612_m3177769408_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t4188143612  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t4188143612 *)(&V_0));
		KeyValuePair_2_t4188143612  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>(System.Int32)
extern "C"  KeyValuePair_2_t3842366416  Array_InternalArray__get_Item_TisKeyValuePair_2_t3842366416_m3937535230_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t3842366416_m3937535230_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3842366416  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t3842366416 *)(&V_0));
		KeyValuePair_2_t3842366416  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.DateTime>>(System.Int32)
extern "C"  KeyValuePair_2_t3188640940  Array_InternalArray__get_Item_TisKeyValuePair_2_t3188640940_m541164372_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t3188640940_m541164372_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3188640940  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t3188640940 *)(&V_0));
		KeyValuePair_2_t3188640940  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>(System.Int32)
extern "C"  KeyValuePair_2_t2401056908  Array_InternalArray__get_Item_TisKeyValuePair_2_t2401056908_m3647027688_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t2401056908_m3647027688_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t2401056908  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t2401056908 *)(&V_0));
		KeyValuePair_2_t2401056908  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t2530217319  Array_InternalArray__get_Item_TisKeyValuePair_2_t2530217319_m2886833132_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t2530217319_m2886833132_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t2530217319  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t2530217319 *)(&V_0));
		KeyValuePair_2_t2530217319  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>(System.Int32)
extern "C"  KeyValuePair_2_t1627836113  Array_InternalArray__get_Item_TisKeyValuePair_2_t1627836113_m1686737554_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t1627836113_m1686737554_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t1627836113  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t1627836113 *)(&V_0));
		KeyValuePair_2_t1627836113  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>(System.Int32)
extern "C"  KeyValuePair_2_t1377593753  Array_InternalArray__get_Item_TisKeyValuePair_2_t1377593753_m3379274744_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t1377593753_m3379274744_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t1377593753  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t1377593753 *)(&V_0));
		KeyValuePair_2_t1377593753  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>(System.Int32)
extern "C"  KeyValuePair_2_t2969503080  Array_InternalArray__get_Item_TisKeyValuePair_2_t2969503080_m3564523187_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t2969503080_m3564523187_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t2969503080  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t2969503080 *)(&V_0));
		KeyValuePair_2_t2969503080  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t4068375620  Array_InternalArray__get_Item_TisKeyValuePair_2_t4068375620_m428999009_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t4068375620_m428999009_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t4068375620  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t4068375620 *)(&V_0));
		KeyValuePair_2_t4068375620  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>>(System.Int32)
extern "C"  KeyValuePair_2_t3558069120  Array_InternalArray__get_Item_TisKeyValuePair_2_t3558069120_m1688560335_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t3558069120_m1688560335_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3558069120  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t3558069120 *)(&V_0));
		KeyValuePair_2_t3558069120  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>>(System.Int32)
extern "C"  KeyValuePair_2_t2989632341  Array_InternalArray__get_Item_TisKeyValuePair_2_t2989632341_m3852954896_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t2989632341_m3852954896_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t2989632341  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t2989632341 *)(&V_0));
		KeyValuePair_2_t2989632341  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>>(System.Int32)
extern "C"  KeyValuePair_2_t2477808976  Array_InternalArray__get_Item_TisKeyValuePair_2_t2477808976_m2299162451_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t2477808976_m2299162451_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t2477808976  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t2477808976 *)(&V_0));
		KeyValuePair_2_t2477808976  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.Link>(System.Int32)
extern "C"  Link_t544317964  Array_InternalArray__get_Item_TisLink_t544317964_m1669566993_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisLink_t544317964_m1669566993_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Link_t544317964  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Link_t544317964 *)(&V_0));
		Link_t544317964  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Hashtable/Slot>(System.Int32)
extern "C"  Slot_t3975888750  Array_InternalArray__get_Item_TisSlot_t3975888750_m905303097_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisSlot_t3975888750_m905303097_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Slot_t3975888750  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Slot_t3975888750 *)(&V_0));
		Slot_t3975888750  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.SortedList/Slot>(System.Int32)
extern "C"  Slot_t384495010  Array_InternalArray__get_Item_TisSlot_t384495010_m2861978404_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisSlot_t384495010_m2861978404_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Slot_t384495010  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Slot_t384495010 *)(&V_0));
		Slot_t384495010  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
extern "C"  DateTime_t3738529785  Array_InternalArray__get_Item_TisDateTime_t3738529785_m623181444_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisDateTime_t3738529785_m623181444_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DateTime_t3738529785  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (DateTime_t3738529785 *)(&V_0));
		DateTime_t3738529785  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Decimal>(System.Int32)
extern "C"  Decimal_t2948259380  Array_InternalArray__get_Item_TisDecimal_t2948259380_m3511003792_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisDecimal_t2948259380_m3511003792_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Decimal_t2948259380  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Decimal_t2948259380 *)(&V_0));
		Decimal_t2948259380  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Double>(System.Int32)
extern "C"  double Array_InternalArray__get_Item_TisDouble_t594665363_m850827605_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisDouble_t594665363_m850827605_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	double V_0 = 0.0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (double*)(&V_0));
		double L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Drawing.Color>(System.Int32)
extern "C"  Color_t1869934208  Array_InternalArray__get_Item_TisColor_t1869934208_m1908199522_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisColor_t1869934208_m1908199522_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t1869934208  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Color_t1869934208 *)(&V_0));
		Color_t1869934208  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Drawing.Icon/IconDirEntry>(System.Int32)
extern "C"  IconDirEntry_t3003987536  Array_InternalArray__get_Item_TisIconDirEntry_t3003987536_m2088850479_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisIconDirEntry_t3003987536_m2088850479_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IconDirEntry_t3003987536  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (IconDirEntry_t3003987536 *)(&V_0));
		IconDirEntry_t3003987536  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Drawing.Icon/IconImage>(System.Int32)
extern "C"  IconImage_t1835934191  Array_InternalArray__get_Item_TisIconImage_t1835934191_m4181697865_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisIconImage_t1835934191_m4181697865_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IconImage_t1835934191  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (IconImage_t1835934191 *)(&V_0));
		IconImage_t1835934191  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Int16>(System.Int32)
extern "C"  int16_t Array_InternalArray__get_Item_TisInt16_t2552820387_m76930473_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisInt16_t2552820387_m76930473_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int16_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int16_t*)(&V_0));
		int16_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Int32>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisInt32_t2950945753_m714868479_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisInt32_t2950945753_m714868479_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Int64>(System.Int32)
extern "C"  int64_t Array_InternalArray__get_Item_TisInt64_t3736567304_m3562990826_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisInt64_t3736567304_m3562990826_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int64_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int64_t*)(&V_0));
		int64_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.IntPtr>(System.Int32)
extern "C"  IntPtr_t Array_InternalArray__get_Item_TisIntPtr_t_m784054003_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisIntPtr_t_m784054003_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (IntPtr_t*)(&V_0));
		IntPtr_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Object>(System.Int32)
extern "C"  Il2CppObject * Array_InternalArray__get_Item_TisIl2CppObject_m3347010206_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisIl2CppObject_m3347010206_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Il2CppObject **)(&V_0));
		Il2CppObject * L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeNamedArgument>(System.Int32)
extern "C"  CustomAttributeNamedArgument_t287865710  Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t287865710_m2282658220_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t287865710_m2282658220_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CustomAttributeNamedArgument_t287865710  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (CustomAttributeNamedArgument_t287865710 *)(&V_0));
		CustomAttributeNamedArgument_t287865710  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeTypedArgument>(System.Int32)
extern "C"  CustomAttributeTypedArgument_t2723150157  Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t2723150157_m2639399822_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t2723150157_m2639399822_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CustomAttributeTypedArgument_t2723150157  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (CustomAttributeTypedArgument_t2723150157 *)(&V_0));
		CustomAttributeTypedArgument_t2723150157  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32)
extern "C"  LabelData_t360167391  Array_InternalArray__get_Item_TisLabelData_t360167391_m1054702781_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisLabelData_t360167391_m1054702781_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LabelData_t360167391  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (LabelData_t360167391 *)(&V_0));
		LabelData_t360167391  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32)
extern "C"  LabelFixup_t858502054  Array_InternalArray__get_Item_TisLabelFixup_t858502054_m3276643490_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisLabelFixup_t858502054_m3276643490_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LabelFixup_t858502054  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (LabelFixup_t858502054 *)(&V_0));
		LabelFixup_t858502054  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILTokenInfo>(System.Int32)
extern "C"  ILTokenInfo_t2325775114  Array_InternalArray__get_Item_TisILTokenInfo_t2325775114_m3110830457_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisILTokenInfo_t2325775114_m3110830457_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ILTokenInfo_t2325775114  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ILTokenInfo_t2325775114 *)(&V_0));
		ILTokenInfo_t2325775114  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.Label>(System.Int32)
extern "C"  Label_t2281661643  Array_InternalArray__get_Item_TisLabel_t2281661643_m2294767982_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisLabel_t2281661643_m2294767982_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Label_t2281661643  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Label_t2281661643 *)(&V_0));
		Label_t2281661643  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.MonoResource>(System.Int32)
extern "C"  MonoResource_t4103430009  Array_InternalArray__get_Item_TisMonoResource_t4103430009_m2937222811_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisMonoResource_t4103430009_m2937222811_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MonoResource_t4103430009  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (MonoResource_t4103430009 *)(&V_0));
		MonoResource_t4103430009  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.RefEmitPermissionSet>(System.Int32)
extern "C"  RefEmitPermissionSet_t484390987  Array_InternalArray__get_Item_TisRefEmitPermissionSet_t484390987_m1505876205_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRefEmitPermissionSet_t484390987_m1505876205_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RefEmitPermissionSet_t484390987  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (RefEmitPermissionSet_t484390987 *)(&V_0));
		RefEmitPermissionSet_t484390987  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.ParameterModifier>(System.Int32)
extern "C"  ParameterModifier_t1461694466  Array_InternalArray__get_Item_TisParameterModifier_t1461694466_m29553316_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisParameterModifier_t1461694466_m29553316_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ParameterModifier_t1461694466  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ParameterModifier_t1461694466 *)(&V_0));
		ParameterModifier_t1461694466  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32)
extern "C"  ResourceCacheItem_t51292791  Array_InternalArray__get_Item_TisResourceCacheItem_t51292791_m1306056717_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisResourceCacheItem_t51292791_m1306056717_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ResourceCacheItem_t51292791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ResourceCacheItem_t51292791 *)(&V_0));
		ResourceCacheItem_t51292791  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceInfo>(System.Int32)
extern "C"  ResourceInfo_t2872965302  Array_InternalArray__get_Item_TisResourceInfo_t2872965302_m3865610257_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisResourceInfo_t2872965302_m3865610257_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ResourceInfo_t2872965302  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ResourceInfo_t2872965302 *)(&V_0));
		ResourceInfo_t2872965302  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Runtime.Serialization.EnumMemberInfo>(System.Int32)
extern "C"  EnumMemberInfo_t3072296154  Array_InternalArray__get_Item_TisEnumMemberInfo_t3072296154_m3120872482_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisEnumMemberInfo_t3072296154_m3120872482_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EnumMemberInfo_t3072296154  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (EnumMemberInfo_t3072296154 *)(&V_0));
		EnumMemberInfo_t3072296154  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisTypeTag_t3541821701_m4208350471_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTypeTag_t3541821701_m4208350471_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint8_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (uint8_t*)(&V_0));
		uint8_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.SByte>(System.Int32)
extern "C"  int8_t Array_InternalArray__get_Item_TisSByte_t1669577662_m2349608172_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisSByte_t1669577662_m2349608172_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int8_t V_0 = 0x0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int8_t*)(&V_0));
		int8_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Security.Cryptography.X509Certificates.X509ChainStatus>(System.Int32)
extern "C"  X509ChainStatus_t133602714  Array_InternalArray__get_Item_TisX509ChainStatus_t133602714_m2237651489_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisX509ChainStatus_t133602714_m2237651489_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	X509ChainStatus_t133602714  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (X509ChainStatus_t133602714 *)(&V_0));
		X509ChainStatus_t133602714  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Single>(System.Int32)
extern "C"  float Array_InternalArray__get_Item_TisSingle_t1397266774_m1672589487_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisSingle_t1397266774_m1672589487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (float*)(&V_0));
		float L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.TermInfoStrings>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisTermInfoStrings_t290279955_m1174084225_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTermInfoStrings_t290279955_m1174084225_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Text.RegularExpressions.Mark>(System.Int32)
extern "C"  Mark_t3471605523  Array_InternalArray__get_Item_TisMark_t3471605523_m3397473850_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisMark_t3471605523_m3397473850_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Mark_t3471605523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Mark_t3471605523 *)(&V_0));
		Mark_t3471605523  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
extern "C"  TimeSpan_t881159249  Array_InternalArray__get_Item_TisTimeSpan_t881159249_m1885583191_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTimeSpan_t881159249_m1885583191_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TimeSpan_t881159249  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (TimeSpan_t881159249 *)(&V_0));
		TimeSpan_t881159249  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.TypeCode>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisTypeCode_t2987224087_m3502052715_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTypeCode_t2987224087_m3502052715_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.UInt16>(System.Int32)
extern "C"  uint16_t Array_InternalArray__get_Item_TisUInt16_t2177724958_m3601205466_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUInt16_t2177724958_m3601205466_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint16_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (uint16_t*)(&V_0));
		uint16_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.UInt32>(System.Int32)
extern "C"  uint32_t Array_InternalArray__get_Item_TisUInt32_t2560061978_m1955195035_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUInt32_t2560061978_m1955195035_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (uint32_t*)(&V_0));
		uint32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.UInt64>(System.Int32)
extern "C"  uint64_t Array_InternalArray__get_Item_TisUInt64_t4134040092_m129291315_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUInt64_t4134040092_m129291315_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint64_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (uint64_t*)(&V_0));
		uint64_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Uri/UriScheme>(System.Int32)
extern "C"  UriScheme_t722425697  Array_InternalArray__get_Item_TisUriScheme_t722425697_m2816273040_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUriScheme_t722425697_m2816273040_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UriScheme_t722425697  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (UriScheme_t722425697 *)(&V_0));
		UriScheme_t722425697  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Web.Compilation.AssemblyBuilder/CodeUnit>(System.Int32)
extern "C"  CodeUnit_t1519973372  Array_InternalArray__get_Item_TisCodeUnit_t1519973372_m4096517762_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisCodeUnit_t1519973372_m4096517762_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CodeUnit_t1519973372  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (CodeUnit_t1519973372 *)(&V_0));
		CodeUnit_t1519973372  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Web.UI.HtmlTextWriter/AddedAttr>(System.Int32)
extern "C"  AddedAttr_t2359971688  Array_InternalArray__get_Item_TisAddedAttr_t2359971688_m1570912593_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisAddedAttr_t2359971688_m1570912593_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AddedAttr_t2359971688  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (AddedAttr_t2359971688 *)(&V_0));
		AddedAttr_t2359971688  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Web.UI.HtmlTextWriter/AddedStyle>(System.Int32)
extern "C"  AddedStyle_t611321135  Array_InternalArray__get_Item_TisAddedStyle_t611321135_m3480009420_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisAddedStyle_t611321135_m3480009420_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AddedStyle_t611321135  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (AddedStyle_t611321135 *)(&V_0));
		AddedStyle_t611321135  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Web.UI.HtmlTextWriter/AddedTag>(System.Int32)
extern "C"  AddedTag_t1198678936  Array_InternalArray__get_Item_TisAddedTag_t1198678936_m2674902006_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisAddedTag_t1198678936_m2674902006_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AddedTag_t1198678936  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (AddedTag_t1198678936 *)(&V_0));
		AddedTag_t1198678936  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Xml.XmlNamespaceManager/NsDecl>(System.Int32)
extern "C"  NsDecl_t3938094415  Array_InternalArray__get_Item_TisNsDecl_t3938094415_m3184366930_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisNsDecl_t3938094415_m3184366930_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NsDecl_t3938094415  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (NsDecl_t3938094415 *)(&V_0));
		NsDecl_t3938094415  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Xml.XmlNamespaceManager/NsScope>(System.Int32)
extern "C"  NsScope_t3958624705  Array_InternalArray__get_Item_TisNsScope_t3958624705_m3796816205_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisNsScope_t3958624705_m3796816205_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NsScope_t3958624705  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (NsScope_t3958624705 *)(&V_0));
		NsScope_t3958624705  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Xml.XmlSpace>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisXmlSpace_t3324193251_m4247271528_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisXmlSpace_t3324193251_m4247271528_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Xml.XPath.XPathResultType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisXPathResultType_t2828988488_m305960836_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisXPathResultType_t2828988488_m305960836_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<TypewriterEffect/FadeEntry>(System.Int32)
extern "C"  FadeEntry_t639421133  Array_InternalArray__get_Item_TisFadeEntry_t639421133_m3938258723_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisFadeEntry_t639421133_m3938258723_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FadeEntry_t639421133  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (FadeEntry_t639421133 *)(&V_0));
		FadeEntry_t639421133  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UICamera/DepthEntry>(System.Int32)
extern "C"  DepthEntry_t628749918  Array_InternalArray__get_Item_TisDepthEntry_t628749918_m111472233_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisDepthEntry_t628749918_m111472233_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DepthEntry_t628749918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (DepthEntry_t628749918 *)(&V_0));
		DepthEntry_t628749918  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.AnimatorClipInfo>(System.Int32)
extern "C"  AnimatorClipInfo_t3156717155  Array_InternalArray__get_Item_TisAnimatorClipInfo_t3156717155_m2815898241_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisAnimatorClipInfo_t3156717155_m2815898241_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AnimatorClipInfo_t3156717155  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (AnimatorClipInfo_t3156717155 *)(&V_0));
		AnimatorClipInfo_t3156717155  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Bounds>(System.Int32)
extern "C"  Bounds_t2266837910  Array_InternalArray__get_Item_TisBounds_t2266837910_m2942110546_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisBounds_t2266837910_m2942110546_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Bounds_t2266837910  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Bounds_t2266837910 *)(&V_0));
		Bounds_t2266837910  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Color>(System.Int32)
extern "C"  Color_t2555686324  Array_InternalArray__get_Item_TisColor_t2555686324_m2985413820_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisColor_t2555686324_m2985413820_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2555686324  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Color_t2555686324 *)(&V_0));
		Color_t2555686324  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Color32>(System.Int32)
extern "C"  Color32_t2600501292  Array_InternalArray__get_Item_TisColor32_t2600501292_m1325986122_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisColor32_t2600501292_m1325986122_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color32_t2600501292  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Color32_t2600501292 *)(&V_0));
		Color32_t2600501292  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.ContactPoint>(System.Int32)
extern "C"  ContactPoint_t3758755253  Array_InternalArray__get_Item_TisContactPoint_t3758755253_m2489897608_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisContactPoint_t3758755253_m2489897608_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ContactPoint_t3758755253  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ContactPoint_t3758755253 *)(&V_0));
		ContactPoint_t3758755253  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.ContactPoint2D>(System.Int32)
extern "C"  ContactPoint2D_t3390240644  Array_InternalArray__get_Item_TisContactPoint2D_t3390240644_m1483798952_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisContactPoint2D_t3390240644_m1483798952_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ContactPoint2D_t3390240644  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ContactPoint2D_t3390240644 *)(&V_0));
		ContactPoint2D_t3390240644  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.EventSystems.RaycastResult>(System.Int32)
extern "C"  RaycastResult_t3360306849  Array_InternalArray__get_Item_TisRaycastResult_t3360306849_m1872700081_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRaycastResult_t3360306849_m1872700081_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastResult_t3360306849  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (RaycastResult_t3360306849 *)(&V_0));
		RaycastResult_t3360306849  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.jvalue>(System.Int32)
extern "C"  jvalue_t1372148875  Array_InternalArray__get_Item_Tisjvalue_t1372148875_m327974056_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_Tisjvalue_t1372148875_m327974056_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	jvalue_t1372148875  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (jvalue_t1372148875 *)(&V_0));
		jvalue_t1372148875  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.KeyCode>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisKeyCode_t2599294277_m2562599561_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyCode_t2599294277_m2562599561_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Keyframe>(System.Int32)
extern "C"  Keyframe_t4206410242  Array_InternalArray__get_Item_TisKeyframe_t4206410242_m27698365_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyframe_t4206410242_m27698365_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Keyframe_t4206410242  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Keyframe_t4206410242 *)(&V_0));
		Keyframe_t4206410242  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Plane>(System.Int32)
extern "C"  Plane_t1000493321  Array_InternalArray__get_Item_TisPlane_t1000493321_m2362885170_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisPlane_t1000493321_m2362885170_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Plane_t1000493321  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Plane_t1000493321 *)(&V_0));
		Plane_t1000493321  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.RaycastHit>(System.Int32)
extern "C"  RaycastHit_t1056001966  Array_InternalArray__get_Item_TisRaycastHit_t1056001966_m3352067444_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRaycastHit_t1056001966_m3352067444_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit_t1056001966  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (RaycastHit_t1056001966 *)(&V_0));
		RaycastHit_t1056001966  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.RaycastHit2D>(System.Int32)
extern "C"  RaycastHit2D_t2279581989  Array_InternalArray__get_Item_TisRaycastHit2D_t2279581989_m2440275162_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRaycastHit2D_t2279581989_m2440275162_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit2D_t2279581989  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (RaycastHit2D_t2279581989 *)(&V_0));
		RaycastHit2D_t2279581989  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Rect>(System.Int32)
extern "C"  Rect_t2360479859  Array_InternalArray__get_Item_TisRect_t2360479859_m1464655811_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRect_t2360479859_m1464655811_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t2360479859  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Rect_t2360479859 *)(&V_0));
		Rect_t2360479859  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.RenderBuffer>(System.Int32)
extern "C"  RenderBuffer_t586150500  Array_InternalArray__get_Item_TisRenderBuffer_t586150500_m686729538_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRenderBuffer_t586150500_m686729538_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RenderBuffer_t586150500  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (RenderBuffer_t586150500 *)(&V_0));
		RenderBuffer_t586150500  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Rendering.RenderTargetIdentifier>(System.Int32)
extern "C"  RenderTargetIdentifier_t2079184500  Array_InternalArray__get_Item_TisRenderTargetIdentifier_t2079184500_m4172643256_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRenderTargetIdentifier_t2079184500_m4172643256_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RenderTargetIdentifier_t2079184500  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (RenderTargetIdentifier_t2079184500 *)(&V_0));
		RenderTargetIdentifier_t2079184500  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.RenderTextureFormat>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisRenderTextureFormat_t962350765_m925159806_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRenderTextureFormat_t962350765_m925159806_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.SendMouseEvents/HitInfo>(System.Int32)
extern "C"  HitInfo_t3229609740  Array_InternalArray__get_Item_TisHitInfo_t3229609740_m2260995172_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisHitInfo_t3229609740_m2260995172_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HitInfo_t3229609740  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (HitInfo_t3229609740 *)(&V_0));
		HitInfo_t3229609740  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>(System.Int32)
extern "C"  GcAchievementData_t675222246  Array_InternalArray__get_Item_TisGcAchievementData_t675222246_m2680268485_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisGcAchievementData_t675222246_m2680268485_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GcAchievementData_t675222246  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (GcAchievementData_t675222246 *)(&V_0));
		GcAchievementData_t675222246  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>(System.Int32)
extern "C"  GcScoreData_t2125309831  Array_InternalArray__get_Item_TisGcScoreData_t2125309831_m174676143_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisGcScoreData_t2125309831_m174676143_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GcScoreData_t2125309831  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (GcScoreData_t2125309831 *)(&V_0));
		GcScoreData_t2125309831  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.TextEditor/TextEditOp>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisTextEditOp_t1927482598_m730697523_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTextEditOp_t1927482598_m730697523_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Touch>(System.Int32)
extern "C"  Touch_t1921856868  Array_InternalArray__get_Item_TisTouch_t1921856868_m1955772797_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTouch_t1921856868_m1955772797_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Touch_t1921856868  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Touch_t1921856868 *)(&V_0));
		Touch_t1921856868  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.UI.InputField/ContentType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisContentType_t1787303396_m421427711_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisContentType_t1787303396_m421427711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.UICharInfo>(System.Int32)
extern "C"  UICharInfo_t75501106  Array_InternalArray__get_Item_TisUICharInfo_t75501106_m1797321427_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUICharInfo_t75501106_m1797321427_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UICharInfo_t75501106  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (UICharInfo_t75501106 *)(&V_0));
		UICharInfo_t75501106  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.UILineInfo>(System.Int32)
extern "C"  UILineInfo_t4195266810  Array_InternalArray__get_Item_TisUILineInfo_t4195266810_m1305614921_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUILineInfo_t4195266810_m1305614921_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UILineInfo_t4195266810  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (UILineInfo_t4195266810 *)(&V_0));
		UILineInfo_t4195266810  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.UIVertex>(System.Int32)
extern "C"  UIVertex_t4057497605  Array_InternalArray__get_Item_TisUIVertex_t4057497605_m289307453_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUIVertex_t4057497605_m289307453_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UIVertex_t4057497605  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (UIVertex_t4057497605 *)(&V_0));
		UIVertex_t4057497605  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Vector2>(System.Int32)
extern "C"  Vector2_t2156229523  Array_InternalArray__get_Item_TisVector2_t2156229523_m2502961026_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisVector2_t2156229523_m2502961026_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Vector2_t2156229523 *)(&V_0));
		Vector2_t2156229523  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Vector3>(System.Int32)
extern "C"  Vector3_t3722313464  Array_InternalArray__get_Item_TisVector3_t3722313464_m2720091419_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisVector3_t3722313464_m2720091419_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Vector3_t3722313464 *)(&V_0));
		Vector3_t3722313464  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Vector4>(System.Int32)
extern "C"  Vector4_t3319028937  Array_InternalArray__get_Item_TisVector4_t3319028937_m1117939728_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisVector4_t3319028937_m1117939728_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector4_t3319028937  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Vector4_t3319028937 *)(&V_0));
		Vector4_t3319028937  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.WebCamDevice>(System.Int32)
extern "C"  WebCamDevice_t1322781432  Array_InternalArray__get_Item_TisWebCamDevice_t1322781432_m2046557297_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisWebCamDevice_t1322781432_m2046557297_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	WebCamDevice_t1322781432  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (WebCamDevice_t1322781432 *)(&V_0));
		WebCamDevice_t1322781432  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityStandardAssets.CinematicEffects.DepthOfField/QualitySettings>(System.Int32)
extern "C"  QualitySettings_t1379802392  Array_InternalArray__get_Item_TisQualitySettings_t1379802392_m2691165501_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisQualitySettings_t1379802392_m2691165501_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	QualitySettings_t1379802392  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (QualitySettings_t1379802392 *)(&V_0));
		QualitySettings_t1379802392  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityStandardAssets.CinematicEffects.FXAA/Preset>(System.Int32)
extern "C"  Preset_t736195574  Array_InternalArray__get_Item_TisPreset_t736195574_m203402203_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisPreset_t736195574_m203402203_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Preset_t736195574  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Preset_t736195574 *)(&V_0));
		Preset_t736195574  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter/Frame>(System.Int32)
extern "C"  Frame_t3511111948  Array_InternalArray__get_Item_TisFrame_t3511111948_m2708263693_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisFrame_t3511111948_m2708263693_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Frame_t3511111948  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Frame_t3511111948 *)(&V_0));
		Frame_t3511111948  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityStandardAssets.CinematicEffects.SMAA/QualitySettings>(System.Int32)
extern "C"  QualitySettings_t1869387270  Array_InternalArray__get_Item_TisQualitySettings_t1869387270_m3375905464_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisQualitySettings_t1869387270_m3375905464_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	QualitySettings_t1869387270  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (QualitySettings_t1869387270 *)(&V_0));
		QualitySettings_t1869387270  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.CameraDevice/CameraField>(System.Int32)
extern "C"  CameraField_t1483002240  Array_InternalArray__get_Item_TisCameraField_t1483002240_m4229449498_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisCameraField_t1483002240_m4229449498_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CameraField_t1483002240  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (CameraField_t1483002240 *)(&V_0));
		CameraField_t1483002240  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.EyewearDevice/EyewearCalibrationReading>(System.Int32)
extern "C"  EyewearCalibrationReading_t664929988  Array_InternalArray__get_Item_TisEyewearCalibrationReading_t664929988_m608053677_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisEyewearCalibrationReading_t664929988_m608053677_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EyewearCalibrationReading_t664929988  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (EyewearCalibrationReading_t664929988 *)(&V_0));
		EyewearCalibrationReading_t664929988  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>(System.Int32)
extern "C"  PoseAgeEntry_t2181165958  Array_InternalArray__get_Item_TisPoseAgeEntry_t2181165958_m1672129510_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisPoseAgeEntry_t2181165958_m1672129510_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PoseAgeEntry_t2181165958  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (PoseAgeEntry_t2181165958 *)(&V_0));
		PoseAgeEntry_t2181165958  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.HoloLensExtendedTrackingManager/PoseInfo>(System.Int32)
extern "C"  PoseInfo_t1612729179  Array_InternalArray__get_Item_TisPoseInfo_t1612729179_m3253150969_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisPoseInfo_t1612729179_m3253150969_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PoseInfo_t1612729179  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (PoseInfo_t1612729179 *)(&V_0));
		PoseInfo_t1612729179  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.Image/PIXEL_FORMAT>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisPIXEL_FORMAT_t3209881435_m1056154199_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisPIXEL_FORMAT_t3209881435_m1056154199_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.RectangleData>(System.Int32)
extern "C"  RectangleData_t1039179782  Array_InternalArray__get_Item_TisRectangleData_t1039179782_m3235823909_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRectangleData_t1039179782_m3235823909_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RectangleData_t1039179782  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (RectangleData_t1039179782 *)(&V_0));
		RectangleData_t1039179782  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.TargetFinder/TargetSearchResult>(System.Int32)
extern "C"  TargetSearchResult_t3441982613  Array_InternalArray__get_Item_TisTargetSearchResult_t3441982613_m1391463760_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTargetSearchResult_t3441982613_m1391463760_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TargetSearchResult_t3441982613  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (TargetSearchResult_t3441982613 *)(&V_0));
		TargetSearchResult_t3441982613  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.TrackableBehaviour/Status>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisStatus_t1100905814_m1160951035_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisStatus_t1100905814_m1160951035_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManager/TrackableIdPair>(System.Int32)
extern "C"  TrackableIdPair_t4227350457  Array_InternalArray__get_Item_TisTrackableIdPair_t4227350457_m281251552_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTrackableIdPair_t4227350457_m281251552_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TrackableIdPair_t4227350457  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (TrackableIdPair_t4227350457 *)(&V_0));
		TrackableIdPair_t4227350457  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/PropData>(System.Int32)
extern "C"  PropData_t489181770  Array_InternalArray__get_Item_TisPropData_t489181770_m770296646_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisPropData_t489181770_m770296646_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PropData_t489181770  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (PropData_t489181770 *)(&V_0));
		PropData_t489181770  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>(System.Int32)
extern "C"  SmartTerrainRevisionData_t2744445717  Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t2744445717_m4103853089_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t2744445717_m4103853089_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SmartTerrainRevisionData_t2744445717  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (SmartTerrainRevisionData_t2744445717 *)(&V_0));
		SmartTerrainRevisionData_t2744445717  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/SurfaceData>(System.Int32)
extern "C"  SurfaceData_t1300926610  Array_InternalArray__get_Item_TisSurfaceData_t1300926610_m3000210253_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisSurfaceData_t1300926610_m3000210253_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SurfaceData_t1300926610  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (SurfaceData_t1300926610 *)(&V_0));
		SurfaceData_t1300926610  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/TrackableResultData>(System.Int32)
extern "C"  TrackableResultData_t3800049328  Array_InternalArray__get_Item_TisTrackableResultData_t3800049328_m112364720_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTrackableResultData_t3800049328_m112364720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TrackableResultData_t3800049328  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (TrackableResultData_t3800049328 *)(&V_0));
		TrackableResultData_t3800049328  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/VirtualButtonData>(System.Int32)
extern "C"  VirtualButtonData_t2901758114  Array_InternalArray__get_Item_TisVirtualButtonData_t2901758114_m352267582_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisVirtualButtonData_t2901758114_m352267582_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VirtualButtonData_t2901758114  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (VirtualButtonData_t2901758114 *)(&V_0));
		VirtualButtonData_t2901758114  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/VuMarkTargetData>(System.Int32)
extern "C"  VuMarkTargetData_t3925829072  Array_InternalArray__get_Item_TisVuMarkTargetData_t3925829072_m4035236093_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisVuMarkTargetData_t3925829072_m4035236093_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VuMarkTargetData_t3925829072  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (VuMarkTargetData_t3925829072 *)(&V_0));
		VuMarkTargetData_t3925829072  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/VuMarkTargetResultData>(System.Int32)
extern "C"  VuMarkTargetResultData_t1414459591  Array_InternalArray__get_Item_TisVuMarkTargetResultData_t1414459591_m3260416437_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisVuMarkTargetResultData_t1414459591_m3260416437_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VuMarkTargetResultData_t1414459591  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (VuMarkTargetResultData_t1414459591 *)(&V_0));
		VuMarkTargetResultData_t1414459591  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/WordData>(System.Int32)
extern "C"  WordData_t2883825721  Array_InternalArray__get_Item_TisWordData_t2883825721_m4217774079_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisWordData_t2883825721_m4217774079_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	WordData_t2883825721  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (WordData_t2883825721 *)(&V_0));
		WordData_t2883825721  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/WordResultData>(System.Int32)
extern "C"  WordResultData_t1835118957  Array_InternalArray__get_Item_TisWordResultData_t1835118957_m2197241955_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisWordResultData_t1835118957_m2197241955_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	WordResultData_t1835118957  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (WordResultData_t1835118957 *)(&V_0));
		WordResultData_t1835118957  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.WebCamProfile/ProfileData>(System.Int32)
extern "C"  ProfileData_t3519391925  Array_InternalArray__get_Item_TisProfileData_t3519391925_m544952548_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisProfileData_t3519391925_m544952548_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ProfileData_t3519391925  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m21610649((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_2 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_2, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ProfileData_t3519391925 *)(&V_0));
		ProfileData_t3519391925  L_4 = V_0;
		return L_4;
	}
}
// T System.Collections.Generic.KeyedByTypeCollection`1<System.Object>::Find<System.Object>()
extern "C"  Il2CppObject * KeyedByTypeCollection_1_Find_TisIl2CppObject_m727987262_gshared (KeyedByTypeCollection_1_t1429017627 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyedByTypeCollection_1_Find_TisIl2CppObject_m727987262_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((Collection_1_t2024462082 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (Collection_1_t2024462082 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Collection_1_t2024462082 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_1 = (Il2CppObject*)L_0;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0034;
		}

IL_000c:
		{
			Il2CppObject* L_1 = V_1;
			NullCheck((Il2CppObject*)L_1);
			Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_1);
			V_0 = (Il2CppObject *)L_2;
			Il2CppObject * L_3 = V_0;
			if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->rgctx_data, 0))))
			{
				goto IL_0034;
			}
		}

IL_0023:
		{
			Il2CppObject * L_4 = V_0;
			V_2 = (Il2CppObject *)((Il2CppObject *)Castclass(L_4, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
			IL2CPP_LEAVE(0x59, FINALLY_0044);
		}

IL_0034:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck((Il2CppObject *)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_003f:
		{
			IL2CPP_LEAVE(0x4F, FINALLY_0044);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0044;
	}

FINALLY_0044:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_0048;
			}
		}

IL_0047:
		{
			IL2CPP_END_FINALLY(68)
		}

IL_0048:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			IL2CPP_END_FINALLY(68)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(68)
	{
		IL2CPP_JUMP_TBL(0x59, IL_0059)
		IL2CPP_JUMP_TBL(0x4F, IL_004f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_004f:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_3));
		Il2CppObject * L_9 = V_3;
		return L_9;
	}

IL_0059:
	{
		Il2CppObject * L_10 = V_2;
		return L_10;
	}
}
// T System.IdentityModel.Tokens.GenericXmlSecurityToken::CreateKeyIdentifierClause<System.Object>()
extern "C"  Il2CppObject * GenericXmlSecurityToken_CreateKeyIdentifierClause_TisIl2CppObject_m932525054_gshared (GenericXmlSecurityToken_t395812252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GenericXmlSecurityToken_CreateKeyIdentifierClause_TisIl2CppObject_m932525054_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t3489357830 * L_0 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// T System.IdentityModel.Tokens.KerberosRequestorSecurityToken::CreateKeyIdentifierClause<System.Object>()
extern "C"  Il2CppObject * KerberosRequestorSecurityToken_CreateKeyIdentifierClause_TisIl2CppObject_m2465289282_gshared (KerberosRequestorSecurityToken_t1189367464 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KerberosRequestorSecurityToken_CreateKeyIdentifierClause_TisIl2CppObject_m2465289282_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t3489357830 * L_0 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// T System.IdentityModel.Tokens.RsaSecurityToken::CreateKeyIdentifierClause<System.Object>()
extern "C"  Il2CppObject * RsaSecurityToken_CreateKeyIdentifierClause_TisIl2CppObject_m3998194118_gshared (RsaSecurityToken_t2003951325 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RsaSecurityToken_CreateKeyIdentifierClause_TisIl2CppObject_m3998194118_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t3489357830 * L_0 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// T System.IdentityModel.Tokens.SamlSecurityToken::CreateKeyIdentifierClause<System.Object>()
extern "C"  Il2CppObject * SamlSecurityToken_CreateKeyIdentifierClause_TisIl2CppObject_m723444784_gshared (SamlSecurityToken_t1930669850 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SamlSecurityToken_CreateKeyIdentifierClause_TisIl2CppObject_m723444784_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t3489357830 * L_0 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// T System.IdentityModel.Tokens.SecurityToken::CreateKeyIdentifierClause<System.Object>()
extern "C"  Il2CppObject * SecurityToken_CreateKeyIdentifierClause_TisIl2CppObject_m3879203870_gshared (SecurityToken_t1271873540 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SecurityToken_CreateKeyIdentifierClause_TisIl2CppObject_m3879203870_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t3489357830 * L_0 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// T System.IdentityModel.Tokens.X509SecurityToken::CreateKeyIdentifierClause<System.Object>()
extern "C"  Il2CppObject * X509SecurityToken_CreateKeyIdentifierClause_TisIl2CppObject_m332784898_gshared (X509SecurityToken_t1311826287 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (X509SecurityToken_CreateKeyIdentifierClause_TisIl2CppObject_m332784898_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Type_t * L_1 = V_0;
		Type_t * L_2 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(X509ThumbprintKeyIdentifierClause_t1082272133_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_1) == ((Il2CppObject*)(Type_t *)L_2))))
		{
			goto IL_002c;
		}
	}
	{
		X509Certificate2_t714049126 * L_3 = (X509Certificate2_t714049126 *)__this->get_cert_0();
		X509ThumbprintKeyIdentifierClause_t1082272133 * L_4 = (X509ThumbprintKeyIdentifierClause_t1082272133 *)il2cpp_codegen_object_new(X509ThumbprintKeyIdentifierClause_t1082272133_il2cpp_TypeInfo_var);
		X509ThumbprintKeyIdentifierClause__ctor_m618984855(L_4, (X509Certificate2_t714049126 *)L_3, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_4, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_002c:
	{
		Type_t * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(X509IssuerSerialKeyIdentifierClause_t3285382384_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_5) == ((Il2CppObject*)(Type_t *)L_6))))
		{
			goto IL_004d;
		}
	}
	{
		X509Certificate2_t714049126 * L_7 = (X509Certificate2_t714049126 *)__this->get_cert_0();
		X509IssuerSerialKeyIdentifierClause_t3285382384 * L_8 = (X509IssuerSerialKeyIdentifierClause_t3285382384 *)il2cpp_codegen_object_new(X509IssuerSerialKeyIdentifierClause_t3285382384_il2cpp_TypeInfo_var);
		X509IssuerSerialKeyIdentifierClause__ctor_m1675252971(L_8, (X509Certificate2_t714049126 *)L_7, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_8, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_004d:
	{
		Type_t * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(X509RawDataKeyIdentifierClause_t329336726_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_9) == ((Il2CppObject*)(Type_t *)L_10))))
		{
			goto IL_006e;
		}
	}
	{
		X509Certificate2_t714049126 * L_11 = (X509Certificate2_t714049126 *)__this->get_cert_0();
		X509RawDataKeyIdentifierClause_t329336726 * L_12 = (X509RawDataKeyIdentifierClause_t329336726 *)il2cpp_codegen_object_new(X509RawDataKeyIdentifierClause_t329336726_il2cpp_TypeInfo_var);
		X509RawDataKeyIdentifierClause__ctor_m2770603042(L_12, (X509Certificate2_t714049126 *)L_11, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_12, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_006e:
	{
		Type_t * L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Format_m2844511972(NULL /*static, unused*/, (String_t*)_stringLiteral686316168, (Il2CppObject *)L_13, /*hidden argument*/NULL);
		NotSupportedException_t1314879016 * L_15 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2494070935(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}
}
// T System.Runtime.Serialization.KnownTypeCollection::GetAttribute<System.Object>(System.Reflection.MemberInfo)
extern "C"  Il2CppObject * KnownTypeCollection_GetAttribute_TisIl2CppObject_m197020807_gshared (KnownTypeCollection_t3509697551 * __this, MemberInfo_t * ___mi0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KnownTypeCollection_GetAttribute_TisIl2CppObject_m197020807_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	{
		MemberInfo_t * L_0 = ___mi0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		ObjectU5BU5D_t2843939325* L_2 = VirtFuncInvoker2< ObjectU5BU5D_t2843939325*, Type_t *, bool >::Invoke(14 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, (MemberInfo_t *)L_0, (Type_t *)L_1, (bool)0);
		V_0 = (ObjectU5BU5D_t2843939325*)L_2;
		ObjectU5BU5D_t2843939325* L_3 = V_0;
		NullCheck(L_3);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))
		{
			goto IL_0025;
		}
	}
	{
		G_B3_0 = ((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_002d;
	}

IL_0025:
	{
		ObjectU5BU5D_t2843939325* L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = 0;
		Il2CppObject * L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		G_B3_0 = ((Il2CppObject *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_002d:
	{
		return G_B3_0;
	}
}
// T System.Runtime.Serialization.TypeExtensions::GetCustomAttribute<System.Object>(System.Type,System.Boolean)
extern "C"  Il2CppObject * TypeExtensions_GetCustomAttribute_TisIl2CppObject_m1592309407_gshared (Il2CppObject * __this /* static, unused */, Type_t * ___type0, bool ___inherit1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetCustomAttribute_TisIl2CppObject_m1592309407_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * G_B4_0 = NULL;
	{
		Type_t * L_0 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_2 = ___inherit1;
		NullCheck((MemberInfo_t *)L_0);
		ObjectU5BU5D_t2843939325* L_3 = VirtFuncInvoker2< ObjectU5BU5D_t2843939325*, Type_t *, bool >::Invoke(14 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, (MemberInfo_t *)L_0, (Type_t *)L_1, (bool)L_2);
		V_0 = (ObjectU5BU5D_t2843939325*)L_3;
		ObjectU5BU5D_t2843939325* L_4 = V_0;
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_5 = V_0;
		NullCheck(L_5);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_6 = V_0;
		NullCheck(L_6);
		int32_t L_7 = 0;
		Il2CppObject * L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		G_B4_0 = ((Il2CppObject *)Castclass(L_8, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_0037;
	}

IL_002e:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_9 = V_1;
		G_B4_0 = L_9;
	}

IL_0037:
	{
		return G_B4_0;
	}
}
// T System.ServiceModel.Channels.AsymmetricSecurityBindingElement::GetProperty<System.Object>(System.ServiceModel.Channels.BindingContext)
extern "C"  Il2CppObject * AsymmetricSecurityBindingElement_GetProperty_TisIl2CppObject_m2388565458_gshared (AsymmetricSecurityBindingElement_t2673164393 * __this, BindingContext_t2842489830 * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsymmetricSecurityBindingElement_GetProperty_TisIl2CppObject_m2388565458_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		BindingContext_t2842489830 * L_0 = ___context0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, (String_t*)_stringLiteral1340338390, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(ISecurityCapabilities_t4129699402_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_2) == ((Il2CppObject*)(Type_t *)L_3))))
		{
			goto IL_0036;
		}
	}
	{
		NullCheck((AsymmetricSecurityBindingElement_t2673164393 *)__this);
		AsymmetricSecurityCapabilities_t888703423 * L_4 = AsymmetricSecurityBindingElement_GetCapabilities_m330455506((AsymmetricSecurityBindingElement_t2673164393 *)__this, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_4, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0036:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_6 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IdentityVerifier_t614541315_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_5) == ((Il2CppObject*)(Type_t *)L_6))))
		{
			goto IL_0055;
		}
	}
	{
		NotImplementedException_t3489357830 * L_7 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0055:
	{
		BindingContext_t2842489830 * L_8 = ___context0;
		NullCheck((BindingContext_t2842489830 *)L_8);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (BindingContext_t2842489830 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((BindingContext_t2842489830 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_9;
	}
}
// T System.ServiceModel.Channels.BinaryMessageEncodingBindingElement::GetProperty<System.Object>(System.ServiceModel.Channels.BindingContext)
extern "C"  Il2CppObject * BinaryMessageEncodingBindingElement_GetProperty_TisIl2CppObject_m3714584124_gshared (BinaryMessageEncodingBindingElement_t4154157131 * __this, BindingContext_t2842489830 * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BinaryMessageEncodingBindingElement_GetProperty_TisIl2CppObject_m3714584124_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(MessageVersion_t1958933598_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(Type_t *)L_1))))
		{
			goto IL_0025;
		}
	}
	{
		NullCheck((BinaryMessageEncodingBindingElement_t4154157131 *)__this);
		MessageVersion_t1958933598 * L_2 = BinaryMessageEncodingBindingElement_get_MessageVersion_m3795418149((BinaryMessageEncodingBindingElement_t4154157131 *)__this, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0025:
	{
		return ((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T System.ServiceModel.Channels.BindingContext::GetInnerProperty<System.Object>()
extern "C"  Il2CppObject * BindingContext_GetInnerProperty_TisIl2CppObject_m42862505_gshared (BindingContext_t2842489830 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BindingContext_GetInnerProperty_TisIl2CppObject_m42862505_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	BindingElement_t3643137745 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	Il2CppObject * G_B4_0 = NULL;
	{
		NullCheck((BindingContext_t2842489830 *)__this);
		bool L_0 = BindingContext_PrepareElements_m1563199935((BindingContext_t2842489830 *)__this, /*hidden argument*/NULL);
		V_0 = (bool)L_0;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((BindingContext_t2842489830 *)__this);
			BindingElement_t3643137745 * L_1 = BindingContext_DequeueBindingElement_m1422365611((BindingContext_t2842489830 *)__this, (bool)0, /*hidden argument*/NULL);
			V_1 = (BindingElement_t3643137745 *)L_1;
			BindingElement_t3643137745 * L_2 = V_1;
			if (L_2)
			{
				goto IL_0020;
			}
		}

IL_0015:
		{
			G_B4_0 = ((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
			goto IL_0027;
		}

IL_0020:
		{
			BindingElement_t3643137745 * L_3 = V_1;
			NullCheck((BindingElement_t3643137745 *)L_3);
			Il2CppObject * L_4 = GenericVirtFuncInvoker1< Il2CppObject *, BindingContext_t2842489830 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1), (BindingElement_t3643137745 *)L_3, (BindingContext_t2842489830 *)__this);
			G_B4_0 = L_4;
		}

IL_0027:
		{
			V_2 = (Il2CppObject *)G_B4_0;
			IL2CPP_LEAVE(0x44, FINALLY_0032);
		}

IL_002d:
		{
			; // IL_002d: leave IL_0044
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0032;
	}

FINALLY_0032:
	{ // begin finally (depth: 1)
		{
			bool L_5 = V_0;
			if (!L_5)
			{
				goto IL_0043;
			}
		}

IL_0038:
		{
			IL2CPP_RUNTIME_CLASS_INIT(BindingContext_t2842489830_il2cpp_TypeInfo_var);
			BindingElementCollection_t2456870521 * L_6 = ((BindingContext_t2842489830_StaticFields*)BindingContext_t2842489830_il2cpp_TypeInfo_var->static_fields)->get_empty_collection_0();
			__this->set_elements_3(L_6);
		}

IL_0043:
		{
			IL2CPP_END_FINALLY(50)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(50)
	{
		IL2CPP_JUMP_TBL(0x44, IL_0044)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0044:
	{
		Il2CppObject * L_7 = V_2;
		return L_7;
	}
}
// T System.ServiceModel.Channels.BindingElementCollection::Find<System.Object>()
extern "C"  Il2CppObject * BindingElementCollection_Find_TisIl2CppObject_m774808656_gshared (BindingElementCollection_t2456870521 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BindingElementCollection_Find_TisIl2CppObject_m774808656_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BindingElement_t3643137745 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((Collection_1_t2587493663 *)__this);
		Il2CppObject* L_0 = Collection_1_GetEnumerator_m2650847642((Collection_1_t2587493663 *)__this, /*hidden argument*/Collection_1_GetEnumerator_m2650847642_MethodInfo_var);
		V_1 = (Il2CppObject*)L_0;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002a;
		}

IL_000c:
		{
			Il2CppObject* L_1 = V_1;
			NullCheck((Il2CppObject*)L_1);
			BindingElement_t3643137745 * L_2 = InterfaceFuncInvoker0< BindingElement_t3643137745 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.ServiceModel.Channels.BindingElement>::get_Current() */, IEnumerator_1_t4075708213_il2cpp_TypeInfo_var, (Il2CppObject*)L_1);
			V_0 = (BindingElement_t3643137745 *)L_2;
			BindingElement_t3643137745 * L_3 = V_0;
			if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->rgctx_data, 0))))
			{
				goto IL_002a;
			}
		}

IL_001e:
		{
			BindingElement_t3643137745 * L_4 = V_0;
			V_2 = (Il2CppObject *)((Il2CppObject *)Castclass(L_4, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
			IL2CPP_LEAVE(0x4F, FINALLY_003a);
		}

IL_002a:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck((Il2CppObject *)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0035:
		{
			IL2CPP_LEAVE(0x45, FINALLY_003a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_003a;
	}

FINALLY_003a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_003e;
			}
		}

IL_003d:
		{
			IL2CPP_END_FINALLY(58)
		}

IL_003e:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			IL2CPP_END_FINALLY(58)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(58)
	{
		IL2CPP_JUMP_TBL(0x4F, IL_004f)
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0045:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_3));
		Il2CppObject * L_9 = V_3;
		return L_9;
	}

IL_004f:
	{
		Il2CppObject * L_10 = V_2;
		return L_10;
	}
}
// T System.ServiceModel.Channels.ChannelListenerBase::GetProperty<System.Object>()
extern "C"  Il2CppObject * ChannelListenerBase_GetProperty_TisIl2CppObject_m1410449028_gshared (ChannelListenerBase_t990592377 * __this, const MethodInfo* method)
{
	Il2CppObject * G_B3_0 = NULL;
	{
		KeyedByTypeCollection_1_t1429017627 * L_0 = (KeyedByTypeCollection_1_t1429017627 *)__this->get_properties_10();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		KeyedByTypeCollection_1_t1429017627 * L_1 = (KeyedByTypeCollection_1_t1429017627 *)__this->get_properties_10();
		NullCheck((KeyedByTypeCollection_1_t1429017627 *)L_1);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (KeyedByTypeCollection_1_t1429017627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((KeyedByTypeCollection_1_t1429017627 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		G_B3_0 = L_2;
		goto IL_0021;
	}

IL_001b:
	{
		G_B3_0 = ((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0021:
	{
		return G_B3_0;
	}
}
// T System.ServiceModel.Channels.HttpTransportBindingElement::GetProperty<System.Object>(System.ServiceModel.Channels.BindingContext)
extern "C"  Il2CppObject * HttpTransportBindingElement_GetProperty_TisIl2CppObject_m2685399948_gshared (HttpTransportBindingElement_t2392894562 * __this, BindingContext_t2842489830 * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HttpTransportBindingElement_GetProperty_TisIl2CppObject_m2685399948_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(ISecurityCapabilities_t4129699402_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(Type_t *)L_1))))
		{
			goto IL_0025;
		}
	}
	{
		HttpBindingProperties_t3982995109 * L_2 = (HttpBindingProperties_t3982995109 *)il2cpp_codegen_object_new(HttpBindingProperties_t3982995109_il2cpp_TypeInfo_var);
		HttpBindingProperties__ctor_m3977539646(L_2, (HttpTransportBindingElement_t2392894562 *)__this, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0025:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_4 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IBindingDeliveryCapabilities_t3355695844_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_3) == ((Il2CppObject*)(Type_t *)L_4))))
		{
			goto IL_004a;
		}
	}
	{
		HttpBindingProperties_t3982995109 * L_5 = (HttpBindingProperties_t3982995109 *)il2cpp_codegen_object_new(HttpBindingProperties_t3982995109_il2cpp_TypeInfo_var);
		HttpBindingProperties__ctor_m3977539646(L_5, (HttpTransportBindingElement_t2392894562 *)__this, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_004a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_7 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(TransferMode_t436880017_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7))))
		{
			goto IL_0074;
		}
	}
	{
		NullCheck((HttpTransportBindingElement_t2392894562 *)__this);
		int32_t L_8 = HttpTransportBindingElement_get_TransferMode_m2062667909((HttpTransportBindingElement_t2392894562 *)__this, /*hidden argument*/NULL);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = Box(TransferMode_t436880017_il2cpp_TypeInfo_var, &L_9);
		return ((Il2CppObject *)Castclass(L_10, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0074:
	{
		BindingContext_t2842489830 * L_11 = ___context0;
		NullCheck((TransportBindingElement_t2144904149 *)__this);
		Il2CppObject * L_12 = ((  Il2CppObject * (*) (TransportBindingElement_t2144904149 *, BindingContext_t2842489830 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((TransportBindingElement_t2144904149 *)__this, (BindingContext_t2842489830 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_12;
	}
}
// T System.ServiceModel.Channels.MessageHeaders::GetHeader<System.Object>(System.Int32)
extern "C"  Il2CppObject * MessageHeaders_GetHeader_TisIl2CppObject_m1623046356_gshared (MessageHeaders_t4050072634 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MessageHeaders_GetHeader_TisIl2CppObject_m1623046356_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DefaultMessageHeader_t3682274810 * V_0 = NULL;
	XmlDictionaryReader_t1044334689 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * G_B10_0 = NULL;
	{
		List_1_t1936922331 * L_0 = (List_1_t1936922331 *)__this->get_l_2();
		NullCheck((List_1_t1936922331 *)L_0);
		int32_t L_1 = List_1_get_Count_m14301231((List_1_t1936922331 *)L_0, /*hidden argument*/List_1_get_Count_m14301231_MethodInfo_var);
		int32_t L_2 = ___index0;
		if ((((int32_t)L_1) > ((int32_t)L_2)))
		{
			goto IL_001c;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_3 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_3, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		List_1_t1936922331 * L_4 = (List_1_t1936922331 *)__this->get_l_2();
		int32_t L_5 = ___index0;
		NullCheck((List_1_t1936922331 *)L_4);
		MessageHeaderInfo_t464847589 * L_6 = List_1_get_Item_m1657857521((List_1_t1936922331 *)L_4, (int32_t)L_5, /*hidden argument*/List_1_get_Item_m1657857521_MethodInfo_var);
		V_0 = (DefaultMessageHeader_t3682274810 *)((DefaultMessageHeader_t3682274810 *)IsInst(L_6, DefaultMessageHeader_t3682274810_il2cpp_TypeInfo_var));
		DefaultMessageHeader_t3682274810 * L_7 = V_0;
		if (!L_7)
		{
			goto IL_006a;
		}
	}
	{
		DefaultMessageHeader_t3682274810 * L_8 = V_0;
		NullCheck((DefaultMessageHeader_t3682274810 *)L_8);
		Il2CppObject * L_9 = DefaultMessageHeader_get_Value_m164191447((DefaultMessageHeader_t3682274810 *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_006a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		DefaultMessageHeader_t3682274810 * L_11 = V_0;
		NullCheck((DefaultMessageHeader_t3682274810 *)L_11);
		Il2CppObject * L_12 = DefaultMessageHeader_get_Value_m164191447((DefaultMessageHeader_t3682274810 *)L_11, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_12);
		Type_t * L_13 = Object_GetType_m88164663((Il2CppObject *)L_12, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_10);
		bool L_14 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_10, (Type_t *)L_13);
		if (!L_14)
		{
			goto IL_006a;
		}
	}
	{
		DefaultMessageHeader_t3682274810 * L_15 = V_0;
		NullCheck((DefaultMessageHeader_t3682274810 *)L_15);
		Il2CppObject * L_16 = DefaultMessageHeader_get_Value_m164191447((DefaultMessageHeader_t3682274810 *)L_15, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_16, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_006a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_17 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_18 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(EndpointAddress_t3119842923_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18))))
		{
			goto IL_00b1;
		}
	}
	{
		int32_t L_19 = ___index0;
		NullCheck((MessageHeaders_t4050072634 *)__this);
		XmlDictionaryReader_t1044334689 * L_20 = MessageHeaders_GetReaderAtHeader_m2101511628((MessageHeaders_t4050072634 *)__this, (int32_t)L_19, /*hidden argument*/NULL);
		V_1 = (XmlDictionaryReader_t1044334689 *)L_20;
		XmlDictionaryReader_t1044334689 * L_21 = V_1;
		NullCheck((XmlReader_t3121518892 *)L_21);
		int32_t L_22 = VirtFuncInvoker0< int32_t >::Invoke(22 /* System.Xml.XmlNodeType System.Xml.XmlReader::get_NodeType() */, (XmlReader_t3121518892 *)L_21);
		if ((((int32_t)L_22) == ((int32_t)1)))
		{
			goto IL_00a5;
		}
	}
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_23 = V_2;
		G_B10_0 = L_23;
		goto IL_00b0;
	}

IL_00a5:
	{
		XmlDictionaryReader_t1044334689 * L_24 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(EndpointAddress_t3119842923_il2cpp_TypeInfo_var);
		EndpointAddress_t3119842923 * L_25 = EndpointAddress_ReadFrom_m2950182928(NULL /*static, unused*/, (XmlDictionaryReader_t1044334689 *)L_24, /*hidden argument*/NULL);
		G_B10_0 = ((Il2CppObject *)Castclass(L_25, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_00b0:
	{
		return G_B10_0;
	}

IL_00b1:
	{
		int32_t L_26 = ___index0;
		int32_t L_27 = ___index0;
		NullCheck((MessageHeaders_t4050072634 *)__this);
		XmlObjectSerializer_t3967301761 * L_28 = ((  XmlObjectSerializer_t3967301761 * (*) (MessageHeaders_t4050072634 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((MessageHeaders_t4050072634 *)__this, (int32_t)L_27, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		NullCheck((MessageHeaders_t4050072634 *)__this);
		Il2CppObject * L_29 = ((  Il2CppObject * (*) (MessageHeaders_t4050072634 *, int32_t, XmlObjectSerializer_t3967301761 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)((MessageHeaders_t4050072634 *)__this, (int32_t)L_26, (XmlObjectSerializer_t3967301761 *)L_28, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		return L_29;
	}
}
// T System.ServiceModel.Channels.MessageHeaders::GetHeader<System.Object>(System.Int32,System.Runtime.Serialization.XmlObjectSerializer)
extern "C"  Il2CppObject * MessageHeaders_GetHeader_TisIl2CppObject_m1357331642_gshared (MessageHeaders_t4050072634 * __this, int32_t ___index0, XmlObjectSerializer_t3967301761 * ___serializer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MessageHeaders_GetHeader_TisIl2CppObject_m1357331642_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XmlDictionaryReader_t1044334689 * V_0 = NULL;
	{
		XmlObjectSerializer_t3967301761 * L_0 = ___serializer1;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, (String_t*)_stringLiteral3975789767, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index0;
		NullCheck((MessageHeaders_t4050072634 *)__this);
		XmlDictionaryReader_t1044334689 * L_3 = MessageHeaders_GetReaderAtHeader_m2101511628((MessageHeaders_t4050072634 *)__this, (int32_t)L_2, /*hidden argument*/NULL);
		V_0 = (XmlDictionaryReader_t1044334689 *)L_3;
		XmlObjectSerializer_t3967301761 * L_4 = ___serializer1;
		XmlDictionaryReader_t1044334689 * L_5 = V_0;
		NullCheck((XmlObjectSerializer_t3967301761 *)L_4);
		Il2CppObject * L_6 = VirtFuncInvoker2< Il2CppObject *, XmlDictionaryReader_t1044334689 *, bool >::Invoke(5 /* System.Object System.Runtime.Serialization.XmlObjectSerializer::ReadObject(System.Xml.XmlDictionaryReader,System.Boolean) */, (XmlObjectSerializer_t3967301761 *)L_4, (XmlDictionaryReader_t1044334689 *)L_5, (bool)0);
		return ((Il2CppObject *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
	}
}
// T System.ServiceModel.Channels.MsmqBindingElementBase::GetProperty<System.Object>(System.ServiceModel.Channels.BindingContext)
extern "C"  Il2CppObject * MsmqBindingElementBase_GetProperty_TisIl2CppObject_m4150449364_gshared (MsmqBindingElementBase_t3589678829 * __this, BindingContext_t2842489830 * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MsmqBindingElementBase_GetProperty_TisIl2CppObject_m4150449364_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		if (!((Il2CppObject *)IsInst(L_0, IBindingDeliveryCapabilities_t3355695844_il2cpp_TypeInfo_var)))
		{
			goto IL_001a;
		}
	}
	{
		NotImplementedException_t3489357830 * L_1 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		if (!((Il2CppObject *)IsInst(L_2, ISecurityCapabilities_t4129699402_il2cpp_TypeInfo_var)))
		{
			goto IL_0034;
		}
	}
	{
		NotImplementedException_t3489357830 * L_3 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0034:
	{
		BindingContext_t2842489830 * L_4 = ___context0;
		NullCheck((TransportBindingElement_t2144904149 *)__this);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (TransportBindingElement_t2144904149 *, BindingContext_t2842489830 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((TransportBindingElement_t2144904149 *)__this, (BindingContext_t2842489830 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_5;
	}
}
// T System.ServiceModel.Channels.MtomMessageEncodingBindingElement::GetProperty<System.Object>(System.ServiceModel.Channels.BindingContext)
extern "C"  Il2CppObject * MtomMessageEncodingBindingElement_GetProperty_TisIl2CppObject_m3684534025_gshared (MtomMessageEncodingBindingElement_t3584114940 * __this, BindingContext_t2842489830 * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MtomMessageEncodingBindingElement_GetProperty_TisIl2CppObject_m3684534025_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t3489357830 * L_0 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// T System.ServiceModel.Channels.NamedPipeTransportBindingElement::GetProperty<System.Object>(System.ServiceModel.Channels.BindingContext)
extern "C"  Il2CppObject * NamedPipeTransportBindingElement_GetProperty_TisIl2CppObject_m1785480668_gshared (NamedPipeTransportBindingElement_t654821915 * __this, BindingContext_t2842489830 * ___context0, const MethodInfo* method)
{
	{
		BindingContext_t2842489830 * L_0 = ___context0;
		NullCheck((TransportBindingElement_t2144904149 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (TransportBindingElement_t2144904149 *, BindingContext_t2842489830 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((TransportBindingElement_t2144904149 *)__this, (BindingContext_t2842489830 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
// T System.ServiceModel.Channels.PeerCustomResolverBindingElement::GetProperty<System.Object>(System.ServiceModel.Channels.BindingContext)
extern "C"  Il2CppObject * PeerCustomResolverBindingElement_GetProperty_TisIl2CppObject_m1644286882_gshared (PeerCustomResolverBindingElement_t922597067 * __this, BindingContext_t2842489830 * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PeerCustomResolverBindingElement_GetProperty_TisIl2CppObject_m1644286882_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t3489357830 * L_0 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// T System.ServiceModel.Channels.PeerTransportBindingElement::GetProperty<System.Object>(System.ServiceModel.Channels.BindingContext)
extern "C"  Il2CppObject * PeerTransportBindingElement_GetProperty_TisIl2CppObject_m695965485_gshared (PeerTransportBindingElement_t261693216 * __this, BindingContext_t2842489830 * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PeerTransportBindingElement_GetProperty_TisIl2CppObject_m695965485_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IBindingMulticastCapabilities_t1459441759_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(Type_t *)L_1))))
		{
			goto IL_0020;
		}
	}
	{
		return ((Il2CppObject *)Castclass(__this, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(ISecurityCapabilities_t4129699402_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_2) == ((Il2CppObject*)(Type_t *)L_3))))
		{
			goto IL_0040;
		}
	}
	{
		return ((Il2CppObject *)Castclass(__this, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0040:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_5 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IBindingDeliveryCapabilities_t3355695844_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5))))
		{
			goto IL_0060;
		}
	}
	{
		return ((Il2CppObject *)Castclass(__this, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0060:
	{
		BindingContext_t2842489830 * L_6 = ___context0;
		NullCheck((TransportBindingElement_t2144904149 *)__this);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (TransportBindingElement_t2144904149 *, BindingContext_t2842489830 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((TransportBindingElement_t2144904149 *)__this, (BindingContext_t2842489830 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_7;
	}
}
// T System.ServiceModel.Channels.PnrpPeerResolverBindingElement::GetProperty<System.Object>(System.ServiceModel.Channels.BindingContext)
extern "C"  Il2CppObject * PnrpPeerResolverBindingElement_GetProperty_TisIl2CppObject_m1493533106_gshared (PnrpPeerResolverBindingElement_t85468628 * __this, BindingContext_t2842489830 * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PnrpPeerResolverBindingElement_GetProperty_TisIl2CppObject_m1493533106_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t3489357830 * L_0 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// T System.ServiceModel.Channels.SecurityChannelListener`1<System.Object>::GetProperty<System.Object>()
extern "C"  Il2CppObject * SecurityChannelListener_1_GetProperty_TisIl2CppObject_m2093574432_gshared (SecurityChannelListener_1_t569455982 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SecurityChannelListener_1_GetProperty_TisIl2CppObject_m2093574432_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(MessageSecurityBindingSupport_t1904510157_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(Type_t *)L_1))))
		{
			goto IL_0025;
		}
	}
	{
		RecipientMessageSecurityBindingSupport_t1155974079 * L_2 = (RecipientMessageSecurityBindingSupport_t1155974079 *)__this->get_security_13();
		return ((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0025:
	{
		NullCheck((ChannelListenerBase_t990592377 *)__this);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (ChannelListenerBase_t990592377 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((ChannelListenerBase_t990592377 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_3;
	}
}
// T System.ServiceModel.Channels.SymmetricSecurityBindingElement::GetProperty<System.Object>(System.ServiceModel.Channels.BindingContext)
extern "C"  Il2CppObject * SymmetricSecurityBindingElement_GetProperty_TisIl2CppObject_m697793203_gshared (SymmetricSecurityBindingElement_t3733530694 * __this, BindingContext_t2842489830 * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SymmetricSecurityBindingElement_GetProperty_TisIl2CppObject_m697793203_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		BindingContext_t2842489830 * L_0 = ___context0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, (String_t*)_stringLiteral1340338390, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(ISecurityCapabilities_t4129699402_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_2) == ((Il2CppObject*)(Type_t *)L_3))))
		{
			goto IL_0036;
		}
	}
	{
		NullCheck((SymmetricSecurityBindingElement_t3733530694 *)__this);
		SymmetricSecurityCapabilities_t506905939 * L_4 = SymmetricSecurityBindingElement_GetCapabilities_m3874139475((SymmetricSecurityBindingElement_t3733530694 *)__this, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_4, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0036:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_6 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IdentityVerifier_t614541315_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_5) == ((Il2CppObject*)(Type_t *)L_6))))
		{
			goto IL_0055;
		}
	}
	{
		NotImplementedException_t3489357830 * L_7 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0055:
	{
		BindingContext_t2842489830 * L_8 = ___context0;
		NullCheck((BindingContext_t2842489830 *)L_8);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (BindingContext_t2842489830 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((BindingContext_t2842489830 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_9;
	}
}
// T System.ServiceModel.Channels.TcpTransportBindingElement::GetProperty<System.Object>(System.ServiceModel.Channels.BindingContext)
extern "C"  Il2CppObject * TcpTransportBindingElement_GetProperty_TisIl2CppObject_m1234431736_gshared (TcpTransportBindingElement_t1965552937 * __this, BindingContext_t2842489830 * ___context0, const MethodInfo* method)
{
	{
		BindingContext_t2842489830 * L_0 = ___context0;
		NullCheck((BindingContext_t2842489830 *)L_0);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (BindingContext_t2842489830 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((BindingContext_t2842489830 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
// T System.ServiceModel.Channels.TextMessageEncodingBindingElement::GetProperty<System.Object>(System.ServiceModel.Channels.BindingContext)
extern "C"  Il2CppObject * TextMessageEncodingBindingElement_GetProperty_TisIl2CppObject_m445773248_gshared (TextMessageEncodingBindingElement_t2133327734 * __this, BindingContext_t2842489830 * ___context0, const MethodInfo* method)
{
	{
		BindingContext_t2842489830 * L_0 = ___context0;
		NullCheck((BindingContext_t2842489830 *)L_0);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (BindingContext_t2842489830 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((BindingContext_t2842489830 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
// T System.ServiceModel.Channels.TransactionFlowBindingElement::GetProperty<System.Object>(System.ServiceModel.Channels.BindingContext)
extern "C"  Il2CppObject * TransactionFlowBindingElement_GetProperty_TisIl2CppObject_m137456711_gshared (TransactionFlowBindingElement_t881905444 * __this, BindingContext_t2842489830 * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransactionFlowBindingElement_GetProperty_TisIl2CppObject_m137456711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t3489357830 * L_0 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// T System.ServiceModel.Channels.TransportBindingElement::GetProperty<System.Object>(System.ServiceModel.Channels.BindingContext)
extern "C"  Il2CppObject * TransportBindingElement_GetProperty_TisIl2CppObject_m3683280528_gshared (TransportBindingElement_t2144904149 * __this, BindingContext_t2842489830 * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransportBindingElement_GetProperty_TisIl2CppObject_m3683280528_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XmlDictionaryReaderQuotas_t173030297 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(XmlDictionaryReaderQuotas_t173030297_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(Type_t *)L_1))))
		{
			goto IL_0033;
		}
	}
	{
		XmlDictionaryReaderQuotas_t173030297 * L_2 = (XmlDictionaryReaderQuotas_t173030297 *)il2cpp_codegen_object_new(XmlDictionaryReaderQuotas_t173030297_il2cpp_TypeInfo_var);
		XmlDictionaryReaderQuotas__ctor_m1273539748(L_2, /*hidden argument*/NULL);
		V_0 = (XmlDictionaryReaderQuotas_t173030297 *)L_2;
		XmlDictionaryReaderQuotas_t173030297 * L_3 = V_0;
		NullCheck((TransportBindingElement_t2144904149 *)__this);
		int64_t L_4 = VirtFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.ServiceModel.Channels.TransportBindingElement::get_MaxReceivedMessageSize() */, (TransportBindingElement_t2144904149 *)__this);
		NullCheck((XmlDictionaryReaderQuotas_t173030297 *)L_3);
		XmlDictionaryReaderQuotas_set_MaxStringContentLength_m2248955720((XmlDictionaryReaderQuotas_t173030297 *)L_3, (int32_t)(((int32_t)((int32_t)L_4))), /*hidden argument*/NULL);
		XmlDictionaryReaderQuotas_t173030297 * L_5 = V_0;
		return ((Il2CppObject *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_7 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(ChannelProtectionRequirements_t2141883287_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7))))
		{
			goto IL_0057;
		}
	}
	{
		ChannelProtectionRequirements_t2141883287 * L_8 = (ChannelProtectionRequirements_t2141883287 *)il2cpp_codegen_object_new(ChannelProtectionRequirements_t2141883287_il2cpp_TypeInfo_var);
		ChannelProtectionRequirements__ctor_m4267701572(L_8, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_8, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0057:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_10 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(MessageVersion_t1958933598_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_9) == ((Il2CppObject*)(Type_t *)L_10))))
		{
			goto IL_007b;
		}
	}
	{
		MessageVersion_t1958933598 * L_11 = MessageVersion_get_Default_m3338319686(NULL /*static, unused*/, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_11, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_007b:
	{
		BindingContext_t2842489830 * L_12 = ___context0;
		NullCheck((BindingContext_t2842489830 *)L_12);
		Il2CppObject * L_13 = ((  Il2CppObject * (*) (BindingContext_t2842489830 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((BindingContext_t2842489830 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_13;
	}
}
// T System.ServiceModel.Channels.TransportSecurityBindingElement::GetProperty<System.Object>(System.ServiceModel.Channels.BindingContext)
extern "C"  Il2CppObject * TransportSecurityBindingElement_GetProperty_TisIl2CppObject_m2606895813_gshared (TransportSecurityBindingElement_t3016354099 * __this, BindingContext_t2842489830 * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransportSecurityBindingElement_GetProperty_TisIl2CppObject_m2606895813_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t3489357830 * L_0 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// T System.ServiceModel.Channels.WSSecurityMessageHeader::Find<System.Object>()
extern "C"  Il2CppObject * WSSecurityMessageHeader_Find_TisIl2CppObject_m526510621_gshared (WSSecurityMessageHeader_t97321761 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WSSecurityMessageHeader_Find_TisIl2CppObject_m526510621_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((WSSecurityMessageHeader_t97321761 *)__this);
		Collection_1_t2024462082 * L_0 = WSSecurityMessageHeader_get_Contents_m3609380460((WSSecurityMessageHeader_t97321761 *)__this, /*hidden argument*/NULL);
		NullCheck((Collection_1_t2024462082 *)L_0);
		Il2CppObject* L_1 = Collection_1_GetEnumerator_m3456018579((Collection_1_t2024462082 *)L_0, /*hidden argument*/Collection_1_GetEnumerator_m3456018579_MethodInfo_var);
		V_1 = (Il2CppObject*)L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003e;
		}

IL_0011:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IEnumerator_1_t3512676632_il2cpp_TypeInfo_var, (Il2CppObject*)L_2);
			V_0 = (Il2CppObject *)L_3;
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_4 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
			Il2CppObject * L_5 = V_0;
			NullCheck((Il2CppObject *)L_5);
			Type_t * L_6 = Object_GetType_m88164663((Il2CppObject *)L_5, /*hidden argument*/NULL);
			NullCheck((Type_t *)L_4);
			bool L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_4, (Type_t *)L_6);
			if (!L_7)
			{
				goto IL_003e;
			}
		}

IL_0032:
		{
			Il2CppObject * L_8 = V_0;
			V_2 = (Il2CppObject *)((Il2CppObject *)Castclass(L_8, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
			IL2CPP_LEAVE(0x63, FINALLY_004e);
		}

IL_003e:
		{
			Il2CppObject* L_9 = V_1;
			NullCheck((Il2CppObject *)L_9);
			bool L_10 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
			if (L_10)
			{
				goto IL_0011;
			}
		}

IL_0049:
		{
			IL2CPP_LEAVE(0x59, FINALLY_004e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_004e;
	}

FINALLY_004e:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_11 = V_1;
			if (L_11)
			{
				goto IL_0052;
			}
		}

IL_0051:
		{
			IL2CPP_END_FINALLY(78)
		}

IL_0052:
		{
			Il2CppObject* L_12 = V_1;
			NullCheck((Il2CppObject *)L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
			IL2CPP_END_FINALLY(78)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(78)
	{
		IL2CPP_JUMP_TBL(0x63, IL_0063)
		IL2CPP_JUMP_TBL(0x59, IL_0059)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0059:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_3));
		Il2CppObject * L_13 = V_3;
		return L_13;
	}

IL_0063:
	{
		Il2CppObject * L_14 = V_2;
		return L_14;
	}
}
// T System.ServiceModel.OperationContext::GetCallbackChannel<System.Object>()
extern "C"  Il2CppObject * OperationContext_GetCallbackChannel_TisIl2CppObject_m991702261_gshared (OperationContext_t2829644788 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_channel_3();
		return ((Il2CppObject *)Castclass(L_0, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
	}
}
// T System.ServiceModel.Security.Tokens.SecurityContextSecurityToken::CreateKeyIdentifierClause<System.Object>()
extern "C"  Il2CppObject * SecurityContextSecurityToken_CreateKeyIdentifierClause_TisIl2CppObject_m359550936_gshared (SecurityContextSecurityToken_t3624779732 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SecurityContextSecurityToken_CreateKeyIdentifierClause_TisIl2CppObject_m359550936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Type_t * L_1 = V_0;
		Type_t * L_2 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(SecurityContextKeyIdentifierClause_t698951267_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_1) == ((Il2CppObject*)(Type_t *)L_2))))
		{
			goto IL_0032;
		}
	}
	{
		NullCheck((SecurityContextSecurityToken_t3624779732 *)__this);
		UniqueId_t1383576913 * L_3 = SecurityContextSecurityToken_get_ContextId_m3447042347((SecurityContextSecurityToken_t3624779732 *)__this, /*hidden argument*/NULL);
		NullCheck((SecurityContextSecurityToken_t3624779732 *)__this);
		UniqueId_t1383576913 * L_4 = SecurityContextSecurityToken_get_KeyGeneration_m1708319148((SecurityContextSecurityToken_t3624779732 *)__this, /*hidden argument*/NULL);
		SecurityContextKeyIdentifierClause_t698951267 * L_5 = (SecurityContextKeyIdentifierClause_t698951267 *)il2cpp_codegen_object_new(SecurityContextKeyIdentifierClause_t698951267_il2cpp_TypeInfo_var);
		SecurityContextKeyIdentifierClause__ctor_m366404922(L_5, (UniqueId_t1383576913 *)L_3, (UniqueId_t1383576913 *)L_4, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0032:
	{
		Type_t * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Format_m2844511972(NULL /*static, unused*/, (String_t*)_stringLiteral686316168, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		NotSupportedException_t1314879016 * L_8 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2494070935(L_8, (String_t*)L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// T System.ServiceModel.Security.Tokens.WrappedKeySecurityToken::CreateKeyIdentifierClause<System.Object>()
extern "C"  Il2CppObject * WrappedKeySecurityToken_CreateKeyIdentifierClause_TisIl2CppObject_m4062238421_gshared (WrappedKeySecurityToken_t738218815 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WrappedKeySecurityToken_CreateKeyIdentifierClause_TisIl2CppObject_m4062238421_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Format_m2844511972(NULL /*static, unused*/, (String_t*)_stringLiteral2654637398, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		NotSupportedException_t1314879016 * L_2 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2494070935(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}
}
// T UITweener::Begin<System.Object>(UnityEngine.GameObject,System.Single)
extern "C"  Il2CppObject * UITweener_Begin_TisIl2CppObject_m2692891208_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1113636619 * ___go0, float ___duration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITweener_Begin_TisIl2CppObject_m2692891208_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	ObjectU5BU5D_t2843939325* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		GameObject_t1113636619 * L_0 = ___go0;
		NullCheck((GameObject_t1113636619 *)L_0);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (GameObject_t1113636619 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((GameObject_t1113636619 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, (Object_t631007953 *)L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0081;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = (int32_t)((UITweener_t260334902 *)L_4)->get_tweenGroup_10();
		if (!L_5)
		{
			goto IL_0081;
		}
	}
	{
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		GameObject_t1113636619 * L_6 = ___go0;
		NullCheck((GameObject_t1113636619 *)L_6);
		ObjectU5BU5D_t2843939325* L_7 = ((  ObjectU5BU5D_t2843939325* (*) (GameObject_t1113636619 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((GameObject_t1113636619 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		V_1 = (ObjectU5BU5D_t2843939325*)L_7;
		V_2 = (int32_t)0;
		ObjectU5BU5D_t2843939325* L_8 = V_1;
		NullCheck(L_8);
		V_3 = (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length))));
		goto IL_007a;
	}

IL_0041:
	{
		ObjectU5BU5D_t2843939325* L_9 = V_1;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		V_0 = (Il2CppObject *)L_12;
		Il2CppObject * L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, (Object_t631007953 *)L_13, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_006f;
		}
	}
	{
		Il2CppObject * L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = (int32_t)((UITweener_t260334902 *)L_15)->get_tweenGroup_10();
		if (L_16)
		{
			goto IL_006f;
		}
	}
	{
		goto IL_0081;
	}

IL_006f:
	{
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		int32_t L_17 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_007a:
	{
		int32_t L_18 = V_2;
		int32_t L_19 = V_3;
		if ((((int32_t)L_18) < ((int32_t)L_19)))
		{
			goto IL_0041;
		}
	}

IL_0081:
	{
		Il2CppObject * L_20 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_21 = Object_op_Equality_m1454075600(NULL /*static, unused*/, (Object_t631007953 *)L_20, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00e8;
		}
	}
	{
		GameObject_t1113636619 * L_22 = ___go0;
		NullCheck((GameObject_t1113636619 *)L_22);
		Il2CppObject * L_23 = ((  Il2CppObject * (*) (GameObject_t1113636619 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)((GameObject_t1113636619 *)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		V_0 = (Il2CppObject *)L_23;
		Il2CppObject * L_24 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_25 = Object_op_Equality_m1454075600(NULL /*static, unused*/, (Object_t631007953 *)L_24, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00e8;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_26 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral3466665877);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3466665877);
		ObjectU5BU5D_t2843939325* L_27 = (ObjectU5BU5D_t2843939325*)L_26;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 4)), /*hidden argument*/NULL);
		NullCheck(L_27);
		ArrayElementTypeCheck (L_27, L_28);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_28);
		ObjectU5BU5D_t2843939325* L_29 = (ObjectU5BU5D_t2843939325*)L_27;
		NullCheck(L_29);
		ArrayElementTypeCheck (L_29, _stringLiteral684852591);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral684852591);
		ObjectU5BU5D_t2843939325* L_30 = (ObjectU5BU5D_t2843939325*)L_29;
		GameObject_t1113636619 * L_31 = ___go0;
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t1206951095_il2cpp_TypeInfo_var);
		String_t* L_32 = NGUITools_GetHierarchy_m3080800227(NULL /*static, unused*/, (GameObject_t1113636619 *)L_31, /*hidden argument*/NULL);
		NullCheck(L_30);
		ArrayElementTypeCheck (L_30, L_32);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_32);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = String_Concat_m2971454694(NULL /*static, unused*/, (ObjectU5BU5D_t2843939325*)L_30, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_34 = ___go0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m3356949141(NULL /*static, unused*/, (Il2CppObject *)L_33, (Object_t631007953 *)L_34, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_00e8:
	{
		Il2CppObject * L_35 = V_0;
		NullCheck(L_35);
		((UITweener_t260334902 *)L_35)->set_mStarted_14((bool)0);
		Il2CppObject * L_36 = V_0;
		float L_37 = ___duration1;
		NullCheck(L_36);
		((UITweener_t260334902 *)L_36)->set_duration_8(L_37);
		Il2CppObject * L_38 = V_0;
		NullCheck(L_38);
		((UITweener_t260334902 *)L_38)->set_mFactor_18((0.0f));
		Il2CppObject * L_39 = V_0;
		NullCheck((UITweener_t260334902 *)(*(&V_0)));
		float L_40 = UITweener_get_amountPerDelta_m1343789098((UITweener_t260334902 *)(*(&V_0)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_41 = fabsf((float)L_40);
		NullCheck(L_39);
		((UITweener_t260334902 *)L_39)->set_mAmountPerDelta_17(L_41);
		Il2CppObject * L_42 = V_0;
		NullCheck(L_42);
		((UITweener_t260334902 *)L_42)->set_style_4(0);
		Il2CppObject * L_43 = V_0;
		KeyframeU5BU5D_t1068524471* L_44 = (KeyframeU5BU5D_t1068524471*)((KeyframeU5BU5D_t1068524471*)SZArrayNew(KeyframeU5BU5D_t1068524471_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_44);
		Keyframe_t4206410242  L_45;
		memset(&L_45, 0, sizeof(L_45));
		Keyframe__ctor_m2938821330(&L_45, (float)(0.0f), (float)(0.0f), (float)(0.0f), (float)(1.0f), /*hidden argument*/NULL);
		(*(Keyframe_t4206410242 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_45;
		KeyframeU5BU5D_t1068524471* L_46 = (KeyframeU5BU5D_t1068524471*)L_44;
		NullCheck(L_46);
		Keyframe_t4206410242  L_47;
		memset(&L_47, 0, sizeof(L_47));
		Keyframe__ctor_m2938821330(&L_47, (float)(1.0f), (float)(1.0f), (float)(1.0f), (float)(0.0f), /*hidden argument*/NULL);
		(*(Keyframe_t4206410242 *)((L_46)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_47;
		AnimationCurve_t3046754366 * L_48 = (AnimationCurve_t3046754366 *)il2cpp_codegen_object_new(AnimationCurve_t3046754366_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m1726807957(L_48, (KeyframeU5BU5D_t1068524471*)L_46, /*hidden argument*/NULL);
		NullCheck(L_43);
		((UITweener_t260334902 *)L_43)->set_animationCurve_5(L_48);
		Il2CppObject * L_49 = V_0;
		NullCheck(L_49);
		((UITweener_t260334902 *)L_49)->set_eventReceiver_12((GameObject_t1113636619 *)NULL);
		Il2CppObject * L_50 = V_0;
		NullCheck(L_50);
		((UITweener_t260334902 *)L_50)->set_callWhenFinished_13((String_t*)NULL);
		NullCheck((Behaviour_t1437897464 *)(*(&V_0)));
		Behaviour_set_enabled_m3107225489((Behaviour_t1437897464 *)(*(&V_0)), (bool)1, /*hidden argument*/NULL);
		Il2CppObject * L_51 = V_0;
		return L_51;
	}
}
// T UnityEngine.AttributeHelperEngine::GetCustomAttributeOfType<System.Object>(System.Type)
extern "C"  Il2CppObject * AttributeHelperEngine_GetCustomAttributeOfType_TisIl2CppObject_m2601070632_gshared (Il2CppObject * __this /* static, unused */, Type_t * ___klass0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_GetCustomAttributeOfType_TisIl2CppObject_m2601070632_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	ObjectU5BU5D_t2843939325* V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Type_t * L_1 = ___klass0;
		Type_t * L_2 = V_0;
		NullCheck((MemberInfo_t *)L_1);
		ObjectU5BU5D_t2843939325* L_3 = VirtFuncInvoker2< ObjectU5BU5D_t2843939325*, Type_t *, bool >::Invoke(14 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, (MemberInfo_t *)L_1, (Type_t *)L_2, (bool)1);
		V_1 = (ObjectU5BU5D_t2843939325*)L_3;
		ObjectU5BU5D_t2843939325* L_4 = V_1;
		if (!L_4)
		{
			goto IL_0031;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_5 = V_1;
		NullCheck(L_5);
		if (!(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))))
		{
			goto IL_0031;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_6 = V_1;
		NullCheck(L_6);
		int32_t L_7 = 0;
		Il2CppObject * L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		V_2 = (Il2CppObject *)((Il2CppObject *)Castclass(L_8, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_003d;
	}

IL_0031:
	{
		V_2 = (Il2CppObject *)((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_003d;
	}

IL_003d:
	{
		Il2CppObject * L_9 = V_2;
		return L_9;
	}
}
// T UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m2735705429_gshared (Component_t1923634451 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Component_GetComponent_TisIl2CppObject_m2735705429_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CastHelper_1_t2613165452  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject * V_1 = NULL;
	{
		Initobj (CastHelper_1_t2613165452_il2cpp_TypeInfo_var, (&V_0));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		IntPtr_t* L_1 = (IntPtr_t*)(&V_0)->get_address_of_onePointerFurtherThanT_1();
		IntPtr_t L_2;
		memset(&L_2, 0, sizeof(L_2));
		IntPtr__ctor_m3384658186(&L_2, (void*)(void*)L_1, /*hidden argument*/NULL);
		NullCheck((Component_t1923634451 *)__this);
		Component_GetComponentFastPath_m476901382((Component_t1923634451 *)__this, (Type_t *)L_0, (IntPtr_t)L_2, /*hidden argument*/NULL);
		Il2CppObject * L_3 = (Il2CppObject *)(&V_0)->get_t_0();
		V_1 = (Il2CppObject *)L_3;
		goto IL_0032;
	}

IL_0032:
	{
		Il2CppObject * L_4 = V_1;
		return L_4;
	}
}
// T UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m3475884674_gshared (Component_t1923634451 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	{
		V_0 = (bool)0;
		bool L_0 = V_0;
		NullCheck((Component_t1923634451 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Component_t1923634451 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Component_t1923634451 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_1 = (Il2CppObject *)L_1;
		goto IL_0010;
	}

IL_0010:
	{
		Il2CppObject * L_2 = V_1;
		return L_2;
	}
}
// T UnityEngine.Component::GetComponentInChildren<System.Object>(System.Boolean)
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m3600364750_gshared (Component_t1923634451 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Component_GetComponentInChildren_TisIl2CppObject_m3600364750_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((Component_t1923634451 *)__this);
		Component_t1923634451 * L_2 = Component_GetComponentInChildren_m2139176229((Component_t1923634451 *)__this, (Type_t *)L_0, (bool)L_1, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_001d;
	}

IL_001d:
	{
		Il2CppObject * L_3 = V_0;
		return L_3;
	}
}
// T UnityEngine.Component::GetComponentInParent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInParent_TisIl2CppObject_m3491943679_gshared (Component_t1923634451 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Component_GetComponentInParent_TisIl2CppObject_m3491943679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck((Component_t1923634451 *)__this);
		Component_t1923634451 * L_1 = Component_GetComponentInParent_m2023105846((Component_t1923634451 *)__this, (Type_t *)L_0, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_001c;
	}

IL_001c:
	{
		Il2CppObject * L_2 = V_0;
		return L_2;
	}
}
// T UnityEngine.EventSystems.ExecuteEvents::ValidateEventData<System.Object>(UnityEngine.EventSystems.BaseEventData)
extern "C"  Il2CppObject * ExecuteEvents_ValidateEventData_TisIl2CppObject_m1594546529_gshared (Il2CppObject * __this /* static, unused */, BaseEventData_t3903027533 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExecuteEvents_ValidateEventData_TisIl2CppObject_m1594546529_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		BaseEventData_t3903027533 * L_0 = ___data0;
		if (((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->rgctx_data, 0))), IL2CPP_RGCTX_DATA(method->rgctx_data, 0))))
		{
			goto IL_003b;
		}
	}
	{
		BaseEventData_t3903027533 * L_1 = ___data0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m88164663((Il2CppObject *)L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m2556382932(NULL /*static, unused*/, (String_t*)_stringLiteral2964872255, (Il2CppObject *)L_2, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_5 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_003b:
	{
		BaseEventData_t3903027533 * L_6 = ___data0;
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 0))), IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		goto IL_0051;
	}

IL_0051:
	{
		Il2CppObject * L_7 = V_0;
		return L_7;
	}
}
// T UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m3678101806_gshared (GameObject_t1113636619 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject_AddComponent_TisIl2CppObject_m3678101806_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck((GameObject_t1113636619 *)__this);
		Component_t1923634451 * L_1 = GameObject_AddComponent_m2485158059((GameObject_t1113636619 *)__this, (Type_t *)L_0, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_0021;
	}

IL_0021:
	{
		Il2CppObject * L_2 = V_0;
		return L_2;
	}
}
// T UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2385344436_gshared (GameObject_t1113636619 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponent_TisIl2CppObject_m2385344436_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CastHelper_1_t2613165452  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject * V_1 = NULL;
	{
		Initobj (CastHelper_1_t2613165452_il2cpp_TypeInfo_var, (&V_0));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		IntPtr_t* L_1 = (IntPtr_t*)(&V_0)->get_address_of_onePointerFurtherThanT_1();
		IntPtr_t L_2;
		memset(&L_2, 0, sizeof(L_2));
		IntPtr__ctor_m3384658186(&L_2, (void*)(void*)L_1, /*hidden argument*/NULL);
		NullCheck((GameObject_t1113636619 *)__this);
		GameObject_GetComponentFastPath_m730500428((GameObject_t1113636619 *)__this, (Type_t *)L_0, (IntPtr_t)L_2, /*hidden argument*/NULL);
		Il2CppObject * L_3 = (Il2CppObject *)(&V_0)->get_t_0();
		V_1 = (Il2CppObject *)L_3;
		goto IL_0032;
	}

IL_0032:
	{
		Il2CppObject * L_4 = V_1;
		return L_4;
	}
}
// T UnityEngine.GameObject::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m3437557578_gshared (GameObject_t1113636619 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	{
		V_0 = (bool)0;
		bool L_0 = V_0;
		NullCheck((GameObject_t1113636619 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (GameObject_t1113636619 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((GameObject_t1113636619 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_1 = (Il2CppObject *)L_1;
		goto IL_0010;
	}

IL_0010:
	{
		Il2CppObject * L_2 = V_1;
		return L_2;
	}
}
// T UnityEngine.GameObject::GetComponentInChildren<System.Object>(System.Boolean)
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m1898919846_gshared (GameObject_t1113636619 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponentInChildren_TisIl2CppObject_m1898919846_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((GameObject_t1113636619 *)__this);
		Component_t1923634451 * L_2 = GameObject_GetComponentInChildren_m230563287((GameObject_t1113636619 *)__this, (Type_t *)L_0, (bool)L_1, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_001d;
	}

IL_001d:
	{
		Il2CppObject * L_3 = V_0;
		return L_3;
	}
}
// T UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C"  Il2CppObject * Object_FindObjectOfType_TisIl2CppObject_m1542987838_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_FindObjectOfType_TisIl2CppObject_m1542987838_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_t631007953 * L_1 = Object_FindObjectOfType_m1736538631(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_001b;
	}

IL_001b:
	{
		Il2CppObject * L_2 = V_0;
		return L_2;
	}
}
// T UnityEngine.Object::Instantiate<System.Object>(T)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3168325340_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___original0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_TisIl2CppObject_m3168325340_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m129989854(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)_stringLiteral2475671027, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___original0;
		Object_t631007953 * L_2 = Object_Internal_CloneSingle_m1527886285(NULL /*static, unused*/, (Object_t631007953 *)L_1, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		goto IL_0027;
	}

IL_0027:
	{
		Il2CppObject * L_3 = V_0;
		return L_3;
	}
}
// T UnityEngine.Object::Instantiate<System.Object>(T,UnityEngine.Transform)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m878221869_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___original0, Transform_t3600365921 * ___parent1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_TisIl2CppObject_m878221869_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___original0;
		Transform_t3600365921 * L_1 = ___parent1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Transform_t3600365921 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, (Transform_t3600365921 *)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_2;
		goto IL_000f;
	}

IL_000f:
	{
		Il2CppObject * L_3 = V_0;
		return L_3;
	}
}
// T UnityEngine.Object::Instantiate<System.Object>(T,UnityEngine.Transform,System.Boolean)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m864675153_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___original0, Transform_t3600365921 * ___parent1, bool ___worldPositionStays2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_TisIl2CppObject_m864675153_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___original0;
		Transform_t3600365921 * L_1 = ___parent1;
		bool L_2 = ___worldPositionStays2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_t631007953 * L_3 = Object_Instantiate_m3931415074(NULL /*static, unused*/, (Object_t631007953 *)L_0, (Transform_t3600365921 *)L_1, (bool)L_2, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(L_3, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		goto IL_0019;
	}

IL_0019:
	{
		Il2CppObject * L_4 = V_0;
		return L_4;
	}
}
// T UnityEngine.Object::Instantiate<System.Object>(T,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m1135049463_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___original0, Vector3_t3722313464  ___position1, Quaternion_t2301928331  ___rotation2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_TisIl2CppObject_m1135049463_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___original0;
		Vector3_t3722313464  L_1 = ___position1;
		Quaternion_t2301928331  L_2 = ___rotation2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_t631007953 * L_3 = Object_Instantiate_m2563497874(NULL /*static, unused*/, (Object_t631007953 *)L_0, (Vector3_t3722313464 )L_1, (Quaternion_t2301928331 )L_2, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(L_3, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		goto IL_0019;
	}

IL_0019:
	{
		Il2CppObject * L_4 = V_0;
		return L_4;
	}
}
// T UnityEngine.Object::Instantiate<System.Object>(T,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3913942063_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___original0, Vector3_t3722313464  ___position1, Quaternion_t2301928331  ___rotation2, Transform_t3600365921 * ___parent3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_TisIl2CppObject_m3913942063_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___original0;
		Vector3_t3722313464  L_1 = ___position1;
		Quaternion_t2301928331  L_2 = ___rotation2;
		Transform_t3600365921 * L_3 = ___parent3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_t631007953 * L_4 = Object_Instantiate_m3242828326(NULL /*static, unused*/, (Object_t631007953 *)L_0, (Vector3_t3722313464 )L_1, (Quaternion_t2301928331 )L_2, (Transform_t3600365921 *)L_3, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(L_4, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		goto IL_001a;
	}

IL_001a:
	{
		Il2CppObject * L_5 = V_0;
		return L_5;
	}
}
// T UnityEngine.Resources::GetBuiltinResource<System.Object>(System.String)
extern "C"  Il2CppObject * Resources_GetBuiltinResource_TisIl2CppObject_m3352626831_gshared (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Resources_GetBuiltinResource_TisIl2CppObject_m3352626831_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		String_t* L_1 = ___path0;
		Object_t631007953 * L_2 = Resources_GetBuiltinResource_m629864230(NULL /*static, unused*/, (Type_t *)L_0, (String_t*)L_1, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_001c;
	}

IL_001c:
	{
		Il2CppObject * L_3 = V_0;
		return L_3;
	}
}
// T UnityEngine.Resources::Load<System.Object>(System.String)
extern "C"  Il2CppObject * Resources_Load_TisIl2CppObject_m1502289511_gshared (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Resources_Load_TisIl2CppObject_m1502289511_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		String_t* L_0 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Object_t631007953 * L_2 = Resources_Load_m4202687077(NULL /*static, unused*/, (String_t*)L_0, (Type_t *)L_1, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_001c;
	}

IL_001c:
	{
		Il2CppObject * L_3 = V_0;
		return L_3;
	}
}
// T UnityEngine.ScriptableObject::CreateInstance<System.Object>()
extern "C"  Il2CppObject * ScriptableObject_CreateInstance_TisIl2CppObject_m3068602743_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScriptableObject_CreateInstance_TisIl2CppObject_m3068602743_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		ScriptableObject_t2528358522 * L_1 = ScriptableObject_CreateInstance_m2508048690(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_001b;
	}

IL_001b:
	{
		Il2CppObject * L_2 = V_0;
		return L_2;
	}
}
// T UnityEngine.UI.Dropdown::GetOrAddComponent<System.Object>(UnityEngine.GameObject)
extern "C"  Il2CppObject * Dropdown_GetOrAddComponent_TisIl2CppObject_m769901662_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1113636619 * ___go0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dropdown_GetOrAddComponent_TisIl2CppObject_m769901662_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		GameObject_t1113636619 * L_0 = ___go0;
		NullCheck((GameObject_t1113636619 *)L_0);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (GameObject_t1113636619 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((GameObject_t1113636619 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m487959476(NULL /*static, unused*/, (Object_t631007953 *)L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_001f;
		}
	}
	{
		GameObject_t1113636619 * L_4 = ___go0;
		NullCheck((GameObject_t1113636619 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (GameObject_t1113636619 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((GameObject_t1113636619 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_5;
	}

IL_001f:
	{
		Il2CppObject * L_6 = V_0;
		V_1 = (Il2CppObject *)L_6;
		goto IL_0026;
	}

IL_0026:
	{
		Il2CppObject * L_7 = V_1;
		return L_7;
	}
}
// T Vuforia.PremiumObjectFactory/NullPremiumObjectFactory::CreateReconstruction<System.Object>()
extern "C"  Il2CppObject * NullPremiumObjectFactory_CreateReconstruction_TisIl2CppObject_m2685558895_gshared (NullPremiumObjectFactory_t3495665550 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NullPremiumObjectFactory_CreateReconstruction_TisIl2CppObject_m2685558895_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_0 = V_0;
		return L_0;
	}
}
// T Vuforia.SmartTerrainBuilderImpl::CreateReconstruction<System.Object>()
extern "C"  Il2CppObject * SmartTerrainBuilderImpl_CreateReconstruction_TisIl2CppObject_m2589243573_gshared (SmartTerrainBuilderImpl_t1656443109 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmartTerrainBuilderImpl_CreateReconstruction_TisIl2CppObject_m2589243573_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(ReconstructionFromTarget_t3809448279_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(Type_t *)L_1))))
		{
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaWrapper_t2763746413_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = VuforiaWrapper_get_Instance_m2947582594(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_2);
		IntPtr_t L_3 = InterfaceFuncInvoker0< IntPtr_t >::Invoke(158 /* System.IntPtr Vuforia.IVuforiaWrapper::SmartTerrainBuilderCreateReconstructionFromTarget() */, IVuforiaWrapper_t2381307640_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		ReconstructionFromTargetImpl_t676137874 * L_4 = (ReconstructionFromTargetImpl_t676137874 *)il2cpp_codegen_object_new(ReconstructionFromTargetImpl_t676137874_il2cpp_TypeInfo_var);
		ReconstructionFromTargetImpl__ctor_m1359819978(L_4, (IntPtr_t)L_3, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0030:
	{
		Il2CppObject * L_5 = PremiumObjectFactory_get_Instance_m2847660535(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_5);
		Il2CppObject * L_6 = GenericInterfaceFuncInvoker0< Il2CppObject * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2), (Il2CppObject *)L_5);
		return L_6;
	}
}
// T Vuforia.TrackerManagerImpl::GetTracker<System.Object>()
extern "C"  Il2CppObject * TrackerManagerImpl_GetTracker_TisIl2CppObject_m3003825392_gshared (TrackerManagerImpl_t3998245004 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackerManagerImpl_GetTracker_TisIl2CppObject_m3003825392_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(ObjectTracker_t4177997237_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(Type_t *)L_1))))
		{
			goto IL_0027;
		}
	}
	{
		ObjectTracker_t4177997237 * L_2 = (ObjectTracker_t4177997237 *)__this->get_mObjectTracker_1();
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_4 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(TextTracker_t3950053289_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_3) == ((Il2CppObject*)(Type_t *)L_4))))
		{
			goto IL_004e;
		}
	}
	{
		TextTracker_t3950053289 * L_5 = (TextTracker_t3950053289 *)__this->get_mTextTracker_2();
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_5, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_004e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_7 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(SmartTerrainTracker_t1238706968_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7))))
		{
			goto IL_0075;
		}
	}
	{
		SmartTerrainTracker_t1238706968 * L_8 = (SmartTerrainTracker_t1238706968 *)__this->get_mSmartTerrainTracker_3();
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_8, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0075:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_10 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(DeviceTracker_t2315692373_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_9) == ((Il2CppObject*)(Type_t *)L_10)))
		{
			goto IL_00a1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_12 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(RotationalDeviceTracker_t2847210804_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_11) == ((Il2CppObject*)(Type_t *)L_12))))
		{
			goto IL_00b2;
		}
	}

IL_00a1:
	{
		DeviceTracker_t2315692373 * L_13 = (DeviceTracker_t2315692373 *)__this->get_mDeviceTracker_4();
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_13, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_00b2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2059623341(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2521874635, /*hidden argument*/NULL);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_14 = V_0;
		return L_14;
	}
}
// T Vuforia.TrackerManagerImpl::InitTracker<System.Object>()
extern "C"  Il2CppObject * TrackerManagerImpl_InitTracker_TisIl2CppObject_m917366830_gshared (TrackerManagerImpl_t3998245004 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackerManagerImpl_InitTracker_TisIl2CppObject_m917366830_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t399660591_il2cpp_TypeInfo_var);
		bool L_0 = VuforiaRuntimeUtilities_IsVuforiaEnabled_m1152377305(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_1 = V_1;
		return L_1;
	}

IL_0011:
	{
		V_0 = (bool)1;
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t399660591_il2cpp_TypeInfo_var);
		bool L_2 = VuforiaRuntimeUtilities_IsPlayMode_m4165764373(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0048;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_4 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(DeviceTracker_t2315692373_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_3) == ((Il2CppObject*)(Type_t *)L_4)))
		{
			goto IL_0046;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_6 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(RotationalDeviceTracker_t2847210804_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_5) == ((Il2CppObject*)(Type_t *)L_6))))
		{
			goto IL_0048;
		}
	}

IL_0046:
	{
		V_0 = (bool)0;
	}

IL_0048:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_007a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaWrapper_t2763746413_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = VuforiaWrapper_get_Instance_m2947582594(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TypeMapping_t3272509632_il2cpp_TypeInfo_var);
		uint16_t L_10 = TypeMapping_GetTypeID_m2138532019(NULL /*static, unused*/, (Type_t *)L_9, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		int32_t L_11 = InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(179 /* System.Int32 Vuforia.IVuforiaWrapper::TrackerManagerInitTracker(System.Int32) */, IVuforiaWrapper_t2381307640_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (int32_t)L_10);
		if (L_11)
		{
			goto IL_007a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2059623341(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral3741637024, /*hidden argument*/NULL);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_12 = V_1;
		return L_12;
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_13 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_14 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(ObjectTracker_t4177997237_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_13) == ((Il2CppObject*)(Type_t *)L_14))))
		{
			goto IL_00b4;
		}
	}
	{
		ObjectTracker_t4177997237 * L_15 = (ObjectTracker_t4177997237 *)__this->get_mObjectTracker_1();
		if (L_15)
		{
			goto IL_00a3;
		}
	}
	{
		ObjectTrackerImpl_t4028644236 * L_16 = (ObjectTrackerImpl_t4028644236 *)il2cpp_codegen_object_new(ObjectTrackerImpl_t4028644236_il2cpp_TypeInfo_var);
		ObjectTrackerImpl__ctor_m236183719(L_16, /*hidden argument*/NULL);
		__this->set_mObjectTracker_1(L_16);
	}

IL_00a3:
	{
		ObjectTracker_t4177997237 * L_17 = (ObjectTracker_t4177997237 *)__this->get_mObjectTracker_1();
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_00b4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_19 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(TextTracker_t3950053289_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_18) == ((Il2CppObject*)(Type_t *)L_19))))
		{
			goto IL_00ee;
		}
	}
	{
		TextTracker_t3950053289 * L_20 = (TextTracker_t3950053289 *)__this->get_mTextTracker_2();
		if (L_20)
		{
			goto IL_00dd;
		}
	}
	{
		TextTrackerImpl_t1410587152 * L_21 = (TextTrackerImpl_t1410587152 *)il2cpp_codegen_object_new(TextTrackerImpl_t1410587152_il2cpp_TypeInfo_var);
		TextTrackerImpl__ctor_m3064977494(L_21, /*hidden argument*/NULL);
		__this->set_mTextTracker_2(L_21);
	}

IL_00dd:
	{
		TextTracker_t3950053289 * L_22 = (TextTracker_t3950053289 *)__this->get_mTextTracker_2();
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_22, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_00ee:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_23 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_24 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(SmartTerrainTracker_t1238706968_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_23) == ((Il2CppObject*)(Type_t *)L_24))))
		{
			goto IL_0128;
		}
	}
	{
		SmartTerrainTracker_t1238706968 * L_25 = (SmartTerrainTracker_t1238706968 *)__this->get_mSmartTerrainTracker_3();
		if (L_25)
		{
			goto IL_0117;
		}
	}
	{
		SmartTerrainTrackerImpl_t651952228 * L_26 = (SmartTerrainTrackerImpl_t651952228 *)il2cpp_codegen_object_new(SmartTerrainTrackerImpl_t651952228_il2cpp_TypeInfo_var);
		SmartTerrainTrackerImpl__ctor_m2510818320(L_26, /*hidden argument*/NULL);
		__this->set_mSmartTerrainTracker_3(L_26);
	}

IL_0117:
	{
		SmartTerrainTracker_t1238706968 * L_27 = (SmartTerrainTracker_t1238706968 *)__this->get_mSmartTerrainTracker_3();
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_27, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0128:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_29 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(DeviceTracker_t2315692373_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_28) == ((Il2CppObject*)(Type_t *)L_29)))
		{
			goto IL_0154;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_30 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_31 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(RotationalDeviceTracker_t2847210804_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_30) == ((Il2CppObject*)(Type_t *)L_31))))
		{
			goto IL_018c;
		}
	}

IL_0154:
	{
		DeviceTracker_t2315692373 * L_32 = (DeviceTracker_t2315692373 *)__this->get_mDeviceTracker_4();
		if (L_32)
		{
			goto IL_017b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t399660591_il2cpp_TypeInfo_var);
		bool L_33 = VuforiaRuntimeUtilities_IsPlayMode_m4165764373(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0170;
		}
	}
	{
		RotationalPlayModeDeviceTrackerImpl_t4048275232 * L_34 = (RotationalPlayModeDeviceTrackerImpl_t4048275232 *)il2cpp_codegen_object_new(RotationalPlayModeDeviceTrackerImpl_t4048275232_il2cpp_TypeInfo_var);
		RotationalPlayModeDeviceTrackerImpl__ctor_m3977961433(L_34, /*hidden argument*/NULL);
		__this->set_mDeviceTracker_4(L_34);
		goto IL_017b;
	}

IL_0170:
	{
		RotationalDeviceTrackerImpl_t1573407597 * L_35 = (RotationalDeviceTrackerImpl_t1573407597 *)il2cpp_codegen_object_new(RotationalDeviceTrackerImpl_t1573407597_il2cpp_TypeInfo_var);
		RotationalDeviceTrackerImpl__ctor_m134363731(L_35, /*hidden argument*/NULL);
		__this->set_mDeviceTracker_4(L_35);
	}

IL_017b:
	{
		DeviceTracker_t2315692373 * L_36 = (DeviceTracker_t2315692373 *)__this->get_mDeviceTracker_4();
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_36, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_018c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2059623341(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1345897578, /*hidden argument*/NULL);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_37 = V_1;
		return L_37;
	}
}
// T[] NGUITools::FindActive<System.Object>()
extern "C"  ObjectU5BU5D_t2843939325* NGUITools_FindActive_TisIl2CppObject_m1941122218_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NGUITools_FindActive_TisIl2CppObject_m1941122218_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t1417781964* L_1 = Object_FindObjectsOfType_m2898745631(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		return ((ObjectU5BU5D_t2843939325*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T[] System.Array::FindAll<System.Object>(T[],System.Predicate`1<T>)
extern "C"  ObjectU5BU5D_t2843939325* Array_FindAll_TisIl2CppObject_m3566631088_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t2843939325* ___array0, Predicate_1_t3905400288 * ___match1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_FindAll_TisIl2CppObject_m3566631088_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ObjectU5BU5D_t2843939325* V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	ObjectU5BU5D_t2843939325* V_3 = NULL;
	int32_t V_4 = 0;
	{
		ObjectU5BU5D_t2843939325* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, (String_t*)_stringLiteral4007973390, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Predicate_1_t3905400288 * L_2 = ___match1;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_3 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_3, (String_t*)_stringLiteral461028519, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		V_0 = (int32_t)0;
		ObjectU5BU5D_t2843939325* L_4 = ___array0;
		NullCheck(L_4);
		V_1 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))));
		ObjectU5BU5D_t2843939325* L_5 = ___array0;
		V_3 = (ObjectU5BU5D_t2843939325*)L_5;
		V_4 = (int32_t)0;
		goto IL_005e;
	}

IL_0037:
	{
		ObjectU5BU5D_t2843939325* L_6 = V_3;
		int32_t L_7 = V_4;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_2 = (Il2CppObject *)L_9;
		Predicate_1_t3905400288 * L_10 = ___match1;
		Il2CppObject * L_11 = V_2;
		NullCheck((Predicate_1_t3905400288 *)L_10);
		bool L_12 = ((  bool (*) (Predicate_1_t3905400288 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Predicate_1_t3905400288 *)L_10, (Il2CppObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		if (!L_12)
		{
			goto IL_0058;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_13 = V_1;
		int32_t L_14 = V_0;
		int32_t L_15 = (int32_t)L_14;
		V_0 = (int32_t)((int32_t)((int32_t)L_15+(int32_t)1));
		Il2CppObject * L_16 = V_2;
		NullCheck(L_13);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_15), (Il2CppObject *)L_16);
	}

IL_0058:
	{
		int32_t L_17 = V_4;
		V_4 = (int32_t)((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_005e:
	{
		int32_t L_18 = V_4;
		ObjectU5BU5D_t2843939325* L_19 = V_3;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length)))))))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_20 = V_0;
		((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t2843939325**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t2843939325**)(&V_1), (int32_t)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		ObjectU5BU5D_t2843939325* L_21 = V_1;
		return L_21;
	}
}
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Object>(System.Object[])
extern "C"  ObjectU5BU5D_t2843939325* CustomAttributeData_UnboxValues_TisIl2CppObject_m160061819_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t2843939325* ___values0, const MethodInfo* method)
{
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t2843939325* L_0 = ___values0;
		NullCheck(L_0);
		V_0 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_0023;
	}

IL_0010:
	{
		ObjectU5BU5D_t2843939325* L_1 = V_0;
		int32_t L_2 = V_1;
		ObjectU5BU5D_t2843939325* L_3 = ___values0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (Il2CppObject *)((Il2CppObject *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))));
		int32_t L_7 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_8 = V_1;
		ObjectU5BU5D_t2843939325* L_9 = ___values0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_10 = V_0;
		return L_10;
	}
}
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeNamedArgument>(System.Object[])
extern "C"  CustomAttributeNamedArgumentU5BU5D_t3710464795* CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t287865710_m2244692512_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t2843939325* ___values0, const MethodInfo* method)
{
	CustomAttributeNamedArgumentU5BU5D_t3710464795* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t2843939325* L_0 = ___values0;
		NullCheck(L_0);
		V_0 = (CustomAttributeNamedArgumentU5BU5D_t3710464795*)((CustomAttributeNamedArgumentU5BU5D_t3710464795*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_0023;
	}

IL_0010:
	{
		CustomAttributeNamedArgumentU5BU5D_t3710464795* L_1 = V_0;
		int32_t L_2 = V_1;
		ObjectU5BU5D_t2843939325* L_3 = ___values0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (CustomAttributeNamedArgument_t287865710 )((*(CustomAttributeNamedArgument_t287865710 *)((CustomAttributeNamedArgument_t287865710 *)UnBox(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))))));
		int32_t L_7 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_8 = V_1;
		ObjectU5BU5D_t2843939325* L_9 = ___values0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t3710464795* L_10 = V_0;
		return L_10;
	}
}
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeTypedArgument>(System.Object[])
extern "C"  CustomAttributeTypedArgumentU5BU5D_t1465843424* CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t2723150157_m679789813_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t2843939325* ___values0, const MethodInfo* method)
{
	CustomAttributeTypedArgumentU5BU5D_t1465843424* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t2843939325* L_0 = ___values0;
		NullCheck(L_0);
		V_0 = (CustomAttributeTypedArgumentU5BU5D_t1465843424*)((CustomAttributeTypedArgumentU5BU5D_t1465843424*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_0023;
	}

IL_0010:
	{
		CustomAttributeTypedArgumentU5BU5D_t1465843424* L_1 = V_0;
		int32_t L_2 = V_1;
		ObjectU5BU5D_t2843939325* L_3 = ___values0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (CustomAttributeTypedArgument_t2723150157 )((*(CustomAttributeTypedArgument_t2723150157 *)((CustomAttributeTypedArgument_t2723150157 *)UnBox(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))))));
		int32_t L_7 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_8 = V_1;
		ObjectU5BU5D_t2843939325* L_9 = ___values0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t1465843424* L_10 = V_0;
		return L_10;
	}
}
// T[] UnityEngine.Component::GetComponents<System.Object>()
extern "C"  ObjectU5BU5D_t2843939325* Component_GetComponents_TisIl2CppObject_m539078962_gshared (Component_t1923634451 * __this, const MethodInfo* method)
{
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	{
		NullCheck((Component_t1923634451 *)__this);
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m2648350745((Component_t1923634451 *)__this, /*hidden argument*/NULL);
		NullCheck((GameObject_t1113636619 *)L_0);
		ObjectU5BU5D_t2843939325* L_1 = ((  ObjectU5BU5D_t2843939325* (*) (GameObject_t1113636619 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((GameObject_t1113636619 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (ObjectU5BU5D_t2843939325*)L_1;
		goto IL_0012;
	}

IL_0012:
	{
		ObjectU5BU5D_t2843939325* L_2 = V_0;
		return L_2;
	}
}
// T[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t2843939325* Component_GetComponentsInChildren_TisIl2CppObject_m1664944732_gshared (Component_t1923634451 * __this, const MethodInfo* method)
{
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	{
		NullCheck((Component_t1923634451 *)__this);
		ObjectU5BU5D_t2843939325* L_0 = ((  ObjectU5BU5D_t2843939325* (*) (Component_t1923634451 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Component_t1923634451 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (ObjectU5BU5D_t2843939325*)L_0;
		goto IL_000e;
	}

IL_000e:
	{
		ObjectU5BU5D_t2843939325* L_1 = V_0;
		return L_1;
	}
}
// T[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t2843939325* Component_GetComponentsInChildren_TisIl2CppObject_m2748495586_gshared (Component_t1923634451 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	{
		NullCheck((Component_t1923634451 *)__this);
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m2648350745((Component_t1923634451 *)__this, /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((GameObject_t1113636619 *)L_0);
		ObjectU5BU5D_t2843939325* L_2 = ((  ObjectU5BU5D_t2843939325* (*) (GameObject_t1113636619 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((GameObject_t1113636619 *)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (ObjectU5BU5D_t2843939325*)L_2;
		goto IL_0013;
	}

IL_0013:
	{
		ObjectU5BU5D_t2843939325* L_3 = V_0;
		return L_3;
	}
}
// T[] UnityEngine.Component::GetComponentsInParent<System.Object>()
extern "C"  ObjectU5BU5D_t2843939325* Component_GetComponentsInParent_TisIl2CppObject_m459706017_gshared (Component_t1923634451 * __this, const MethodInfo* method)
{
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	{
		NullCheck((Component_t1923634451 *)__this);
		ObjectU5BU5D_t2843939325* L_0 = ((  ObjectU5BU5D_t2843939325* (*) (Component_t1923634451 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Component_t1923634451 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (ObjectU5BU5D_t2843939325*)L_0;
		goto IL_000e;
	}

IL_000e:
	{
		ObjectU5BU5D_t2843939325* L_1 = V_0;
		return L_1;
	}
}
// T[] UnityEngine.Component::GetComponentsInParent<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t2843939325* Component_GetComponentsInParent_TisIl2CppObject_m2554470879_gshared (Component_t1923634451 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	{
		NullCheck((Component_t1923634451 *)__this);
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m2648350745((Component_t1923634451 *)__this, /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((GameObject_t1113636619 *)L_0);
		ObjectU5BU5D_t2843939325* L_2 = ((  ObjectU5BU5D_t2843939325* (*) (GameObject_t1113636619 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((GameObject_t1113636619 *)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (ObjectU5BU5D_t2843939325*)L_2;
		goto IL_0013;
	}

IL_0013:
	{
		ObjectU5BU5D_t2843939325* L_3 = V_0;
		return L_3;
	}
}
// T[] UnityEngine.GameObject::GetComponents<System.Object>()
extern "C"  ObjectU5BU5D_t2843939325* GameObject_GetComponents_TisIl2CppObject_m1405838976_gshared (GameObject_t1113636619 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponents_TisIl2CppObject_m1405838976_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck((GameObject_t1113636619 *)__this);
		Il2CppArray * L_1 = GameObject_GetComponentsInternal_m525215727((GameObject_t1113636619 *)__this, (Type_t *)L_0, (bool)1, (bool)0, (bool)1, (bool)0, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		V_0 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_0021;
	}

IL_0021:
	{
		ObjectU5BU5D_t2843939325* L_2 = V_0;
		return L_2;
	}
}
// T[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t2843939325* GameObject_GetComponentsInChildren_TisIl2CppObject_m1461871634_gshared (GameObject_t1113636619 * __this, const MethodInfo* method)
{
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	{
		NullCheck((GameObject_t1113636619 *)__this);
		ObjectU5BU5D_t2843939325* L_0 = ((  ObjectU5BU5D_t2843939325* (*) (GameObject_t1113636619 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((GameObject_t1113636619 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (ObjectU5BU5D_t2843939325*)L_0;
		goto IL_000e;
	}

IL_000e:
	{
		ObjectU5BU5D_t2843939325* L_1 = V_0;
		return L_1;
	}
}
// T[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t2843939325* GameObject_GetComponentsInChildren_TisIl2CppObject_m836045_gshared (GameObject_t1113636619 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponentsInChildren_TisIl2CppObject_m836045_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((GameObject_t1113636619 *)__this);
		Il2CppArray * L_2 = GameObject_GetComponentsInternal_m525215727((GameObject_t1113636619 *)__this, (Type_t *)L_0, (bool)1, (bool)1, (bool)L_1, (bool)0, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		V_0 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_0021;
	}

IL_0021:
	{
		ObjectU5BU5D_t2843939325* L_3 = V_0;
		return L_3;
	}
}
// T[] UnityEngine.GameObject::GetComponentsInParent<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t2843939325* GameObject_GetComponentsInParent_TisIl2CppObject_m145281020_gshared (GameObject_t1113636619 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponentsInParent_TisIl2CppObject_m145281020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((GameObject_t1113636619 *)__this);
		Il2CppArray * L_2 = GameObject_GetComponentsInternal_m525215727((GameObject_t1113636619 *)__this, (Type_t *)L_0, (bool)1, (bool)1, (bool)L_1, (bool)1, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		V_0 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		goto IL_0021;
	}

IL_0021:
	{
		ObjectU5BU5D_t2843939325* L_3 = V_0;
		return L_3;
	}
}
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<System.Object>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  ObjectU5BU5D_t2843939325* Mesh_GetAllocArrayFromChannel_TisIl2CppObject_m2282921712_gshared (Mesh_t3648964284 * __this, int32_t ___channel0, const MethodInfo* method)
{
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	{
		int32_t L_0 = ___channel0;
		int32_t L_1 = ___channel0;
		int32_t L_2 = Mesh_DefaultDimensionForChannel_m4089233615(NULL /*static, unused*/, (int32_t)L_1, /*hidden argument*/NULL);
		NullCheck((Mesh_t3648964284 *)__this);
		ObjectU5BU5D_t2843939325* L_3 = ((  ObjectU5BU5D_t2843939325* (*) (Mesh_t3648964284 *, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Mesh_t3648964284 *)__this, (int32_t)L_0, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (ObjectU5BU5D_t2843939325*)L_3;
		goto IL_0015;
	}

IL_0015:
	{
		ObjectU5BU5D_t2843939325* L_4 = V_0;
		return L_4;
	}
}
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<System.Object>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  ObjectU5BU5D_t2843939325* Mesh_GetAllocArrayFromChannel_TisIl2CppObject_m1957112185_gshared (Mesh_t3648964284 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const MethodInfo* method)
{
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	{
		NullCheck((Mesh_t3648964284 *)__this);
		bool L_0 = Mesh_get_canAccess_m2982329792((Mesh_t3648964284 *)__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_1 = ___channel0;
		NullCheck((Mesh_t3648964284 *)__this);
		bool L_2 = Mesh_HasChannel_m3577110678((Mesh_t3648964284 *)__this, (int32_t)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_3 = ___channel0;
		int32_t L_4 = ___format1;
		int32_t L_5 = ___dim2;
		NullCheck((Mesh_t3648964284 *)__this);
		Il2CppArray * L_6 = Mesh_GetAllocArrayFromChannelImpl_m1881002877((Mesh_t3648964284 *)__this, (int32_t)L_3, (int32_t)L_4, (int32_t)L_5, /*hidden argument*/NULL);
		V_0 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		goto IL_0048;
	}

IL_002d:
	{
		goto IL_003c;
	}

IL_0033:
	{
		int32_t L_7 = ___channel0;
		NullCheck((Mesh_t3648964284 *)__this);
		Mesh_PrintErrorCantAccessMesh_m868675854((Mesh_t3648964284 *)__this, (int32_t)L_7, /*hidden argument*/NULL);
	}

IL_003c:
	{
		V_0 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)0));
		goto IL_0048;
	}

IL_0048:
	{
		ObjectU5BU5D_t2843939325* L_8 = V_0;
		return L_8;
	}
}
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Color32>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  Color32U5BU5D_t3850468773* Mesh_GetAllocArrayFromChannel_TisColor32_t2600501292_m511552227_gshared (Mesh_t3648964284 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const MethodInfo* method)
{
	Color32U5BU5D_t3850468773* V_0 = NULL;
	{
		NullCheck((Mesh_t3648964284 *)__this);
		bool L_0 = Mesh_get_canAccess_m2982329792((Mesh_t3648964284 *)__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_1 = ___channel0;
		NullCheck((Mesh_t3648964284 *)__this);
		bool L_2 = Mesh_HasChannel_m3577110678((Mesh_t3648964284 *)__this, (int32_t)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_3 = ___channel0;
		int32_t L_4 = ___format1;
		int32_t L_5 = ___dim2;
		NullCheck((Mesh_t3648964284 *)__this);
		Il2CppArray * L_6 = Mesh_GetAllocArrayFromChannelImpl_m1881002877((Mesh_t3648964284 *)__this, (int32_t)L_3, (int32_t)L_4, (int32_t)L_5, /*hidden argument*/NULL);
		V_0 = (Color32U5BU5D_t3850468773*)((Color32U5BU5D_t3850468773*)Castclass(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		goto IL_0048;
	}

IL_002d:
	{
		goto IL_003c;
	}

IL_0033:
	{
		int32_t L_7 = ___channel0;
		NullCheck((Mesh_t3648964284 *)__this);
		Mesh_PrintErrorCantAccessMesh_m868675854((Mesh_t3648964284 *)__this, (int32_t)L_7, /*hidden argument*/NULL);
	}

IL_003c:
	{
		V_0 = (Color32U5BU5D_t3850468773*)((Color32U5BU5D_t3850468773*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)0));
		goto IL_0048;
	}

IL_0048:
	{
		Color32U5BU5D_t3850468773* L_8 = V_0;
		return L_8;
	}
}
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector2>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  Vector2U5BU5D_t1457185986* Mesh_GetAllocArrayFromChannel_TisVector2_t2156229523_m2030487332_gshared (Mesh_t3648964284 * __this, int32_t ___channel0, const MethodInfo* method)
{
	Vector2U5BU5D_t1457185986* V_0 = NULL;
	{
		int32_t L_0 = ___channel0;
		int32_t L_1 = ___channel0;
		int32_t L_2 = Mesh_DefaultDimensionForChannel_m4089233615(NULL /*static, unused*/, (int32_t)L_1, /*hidden argument*/NULL);
		NullCheck((Mesh_t3648964284 *)__this);
		Vector2U5BU5D_t1457185986* L_3 = ((  Vector2U5BU5D_t1457185986* (*) (Mesh_t3648964284 *, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Mesh_t3648964284 *)__this, (int32_t)L_0, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Vector2U5BU5D_t1457185986*)L_3;
		goto IL_0015;
	}

IL_0015:
	{
		Vector2U5BU5D_t1457185986* L_4 = V_0;
		return L_4;
	}
}
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector2>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  Vector2U5BU5D_t1457185986* Mesh_GetAllocArrayFromChannel_TisVector2_t2156229523_m239371797_gshared (Mesh_t3648964284 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const MethodInfo* method)
{
	Vector2U5BU5D_t1457185986* V_0 = NULL;
	{
		NullCheck((Mesh_t3648964284 *)__this);
		bool L_0 = Mesh_get_canAccess_m2982329792((Mesh_t3648964284 *)__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_1 = ___channel0;
		NullCheck((Mesh_t3648964284 *)__this);
		bool L_2 = Mesh_HasChannel_m3577110678((Mesh_t3648964284 *)__this, (int32_t)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_3 = ___channel0;
		int32_t L_4 = ___format1;
		int32_t L_5 = ___dim2;
		NullCheck((Mesh_t3648964284 *)__this);
		Il2CppArray * L_6 = Mesh_GetAllocArrayFromChannelImpl_m1881002877((Mesh_t3648964284 *)__this, (int32_t)L_3, (int32_t)L_4, (int32_t)L_5, /*hidden argument*/NULL);
		V_0 = (Vector2U5BU5D_t1457185986*)((Vector2U5BU5D_t1457185986*)Castclass(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		goto IL_0048;
	}

IL_002d:
	{
		goto IL_003c;
	}

IL_0033:
	{
		int32_t L_7 = ___channel0;
		NullCheck((Mesh_t3648964284 *)__this);
		Mesh_PrintErrorCantAccessMesh_m868675854((Mesh_t3648964284 *)__this, (int32_t)L_7, /*hidden argument*/NULL);
	}

IL_003c:
	{
		V_0 = (Vector2U5BU5D_t1457185986*)((Vector2U5BU5D_t1457185986*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)0));
		goto IL_0048;
	}

IL_0048:
	{
		Vector2U5BU5D_t1457185986* L_8 = V_0;
		return L_8;
	}
}
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector3>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  Vector3U5BU5D_t1718750761* Mesh_GetAllocArrayFromChannel_TisVector3_t3722313464_m3084347085_gshared (Mesh_t3648964284 * __this, int32_t ___channel0, const MethodInfo* method)
{
	Vector3U5BU5D_t1718750761* V_0 = NULL;
	{
		int32_t L_0 = ___channel0;
		int32_t L_1 = ___channel0;
		int32_t L_2 = Mesh_DefaultDimensionForChannel_m4089233615(NULL /*static, unused*/, (int32_t)L_1, /*hidden argument*/NULL);
		NullCheck((Mesh_t3648964284 *)__this);
		Vector3U5BU5D_t1718750761* L_3 = ((  Vector3U5BU5D_t1718750761* (*) (Mesh_t3648964284 *, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Mesh_t3648964284 *)__this, (int32_t)L_0, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Vector3U5BU5D_t1718750761*)L_3;
		goto IL_0015;
	}

IL_0015:
	{
		Vector3U5BU5D_t1718750761* L_4 = V_0;
		return L_4;
	}
}
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector3>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  Vector3U5BU5D_t1718750761* Mesh_GetAllocArrayFromChannel_TisVector3_t3722313464_m3999801908_gshared (Mesh_t3648964284 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const MethodInfo* method)
{
	Vector3U5BU5D_t1718750761* V_0 = NULL;
	{
		NullCheck((Mesh_t3648964284 *)__this);
		bool L_0 = Mesh_get_canAccess_m2982329792((Mesh_t3648964284 *)__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_1 = ___channel0;
		NullCheck((Mesh_t3648964284 *)__this);
		bool L_2 = Mesh_HasChannel_m3577110678((Mesh_t3648964284 *)__this, (int32_t)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_3 = ___channel0;
		int32_t L_4 = ___format1;
		int32_t L_5 = ___dim2;
		NullCheck((Mesh_t3648964284 *)__this);
		Il2CppArray * L_6 = Mesh_GetAllocArrayFromChannelImpl_m1881002877((Mesh_t3648964284 *)__this, (int32_t)L_3, (int32_t)L_4, (int32_t)L_5, /*hidden argument*/NULL);
		V_0 = (Vector3U5BU5D_t1718750761*)((Vector3U5BU5D_t1718750761*)Castclass(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		goto IL_0048;
	}

IL_002d:
	{
		goto IL_003c;
	}

IL_0033:
	{
		int32_t L_7 = ___channel0;
		NullCheck((Mesh_t3648964284 *)__this);
		Mesh_PrintErrorCantAccessMesh_m868675854((Mesh_t3648964284 *)__this, (int32_t)L_7, /*hidden argument*/NULL);
	}

IL_003c:
	{
		V_0 = (Vector3U5BU5D_t1718750761*)((Vector3U5BU5D_t1718750761*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)0));
		goto IL_0048;
	}

IL_0048:
	{
		Vector3U5BU5D_t1718750761* L_8 = V_0;
		return L_8;
	}
}
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector4>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  Vector4U5BU5D_t934056436* Mesh_GetAllocArrayFromChannel_TisVector4_t3319028937_m3652758048_gshared (Mesh_t3648964284 * __this, int32_t ___channel0, const MethodInfo* method)
{
	Vector4U5BU5D_t934056436* V_0 = NULL;
	{
		int32_t L_0 = ___channel0;
		int32_t L_1 = ___channel0;
		int32_t L_2 = Mesh_DefaultDimensionForChannel_m4089233615(NULL /*static, unused*/, (int32_t)L_1, /*hidden argument*/NULL);
		NullCheck((Mesh_t3648964284 *)__this);
		Vector4U5BU5D_t934056436* L_3 = ((  Vector4U5BU5D_t934056436* (*) (Mesh_t3648964284 *, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Mesh_t3648964284 *)__this, (int32_t)L_0, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Vector4U5BU5D_t934056436*)L_3;
		goto IL_0015;
	}

IL_0015:
	{
		Vector4U5BU5D_t934056436* L_4 = V_0;
		return L_4;
	}
}
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector4>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  Vector4U5BU5D_t934056436* Mesh_GetAllocArrayFromChannel_TisVector4_t3319028937_m3273965775_gshared (Mesh_t3648964284 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const MethodInfo* method)
{
	Vector4U5BU5D_t934056436* V_0 = NULL;
	{
		NullCheck((Mesh_t3648964284 *)__this);
		bool L_0 = Mesh_get_canAccess_m2982329792((Mesh_t3648964284 *)__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_1 = ___channel0;
		NullCheck((Mesh_t3648964284 *)__this);
		bool L_2 = Mesh_HasChannel_m3577110678((Mesh_t3648964284 *)__this, (int32_t)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_3 = ___channel0;
		int32_t L_4 = ___format1;
		int32_t L_5 = ___dim2;
		NullCheck((Mesh_t3648964284 *)__this);
		Il2CppArray * L_6 = Mesh_GetAllocArrayFromChannelImpl_m1881002877((Mesh_t3648964284 *)__this, (int32_t)L_3, (int32_t)L_4, (int32_t)L_5, /*hidden argument*/NULL);
		V_0 = (Vector4U5BU5D_t934056436*)((Vector4U5BU5D_t934056436*)Castclass(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		goto IL_0048;
	}

IL_002d:
	{
		goto IL_003c;
	}

IL_0033:
	{
		int32_t L_7 = ___channel0;
		NullCheck((Mesh_t3648964284 *)__this);
		Mesh_PrintErrorCantAccessMesh_m868675854((Mesh_t3648964284 *)__this, (int32_t)L_7, /*hidden argument*/NULL);
	}

IL_003c:
	{
		V_0 = (Vector4U5BU5D_t934056436*)((Vector4U5BU5D_t934056436*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)0));
		goto IL_0048;
	}

IL_0048:
	{
		Vector4U5BU5D_t934056436* L_8 = V_0;
		return L_8;
	}
}
// T[] UnityEngine.Object::FindObjectsOfType<System.Object>()
extern "C"  ObjectU5BU5D_t2843939325* Object_FindObjectsOfType_TisIl2CppObject_m2647183545_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_FindObjectsOfType_TisIl2CppObject_m2647183545_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t1417781964* L_1 = Object_FindObjectsOfType_m2898745631(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		ObjectU5BU5D_t2843939325* L_2 = ((  ObjectU5BU5D_t2843939325* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t1417781964*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t1417781964*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (ObjectU5BU5D_t2843939325*)L_2;
		goto IL_001b;
	}

IL_001b:
	{
		ObjectU5BU5D_t2843939325* L_3 = V_0;
		return L_3;
	}
}
// T[] UnityEngine.Resources::ConvertObjects<System.Object>(UnityEngine.Object[])
extern "C"  ObjectU5BU5D_t2843939325* Resources_ConvertObjects_TisIl2CppObject_m463569367_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1417781964* ___rawObjects0, const MethodInfo* method)
{
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	ObjectU5BU5D_t2843939325* V_1 = NULL;
	int32_t V_2 = 0;
	{
		ObjectU5BU5D_t1417781964* L_0 = ___rawObjects0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		V_0 = (ObjectU5BU5D_t2843939325*)NULL;
		goto IL_0041;
	}

IL_000e:
	{
		ObjectU5BU5D_t1417781964* L_1 = ___rawObjects0;
		NullCheck(L_1);
		V_1 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))));
		V_2 = (int32_t)0;
		goto IL_0031;
	}

IL_001e:
	{
		ObjectU5BU5D_t2843939325* L_2 = V_1;
		int32_t L_3 = V_2;
		ObjectU5BU5D_t1417781964* L_4 = ___rawObjects0;
		int32_t L_5 = V_2;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Object_t631007953 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (Il2CppObject *)((Il2CppObject *)Castclass(L_7, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))));
		int32_t L_8 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_9 = V_2;
		ObjectU5BU5D_t2843939325* L_10 = V_1;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_001e;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_11 = V_1;
		V_0 = (ObjectU5BU5D_t2843939325*)L_11;
		goto IL_0041;
	}

IL_0041:
	{
		ObjectU5BU5D_t2843939325* L_12 = V_0;
		return L_12;
	}
}
// TAttr System.ServiceModel.ServiceHost::PopulateAttribute<System.Object>()
extern "C"  Il2CppObject * ServiceHost_PopulateAttribute_TisIl2CppObject_m1706125618_gshared (ServiceHost_t1894294968 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ServiceHost_PopulateAttribute_TisIl2CppObject_m1706125618_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	{
		Type_t * L_0 = (Type_t *)__this->get_service_type_26();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		ObjectU5BU5D_t2843939325* L_2 = VirtFuncInvoker2< ObjectU5BU5D_t2843939325*, Type_t *, bool >::Invoke(14 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, (MemberInfo_t *)L_0, (Type_t *)L_1, (bool)0);
		V_0 = (ObjectU5BU5D_t2843939325*)L_2;
		ObjectU5BU5D_t2843939325* L_3 = V_0;
		NullCheck(L_3);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0028;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = 0;
		Il2CppObject * L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		G_B3_0 = L_6;
		goto IL_0037;
	}

IL_0028:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_8 = Activator_CreateInstance_m3631483688(NULL /*static, unused*/, (Type_t *)L_7, /*hidden argument*/NULL);
		G_B3_0 = L_8;
	}

IL_0037:
	{
		return ((Il2CppObject *)Castclass(G_B3_0, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// TOutput[] System.Array::ConvertAll<System.Object,System.Object>(TInput[],System.Converter`2<TInput,TOutput>)
extern "C"  ObjectU5BU5D_t2843939325* Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2417852296_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t2843939325* ___array0, Converter_2_t2442480487 * ___converter1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2417852296_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t2843939325* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, (String_t*)_stringLiteral4007973390, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Converter_2_t2442480487 * L_2 = ___converter1;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_3 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_3, (String_t*)_stringLiteral879148213, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		ObjectU5BU5D_t2843939325* L_4 = ___array0;
		NullCheck(L_4);
		V_0 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_004a;
	}

IL_0032:
	{
		ObjectU5BU5D_t2843939325* L_5 = V_0;
		int32_t L_6 = V_1;
		Converter_2_t2442480487 * L_7 = ___converter1;
		ObjectU5BU5D_t2843939325* L_8 = ___array0;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck((Converter_2_t2442480487 *)L_7);
		Il2CppObject * L_12 = ((  Il2CppObject * (*) (Converter_2_t2442480487 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Converter_2_t2442480487 *)L_7, (Il2CppObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Il2CppObject *)L_12);
		int32_t L_13 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_004a:
	{
		int32_t L_14 = V_1;
		ObjectU5BU5D_t2843939325* L_15 = ___array0;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0032;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_16 = V_0;
		return L_16;
	}
}
// TSection System.Web.UI.BaseParser::GetConfigSection<System.Object>(System.String)
extern "C"  Il2CppObject * BaseParser_GetConfigSection_TisIl2CppObject_m2542956124_gshared (BaseParser_t4057001241 * __this, String_t* ___section0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BaseParser_GetConfigSection_TisIl2CppObject_m2542956124_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VirtualPath_t4270372584 * V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* G_B3_0 = NULL;
	{
		NullCheck((BaseParser_t4057001241 *)__this);
		VirtualPath_t4270372584 * L_0 = BaseParser_get_VirtualPath_m2391628584((BaseParser_t4057001241 *)__this, /*hidden argument*/NULL);
		V_0 = (VirtualPath_t4270372584 *)L_0;
		VirtualPath_t4270372584 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		VirtualPath_t4270372584 * L_2 = V_0;
		NullCheck((VirtualPath_t4270372584 *)L_2);
		String_t* L_3 = VirtualPath_get_Absolute_m3947569336((VirtualPath_t4270372584 *)L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = ((String_t*)(NULL));
	}

IL_0019:
	{
		V_1 = (String_t*)G_B3_0;
		String_t* L_4 = V_1;
		if (L_4)
		{
			goto IL_0031;
		}
	}
	{
		String_t* L_5 = ___section0;
		IL2CPP_RUNTIME_CLASS_INIT(WebConfigurationManager_t2451561378_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = WebConfigurationManager_GetSection_m1176823268(NULL /*static, unused*/, (String_t*)L_5, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 0))), IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
	}

IL_0031:
	{
		String_t* L_7 = ___section0;
		String_t* L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(WebConfigurationManager_t2451561378_il2cpp_TypeInfo_var);
		Il2CppObject * L_9 = WebConfigurationManager_GetSection_m285847730(NULL /*static, unused*/, (String_t*)L_7, (String_t*)L_8, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_9, IL2CPP_RGCTX_DATA(method->rgctx_data, 0))), IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
	}
}
// TSource System.Linq.Enumerable::First<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  Il2CppObject * Enumerable_First_TisIl2CppObject_m3835263318_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerable_First_TisIl2CppObject_m3835263318_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___source0;
		Check_Source_m4098695967(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (Il2CppObject*)L_3);
		if (!L_4)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		Il2CppObject * L_6 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_5, (int32_t)0);
		return L_6;
	}

IL_0026:
	{
		InvalidOperationException_t56020091 * L_7 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2734335978(L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_002c:
	{
		Il2CppObject* L_8 = ___source0;
		NullCheck((Il2CppObject*)L_8);
		Il2CppObject* L_9 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 2), (Il2CppObject*)L_8);
		V_1 = (Il2CppObject*)L_9;
	}

IL_0033:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject* L_10 = V_1;
			NullCheck((Il2CppObject *)L_10);
			bool L_11 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
			if (!L_11)
			{
				goto IL_004a;
			}
		}

IL_003e:
		{
			Il2CppObject* L_12 = V_1;
			NullCheck((Il2CppObject*)L_12);
			Il2CppObject * L_13 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 3), (Il2CppObject*)L_12);
			V_2 = (Il2CppObject *)L_13;
			IL2CPP_LEAVE(0x62, FINALLY_004f);
		}

IL_004a:
		{
			IL2CPP_LEAVE(0x5C, FINALLY_004f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_004f;
	}

FINALLY_004f:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_14 = V_1;
			if (!L_14)
			{
				goto IL_005b;
			}
		}

IL_0055:
		{
			Il2CppObject* L_15 = V_1;
			NullCheck((Il2CppObject *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
		}

IL_005b:
		{
			IL2CPP_END_FINALLY(79)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(79)
	{
		IL2CPP_JUMP_TBL(0x62, IL_0062)
		IL2CPP_JUMP_TBL(0x5C, IL_005c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_005c:
	{
		InvalidOperationException_t56020091 * L_16 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2734335978(L_16, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}

IL_0062:
	{
		Il2CppObject * L_17 = V_2;
		return L_17;
	}
}
// TSource System.Linq.Enumerable::First<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
extern "C"  Il2CppObject * Enumerable_First_TisIl2CppObject_m1109096697_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Func_2_t3759279471 * ___predicate1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		Func_2_t3759279471 * L_1 = ___predicate1;
		Check_SourceAndPredicate_m2332465641(NULL /*static, unused*/, (Il2CppObject *)L_0, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		Il2CppObject* L_2 = ___source0;
		Func_2_t3759279471 * L_3 = ___predicate1;
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3759279471 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_2, (Func_2_t3759279471 *)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_4;
	}
}
// TSource System.Linq.Enumerable::First<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Linq.Enumerable/Fallback)
extern "C"  Il2CppObject * Enumerable_First_TisIl2CppObject_m2925268555_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Func_2_t3759279471 * ___predicate1, int32_t ___fallback2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerable_First_TisIl2CppObject_m2925268555_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___source0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0026;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (Il2CppObject*)L_2);
			V_0 = (Il2CppObject *)L_3;
			Func_2_t3759279471 * L_4 = ___predicate1;
			Il2CppObject * L_5 = V_0;
			NullCheck((Func_2_t3759279471 *)L_4);
			bool L_6 = ((  bool (*) (Func_2_t3759279471 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((Func_2_t3759279471 *)L_4, (Il2CppObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
			if (!L_6)
			{
				goto IL_0026;
			}
		}

IL_001f:
		{
			Il2CppObject * L_7 = V_0;
			V_2 = (Il2CppObject *)L_7;
			IL2CPP_LEAVE(0x58, FINALLY_0036);
		}

IL_0026:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			if (L_9)
			{
				goto IL_000c;
			}
		}

IL_0031:
		{
			IL2CPP_LEAVE(0x41, FINALLY_0036);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0036;
	}

FINALLY_0036:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_10 = V_1;
			if (L_10)
			{
				goto IL_003a;
			}
		}

IL_0039:
		{
			IL2CPP_END_FINALLY(54)
		}

IL_003a:
		{
			Il2CppObject* L_11 = V_1;
			NullCheck((Il2CppObject *)L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			IL2CPP_END_FINALLY(54)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(54)
	{
		IL2CPP_JUMP_TBL(0x58, IL_0058)
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0041:
	{
		int32_t L_12 = ___fallback2;
		if ((!(((uint32_t)L_12) == ((uint32_t)1))))
		{
			goto IL_004e;
		}
	}
	{
		InvalidOperationException_t56020091 * L_13 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2734335978(L_13, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13);
	}

IL_004e:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_3));
		Il2CppObject * L_14 = V_3;
		return L_14;
	}

IL_0058:
	{
		Il2CppObject * L_15 = V_2;
		return L_15;
	}
}
// TSource System.Linq.Enumerable::FirstOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
extern "C"  Il2CppObject * Enumerable_FirstOrDefault_TisIl2CppObject_m3892742505_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Func_2_t3759279471 * ___predicate1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		Func_2_t3759279471 * L_1 = ___predicate1;
		Check_SourceAndPredicate_m2332465641(NULL /*static, unused*/, (Il2CppObject *)L_0, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		Il2CppObject* L_2 = ___source0;
		Func_2_t3759279471 * L_3 = ___predicate1;
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3759279471 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_2, (Func_2_t3759279471 *)L_3, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_4;
	}
}
// TSource System.Linq.Enumerable::Last<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Linq.Enumerable/Fallback)
extern "C"  KeyValuePair_2_t2530217319  Enumerable_Last_TisKeyValuePair_2_t2530217319_m2391423423_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Func_2_t1033609360 * ___predicate1, int32_t ___fallback2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerable_Last_TisKeyValuePair_2_t2530217319_m2391423423_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	KeyValuePair_2_t2530217319  V_1;
	memset(&V_1, 0, sizeof(V_1));
	KeyValuePair_2_t2530217319  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Il2CppObject* V_3 = NULL;
	KeyValuePair_2_t2530217319  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (bool)1;
		Initobj (KeyValuePair_2_t2530217319_il2cpp_TypeInfo_var, (&V_4));
		KeyValuePair_2_t2530217319  L_0 = V_4;
		V_1 = (KeyValuePair_2_t2530217319 )L_0;
		Il2CppObject* L_1 = ___source0;
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject* L_2 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_1);
		V_3 = (Il2CppObject*)L_2;
	}

IL_0014:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0035;
		}

IL_0019:
		{
			Il2CppObject* L_3 = V_3;
			NullCheck((Il2CppObject*)L_3);
			KeyValuePair_2_t2530217319  L_4 = InterfaceFuncInvoker0< KeyValuePair_2_t2530217319  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (Il2CppObject*)L_3);
			V_2 = (KeyValuePair_2_t2530217319 )L_4;
			Func_2_t1033609360 * L_5 = ___predicate1;
			KeyValuePair_2_t2530217319  L_6 = V_2;
			NullCheck((Func_2_t1033609360 *)L_5);
			bool L_7 = ((  bool (*) (Func_2_t1033609360 *, KeyValuePair_2_t2530217319 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((Func_2_t1033609360 *)L_5, (KeyValuePair_2_t2530217319 )L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
			if (L_7)
			{
				goto IL_0031;
			}
		}

IL_002c:
		{
			goto IL_0035;
		}

IL_0031:
		{
			KeyValuePair_2_t2530217319  L_8 = V_2;
			V_1 = (KeyValuePair_2_t2530217319 )L_8;
			V_0 = (bool)0;
		}

IL_0035:
		{
			Il2CppObject* L_9 = V_3;
			NullCheck((Il2CppObject *)L_9);
			bool L_10 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
			if (L_10)
			{
				goto IL_0019;
			}
		}

IL_0040:
		{
			IL2CPP_LEAVE(0x50, FINALLY_0045);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0045;
	}

FINALLY_0045:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_11 = V_3;
			if (L_11)
			{
				goto IL_0049;
			}
		}

IL_0048:
		{
			IL2CPP_END_FINALLY(69)
		}

IL_0049:
		{
			Il2CppObject* L_12 = V_3;
			NullCheck((Il2CppObject *)L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
			IL2CPP_END_FINALLY(69)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(69)
	{
		IL2CPP_JUMP_TBL(0x50, IL_0050)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0050:
	{
		bool L_13 = V_0;
		if (L_13)
		{
			goto IL_0058;
		}
	}
	{
		KeyValuePair_2_t2530217319  L_14 = V_1;
		return L_14;
	}

IL_0058:
	{
		int32_t L_15 = ___fallback2;
		if ((!(((uint32_t)L_15) == ((uint32_t)1))))
		{
			goto IL_0065;
		}
	}
	{
		InvalidOperationException_t56020091 * L_16 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2734335978(L_16, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}

IL_0065:
	{
		KeyValuePair_2_t2530217319  L_17 = V_1;
		return L_17;
	}
}
// TSource System.Linq.Enumerable::Last<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Linq.Enumerable/Fallback)
extern "C"  Il2CppObject * Enumerable_Last_TisIl2CppObject_m191782855_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Func_2_t3759279471 * ___predicate1, int32_t ___fallback2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerable_Last_TisIl2CppObject_m191782855_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject* V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (bool)1;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_4));
		Il2CppObject * L_0 = V_4;
		V_1 = (Il2CppObject *)L_0;
		Il2CppObject* L_1 = ___source0;
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject* L_2 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_1);
		V_3 = (Il2CppObject*)L_2;
	}

IL_0014:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0035;
		}

IL_0019:
		{
			Il2CppObject* L_3 = V_3;
			NullCheck((Il2CppObject*)L_3);
			Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (Il2CppObject*)L_3);
			V_2 = (Il2CppObject *)L_4;
			Func_2_t3759279471 * L_5 = ___predicate1;
			Il2CppObject * L_6 = V_2;
			NullCheck((Func_2_t3759279471 *)L_5);
			bool L_7 = ((  bool (*) (Func_2_t3759279471 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((Func_2_t3759279471 *)L_5, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
			if (L_7)
			{
				goto IL_0031;
			}
		}

IL_002c:
		{
			goto IL_0035;
		}

IL_0031:
		{
			Il2CppObject * L_8 = V_2;
			V_1 = (Il2CppObject *)L_8;
			V_0 = (bool)0;
		}

IL_0035:
		{
			Il2CppObject* L_9 = V_3;
			NullCheck((Il2CppObject *)L_9);
			bool L_10 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
			if (L_10)
			{
				goto IL_0019;
			}
		}

IL_0040:
		{
			IL2CPP_LEAVE(0x50, FINALLY_0045);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0045;
	}

FINALLY_0045:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_11 = V_3;
			if (L_11)
			{
				goto IL_0049;
			}
		}

IL_0048:
		{
			IL2CPP_END_FINALLY(69)
		}

IL_0049:
		{
			Il2CppObject* L_12 = V_3;
			NullCheck((Il2CppObject *)L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
			IL2CPP_END_FINALLY(69)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(69)
	{
		IL2CPP_JUMP_TBL(0x50, IL_0050)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0050:
	{
		bool L_13 = V_0;
		if (L_13)
		{
			goto IL_0058;
		}
	}
	{
		Il2CppObject * L_14 = V_1;
		return L_14;
	}

IL_0058:
	{
		int32_t L_15 = ___fallback2;
		if ((!(((uint32_t)L_15) == ((uint32_t)1))))
		{
			goto IL_0065;
		}
	}
	{
		InvalidOperationException_t56020091 * L_16 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2734335978(L_16, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}

IL_0065:
	{
		Il2CppObject * L_17 = V_1;
		return L_17;
	}
}
// TSource System.Linq.Enumerable::LastOrDefault<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
extern "C"  KeyValuePair_2_t2530217319  Enumerable_LastOrDefault_TisKeyValuePair_2_t2530217319_m243036989_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Func_2_t1033609360 * ___predicate1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		Func_2_t1033609360 * L_1 = ___predicate1;
		Check_SourceAndPredicate_m2332465641(NULL /*static, unused*/, (Il2CppObject *)L_0, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		Il2CppObject* L_2 = ___source0;
		Func_2_t1033609360 * L_3 = ___predicate1;
		KeyValuePair_2_t2530217319  L_4 = ((  KeyValuePair_2_t2530217319  (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1033609360 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_2, (Func_2_t1033609360 *)L_3, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_4;
	}
}
// TSource System.Linq.Enumerable::LastOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
extern "C"  Il2CppObject * Enumerable_LastOrDefault_TisIl2CppObject_m1254320682_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Func_2_t3759279471 * ___predicate1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		Func_2_t3759279471 * L_1 = ___predicate1;
		Check_SourceAndPredicate_m2332465641(NULL /*static, unused*/, (Il2CppObject *)L_0, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		Il2CppObject* L_2 = ___source0;
		Func_2_t3759279471 * L_3 = ___predicate1;
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3759279471 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_2, (Func_2_t3759279471 *)L_3, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_4;
	}
}
// TSource[] System.Linq.Enumerable::ToArray<System.Int32>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  Int32U5BU5D_t385246372* Enumerable_ToArray_TisInt32_t2950945753_m2311522548_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	Int32U5BU5D_t385246372* V_1 = NULL;
	{
		Il2CppObject* L_0 = ___source0;
		Check_Source_m4098695967(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Int32>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_3);
		V_1 = (Int32U5BU5D_t385246372*)((Int32U5BU5D_t385246372*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)L_4));
		Il2CppObject* L_5 = V_0;
		Int32U5BU5D_t385246372* L_6 = V_1;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker2< Int32U5BU5D_t385246372*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Int32>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_5, (Int32U5BU5D_t385246372*)L_6, (int32_t)0);
		Int32U5BU5D_t385246372* L_7 = V_1;
		return L_7;
	}

IL_0029:
	{
		Il2CppObject* L_8 = ___source0;
		List_1_t128053199 * L_9 = (List_1_t128053199 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 2));
		((  void (*) (List_1_t128053199 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)(L_9, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		NullCheck((List_1_t128053199 *)L_9);
		Int32U5BU5D_t385246372* L_10 = ((  Int32U5BU5D_t385246372* (*) (List_1_t128053199 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)((List_1_t128053199 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_10;
	}
}
// TSource[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  ObjectU5BU5D_t2843939325* Enumerable_ToArray_TisIl2CppObject_m3447781822_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	ObjectU5BU5D_t2843939325* V_1 = NULL;
	{
		Il2CppObject* L_0 = ___source0;
		Check_Source_m4098695967(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_3);
		V_1 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)L_4));
		Il2CppObject* L_5 = V_0;
		ObjectU5BU5D_t2843939325* L_6 = V_1;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker2< ObjectU5BU5D_t2843939325*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_5, (ObjectU5BU5D_t2843939325*)L_6, (int32_t)0);
		ObjectU5BU5D_t2843939325* L_7 = V_1;
		return L_7;
	}

IL_0029:
	{
		Il2CppObject* L_8 = ___source0;
		List_1_t257213610 * L_9 = (List_1_t257213610 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 2));
		((  void (*) (List_1_t257213610 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)(L_9, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		NullCheck((List_1_t257213610 *)L_9);
		ObjectU5BU5D_t2843939325* L_10 = ((  ObjectU5BU5D_t2843939325* (*) (List_1_t257213610 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)((List_1_t257213610 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_10;
	}
}
// TSource[] System.Linq.Enumerable::ToArray<Vuforia.VuforiaManager/TrackableIdPair>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  TrackableIdPairU5BU5D_t475764036* Enumerable_ToArray_TisTrackableIdPair_t4227350457_m4292962904_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	TrackableIdPairU5BU5D_t475764036* V_1 = NULL;
	{
		Il2CppObject* L_0 = ___source0;
		Check_Source_m4098695967(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<Vuforia.VuforiaManager/TrackableIdPair>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_3);
		V_1 = (TrackableIdPairU5BU5D_t475764036*)((TrackableIdPairU5BU5D_t475764036*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)L_4));
		Il2CppObject* L_5 = V_0;
		TrackableIdPairU5BU5D_t475764036* L_6 = V_1;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker2< TrackableIdPairU5BU5D_t475764036*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<Vuforia.VuforiaManager/TrackableIdPair>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_5, (TrackableIdPairU5BU5D_t475764036*)L_6, (int32_t)0);
		TrackableIdPairU5BU5D_t475764036* L_7 = V_1;
		return L_7;
	}

IL_0029:
	{
		Il2CppObject* L_8 = ___source0;
		List_1_t1404457903 * L_9 = (List_1_t1404457903 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 2));
		((  void (*) (List_1_t1404457903 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)(L_9, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		NullCheck((List_1_t1404457903 *)L_9);
		TrackableIdPairU5BU5D_t475764036* L_10 = ((  TrackableIdPairU5BU5D_t475764036* (*) (List_1_t1404457903 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)((List_1_t1404457903 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_10;
	}
}
// TValue System.IdentityModel.Selectors.SecurityTokenRequirement::GetProperty<System.Object>(System.String)
extern "C"  Il2CppObject * SecurityTokenRequirement_GetProperty_TisIl2CppObject_m4175878597_gshared (SecurityTokenRequirement_t1975057878 * __this, String_t* ___property0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SecurityTokenRequirement_GetProperty_TisIl2CppObject_m4175878597_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		String_t* L_0 = ___property0;
		NullCheck((SecurityTokenRequirement_t1975057878 *)__this);
		bool L_1 = ((  bool (*) (SecurityTokenRequirement_t1975057878 *, String_t*, Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((SecurityTokenRequirement_t1975057878 *)__this, (String_t*)L_0, (Il2CppObject **)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		Il2CppObject * L_2 = V_0;
		return L_2;
	}

IL_0010:
	{
		String_t* L_3 = ___property0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m2844511972(NULL /*static, unused*/, (String_t*)_stringLiteral3710528600, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_5 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}
}
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<System.Object>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
extern "C"  GameObject_t1113636619 * ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m3266560969_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1113636619 * ___root0, BaseEventData_t3903027533 * ___eventData1, EventFunction_1_t1764640198 * ___callbackFunction2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m3266560969_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Transform_t3600365921 * V_1 = NULL;
	GameObject_t1113636619 * V_2 = NULL;
	{
		GameObject_t1113636619 * L_0 = ___root0;
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t3484638744_il2cpp_TypeInfo_var);
		List_1_t777473367 * L_1 = ((ExecuteEvents_t3484638744_StaticFields*)ExecuteEvents_t3484638744_il2cpp_TypeInfo_var->static_fields)->get_s_InternalTransformList_18();
		ExecuteEvents_GetEventChain_m2404658789(NULL /*static, unused*/, (GameObject_t1113636619 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/NULL);
		V_0 = (int32_t)0;
		goto IL_0043;
	}

IL_0013:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t3484638744_il2cpp_TypeInfo_var);
		List_1_t777473367 * L_2 = ((ExecuteEvents_t3484638744_StaticFields*)ExecuteEvents_t3484638744_il2cpp_TypeInfo_var->static_fields)->get_s_InternalTransformList_18();
		int32_t L_3 = V_0;
		NullCheck((List_1_t777473367 *)L_2);
		Transform_t3600365921 * L_4 = List_1_get_Item_m3022113929((List_1_t777473367 *)L_2, (int32_t)L_3, /*hidden argument*/List_1_get_Item_m3022113929_MethodInfo_var);
		V_1 = (Transform_t3600365921 *)L_4;
		Transform_t3600365921 * L_5 = V_1;
		NullCheck((Component_t1923634451 *)L_5);
		GameObject_t1113636619 * L_6 = Component_get_gameObject_m2648350745((Component_t1923634451 *)L_5, /*hidden argument*/NULL);
		BaseEventData_t3903027533 * L_7 = ___eventData1;
		EventFunction_1_t1764640198 * L_8 = ___callbackFunction2;
		bool L_9 = ((  bool (*) (Il2CppObject * /* static, unused */, GameObject_t1113636619 *, BaseEventData_t3903027533 *, EventFunction_1_t1764640198 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (GameObject_t1113636619 *)L_6, (BaseEventData_t3903027533 *)L_7, (EventFunction_1_t1764640198 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_9)
		{
			goto IL_003e;
		}
	}
	{
		Transform_t3600365921 * L_10 = V_1;
		NullCheck((Component_t1923634451 *)L_10);
		GameObject_t1113636619 * L_11 = Component_get_gameObject_m2648350745((Component_t1923634451 *)L_10, /*hidden argument*/NULL);
		V_2 = (GameObject_t1113636619 *)L_11;
		goto IL_005a;
	}

IL_003e:
	{
		int32_t L_12 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0043:
	{
		int32_t L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t3484638744_il2cpp_TypeInfo_var);
		List_1_t777473367 * L_14 = ((ExecuteEvents_t3484638744_StaticFields*)ExecuteEvents_t3484638744_il2cpp_TypeInfo_var->static_fields)->get_s_InternalTransformList_18();
		NullCheck((List_1_t777473367 *)L_14);
		int32_t L_15 = List_1_get_Count_m3787308655((List_1_t777473367 *)L_14, /*hidden argument*/List_1_get_Count_m3787308655_MethodInfo_var);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0013;
		}
	}
	{
		V_2 = (GameObject_t1113636619 *)NULL;
		goto IL_005a;
	}

IL_005a:
	{
		GameObject_t1113636619 * L_16 = V_2;
		return L_16;
	}
}
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::GetEventHandler<System.Object>(UnityEngine.GameObject)
extern "C"  GameObject_t1113636619 * ExecuteEvents_GetEventHandler_TisIl2CppObject_m3687647312_gshared (Il2CppObject * __this /* static, unused */, GameObject_t1113636619 * ___root0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExecuteEvents_GetEventHandler_TisIl2CppObject_m3687647312_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	Transform_t3600365921 * V_1 = NULL;
	{
		GameObject_t1113636619 * L_0 = ___root0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1454075600(NULL /*static, unused*/, (Object_t631007953 *)L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		V_0 = (GameObject_t1113636619 *)NULL;
		goto IL_0058;
	}

IL_0014:
	{
		GameObject_t1113636619 * L_2 = ___root0;
		NullCheck((GameObject_t1113636619 *)L_2);
		Transform_t3600365921 * L_3 = GameObject_get_transform_m393750976((GameObject_t1113636619 *)L_2, /*hidden argument*/NULL);
		V_1 = (Transform_t3600365921 *)L_3;
		goto IL_0045;
	}

IL_0020:
	{
		Transform_t3600365921 * L_4 = V_1;
		NullCheck((Component_t1923634451 *)L_4);
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m2648350745((Component_t1923634451 *)L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t3484638744_il2cpp_TypeInfo_var);
		bool L_6 = ((  bool (*) (Il2CppObject * /* static, unused */, GameObject_t1113636619 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (GameObject_t1113636619 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_6)
		{
			goto IL_003d;
		}
	}
	{
		Transform_t3600365921 * L_7 = V_1;
		NullCheck((Component_t1923634451 *)L_7);
		GameObject_t1113636619 * L_8 = Component_get_gameObject_m2648350745((Component_t1923634451 *)L_7, /*hidden argument*/NULL);
		V_0 = (GameObject_t1113636619 *)L_8;
		goto IL_0058;
	}

IL_003d:
	{
		Transform_t3600365921 * L_9 = V_1;
		NullCheck((Transform_t3600365921 *)L_9);
		Transform_t3600365921 * L_10 = Transform_get_parent_m1293647796((Transform_t3600365921 *)L_9, /*hidden argument*/NULL);
		V_1 = (Transform_t3600365921 *)L_10;
	}

IL_0045:
	{
		Transform_t3600365921 * L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, (Object_t631007953 *)L_11, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0020;
		}
	}
	{
		V_0 = (GameObject_t1113636619 *)NULL;
		goto IL_0058;
	}

IL_0058:
	{
		GameObject_t1113636619 * L_13 = V_0;
		return L_13;
	}
}
