﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_DateTime3738529785.h"
#include "System_Web_System_Web_SessionState_SessionStateAct2424179227.h"

// System.Byte[]
struct ByteU5BU5D_t4116647657;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.SessionState.StateServerItem
struct  StateServerItem_t1723435776  : public Il2CppObject
{
public:
	// System.Byte[] System.Web.SessionState.StateServerItem::CollectionData
	ByteU5BU5D_t4116647657* ___CollectionData_0;
	// System.Byte[] System.Web.SessionState.StateServerItem::StaticObjectsData
	ByteU5BU5D_t4116647657* ___StaticObjectsData_1;
	// System.DateTime System.Web.SessionState.StateServerItem::last_access
	DateTime_t3738529785  ___last_access_2;
	// System.Int32 System.Web.SessionState.StateServerItem::Timeout
	int32_t ___Timeout_3;
	// System.Int32 System.Web.SessionState.StateServerItem::LockId
	int32_t ___LockId_4;
	// System.Boolean System.Web.SessionState.StateServerItem::Locked
	bool ___Locked_5;
	// System.DateTime System.Web.SessionState.StateServerItem::LockedTime
	DateTime_t3738529785  ___LockedTime_6;
	// System.Web.SessionState.SessionStateActions System.Web.SessionState.StateServerItem::Action
	int32_t ___Action_7;

public:
	inline static int32_t get_offset_of_CollectionData_0() { return static_cast<int32_t>(offsetof(StateServerItem_t1723435776, ___CollectionData_0)); }
	inline ByteU5BU5D_t4116647657* get_CollectionData_0() const { return ___CollectionData_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_CollectionData_0() { return &___CollectionData_0; }
	inline void set_CollectionData_0(ByteU5BU5D_t4116647657* value)
	{
		___CollectionData_0 = value;
		Il2CppCodeGenWriteBarrier(&___CollectionData_0, value);
	}

	inline static int32_t get_offset_of_StaticObjectsData_1() { return static_cast<int32_t>(offsetof(StateServerItem_t1723435776, ___StaticObjectsData_1)); }
	inline ByteU5BU5D_t4116647657* get_StaticObjectsData_1() const { return ___StaticObjectsData_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_StaticObjectsData_1() { return &___StaticObjectsData_1; }
	inline void set_StaticObjectsData_1(ByteU5BU5D_t4116647657* value)
	{
		___StaticObjectsData_1 = value;
		Il2CppCodeGenWriteBarrier(&___StaticObjectsData_1, value);
	}

	inline static int32_t get_offset_of_last_access_2() { return static_cast<int32_t>(offsetof(StateServerItem_t1723435776, ___last_access_2)); }
	inline DateTime_t3738529785  get_last_access_2() const { return ___last_access_2; }
	inline DateTime_t3738529785 * get_address_of_last_access_2() { return &___last_access_2; }
	inline void set_last_access_2(DateTime_t3738529785  value)
	{
		___last_access_2 = value;
	}

	inline static int32_t get_offset_of_Timeout_3() { return static_cast<int32_t>(offsetof(StateServerItem_t1723435776, ___Timeout_3)); }
	inline int32_t get_Timeout_3() const { return ___Timeout_3; }
	inline int32_t* get_address_of_Timeout_3() { return &___Timeout_3; }
	inline void set_Timeout_3(int32_t value)
	{
		___Timeout_3 = value;
	}

	inline static int32_t get_offset_of_LockId_4() { return static_cast<int32_t>(offsetof(StateServerItem_t1723435776, ___LockId_4)); }
	inline int32_t get_LockId_4() const { return ___LockId_4; }
	inline int32_t* get_address_of_LockId_4() { return &___LockId_4; }
	inline void set_LockId_4(int32_t value)
	{
		___LockId_4 = value;
	}

	inline static int32_t get_offset_of_Locked_5() { return static_cast<int32_t>(offsetof(StateServerItem_t1723435776, ___Locked_5)); }
	inline bool get_Locked_5() const { return ___Locked_5; }
	inline bool* get_address_of_Locked_5() { return &___Locked_5; }
	inline void set_Locked_5(bool value)
	{
		___Locked_5 = value;
	}

	inline static int32_t get_offset_of_LockedTime_6() { return static_cast<int32_t>(offsetof(StateServerItem_t1723435776, ___LockedTime_6)); }
	inline DateTime_t3738529785  get_LockedTime_6() const { return ___LockedTime_6; }
	inline DateTime_t3738529785 * get_address_of_LockedTime_6() { return &___LockedTime_6; }
	inline void set_LockedTime_6(DateTime_t3738529785  value)
	{
		___LockedTime_6 = value;
	}

	inline static int32_t get_offset_of_Action_7() { return static_cast<int32_t>(offsetof(StateServerItem_t1723435776, ___Action_7)); }
	inline int32_t get_Action_7() const { return ___Action_7; }
	inline int32_t* get_address_of_Action_7() { return &___Action_7; }
	inline void set_Action_7(int32_t value)
	{
		___Action_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
