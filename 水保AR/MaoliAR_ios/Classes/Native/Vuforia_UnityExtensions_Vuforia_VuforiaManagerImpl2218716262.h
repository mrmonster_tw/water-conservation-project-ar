﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManager_Tra4227350457.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManagerImpl/<>c__DisplayClass86_0
struct  U3CU3Ec__DisplayClass86_0_t2218716262  : public Il2CppObject
{
public:
	// Vuforia.VuforiaManager/TrackableIdPair Vuforia.VuforiaManagerImpl/<>c__DisplayClass86_0::id
	TrackableIdPair_t4227350457  ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass86_0_t2218716262, ___id_0)); }
	inline TrackableIdPair_t4227350457  get_id_0() const { return ___id_0; }
	inline TrackableIdPair_t4227350457 * get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(TrackableIdPair_t4227350457  value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
