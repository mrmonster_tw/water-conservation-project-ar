﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Collections_ObjectModel_KeyedColle1141281317.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.MessagePartDescriptionCollection
struct  MessagePartDescriptionCollection_t1471748572  : public KeyedCollection_2_t1141281317
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
