﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_StateMachineBehaviour957311111.h"

// System.Collections.Generic.List`1<SetAnimArray/SetAnimInfo>
struct List_1_t3599602936;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SetAnimArray
struct  SetAnimArray_t1762733004  : public StateMachineBehaviour_t957311111
{
public:
	// System.Collections.Generic.List`1<SetAnimArray/SetAnimInfo> SetAnimArray::setAnimInfo
	List_1_t3599602936 * ___setAnimInfo_2;

public:
	inline static int32_t get_offset_of_setAnimInfo_2() { return static_cast<int32_t>(offsetof(SetAnimArray_t1762733004, ___setAnimInfo_2)); }
	inline List_1_t3599602936 * get_setAnimInfo_2() const { return ___setAnimInfo_2; }
	inline List_1_t3599602936 ** get_address_of_setAnimInfo_2() { return &___setAnimInfo_2; }
	inline void set_setAnimInfo_2(List_1_t3599602936 * value)
	{
		___setAnimInfo_2 = value;
		Il2CppCodeGenWriteBarrier(&___setAnimInfo_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
