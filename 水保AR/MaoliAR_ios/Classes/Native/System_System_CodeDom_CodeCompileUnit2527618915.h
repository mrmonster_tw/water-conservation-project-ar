﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeObject3927604602.h"

// System.CodeDom.CodeAttributeDeclarationCollection
struct CodeAttributeDeclarationCollection_t3890917538;
// System.CodeDom.CodeNamespaceCollection
struct CodeNamespaceCollection_t381303288;
// System.Collections.Specialized.StringCollection
struct StringCollection_t167406615;
// System.CodeDom.CodeDirectiveCollection
struct CodeDirectiveCollection_t1153190035;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeCompileUnit
struct  CodeCompileUnit_t2527618915  : public CodeObject_t3927604602
{
public:
	// System.CodeDom.CodeAttributeDeclarationCollection System.CodeDom.CodeCompileUnit::attributes
	CodeAttributeDeclarationCollection_t3890917538 * ___attributes_1;
	// System.CodeDom.CodeNamespaceCollection System.CodeDom.CodeCompileUnit::namespaces
	CodeNamespaceCollection_t381303288 * ___namespaces_2;
	// System.Collections.Specialized.StringCollection System.CodeDom.CodeCompileUnit::assemblies
	StringCollection_t167406615 * ___assemblies_3;
	// System.CodeDom.CodeDirectiveCollection System.CodeDom.CodeCompileUnit::startDirectives
	CodeDirectiveCollection_t1153190035 * ___startDirectives_4;
	// System.CodeDom.CodeDirectiveCollection System.CodeDom.CodeCompileUnit::endDirectives
	CodeDirectiveCollection_t1153190035 * ___endDirectives_5;

public:
	inline static int32_t get_offset_of_attributes_1() { return static_cast<int32_t>(offsetof(CodeCompileUnit_t2527618915, ___attributes_1)); }
	inline CodeAttributeDeclarationCollection_t3890917538 * get_attributes_1() const { return ___attributes_1; }
	inline CodeAttributeDeclarationCollection_t3890917538 ** get_address_of_attributes_1() { return &___attributes_1; }
	inline void set_attributes_1(CodeAttributeDeclarationCollection_t3890917538 * value)
	{
		___attributes_1 = value;
		Il2CppCodeGenWriteBarrier(&___attributes_1, value);
	}

	inline static int32_t get_offset_of_namespaces_2() { return static_cast<int32_t>(offsetof(CodeCompileUnit_t2527618915, ___namespaces_2)); }
	inline CodeNamespaceCollection_t381303288 * get_namespaces_2() const { return ___namespaces_2; }
	inline CodeNamespaceCollection_t381303288 ** get_address_of_namespaces_2() { return &___namespaces_2; }
	inline void set_namespaces_2(CodeNamespaceCollection_t381303288 * value)
	{
		___namespaces_2 = value;
		Il2CppCodeGenWriteBarrier(&___namespaces_2, value);
	}

	inline static int32_t get_offset_of_assemblies_3() { return static_cast<int32_t>(offsetof(CodeCompileUnit_t2527618915, ___assemblies_3)); }
	inline StringCollection_t167406615 * get_assemblies_3() const { return ___assemblies_3; }
	inline StringCollection_t167406615 ** get_address_of_assemblies_3() { return &___assemblies_3; }
	inline void set_assemblies_3(StringCollection_t167406615 * value)
	{
		___assemblies_3 = value;
		Il2CppCodeGenWriteBarrier(&___assemblies_3, value);
	}

	inline static int32_t get_offset_of_startDirectives_4() { return static_cast<int32_t>(offsetof(CodeCompileUnit_t2527618915, ___startDirectives_4)); }
	inline CodeDirectiveCollection_t1153190035 * get_startDirectives_4() const { return ___startDirectives_4; }
	inline CodeDirectiveCollection_t1153190035 ** get_address_of_startDirectives_4() { return &___startDirectives_4; }
	inline void set_startDirectives_4(CodeDirectiveCollection_t1153190035 * value)
	{
		___startDirectives_4 = value;
		Il2CppCodeGenWriteBarrier(&___startDirectives_4, value);
	}

	inline static int32_t get_offset_of_endDirectives_5() { return static_cast<int32_t>(offsetof(CodeCompileUnit_t2527618915, ___endDirectives_5)); }
	inline CodeDirectiveCollection_t1153190035 * get_endDirectives_5() const { return ___endDirectives_5; }
	inline CodeDirectiveCollection_t1153190035 ** get_address_of_endDirectives_5() { return &___endDirectives_5; }
	inline void set_endDirectives_5(CodeDirectiveCollection_t1153190035 * value)
	{
		___endDirectives_5 = value;
		Il2CppCodeGenWriteBarrier(&___endDirectives_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
