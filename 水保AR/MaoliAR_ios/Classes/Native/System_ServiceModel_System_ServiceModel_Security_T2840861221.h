﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Security_T3240244108.h"

// System.ServiceModel.Description.WSTrustSecurityTokenServiceProxy
struct WSTrustSecurityTokenServiceProxy_t2160987012;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.WsscAuthenticatorCommunicationObject
struct  WsscAuthenticatorCommunicationObject_t2840861221  : public AuthenticatorCommunicationObject_t3240244108
{
public:
	// System.ServiceModel.Description.WSTrustSecurityTokenServiceProxy System.ServiceModel.Security.Tokens.WsscAuthenticatorCommunicationObject::proxy
	WSTrustSecurityTokenServiceProxy_t2160987012 * ___proxy_16;

public:
	inline static int32_t get_offset_of_proxy_16() { return static_cast<int32_t>(offsetof(WsscAuthenticatorCommunicationObject_t2840861221, ___proxy_16)); }
	inline WSTrustSecurityTokenServiceProxy_t2160987012 * get_proxy_16() const { return ___proxy_16; }
	inline WSTrustSecurityTokenServiceProxy_t2160987012 ** get_address_of_proxy_16() { return &___proxy_16; }
	inline void set_proxy_16(WSTrustSecurityTokenServiceProxy_t2160987012 * value)
	{
		___proxy_16 = value;
		Il2CppCodeGenWriteBarrier(&___proxy_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
