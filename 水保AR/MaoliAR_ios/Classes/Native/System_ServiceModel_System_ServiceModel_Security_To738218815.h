﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Tokens_S1271873540.h"
#include "mscorlib_System_DateTime3738529785.h"

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.IdentityModel.Tokens.SecurityToken
struct SecurityToken_t1271873540;
// System.IdentityModel.Tokens.SecurityKeyIdentifier
struct SecurityKeyIdentifier_t806165896;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IdentityModel.Tokens.SecurityKey>
struct ReadOnlyCollection_1_t1046284793;
// System.Security.Cryptography.Xml.ReferenceList
struct ReferenceList_t2222396100;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.WrappedKeySecurityToken
struct  WrappedKeySecurityToken_t738218815  : public SecurityToken_t1271873540
{
public:
	// System.String System.ServiceModel.Security.Tokens.WrappedKeySecurityToken::id
	String_t* ___id_0;
	// System.Byte[] System.ServiceModel.Security.Tokens.WrappedKeySecurityToken::raw_key
	ByteU5BU5D_t4116647657* ___raw_key_1;
	// System.Byte[] System.ServiceModel.Security.Tokens.WrappedKeySecurityToken::wrapped_key
	ByteU5BU5D_t4116647657* ___wrapped_key_2;
	// System.String System.ServiceModel.Security.Tokens.WrappedKeySecurityToken::wrap_alg
	String_t* ___wrap_alg_3;
	// System.IdentityModel.Tokens.SecurityToken System.ServiceModel.Security.Tokens.WrappedKeySecurityToken::wrap_token
	SecurityToken_t1271873540 * ___wrap_token_4;
	// System.IdentityModel.Tokens.SecurityKeyIdentifier System.ServiceModel.Security.Tokens.WrappedKeySecurityToken::wrap_token_ref
	SecurityKeyIdentifier_t806165896 * ___wrap_token_ref_5;
	// System.DateTime System.ServiceModel.Security.Tokens.WrappedKeySecurityToken::valid_from
	DateTime_t3738529785  ___valid_from_6;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IdentityModel.Tokens.SecurityKey> System.ServiceModel.Security.Tokens.WrappedKeySecurityToken::keys
	ReadOnlyCollection_1_t1046284793 * ___keys_7;
	// System.Security.Cryptography.Xml.ReferenceList System.ServiceModel.Security.Tokens.WrappedKeySecurityToken::reference_list
	ReferenceList_t2222396100 * ___reference_list_8;
	// System.Byte[] System.ServiceModel.Security.Tokens.WrappedKeySecurityToken::keyhash
	ByteU5BU5D_t4116647657* ___keyhash_9;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(WrappedKeySecurityToken_t738218815, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier(&___id_0, value);
	}

	inline static int32_t get_offset_of_raw_key_1() { return static_cast<int32_t>(offsetof(WrappedKeySecurityToken_t738218815, ___raw_key_1)); }
	inline ByteU5BU5D_t4116647657* get_raw_key_1() const { return ___raw_key_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_raw_key_1() { return &___raw_key_1; }
	inline void set_raw_key_1(ByteU5BU5D_t4116647657* value)
	{
		___raw_key_1 = value;
		Il2CppCodeGenWriteBarrier(&___raw_key_1, value);
	}

	inline static int32_t get_offset_of_wrapped_key_2() { return static_cast<int32_t>(offsetof(WrappedKeySecurityToken_t738218815, ___wrapped_key_2)); }
	inline ByteU5BU5D_t4116647657* get_wrapped_key_2() const { return ___wrapped_key_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_wrapped_key_2() { return &___wrapped_key_2; }
	inline void set_wrapped_key_2(ByteU5BU5D_t4116647657* value)
	{
		___wrapped_key_2 = value;
		Il2CppCodeGenWriteBarrier(&___wrapped_key_2, value);
	}

	inline static int32_t get_offset_of_wrap_alg_3() { return static_cast<int32_t>(offsetof(WrappedKeySecurityToken_t738218815, ___wrap_alg_3)); }
	inline String_t* get_wrap_alg_3() const { return ___wrap_alg_3; }
	inline String_t** get_address_of_wrap_alg_3() { return &___wrap_alg_3; }
	inline void set_wrap_alg_3(String_t* value)
	{
		___wrap_alg_3 = value;
		Il2CppCodeGenWriteBarrier(&___wrap_alg_3, value);
	}

	inline static int32_t get_offset_of_wrap_token_4() { return static_cast<int32_t>(offsetof(WrappedKeySecurityToken_t738218815, ___wrap_token_4)); }
	inline SecurityToken_t1271873540 * get_wrap_token_4() const { return ___wrap_token_4; }
	inline SecurityToken_t1271873540 ** get_address_of_wrap_token_4() { return &___wrap_token_4; }
	inline void set_wrap_token_4(SecurityToken_t1271873540 * value)
	{
		___wrap_token_4 = value;
		Il2CppCodeGenWriteBarrier(&___wrap_token_4, value);
	}

	inline static int32_t get_offset_of_wrap_token_ref_5() { return static_cast<int32_t>(offsetof(WrappedKeySecurityToken_t738218815, ___wrap_token_ref_5)); }
	inline SecurityKeyIdentifier_t806165896 * get_wrap_token_ref_5() const { return ___wrap_token_ref_5; }
	inline SecurityKeyIdentifier_t806165896 ** get_address_of_wrap_token_ref_5() { return &___wrap_token_ref_5; }
	inline void set_wrap_token_ref_5(SecurityKeyIdentifier_t806165896 * value)
	{
		___wrap_token_ref_5 = value;
		Il2CppCodeGenWriteBarrier(&___wrap_token_ref_5, value);
	}

	inline static int32_t get_offset_of_valid_from_6() { return static_cast<int32_t>(offsetof(WrappedKeySecurityToken_t738218815, ___valid_from_6)); }
	inline DateTime_t3738529785  get_valid_from_6() const { return ___valid_from_6; }
	inline DateTime_t3738529785 * get_address_of_valid_from_6() { return &___valid_from_6; }
	inline void set_valid_from_6(DateTime_t3738529785  value)
	{
		___valid_from_6 = value;
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(WrappedKeySecurityToken_t738218815, ___keys_7)); }
	inline ReadOnlyCollection_1_t1046284793 * get_keys_7() const { return ___keys_7; }
	inline ReadOnlyCollection_1_t1046284793 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(ReadOnlyCollection_1_t1046284793 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier(&___keys_7, value);
	}

	inline static int32_t get_offset_of_reference_list_8() { return static_cast<int32_t>(offsetof(WrappedKeySecurityToken_t738218815, ___reference_list_8)); }
	inline ReferenceList_t2222396100 * get_reference_list_8() const { return ___reference_list_8; }
	inline ReferenceList_t2222396100 ** get_address_of_reference_list_8() { return &___reference_list_8; }
	inline void set_reference_list_8(ReferenceList_t2222396100 * value)
	{
		___reference_list_8 = value;
		Il2CppCodeGenWriteBarrier(&___reference_list_8, value);
	}

	inline static int32_t get_offset_of_keyhash_9() { return static_cast<int32_t>(offsetof(WrappedKeySecurityToken_t738218815, ___keyhash_9)); }
	inline ByteU5BU5D_t4116647657* get_keyhash_9() const { return ___keyhash_9; }
	inline ByteU5BU5D_t4116647657** get_address_of_keyhash_9() { return &___keyhash_9; }
	inline void set_keyhash_9(ByteU5BU5D_t4116647657* value)
	{
		___keyhash_9 = value;
		Il2CppCodeGenWriteBarrier(&___keyhash_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
