﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Me869514973.h"

// System.ServiceModel.Channels.MessageHeaders
struct MessageHeaders_t4050072634;
// System.ServiceModel.Channels.MessageProperties
struct MessageProperties_t4101341573;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.MessageImplBase
struct  MessageImplBase_t2736340907  : public Message_t869514973
{
public:
	// System.ServiceModel.Channels.MessageHeaders System.ServiceModel.Channels.MessageImplBase::headers
	MessageHeaders_t4050072634 * ___headers_3;
	// System.ServiceModel.Channels.MessageProperties System.ServiceModel.Channels.MessageImplBase::properties
	MessageProperties_t4101341573 * ___properties_4;

public:
	inline static int32_t get_offset_of_headers_3() { return static_cast<int32_t>(offsetof(MessageImplBase_t2736340907, ___headers_3)); }
	inline MessageHeaders_t4050072634 * get_headers_3() const { return ___headers_3; }
	inline MessageHeaders_t4050072634 ** get_address_of_headers_3() { return &___headers_3; }
	inline void set_headers_3(MessageHeaders_t4050072634 * value)
	{
		___headers_3 = value;
		Il2CppCodeGenWriteBarrier(&___headers_3, value);
	}

	inline static int32_t get_offset_of_properties_4() { return static_cast<int32_t>(offsetof(MessageImplBase_t2736340907, ___properties_4)); }
	inline MessageProperties_t4101341573 * get_properties_4() const { return ___properties_4; }
	inline MessageProperties_t4101341573 ** get_address_of_properties_4() { return &___properties_4; }
	inline void set_properties_4(MessageProperties_t4101341573 * value)
	{
		___properties_4 = value;
		Il2CppCodeGenWriteBarrier(&___properties_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
