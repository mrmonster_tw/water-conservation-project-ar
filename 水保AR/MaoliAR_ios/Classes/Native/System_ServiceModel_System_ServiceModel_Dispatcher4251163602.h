﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Dispatcher4072893799.h"

// System.ServiceModel.EndpointAddress
struct EndpointAddress_t3119842923;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.EndpointAddressMessageFilter
struct  EndpointAddressMessageFilter_t4251163602  : public MessageFilter_t4072893799
{
public:
	// System.ServiceModel.EndpointAddress System.ServiceModel.Dispatcher.EndpointAddressMessageFilter::address
	EndpointAddress_t3119842923 * ___address_0;
	// System.Boolean System.ServiceModel.Dispatcher.EndpointAddressMessageFilter::cmp_host
	bool ___cmp_host_1;

public:
	inline static int32_t get_offset_of_address_0() { return static_cast<int32_t>(offsetof(EndpointAddressMessageFilter_t4251163602, ___address_0)); }
	inline EndpointAddress_t3119842923 * get_address_0() const { return ___address_0; }
	inline EndpointAddress_t3119842923 ** get_address_of_address_0() { return &___address_0; }
	inline void set_address_0(EndpointAddress_t3119842923 * value)
	{
		___address_0 = value;
		Il2CppCodeGenWriteBarrier(&___address_0, value);
	}

	inline static int32_t get_offset_of_cmp_host_1() { return static_cast<int32_t>(offsetof(EndpointAddressMessageFilter_t4251163602, ___cmp_host_1)); }
	inline bool get_cmp_host_1() const { return ___cmp_host_1; }
	inline bool* get_address_of_cmp_host_1() { return &___cmp_host_1; }
	inline void set_cmp_host_1(bool value)
	{
		___cmp_host_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
