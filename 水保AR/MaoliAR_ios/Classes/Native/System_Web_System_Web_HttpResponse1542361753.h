﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.HttpWorkerRequest
struct HttpWorkerRequest_t998871775;
// System.Web.HttpResponseStream
struct HttpResponseStream_t38354555;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Web.HttpContext
struct HttpContext_t1969259010;
// System.IO.TextWriter
struct TextWriter_t3478189236;
// System.Web.HttpCachePolicy
struct HttpCachePolicy_t379126981;
// System.Text.Encoding
struct Encoding_t1523322056;
// System.Web.HttpCookieCollection
struct HttpCookieCollection_t1178559666;
// System.String
struct String_t;
// System.Web.Caching.CachedRawResponse
struct CachedRawResponse_t4252118804;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t407452768;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpResponse
struct  HttpResponse_t1542361753  : public Il2CppObject
{
public:
	// System.Web.HttpWorkerRequest System.Web.HttpResponse::WorkerRequest
	HttpWorkerRequest_t998871775 * ___WorkerRequest_0;
	// System.Web.HttpResponseStream System.Web.HttpResponse::output_stream
	HttpResponseStream_t38354555 * ___output_stream_1;
	// System.Boolean System.Web.HttpResponse::buffer
	bool ___buffer_2;
	// System.Collections.ArrayList System.Web.HttpResponse::fileDependencies
	ArrayList_t2718874744 * ___fileDependencies_3;
	// System.Web.HttpContext System.Web.HttpResponse::context
	HttpContext_t1969259010 * ___context_4;
	// System.IO.TextWriter System.Web.HttpResponse::writer
	TextWriter_t3478189236 * ___writer_5;
	// System.Web.HttpCachePolicy System.Web.HttpResponse::cache_policy
	HttpCachePolicy_t379126981 * ___cache_policy_6;
	// System.Text.Encoding System.Web.HttpResponse::encoding
	Encoding_t1523322056 * ___encoding_7;
	// System.Web.HttpCookieCollection System.Web.HttpResponse::cookies
	HttpCookieCollection_t1178559666 * ___cookies_8;
	// System.Int32 System.Web.HttpResponse::status_code
	int32_t ___status_code_9;
	// System.String System.Web.HttpResponse::status_description
	String_t* ___status_description_10;
	// System.String System.Web.HttpResponse::content_type
	String_t* ___content_type_11;
	// System.String System.Web.HttpResponse::charset
	String_t* ___charset_12;
	// System.Boolean System.Web.HttpResponse::charset_set
	bool ___charset_set_13;
	// System.Web.Caching.CachedRawResponse System.Web.HttpResponse::cached_response
	CachedRawResponse_t4252118804 * ___cached_response_14;
	// System.String System.Web.HttpResponse::user_cache_control
	String_t* ___user_cache_control_15;
	// System.String System.Web.HttpResponse::redirect_location
	String_t* ___redirect_location_16;
	// System.String System.Web.HttpResponse::version_header
	String_t* ___version_header_17;
	// System.Boolean System.Web.HttpResponse::version_header_checked
	bool ___version_header_checked_18;
	// System.Int64 System.Web.HttpResponse::content_length
	int64_t ___content_length_19;
	// System.Collections.Specialized.NameValueCollection System.Web.HttpResponse::headers
	NameValueCollection_t407452768 * ___headers_20;
	// System.Boolean System.Web.HttpResponse::headers_sent
	bool ___headers_sent_21;
	// System.Collections.Specialized.NameValueCollection System.Web.HttpResponse::cached_headers
	NameValueCollection_t407452768 * ___cached_headers_22;
	// System.String System.Web.HttpResponse::transfer_encoding
	String_t* ___transfer_encoding_23;
	// System.Boolean System.Web.HttpResponse::use_chunked
	bool ___use_chunked_24;
	// System.Boolean System.Web.HttpResponse::closed
	bool ___closed_25;
	// System.Boolean System.Web.HttpResponse::suppress_content
	bool ___suppress_content_26;
	// System.String System.Web.HttpResponse::app_path_mod
	String_t* ___app_path_mod_27;
	// System.Boolean System.Web.HttpResponse::is_request_being_redirected
	bool ___is_request_being_redirected_28;

public:
	inline static int32_t get_offset_of_WorkerRequest_0() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___WorkerRequest_0)); }
	inline HttpWorkerRequest_t998871775 * get_WorkerRequest_0() const { return ___WorkerRequest_0; }
	inline HttpWorkerRequest_t998871775 ** get_address_of_WorkerRequest_0() { return &___WorkerRequest_0; }
	inline void set_WorkerRequest_0(HttpWorkerRequest_t998871775 * value)
	{
		___WorkerRequest_0 = value;
		Il2CppCodeGenWriteBarrier(&___WorkerRequest_0, value);
	}

	inline static int32_t get_offset_of_output_stream_1() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___output_stream_1)); }
	inline HttpResponseStream_t38354555 * get_output_stream_1() const { return ___output_stream_1; }
	inline HttpResponseStream_t38354555 ** get_address_of_output_stream_1() { return &___output_stream_1; }
	inline void set_output_stream_1(HttpResponseStream_t38354555 * value)
	{
		___output_stream_1 = value;
		Il2CppCodeGenWriteBarrier(&___output_stream_1, value);
	}

	inline static int32_t get_offset_of_buffer_2() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___buffer_2)); }
	inline bool get_buffer_2() const { return ___buffer_2; }
	inline bool* get_address_of_buffer_2() { return &___buffer_2; }
	inline void set_buffer_2(bool value)
	{
		___buffer_2 = value;
	}

	inline static int32_t get_offset_of_fileDependencies_3() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___fileDependencies_3)); }
	inline ArrayList_t2718874744 * get_fileDependencies_3() const { return ___fileDependencies_3; }
	inline ArrayList_t2718874744 ** get_address_of_fileDependencies_3() { return &___fileDependencies_3; }
	inline void set_fileDependencies_3(ArrayList_t2718874744 * value)
	{
		___fileDependencies_3 = value;
		Il2CppCodeGenWriteBarrier(&___fileDependencies_3, value);
	}

	inline static int32_t get_offset_of_context_4() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___context_4)); }
	inline HttpContext_t1969259010 * get_context_4() const { return ___context_4; }
	inline HttpContext_t1969259010 ** get_address_of_context_4() { return &___context_4; }
	inline void set_context_4(HttpContext_t1969259010 * value)
	{
		___context_4 = value;
		Il2CppCodeGenWriteBarrier(&___context_4, value);
	}

	inline static int32_t get_offset_of_writer_5() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___writer_5)); }
	inline TextWriter_t3478189236 * get_writer_5() const { return ___writer_5; }
	inline TextWriter_t3478189236 ** get_address_of_writer_5() { return &___writer_5; }
	inline void set_writer_5(TextWriter_t3478189236 * value)
	{
		___writer_5 = value;
		Il2CppCodeGenWriteBarrier(&___writer_5, value);
	}

	inline static int32_t get_offset_of_cache_policy_6() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___cache_policy_6)); }
	inline HttpCachePolicy_t379126981 * get_cache_policy_6() const { return ___cache_policy_6; }
	inline HttpCachePolicy_t379126981 ** get_address_of_cache_policy_6() { return &___cache_policy_6; }
	inline void set_cache_policy_6(HttpCachePolicy_t379126981 * value)
	{
		___cache_policy_6 = value;
		Il2CppCodeGenWriteBarrier(&___cache_policy_6, value);
	}

	inline static int32_t get_offset_of_encoding_7() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___encoding_7)); }
	inline Encoding_t1523322056 * get_encoding_7() const { return ___encoding_7; }
	inline Encoding_t1523322056 ** get_address_of_encoding_7() { return &___encoding_7; }
	inline void set_encoding_7(Encoding_t1523322056 * value)
	{
		___encoding_7 = value;
		Il2CppCodeGenWriteBarrier(&___encoding_7, value);
	}

	inline static int32_t get_offset_of_cookies_8() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___cookies_8)); }
	inline HttpCookieCollection_t1178559666 * get_cookies_8() const { return ___cookies_8; }
	inline HttpCookieCollection_t1178559666 ** get_address_of_cookies_8() { return &___cookies_8; }
	inline void set_cookies_8(HttpCookieCollection_t1178559666 * value)
	{
		___cookies_8 = value;
		Il2CppCodeGenWriteBarrier(&___cookies_8, value);
	}

	inline static int32_t get_offset_of_status_code_9() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___status_code_9)); }
	inline int32_t get_status_code_9() const { return ___status_code_9; }
	inline int32_t* get_address_of_status_code_9() { return &___status_code_9; }
	inline void set_status_code_9(int32_t value)
	{
		___status_code_9 = value;
	}

	inline static int32_t get_offset_of_status_description_10() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___status_description_10)); }
	inline String_t* get_status_description_10() const { return ___status_description_10; }
	inline String_t** get_address_of_status_description_10() { return &___status_description_10; }
	inline void set_status_description_10(String_t* value)
	{
		___status_description_10 = value;
		Il2CppCodeGenWriteBarrier(&___status_description_10, value);
	}

	inline static int32_t get_offset_of_content_type_11() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___content_type_11)); }
	inline String_t* get_content_type_11() const { return ___content_type_11; }
	inline String_t** get_address_of_content_type_11() { return &___content_type_11; }
	inline void set_content_type_11(String_t* value)
	{
		___content_type_11 = value;
		Il2CppCodeGenWriteBarrier(&___content_type_11, value);
	}

	inline static int32_t get_offset_of_charset_12() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___charset_12)); }
	inline String_t* get_charset_12() const { return ___charset_12; }
	inline String_t** get_address_of_charset_12() { return &___charset_12; }
	inline void set_charset_12(String_t* value)
	{
		___charset_12 = value;
		Il2CppCodeGenWriteBarrier(&___charset_12, value);
	}

	inline static int32_t get_offset_of_charset_set_13() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___charset_set_13)); }
	inline bool get_charset_set_13() const { return ___charset_set_13; }
	inline bool* get_address_of_charset_set_13() { return &___charset_set_13; }
	inline void set_charset_set_13(bool value)
	{
		___charset_set_13 = value;
	}

	inline static int32_t get_offset_of_cached_response_14() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___cached_response_14)); }
	inline CachedRawResponse_t4252118804 * get_cached_response_14() const { return ___cached_response_14; }
	inline CachedRawResponse_t4252118804 ** get_address_of_cached_response_14() { return &___cached_response_14; }
	inline void set_cached_response_14(CachedRawResponse_t4252118804 * value)
	{
		___cached_response_14 = value;
		Il2CppCodeGenWriteBarrier(&___cached_response_14, value);
	}

	inline static int32_t get_offset_of_user_cache_control_15() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___user_cache_control_15)); }
	inline String_t* get_user_cache_control_15() const { return ___user_cache_control_15; }
	inline String_t** get_address_of_user_cache_control_15() { return &___user_cache_control_15; }
	inline void set_user_cache_control_15(String_t* value)
	{
		___user_cache_control_15 = value;
		Il2CppCodeGenWriteBarrier(&___user_cache_control_15, value);
	}

	inline static int32_t get_offset_of_redirect_location_16() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___redirect_location_16)); }
	inline String_t* get_redirect_location_16() const { return ___redirect_location_16; }
	inline String_t** get_address_of_redirect_location_16() { return &___redirect_location_16; }
	inline void set_redirect_location_16(String_t* value)
	{
		___redirect_location_16 = value;
		Il2CppCodeGenWriteBarrier(&___redirect_location_16, value);
	}

	inline static int32_t get_offset_of_version_header_17() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___version_header_17)); }
	inline String_t* get_version_header_17() const { return ___version_header_17; }
	inline String_t** get_address_of_version_header_17() { return &___version_header_17; }
	inline void set_version_header_17(String_t* value)
	{
		___version_header_17 = value;
		Il2CppCodeGenWriteBarrier(&___version_header_17, value);
	}

	inline static int32_t get_offset_of_version_header_checked_18() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___version_header_checked_18)); }
	inline bool get_version_header_checked_18() const { return ___version_header_checked_18; }
	inline bool* get_address_of_version_header_checked_18() { return &___version_header_checked_18; }
	inline void set_version_header_checked_18(bool value)
	{
		___version_header_checked_18 = value;
	}

	inline static int32_t get_offset_of_content_length_19() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___content_length_19)); }
	inline int64_t get_content_length_19() const { return ___content_length_19; }
	inline int64_t* get_address_of_content_length_19() { return &___content_length_19; }
	inline void set_content_length_19(int64_t value)
	{
		___content_length_19 = value;
	}

	inline static int32_t get_offset_of_headers_20() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___headers_20)); }
	inline NameValueCollection_t407452768 * get_headers_20() const { return ___headers_20; }
	inline NameValueCollection_t407452768 ** get_address_of_headers_20() { return &___headers_20; }
	inline void set_headers_20(NameValueCollection_t407452768 * value)
	{
		___headers_20 = value;
		Il2CppCodeGenWriteBarrier(&___headers_20, value);
	}

	inline static int32_t get_offset_of_headers_sent_21() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___headers_sent_21)); }
	inline bool get_headers_sent_21() const { return ___headers_sent_21; }
	inline bool* get_address_of_headers_sent_21() { return &___headers_sent_21; }
	inline void set_headers_sent_21(bool value)
	{
		___headers_sent_21 = value;
	}

	inline static int32_t get_offset_of_cached_headers_22() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___cached_headers_22)); }
	inline NameValueCollection_t407452768 * get_cached_headers_22() const { return ___cached_headers_22; }
	inline NameValueCollection_t407452768 ** get_address_of_cached_headers_22() { return &___cached_headers_22; }
	inline void set_cached_headers_22(NameValueCollection_t407452768 * value)
	{
		___cached_headers_22 = value;
		Il2CppCodeGenWriteBarrier(&___cached_headers_22, value);
	}

	inline static int32_t get_offset_of_transfer_encoding_23() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___transfer_encoding_23)); }
	inline String_t* get_transfer_encoding_23() const { return ___transfer_encoding_23; }
	inline String_t** get_address_of_transfer_encoding_23() { return &___transfer_encoding_23; }
	inline void set_transfer_encoding_23(String_t* value)
	{
		___transfer_encoding_23 = value;
		Il2CppCodeGenWriteBarrier(&___transfer_encoding_23, value);
	}

	inline static int32_t get_offset_of_use_chunked_24() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___use_chunked_24)); }
	inline bool get_use_chunked_24() const { return ___use_chunked_24; }
	inline bool* get_address_of_use_chunked_24() { return &___use_chunked_24; }
	inline void set_use_chunked_24(bool value)
	{
		___use_chunked_24 = value;
	}

	inline static int32_t get_offset_of_closed_25() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___closed_25)); }
	inline bool get_closed_25() const { return ___closed_25; }
	inline bool* get_address_of_closed_25() { return &___closed_25; }
	inline void set_closed_25(bool value)
	{
		___closed_25 = value;
	}

	inline static int32_t get_offset_of_suppress_content_26() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___suppress_content_26)); }
	inline bool get_suppress_content_26() const { return ___suppress_content_26; }
	inline bool* get_address_of_suppress_content_26() { return &___suppress_content_26; }
	inline void set_suppress_content_26(bool value)
	{
		___suppress_content_26 = value;
	}

	inline static int32_t get_offset_of_app_path_mod_27() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___app_path_mod_27)); }
	inline String_t* get_app_path_mod_27() const { return ___app_path_mod_27; }
	inline String_t** get_address_of_app_path_mod_27() { return &___app_path_mod_27; }
	inline void set_app_path_mod_27(String_t* value)
	{
		___app_path_mod_27 = value;
		Il2CppCodeGenWriteBarrier(&___app_path_mod_27, value);
	}

	inline static int32_t get_offset_of_is_request_being_redirected_28() { return static_cast<int32_t>(offsetof(HttpResponse_t1542361753, ___is_request_being_redirected_28)); }
	inline bool get_is_request_being_redirected_28() const { return ___is_request_being_redirected_28; }
	inline bool* get_address_of_is_request_being_redirected_28() { return &___is_request_being_redirected_28; }
	inline void set_is_request_being_redirected_28(bool value)
	{
		___is_request_being_redirected_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
