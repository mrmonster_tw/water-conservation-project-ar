﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_Services_System_Web_Services_Descriptio2466402210.h"

// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2760654312;
// System.Web.Services.Description.Message
struct Message_t1729997838;
// System.Web.Services.Description.ServiceDescriptionFormatExtensionCollection
struct ServiceDescriptionFormatExtensionCollection_t2630153888;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.MessagePart
struct  MessagePart_t466850992  : public NamedItem_t2466402210
{
public:
	// System.Xml.XmlQualifiedName System.Web.Services.Description.MessagePart::element
	XmlQualifiedName_t2760654312 * ___element_4;
	// System.Web.Services.Description.Message System.Web.Services.Description.MessagePart::message
	Message_t1729997838 * ___message_5;
	// System.Xml.XmlQualifiedName System.Web.Services.Description.MessagePart::type
	XmlQualifiedName_t2760654312 * ___type_6;
	// System.Web.Services.Description.ServiceDescriptionFormatExtensionCollection System.Web.Services.Description.MessagePart::extensions
	ServiceDescriptionFormatExtensionCollection_t2630153888 * ___extensions_7;

public:
	inline static int32_t get_offset_of_element_4() { return static_cast<int32_t>(offsetof(MessagePart_t466850992, ___element_4)); }
	inline XmlQualifiedName_t2760654312 * get_element_4() const { return ___element_4; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_element_4() { return &___element_4; }
	inline void set_element_4(XmlQualifiedName_t2760654312 * value)
	{
		___element_4 = value;
		Il2CppCodeGenWriteBarrier(&___element_4, value);
	}

	inline static int32_t get_offset_of_message_5() { return static_cast<int32_t>(offsetof(MessagePart_t466850992, ___message_5)); }
	inline Message_t1729997838 * get_message_5() const { return ___message_5; }
	inline Message_t1729997838 ** get_address_of_message_5() { return &___message_5; }
	inline void set_message_5(Message_t1729997838 * value)
	{
		___message_5 = value;
		Il2CppCodeGenWriteBarrier(&___message_5, value);
	}

	inline static int32_t get_offset_of_type_6() { return static_cast<int32_t>(offsetof(MessagePart_t466850992, ___type_6)); }
	inline XmlQualifiedName_t2760654312 * get_type_6() const { return ___type_6; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_type_6() { return &___type_6; }
	inline void set_type_6(XmlQualifiedName_t2760654312 * value)
	{
		___type_6 = value;
		Il2CppCodeGenWriteBarrier(&___type_6, value);
	}

	inline static int32_t get_offset_of_extensions_7() { return static_cast<int32_t>(offsetof(MessagePart_t466850992, ___extensions_7)); }
	inline ServiceDescriptionFormatExtensionCollection_t2630153888 * get_extensions_7() const { return ___extensions_7; }
	inline ServiceDescriptionFormatExtensionCollection_t2630153888 ** get_address_of_extensions_7() { return &___extensions_7; }
	inline void set_extensions_7(ServiceDescriptionFormatExtensionCollection_t2630153888 * value)
	{
		___extensions_7 = value;
		Il2CppCodeGenWriteBarrier(&___extensions_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
