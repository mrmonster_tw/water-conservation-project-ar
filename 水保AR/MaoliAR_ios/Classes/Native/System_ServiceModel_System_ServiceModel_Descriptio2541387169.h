﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.ServiceModel.Description.MessagePartDescriptionCollection
struct MessagePartDescriptionCollection_t1471748572;
// System.ServiceModel.Description.MessagePartDescription
struct MessagePartDescription_t2026856693;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.MessageBodyDescription
struct  MessageBodyDescription_t2541387169  : public Il2CppObject
{
public:
	// System.String System.ServiceModel.Description.MessageBodyDescription::wrapper_name
	String_t* ___wrapper_name_0;
	// System.String System.ServiceModel.Description.MessageBodyDescription::wrapper_ns
	String_t* ___wrapper_ns_1;
	// System.ServiceModel.Description.MessagePartDescriptionCollection System.ServiceModel.Description.MessageBodyDescription::parts
	MessagePartDescriptionCollection_t1471748572 * ___parts_2;
	// System.ServiceModel.Description.MessagePartDescription System.ServiceModel.Description.MessageBodyDescription::return_value
	MessagePartDescription_t2026856693 * ___return_value_3;

public:
	inline static int32_t get_offset_of_wrapper_name_0() { return static_cast<int32_t>(offsetof(MessageBodyDescription_t2541387169, ___wrapper_name_0)); }
	inline String_t* get_wrapper_name_0() const { return ___wrapper_name_0; }
	inline String_t** get_address_of_wrapper_name_0() { return &___wrapper_name_0; }
	inline void set_wrapper_name_0(String_t* value)
	{
		___wrapper_name_0 = value;
		Il2CppCodeGenWriteBarrier(&___wrapper_name_0, value);
	}

	inline static int32_t get_offset_of_wrapper_ns_1() { return static_cast<int32_t>(offsetof(MessageBodyDescription_t2541387169, ___wrapper_ns_1)); }
	inline String_t* get_wrapper_ns_1() const { return ___wrapper_ns_1; }
	inline String_t** get_address_of_wrapper_ns_1() { return &___wrapper_ns_1; }
	inline void set_wrapper_ns_1(String_t* value)
	{
		___wrapper_ns_1 = value;
		Il2CppCodeGenWriteBarrier(&___wrapper_ns_1, value);
	}

	inline static int32_t get_offset_of_parts_2() { return static_cast<int32_t>(offsetof(MessageBodyDescription_t2541387169, ___parts_2)); }
	inline MessagePartDescriptionCollection_t1471748572 * get_parts_2() const { return ___parts_2; }
	inline MessagePartDescriptionCollection_t1471748572 ** get_address_of_parts_2() { return &___parts_2; }
	inline void set_parts_2(MessagePartDescriptionCollection_t1471748572 * value)
	{
		___parts_2 = value;
		Il2CppCodeGenWriteBarrier(&___parts_2, value);
	}

	inline static int32_t get_offset_of_return_value_3() { return static_cast<int32_t>(offsetof(MessageBodyDescription_t2541387169, ___return_value_3)); }
	inline MessagePartDescription_t2026856693 * get_return_value_3() const { return ___return_value_3; }
	inline MessagePartDescription_t2026856693 ** get_address_of_return_value_3() { return &___return_value_3; }
	inline void set_return_value_3(MessagePartDescription_t2026856693 * value)
	{
		___return_value_3 = value;
		Il2CppCodeGenWriteBarrier(&___return_value_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
