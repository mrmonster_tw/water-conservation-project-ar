﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Selectors.SecurityTokenRequirement
struct  SecurityTokenRequirement_t1975057878  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> System.IdentityModel.Selectors.SecurityTokenRequirement::properties
	Dictionary_2_t2865362463 * ___properties_0;

public:
	inline static int32_t get_offset_of_properties_0() { return static_cast<int32_t>(offsetof(SecurityTokenRequirement_t1975057878, ___properties_0)); }
	inline Dictionary_2_t2865362463 * get_properties_0() const { return ___properties_0; }
	inline Dictionary_2_t2865362463 ** get_address_of_properties_0() { return &___properties_0; }
	inline void set_properties_0(Dictionary_2_t2865362463 * value)
	{
		___properties_0 = value;
		Il2CppCodeGenWriteBarrier(&___properties_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
