﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.HtmlControlPersistableAttribute
struct  HtmlControlPersistableAttribute_t4050539499  : public Attribute_t861562559
{
public:
	// System.Boolean System.Web.UI.HtmlControlPersistableAttribute::persist
	bool ___persist_0;

public:
	inline static int32_t get_offset_of_persist_0() { return static_cast<int32_t>(offsetof(HtmlControlPersistableAttribute_t4050539499, ___persist_0)); }
	inline bool get_persist_0() const { return ___persist_0; }
	inline bool* get_address_of_persist_0() { return &___persist_0; }
	inline void set_persist_0(bool value)
	{
		___persist_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
