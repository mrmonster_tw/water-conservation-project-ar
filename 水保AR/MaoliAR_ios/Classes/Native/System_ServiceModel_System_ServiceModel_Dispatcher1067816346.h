﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_B3441673553.h"

// System.ServiceModel.Description.MessageBodyDescription
struct MessageBodyDescription_t2541387169;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.ServiceModel.Dispatcher.DataContractMessagesFormatter
struct DataContractMessagesFormatter_t1452436551;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.DataContractMessagesFormatter/DataContractBodyWriter
struct  DataContractBodyWriter_t1067816346  : public BodyWriter_t3441673553
{
public:
	// System.ServiceModel.Description.MessageBodyDescription System.ServiceModel.Dispatcher.DataContractMessagesFormatter/DataContractBodyWriter::desc
	MessageBodyDescription_t2541387169 * ___desc_1;
	// System.Object[] System.ServiceModel.Dispatcher.DataContractMessagesFormatter/DataContractBodyWriter::parts
	ObjectU5BU5D_t2843939325* ___parts_2;
	// System.ServiceModel.Dispatcher.DataContractMessagesFormatter System.ServiceModel.Dispatcher.DataContractMessagesFormatter/DataContractBodyWriter::parent
	DataContractMessagesFormatter_t1452436551 * ___parent_3;

public:
	inline static int32_t get_offset_of_desc_1() { return static_cast<int32_t>(offsetof(DataContractBodyWriter_t1067816346, ___desc_1)); }
	inline MessageBodyDescription_t2541387169 * get_desc_1() const { return ___desc_1; }
	inline MessageBodyDescription_t2541387169 ** get_address_of_desc_1() { return &___desc_1; }
	inline void set_desc_1(MessageBodyDescription_t2541387169 * value)
	{
		___desc_1 = value;
		Il2CppCodeGenWriteBarrier(&___desc_1, value);
	}

	inline static int32_t get_offset_of_parts_2() { return static_cast<int32_t>(offsetof(DataContractBodyWriter_t1067816346, ___parts_2)); }
	inline ObjectU5BU5D_t2843939325* get_parts_2() const { return ___parts_2; }
	inline ObjectU5BU5D_t2843939325** get_address_of_parts_2() { return &___parts_2; }
	inline void set_parts_2(ObjectU5BU5D_t2843939325* value)
	{
		___parts_2 = value;
		Il2CppCodeGenWriteBarrier(&___parts_2, value);
	}

	inline static int32_t get_offset_of_parent_3() { return static_cast<int32_t>(offsetof(DataContractBodyWriter_t1067816346, ___parent_3)); }
	inline DataContractMessagesFormatter_t1452436551 * get_parent_3() const { return ___parent_3; }
	inline DataContractMessagesFormatter_t1452436551 ** get_address_of_parent_3() { return &___parent_3; }
	inline void set_parent_3(DataContractMessagesFormatter_t1452436551 * value)
	{
		___parent_3 = value;
		Il2CppCodeGenWriteBarrier(&___parent_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
