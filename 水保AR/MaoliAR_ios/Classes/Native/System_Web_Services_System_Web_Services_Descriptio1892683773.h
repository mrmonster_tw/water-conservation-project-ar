﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_Services_System_Web_Services_Description546804031.h"

// System.Web.Services.Description.ServiceDescriptionFormatExtensionCollection
struct ServiceDescriptionFormatExtensionCollection_t2630153888;
// System.Xml.Serialization.XmlSchemas
struct XmlSchemas_t3283371924;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.Types
struct  Types_t1892683773  : public DocumentableItem_t546804031
{
public:
	// System.Web.Services.Description.ServiceDescriptionFormatExtensionCollection System.Web.Services.Description.Types::extensions
	ServiceDescriptionFormatExtensionCollection_t2630153888 * ___extensions_3;
	// System.Xml.Serialization.XmlSchemas System.Web.Services.Description.Types::schemas
	XmlSchemas_t3283371924 * ___schemas_4;

public:
	inline static int32_t get_offset_of_extensions_3() { return static_cast<int32_t>(offsetof(Types_t1892683773, ___extensions_3)); }
	inline ServiceDescriptionFormatExtensionCollection_t2630153888 * get_extensions_3() const { return ___extensions_3; }
	inline ServiceDescriptionFormatExtensionCollection_t2630153888 ** get_address_of_extensions_3() { return &___extensions_3; }
	inline void set_extensions_3(ServiceDescriptionFormatExtensionCollection_t2630153888 * value)
	{
		___extensions_3 = value;
		Il2CppCodeGenWriteBarrier(&___extensions_3, value);
	}

	inline static int32_t get_offset_of_schemas_4() { return static_cast<int32_t>(offsetof(Types_t1892683773, ___schemas_4)); }
	inline XmlSchemas_t3283371924 * get_schemas_4() const { return ___schemas_4; }
	inline XmlSchemas_t3283371924 ** get_address_of_schemas_4() { return &___schemas_4; }
	inline void set_schemas_4(XmlSchemas_t3283371924 * value)
	{
		___schemas_4 = value;
		Il2CppCodeGenWriteBarrier(&___schemas_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
