﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_ObjectStateFormatter_Obje1981273209.h"

// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
struct BinaryFormatter_t3197753202;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.ObjectStateFormatter/SingleRankArrayFormatter
struct  SingleRankArrayFormatter_t1493189349  : public ObjectFormatter_t1981273209
{
public:
	// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter System.Web.UI.ObjectStateFormatter/SingleRankArrayFormatter::_binaryFormatter
	BinaryFormatter_t3197753202 * ____binaryFormatter_11;

public:
	inline static int32_t get_offset_of__binaryFormatter_11() { return static_cast<int32_t>(offsetof(SingleRankArrayFormatter_t1493189349, ____binaryFormatter_11)); }
	inline BinaryFormatter_t3197753202 * get__binaryFormatter_11() const { return ____binaryFormatter_11; }
	inline BinaryFormatter_t3197753202 ** get_address_of__binaryFormatter_11() { return &____binaryFormatter_11; }
	inline void set__binaryFormatter_11(BinaryFormatter_t3197753202 * value)
	{
		____binaryFormatter_11 = value;
		Il2CppCodeGenWriteBarrier(&____binaryFormatter_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
