﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Web_System_Web_InfoTraceData1062706700.h"
#include "System_Web_System_Web_ControlTraceData1335153449.h"
#include "System_Web_System_Web_NameValueTraceData2568000481.h"
#include "System_Web_System_Web_TraceData4177544667.h"
#include "System_Web_System_Web_TraceManager157182125.h"
#include "System_Web_System_Web_TraceMode4125462420.h"
#include "System_Web_System_Web_UI_Adapters_ControlAdapter3811171780.h"
#include "System_Web_System_Web_UI_Adapters_PageAdapter1403647649.h"
#include "System_Web_System_Web_UI_ApplicationFileParser2181270929.h"
#include "System_Web_System_Web_UI_AttributeCollection3488369622.h"
#include "System_Web_System_Web_UI_BaseParser4057001241.h"
#include "System_Web_System_Web_UI_BaseTemplateParser265587934.h"
#include "System_Web_System_Web_UI_BoundPropertyEntry648815465.h"
#include "System_Web_System_Web_UI_ClientScriptManager602632898.h"
#include "System_Web_System_Web_UI_ClientScriptManager_Scrip1923521595.h"
#include "System_Web_System_Web_UI_ClientScriptManager_Scrip1536882111.h"
#include "System_Web_System_Web_UI_CodeBuilder1710888898.h"
#include "System_Web_System_Web_UI_CodeConstructType2869301444.h"
#include "System_Web_System_Web_UI_CodeRenderBuilder1143168716.h"
#include "System_Web_System_Web_UI_CollectionBuilder4289422969.h"
#include "System_Web_System_Web_UI_CompilationMode3525881133.h"
#include "System_Web_System_Web_UI_CompiledBindableTemplateBu868243186.h"
#include "System_Web_System_Web_UI_CompiledTemplateBuilder2316831309.h"
#include "System_Web_System_Web_UI_ConstructorNeedsTagAttrib1222598857.h"
#include "System_Web_System_Web_UI_ControlBuilderAttribute1654838440.h"
#include "System_Web_System_Web_UI_ControlBuilder2523018631.h"
#include "System_Web_System_Web_UI_ControlCollection4212191938.h"
#include "System_Web_System_Web_UI_ControlCollection_SimpleE4072094266.h"
#include "System_Web_System_Web_UI_Control3006474639.h"
#include "System_Web_System_Web_UI_ControlSkin212484848.h"
#include "System_Web_System_Web_UI_ControlValuePropertyAttri2054366362.h"
#include "System_Web_System_Web_UI_CssStyleCollection493833185.h"
#include "System_Web_System_Web_UI_DataBinder4263271672.h"
#include "System_Web_System_Web_UI_DataBindingBuilder2950691791.h"
#include "System_Web_System_Web_UI_DataBindingHandlerAttribu3675219768.h"
#include "System_Web_System_Web_UI_DataBoundLiteralControl1125045578.h"
#include "System_Web_System_Web_UI_DataSourceCapabilities29834462.h"
#include "System_Web_System_Web_UI_DataSourceSelectArguments3537378383.h"
#include "System_Web_System_Web_UI_DataSourceView281661168.h"
#include "System_Web_System_Web_UI_EmptyControlCollection2839671886.h"
#include "System_Web_System_Web_UI_FileLevelUserControlBuilde908632178.h"
#include "System_Web_System_Web_UI_FileLevelControlBuilderAt1394581703.h"
#include "System_Web_System_Web_UI_FileLevelPageControlBuild3675283787.h"
#include "System_Web_System_Web_UI_FilterableAttribute745406530.h"
#include "System_Web_System_Web_UI_HiddenFieldPageStatePersis564913548.h"
#include "System_Web_System_Web_UI_HtmlControlPersistableAtt4050539499.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlAnchor1702942910.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlButton397777233.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlContainer641877197.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlControl1899600556.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlEmptyTag1402868694.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlForm2905737469.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlGenericC1038584562.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlHeadBuild470232228.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlHead3571196538.h"
#include "System_Web_System_Web_UI_HtmlControls_StyleSheetBa3875810066.h"
#include "System_Web_System_Web_UI_HtmlControls_StyleSheetBa2655376168.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlImage3477587105.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlInputButt695971026.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlInputChe4276025461.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlInputCon1839583126.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlInputFil3149771952.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlInputHidd210695890.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlInputIma1805692912.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlInputPas1498141529.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlInputRad1960353643.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlInputRes1856392234.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlInputSub3730901663.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlInputTex3686373506.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlLink758074721.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlMeta2409290611.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlSelectBu3979072189.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlSelect1438637324.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlSelect_U1645920000.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlTableCel1920277368.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlTableCel2648487263.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlTable1386773926.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlTable_Ht1750687539.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlTableRow3030096891.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlTableRow3266733544.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlTextArea1156814009.h"
#include "System_Web_System_Web_UI_HtmlControls_HtmlTitle1385856414.h"
#include "System_Web_System_Web_UI_HtmlTextWriterAttribute2713090206.h"
#include "System_Web_System_Web_UI_HtmlTextWriter1404937429.h"
#include "System_Web_System_Web_UI_HtmlTextWriter_AddedTag1198678936.h"
#include "System_Web_System_Web_UI_HtmlTextWriter_AddedStyle611321135.h"
#include "System_Web_System_Web_UI_HtmlTextWriter_AddedAttr2359971688.h"
#include "System_Web_System_Web_UI_HtmlTextWriter_TagType3523149326.h"
#include "System_Web_System_Web_UI_HtmlTextWriter_HtmlTag2430431696.h"
#include "System_Web_System_Web_UI_HtmlTextWriter_HtmlStyle2610452647.h"
#include "System_Web_System_Web_UI_HtmlTextWriter_HtmlAttribu421148674.h"
#include "System_Web_System_Web_UI_HtmlTextWriterStyle3329380222.h"
#include "System_Web_System_Web_UI_HtmlTextWriterTag4267392773.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3400 = { sizeof (InfoTraceData_t1062706700), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3400[6] = 
{
	InfoTraceData_t1062706700::get_offset_of_Category_0(),
	InfoTraceData_t1062706700::get_offset_of_Message_1(),
	InfoTraceData_t1062706700::get_offset_of_Exception_2(),
	InfoTraceData_t1062706700::get_offset_of_TimeSinceFirst_3(),
	InfoTraceData_t1062706700::get_offset_of_TimeSinceLast_4(),
	InfoTraceData_t1062706700::get_offset_of_IsWarning_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3401 = { sizeof (ControlTraceData_t1335153449), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3401[6] = 
{
	ControlTraceData_t1335153449::get_offset_of_ControlId_0(),
	ControlTraceData_t1335153449::get_offset_of_Type_1(),
	ControlTraceData_t1335153449::get_offset_of_RenderSize_2(),
	ControlTraceData_t1335153449::get_offset_of_ViewstateSize_3(),
	ControlTraceData_t1335153449::get_offset_of_Depth_4(),
	ControlTraceData_t1335153449::get_offset_of_ControlstateSize_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3402 = { sizeof (NameValueTraceData_t2568000481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3402[2] = 
{
	NameValueTraceData_t2568000481::get_offset_of_Name_0(),
	NameValueTraceData_t2568000481::get_offset_of_Value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3403 = { sizeof (TraceData_t4177544667), -1, sizeof(TraceData_t4177544667_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3403[21] = 
{
	TraceData_t4177544667::get_offset_of_is_first_time_0(),
	TraceData_t4177544667::get_offset_of_first_time_1(),
	TraceData_t4177544667::get_offset_of_prev_time_2(),
	TraceData_t4177544667::get_offset_of_info_3(),
	TraceData_t4177544667::get_offset_of_control_data_4(),
	TraceData_t4177544667::get_offset_of_cookie_data_5(),
	TraceData_t4177544667::get_offset_of_header_data_6(),
	TraceData_t4177544667::get_offset_of_servervar_data_7(),
	TraceData_t4177544667::get_offset_of_ctrl_cs_8(),
	TraceData_t4177544667::get_offset_of_request_path_9(),
	TraceData_t4177544667::get_offset_of_session_id_10(),
	TraceData_t4177544667::get_offset_of_request_time_11(),
	TraceData_t4177544667::get_offset_of_request_encoding_12(),
	TraceData_t4177544667::get_offset_of_response_encoding_13(),
	TraceData_t4177544667::get_offset_of_request_type_14(),
	TraceData_t4177544667::get_offset_of_status_code_15(),
	TraceData_t4177544667::get_offset_of_page_16(),
	TraceData_t4177544667::get_offset_of__traceMode_17(),
	TraceData_t4177544667::get_offset_of_sizes_18(),
	TraceData_t4177544667::get_offset_of_ctrl_vs_19(),
	TraceData_t4177544667_StaticFields::get_offset_of_U3CU3Ef__amU24cache14_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3404 = { sizeof (TraceManager_t157182125), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3404[8] = 
{
	TraceManager_t157182125::get_offset_of_enabled_0(),
	TraceManager_t157182125::get_offset_of_local_only_1(),
	TraceManager_t157182125::get_offset_of_page_output_2(),
	TraceManager_t157182125::get_offset_of_mode_3(),
	TraceManager_t157182125::get_offset_of_request_limit_4(),
	TraceManager_t157182125::get_offset_of_cur_item_5(),
	TraceManager_t157182125::get_offset_of_data_6(),
	TraceManager_t157182125::get_offset_of_initialException_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3405 = { sizeof (TraceMode_t4125462420)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3405[4] = 
{
	TraceMode_t4125462420::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3406 = { sizeof (ControlAdapter_t3811171780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3406[1] = 
{
	ControlAdapter_t3811171780::get_offset_of_control_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3407 = { sizeof (PageAdapter_t1403647649), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3408 = { sizeof (ApplicationFileParser_t2181270929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3408[1] = 
{
	ApplicationFileParser_t2181270929::get_offset_of_reader_57(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3409 = { sizeof (AttributeCollection_t3488369622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3409[2] = 
{
	AttributeCollection_t3488369622::get_offset_of_bag_0(),
	AttributeCollection_t3488369622::get_offset_of_styleCollection_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3410 = { sizeof (BaseParser_t4057001241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3410[5] = 
{
	BaseParser_t4057001241::get_offset_of_context_0(),
	BaseParser_t4057001241::get_offset_of_baseDir_1(),
	BaseParser_t4057001241::get_offset_of_baseVDir_2(),
	BaseParser_t4057001241::get_offset_of_location_3(),
	BaseParser_t4057001241::get_offset_of_U3CVirtualPathU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3411 = { sizeof (BaseTemplateParser_t265587934), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3412 = { sizeof (BoundPropertyEntry_t648815465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3412[4] = 
{
	BoundPropertyEntry_t648815465::get_offset_of_expression_3(),
	BoundPropertyEntry_t648815465::get_offset_of_expressionPrefix_4(),
	BoundPropertyEntry_t648815465::get_offset_of_generated_5(),
	BoundPropertyEntry_t648815465::get_offset_of_useSetAttribute_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3413 = { sizeof (ClientScriptManager_t602632898), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3413[15] = 
{
	ClientScriptManager_t602632898::get_offset_of_registeredArrayDeclares_0(),
	ClientScriptManager_t602632898::get_offset_of_clientScriptBlocks_1(),
	ClientScriptManager_t602632898::get_offset_of_startupScriptBlocks_2(),
	ClientScriptManager_t602632898::get_offset_of_hiddenFields_3(),
	ClientScriptManager_t602632898::get_offset_of_submitStatements_4(),
	ClientScriptManager_t602632898::get_offset_of_page_5(),
	ClientScriptManager_t602632898::get_offset_of_eventValidationValues_6(),
	ClientScriptManager_t602632898::get_offset_of_eventValidationPos_7(),
	ClientScriptManager_t602632898::get_offset_of_expandoAttributes_8(),
	ClientScriptManager_t602632898::get_offset_of__hasRegisteredForEventValidationOnCallback_9(),
	ClientScriptManager_t602632898::get_offset_of__pageInRender_10(),
	ClientScriptManager_t602632898::get_offset_of__initCallBackRegistered_11(),
	ClientScriptManager_t602632898::get_offset_of__webFormClientScriptRendered_12(),
	ClientScriptManager_t602632898::get_offset_of__webFormClientScriptRequired_13(),
	ClientScriptManager_t602632898::get_offset_of__scriptTagOpened_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3414 = { sizeof (ScriptEntry_t1923521595), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3414[5] = 
{
	ScriptEntry_t1923521595::get_offset_of_Type_0(),
	ScriptEntry_t1923521595::get_offset_of_Key_1(),
	ScriptEntry_t1923521595::get_offset_of_Script_2(),
	ScriptEntry_t1923521595::get_offset_of_Format_3(),
	ScriptEntry_t1923521595::get_offset_of_Next_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3415 = { sizeof (ScriptEntryFormat_t1536882111)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3415[4] = 
{
	ScriptEntryFormat_t1536882111::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3416 = { sizeof (CodeBuilder_t1710888898), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3416[2] = 
{
	CodeBuilder_t1710888898::get_offset_of_code_28(),
	CodeBuilder_t1710888898::get_offset_of_isAssign_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3417 = { sizeof (CodeConstructType_t2869301444)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3417[5] = 
{
	CodeConstructType_t2869301444::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3418 = { sizeof (CodeRenderBuilder_t1143168716), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3419 = { sizeof (CollectionBuilder_t4289422969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3419[1] = 
{
	CollectionBuilder_t4289422969::get_offset_of_possibleElementTypes_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3420 = { sizeof (CompilationMode_t3525881133)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3420[4] = 
{
	CompilationMode_t3525881133::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3421 = { sizeof (CompiledBindableTemplateBuilder_t868243186), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3422 = { sizeof (CompiledTemplateBuilder_t2316831309), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3423 = { sizeof (ConstructorNeedsTagAttribute_t1222598857), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3423[1] = 
{
	ConstructorNeedsTagAttribute_t1222598857::get_offset_of_needsTag_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3424 = { sizeof (ControlBuilderAttribute_t1654838440), -1, sizeof(ControlBuilderAttribute_t1654838440_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3424[2] = 
{
	ControlBuilderAttribute_t1654838440::get_offset_of_builderType_0(),
	ControlBuilderAttribute_t1654838440_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3425 = { sizeof (ControlBuilder_t2523018631), -1, sizeof(ControlBuilder_t2523018631_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3425[28] = 
{
	ControlBuilder_t2523018631_StaticFields::get_offset_of_FlagsNoCase_0(),
	ControlBuilder_t2523018631::get_offset_of_myNamingContainer_1(),
	ControlBuilder_t2523018631::get_offset_of_parser_2(),
	ControlBuilder_t2523018631::get_offset_of_parserType_3(),
	ControlBuilder_t2523018631::get_offset_of_parentBuilder_4(),
	ControlBuilder_t2523018631::get_offset_of_type_5(),
	ControlBuilder_t2523018631::get_offset_of_tagName_6(),
	ControlBuilder_t2523018631::get_offset_of_originalTagName_7(),
	ControlBuilder_t2523018631::get_offset_of_id_8(),
	ControlBuilder_t2523018631::get_offset_of_attribs_9(),
	ControlBuilder_t2523018631::get_offset_of_line_10(),
	ControlBuilder_t2523018631::get_offset_of_fileName_11(),
	ControlBuilder_t2523018631::get_offset_of_childrenAsProperties_12(),
	ControlBuilder_t2523018631::get_offset_of_isIParserAccessor_13(),
	ControlBuilder_t2523018631::get_offset_of_hasAspCode_14(),
	ControlBuilder_t2523018631::get_offset_of_defaultPropertyBuilder_15(),
	ControlBuilder_t2523018631::get_offset_of_children_16(),
	ControlBuilder_t2523018631::get_offset_of_templateChildren_17(),
	ControlBuilder_t2523018631_StaticFields::get_offset_of_nextID_18(),
	ControlBuilder_t2523018631::get_offset_of_haveParserVariable_19(),
	ControlBuilder_t2523018631::get_offset_of_method_20(),
	ControlBuilder_t2523018631::get_offset_of_methodStatements_21(),
	ControlBuilder_t2523018631::get_offset_of_renderMethod_22(),
	ControlBuilder_t2523018631::get_offset_of_renderIndex_23(),
	ControlBuilder_t2523018631::get_offset_of_isProperty_24(),
	ControlBuilder_t2523018631::get_offset_of_location_25(),
	ControlBuilder_t2523018631::get_offset_of_otherTags_26(),
	ControlBuilder_t2523018631::get_offset_of_U3CDataBindingMethodU3Ek__BackingField_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3426 = { sizeof (ControlCollection_t4212191938), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3426[5] = 
{
	ControlCollection_t4212191938::get_offset_of_owner_0(),
	ControlCollection_t4212191938::get_offset_of_controls_1(),
	ControlCollection_t4212191938::get_offset_of_version_2(),
	ControlCollection_t4212191938::get_offset_of_count_3(),
	ControlCollection_t4212191938::get_offset_of_readOnly_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3427 = { sizeof (SimpleEnumerator_t4072094266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3427[4] = 
{
	SimpleEnumerator_t4072094266::get_offset_of_coll_0(),
	SimpleEnumerator_t4072094266::get_offset_of_index_1(),
	SimpleEnumerator_t4072094266::get_offset_of_version_2(),
	SimpleEnumerator_t4072094266::get_offset_of_currentElement_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3428 = { sizeof (Control_t3006474639), -1, sizeof(Control_t3006474639_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3428[28] = 
{
	Control_t3006474639_StaticFields::get_offset_of_DataBindingEvent_0(),
	Control_t3006474639_StaticFields::get_offset_of_DisposedEvent_1(),
	Control_t3006474639_StaticFields::get_offset_of_InitEvent_2(),
	Control_t3006474639_StaticFields::get_offset_of_LoadEvent_3(),
	Control_t3006474639_StaticFields::get_offset_of_PreRenderEvent_4(),
	Control_t3006474639_StaticFields::get_offset_of_UnloadEvent_5(),
	Control_t3006474639_StaticFields::get_offset_of_defaultNameArray_6(),
	Control_t3006474639::get_offset_of_event_mask_7(),
	Control_t3006474639::get_offset_of_uniqueID_8(),
	Control_t3006474639::get_offset_of__userId_9(),
	Control_t3006474639::get_offset_of__controls_10(),
	Control_t3006474639::get_offset_of__namingContainer_11(),
	Control_t3006474639::get_offset_of__page_12(),
	Control_t3006474639::get_offset_of__parent_13(),
	Control_t3006474639::get_offset_of__site_14(),
	Control_t3006474639::get_offset_of__viewState_15(),
	Control_t3006474639::get_offset_of__events_16(),
	Control_t3006474639::get_offset_of__renderMethodDelegate_17(),
	Control_t3006474639::get_offset_of__controlsCache_18(),
	Control_t3006474639::get_offset_of_defaultNumberID_19(),
	Control_t3006474639::get_offset_of_pendingVS_20(),
	Control_t3006474639::get_offset_of__templateControl_21(),
	Control_t3006474639::get_offset_of__templateSourceDirectory_22(),
	Control_t3006474639::get_offset_of_stateMask_23(),
	Control_t3006474639::get_offset_of_adapter_24(),
	Control_t3006474639::get_offset_of_did_adapter_lookup_25(),
	Control_t3006474639::get_offset_of_skinId_26(),
	Control_t3006474639::get_offset_of__enableTheming_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3429 = { sizeof (ControlSkin_t212484848), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3429[1] = 
{
	ControlSkin_t212484848::get_offset_of_themeDelegate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3430 = { sizeof (ControlValuePropertyAttribute_t2054366362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3430[3] = 
{
	ControlValuePropertyAttribute_t2054366362::get_offset_of_propertyName_0(),
	ControlValuePropertyAttribute_t2054366362::get_offset_of_propertyValue_1(),
	ControlValuePropertyAttribute_t2054366362::get_offset_of_propertyType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3431 = { sizeof (CssStyleCollection_t493833185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3431[3] = 
{
	CssStyleCollection_t493833185::get_offset_of_bag_0(),
	CssStyleCollection_t493833185::get_offset_of_style_1(),
	CssStyleCollection_t493833185::get_offset_of__value_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3432 = { sizeof (DataBinder_t4263271672), -1, 0, sizeof(DataBinder_t4263271672_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable3432[1] = 
{
	THREAD_STATIC_FIELD_OFFSET,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3433 = { sizeof (DataBindingBuilder_t2950691791), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3434 = { sizeof (DataBindingHandlerAttribute_t3675219768), -1, sizeof(DataBindingHandlerAttribute_t3675219768_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3434[2] = 
{
	DataBindingHandlerAttribute_t3675219768::get_offset_of_name_0(),
	DataBindingHandlerAttribute_t3675219768_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3435 = { sizeof (DataBoundLiteralControl_t1125045578), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3435[2] = 
{
	DataBoundLiteralControl_t1125045578::get_offset_of_staticLiterals_28(),
	DataBoundLiteralControl_t1125045578::get_offset_of_dataBoundLiterals_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3436 = { sizeof (DataSourceCapabilities_t29834462)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3436[5] = 
{
	DataSourceCapabilities_t29834462::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3437 = { sizeof (DataSourceSelectArguments_t3537378383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3437[6] = 
{
	DataSourceSelectArguments_t3537378383::get_offset_of_sortExpression_0(),
	DataSourceSelectArguments_t3537378383::get_offset_of_startingRowIndex_1(),
	DataSourceSelectArguments_t3537378383::get_offset_of_maxRows_2(),
	DataSourceSelectArguments_t3537378383::get_offset_of_getTotalRowCount_3(),
	DataSourceSelectArguments_t3537378383::get_offset_of_totalRowCount_4(),
	DataSourceSelectArguments_t3537378383::get_offset_of_dsc_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3438 = { sizeof (DataSourceView_t281661168), -1, sizeof(DataSourceView_t281661168_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3438[2] = 
{
	DataSourceView_t281661168::get_offset_of_eventsList_0(),
	DataSourceView_t281661168_StaticFields::get_offset_of_EventDataSourceViewChanged_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3439 = { sizeof (EmptyControlCollection_t2839671886), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3440 = { sizeof (FileLevelUserControlBuilder_t908632178), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3441 = { sizeof (FileLevelControlBuilderAttribute_t1394581703), -1, sizeof(FileLevelControlBuilderAttribute_t1394581703_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3441[2] = 
{
	FileLevelControlBuilderAttribute_t1394581703_StaticFields::get_offset_of_Default_0(),
	FileLevelControlBuilderAttribute_t1394581703::get_offset_of_U3CBuilderTypeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3442 = { sizeof (FileLevelPageControlBuilder_t3675283787), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3442[3] = 
{
	FileLevelPageControlBuilder_t3675283787::get_offset_of_hasContentControls_35(),
	FileLevelPageControlBuilder_t3675283787::get_offset_of_hasLiteralControls_36(),
	FileLevelPageControlBuilder_t3675283787::get_offset_of_hasOtherControls_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3443 = { sizeof (FilterableAttribute_t745406530), -1, sizeof(FilterableAttribute_t745406530_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3443[4] = 
{
	FilterableAttribute_t745406530::get_offset_of_filterable_0(),
	FilterableAttribute_t745406530_StaticFields::get_offset_of_Default_1(),
	FilterableAttribute_t745406530_StaticFields::get_offset_of_No_2(),
	FilterableAttribute_t745406530_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3444 = { sizeof (HiddenFieldPageStatePersister_t564913548), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3445 = { sizeof (HtmlControlPersistableAttribute_t4050539499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3445[1] = 
{
	HtmlControlPersistableAttribute_t4050539499::get_offset_of_persist_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3446 = { sizeof (HtmlAnchor_t1702942910), -1, sizeof(HtmlAnchor_t1702942910_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3446[1] = 
{
	HtmlAnchor_t1702942910_StaticFields::get_offset_of_serverClickEvent_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3447 = { sizeof (HtmlButton_t397777233), -1, sizeof(HtmlButton_t397777233_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3447[1] = 
{
	HtmlButton_t397777233_StaticFields::get_offset_of_ServerClickEvent_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3448 = { sizeof (HtmlContainerControl_t641877197), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3449 = { sizeof (HtmlControl_t1899600556), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3449[2] = 
{
	HtmlControl_t1899600556::get_offset_of__tagName_28(),
	HtmlControl_t1899600556::get_offset_of__attributes_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3450 = { sizeof (HtmlEmptyTagControlBuilder_t1402868694), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3451 = { sizeof (HtmlForm_t2905737469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3451[5] = 
{
	HtmlForm_t2905737469::get_offset_of_inited_30(),
	HtmlForm_t2905737469::get_offset_of__defaultbutton_31(),
	HtmlForm_t2905737469::get_offset_of__defaultfocus_32(),
	HtmlForm_t2905737469::get_offset_of_submitdisabledcontrols_33(),
	HtmlForm_t2905737469::get_offset_of_isUplevel_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3452 = { sizeof (HtmlGenericControl_t1038584562), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3453 = { sizeof (HtmlHeadBuilder_t470232228), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3454 = { sizeof (HtmlHead_t3571196538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3454[3] = 
{
	HtmlHead_t3571196538::get_offset_of_titleText_30(),
	HtmlHead_t3571196538::get_offset_of_title_31(),
	HtmlHead_t3571196538::get_offset_of_styleSheet_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3455 = { sizeof (StyleSheetBag_t3875810066), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3455[1] = 
{
	StyleSheetBag_t3875810066::get_offset_of_entries_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3456 = { sizeof (StyleEntry_t2655376168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3456[3] = 
{
	StyleEntry_t2655376168::get_offset_of_Style_0(),
	StyleEntry_t2655376168::get_offset_of_Selection_1(),
	StyleEntry_t2655376168::get_offset_of_UrlResolver_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3457 = { sizeof (HtmlImage_t3477587105), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3458 = { sizeof (HtmlInputButton_t695971026), -1, sizeof(HtmlInputButton_t695971026_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3458[1] = 
{
	HtmlInputButton_t695971026_StaticFields::get_offset_of_ServerClickEvent_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3459 = { sizeof (HtmlInputCheckBox_t4276025461), -1, sizeof(HtmlInputCheckBox_t4276025461_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3459[1] = 
{
	HtmlInputCheckBox_t4276025461_StaticFields::get_offset_of_EventServerChange_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3460 = { sizeof (HtmlInputControl_t1839583126), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3461 = { sizeof (HtmlInputFile_t3149771952), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3461[1] = 
{
	HtmlInputFile_t3149771952::get_offset_of_posted_file_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3462 = { sizeof (HtmlInputHidden_t210695890), -1, sizeof(HtmlInputHidden_t210695890_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3462[1] = 
{
	HtmlInputHidden_t210695890_StaticFields::get_offset_of_ServerChangeEvent_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3463 = { sizeof (HtmlInputImage_t1805692912), -1, sizeof(HtmlInputImage_t1805692912_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3463[3] = 
{
	HtmlInputImage_t1805692912_StaticFields::get_offset_of_ServerClickEvent_30(),
	HtmlInputImage_t1805692912::get_offset_of_clicked_x_31(),
	HtmlInputImage_t1805692912::get_offset_of_clicked_y_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3464 = { sizeof (HtmlInputPassword_t1498141529), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3465 = { sizeof (HtmlInputRadioButton_t1960353643), -1, sizeof(HtmlInputRadioButton_t1960353643_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3465[1] = 
{
	HtmlInputRadioButton_t1960353643_StaticFields::get_offset_of_serverChangeEvent_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3466 = { sizeof (HtmlInputReset_t1856392234), -1, sizeof(HtmlInputReset_t1856392234_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3466[1] = 
{
	HtmlInputReset_t1856392234_StaticFields::get_offset_of_ServerClickEvent_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3467 = { sizeof (HtmlInputSubmit_t3730901663), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3468 = { sizeof (HtmlInputText_t3686373506), -1, sizeof(HtmlInputText_t3686373506_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3468[1] = 
{
	HtmlInputText_t3686373506_StaticFields::get_offset_of_serverChangeEvent_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3469 = { sizeof (HtmlLink_t758074721), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3470 = { sizeof (HtmlMeta_t2409290611), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3471 = { sizeof (HtmlSelectBuilder_t3979072189), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3472 = { sizeof (HtmlSelect_t1438637324), -1, sizeof(HtmlSelect_t1438637324_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3472[6] = 
{
	HtmlSelect_t1438637324::get_offset_of__boundDataSourceView_30(),
	HtmlSelect_t1438637324::get_offset_of_requiresDataBinding_31(),
	HtmlSelect_t1438637324::get_offset_of__initialized_32(),
	HtmlSelect_t1438637324::get_offset_of_datasource_33(),
	HtmlSelect_t1438637324::get_offset_of_items_34(),
	HtmlSelect_t1438637324_StaticFields::get_offset_of_EventServerChange_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3473 = { sizeof (U3CGetDataU3Ec__AnonStorey7_t1645920000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3473[1] = 
{
	U3CGetDataU3Ec__AnonStorey7_t1645920000::get_offset_of_result_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3474 = { sizeof (HtmlTableCellCollection_t1920277368), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3474[1] = 
{
	HtmlTableCellCollection_t1920277368::get_offset_of_cc_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3475 = { sizeof (HtmlTableCell_t2648487263), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3476 = { sizeof (HtmlTable_t1386773926), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3477 = { sizeof (HtmlTableRowControlCollection_t1750687539), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3478 = { sizeof (HtmlTableRow_t3030096891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3478[1] = 
{
	HtmlTableRow_t3030096891::get_offset_of__cells_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3479 = { sizeof (HtmlTableCellControlCollection_t3266733544), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3480 = { sizeof (HtmlTextArea_t1156814009), -1, sizeof(HtmlTextArea_t1156814009_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3480[1] = 
{
	HtmlTextArea_t1156814009_StaticFields::get_offset_of_serverChangeEvent_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3481 = { sizeof (HtmlTitle_t1385856414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3481[1] = 
{
	HtmlTitle_t1385856414::get_offset_of_text_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3482 = { sizeof (HtmlTextWriterAttribute_t2713090206)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3482[55] = 
{
	HtmlTextWriterAttribute_t2713090206::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3483 = { sizeof (HtmlTextWriter_t1404937429), -1, sizeof(HtmlTextWriter_t1404937429_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3483[16] = 
{
	HtmlTextWriter_t1404937429_StaticFields::get_offset_of__tagTable_4(),
	HtmlTextWriter_t1404937429_StaticFields::get_offset_of__attributeTable_5(),
	HtmlTextWriter_t1404937429_StaticFields::get_offset_of__styleTable_6(),
	HtmlTextWriter_t1404937429::get_offset_of_indent_7(),
	HtmlTextWriter_t1404937429::get_offset_of_b_8(),
	HtmlTextWriter_t1404937429::get_offset_of_tab_string_9(),
	HtmlTextWriter_t1404937429::get_offset_of_newline_10(),
	HtmlTextWriter_t1404937429::get_offset_of_styles_11(),
	HtmlTextWriter_t1404937429::get_offset_of_attrs_12(),
	HtmlTextWriter_t1404937429::get_offset_of_tagstack_13(),
	HtmlTextWriter_t1404937429::get_offset_of_styles_pos_14(),
	HtmlTextWriter_t1404937429::get_offset_of_attrs_pos_15(),
	HtmlTextWriter_t1404937429::get_offset_of_tagstack_pos_16(),
	HtmlTextWriter_t1404937429_StaticFields::get_offset_of_tags_17(),
	HtmlTextWriter_t1404937429_StaticFields::get_offset_of_htmlattrs_18(),
	HtmlTextWriter_t1404937429_StaticFields::get_offset_of_htmlstyles_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3484 = { sizeof (AddedTag_t1198678936)+ sizeof (Il2CppObject), sizeof(AddedTag_t1198678936_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3484[3] = 
{
	AddedTag_t1198678936::get_offset_of_name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AddedTag_t1198678936::get_offset_of_key_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AddedTag_t1198678936::get_offset_of_ignore_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3485 = { sizeof (AddedStyle_t611321135)+ sizeof (Il2CppObject), sizeof(AddedStyle_t611321135_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3485[3] = 
{
	AddedStyle_t611321135::get_offset_of_name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AddedStyle_t611321135::get_offset_of_key_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AddedStyle_t611321135::get_offset_of_value_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3486 = { sizeof (AddedAttr_t2359971688)+ sizeof (Il2CppObject), sizeof(AddedAttr_t2359971688_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3486[3] = 
{
	AddedAttr_t2359971688::get_offset_of_name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AddedAttr_t2359971688::get_offset_of_key_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AddedAttr_t2359971688::get_offset_of_value_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3487 = { sizeof (TagType_t3523149326)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3487[4] = 
{
	TagType_t3523149326::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3488 = { sizeof (HtmlTag_t2430431696), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3488[3] = 
{
	HtmlTag_t2430431696::get_offset_of_key_0(),
	HtmlTag_t2430431696::get_offset_of_name_1(),
	HtmlTag_t2430431696::get_offset_of_tag_type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3489 = { sizeof (HtmlStyle_t2610452647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3489[2] = 
{
	HtmlStyle_t2610452647::get_offset_of_key_0(),
	HtmlStyle_t2610452647::get_offset_of_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3490 = { sizeof (HtmlAttribute_t421148674), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3490[2] = 
{
	HtmlAttribute_t421148674::get_offset_of_key_0(),
	HtmlAttribute_t421148674::get_offset_of_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3491 = { sizeof (HtmlTextWriterStyle_t3329380222)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3491[44] = 
{
	HtmlTextWriterStyle_t3329380222::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3492 = { sizeof (HtmlTextWriterTag_t4267392773)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3492[98] = 
{
	HtmlTextWriterTag_t4267392773::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3493 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3494 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3495 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3496 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3497 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3498 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3499 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
