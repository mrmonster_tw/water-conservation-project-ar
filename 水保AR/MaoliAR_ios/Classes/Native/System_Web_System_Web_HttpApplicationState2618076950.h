﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Collections_Specialized_NameObjectCo2091847364.h"

// System.Web.HttpStaticObjectsCollection
struct HttpStaticObjectsCollection_t518175362;
// System.Threading.ReaderWriterLock
struct ReaderWriterLock_t367846299;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpApplicationState
struct  HttpApplicationState_t2618076950  : public NameObjectCollectionBase_t2091847364
{
public:
	// System.Web.HttpStaticObjectsCollection System.Web.HttpApplicationState::_AppObjects
	HttpStaticObjectsCollection_t518175362 * ____AppObjects_10;
	// System.Web.HttpStaticObjectsCollection System.Web.HttpApplicationState::_SessionObjects
	HttpStaticObjectsCollection_t518175362 * ____SessionObjects_11;
	// System.Threading.ReaderWriterLock System.Web.HttpApplicationState::_Lock
	ReaderWriterLock_t367846299 * ____Lock_12;

public:
	inline static int32_t get_offset_of__AppObjects_10() { return static_cast<int32_t>(offsetof(HttpApplicationState_t2618076950, ____AppObjects_10)); }
	inline HttpStaticObjectsCollection_t518175362 * get__AppObjects_10() const { return ____AppObjects_10; }
	inline HttpStaticObjectsCollection_t518175362 ** get_address_of__AppObjects_10() { return &____AppObjects_10; }
	inline void set__AppObjects_10(HttpStaticObjectsCollection_t518175362 * value)
	{
		____AppObjects_10 = value;
		Il2CppCodeGenWriteBarrier(&____AppObjects_10, value);
	}

	inline static int32_t get_offset_of__SessionObjects_11() { return static_cast<int32_t>(offsetof(HttpApplicationState_t2618076950, ____SessionObjects_11)); }
	inline HttpStaticObjectsCollection_t518175362 * get__SessionObjects_11() const { return ____SessionObjects_11; }
	inline HttpStaticObjectsCollection_t518175362 ** get_address_of__SessionObjects_11() { return &____SessionObjects_11; }
	inline void set__SessionObjects_11(HttpStaticObjectsCollection_t518175362 * value)
	{
		____SessionObjects_11 = value;
		Il2CppCodeGenWriteBarrier(&____SessionObjects_11, value);
	}

	inline static int32_t get_offset_of__Lock_12() { return static_cast<int32_t>(offsetof(HttpApplicationState_t2618076950, ____Lock_12)); }
	inline ReaderWriterLock_t367846299 * get__Lock_12() const { return ____Lock_12; }
	inline ReaderWriterLock_t367846299 ** get_address_of__Lock_12() { return &____Lock_12; }
	inline void set__Lock_12(ReaderWriterLock_t367846299 * value)
	{
		____Lock_12 = value;
		Il2CppCodeGenWriteBarrier(&____Lock_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
