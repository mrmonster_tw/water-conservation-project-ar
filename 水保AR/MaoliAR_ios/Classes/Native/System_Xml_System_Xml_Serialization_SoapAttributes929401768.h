﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Xml.Serialization.SoapAttributeAttribute
struct SoapAttributeAttribute_t495928767;
// System.Object
struct Il2CppObject;
// System.Xml.Serialization.SoapElementAttribute
struct SoapElementAttribute_t1210796251;
// System.Xml.Serialization.SoapEnumAttribute
struct SoapEnumAttribute_t306263779;
// System.Xml.Serialization.SoapTypeAttribute
struct SoapTypeAttribute_t2580020517;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.SoapAttributes
struct  SoapAttributes_t929401768  : public Il2CppObject
{
public:
	// System.Xml.Serialization.SoapAttributeAttribute System.Xml.Serialization.SoapAttributes::soapAttribute
	SoapAttributeAttribute_t495928767 * ___soapAttribute_0;
	// System.Object System.Xml.Serialization.SoapAttributes::soapDefaultValue
	Il2CppObject * ___soapDefaultValue_1;
	// System.Xml.Serialization.SoapElementAttribute System.Xml.Serialization.SoapAttributes::soapElement
	SoapElementAttribute_t1210796251 * ___soapElement_2;
	// System.Xml.Serialization.SoapEnumAttribute System.Xml.Serialization.SoapAttributes::soapEnum
	SoapEnumAttribute_t306263779 * ___soapEnum_3;
	// System.Boolean System.Xml.Serialization.SoapAttributes::soapIgnore
	bool ___soapIgnore_4;
	// System.Xml.Serialization.SoapTypeAttribute System.Xml.Serialization.SoapAttributes::soapType
	SoapTypeAttribute_t2580020517 * ___soapType_5;

public:
	inline static int32_t get_offset_of_soapAttribute_0() { return static_cast<int32_t>(offsetof(SoapAttributes_t929401768, ___soapAttribute_0)); }
	inline SoapAttributeAttribute_t495928767 * get_soapAttribute_0() const { return ___soapAttribute_0; }
	inline SoapAttributeAttribute_t495928767 ** get_address_of_soapAttribute_0() { return &___soapAttribute_0; }
	inline void set_soapAttribute_0(SoapAttributeAttribute_t495928767 * value)
	{
		___soapAttribute_0 = value;
		Il2CppCodeGenWriteBarrier(&___soapAttribute_0, value);
	}

	inline static int32_t get_offset_of_soapDefaultValue_1() { return static_cast<int32_t>(offsetof(SoapAttributes_t929401768, ___soapDefaultValue_1)); }
	inline Il2CppObject * get_soapDefaultValue_1() const { return ___soapDefaultValue_1; }
	inline Il2CppObject ** get_address_of_soapDefaultValue_1() { return &___soapDefaultValue_1; }
	inline void set_soapDefaultValue_1(Il2CppObject * value)
	{
		___soapDefaultValue_1 = value;
		Il2CppCodeGenWriteBarrier(&___soapDefaultValue_1, value);
	}

	inline static int32_t get_offset_of_soapElement_2() { return static_cast<int32_t>(offsetof(SoapAttributes_t929401768, ___soapElement_2)); }
	inline SoapElementAttribute_t1210796251 * get_soapElement_2() const { return ___soapElement_2; }
	inline SoapElementAttribute_t1210796251 ** get_address_of_soapElement_2() { return &___soapElement_2; }
	inline void set_soapElement_2(SoapElementAttribute_t1210796251 * value)
	{
		___soapElement_2 = value;
		Il2CppCodeGenWriteBarrier(&___soapElement_2, value);
	}

	inline static int32_t get_offset_of_soapEnum_3() { return static_cast<int32_t>(offsetof(SoapAttributes_t929401768, ___soapEnum_3)); }
	inline SoapEnumAttribute_t306263779 * get_soapEnum_3() const { return ___soapEnum_3; }
	inline SoapEnumAttribute_t306263779 ** get_address_of_soapEnum_3() { return &___soapEnum_3; }
	inline void set_soapEnum_3(SoapEnumAttribute_t306263779 * value)
	{
		___soapEnum_3 = value;
		Il2CppCodeGenWriteBarrier(&___soapEnum_3, value);
	}

	inline static int32_t get_offset_of_soapIgnore_4() { return static_cast<int32_t>(offsetof(SoapAttributes_t929401768, ___soapIgnore_4)); }
	inline bool get_soapIgnore_4() const { return ___soapIgnore_4; }
	inline bool* get_address_of_soapIgnore_4() { return &___soapIgnore_4; }
	inline void set_soapIgnore_4(bool value)
	{
		___soapIgnore_4 = value;
	}

	inline static int32_t get_offset_of_soapType_5() { return static_cast<int32_t>(offsetof(SoapAttributes_t929401768, ___soapType_5)); }
	inline SoapTypeAttribute_t2580020517 * get_soapType_5() const { return ___soapType_5; }
	inline SoapTypeAttribute_t2580020517 ** get_address_of_soapType_5() { return &___soapType_5; }
	inline void set_soapType_5(SoapTypeAttribute_t2580020517 * value)
	{
		___soapType_5 = value;
		Il2CppCodeGenWriteBarrier(&___soapType_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
