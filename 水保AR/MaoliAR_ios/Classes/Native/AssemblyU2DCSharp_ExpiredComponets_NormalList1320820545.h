﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"

// UnityEngine.RectTransform
struct RectTransform_t3704657025;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExpiredComponets/NormalList
struct  NormalList_t1320820545 
{
public:
	// UnityEngine.RectTransform ExpiredComponets/NormalList::girl
	RectTransform_t3704657025 * ___girl_0;
	// UnityEngine.RectTransform ExpiredComponets/NormalList::buttonList
	RectTransform_t3704657025 * ___buttonList_1;
	// UnityEngine.RectTransform ExpiredComponets/NormalList::msg
	RectTransform_t3704657025 * ___msg_2;

public:
	inline static int32_t get_offset_of_girl_0() { return static_cast<int32_t>(offsetof(NormalList_t1320820545, ___girl_0)); }
	inline RectTransform_t3704657025 * get_girl_0() const { return ___girl_0; }
	inline RectTransform_t3704657025 ** get_address_of_girl_0() { return &___girl_0; }
	inline void set_girl_0(RectTransform_t3704657025 * value)
	{
		___girl_0 = value;
		Il2CppCodeGenWriteBarrier(&___girl_0, value);
	}

	inline static int32_t get_offset_of_buttonList_1() { return static_cast<int32_t>(offsetof(NormalList_t1320820545, ___buttonList_1)); }
	inline RectTransform_t3704657025 * get_buttonList_1() const { return ___buttonList_1; }
	inline RectTransform_t3704657025 ** get_address_of_buttonList_1() { return &___buttonList_1; }
	inline void set_buttonList_1(RectTransform_t3704657025 * value)
	{
		___buttonList_1 = value;
		Il2CppCodeGenWriteBarrier(&___buttonList_1, value);
	}

	inline static int32_t get_offset_of_msg_2() { return static_cast<int32_t>(offsetof(NormalList_t1320820545, ___msg_2)); }
	inline RectTransform_t3704657025 * get_msg_2() const { return ___msg_2; }
	inline RectTransform_t3704657025 ** get_address_of_msg_2() { return &___msg_2; }
	inline void set_msg_2(RectTransform_t3704657025 * value)
	{
		___msg_2 = value;
		Il2CppCodeGenWriteBarrier(&___msg_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of ExpiredComponets/NormalList
struct NormalList_t1320820545_marshaled_pinvoke
{
	RectTransform_t3704657025 * ___girl_0;
	RectTransform_t3704657025 * ___buttonList_1;
	RectTransform_t3704657025 * ___msg_2;
};
// Native definition for COM marshalling of ExpiredComponets/NormalList
struct NormalList_t1320820545_marshaled_com
{
	RectTransform_t3704657025 * ___girl_0;
	RectTransform_t3704657025 * ___buttonList_1;
	RectTransform_t3704657025 * ___msg_2;
};
