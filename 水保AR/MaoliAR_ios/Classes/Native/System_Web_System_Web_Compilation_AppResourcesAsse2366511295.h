﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.Configuration.CompilationSection
struct CompilationSection_t1832220892;
// System.CodeDom.Compiler.CompilerInfo
struct CompilerInfo_t947794800;
// System.CodeDom.Compiler.CodeDomProvider
struct CodeDomProvider_t110349836;
// System.String
struct String_t;
// System.Reflection.Assembly
struct Assembly_t4102432799;
// System.Web.Compilation.AppResourcesCompiler
struct AppResourcesCompiler_t3512896086;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.AppResourcesAssemblyBuilder
struct  AppResourcesAssemblyBuilder_t2366511295  : public Il2CppObject
{
public:
	// System.Web.Configuration.CompilationSection System.Web.Compilation.AppResourcesAssemblyBuilder::config
	CompilationSection_t1832220892 * ___config_0;
	// System.CodeDom.Compiler.CompilerInfo System.Web.Compilation.AppResourcesAssemblyBuilder::ci
	CompilerInfo_t947794800 * ___ci_1;
	// System.CodeDom.Compiler.CodeDomProvider System.Web.Compilation.AppResourcesAssemblyBuilder::_provider
	CodeDomProvider_t110349836 * ____provider_2;
	// System.String System.Web.Compilation.AppResourcesAssemblyBuilder::baseAssemblyPath
	String_t* ___baseAssemblyPath_3;
	// System.String System.Web.Compilation.AppResourcesAssemblyBuilder::baseAssemblyDirectory
	String_t* ___baseAssemblyDirectory_4;
	// System.String System.Web.Compilation.AppResourcesAssemblyBuilder::canonicAssemblyName
	String_t* ___canonicAssemblyName_5;
	// System.Reflection.Assembly System.Web.Compilation.AppResourcesAssemblyBuilder::mainAssembly
	Assembly_t4102432799 * ___mainAssembly_6;
	// System.Web.Compilation.AppResourcesCompiler System.Web.Compilation.AppResourcesAssemblyBuilder::appResourcesCompiler
	AppResourcesCompiler_t3512896086 * ___appResourcesCompiler_7;

public:
	inline static int32_t get_offset_of_config_0() { return static_cast<int32_t>(offsetof(AppResourcesAssemblyBuilder_t2366511295, ___config_0)); }
	inline CompilationSection_t1832220892 * get_config_0() const { return ___config_0; }
	inline CompilationSection_t1832220892 ** get_address_of_config_0() { return &___config_0; }
	inline void set_config_0(CompilationSection_t1832220892 * value)
	{
		___config_0 = value;
		Il2CppCodeGenWriteBarrier(&___config_0, value);
	}

	inline static int32_t get_offset_of_ci_1() { return static_cast<int32_t>(offsetof(AppResourcesAssemblyBuilder_t2366511295, ___ci_1)); }
	inline CompilerInfo_t947794800 * get_ci_1() const { return ___ci_1; }
	inline CompilerInfo_t947794800 ** get_address_of_ci_1() { return &___ci_1; }
	inline void set_ci_1(CompilerInfo_t947794800 * value)
	{
		___ci_1 = value;
		Il2CppCodeGenWriteBarrier(&___ci_1, value);
	}

	inline static int32_t get_offset_of__provider_2() { return static_cast<int32_t>(offsetof(AppResourcesAssemblyBuilder_t2366511295, ____provider_2)); }
	inline CodeDomProvider_t110349836 * get__provider_2() const { return ____provider_2; }
	inline CodeDomProvider_t110349836 ** get_address_of__provider_2() { return &____provider_2; }
	inline void set__provider_2(CodeDomProvider_t110349836 * value)
	{
		____provider_2 = value;
		Il2CppCodeGenWriteBarrier(&____provider_2, value);
	}

	inline static int32_t get_offset_of_baseAssemblyPath_3() { return static_cast<int32_t>(offsetof(AppResourcesAssemblyBuilder_t2366511295, ___baseAssemblyPath_3)); }
	inline String_t* get_baseAssemblyPath_3() const { return ___baseAssemblyPath_3; }
	inline String_t** get_address_of_baseAssemblyPath_3() { return &___baseAssemblyPath_3; }
	inline void set_baseAssemblyPath_3(String_t* value)
	{
		___baseAssemblyPath_3 = value;
		Il2CppCodeGenWriteBarrier(&___baseAssemblyPath_3, value);
	}

	inline static int32_t get_offset_of_baseAssemblyDirectory_4() { return static_cast<int32_t>(offsetof(AppResourcesAssemblyBuilder_t2366511295, ___baseAssemblyDirectory_4)); }
	inline String_t* get_baseAssemblyDirectory_4() const { return ___baseAssemblyDirectory_4; }
	inline String_t** get_address_of_baseAssemblyDirectory_4() { return &___baseAssemblyDirectory_4; }
	inline void set_baseAssemblyDirectory_4(String_t* value)
	{
		___baseAssemblyDirectory_4 = value;
		Il2CppCodeGenWriteBarrier(&___baseAssemblyDirectory_4, value);
	}

	inline static int32_t get_offset_of_canonicAssemblyName_5() { return static_cast<int32_t>(offsetof(AppResourcesAssemblyBuilder_t2366511295, ___canonicAssemblyName_5)); }
	inline String_t* get_canonicAssemblyName_5() const { return ___canonicAssemblyName_5; }
	inline String_t** get_address_of_canonicAssemblyName_5() { return &___canonicAssemblyName_5; }
	inline void set_canonicAssemblyName_5(String_t* value)
	{
		___canonicAssemblyName_5 = value;
		Il2CppCodeGenWriteBarrier(&___canonicAssemblyName_5, value);
	}

	inline static int32_t get_offset_of_mainAssembly_6() { return static_cast<int32_t>(offsetof(AppResourcesAssemblyBuilder_t2366511295, ___mainAssembly_6)); }
	inline Assembly_t4102432799 * get_mainAssembly_6() const { return ___mainAssembly_6; }
	inline Assembly_t4102432799 ** get_address_of_mainAssembly_6() { return &___mainAssembly_6; }
	inline void set_mainAssembly_6(Assembly_t4102432799 * value)
	{
		___mainAssembly_6 = value;
		Il2CppCodeGenWriteBarrier(&___mainAssembly_6, value);
	}

	inline static int32_t get_offset_of_appResourcesCompiler_7() { return static_cast<int32_t>(offsetof(AppResourcesAssemblyBuilder_t2366511295, ___appResourcesCompiler_7)); }
	inline AppResourcesCompiler_t3512896086 * get_appResourcesCompiler_7() const { return ___appResourcesCompiler_7; }
	inline AppResourcesCompiler_t3512896086 ** get_address_of_appResourcesCompiler_7() { return &___appResourcesCompiler_7; }
	inline void set_appResourcesCompiler_7(AppResourcesCompiler_t3512896086 * value)
	{
		___appResourcesCompiler_7 = value;
		Il2CppCodeGenWriteBarrier(&___appResourcesCompiler_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
