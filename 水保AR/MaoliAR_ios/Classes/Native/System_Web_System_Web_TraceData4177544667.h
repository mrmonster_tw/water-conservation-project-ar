﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_DateTime3738529785.h"
#include "System_Web_System_Web_TraceMode4125462420.h"

// System.Collections.Generic.Queue`1<System.Web.InfoTraceData>
struct Queue_1_t908966194;
// System.Collections.Generic.Queue`1<System.Web.ControlTraceData>
struct Queue_1_t1181412943;
// System.Collections.Generic.Queue`1<System.Web.NameValueTraceData>
struct Queue_1_t2414259975;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.String
struct String_t;
// System.Text.Encoding
struct Encoding_t1523322056;
// System.Web.UI.Page
struct Page_t770747966;
// System.Comparison`1<System.Web.InfoTraceData>
struct Comparison_1_t837637879;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.TraceData
struct  TraceData_t4177544667  : public Il2CppObject
{
public:
	// System.Boolean System.Web.TraceData::is_first_time
	bool ___is_first_time_0;
	// System.DateTime System.Web.TraceData::first_time
	DateTime_t3738529785  ___first_time_1;
	// System.Double System.Web.TraceData::prev_time
	double ___prev_time_2;
	// System.Collections.Generic.Queue`1<System.Web.InfoTraceData> System.Web.TraceData::info
	Queue_1_t908966194 * ___info_3;
	// System.Collections.Generic.Queue`1<System.Web.ControlTraceData> System.Web.TraceData::control_data
	Queue_1_t1181412943 * ___control_data_4;
	// System.Collections.Generic.Queue`1<System.Web.NameValueTraceData> System.Web.TraceData::cookie_data
	Queue_1_t2414259975 * ___cookie_data_5;
	// System.Collections.Generic.Queue`1<System.Web.NameValueTraceData> System.Web.TraceData::header_data
	Queue_1_t2414259975 * ___header_data_6;
	// System.Collections.Generic.Queue`1<System.Web.NameValueTraceData> System.Web.TraceData::servervar_data
	Queue_1_t2414259975 * ___servervar_data_7;
	// System.Collections.Hashtable System.Web.TraceData::ctrl_cs
	Hashtable_t1853889766 * ___ctrl_cs_8;
	// System.String System.Web.TraceData::request_path
	String_t* ___request_path_9;
	// System.String System.Web.TraceData::session_id
	String_t* ___session_id_10;
	// System.DateTime System.Web.TraceData::request_time
	DateTime_t3738529785  ___request_time_11;
	// System.Text.Encoding System.Web.TraceData::request_encoding
	Encoding_t1523322056 * ___request_encoding_12;
	// System.Text.Encoding System.Web.TraceData::response_encoding
	Encoding_t1523322056 * ___response_encoding_13;
	// System.String System.Web.TraceData::request_type
	String_t* ___request_type_14;
	// System.Int32 System.Web.TraceData::status_code
	int32_t ___status_code_15;
	// System.Web.UI.Page System.Web.TraceData::page
	Page_t770747966 * ___page_16;
	// System.Web.TraceMode System.Web.TraceData::_traceMode
	int32_t ____traceMode_17;
	// System.Collections.Hashtable System.Web.TraceData::sizes
	Hashtable_t1853889766 * ___sizes_18;
	// System.Collections.Hashtable System.Web.TraceData::ctrl_vs
	Hashtable_t1853889766 * ___ctrl_vs_19;

public:
	inline static int32_t get_offset_of_is_first_time_0() { return static_cast<int32_t>(offsetof(TraceData_t4177544667, ___is_first_time_0)); }
	inline bool get_is_first_time_0() const { return ___is_first_time_0; }
	inline bool* get_address_of_is_first_time_0() { return &___is_first_time_0; }
	inline void set_is_first_time_0(bool value)
	{
		___is_first_time_0 = value;
	}

	inline static int32_t get_offset_of_first_time_1() { return static_cast<int32_t>(offsetof(TraceData_t4177544667, ___first_time_1)); }
	inline DateTime_t3738529785  get_first_time_1() const { return ___first_time_1; }
	inline DateTime_t3738529785 * get_address_of_first_time_1() { return &___first_time_1; }
	inline void set_first_time_1(DateTime_t3738529785  value)
	{
		___first_time_1 = value;
	}

	inline static int32_t get_offset_of_prev_time_2() { return static_cast<int32_t>(offsetof(TraceData_t4177544667, ___prev_time_2)); }
	inline double get_prev_time_2() const { return ___prev_time_2; }
	inline double* get_address_of_prev_time_2() { return &___prev_time_2; }
	inline void set_prev_time_2(double value)
	{
		___prev_time_2 = value;
	}

	inline static int32_t get_offset_of_info_3() { return static_cast<int32_t>(offsetof(TraceData_t4177544667, ___info_3)); }
	inline Queue_1_t908966194 * get_info_3() const { return ___info_3; }
	inline Queue_1_t908966194 ** get_address_of_info_3() { return &___info_3; }
	inline void set_info_3(Queue_1_t908966194 * value)
	{
		___info_3 = value;
		Il2CppCodeGenWriteBarrier(&___info_3, value);
	}

	inline static int32_t get_offset_of_control_data_4() { return static_cast<int32_t>(offsetof(TraceData_t4177544667, ___control_data_4)); }
	inline Queue_1_t1181412943 * get_control_data_4() const { return ___control_data_4; }
	inline Queue_1_t1181412943 ** get_address_of_control_data_4() { return &___control_data_4; }
	inline void set_control_data_4(Queue_1_t1181412943 * value)
	{
		___control_data_4 = value;
		Il2CppCodeGenWriteBarrier(&___control_data_4, value);
	}

	inline static int32_t get_offset_of_cookie_data_5() { return static_cast<int32_t>(offsetof(TraceData_t4177544667, ___cookie_data_5)); }
	inline Queue_1_t2414259975 * get_cookie_data_5() const { return ___cookie_data_5; }
	inline Queue_1_t2414259975 ** get_address_of_cookie_data_5() { return &___cookie_data_5; }
	inline void set_cookie_data_5(Queue_1_t2414259975 * value)
	{
		___cookie_data_5 = value;
		Il2CppCodeGenWriteBarrier(&___cookie_data_5, value);
	}

	inline static int32_t get_offset_of_header_data_6() { return static_cast<int32_t>(offsetof(TraceData_t4177544667, ___header_data_6)); }
	inline Queue_1_t2414259975 * get_header_data_6() const { return ___header_data_6; }
	inline Queue_1_t2414259975 ** get_address_of_header_data_6() { return &___header_data_6; }
	inline void set_header_data_6(Queue_1_t2414259975 * value)
	{
		___header_data_6 = value;
		Il2CppCodeGenWriteBarrier(&___header_data_6, value);
	}

	inline static int32_t get_offset_of_servervar_data_7() { return static_cast<int32_t>(offsetof(TraceData_t4177544667, ___servervar_data_7)); }
	inline Queue_1_t2414259975 * get_servervar_data_7() const { return ___servervar_data_7; }
	inline Queue_1_t2414259975 ** get_address_of_servervar_data_7() { return &___servervar_data_7; }
	inline void set_servervar_data_7(Queue_1_t2414259975 * value)
	{
		___servervar_data_7 = value;
		Il2CppCodeGenWriteBarrier(&___servervar_data_7, value);
	}

	inline static int32_t get_offset_of_ctrl_cs_8() { return static_cast<int32_t>(offsetof(TraceData_t4177544667, ___ctrl_cs_8)); }
	inline Hashtable_t1853889766 * get_ctrl_cs_8() const { return ___ctrl_cs_8; }
	inline Hashtable_t1853889766 ** get_address_of_ctrl_cs_8() { return &___ctrl_cs_8; }
	inline void set_ctrl_cs_8(Hashtable_t1853889766 * value)
	{
		___ctrl_cs_8 = value;
		Il2CppCodeGenWriteBarrier(&___ctrl_cs_8, value);
	}

	inline static int32_t get_offset_of_request_path_9() { return static_cast<int32_t>(offsetof(TraceData_t4177544667, ___request_path_9)); }
	inline String_t* get_request_path_9() const { return ___request_path_9; }
	inline String_t** get_address_of_request_path_9() { return &___request_path_9; }
	inline void set_request_path_9(String_t* value)
	{
		___request_path_9 = value;
		Il2CppCodeGenWriteBarrier(&___request_path_9, value);
	}

	inline static int32_t get_offset_of_session_id_10() { return static_cast<int32_t>(offsetof(TraceData_t4177544667, ___session_id_10)); }
	inline String_t* get_session_id_10() const { return ___session_id_10; }
	inline String_t** get_address_of_session_id_10() { return &___session_id_10; }
	inline void set_session_id_10(String_t* value)
	{
		___session_id_10 = value;
		Il2CppCodeGenWriteBarrier(&___session_id_10, value);
	}

	inline static int32_t get_offset_of_request_time_11() { return static_cast<int32_t>(offsetof(TraceData_t4177544667, ___request_time_11)); }
	inline DateTime_t3738529785  get_request_time_11() const { return ___request_time_11; }
	inline DateTime_t3738529785 * get_address_of_request_time_11() { return &___request_time_11; }
	inline void set_request_time_11(DateTime_t3738529785  value)
	{
		___request_time_11 = value;
	}

	inline static int32_t get_offset_of_request_encoding_12() { return static_cast<int32_t>(offsetof(TraceData_t4177544667, ___request_encoding_12)); }
	inline Encoding_t1523322056 * get_request_encoding_12() const { return ___request_encoding_12; }
	inline Encoding_t1523322056 ** get_address_of_request_encoding_12() { return &___request_encoding_12; }
	inline void set_request_encoding_12(Encoding_t1523322056 * value)
	{
		___request_encoding_12 = value;
		Il2CppCodeGenWriteBarrier(&___request_encoding_12, value);
	}

	inline static int32_t get_offset_of_response_encoding_13() { return static_cast<int32_t>(offsetof(TraceData_t4177544667, ___response_encoding_13)); }
	inline Encoding_t1523322056 * get_response_encoding_13() const { return ___response_encoding_13; }
	inline Encoding_t1523322056 ** get_address_of_response_encoding_13() { return &___response_encoding_13; }
	inline void set_response_encoding_13(Encoding_t1523322056 * value)
	{
		___response_encoding_13 = value;
		Il2CppCodeGenWriteBarrier(&___response_encoding_13, value);
	}

	inline static int32_t get_offset_of_request_type_14() { return static_cast<int32_t>(offsetof(TraceData_t4177544667, ___request_type_14)); }
	inline String_t* get_request_type_14() const { return ___request_type_14; }
	inline String_t** get_address_of_request_type_14() { return &___request_type_14; }
	inline void set_request_type_14(String_t* value)
	{
		___request_type_14 = value;
		Il2CppCodeGenWriteBarrier(&___request_type_14, value);
	}

	inline static int32_t get_offset_of_status_code_15() { return static_cast<int32_t>(offsetof(TraceData_t4177544667, ___status_code_15)); }
	inline int32_t get_status_code_15() const { return ___status_code_15; }
	inline int32_t* get_address_of_status_code_15() { return &___status_code_15; }
	inline void set_status_code_15(int32_t value)
	{
		___status_code_15 = value;
	}

	inline static int32_t get_offset_of_page_16() { return static_cast<int32_t>(offsetof(TraceData_t4177544667, ___page_16)); }
	inline Page_t770747966 * get_page_16() const { return ___page_16; }
	inline Page_t770747966 ** get_address_of_page_16() { return &___page_16; }
	inline void set_page_16(Page_t770747966 * value)
	{
		___page_16 = value;
		Il2CppCodeGenWriteBarrier(&___page_16, value);
	}

	inline static int32_t get_offset_of__traceMode_17() { return static_cast<int32_t>(offsetof(TraceData_t4177544667, ____traceMode_17)); }
	inline int32_t get__traceMode_17() const { return ____traceMode_17; }
	inline int32_t* get_address_of__traceMode_17() { return &____traceMode_17; }
	inline void set__traceMode_17(int32_t value)
	{
		____traceMode_17 = value;
	}

	inline static int32_t get_offset_of_sizes_18() { return static_cast<int32_t>(offsetof(TraceData_t4177544667, ___sizes_18)); }
	inline Hashtable_t1853889766 * get_sizes_18() const { return ___sizes_18; }
	inline Hashtable_t1853889766 ** get_address_of_sizes_18() { return &___sizes_18; }
	inline void set_sizes_18(Hashtable_t1853889766 * value)
	{
		___sizes_18 = value;
		Il2CppCodeGenWriteBarrier(&___sizes_18, value);
	}

	inline static int32_t get_offset_of_ctrl_vs_19() { return static_cast<int32_t>(offsetof(TraceData_t4177544667, ___ctrl_vs_19)); }
	inline Hashtable_t1853889766 * get_ctrl_vs_19() const { return ___ctrl_vs_19; }
	inline Hashtable_t1853889766 ** get_address_of_ctrl_vs_19() { return &___ctrl_vs_19; }
	inline void set_ctrl_vs_19(Hashtable_t1853889766 * value)
	{
		___ctrl_vs_19 = value;
		Il2CppCodeGenWriteBarrier(&___ctrl_vs_19, value);
	}
};

struct TraceData_t4177544667_StaticFields
{
public:
	// System.Comparison`1<System.Web.InfoTraceData> System.Web.TraceData::<>f__am$cache14
	Comparison_1_t837637879 * ___U3CU3Ef__amU24cache14_20;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache14_20() { return static_cast<int32_t>(offsetof(TraceData_t4177544667_StaticFields, ___U3CU3Ef__amU24cache14_20)); }
	inline Comparison_1_t837637879 * get_U3CU3Ef__amU24cache14_20() const { return ___U3CU3Ef__amU24cache14_20; }
	inline Comparison_1_t837637879 ** get_address_of_U3CU3Ef__amU24cache14_20() { return &___U3CU3Ef__amU24cache14_20; }
	inline void set_U3CU3Ef__amU24cache14_20(Comparison_1_t837637879 * value)
	{
		___U3CU3Ef__amU24cache14_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache14_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
