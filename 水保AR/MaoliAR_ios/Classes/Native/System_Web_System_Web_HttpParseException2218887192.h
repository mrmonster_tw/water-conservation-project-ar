﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_HttpException2907797370.h"

// System.String
struct String_t;
// System.Web.ParserErrorCollection
struct ParserErrorCollection_t485049528;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpParseException
struct  HttpParseException_t2218887192  : public HttpException_t2907797370
{
public:
	// System.Int32 System.Web.HttpParseException::line
	int32_t ___line_14;
	// System.String System.Web.HttpParseException::virtualPath
	String_t* ___virtualPath_15;
	// System.Web.ParserErrorCollection System.Web.HttpParseException::errors
	ParserErrorCollection_t485049528 * ___errors_16;

public:
	inline static int32_t get_offset_of_line_14() { return static_cast<int32_t>(offsetof(HttpParseException_t2218887192, ___line_14)); }
	inline int32_t get_line_14() const { return ___line_14; }
	inline int32_t* get_address_of_line_14() { return &___line_14; }
	inline void set_line_14(int32_t value)
	{
		___line_14 = value;
	}

	inline static int32_t get_offset_of_virtualPath_15() { return static_cast<int32_t>(offsetof(HttpParseException_t2218887192, ___virtualPath_15)); }
	inline String_t* get_virtualPath_15() const { return ___virtualPath_15; }
	inline String_t** get_address_of_virtualPath_15() { return &___virtualPath_15; }
	inline void set_virtualPath_15(String_t* value)
	{
		___virtualPath_15 = value;
		Il2CppCodeGenWriteBarrier(&___virtualPath_15, value);
	}

	inline static int32_t get_offset_of_errors_16() { return static_cast<int32_t>(offsetof(HttpParseException_t2218887192, ___errors_16)); }
	inline ParserErrorCollection_t485049528 * get_errors_16() const { return ___errors_16; }
	inline ParserErrorCollection_t485049528 ** get_address_of_errors_16() { return &___errors_16; }
	inline void set_errors_16(ParserErrorCollection_t485049528 * value)
	{
		___errors_16 = value;
		Il2CppCodeGenWriteBarrier(&___errors_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
