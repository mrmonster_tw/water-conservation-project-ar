﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"

// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.ControlValuePropertyAttribute
struct  ControlValuePropertyAttribute_t2054366362  : public Attribute_t861562559
{
public:
	// System.String System.Web.UI.ControlValuePropertyAttribute::propertyName
	String_t* ___propertyName_0;
	// System.Object System.Web.UI.ControlValuePropertyAttribute::propertyValue
	Il2CppObject * ___propertyValue_1;
	// System.Type System.Web.UI.ControlValuePropertyAttribute::propertyType
	Type_t * ___propertyType_2;

public:
	inline static int32_t get_offset_of_propertyName_0() { return static_cast<int32_t>(offsetof(ControlValuePropertyAttribute_t2054366362, ___propertyName_0)); }
	inline String_t* get_propertyName_0() const { return ___propertyName_0; }
	inline String_t** get_address_of_propertyName_0() { return &___propertyName_0; }
	inline void set_propertyName_0(String_t* value)
	{
		___propertyName_0 = value;
		Il2CppCodeGenWriteBarrier(&___propertyName_0, value);
	}

	inline static int32_t get_offset_of_propertyValue_1() { return static_cast<int32_t>(offsetof(ControlValuePropertyAttribute_t2054366362, ___propertyValue_1)); }
	inline Il2CppObject * get_propertyValue_1() const { return ___propertyValue_1; }
	inline Il2CppObject ** get_address_of_propertyValue_1() { return &___propertyValue_1; }
	inline void set_propertyValue_1(Il2CppObject * value)
	{
		___propertyValue_1 = value;
		Il2CppCodeGenWriteBarrier(&___propertyValue_1, value);
	}

	inline static int32_t get_offset_of_propertyType_2() { return static_cast<int32_t>(offsetof(ControlValuePropertyAttribute_t2054366362, ___propertyType_2)); }
	inline Type_t * get_propertyType_2() const { return ___propertyType_2; }
	inline Type_t ** get_address_of_propertyType_2() { return &___propertyType_2; }
	inline void set_propertyType_2(Type_t * value)
	{
		___propertyType_2 = value;
		Il2CppCodeGenWriteBarrier(&___propertyType_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
