﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1193879517.h"
#include "AssemblyU2DCSharp_SetAnimArray_SetAnimInfo2127528194.h"

// UnityEngine.Animator
struct Animator_t434523843;
// SetAnimArray
struct SetAnimArray_t1762733004;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SetAnimArray/<late>c__Iterator0
struct  U3ClateU3Ec__Iterator0_t3205559430  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1/Enumerator<SetAnimArray/SetAnimInfo> SetAnimArray/<late>c__Iterator0::$locvar0
	Enumerator_t1193879517  ___U24locvar0_0;
	// SetAnimArray/SetAnimInfo SetAnimArray/<late>c__Iterator0::<si>__1
	SetAnimInfo_t2127528194  ___U3CsiU3E__1_1;
	// UnityEngine.Animator SetAnimArray/<late>c__Iterator0::animator
	Animator_t434523843 * ___animator_2;
	// SetAnimArray SetAnimArray/<late>c__Iterator0::$this
	SetAnimArray_t1762733004 * ___U24this_3;
	// System.Object SetAnimArray/<late>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean SetAnimArray/<late>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 SetAnimArray/<late>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3ClateU3Ec__Iterator0_t3205559430, ___U24locvar0_0)); }
	inline Enumerator_t1193879517  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t1193879517 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t1193879517  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U3CsiU3E__1_1() { return static_cast<int32_t>(offsetof(U3ClateU3Ec__Iterator0_t3205559430, ___U3CsiU3E__1_1)); }
	inline SetAnimInfo_t2127528194  get_U3CsiU3E__1_1() const { return ___U3CsiU3E__1_1; }
	inline SetAnimInfo_t2127528194 * get_address_of_U3CsiU3E__1_1() { return &___U3CsiU3E__1_1; }
	inline void set_U3CsiU3E__1_1(SetAnimInfo_t2127528194  value)
	{
		___U3CsiU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_animator_2() { return static_cast<int32_t>(offsetof(U3ClateU3Ec__Iterator0_t3205559430, ___animator_2)); }
	inline Animator_t434523843 * get_animator_2() const { return ___animator_2; }
	inline Animator_t434523843 ** get_address_of_animator_2() { return &___animator_2; }
	inline void set_animator_2(Animator_t434523843 * value)
	{
		___animator_2 = value;
		Il2CppCodeGenWriteBarrier(&___animator_2, value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3ClateU3Ec__Iterator0_t3205559430, ___U24this_3)); }
	inline SetAnimArray_t1762733004 * get_U24this_3() const { return ___U24this_3; }
	inline SetAnimArray_t1762733004 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(SetAnimArray_t1762733004 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3ClateU3Ec__Iterator0_t3205559430, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3ClateU3Ec__Iterator0_t3205559430, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3ClateU3Ec__Iterator0_t3205559430, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
