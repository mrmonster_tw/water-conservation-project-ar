﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Reflection.Assembly
struct Assembly_t4102432799;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Byte[]
struct ByteU5BU5D_t4116647657;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Handlers.AssemblyResourceLoader
struct  AssemblyResourceLoader_t761489670  : public Il2CppObject
{
public:

public:
};

struct AssemblyResourceLoader_t761489670_StaticFields
{
public:
	// System.Reflection.Assembly System.Web.Handlers.AssemblyResourceLoader::currAsm
	Assembly_t4102432799 * ___currAsm_0;
	// System.Collections.Hashtable System.Web.Handlers.AssemblyResourceLoader::_embeddedResources
	Hashtable_t1853889766 * ____embeddedResources_1;
	// System.Byte[] System.Web.Handlers.AssemblyResourceLoader::init_vector
	ByteU5BU5D_t4116647657* ___init_vector_2;

public:
	inline static int32_t get_offset_of_currAsm_0() { return static_cast<int32_t>(offsetof(AssemblyResourceLoader_t761489670_StaticFields, ___currAsm_0)); }
	inline Assembly_t4102432799 * get_currAsm_0() const { return ___currAsm_0; }
	inline Assembly_t4102432799 ** get_address_of_currAsm_0() { return &___currAsm_0; }
	inline void set_currAsm_0(Assembly_t4102432799 * value)
	{
		___currAsm_0 = value;
		Il2CppCodeGenWriteBarrier(&___currAsm_0, value);
	}

	inline static int32_t get_offset_of__embeddedResources_1() { return static_cast<int32_t>(offsetof(AssemblyResourceLoader_t761489670_StaticFields, ____embeddedResources_1)); }
	inline Hashtable_t1853889766 * get__embeddedResources_1() const { return ____embeddedResources_1; }
	inline Hashtable_t1853889766 ** get_address_of__embeddedResources_1() { return &____embeddedResources_1; }
	inline void set__embeddedResources_1(Hashtable_t1853889766 * value)
	{
		____embeddedResources_1 = value;
		Il2CppCodeGenWriteBarrier(&____embeddedResources_1, value);
	}

	inline static int32_t get_offset_of_init_vector_2() { return static_cast<int32_t>(offsetof(AssemblyResourceLoader_t761489670_StaticFields, ___init_vector_2)); }
	inline ByteU5BU5D_t4116647657* get_init_vector_2() const { return ___init_vector_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_init_vector_2() { return &___init_vector_2; }
	inline void set_init_vector_2(ByteU5BU5D_t4116647657* value)
	{
		___init_vector_2 = value;
		Il2CppCodeGenWriteBarrier(&___init_vector_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
