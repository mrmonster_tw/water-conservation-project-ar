﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeStatement371410868.h"

// System.CodeDom.CodeExpression
struct CodeExpression_t2166265795;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeMethodReturnStatement
struct  CodeMethodReturnStatement_t2523153104  : public CodeStatement_t371410868
{
public:
	// System.CodeDom.CodeExpression System.CodeDom.CodeMethodReturnStatement::expression
	CodeExpression_t2166265795 * ___expression_4;

public:
	inline static int32_t get_offset_of_expression_4() { return static_cast<int32_t>(offsetof(CodeMethodReturnStatement_t2523153104, ___expression_4)); }
	inline CodeExpression_t2166265795 * get_expression_4() const { return ___expression_4; }
	inline CodeExpression_t2166265795 ** get_address_of_expression_4() { return &___expression_4; }
	inline void set_expression_4(CodeExpression_t2166265795 * value)
	{
		___expression_4 = value;
		Il2CppCodeGenWriteBarrier(&___expression_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
