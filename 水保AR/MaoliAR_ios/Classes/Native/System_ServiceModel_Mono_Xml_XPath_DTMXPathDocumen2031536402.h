﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XmlWriter127905479.h"
#include "System_Xml_System_Xml_WriteState3983380671.h"

// System.Xml.XmlNameTable
struct XmlNameTable_t71772148;
// Mono.Xml.XPath.DTMXPathLinkedNode2[]
struct DTMXPathLinkedNode2U5BU5D_t3499139399;
// Mono.Xml.XPath.DTMXPathAttributeNode2[]
struct DTMXPathAttributeNode2U5BU5D_t1081050938;
// Mono.Xml.XPath.DTMXPathNamespaceNode2[]
struct DTMXPathNamespaceNode2U5BU5D_t3587247258;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Int32[]
struct Int32U5BU5D_t385246372;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.XPath.DTMXPathDocumentWriter2
struct  DTMXPathDocumentWriter2_t2031536403  : public XmlWriter_t127905479
{
public:
	// System.Xml.XmlNameTable Mono.Xml.XPath.DTMXPathDocumentWriter2::nameTable
	XmlNameTable_t71772148 * ___nameTable_1;
	// System.Int32 Mono.Xml.XPath.DTMXPathDocumentWriter2::nodeCapacity
	int32_t ___nodeCapacity_2;
	// System.Int32 Mono.Xml.XPath.DTMXPathDocumentWriter2::attributeCapacity
	int32_t ___attributeCapacity_3;
	// System.Int32 Mono.Xml.XPath.DTMXPathDocumentWriter2::nsCapacity
	int32_t ___nsCapacity_4;
	// Mono.Xml.XPath.DTMXPathLinkedNode2[] Mono.Xml.XPath.DTMXPathDocumentWriter2::nodes
	DTMXPathLinkedNode2U5BU5D_t3499139399* ___nodes_5;
	// Mono.Xml.XPath.DTMXPathAttributeNode2[] Mono.Xml.XPath.DTMXPathDocumentWriter2::attributes
	DTMXPathAttributeNode2U5BU5D_t1081050938* ___attributes_6;
	// Mono.Xml.XPath.DTMXPathNamespaceNode2[] Mono.Xml.XPath.DTMXPathDocumentWriter2::namespaces
	DTMXPathNamespaceNode2U5BU5D_t3587247258* ___namespaces_7;
	// System.String[] Mono.Xml.XPath.DTMXPathDocumentWriter2::atomicStringPool
	StringU5BU5D_t1281789340* ___atomicStringPool_8;
	// System.Int32 Mono.Xml.XPath.DTMXPathDocumentWriter2::atomicIndex
	int32_t ___atomicIndex_9;
	// System.String[] Mono.Xml.XPath.DTMXPathDocumentWriter2::nonAtomicStringPool
	StringU5BU5D_t1281789340* ___nonAtomicStringPool_10;
	// System.Int32 Mono.Xml.XPath.DTMXPathDocumentWriter2::nonAtomicIndex
	int32_t ___nonAtomicIndex_11;
	// System.Collections.Hashtable Mono.Xml.XPath.DTMXPathDocumentWriter2::idTable
	Hashtable_t1853889766 * ___idTable_12;
	// System.Int32 Mono.Xml.XPath.DTMXPathDocumentWriter2::nodeIndex
	int32_t ___nodeIndex_13;
	// System.Int32 Mono.Xml.XPath.DTMXPathDocumentWriter2::attributeIndex
	int32_t ___attributeIndex_14;
	// System.Int32 Mono.Xml.XPath.DTMXPathDocumentWriter2::nsIndex
	int32_t ___nsIndex_15;
	// System.Int32[] Mono.Xml.XPath.DTMXPathDocumentWriter2::parentStack
	Int32U5BU5D_t385246372* ___parentStack_16;
	// System.Int32 Mono.Xml.XPath.DTMXPathDocumentWriter2::parentStackIndex
	int32_t ___parentStackIndex_17;
	// System.Boolean Mono.Xml.XPath.DTMXPathDocumentWriter2::hasAttributes
	bool ___hasAttributes_18;
	// System.Boolean Mono.Xml.XPath.DTMXPathDocumentWriter2::hasLocalNs
	bool ___hasLocalNs_19;
	// System.Int32 Mono.Xml.XPath.DTMXPathDocumentWriter2::attrIndexAtStart
	int32_t ___attrIndexAtStart_20;
	// System.Int32 Mono.Xml.XPath.DTMXPathDocumentWriter2::nsIndexAtStart
	int32_t ___nsIndexAtStart_21;
	// System.Int32 Mono.Xml.XPath.DTMXPathDocumentWriter2::lastNsInScope
	int32_t ___lastNsInScope_22;
	// System.Int32 Mono.Xml.XPath.DTMXPathDocumentWriter2::prevSibling
	int32_t ___prevSibling_23;
	// System.Xml.WriteState Mono.Xml.XPath.DTMXPathDocumentWriter2::state
	int32_t ___state_24;
	// System.Boolean Mono.Xml.XPath.DTMXPathDocumentWriter2::openNamespace
	bool ___openNamespace_25;
	// System.Boolean Mono.Xml.XPath.DTMXPathDocumentWriter2::isClosed
	bool ___isClosed_26;

public:
	inline static int32_t get_offset_of_nameTable_1() { return static_cast<int32_t>(offsetof(DTMXPathDocumentWriter2_t2031536403, ___nameTable_1)); }
	inline XmlNameTable_t71772148 * get_nameTable_1() const { return ___nameTable_1; }
	inline XmlNameTable_t71772148 ** get_address_of_nameTable_1() { return &___nameTable_1; }
	inline void set_nameTable_1(XmlNameTable_t71772148 * value)
	{
		___nameTable_1 = value;
		Il2CppCodeGenWriteBarrier(&___nameTable_1, value);
	}

	inline static int32_t get_offset_of_nodeCapacity_2() { return static_cast<int32_t>(offsetof(DTMXPathDocumentWriter2_t2031536403, ___nodeCapacity_2)); }
	inline int32_t get_nodeCapacity_2() const { return ___nodeCapacity_2; }
	inline int32_t* get_address_of_nodeCapacity_2() { return &___nodeCapacity_2; }
	inline void set_nodeCapacity_2(int32_t value)
	{
		___nodeCapacity_2 = value;
	}

	inline static int32_t get_offset_of_attributeCapacity_3() { return static_cast<int32_t>(offsetof(DTMXPathDocumentWriter2_t2031536403, ___attributeCapacity_3)); }
	inline int32_t get_attributeCapacity_3() const { return ___attributeCapacity_3; }
	inline int32_t* get_address_of_attributeCapacity_3() { return &___attributeCapacity_3; }
	inline void set_attributeCapacity_3(int32_t value)
	{
		___attributeCapacity_3 = value;
	}

	inline static int32_t get_offset_of_nsCapacity_4() { return static_cast<int32_t>(offsetof(DTMXPathDocumentWriter2_t2031536403, ___nsCapacity_4)); }
	inline int32_t get_nsCapacity_4() const { return ___nsCapacity_4; }
	inline int32_t* get_address_of_nsCapacity_4() { return &___nsCapacity_4; }
	inline void set_nsCapacity_4(int32_t value)
	{
		___nsCapacity_4 = value;
	}

	inline static int32_t get_offset_of_nodes_5() { return static_cast<int32_t>(offsetof(DTMXPathDocumentWriter2_t2031536403, ___nodes_5)); }
	inline DTMXPathLinkedNode2U5BU5D_t3499139399* get_nodes_5() const { return ___nodes_5; }
	inline DTMXPathLinkedNode2U5BU5D_t3499139399** get_address_of_nodes_5() { return &___nodes_5; }
	inline void set_nodes_5(DTMXPathLinkedNode2U5BU5D_t3499139399* value)
	{
		___nodes_5 = value;
		Il2CppCodeGenWriteBarrier(&___nodes_5, value);
	}

	inline static int32_t get_offset_of_attributes_6() { return static_cast<int32_t>(offsetof(DTMXPathDocumentWriter2_t2031536403, ___attributes_6)); }
	inline DTMXPathAttributeNode2U5BU5D_t1081050938* get_attributes_6() const { return ___attributes_6; }
	inline DTMXPathAttributeNode2U5BU5D_t1081050938** get_address_of_attributes_6() { return &___attributes_6; }
	inline void set_attributes_6(DTMXPathAttributeNode2U5BU5D_t1081050938* value)
	{
		___attributes_6 = value;
		Il2CppCodeGenWriteBarrier(&___attributes_6, value);
	}

	inline static int32_t get_offset_of_namespaces_7() { return static_cast<int32_t>(offsetof(DTMXPathDocumentWriter2_t2031536403, ___namespaces_7)); }
	inline DTMXPathNamespaceNode2U5BU5D_t3587247258* get_namespaces_7() const { return ___namespaces_7; }
	inline DTMXPathNamespaceNode2U5BU5D_t3587247258** get_address_of_namespaces_7() { return &___namespaces_7; }
	inline void set_namespaces_7(DTMXPathNamespaceNode2U5BU5D_t3587247258* value)
	{
		___namespaces_7 = value;
		Il2CppCodeGenWriteBarrier(&___namespaces_7, value);
	}

	inline static int32_t get_offset_of_atomicStringPool_8() { return static_cast<int32_t>(offsetof(DTMXPathDocumentWriter2_t2031536403, ___atomicStringPool_8)); }
	inline StringU5BU5D_t1281789340* get_atomicStringPool_8() const { return ___atomicStringPool_8; }
	inline StringU5BU5D_t1281789340** get_address_of_atomicStringPool_8() { return &___atomicStringPool_8; }
	inline void set_atomicStringPool_8(StringU5BU5D_t1281789340* value)
	{
		___atomicStringPool_8 = value;
		Il2CppCodeGenWriteBarrier(&___atomicStringPool_8, value);
	}

	inline static int32_t get_offset_of_atomicIndex_9() { return static_cast<int32_t>(offsetof(DTMXPathDocumentWriter2_t2031536403, ___atomicIndex_9)); }
	inline int32_t get_atomicIndex_9() const { return ___atomicIndex_9; }
	inline int32_t* get_address_of_atomicIndex_9() { return &___atomicIndex_9; }
	inline void set_atomicIndex_9(int32_t value)
	{
		___atomicIndex_9 = value;
	}

	inline static int32_t get_offset_of_nonAtomicStringPool_10() { return static_cast<int32_t>(offsetof(DTMXPathDocumentWriter2_t2031536403, ___nonAtomicStringPool_10)); }
	inline StringU5BU5D_t1281789340* get_nonAtomicStringPool_10() const { return ___nonAtomicStringPool_10; }
	inline StringU5BU5D_t1281789340** get_address_of_nonAtomicStringPool_10() { return &___nonAtomicStringPool_10; }
	inline void set_nonAtomicStringPool_10(StringU5BU5D_t1281789340* value)
	{
		___nonAtomicStringPool_10 = value;
		Il2CppCodeGenWriteBarrier(&___nonAtomicStringPool_10, value);
	}

	inline static int32_t get_offset_of_nonAtomicIndex_11() { return static_cast<int32_t>(offsetof(DTMXPathDocumentWriter2_t2031536403, ___nonAtomicIndex_11)); }
	inline int32_t get_nonAtomicIndex_11() const { return ___nonAtomicIndex_11; }
	inline int32_t* get_address_of_nonAtomicIndex_11() { return &___nonAtomicIndex_11; }
	inline void set_nonAtomicIndex_11(int32_t value)
	{
		___nonAtomicIndex_11 = value;
	}

	inline static int32_t get_offset_of_idTable_12() { return static_cast<int32_t>(offsetof(DTMXPathDocumentWriter2_t2031536403, ___idTable_12)); }
	inline Hashtable_t1853889766 * get_idTable_12() const { return ___idTable_12; }
	inline Hashtable_t1853889766 ** get_address_of_idTable_12() { return &___idTable_12; }
	inline void set_idTable_12(Hashtable_t1853889766 * value)
	{
		___idTable_12 = value;
		Il2CppCodeGenWriteBarrier(&___idTable_12, value);
	}

	inline static int32_t get_offset_of_nodeIndex_13() { return static_cast<int32_t>(offsetof(DTMXPathDocumentWriter2_t2031536403, ___nodeIndex_13)); }
	inline int32_t get_nodeIndex_13() const { return ___nodeIndex_13; }
	inline int32_t* get_address_of_nodeIndex_13() { return &___nodeIndex_13; }
	inline void set_nodeIndex_13(int32_t value)
	{
		___nodeIndex_13 = value;
	}

	inline static int32_t get_offset_of_attributeIndex_14() { return static_cast<int32_t>(offsetof(DTMXPathDocumentWriter2_t2031536403, ___attributeIndex_14)); }
	inline int32_t get_attributeIndex_14() const { return ___attributeIndex_14; }
	inline int32_t* get_address_of_attributeIndex_14() { return &___attributeIndex_14; }
	inline void set_attributeIndex_14(int32_t value)
	{
		___attributeIndex_14 = value;
	}

	inline static int32_t get_offset_of_nsIndex_15() { return static_cast<int32_t>(offsetof(DTMXPathDocumentWriter2_t2031536403, ___nsIndex_15)); }
	inline int32_t get_nsIndex_15() const { return ___nsIndex_15; }
	inline int32_t* get_address_of_nsIndex_15() { return &___nsIndex_15; }
	inline void set_nsIndex_15(int32_t value)
	{
		___nsIndex_15 = value;
	}

	inline static int32_t get_offset_of_parentStack_16() { return static_cast<int32_t>(offsetof(DTMXPathDocumentWriter2_t2031536403, ___parentStack_16)); }
	inline Int32U5BU5D_t385246372* get_parentStack_16() const { return ___parentStack_16; }
	inline Int32U5BU5D_t385246372** get_address_of_parentStack_16() { return &___parentStack_16; }
	inline void set_parentStack_16(Int32U5BU5D_t385246372* value)
	{
		___parentStack_16 = value;
		Il2CppCodeGenWriteBarrier(&___parentStack_16, value);
	}

	inline static int32_t get_offset_of_parentStackIndex_17() { return static_cast<int32_t>(offsetof(DTMXPathDocumentWriter2_t2031536403, ___parentStackIndex_17)); }
	inline int32_t get_parentStackIndex_17() const { return ___parentStackIndex_17; }
	inline int32_t* get_address_of_parentStackIndex_17() { return &___parentStackIndex_17; }
	inline void set_parentStackIndex_17(int32_t value)
	{
		___parentStackIndex_17 = value;
	}

	inline static int32_t get_offset_of_hasAttributes_18() { return static_cast<int32_t>(offsetof(DTMXPathDocumentWriter2_t2031536403, ___hasAttributes_18)); }
	inline bool get_hasAttributes_18() const { return ___hasAttributes_18; }
	inline bool* get_address_of_hasAttributes_18() { return &___hasAttributes_18; }
	inline void set_hasAttributes_18(bool value)
	{
		___hasAttributes_18 = value;
	}

	inline static int32_t get_offset_of_hasLocalNs_19() { return static_cast<int32_t>(offsetof(DTMXPathDocumentWriter2_t2031536403, ___hasLocalNs_19)); }
	inline bool get_hasLocalNs_19() const { return ___hasLocalNs_19; }
	inline bool* get_address_of_hasLocalNs_19() { return &___hasLocalNs_19; }
	inline void set_hasLocalNs_19(bool value)
	{
		___hasLocalNs_19 = value;
	}

	inline static int32_t get_offset_of_attrIndexAtStart_20() { return static_cast<int32_t>(offsetof(DTMXPathDocumentWriter2_t2031536403, ___attrIndexAtStart_20)); }
	inline int32_t get_attrIndexAtStart_20() const { return ___attrIndexAtStart_20; }
	inline int32_t* get_address_of_attrIndexAtStart_20() { return &___attrIndexAtStart_20; }
	inline void set_attrIndexAtStart_20(int32_t value)
	{
		___attrIndexAtStart_20 = value;
	}

	inline static int32_t get_offset_of_nsIndexAtStart_21() { return static_cast<int32_t>(offsetof(DTMXPathDocumentWriter2_t2031536403, ___nsIndexAtStart_21)); }
	inline int32_t get_nsIndexAtStart_21() const { return ___nsIndexAtStart_21; }
	inline int32_t* get_address_of_nsIndexAtStart_21() { return &___nsIndexAtStart_21; }
	inline void set_nsIndexAtStart_21(int32_t value)
	{
		___nsIndexAtStart_21 = value;
	}

	inline static int32_t get_offset_of_lastNsInScope_22() { return static_cast<int32_t>(offsetof(DTMXPathDocumentWriter2_t2031536403, ___lastNsInScope_22)); }
	inline int32_t get_lastNsInScope_22() const { return ___lastNsInScope_22; }
	inline int32_t* get_address_of_lastNsInScope_22() { return &___lastNsInScope_22; }
	inline void set_lastNsInScope_22(int32_t value)
	{
		___lastNsInScope_22 = value;
	}

	inline static int32_t get_offset_of_prevSibling_23() { return static_cast<int32_t>(offsetof(DTMXPathDocumentWriter2_t2031536403, ___prevSibling_23)); }
	inline int32_t get_prevSibling_23() const { return ___prevSibling_23; }
	inline int32_t* get_address_of_prevSibling_23() { return &___prevSibling_23; }
	inline void set_prevSibling_23(int32_t value)
	{
		___prevSibling_23 = value;
	}

	inline static int32_t get_offset_of_state_24() { return static_cast<int32_t>(offsetof(DTMXPathDocumentWriter2_t2031536403, ___state_24)); }
	inline int32_t get_state_24() const { return ___state_24; }
	inline int32_t* get_address_of_state_24() { return &___state_24; }
	inline void set_state_24(int32_t value)
	{
		___state_24 = value;
	}

	inline static int32_t get_offset_of_openNamespace_25() { return static_cast<int32_t>(offsetof(DTMXPathDocumentWriter2_t2031536403, ___openNamespace_25)); }
	inline bool get_openNamespace_25() const { return ___openNamespace_25; }
	inline bool* get_address_of_openNamespace_25() { return &___openNamespace_25; }
	inline void set_openNamespace_25(bool value)
	{
		___openNamespace_25 = value;
	}

	inline static int32_t get_offset_of_isClosed_26() { return static_cast<int32_t>(offsetof(DTMXPathDocumentWriter2_t2031536403, ___isClosed_26)); }
	inline bool get_isClosed_26() const { return ___isClosed_26; }
	inline bool* get_address_of_isClosed_26() { return &___isClosed_26; }
	inline void set_isClosed_26(bool value)
	{
		___isClosed_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
