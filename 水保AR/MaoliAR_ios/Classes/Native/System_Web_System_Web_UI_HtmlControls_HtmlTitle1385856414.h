﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_HtmlControls_HtmlControl1899600556.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.HtmlControls.HtmlTitle
struct  HtmlTitle_t1385856414  : public HtmlControl_t1899600556
{
public:
	// System.String System.Web.UI.HtmlControls.HtmlTitle::text
	String_t* ___text_30;

public:
	inline static int32_t get_offset_of_text_30() { return static_cast<int32_t>(offsetof(HtmlTitle_t1385856414, ___text_30)); }
	inline String_t* get_text_30() const { return ___text_30; }
	inline String_t** get_address_of_text_30() { return &___text_30; }
	inline void set_text_30(String_t* value)
	{
		___text_30 = value;
		Il2CppCodeGenWriteBarrier(&___text_30, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
