﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Selectors202133775.h"

// System.ServiceModel.Description.ClientCredentials
struct ClientCredentials_t1418047401;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.ClientCredentialsSecurityTokenManager
struct  ClientCredentialsSecurityTokenManager_t1905807029  : public SecurityTokenManager_t202133775
{
public:
	// System.ServiceModel.Description.ClientCredentials System.ServiceModel.ClientCredentialsSecurityTokenManager::credentials
	ClientCredentials_t1418047401 * ___credentials_0;

public:
	inline static int32_t get_offset_of_credentials_0() { return static_cast<int32_t>(offsetof(ClientCredentialsSecurityTokenManager_t1905807029, ___credentials_0)); }
	inline ClientCredentials_t1418047401 * get_credentials_0() const { return ___credentials_0; }
	inline ClientCredentials_t1418047401 ** get_address_of_credentials_0() { return &___credentials_0; }
	inline void set_credentials_0(ClientCredentials_t1418047401 * value)
	{
		___credentials_0 = value;
		Il2CppCodeGenWriteBarrier(&___credentials_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
