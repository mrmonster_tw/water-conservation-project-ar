﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_Mono_Security_Protocol_Tls_Con3971234707.h"

// Mono.Security.Protocol.Tls.SslClientStream
struct SslClientStream_t3914624662;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.ClientContext
struct  ClientContext_t2797401966  : public Context_t3971234708
{
public:
	// Mono.Security.Protocol.Tls.SslClientStream Mono.Security.Protocol.Tls.ClientContext::sslStream
	SslClientStream_t3914624662 * ___sslStream_29;
	// System.Int16 Mono.Security.Protocol.Tls.ClientContext::clientHelloProtocol
	int16_t ___clientHelloProtocol_30;

public:
	inline static int32_t get_offset_of_sslStream_29() { return static_cast<int32_t>(offsetof(ClientContext_t2797401966, ___sslStream_29)); }
	inline SslClientStream_t3914624662 * get_sslStream_29() const { return ___sslStream_29; }
	inline SslClientStream_t3914624662 ** get_address_of_sslStream_29() { return &___sslStream_29; }
	inline void set_sslStream_29(SslClientStream_t3914624662 * value)
	{
		___sslStream_29 = value;
		Il2CppCodeGenWriteBarrier(&___sslStream_29, value);
	}

	inline static int32_t get_offset_of_clientHelloProtocol_30() { return static_cast<int32_t>(offsetof(ClientContext_t2797401966, ___clientHelloProtocol_30)); }
	inline int16_t get_clientHelloProtocol_30() const { return ___clientHelloProtocol_30; }
	inline int16_t* get_address_of_clientHelloProtocol_30() { return &___clientHelloProtocol_30; }
	inline void set_clientHelloProtocol_30(int16_t value)
	{
		___clientHelloProtocol_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
