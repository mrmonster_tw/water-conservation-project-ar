﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_Guid3193532887.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.PeerResolvers.ResolveInfoDC
struct  ResolveInfoDC_t3820618709  : public Il2CppObject
{
public:
	// System.Guid System.ServiceModel.PeerResolvers.ResolveInfoDC::client_id
	Guid_t  ___client_id_0;
	// System.Int32 System.ServiceModel.PeerResolvers.ResolveInfoDC::max_addresses
	int32_t ___max_addresses_1;
	// System.String System.ServiceModel.PeerResolvers.ResolveInfoDC::mesh_id
	String_t* ___mesh_id_2;

public:
	inline static int32_t get_offset_of_client_id_0() { return static_cast<int32_t>(offsetof(ResolveInfoDC_t3820618709, ___client_id_0)); }
	inline Guid_t  get_client_id_0() const { return ___client_id_0; }
	inline Guid_t * get_address_of_client_id_0() { return &___client_id_0; }
	inline void set_client_id_0(Guid_t  value)
	{
		___client_id_0 = value;
	}

	inline static int32_t get_offset_of_max_addresses_1() { return static_cast<int32_t>(offsetof(ResolveInfoDC_t3820618709, ___max_addresses_1)); }
	inline int32_t get_max_addresses_1() const { return ___max_addresses_1; }
	inline int32_t* get_address_of_max_addresses_1() { return &___max_addresses_1; }
	inline void set_max_addresses_1(int32_t value)
	{
		___max_addresses_1 = value;
	}

	inline static int32_t get_offset_of_mesh_id_2() { return static_cast<int32_t>(offsetof(ResolveInfoDC_t3820618709, ___mesh_id_2)); }
	inline String_t* get_mesh_id_2() const { return ___mesh_id_2; }
	inline String_t** get_address_of_mesh_id_2() { return &___mesh_id_2; }
	inline void set_mesh_id_2(String_t* value)
	{
		___mesh_id_2 = value;
		Il2CppCodeGenWriteBarrier(&___mesh_id_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
