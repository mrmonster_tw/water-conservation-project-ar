﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FPSCounter
struct  FPSCounter_t4258725281  : public MonoBehaviour_t3962482529
{
public:
	// System.Single FPSCounter::frames
	float ___frames_3;
	// System.Single FPSCounter::time
	float ___time_4;
	// System.String FPSCounter::fps
	String_t* ___fps_5;

public:
	inline static int32_t get_offset_of_frames_3() { return static_cast<int32_t>(offsetof(FPSCounter_t4258725281, ___frames_3)); }
	inline float get_frames_3() const { return ___frames_3; }
	inline float* get_address_of_frames_3() { return &___frames_3; }
	inline void set_frames_3(float value)
	{
		___frames_3 = value;
	}

	inline static int32_t get_offset_of_time_4() { return static_cast<int32_t>(offsetof(FPSCounter_t4258725281, ___time_4)); }
	inline float get_time_4() const { return ___time_4; }
	inline float* get_address_of_time_4() { return &___time_4; }
	inline void set_time_4(float value)
	{
		___time_4 = value;
	}

	inline static int32_t get_offset_of_fps_5() { return static_cast<int32_t>(offsetof(FPSCounter_t4258725281, ___fps_5)); }
	inline String_t* get_fps_5() const { return ___fps_5; }
	inline String_t** get_address_of_fps_5() { return &___fps_5; }
	inline void set_fps_5(String_t* value)
	{
		___fps_5 = value;
		Il2CppCodeGenWriteBarrier(&___fps_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
