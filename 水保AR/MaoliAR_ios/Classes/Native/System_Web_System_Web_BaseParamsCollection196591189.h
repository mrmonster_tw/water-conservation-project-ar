﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_WebROCollection4065660147.h"

// System.Web.HttpRequest
struct HttpRequest_t809700260;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.BaseParamsCollection
struct  BaseParamsCollection_t196591189  : public WebROCollection_t4065660147
{
public:
	// System.Web.HttpRequest System.Web.BaseParamsCollection::_request
	HttpRequest_t809700260 * ____request_14;
	// System.Boolean System.Web.BaseParamsCollection::_loaded
	bool ____loaded_15;

public:
	inline static int32_t get_offset_of__request_14() { return static_cast<int32_t>(offsetof(BaseParamsCollection_t196591189, ____request_14)); }
	inline HttpRequest_t809700260 * get__request_14() const { return ____request_14; }
	inline HttpRequest_t809700260 ** get_address_of__request_14() { return &____request_14; }
	inline void set__request_14(HttpRequest_t809700260 * value)
	{
		____request_14 = value;
		Il2CppCodeGenWriteBarrier(&____request_14, value);
	}

	inline static int32_t get_offset_of__loaded_15() { return static_cast<int32_t>(offsetof(BaseParamsCollection_t196591189, ____loaded_15)); }
	inline bool get__loaded_15() const { return ____loaded_15; }
	inline bool* get_address_of__loaded_15() { return &____loaded_15; }
	inline void set__loaded_15(bool value)
	{
		____loaded_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
