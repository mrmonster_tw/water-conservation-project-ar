﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.Text.RegularExpressions.Regex
struct Regex_t3657309853;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.nBrowser.Identification
struct  Identification_t455147138  : public Il2CppObject
{
public:
	// System.Boolean System.Web.Configuration.nBrowser.Identification::MatchType
	bool ___MatchType_0;
	// System.String System.Web.Configuration.nBrowser.Identification::MatchName
	String_t* ___MatchName_1;
	// System.String System.Web.Configuration.nBrowser.Identification::MatchGroup
	String_t* ___MatchGroup_2;
	// System.String System.Web.Configuration.nBrowser.Identification::MatchPattern
	String_t* ___MatchPattern_3;
	// System.Text.RegularExpressions.Regex System.Web.Configuration.nBrowser.Identification::RegexPattern
	Regex_t3657309853 * ___RegexPattern_4;

public:
	inline static int32_t get_offset_of_MatchType_0() { return static_cast<int32_t>(offsetof(Identification_t455147138, ___MatchType_0)); }
	inline bool get_MatchType_0() const { return ___MatchType_0; }
	inline bool* get_address_of_MatchType_0() { return &___MatchType_0; }
	inline void set_MatchType_0(bool value)
	{
		___MatchType_0 = value;
	}

	inline static int32_t get_offset_of_MatchName_1() { return static_cast<int32_t>(offsetof(Identification_t455147138, ___MatchName_1)); }
	inline String_t* get_MatchName_1() const { return ___MatchName_1; }
	inline String_t** get_address_of_MatchName_1() { return &___MatchName_1; }
	inline void set_MatchName_1(String_t* value)
	{
		___MatchName_1 = value;
		Il2CppCodeGenWriteBarrier(&___MatchName_1, value);
	}

	inline static int32_t get_offset_of_MatchGroup_2() { return static_cast<int32_t>(offsetof(Identification_t455147138, ___MatchGroup_2)); }
	inline String_t* get_MatchGroup_2() const { return ___MatchGroup_2; }
	inline String_t** get_address_of_MatchGroup_2() { return &___MatchGroup_2; }
	inline void set_MatchGroup_2(String_t* value)
	{
		___MatchGroup_2 = value;
		Il2CppCodeGenWriteBarrier(&___MatchGroup_2, value);
	}

	inline static int32_t get_offset_of_MatchPattern_3() { return static_cast<int32_t>(offsetof(Identification_t455147138, ___MatchPattern_3)); }
	inline String_t* get_MatchPattern_3() const { return ___MatchPattern_3; }
	inline String_t** get_address_of_MatchPattern_3() { return &___MatchPattern_3; }
	inline void set_MatchPattern_3(String_t* value)
	{
		___MatchPattern_3 = value;
		Il2CppCodeGenWriteBarrier(&___MatchPattern_3, value);
	}

	inline static int32_t get_offset_of_RegexPattern_4() { return static_cast<int32_t>(offsetof(Identification_t455147138, ___RegexPattern_4)); }
	inline Regex_t3657309853 * get_RegexPattern_4() const { return ___RegexPattern_4; }
	inline Regex_t3657309853 ** get_address_of_RegexPattern_4() { return &___RegexPattern_4; }
	inline void set_RegexPattern_4(Regex_t3657309853 * value)
	{
		___RegexPattern_4 = value;
		Il2CppCodeGenWriteBarrier(&___RegexPattern_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
