﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeExpression2166265795.h"

// System.CodeDom.CodeTypeReference
struct CodeTypeReference_t3809997434;
// System.CodeDom.CodeExpressionCollection
struct CodeExpressionCollection_t2370433003;
// System.CodeDom.CodeExpression
struct CodeExpression_t2166265795;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeArrayCreateExpression
struct  CodeArrayCreateExpression_t2421540765  : public CodeExpression_t2166265795
{
public:
	// System.CodeDom.CodeTypeReference System.CodeDom.CodeArrayCreateExpression::createType
	CodeTypeReference_t3809997434 * ___createType_1;
	// System.CodeDom.CodeExpressionCollection System.CodeDom.CodeArrayCreateExpression::initializers
	CodeExpressionCollection_t2370433003 * ___initializers_2;
	// System.CodeDom.CodeExpression System.CodeDom.CodeArrayCreateExpression::sizeExpression
	CodeExpression_t2166265795 * ___sizeExpression_3;
	// System.Int32 System.CodeDom.CodeArrayCreateExpression::size
	int32_t ___size_4;

public:
	inline static int32_t get_offset_of_createType_1() { return static_cast<int32_t>(offsetof(CodeArrayCreateExpression_t2421540765, ___createType_1)); }
	inline CodeTypeReference_t3809997434 * get_createType_1() const { return ___createType_1; }
	inline CodeTypeReference_t3809997434 ** get_address_of_createType_1() { return &___createType_1; }
	inline void set_createType_1(CodeTypeReference_t3809997434 * value)
	{
		___createType_1 = value;
		Il2CppCodeGenWriteBarrier(&___createType_1, value);
	}

	inline static int32_t get_offset_of_initializers_2() { return static_cast<int32_t>(offsetof(CodeArrayCreateExpression_t2421540765, ___initializers_2)); }
	inline CodeExpressionCollection_t2370433003 * get_initializers_2() const { return ___initializers_2; }
	inline CodeExpressionCollection_t2370433003 ** get_address_of_initializers_2() { return &___initializers_2; }
	inline void set_initializers_2(CodeExpressionCollection_t2370433003 * value)
	{
		___initializers_2 = value;
		Il2CppCodeGenWriteBarrier(&___initializers_2, value);
	}

	inline static int32_t get_offset_of_sizeExpression_3() { return static_cast<int32_t>(offsetof(CodeArrayCreateExpression_t2421540765, ___sizeExpression_3)); }
	inline CodeExpression_t2166265795 * get_sizeExpression_3() const { return ___sizeExpression_3; }
	inline CodeExpression_t2166265795 ** get_address_of_sizeExpression_3() { return &___sizeExpression_3; }
	inline void set_sizeExpression_3(CodeExpression_t2166265795 * value)
	{
		___sizeExpression_3 = value;
		Il2CppCodeGenWriteBarrier(&___sizeExpression_3, value);
	}

	inline static int32_t get_offset_of_size_4() { return static_cast<int32_t>(offsetof(CodeArrayCreateExpression_t2421540765, ___size_4)); }
	inline int32_t get_size_4() const { return ___size_4; }
	inline int32_t* get_address_of_size_4() { return &___size_4; }
	inline void set_size_4(int32_t value)
	{
		___size_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
