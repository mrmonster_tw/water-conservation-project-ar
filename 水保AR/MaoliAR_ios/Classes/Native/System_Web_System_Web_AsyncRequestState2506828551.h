﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Object
struct Il2CppObject;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t451242010;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.AsyncRequestState
struct  AsyncRequestState_t2506828551  : public Il2CppObject
{
public:
	// System.AsyncCallback System.Web.AsyncRequestState::cb
	AsyncCallback_t3962456242 * ___cb_0;
	// System.Object System.Web.AsyncRequestState::cb_data
	Il2CppObject * ___cb_data_1;
	// System.Boolean System.Web.AsyncRequestState::completed
	bool ___completed_2;
	// System.Threading.ManualResetEvent System.Web.AsyncRequestState::complete_event
	ManualResetEvent_t451242010 * ___complete_event_3;

public:
	inline static int32_t get_offset_of_cb_0() { return static_cast<int32_t>(offsetof(AsyncRequestState_t2506828551, ___cb_0)); }
	inline AsyncCallback_t3962456242 * get_cb_0() const { return ___cb_0; }
	inline AsyncCallback_t3962456242 ** get_address_of_cb_0() { return &___cb_0; }
	inline void set_cb_0(AsyncCallback_t3962456242 * value)
	{
		___cb_0 = value;
		Il2CppCodeGenWriteBarrier(&___cb_0, value);
	}

	inline static int32_t get_offset_of_cb_data_1() { return static_cast<int32_t>(offsetof(AsyncRequestState_t2506828551, ___cb_data_1)); }
	inline Il2CppObject * get_cb_data_1() const { return ___cb_data_1; }
	inline Il2CppObject ** get_address_of_cb_data_1() { return &___cb_data_1; }
	inline void set_cb_data_1(Il2CppObject * value)
	{
		___cb_data_1 = value;
		Il2CppCodeGenWriteBarrier(&___cb_data_1, value);
	}

	inline static int32_t get_offset_of_completed_2() { return static_cast<int32_t>(offsetof(AsyncRequestState_t2506828551, ___completed_2)); }
	inline bool get_completed_2() const { return ___completed_2; }
	inline bool* get_address_of_completed_2() { return &___completed_2; }
	inline void set_completed_2(bool value)
	{
		___completed_2 = value;
	}

	inline static int32_t get_offset_of_complete_event_3() { return static_cast<int32_t>(offsetof(AsyncRequestState_t2506828551, ___complete_event_3)); }
	inline ManualResetEvent_t451242010 * get_complete_event_3() const { return ___complete_event_3; }
	inline ManualResetEvent_t451242010 ** get_address_of_complete_event_3() { return &___complete_event_3; }
	inline void set_complete_event_3(ManualResetEvent_t451242010 * value)
	{
		___complete_event_3 = value;
		Il2CppCodeGenWriteBarrier(&___complete_event_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
