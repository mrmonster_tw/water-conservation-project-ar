﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.IO.Stream
struct Stream_t1273022909;
// System.IO.TextWriter
struct TextWriter_t3478189236;
// System.Xml.XmlTextWriter
struct XmlTextWriter_t2114213153;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Resources.ResXResourceWriter
struct  ResXResourceWriter_t1119841949  : public Il2CppObject
{
public:
	// System.String System.Resources.ResXResourceWriter::filename
	String_t* ___filename_0;
	// System.IO.Stream System.Resources.ResXResourceWriter::stream
	Stream_t1273022909 * ___stream_1;
	// System.IO.TextWriter System.Resources.ResXResourceWriter::textwriter
	TextWriter_t3478189236 * ___textwriter_2;
	// System.Xml.XmlTextWriter System.Resources.ResXResourceWriter::writer
	XmlTextWriter_t2114213153 * ___writer_3;
	// System.Boolean System.Resources.ResXResourceWriter::written
	bool ___written_4;

public:
	inline static int32_t get_offset_of_filename_0() { return static_cast<int32_t>(offsetof(ResXResourceWriter_t1119841949, ___filename_0)); }
	inline String_t* get_filename_0() const { return ___filename_0; }
	inline String_t** get_address_of_filename_0() { return &___filename_0; }
	inline void set_filename_0(String_t* value)
	{
		___filename_0 = value;
		Il2CppCodeGenWriteBarrier(&___filename_0, value);
	}

	inline static int32_t get_offset_of_stream_1() { return static_cast<int32_t>(offsetof(ResXResourceWriter_t1119841949, ___stream_1)); }
	inline Stream_t1273022909 * get_stream_1() const { return ___stream_1; }
	inline Stream_t1273022909 ** get_address_of_stream_1() { return &___stream_1; }
	inline void set_stream_1(Stream_t1273022909 * value)
	{
		___stream_1 = value;
		Il2CppCodeGenWriteBarrier(&___stream_1, value);
	}

	inline static int32_t get_offset_of_textwriter_2() { return static_cast<int32_t>(offsetof(ResXResourceWriter_t1119841949, ___textwriter_2)); }
	inline TextWriter_t3478189236 * get_textwriter_2() const { return ___textwriter_2; }
	inline TextWriter_t3478189236 ** get_address_of_textwriter_2() { return &___textwriter_2; }
	inline void set_textwriter_2(TextWriter_t3478189236 * value)
	{
		___textwriter_2 = value;
		Il2CppCodeGenWriteBarrier(&___textwriter_2, value);
	}

	inline static int32_t get_offset_of_writer_3() { return static_cast<int32_t>(offsetof(ResXResourceWriter_t1119841949, ___writer_3)); }
	inline XmlTextWriter_t2114213153 * get_writer_3() const { return ___writer_3; }
	inline XmlTextWriter_t2114213153 ** get_address_of_writer_3() { return &___writer_3; }
	inline void set_writer_3(XmlTextWriter_t2114213153 * value)
	{
		___writer_3 = value;
		Il2CppCodeGenWriteBarrier(&___writer_3, value);
	}

	inline static int32_t get_offset_of_written_4() { return static_cast<int32_t>(offsetof(ResXResourceWriter_t1119841949, ___written_4)); }
	inline bool get_written_4() const { return ___written_4; }
	inline bool* get_address_of_written_4() { return &___written_4; }
	inline void set_written_4(bool value)
	{
		___written_4 = value;
	}
};

struct ResXResourceWriter_t1119841949_StaticFields
{
public:
	// System.String System.Resources.ResXResourceWriter::BinSerializedObjectMimeType
	String_t* ___BinSerializedObjectMimeType_5;
	// System.String System.Resources.ResXResourceWriter::ByteArraySerializedObjectMimeType
	String_t* ___ByteArraySerializedObjectMimeType_6;
	// System.String System.Resources.ResXResourceWriter::DefaultSerializedObjectMimeType
	String_t* ___DefaultSerializedObjectMimeType_7;
	// System.String System.Resources.ResXResourceWriter::ResMimeType
	String_t* ___ResMimeType_8;
	// System.String System.Resources.ResXResourceWriter::ResourceSchema
	String_t* ___ResourceSchema_9;
	// System.String System.Resources.ResXResourceWriter::SoapSerializedObjectMimeType
	String_t* ___SoapSerializedObjectMimeType_10;
	// System.String System.Resources.ResXResourceWriter::Version
	String_t* ___Version_11;
	// System.String System.Resources.ResXResourceWriter::schema
	String_t* ___schema_12;

public:
	inline static int32_t get_offset_of_BinSerializedObjectMimeType_5() { return static_cast<int32_t>(offsetof(ResXResourceWriter_t1119841949_StaticFields, ___BinSerializedObjectMimeType_5)); }
	inline String_t* get_BinSerializedObjectMimeType_5() const { return ___BinSerializedObjectMimeType_5; }
	inline String_t** get_address_of_BinSerializedObjectMimeType_5() { return &___BinSerializedObjectMimeType_5; }
	inline void set_BinSerializedObjectMimeType_5(String_t* value)
	{
		___BinSerializedObjectMimeType_5 = value;
		Il2CppCodeGenWriteBarrier(&___BinSerializedObjectMimeType_5, value);
	}

	inline static int32_t get_offset_of_ByteArraySerializedObjectMimeType_6() { return static_cast<int32_t>(offsetof(ResXResourceWriter_t1119841949_StaticFields, ___ByteArraySerializedObjectMimeType_6)); }
	inline String_t* get_ByteArraySerializedObjectMimeType_6() const { return ___ByteArraySerializedObjectMimeType_6; }
	inline String_t** get_address_of_ByteArraySerializedObjectMimeType_6() { return &___ByteArraySerializedObjectMimeType_6; }
	inline void set_ByteArraySerializedObjectMimeType_6(String_t* value)
	{
		___ByteArraySerializedObjectMimeType_6 = value;
		Il2CppCodeGenWriteBarrier(&___ByteArraySerializedObjectMimeType_6, value);
	}

	inline static int32_t get_offset_of_DefaultSerializedObjectMimeType_7() { return static_cast<int32_t>(offsetof(ResXResourceWriter_t1119841949_StaticFields, ___DefaultSerializedObjectMimeType_7)); }
	inline String_t* get_DefaultSerializedObjectMimeType_7() const { return ___DefaultSerializedObjectMimeType_7; }
	inline String_t** get_address_of_DefaultSerializedObjectMimeType_7() { return &___DefaultSerializedObjectMimeType_7; }
	inline void set_DefaultSerializedObjectMimeType_7(String_t* value)
	{
		___DefaultSerializedObjectMimeType_7 = value;
		Il2CppCodeGenWriteBarrier(&___DefaultSerializedObjectMimeType_7, value);
	}

	inline static int32_t get_offset_of_ResMimeType_8() { return static_cast<int32_t>(offsetof(ResXResourceWriter_t1119841949_StaticFields, ___ResMimeType_8)); }
	inline String_t* get_ResMimeType_8() const { return ___ResMimeType_8; }
	inline String_t** get_address_of_ResMimeType_8() { return &___ResMimeType_8; }
	inline void set_ResMimeType_8(String_t* value)
	{
		___ResMimeType_8 = value;
		Il2CppCodeGenWriteBarrier(&___ResMimeType_8, value);
	}

	inline static int32_t get_offset_of_ResourceSchema_9() { return static_cast<int32_t>(offsetof(ResXResourceWriter_t1119841949_StaticFields, ___ResourceSchema_9)); }
	inline String_t* get_ResourceSchema_9() const { return ___ResourceSchema_9; }
	inline String_t** get_address_of_ResourceSchema_9() { return &___ResourceSchema_9; }
	inline void set_ResourceSchema_9(String_t* value)
	{
		___ResourceSchema_9 = value;
		Il2CppCodeGenWriteBarrier(&___ResourceSchema_9, value);
	}

	inline static int32_t get_offset_of_SoapSerializedObjectMimeType_10() { return static_cast<int32_t>(offsetof(ResXResourceWriter_t1119841949_StaticFields, ___SoapSerializedObjectMimeType_10)); }
	inline String_t* get_SoapSerializedObjectMimeType_10() const { return ___SoapSerializedObjectMimeType_10; }
	inline String_t** get_address_of_SoapSerializedObjectMimeType_10() { return &___SoapSerializedObjectMimeType_10; }
	inline void set_SoapSerializedObjectMimeType_10(String_t* value)
	{
		___SoapSerializedObjectMimeType_10 = value;
		Il2CppCodeGenWriteBarrier(&___SoapSerializedObjectMimeType_10, value);
	}

	inline static int32_t get_offset_of_Version_11() { return static_cast<int32_t>(offsetof(ResXResourceWriter_t1119841949_StaticFields, ___Version_11)); }
	inline String_t* get_Version_11() const { return ___Version_11; }
	inline String_t** get_address_of_Version_11() { return &___Version_11; }
	inline void set_Version_11(String_t* value)
	{
		___Version_11 = value;
		Il2CppCodeGenWriteBarrier(&___Version_11, value);
	}

	inline static int32_t get_offset_of_schema_12() { return static_cast<int32_t>(offsetof(ResXResourceWriter_t1119841949_StaticFields, ___schema_12)); }
	inline String_t* get_schema_12() const { return ___schema_12; }
	inline String_t** get_address_of_schema_12() { return &___schema_12; }
	inline void set_schema_12(String_t* value)
	{
		___schema_12 = value;
		Il2CppCodeGenWriteBarrier(&___schema_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
