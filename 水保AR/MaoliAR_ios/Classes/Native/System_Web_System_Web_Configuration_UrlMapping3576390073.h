﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.UrlMapping
struct  UrlMapping_t3576390073  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct UrlMapping_t3576390073_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Web.Configuration.UrlMapping::mappedUrlProp
	ConfigurationProperty_t3590861854 * ___mappedUrlProp_13;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.UrlMapping::urlProp
	ConfigurationProperty_t3590861854 * ___urlProp_14;
	// System.Configuration.ConfigurationPropertyCollection System.Web.Configuration.UrlMapping::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_15;

public:
	inline static int32_t get_offset_of_mappedUrlProp_13() { return static_cast<int32_t>(offsetof(UrlMapping_t3576390073_StaticFields, ___mappedUrlProp_13)); }
	inline ConfigurationProperty_t3590861854 * get_mappedUrlProp_13() const { return ___mappedUrlProp_13; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_mappedUrlProp_13() { return &___mappedUrlProp_13; }
	inline void set_mappedUrlProp_13(ConfigurationProperty_t3590861854 * value)
	{
		___mappedUrlProp_13 = value;
		Il2CppCodeGenWriteBarrier(&___mappedUrlProp_13, value);
	}

	inline static int32_t get_offset_of_urlProp_14() { return static_cast<int32_t>(offsetof(UrlMapping_t3576390073_StaticFields, ___urlProp_14)); }
	inline ConfigurationProperty_t3590861854 * get_urlProp_14() const { return ___urlProp_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_urlProp_14() { return &___urlProp_14; }
	inline void set_urlProp_14(ConfigurationProperty_t3590861854 * value)
	{
		___urlProp_14 = value;
		Il2CppCodeGenWriteBarrier(&___urlProp_14, value);
	}

	inline static int32_t get_offset_of_properties_15() { return static_cast<int32_t>(offsetof(UrlMapping_t3576390073_StaticFields, ___properties_15)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_15() const { return ___properties_15; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_15() { return &___properties_15; }
	inline void set_properties_15(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_15 = value;
		Il2CppCodeGenWriteBarrier(&___properties_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
