﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Selector4240724900.h"

// System.Web.Security.MembershipProvider
struct MembershipProvider_t1994512972;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Selectors.UserNamePasswordValidator/MembershipUserNameValidator
struct  MembershipUserNameValidator_t2535273929  : public UserNamePasswordValidator_t4240724900
{
public:
	// System.Web.Security.MembershipProvider System.IdentityModel.Selectors.UserNamePasswordValidator/MembershipUserNameValidator::provider
	MembershipProvider_t1994512972 * ___provider_1;

public:
	inline static int32_t get_offset_of_provider_1() { return static_cast<int32_t>(offsetof(MembershipUserNameValidator_t2535273929, ___provider_1)); }
	inline MembershipProvider_t1994512972 * get_provider_1() const { return ___provider_1; }
	inline MembershipProvider_t1994512972 ** get_address_of_provider_1() { return &___provider_1; }
	inline void set_provider_1(MembershipProvider_t1994512972 * value)
	{
		___provider_1 = value;
		Il2CppCodeGenWriteBarrier(&___provider_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
