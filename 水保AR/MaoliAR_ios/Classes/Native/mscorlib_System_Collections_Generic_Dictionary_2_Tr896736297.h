﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_MulticastDelegate157516450.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24278733032.h"

// System.ServiceModel.Description.ServiceEndpoint
struct ServiceEndpoint_t4038493094;
// System.ServiceModel.Dispatcher.ChannelDispatcher
struct ChannelDispatcher_t2781106991;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Transform`1<System.ServiceModel.Description.ServiceEndpoint,System.ServiceModel.Dispatcher.ChannelDispatcher,System.Collections.Generic.KeyValuePair`2<System.ServiceModel.Description.ServiceEndpoint,System.ServiceModel.Dispatcher.ChannelDispatcher>>
struct  Transform_1_t896736297  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
