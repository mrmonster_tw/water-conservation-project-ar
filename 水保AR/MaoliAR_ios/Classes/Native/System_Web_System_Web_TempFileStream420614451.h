﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_FileStream4292183065.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.TempFileStream
struct  TempFileStream_t420614451  : public FileStream_t4292183065
{
public:
	// System.Boolean System.Web.TempFileStream::read_mode
	bool ___read_mode_16;
	// System.Boolean System.Web.TempFileStream::disposed
	bool ___disposed_17;
	// System.Int64 System.Web.TempFileStream::saved_position
	int64_t ___saved_position_18;

public:
	inline static int32_t get_offset_of_read_mode_16() { return static_cast<int32_t>(offsetof(TempFileStream_t420614451, ___read_mode_16)); }
	inline bool get_read_mode_16() const { return ___read_mode_16; }
	inline bool* get_address_of_read_mode_16() { return &___read_mode_16; }
	inline void set_read_mode_16(bool value)
	{
		___read_mode_16 = value;
	}

	inline static int32_t get_offset_of_disposed_17() { return static_cast<int32_t>(offsetof(TempFileStream_t420614451, ___disposed_17)); }
	inline bool get_disposed_17() const { return ___disposed_17; }
	inline bool* get_address_of_disposed_17() { return &___disposed_17; }
	inline void set_disposed_17(bool value)
	{
		___disposed_17 = value;
	}

	inline static int32_t get_offset_of_saved_position_18() { return static_cast<int32_t>(offsetof(TempFileStream_t420614451, ___saved_position_18)); }
	inline int64_t get_saved_position_18() const { return ___saved_position_18; }
	inline int64_t* get_address_of_saved_position_18() { return &___saved_position_18; }
	inline void set_saved_position_18(int64_t value)
	{
		___saved_position_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
