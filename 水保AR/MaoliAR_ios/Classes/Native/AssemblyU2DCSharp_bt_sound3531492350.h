﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.AudioClip
struct AudioClip_t3680889665;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// bt_sound
struct  bt_sound_t3531492350  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioClip bt_sound::bt_s
	AudioClip_t3680889665 * ___bt_s_2;

public:
	inline static int32_t get_offset_of_bt_s_2() { return static_cast<int32_t>(offsetof(bt_sound_t3531492350, ___bt_s_2)); }
	inline AudioClip_t3680889665 * get_bt_s_2() const { return ___bt_s_2; }
	inline AudioClip_t3680889665 ** get_address_of_bt_s_2() { return &___bt_s_2; }
	inline void set_bt_s_2(AudioClip_t3680889665 * value)
	{
		___bt_s_2 = value;
		Il2CppCodeGenWriteBarrier(&___bt_s_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
