﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// HighlightingSystem.Highlighter
struct Highlighter_t672210414;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HighlighterController
struct  HighlighterController_t3493614325  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean HighlighterController::seeThrough
	bool ___seeThrough_2;
	// System.Boolean HighlighterController::_seeThrough
	bool ____seeThrough_3;
	// HighlightingSystem.Highlighter HighlighterController::h
	Highlighter_t672210414 * ___h_4;

public:
	inline static int32_t get_offset_of_seeThrough_2() { return static_cast<int32_t>(offsetof(HighlighterController_t3493614325, ___seeThrough_2)); }
	inline bool get_seeThrough_2() const { return ___seeThrough_2; }
	inline bool* get_address_of_seeThrough_2() { return &___seeThrough_2; }
	inline void set_seeThrough_2(bool value)
	{
		___seeThrough_2 = value;
	}

	inline static int32_t get_offset_of__seeThrough_3() { return static_cast<int32_t>(offsetof(HighlighterController_t3493614325, ____seeThrough_3)); }
	inline bool get__seeThrough_3() const { return ____seeThrough_3; }
	inline bool* get_address_of__seeThrough_3() { return &____seeThrough_3; }
	inline void set__seeThrough_3(bool value)
	{
		____seeThrough_3 = value;
	}

	inline static int32_t get_offset_of_h_4() { return static_cast<int32_t>(offsetof(HighlighterController_t3493614325, ___h_4)); }
	inline Highlighter_t672210414 * get_h_4() const { return ___h_4; }
	inline Highlighter_t672210414 ** get_address_of_h_4() { return &___h_4; }
	inline void set_h_4(Highlighter_t672210414 * value)
	{
		___h_4 = value;
		Il2CppCodeGenWriteBarrier(&___h_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
