﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XmlReader3121518892.h"

// System.Xml.XmlDictionaryReaderQuotas
struct XmlDictionaryReaderQuotas_t173030297;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Char[]
struct CharU5BU5D_t3528271667;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDictionaryReader
struct  XmlDictionaryReader_t1044334689  : public XmlReader_t3121518892
{
public:
	// System.Xml.XmlDictionaryReaderQuotas System.Xml.XmlDictionaryReader::quotas
	XmlDictionaryReaderQuotas_t173030297 * ___quotas_3;
	// System.Reflection.MethodInfo System.Xml.XmlDictionaryReader::xmlconv_from_bin_hex
	MethodInfo_t * ___xmlconv_from_bin_hex_4;

public:
	inline static int32_t get_offset_of_quotas_3() { return static_cast<int32_t>(offsetof(XmlDictionaryReader_t1044334689, ___quotas_3)); }
	inline XmlDictionaryReaderQuotas_t173030297 * get_quotas_3() const { return ___quotas_3; }
	inline XmlDictionaryReaderQuotas_t173030297 ** get_address_of_quotas_3() { return &___quotas_3; }
	inline void set_quotas_3(XmlDictionaryReaderQuotas_t173030297 * value)
	{
		___quotas_3 = value;
		Il2CppCodeGenWriteBarrier(&___quotas_3, value);
	}

	inline static int32_t get_offset_of_xmlconv_from_bin_hex_4() { return static_cast<int32_t>(offsetof(XmlDictionaryReader_t1044334689, ___xmlconv_from_bin_hex_4)); }
	inline MethodInfo_t * get_xmlconv_from_bin_hex_4() const { return ___xmlconv_from_bin_hex_4; }
	inline MethodInfo_t ** get_address_of_xmlconv_from_bin_hex_4() { return &___xmlconv_from_bin_hex_4; }
	inline void set_xmlconv_from_bin_hex_4(MethodInfo_t * value)
	{
		___xmlconv_from_bin_hex_4 = value;
		Il2CppCodeGenWriteBarrier(&___xmlconv_from_bin_hex_4, value);
	}
};

struct XmlDictionaryReader_t1044334689_StaticFields
{
public:
	// System.Char[] System.Xml.XmlDictionaryReader::wsChars
	CharU5BU5D_t3528271667* ___wsChars_5;

public:
	inline static int32_t get_offset_of_wsChars_5() { return static_cast<int32_t>(offsetof(XmlDictionaryReader_t1044334689_StaticFields, ___wsChars_5)); }
	inline CharU5BU5D_t3528271667* get_wsChars_5() const { return ___wsChars_5; }
	inline CharU5BU5D_t3528271667** get_address_of_wsChars_5() { return &___wsChars_5; }
	inline void set_wsChars_5(CharU5BU5D_t3528271667* value)
	{
		___wsChars_5 = value;
		Il2CppCodeGenWriteBarrier(&___wsChars_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
