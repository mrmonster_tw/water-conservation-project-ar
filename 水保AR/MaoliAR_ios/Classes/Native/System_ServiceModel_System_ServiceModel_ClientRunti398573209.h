﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Co518829156.h"
#include "mscorlib_System_TimeSpan881159249.h"

// System.ServiceModel.Dispatcher.ClientRuntime
struct ClientRuntime_t3616476419;
// System.ServiceModel.Channels.IChannel
struct IChannel_t937207545;
// System.ServiceModel.Channels.IChannelFactory
struct IChannelFactory_t1438449236;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.ClientRuntimeChannel
struct  ClientRuntimeChannel_t398573209  : public CommunicationObject_t518829156
{
public:
	// System.ServiceModel.Dispatcher.ClientRuntime System.ServiceModel.ClientRuntimeChannel::runtime
	ClientRuntime_t3616476419 * ___runtime_9;
	// System.TimeSpan System.ServiceModel.ClientRuntimeChannel::default_open_timeout
	TimeSpan_t881159249  ___default_open_timeout_10;
	// System.TimeSpan System.ServiceModel.ClientRuntimeChannel::default_close_timeout
	TimeSpan_t881159249  ___default_close_timeout_11;
	// System.ServiceModel.Channels.IChannel System.ServiceModel.ClientRuntimeChannel::channel
	Il2CppObject * ___channel_12;
	// System.ServiceModel.Channels.IChannelFactory System.ServiceModel.ClientRuntimeChannel::factory
	Il2CppObject * ___factory_13;
	// System.Boolean System.ServiceModel.ClientRuntimeChannel::did_interactive_initialization
	bool ___did_interactive_initialization_14;
	// System.TimeSpan System.ServiceModel.ClientRuntimeChannel::<OperationTimeout>k__BackingField
	TimeSpan_t881159249  ___U3COperationTimeoutU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_runtime_9() { return static_cast<int32_t>(offsetof(ClientRuntimeChannel_t398573209, ___runtime_9)); }
	inline ClientRuntime_t3616476419 * get_runtime_9() const { return ___runtime_9; }
	inline ClientRuntime_t3616476419 ** get_address_of_runtime_9() { return &___runtime_9; }
	inline void set_runtime_9(ClientRuntime_t3616476419 * value)
	{
		___runtime_9 = value;
		Il2CppCodeGenWriteBarrier(&___runtime_9, value);
	}

	inline static int32_t get_offset_of_default_open_timeout_10() { return static_cast<int32_t>(offsetof(ClientRuntimeChannel_t398573209, ___default_open_timeout_10)); }
	inline TimeSpan_t881159249  get_default_open_timeout_10() const { return ___default_open_timeout_10; }
	inline TimeSpan_t881159249 * get_address_of_default_open_timeout_10() { return &___default_open_timeout_10; }
	inline void set_default_open_timeout_10(TimeSpan_t881159249  value)
	{
		___default_open_timeout_10 = value;
	}

	inline static int32_t get_offset_of_default_close_timeout_11() { return static_cast<int32_t>(offsetof(ClientRuntimeChannel_t398573209, ___default_close_timeout_11)); }
	inline TimeSpan_t881159249  get_default_close_timeout_11() const { return ___default_close_timeout_11; }
	inline TimeSpan_t881159249 * get_address_of_default_close_timeout_11() { return &___default_close_timeout_11; }
	inline void set_default_close_timeout_11(TimeSpan_t881159249  value)
	{
		___default_close_timeout_11 = value;
	}

	inline static int32_t get_offset_of_channel_12() { return static_cast<int32_t>(offsetof(ClientRuntimeChannel_t398573209, ___channel_12)); }
	inline Il2CppObject * get_channel_12() const { return ___channel_12; }
	inline Il2CppObject ** get_address_of_channel_12() { return &___channel_12; }
	inline void set_channel_12(Il2CppObject * value)
	{
		___channel_12 = value;
		Il2CppCodeGenWriteBarrier(&___channel_12, value);
	}

	inline static int32_t get_offset_of_factory_13() { return static_cast<int32_t>(offsetof(ClientRuntimeChannel_t398573209, ___factory_13)); }
	inline Il2CppObject * get_factory_13() const { return ___factory_13; }
	inline Il2CppObject ** get_address_of_factory_13() { return &___factory_13; }
	inline void set_factory_13(Il2CppObject * value)
	{
		___factory_13 = value;
		Il2CppCodeGenWriteBarrier(&___factory_13, value);
	}

	inline static int32_t get_offset_of_did_interactive_initialization_14() { return static_cast<int32_t>(offsetof(ClientRuntimeChannel_t398573209, ___did_interactive_initialization_14)); }
	inline bool get_did_interactive_initialization_14() const { return ___did_interactive_initialization_14; }
	inline bool* get_address_of_did_interactive_initialization_14() { return &___did_interactive_initialization_14; }
	inline void set_did_interactive_initialization_14(bool value)
	{
		___did_interactive_initialization_14 = value;
	}

	inline static int32_t get_offset_of_U3COperationTimeoutU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(ClientRuntimeChannel_t398573209, ___U3COperationTimeoutU3Ek__BackingField_15)); }
	inline TimeSpan_t881159249  get_U3COperationTimeoutU3Ek__BackingField_15() const { return ___U3COperationTimeoutU3Ek__BackingField_15; }
	inline TimeSpan_t881159249 * get_address_of_U3COperationTimeoutU3Ek__BackingField_15() { return &___U3COperationTimeoutU3Ek__BackingField_15; }
	inline void set_U3COperationTimeoutU3Ek__BackingField_15(TimeSpan_t881159249  value)
	{
		___U3COperationTimeoutU3Ek__BackingField_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
