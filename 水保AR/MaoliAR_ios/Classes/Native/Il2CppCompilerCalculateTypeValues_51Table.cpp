﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_ServiceModel_System_ServiceModel_Dispatcher_168687496.h"
#include "System_ServiceModel_System_ServiceModel_Dispatcher3584320896.h"
#include "System_ServiceModel_System_ServiceModel_Dispatcher2253379011.h"
#include "System_ServiceModel_System_ServiceModel_Dispatcher2443828844.h"
#include "System_ServiceModel_System_ServiceModel_Dispatcher4072893799.h"
#include "System_ServiceModel_System_ServiceModel_Dispatcher1414617556.h"
#include "System_ServiceModel_System_ServiceModel_Dispatcher1403801399.h"
#include "System_ServiceModel_System_ServiceModel_Dispatcher_M95375621.h"
#include "System_ServiceModel_System_ServiceModel_Dispatcher3165538073.h"
#include "System_ServiceModel_System_ServiceModel_Dispatcher_784726222.h"
#include "System_ServiceModel_System_ServiceModel_Dispatcher3356814020.h"
#include "System_ServiceModel_System_ServiceModel_Dispatcher3681190363.h"
#include "System_ServiceModel_System_ServiceModel_Dispatcher_689391465.h"
#include "System_ServiceModel_System_ServiceModel_Dispatcher1446901836.h"
#include "System_ServiceModel_System_ServiceModel_Dispatcher3841213848.h"
#include "System_ServiceModel_System_ServiceModel_Dispatcher3562360033.h"
#include "System_ServiceModel_System_ServiceModel_Dispatcher1510136530.h"
#include "System_ServiceModel_System_ServiceModel_Dispatcher_115636188.h"
#include "System_ServiceModel_System_ServiceModel_Dispatcher2611969614.h"
#include "System_ServiceModel_System_ServiceModel_MsmqIntegr1515740572.h"
#include "System_ServiceModel_System_ServiceModel_PeerResolv2606082360.h"
#include "System_ServiceModel_System_ServiceModel_PeerResolv1221202520.h"
#include "System_ServiceModel_System_ServiceModel_PeerResolv2704353376.h"
#include "System_ServiceModel_System_ServiceModel_PeerResolv1049730577.h"
#include "System_ServiceModel_System_ServiceModel_PeerResolv3246124786.h"
#include "System_ServiceModel_System_ServiceModel_PeerResolv1552258726.h"
#include "System_ServiceModel_System_ServiceModel_PeerResolv2102618862.h"
#include "System_ServiceModel_System_ServiceModel_PeerResolv1743278054.h"
#include "System_ServiceModel_System_ServiceModel_PeerResolve868720659.h"
#include "System_ServiceModel_System_ServiceModel_PeerResolv2596418321.h"
#include "System_ServiceModel_System_ServiceModel_PeerResolve116120264.h"
#include "System_ServiceModel_System_ServiceModel_PeerResolv1503758284.h"
#include "System_ServiceModel_System_ServiceModel_PeerResolve689256922.h"
#include "System_ServiceModel_System_ServiceModel_PeerResolv3820618709.h"
#include "System_ServiceModel_System_ServiceModel_PeerResolve238856994.h"
#include "System_ServiceModel_System_ServiceModel_PeerResolv2470312020.h"
#include "System_ServiceModel_System_ServiceModel_PeerResolve650062324.h"
#include "System_ServiceModel_System_ServiceModel_PeerResolv1004748920.h"
#include "System_ServiceModel_System_ServiceModel_Security_T3240244108.h"
#include "System_ServiceModel_System_ServiceModel_Security_T3798411224.h"
#include "System_ServiceModel_System_ServiceModel_Security_T1730123968.h"
#include "System_ServiceModel_System_ServiceModel_Security_To589097440.h"
#include "System_ServiceModel_System_ServiceModel_Security_T3159509539.h"
#include "System_ServiceModel_System_ServiceModel_Security_T1977550489.h"
#include "System_ServiceModel_System_ServiceModel_Security_T3764096797.h"
#include "System_ServiceModel_System_ServiceModel_Security_T3823565339.h"
#include "System_ServiceModel_System_ServiceModel_Security_T3300559348.h"
#include "System_ServiceModel_System_ServiceModel_Security_To994829549.h"
#include "System_ServiceModel_System_ServiceModel_Security_T3337991277.h"
#include "System_ServiceModel_System_ServiceModel_Security_T1285034423.h"
#include "System_ServiceModel_System_ServiceModel_Security_T3373317487.h"
#include "System_ServiceModel_System_ServiceModel_Security_To268315636.h"
#include "System_ServiceModel_System_ServiceModel_Security_T2082245521.h"
#include "System_ServiceModel_System_ServiceModel_Security_T2840861221.h"
#include "System_ServiceModel_System_ServiceModel_Security_T2961949979.h"
#include "System_ServiceModel_System_ServiceModel_Security_T3624779732.h"
#include "System_ServiceModel_System_ServiceModel_Security_T4052758026.h"
#include "System_ServiceModel_System_ServiceModel_Security_T1523964452.h"
#include "System_ServiceModel_System_ServiceModel_Security_T2868958784.h"
#include "System_ServiceModel_System_ServiceModel_Security_T3428665768.h"
#include "System_ServiceModel_System_ServiceModel_Security_Tok56711536.h"
#include "System_ServiceModel_System_ServiceModel_Security_To106247968.h"
#include "System_ServiceModel_System_ServiceModel_Security_To485325505.h"
#include "System_ServiceModel_System_ServiceModel_Security_T2143488888.h"
#include "System_ServiceModel_System_ServiceModel_Security_S3056146291.h"
#include "System_ServiceModel_System_ServiceModel_Security_S1015217490.h"
#include "System_ServiceModel_System_ServiceModel_Security_T2549432055.h"
#include "System_ServiceModel_System_ServiceModel_Security_T1333352365.h"
#include "System_ServiceModel_System_ServiceModel_Security_To613821154.h"
#include "System_ServiceModel_System_ServiceModel_Security_T4188723207.h"
#include "System_ServiceModel_System_ServiceModel_Security_T2013392034.h"
#include "System_ServiceModel_System_ServiceModel_Security_T3237646940.h"
#include "System_ServiceModel_System_ServiceModel_Security_T3168857361.h"
#include "System_ServiceModel_System_ServiceModel_Security_To599712834.h"
#include "System_ServiceModel_System_ServiceModel_Security_T2582918163.h"
#include "System_ServiceModel_System_ServiceModel_Security_To950680249.h"
#include "System_ServiceModel_System_ServiceModel_Security_T1739095634.h"
#include "System_ServiceModel_System_ServiceModel_Security_S1506236276.h"
#include "System_ServiceModel_System_ServiceModel_Security_S1775079126.h"
#include "System_ServiceModel_System_ServiceModel_Security_S2076952883.h"
#include "System_ServiceModel_System_ServiceModel_Security_To907883796.h"
#include "System_ServiceModel_System_ServiceModel_Security_T2685676710.h"
#include "System_ServiceModel_System_ServiceModel_Security_To151923929.h"
#include "System_ServiceModel_System_ServiceModel_Security_T3338798525.h"
#include "System_ServiceModel_System_ServiceModel_Security_T2219064801.h"
#include "System_ServiceModel_System_ServiceModel_Security_T4004515376.h"
#include "System_ServiceModel_System_ServiceModel_Security_To738218815.h"
#include "System_ServiceModel_System_ServiceModel_Security_T3162014039.h"
#include "System_ServiceModel_System_ServiceModel_Security_B3200312853.h"
#include "System_ServiceModel_System_ServiceModel_Security_C2141883287.h"
#include "System_ServiceModel_System_ServiceModel_Security_D2922702212.h"
#include "System_ServiceModel_System_ServiceModel_Security_H4244309379.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5100 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5101 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5102 = { sizeof (InitializingHandler_t168687496), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5103 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5104 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5105 = { sizeof (InputOrReplyRequestProcessor_t3584320896), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5105[2] = 
{
	InputOrReplyRequestProcessor_t3584320896::get_offset_of_dispatch_runtime_4(),
	InputOrReplyRequestProcessor_t3584320896::get_offset_of_reply_or_input_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5106 = { sizeof (MatchAllMessageFilter_t2253379011), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5107 = { sizeof (MatchNoneMessageFilter_t2443828844), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5108 = { sizeof (MessageFilter_t4072893799), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5109 = { sizeof (MessageProcessingContext_t1414617556), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5109[8] = 
{
	MessageProcessingContext_t1414617556::get_offset_of_operation_context_0(),
	MessageProcessingContext_t1414617556::get_offset_of_request_context_1(),
	MessageProcessingContext_t1414617556::get_offset_of_incoming_message_2(),
	MessageProcessingContext_t1414617556::get_offset_of_reply_message_3(),
	MessageProcessingContext_t1414617556::get_offset_of_instance_context_4(),
	MessageProcessingContext_t1414617556::get_offset_of_processingException_5(),
	MessageProcessingContext_t1414617556::get_offset_of_operation_6(),
	MessageProcessingContext_t1414617556::get_offset_of_user_events_handler_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5110 = { sizeof (UserEventsHandler_t1403801399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5110[5] = 
{
	UserEventsHandler_t1403801399::get_offset_of_request_context_0(),
	UserEventsHandler_t1403801399::get_offset_of_dispatch_runtime_1(),
	UserEventsHandler_t1403801399::get_offset_of_channel_2(),
	UserEventsHandler_t1403801399::get_offset_of_msg_inspectors_states_3(),
	UserEventsHandler_t1403801399::get_offset_of_callcontext_initializers_states_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5111 = { sizeof (MexInstanceContextProvider_t95375621), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5111[1] = 
{
	MexInstanceContextProvider_t95375621::get_offset_of_ctx_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5112 = { sizeof (MetadataExchange_t3165538073), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5112[1] = 
{
	MetadataExchange_t3165538073::get_offset_of_beh_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5113 = { sizeof (MultipleFilterMatchesException_t784726222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5114 = { sizeof (OperationInvokerHandler_t3356814020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5114[1] = 
{
	OperationInvokerHandler_t3356814020::get_offset_of_duplex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5115 = { sizeof (PostReceiveRequestHandler_t3681190363), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5116 = { sizeof (SecurityHandler_t689391465), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5117 = { sizeof (SeekableXPathNavigator_t1446901836), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5118 = { sizeof (ServiceThrottle_t3841213848), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5118[3] = 
{
	ServiceThrottle_t3841213848::get_offset_of_max_call_0(),
	ServiceThrottle_t3841213848::get_offset_of_max_session_1(),
	ServiceThrottle_t3841213848::get_offset_of_max_instance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5119 = { sizeof (SessionInstanceContextProvider_t3562360033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5119[2] = 
{
	SessionInstanceContextProvider_t3562360033::get_offset_of_host_0(),
	SessionInstanceContextProvider_t3562360033::get_offset_of_pool_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5120 = { sizeof (SingletonInstanceContextProvider_t1510136530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5120[1] = 
{
	SingletonInstanceContextProvider_t1510136530::get_offset_of_ctx_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5121 = { sizeof (XPathMessageFilter_t115636188), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5122 = { sizeof (InstanceBehavior_t2611969614), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5122[1] = 
{
	InstanceBehavior_t2611969614::get_offset_of_dispatch_runtime_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5123 = { sizeof (MsmqIntegrationBinding_t1515740572), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5124 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5125 = { sizeof (ConnectInfoDC_t2606082360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5125[2] = 
{
	ConnectInfoDC_t2606082360::get_offset_of_U3CAddressU3Ek__BackingField_0(),
	ConnectInfoDC_t2606082360::get_offset_of_U3CNodeIdU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5126 = { sizeof (ConnectInfo_t1221202520), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5126[1] = 
{
	ConnectInfo_t1221202520::get_offset_of_dc_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5127 = { sizeof (WelcomeInfoDC_t2704353376), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5127[1] = 
{
	WelcomeInfoDC_t2704353376::get_offset_of_U3CNodeIdU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5128 = { sizeof (WelcomeInfo_t1049730577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5128[1] = 
{
	WelcomeInfo_t1049730577::get_offset_of_dc_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5129 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5130 = { sizeof (PeerCustomResolverSettings_t3246124786), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5130[3] = 
{
	PeerCustomResolverSettings_t3246124786::get_offset_of_U3CAddressU3Ek__BackingField_0(),
	PeerCustomResolverSettings_t3246124786::get_offset_of_U3CBindingU3Ek__BackingField_1(),
	PeerCustomResolverSettings_t3246124786::get_offset_of_U3CResolverU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5131 = { sizeof (PeerReferralPolicy_t1552258726)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5131[4] = 
{
	PeerReferralPolicy_t1552258726::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5132 = { sizeof (PeerResolverMode_t2102618862)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5132[4] = 
{
	PeerResolverMode_t2102618862::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5133 = { sizeof (PeerResolverSettings_t1743278054), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5133[3] = 
{
	PeerResolverSettings_t1743278054::get_offset_of_custom_0(),
	PeerResolverSettings_t1743278054::get_offset_of_U3CModeU3Ek__BackingField_1(),
	PeerResolverSettings_t1743278054::get_offset_of_U3CReferralPolicyU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5134 = { sizeof (RegisterInfo_t868720659), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5134[1] = 
{
	RegisterInfo_t868720659::get_offset_of_body_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5135 = { sizeof (RegisterInfoDC_t2596418321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5135[3] = 
{
	RegisterInfoDC_t2596418321::get_offset_of_client_id_0(),
	RegisterInfoDC_t2596418321::get_offset_of_mesh_id_1(),
	RegisterInfoDC_t2596418321::get_offset_of_node_address_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5136 = { sizeof (RegisterResponseInfo_t116120264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5136[1] = 
{
	RegisterResponseInfo_t116120264::get_offset_of_body_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5137 = { sizeof (RegisterResponseInfoDC_t1503758284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5137[1] = 
{
	RegisterResponseInfoDC_t1503758284::get_offset_of_registration_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5138 = { sizeof (ResolveInfo_t689256922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5138[1] = 
{
	ResolveInfo_t689256922::get_offset_of_body_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5139 = { sizeof (ResolveInfoDC_t3820618709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5139[3] = 
{
	ResolveInfoDC_t3820618709::get_offset_of_client_id_0(),
	ResolveInfoDC_t3820618709::get_offset_of_max_addresses_1(),
	ResolveInfoDC_t3820618709::get_offset_of_mesh_id_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5140 = { sizeof (ResolveResponseInfo_t238856994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5140[1] = 
{
	ResolveResponseInfo_t238856994::get_offset_of_body_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5141 = { sizeof (ResolveResponseInfoDC_t2470312020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5141[1] = 
{
	ResolveResponseInfoDC_t2470312020::get_offset_of_addresses_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5142 = { sizeof (UnregisterInfo_t650062324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5142[1] = 
{
	UnregisterInfo_t650062324::get_offset_of_body_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5143 = { sizeof (UnregisterInfoDC_t1004748920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5143[2] = 
{
	UnregisterInfoDC_t1004748920::get_offset_of_mesh_id_0(),
	UnregisterInfoDC_t1004748920::get_offset_of_registration_id_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5144 = { sizeof (AuthenticatorCommunicationObject_t3240244108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5144[7] = 
{
	AuthenticatorCommunicationObject_t3240244108::get_offset_of_issuer_binding_9(),
	AuthenticatorCommunicationObject_t3240244108::get_offset_of_issuer_address_10(),
	AuthenticatorCommunicationObject_t3240244108::get_offset_of_listen_uri_11(),
	AuthenticatorCommunicationObject_t3240244108::get_offset_of_behaviors_12(),
	AuthenticatorCommunicationObject_t3240244108::get_offset_of_serializer_13(),
	AuthenticatorCommunicationObject_t3240244108::get_offset_of_algorithm_14(),
	AuthenticatorCommunicationObject_t3240244108::get_offset_of_element_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5145 = { sizeof (BinarySecretSecurityToken_t3798411224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5145[5] = 
{
	BinarySecretSecurityToken_t3798411224::get_offset_of_keys_0(),
	BinarySecretSecurityToken_t3798411224::get_offset_of_id_1(),
	BinarySecretSecurityToken_t3798411224::get_offset_of_key_2(),
	BinarySecretSecurityToken_t3798411224::get_offset_of_allow_crypto_3(),
	BinarySecretSecurityToken_t3798411224::get_offset_of_valid_from_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5146 = { sizeof (ClaimTypeRequirement_t1730123968), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5147 = { sizeof (CommunicationSecurityTokenAuthenticator_t589097440), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5147[1] = 
{
	CommunicationSecurityTokenAuthenticator_t589097440::get_offset_of_issuance_handler_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5148 = { sizeof (CommunicationSecurityTokenProvider_t3159509539), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5149 = { sizeof (DerivedKeySecurityToken_t1977550489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5149[11] = 
{
	DerivedKeySecurityToken_t1977550489::get_offset_of_algorithm_0(),
	DerivedKeySecurityToken_t1977550489::get_offset_of_reference_1(),
	DerivedKeySecurityToken_t1977550489::get_offset_of_generation_2(),
	DerivedKeySecurityToken_t1977550489::get_offset_of_offset_3(),
	DerivedKeySecurityToken_t1977550489::get_offset_of_length_4(),
	DerivedKeySecurityToken_t1977550489::get_offset_of_id_5(),
	DerivedKeySecurityToken_t1977550489::get_offset_of_name_6(),
	DerivedKeySecurityToken_t1977550489::get_offset_of_label_7(),
	DerivedKeySecurityToken_t1977550489::get_offset_of_nonce_8(),
	DerivedKeySecurityToken_t1977550489::get_offset_of_keys_9(),
	DerivedKeySecurityToken_t1977550489::get_offset_of_reflist_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5150 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5151 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5152 = { sizeof (InitiatorServiceModelSecurityTokenRequirement_t3764096797), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5153 = { sizeof (InternalEncryptedKeyIdentifierClause_t3823565339), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5154 = { sizeof (IssuedSecurityTokenParameters_t3300559348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5154[8] = 
{
	IssuedSecurityTokenParameters_t3300559348::get_offset_of_binding_4(),
	IssuedSecurityTokenParameters_t3300559348::get_offset_of_issuer_address_5(),
	IssuedSecurityTokenParameters_t3300559348::get_offset_of_issuer_meta_address_6(),
	IssuedSecurityTokenParameters_t3300559348::get_offset_of_key_size_7(),
	IssuedSecurityTokenParameters_t3300559348::get_offset_of_key_type_8(),
	IssuedSecurityTokenParameters_t3300559348::get_offset_of_token_type_9(),
	IssuedSecurityTokenParameters_t3300559348::get_offset_of_reqs_10(),
	IssuedSecurityTokenParameters_t3300559348::get_offset_of_additional_reqs_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5155 = { sizeof (IssuedSecurityTokenProvider_t994829549), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5155[8] = 
{
	IssuedSecurityTokenProvider_t994829549::get_offset_of_comm_0(),
	IssuedSecurityTokenProvider_t994829549::get_offset_of_entropy_mode_1(),
	IssuedSecurityTokenProvider_t994829549::get_offset_of_max_cache_time_2(),
	IssuedSecurityTokenProvider_t994829549::get_offset_of_version_3(),
	IssuedSecurityTokenProvider_t994829549::get_offset_of_threshold_4(),
	IssuedSecurityTokenProvider_t994829549::get_offset_of_verifier_5(),
	IssuedSecurityTokenProvider_t994829549::get_offset_of_cache_issued_tokens_6(),
	IssuedSecurityTokenProvider_t994829549::get_offset_of_request_params_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5156 = { sizeof (IssuedTokenCommunicationObject_t3337991277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5156[1] = 
{
	IssuedTokenCommunicationObject_t3337991277::get_offset_of_comm_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5157 = { sizeof (KerberosSecurityTokenParameters_t1285034423), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5158 = { sizeof (ProviderCommunicationObject_t3373317487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5158[6] = 
{
	ProviderCommunicationObject_t3373317487::get_offset_of_issuer_binding_9(),
	ProviderCommunicationObject_t3373317487::get_offset_of_issuer_address_10(),
	ProviderCommunicationObject_t3373317487::get_offset_of_target_address_11(),
	ProviderCommunicationObject_t3373317487::get_offset_of_behaviors_12(),
	ProviderCommunicationObject_t3373317487::get_offset_of_serializer_13(),
	ProviderCommunicationObject_t3373317487::get_offset_of_algorithm_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5159 = { sizeof (RecipientServiceModelSecurityTokenRequirement_t268315636), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5160 = { sizeof (SecureConversationSecurityTokenAuthenticator_t2082245521), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5160[4] = 
{
	SecureConversationSecurityTokenAuthenticator_t2082245521::get_offset_of_req_1(),
	SecureConversationSecurityTokenAuthenticator_t2082245521::get_offset_of_sc_auth_2(),
	SecureConversationSecurityTokenAuthenticator_t2082245521::get_offset_of_sc_res_3(),
	SecureConversationSecurityTokenAuthenticator_t2082245521::get_offset_of_comm_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5161 = { sizeof (WsscAuthenticatorCommunicationObject_t2840861221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5161[1] = 
{
	WsscAuthenticatorCommunicationObject_t2840861221::get_offset_of_proxy_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5162 = { sizeof (SecureConversationSecurityTokenParameters_t2961949979), -1, sizeof(SecureConversationSecurityTokenParameters_t2961949979_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5162[5] = 
{
	SecureConversationSecurityTokenParameters_t2961949979_StaticFields::get_offset_of_default_channel_protection_requirements_4(),
	SecureConversationSecurityTokenParameters_t2961949979_StaticFields::get_offset_of_dummy_context_5(),
	SecureConversationSecurityTokenParameters_t2961949979::get_offset_of_element_6(),
	SecureConversationSecurityTokenParameters_t2961949979::get_offset_of_requirements_7(),
	SecureConversationSecurityTokenParameters_t2961949979::get_offset_of_cancellable_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5163 = { sizeof (SecurityContextSecurityToken_t3624779732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5163[11] = 
{
	SecurityContextSecurityToken_t3624779732::get_offset_of_id_0(),
	SecurityContextSecurityToken_t3624779732::get_offset_of_key_1(),
	SecurityContextSecurityToken_t3624779732::get_offset_of_keys_2(),
	SecurityContextSecurityToken_t3624779732::get_offset_of_token_since_3(),
	SecurityContextSecurityToken_t3624779732::get_offset_of_token_until_4(),
	SecurityContextSecurityToken_t3624779732::get_offset_of_key_since_5(),
	SecurityContextSecurityToken_t3624779732::get_offset_of_key_until_6(),
	SecurityContextSecurityToken_t3624779732::get_offset_of_context_id_7(),
	SecurityContextSecurityToken_t3624779732::get_offset_of_key_generation_8(),
	SecurityContextSecurityToken_t3624779732::get_offset_of_policies_9(),
	SecurityContextSecurityToken_t3624779732::get_offset_of_cookie_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5164 = { sizeof (SecurityContextSecurityTokenAuthenticator_t4052758026), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5165 = { sizeof (SecurityContextSecurityTokenResolver_t1523964452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5165[3] = 
{
	SecurityContextSecurityTokenResolver_t1523964452::get_offset_of_capacity_0(),
	SecurityContextSecurityTokenResolver_t1523964452::get_offset_of_allow_removal_1(),
	SecurityContextSecurityTokenResolver_t1523964452::get_offset_of_cache_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5166 = { sizeof (SecurityTokenParameters_t2868958784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5166[4] = 
{
	SecurityTokenParameters_t2868958784::get_offset_of_inclusion_mode_0(),
	SecurityTokenParameters_t2868958784::get_offset_of_reference_style_1(),
	SecurityTokenParameters_t2868958784::get_offset_of_require_derived_keys_2(),
	SecurityTokenParameters_t2868958784::get_offset_of_issuer_binding_context_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5167 = { sizeof (SecurityTokenReferenceStyle_t3428665768)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5167[3] = 
{
	SecurityTokenReferenceStyle_t3428665768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5168 = { sizeof (ServiceModelSecurityTokenRequirement_t56711536), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5169 = { sizeof (ServiceModelSecurityTokenTypes_t106247968), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5170 = { sizeof (SpnegoSecurityTokenAuthenticator_t485325505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5170[2] = 
{
	SpnegoSecurityTokenAuthenticator_t485325505::get_offset_of_manager_1(),
	SpnegoSecurityTokenAuthenticator_t485325505::get_offset_of_comm_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5171 = { sizeof (SpnegoAuthenticatorCommunicationObject_t2143488888), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5171[3] = 
{
	SpnegoAuthenticatorCommunicationObject_t2143488888::get_offset_of_owner_16(),
	SpnegoAuthenticatorCommunicationObject_t2143488888::get_offset_of_proxy_17(),
	SpnegoAuthenticatorCommunicationObject_t2143488888::get_offset_of_sessions_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5172 = { sizeof (SpnegoSecurityTokenProvider_t3056146291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5172[2] = 
{
	SpnegoSecurityTokenProvider_t3056146291::get_offset_of_manager_0(),
	SpnegoSecurityTokenProvider_t3056146291::get_offset_of_comm_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5173 = { sizeof (SpnegoCommunicationObject_t1015217490), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5173[2] = 
{
	SpnegoCommunicationObject_t1015217490::get_offset_of_owner_15(),
	SpnegoCommunicationObject_t1015217490::get_offset_of_proxy_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5174 = { sizeof (SslSecurityTokenAuthenticator_t2549432055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5174[3] = 
{
	SslSecurityTokenAuthenticator_t2549432055::get_offset_of_manager_1(),
	SslSecurityTokenAuthenticator_t2549432055::get_offset_of_comm_2(),
	SslSecurityTokenAuthenticator_t2549432055::get_offset_of_mutual_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5175 = { sizeof (SslAuthenticatorCommunicationObject_t1333352365), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5175[3] = 
{
	SslAuthenticatorCommunicationObject_t1333352365::get_offset_of_owner_16(),
	SslAuthenticatorCommunicationObject_t1333352365::get_offset_of_proxy_17(),
	SslAuthenticatorCommunicationObject_t1333352365::get_offset_of_sessions_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5176 = { sizeof (TlsServerSessionInfo_t613821154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5176[3] = 
{
	TlsServerSessionInfo_t613821154::get_offset_of_ContextId_0(),
	TlsServerSessionInfo_t613821154::get_offset_of_Tls_1(),
	TlsServerSessionInfo_t613821154::get_offset_of_Messages_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5177 = { sizeof (SslSecurityTokenParameters_t4188723207), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5177[2] = 
{
	SslSecurityTokenParameters_t4188723207::get_offset_of_cert_4(),
	SslSecurityTokenParameters_t4188723207::get_offset_of_cancel_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5178 = { sizeof (SslSecurityTokenProvider_t2013392034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5178[2] = 
{
	SslSecurityTokenProvider_t2013392034::get_offset_of_comm_0(),
	SslSecurityTokenProvider_t2013392034::get_offset_of_manager_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5179 = { sizeof (SslCommunicationObject_t3237646940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5179[3] = 
{
	SslCommunicationObject_t3237646940::get_offset_of_owner_15(),
	SslCommunicationObject_t3237646940::get_offset_of_proxy_16(),
	SslCommunicationObject_t3237646940::get_offset_of_client_certificate_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5180 = { sizeof (TlsnegoClientSessionContext_t3168857361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5180[3] = 
{
	TlsnegoClientSessionContext_t3168857361::get_offset_of_doc_0(),
	TlsnegoClientSessionContext_t3168857361::get_offset_of_t_1(),
	TlsnegoClientSessionContext_t3168857361::get_offset_of_stream_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5181 = { sizeof (SslnegoCookieResolver_t599712834), -1, sizeof(SslnegoCookieResolver_t599712834_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5181[1] = 
{
	SslnegoCookieResolver_t599712834_StaticFields::get_offset_of_U3CU3Ef__switchU24map1C_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5182 = { sizeof (SspiClientSecurityTokenAuthenticator_t2582918163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5182[1] = 
{
	SspiClientSecurityTokenAuthenticator_t2582918163::get_offset_of_manager_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5183 = { sizeof (SspiSecurityToken_t950680249), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5184 = { sizeof (SspiSecurityTokenParameters_t1739095634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5184[1] = 
{
	SspiSecurityTokenParameters_t1739095634::get_offset_of_cancel_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5185 = { sizeof (SspiSession_t1506236276), -1, sizeof(SspiSession_t1506236276_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5185[8] = 
{
	SspiSession_t1506236276_StaticFields::get_offset_of_NtlmSSP_0(),
	SspiSession_t1506236276::get_offset_of_Challenge_1(),
	SspiSession_t1506236276::get_offset_of_Context_2(),
	SspiSession_t1506236276::get_offset_of_ServerOSVersion_3(),
	SspiSession_t1506236276::get_offset_of_ServerName_4(),
	SspiSession_t1506236276::get_offset_of_DomainName_5(),
	SspiSession_t1506236276::get_offset_of_DnsHostName_6(),
	SspiSession_t1506236276::get_offset_of_DnsDomainName_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5186 = { sizeof (SspiClientSession_t1775079126), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5186[3] = 
{
	SspiClientSession_t1775079126::get_offset_of_type2_8(),
	SspiClientSession_t1775079126::get_offset_of_type3_9(),
	SspiClientSession_t1775079126::get_offset_of_TargetName_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5187 = { sizeof (SspiServerSession_t2076952883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5187[4] = 
{
	SspiServerSession_t2076952883::get_offset_of_TargetName_8(),
	SspiServerSession_t2076952883::get_offset_of_type1_9(),
	SspiServerSession_t2076952883::get_offset_of_type2_10(),
	SspiServerSession_t2076952883::get_offset_of_type3_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5188 = { sizeof (SupportingTokenParameters_t907883796), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5188[4] = 
{
	SupportingTokenParameters_t907883796::get_offset_of_endorsing_0(),
	SupportingTokenParameters_t907883796::get_offset_of_signed_1(),
	SupportingTokenParameters_t907883796::get_offset_of_signed_encrypted_2(),
	SupportingTokenParameters_t907883796::get_offset_of_signed_endorsing_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5189 = { sizeof (TlsSession_t2685676710), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5190 = { sizeof (TlsClientSession_t151923929), -1, sizeof(TlsClientSession_t151923929_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5190[4] = 
{
	TlsClientSession_t151923929::get_offset_of_ssl_0(),
	TlsClientSession_t151923929::get_offset_of_stream_1(),
	TlsClientSession_t151923929::get_offset_of_mutual_2(),
	TlsClientSession_t151923929_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5191 = { sizeof (TlsServerSession_t3338798525), -1, sizeof(TlsServerSession_t3338798525_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5191[4] = 
{
	TlsServerSession_t3338798525::get_offset_of_ssl_0(),
	TlsServerSession_t3338798525::get_offset_of_stream_1(),
	TlsServerSession_t3338798525::get_offset_of_mutual_2(),
	TlsServerSession_t3338798525_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5192 = { sizeof (U3CTlsServerSessionU3Ec__AnonStorey21_t2219064801), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5192[1] = 
{
	U3CTlsServerSessionU3Ec__AnonStorey21_t2219064801::get_offset_of_cert_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5193 = { sizeof (UserNameSecurityTokenParameters_t4004515376), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5194 = { sizeof (WrappedKeySecurityToken_t738218815), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5194[10] = 
{
	WrappedKeySecurityToken_t738218815::get_offset_of_id_0(),
	WrappedKeySecurityToken_t738218815::get_offset_of_raw_key_1(),
	WrappedKeySecurityToken_t738218815::get_offset_of_wrapped_key_2(),
	WrappedKeySecurityToken_t738218815::get_offset_of_wrap_alg_3(),
	WrappedKeySecurityToken_t738218815::get_offset_of_wrap_token_4(),
	WrappedKeySecurityToken_t738218815::get_offset_of_wrap_token_ref_5(),
	WrappedKeySecurityToken_t738218815::get_offset_of_valid_from_6(),
	WrappedKeySecurityToken_t738218815::get_offset_of_keys_7(),
	WrappedKeySecurityToken_t738218815::get_offset_of_reference_list_8(),
	WrappedKeySecurityToken_t738218815::get_offset_of_keyhash_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5195 = { sizeof (X509SecurityTokenParameters_t3162014039), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5195[1] = 
{
	X509SecurityTokenParameters_t3162014039::get_offset_of_reference_style_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5196 = { sizeof (BinarySecretKeyIdentifierClause_t3200312853), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5197 = { sizeof (ChannelProtectionRequirements_t2141883287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5197[5] = 
{
	ChannelProtectionRequirements_t2141883287::get_offset_of_is_readonly_0(),
	ChannelProtectionRequirements_t2141883287::get_offset_of_in_enc_1(),
	ChannelProtectionRequirements_t2141883287::get_offset_of_in_sign_2(),
	ChannelProtectionRequirements_t2141883287::get_offset_of_out_enc_3(),
	ChannelProtectionRequirements_t2141883287::get_offset_of_out_sign_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5198 = { sizeof (DataProtectionSecurityStateEncoder_t2922702212), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5198[2] = 
{
	DataProtectionSecurityStateEncoder_t2922702212::get_offset_of_user_0(),
	DataProtectionSecurityStateEncoder_t2922702212::get_offset_of_entropy_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5199 = { sizeof (HttpDigestClientCredential_t4244309379), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5199[1] = 
{
	HttpDigestClientCredential_t4244309379::get_offset_of_credential_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
