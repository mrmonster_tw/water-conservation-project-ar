﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_PeerNode3248961724.h"

// System.Net.IPAddress
struct IPAddress_t241777590;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.PeerNodeImpl
struct  PeerNodeImpl_t609519394  : public PeerNode_t3248961724
{
public:
	// System.Net.IPAddress System.ServiceModel.PeerNodeImpl::listen_address
	IPAddress_t241777590 * ___listen_address_7;

public:
	inline static int32_t get_offset_of_listen_address_7() { return static_cast<int32_t>(offsetof(PeerNodeImpl_t609519394, ___listen_address_7)); }
	inline IPAddress_t241777590 * get_listen_address_7() const { return ___listen_address_7; }
	inline IPAddress_t241777590 ** get_address_of_listen_address_7() { return &___listen_address_7; }
	inline void set_listen_address_7(IPAddress_t241777590 * value)
	{
		___listen_address_7 = value;
		Il2CppCodeGenWriteBarrier(&___listen_address_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
