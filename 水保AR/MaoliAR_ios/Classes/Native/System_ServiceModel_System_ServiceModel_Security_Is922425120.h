﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_System_ServiceModel_Security_X3346525935.h"
#include "System_System_Security_Cryptography_X509Certificat2571829933.h"
#include "System_System_Security_Cryptography_X509Certificat2864310644.h"

// System.Collections.Generic.List`1<System.Security.Cryptography.X509Certificates.X509Certificate2>
struct List_1_t2186123868;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.IssuedTokenServiceCredential
struct  IssuedTokenServiceCredential_t922425120  : public Il2CppObject
{
public:
	// System.ServiceModel.Security.X509CertificateValidationMode System.ServiceModel.Security.IssuedTokenServiceCredential::cert_verify_mode
	int32_t ___cert_verify_mode_0;
	// System.Collections.Generic.List`1<System.Security.Cryptography.X509Certificates.X509Certificate2> System.ServiceModel.Security.IssuedTokenServiceCredential::known_certs
	List_1_t2186123868 * ___known_certs_1;
	// System.Security.Cryptography.X509Certificates.X509RevocationMode System.ServiceModel.Security.IssuedTokenServiceCredential::revocation_mode
	int32_t ___revocation_mode_2;
	// System.Security.Cryptography.X509Certificates.StoreLocation System.ServiceModel.Security.IssuedTokenServiceCredential::store_location
	int32_t ___store_location_3;

public:
	inline static int32_t get_offset_of_cert_verify_mode_0() { return static_cast<int32_t>(offsetof(IssuedTokenServiceCredential_t922425120, ___cert_verify_mode_0)); }
	inline int32_t get_cert_verify_mode_0() const { return ___cert_verify_mode_0; }
	inline int32_t* get_address_of_cert_verify_mode_0() { return &___cert_verify_mode_0; }
	inline void set_cert_verify_mode_0(int32_t value)
	{
		___cert_verify_mode_0 = value;
	}

	inline static int32_t get_offset_of_known_certs_1() { return static_cast<int32_t>(offsetof(IssuedTokenServiceCredential_t922425120, ___known_certs_1)); }
	inline List_1_t2186123868 * get_known_certs_1() const { return ___known_certs_1; }
	inline List_1_t2186123868 ** get_address_of_known_certs_1() { return &___known_certs_1; }
	inline void set_known_certs_1(List_1_t2186123868 * value)
	{
		___known_certs_1 = value;
		Il2CppCodeGenWriteBarrier(&___known_certs_1, value);
	}

	inline static int32_t get_offset_of_revocation_mode_2() { return static_cast<int32_t>(offsetof(IssuedTokenServiceCredential_t922425120, ___revocation_mode_2)); }
	inline int32_t get_revocation_mode_2() const { return ___revocation_mode_2; }
	inline int32_t* get_address_of_revocation_mode_2() { return &___revocation_mode_2; }
	inline void set_revocation_mode_2(int32_t value)
	{
		___revocation_mode_2 = value;
	}

	inline static int32_t get_offset_of_store_location_3() { return static_cast<int32_t>(offsetof(IssuedTokenServiceCredential_t922425120, ___store_location_3)); }
	inline int32_t get_store_location_3() const { return ___store_location_3; }
	inline int32_t* get_address_of_store_location_3() { return &___store_location_3; }
	inline void set_store_location_3(int32_t value)
	{
		___store_location_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
