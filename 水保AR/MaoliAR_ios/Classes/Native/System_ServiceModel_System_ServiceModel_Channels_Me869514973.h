﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_System_ServiceModel_Channels_M3863555980.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.Message
struct  Message_t869514973  : public Il2CppObject
{
public:
	// System.Boolean System.ServiceModel.Channels.Message::disposed
	bool ___disposed_0;
	// System.String System.ServiceModel.Channels.Message::body_id
	String_t* ___body_id_1;
	// System.ServiceModel.Channels.MessageState System.ServiceModel.Channels.Message::___state
	int32_t ______state_2;

public:
	inline static int32_t get_offset_of_disposed_0() { return static_cast<int32_t>(offsetof(Message_t869514973, ___disposed_0)); }
	inline bool get_disposed_0() const { return ___disposed_0; }
	inline bool* get_address_of_disposed_0() { return &___disposed_0; }
	inline void set_disposed_0(bool value)
	{
		___disposed_0 = value;
	}

	inline static int32_t get_offset_of_body_id_1() { return static_cast<int32_t>(offsetof(Message_t869514973, ___body_id_1)); }
	inline String_t* get_body_id_1() const { return ___body_id_1; }
	inline String_t** get_address_of_body_id_1() { return &___body_id_1; }
	inline void set_body_id_1(String_t* value)
	{
		___body_id_1 = value;
		Il2CppCodeGenWriteBarrier(&___body_id_1, value);
	}

	inline static int32_t get_offset_of____state_2() { return static_cast<int32_t>(offsetof(Message_t869514973, ______state_2)); }
	inline int32_t get____state_2() const { return ______state_2; }
	inline int32_t* get_address_of____state_2() { return &______state_2; }
	inline void set____state_2(int32_t value)
	{
		______state_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
