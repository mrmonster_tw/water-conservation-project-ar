﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_Compilation_BaseCompiler69158820.h"
#include "mscorlib_System_Reflection_BindingFlags2721792723.h"

// System.Type
struct Type_t;
// System.Web.UI.TemplateControlParser
struct TemplateControlParser_t3072921516;
// System.Web.Compilation.ILocation
struct ILocation_t3961726794;
// System.ComponentModel.TypeConverter
struct TypeConverter_t2249118273;
// System.CodeDom.CodeVariableReferenceExpression
struct CodeVariableReferenceExpression_t2242020292;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Text.RegularExpressions.Regex
struct Regex_t3657309853;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.TemplateControlCompiler
struct  TemplateControlCompiler_t645890099  : public BaseCompiler_t69158820
{
public:
	// System.Web.UI.TemplateControlParser System.Web.Compilation.TemplateControlCompiler::parser
	TemplateControlParser_t3072921516 * ___parser_14;
	// System.Int32 System.Web.Compilation.TemplateControlCompiler::dataBoundAtts
	int32_t ___dataBoundAtts_15;
	// System.Web.Compilation.ILocation System.Web.Compilation.TemplateControlCompiler::currentLocation
	Il2CppObject * ___currentLocation_16;
	// System.Collections.Generic.List`1<System.String> System.Web.Compilation.TemplateControlCompiler::masterPageContentPlaceHolders
	List_1_t3319525431 * ___masterPageContentPlaceHolders_19;

public:
	inline static int32_t get_offset_of_parser_14() { return static_cast<int32_t>(offsetof(TemplateControlCompiler_t645890099, ___parser_14)); }
	inline TemplateControlParser_t3072921516 * get_parser_14() const { return ___parser_14; }
	inline TemplateControlParser_t3072921516 ** get_address_of_parser_14() { return &___parser_14; }
	inline void set_parser_14(TemplateControlParser_t3072921516 * value)
	{
		___parser_14 = value;
		Il2CppCodeGenWriteBarrier(&___parser_14, value);
	}

	inline static int32_t get_offset_of_dataBoundAtts_15() { return static_cast<int32_t>(offsetof(TemplateControlCompiler_t645890099, ___dataBoundAtts_15)); }
	inline int32_t get_dataBoundAtts_15() const { return ___dataBoundAtts_15; }
	inline int32_t* get_address_of_dataBoundAtts_15() { return &___dataBoundAtts_15; }
	inline void set_dataBoundAtts_15(int32_t value)
	{
		___dataBoundAtts_15 = value;
	}

	inline static int32_t get_offset_of_currentLocation_16() { return static_cast<int32_t>(offsetof(TemplateControlCompiler_t645890099, ___currentLocation_16)); }
	inline Il2CppObject * get_currentLocation_16() const { return ___currentLocation_16; }
	inline Il2CppObject ** get_address_of_currentLocation_16() { return &___currentLocation_16; }
	inline void set_currentLocation_16(Il2CppObject * value)
	{
		___currentLocation_16 = value;
		Il2CppCodeGenWriteBarrier(&___currentLocation_16, value);
	}

	inline static int32_t get_offset_of_masterPageContentPlaceHolders_19() { return static_cast<int32_t>(offsetof(TemplateControlCompiler_t645890099, ___masterPageContentPlaceHolders_19)); }
	inline List_1_t3319525431 * get_masterPageContentPlaceHolders_19() const { return ___masterPageContentPlaceHolders_19; }
	inline List_1_t3319525431 ** get_address_of_masterPageContentPlaceHolders_19() { return &___masterPageContentPlaceHolders_19; }
	inline void set_masterPageContentPlaceHolders_19(List_1_t3319525431 * value)
	{
		___masterPageContentPlaceHolders_19 = value;
		Il2CppCodeGenWriteBarrier(&___masterPageContentPlaceHolders_19, value);
	}
};

struct TemplateControlCompiler_t645890099_StaticFields
{
public:
	// System.Reflection.BindingFlags System.Web.Compilation.TemplateControlCompiler::noCaseFlags
	int32_t ___noCaseFlags_12;
	// System.Type System.Web.Compilation.TemplateControlCompiler::monoTypeType
	Type_t * ___monoTypeType_13;
	// System.ComponentModel.TypeConverter System.Web.Compilation.TemplateControlCompiler::colorConverter
	TypeConverter_t2249118273 * ___colorConverter_17;
	// System.CodeDom.CodeVariableReferenceExpression System.Web.Compilation.TemplateControlCompiler::ctrlVar
	CodeVariableReferenceExpression_t2242020292 * ___ctrlVar_18;
	// System.Text.RegularExpressions.Regex System.Web.Compilation.TemplateControlCompiler::startsWithBindRegex
	Regex_t3657309853 * ___startsWithBindRegex_20;
	// System.Text.RegularExpressions.Regex System.Web.Compilation.TemplateControlCompiler::bindRegex
	Regex_t3657309853 * ___bindRegex_21;
	// System.Text.RegularExpressions.Regex System.Web.Compilation.TemplateControlCompiler::bindRegexInValue
	Regex_t3657309853 * ___bindRegexInValue_22;
	// System.Text.RegularExpressions.Regex System.Web.Compilation.TemplateControlCompiler::evalRegexInValue
	Regex_t3657309853 * ___evalRegexInValue_23;

public:
	inline static int32_t get_offset_of_noCaseFlags_12() { return static_cast<int32_t>(offsetof(TemplateControlCompiler_t645890099_StaticFields, ___noCaseFlags_12)); }
	inline int32_t get_noCaseFlags_12() const { return ___noCaseFlags_12; }
	inline int32_t* get_address_of_noCaseFlags_12() { return &___noCaseFlags_12; }
	inline void set_noCaseFlags_12(int32_t value)
	{
		___noCaseFlags_12 = value;
	}

	inline static int32_t get_offset_of_monoTypeType_13() { return static_cast<int32_t>(offsetof(TemplateControlCompiler_t645890099_StaticFields, ___monoTypeType_13)); }
	inline Type_t * get_monoTypeType_13() const { return ___monoTypeType_13; }
	inline Type_t ** get_address_of_monoTypeType_13() { return &___monoTypeType_13; }
	inline void set_monoTypeType_13(Type_t * value)
	{
		___monoTypeType_13 = value;
		Il2CppCodeGenWriteBarrier(&___monoTypeType_13, value);
	}

	inline static int32_t get_offset_of_colorConverter_17() { return static_cast<int32_t>(offsetof(TemplateControlCompiler_t645890099_StaticFields, ___colorConverter_17)); }
	inline TypeConverter_t2249118273 * get_colorConverter_17() const { return ___colorConverter_17; }
	inline TypeConverter_t2249118273 ** get_address_of_colorConverter_17() { return &___colorConverter_17; }
	inline void set_colorConverter_17(TypeConverter_t2249118273 * value)
	{
		___colorConverter_17 = value;
		Il2CppCodeGenWriteBarrier(&___colorConverter_17, value);
	}

	inline static int32_t get_offset_of_ctrlVar_18() { return static_cast<int32_t>(offsetof(TemplateControlCompiler_t645890099_StaticFields, ___ctrlVar_18)); }
	inline CodeVariableReferenceExpression_t2242020292 * get_ctrlVar_18() const { return ___ctrlVar_18; }
	inline CodeVariableReferenceExpression_t2242020292 ** get_address_of_ctrlVar_18() { return &___ctrlVar_18; }
	inline void set_ctrlVar_18(CodeVariableReferenceExpression_t2242020292 * value)
	{
		___ctrlVar_18 = value;
		Il2CppCodeGenWriteBarrier(&___ctrlVar_18, value);
	}

	inline static int32_t get_offset_of_startsWithBindRegex_20() { return static_cast<int32_t>(offsetof(TemplateControlCompiler_t645890099_StaticFields, ___startsWithBindRegex_20)); }
	inline Regex_t3657309853 * get_startsWithBindRegex_20() const { return ___startsWithBindRegex_20; }
	inline Regex_t3657309853 ** get_address_of_startsWithBindRegex_20() { return &___startsWithBindRegex_20; }
	inline void set_startsWithBindRegex_20(Regex_t3657309853 * value)
	{
		___startsWithBindRegex_20 = value;
		Il2CppCodeGenWriteBarrier(&___startsWithBindRegex_20, value);
	}

	inline static int32_t get_offset_of_bindRegex_21() { return static_cast<int32_t>(offsetof(TemplateControlCompiler_t645890099_StaticFields, ___bindRegex_21)); }
	inline Regex_t3657309853 * get_bindRegex_21() const { return ___bindRegex_21; }
	inline Regex_t3657309853 ** get_address_of_bindRegex_21() { return &___bindRegex_21; }
	inline void set_bindRegex_21(Regex_t3657309853 * value)
	{
		___bindRegex_21 = value;
		Il2CppCodeGenWriteBarrier(&___bindRegex_21, value);
	}

	inline static int32_t get_offset_of_bindRegexInValue_22() { return static_cast<int32_t>(offsetof(TemplateControlCompiler_t645890099_StaticFields, ___bindRegexInValue_22)); }
	inline Regex_t3657309853 * get_bindRegexInValue_22() const { return ___bindRegexInValue_22; }
	inline Regex_t3657309853 ** get_address_of_bindRegexInValue_22() { return &___bindRegexInValue_22; }
	inline void set_bindRegexInValue_22(Regex_t3657309853 * value)
	{
		___bindRegexInValue_22 = value;
		Il2CppCodeGenWriteBarrier(&___bindRegexInValue_22, value);
	}

	inline static int32_t get_offset_of_evalRegexInValue_23() { return static_cast<int32_t>(offsetof(TemplateControlCompiler_t645890099_StaticFields, ___evalRegexInValue_23)); }
	inline Regex_t3657309853 * get_evalRegexInValue_23() const { return ___evalRegexInValue_23; }
	inline Regex_t3657309853 ** get_address_of_evalRegexInValue_23() { return &___evalRegexInValue_23; }
	inline void set_evalRegexInValue_23(Regex_t3657309853 * value)
	{
		___evalRegexInValue_23 = value;
		Il2CppCodeGenWriteBarrier(&___evalRegexInValue_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
