﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"

// System.Web.Compilation.BuildProvider
struct BuildProvider_t3736381005;
// System.CodeDom.CodeCompileUnit
struct CodeCompileUnit_t2527618915;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.AssemblyBuilder/CodeUnit
struct  CodeUnit_t1519973372 
{
public:
	// System.Web.Compilation.BuildProvider System.Web.Compilation.AssemblyBuilder/CodeUnit::BuildProvider
	BuildProvider_t3736381005 * ___BuildProvider_0;
	// System.CodeDom.CodeCompileUnit System.Web.Compilation.AssemblyBuilder/CodeUnit::Unit
	CodeCompileUnit_t2527618915 * ___Unit_1;

public:
	inline static int32_t get_offset_of_BuildProvider_0() { return static_cast<int32_t>(offsetof(CodeUnit_t1519973372, ___BuildProvider_0)); }
	inline BuildProvider_t3736381005 * get_BuildProvider_0() const { return ___BuildProvider_0; }
	inline BuildProvider_t3736381005 ** get_address_of_BuildProvider_0() { return &___BuildProvider_0; }
	inline void set_BuildProvider_0(BuildProvider_t3736381005 * value)
	{
		___BuildProvider_0 = value;
		Il2CppCodeGenWriteBarrier(&___BuildProvider_0, value);
	}

	inline static int32_t get_offset_of_Unit_1() { return static_cast<int32_t>(offsetof(CodeUnit_t1519973372, ___Unit_1)); }
	inline CodeCompileUnit_t2527618915 * get_Unit_1() const { return ___Unit_1; }
	inline CodeCompileUnit_t2527618915 ** get_address_of_Unit_1() { return &___Unit_1; }
	inline void set_Unit_1(CodeCompileUnit_t2527618915 * value)
	{
		___Unit_1 = value;
		Il2CppCodeGenWriteBarrier(&___Unit_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Web.Compilation.AssemblyBuilder/CodeUnit
struct CodeUnit_t1519973372_marshaled_pinvoke
{
	BuildProvider_t3736381005 * ___BuildProvider_0;
	CodeCompileUnit_t2527618915 * ___Unit_1;
};
// Native definition for COM marshalling of System.Web.Compilation.AssemblyBuilder/CodeUnit
struct CodeUnit_t1519973372_marshaled_com
{
	BuildProvider_t3736381005 * ___BuildProvider_0;
	CodeCompileUnit_t2527618915 * ___Unit_1;
};
