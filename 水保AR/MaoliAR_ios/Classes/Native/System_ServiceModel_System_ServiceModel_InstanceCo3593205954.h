﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Co518829156.h"

// System.ServiceModel.ServiceHostBase
struct ServiceHostBase_t3741910535;
// System.Object
struct Il2CppObject;
// System.ServiceModel.Dispatcher.InstanceBehavior
struct InstanceBehavior_t2611969614;
// System.ServiceModel.Dispatcher.InstanceContextIdleCallback
struct InstanceContextIdleCallback_t4033177082;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.InstanceContext
struct  InstanceContext_t3593205954  : public CommunicationObject_t518829156
{
public:
	// System.ServiceModel.ServiceHostBase System.ServiceModel.InstanceContext::host
	ServiceHostBase_t3741910535 * ___host_9;
	// System.Object System.ServiceModel.InstanceContext::implementation
	Il2CppObject * ___implementation_10;
	// System.ServiceModel.Dispatcher.InstanceBehavior System.ServiceModel.InstanceContext::_behavior
	InstanceBehavior_t2611969614 * ____behavior_11;
	// System.Boolean System.ServiceModel.InstanceContext::is_user_instance_provider
	bool ___is_user_instance_provider_12;
	// System.Boolean System.ServiceModel.InstanceContext::is_user_context_provider
	bool ___is_user_context_provider_13;

public:
	inline static int32_t get_offset_of_host_9() { return static_cast<int32_t>(offsetof(InstanceContext_t3593205954, ___host_9)); }
	inline ServiceHostBase_t3741910535 * get_host_9() const { return ___host_9; }
	inline ServiceHostBase_t3741910535 ** get_address_of_host_9() { return &___host_9; }
	inline void set_host_9(ServiceHostBase_t3741910535 * value)
	{
		___host_9 = value;
		Il2CppCodeGenWriteBarrier(&___host_9, value);
	}

	inline static int32_t get_offset_of_implementation_10() { return static_cast<int32_t>(offsetof(InstanceContext_t3593205954, ___implementation_10)); }
	inline Il2CppObject * get_implementation_10() const { return ___implementation_10; }
	inline Il2CppObject ** get_address_of_implementation_10() { return &___implementation_10; }
	inline void set_implementation_10(Il2CppObject * value)
	{
		___implementation_10 = value;
		Il2CppCodeGenWriteBarrier(&___implementation_10, value);
	}

	inline static int32_t get_offset_of__behavior_11() { return static_cast<int32_t>(offsetof(InstanceContext_t3593205954, ____behavior_11)); }
	inline InstanceBehavior_t2611969614 * get__behavior_11() const { return ____behavior_11; }
	inline InstanceBehavior_t2611969614 ** get_address_of__behavior_11() { return &____behavior_11; }
	inline void set__behavior_11(InstanceBehavior_t2611969614 * value)
	{
		____behavior_11 = value;
		Il2CppCodeGenWriteBarrier(&____behavior_11, value);
	}

	inline static int32_t get_offset_of_is_user_instance_provider_12() { return static_cast<int32_t>(offsetof(InstanceContext_t3593205954, ___is_user_instance_provider_12)); }
	inline bool get_is_user_instance_provider_12() const { return ___is_user_instance_provider_12; }
	inline bool* get_address_of_is_user_instance_provider_12() { return &___is_user_instance_provider_12; }
	inline void set_is_user_instance_provider_12(bool value)
	{
		___is_user_instance_provider_12 = value;
	}

	inline static int32_t get_offset_of_is_user_context_provider_13() { return static_cast<int32_t>(offsetof(InstanceContext_t3593205954, ___is_user_context_provider_13)); }
	inline bool get_is_user_context_provider_13() const { return ___is_user_context_provider_13; }
	inline bool* get_address_of_is_user_context_provider_13() { return &___is_user_context_provider_13; }
	inline void set_is_user_context_provider_13(bool value)
	{
		___is_user_context_provider_13 = value;
	}
};

struct InstanceContext_t3593205954_StaticFields
{
public:
	// System.ServiceModel.Dispatcher.InstanceContextIdleCallback System.ServiceModel.InstanceContext::idle_callback
	InstanceContextIdleCallback_t4033177082 * ___idle_callback_14;

public:
	inline static int32_t get_offset_of_idle_callback_14() { return static_cast<int32_t>(offsetof(InstanceContext_t3593205954_StaticFields, ___idle_callback_14)); }
	inline InstanceContextIdleCallback_t4033177082 * get_idle_callback_14() const { return ___idle_callback_14; }
	inline InstanceContextIdleCallback_t4033177082 ** get_address_of_idle_callback_14() { return &___idle_callback_14; }
	inline void set_idle_callback_14(InstanceContextIdleCallback_t4033177082 * value)
	{
		___idle_callback_14 = value;
		Il2CppCodeGenWriteBarrier(&___idle_callback_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
