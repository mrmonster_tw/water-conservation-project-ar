﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_ChannelFac2688374905.h"

// System.ServiceModel.InstanceContext
struct InstanceContext_t3593205954;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.DuplexChannelFactory`1<System.ServiceModel.Channels.PeerDuplexChannel/IPeerConnectorClient>
struct  DuplexChannelFactory_1_t3721828160  : public ChannelFactory_1_t2688374905
{
public:
	// System.ServiceModel.InstanceContext System.ServiceModel.DuplexChannelFactory`1::callback_instance
	InstanceContext_t3593205954 * ___callback_instance_12;

public:
	inline static int32_t get_offset_of_callback_instance_12() { return static_cast<int32_t>(offsetof(DuplexChannelFactory_1_t3721828160, ___callback_instance_12)); }
	inline InstanceContext_t3593205954 * get_callback_instance_12() const { return ___callback_instance_12; }
	inline InstanceContext_t3593205954 ** get_address_of_callback_instance_12() { return &___callback_instance_12; }
	inline void set_callback_instance_12(InstanceContext_t3593205954 * value)
	{
		___callback_instance_12 = value;
		Il2CppCodeGenWriteBarrier(&___callback_instance_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
