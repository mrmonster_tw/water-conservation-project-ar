﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Operations_XslCompiledElemen50593777.h"

// System.Xml.XPath.XPathExpression
struct XPathExpression_t1723793351;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslValueOf
struct  XslValueOf_t3484759627  : public XslCompiledElement_t50593777
{
public:
	// System.Xml.XPath.XPathExpression Mono.Xml.Xsl.Operations.XslValueOf::select
	XPathExpression_t1723793351 * ___select_3;
	// System.Boolean Mono.Xml.Xsl.Operations.XslValueOf::disableOutputEscaping
	bool ___disableOutputEscaping_4;

public:
	inline static int32_t get_offset_of_select_3() { return static_cast<int32_t>(offsetof(XslValueOf_t3484759627, ___select_3)); }
	inline XPathExpression_t1723793351 * get_select_3() const { return ___select_3; }
	inline XPathExpression_t1723793351 ** get_address_of_select_3() { return &___select_3; }
	inline void set_select_3(XPathExpression_t1723793351 * value)
	{
		___select_3 = value;
		Il2CppCodeGenWriteBarrier(&___select_3, value);
	}

	inline static int32_t get_offset_of_disableOutputEscaping_4() { return static_cast<int32_t>(offsetof(XslValueOf_t3484759627, ___disableOutputEscaping_4)); }
	inline bool get_disableOutputEscaping_4() const { return ___disableOutputEscaping_4; }
	inline bool* get_address_of_disableOutputEscaping_4() { return &___disableOutputEscaping_4; }
	inline void set_disableOutputEscaping_4(bool value)
	{
		___disableOutputEscaping_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
