﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Drawing.Icon/BitmapInfoHeader
struct  BitmapInfoHeader_t2752024591 
{
public:
	// System.UInt32 System.Drawing.Icon/BitmapInfoHeader::biSize
	uint32_t ___biSize_0;
	// System.Int32 System.Drawing.Icon/BitmapInfoHeader::biWidth
	int32_t ___biWidth_1;
	// System.Int32 System.Drawing.Icon/BitmapInfoHeader::biHeight
	int32_t ___biHeight_2;
	// System.UInt16 System.Drawing.Icon/BitmapInfoHeader::biPlanes
	uint16_t ___biPlanes_3;
	// System.UInt16 System.Drawing.Icon/BitmapInfoHeader::biBitCount
	uint16_t ___biBitCount_4;
	// System.UInt32 System.Drawing.Icon/BitmapInfoHeader::biCompression
	uint32_t ___biCompression_5;
	// System.UInt32 System.Drawing.Icon/BitmapInfoHeader::biSizeImage
	uint32_t ___biSizeImage_6;
	// System.Int32 System.Drawing.Icon/BitmapInfoHeader::biXPelsPerMeter
	int32_t ___biXPelsPerMeter_7;
	// System.Int32 System.Drawing.Icon/BitmapInfoHeader::biYPelsPerMeter
	int32_t ___biYPelsPerMeter_8;
	// System.UInt32 System.Drawing.Icon/BitmapInfoHeader::biClrUsed
	uint32_t ___biClrUsed_9;
	// System.UInt32 System.Drawing.Icon/BitmapInfoHeader::biClrImportant
	uint32_t ___biClrImportant_10;

public:
	inline static int32_t get_offset_of_biSize_0() { return static_cast<int32_t>(offsetof(BitmapInfoHeader_t2752024591, ___biSize_0)); }
	inline uint32_t get_biSize_0() const { return ___biSize_0; }
	inline uint32_t* get_address_of_biSize_0() { return &___biSize_0; }
	inline void set_biSize_0(uint32_t value)
	{
		___biSize_0 = value;
	}

	inline static int32_t get_offset_of_biWidth_1() { return static_cast<int32_t>(offsetof(BitmapInfoHeader_t2752024591, ___biWidth_1)); }
	inline int32_t get_biWidth_1() const { return ___biWidth_1; }
	inline int32_t* get_address_of_biWidth_1() { return &___biWidth_1; }
	inline void set_biWidth_1(int32_t value)
	{
		___biWidth_1 = value;
	}

	inline static int32_t get_offset_of_biHeight_2() { return static_cast<int32_t>(offsetof(BitmapInfoHeader_t2752024591, ___biHeight_2)); }
	inline int32_t get_biHeight_2() const { return ___biHeight_2; }
	inline int32_t* get_address_of_biHeight_2() { return &___biHeight_2; }
	inline void set_biHeight_2(int32_t value)
	{
		___biHeight_2 = value;
	}

	inline static int32_t get_offset_of_biPlanes_3() { return static_cast<int32_t>(offsetof(BitmapInfoHeader_t2752024591, ___biPlanes_3)); }
	inline uint16_t get_biPlanes_3() const { return ___biPlanes_3; }
	inline uint16_t* get_address_of_biPlanes_3() { return &___biPlanes_3; }
	inline void set_biPlanes_3(uint16_t value)
	{
		___biPlanes_3 = value;
	}

	inline static int32_t get_offset_of_biBitCount_4() { return static_cast<int32_t>(offsetof(BitmapInfoHeader_t2752024591, ___biBitCount_4)); }
	inline uint16_t get_biBitCount_4() const { return ___biBitCount_4; }
	inline uint16_t* get_address_of_biBitCount_4() { return &___biBitCount_4; }
	inline void set_biBitCount_4(uint16_t value)
	{
		___biBitCount_4 = value;
	}

	inline static int32_t get_offset_of_biCompression_5() { return static_cast<int32_t>(offsetof(BitmapInfoHeader_t2752024591, ___biCompression_5)); }
	inline uint32_t get_biCompression_5() const { return ___biCompression_5; }
	inline uint32_t* get_address_of_biCompression_5() { return &___biCompression_5; }
	inline void set_biCompression_5(uint32_t value)
	{
		___biCompression_5 = value;
	}

	inline static int32_t get_offset_of_biSizeImage_6() { return static_cast<int32_t>(offsetof(BitmapInfoHeader_t2752024591, ___biSizeImage_6)); }
	inline uint32_t get_biSizeImage_6() const { return ___biSizeImage_6; }
	inline uint32_t* get_address_of_biSizeImage_6() { return &___biSizeImage_6; }
	inline void set_biSizeImage_6(uint32_t value)
	{
		___biSizeImage_6 = value;
	}

	inline static int32_t get_offset_of_biXPelsPerMeter_7() { return static_cast<int32_t>(offsetof(BitmapInfoHeader_t2752024591, ___biXPelsPerMeter_7)); }
	inline int32_t get_biXPelsPerMeter_7() const { return ___biXPelsPerMeter_7; }
	inline int32_t* get_address_of_biXPelsPerMeter_7() { return &___biXPelsPerMeter_7; }
	inline void set_biXPelsPerMeter_7(int32_t value)
	{
		___biXPelsPerMeter_7 = value;
	}

	inline static int32_t get_offset_of_biYPelsPerMeter_8() { return static_cast<int32_t>(offsetof(BitmapInfoHeader_t2752024591, ___biYPelsPerMeter_8)); }
	inline int32_t get_biYPelsPerMeter_8() const { return ___biYPelsPerMeter_8; }
	inline int32_t* get_address_of_biYPelsPerMeter_8() { return &___biYPelsPerMeter_8; }
	inline void set_biYPelsPerMeter_8(int32_t value)
	{
		___biYPelsPerMeter_8 = value;
	}

	inline static int32_t get_offset_of_biClrUsed_9() { return static_cast<int32_t>(offsetof(BitmapInfoHeader_t2752024591, ___biClrUsed_9)); }
	inline uint32_t get_biClrUsed_9() const { return ___biClrUsed_9; }
	inline uint32_t* get_address_of_biClrUsed_9() { return &___biClrUsed_9; }
	inline void set_biClrUsed_9(uint32_t value)
	{
		___biClrUsed_9 = value;
	}

	inline static int32_t get_offset_of_biClrImportant_10() { return static_cast<int32_t>(offsetof(BitmapInfoHeader_t2752024591, ___biClrImportant_10)); }
	inline uint32_t get_biClrImportant_10() const { return ___biClrImportant_10; }
	inline uint32_t* get_address_of_biClrImportant_10() { return &___biClrImportant_10; }
	inline void set_biClrImportant_10(uint32_t value)
	{
		___biClrImportant_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
