﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Collections.ArrayList
struct ArrayList_t2718874744;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.TagAttributes
struct  TagAttributes_t1165003511  : public Il2CppObject
{
public:
	// System.Collections.Hashtable System.Web.Compilation.TagAttributes::atts_hash
	Hashtable_t1853889766 * ___atts_hash_0;
	// System.Collections.Hashtable System.Web.Compilation.TagAttributes::tmp_hash
	Hashtable_t1853889766 * ___tmp_hash_1;
	// System.Collections.ArrayList System.Web.Compilation.TagAttributes::keys
	ArrayList_t2718874744 * ___keys_2;
	// System.Collections.ArrayList System.Web.Compilation.TagAttributes::values
	ArrayList_t2718874744 * ___values_3;
	// System.Boolean System.Web.Compilation.TagAttributes::got_hashed
	bool ___got_hashed_4;

public:
	inline static int32_t get_offset_of_atts_hash_0() { return static_cast<int32_t>(offsetof(TagAttributes_t1165003511, ___atts_hash_0)); }
	inline Hashtable_t1853889766 * get_atts_hash_0() const { return ___atts_hash_0; }
	inline Hashtable_t1853889766 ** get_address_of_atts_hash_0() { return &___atts_hash_0; }
	inline void set_atts_hash_0(Hashtable_t1853889766 * value)
	{
		___atts_hash_0 = value;
		Il2CppCodeGenWriteBarrier(&___atts_hash_0, value);
	}

	inline static int32_t get_offset_of_tmp_hash_1() { return static_cast<int32_t>(offsetof(TagAttributes_t1165003511, ___tmp_hash_1)); }
	inline Hashtable_t1853889766 * get_tmp_hash_1() const { return ___tmp_hash_1; }
	inline Hashtable_t1853889766 ** get_address_of_tmp_hash_1() { return &___tmp_hash_1; }
	inline void set_tmp_hash_1(Hashtable_t1853889766 * value)
	{
		___tmp_hash_1 = value;
		Il2CppCodeGenWriteBarrier(&___tmp_hash_1, value);
	}

	inline static int32_t get_offset_of_keys_2() { return static_cast<int32_t>(offsetof(TagAttributes_t1165003511, ___keys_2)); }
	inline ArrayList_t2718874744 * get_keys_2() const { return ___keys_2; }
	inline ArrayList_t2718874744 ** get_address_of_keys_2() { return &___keys_2; }
	inline void set_keys_2(ArrayList_t2718874744 * value)
	{
		___keys_2 = value;
		Il2CppCodeGenWriteBarrier(&___keys_2, value);
	}

	inline static int32_t get_offset_of_values_3() { return static_cast<int32_t>(offsetof(TagAttributes_t1165003511, ___values_3)); }
	inline ArrayList_t2718874744 * get_values_3() const { return ___values_3; }
	inline ArrayList_t2718874744 ** get_address_of_values_3() { return &___values_3; }
	inline void set_values_3(ArrayList_t2718874744 * value)
	{
		___values_3 = value;
		Il2CppCodeGenWriteBarrier(&___values_3, value);
	}

	inline static int32_t get_offset_of_got_hashed_4() { return static_cast<int32_t>(offsetof(TagAttributes_t1165003511, ___got_hashed_4)); }
	inline bool get_got_hashed_4() const { return ___got_hashed_4; }
	inline bool* get_address_of_got_hashed_4() { return &___got_hashed_4; }
	inline void set_got_hashed_4(bool value)
	{
		___got_hashed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
