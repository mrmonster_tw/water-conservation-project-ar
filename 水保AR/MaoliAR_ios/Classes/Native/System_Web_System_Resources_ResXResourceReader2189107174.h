﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.IO.Stream
struct Stream_t1273022909;
// System.IO.TextReader
struct TextReader_t283511965;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.ComponentModel.Design.ITypeResolutionService
struct ITypeResolutionService_t3189425787;
// System.Xml.XmlTextReader
struct XmlTextReader_t4233384356;
// System.Reflection.AssemblyName[]
struct AssemblyNameU5BU5D_t4133974615;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Resources.ResXResourceReader
struct  ResXResourceReader_t2189107174  : public Il2CppObject
{
public:
	// System.String System.Resources.ResXResourceReader::fileName
	String_t* ___fileName_0;
	// System.IO.Stream System.Resources.ResXResourceReader::stream
	Stream_t1273022909 * ___stream_1;
	// System.IO.TextReader System.Resources.ResXResourceReader::reader
	TextReader_t283511965 * ___reader_2;
	// System.Collections.Hashtable System.Resources.ResXResourceReader::hasht
	Hashtable_t1853889766 * ___hasht_3;
	// System.ComponentModel.Design.ITypeResolutionService System.Resources.ResXResourceReader::typeresolver
	Il2CppObject * ___typeresolver_4;
	// System.Xml.XmlTextReader System.Resources.ResXResourceReader::xmlReader
	XmlTextReader_t4233384356 * ___xmlReader_5;
	// System.String System.Resources.ResXResourceReader::basepath
	String_t* ___basepath_6;
	// System.Boolean System.Resources.ResXResourceReader::useResXDataNodes
	bool ___useResXDataNodes_7;
	// System.Reflection.AssemblyName[] System.Resources.ResXResourceReader::assemblyNames
	AssemblyNameU5BU5D_t4133974615* ___assemblyNames_8;
	// System.Collections.Hashtable System.Resources.ResXResourceReader::hashtm
	Hashtable_t1853889766 * ___hashtm_9;

public:
	inline static int32_t get_offset_of_fileName_0() { return static_cast<int32_t>(offsetof(ResXResourceReader_t2189107174, ___fileName_0)); }
	inline String_t* get_fileName_0() const { return ___fileName_0; }
	inline String_t** get_address_of_fileName_0() { return &___fileName_0; }
	inline void set_fileName_0(String_t* value)
	{
		___fileName_0 = value;
		Il2CppCodeGenWriteBarrier(&___fileName_0, value);
	}

	inline static int32_t get_offset_of_stream_1() { return static_cast<int32_t>(offsetof(ResXResourceReader_t2189107174, ___stream_1)); }
	inline Stream_t1273022909 * get_stream_1() const { return ___stream_1; }
	inline Stream_t1273022909 ** get_address_of_stream_1() { return &___stream_1; }
	inline void set_stream_1(Stream_t1273022909 * value)
	{
		___stream_1 = value;
		Il2CppCodeGenWriteBarrier(&___stream_1, value);
	}

	inline static int32_t get_offset_of_reader_2() { return static_cast<int32_t>(offsetof(ResXResourceReader_t2189107174, ___reader_2)); }
	inline TextReader_t283511965 * get_reader_2() const { return ___reader_2; }
	inline TextReader_t283511965 ** get_address_of_reader_2() { return &___reader_2; }
	inline void set_reader_2(TextReader_t283511965 * value)
	{
		___reader_2 = value;
		Il2CppCodeGenWriteBarrier(&___reader_2, value);
	}

	inline static int32_t get_offset_of_hasht_3() { return static_cast<int32_t>(offsetof(ResXResourceReader_t2189107174, ___hasht_3)); }
	inline Hashtable_t1853889766 * get_hasht_3() const { return ___hasht_3; }
	inline Hashtable_t1853889766 ** get_address_of_hasht_3() { return &___hasht_3; }
	inline void set_hasht_3(Hashtable_t1853889766 * value)
	{
		___hasht_3 = value;
		Il2CppCodeGenWriteBarrier(&___hasht_3, value);
	}

	inline static int32_t get_offset_of_typeresolver_4() { return static_cast<int32_t>(offsetof(ResXResourceReader_t2189107174, ___typeresolver_4)); }
	inline Il2CppObject * get_typeresolver_4() const { return ___typeresolver_4; }
	inline Il2CppObject ** get_address_of_typeresolver_4() { return &___typeresolver_4; }
	inline void set_typeresolver_4(Il2CppObject * value)
	{
		___typeresolver_4 = value;
		Il2CppCodeGenWriteBarrier(&___typeresolver_4, value);
	}

	inline static int32_t get_offset_of_xmlReader_5() { return static_cast<int32_t>(offsetof(ResXResourceReader_t2189107174, ___xmlReader_5)); }
	inline XmlTextReader_t4233384356 * get_xmlReader_5() const { return ___xmlReader_5; }
	inline XmlTextReader_t4233384356 ** get_address_of_xmlReader_5() { return &___xmlReader_5; }
	inline void set_xmlReader_5(XmlTextReader_t4233384356 * value)
	{
		___xmlReader_5 = value;
		Il2CppCodeGenWriteBarrier(&___xmlReader_5, value);
	}

	inline static int32_t get_offset_of_basepath_6() { return static_cast<int32_t>(offsetof(ResXResourceReader_t2189107174, ___basepath_6)); }
	inline String_t* get_basepath_6() const { return ___basepath_6; }
	inline String_t** get_address_of_basepath_6() { return &___basepath_6; }
	inline void set_basepath_6(String_t* value)
	{
		___basepath_6 = value;
		Il2CppCodeGenWriteBarrier(&___basepath_6, value);
	}

	inline static int32_t get_offset_of_useResXDataNodes_7() { return static_cast<int32_t>(offsetof(ResXResourceReader_t2189107174, ___useResXDataNodes_7)); }
	inline bool get_useResXDataNodes_7() const { return ___useResXDataNodes_7; }
	inline bool* get_address_of_useResXDataNodes_7() { return &___useResXDataNodes_7; }
	inline void set_useResXDataNodes_7(bool value)
	{
		___useResXDataNodes_7 = value;
	}

	inline static int32_t get_offset_of_assemblyNames_8() { return static_cast<int32_t>(offsetof(ResXResourceReader_t2189107174, ___assemblyNames_8)); }
	inline AssemblyNameU5BU5D_t4133974615* get_assemblyNames_8() const { return ___assemblyNames_8; }
	inline AssemblyNameU5BU5D_t4133974615** get_address_of_assemblyNames_8() { return &___assemblyNames_8; }
	inline void set_assemblyNames_8(AssemblyNameU5BU5D_t4133974615* value)
	{
		___assemblyNames_8 = value;
		Il2CppCodeGenWriteBarrier(&___assemblyNames_8, value);
	}

	inline static int32_t get_offset_of_hashtm_9() { return static_cast<int32_t>(offsetof(ResXResourceReader_t2189107174, ___hashtm_9)); }
	inline Hashtable_t1853889766 * get_hashtm_9() const { return ___hashtm_9; }
	inline Hashtable_t1853889766 ** get_address_of_hashtm_9() { return &___hashtm_9; }
	inline void set_hashtm_9(Hashtable_t1853889766 * value)
	{
		___hashtm_9 = value;
		Il2CppCodeGenWriteBarrier(&___hashtm_9, value);
	}
};

struct ResXResourceReader_t2189107174_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Resources.ResXResourceReader::<>f__switch$map0
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map0_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_10() { return static_cast<int32_t>(offsetof(ResXResourceReader_t2189107174_StaticFields, ___U3CU3Ef__switchU24map0_10)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map0_10() const { return ___U3CU3Ef__switchU24map0_10; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map0_10() { return &___U3CU3Ef__switchU24map0_10; }
	inline void set_U3CU3Ef__switchU24map0_10(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map0_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map0_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
