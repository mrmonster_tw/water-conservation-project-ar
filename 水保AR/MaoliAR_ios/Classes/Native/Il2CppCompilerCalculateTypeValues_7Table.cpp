﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfo3261134217.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_AsyncRe4194309572.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_CallCon3349742090.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ClientC4064115021.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_Constru4011594745.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_Construc686578562.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_EnvoyTe3654193516.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_Header549724581.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_Logical3342013719.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_CallCon2260963392.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCa861078140.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCa605791082.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDi207894204.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodD2516729552.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodR2551046119.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_MonoMet2807636944.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_CallTyp3372275391.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_OneWayAt936468379.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_Remotin2472351973.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_Remotin2834579653.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ObjRefS3860276170.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ReturnM1376985608.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ServerCon16296004.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ServerO4187339465.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_StackBui883411942.h"
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapAttri411004526.h"
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapFiel1164342470.h"
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapMeth1935009310.h"
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapPara1445364003.h"
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapType1750930817.h"
#include "mscorlib_System_Runtime_Remoting_Proxies_Transparen431418284.h"
#include "mscorlib_System_Runtime_Remoting_Proxies_RealProxy2312050253.h"
#include "mscorlib_System_Runtime_Remoting_Proxies_RemotingP2444335113.h"
#include "mscorlib_System_Runtime_Remoting_Proxies_ProxyAttri455382724.h"
#include "mscorlib_System_Runtime_Remoting_Services_TrackingS409076583.h"
#include "mscorlib_System_Runtime_Serialization_FormatterCon2760117746.h"
#include "mscorlib_System_Runtime_Serialization_FormatterServ305532257.h"
#include "mscorlib_System_Runtime_Serialization_ObjectIDGene1260826161.h"
#include "mscorlib_System_Runtime_Serialization_ObjectIDGene1261591096.h"
#include "mscorlib_System_Runtime_Serialization_ObjectManage1653064325.h"
#include "mscorlib_System_Runtime_Serialization_BaseFixupRec3185653456.h"
#include "mscorlib_System_Runtime_Serialization_ArrayFixupRe4271256269.h"
#include "mscorlib_System_Runtime_Serialization_MultiArrayFi2501518442.h"
#include "mscorlib_System_Runtime_Serialization_FixupRecord4176629420.h"
#include "mscorlib_System_Runtime_Serialization_DelayedFixup2425570624.h"
#include "mscorlib_System_Runtime_Serialization_ObjectRecord2117702867.h"
#include "mscorlib_System_Runtime_Serialization_ObjectRecord1187467272.h"
#include "mscorlib_System_Runtime_Serialization_OnDeserializ1335880599.h"
#include "mscorlib_System_Runtime_Serialization_OnDeserializi338753086.h"
#include "mscorlib_System_Runtime_Serialization_OnSerialized2595932830.h"
#include "mscorlib_System_Runtime_Serialization_OnSerializin2580696919.h"
#include "mscorlib_System_Runtime_Serialization_Serialization274213469.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2570604627.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio3280319253.h"
#include "mscorlib_System_Runtime_Serialization_Serialization648286436.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio3941511869.h"
#include "mscorlib_System_Runtime_Serialization_Serialization950877179.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2232395945.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio4107654543.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio3516576679.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon3711869237.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon3580100459.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Fo868039825.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_F3400733584.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Ty977535029.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3197753202.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3059247396.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B4114212191.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3541821701.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1925520788.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Bi773701450.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1096194562.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1940957392.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3139523260.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1685365060.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2187352167.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3885853035.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2796913665.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Bi976220688.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2968406231.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B4077873660.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Bi457758949.h"
#include "mscorlib_System_Security_AllowPartiallyTrustedCall1828959166.h"
#include "mscorlib_System_Security_CodeAccessPermission2681295399.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize700 = { sizeof (ArgInfo_t3261134217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable700[3] = 
{
	ArgInfo_t3261134217::get_offset_of__paramMap_0(),
	ArgInfo_t3261134217::get_offset_of__inoutArgCount_1(),
	ArgInfo_t3261134217::get_offset_of__method_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize701 = { sizeof (AsyncResult_t4194309572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable701[15] = 
{
	AsyncResult_t4194309572::get_offset_of_async_state_0(),
	AsyncResult_t4194309572::get_offset_of_handle_1(),
	AsyncResult_t4194309572::get_offset_of_async_delegate_2(),
	AsyncResult_t4194309572::get_offset_of_data_3(),
	AsyncResult_t4194309572::get_offset_of_object_data_4(),
	AsyncResult_t4194309572::get_offset_of_sync_completed_5(),
	AsyncResult_t4194309572::get_offset_of_completed_6(),
	AsyncResult_t4194309572::get_offset_of_endinvoke_called_7(),
	AsyncResult_t4194309572::get_offset_of_async_callback_8(),
	AsyncResult_t4194309572::get_offset_of_current_9(),
	AsyncResult_t4194309572::get_offset_of_original_10(),
	AsyncResult_t4194309572::get_offset_of_gchandle_11(),
	AsyncResult_t4194309572::get_offset_of_call_message_12(),
	AsyncResult_t4194309572::get_offset_of_message_ctrl_13(),
	AsyncResult_t4194309572::get_offset_of_reply_message_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize702 = { sizeof (CallContext_t3349742090), -1, 0, sizeof(CallContext_t3349742090_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable702[1] = 
{
	THREAD_STATIC_FIELD_OFFSET,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize703 = { sizeof (ClientContextTerminatorSink_t4064115021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable703[1] = 
{
	ClientContextTerminatorSink_t4064115021::get_offset_of__context_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize704 = { sizeof (ConstructionCall_t4011594745), -1, sizeof(ConstructionCall_t4011594745_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable704[7] = 
{
	ConstructionCall_t4011594745::get_offset_of__activator_11(),
	ConstructionCall_t4011594745::get_offset_of__activationAttributes_12(),
	ConstructionCall_t4011594745::get_offset_of__contextProperties_13(),
	ConstructionCall_t4011594745::get_offset_of__activationType_14(),
	ConstructionCall_t4011594745::get_offset_of__activationTypeName_15(),
	ConstructionCall_t4011594745::get_offset_of__isContextOk_16(),
	ConstructionCall_t4011594745_StaticFields::get_offset_of_U3CU3Ef__switchU24map25_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize705 = { sizeof (ConstructionCallDictionary_t686578562), -1, sizeof(ConstructionCallDictionary_t686578562_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable705[3] = 
{
	ConstructionCallDictionary_t686578562_StaticFields::get_offset_of_InternalKeys_6(),
	ConstructionCallDictionary_t686578562_StaticFields::get_offset_of_U3CU3Ef__switchU24map28_7(),
	ConstructionCallDictionary_t686578562_StaticFields::get_offset_of_U3CU3Ef__switchU24map29_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize706 = { sizeof (EnvoyTerminatorSink_t3654193516), -1, sizeof(EnvoyTerminatorSink_t3654193516_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable706[1] = 
{
	EnvoyTerminatorSink_t3654193516_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize707 = { sizeof (Header_t549724581), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable707[4] = 
{
	Header_t549724581::get_offset_of_HeaderNamespace_0(),
	Header_t549724581::get_offset_of_MustUnderstand_1(),
	Header_t549724581::get_offset_of_Name_2(),
	Header_t549724581::get_offset_of_Value_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize708 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize709 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize710 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize711 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize712 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize713 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize714 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize715 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize716 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize717 = { sizeof (LogicalCallContext_t3342013719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable717[2] = 
{
	LogicalCallContext_t3342013719::get_offset_of__data_0(),
	LogicalCallContext_t3342013719::get_offset_of__remotingData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize718 = { sizeof (CallContextRemotingData_t2260963392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable718[1] = 
{
	CallContextRemotingData_t2260963392::get_offset_of__logicalCallID_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize719 = { sizeof (MethodCall_t861078140), -1, sizeof(MethodCall_t861078140_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable719[11] = 
{
	MethodCall_t861078140::get_offset_of__uri_0(),
	MethodCall_t861078140::get_offset_of__typeName_1(),
	MethodCall_t861078140::get_offset_of__methodName_2(),
	MethodCall_t861078140::get_offset_of__args_3(),
	MethodCall_t861078140::get_offset_of__methodSignature_4(),
	MethodCall_t861078140::get_offset_of__methodBase_5(),
	MethodCall_t861078140::get_offset_of__callContext_6(),
	MethodCall_t861078140::get_offset_of__genericArguments_7(),
	MethodCall_t861078140::get_offset_of_ExternalProperties_8(),
	MethodCall_t861078140::get_offset_of_InternalProperties_9(),
	MethodCall_t861078140_StaticFields::get_offset_of_U3CU3Ef__switchU24map24_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize720 = { sizeof (MethodCallDictionary_t605791082), -1, sizeof(MethodCallDictionary_t605791082_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable720[1] = 
{
	MethodCallDictionary_t605791082_StaticFields::get_offset_of_InternalKeys_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize721 = { sizeof (MethodDictionary_t207894204), -1, sizeof(MethodDictionary_t207894204_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable721[6] = 
{
	MethodDictionary_t207894204::get_offset_of__internalProperties_0(),
	MethodDictionary_t207894204::get_offset_of__message_1(),
	MethodDictionary_t207894204::get_offset_of__methodKeys_2(),
	MethodDictionary_t207894204::get_offset_of__ownProperties_3(),
	MethodDictionary_t207894204_StaticFields::get_offset_of_U3CU3Ef__switchU24map26_4(),
	MethodDictionary_t207894204_StaticFields::get_offset_of_U3CU3Ef__switchU24map27_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize722 = { sizeof (DictionaryEnumerator_t2516729552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable722[3] = 
{
	DictionaryEnumerator_t2516729552::get_offset_of__methodDictionary_0(),
	DictionaryEnumerator_t2516729552::get_offset_of__hashtableEnum_1(),
	DictionaryEnumerator_t2516729552::get_offset_of__posMethod_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize723 = { sizeof (MethodReturnDictionary_t2551046119), -1, sizeof(MethodReturnDictionary_t2551046119_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable723[2] = 
{
	MethodReturnDictionary_t2551046119_StaticFields::get_offset_of_InternalReturnKeys_6(),
	MethodReturnDictionary_t2551046119_StaticFields::get_offset_of_InternalExceptionKeys_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize724 = { sizeof (MonoMethodMessage_t2807636944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable724[10] = 
{
	MonoMethodMessage_t2807636944::get_offset_of_method_0(),
	MonoMethodMessage_t2807636944::get_offset_of_args_1(),
	MonoMethodMessage_t2807636944::get_offset_of_arg_types_2(),
	MonoMethodMessage_t2807636944::get_offset_of_ctx_3(),
	MonoMethodMessage_t2807636944::get_offset_of_rval_4(),
	MonoMethodMessage_t2807636944::get_offset_of_exc_5(),
	MonoMethodMessage_t2807636944::get_offset_of_call_type_6(),
	MonoMethodMessage_t2807636944::get_offset_of_uri_7(),
	MonoMethodMessage_t2807636944::get_offset_of_properties_8(),
	MonoMethodMessage_t2807636944::get_offset_of_methodSignature_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize725 = { sizeof (CallType_t3372275391)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable725[5] = 
{
	CallType_t3372275391::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize726 = { sizeof (OneWayAttribute_t936468379), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize727 = { sizeof (RemotingSurrogateSelector_t2472351973), -1, sizeof(RemotingSurrogateSelector_t2472351973_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable727[4] = 
{
	RemotingSurrogateSelector_t2472351973_StaticFields::get_offset_of_s_cachedTypeObjRef_0(),
	RemotingSurrogateSelector_t2472351973_StaticFields::get_offset_of__objRefSurrogate_1(),
	RemotingSurrogateSelector_t2472351973_StaticFields::get_offset_of__objRemotingSurrogate_2(),
	RemotingSurrogateSelector_t2472351973::get_offset_of__next_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize728 = { sizeof (RemotingSurrogate_t2834579653), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize729 = { sizeof (ObjRefSurrogate_t3860276170), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize730 = { sizeof (ReturnMessage_t1376985608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable730[13] = 
{
	ReturnMessage_t1376985608::get_offset_of__outArgs_0(),
	ReturnMessage_t1376985608::get_offset_of__args_1(),
	ReturnMessage_t1376985608::get_offset_of__outArgsCount_2(),
	ReturnMessage_t1376985608::get_offset_of__callCtx_3(),
	ReturnMessage_t1376985608::get_offset_of__returnValue_4(),
	ReturnMessage_t1376985608::get_offset_of__uri_5(),
	ReturnMessage_t1376985608::get_offset_of__exception_6(),
	ReturnMessage_t1376985608::get_offset_of__methodBase_7(),
	ReturnMessage_t1376985608::get_offset_of__methodName_8(),
	ReturnMessage_t1376985608::get_offset_of__methodSignature_9(),
	ReturnMessage_t1376985608::get_offset_of__typeName_10(),
	ReturnMessage_t1376985608::get_offset_of__properties_11(),
	ReturnMessage_t1376985608::get_offset_of__inArgInfo_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize731 = { sizeof (ServerContextTerminatorSink_t16296004), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize732 = { sizeof (ServerObjectTerminatorSink_t4187339465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable732[1] = 
{
	ServerObjectTerminatorSink_t4187339465::get_offset_of__nextSink_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize733 = { sizeof (StackBuilderSink_t883411942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable733[2] = 
{
	StackBuilderSink_t883411942::get_offset_of__target_0(),
	StackBuilderSink_t883411942::get_offset_of__rp_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize734 = { sizeof (SoapAttribute_t411004526), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable734[3] = 
{
	SoapAttribute_t411004526::get_offset_of__useAttribute_0(),
	SoapAttribute_t411004526::get_offset_of_ProtXmlNamespace_1(),
	SoapAttribute_t411004526::get_offset_of_ReflectInfo_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize735 = { sizeof (SoapFieldAttribute_t1164342470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable735[2] = 
{
	SoapFieldAttribute_t1164342470::get_offset_of__elementName_3(),
	SoapFieldAttribute_t1164342470::get_offset_of__isElement_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize736 = { sizeof (SoapMethodAttribute_t1935009310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable736[6] = 
{
	SoapMethodAttribute_t1935009310::get_offset_of__responseElement_3(),
	SoapMethodAttribute_t1935009310::get_offset_of__responseNamespace_4(),
	SoapMethodAttribute_t1935009310::get_offset_of__returnElement_5(),
	SoapMethodAttribute_t1935009310::get_offset_of__soapAction_6(),
	SoapMethodAttribute_t1935009310::get_offset_of__useAttribute_7(),
	SoapMethodAttribute_t1935009310::get_offset_of__namespace_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize737 = { sizeof (SoapParameterAttribute_t1445364003), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize738 = { sizeof (SoapTypeAttribute_t1750930817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable738[7] = 
{
	SoapTypeAttribute_t1750930817::get_offset_of__useAttribute_3(),
	SoapTypeAttribute_t1750930817::get_offset_of__xmlElementName_4(),
	SoapTypeAttribute_t1750930817::get_offset_of__xmlNamespace_5(),
	SoapTypeAttribute_t1750930817::get_offset_of__xmlTypeName_6(),
	SoapTypeAttribute_t1750930817::get_offset_of__xmlTypeNamespace_7(),
	SoapTypeAttribute_t1750930817::get_offset_of__isType_8(),
	SoapTypeAttribute_t1750930817::get_offset_of__isElement_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize739 = { sizeof (TransparentProxy_t431418284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable739[1] = 
{
	TransparentProxy_t431418284::get_offset_of__rp_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize740 = { sizeof (RealProxy_t2312050253), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable740[5] = 
{
	RealProxy_t2312050253::get_offset_of_class_to_proxy_0(),
	RealProxy_t2312050253::get_offset_of__targetDomainId_1(),
	RealProxy_t2312050253::get_offset_of__targetUri_2(),
	RealProxy_t2312050253::get_offset_of__objectIdentity_3(),
	RealProxy_t2312050253::get_offset_of__objTP_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize741 = { sizeof (RemotingProxy_t2444335113), -1, sizeof(RemotingProxy_t2444335113_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable741[5] = 
{
	RemotingProxy_t2444335113_StaticFields::get_offset_of__cache_GetTypeMethod_5(),
	RemotingProxy_t2444335113_StaticFields::get_offset_of__cache_GetHashCodeMethod_6(),
	RemotingProxy_t2444335113::get_offset_of__sink_7(),
	RemotingProxy_t2444335113::get_offset_of__hasEnvoySink_8(),
	RemotingProxy_t2444335113::get_offset_of__ctorCall_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize742 = { sizeof (ProxyAttribute_t455382724), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize743 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize744 = { sizeof (TrackingServices_t409076583), -1, sizeof(TrackingServices_t409076583_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable744[1] = 
{
	TrackingServices_t409076583_StaticFields::get_offset_of__handlers_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize745 = { sizeof (FormatterConverter_t2760117746), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize746 = { sizeof (FormatterServices_t305532257), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize747 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize748 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize749 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize750 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize751 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize752 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize753 = { sizeof (ObjectIDGenerator_t1260826161), -1, sizeof(ObjectIDGenerator_t1260826161_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable753[3] = 
{
	ObjectIDGenerator_t1260826161::get_offset_of_table_0(),
	ObjectIDGenerator_t1260826161::get_offset_of_current_1(),
	ObjectIDGenerator_t1260826161_StaticFields::get_offset_of_comparer_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize754 = { sizeof (InstanceComparer_t1261591096), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize755 = { sizeof (ObjectManager_t1653064325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable755[9] = 
{
	ObjectManager_t1653064325::get_offset_of__objectRecordChain_0(),
	ObjectManager_t1653064325::get_offset_of__lastObjectRecord_1(),
	ObjectManager_t1653064325::get_offset_of__deserializedRecords_2(),
	ObjectManager_t1653064325::get_offset_of__onDeserializedCallbackRecords_3(),
	ObjectManager_t1653064325::get_offset_of__objectRecords_4(),
	ObjectManager_t1653064325::get_offset_of__finalFixup_5(),
	ObjectManager_t1653064325::get_offset_of__selector_6(),
	ObjectManager_t1653064325::get_offset_of__context_7(),
	ObjectManager_t1653064325::get_offset_of__registeredObjectsCount_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize756 = { sizeof (BaseFixupRecord_t3185653456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable756[4] = 
{
	BaseFixupRecord_t3185653456::get_offset_of_ObjectToBeFixed_0(),
	BaseFixupRecord_t3185653456::get_offset_of_ObjectRequired_1(),
	BaseFixupRecord_t3185653456::get_offset_of_NextSameContainer_2(),
	BaseFixupRecord_t3185653456::get_offset_of_NextSameRequired_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize757 = { sizeof (ArrayFixupRecord_t4271256269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable757[1] = 
{
	ArrayFixupRecord_t4271256269::get_offset_of__index_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize758 = { sizeof (MultiArrayFixupRecord_t2501518442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable758[1] = 
{
	MultiArrayFixupRecord_t2501518442::get_offset_of__indices_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize759 = { sizeof (FixupRecord_t4176629420), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable759[1] = 
{
	FixupRecord_t4176629420::get_offset_of__member_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize760 = { sizeof (DelayedFixupRecord_t2425570624), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable760[1] = 
{
	DelayedFixupRecord_t2425570624::get_offset_of__memberName_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize761 = { sizeof (ObjectRecordStatus_t2117702867)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable761[5] = 
{
	ObjectRecordStatus_t2117702867::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize762 = { sizeof (ObjectRecord_t1187467272), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable762[13] = 
{
	ObjectRecord_t1187467272::get_offset_of_Status_0(),
	ObjectRecord_t1187467272::get_offset_of_OriginalObject_1(),
	ObjectRecord_t1187467272::get_offset_of_ObjectInstance_2(),
	ObjectRecord_t1187467272::get_offset_of_ObjectID_3(),
	ObjectRecord_t1187467272::get_offset_of_Info_4(),
	ObjectRecord_t1187467272::get_offset_of_IdOfContainingObj_5(),
	ObjectRecord_t1187467272::get_offset_of_Surrogate_6(),
	ObjectRecord_t1187467272::get_offset_of_SurrogateSelector_7(),
	ObjectRecord_t1187467272::get_offset_of_Member_8(),
	ObjectRecord_t1187467272::get_offset_of_ArrayIndex_9(),
	ObjectRecord_t1187467272::get_offset_of_FixupChainAsContainer_10(),
	ObjectRecord_t1187467272::get_offset_of_FixupChainAsRequired_11(),
	ObjectRecord_t1187467272::get_offset_of_Next_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize763 = { sizeof (OnDeserializedAttribute_t1335880599), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize764 = { sizeof (OnDeserializingAttribute_t338753086), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize765 = { sizeof (OnSerializedAttribute_t2595932830), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize766 = { sizeof (OnSerializingAttribute_t2580696919), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize767 = { sizeof (SerializationBinder_t274213469), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize768 = { sizeof (SerializationCallbacks_t2570604627), -1, sizeof(SerializationCallbacks_t2570604627_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable768[6] = 
{
	SerializationCallbacks_t2570604627::get_offset_of_onSerializingList_0(),
	SerializationCallbacks_t2570604627::get_offset_of_onSerializedList_1(),
	SerializationCallbacks_t2570604627::get_offset_of_onDeserializingList_2(),
	SerializationCallbacks_t2570604627::get_offset_of_onDeserializedList_3(),
	SerializationCallbacks_t2570604627_StaticFields::get_offset_of_cache_4(),
	SerializationCallbacks_t2570604627_StaticFields::get_offset_of_cache_lock_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize769 = { sizeof (CallbackHandler_t3280319253), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize770 = { sizeof (SerializationEntry_t648286436)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable770[3] = 
{
	SerializationEntry_t648286436::get_offset_of_name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SerializationEntry_t648286436::get_offset_of_objectType_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SerializationEntry_t648286436::get_offset_of_value_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize771 = { sizeof (SerializationException_t3941511869), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize772 = { sizeof (SerializationInfo_t950877179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable772[5] = 
{
	SerializationInfo_t950877179::get_offset_of_serialized_0(),
	SerializationInfo_t950877179::get_offset_of_values_1(),
	SerializationInfo_t950877179::get_offset_of_assemblyName_2(),
	SerializationInfo_t950877179::get_offset_of_fullTypeName_3(),
	SerializationInfo_t950877179::get_offset_of_converter_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize773 = { sizeof (SerializationInfoEnumerator_t2232395945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable773[1] = 
{
	SerializationInfoEnumerator_t2232395945::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize774 = { sizeof (SerializationObjectManager_t4107654543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable774[3] = 
{
	SerializationObjectManager_t4107654543::get_offset_of_context_0(),
	SerializationObjectManager_t4107654543::get_offset_of_seen_1(),
	SerializationObjectManager_t4107654543::get_offset_of_callbacks_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize775 = { sizeof (U3CRegisterObjectU3Ec__AnonStorey2_t3516576679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable775[2] = 
{
	U3CRegisterObjectU3Ec__AnonStorey2_t3516576679::get_offset_of_sc_0(),
	U3CRegisterObjectU3Ec__AnonStorey2_t3516576679::get_offset_of_obj_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize776 = { sizeof (StreamingContext_t3711869237)+ sizeof (Il2CppObject), sizeof(StreamingContext_t3711869237_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable776[2] = 
{
	StreamingContext_t3711869237::get_offset_of_state_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StreamingContext_t3711869237::get_offset_of_additional_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize777 = { sizeof (StreamingContextStates_t3580100459)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable777[10] = 
{
	StreamingContextStates_t3580100459::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize778 = { sizeof (FormatterAssemblyStyle_t868039825)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable778[3] = 
{
	FormatterAssemblyStyle_t868039825::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize779 = { sizeof (FormatterTypeStyle_t3400733584)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable779[4] = 
{
	FormatterTypeStyle_t3400733584::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize780 = { sizeof (TypeFilterLevel_t977535029)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable780[3] = 
{
	TypeFilterLevel_t977535029::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize781 = { sizeof (BinaryFormatter_t3197753202), -1, sizeof(BinaryFormatter_t3197753202_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable781[7] = 
{
	BinaryFormatter_t3197753202::get_offset_of_assembly_format_0(),
	BinaryFormatter_t3197753202::get_offset_of_binder_1(),
	BinaryFormatter_t3197753202::get_offset_of_context_2(),
	BinaryFormatter_t3197753202::get_offset_of_surrogate_selector_3(),
	BinaryFormatter_t3197753202::get_offset_of_type_format_4(),
	BinaryFormatter_t3197753202::get_offset_of_filter_level_5(),
	BinaryFormatter_t3197753202_StaticFields::get_offset_of_U3CDefaultSurrogateSelectorU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize782 = { sizeof (BinaryCommon_t3059247396), -1, sizeof(BinaryCommon_t3059247396_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable782[4] = 
{
	BinaryCommon_t3059247396_StaticFields::get_offset_of_BinaryHeader_0(),
	BinaryCommon_t3059247396_StaticFields::get_offset_of__typeCodesToType_1(),
	BinaryCommon_t3059247396_StaticFields::get_offset_of__typeCodeMap_2(),
	BinaryCommon_t3059247396_StaticFields::get_offset_of_UseReflectionSerialization_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize783 = { sizeof (BinaryElement_t4114212191)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable783[24] = 
{
	BinaryElement_t4114212191::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize784 = { sizeof (TypeTag_t3541821701)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable784[9] = 
{
	TypeTag_t3541821701::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize785 = { sizeof (MethodFlags_t1925520788)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable785[11] = 
{
	MethodFlags_t1925520788::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize786 = { sizeof (ReturnTypeTag_t773701450)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable786[5] = 
{
	ReturnTypeTag_t773701450::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize787 = { sizeof (CodeGenerator_t1096194562), -1, sizeof(CodeGenerator_t1096194562_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable787[2] = 
{
	CodeGenerator_t1096194562_StaticFields::get_offset_of_monitor_0(),
	CodeGenerator_t1096194562_StaticFields::get_offset_of__module_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize788 = { sizeof (ObjectReader_t1940957392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable788[12] = 
{
	ObjectReader_t1940957392::get_offset_of__surrogateSelector_0(),
	ObjectReader_t1940957392::get_offset_of__context_1(),
	ObjectReader_t1940957392::get_offset_of__binder_2(),
	ObjectReader_t1940957392::get_offset_of__filterLevel_3(),
	ObjectReader_t1940957392::get_offset_of__manager_4(),
	ObjectReader_t1940957392::get_offset_of__registeredAssemblies_5(),
	ObjectReader_t1940957392::get_offset_of__typeMetadataCache_6(),
	ObjectReader_t1940957392::get_offset_of__lastObject_7(),
	ObjectReader_t1940957392::get_offset_of__lastObjectID_8(),
	ObjectReader_t1940957392::get_offset_of__rootObjectID_9(),
	ObjectReader_t1940957392::get_offset_of_arrayBuffer_10(),
	ObjectReader_t1940957392::get_offset_of_ArrayBufferLength_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize789 = { sizeof (TypeMetadata_t3139523260), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable789[6] = 
{
	TypeMetadata_t3139523260::get_offset_of_Type_0(),
	TypeMetadata_t3139523260::get_offset_of_MemberTypes_1(),
	TypeMetadata_t3139523260::get_offset_of_MemberNames_2(),
	TypeMetadata_t3139523260::get_offset_of_MemberInfos_3(),
	TypeMetadata_t3139523260::get_offset_of_FieldCount_4(),
	TypeMetadata_t3139523260::get_offset_of_NeedsSerializationInfo_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize790 = { sizeof (ArrayNullFiller_t1685365060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable790[1] = 
{
	ArrayNullFiller_t1685365060::get_offset_of_NullCount_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize791 = { sizeof (TypeMetadata_t2187352167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable791[2] = 
{
	TypeMetadata_t2187352167::get_offset_of_TypeAssemblyName_0(),
	TypeMetadata_t2187352167::get_offset_of_InstanceTypeName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize792 = { sizeof (ClrTypeMetadata_t3885853035), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable792[1] = 
{
	ClrTypeMetadata_t3885853035::get_offset_of_InstanceType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize793 = { sizeof (SerializableTypeMetadata_t2796913665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable793[2] = 
{
	SerializableTypeMetadata_t2796913665::get_offset_of_types_2(),
	SerializableTypeMetadata_t2796913665::get_offset_of_names_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize794 = { sizeof (MemberTypeMetadata_t976220688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable794[1] = 
{
	MemberTypeMetadata_t976220688::get_offset_of_members_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize795 = { sizeof (ObjectWriter_t2968406231), -1, sizeof(ObjectWriter_t2968406231_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable795[14] = 
{
	ObjectWriter_t2968406231::get_offset_of__idGenerator_0(),
	ObjectWriter_t2968406231::get_offset_of__cachedMetadata_1(),
	ObjectWriter_t2968406231::get_offset_of__pendingObjects_2(),
	ObjectWriter_t2968406231::get_offset_of__assemblyCache_3(),
	ObjectWriter_t2968406231_StaticFields::get_offset_of__cachedTypes_4(),
	ObjectWriter_t2968406231_StaticFields::get_offset_of_CorlibAssembly_5(),
	ObjectWriter_t2968406231_StaticFields::get_offset_of_CorlibAssemblyName_6(),
	ObjectWriter_t2968406231::get_offset_of__surrogateSelector_7(),
	ObjectWriter_t2968406231::get_offset_of__context_8(),
	ObjectWriter_t2968406231::get_offset_of__assemblyFormat_9(),
	ObjectWriter_t2968406231::get_offset_of__typeFormat_10(),
	ObjectWriter_t2968406231::get_offset_of_arrayBuffer_11(),
	ObjectWriter_t2968406231::get_offset_of_ArrayBufferLength_12(),
	ObjectWriter_t2968406231::get_offset_of__manager_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize796 = { sizeof (MetadataReference_t4077873660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable796[2] = 
{
	MetadataReference_t4077873660::get_offset_of_Metadata_0(),
	MetadataReference_t4077873660::get_offset_of_ObjectID_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize797 = { sizeof (MessageFormatter_t457758949), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize798 = { sizeof (AllowPartiallyTrustedCallersAttribute_t1828959166), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize799 = { sizeof (CodeAccessPermission_t2681295399), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
