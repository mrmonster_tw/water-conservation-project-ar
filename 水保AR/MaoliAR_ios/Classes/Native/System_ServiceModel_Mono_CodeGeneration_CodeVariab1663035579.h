﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_Mono_CodeGeneration_CodeStatemen30174410.h"

// Mono.CodeGeneration.CodeVariableReference
struct CodeVariableReference_t2879773301;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.CodeGeneration.CodeVariableDeclaration
struct  CodeVariableDeclaration_t1663035579  : public CodeStatement_t30174410
{
public:
	// Mono.CodeGeneration.CodeVariableReference Mono.CodeGeneration.CodeVariableDeclaration::var
	CodeVariableReference_t2879773301 * ___var_0;

public:
	inline static int32_t get_offset_of_var_0() { return static_cast<int32_t>(offsetof(CodeVariableDeclaration_t1663035579, ___var_0)); }
	inline CodeVariableReference_t2879773301 * get_var_0() const { return ___var_0; }
	inline CodeVariableReference_t2879773301 ** get_address_of_var_0() { return &___var_0; }
	inline void set_var_0(CodeVariableReference_t2879773301 * value)
	{
		___var_0 = value;
		Il2CppCodeGenWriteBarrier(&___var_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
