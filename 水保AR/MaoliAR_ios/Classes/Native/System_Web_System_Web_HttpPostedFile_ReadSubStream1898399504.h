﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_Stream1273022909.h"

// System.IO.Stream
struct Stream_t1273022909;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpPostedFile/ReadSubStream
struct  ReadSubStream_t1898399504  : public Stream_t1273022909
{
public:
	// System.IO.Stream System.Web.HttpPostedFile/ReadSubStream::s
	Stream_t1273022909 * ___s_2;
	// System.Int64 System.Web.HttpPostedFile/ReadSubStream::offset
	int64_t ___offset_3;
	// System.Int64 System.Web.HttpPostedFile/ReadSubStream::end
	int64_t ___end_4;
	// System.Int64 System.Web.HttpPostedFile/ReadSubStream::position
	int64_t ___position_5;

public:
	inline static int32_t get_offset_of_s_2() { return static_cast<int32_t>(offsetof(ReadSubStream_t1898399504, ___s_2)); }
	inline Stream_t1273022909 * get_s_2() const { return ___s_2; }
	inline Stream_t1273022909 ** get_address_of_s_2() { return &___s_2; }
	inline void set_s_2(Stream_t1273022909 * value)
	{
		___s_2 = value;
		Il2CppCodeGenWriteBarrier(&___s_2, value);
	}

	inline static int32_t get_offset_of_offset_3() { return static_cast<int32_t>(offsetof(ReadSubStream_t1898399504, ___offset_3)); }
	inline int64_t get_offset_3() const { return ___offset_3; }
	inline int64_t* get_address_of_offset_3() { return &___offset_3; }
	inline void set_offset_3(int64_t value)
	{
		___offset_3 = value;
	}

	inline static int32_t get_offset_of_end_4() { return static_cast<int32_t>(offsetof(ReadSubStream_t1898399504, ___end_4)); }
	inline int64_t get_end_4() const { return ___end_4; }
	inline int64_t* get_address_of_end_4() { return &___end_4; }
	inline void set_end_4(int64_t value)
	{
		___end_4 = value;
	}

	inline static int32_t get_offset_of_position_5() { return static_cast<int32_t>(offsetof(ReadSubStream_t1898399504, ___position_5)); }
	inline int64_t get_position_5() const { return ___position_5; }
	inline int64_t* get_address_of_position_5() { return &___position_5; }
	inline void set_position_5(int64_t value)
	{
		___position_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
