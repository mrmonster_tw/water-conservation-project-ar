﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.EndpointAddress
struct EndpointAddress_t3119842923;
// System.ServiceModel.Channels.Binding
struct Binding_t859993683;
// System.ServiceModel.PeerResolver
struct PeerResolver_t1567980581;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.PeerResolvers.PeerCustomResolverSettings
struct  PeerCustomResolverSettings_t3246124786  : public Il2CppObject
{
public:
	// System.ServiceModel.EndpointAddress System.ServiceModel.PeerResolvers.PeerCustomResolverSettings::<Address>k__BackingField
	EndpointAddress_t3119842923 * ___U3CAddressU3Ek__BackingField_0;
	// System.ServiceModel.Channels.Binding System.ServiceModel.PeerResolvers.PeerCustomResolverSettings::<Binding>k__BackingField
	Binding_t859993683 * ___U3CBindingU3Ek__BackingField_1;
	// System.ServiceModel.PeerResolver System.ServiceModel.PeerResolvers.PeerCustomResolverSettings::<Resolver>k__BackingField
	PeerResolver_t1567980581 * ___U3CResolverU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CAddressU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PeerCustomResolverSettings_t3246124786, ___U3CAddressU3Ek__BackingField_0)); }
	inline EndpointAddress_t3119842923 * get_U3CAddressU3Ek__BackingField_0() const { return ___U3CAddressU3Ek__BackingField_0; }
	inline EndpointAddress_t3119842923 ** get_address_of_U3CAddressU3Ek__BackingField_0() { return &___U3CAddressU3Ek__BackingField_0; }
	inline void set_U3CAddressU3Ek__BackingField_0(EndpointAddress_t3119842923 * value)
	{
		___U3CAddressU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAddressU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CBindingU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PeerCustomResolverSettings_t3246124786, ___U3CBindingU3Ek__BackingField_1)); }
	inline Binding_t859993683 * get_U3CBindingU3Ek__BackingField_1() const { return ___U3CBindingU3Ek__BackingField_1; }
	inline Binding_t859993683 ** get_address_of_U3CBindingU3Ek__BackingField_1() { return &___U3CBindingU3Ek__BackingField_1; }
	inline void set_U3CBindingU3Ek__BackingField_1(Binding_t859993683 * value)
	{
		___U3CBindingU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CBindingU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CResolverU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PeerCustomResolverSettings_t3246124786, ___U3CResolverU3Ek__BackingField_2)); }
	inline PeerResolver_t1567980581 * get_U3CResolverU3Ek__BackingField_2() const { return ___U3CResolverU3Ek__BackingField_2; }
	inline PeerResolver_t1567980581 ** get_address_of_U3CResolverU3Ek__BackingField_2() { return &___U3CResolverU3Ek__BackingField_2; }
	inline void set_U3CResolverU3Ek__BackingField_2(PeerResolver_t1567980581 * value)
	{
		___U3CResolverU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CResolverU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
