﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Security_T2868958784.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.SspiSecurityTokenParameters
struct  SspiSecurityTokenParameters_t1739095634  : public SecurityTokenParameters_t2868958784
{
public:
	// System.Boolean System.ServiceModel.Security.Tokens.SspiSecurityTokenParameters::cancel
	bool ___cancel_4;

public:
	inline static int32_t get_offset_of_cancel_4() { return static_cast<int32_t>(offsetof(SspiSecurityTokenParameters_t1739095634, ___cancel_4)); }
	inline bool get_cancel_4() const { return ___cancel_4; }
	inline bool* get_address_of_cancel_4() { return &___cancel_4; }
	inline void set_cancel_4(bool value)
	{
		___cancel_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
