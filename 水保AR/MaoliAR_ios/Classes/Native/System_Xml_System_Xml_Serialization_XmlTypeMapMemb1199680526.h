﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Serialization_XmlTypeMapMemb3739392753.h"

// System.Xml.Serialization.XmlTypeMapElementInfoList
struct XmlTypeMapElementInfoList_t2115305165;
// System.String
struct String_t;
// System.Xml.Serialization.TypeData
struct TypeData_t476999220;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeMapMemberElement
struct  XmlTypeMapMemberElement_t1199680526  : public XmlTypeMapMember_t3739392753
{
public:
	// System.Xml.Serialization.XmlTypeMapElementInfoList System.Xml.Serialization.XmlTypeMapMemberElement::_elementInfo
	XmlTypeMapElementInfoList_t2115305165 * ____elementInfo_9;
	// System.String System.Xml.Serialization.XmlTypeMapMemberElement::_choiceMember
	String_t* ____choiceMember_10;
	// System.Boolean System.Xml.Serialization.XmlTypeMapMemberElement::_isTextCollector
	bool ____isTextCollector_11;
	// System.Xml.Serialization.TypeData System.Xml.Serialization.XmlTypeMapMemberElement::_choiceTypeData
	TypeData_t476999220 * ____choiceTypeData_12;

public:
	inline static int32_t get_offset_of__elementInfo_9() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberElement_t1199680526, ____elementInfo_9)); }
	inline XmlTypeMapElementInfoList_t2115305165 * get__elementInfo_9() const { return ____elementInfo_9; }
	inline XmlTypeMapElementInfoList_t2115305165 ** get_address_of__elementInfo_9() { return &____elementInfo_9; }
	inline void set__elementInfo_9(XmlTypeMapElementInfoList_t2115305165 * value)
	{
		____elementInfo_9 = value;
		Il2CppCodeGenWriteBarrier(&____elementInfo_9, value);
	}

	inline static int32_t get_offset_of__choiceMember_10() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberElement_t1199680526, ____choiceMember_10)); }
	inline String_t* get__choiceMember_10() const { return ____choiceMember_10; }
	inline String_t** get_address_of__choiceMember_10() { return &____choiceMember_10; }
	inline void set__choiceMember_10(String_t* value)
	{
		____choiceMember_10 = value;
		Il2CppCodeGenWriteBarrier(&____choiceMember_10, value);
	}

	inline static int32_t get_offset_of__isTextCollector_11() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberElement_t1199680526, ____isTextCollector_11)); }
	inline bool get__isTextCollector_11() const { return ____isTextCollector_11; }
	inline bool* get_address_of__isTextCollector_11() { return &____isTextCollector_11; }
	inline void set__isTextCollector_11(bool value)
	{
		____isTextCollector_11 = value;
	}

	inline static int32_t get_offset_of__choiceTypeData_12() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberElement_t1199680526, ____choiceTypeData_12)); }
	inline TypeData_t476999220 * get__choiceTypeData_12() const { return ____choiceTypeData_12; }
	inline TypeData_t476999220 ** get_address_of__choiceTypeData_12() { return &____choiceTypeData_12; }
	inline void set__choiceTypeData_12(TypeData_t476999220 * value)
	{
		____choiceTypeData_12 = value;
		Il2CppCodeGenWriteBarrier(&____choiceTypeData_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
