﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_WebControls_Label3626644934.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.WebControls.BaseValidator
struct  BaseValidator_t3559083609  : public Label_t3626644934
{
public:
	// System.Boolean System.Web.UI.WebControls.BaseValidator::render_uplevel
	bool ___render_uplevel_36;
	// System.Boolean System.Web.UI.WebControls.BaseValidator::valid
	bool ___valid_37;
	// System.Boolean System.Web.UI.WebControls.BaseValidator::pre_render_called
	bool ___pre_render_called_38;

public:
	inline static int32_t get_offset_of_render_uplevel_36() { return static_cast<int32_t>(offsetof(BaseValidator_t3559083609, ___render_uplevel_36)); }
	inline bool get_render_uplevel_36() const { return ___render_uplevel_36; }
	inline bool* get_address_of_render_uplevel_36() { return &___render_uplevel_36; }
	inline void set_render_uplevel_36(bool value)
	{
		___render_uplevel_36 = value;
	}

	inline static int32_t get_offset_of_valid_37() { return static_cast<int32_t>(offsetof(BaseValidator_t3559083609, ___valid_37)); }
	inline bool get_valid_37() const { return ___valid_37; }
	inline bool* get_address_of_valid_37() { return &___valid_37; }
	inline void set_valid_37(bool value)
	{
		___valid_37 = value;
	}

	inline static int32_t get_offset_of_pre_render_called_38() { return static_cast<int32_t>(offsetof(BaseValidator_t3559083609, ___pre_render_called_38)); }
	inline bool get_pre_render_called_38() const { return ___pre_render_called_38; }
	inline bool* get_address_of_pre_render_called_38() { return &___pre_render_called_38; }
	inline void set_pre_render_called_38(bool value)
	{
		___pre_render_called_38 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
