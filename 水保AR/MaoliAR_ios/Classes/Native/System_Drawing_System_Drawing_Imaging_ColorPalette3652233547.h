﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Drawing.Color[]
struct ColorU5BU5D_t1089800065;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Drawing.Imaging.ColorPalette
struct  ColorPalette_t3652233547  : public Il2CppObject
{
public:
	// System.Int32 System.Drawing.Imaging.ColorPalette::flags
	int32_t ___flags_0;
	// System.Drawing.Color[] System.Drawing.Imaging.ColorPalette::entries
	ColorU5BU5D_t1089800065* ___entries_1;

public:
	inline static int32_t get_offset_of_flags_0() { return static_cast<int32_t>(offsetof(ColorPalette_t3652233547, ___flags_0)); }
	inline int32_t get_flags_0() const { return ___flags_0; }
	inline int32_t* get_address_of_flags_0() { return &___flags_0; }
	inline void set_flags_0(int32_t value)
	{
		___flags_0 = value;
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(ColorPalette_t3652233547, ___entries_1)); }
	inline ColorU5BU5D_t1089800065* get_entries_1() const { return ___entries_1; }
	inline ColorU5BU5D_t1089800065** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(ColorU5BU5D_t1089800065* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier(&___entries_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
