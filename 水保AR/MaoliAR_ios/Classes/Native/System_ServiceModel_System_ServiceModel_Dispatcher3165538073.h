﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Description.ServiceMetadataBehavior
struct ServiceMetadataBehavior_t566713614;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.MetadataExchange
struct  MetadataExchange_t3165538073  : public Il2CppObject
{
public:
	// System.ServiceModel.Description.ServiceMetadataBehavior System.ServiceModel.Dispatcher.MetadataExchange::beh
	ServiceMetadataBehavior_t566713614 * ___beh_0;

public:
	inline static int32_t get_offset_of_beh_0() { return static_cast<int32_t>(offsetof(MetadataExchange_t3165538073, ___beh_0)); }
	inline ServiceMetadataBehavior_t566713614 * get_beh_0() const { return ___beh_0; }
	inline ServiceMetadataBehavior_t566713614 ** get_address_of_beh_0() { return &___beh_0; }
	inline void set_beh_0(ServiceMetadataBehavior_t566713614 * value)
	{
		___beh_0 = value;
		Il2CppCodeGenWriteBarrier(&___beh_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
