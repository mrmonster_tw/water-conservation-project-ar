﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BookingInitialize
struct  BookingInitialize_t412933033  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> BookingInitialize::openObject
	List_1_t2585711361 * ___openObject_2;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> BookingInitialize::closeObject
	List_1_t2585711361 * ___closeObject_3;

public:
	inline static int32_t get_offset_of_openObject_2() { return static_cast<int32_t>(offsetof(BookingInitialize_t412933033, ___openObject_2)); }
	inline List_1_t2585711361 * get_openObject_2() const { return ___openObject_2; }
	inline List_1_t2585711361 ** get_address_of_openObject_2() { return &___openObject_2; }
	inline void set_openObject_2(List_1_t2585711361 * value)
	{
		___openObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___openObject_2, value);
	}

	inline static int32_t get_offset_of_closeObject_3() { return static_cast<int32_t>(offsetof(BookingInitialize_t412933033, ___closeObject_3)); }
	inline List_1_t2585711361 * get_closeObject_3() const { return ___closeObject_3; }
	inline List_1_t2585711361 ** get_address_of_closeObject_3() { return &___closeObject_3; }
	inline void set_closeObject_3(List_1_t2585711361 * value)
	{
		___closeObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___closeObject_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
