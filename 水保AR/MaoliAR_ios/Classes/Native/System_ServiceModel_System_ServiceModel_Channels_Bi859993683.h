﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_TimeSpan881159249.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.Binding
struct  Binding_t859993683  : public Il2CppObject
{
public:
	// System.String System.ServiceModel.Channels.Binding::name
	String_t* ___name_0;
	// System.String System.ServiceModel.Channels.Binding::ns
	String_t* ___ns_1;
	// System.TimeSpan System.ServiceModel.Channels.Binding::open_timeout
	TimeSpan_t881159249  ___open_timeout_2;
	// System.TimeSpan System.ServiceModel.Channels.Binding::close_timeout
	TimeSpan_t881159249  ___close_timeout_3;
	// System.TimeSpan System.ServiceModel.Channels.Binding::receive_timeout
	TimeSpan_t881159249  ___receive_timeout_4;
	// System.TimeSpan System.ServiceModel.Channels.Binding::send_timeout
	TimeSpan_t881159249  ___send_timeout_5;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Binding_t859993683, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_ns_1() { return static_cast<int32_t>(offsetof(Binding_t859993683, ___ns_1)); }
	inline String_t* get_ns_1() const { return ___ns_1; }
	inline String_t** get_address_of_ns_1() { return &___ns_1; }
	inline void set_ns_1(String_t* value)
	{
		___ns_1 = value;
		Il2CppCodeGenWriteBarrier(&___ns_1, value);
	}

	inline static int32_t get_offset_of_open_timeout_2() { return static_cast<int32_t>(offsetof(Binding_t859993683, ___open_timeout_2)); }
	inline TimeSpan_t881159249  get_open_timeout_2() const { return ___open_timeout_2; }
	inline TimeSpan_t881159249 * get_address_of_open_timeout_2() { return &___open_timeout_2; }
	inline void set_open_timeout_2(TimeSpan_t881159249  value)
	{
		___open_timeout_2 = value;
	}

	inline static int32_t get_offset_of_close_timeout_3() { return static_cast<int32_t>(offsetof(Binding_t859993683, ___close_timeout_3)); }
	inline TimeSpan_t881159249  get_close_timeout_3() const { return ___close_timeout_3; }
	inline TimeSpan_t881159249 * get_address_of_close_timeout_3() { return &___close_timeout_3; }
	inline void set_close_timeout_3(TimeSpan_t881159249  value)
	{
		___close_timeout_3 = value;
	}

	inline static int32_t get_offset_of_receive_timeout_4() { return static_cast<int32_t>(offsetof(Binding_t859993683, ___receive_timeout_4)); }
	inline TimeSpan_t881159249  get_receive_timeout_4() const { return ___receive_timeout_4; }
	inline TimeSpan_t881159249 * get_address_of_receive_timeout_4() { return &___receive_timeout_4; }
	inline void set_receive_timeout_4(TimeSpan_t881159249  value)
	{
		___receive_timeout_4 = value;
	}

	inline static int32_t get_offset_of_send_timeout_5() { return static_cast<int32_t>(offsetof(Binding_t859993683, ___send_timeout_5)); }
	inline TimeSpan_t881159249  get_send_timeout_5() const { return ___send_timeout_5; }
	inline TimeSpan_t881159249 * get_address_of_send_timeout_5() { return &___send_timeout_5; }
	inline void set_send_timeout_5(TimeSpan_t881159249  value)
	{
		___send_timeout_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
