﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"
#include "System_Web_System_Web_Compilation_BuildProviderAppl131801534.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.BuildProviderAppliesToAttribute
struct  BuildProviderAppliesToAttribute_t3516266216  : public Attribute_t861562559
{
public:
	// System.Web.Compilation.BuildProviderAppliesTo System.Web.Compilation.BuildProviderAppliesToAttribute::appliesTo
	int32_t ___appliesTo_0;

public:
	inline static int32_t get_offset_of_appliesTo_0() { return static_cast<int32_t>(offsetof(BuildProviderAppliesToAttribute_t3516266216, ___appliesTo_0)); }
	inline int32_t get_appliesTo_0() const { return ___appliesTo_0; }
	inline int32_t* get_address_of_appliesTo_0() { return &___appliesTo_0; }
	inline void set_appliesTo_0(int32_t value)
	{
		___appliesTo_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
