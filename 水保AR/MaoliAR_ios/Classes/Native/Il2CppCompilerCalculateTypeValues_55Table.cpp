﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_U3CMouseD3909241878.h"
#include "UnityEngine_UI_UnityEngine_UI_Mask1803652131.h"
#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphic3839221559.h"
#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphic_Cull3661388177.h"
#include "UnityEngine_UI_UnityEngine_UI_MaskUtilities4151184739.h"
#include "UnityEngine_UI_UnityEngine_UI_Misc1447421763.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation3049316579.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation_Mode1066900953.h"
#include "UnityEngine_UI_UnityEngine_UI_RawImage3182918964.h"
#include "UnityEngine_UI_UnityEngine_UI_RectMask2D3474889437.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar1494447233.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Direction3470714353.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_ScrollEvent149898510.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Axis1697763317.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_U3CClickRe3442648935.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect4137855814.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_MovementT4072922106.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollbarV705693775.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollRect343079324.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable3250028441.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transitio1769908631.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Selection2656606514.h"
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtility3359423571.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider3903728902.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction337909235.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_SliderEvent3180273144.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis809944411.h"
#include "UnityEngine_UI_UnityEngine_UI_SpriteState1362986479.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial3850132571.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatE2957107092.h"
#include "UnityEngine_UI_UnityEngine_UI_Text1901882714.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle2735377061.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransit3587297765.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent1873685584.h"
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroup123837990.h"
#include "UnityEngine_UI_UnityEngine_UI_ClipperRegistry2428680409.h"
#include "UnityEngine_UI_UnityEngine_UI_Clipping312708592.h"
#include "UnityEngine_UI_UnityEngine_UI_RectangularVertexClip626611136.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter3312407083.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_As3417192999.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler2767979955.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMo2604066427.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenM3675272090.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit2218508340.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter3850442145.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_Fi3267881214.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup3046220461.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corn1493259673.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis3613393006.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Const814224393.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGrou2586782146.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVerticalL729725570.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement1785403678.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup2436138090.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup_U3CDelay3170500204.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder541313304.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility2745813735.h"
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup923838031.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2103211062.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCach701940803.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCach768590915.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac1884415901.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3913627115.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper2453304189.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect2675891272.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect2440176439.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline2536100125.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV13991086357.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow773074319.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E3057255361.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_2488454196.h"
#include "Vuforia_UnityExtensions_iOS_U3CModuleU3E692745525.h"
#include "Vuforia_UnityExtensions_iOS_Vuforia_VuforiaNativeI2037146173.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E692745525.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BtConnection2892495216.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BtConnector901688599.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_Hi672210414.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H1904069300.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H1588725102.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H1923179915.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_Hi981364519.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_Hi582374880.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_Sh567755140.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MoveSample3412539464.h"
#include "AssemblyU2DCSharpU2Dfirstpass_RotateSample3002381122.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SampleInfo3693012684.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween770867771.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5500 = { sizeof (U3CMouseDragOutsideRectU3Ec__Iterator1_t3909241878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5500[8] = 
{
	U3CMouseDragOutsideRectU3Ec__Iterator1_t3909241878::get_offset_of_eventData_0(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t3909241878::get_offset_of_U3ClocalMousePosU3E__1_1(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t3909241878::get_offset_of_U3CrectU3E__1_2(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t3909241878::get_offset_of_U3CdelayU3E__1_3(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t3909241878::get_offset_of_U24this_4(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t3909241878::get_offset_of_U24current_5(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t3909241878::get_offset_of_U24disposing_6(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t3909241878::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5501 = { sizeof (Mask_t1803652131), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5501[5] = 
{
	Mask_t1803652131::get_offset_of_m_RectTransform_2(),
	Mask_t1803652131::get_offset_of_m_ShowMaskGraphic_3(),
	Mask_t1803652131::get_offset_of_m_Graphic_4(),
	Mask_t1803652131::get_offset_of_m_MaskMaterial_5(),
	Mask_t1803652131::get_offset_of_m_UnmaskMaterial_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5502 = { sizeof (MaskableGraphic_t3839221559), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5502[9] = 
{
	MaskableGraphic_t3839221559::get_offset_of_m_ShouldRecalculateStencil_19(),
	MaskableGraphic_t3839221559::get_offset_of_m_MaskMaterial_20(),
	MaskableGraphic_t3839221559::get_offset_of_m_ParentMask_21(),
	MaskableGraphic_t3839221559::get_offset_of_m_Maskable_22(),
	MaskableGraphic_t3839221559::get_offset_of_m_IncludeForMasking_23(),
	MaskableGraphic_t3839221559::get_offset_of_m_OnCullStateChanged_24(),
	MaskableGraphic_t3839221559::get_offset_of_m_ShouldRecalculate_25(),
	MaskableGraphic_t3839221559::get_offset_of_m_StencilValue_26(),
	MaskableGraphic_t3839221559::get_offset_of_m_Corners_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5503 = { sizeof (CullStateChangedEvent_t3661388177), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5504 = { sizeof (MaskUtilities_t4151184739), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5505 = { sizeof (Misc_t1447421763), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5506 = { sizeof (Navigation_t3049316579)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5506[5] = 
{
	Navigation_t3049316579::get_offset_of_m_Mode_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Navigation_t3049316579::get_offset_of_m_SelectOnUp_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Navigation_t3049316579::get_offset_of_m_SelectOnDown_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Navigation_t3049316579::get_offset_of_m_SelectOnLeft_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Navigation_t3049316579::get_offset_of_m_SelectOnRight_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5507 = { sizeof (Mode_t1066900953)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5507[6] = 
{
	Mode_t1066900953::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5508 = { sizeof (RawImage_t3182918964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5508[2] = 
{
	RawImage_t3182918964::get_offset_of_m_Texture_28(),
	RawImage_t3182918964::get_offset_of_m_UVRect_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5509 = { sizeof (RectMask2D_t3474889437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5509[8] = 
{
	RectMask2D_t3474889437::get_offset_of_m_VertexClipper_2(),
	RectMask2D_t3474889437::get_offset_of_m_RectTransform_3(),
	RectMask2D_t3474889437::get_offset_of_m_ClipTargets_4(),
	RectMask2D_t3474889437::get_offset_of_m_ShouldRecalculateClipRects_5(),
	RectMask2D_t3474889437::get_offset_of_m_Clippers_6(),
	RectMask2D_t3474889437::get_offset_of_m_LastClipRectCanvasSpace_7(),
	RectMask2D_t3474889437::get_offset_of_m_LastValidClipRect_8(),
	RectMask2D_t3474889437::get_offset_of_m_ForceClip_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5510 = { sizeof (Scrollbar_t1494447233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5510[11] = 
{
	Scrollbar_t1494447233::get_offset_of_m_HandleRect_16(),
	Scrollbar_t1494447233::get_offset_of_m_Direction_17(),
	Scrollbar_t1494447233::get_offset_of_m_Value_18(),
	Scrollbar_t1494447233::get_offset_of_m_Size_19(),
	Scrollbar_t1494447233::get_offset_of_m_NumberOfSteps_20(),
	Scrollbar_t1494447233::get_offset_of_m_OnValueChanged_21(),
	Scrollbar_t1494447233::get_offset_of_m_ContainerRect_22(),
	Scrollbar_t1494447233::get_offset_of_m_Offset_23(),
	Scrollbar_t1494447233::get_offset_of_m_Tracker_24(),
	Scrollbar_t1494447233::get_offset_of_m_PointerDownRepeat_25(),
	Scrollbar_t1494447233::get_offset_of_isPointerDownAndNotDragging_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5511 = { sizeof (Direction_t3470714353)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5511[5] = 
{
	Direction_t3470714353::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5512 = { sizeof (ScrollEvent_t149898510), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5513 = { sizeof (Axis_t1697763317)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5513[3] = 
{
	Axis_t1697763317::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5514 = { sizeof (U3CClickRepeatU3Ec__Iterator0_t3442648935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5514[5] = 
{
	U3CClickRepeatU3Ec__Iterator0_t3442648935::get_offset_of_eventData_0(),
	U3CClickRepeatU3Ec__Iterator0_t3442648935::get_offset_of_U24this_1(),
	U3CClickRepeatU3Ec__Iterator0_t3442648935::get_offset_of_U24current_2(),
	U3CClickRepeatU3Ec__Iterator0_t3442648935::get_offset_of_U24disposing_3(),
	U3CClickRepeatU3Ec__Iterator0_t3442648935::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5515 = { sizeof (ScrollRect_t4137855814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5515[36] = 
{
	ScrollRect_t4137855814::get_offset_of_m_Content_2(),
	ScrollRect_t4137855814::get_offset_of_m_Horizontal_3(),
	ScrollRect_t4137855814::get_offset_of_m_Vertical_4(),
	ScrollRect_t4137855814::get_offset_of_m_MovementType_5(),
	ScrollRect_t4137855814::get_offset_of_m_Elasticity_6(),
	ScrollRect_t4137855814::get_offset_of_m_Inertia_7(),
	ScrollRect_t4137855814::get_offset_of_m_DecelerationRate_8(),
	ScrollRect_t4137855814::get_offset_of_m_ScrollSensitivity_9(),
	ScrollRect_t4137855814::get_offset_of_m_Viewport_10(),
	ScrollRect_t4137855814::get_offset_of_m_HorizontalScrollbar_11(),
	ScrollRect_t4137855814::get_offset_of_m_VerticalScrollbar_12(),
	ScrollRect_t4137855814::get_offset_of_m_HorizontalScrollbarVisibility_13(),
	ScrollRect_t4137855814::get_offset_of_m_VerticalScrollbarVisibility_14(),
	ScrollRect_t4137855814::get_offset_of_m_HorizontalScrollbarSpacing_15(),
	ScrollRect_t4137855814::get_offset_of_m_VerticalScrollbarSpacing_16(),
	ScrollRect_t4137855814::get_offset_of_m_OnValueChanged_17(),
	ScrollRect_t4137855814::get_offset_of_m_PointerStartLocalCursor_18(),
	ScrollRect_t4137855814::get_offset_of_m_ContentStartPosition_19(),
	ScrollRect_t4137855814::get_offset_of_m_ViewRect_20(),
	ScrollRect_t4137855814::get_offset_of_m_ContentBounds_21(),
	ScrollRect_t4137855814::get_offset_of_m_ViewBounds_22(),
	ScrollRect_t4137855814::get_offset_of_m_Velocity_23(),
	ScrollRect_t4137855814::get_offset_of_m_Dragging_24(),
	ScrollRect_t4137855814::get_offset_of_m_PrevPosition_25(),
	ScrollRect_t4137855814::get_offset_of_m_PrevContentBounds_26(),
	ScrollRect_t4137855814::get_offset_of_m_PrevViewBounds_27(),
	ScrollRect_t4137855814::get_offset_of_m_HasRebuiltLayout_28(),
	ScrollRect_t4137855814::get_offset_of_m_HSliderExpand_29(),
	ScrollRect_t4137855814::get_offset_of_m_VSliderExpand_30(),
	ScrollRect_t4137855814::get_offset_of_m_HSliderHeight_31(),
	ScrollRect_t4137855814::get_offset_of_m_VSliderWidth_32(),
	ScrollRect_t4137855814::get_offset_of_m_Rect_33(),
	ScrollRect_t4137855814::get_offset_of_m_HorizontalScrollbarRect_34(),
	ScrollRect_t4137855814::get_offset_of_m_VerticalScrollbarRect_35(),
	ScrollRect_t4137855814::get_offset_of_m_Tracker_36(),
	ScrollRect_t4137855814::get_offset_of_m_Corners_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5516 = { sizeof (MovementType_t4072922106)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5516[4] = 
{
	MovementType_t4072922106::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5517 = { sizeof (ScrollbarVisibility_t705693775)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5517[4] = 
{
	ScrollbarVisibility_t705693775::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5518 = { sizeof (ScrollRectEvent_t343079324), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5519 = { sizeof (Selectable_t3250028441), -1, sizeof(Selectable_t3250028441_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5519[14] = 
{
	Selectable_t3250028441_StaticFields::get_offset_of_s_List_2(),
	Selectable_t3250028441::get_offset_of_m_Navigation_3(),
	Selectable_t3250028441::get_offset_of_m_Transition_4(),
	Selectable_t3250028441::get_offset_of_m_Colors_5(),
	Selectable_t3250028441::get_offset_of_m_SpriteState_6(),
	Selectable_t3250028441::get_offset_of_m_AnimationTriggers_7(),
	Selectable_t3250028441::get_offset_of_m_Interactable_8(),
	Selectable_t3250028441::get_offset_of_m_TargetGraphic_9(),
	Selectable_t3250028441::get_offset_of_m_GroupsAllowInteraction_10(),
	Selectable_t3250028441::get_offset_of_m_CurrentSelectionState_11(),
	Selectable_t3250028441::get_offset_of_U3CisPointerInsideU3Ek__BackingField_12(),
	Selectable_t3250028441::get_offset_of_U3CisPointerDownU3Ek__BackingField_13(),
	Selectable_t3250028441::get_offset_of_U3ChasSelectionU3Ek__BackingField_14(),
	Selectable_t3250028441::get_offset_of_m_CanvasGroupCache_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5520 = { sizeof (Transition_t1769908631)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5520[5] = 
{
	Transition_t1769908631::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5521 = { sizeof (SelectionState_t2656606514)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5521[5] = 
{
	SelectionState_t2656606514::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5522 = { sizeof (SetPropertyUtility_t3359423571), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5523 = { sizeof (Slider_t3903728902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5523[15] = 
{
	Slider_t3903728902::get_offset_of_m_FillRect_16(),
	Slider_t3903728902::get_offset_of_m_HandleRect_17(),
	Slider_t3903728902::get_offset_of_m_Direction_18(),
	Slider_t3903728902::get_offset_of_m_MinValue_19(),
	Slider_t3903728902::get_offset_of_m_MaxValue_20(),
	Slider_t3903728902::get_offset_of_m_WholeNumbers_21(),
	Slider_t3903728902::get_offset_of_m_Value_22(),
	Slider_t3903728902::get_offset_of_m_OnValueChanged_23(),
	Slider_t3903728902::get_offset_of_m_FillImage_24(),
	Slider_t3903728902::get_offset_of_m_FillTransform_25(),
	Slider_t3903728902::get_offset_of_m_FillContainerRect_26(),
	Slider_t3903728902::get_offset_of_m_HandleTransform_27(),
	Slider_t3903728902::get_offset_of_m_HandleContainerRect_28(),
	Slider_t3903728902::get_offset_of_m_Offset_29(),
	Slider_t3903728902::get_offset_of_m_Tracker_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5524 = { sizeof (Direction_t337909235)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5524[5] = 
{
	Direction_t337909235::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5525 = { sizeof (SliderEvent_t3180273144), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5526 = { sizeof (Axis_t809944411)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5526[3] = 
{
	Axis_t809944411::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5527 = { sizeof (SpriteState_t1362986479)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5527[3] = 
{
	SpriteState_t1362986479::get_offset_of_m_HighlightedSprite_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteState_t1362986479::get_offset_of_m_PressedSprite_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteState_t1362986479::get_offset_of_m_DisabledSprite_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5528 = { sizeof (StencilMaterial_t3850132571), -1, sizeof(StencilMaterial_t3850132571_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5528[1] = 
{
	StencilMaterial_t3850132571_StaticFields::get_offset_of_m_List_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5529 = { sizeof (MatEntry_t2957107092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5529[10] = 
{
	MatEntry_t2957107092::get_offset_of_baseMat_0(),
	MatEntry_t2957107092::get_offset_of_customMat_1(),
	MatEntry_t2957107092::get_offset_of_count_2(),
	MatEntry_t2957107092::get_offset_of_stencilId_3(),
	MatEntry_t2957107092::get_offset_of_operation_4(),
	MatEntry_t2957107092::get_offset_of_compareFunction_5(),
	MatEntry_t2957107092::get_offset_of_readMask_6(),
	MatEntry_t2957107092::get_offset_of_writeMask_7(),
	MatEntry_t2957107092::get_offset_of_useAlphaClip_8(),
	MatEntry_t2957107092::get_offset_of_colorMask_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5530 = { sizeof (Text_t1901882714), -1, sizeof(Text_t1901882714_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5530[7] = 
{
	Text_t1901882714::get_offset_of_m_FontData_28(),
	Text_t1901882714::get_offset_of_m_Text_29(),
	Text_t1901882714::get_offset_of_m_TextCache_30(),
	Text_t1901882714::get_offset_of_m_TextCacheForLayout_31(),
	Text_t1901882714_StaticFields::get_offset_of_s_DefaultText_32(),
	Text_t1901882714::get_offset_of_m_DisableFontTextureRebuiltCallback_33(),
	Text_t1901882714::get_offset_of_m_TempVerts_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5531 = { sizeof (Toggle_t2735377061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5531[5] = 
{
	Toggle_t2735377061::get_offset_of_toggleTransition_16(),
	Toggle_t2735377061::get_offset_of_graphic_17(),
	Toggle_t2735377061::get_offset_of_m_Group_18(),
	Toggle_t2735377061::get_offset_of_onValueChanged_19(),
	Toggle_t2735377061::get_offset_of_m_IsOn_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5532 = { sizeof (ToggleTransition_t3587297765)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5532[3] = 
{
	ToggleTransition_t3587297765::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5533 = { sizeof (ToggleEvent_t1873685584), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5534 = { sizeof (ToggleGroup_t123837990), -1, sizeof(ToggleGroup_t123837990_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5534[4] = 
{
	ToggleGroup_t123837990::get_offset_of_m_AllowSwitchOff_2(),
	ToggleGroup_t123837990::get_offset_of_m_Toggles_3(),
	ToggleGroup_t123837990_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	ToggleGroup_t123837990_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5535 = { sizeof (ClipperRegistry_t2428680409), -1, sizeof(ClipperRegistry_t2428680409_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5535[2] = 
{
	ClipperRegistry_t2428680409_StaticFields::get_offset_of_s_Instance_0(),
	ClipperRegistry_t2428680409::get_offset_of_m_Clippers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5536 = { sizeof (Clipping_t312708592), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5537 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5538 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5539 = { sizeof (RectangularVertexClipper_t626611136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5539[2] = 
{
	RectangularVertexClipper_t626611136::get_offset_of_m_WorldCorners_0(),
	RectangularVertexClipper_t626611136::get_offset_of_m_CanvasCorners_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5540 = { sizeof (AspectRatioFitter_t3312407083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5540[4] = 
{
	AspectRatioFitter_t3312407083::get_offset_of_m_AspectMode_2(),
	AspectRatioFitter_t3312407083::get_offset_of_m_AspectRatio_3(),
	AspectRatioFitter_t3312407083::get_offset_of_m_Rect_4(),
	AspectRatioFitter_t3312407083::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5541 = { sizeof (AspectMode_t3417192999)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5541[6] = 
{
	AspectMode_t3417192999::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5542 = { sizeof (CanvasScaler_t2767979955), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5542[14] = 
{
	CanvasScaler_t2767979955::get_offset_of_m_UiScaleMode_2(),
	CanvasScaler_t2767979955::get_offset_of_m_ReferencePixelsPerUnit_3(),
	CanvasScaler_t2767979955::get_offset_of_m_ScaleFactor_4(),
	CanvasScaler_t2767979955::get_offset_of_m_ReferenceResolution_5(),
	CanvasScaler_t2767979955::get_offset_of_m_ScreenMatchMode_6(),
	CanvasScaler_t2767979955::get_offset_of_m_MatchWidthOrHeight_7(),
	0,
	CanvasScaler_t2767979955::get_offset_of_m_PhysicalUnit_9(),
	CanvasScaler_t2767979955::get_offset_of_m_FallbackScreenDPI_10(),
	CanvasScaler_t2767979955::get_offset_of_m_DefaultSpriteDPI_11(),
	CanvasScaler_t2767979955::get_offset_of_m_DynamicPixelsPerUnit_12(),
	CanvasScaler_t2767979955::get_offset_of_m_Canvas_13(),
	CanvasScaler_t2767979955::get_offset_of_m_PrevScaleFactor_14(),
	CanvasScaler_t2767979955::get_offset_of_m_PrevReferencePixelsPerUnit_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5543 = { sizeof (ScaleMode_t2604066427)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5543[4] = 
{
	ScaleMode_t2604066427::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5544 = { sizeof (ScreenMatchMode_t3675272090)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5544[4] = 
{
	ScreenMatchMode_t3675272090::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5545 = { sizeof (Unit_t2218508340)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5545[6] = 
{
	Unit_t2218508340::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5546 = { sizeof (ContentSizeFitter_t3850442145), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5546[4] = 
{
	ContentSizeFitter_t3850442145::get_offset_of_m_HorizontalFit_2(),
	ContentSizeFitter_t3850442145::get_offset_of_m_VerticalFit_3(),
	ContentSizeFitter_t3850442145::get_offset_of_m_Rect_4(),
	ContentSizeFitter_t3850442145::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5547 = { sizeof (FitMode_t3267881214)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5547[4] = 
{
	FitMode_t3267881214::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5548 = { sizeof (GridLayoutGroup_t3046220461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5548[6] = 
{
	GridLayoutGroup_t3046220461::get_offset_of_m_StartCorner_10(),
	GridLayoutGroup_t3046220461::get_offset_of_m_StartAxis_11(),
	GridLayoutGroup_t3046220461::get_offset_of_m_CellSize_12(),
	GridLayoutGroup_t3046220461::get_offset_of_m_Spacing_13(),
	GridLayoutGroup_t3046220461::get_offset_of_m_Constraint_14(),
	GridLayoutGroup_t3046220461::get_offset_of_m_ConstraintCount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5549 = { sizeof (Corner_t1493259673)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5549[5] = 
{
	Corner_t1493259673::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5550 = { sizeof (Axis_t3613393006)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5550[3] = 
{
	Axis_t3613393006::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5551 = { sizeof (Constraint_t814224393)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5551[4] = 
{
	Constraint_t814224393::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5552 = { sizeof (HorizontalLayoutGroup_t2586782146), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5553 = { sizeof (HorizontalOrVerticalLayoutGroup_t729725570), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5553[5] = 
{
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_Spacing_10(),
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_ChildForceExpandWidth_11(),
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_ChildForceExpandHeight_12(),
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_ChildControlWidth_13(),
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_ChildControlHeight_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5554 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5555 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5556 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5557 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5558 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5559 = { sizeof (LayoutElement_t1785403678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5559[7] = 
{
	LayoutElement_t1785403678::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t1785403678::get_offset_of_m_MinWidth_3(),
	LayoutElement_t1785403678::get_offset_of_m_MinHeight_4(),
	LayoutElement_t1785403678::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t1785403678::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t1785403678::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t1785403678::get_offset_of_m_FlexibleHeight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5560 = { sizeof (LayoutGroup_t2436138090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5560[8] = 
{
	LayoutGroup_t2436138090::get_offset_of_m_Padding_2(),
	LayoutGroup_t2436138090::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t2436138090::get_offset_of_m_Rect_4(),
	LayoutGroup_t2436138090::get_offset_of_m_Tracker_5(),
	LayoutGroup_t2436138090::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t2436138090::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t2436138090::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t2436138090::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5561 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5561[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5562 = { sizeof (LayoutRebuilder_t541313304), -1, sizeof(LayoutRebuilder_t541313304_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5562[9] = 
{
	LayoutRebuilder_t541313304::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t541313304::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5563 = { sizeof (LayoutUtility_t2745813735), -1, sizeof(LayoutUtility_t2745813735_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5563[8] = 
{
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5564 = { sizeof (VerticalLayoutGroup_t923838031), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5565 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5566 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5566[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5567 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5567[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5568 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5568[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5569 = { sizeof (ReflectionMethodsCache_t2103211062), -1, sizeof(ReflectionMethodsCache_t2103211062_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5569[5] = 
{
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t2103211062::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t2103211062_StaticFields::get_offset_of_s_ReflectionMethodsCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5570 = { sizeof (Raycast3DCallback_t701940803), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5571 = { sizeof (Raycast2DCallback_t768590915), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5572 = { sizeof (RaycastAllCallback_t1884415901), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5573 = { sizeof (GetRayIntersectionAllCallback_t3913627115), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5574 = { sizeof (VertexHelper_t2453304189), -1, sizeof(VertexHelper_t2453304189_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5574[11] = 
{
	VertexHelper_t2453304189::get_offset_of_m_Positions_0(),
	VertexHelper_t2453304189::get_offset_of_m_Colors_1(),
	VertexHelper_t2453304189::get_offset_of_m_Uv0S_2(),
	VertexHelper_t2453304189::get_offset_of_m_Uv1S_3(),
	VertexHelper_t2453304189::get_offset_of_m_Uv2S_4(),
	VertexHelper_t2453304189::get_offset_of_m_Uv3S_5(),
	VertexHelper_t2453304189::get_offset_of_m_Normals_6(),
	VertexHelper_t2453304189::get_offset_of_m_Tangents_7(),
	VertexHelper_t2453304189::get_offset_of_m_Indices_8(),
	VertexHelper_t2453304189_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t2453304189_StaticFields::get_offset_of_s_DefaultNormal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5575 = { sizeof (BaseVertexEffect_t2675891272), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5576 = { sizeof (BaseMeshEffect_t2440176439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5576[1] = 
{
	BaseMeshEffect_t2440176439::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5577 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5578 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5579 = { sizeof (Outline_t2536100125), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5580 = { sizeof (PositionAsUV1_t3991086357), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5581 = { sizeof (Shadow_t773074319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5581[4] = 
{
	Shadow_t773074319::get_offset_of_m_EffectColor_3(),
	Shadow_t773074319::get_offset_of_m_EffectDistance_4(),
	Shadow_t773074319::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5582 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255373), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255373_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5582[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255373_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5583 = { sizeof (U24ArrayTypeU3D12_t2488454196)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t2488454196 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5584 = { sizeof (U3CModuleU3E_t692745556), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5585 = { sizeof (VuforiaNativeIosWrapper_t2037146173), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5586 = { sizeof (U3CModuleU3E_t692745557), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5587 = { sizeof (BtConnection_t2892495216), -1, sizeof(BtConnection_t2892495216_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5587[14] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	BtConnection_t2892495216_StaticFields::get_offset_of_ajc_12(),
	BtConnection_t2892495216_StaticFields::get_offset_of_PluginReady_13(),
	0,
	BtConnection_t2892495216_StaticFields::get_offset_of_modeNumber_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5588 = { sizeof (BtConnector_t901688599), -1, sizeof(BtConnector_t901688599_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5588[11] = 
{
	BtConnector_t901688599_StaticFields::get_offset_of_message_2(),
	BtConnector_t901688599_StaticFields::get_offset_of_byteMessage_3(),
	BtConnector_t901688599::get_offset_of_stopReading_4(),
	BtConnector_t901688599::get_offset_of_mode0_5(),
	BtConnector_t901688599::get_offset_of_mode2_6(),
	BtConnector_t901688599::get_offset_of_mode3_7(),
	BtConnector_t901688599::get_offset_of_length_8(),
	BtConnector_t901688599::get_offset_of_terminalByte_9(),
	BtConnector_t901688599_StaticFields::get_offset_of_dataAvailable_10(),
	BtConnector_t901688599_StaticFields::get_offset_of_connected_11(),
	BtConnector_t901688599_StaticFields::get_offset_of_isDevicePicked_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5589 = { sizeof (Highlighter_t672210414), -1, sizeof(Highlighter_t672210414_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5589[39] = 
{
	Highlighter_t672210414_StaticFields::get_offset_of_constantOnSpeed_2(),
	Highlighter_t672210414_StaticFields::get_offset_of_constantOffSpeed_3(),
	Highlighter_t672210414_StaticFields::get_offset_of_transparentCutoff_4(),
	0,
	Highlighter_t672210414_StaticFields::get_offset_of_types_6(),
	0,
	Highlighter_t672210414::get_offset_of_occluderColor_8(),
	0,
	0,
	0,
	Highlighter_t672210414_StaticFields::get_offset_of_zWrite_12(),
	Highlighter_t672210414_StaticFields::get_offset_of_offsetFactor_13(),
	Highlighter_t672210414_StaticFields::get_offset_of_offsetUnits_14(),
	Highlighter_t672210414::get_offset_of_U3ChighlightedU3Ek__BackingField_15(),
	Highlighter_t672210414::get_offset_of_tr_16(),
	Highlighter_t672210414::get_offset_of_highlightableRenderers_17(),
	Highlighter_t672210414::get_offset_of_visibilityCheckFrame_18(),
	Highlighter_t672210414::get_offset_of_visibilityChanged_19(),
	Highlighter_t672210414::get_offset_of_visible_20(),
	Highlighter_t672210414::get_offset_of_renderersDirty_21(),
	Highlighter_t672210414::get_offset_of_currentColor_22(),
	Highlighter_t672210414::get_offset_of_transitionActive_23(),
	Highlighter_t672210414::get_offset_of_transitionValue_24(),
	Highlighter_t672210414::get_offset_of_flashingFreq_25(),
	Highlighter_t672210414::get_offset_of__once_26(),
	Highlighter_t672210414::get_offset_of_onceColor_27(),
	Highlighter_t672210414::get_offset_of_flashing_28(),
	Highlighter_t672210414::get_offset_of_flashingColorMin_29(),
	Highlighter_t672210414::get_offset_of_flashingColorMax_30(),
	Highlighter_t672210414::get_offset_of_constantly_31(),
	Highlighter_t672210414::get_offset_of_constantColor_32(),
	Highlighter_t672210414::get_offset_of_occluder_33(),
	Highlighter_t672210414::get_offset_of_seeThrough_34(),
	Highlighter_t672210414::get_offset_of_renderQueue_35(),
	Highlighter_t672210414::get_offset_of_zTest_36(),
	Highlighter_t672210414::get_offset_of_stencilRef_37(),
	Highlighter_t672210414_StaticFields::get_offset_of__opaqueShader_38(),
	Highlighter_t672210414_StaticFields::get_offset_of__transparentShader_39(),
	Highlighter_t672210414::get_offset_of__opaqueMaterial_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5590 = { sizeof (RendererCache_t1904069300), -1, sizeof(RendererCache_t1904069300_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5590[11] = 
{
	RendererCache_t1904069300_StaticFields::get_offset_of_sRenderType_0(),
	RendererCache_t1904069300_StaticFields::get_offset_of_sOpaque_1(),
	RendererCache_t1904069300_StaticFields::get_offset_of_sTransparent_2(),
	RendererCache_t1904069300_StaticFields::get_offset_of_sTransparentCutout_3(),
	RendererCache_t1904069300_StaticFields::get_offset_of_sMainTex_4(),
	0,
	0,
	RendererCache_t1904069300::get_offset_of_U3CvisibleU3Ek__BackingField_7(),
	RendererCache_t1904069300::get_offset_of_go_8(),
	RendererCache_t1904069300::get_offset_of_renderer_9(),
	RendererCache_t1904069300::get_offset_of_data_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5591 = { sizeof (Data_t1588725102)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5591[3] = 
{
	Data_t1588725102::get_offset_of_material_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Data_t1588725102::get_offset_of_submeshIndex_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Data_t1588725102::get_offset_of_transparent_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5592 = { sizeof (HighlightingRenderer_t1923179915), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5593 = { sizeof (HighlighterManager_t981364519), -1, sizeof(HighlighterManager_t981364519_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5593[2] = 
{
	HighlighterManager_t981364519_StaticFields::get_offset_of_dirtyFrame_0(),
	HighlighterManager_t981364519_StaticFields::get_offset_of_highlighters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5594 = { sizeof (HighlightingBase_t582374880), -1, sizeof(HighlightingBase_t582374880_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5594[37] = 
{
	HighlightingBase_t582374880_StaticFields::get_offset_of_colorClear_2(),
	HighlightingBase_t582374880_StaticFields::get_offset_of_renderBufferName_3(),
	HighlightingBase_t582374880_StaticFields::get_offset_of_identityMatrix_4(),
	0,
	HighlightingBase_t582374880_StaticFields::get_offset_of_cameraTargetID_6(),
	HighlightingBase_t582374880_StaticFields::get_offset_of_quad_7(),
	0,
	0,
	0,
	HighlightingBase_t582374880_StaticFields::get_offset_of_graphicsDeviceVersion_11(),
	HighlightingBase_t582374880::get_offset_of_offsetFactor_12(),
	HighlightingBase_t582374880::get_offset_of_offsetUnits_13(),
	HighlightingBase_t582374880::get_offset_of_renderBuffer_14(),
	HighlightingBase_t582374880::get_offset_of_isDirty_15(),
	HighlightingBase_t582374880::get_offset_of_cachedWidth_16(),
	HighlightingBase_t582374880::get_offset_of_cachedHeight_17(),
	HighlightingBase_t582374880::get_offset_of_cachedAA_18(),
	HighlightingBase_t582374880::get_offset_of__downsampleFactor_19(),
	HighlightingBase_t582374880::get_offset_of__iterations_20(),
	HighlightingBase_t582374880::get_offset_of__blurMinSpread_21(),
	HighlightingBase_t582374880::get_offset_of__blurSpread_22(),
	HighlightingBase_t582374880::get_offset_of__blurIntensity_23(),
	HighlightingBase_t582374880::get_offset_of_highlightingBufferID_24(),
	HighlightingBase_t582374880::get_offset_of_highlightingBuffer_25(),
	HighlightingBase_t582374880::get_offset_of_cam_26(),
	HighlightingBase_t582374880::get_offset_of_isSupported_27(),
	HighlightingBase_t582374880::get_offset_of_isDepthAvailable_28(),
	0,
	0,
	0,
	HighlightingBase_t582374880_StaticFields::get_offset_of_shaderPaths_32(),
	HighlightingBase_t582374880_StaticFields::get_offset_of_shaders_33(),
	HighlightingBase_t582374880_StaticFields::get_offset_of_materials_34(),
	HighlightingBase_t582374880_StaticFields::get_offset_of_cutMaterial_35(),
	HighlightingBase_t582374880_StaticFields::get_offset_of_compMaterial_36(),
	HighlightingBase_t582374880::get_offset_of_blurMaterial_37(),
	HighlightingBase_t582374880_StaticFields::get_offset_of_initialized_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5595 = { sizeof (ShaderPropertyID_t567755140), -1, sizeof(ShaderPropertyID_t567755140_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5595[17] = 
{
	ShaderPropertyID_t567755140_StaticFields::get_offset_of_U3C_MainTexU3Ek__BackingField_0(),
	ShaderPropertyID_t567755140_StaticFields::get_offset_of_U3C_OutlineU3Ek__BackingField_1(),
	ShaderPropertyID_t567755140_StaticFields::get_offset_of_U3C_CutoffU3Ek__BackingField_2(),
	ShaderPropertyID_t567755140_StaticFields::get_offset_of_U3C_IntensityU3Ek__BackingField_3(),
	ShaderPropertyID_t567755140_StaticFields::get_offset_of_U3C_ZTestU3Ek__BackingField_4(),
	ShaderPropertyID_t567755140_StaticFields::get_offset_of_U3C_StencilRefU3Ek__BackingField_5(),
	ShaderPropertyID_t567755140_StaticFields::get_offset_of_U3C_CullU3Ek__BackingField_6(),
	ShaderPropertyID_t567755140_StaticFields::get_offset_of_U3C_HighlightingBlur1U3Ek__BackingField_7(),
	ShaderPropertyID_t567755140_StaticFields::get_offset_of_U3C_HighlightingBlur2U3Ek__BackingField_8(),
	ShaderPropertyID_t567755140_StaticFields::get_offset_of_U3C_HighlightingBufferU3Ek__BackingField_9(),
	ShaderPropertyID_t567755140_StaticFields::get_offset_of_U3C_HighlightingBlurredU3Ek__BackingField_10(),
	ShaderPropertyID_t567755140_StaticFields::get_offset_of_U3C_HighlightingBlurOffsetU3Ek__BackingField_11(),
	ShaderPropertyID_t567755140_StaticFields::get_offset_of_U3C_HighlightingZWriteU3Ek__BackingField_12(),
	ShaderPropertyID_t567755140_StaticFields::get_offset_of_U3C_HighlightingOffsetFactorU3Ek__BackingField_13(),
	ShaderPropertyID_t567755140_StaticFields::get_offset_of_U3C_HighlightingOffsetUnitsU3Ek__BackingField_14(),
	ShaderPropertyID_t567755140_StaticFields::get_offset_of_U3C_HighlightingBufferTexelSizeU3Ek__BackingField_15(),
	ShaderPropertyID_t567755140_StaticFields::get_offset_of_initialized_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5596 = { sizeof (MoveSample_t3412539464), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5597 = { sizeof (RotateSample_t3002381122), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5598 = { sizeof (SampleInfo_t3693012684), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5599 = { sizeof (iTween_t770867771), -1, sizeof(iTween_t770867771_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5599[38] = 
{
	iTween_t770867771_StaticFields::get_offset_of_tweens_2(),
	iTween_t770867771_StaticFields::get_offset_of_cameraFade_3(),
	iTween_t770867771::get_offset_of_id_4(),
	iTween_t770867771::get_offset_of_type_5(),
	iTween_t770867771::get_offset_of_method_6(),
	iTween_t770867771::get_offset_of_easeType_7(),
	iTween_t770867771::get_offset_of_time_8(),
	iTween_t770867771::get_offset_of_delay_9(),
	iTween_t770867771::get_offset_of_loopType_10(),
	iTween_t770867771::get_offset_of_isRunning_11(),
	iTween_t770867771::get_offset_of_isPaused_12(),
	iTween_t770867771::get_offset_of__name_13(),
	iTween_t770867771::get_offset_of_runningTime_14(),
	iTween_t770867771::get_offset_of_percentage_15(),
	iTween_t770867771::get_offset_of_delayStarted_16(),
	iTween_t770867771::get_offset_of_isLocal_17(),
	iTween_t770867771::get_offset_of_loop_18(),
	iTween_t770867771::get_offset_of_reverse_19(),
	iTween_t770867771::get_offset_of_wasPaused_20(),
	iTween_t770867771::get_offset_of_physics_21(),
	iTween_t770867771::get_offset_of_tweenArguments_22(),
	iTween_t770867771::get_offset_of_space_23(),
	iTween_t770867771::get_offset_of_ease_24(),
	iTween_t770867771::get_offset_of_apply_25(),
	iTween_t770867771::get_offset_of_audioSource_26(),
	iTween_t770867771::get_offset_of_vector3s_27(),
	iTween_t770867771::get_offset_of_vector2s_28(),
	iTween_t770867771::get_offset_of_colors_29(),
	iTween_t770867771::get_offset_of_floats_30(),
	iTween_t770867771::get_offset_of_rects_31(),
	iTween_t770867771::get_offset_of_path_32(),
	iTween_t770867771::get_offset_of_preUpdate_33(),
	iTween_t770867771::get_offset_of_postUpdate_34(),
	iTween_t770867771::get_offset_of_namedcolorvalue_35(),
	iTween_t770867771::get_offset_of_lastRealTime_36(),
	iTween_t770867771::get_offset_of_useRealTime_37(),
	iTween_t770867771::get_offset_of_thisTransform_38(),
	iTween_t770867771_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_39(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
