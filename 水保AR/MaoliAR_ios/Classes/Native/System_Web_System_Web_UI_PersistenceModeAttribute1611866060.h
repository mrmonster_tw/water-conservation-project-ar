﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"
#include "System_Web_System_Web_UI_PersistenceMode2222411918.h"

// System.Web.UI.PersistenceModeAttribute
struct PersistenceModeAttribute_t1611866060;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.PersistenceModeAttribute
struct  PersistenceModeAttribute_t1611866060  : public Attribute_t861562559
{
public:
	// System.Web.UI.PersistenceMode System.Web.UI.PersistenceModeAttribute::mode
	int32_t ___mode_0;

public:
	inline static int32_t get_offset_of_mode_0() { return static_cast<int32_t>(offsetof(PersistenceModeAttribute_t1611866060, ___mode_0)); }
	inline int32_t get_mode_0() const { return ___mode_0; }
	inline int32_t* get_address_of_mode_0() { return &___mode_0; }
	inline void set_mode_0(int32_t value)
	{
		___mode_0 = value;
	}
};

struct PersistenceModeAttribute_t1611866060_StaticFields
{
public:
	// System.Web.UI.PersistenceModeAttribute System.Web.UI.PersistenceModeAttribute::Attribute
	PersistenceModeAttribute_t1611866060 * ___Attribute_1;
	// System.Web.UI.PersistenceModeAttribute System.Web.UI.PersistenceModeAttribute::Default
	PersistenceModeAttribute_t1611866060 * ___Default_2;
	// System.Web.UI.PersistenceModeAttribute System.Web.UI.PersistenceModeAttribute::EncodedInnerDefaultProperty
	PersistenceModeAttribute_t1611866060 * ___EncodedInnerDefaultProperty_3;
	// System.Web.UI.PersistenceModeAttribute System.Web.UI.PersistenceModeAttribute::InnerDefaultProperty
	PersistenceModeAttribute_t1611866060 * ___InnerDefaultProperty_4;
	// System.Web.UI.PersistenceModeAttribute System.Web.UI.PersistenceModeAttribute::InnerProperty
	PersistenceModeAttribute_t1611866060 * ___InnerProperty_5;

public:
	inline static int32_t get_offset_of_Attribute_1() { return static_cast<int32_t>(offsetof(PersistenceModeAttribute_t1611866060_StaticFields, ___Attribute_1)); }
	inline PersistenceModeAttribute_t1611866060 * get_Attribute_1() const { return ___Attribute_1; }
	inline PersistenceModeAttribute_t1611866060 ** get_address_of_Attribute_1() { return &___Attribute_1; }
	inline void set_Attribute_1(PersistenceModeAttribute_t1611866060 * value)
	{
		___Attribute_1 = value;
		Il2CppCodeGenWriteBarrier(&___Attribute_1, value);
	}

	inline static int32_t get_offset_of_Default_2() { return static_cast<int32_t>(offsetof(PersistenceModeAttribute_t1611866060_StaticFields, ___Default_2)); }
	inline PersistenceModeAttribute_t1611866060 * get_Default_2() const { return ___Default_2; }
	inline PersistenceModeAttribute_t1611866060 ** get_address_of_Default_2() { return &___Default_2; }
	inline void set_Default_2(PersistenceModeAttribute_t1611866060 * value)
	{
		___Default_2 = value;
		Il2CppCodeGenWriteBarrier(&___Default_2, value);
	}

	inline static int32_t get_offset_of_EncodedInnerDefaultProperty_3() { return static_cast<int32_t>(offsetof(PersistenceModeAttribute_t1611866060_StaticFields, ___EncodedInnerDefaultProperty_3)); }
	inline PersistenceModeAttribute_t1611866060 * get_EncodedInnerDefaultProperty_3() const { return ___EncodedInnerDefaultProperty_3; }
	inline PersistenceModeAttribute_t1611866060 ** get_address_of_EncodedInnerDefaultProperty_3() { return &___EncodedInnerDefaultProperty_3; }
	inline void set_EncodedInnerDefaultProperty_3(PersistenceModeAttribute_t1611866060 * value)
	{
		___EncodedInnerDefaultProperty_3 = value;
		Il2CppCodeGenWriteBarrier(&___EncodedInnerDefaultProperty_3, value);
	}

	inline static int32_t get_offset_of_InnerDefaultProperty_4() { return static_cast<int32_t>(offsetof(PersistenceModeAttribute_t1611866060_StaticFields, ___InnerDefaultProperty_4)); }
	inline PersistenceModeAttribute_t1611866060 * get_InnerDefaultProperty_4() const { return ___InnerDefaultProperty_4; }
	inline PersistenceModeAttribute_t1611866060 ** get_address_of_InnerDefaultProperty_4() { return &___InnerDefaultProperty_4; }
	inline void set_InnerDefaultProperty_4(PersistenceModeAttribute_t1611866060 * value)
	{
		___InnerDefaultProperty_4 = value;
		Il2CppCodeGenWriteBarrier(&___InnerDefaultProperty_4, value);
	}

	inline static int32_t get_offset_of_InnerProperty_5() { return static_cast<int32_t>(offsetof(PersistenceModeAttribute_t1611866060_StaticFields, ___InnerProperty_5)); }
	inline PersistenceModeAttribute_t1611866060 * get_InnerProperty_5() const { return ___InnerProperty_5; }
	inline PersistenceModeAttribute_t1611866060 ** get_address_of_InnerProperty_5() { return &___InnerProperty_5; }
	inline void set_InnerProperty_5(PersistenceModeAttribute_t1611866060 * value)
	{
		___InnerProperty_5 = value;
		Il2CppCodeGenWriteBarrier(&___InnerProperty_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
