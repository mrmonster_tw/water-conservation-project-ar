﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"

// System.Xml.UniqueId
struct UniqueId_t1383576913;
// System.Collections.Generic.Dictionary`2<System.Xml.UniqueId,System.ServiceModel.Security.Tokens.SecurityContextSecurityToken>
struct Dictionary_2_t3105478127;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Xml.UniqueId,System.Collections.Generic.Dictionary`2<System.Xml.UniqueId,System.ServiceModel.Security.Tokens.SecurityContextSecurityToken>>
struct  KeyValuePair_2_t688881393 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	UniqueId_t1383576913 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	Dictionary_2_t3105478127 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t688881393, ___key_0)); }
	inline UniqueId_t1383576913 * get_key_0() const { return ___key_0; }
	inline UniqueId_t1383576913 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(UniqueId_t1383576913 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier(&___key_0, value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t688881393, ___value_1)); }
	inline Dictionary_2_t3105478127 * get_value_1() const { return ___value_1; }
	inline Dictionary_2_t3105478127 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Dictionary_2_t3105478127 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
