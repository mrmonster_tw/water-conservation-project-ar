﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_C1035805278.h"

// System.ServiceModel.Channels.IChannelListener`1<System.ServiceModel.Channels.IReplyChannel>
struct IChannelListener_1_t3314941542;
// System.ServiceModel.Channels.RecipientMessageSecurityBindingSupport
struct RecipientMessageSecurityBindingSupport_t1155974079;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.SecurityChannelListener`1<System.ServiceModel.Channels.IReplyChannel>
struct  SecurityChannelListener_1_t1345969408  : public ChannelListenerBase_1_t1035805278
{
public:
	// System.ServiceModel.Channels.IChannelListener`1<TChannel> System.ServiceModel.Channels.SecurityChannelListener`1::inner
	Il2CppObject* ___inner_12;
	// System.ServiceModel.Channels.RecipientMessageSecurityBindingSupport System.ServiceModel.Channels.SecurityChannelListener`1::security
	RecipientMessageSecurityBindingSupport_t1155974079 * ___security_13;

public:
	inline static int32_t get_offset_of_inner_12() { return static_cast<int32_t>(offsetof(SecurityChannelListener_1_t1345969408, ___inner_12)); }
	inline Il2CppObject* get_inner_12() const { return ___inner_12; }
	inline Il2CppObject** get_address_of_inner_12() { return &___inner_12; }
	inline void set_inner_12(Il2CppObject* value)
	{
		___inner_12 = value;
		Il2CppCodeGenWriteBarrier(&___inner_12, value);
	}

	inline static int32_t get_offset_of_security_13() { return static_cast<int32_t>(offsetof(SecurityChannelListener_1_t1345969408, ___security_13)); }
	inline RecipientMessageSecurityBindingSupport_t1155974079 * get_security_13() const { return ___security_13; }
	inline RecipientMessageSecurityBindingSupport_t1155974079 ** get_address_of_security_13() { return &___security_13; }
	inline void set_security_13(RecipientMessageSecurityBindingSupport_t1155974079 * value)
	{
		___security_13 = value;
		Il2CppCodeGenWriteBarrier(&___security_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
