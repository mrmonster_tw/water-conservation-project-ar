﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.BeginEventHandler
struct BeginEventHandler_t3912883583;
// System.Web.EndEventHandler
struct EndEventHandler_t957584525;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.AsyncInvoker
struct  AsyncInvoker_t1593358297  : public Il2CppObject
{
public:
	// System.Web.BeginEventHandler System.Web.AsyncInvoker::begin
	BeginEventHandler_t3912883583 * ___begin_0;
	// System.Web.EndEventHandler System.Web.AsyncInvoker::end
	EndEventHandler_t957584525 * ___end_1;
	// System.Object System.Web.AsyncInvoker::data
	Il2CppObject * ___data_2;

public:
	inline static int32_t get_offset_of_begin_0() { return static_cast<int32_t>(offsetof(AsyncInvoker_t1593358297, ___begin_0)); }
	inline BeginEventHandler_t3912883583 * get_begin_0() const { return ___begin_0; }
	inline BeginEventHandler_t3912883583 ** get_address_of_begin_0() { return &___begin_0; }
	inline void set_begin_0(BeginEventHandler_t3912883583 * value)
	{
		___begin_0 = value;
		Il2CppCodeGenWriteBarrier(&___begin_0, value);
	}

	inline static int32_t get_offset_of_end_1() { return static_cast<int32_t>(offsetof(AsyncInvoker_t1593358297, ___end_1)); }
	inline EndEventHandler_t957584525 * get_end_1() const { return ___end_1; }
	inline EndEventHandler_t957584525 ** get_address_of_end_1() { return &___end_1; }
	inline void set_end_1(EndEventHandler_t957584525 * value)
	{
		___end_1 = value;
		Il2CppCodeGenWriteBarrier(&___end_1, value);
	}

	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(AsyncInvoker_t1593358297, ___data_2)); }
	inline Il2CppObject * get_data_2() const { return ___data_2; }
	inline Il2CppObject ** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(Il2CppObject * value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier(&___data_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
