﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_ScriptableObject2528358522.h"

// Vuforia.VuforiaAbstractConfiguration
struct VuforiaAbstractConfiguration_t2684447159;
// System.Object
struct Il2CppObject;
// Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration
struct GenericVuforiaConfiguration_t1909783692;
// Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration
struct DigitalEyewearConfiguration_t2828783036;
// Vuforia.VuforiaAbstractConfiguration/DatabaseLoadConfiguration
struct DatabaseLoadConfiguration_t3815674388;
// Vuforia.VuforiaAbstractConfiguration/VideoBackgroundConfiguration
struct VideoBackgroundConfiguration_t1174662728;
// Vuforia.VuforiaAbstractConfiguration/SmartTerrainTrackerConfiguration
struct SmartTerrainTrackerConfiguration_t3112457179;
// Vuforia.VuforiaAbstractConfiguration/DeviceTrackerConfiguration
struct DeviceTrackerConfiguration_t1841344395;
// Vuforia.VuforiaAbstractConfiguration/WebCamConfiguration
struct WebCamConfiguration_t802847339;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaAbstractConfiguration
struct  VuforiaAbstractConfiguration_t2684447159  : public ScriptableObject_t2528358522
{
public:
	// Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration Vuforia.VuforiaAbstractConfiguration::vuforia
	GenericVuforiaConfiguration_t1909783692 * ___vuforia_4;
	// Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration Vuforia.VuforiaAbstractConfiguration::digitalEyewear
	DigitalEyewearConfiguration_t2828783036 * ___digitalEyewear_5;
	// Vuforia.VuforiaAbstractConfiguration/DatabaseLoadConfiguration Vuforia.VuforiaAbstractConfiguration::databaseLoad
	DatabaseLoadConfiguration_t3815674388 * ___databaseLoad_6;
	// Vuforia.VuforiaAbstractConfiguration/VideoBackgroundConfiguration Vuforia.VuforiaAbstractConfiguration::videoBackground
	VideoBackgroundConfiguration_t1174662728 * ___videoBackground_7;
	// Vuforia.VuforiaAbstractConfiguration/SmartTerrainTrackerConfiguration Vuforia.VuforiaAbstractConfiguration::smartTerrainTracker
	SmartTerrainTrackerConfiguration_t3112457179 * ___smartTerrainTracker_8;
	// Vuforia.VuforiaAbstractConfiguration/DeviceTrackerConfiguration Vuforia.VuforiaAbstractConfiguration::deviceTracker
	DeviceTrackerConfiguration_t1841344395 * ___deviceTracker_9;
	// Vuforia.VuforiaAbstractConfiguration/WebCamConfiguration Vuforia.VuforiaAbstractConfiguration::webcam
	WebCamConfiguration_t802847339 * ___webcam_10;

public:
	inline static int32_t get_offset_of_vuforia_4() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t2684447159, ___vuforia_4)); }
	inline GenericVuforiaConfiguration_t1909783692 * get_vuforia_4() const { return ___vuforia_4; }
	inline GenericVuforiaConfiguration_t1909783692 ** get_address_of_vuforia_4() { return &___vuforia_4; }
	inline void set_vuforia_4(GenericVuforiaConfiguration_t1909783692 * value)
	{
		___vuforia_4 = value;
		Il2CppCodeGenWriteBarrier(&___vuforia_4, value);
	}

	inline static int32_t get_offset_of_digitalEyewear_5() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t2684447159, ___digitalEyewear_5)); }
	inline DigitalEyewearConfiguration_t2828783036 * get_digitalEyewear_5() const { return ___digitalEyewear_5; }
	inline DigitalEyewearConfiguration_t2828783036 ** get_address_of_digitalEyewear_5() { return &___digitalEyewear_5; }
	inline void set_digitalEyewear_5(DigitalEyewearConfiguration_t2828783036 * value)
	{
		___digitalEyewear_5 = value;
		Il2CppCodeGenWriteBarrier(&___digitalEyewear_5, value);
	}

	inline static int32_t get_offset_of_databaseLoad_6() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t2684447159, ___databaseLoad_6)); }
	inline DatabaseLoadConfiguration_t3815674388 * get_databaseLoad_6() const { return ___databaseLoad_6; }
	inline DatabaseLoadConfiguration_t3815674388 ** get_address_of_databaseLoad_6() { return &___databaseLoad_6; }
	inline void set_databaseLoad_6(DatabaseLoadConfiguration_t3815674388 * value)
	{
		___databaseLoad_6 = value;
		Il2CppCodeGenWriteBarrier(&___databaseLoad_6, value);
	}

	inline static int32_t get_offset_of_videoBackground_7() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t2684447159, ___videoBackground_7)); }
	inline VideoBackgroundConfiguration_t1174662728 * get_videoBackground_7() const { return ___videoBackground_7; }
	inline VideoBackgroundConfiguration_t1174662728 ** get_address_of_videoBackground_7() { return &___videoBackground_7; }
	inline void set_videoBackground_7(VideoBackgroundConfiguration_t1174662728 * value)
	{
		___videoBackground_7 = value;
		Il2CppCodeGenWriteBarrier(&___videoBackground_7, value);
	}

	inline static int32_t get_offset_of_smartTerrainTracker_8() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t2684447159, ___smartTerrainTracker_8)); }
	inline SmartTerrainTrackerConfiguration_t3112457179 * get_smartTerrainTracker_8() const { return ___smartTerrainTracker_8; }
	inline SmartTerrainTrackerConfiguration_t3112457179 ** get_address_of_smartTerrainTracker_8() { return &___smartTerrainTracker_8; }
	inline void set_smartTerrainTracker_8(SmartTerrainTrackerConfiguration_t3112457179 * value)
	{
		___smartTerrainTracker_8 = value;
		Il2CppCodeGenWriteBarrier(&___smartTerrainTracker_8, value);
	}

	inline static int32_t get_offset_of_deviceTracker_9() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t2684447159, ___deviceTracker_9)); }
	inline DeviceTrackerConfiguration_t1841344395 * get_deviceTracker_9() const { return ___deviceTracker_9; }
	inline DeviceTrackerConfiguration_t1841344395 ** get_address_of_deviceTracker_9() { return &___deviceTracker_9; }
	inline void set_deviceTracker_9(DeviceTrackerConfiguration_t1841344395 * value)
	{
		___deviceTracker_9 = value;
		Il2CppCodeGenWriteBarrier(&___deviceTracker_9, value);
	}

	inline static int32_t get_offset_of_webcam_10() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t2684447159, ___webcam_10)); }
	inline WebCamConfiguration_t802847339 * get_webcam_10() const { return ___webcam_10; }
	inline WebCamConfiguration_t802847339 ** get_address_of_webcam_10() { return &___webcam_10; }
	inline void set_webcam_10(WebCamConfiguration_t802847339 * value)
	{
		___webcam_10 = value;
		Il2CppCodeGenWriteBarrier(&___webcam_10, value);
	}
};

struct VuforiaAbstractConfiguration_t2684447159_StaticFields
{
public:
	// Vuforia.VuforiaAbstractConfiguration Vuforia.VuforiaAbstractConfiguration::mInstance
	VuforiaAbstractConfiguration_t2684447159 * ___mInstance_2;
	// System.Object Vuforia.VuforiaAbstractConfiguration::mPadlock
	Il2CppObject * ___mPadlock_3;

public:
	inline static int32_t get_offset_of_mInstance_2() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t2684447159_StaticFields, ___mInstance_2)); }
	inline VuforiaAbstractConfiguration_t2684447159 * get_mInstance_2() const { return ___mInstance_2; }
	inline VuforiaAbstractConfiguration_t2684447159 ** get_address_of_mInstance_2() { return &___mInstance_2; }
	inline void set_mInstance_2(VuforiaAbstractConfiguration_t2684447159 * value)
	{
		___mInstance_2 = value;
		Il2CppCodeGenWriteBarrier(&___mInstance_2, value);
	}

	inline static int32_t get_offset_of_mPadlock_3() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t2684447159_StaticFields, ___mPadlock_3)); }
	inline Il2CppObject * get_mPadlock_3() const { return ___mPadlock_3; }
	inline Il2CppObject ** get_address_of_mPadlock_3() { return &___mPadlock_3; }
	inline void set_mPadlock_3(Il2CppObject * value)
	{
		___mPadlock_3 = value;
		Il2CppCodeGenWriteBarrier(&___mPadlock_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
