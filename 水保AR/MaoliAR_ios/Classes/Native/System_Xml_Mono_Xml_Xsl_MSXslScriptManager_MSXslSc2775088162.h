﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Xml_Mono_Xml_Xsl_MSXslScriptManager_Scriptin290502390.h"

// System.String
struct String_t;
// System.Security.Policy.Evidence
struct Evidence_t2008144148;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.MSXslScriptManager/MSXslScript
struct  MSXslScript_t2775088162  : public Il2CppObject
{
public:
	// Mono.Xml.Xsl.MSXslScriptManager/ScriptingLanguage Mono.Xml.Xsl.MSXslScriptManager/MSXslScript::language
	int32_t ___language_0;
	// System.String Mono.Xml.Xsl.MSXslScriptManager/MSXslScript::implementsPrefix
	String_t* ___implementsPrefix_1;
	// System.String Mono.Xml.Xsl.MSXslScriptManager/MSXslScript::code
	String_t* ___code_2;
	// System.Security.Policy.Evidence Mono.Xml.Xsl.MSXslScriptManager/MSXslScript::evidence
	Evidence_t2008144148 * ___evidence_3;

public:
	inline static int32_t get_offset_of_language_0() { return static_cast<int32_t>(offsetof(MSXslScript_t2775088162, ___language_0)); }
	inline int32_t get_language_0() const { return ___language_0; }
	inline int32_t* get_address_of_language_0() { return &___language_0; }
	inline void set_language_0(int32_t value)
	{
		___language_0 = value;
	}

	inline static int32_t get_offset_of_implementsPrefix_1() { return static_cast<int32_t>(offsetof(MSXslScript_t2775088162, ___implementsPrefix_1)); }
	inline String_t* get_implementsPrefix_1() const { return ___implementsPrefix_1; }
	inline String_t** get_address_of_implementsPrefix_1() { return &___implementsPrefix_1; }
	inline void set_implementsPrefix_1(String_t* value)
	{
		___implementsPrefix_1 = value;
		Il2CppCodeGenWriteBarrier(&___implementsPrefix_1, value);
	}

	inline static int32_t get_offset_of_code_2() { return static_cast<int32_t>(offsetof(MSXslScript_t2775088162, ___code_2)); }
	inline String_t* get_code_2() const { return ___code_2; }
	inline String_t** get_address_of_code_2() { return &___code_2; }
	inline void set_code_2(String_t* value)
	{
		___code_2 = value;
		Il2CppCodeGenWriteBarrier(&___code_2, value);
	}

	inline static int32_t get_offset_of_evidence_3() { return static_cast<int32_t>(offsetof(MSXslScript_t2775088162, ___evidence_3)); }
	inline Evidence_t2008144148 * get_evidence_3() const { return ___evidence_3; }
	inline Evidence_t2008144148 ** get_address_of_evidence_3() { return &___evidence_3; }
	inline void set_evidence_3(Evidence_t2008144148 * value)
	{
		___evidence_3 = value;
		Il2CppCodeGenWriteBarrier(&___evidence_3, value);
	}
};

struct MSXslScript_t2775088162_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.MSXslScriptManager/MSXslScript::<>f__switch$map19
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map19_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.MSXslScriptManager/MSXslScript::<>f__switch$map1A
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map1A_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map19_4() { return static_cast<int32_t>(offsetof(MSXslScript_t2775088162_StaticFields, ___U3CU3Ef__switchU24map19_4)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map19_4() const { return ___U3CU3Ef__switchU24map19_4; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map19_4() { return &___U3CU3Ef__switchU24map19_4; }
	inline void set_U3CU3Ef__switchU24map19_4(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map19_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map19_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1A_5() { return static_cast<int32_t>(offsetof(MSXslScript_t2775088162_StaticFields, ___U3CU3Ef__switchU24map1A_5)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map1A_5() const { return ___U3CU3Ef__switchU24map1A_5; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map1A_5() { return &___U3CU3Ef__switchU24map1A_5; }
	inline void set_U3CU3Ef__switchU24map1A_5(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map1A_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1A_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
