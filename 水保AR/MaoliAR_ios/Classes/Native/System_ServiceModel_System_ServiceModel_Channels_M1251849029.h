﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_B3441673553.h"

// System.ServiceModel.Channels.MessageFault
struct MessageFault_t929740066;
// System.ServiceModel.Channels.MessageVersion
struct MessageVersion_t1958933598;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.MessageFaultBodyWriter
struct  MessageFaultBodyWriter_t1251849029  : public BodyWriter_t3441673553
{
public:
	// System.ServiceModel.Channels.MessageFault System.ServiceModel.Channels.MessageFaultBodyWriter::fault
	MessageFault_t929740066 * ___fault_1;
	// System.ServiceModel.Channels.MessageVersion System.ServiceModel.Channels.MessageFaultBodyWriter::version
	MessageVersion_t1958933598 * ___version_2;

public:
	inline static int32_t get_offset_of_fault_1() { return static_cast<int32_t>(offsetof(MessageFaultBodyWriter_t1251849029, ___fault_1)); }
	inline MessageFault_t929740066 * get_fault_1() const { return ___fault_1; }
	inline MessageFault_t929740066 ** get_address_of_fault_1() { return &___fault_1; }
	inline void set_fault_1(MessageFault_t929740066 * value)
	{
		___fault_1 = value;
		Il2CppCodeGenWriteBarrier(&___fault_1, value);
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(MessageFaultBodyWriter_t1251849029, ___version_2)); }
	inline MessageVersion_t1958933598 * get_version_2() const { return ___version_2; }
	inline MessageVersion_t1958933598 ** get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(MessageVersion_t1958933598 * value)
	{
		___version_2 = value;
		Il2CppCodeGenWriteBarrier(&___version_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
