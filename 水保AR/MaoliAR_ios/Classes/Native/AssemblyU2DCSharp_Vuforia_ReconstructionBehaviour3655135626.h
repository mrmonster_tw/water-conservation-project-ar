﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Vuforia_UnityExtensions_Vuforia_ReconstructionAbstr818896732.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ReconstructionBehaviour
struct  ReconstructionBehaviour_t3655135626  : public ReconstructionAbstractBehaviour_t818896732
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
