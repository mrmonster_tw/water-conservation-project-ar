﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.ImportedFromTypeLibAttribute
struct  ImportedFromTypeLibAttribute_t385006552  : public Attribute_t861562559
{
public:
	// System.String System.Runtime.InteropServices.ImportedFromTypeLibAttribute::TlbFile
	String_t* ___TlbFile_0;

public:
	inline static int32_t get_offset_of_TlbFile_0() { return static_cast<int32_t>(offsetof(ImportedFromTypeLibAttribute_t385006552, ___TlbFile_0)); }
	inline String_t* get_TlbFile_0() const { return ___TlbFile_0; }
	inline String_t** get_address_of_TlbFile_0() { return &___TlbFile_0; }
	inline void set_TlbFile_0(String_t* value)
	{
		___TlbFile_0 = value;
		Il2CppCodeGenWriteBarrier(&___TlbFile_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
