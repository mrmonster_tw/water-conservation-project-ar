﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3549286319.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_U3CModuleU3E692745525.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_PostEffectsBase3718631875.h"
#include "mscorlib_System_Void1185182177.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "mscorlib_System_Boolean97287965.h"
#include "UnityEngine_UnityEngine_Shader4151988712.h"
#include "UnityEngine_UnityEngine_Material340375123.h"
#include "UnityEngine_UnityEngine_Object631007953.h"
#include "mscorlib_System_String1847450689.h"
#include "mscorlib_System_Object3080106164.h"
#include "UnityEngine_UnityEngine_HideFlags4250555765.h"
#include "UnityEngine_UnityEngine_Behaviour1437897464.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat962350765.h"
#include "UnityEngine_UnityEngine_DepthTextureMode4161834719.h"
#include "mscorlib_System_Int322950945753.h"
#include "UnityEngine_UnityEngine_Camera4157153871.h"
#include "UnityEngine_UnityEngine_Component1923634451.h"
#include "UnityEngine_UnityEngine_RenderTexture2108887433.h"
#include "mscorlib_System_Single1397266774.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_ShaftsScreenBle2434095160.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_SunShafts3552083306.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_SunShaftsResolut277016971.h"
#include "UnityEngine_UnityEngine_Texture3661962703.h"
#include "UnityEngine_UnityEngine_Vector33722313464.h"
#include "UnityEngine_UnityEngine_Vector43319028937.h"
#include "UnityEngine_UnityEngine_Transform3600365921.h"

// PostEffectsBase
struct PostEffectsBase_t3718631875;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.Object
struct Object_t631007953;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Behaviour
struct Behaviour_t1437897464;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// SunShafts
struct SunShafts_t3552083306;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.Transform
struct Transform_t3600365921;
extern Il2CppClass* Object_t631007953_il2cpp_TypeInfo_var;
extern Il2CppClass* RuntimeServices_t2098243569_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern Il2CppClass* Material_t340375123_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2983398388;
extern Il2CppCodeGenString* _stringLiteral3300412731;
extern Il2CppCodeGenString* _stringLiteral3193219807;
extern Il2CppCodeGenString* _stringLiteral3386904557;
extern const uint32_t PostEffectsBase_CheckShaderAndCreateMaterial_m3850015534_MetadataUsageId;
extern const uint32_t PostEffectsBase_CreateMaterial_m2936897475_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral261186095;
extern Il2CppCodeGenString* _stringLiteral3916679800;
extern const uint32_t PostEffectsBase_CheckResources_m3086559110_MetadataUsageId;
extern const MethodInfo* Component_GetComponent_TisCamera_t4157153871_m3977033459_MethodInfo_var;
extern const uint32_t PostEffectsBase_CheckSupport_m507550616_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3451747185;
extern Il2CppCodeGenString* _stringLiteral2084055514;
extern const uint32_t PostEffectsBase_ReportAutoDisable_m1244590241_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2339937023;
extern const uint32_t PostEffectsBase_CheckShader_m2015217677_MetadataUsageId;
extern Il2CppClass* Single_t1397266774_il2cpp_TypeInfo_var;
extern const uint32_t PostEffectsBase_DrawBorder_m4095668195_MetadataUsageId;
extern Il2CppClass* Graphics_t783367614_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2517340756;
extern Il2CppCodeGenString* _stringLiteral422554171;
extern Il2CppCodeGenString* _stringLiteral2727045764;
extern Il2CppCodeGenString* _stringLiteral583914606;
extern Il2CppCodeGenString* _stringLiteral2161159581;
extern Il2CppCodeGenString* _stringLiteral147475429;
extern const uint32_t SunShafts_OnRenderImage_m4124484714_MetadataUsageId;



// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m2735705429_gshared (Component_t1923634451 * __this, const MethodInfo* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1339182015 (MonoBehaviour_t3962482529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m487959476 (Il2CppObject * __this /* static, unused */, Object_t631007953 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Boo.Lang.Runtime.RuntimeServices::op_Addition(System.String,System.String)
extern "C"  String_t* RuntimeServices_op_Addition_m583005490 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m1780991845 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C"  void Behaviour_set_enabled_m3107225489 (Behaviour_t1437897464 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Shader::get_isSupported()
extern "C"  bool Shader_get_isSupported_m4152045473 (Shader_t4151988712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Shader UnityEngine.Material::get_shader()
extern "C"  Shader_t4151988712 * Material_get_shader_m748054455 (Material_t340375123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1454075600 (Il2CppObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::.ctor(UnityEngine.Shader)
extern "C"  void Material__ctor_m1727794963 (Material_t340375123 * __this, Shader_t4151988712 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C"  void Object_set_hideFlags_m395729791 (Object_t631007953 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C"  void Debug_LogWarning_m3661709751 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SystemInfo::SupportsRenderTextureFormat(UnityEngine.RenderTextureFormat)
extern "C"  bool SystemInfo_SupportsRenderTextureFormat_m1154813049 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SystemInfo::get_graphicsShaderLevel()
extern "C"  int32_t SystemInfo_get_graphicsShaderLevel_m775786077 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SystemInfo::get_supportsComputeShaders()
extern "C"  bool SystemInfo_get_supportsComputeShaders_m562042917 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SystemInfo::get_supportsImageEffects()
extern "C"  bool SystemInfo_get_supportsImageEffects_m2467564449 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SystemInfo::get_supportsRenderTextures()
extern "C"  bool SystemInfo_get_supportsRenderTextures_m2448215108 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t4157153871_m3977033459(__this, method) ((  Camera_t4157153871 * (*) (Component_t1923634451 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2735705429_gshared)(__this, method)
// UnityEngine.DepthTextureMode UnityEngine.Camera::get_depthTextureMode()
extern "C"  int32_t Camera_get_depthTextureMode_m671431136 (Camera_t4157153871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_depthTextureMode(UnityEngine.DepthTextureMode)
extern "C"  void Camera_set_depthTextureMode_m2892547683 (Camera_t4157153871 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::set_active(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_set_active_m2767173185 (Il2CppObject * __this /* static, unused */, RenderTexture_t2108887433 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::PushMatrix()
extern "C"  void GL_PushMatrix_m1761957053 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::LoadOrtho()
extern "C"  void GL_LoadOrtho_m690359636 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Material::SetPass(System.Int32)
extern "C"  bool Material_SetPass_m33774367 (Material_t340375123 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::Begin(System.Int32)
extern "C"  void GL_Begin_m1275140189 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::TexCoord2(System.Single,System.Single)
extern "C"  void GL_TexCoord2_m4227718769 (Il2CppObject * __this /* static, unused */, float p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::Vertex3(System.Single,System.Single,System.Single)
extern "C"  void GL_Vertex3_m1225212110 (Il2CppObject * __this /* static, unused */, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::End()
extern "C"  void GL_End_m4063101722 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Material::get_passCount()
extern "C"  int32_t Material_get_passCount_m1126240344 (Material_t340375123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::PopMatrix()
extern "C"  void GL_PopMatrix_m3972093049 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PostEffectsBase::.ctor()
extern "C"  void PostEffectsBase__ctor_m99228393 (PostEffectsBase_t3718631875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C"  Color_t2555686324  Color_get_white_m3544547002 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture)
extern "C"  void Graphics_Blit_m453085112 (Il2CppObject * __this /* static, unused */, Texture_t3661962703 * p0, RenderTexture_t2108887433 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
extern "C"  Vector3_t3722313464  Vector3_get_one_m1058616769 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Vector3_op_Multiply_m3506743150 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t3722313464  Transform_get_position_m102368104 (Transform_t3600365921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Camera::WorldToViewportPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Camera_WorldToViewportPoint_m3409072832 (Camera_t4157153871 * __this, Vector3_t3722313464  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m1197556204 (Vector3_t3722313464 * __this, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32)
extern "C"  RenderTexture_t2108887433 * RenderTexture_GetTemporary_m2970995366 (Il2CppObject * __this /* static, unused */, int32_t p0, int32_t p1, int32_t p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Vector4__ctor_m1702261405 (Vector4_t3319028937 * __this, float p0, float p1, float p2, float p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::op_Multiply(UnityEngine.Vector4,System.Single)
extern "C"  Vector4_t3319028937  Vector4_op_Multiply_m1180700625 (Il2CppObject * __this /* static, unused */, Vector4_t3319028937  p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetVector(System.String,UnityEngine.Vector4)
extern "C"  void Material_SetVector_m3662241165 (Material_t340375123 * __this, String_t* p0, Vector4_t3319028937  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetFloat(System.String,System.Single)
extern "C"  void Material_SetFloat_m3366878636 (Material_t340375123 * __this, String_t* p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::ClearWithSkybox(System.Boolean,UnityEngine.Camera)
extern "C"  void GL_ClearWithSkybox_m3187363567 (Il2CppObject * __this /* static, unused */, bool p0, Camera_t4157153871 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetTexture(System.String,UnityEngine.Texture)
extern "C"  void Material_SetTexture_m2801217397 (Material_t340375123 * __this, String_t* p0, Texture_t3661962703 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32)
extern "C"  void Graphics_Blit_m2694712511 (Il2CppObject * __this /* static, unused */, Texture_t3661962703 * p0, RenderTexture_t2108887433 * p1, Material_t340375123 * p2, int32_t p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_ReleaseTemporary_m739636844 (Il2CppObject * __this /* static, unused */, RenderTexture_t2108887433 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mathf::Clamp(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t Mathf_Clamp_m2702811178 (Il2CppObject * __this /* static, unused */, int32_t p0, int32_t p1, int32_t p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::get_zero()
extern "C"  Vector4_t3319028937  Vector4_get_zero_m3390620726 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PostEffectsBase::.ctor()
extern "C"  void PostEffectsBase__ctor_m99228393 (PostEffectsBase_t3718631875 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1339182015(__this, /*hidden argument*/NULL);
		__this->set_supportHDRTextures_2((bool)1);
		__this->set_isSupported_4((bool)1);
		return;
	}
}
// UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material)
extern "C"  Material_t340375123 * PostEffectsBase_CheckShaderAndCreateMaterial_m3850015534 (PostEffectsBase_t3718631875 * __this, Shader_t4151988712 * ___s0, Material_t340375123 * ___m2Create1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsBase_CheckShaderAndCreateMaterial_m3850015534_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Material_t340375123 * G_B11_0 = NULL;
	{
		Shader_t4151988712 * L_0 = ___s0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m487959476(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.Object::ToString() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t2098243569_il2cpp_TypeInfo_var);
		String_t* L_3 = RuntimeServices_op_Addition_m583005490(NULL /*static, unused*/, _stringLiteral2983398388, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m1780991845(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Behaviour_set_enabled_m3107225489(__this, (bool)0, /*hidden argument*/NULL);
		G_B11_0 = ((Material_t340375123 *)(NULL));
		goto IL_00d0;
	}

IL_002d:
	{
		Shader_t4151988712 * L_4 = ___s0;
		NullCheck(L_4);
		bool L_5 = Shader_get_isSupported_m4152045473(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_005a;
		}
	}
	{
		Material_t340375123 * L_6 = ___m2Create1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Implicit_m487959476(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_005a;
		}
	}
	{
		Material_t340375123 * L_8 = ___m2Create1;
		NullCheck(L_8);
		Shader_t4151988712 * L_9 = Material_get_shader_m748054455(L_8, /*hidden argument*/NULL);
		Shader_t4151988712 * L_10 = ___s0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_005a;
		}
	}
	{
		Material_t340375123 * L_12 = ___m2Create1;
		G_B11_0 = L_12;
		goto IL_00d0;
	}

IL_005a:
	{
		Shader_t4151988712 * L_13 = ___s0;
		NullCheck(L_13);
		bool L_14 = Shader_get_isSupported_m4152045473(L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_00a5;
		}
	}
	{
		VirtActionInvoker0::Invoke(15 /* System.Void PostEffectsBase::NotSupported() */, __this);
		Shader_t4151988712 * L_15 = ___s0;
		NullCheck(L_15);
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.Object::ToString() */, L_15);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t2098243569_il2cpp_TypeInfo_var);
		String_t* L_17 = RuntimeServices_op_Addition_m583005490(NULL /*static, unused*/, _stringLiteral3300412731, L_16, /*hidden argument*/NULL);
		String_t* L_18 = RuntimeServices_op_Addition_m583005490(NULL /*static, unused*/, L_17, _stringLiteral3193219807, /*hidden argument*/NULL);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.Object::ToString() */, __this);
		String_t* L_20 = RuntimeServices_op_Addition_m583005490(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		String_t* L_21 = RuntimeServices_op_Addition_m583005490(NULL /*static, unused*/, L_20, _stringLiteral3386904557, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m1780991845(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		G_B11_0 = ((Material_t340375123 *)(NULL));
		goto IL_00d0;
	}

IL_00a5:
	{
		Shader_t4151988712 * L_22 = ___s0;
		Material_t340375123 * L_23 = (Material_t340375123 *)il2cpp_codegen_object_new(Material_t340375123_il2cpp_TypeInfo_var);
		Material__ctor_m1727794963(L_23, L_22, /*hidden argument*/NULL);
		___m2Create1 = L_23;
		Material_t340375123 * L_24 = ___m2Create1;
		NullCheck(L_24);
		Object_set_hideFlags_m395729791(L_24, ((int32_t)52), /*hidden argument*/NULL);
		Material_t340375123 * L_25 = ___m2Create1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_26 = Object_op_Implicit_m487959476(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00ca;
		}
	}
	{
		Material_t340375123 * L_27 = ___m2Create1;
		G_B11_0 = L_27;
		goto IL_00d0;
	}

IL_00ca:
	{
		G_B11_0 = ((Material_t340375123 *)(NULL));
		goto IL_00d0;
	}

IL_00d0:
	{
		return G_B11_0;
	}
}
// UnityEngine.Material PostEffectsBase::CreateMaterial(UnityEngine.Shader,UnityEngine.Material)
extern "C"  Material_t340375123 * PostEffectsBase_CreateMaterial_m2936897475 (PostEffectsBase_t3718631875 * __this, Shader_t4151988712 * ___s0, Material_t340375123 * ___m2Create1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsBase_CreateMaterial_m2936897475_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Material_t340375123 * G_B11_0 = NULL;
	{
		Shader_t4151988712 * L_0 = ___s0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m487959476(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0026;
		}
	}
	{
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.Object::ToString() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t2098243569_il2cpp_TypeInfo_var);
		String_t* L_3 = RuntimeServices_op_Addition_m583005490(NULL /*static, unused*/, _stringLiteral2983398388, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m1780991845(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		G_B11_0 = ((Material_t340375123 *)(NULL));
		goto IL_008f;
	}

IL_0026:
	{
		Material_t340375123 * L_4 = ___m2Create1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Implicit_m487959476(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0053;
		}
	}
	{
		Material_t340375123 * L_6 = ___m2Create1;
		NullCheck(L_6);
		Shader_t4151988712 * L_7 = Material_get_shader_m748054455(L_6, /*hidden argument*/NULL);
		Shader_t4151988712 * L_8 = ___s0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0053;
		}
	}
	{
		Shader_t4151988712 * L_10 = ___s0;
		NullCheck(L_10);
		bool L_11 = Shader_get_isSupported_m4152045473(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0053;
		}
	}
	{
		Material_t340375123 * L_12 = ___m2Create1;
		G_B11_0 = L_12;
		goto IL_008f;
	}

IL_0053:
	{
		Shader_t4151988712 * L_13 = ___s0;
		NullCheck(L_13);
		bool L_14 = Shader_get_isSupported_m4152045473(L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0064;
		}
	}
	{
		G_B11_0 = ((Material_t340375123 *)(NULL));
		goto IL_008f;
	}

IL_0064:
	{
		Shader_t4151988712 * L_15 = ___s0;
		Material_t340375123 * L_16 = (Material_t340375123 *)il2cpp_codegen_object_new(Material_t340375123_il2cpp_TypeInfo_var);
		Material__ctor_m1727794963(L_16, L_15, /*hidden argument*/NULL);
		___m2Create1 = L_16;
		Material_t340375123 * L_17 = ___m2Create1;
		NullCheck(L_17);
		Object_set_hideFlags_m395729791(L_17, ((int32_t)52), /*hidden argument*/NULL);
		Material_t340375123 * L_18 = ___m2Create1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Implicit_m487959476(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0089;
		}
	}
	{
		Material_t340375123 * L_20 = ___m2Create1;
		G_B11_0 = L_20;
		goto IL_008f;
	}

IL_0089:
	{
		G_B11_0 = ((Material_t340375123 *)(NULL));
		goto IL_008f;
	}

IL_008f:
	{
		return G_B11_0;
	}
}
// System.Void PostEffectsBase::OnEnable()
extern "C"  void PostEffectsBase_OnEnable_m971482546 (PostEffectsBase_t3718631875 * __this, const MethodInfo* method)
{
	{
		__this->set_isSupported_4((bool)1);
		return;
	}
}
// System.Boolean PostEffectsBase::CheckSupport()
extern "C"  bool PostEffectsBase_CheckSupport_m1272871479 (PostEffectsBase_t3718631875 * __this, const MethodInfo* method)
{
	{
		bool L_0 = VirtFuncInvoker1< bool, bool >::Invoke(10 /* System.Boolean PostEffectsBase::CheckSupport(System.Boolean) */, __this, (bool)0);
		return L_0;
	}
}
// System.Boolean PostEffectsBase::CheckResources()
extern "C"  bool PostEffectsBase_CheckResources_m3086559110 (PostEffectsBase_t3718631875 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsBase_CheckResources_m3086559110_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.Object::ToString() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t2098243569_il2cpp_TypeInfo_var);
		String_t* L_1 = RuntimeServices_op_Addition_m583005490(NULL /*static, unused*/, _stringLiteral261186095, L_0, /*hidden argument*/NULL);
		String_t* L_2 = RuntimeServices_op_Addition_m583005490(NULL /*static, unused*/, L_1, _stringLiteral3916679800, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3661709751(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		bool L_3 = __this->get_isSupported_4();
		return L_3;
	}
}
// System.Void PostEffectsBase::Start()
extern "C"  void PostEffectsBase_Start_m1182687183 (PostEffectsBase_t3718631875 * __this, const MethodInfo* method)
{
	{
		VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean PostEffectsBase::CheckResources() */, __this);
		return;
	}
}
// System.Boolean PostEffectsBase::CheckSupport(System.Boolean)
extern "C"  bool PostEffectsBase_CheckSupport_m507550616 (PostEffectsBase_t3718631875 * __this, bool ___needDepth0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsBase_CheckSupport_m507550616_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B2_0 = 0;
	PostEffectsBase_t3718631875 * G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	PostEffectsBase_t3718631875 * G_B1_1 = NULL;
	int32_t G_B11_0 = 0;
	{
		__this->set_isSupported_4((bool)1);
		bool L_0 = SystemInfo_SupportsRenderTextureFormat_m1154813049(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
		__this->set_supportHDRTextures_2(L_0);
		int32_t L_1 = SystemInfo_get_graphicsShaderLevel_m775786077(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = ((((int32_t)((((int32_t)L_1) < ((int32_t)((int32_t)50)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		G_B1_0 = L_2;
		G_B1_1 = __this;
		if (!L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = __this;
			goto IL_002c;
		}
	}
	{
		bool L_3 = SystemInfo_get_supportsComputeShaders_m562042917(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B2_0 = ((int32_t)(L_3));
		G_B2_1 = G_B1_1;
	}

IL_002c:
	{
		NullCheck(G_B2_1);
		G_B2_1->set_supportDX11_3((bool)G_B2_0);
		bool L_4 = SystemInfo_get_supportsImageEffects_m2467564449(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0045;
		}
	}
	{
		bool L_5 = SystemInfo_get_supportsRenderTextures_m2448215108(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0051;
		}
	}

IL_0045:
	{
		VirtActionInvoker0::Invoke(15 /* System.Void PostEffectsBase::NotSupported() */, __this);
		G_B11_0 = 0;
		goto IL_008d;
	}

IL_0051:
	{
		bool L_6 = ___needDepth0;
		if (!L_6)
		{
			goto IL_006e;
		}
	}
	{
		bool L_7 = SystemInfo_SupportsRenderTextureFormat_m1154813049(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_006e;
		}
	}
	{
		VirtActionInvoker0::Invoke(15 /* System.Void PostEffectsBase::NotSupported() */, __this);
		G_B11_0 = 0;
		goto IL_008d;
	}

IL_006e:
	{
		bool L_8 = ___needDepth0;
		if (!L_8)
		{
			goto IL_008c;
		}
	}
	{
		Camera_t4157153871 * L_9 = Component_GetComponent_TisCamera_t4157153871_m3977033459(__this, /*hidden argument*/Component_GetComponent_TisCamera_t4157153871_m3977033459_MethodInfo_var);
		Camera_t4157153871 * L_10 = Component_GetComponent_TisCamera_t4157153871_m3977033459(__this, /*hidden argument*/Component_GetComponent_TisCamera_t4157153871_m3977033459_MethodInfo_var);
		NullCheck(L_10);
		int32_t L_11 = Camera_get_depthTextureMode_m671431136(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		Camera_set_depthTextureMode_m2892547683(L_9, ((int32_t)((int32_t)L_11|(int32_t)1)), /*hidden argument*/NULL);
	}

IL_008c:
	{
		G_B11_0 = 1;
	}

IL_008d:
	{
		return (bool)G_B11_0;
	}
}
// System.Boolean PostEffectsBase::CheckSupport(System.Boolean,System.Boolean)
extern "C"  bool PostEffectsBase_CheckSupport_m4281490439 (PostEffectsBase_t3718631875 * __this, bool ___needDepth0, bool ___needHdr1, const MethodInfo* method)
{
	int32_t G_B6_0 = 0;
	{
		bool L_0 = ___needDepth0;
		bool L_1 = VirtFuncInvoker1< bool, bool >::Invoke(10 /* System.Boolean PostEffectsBase::CheckSupport(System.Boolean) */, __this, L_0);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B6_0 = 0;
		goto IL_0030;
	}

IL_0012:
	{
		bool L_2 = ___needHdr1;
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		bool L_3 = __this->get_supportHDRTextures_2();
		if (L_3)
		{
			goto IL_002f;
		}
	}
	{
		VirtActionInvoker0::Invoke(15 /* System.Void PostEffectsBase::NotSupported() */, __this);
		G_B6_0 = 0;
		goto IL_0030;
	}

IL_002f:
	{
		G_B6_0 = 1;
	}

IL_0030:
	{
		return (bool)G_B6_0;
	}
}
// System.Boolean PostEffectsBase::Dx11Support()
extern "C"  bool PostEffectsBase_Dx11Support_m337155416 (PostEffectsBase_t3718631875 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_supportDX11_3();
		return L_0;
	}
}
// System.Void PostEffectsBase::ReportAutoDisable()
extern "C"  void PostEffectsBase_ReportAutoDisable_m1244590241 (PostEffectsBase_t3718631875 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsBase_ReportAutoDisable_m1244590241_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.Object::ToString() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t2098243569_il2cpp_TypeInfo_var);
		String_t* L_1 = RuntimeServices_op_Addition_m583005490(NULL /*static, unused*/, _stringLiteral3451747185, L_0, /*hidden argument*/NULL);
		String_t* L_2 = RuntimeServices_op_Addition_m583005490(NULL /*static, unused*/, L_1, _stringLiteral2084055514, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3661709751(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PostEffectsBase::CheckShader(UnityEngine.Shader)
extern "C"  bool PostEffectsBase_CheckShader_m2015217677 (PostEffectsBase_t3718631875 * __this, Shader_t4151988712 * ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsBase_CheckShader_m2015217677_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		Shader_t4151988712 * L_0 = ___s0;
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.Object::ToString() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t2098243569_il2cpp_TypeInfo_var);
		String_t* L_2 = RuntimeServices_op_Addition_m583005490(NULL /*static, unused*/, _stringLiteral3300412731, L_1, /*hidden argument*/NULL);
		String_t* L_3 = RuntimeServices_op_Addition_m583005490(NULL /*static, unused*/, L_2, _stringLiteral3193219807, /*hidden argument*/NULL);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.Object::ToString() */, __this);
		String_t* L_5 = RuntimeServices_op_Addition_m583005490(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		String_t* L_6 = RuntimeServices_op_Addition_m583005490(NULL /*static, unused*/, L_5, _stringLiteral2339937023, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m1780991845(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Shader_t4151988712 * L_7 = ___s0;
		NullCheck(L_7);
		bool L_8 = Shader_get_isSupported_m4152045473(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_004b;
		}
	}
	{
		VirtActionInvoker0::Invoke(15 /* System.Void PostEffectsBase::NotSupported() */, __this);
		G_B3_0 = 0;
		goto IL_0051;
	}

IL_004b:
	{
		G_B3_0 = 0;
		goto IL_0051;
	}

IL_0051:
	{
		return (bool)G_B3_0;
	}
}
// System.Void PostEffectsBase::NotSupported()
extern "C"  void PostEffectsBase_NotSupported_m1688668870 (PostEffectsBase_t3718631875 * __this, const MethodInfo* method)
{
	{
		Behaviour_set_enabled_m3107225489(__this, (bool)0, /*hidden argument*/NULL);
		__this->set_isSupported_4((bool)0);
		return;
	}
}
// System.Void PostEffectsBase::DrawBorder(UnityEngine.RenderTexture,UnityEngine.Material)
extern "C"  void PostEffectsBase_DrawBorder_m4095668195 (PostEffectsBase_t3718631875 * __this, RenderTexture_t2108887433 * ___dest0, Material_t340375123 * ___material1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsBase_DrawBorder_m4095668195_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	bool V_4 = false;
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	{
		Initobj (Single_t1397266774_il2cpp_TypeInfo_var, (&V_0));
		Initobj (Single_t1397266774_il2cpp_TypeInfo_var, (&V_1));
		Initobj (Single_t1397266774_il2cpp_TypeInfo_var, (&V_2));
		Initobj (Single_t1397266774_il2cpp_TypeInfo_var, (&V_3));
		RenderTexture_t2108887433 * L_0 = ___dest0;
		RenderTexture_set_active_m2767173185(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_4 = (bool)1;
		GL_PushMatrix_m1761957053(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_LoadOrtho_m690359636(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_5 = 0;
		goto IL_0287;
	}

IL_003b:
	{
		Material_t340375123 * L_1 = ___material1;
		int32_t L_2 = V_5;
		NullCheck(L_1);
		Material_SetPass_m33774367(L_1, L_2, /*hidden argument*/NULL);
		Initobj (Single_t1397266774_il2cpp_TypeInfo_var, (&V_6));
		Initobj (Single_t1397266774_il2cpp_TypeInfo_var, (&V_7));
		bool L_3 = V_4;
		if (!L_3)
		{
			goto IL_006b;
		}
	}
	{
		V_6 = (1.0f);
		V_7 = (((float)((float)0)));
		goto IL_0076;
	}

IL_006b:
	{
		V_6 = (((float)((float)0)));
		V_7 = (1.0f);
	}

IL_0076:
	{
		V_0 = (((float)((float)0)));
		RenderTexture_t2108887433 * L_4 = ___dest0;
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_4);
		V_1 = ((float)((float)(((float)((float)0)))+(float)((float)((float)(1.0f)/(float)((float)((float)(((float)((float)L_5)))*(float)(1.0f)))))));
		V_2 = (((float)((float)0)));
		V_3 = (1.0f);
		GL_Begin_m1275140189(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		float L_6 = V_6;
		GL_TexCoord2_m4227718769(NULL /*static, unused*/, (((float)((float)0))), L_6, /*hidden argument*/NULL);
		float L_7 = V_0;
		float L_8 = V_2;
		GL_Vertex3_m1225212110(NULL /*static, unused*/, L_7, L_8, (0.1f), /*hidden argument*/NULL);
		float L_9 = V_6;
		GL_TexCoord2_m4227718769(NULL /*static, unused*/, (1.0f), L_9, /*hidden argument*/NULL);
		float L_10 = V_1;
		float L_11 = V_2;
		GL_Vertex3_m1225212110(NULL /*static, unused*/, L_10, L_11, (0.1f), /*hidden argument*/NULL);
		float L_12 = V_7;
		GL_TexCoord2_m4227718769(NULL /*static, unused*/, (1.0f), L_12, /*hidden argument*/NULL);
		float L_13 = V_1;
		float L_14 = V_3;
		GL_Vertex3_m1225212110(NULL /*static, unused*/, L_13, L_14, (0.1f), /*hidden argument*/NULL);
		float L_15 = V_7;
		GL_TexCoord2_m4227718769(NULL /*static, unused*/, (((float)((float)0))), L_15, /*hidden argument*/NULL);
		float L_16 = V_0;
		float L_17 = V_3;
		GL_Vertex3_m1225212110(NULL /*static, unused*/, L_16, L_17, (0.1f), /*hidden argument*/NULL);
		RenderTexture_t2108887433 * L_18 = ___dest0;
		NullCheck(L_18);
		int32_t L_19 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_18);
		V_0 = ((float)((float)(1.0f)-(float)((float)((float)(1.0f)/(float)((float)((float)(((float)((float)L_19)))*(float)(1.0f)))))));
		V_1 = (1.0f);
		V_2 = (((float)((float)0)));
		V_3 = (1.0f);
		float L_20 = V_6;
		GL_TexCoord2_m4227718769(NULL /*static, unused*/, (((float)((float)0))), L_20, /*hidden argument*/NULL);
		float L_21 = V_0;
		float L_22 = V_2;
		GL_Vertex3_m1225212110(NULL /*static, unused*/, L_21, L_22, (0.1f), /*hidden argument*/NULL);
		float L_23 = V_6;
		GL_TexCoord2_m4227718769(NULL /*static, unused*/, (1.0f), L_23, /*hidden argument*/NULL);
		float L_24 = V_1;
		float L_25 = V_2;
		GL_Vertex3_m1225212110(NULL /*static, unused*/, L_24, L_25, (0.1f), /*hidden argument*/NULL);
		float L_26 = V_7;
		GL_TexCoord2_m4227718769(NULL /*static, unused*/, (1.0f), L_26, /*hidden argument*/NULL);
		float L_27 = V_1;
		float L_28 = V_3;
		GL_Vertex3_m1225212110(NULL /*static, unused*/, L_27, L_28, (0.1f), /*hidden argument*/NULL);
		float L_29 = V_7;
		GL_TexCoord2_m4227718769(NULL /*static, unused*/, (((float)((float)0))), L_29, /*hidden argument*/NULL);
		float L_30 = V_0;
		float L_31 = V_3;
		GL_Vertex3_m1225212110(NULL /*static, unused*/, L_30, L_31, (0.1f), /*hidden argument*/NULL);
		V_0 = (((float)((float)0)));
		V_1 = (1.0f);
		V_2 = (((float)((float)0)));
		RenderTexture_t2108887433 * L_32 = ___dest0;
		NullCheck(L_32);
		int32_t L_33 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_32);
		V_3 = ((float)((float)(((float)((float)0)))+(float)((float)((float)(1.0f)/(float)((float)((float)(((float)((float)L_33)))*(float)(1.0f)))))));
		float L_34 = V_6;
		GL_TexCoord2_m4227718769(NULL /*static, unused*/, (((float)((float)0))), L_34, /*hidden argument*/NULL);
		float L_35 = V_0;
		float L_36 = V_2;
		GL_Vertex3_m1225212110(NULL /*static, unused*/, L_35, L_36, (0.1f), /*hidden argument*/NULL);
		float L_37 = V_6;
		GL_TexCoord2_m4227718769(NULL /*static, unused*/, (1.0f), L_37, /*hidden argument*/NULL);
		float L_38 = V_1;
		float L_39 = V_2;
		GL_Vertex3_m1225212110(NULL /*static, unused*/, L_38, L_39, (0.1f), /*hidden argument*/NULL);
		float L_40 = V_7;
		GL_TexCoord2_m4227718769(NULL /*static, unused*/, (1.0f), L_40, /*hidden argument*/NULL);
		float L_41 = V_1;
		float L_42 = V_3;
		GL_Vertex3_m1225212110(NULL /*static, unused*/, L_41, L_42, (0.1f), /*hidden argument*/NULL);
		float L_43 = V_7;
		GL_TexCoord2_m4227718769(NULL /*static, unused*/, (((float)((float)0))), L_43, /*hidden argument*/NULL);
		float L_44 = V_0;
		float L_45 = V_3;
		GL_Vertex3_m1225212110(NULL /*static, unused*/, L_44, L_45, (0.1f), /*hidden argument*/NULL);
		V_0 = (((float)((float)0)));
		V_1 = (1.0f);
		RenderTexture_t2108887433 * L_46 = ___dest0;
		NullCheck(L_46);
		int32_t L_47 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_46);
		V_2 = ((float)((float)(1.0f)-(float)((float)((float)(1.0f)/(float)((float)((float)(((float)((float)L_47)))*(float)(1.0f)))))));
		V_3 = (1.0f);
		float L_48 = V_6;
		GL_TexCoord2_m4227718769(NULL /*static, unused*/, (((float)((float)0))), L_48, /*hidden argument*/NULL);
		float L_49 = V_0;
		float L_50 = V_2;
		GL_Vertex3_m1225212110(NULL /*static, unused*/, L_49, L_50, (0.1f), /*hidden argument*/NULL);
		float L_51 = V_6;
		GL_TexCoord2_m4227718769(NULL /*static, unused*/, (1.0f), L_51, /*hidden argument*/NULL);
		float L_52 = V_1;
		float L_53 = V_2;
		GL_Vertex3_m1225212110(NULL /*static, unused*/, L_52, L_53, (0.1f), /*hidden argument*/NULL);
		float L_54 = V_7;
		GL_TexCoord2_m4227718769(NULL /*static, unused*/, (1.0f), L_54, /*hidden argument*/NULL);
		float L_55 = V_1;
		float L_56 = V_3;
		GL_Vertex3_m1225212110(NULL /*static, unused*/, L_55, L_56, (0.1f), /*hidden argument*/NULL);
		float L_57 = V_7;
		GL_TexCoord2_m4227718769(NULL /*static, unused*/, (((float)((float)0))), L_57, /*hidden argument*/NULL);
		float L_58 = V_0;
		float L_59 = V_3;
		GL_Vertex3_m1225212110(NULL /*static, unused*/, L_58, L_59, (0.1f), /*hidden argument*/NULL);
		GL_End_m4063101722(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_60 = V_5;
		V_5 = ((int32_t)((int32_t)L_60+(int32_t)1));
	}

IL_0287:
	{
		int32_t L_61 = V_5;
		Material_t340375123 * L_62 = ___material1;
		NullCheck(L_62);
		int32_t L_63 = Material_get_passCount_m1126240344(L_62, /*hidden argument*/NULL);
		if ((((int32_t)L_61) < ((int32_t)L_63)))
		{
			goto IL_003b;
		}
	}
	{
		GL_PopMatrix_m3972093049(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PostEffectsBase::Main()
extern "C"  void PostEffectsBase_Main_m4126823709 (PostEffectsBase_t3718631875 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SunShafts::.ctor()
extern "C"  void SunShafts__ctor_m2784293110 (SunShafts_t3552083306 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase__ctor_m99228393(__this, /*hidden argument*/NULL);
		__this->set_resolution_5(1);
		__this->set_screenBlendMode_6(0);
		__this->set_radialBlurIterations_8(2);
		Color_t2555686324  L_0 = Color_get_white_m3544547002(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_sunColor_9(L_0);
		__this->set_sunShaftBlurRadius_10((2.5f));
		__this->set_sunShaftIntensity_11((1.15f));
		__this->set_useSkyBoxAlpha_12((0.75f));
		__this->set_maxRadius_13((0.75f));
		__this->set_useDepthTexture_14((bool)1);
		return;
	}
}
// System.Boolean SunShafts::CheckResources()
extern "C"  bool SunShafts_CheckResources_m3498899607 (SunShafts_t3552083306 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_useDepthTexture_14();
		VirtFuncInvoker1< bool, bool >::Invoke(10 /* System.Boolean PostEffectsBase::CheckSupport(System.Boolean) */, __this, L_0);
		Shader_t4151988712 * L_1 = __this->get_sunShaftsShader_15();
		Material_t340375123 * L_2 = __this->get_sunShaftsMaterial_16();
		Material_t340375123 * L_3 = VirtFuncInvoker2< Material_t340375123 *, Shader_t4151988712 *, Material_t340375123 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_1, L_2);
		__this->set_sunShaftsMaterial_16(L_3);
		Shader_t4151988712 * L_4 = __this->get_simpleClearShader_17();
		Material_t340375123 * L_5 = __this->get_simpleClearMaterial_18();
		Material_t340375123 * L_6 = VirtFuncInvoker2< Material_t340375123 *, Shader_t4151988712 *, Material_t340375123 * >::Invoke(4 /* UnityEngine.Material PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material) */, __this, L_4, L_5);
		__this->set_simpleClearMaterial_18(L_6);
		bool L_7 = ((PostEffectsBase_t3718631875 *)__this)->get_isSupported_4();
		if (L_7)
		{
			goto IL_004e;
		}
	}
	{
		VirtActionInvoker0::Invoke(13 /* System.Void PostEffectsBase::ReportAutoDisable() */, __this);
	}

IL_004e:
	{
		bool L_8 = ((PostEffectsBase_t3718631875 *)__this)->get_isSupported_4();
		return L_8;
	}
}
// System.Void SunShafts::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void SunShafts_OnRenderImage_m4124484714 (SunShafts_t3552083306 * __this, RenderTexture_t2108887433 * ___source0, RenderTexture_t2108887433 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SunShafts_OnRenderImage_m4124484714_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	RenderTexture_t2108887433 * V_4 = NULL;
	RenderTexture_t2108887433 * V_5 = NULL;
	RenderTexture_t2108887433 * V_6 = NULL;
	float V_7 = 0.0f;
	int32_t V_8 = 0;
	Material_t340375123 * G_B22_0 = NULL;
	RenderTexture_t2108887433 * G_B22_1 = NULL;
	RenderTexture_t2108887433 * G_B22_2 = NULL;
	Material_t340375123 * G_B21_0 = NULL;
	RenderTexture_t2108887433 * G_B21_1 = NULL;
	RenderTexture_t2108887433 * G_B21_2 = NULL;
	int32_t G_B23_0 = 0;
	Material_t340375123 * G_B23_1 = NULL;
	RenderTexture_t2108887433 * G_B23_2 = NULL;
	RenderTexture_t2108887433 * G_B23_3 = NULL;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean SunShafts::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		RenderTexture_t2108887433 * L_1 = ___source0;
		RenderTexture_t2108887433 * L_2 = ___destination1;
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t783367614_il2cpp_TypeInfo_var);
		Graphics_Blit_m453085112(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		goto IL_03ba;
	}

IL_0017:
	{
		bool L_3 = __this->get_useDepthTexture_14();
		if (!L_3)
		{
			goto IL_003a;
		}
	}
	{
		Camera_t4157153871 * L_4 = Component_GetComponent_TisCamera_t4157153871_m3977033459(__this, /*hidden argument*/Component_GetComponent_TisCamera_t4157153871_m3977033459_MethodInfo_var);
		Camera_t4157153871 * L_5 = Component_GetComponent_TisCamera_t4157153871_m3977033459(__this, /*hidden argument*/Component_GetComponent_TisCamera_t4157153871_m3977033459_MethodInfo_var);
		NullCheck(L_5);
		int32_t L_6 = Camera_get_depthTextureMode_m671431136(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Camera_set_depthTextureMode_m2892547683(L_4, ((int32_t)((int32_t)L_6|(int32_t)1)), /*hidden argument*/NULL);
	}

IL_003a:
	{
		V_0 = 4;
		int32_t L_7 = __this->get_resolution_5();
		if ((!(((uint32_t)L_7) == ((uint32_t)1))))
		{
			goto IL_004f;
		}
	}
	{
		V_0 = 2;
		goto IL_005d;
	}

IL_004f:
	{
		int32_t L_8 = __this->get_resolution_5();
		if ((!(((uint32_t)L_8) == ((uint32_t)2))))
		{
			goto IL_005d;
		}
	}
	{
		V_0 = 1;
	}

IL_005d:
	{
		Vector3_t3722313464  L_9 = Vector3_get_one_m1058616769(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_10 = Vector3_op_Multiply_m3506743150(NULL /*static, unused*/, L_9, (0.5f), /*hidden argument*/NULL);
		V_1 = L_10;
		Transform_t3600365921 * L_11 = __this->get_sunTransform_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Implicit_m487959476(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0099;
		}
	}
	{
		Camera_t4157153871 * L_13 = Component_GetComponent_TisCamera_t4157153871_m3977033459(__this, /*hidden argument*/Component_GetComponent_TisCamera_t4157153871_m3977033459_MethodInfo_var);
		Transform_t3600365921 * L_14 = __this->get_sunTransform_7();
		NullCheck(L_14);
		Vector3_t3722313464  L_15 = Transform_get_position_m102368104(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t3722313464  L_16 = Camera_WorldToViewportPoint_m3409072832(L_13, L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		goto IL_00ab;
	}

IL_0099:
	{
		Vector3_t3722313464  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Vector3__ctor_m1197556204(&L_17, (0.5f), (0.5f), (((float)((float)0))), /*hidden argument*/NULL);
		V_1 = L_17;
	}

IL_00ab:
	{
		RenderTexture_t2108887433 * L_18 = ___source0;
		NullCheck(L_18);
		int32_t L_19 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_18);
		int32_t L_20 = V_0;
		V_2 = ((int32_t)((int32_t)L_19/(int32_t)L_20));
		RenderTexture_t2108887433 * L_21 = ___source0;
		NullCheck(L_21);
		int32_t L_22 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_21);
		int32_t L_23 = V_0;
		V_3 = ((int32_t)((int32_t)L_22/(int32_t)L_23));
		V_4 = (RenderTexture_t2108887433 *)NULL;
		int32_t L_24 = V_2;
		int32_t L_25 = V_3;
		RenderTexture_t2108887433 * L_26 = RenderTexture_GetTemporary_m2970995366(NULL /*static, unused*/, L_24, L_25, 0, /*hidden argument*/NULL);
		V_5 = L_26;
		Material_t340375123 * L_27 = __this->get_sunShaftsMaterial_16();
		Vector4_t3319028937  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Vector4__ctor_m1702261405(&L_28, (1.0f), (1.0f), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		float L_29 = __this->get_sunShaftBlurRadius_10();
		Vector4_t3319028937  L_30 = Vector4_op_Multiply_m1180700625(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		NullCheck(L_27);
		Material_SetVector_m3662241165(L_27, _stringLiteral2517340756, L_30, /*hidden argument*/NULL);
		Material_t340375123 * L_31 = __this->get_sunShaftsMaterial_16();
		float L_32 = (&V_1)->get_x_1();
		float L_33 = (&V_1)->get_y_2();
		float L_34 = (&V_1)->get_z_3();
		float L_35 = __this->get_maxRadius_13();
		Vector4_t3319028937  L_36;
		memset(&L_36, 0, sizeof(L_36));
		Vector4__ctor_m1702261405(&L_36, L_32, L_33, L_34, L_35, /*hidden argument*/NULL);
		NullCheck(L_31);
		Material_SetVector_m3662241165(L_31, _stringLiteral422554171, L_36, /*hidden argument*/NULL);
		Material_t340375123 * L_37 = __this->get_sunShaftsMaterial_16();
		float L_38 = __this->get_useSkyBoxAlpha_12();
		NullCheck(L_37);
		Material_SetFloat_m3366878636(L_37, _stringLiteral2727045764, ((float)((float)(1.0f)-(float)L_38)), /*hidden argument*/NULL);
		bool L_39 = __this->get_useDepthTexture_14();
		if (L_39)
		{
			goto IL_01a3;
		}
	}
	{
		RenderTexture_t2108887433 * L_40 = ___source0;
		NullCheck(L_40);
		int32_t L_41 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_40);
		RenderTexture_t2108887433 * L_42 = ___source0;
		NullCheck(L_42);
		int32_t L_43 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_42);
		RenderTexture_t2108887433 * L_44 = RenderTexture_GetTemporary_m2970995366(NULL /*static, unused*/, L_41, L_43, 0, /*hidden argument*/NULL);
		V_6 = L_44;
		RenderTexture_t2108887433 * L_45 = V_6;
		RenderTexture_set_active_m2767173185(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		Camera_t4157153871 * L_46 = Component_GetComponent_TisCamera_t4157153871_m3977033459(__this, /*hidden argument*/Component_GetComponent_TisCamera_t4157153871_m3977033459_MethodInfo_var);
		GL_ClearWithSkybox_m3187363567(NULL /*static, unused*/, (bool)0, L_46, /*hidden argument*/NULL);
		Material_t340375123 * L_47 = __this->get_sunShaftsMaterial_16();
		RenderTexture_t2108887433 * L_48 = V_6;
		NullCheck(L_47);
		Material_SetTexture_m2801217397(L_47, _stringLiteral583914606, L_48, /*hidden argument*/NULL);
		RenderTexture_t2108887433 * L_49 = ___source0;
		RenderTexture_t2108887433 * L_50 = V_5;
		Material_t340375123 * L_51 = __this->get_sunShaftsMaterial_16();
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t783367614_il2cpp_TypeInfo_var);
		Graphics_Blit_m2694712511(NULL /*static, unused*/, L_49, L_50, L_51, 3, /*hidden argument*/NULL);
		RenderTexture_t2108887433 * L_52 = V_6;
		RenderTexture_ReleaseTemporary_m739636844(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		goto IL_01b2;
	}

IL_01a3:
	{
		RenderTexture_t2108887433 * L_53 = ___source0;
		RenderTexture_t2108887433 * L_54 = V_5;
		Material_t340375123 * L_55 = __this->get_sunShaftsMaterial_16();
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t783367614_il2cpp_TypeInfo_var);
		Graphics_Blit_m2694712511(NULL /*static, unused*/, L_53, L_54, L_55, 2, /*hidden argument*/NULL);
	}

IL_01b2:
	{
		RenderTexture_t2108887433 * L_56 = V_5;
		Material_t340375123 * L_57 = __this->get_simpleClearMaterial_18();
		VirtActionInvoker2< RenderTexture_t2108887433 *, Material_t340375123 * >::Invoke(16 /* System.Void PostEffectsBase::DrawBorder(UnityEngine.RenderTexture,UnityEngine.Material) */, __this, L_56, L_57);
		int32_t L_58 = __this->get_radialBlurIterations_8();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_59 = Mathf_Clamp_m2702811178(NULL /*static, unused*/, L_58, 1, 4, /*hidden argument*/NULL);
		__this->set_radialBlurIterations_8(L_59);
		float L_60 = __this->get_sunShaftBlurRadius_10();
		V_7 = ((float)((float)L_60*(float)(0.00130208337f)));
		Material_t340375123 * L_61 = __this->get_sunShaftsMaterial_16();
		float L_62 = V_7;
		float L_63 = V_7;
		Vector4_t3319028937  L_64;
		memset(&L_64, 0, sizeof(L_64));
		Vector4__ctor_m1702261405(&L_64, L_62, L_63, (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_61);
		Material_SetVector_m3662241165(L_61, _stringLiteral2517340756, L_64, /*hidden argument*/NULL);
		Material_t340375123 * L_65 = __this->get_sunShaftsMaterial_16();
		float L_66 = (&V_1)->get_x_1();
		float L_67 = (&V_1)->get_y_2();
		float L_68 = (&V_1)->get_z_3();
		float L_69 = __this->get_maxRadius_13();
		Vector4_t3319028937  L_70;
		memset(&L_70, 0, sizeof(L_70));
		Vector4__ctor_m1702261405(&L_70, L_66, L_67, L_68, L_69, /*hidden argument*/NULL);
		NullCheck(L_65);
		Material_SetVector_m3662241165(L_65, _stringLiteral422554171, L_70, /*hidden argument*/NULL);
		V_8 = 0;
		goto IL_0300;
	}

IL_0236:
	{
		int32_t L_71 = V_2;
		int32_t L_72 = V_3;
		RenderTexture_t2108887433 * L_73 = RenderTexture_GetTemporary_m2970995366(NULL /*static, unused*/, L_71, L_72, 0, /*hidden argument*/NULL);
		V_4 = L_73;
		RenderTexture_t2108887433 * L_74 = V_5;
		RenderTexture_t2108887433 * L_75 = V_4;
		Material_t340375123 * L_76 = __this->get_sunShaftsMaterial_16();
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t783367614_il2cpp_TypeInfo_var);
		Graphics_Blit_m2694712511(NULL /*static, unused*/, L_74, L_75, L_76, 1, /*hidden argument*/NULL);
		RenderTexture_t2108887433 * L_77 = V_5;
		RenderTexture_ReleaseTemporary_m739636844(NULL /*static, unused*/, L_77, /*hidden argument*/NULL);
		float L_78 = __this->get_sunShaftBlurRadius_10();
		int32_t L_79 = V_8;
		V_7 = ((float)((float)((float)((float)L_78*(float)((float)((float)((float)((float)((float)((float)(((float)((float)L_79)))*(float)(2.0f)))+(float)(1.0f)))*(float)(6.0f)))))/(float)(768.0f)));
		Material_t340375123 * L_80 = __this->get_sunShaftsMaterial_16();
		float L_81 = V_7;
		float L_82 = V_7;
		Vector4_t3319028937  L_83;
		memset(&L_83, 0, sizeof(L_83));
		Vector4__ctor_m1702261405(&L_83, L_81, L_82, (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_80);
		Material_SetVector_m3662241165(L_80, _stringLiteral2517340756, L_83, /*hidden argument*/NULL);
		int32_t L_84 = V_2;
		int32_t L_85 = V_3;
		RenderTexture_t2108887433 * L_86 = RenderTexture_GetTemporary_m2970995366(NULL /*static, unused*/, L_84, L_85, 0, /*hidden argument*/NULL);
		V_5 = L_86;
		RenderTexture_t2108887433 * L_87 = V_4;
		RenderTexture_t2108887433 * L_88 = V_5;
		Material_t340375123 * L_89 = __this->get_sunShaftsMaterial_16();
		Graphics_Blit_m2694712511(NULL /*static, unused*/, L_87, L_88, L_89, 1, /*hidden argument*/NULL);
		RenderTexture_t2108887433 * L_90 = V_4;
		RenderTexture_ReleaseTemporary_m739636844(NULL /*static, unused*/, L_90, /*hidden argument*/NULL);
		float L_91 = __this->get_sunShaftBlurRadius_10();
		int32_t L_92 = V_8;
		V_7 = ((float)((float)((float)((float)L_91*(float)((float)((float)((float)((float)((float)((float)(((float)((float)L_92)))*(float)(2.0f)))+(float)(2.0f)))*(float)(6.0f)))))/(float)(768.0f)));
		Material_t340375123 * L_93 = __this->get_sunShaftsMaterial_16();
		float L_94 = V_7;
		float L_95 = V_7;
		Vector4_t3319028937  L_96;
		memset(&L_96, 0, sizeof(L_96));
		Vector4__ctor_m1702261405(&L_96, L_94, L_95, (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_93);
		Material_SetVector_m3662241165(L_93, _stringLiteral2517340756, L_96, /*hidden argument*/NULL);
		int32_t L_97 = V_8;
		V_8 = ((int32_t)((int32_t)L_97+(int32_t)1));
	}

IL_0300:
	{
		int32_t L_98 = V_8;
		int32_t L_99 = __this->get_radialBlurIterations_8();
		if ((((int32_t)L_98) < ((int32_t)L_99)))
		{
			goto IL_0236;
		}
	}
	{
		float L_100 = (&V_1)->get_z_3();
		if ((((float)L_100) < ((float)(((float)((float)0))))))
		{
			goto IL_036c;
		}
	}
	{
		Material_t340375123 * L_101 = __this->get_sunShaftsMaterial_16();
		Color_t2555686324 * L_102 = __this->get_address_of_sunColor_9();
		float L_103 = L_102->get_r_0();
		Color_t2555686324 * L_104 = __this->get_address_of_sunColor_9();
		float L_105 = L_104->get_g_1();
		Color_t2555686324 * L_106 = __this->get_address_of_sunColor_9();
		float L_107 = L_106->get_b_2();
		Color_t2555686324 * L_108 = __this->get_address_of_sunColor_9();
		float L_109 = L_108->get_a_3();
		Vector4_t3319028937  L_110;
		memset(&L_110, 0, sizeof(L_110));
		Vector4__ctor_m1702261405(&L_110, L_103, L_105, L_107, L_109, /*hidden argument*/NULL);
		float L_111 = __this->get_sunShaftIntensity_11();
		Vector4_t3319028937  L_112 = Vector4_op_Multiply_m1180700625(NULL /*static, unused*/, L_110, L_111, /*hidden argument*/NULL);
		NullCheck(L_101);
		Material_SetVector_m3662241165(L_101, _stringLiteral2161159581, L_112, /*hidden argument*/NULL);
		goto IL_0381;
	}

IL_036c:
	{
		Material_t340375123 * L_113 = __this->get_sunShaftsMaterial_16();
		Vector4_t3319028937  L_114 = Vector4_get_zero_m3390620726(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_113);
		Material_SetVector_m3662241165(L_113, _stringLiteral2161159581, L_114, /*hidden argument*/NULL);
	}

IL_0381:
	{
		Material_t340375123 * L_115 = __this->get_sunShaftsMaterial_16();
		RenderTexture_t2108887433 * L_116 = V_5;
		NullCheck(L_115);
		Material_SetTexture_m2801217397(L_115, _stringLiteral147475429, L_116, /*hidden argument*/NULL);
		RenderTexture_t2108887433 * L_117 = ___source0;
		RenderTexture_t2108887433 * L_118 = ___destination1;
		Material_t340375123 * L_119 = __this->get_sunShaftsMaterial_16();
		int32_t L_120 = __this->get_screenBlendMode_6();
		G_B21_0 = L_119;
		G_B21_1 = L_118;
		G_B21_2 = L_117;
		if ((!(((uint32_t)L_120) == ((uint32_t)0))))
		{
			G_B22_0 = L_119;
			G_B22_1 = L_118;
			G_B22_2 = L_117;
			goto IL_03ad;
		}
	}
	{
		G_B23_0 = 0;
		G_B23_1 = G_B21_0;
		G_B23_2 = G_B21_1;
		G_B23_3 = G_B21_2;
		goto IL_03ae;
	}

IL_03ad:
	{
		G_B23_0 = 4;
		G_B23_1 = G_B22_0;
		G_B23_2 = G_B22_1;
		G_B23_3 = G_B22_2;
	}

IL_03ae:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t783367614_il2cpp_TypeInfo_var);
		Graphics_Blit_m2694712511(NULL /*static, unused*/, G_B23_3, G_B23_2, G_B23_1, G_B23_0, /*hidden argument*/NULL);
		RenderTexture_t2108887433 * L_121 = V_5;
		RenderTexture_ReleaseTemporary_m739636844(NULL /*static, unused*/, L_121, /*hidden argument*/NULL);
	}

IL_03ba:
	{
		return;
	}
}
// System.Void SunShafts::Main()
extern "C"  void SunShafts_Main_m3593930489 (SunShafts_t3552083306 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
