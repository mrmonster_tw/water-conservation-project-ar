﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Xml.XPath.XPathNavigator
struct XPathNavigator_t787956054;
// System.Xml.XmlNameTable
struct XmlNameTable_t71772148;
// Mono.Xml.XPath.DTMXPathLinkedNode2[]
struct DTMXPathLinkedNode2U5BU5D_t3499139399;
// Mono.Xml.XPath.DTMXPathAttributeNode2[]
struct DTMXPathAttributeNode2U5BU5D_t1081050938;
// Mono.Xml.XPath.DTMXPathNamespaceNode2[]
struct DTMXPathNamespaceNode2U5BU5D_t3587247258;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Collections.Hashtable
struct Hashtable_t1853889766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.XPath.DTMXPathDocument2
struct  DTMXPathDocument2_t2335062455  : public Il2CppObject
{
public:
	// System.Xml.XPath.XPathNavigator Mono.Xml.XPath.DTMXPathDocument2::root
	XPathNavigator_t787956054 * ___root_0;
	// System.Xml.XmlNameTable Mono.Xml.XPath.DTMXPathDocument2::NameTable
	XmlNameTable_t71772148 * ___NameTable_1;
	// Mono.Xml.XPath.DTMXPathLinkedNode2[] Mono.Xml.XPath.DTMXPathDocument2::Nodes
	DTMXPathLinkedNode2U5BU5D_t3499139399* ___Nodes_2;
	// Mono.Xml.XPath.DTMXPathAttributeNode2[] Mono.Xml.XPath.DTMXPathDocument2::Attributes
	DTMXPathAttributeNode2U5BU5D_t1081050938* ___Attributes_3;
	// Mono.Xml.XPath.DTMXPathNamespaceNode2[] Mono.Xml.XPath.DTMXPathDocument2::Namespaces
	DTMXPathNamespaceNode2U5BU5D_t3587247258* ___Namespaces_4;
	// System.String[] Mono.Xml.XPath.DTMXPathDocument2::AtomicStringPool
	StringU5BU5D_t1281789340* ___AtomicStringPool_5;
	// System.String[] Mono.Xml.XPath.DTMXPathDocument2::NonAtomicStringPool
	StringU5BU5D_t1281789340* ___NonAtomicStringPool_6;
	// System.Collections.Hashtable Mono.Xml.XPath.DTMXPathDocument2::IdTable
	Hashtable_t1853889766 * ___IdTable_7;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(DTMXPathDocument2_t2335062455, ___root_0)); }
	inline XPathNavigator_t787956054 * get_root_0() const { return ___root_0; }
	inline XPathNavigator_t787956054 ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(XPathNavigator_t787956054 * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier(&___root_0, value);
	}

	inline static int32_t get_offset_of_NameTable_1() { return static_cast<int32_t>(offsetof(DTMXPathDocument2_t2335062455, ___NameTable_1)); }
	inline XmlNameTable_t71772148 * get_NameTable_1() const { return ___NameTable_1; }
	inline XmlNameTable_t71772148 ** get_address_of_NameTable_1() { return &___NameTable_1; }
	inline void set_NameTable_1(XmlNameTable_t71772148 * value)
	{
		___NameTable_1 = value;
		Il2CppCodeGenWriteBarrier(&___NameTable_1, value);
	}

	inline static int32_t get_offset_of_Nodes_2() { return static_cast<int32_t>(offsetof(DTMXPathDocument2_t2335062455, ___Nodes_2)); }
	inline DTMXPathLinkedNode2U5BU5D_t3499139399* get_Nodes_2() const { return ___Nodes_2; }
	inline DTMXPathLinkedNode2U5BU5D_t3499139399** get_address_of_Nodes_2() { return &___Nodes_2; }
	inline void set_Nodes_2(DTMXPathLinkedNode2U5BU5D_t3499139399* value)
	{
		___Nodes_2 = value;
		Il2CppCodeGenWriteBarrier(&___Nodes_2, value);
	}

	inline static int32_t get_offset_of_Attributes_3() { return static_cast<int32_t>(offsetof(DTMXPathDocument2_t2335062455, ___Attributes_3)); }
	inline DTMXPathAttributeNode2U5BU5D_t1081050938* get_Attributes_3() const { return ___Attributes_3; }
	inline DTMXPathAttributeNode2U5BU5D_t1081050938** get_address_of_Attributes_3() { return &___Attributes_3; }
	inline void set_Attributes_3(DTMXPathAttributeNode2U5BU5D_t1081050938* value)
	{
		___Attributes_3 = value;
		Il2CppCodeGenWriteBarrier(&___Attributes_3, value);
	}

	inline static int32_t get_offset_of_Namespaces_4() { return static_cast<int32_t>(offsetof(DTMXPathDocument2_t2335062455, ___Namespaces_4)); }
	inline DTMXPathNamespaceNode2U5BU5D_t3587247258* get_Namespaces_4() const { return ___Namespaces_4; }
	inline DTMXPathNamespaceNode2U5BU5D_t3587247258** get_address_of_Namespaces_4() { return &___Namespaces_4; }
	inline void set_Namespaces_4(DTMXPathNamespaceNode2U5BU5D_t3587247258* value)
	{
		___Namespaces_4 = value;
		Il2CppCodeGenWriteBarrier(&___Namespaces_4, value);
	}

	inline static int32_t get_offset_of_AtomicStringPool_5() { return static_cast<int32_t>(offsetof(DTMXPathDocument2_t2335062455, ___AtomicStringPool_5)); }
	inline StringU5BU5D_t1281789340* get_AtomicStringPool_5() const { return ___AtomicStringPool_5; }
	inline StringU5BU5D_t1281789340** get_address_of_AtomicStringPool_5() { return &___AtomicStringPool_5; }
	inline void set_AtomicStringPool_5(StringU5BU5D_t1281789340* value)
	{
		___AtomicStringPool_5 = value;
		Il2CppCodeGenWriteBarrier(&___AtomicStringPool_5, value);
	}

	inline static int32_t get_offset_of_NonAtomicStringPool_6() { return static_cast<int32_t>(offsetof(DTMXPathDocument2_t2335062455, ___NonAtomicStringPool_6)); }
	inline StringU5BU5D_t1281789340* get_NonAtomicStringPool_6() const { return ___NonAtomicStringPool_6; }
	inline StringU5BU5D_t1281789340** get_address_of_NonAtomicStringPool_6() { return &___NonAtomicStringPool_6; }
	inline void set_NonAtomicStringPool_6(StringU5BU5D_t1281789340* value)
	{
		___NonAtomicStringPool_6 = value;
		Il2CppCodeGenWriteBarrier(&___NonAtomicStringPool_6, value);
	}

	inline static int32_t get_offset_of_IdTable_7() { return static_cast<int32_t>(offsetof(DTMXPathDocument2_t2335062455, ___IdTable_7)); }
	inline Hashtable_t1853889766 * get_IdTable_7() const { return ___IdTable_7; }
	inline Hashtable_t1853889766 ** get_address_of_IdTable_7() { return &___IdTable_7; }
	inline void set_IdTable_7(Hashtable_t1853889766 * value)
	{
		___IdTable_7 = value;
		Il2CppCodeGenWriteBarrier(&___IdTable_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
