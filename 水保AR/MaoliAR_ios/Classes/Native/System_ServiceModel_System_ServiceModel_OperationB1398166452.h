﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"
#include "System_ServiceModel_System_ServiceModel_Impersonat2275648231.h"
#include "System_ServiceModel_System_ServiceModel_ReleaseIns1178280783.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.OperationBehaviorAttribute
struct  OperationBehaviorAttribute_t1398166452  : public Attribute_t861562559
{
public:
	// System.ServiceModel.ImpersonationOption System.ServiceModel.OperationBehaviorAttribute::impersonation
	int32_t ___impersonation_0;
	// System.Boolean System.ServiceModel.OperationBehaviorAttribute::tx_auto_complete
	bool ___tx_auto_complete_1;
	// System.Boolean System.ServiceModel.OperationBehaviorAttribute::tx_scope_required
	bool ___tx_scope_required_2;
	// System.Boolean System.ServiceModel.OperationBehaviorAttribute::auto_dispose_params
	bool ___auto_dispose_params_3;
	// System.ServiceModel.ReleaseInstanceMode System.ServiceModel.OperationBehaviorAttribute::mode
	int32_t ___mode_4;

public:
	inline static int32_t get_offset_of_impersonation_0() { return static_cast<int32_t>(offsetof(OperationBehaviorAttribute_t1398166452, ___impersonation_0)); }
	inline int32_t get_impersonation_0() const { return ___impersonation_0; }
	inline int32_t* get_address_of_impersonation_0() { return &___impersonation_0; }
	inline void set_impersonation_0(int32_t value)
	{
		___impersonation_0 = value;
	}

	inline static int32_t get_offset_of_tx_auto_complete_1() { return static_cast<int32_t>(offsetof(OperationBehaviorAttribute_t1398166452, ___tx_auto_complete_1)); }
	inline bool get_tx_auto_complete_1() const { return ___tx_auto_complete_1; }
	inline bool* get_address_of_tx_auto_complete_1() { return &___tx_auto_complete_1; }
	inline void set_tx_auto_complete_1(bool value)
	{
		___tx_auto_complete_1 = value;
	}

	inline static int32_t get_offset_of_tx_scope_required_2() { return static_cast<int32_t>(offsetof(OperationBehaviorAttribute_t1398166452, ___tx_scope_required_2)); }
	inline bool get_tx_scope_required_2() const { return ___tx_scope_required_2; }
	inline bool* get_address_of_tx_scope_required_2() { return &___tx_scope_required_2; }
	inline void set_tx_scope_required_2(bool value)
	{
		___tx_scope_required_2 = value;
	}

	inline static int32_t get_offset_of_auto_dispose_params_3() { return static_cast<int32_t>(offsetof(OperationBehaviorAttribute_t1398166452, ___auto_dispose_params_3)); }
	inline bool get_auto_dispose_params_3() const { return ___auto_dispose_params_3; }
	inline bool* get_address_of_auto_dispose_params_3() { return &___auto_dispose_params_3; }
	inline void set_auto_dispose_params_3(bool value)
	{
		___auto_dispose_params_3 = value;
	}

	inline static int32_t get_offset_of_mode_4() { return static_cast<int32_t>(offsetof(OperationBehaviorAttribute_t1398166452, ___mode_4)); }
	inline int32_t get_mode_4() const { return ___mode_4; }
	inline int32_t* get_address_of_mode_4() { return &___mode_4; }
	inline void set_mode_4(int32_t value)
	{
		___mode_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
