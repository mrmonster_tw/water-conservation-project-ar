﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_BaseTemplateParser265587934.h"
#include "System_Web_System_Web_UI_CompilationMode3525881133.h"

// System.IO.TextReader
struct TextReader_t283511965;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.TemplateControlParser
struct  TemplateControlParser_t3072921516  : public BaseTemplateParser_t265587934
{
public:
	// System.Boolean System.Web.UI.TemplateControlParser::autoEventWireup
	bool ___autoEventWireup_57;
	// System.Boolean System.Web.UI.TemplateControlParser::enableViewState
	bool ___enableViewState_58;
	// System.Web.UI.CompilationMode System.Web.UI.TemplateControlParser::compilationMode
	int32_t ___compilationMode_59;
	// System.IO.TextReader System.Web.UI.TemplateControlParser::reader
	TextReader_t283511965 * ___reader_60;

public:
	inline static int32_t get_offset_of_autoEventWireup_57() { return static_cast<int32_t>(offsetof(TemplateControlParser_t3072921516, ___autoEventWireup_57)); }
	inline bool get_autoEventWireup_57() const { return ___autoEventWireup_57; }
	inline bool* get_address_of_autoEventWireup_57() { return &___autoEventWireup_57; }
	inline void set_autoEventWireup_57(bool value)
	{
		___autoEventWireup_57 = value;
	}

	inline static int32_t get_offset_of_enableViewState_58() { return static_cast<int32_t>(offsetof(TemplateControlParser_t3072921516, ___enableViewState_58)); }
	inline bool get_enableViewState_58() const { return ___enableViewState_58; }
	inline bool* get_address_of_enableViewState_58() { return &___enableViewState_58; }
	inline void set_enableViewState_58(bool value)
	{
		___enableViewState_58 = value;
	}

	inline static int32_t get_offset_of_compilationMode_59() { return static_cast<int32_t>(offsetof(TemplateControlParser_t3072921516, ___compilationMode_59)); }
	inline int32_t get_compilationMode_59() const { return ___compilationMode_59; }
	inline int32_t* get_address_of_compilationMode_59() { return &___compilationMode_59; }
	inline void set_compilationMode_59(int32_t value)
	{
		___compilationMode_59 = value;
	}

	inline static int32_t get_offset_of_reader_60() { return static_cast<int32_t>(offsetof(TemplateControlParser_t3072921516, ___reader_60)); }
	inline TextReader_t283511965 * get_reader_60() const { return ___reader_60; }
	inline TextReader_t283511965 ** get_address_of_reader_60() { return &___reader_60; }
	inline void set_reader_60(TextReader_t283511965 * value)
	{
		___reader_60 = value;
		Il2CppCodeGenWriteBarrier(&___reader_60, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
