﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.UI.Button
struct Button_t4055032469;
// System.Predicate`1<UnityEngine.UI.Button>
struct Predicate_1_t585359297;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnEnableCallEvent
struct  OnEnableCallEvent_t3645046440  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button OnEnableCallEvent::b
	Button_t4055032469 * ___b_2;
	// System.Boolean OnEnableCallEvent::enb
	bool ___enb_3;

public:
	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(OnEnableCallEvent_t3645046440, ___b_2)); }
	inline Button_t4055032469 * get_b_2() const { return ___b_2; }
	inline Button_t4055032469 ** get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(Button_t4055032469 * value)
	{
		___b_2 = value;
		Il2CppCodeGenWriteBarrier(&___b_2, value);
	}

	inline static int32_t get_offset_of_enb_3() { return static_cast<int32_t>(offsetof(OnEnableCallEvent_t3645046440, ___enb_3)); }
	inline bool get_enb_3() const { return ___enb_3; }
	inline bool* get_address_of_enb_3() { return &___enb_3; }
	inline void set_enb_3(bool value)
	{
		___enb_3 = value;
	}
};

struct OnEnableCallEvent_t3645046440_StaticFields
{
public:
	// System.Predicate`1<UnityEngine.UI.Button> OnEnableCallEvent::<>f__am$cache0
	Predicate_1_t585359297 * ___U3CU3Ef__amU24cache0_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(OnEnableCallEvent_t3645046440_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Predicate_1_t585359297 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Predicate_1_t585359297 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Predicate_1_t585359297 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
