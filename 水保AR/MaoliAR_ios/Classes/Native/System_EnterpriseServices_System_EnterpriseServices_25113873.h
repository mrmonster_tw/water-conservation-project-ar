﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"
#include "mscorlib_System_Guid3193532887.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EnterpriseServices.ApplicationIDAttribute
struct  ApplicationIDAttribute_t25113873  : public Attribute_t861562559
{
public:
	// System.Guid System.EnterpriseServices.ApplicationIDAttribute::guid
	Guid_t  ___guid_0;

public:
	inline static int32_t get_offset_of_guid_0() { return static_cast<int32_t>(offsetof(ApplicationIDAttribute_t25113873, ___guid_0)); }
	inline Guid_t  get_guid_0() const { return ___guid_0; }
	inline Guid_t * get_address_of_guid_0() { return &___guid_0; }
	inline void set_guid_0(Guid_t  value)
	{
		___guid_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
