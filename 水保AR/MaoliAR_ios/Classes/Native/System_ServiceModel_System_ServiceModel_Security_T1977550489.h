﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Tokens_S1271873540.h"
#include "mscorlib_System_Nullable_1_gen378540539.h"

// System.String
struct String_t;
// System.IdentityModel.Tokens.SecurityKeyIdentifierClause
struct SecurityKeyIdentifierClause_t1943429813;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IdentityModel.Tokens.SecurityKey>
struct ReadOnlyCollection_1_t1046284793;
// System.Security.Cryptography.Xml.ReferenceList
struct ReferenceList_t2222396100;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.DerivedKeySecurityToken
struct  DerivedKeySecurityToken_t1977550489  : public SecurityToken_t1271873540
{
public:
	// System.String System.ServiceModel.Security.Tokens.DerivedKeySecurityToken::algorithm
	String_t* ___algorithm_0;
	// System.IdentityModel.Tokens.SecurityKeyIdentifierClause System.ServiceModel.Security.Tokens.DerivedKeySecurityToken::reference
	SecurityKeyIdentifierClause_t1943429813 * ___reference_1;
	// System.Nullable`1<System.Int32> System.ServiceModel.Security.Tokens.DerivedKeySecurityToken::generation
	Nullable_1_t378540539  ___generation_2;
	// System.Nullable`1<System.Int32> System.ServiceModel.Security.Tokens.DerivedKeySecurityToken::offset
	Nullable_1_t378540539  ___offset_3;
	// System.Nullable`1<System.Int32> System.ServiceModel.Security.Tokens.DerivedKeySecurityToken::length
	Nullable_1_t378540539  ___length_4;
	// System.String System.ServiceModel.Security.Tokens.DerivedKeySecurityToken::id
	String_t* ___id_5;
	// System.String System.ServiceModel.Security.Tokens.DerivedKeySecurityToken::name
	String_t* ___name_6;
	// System.String System.ServiceModel.Security.Tokens.DerivedKeySecurityToken::label
	String_t* ___label_7;
	// System.Byte[] System.ServiceModel.Security.Tokens.DerivedKeySecurityToken::nonce
	ByteU5BU5D_t4116647657* ___nonce_8;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IdentityModel.Tokens.SecurityKey> System.ServiceModel.Security.Tokens.DerivedKeySecurityToken::keys
	ReadOnlyCollection_1_t1046284793 * ___keys_9;
	// System.Security.Cryptography.Xml.ReferenceList System.ServiceModel.Security.Tokens.DerivedKeySecurityToken::reflist
	ReferenceList_t2222396100 * ___reflist_10;

public:
	inline static int32_t get_offset_of_algorithm_0() { return static_cast<int32_t>(offsetof(DerivedKeySecurityToken_t1977550489, ___algorithm_0)); }
	inline String_t* get_algorithm_0() const { return ___algorithm_0; }
	inline String_t** get_address_of_algorithm_0() { return &___algorithm_0; }
	inline void set_algorithm_0(String_t* value)
	{
		___algorithm_0 = value;
		Il2CppCodeGenWriteBarrier(&___algorithm_0, value);
	}

	inline static int32_t get_offset_of_reference_1() { return static_cast<int32_t>(offsetof(DerivedKeySecurityToken_t1977550489, ___reference_1)); }
	inline SecurityKeyIdentifierClause_t1943429813 * get_reference_1() const { return ___reference_1; }
	inline SecurityKeyIdentifierClause_t1943429813 ** get_address_of_reference_1() { return &___reference_1; }
	inline void set_reference_1(SecurityKeyIdentifierClause_t1943429813 * value)
	{
		___reference_1 = value;
		Il2CppCodeGenWriteBarrier(&___reference_1, value);
	}

	inline static int32_t get_offset_of_generation_2() { return static_cast<int32_t>(offsetof(DerivedKeySecurityToken_t1977550489, ___generation_2)); }
	inline Nullable_1_t378540539  get_generation_2() const { return ___generation_2; }
	inline Nullable_1_t378540539 * get_address_of_generation_2() { return &___generation_2; }
	inline void set_generation_2(Nullable_1_t378540539  value)
	{
		___generation_2 = value;
	}

	inline static int32_t get_offset_of_offset_3() { return static_cast<int32_t>(offsetof(DerivedKeySecurityToken_t1977550489, ___offset_3)); }
	inline Nullable_1_t378540539  get_offset_3() const { return ___offset_3; }
	inline Nullable_1_t378540539 * get_address_of_offset_3() { return &___offset_3; }
	inline void set_offset_3(Nullable_1_t378540539  value)
	{
		___offset_3 = value;
	}

	inline static int32_t get_offset_of_length_4() { return static_cast<int32_t>(offsetof(DerivedKeySecurityToken_t1977550489, ___length_4)); }
	inline Nullable_1_t378540539  get_length_4() const { return ___length_4; }
	inline Nullable_1_t378540539 * get_address_of_length_4() { return &___length_4; }
	inline void set_length_4(Nullable_1_t378540539  value)
	{
		___length_4 = value;
	}

	inline static int32_t get_offset_of_id_5() { return static_cast<int32_t>(offsetof(DerivedKeySecurityToken_t1977550489, ___id_5)); }
	inline String_t* get_id_5() const { return ___id_5; }
	inline String_t** get_address_of_id_5() { return &___id_5; }
	inline void set_id_5(String_t* value)
	{
		___id_5 = value;
		Il2CppCodeGenWriteBarrier(&___id_5, value);
	}

	inline static int32_t get_offset_of_name_6() { return static_cast<int32_t>(offsetof(DerivedKeySecurityToken_t1977550489, ___name_6)); }
	inline String_t* get_name_6() const { return ___name_6; }
	inline String_t** get_address_of_name_6() { return &___name_6; }
	inline void set_name_6(String_t* value)
	{
		___name_6 = value;
		Il2CppCodeGenWriteBarrier(&___name_6, value);
	}

	inline static int32_t get_offset_of_label_7() { return static_cast<int32_t>(offsetof(DerivedKeySecurityToken_t1977550489, ___label_7)); }
	inline String_t* get_label_7() const { return ___label_7; }
	inline String_t** get_address_of_label_7() { return &___label_7; }
	inline void set_label_7(String_t* value)
	{
		___label_7 = value;
		Il2CppCodeGenWriteBarrier(&___label_7, value);
	}

	inline static int32_t get_offset_of_nonce_8() { return static_cast<int32_t>(offsetof(DerivedKeySecurityToken_t1977550489, ___nonce_8)); }
	inline ByteU5BU5D_t4116647657* get_nonce_8() const { return ___nonce_8; }
	inline ByteU5BU5D_t4116647657** get_address_of_nonce_8() { return &___nonce_8; }
	inline void set_nonce_8(ByteU5BU5D_t4116647657* value)
	{
		___nonce_8 = value;
		Il2CppCodeGenWriteBarrier(&___nonce_8, value);
	}

	inline static int32_t get_offset_of_keys_9() { return static_cast<int32_t>(offsetof(DerivedKeySecurityToken_t1977550489, ___keys_9)); }
	inline ReadOnlyCollection_1_t1046284793 * get_keys_9() const { return ___keys_9; }
	inline ReadOnlyCollection_1_t1046284793 ** get_address_of_keys_9() { return &___keys_9; }
	inline void set_keys_9(ReadOnlyCollection_1_t1046284793 * value)
	{
		___keys_9 = value;
		Il2CppCodeGenWriteBarrier(&___keys_9, value);
	}

	inline static int32_t get_offset_of_reflist_10() { return static_cast<int32_t>(offsetof(DerivedKeySecurityToken_t1977550489, ___reflist_10)); }
	inline ReferenceList_t2222396100 * get_reflist_10() const { return ___reflist_10; }
	inline ReferenceList_t2222396100 ** get_address_of_reflist_10() { return &___reflist_10; }
	inline void set_reflist_10(ReferenceList_t2222396100 * value)
	{
		___reflist_10 = value;
		Il2CppCodeGenWriteBarrier(&___reflist_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
