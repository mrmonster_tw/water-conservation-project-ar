﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Tokens_S1271873540.h"
#include "mscorlib_System_DateTime3738529785.h"

// System.String
struct String_t;
// System.IdentityModel.Tokens.InMemorySymmetricSecurityKey
struct InMemorySymmetricSecurityKey_t3171133310;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IdentityModel.Tokens.SecurityKey>
struct ReadOnlyCollection_1_t1046284793;
// System.Xml.UniqueId
struct UniqueId_t1383576913;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IdentityModel.Policy.IAuthorizationPolicy>
struct ReadOnlyCollection_1_t778487864;
// System.Byte[]
struct ByteU5BU5D_t4116647657;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.SecurityContextSecurityToken
struct  SecurityContextSecurityToken_t3624779732  : public SecurityToken_t1271873540
{
public:
	// System.String System.ServiceModel.Security.Tokens.SecurityContextSecurityToken::id
	String_t* ___id_0;
	// System.IdentityModel.Tokens.InMemorySymmetricSecurityKey System.ServiceModel.Security.Tokens.SecurityContextSecurityToken::key
	InMemorySymmetricSecurityKey_t3171133310 * ___key_1;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IdentityModel.Tokens.SecurityKey> System.ServiceModel.Security.Tokens.SecurityContextSecurityToken::keys
	ReadOnlyCollection_1_t1046284793 * ___keys_2;
	// System.DateTime System.ServiceModel.Security.Tokens.SecurityContextSecurityToken::token_since
	DateTime_t3738529785  ___token_since_3;
	// System.DateTime System.ServiceModel.Security.Tokens.SecurityContextSecurityToken::token_until
	DateTime_t3738529785  ___token_until_4;
	// System.DateTime System.ServiceModel.Security.Tokens.SecurityContextSecurityToken::key_since
	DateTime_t3738529785  ___key_since_5;
	// System.DateTime System.ServiceModel.Security.Tokens.SecurityContextSecurityToken::key_until
	DateTime_t3738529785  ___key_until_6;
	// System.Xml.UniqueId System.ServiceModel.Security.Tokens.SecurityContextSecurityToken::context_id
	UniqueId_t1383576913 * ___context_id_7;
	// System.Xml.UniqueId System.ServiceModel.Security.Tokens.SecurityContextSecurityToken::key_generation
	UniqueId_t1383576913 * ___key_generation_8;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IdentityModel.Policy.IAuthorizationPolicy> System.ServiceModel.Security.Tokens.SecurityContextSecurityToken::policies
	ReadOnlyCollection_1_t778487864 * ___policies_9;
	// System.Byte[] System.ServiceModel.Security.Tokens.SecurityContextSecurityToken::cookie
	ByteU5BU5D_t4116647657* ___cookie_10;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(SecurityContextSecurityToken_t3624779732, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier(&___id_0, value);
	}

	inline static int32_t get_offset_of_key_1() { return static_cast<int32_t>(offsetof(SecurityContextSecurityToken_t3624779732, ___key_1)); }
	inline InMemorySymmetricSecurityKey_t3171133310 * get_key_1() const { return ___key_1; }
	inline InMemorySymmetricSecurityKey_t3171133310 ** get_address_of_key_1() { return &___key_1; }
	inline void set_key_1(InMemorySymmetricSecurityKey_t3171133310 * value)
	{
		___key_1 = value;
		Il2CppCodeGenWriteBarrier(&___key_1, value);
	}

	inline static int32_t get_offset_of_keys_2() { return static_cast<int32_t>(offsetof(SecurityContextSecurityToken_t3624779732, ___keys_2)); }
	inline ReadOnlyCollection_1_t1046284793 * get_keys_2() const { return ___keys_2; }
	inline ReadOnlyCollection_1_t1046284793 ** get_address_of_keys_2() { return &___keys_2; }
	inline void set_keys_2(ReadOnlyCollection_1_t1046284793 * value)
	{
		___keys_2 = value;
		Il2CppCodeGenWriteBarrier(&___keys_2, value);
	}

	inline static int32_t get_offset_of_token_since_3() { return static_cast<int32_t>(offsetof(SecurityContextSecurityToken_t3624779732, ___token_since_3)); }
	inline DateTime_t3738529785  get_token_since_3() const { return ___token_since_3; }
	inline DateTime_t3738529785 * get_address_of_token_since_3() { return &___token_since_3; }
	inline void set_token_since_3(DateTime_t3738529785  value)
	{
		___token_since_3 = value;
	}

	inline static int32_t get_offset_of_token_until_4() { return static_cast<int32_t>(offsetof(SecurityContextSecurityToken_t3624779732, ___token_until_4)); }
	inline DateTime_t3738529785  get_token_until_4() const { return ___token_until_4; }
	inline DateTime_t3738529785 * get_address_of_token_until_4() { return &___token_until_4; }
	inline void set_token_until_4(DateTime_t3738529785  value)
	{
		___token_until_4 = value;
	}

	inline static int32_t get_offset_of_key_since_5() { return static_cast<int32_t>(offsetof(SecurityContextSecurityToken_t3624779732, ___key_since_5)); }
	inline DateTime_t3738529785  get_key_since_5() const { return ___key_since_5; }
	inline DateTime_t3738529785 * get_address_of_key_since_5() { return &___key_since_5; }
	inline void set_key_since_5(DateTime_t3738529785  value)
	{
		___key_since_5 = value;
	}

	inline static int32_t get_offset_of_key_until_6() { return static_cast<int32_t>(offsetof(SecurityContextSecurityToken_t3624779732, ___key_until_6)); }
	inline DateTime_t3738529785  get_key_until_6() const { return ___key_until_6; }
	inline DateTime_t3738529785 * get_address_of_key_until_6() { return &___key_until_6; }
	inline void set_key_until_6(DateTime_t3738529785  value)
	{
		___key_until_6 = value;
	}

	inline static int32_t get_offset_of_context_id_7() { return static_cast<int32_t>(offsetof(SecurityContextSecurityToken_t3624779732, ___context_id_7)); }
	inline UniqueId_t1383576913 * get_context_id_7() const { return ___context_id_7; }
	inline UniqueId_t1383576913 ** get_address_of_context_id_7() { return &___context_id_7; }
	inline void set_context_id_7(UniqueId_t1383576913 * value)
	{
		___context_id_7 = value;
		Il2CppCodeGenWriteBarrier(&___context_id_7, value);
	}

	inline static int32_t get_offset_of_key_generation_8() { return static_cast<int32_t>(offsetof(SecurityContextSecurityToken_t3624779732, ___key_generation_8)); }
	inline UniqueId_t1383576913 * get_key_generation_8() const { return ___key_generation_8; }
	inline UniqueId_t1383576913 ** get_address_of_key_generation_8() { return &___key_generation_8; }
	inline void set_key_generation_8(UniqueId_t1383576913 * value)
	{
		___key_generation_8 = value;
		Il2CppCodeGenWriteBarrier(&___key_generation_8, value);
	}

	inline static int32_t get_offset_of_policies_9() { return static_cast<int32_t>(offsetof(SecurityContextSecurityToken_t3624779732, ___policies_9)); }
	inline ReadOnlyCollection_1_t778487864 * get_policies_9() const { return ___policies_9; }
	inline ReadOnlyCollection_1_t778487864 ** get_address_of_policies_9() { return &___policies_9; }
	inline void set_policies_9(ReadOnlyCollection_1_t778487864 * value)
	{
		___policies_9 = value;
		Il2CppCodeGenWriteBarrier(&___policies_9, value);
	}

	inline static int32_t get_offset_of_cookie_10() { return static_cast<int32_t>(offsetof(SecurityContextSecurityToken_t3624779732, ___cookie_10)); }
	inline ByteU5BU5D_t4116647657* get_cookie_10() const { return ___cookie_10; }
	inline ByteU5BU5D_t4116647657** get_address_of_cookie_10() { return &___cookie_10; }
	inline void set_cookie_10(ByteU5BU5D_t4116647657* value)
	{
		___cookie_10 = value;
		Il2CppCodeGenWriteBarrier(&___cookie_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
