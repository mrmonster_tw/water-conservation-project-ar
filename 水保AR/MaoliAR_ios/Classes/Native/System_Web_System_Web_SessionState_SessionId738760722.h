﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t386037858;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.SessionState.SessionId
struct  SessionId_t738760722  : public Il2CppObject
{
public:

public:
};

struct SessionId_t738760722_StaticFields
{
public:
	// System.Char[] System.Web.SessionState.SessionId::allowed
	CharU5BU5D_t3528271667* ___allowed_0;
	// System.Security.Cryptography.RandomNumberGenerator System.Web.SessionState.SessionId::rng
	RandomNumberGenerator_t386037858 * ___rng_1;

public:
	inline static int32_t get_offset_of_allowed_0() { return static_cast<int32_t>(offsetof(SessionId_t738760722_StaticFields, ___allowed_0)); }
	inline CharU5BU5D_t3528271667* get_allowed_0() const { return ___allowed_0; }
	inline CharU5BU5D_t3528271667** get_address_of_allowed_0() { return &___allowed_0; }
	inline void set_allowed_0(CharU5BU5D_t3528271667* value)
	{
		___allowed_0 = value;
		Il2CppCodeGenWriteBarrier(&___allowed_0, value);
	}

	inline static int32_t get_offset_of_rng_1() { return static_cast<int32_t>(offsetof(SessionId_t738760722_StaticFields, ___rng_1)); }
	inline RandomNumberGenerator_t386037858 * get_rng_1() const { return ___rng_1; }
	inline RandomNumberGenerator_t386037858 ** get_address_of_rng_1() { return &___rng_1; }
	inline void set_rng_1(RandomNumberGenerator_t386037858 * value)
	{
		___rng_1 = value;
		Il2CppCodeGenWriteBarrier(&___rng_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
