﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.SoapAttributeAttribute
struct  SoapAttributeAttribute_t495928767  : public Attribute_t861562559
{
public:
	// System.String System.Xml.Serialization.SoapAttributeAttribute::attrName
	String_t* ___attrName_0;
	// System.String System.Xml.Serialization.SoapAttributeAttribute::dataType
	String_t* ___dataType_1;
	// System.String System.Xml.Serialization.SoapAttributeAttribute::ns
	String_t* ___ns_2;

public:
	inline static int32_t get_offset_of_attrName_0() { return static_cast<int32_t>(offsetof(SoapAttributeAttribute_t495928767, ___attrName_0)); }
	inline String_t* get_attrName_0() const { return ___attrName_0; }
	inline String_t** get_address_of_attrName_0() { return &___attrName_0; }
	inline void set_attrName_0(String_t* value)
	{
		___attrName_0 = value;
		Il2CppCodeGenWriteBarrier(&___attrName_0, value);
	}

	inline static int32_t get_offset_of_dataType_1() { return static_cast<int32_t>(offsetof(SoapAttributeAttribute_t495928767, ___dataType_1)); }
	inline String_t* get_dataType_1() const { return ___dataType_1; }
	inline String_t** get_address_of_dataType_1() { return &___dataType_1; }
	inline void set_dataType_1(String_t* value)
	{
		___dataType_1 = value;
		Il2CppCodeGenWriteBarrier(&___dataType_1, value);
	}

	inline static int32_t get_offset_of_ns_2() { return static_cast<int32_t>(offsetof(SoapAttributeAttribute_t495928767, ___ns_2)); }
	inline String_t* get_ns_2() const { return ___ns_2; }
	inline String_t** get_address_of_ns_2() { return &___ns_2; }
	inline void set_ns_2(String_t* value)
	{
		___ns_2 = value;
		Il2CppCodeGenWriteBarrier(&___ns_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
