﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_HtmlControls_HtmlContainer641877197.h"
#include "mscorlib_System_Nullable_1_gen1819850047.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.HtmlControls.HtmlForm
struct  HtmlForm_t2905737469  : public HtmlContainerControl_t641877197
{
public:
	// System.Boolean System.Web.UI.HtmlControls.HtmlForm::inited
	bool ___inited_30;
	// System.String System.Web.UI.HtmlControls.HtmlForm::_defaultbutton
	String_t* ____defaultbutton_31;
	// System.String System.Web.UI.HtmlControls.HtmlForm::_defaultfocus
	String_t* ____defaultfocus_32;
	// System.Boolean System.Web.UI.HtmlControls.HtmlForm::submitdisabledcontrols
	bool ___submitdisabledcontrols_33;
	// System.Nullable`1<System.Boolean> System.Web.UI.HtmlControls.HtmlForm::isUplevel
	Nullable_1_t1819850047  ___isUplevel_34;

public:
	inline static int32_t get_offset_of_inited_30() { return static_cast<int32_t>(offsetof(HtmlForm_t2905737469, ___inited_30)); }
	inline bool get_inited_30() const { return ___inited_30; }
	inline bool* get_address_of_inited_30() { return &___inited_30; }
	inline void set_inited_30(bool value)
	{
		___inited_30 = value;
	}

	inline static int32_t get_offset_of__defaultbutton_31() { return static_cast<int32_t>(offsetof(HtmlForm_t2905737469, ____defaultbutton_31)); }
	inline String_t* get__defaultbutton_31() const { return ____defaultbutton_31; }
	inline String_t** get_address_of__defaultbutton_31() { return &____defaultbutton_31; }
	inline void set__defaultbutton_31(String_t* value)
	{
		____defaultbutton_31 = value;
		Il2CppCodeGenWriteBarrier(&____defaultbutton_31, value);
	}

	inline static int32_t get_offset_of__defaultfocus_32() { return static_cast<int32_t>(offsetof(HtmlForm_t2905737469, ____defaultfocus_32)); }
	inline String_t* get__defaultfocus_32() const { return ____defaultfocus_32; }
	inline String_t** get_address_of__defaultfocus_32() { return &____defaultfocus_32; }
	inline void set__defaultfocus_32(String_t* value)
	{
		____defaultfocus_32 = value;
		Il2CppCodeGenWriteBarrier(&____defaultfocus_32, value);
	}

	inline static int32_t get_offset_of_submitdisabledcontrols_33() { return static_cast<int32_t>(offsetof(HtmlForm_t2905737469, ___submitdisabledcontrols_33)); }
	inline bool get_submitdisabledcontrols_33() const { return ___submitdisabledcontrols_33; }
	inline bool* get_address_of_submitdisabledcontrols_33() { return &___submitdisabledcontrols_33; }
	inline void set_submitdisabledcontrols_33(bool value)
	{
		___submitdisabledcontrols_33 = value;
	}

	inline static int32_t get_offset_of_isUplevel_34() { return static_cast<int32_t>(offsetof(HtmlForm_t2905737469, ___isUplevel_34)); }
	inline Nullable_1_t1819850047  get_isUplevel_34() const { return ___isUplevel_34; }
	inline Nullable_1_t1819850047 * get_address_of_isUplevel_34() { return &___isUplevel_34; }
	inline void set_isUplevel_34(Nullable_1_t1819850047  value)
	{
		___isUplevel_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
