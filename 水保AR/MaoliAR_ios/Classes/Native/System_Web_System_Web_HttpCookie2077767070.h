﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_DateTime3738529785.h"
#include "System_Web_System_Web_CookieFlags3398431624.h"

// System.String
struct String_t;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t407452768;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpCookie
struct  HttpCookie_t2077767070  : public Il2CppObject
{
public:
	// System.String System.Web.HttpCookie::path
	String_t* ___path_0;
	// System.String System.Web.HttpCookie::domain
	String_t* ___domain_1;
	// System.DateTime System.Web.HttpCookie::expires
	DateTime_t3738529785  ___expires_2;
	// System.String System.Web.HttpCookie::name
	String_t* ___name_3;
	// System.Web.CookieFlags System.Web.HttpCookie::flags
	uint8_t ___flags_4;
	// System.Collections.Specialized.NameValueCollection System.Web.HttpCookie::values
	NameValueCollection_t407452768 * ___values_5;

public:
	inline static int32_t get_offset_of_path_0() { return static_cast<int32_t>(offsetof(HttpCookie_t2077767070, ___path_0)); }
	inline String_t* get_path_0() const { return ___path_0; }
	inline String_t** get_address_of_path_0() { return &___path_0; }
	inline void set_path_0(String_t* value)
	{
		___path_0 = value;
		Il2CppCodeGenWriteBarrier(&___path_0, value);
	}

	inline static int32_t get_offset_of_domain_1() { return static_cast<int32_t>(offsetof(HttpCookie_t2077767070, ___domain_1)); }
	inline String_t* get_domain_1() const { return ___domain_1; }
	inline String_t** get_address_of_domain_1() { return &___domain_1; }
	inline void set_domain_1(String_t* value)
	{
		___domain_1 = value;
		Il2CppCodeGenWriteBarrier(&___domain_1, value);
	}

	inline static int32_t get_offset_of_expires_2() { return static_cast<int32_t>(offsetof(HttpCookie_t2077767070, ___expires_2)); }
	inline DateTime_t3738529785  get_expires_2() const { return ___expires_2; }
	inline DateTime_t3738529785 * get_address_of_expires_2() { return &___expires_2; }
	inline void set_expires_2(DateTime_t3738529785  value)
	{
		___expires_2 = value;
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(HttpCookie_t2077767070, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier(&___name_3, value);
	}

	inline static int32_t get_offset_of_flags_4() { return static_cast<int32_t>(offsetof(HttpCookie_t2077767070, ___flags_4)); }
	inline uint8_t get_flags_4() const { return ___flags_4; }
	inline uint8_t* get_address_of_flags_4() { return &___flags_4; }
	inline void set_flags_4(uint8_t value)
	{
		___flags_4 = value;
	}

	inline static int32_t get_offset_of_values_5() { return static_cast<int32_t>(offsetof(HttpCookie_t2077767070, ___values_5)); }
	inline NameValueCollection_t407452768 * get_values_5() const { return ___values_5; }
	inline NameValueCollection_t407452768 ** get_address_of_values_5() { return &___values_5; }
	inline void set_values_5(NameValueCollection_t407452768 * value)
	{
		___values_5 = value;
		Il2CppCodeGenWriteBarrier(&___values_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
