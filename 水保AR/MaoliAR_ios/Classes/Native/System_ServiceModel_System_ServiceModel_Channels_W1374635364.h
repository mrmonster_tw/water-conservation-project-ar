﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.Wss11SignatureConfirmation
struct  Wss11SignatureConfirmation_t1374635364  : public Il2CppObject
{
public:
	// System.String System.ServiceModel.Channels.Wss11SignatureConfirmation::id
	String_t* ___id_0;
	// System.String System.ServiceModel.Channels.Wss11SignatureConfirmation::value
	String_t* ___value_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(Wss11SignatureConfirmation_t1374635364, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier(&___id_0, value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(Wss11SignatureConfirmation_t1374635364, ___value_1)); }
	inline String_t* get_value_1() const { return ___value_1; }
	inline String_t** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(String_t* value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
