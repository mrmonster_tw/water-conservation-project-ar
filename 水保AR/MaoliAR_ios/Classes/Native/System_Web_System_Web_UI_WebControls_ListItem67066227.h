﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.Web.UI.StateBag
struct StateBag_t282928164;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.WebControls.ListItem
struct  ListItem_t67066227  : public Il2CppObject
{
public:
	// System.String System.Web.UI.WebControls.ListItem::text
	String_t* ___text_0;
	// System.String System.Web.UI.WebControls.ListItem::value
	String_t* ___value_1;
	// System.Boolean System.Web.UI.WebControls.ListItem::selected
	bool ___selected_2;
	// System.Boolean System.Web.UI.WebControls.ListItem::dirty
	bool ___dirty_3;
	// System.Boolean System.Web.UI.WebControls.ListItem::enabled
	bool ___enabled_4;
	// System.Boolean System.Web.UI.WebControls.ListItem::tracking
	bool ___tracking_5;
	// System.Web.UI.StateBag System.Web.UI.WebControls.ListItem::sb
	StateBag_t282928164 * ___sb_6;

public:
	inline static int32_t get_offset_of_text_0() { return static_cast<int32_t>(offsetof(ListItem_t67066227, ___text_0)); }
	inline String_t* get_text_0() const { return ___text_0; }
	inline String_t** get_address_of_text_0() { return &___text_0; }
	inline void set_text_0(String_t* value)
	{
		___text_0 = value;
		Il2CppCodeGenWriteBarrier(&___text_0, value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ListItem_t67066227, ___value_1)); }
	inline String_t* get_value_1() const { return ___value_1; }
	inline String_t** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(String_t* value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}

	inline static int32_t get_offset_of_selected_2() { return static_cast<int32_t>(offsetof(ListItem_t67066227, ___selected_2)); }
	inline bool get_selected_2() const { return ___selected_2; }
	inline bool* get_address_of_selected_2() { return &___selected_2; }
	inline void set_selected_2(bool value)
	{
		___selected_2 = value;
	}

	inline static int32_t get_offset_of_dirty_3() { return static_cast<int32_t>(offsetof(ListItem_t67066227, ___dirty_3)); }
	inline bool get_dirty_3() const { return ___dirty_3; }
	inline bool* get_address_of_dirty_3() { return &___dirty_3; }
	inline void set_dirty_3(bool value)
	{
		___dirty_3 = value;
	}

	inline static int32_t get_offset_of_enabled_4() { return static_cast<int32_t>(offsetof(ListItem_t67066227, ___enabled_4)); }
	inline bool get_enabled_4() const { return ___enabled_4; }
	inline bool* get_address_of_enabled_4() { return &___enabled_4; }
	inline void set_enabled_4(bool value)
	{
		___enabled_4 = value;
	}

	inline static int32_t get_offset_of_tracking_5() { return static_cast<int32_t>(offsetof(ListItem_t67066227, ___tracking_5)); }
	inline bool get_tracking_5() const { return ___tracking_5; }
	inline bool* get_address_of_tracking_5() { return &___tracking_5; }
	inline void set_tracking_5(bool value)
	{
		___tracking_5 = value;
	}

	inline static int32_t get_offset_of_sb_6() { return static_cast<int32_t>(offsetof(ListItem_t67066227, ___sb_6)); }
	inline StateBag_t282928164 * get_sb_6() const { return ___sb_6; }
	inline StateBag_t282928164 ** get_address_of_sb_6() { return &___sb_6; }
	inline void set_sb_6(StateBag_t282928164 * value)
	{
		___sb_6 = value;
		Il2CppCodeGenWriteBarrier(&___sb_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
