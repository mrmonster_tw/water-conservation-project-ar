﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_I1532512053.h"

// System.ServiceModel.Channels.MessageEncoder
struct MessageEncoder_t3063398011;
// System.Xml.XmlDictionaryReaderQuotas
struct XmlDictionaryReaderQuotas_t173030297;
// System.IO.Pipes.NamedPipeServerStream
struct NamedPipeServerStream_t125618231;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t1333520283;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.NamedPipeChannelListener`1<System.Object>
struct  NamedPipeChannelListener_1_t2232250219  : public InternalChannelListenerBase_1_t1532512053
{
public:
	// System.ServiceModel.Channels.MessageEncoder System.ServiceModel.Channels.NamedPipeChannelListener`1::encoder
	MessageEncoder_t3063398011 * ___encoder_16;
	// System.Xml.XmlDictionaryReaderQuotas System.ServiceModel.Channels.NamedPipeChannelListener`1::quotas
	XmlDictionaryReaderQuotas_t173030297 * ___quotas_17;
	// System.IO.Pipes.NamedPipeServerStream System.ServiceModel.Channels.NamedPipeChannelListener`1::active_server
	NamedPipeServerStream_t125618231 * ___active_server_18;
	// System.Threading.AutoResetEvent System.ServiceModel.Channels.NamedPipeChannelListener`1::server_release_handle
	AutoResetEvent_t1333520283 * ___server_release_handle_19;

public:
	inline static int32_t get_offset_of_encoder_16() { return static_cast<int32_t>(offsetof(NamedPipeChannelListener_1_t2232250219, ___encoder_16)); }
	inline MessageEncoder_t3063398011 * get_encoder_16() const { return ___encoder_16; }
	inline MessageEncoder_t3063398011 ** get_address_of_encoder_16() { return &___encoder_16; }
	inline void set_encoder_16(MessageEncoder_t3063398011 * value)
	{
		___encoder_16 = value;
		Il2CppCodeGenWriteBarrier(&___encoder_16, value);
	}

	inline static int32_t get_offset_of_quotas_17() { return static_cast<int32_t>(offsetof(NamedPipeChannelListener_1_t2232250219, ___quotas_17)); }
	inline XmlDictionaryReaderQuotas_t173030297 * get_quotas_17() const { return ___quotas_17; }
	inline XmlDictionaryReaderQuotas_t173030297 ** get_address_of_quotas_17() { return &___quotas_17; }
	inline void set_quotas_17(XmlDictionaryReaderQuotas_t173030297 * value)
	{
		___quotas_17 = value;
		Il2CppCodeGenWriteBarrier(&___quotas_17, value);
	}

	inline static int32_t get_offset_of_active_server_18() { return static_cast<int32_t>(offsetof(NamedPipeChannelListener_1_t2232250219, ___active_server_18)); }
	inline NamedPipeServerStream_t125618231 * get_active_server_18() const { return ___active_server_18; }
	inline NamedPipeServerStream_t125618231 ** get_address_of_active_server_18() { return &___active_server_18; }
	inline void set_active_server_18(NamedPipeServerStream_t125618231 * value)
	{
		___active_server_18 = value;
		Il2CppCodeGenWriteBarrier(&___active_server_18, value);
	}

	inline static int32_t get_offset_of_server_release_handle_19() { return static_cast<int32_t>(offsetof(NamedPipeChannelListener_1_t2232250219, ___server_release_handle_19)); }
	inline AutoResetEvent_t1333520283 * get_server_release_handle_19() const { return ___server_release_handle_19; }
	inline AutoResetEvent_t1333520283 ** get_address_of_server_release_handle_19() { return &___server_release_handle_19; }
	inline void set_server_release_handle_19(AutoResetEvent_t1333520283 * value)
	{
		___server_release_handle_19 = value;
		Il2CppCodeGenWriteBarrier(&___server_release_handle_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
