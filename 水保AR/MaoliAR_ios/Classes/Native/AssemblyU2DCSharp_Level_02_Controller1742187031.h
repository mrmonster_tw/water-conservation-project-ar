﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// Level_Controller
struct Level_Controller_t1433348427;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.GameObject
struct GameObject_t1113636619;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Level_02_Controller
struct  Level_02_Controller_t1742187031  : public MonoBehaviour_t3962482529
{
public:
	// Level_Controller Level_02_Controller::lvManager
	Level_Controller_t1433348427 * ___lvManager_2;
	// UnityEngine.GameObject[] Level_02_Controller::all_unShow_Objs
	GameObjectU5BU5D_t3328599146* ___all_unShow_Objs_3;
	// UnityEngine.GameObject[] Level_02_Controller::all_Objs
	GameObjectU5BU5D_t3328599146* ___all_Objs_4;
	// UnityEngine.GameObject[] Level_02_Controller::chapter_01
	GameObjectU5BU5D_t3328599146* ___chapter_01_5;
	// UnityEngine.GameObject Level_02_Controller::win_panel
	GameObject_t1113636619 * ___win_panel_6;
	// UnityEngine.GameObject Level_02_Controller::good_1
	GameObject_t1113636619 * ___good_1_7;
	// UnityEngine.GameObject Level_02_Controller::good_2
	GameObject_t1113636619 * ___good_2_8;
	// UnityEngine.GameObject Level_02_Controller::good_3
	GameObject_t1113636619 * ___good_3_9;
	// UnityEngine.GameObject Level_02_Controller::tip_1
	GameObject_t1113636619 * ___tip_1_10;
	// System.Boolean Level_02_Controller::do_once
	bool ___do_once_11;

public:
	inline static int32_t get_offset_of_lvManager_2() { return static_cast<int32_t>(offsetof(Level_02_Controller_t1742187031, ___lvManager_2)); }
	inline Level_Controller_t1433348427 * get_lvManager_2() const { return ___lvManager_2; }
	inline Level_Controller_t1433348427 ** get_address_of_lvManager_2() { return &___lvManager_2; }
	inline void set_lvManager_2(Level_Controller_t1433348427 * value)
	{
		___lvManager_2 = value;
		Il2CppCodeGenWriteBarrier(&___lvManager_2, value);
	}

	inline static int32_t get_offset_of_all_unShow_Objs_3() { return static_cast<int32_t>(offsetof(Level_02_Controller_t1742187031, ___all_unShow_Objs_3)); }
	inline GameObjectU5BU5D_t3328599146* get_all_unShow_Objs_3() const { return ___all_unShow_Objs_3; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_all_unShow_Objs_3() { return &___all_unShow_Objs_3; }
	inline void set_all_unShow_Objs_3(GameObjectU5BU5D_t3328599146* value)
	{
		___all_unShow_Objs_3 = value;
		Il2CppCodeGenWriteBarrier(&___all_unShow_Objs_3, value);
	}

	inline static int32_t get_offset_of_all_Objs_4() { return static_cast<int32_t>(offsetof(Level_02_Controller_t1742187031, ___all_Objs_4)); }
	inline GameObjectU5BU5D_t3328599146* get_all_Objs_4() const { return ___all_Objs_4; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_all_Objs_4() { return &___all_Objs_4; }
	inline void set_all_Objs_4(GameObjectU5BU5D_t3328599146* value)
	{
		___all_Objs_4 = value;
		Il2CppCodeGenWriteBarrier(&___all_Objs_4, value);
	}

	inline static int32_t get_offset_of_chapter_01_5() { return static_cast<int32_t>(offsetof(Level_02_Controller_t1742187031, ___chapter_01_5)); }
	inline GameObjectU5BU5D_t3328599146* get_chapter_01_5() const { return ___chapter_01_5; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_chapter_01_5() { return &___chapter_01_5; }
	inline void set_chapter_01_5(GameObjectU5BU5D_t3328599146* value)
	{
		___chapter_01_5 = value;
		Il2CppCodeGenWriteBarrier(&___chapter_01_5, value);
	}

	inline static int32_t get_offset_of_win_panel_6() { return static_cast<int32_t>(offsetof(Level_02_Controller_t1742187031, ___win_panel_6)); }
	inline GameObject_t1113636619 * get_win_panel_6() const { return ___win_panel_6; }
	inline GameObject_t1113636619 ** get_address_of_win_panel_6() { return &___win_panel_6; }
	inline void set_win_panel_6(GameObject_t1113636619 * value)
	{
		___win_panel_6 = value;
		Il2CppCodeGenWriteBarrier(&___win_panel_6, value);
	}

	inline static int32_t get_offset_of_good_1_7() { return static_cast<int32_t>(offsetof(Level_02_Controller_t1742187031, ___good_1_7)); }
	inline GameObject_t1113636619 * get_good_1_7() const { return ___good_1_7; }
	inline GameObject_t1113636619 ** get_address_of_good_1_7() { return &___good_1_7; }
	inline void set_good_1_7(GameObject_t1113636619 * value)
	{
		___good_1_7 = value;
		Il2CppCodeGenWriteBarrier(&___good_1_7, value);
	}

	inline static int32_t get_offset_of_good_2_8() { return static_cast<int32_t>(offsetof(Level_02_Controller_t1742187031, ___good_2_8)); }
	inline GameObject_t1113636619 * get_good_2_8() const { return ___good_2_8; }
	inline GameObject_t1113636619 ** get_address_of_good_2_8() { return &___good_2_8; }
	inline void set_good_2_8(GameObject_t1113636619 * value)
	{
		___good_2_8 = value;
		Il2CppCodeGenWriteBarrier(&___good_2_8, value);
	}

	inline static int32_t get_offset_of_good_3_9() { return static_cast<int32_t>(offsetof(Level_02_Controller_t1742187031, ___good_3_9)); }
	inline GameObject_t1113636619 * get_good_3_9() const { return ___good_3_9; }
	inline GameObject_t1113636619 ** get_address_of_good_3_9() { return &___good_3_9; }
	inline void set_good_3_9(GameObject_t1113636619 * value)
	{
		___good_3_9 = value;
		Il2CppCodeGenWriteBarrier(&___good_3_9, value);
	}

	inline static int32_t get_offset_of_tip_1_10() { return static_cast<int32_t>(offsetof(Level_02_Controller_t1742187031, ___tip_1_10)); }
	inline GameObject_t1113636619 * get_tip_1_10() const { return ___tip_1_10; }
	inline GameObject_t1113636619 ** get_address_of_tip_1_10() { return &___tip_1_10; }
	inline void set_tip_1_10(GameObject_t1113636619 * value)
	{
		___tip_1_10 = value;
		Il2CppCodeGenWriteBarrier(&___tip_1_10, value);
	}

	inline static int32_t get_offset_of_do_once_11() { return static_cast<int32_t>(offsetof(Level_02_Controller_t1742187031, ___do_once_11)); }
	inline bool get_do_once_11() const { return ___do_once_11; }
	inline bool* get_address_of_do_once_11() { return &___do_once_11; }
	inline void set_do_once_11(bool value)
	{
		___do_once_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
