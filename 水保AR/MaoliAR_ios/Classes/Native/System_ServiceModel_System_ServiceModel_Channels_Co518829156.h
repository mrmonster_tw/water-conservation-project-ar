﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_System_ServiceModel_Communicat1609160389.h"
#include "mscorlib_System_TimeSpan881159249.h"

// System.Object
struct Il2CppObject;
// System.EventHandler
struct EventHandler_t1348719766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.CommunicationObject
struct  CommunicationObject_t518829156  : public Il2CppObject
{
public:
	// System.Object System.ServiceModel.Channels.CommunicationObject::mutex
	Il2CppObject * ___mutex_0;
	// System.ServiceModel.CommunicationState System.ServiceModel.Channels.CommunicationObject::state
	int32_t ___state_1;
	// System.TimeSpan System.ServiceModel.Channels.CommunicationObject::default_open_timeout
	TimeSpan_t881159249  ___default_open_timeout_2;
	// System.TimeSpan System.ServiceModel.Channels.CommunicationObject::default_close_timeout
	TimeSpan_t881159249  ___default_close_timeout_3;
	// System.EventHandler System.ServiceModel.Channels.CommunicationObject::Closed
	EventHandler_t1348719766 * ___Closed_4;
	// System.EventHandler System.ServiceModel.Channels.CommunicationObject::Closing
	EventHandler_t1348719766 * ___Closing_5;
	// System.EventHandler System.ServiceModel.Channels.CommunicationObject::Faulted
	EventHandler_t1348719766 * ___Faulted_6;
	// System.EventHandler System.ServiceModel.Channels.CommunicationObject::Opened
	EventHandler_t1348719766 * ___Opened_7;
	// System.EventHandler System.ServiceModel.Channels.CommunicationObject::Opening
	EventHandler_t1348719766 * ___Opening_8;

public:
	inline static int32_t get_offset_of_mutex_0() { return static_cast<int32_t>(offsetof(CommunicationObject_t518829156, ___mutex_0)); }
	inline Il2CppObject * get_mutex_0() const { return ___mutex_0; }
	inline Il2CppObject ** get_address_of_mutex_0() { return &___mutex_0; }
	inline void set_mutex_0(Il2CppObject * value)
	{
		___mutex_0 = value;
		Il2CppCodeGenWriteBarrier(&___mutex_0, value);
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(CommunicationObject_t518829156, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_default_open_timeout_2() { return static_cast<int32_t>(offsetof(CommunicationObject_t518829156, ___default_open_timeout_2)); }
	inline TimeSpan_t881159249  get_default_open_timeout_2() const { return ___default_open_timeout_2; }
	inline TimeSpan_t881159249 * get_address_of_default_open_timeout_2() { return &___default_open_timeout_2; }
	inline void set_default_open_timeout_2(TimeSpan_t881159249  value)
	{
		___default_open_timeout_2 = value;
	}

	inline static int32_t get_offset_of_default_close_timeout_3() { return static_cast<int32_t>(offsetof(CommunicationObject_t518829156, ___default_close_timeout_3)); }
	inline TimeSpan_t881159249  get_default_close_timeout_3() const { return ___default_close_timeout_3; }
	inline TimeSpan_t881159249 * get_address_of_default_close_timeout_3() { return &___default_close_timeout_3; }
	inline void set_default_close_timeout_3(TimeSpan_t881159249  value)
	{
		___default_close_timeout_3 = value;
	}

	inline static int32_t get_offset_of_Closed_4() { return static_cast<int32_t>(offsetof(CommunicationObject_t518829156, ___Closed_4)); }
	inline EventHandler_t1348719766 * get_Closed_4() const { return ___Closed_4; }
	inline EventHandler_t1348719766 ** get_address_of_Closed_4() { return &___Closed_4; }
	inline void set_Closed_4(EventHandler_t1348719766 * value)
	{
		___Closed_4 = value;
		Il2CppCodeGenWriteBarrier(&___Closed_4, value);
	}

	inline static int32_t get_offset_of_Closing_5() { return static_cast<int32_t>(offsetof(CommunicationObject_t518829156, ___Closing_5)); }
	inline EventHandler_t1348719766 * get_Closing_5() const { return ___Closing_5; }
	inline EventHandler_t1348719766 ** get_address_of_Closing_5() { return &___Closing_5; }
	inline void set_Closing_5(EventHandler_t1348719766 * value)
	{
		___Closing_5 = value;
		Il2CppCodeGenWriteBarrier(&___Closing_5, value);
	}

	inline static int32_t get_offset_of_Faulted_6() { return static_cast<int32_t>(offsetof(CommunicationObject_t518829156, ___Faulted_6)); }
	inline EventHandler_t1348719766 * get_Faulted_6() const { return ___Faulted_6; }
	inline EventHandler_t1348719766 ** get_address_of_Faulted_6() { return &___Faulted_6; }
	inline void set_Faulted_6(EventHandler_t1348719766 * value)
	{
		___Faulted_6 = value;
		Il2CppCodeGenWriteBarrier(&___Faulted_6, value);
	}

	inline static int32_t get_offset_of_Opened_7() { return static_cast<int32_t>(offsetof(CommunicationObject_t518829156, ___Opened_7)); }
	inline EventHandler_t1348719766 * get_Opened_7() const { return ___Opened_7; }
	inline EventHandler_t1348719766 ** get_address_of_Opened_7() { return &___Opened_7; }
	inline void set_Opened_7(EventHandler_t1348719766 * value)
	{
		___Opened_7 = value;
		Il2CppCodeGenWriteBarrier(&___Opened_7, value);
	}

	inline static int32_t get_offset_of_Opening_8() { return static_cast<int32_t>(offsetof(CommunicationObject_t518829156, ___Opening_8)); }
	inline EventHandler_t1348719766 * get_Opening_8() const { return ___Opening_8; }
	inline EventHandler_t1348719766 ** get_address_of_Opening_8() { return &___Opening_8; }
	inline void set_Opening_8(EventHandler_t1348719766 * value)
	{
		___Opening_8 = value;
		Il2CppCodeGenWriteBarrier(&___Opening_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
