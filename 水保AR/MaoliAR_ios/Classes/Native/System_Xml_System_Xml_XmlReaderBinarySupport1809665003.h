﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport_Comma1020432923.h"

// System.Xml.XmlReader
struct XmlReader_t3121518892;
// System.Xml.XmlReaderBinarySupport/CharGetter
struct CharGetter_t1703763694;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Text.StringBuilder
struct StringBuilder_t1712802186;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReaderBinarySupport
struct  XmlReaderBinarySupport_t1809665003  : public Il2CppObject
{
public:
	// System.Xml.XmlReader System.Xml.XmlReaderBinarySupport::reader
	XmlReader_t3121518892 * ___reader_0;
	// System.Xml.XmlReaderBinarySupport/CharGetter System.Xml.XmlReaderBinarySupport::getter
	CharGetter_t1703763694 * ___getter_1;
	// System.Byte[] System.Xml.XmlReaderBinarySupport::base64Cache
	ByteU5BU5D_t4116647657* ___base64Cache_2;
	// System.Int32 System.Xml.XmlReaderBinarySupport::base64CacheStartsAt
	int32_t ___base64CacheStartsAt_3;
	// System.Xml.XmlReaderBinarySupport/CommandState System.Xml.XmlReaderBinarySupport::state
	int32_t ___state_4;
	// System.Text.StringBuilder System.Xml.XmlReaderBinarySupport::textCache
	StringBuilder_t1712802186 * ___textCache_5;
	// System.Boolean System.Xml.XmlReaderBinarySupport::hasCache
	bool ___hasCache_6;
	// System.Boolean System.Xml.XmlReaderBinarySupport::dontReset
	bool ___dontReset_7;

public:
	inline static int32_t get_offset_of_reader_0() { return static_cast<int32_t>(offsetof(XmlReaderBinarySupport_t1809665003, ___reader_0)); }
	inline XmlReader_t3121518892 * get_reader_0() const { return ___reader_0; }
	inline XmlReader_t3121518892 ** get_address_of_reader_0() { return &___reader_0; }
	inline void set_reader_0(XmlReader_t3121518892 * value)
	{
		___reader_0 = value;
		Il2CppCodeGenWriteBarrier(&___reader_0, value);
	}

	inline static int32_t get_offset_of_getter_1() { return static_cast<int32_t>(offsetof(XmlReaderBinarySupport_t1809665003, ___getter_1)); }
	inline CharGetter_t1703763694 * get_getter_1() const { return ___getter_1; }
	inline CharGetter_t1703763694 ** get_address_of_getter_1() { return &___getter_1; }
	inline void set_getter_1(CharGetter_t1703763694 * value)
	{
		___getter_1 = value;
		Il2CppCodeGenWriteBarrier(&___getter_1, value);
	}

	inline static int32_t get_offset_of_base64Cache_2() { return static_cast<int32_t>(offsetof(XmlReaderBinarySupport_t1809665003, ___base64Cache_2)); }
	inline ByteU5BU5D_t4116647657* get_base64Cache_2() const { return ___base64Cache_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_base64Cache_2() { return &___base64Cache_2; }
	inline void set_base64Cache_2(ByteU5BU5D_t4116647657* value)
	{
		___base64Cache_2 = value;
		Il2CppCodeGenWriteBarrier(&___base64Cache_2, value);
	}

	inline static int32_t get_offset_of_base64CacheStartsAt_3() { return static_cast<int32_t>(offsetof(XmlReaderBinarySupport_t1809665003, ___base64CacheStartsAt_3)); }
	inline int32_t get_base64CacheStartsAt_3() const { return ___base64CacheStartsAt_3; }
	inline int32_t* get_address_of_base64CacheStartsAt_3() { return &___base64CacheStartsAt_3; }
	inline void set_base64CacheStartsAt_3(int32_t value)
	{
		___base64CacheStartsAt_3 = value;
	}

	inline static int32_t get_offset_of_state_4() { return static_cast<int32_t>(offsetof(XmlReaderBinarySupport_t1809665003, ___state_4)); }
	inline int32_t get_state_4() const { return ___state_4; }
	inline int32_t* get_address_of_state_4() { return &___state_4; }
	inline void set_state_4(int32_t value)
	{
		___state_4 = value;
	}

	inline static int32_t get_offset_of_textCache_5() { return static_cast<int32_t>(offsetof(XmlReaderBinarySupport_t1809665003, ___textCache_5)); }
	inline StringBuilder_t1712802186 * get_textCache_5() const { return ___textCache_5; }
	inline StringBuilder_t1712802186 ** get_address_of_textCache_5() { return &___textCache_5; }
	inline void set_textCache_5(StringBuilder_t1712802186 * value)
	{
		___textCache_5 = value;
		Il2CppCodeGenWriteBarrier(&___textCache_5, value);
	}

	inline static int32_t get_offset_of_hasCache_6() { return static_cast<int32_t>(offsetof(XmlReaderBinarySupport_t1809665003, ___hasCache_6)); }
	inline bool get_hasCache_6() const { return ___hasCache_6; }
	inline bool* get_address_of_hasCache_6() { return &___hasCache_6; }
	inline void set_hasCache_6(bool value)
	{
		___hasCache_6 = value;
	}

	inline static int32_t get_offset_of_dontReset_7() { return static_cast<int32_t>(offsetof(XmlReaderBinarySupport_t1809665003, ___dontReset_7)); }
	inline bool get_dontReset_7() const { return ___dontReset_7; }
	inline bool* get_address_of_dontReset_7() { return &___dontReset_7; }
	inline void set_dontReset_7(bool value)
	{
		___dontReset_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
