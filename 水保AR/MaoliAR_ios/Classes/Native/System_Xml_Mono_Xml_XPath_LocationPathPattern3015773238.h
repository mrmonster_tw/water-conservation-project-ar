﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_XPath_Pattern1136864796.h"

// Mono.Xml.XPath.LocationPathPattern
struct LocationPathPattern_t3015773238;
// System.Xml.XPath.NodeTest
struct NodeTest_t747859056;
// System.Xml.XPath.ExprFilter
struct ExprFilter_t2551926938;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.XPath.LocationPathPattern
struct  LocationPathPattern_t3015773238  : public Pattern_t1136864796
{
public:
	// Mono.Xml.XPath.LocationPathPattern Mono.Xml.XPath.LocationPathPattern::patternPrevious
	LocationPathPattern_t3015773238 * ___patternPrevious_0;
	// System.Boolean Mono.Xml.XPath.LocationPathPattern::isAncestor
	bool ___isAncestor_1;
	// System.Xml.XPath.NodeTest Mono.Xml.XPath.LocationPathPattern::nodeTest
	NodeTest_t747859056 * ___nodeTest_2;
	// System.Xml.XPath.ExprFilter Mono.Xml.XPath.LocationPathPattern::filter
	ExprFilter_t2551926938 * ___filter_3;

public:
	inline static int32_t get_offset_of_patternPrevious_0() { return static_cast<int32_t>(offsetof(LocationPathPattern_t3015773238, ___patternPrevious_0)); }
	inline LocationPathPattern_t3015773238 * get_patternPrevious_0() const { return ___patternPrevious_0; }
	inline LocationPathPattern_t3015773238 ** get_address_of_patternPrevious_0() { return &___patternPrevious_0; }
	inline void set_patternPrevious_0(LocationPathPattern_t3015773238 * value)
	{
		___patternPrevious_0 = value;
		Il2CppCodeGenWriteBarrier(&___patternPrevious_0, value);
	}

	inline static int32_t get_offset_of_isAncestor_1() { return static_cast<int32_t>(offsetof(LocationPathPattern_t3015773238, ___isAncestor_1)); }
	inline bool get_isAncestor_1() const { return ___isAncestor_1; }
	inline bool* get_address_of_isAncestor_1() { return &___isAncestor_1; }
	inline void set_isAncestor_1(bool value)
	{
		___isAncestor_1 = value;
	}

	inline static int32_t get_offset_of_nodeTest_2() { return static_cast<int32_t>(offsetof(LocationPathPattern_t3015773238, ___nodeTest_2)); }
	inline NodeTest_t747859056 * get_nodeTest_2() const { return ___nodeTest_2; }
	inline NodeTest_t747859056 ** get_address_of_nodeTest_2() { return &___nodeTest_2; }
	inline void set_nodeTest_2(NodeTest_t747859056 * value)
	{
		___nodeTest_2 = value;
		Il2CppCodeGenWriteBarrier(&___nodeTest_2, value);
	}

	inline static int32_t get_offset_of_filter_3() { return static_cast<int32_t>(offsetof(LocationPathPattern_t3015773238, ___filter_3)); }
	inline ExprFilter_t2551926938 * get_filter_3() const { return ___filter_3; }
	inline ExprFilter_t2551926938 ** get_address_of_filter_3() { return &___filter_3; }
	inline void set_filter_3(ExprFilter_t2551926938 * value)
	{
		___filter_3 = value;
		Il2CppCodeGenWriteBarrier(&___filter_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
