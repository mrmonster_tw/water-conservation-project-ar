﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_Mono_CodeGeneration_CodeValueRe975266093.h"

// Mono.CodeGeneration.CodeExpression
struct CodeExpression_t2163794278;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.CodeGeneration.CodeArrayItem
struct  CodeArrayItem_t2712318199  : public CodeValueReference_t975266093
{
public:
	// Mono.CodeGeneration.CodeExpression Mono.CodeGeneration.CodeArrayItem::array
	CodeExpression_t2163794278 * ___array_0;
	// Mono.CodeGeneration.CodeExpression Mono.CodeGeneration.CodeArrayItem::index
	CodeExpression_t2163794278 * ___index_1;

public:
	inline static int32_t get_offset_of_array_0() { return static_cast<int32_t>(offsetof(CodeArrayItem_t2712318199, ___array_0)); }
	inline CodeExpression_t2163794278 * get_array_0() const { return ___array_0; }
	inline CodeExpression_t2163794278 ** get_address_of_array_0() { return &___array_0; }
	inline void set_array_0(CodeExpression_t2163794278 * value)
	{
		___array_0 = value;
		Il2CppCodeGenWriteBarrier(&___array_0, value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(CodeArrayItem_t2712318199, ___index_1)); }
	inline CodeExpression_t2163794278 * get_index_1() const { return ___index_1; }
	inline CodeExpression_t2163794278 ** get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(CodeExpression_t2163794278 * value)
	{
		___index_1 = value;
		Il2CppCodeGenWriteBarrier(&___index_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
