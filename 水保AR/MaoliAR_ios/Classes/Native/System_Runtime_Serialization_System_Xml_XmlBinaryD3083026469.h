﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlBinaryDictionaryWriter/<AddNamespaceChecked>c__AnonStorey4
struct  U3CAddNamespaceCheckedU3Ec__AnonStorey4_t3083026469  : public Il2CppObject
{
public:
	// System.String System.Xml.XmlBinaryDictionaryWriter/<AddNamespaceChecked>c__AnonStorey4::prefix
	String_t* ___prefix_0;

public:
	inline static int32_t get_offset_of_prefix_0() { return static_cast<int32_t>(offsetof(U3CAddNamespaceCheckedU3Ec__AnonStorey4_t3083026469, ___prefix_0)); }
	inline String_t* get_prefix_0() const { return ___prefix_0; }
	inline String_t** get_address_of_prefix_0() { return &___prefix_0; }
	inline void set_prefix_0(String_t* value)
	{
		___prefix_0 = value;
		Il2CppCodeGenWriteBarrier(&___prefix_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
