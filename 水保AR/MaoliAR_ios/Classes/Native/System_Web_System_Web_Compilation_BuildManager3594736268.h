﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_StringComparison3657712135.h"

// System.Object
struct Il2CppObject;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.ComponentModel.EventHandlerList
struct EventHandlerList_t1108123056;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3954782707;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t4177511560;
// System.Collections.Generic.List`1<System.Reflection.Assembly>
struct List_1_t1279540245;
// System.Collections.Generic.Dictionary`2<System.Type,System.CodeDom.Compiler.CodeDomProvider>
struct Dictionary_2_t2554696900;
// System.Collections.Generic.Dictionary`2<System.String,System.Web.Compilation.BuildManagerCacheItem>
struct Dictionary_2_t1429072202;
// System.Collections.Generic.Dictionary`2<System.String,System.Web.Compilation.BuildManager/PreCompilationData>
struct Dictionary_2_t3005514508;
// System.Threading.ReaderWriterLockSlim
struct ReaderWriterLockSlim_t1938317233;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.BuildManager
struct  BuildManager_t3594736268  : public Il2CppObject
{
public:

public:
};

struct BuildManager_t3594736268_StaticFields
{
public:
	// System.Int32 System.Web.Compilation.BuildManager::BUILD_MANAGER_VIRTUAL_PATH_CACHE_PREFIX_LENGTH
	int32_t ___BUILD_MANAGER_VIRTUAL_PATH_CACHE_PREFIX_LENGTH_0;
	// System.Object System.Web.Compilation.BuildManager::bigCompilationLock
	Il2CppObject * ___bigCompilationLock_1;
	// System.Char[] System.Web.Compilation.BuildManager::virtualPathsToIgnoreSplitChars
	CharU5BU5D_t3528271667* ___virtualPathsToIgnoreSplitChars_2;
	// System.ComponentModel.EventHandlerList System.Web.Compilation.BuildManager::events
	EventHandlerList_t1108123056 * ___events_3;
	// System.Object System.Web.Compilation.BuildManager::buildManagerRemoveEntryEvent
	Il2CppObject * ___buildManagerRemoveEntryEvent_4;
	// System.Boolean System.Web.Compilation.BuildManager::hosted
	bool ___hosted_5;
	// System.Collections.Generic.IEqualityComparer`1<System.String> System.Web.Compilation.BuildManager::comparer
	Il2CppObject* ___comparer_6;
	// System.StringComparison System.Web.Compilation.BuildManager::stringComparer
	int32_t ___stringComparer_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Boolean> System.Web.Compilation.BuildManager::virtualPathsToIgnore
	Dictionary_2_t4177511560 * ___virtualPathsToIgnore_8;
	// System.Boolean System.Web.Compilation.BuildManager::haveVirtualPathsToIgnore
	bool ___haveVirtualPathsToIgnore_9;
	// System.Collections.Generic.List`1<System.Reflection.Assembly> System.Web.Compilation.BuildManager::AppCode_Assemblies
	List_1_t1279540245 * ___AppCode_Assemblies_10;
	// System.Collections.Generic.List`1<System.Reflection.Assembly> System.Web.Compilation.BuildManager::TopLevel_Assemblies
	List_1_t1279540245 * ___TopLevel_Assemblies_11;
	// System.Collections.Generic.Dictionary`2<System.Type,System.CodeDom.Compiler.CodeDomProvider> System.Web.Compilation.BuildManager::codeDomProviders
	Dictionary_2_t2554696900 * ___codeDomProviders_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Web.Compilation.BuildManagerCacheItem> System.Web.Compilation.BuildManager::buildCache
	Dictionary_2_t1429072202 * ___buildCache_13;
	// System.Collections.Generic.List`1<System.Reflection.Assembly> System.Web.Compilation.BuildManager::referencedAssemblies
	List_1_t1279540245 * ___referencedAssemblies_14;
	// System.Int32 System.Web.Compilation.BuildManager::buildCount
	int32_t ___buildCount_15;
	// System.Boolean System.Web.Compilation.BuildManager::is_precompiled
	bool ___is_precompiled_16;
	// System.Collections.Generic.Dictionary`2<System.String,System.Web.Compilation.BuildManager/PreCompilationData> System.Web.Compilation.BuildManager::precompiled
	Dictionary_2_t3005514508 * ___precompiled_17;
	// System.Boolean System.Web.Compilation.BuildManager::suppressDebugModeMessages
	bool ___suppressDebugModeMessages_18;
	// System.Threading.ReaderWriterLockSlim System.Web.Compilation.BuildManager::buildCacheLock
	ReaderWriterLockSlim_t1938317233 * ___buildCacheLock_19;
	// System.UInt64 System.Web.Compilation.BuildManager::recursionDepth
	uint64_t ___recursionDepth_20;
	// System.Boolean System.Web.Compilation.BuildManager::<HaveResources>k__BackingField
	bool ___U3CHaveResourcesU3Ek__BackingField_21;

public:
	inline static int32_t get_offset_of_BUILD_MANAGER_VIRTUAL_PATH_CACHE_PREFIX_LENGTH_0() { return static_cast<int32_t>(offsetof(BuildManager_t3594736268_StaticFields, ___BUILD_MANAGER_VIRTUAL_PATH_CACHE_PREFIX_LENGTH_0)); }
	inline int32_t get_BUILD_MANAGER_VIRTUAL_PATH_CACHE_PREFIX_LENGTH_0() const { return ___BUILD_MANAGER_VIRTUAL_PATH_CACHE_PREFIX_LENGTH_0; }
	inline int32_t* get_address_of_BUILD_MANAGER_VIRTUAL_PATH_CACHE_PREFIX_LENGTH_0() { return &___BUILD_MANAGER_VIRTUAL_PATH_CACHE_PREFIX_LENGTH_0; }
	inline void set_BUILD_MANAGER_VIRTUAL_PATH_CACHE_PREFIX_LENGTH_0(int32_t value)
	{
		___BUILD_MANAGER_VIRTUAL_PATH_CACHE_PREFIX_LENGTH_0 = value;
	}

	inline static int32_t get_offset_of_bigCompilationLock_1() { return static_cast<int32_t>(offsetof(BuildManager_t3594736268_StaticFields, ___bigCompilationLock_1)); }
	inline Il2CppObject * get_bigCompilationLock_1() const { return ___bigCompilationLock_1; }
	inline Il2CppObject ** get_address_of_bigCompilationLock_1() { return &___bigCompilationLock_1; }
	inline void set_bigCompilationLock_1(Il2CppObject * value)
	{
		___bigCompilationLock_1 = value;
		Il2CppCodeGenWriteBarrier(&___bigCompilationLock_1, value);
	}

	inline static int32_t get_offset_of_virtualPathsToIgnoreSplitChars_2() { return static_cast<int32_t>(offsetof(BuildManager_t3594736268_StaticFields, ___virtualPathsToIgnoreSplitChars_2)); }
	inline CharU5BU5D_t3528271667* get_virtualPathsToIgnoreSplitChars_2() const { return ___virtualPathsToIgnoreSplitChars_2; }
	inline CharU5BU5D_t3528271667** get_address_of_virtualPathsToIgnoreSplitChars_2() { return &___virtualPathsToIgnoreSplitChars_2; }
	inline void set_virtualPathsToIgnoreSplitChars_2(CharU5BU5D_t3528271667* value)
	{
		___virtualPathsToIgnoreSplitChars_2 = value;
		Il2CppCodeGenWriteBarrier(&___virtualPathsToIgnoreSplitChars_2, value);
	}

	inline static int32_t get_offset_of_events_3() { return static_cast<int32_t>(offsetof(BuildManager_t3594736268_StaticFields, ___events_3)); }
	inline EventHandlerList_t1108123056 * get_events_3() const { return ___events_3; }
	inline EventHandlerList_t1108123056 ** get_address_of_events_3() { return &___events_3; }
	inline void set_events_3(EventHandlerList_t1108123056 * value)
	{
		___events_3 = value;
		Il2CppCodeGenWriteBarrier(&___events_3, value);
	}

	inline static int32_t get_offset_of_buildManagerRemoveEntryEvent_4() { return static_cast<int32_t>(offsetof(BuildManager_t3594736268_StaticFields, ___buildManagerRemoveEntryEvent_4)); }
	inline Il2CppObject * get_buildManagerRemoveEntryEvent_4() const { return ___buildManagerRemoveEntryEvent_4; }
	inline Il2CppObject ** get_address_of_buildManagerRemoveEntryEvent_4() { return &___buildManagerRemoveEntryEvent_4; }
	inline void set_buildManagerRemoveEntryEvent_4(Il2CppObject * value)
	{
		___buildManagerRemoveEntryEvent_4 = value;
		Il2CppCodeGenWriteBarrier(&___buildManagerRemoveEntryEvent_4, value);
	}

	inline static int32_t get_offset_of_hosted_5() { return static_cast<int32_t>(offsetof(BuildManager_t3594736268_StaticFields, ___hosted_5)); }
	inline bool get_hosted_5() const { return ___hosted_5; }
	inline bool* get_address_of_hosted_5() { return &___hosted_5; }
	inline void set_hosted_5(bool value)
	{
		___hosted_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(BuildManager_t3594736268_StaticFields, ___comparer_6)); }
	inline Il2CppObject* get_comparer_6() const { return ___comparer_6; }
	inline Il2CppObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(Il2CppObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier(&___comparer_6, value);
	}

	inline static int32_t get_offset_of_stringComparer_7() { return static_cast<int32_t>(offsetof(BuildManager_t3594736268_StaticFields, ___stringComparer_7)); }
	inline int32_t get_stringComparer_7() const { return ___stringComparer_7; }
	inline int32_t* get_address_of_stringComparer_7() { return &___stringComparer_7; }
	inline void set_stringComparer_7(int32_t value)
	{
		___stringComparer_7 = value;
	}

	inline static int32_t get_offset_of_virtualPathsToIgnore_8() { return static_cast<int32_t>(offsetof(BuildManager_t3594736268_StaticFields, ___virtualPathsToIgnore_8)); }
	inline Dictionary_2_t4177511560 * get_virtualPathsToIgnore_8() const { return ___virtualPathsToIgnore_8; }
	inline Dictionary_2_t4177511560 ** get_address_of_virtualPathsToIgnore_8() { return &___virtualPathsToIgnore_8; }
	inline void set_virtualPathsToIgnore_8(Dictionary_2_t4177511560 * value)
	{
		___virtualPathsToIgnore_8 = value;
		Il2CppCodeGenWriteBarrier(&___virtualPathsToIgnore_8, value);
	}

	inline static int32_t get_offset_of_haveVirtualPathsToIgnore_9() { return static_cast<int32_t>(offsetof(BuildManager_t3594736268_StaticFields, ___haveVirtualPathsToIgnore_9)); }
	inline bool get_haveVirtualPathsToIgnore_9() const { return ___haveVirtualPathsToIgnore_9; }
	inline bool* get_address_of_haveVirtualPathsToIgnore_9() { return &___haveVirtualPathsToIgnore_9; }
	inline void set_haveVirtualPathsToIgnore_9(bool value)
	{
		___haveVirtualPathsToIgnore_9 = value;
	}

	inline static int32_t get_offset_of_AppCode_Assemblies_10() { return static_cast<int32_t>(offsetof(BuildManager_t3594736268_StaticFields, ___AppCode_Assemblies_10)); }
	inline List_1_t1279540245 * get_AppCode_Assemblies_10() const { return ___AppCode_Assemblies_10; }
	inline List_1_t1279540245 ** get_address_of_AppCode_Assemblies_10() { return &___AppCode_Assemblies_10; }
	inline void set_AppCode_Assemblies_10(List_1_t1279540245 * value)
	{
		___AppCode_Assemblies_10 = value;
		Il2CppCodeGenWriteBarrier(&___AppCode_Assemblies_10, value);
	}

	inline static int32_t get_offset_of_TopLevel_Assemblies_11() { return static_cast<int32_t>(offsetof(BuildManager_t3594736268_StaticFields, ___TopLevel_Assemblies_11)); }
	inline List_1_t1279540245 * get_TopLevel_Assemblies_11() const { return ___TopLevel_Assemblies_11; }
	inline List_1_t1279540245 ** get_address_of_TopLevel_Assemblies_11() { return &___TopLevel_Assemblies_11; }
	inline void set_TopLevel_Assemblies_11(List_1_t1279540245 * value)
	{
		___TopLevel_Assemblies_11 = value;
		Il2CppCodeGenWriteBarrier(&___TopLevel_Assemblies_11, value);
	}

	inline static int32_t get_offset_of_codeDomProviders_12() { return static_cast<int32_t>(offsetof(BuildManager_t3594736268_StaticFields, ___codeDomProviders_12)); }
	inline Dictionary_2_t2554696900 * get_codeDomProviders_12() const { return ___codeDomProviders_12; }
	inline Dictionary_2_t2554696900 ** get_address_of_codeDomProviders_12() { return &___codeDomProviders_12; }
	inline void set_codeDomProviders_12(Dictionary_2_t2554696900 * value)
	{
		___codeDomProviders_12 = value;
		Il2CppCodeGenWriteBarrier(&___codeDomProviders_12, value);
	}

	inline static int32_t get_offset_of_buildCache_13() { return static_cast<int32_t>(offsetof(BuildManager_t3594736268_StaticFields, ___buildCache_13)); }
	inline Dictionary_2_t1429072202 * get_buildCache_13() const { return ___buildCache_13; }
	inline Dictionary_2_t1429072202 ** get_address_of_buildCache_13() { return &___buildCache_13; }
	inline void set_buildCache_13(Dictionary_2_t1429072202 * value)
	{
		___buildCache_13 = value;
		Il2CppCodeGenWriteBarrier(&___buildCache_13, value);
	}

	inline static int32_t get_offset_of_referencedAssemblies_14() { return static_cast<int32_t>(offsetof(BuildManager_t3594736268_StaticFields, ___referencedAssemblies_14)); }
	inline List_1_t1279540245 * get_referencedAssemblies_14() const { return ___referencedAssemblies_14; }
	inline List_1_t1279540245 ** get_address_of_referencedAssemblies_14() { return &___referencedAssemblies_14; }
	inline void set_referencedAssemblies_14(List_1_t1279540245 * value)
	{
		___referencedAssemblies_14 = value;
		Il2CppCodeGenWriteBarrier(&___referencedAssemblies_14, value);
	}

	inline static int32_t get_offset_of_buildCount_15() { return static_cast<int32_t>(offsetof(BuildManager_t3594736268_StaticFields, ___buildCount_15)); }
	inline int32_t get_buildCount_15() const { return ___buildCount_15; }
	inline int32_t* get_address_of_buildCount_15() { return &___buildCount_15; }
	inline void set_buildCount_15(int32_t value)
	{
		___buildCount_15 = value;
	}

	inline static int32_t get_offset_of_is_precompiled_16() { return static_cast<int32_t>(offsetof(BuildManager_t3594736268_StaticFields, ___is_precompiled_16)); }
	inline bool get_is_precompiled_16() const { return ___is_precompiled_16; }
	inline bool* get_address_of_is_precompiled_16() { return &___is_precompiled_16; }
	inline void set_is_precompiled_16(bool value)
	{
		___is_precompiled_16 = value;
	}

	inline static int32_t get_offset_of_precompiled_17() { return static_cast<int32_t>(offsetof(BuildManager_t3594736268_StaticFields, ___precompiled_17)); }
	inline Dictionary_2_t3005514508 * get_precompiled_17() const { return ___precompiled_17; }
	inline Dictionary_2_t3005514508 ** get_address_of_precompiled_17() { return &___precompiled_17; }
	inline void set_precompiled_17(Dictionary_2_t3005514508 * value)
	{
		___precompiled_17 = value;
		Il2CppCodeGenWriteBarrier(&___precompiled_17, value);
	}

	inline static int32_t get_offset_of_suppressDebugModeMessages_18() { return static_cast<int32_t>(offsetof(BuildManager_t3594736268_StaticFields, ___suppressDebugModeMessages_18)); }
	inline bool get_suppressDebugModeMessages_18() const { return ___suppressDebugModeMessages_18; }
	inline bool* get_address_of_suppressDebugModeMessages_18() { return &___suppressDebugModeMessages_18; }
	inline void set_suppressDebugModeMessages_18(bool value)
	{
		___suppressDebugModeMessages_18 = value;
	}

	inline static int32_t get_offset_of_buildCacheLock_19() { return static_cast<int32_t>(offsetof(BuildManager_t3594736268_StaticFields, ___buildCacheLock_19)); }
	inline ReaderWriterLockSlim_t1938317233 * get_buildCacheLock_19() const { return ___buildCacheLock_19; }
	inline ReaderWriterLockSlim_t1938317233 ** get_address_of_buildCacheLock_19() { return &___buildCacheLock_19; }
	inline void set_buildCacheLock_19(ReaderWriterLockSlim_t1938317233 * value)
	{
		___buildCacheLock_19 = value;
		Il2CppCodeGenWriteBarrier(&___buildCacheLock_19, value);
	}

	inline static int32_t get_offset_of_recursionDepth_20() { return static_cast<int32_t>(offsetof(BuildManager_t3594736268_StaticFields, ___recursionDepth_20)); }
	inline uint64_t get_recursionDepth_20() const { return ___recursionDepth_20; }
	inline uint64_t* get_address_of_recursionDepth_20() { return &___recursionDepth_20; }
	inline void set_recursionDepth_20(uint64_t value)
	{
		___recursionDepth_20 = value;
	}

	inline static int32_t get_offset_of_U3CHaveResourcesU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(BuildManager_t3594736268_StaticFields, ___U3CHaveResourcesU3Ek__BackingField_21)); }
	inline bool get_U3CHaveResourcesU3Ek__BackingField_21() const { return ___U3CHaveResourcesU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CHaveResourcesU3Ek__BackingField_21() { return &___U3CHaveResourcesU3Ek__BackingField_21; }
	inline void set_U3CHaveResourcesU3Ek__BackingField_21(bool value)
	{
		___U3CHaveResourcesU3Ek__BackingField_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
