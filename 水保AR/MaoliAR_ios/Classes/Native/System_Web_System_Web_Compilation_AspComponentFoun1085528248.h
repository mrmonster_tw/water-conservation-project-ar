﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.AspComponentFoundry/Foundry
struct  Foundry_t1085528248  : public Il2CppObject
{
public:
	// System.Boolean System.Web.Compilation.AspComponentFoundry/Foundry::_fromConfig
	bool ____fromConfig_0;

public:
	inline static int32_t get_offset_of__fromConfig_0() { return static_cast<int32_t>(offsetof(Foundry_t1085528248, ____fromConfig_0)); }
	inline bool get__fromConfig_0() const { return ____fromConfig_0; }
	inline bool* get_address_of__fromConfig_0() { return &____fromConfig_0; }
	inline void set__fromConfig_0(bool value)
	{
		____fromConfig_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
