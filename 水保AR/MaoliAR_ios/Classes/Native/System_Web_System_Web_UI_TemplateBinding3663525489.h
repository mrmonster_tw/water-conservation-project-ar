﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Type
struct Type_t;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.TemplateBinding
struct  TemplateBinding_t3663525489  : public Il2CppObject
{
public:
	// System.Type System.Web.UI.TemplateBinding::ControlType
	Type_t * ___ControlType_0;
	// System.String System.Web.UI.TemplateBinding::ControlProperty
	String_t* ___ControlProperty_1;
	// System.String System.Web.UI.TemplateBinding::ControlId
	String_t* ___ControlId_2;
	// System.String System.Web.UI.TemplateBinding::FieldName
	String_t* ___FieldName_3;

public:
	inline static int32_t get_offset_of_ControlType_0() { return static_cast<int32_t>(offsetof(TemplateBinding_t3663525489, ___ControlType_0)); }
	inline Type_t * get_ControlType_0() const { return ___ControlType_0; }
	inline Type_t ** get_address_of_ControlType_0() { return &___ControlType_0; }
	inline void set_ControlType_0(Type_t * value)
	{
		___ControlType_0 = value;
		Il2CppCodeGenWriteBarrier(&___ControlType_0, value);
	}

	inline static int32_t get_offset_of_ControlProperty_1() { return static_cast<int32_t>(offsetof(TemplateBinding_t3663525489, ___ControlProperty_1)); }
	inline String_t* get_ControlProperty_1() const { return ___ControlProperty_1; }
	inline String_t** get_address_of_ControlProperty_1() { return &___ControlProperty_1; }
	inline void set_ControlProperty_1(String_t* value)
	{
		___ControlProperty_1 = value;
		Il2CppCodeGenWriteBarrier(&___ControlProperty_1, value);
	}

	inline static int32_t get_offset_of_ControlId_2() { return static_cast<int32_t>(offsetof(TemplateBinding_t3663525489, ___ControlId_2)); }
	inline String_t* get_ControlId_2() const { return ___ControlId_2; }
	inline String_t** get_address_of_ControlId_2() { return &___ControlId_2; }
	inline void set_ControlId_2(String_t* value)
	{
		___ControlId_2 = value;
		Il2CppCodeGenWriteBarrier(&___ControlId_2, value);
	}

	inline static int32_t get_offset_of_FieldName_3() { return static_cast<int32_t>(offsetof(TemplateBinding_t3663525489, ___FieldName_3)); }
	inline String_t* get_FieldName_3() const { return ___FieldName_3; }
	inline String_t** get_address_of_FieldName_3() { return &___FieldName_3; }
	inline void set_FieldName_3(String_t* value)
	{
		___FieldName_3 = value;
		Il2CppCodeGenWriteBarrier(&___FieldName_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
