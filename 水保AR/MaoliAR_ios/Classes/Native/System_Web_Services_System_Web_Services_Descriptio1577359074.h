﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_Services_System_Web_Services_Descriptio3443230295.h"

// System.Web.Services.Description.ServiceDescriptionImporter
struct ServiceDescriptionImporter_t2272364891;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.ServiceDescriptionCollection
struct  ServiceDescriptionCollection_t1577359074  : public ServiceDescriptionBaseCollection_t3443230295
{
public:
	// System.Web.Services.Description.ServiceDescriptionImporter System.Web.Services.Description.ServiceDescriptionCollection::importer
	ServiceDescriptionImporter_t2272364891 * ___importer_3;

public:
	inline static int32_t get_offset_of_importer_3() { return static_cast<int32_t>(offsetof(ServiceDescriptionCollection_t1577359074, ___importer_3)); }
	inline ServiceDescriptionImporter_t2272364891 * get_importer_3() const { return ___importer_3; }
	inline ServiceDescriptionImporter_t2272364891 ** get_address_of_importer_3() { return &___importer_3; }
	inline void set_importer_3(ServiceDescriptionImporter_t2272364891 * value)
	{
		___importer_3 = value;
		Il2CppCodeGenWriteBarrier(&___importer_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
