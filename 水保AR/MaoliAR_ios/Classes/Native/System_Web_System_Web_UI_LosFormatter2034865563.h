﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.UI.ObjectStateFormatter
struct ObjectStateFormatter_t1543808709;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.LosFormatter
struct  LosFormatter_t2034865563  : public Il2CppObject
{
public:
	// System.Web.UI.ObjectStateFormatter System.Web.UI.LosFormatter::osf
	ObjectStateFormatter_t1543808709 * ___osf_0;

public:
	inline static int32_t get_offset_of_osf_0() { return static_cast<int32_t>(offsetof(LosFormatter_t2034865563, ___osf_0)); }
	inline ObjectStateFormatter_t1543808709 * get_osf_0() const { return ___osf_0; }
	inline ObjectStateFormatter_t1543808709 ** get_address_of_osf_0() { return &___osf_0; }
	inline void set_osf_0(ObjectStateFormatter_t1543808709 * value)
	{
		___osf_0 = value;
		Il2CppCodeGenWriteBarrier(&___osf_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
