﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Serialization_XmlSerializatio982275218.h"

// System.Reflection.MethodInfo
struct MethodInfo_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.ServiceDescriptionWriterBase
struct  ServiceDescriptionWriterBase_t1979257227  : public XmlSerializationWriter_t982275218
{
public:

public:
};

struct ServiceDescriptionWriterBase_t1979257227_StaticFields
{
public:
	// System.Reflection.MethodInfo System.Web.Services.Description.ServiceDescriptionWriterBase::toBinHexStringMethod
	MethodInfo_t * ___toBinHexStringMethod_8;

public:
	inline static int32_t get_offset_of_toBinHexStringMethod_8() { return static_cast<int32_t>(offsetof(ServiceDescriptionWriterBase_t1979257227_StaticFields, ___toBinHexStringMethod_8)); }
	inline MethodInfo_t * get_toBinHexStringMethod_8() const { return ___toBinHexStringMethod_8; }
	inline MethodInfo_t ** get_address_of_toBinHexStringMethod_8() { return &___toBinHexStringMethod_8; }
	inline void set_toBinHexStringMethod_8(MethodInfo_t * value)
	{
		___toBinHexStringMethod_8 = value;
		Il2CppCodeGenWriteBarrier(&___toBinHexStringMethod_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
