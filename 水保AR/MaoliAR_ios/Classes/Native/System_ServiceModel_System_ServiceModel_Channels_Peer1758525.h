﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_D2906515918.h"

// System.ServiceModel.Channels.IChannelFactory`1<System.ServiceModel.Channels.IDuplexSessionChannel>
struct IChannelFactory_1_t1647295868;
// System.ServiceModel.Channels.PeerTransportBindingElement
struct PeerTransportBindingElement_t261693216;
// System.ServiceModel.PeerResolver
struct PeerResolver_t1567980581;
// System.ServiceModel.PeerNode
struct PeerNode_t3248961724;
// System.ServiceModel.ServiceHost
struct ServiceHost_t1894294968;
// System.ServiceModel.Channels.TcpChannelInfo
struct TcpChannelInfo_t709592533;
// System.Collections.Generic.List`1<System.ServiceModel.Channels.PeerDuplexChannel/RemotePeerConnection>
struct List_1_t3335657358;
// System.ServiceModel.PeerNodeAddress
struct PeerNodeAddress_t2098027372;
// System.Collections.Generic.Queue`1<System.ServiceModel.Channels.Message>
struct Queue_1_t715774467;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t1333520283;
// System.EventHandler`1<System.ServiceModel.UnknownMessageReceivedEventArgs>
struct EventHandler_1_t3913994551;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.PeerDuplexChannel
struct  PeerDuplexChannel_t1758525  : public DuplexChannelBase_t2906515918
{
public:
	// System.ServiceModel.Channels.IChannelFactory`1<System.ServiceModel.Channels.IDuplexSessionChannel> System.ServiceModel.Channels.PeerDuplexChannel::client_factory
	Il2CppObject* ___client_factory_20;
	// System.ServiceModel.Channels.PeerTransportBindingElement System.ServiceModel.Channels.PeerDuplexChannel::binding
	PeerTransportBindingElement_t261693216 * ___binding_21;
	// System.ServiceModel.PeerResolver System.ServiceModel.Channels.PeerDuplexChannel::resolver
	PeerResolver_t1567980581 * ___resolver_22;
	// System.ServiceModel.PeerNode System.ServiceModel.Channels.PeerDuplexChannel::node
	PeerNode_t3248961724 * ___node_23;
	// System.ServiceModel.ServiceHost System.ServiceModel.Channels.PeerDuplexChannel::listener_host
	ServiceHost_t1894294968 * ___listener_host_24;
	// System.ServiceModel.Channels.TcpChannelInfo System.ServiceModel.Channels.PeerDuplexChannel::info
	TcpChannelInfo_t709592533 * ___info_25;
	// System.Collections.Generic.List`1<System.ServiceModel.Channels.PeerDuplexChannel/RemotePeerConnection> System.ServiceModel.Channels.PeerDuplexChannel::peers
	List_1_t3335657358 * ___peers_26;
	// System.ServiceModel.PeerNodeAddress System.ServiceModel.Channels.PeerDuplexChannel::local_node_address
	PeerNodeAddress_t2098027372 * ___local_node_address_27;
	// System.Collections.Generic.Queue`1<System.ServiceModel.Channels.Message> System.ServiceModel.Channels.PeerDuplexChannel::queue
	Queue_1_t715774467 * ___queue_28;
	// System.Threading.AutoResetEvent System.ServiceModel.Channels.PeerDuplexChannel::receive_handle
	AutoResetEvent_t1333520283 * ___receive_handle_29;

public:
	inline static int32_t get_offset_of_client_factory_20() { return static_cast<int32_t>(offsetof(PeerDuplexChannel_t1758525, ___client_factory_20)); }
	inline Il2CppObject* get_client_factory_20() const { return ___client_factory_20; }
	inline Il2CppObject** get_address_of_client_factory_20() { return &___client_factory_20; }
	inline void set_client_factory_20(Il2CppObject* value)
	{
		___client_factory_20 = value;
		Il2CppCodeGenWriteBarrier(&___client_factory_20, value);
	}

	inline static int32_t get_offset_of_binding_21() { return static_cast<int32_t>(offsetof(PeerDuplexChannel_t1758525, ___binding_21)); }
	inline PeerTransportBindingElement_t261693216 * get_binding_21() const { return ___binding_21; }
	inline PeerTransportBindingElement_t261693216 ** get_address_of_binding_21() { return &___binding_21; }
	inline void set_binding_21(PeerTransportBindingElement_t261693216 * value)
	{
		___binding_21 = value;
		Il2CppCodeGenWriteBarrier(&___binding_21, value);
	}

	inline static int32_t get_offset_of_resolver_22() { return static_cast<int32_t>(offsetof(PeerDuplexChannel_t1758525, ___resolver_22)); }
	inline PeerResolver_t1567980581 * get_resolver_22() const { return ___resolver_22; }
	inline PeerResolver_t1567980581 ** get_address_of_resolver_22() { return &___resolver_22; }
	inline void set_resolver_22(PeerResolver_t1567980581 * value)
	{
		___resolver_22 = value;
		Il2CppCodeGenWriteBarrier(&___resolver_22, value);
	}

	inline static int32_t get_offset_of_node_23() { return static_cast<int32_t>(offsetof(PeerDuplexChannel_t1758525, ___node_23)); }
	inline PeerNode_t3248961724 * get_node_23() const { return ___node_23; }
	inline PeerNode_t3248961724 ** get_address_of_node_23() { return &___node_23; }
	inline void set_node_23(PeerNode_t3248961724 * value)
	{
		___node_23 = value;
		Il2CppCodeGenWriteBarrier(&___node_23, value);
	}

	inline static int32_t get_offset_of_listener_host_24() { return static_cast<int32_t>(offsetof(PeerDuplexChannel_t1758525, ___listener_host_24)); }
	inline ServiceHost_t1894294968 * get_listener_host_24() const { return ___listener_host_24; }
	inline ServiceHost_t1894294968 ** get_address_of_listener_host_24() { return &___listener_host_24; }
	inline void set_listener_host_24(ServiceHost_t1894294968 * value)
	{
		___listener_host_24 = value;
		Il2CppCodeGenWriteBarrier(&___listener_host_24, value);
	}

	inline static int32_t get_offset_of_info_25() { return static_cast<int32_t>(offsetof(PeerDuplexChannel_t1758525, ___info_25)); }
	inline TcpChannelInfo_t709592533 * get_info_25() const { return ___info_25; }
	inline TcpChannelInfo_t709592533 ** get_address_of_info_25() { return &___info_25; }
	inline void set_info_25(TcpChannelInfo_t709592533 * value)
	{
		___info_25 = value;
		Il2CppCodeGenWriteBarrier(&___info_25, value);
	}

	inline static int32_t get_offset_of_peers_26() { return static_cast<int32_t>(offsetof(PeerDuplexChannel_t1758525, ___peers_26)); }
	inline List_1_t3335657358 * get_peers_26() const { return ___peers_26; }
	inline List_1_t3335657358 ** get_address_of_peers_26() { return &___peers_26; }
	inline void set_peers_26(List_1_t3335657358 * value)
	{
		___peers_26 = value;
		Il2CppCodeGenWriteBarrier(&___peers_26, value);
	}

	inline static int32_t get_offset_of_local_node_address_27() { return static_cast<int32_t>(offsetof(PeerDuplexChannel_t1758525, ___local_node_address_27)); }
	inline PeerNodeAddress_t2098027372 * get_local_node_address_27() const { return ___local_node_address_27; }
	inline PeerNodeAddress_t2098027372 ** get_address_of_local_node_address_27() { return &___local_node_address_27; }
	inline void set_local_node_address_27(PeerNodeAddress_t2098027372 * value)
	{
		___local_node_address_27 = value;
		Il2CppCodeGenWriteBarrier(&___local_node_address_27, value);
	}

	inline static int32_t get_offset_of_queue_28() { return static_cast<int32_t>(offsetof(PeerDuplexChannel_t1758525, ___queue_28)); }
	inline Queue_1_t715774467 * get_queue_28() const { return ___queue_28; }
	inline Queue_1_t715774467 ** get_address_of_queue_28() { return &___queue_28; }
	inline void set_queue_28(Queue_1_t715774467 * value)
	{
		___queue_28 = value;
		Il2CppCodeGenWriteBarrier(&___queue_28, value);
	}

	inline static int32_t get_offset_of_receive_handle_29() { return static_cast<int32_t>(offsetof(PeerDuplexChannel_t1758525, ___receive_handle_29)); }
	inline AutoResetEvent_t1333520283 * get_receive_handle_29() const { return ___receive_handle_29; }
	inline AutoResetEvent_t1333520283 ** get_address_of_receive_handle_29() { return &___receive_handle_29; }
	inline void set_receive_handle_29(AutoResetEvent_t1333520283 * value)
	{
		___receive_handle_29 = value;
		Il2CppCodeGenWriteBarrier(&___receive_handle_29, value);
	}
};

struct PeerDuplexChannel_t1758525_StaticFields
{
public:
	// System.EventHandler`1<System.ServiceModel.UnknownMessageReceivedEventArgs> System.ServiceModel.Channels.PeerDuplexChannel::<>f__am$cacheA
	EventHandler_1_t3913994551 * ___U3CU3Ef__amU24cacheA_30;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_30() { return static_cast<int32_t>(offsetof(PeerDuplexChannel_t1758525_StaticFields, ___U3CU3Ef__amU24cacheA_30)); }
	inline EventHandler_1_t3913994551 * get_U3CU3Ef__amU24cacheA_30() const { return ___U3CU3Ef__amU24cacheA_30; }
	inline EventHandler_1_t3913994551 ** get_address_of_U3CU3Ef__amU24cacheA_30() { return &___U3CU3Ef__amU24cacheA_30; }
	inline void set_U3CU3Ef__amU24cacheA_30(EventHandler_1_t3913994551 * value)
	{
		___U3CU3Ef__amU24cacheA_30 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_30, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
