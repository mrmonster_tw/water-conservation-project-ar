﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaDeinitBehaviour
struct  VuforiaDeinitBehaviour_t1700985552  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct VuforiaDeinitBehaviour_t1700985552_StaticFields
{
public:
	// System.Boolean Vuforia.VuforiaDeinitBehaviour::mAppIsQuitting
	bool ___mAppIsQuitting_2;

public:
	inline static int32_t get_offset_of_mAppIsQuitting_2() { return static_cast<int32_t>(offsetof(VuforiaDeinitBehaviour_t1700985552_StaticFields, ___mAppIsQuitting_2)); }
	inline bool get_mAppIsQuitting_2() const { return ___mAppIsQuitting_2; }
	inline bool* get_address_of_mAppIsQuitting_2() { return &___mAppIsQuitting_2; }
	inline void set_mAppIsQuitting_2(bool value)
	{
		___mAppIsQuitting_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
