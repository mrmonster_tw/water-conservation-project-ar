﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.Configuration.Web20DefaultConfig
struct Web20DefaultConfig_t163121754;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.Web20DefaultConfig
struct  Web20DefaultConfig_t163121754  : public Il2CppObject
{
public:

public:
};

struct Web20DefaultConfig_t163121754_StaticFields
{
public:
	// System.Web.Configuration.Web20DefaultConfig System.Web.Configuration.Web20DefaultConfig::instance
	Web20DefaultConfig_t163121754 * ___instance_0;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(Web20DefaultConfig_t163121754_StaticFields, ___instance_0)); }
	inline Web20DefaultConfig_t163121754 * get_instance_0() const { return ___instance_0; }
	inline Web20DefaultConfig_t163121754 ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(Web20DefaultConfig_t163121754 * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier(&___instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
