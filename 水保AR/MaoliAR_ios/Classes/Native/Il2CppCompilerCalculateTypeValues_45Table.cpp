﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_ServiceModel_Mono_CodeGeneration_CodeExpres2163794278.h"
#include "System_ServiceModel_Mono_CodeGeneration_CodeGenera3133936989.h"
#include "System_ServiceModel_Mono_CodeGeneration_CodeItem493730090.h"
#include "System_ServiceModel_Mono_CodeGeneration_CodeStatemen30174410.h"
#include "System_ServiceModel_Mono_CodeGeneration_CodeLitera4115576979.h"
#include "System_ServiceModel_Mono_CodeGeneration_CodeMethod1546731557.h"
#include "System_ServiceModel_Mono_CodeGeneration_CodeMethod3555455132.h"
#include "System_ServiceModel_Mono_CodeGeneration_CodeModule2318530162.h"
#include "System_ServiceModel_Mono_CodeGeneration_CodeNewArr3766519279.h"
#include "System_ServiceModel_Mono_CodeGeneration_CodeProper3279272335.h"
#include "System_ServiceModel_Mono_CodeGeneration_CodeReturn3032105681.h"
#include "System_ServiceModel_Mono_CodeGeneration_CodeValueRe975266093.h"
#include "System_ServiceModel_Mono_CodeGeneration_CodeVariab1663035579.h"
#include "System_ServiceModel_Mono_CodeGeneration_CodeVariab2879773301.h"
#include "System_ServiceModel_Mono_Security_Cryptography_MD5S723838944.h"
#include "System_ServiceModel_Mono_Security_Cryptography_HMA3689525210.h"
#include "System_ServiceModel_Mono_Security_BitConverterLE2108532978.h"
#include "System_ServiceModel_Mono_Security_Protocol_Ntlm_Cha399452983.h"
#include "System_ServiceModel_Mono_Security_Protocol_Ntlm_Mes422981883.h"
#include "System_ServiceModel_Mono_Security_Protocol_Ntlm_Ntl822829952.h"
#include "System_ServiceModel_Mono_Security_Protocol_Ntlm_Nt4089317265.h"
#include "System_ServiceModel_Mono_Security_Protocol_Ntlm_Ntl663077424.h"
#include "System_ServiceModel_Mono_Security_Protocol_Ntlm_Ty2139513923.h"
#include "System_ServiceModel_Mono_Security_Protocol_Ntlm_Ty2139513824.h"
#include "System_ServiceModel_Mono_Security_Protocol_Ntlm_Ty2139513857.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Han3519510577.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Han1824902654.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Han2486981163.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Hands97965998.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Hand643923608.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Han2716496392.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Han3690397592.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Han3860330041.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Han3343859594.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Han1850379324.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Hand699469151.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Hand501187049.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Hand826355380.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Han2156931731.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Han2998762695.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Hand507150716.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Hands47429503.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Han2184566575.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Han3563249236.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Han3527482625.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Hand253764541.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Han1048339864.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Han1004704908.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Han3696583168.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Han3062346172.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Ale2246417555.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Ale1549755611.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Ale4059934885.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Cip1174400495.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Cip3414744575.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Cip1129639304.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Cip3316559455.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Cli2797401965.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Cli2031137796.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Cli1775821398.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Cli2353595803.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Con2602934270.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Con3971234707.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Exc1320888206.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Hand756684113.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Has2376832258.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_RSA3558097625.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_RSA2709678514.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Rec3759049701.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Rec3680907657.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Rec3718352467.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Sec4242483129.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Sec2199972650.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Sec1513093309.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Ser3848440993.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Ser3783944360.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Ssl1981645747.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Ssl3914624661.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Ssl2107581772.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_SslS875102504.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Ssl1667413407.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Ssl3504282820.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Tls1545013223.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Tls2486039503.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Tls3534743363.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Tls4144396432.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Tls2365453965.h"
#include "System_ServiceModel_Mono_Xml_XPath_DTMXPathDocumen2335062454.h"
#include "System_ServiceModel_Mono_Xml_XPath_DTMXPathDocumen2031536402.h"
#include "System_ServiceModel_Mono_Xml_XPath_SeekableDTMXPat1826748592.h"
#include "System_ServiceModel_Mono_Xml_XPath_DTMXPathLinkedN3353097823.h"
#include "System_ServiceModel_Mono_Xml_XPath_DTMXPathAttribu3707096872.h"
#include "System_ServiceModel_Mono_Xml_XPath_DTMXPathNamespa1119120712.h"
#include "System_ServiceModel_System_ServiceModel_Activation1459838541.h"
#include "System_ServiceModel_System_ServiceModel_Activation2614556808.h"
#include "System_ServiceModel_System_ServiceModel_Activation1484037768.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4500 = { sizeof (CodeExpression_t2163794278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4501 = { sizeof (CodeGenerationHelper_t3133936989), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4502 = { sizeof (CodeItem_t493730090), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4503 = { sizeof (CodeStatement_t30174410), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4504 = { sizeof (CodeLiteral_t4115576979), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4504[2] = 
{
	CodeLiteral_t4115576979::get_offset_of_value_0(),
	CodeLiteral_t4115576979::get_offset_of_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4505 = { sizeof (CodeMethod_t1546731557), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4505[9] = 
{
	CodeMethod_t1546731557::get_offset_of_methodBase_0(),
	CodeMethod_t1546731557::get_offset_of_builder_1(),
	CodeMethod_t1546731557::get_offset_of_name_2(),
	CodeMethod_t1546731557::get_offset_of_attributes_3(),
	CodeMethod_t1546731557::get_offset_of_returnType_4(),
	CodeMethod_t1546731557::get_offset_of_typeBuilder_5(),
	CodeMethod_t1546731557::get_offset_of_parameterTypes_6(),
	CodeMethod_t1546731557::get_offset_of_customAttributes_7(),
	CodeMethod_t1546731557::get_offset_of_cls_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4506 = { sizeof (CodeMethodCall_t3555455132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4506[4] = 
{
	CodeMethodCall_t3555455132::get_offset_of_target_0(),
	CodeMethodCall_t3555455132::get_offset_of_parameters_1(),
	CodeMethodCall_t3555455132::get_offset_of_method_2(),
	CodeMethodCall_t3555455132::get_offset_of_codeMethod_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4507 = { sizeof (CodeModule_t2318530162), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4507[1] = 
{
	CodeModule_t2318530162::get_offset_of_module_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4508 = { sizeof (CodeNewArray_t3766519279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4508[2] = 
{
	CodeNewArray_t3766519279::get_offset_of_elemType_0(),
	CodeNewArray_t3766519279::get_offset_of_size_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4509 = { sizeof (CodeProperty_t3279272335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4509[4] = 
{
	CodeProperty_t3279272335::get_offset_of_propertyInfo_0(),
	CodeProperty_t3279272335::get_offset_of_get_builder_1(),
	CodeProperty_t3279272335::get_offset_of_set_builder_2(),
	CodeProperty_t3279272335::get_offset_of_parameterTypes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4510 = { sizeof (CodeReturn_t3032105681), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4510[2] = 
{
	CodeReturn_t3032105681::get_offset_of_retValue_0(),
	CodeReturn_t3032105681::get_offset_of_codeBuilder_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4511 = { sizeof (CodeValueReference_t975266093), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4512 = { sizeof (CodeVariableDeclaration_t1663035579), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4512[1] = 
{
	CodeVariableDeclaration_t1663035579::get_offset_of_var_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4513 = { sizeof (CodeVariableReference_t2879773301), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4513[3] = 
{
	CodeVariableReference_t2879773301::get_offset_of_localBuilder_0(),
	CodeVariableReference_t2879773301::get_offset_of_type_1(),
	CodeVariableReference_t2879773301::get_offset_of_name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4514 = { sizeof (MD5SHA1_t723838945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4514[3] = 
{
	MD5SHA1_t723838945::get_offset_of_md5_4(),
	MD5SHA1_t723838945::get_offset_of_sha_5(),
	MD5SHA1_t723838945::get_offset_of_hashing_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4515 = { sizeof (HMAC_t3689525212), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4515[4] = 
{
	HMAC_t3689525212::get_offset_of_hash_5(),
	HMAC_t3689525212::get_offset_of_hashing_6(),
	HMAC_t3689525212::get_offset_of_innerPad_7(),
	HMAC_t3689525212::get_offset_of_outerPad_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4516 = { sizeof (BitConverterLE_t2108532980), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4517 = { sizeof (ChallengeResponse_t399452983), -1, sizeof(ChallengeResponse_t399452983_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4517[6] = 
{
	ChallengeResponse_t399452983_StaticFields::get_offset_of_magic_0(),
	ChallengeResponse_t399452983_StaticFields::get_offset_of_nullEncMagic_1(),
	ChallengeResponse_t399452983::get_offset_of__disposed_2(),
	ChallengeResponse_t399452983::get_offset_of__challenge_3(),
	ChallengeResponse_t399452983::get_offset_of__lmpwd_4(),
	ChallengeResponse_t399452983::get_offset_of__ntpwd_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4518 = { sizeof (MessageBase_t422981883), -1, sizeof(MessageBase_t422981883_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4518[6] = 
{
	MessageBase_t422981883_StaticFields::get_offset_of__current_os_version_0(),
	MessageBase_t422981883_StaticFields::get_offset_of_header_1(),
	MessageBase_t422981883::get_offset_of__type_2(),
	MessageBase_t422981883::get_offset_of__flags_3(),
	MessageBase_t422981883::get_offset_of__version_4(),
	MessageBase_t422981883::get_offset_of__osversion_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4519 = { sizeof (NtlmFlags_t822829952)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4519[17] = 
{
	NtlmFlags_t822829952::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4520 = { sizeof (NtlmTargetInformation_t4089317265), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4520[4] = 
{
	NtlmTargetInformation_t4089317265::get_offset_of__server_0(),
	NtlmTargetInformation_t4089317265::get_offset_of__domain_1(),
	NtlmTargetInformation_t4089317265::get_offset_of__dns_host_2(),
	NtlmTargetInformation_t4089317265::get_offset_of__dns_domain_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4521 = { sizeof (NtlmVersion_t663077424)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4521[4] = 
{
	NtlmVersion_t663077424::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4522 = { sizeof (Type1Message_t2139513923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4522[2] = 
{
	Type1Message_t2139513923::get_offset_of__host_6(),
	Type1Message_t2139513923::get_offset_of__domain_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4523 = { sizeof (Type2Message_t2139513824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4523[4] = 
{
	Type2Message_t2139513824::get_offset_of__nonce_6(),
	Type2Message_t2139513824::get_offset_of__context_7(),
	Type2Message_t2139513824::get_offset_of__target_8(),
	Type2Message_t2139513824::get_offset_of__target_name_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4524 = { sizeof (Type3Message_t2139513857), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4524[8] = 
{
	Type3Message_t2139513857::get_offset_of__challenge_6(),
	Type3Message_t2139513857::get_offset_of__host_7(),
	Type3Message_t2139513857::get_offset_of__domain_8(),
	Type3Message_t2139513857::get_offset_of__username_9(),
	Type3Message_t2139513857::get_offset_of__password_10(),
	Type3Message_t2139513857::get_offset_of__lm_11(),
	Type3Message_t2139513857::get_offset_of__nt_12(),
	Type3Message_t2139513857::get_offset_of__nonce_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4525 = { sizeof (TlsClientCertificate_t3519510578), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4525[2] = 
{
	TlsClientCertificate_t3519510578::get_offset_of_clientCertSelected_10(),
	TlsClientCertificate_t3519510578::get_offset_of_clientCert_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4526 = { sizeof (TlsClientCertificateVerify_t1824902655), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4527 = { sizeof (TlsClientFinished_t2486981164), -1, sizeof(TlsClientFinished_t2486981164_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4527[1] = 
{
	TlsClientFinished_t2486981164_StaticFields::get_offset_of_Ssl3Marker_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4528 = { sizeof (TlsClientHello_t97965999), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4528[1] = 
{
	TlsClientHello_t97965999::get_offset_of_random_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4529 = { sizeof (TlsClientKeyExchange_t643923609), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4530 = { sizeof (TlsServerCertificate_t2716496393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4530[1] = 
{
	TlsServerCertificate_t2716496393::get_offset_of_certificates_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4531 = { sizeof (TlsServerCertificateRequest_t3690397593), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4531[2] = 
{
	TlsServerCertificateRequest_t3690397593::get_offset_of_certificateTypes_10(),
	TlsServerCertificateRequest_t3690397593::get_offset_of_distinguisedNames_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4532 = { sizeof (TlsServerFinished_t3860330042), -1, sizeof(TlsServerFinished_t3860330042_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4532[1] = 
{
	TlsServerFinished_t3860330042_StaticFields::get_offset_of_Ssl3Marker_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4533 = { sizeof (TlsServerHello_t3343859595), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4533[4] = 
{
	TlsServerHello_t3343859595::get_offset_of_compressionMethod_10(),
	TlsServerHello_t3343859595::get_offset_of_random_11(),
	TlsServerHello_t3343859595::get_offset_of_sessionId_12(),
	TlsServerHello_t3343859595::get_offset_of_cipherSuite_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4534 = { sizeof (TlsServerHelloDone_t1850379325), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4535 = { sizeof (TlsServerKeyExchange_t699469152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4535[2] = 
{
	TlsServerKeyExchange_t699469152::get_offset_of_rsaParams_10(),
	TlsServerKeyExchange_t699469152::get_offset_of_signedParams_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4536 = { sizeof (TlsClientCertificate_t501187050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4536[1] = 
{
	TlsClientCertificate_t501187050::get_offset_of_clientCertificates_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4537 = { sizeof (TlsClientCertificateVerify_t826355381), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4538 = { sizeof (TlsClientFinished_t2156931732), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4539 = { sizeof (TlsClientHello_t2998762696), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4539[4] = 
{
	TlsClientHello_t2998762696::get_offset_of_random_10(),
	TlsClientHello_t2998762696::get_offset_of_sessionId_11(),
	TlsClientHello_t2998762696::get_offset_of_cipherSuites_12(),
	TlsClientHello_t2998762696::get_offset_of_compressionMethods_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4540 = { sizeof (TlsClientKeyExchange_t507150717), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4541 = { sizeof (TlsServerCertificate_t47429504), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4542 = { sizeof (TlsServerCertificateRequest_t2184566576), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4543 = { sizeof (TlsServerFinished_t3563249237), -1, sizeof(TlsServerFinished_t3563249237_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4543[1] = 
{
	TlsServerFinished_t3563249237_StaticFields::get_offset_of_Ssl3Marker_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4544 = { sizeof (TlsServerHello_t3527482626), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4544[2] = 
{
	TlsServerHello_t3527482626::get_offset_of_unixTime_10(),
	TlsServerHello_t3527482626::get_offset_of_random_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4545 = { sizeof (TlsServerHelloDone_t253764542), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4546 = { sizeof (TlsServerKeyExchange_t1048339865), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4547 = { sizeof (ClientCertificateType_t1004704909)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4547[6] = 
{
	ClientCertificateType_t1004704909::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4548 = { sizeof (HandshakeMessage_t3696583169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4548[4] = 
{
	HandshakeMessage_t3696583169::get_offset_of_context_6(),
	HandshakeMessage_t3696583169::get_offset_of_handshakeType_7(),
	HandshakeMessage_t3696583169::get_offset_of_contentType_8(),
	HandshakeMessage_t3696583169::get_offset_of_cache_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4549 = { sizeof (HandshakeType_t3062346173)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4549[12] = 
{
	HandshakeType_t3062346173::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4550 = { sizeof (AlertLevel_t2246417556)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4550[3] = 
{
	AlertLevel_t2246417556::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4551 = { sizeof (AlertDescription_t1549755612)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4551[25] = 
{
	AlertDescription_t1549755612::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4552 = { sizeof (Alert_t4059934886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4552[2] = 
{
	Alert_t4059934886::get_offset_of_level_0(),
	Alert_t4059934886::get_offset_of_description_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4553 = { sizeof (CipherAlgorithmType_t1174400496)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4553[8] = 
{
	CipherAlgorithmType_t1174400496::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4554 = { sizeof (CipherSuite_t3414744576), -1, sizeof(CipherSuite_t3414744576_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4554[21] = 
{
	CipherSuite_t3414744576_StaticFields::get_offset_of_EmptyArray_0(),
	CipherSuite_t3414744576::get_offset_of_code_1(),
	CipherSuite_t3414744576::get_offset_of_name_2(),
	CipherSuite_t3414744576::get_offset_of_cipherAlgorithmType_3(),
	CipherSuite_t3414744576::get_offset_of_hashAlgorithmType_4(),
	CipherSuite_t3414744576::get_offset_of_exchangeAlgorithmType_5(),
	CipherSuite_t3414744576::get_offset_of_isExportable_6(),
	CipherSuite_t3414744576::get_offset_of_cipherMode_7(),
	CipherSuite_t3414744576::get_offset_of_keyMaterialSize_8(),
	CipherSuite_t3414744576::get_offset_of_keyBlockSize_9(),
	CipherSuite_t3414744576::get_offset_of_expandedKeyMaterialSize_10(),
	CipherSuite_t3414744576::get_offset_of_effectiveKeyBits_11(),
	CipherSuite_t3414744576::get_offset_of_ivSize_12(),
	CipherSuite_t3414744576::get_offset_of_blockSize_13(),
	CipherSuite_t3414744576::get_offset_of_context_14(),
	CipherSuite_t3414744576::get_offset_of_encryptionAlgorithm_15(),
	CipherSuite_t3414744576::get_offset_of_encryptionCipher_16(),
	CipherSuite_t3414744576::get_offset_of_decryptionAlgorithm_17(),
	CipherSuite_t3414744576::get_offset_of_decryptionCipher_18(),
	CipherSuite_t3414744576::get_offset_of_clientHMAC_19(),
	CipherSuite_t3414744576::get_offset_of_serverHMAC_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4555 = { sizeof (CipherSuiteCollection_t1129639305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4555[2] = 
{
	CipherSuiteCollection_t1129639305::get_offset_of_cipherSuites_0(),
	CipherSuiteCollection_t1129639305::get_offset_of_protocol_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4556 = { sizeof (CipherSuiteFactory_t3316559456), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4557 = { sizeof (ClientContext_t2797401966), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4557[2] = 
{
	ClientContext_t2797401966::get_offset_of_sslStream_29(),
	ClientContext_t2797401966::get_offset_of_clientHelloProtocol_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4558 = { sizeof (ClientRecordProtocol_t2031137797), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4559 = { sizeof (ClientSessionInfo_t1775821399), -1, sizeof(ClientSessionInfo_t1775821399_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4559[6] = 
{
	ClientSessionInfo_t1775821399_StaticFields::get_offset_of_ValidityInterval_0(),
	ClientSessionInfo_t1775821399::get_offset_of_disposed_1(),
	ClientSessionInfo_t1775821399::get_offset_of_validuntil_2(),
	ClientSessionInfo_t1775821399::get_offset_of_host_3(),
	ClientSessionInfo_t1775821399::get_offset_of_sid_4(),
	ClientSessionInfo_t1775821399::get_offset_of_masterSecret_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4560 = { sizeof (ClientSessionCache_t2353595804), -1, sizeof(ClientSessionCache_t2353595804_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4560[2] = 
{
	ClientSessionCache_t2353595804_StaticFields::get_offset_of_cache_0(),
	ClientSessionCache_t2353595804_StaticFields::get_offset_of_locker_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4561 = { sizeof (ContentType_t2602934271)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4561[5] = 
{
	ContentType_t2602934271::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4562 = { sizeof (Context_t3971234708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4562[29] = 
{
	Context_t3971234708::get_offset_of_securityProtocol_0(),
	Context_t3971234708::get_offset_of_sessionId_1(),
	Context_t3971234708::get_offset_of_compressionMethod_2(),
	Context_t3971234708::get_offset_of_serverSettings_3(),
	Context_t3971234708::get_offset_of_clientSettings_4(),
	Context_t3971234708::get_offset_of_current_5(),
	Context_t3971234708::get_offset_of_negotiating_6(),
	Context_t3971234708::get_offset_of_read_7(),
	Context_t3971234708::get_offset_of_write_8(),
	Context_t3971234708::get_offset_of_supportedCiphers_9(),
	Context_t3971234708::get_offset_of_lastHandshakeMsg_10(),
	Context_t3971234708::get_offset_of_handshakeState_11(),
	Context_t3971234708::get_offset_of_abbreviatedHandshake_12(),
	Context_t3971234708::get_offset_of_connectionEnd_13(),
	Context_t3971234708::get_offset_of_protocolNegotiated_14(),
	Context_t3971234708::get_offset_of_writeSequenceNumber_15(),
	Context_t3971234708::get_offset_of_readSequenceNumber_16(),
	Context_t3971234708::get_offset_of_clientRandom_17(),
	Context_t3971234708::get_offset_of_serverRandom_18(),
	Context_t3971234708::get_offset_of_randomCS_19(),
	Context_t3971234708::get_offset_of_randomSC_20(),
	Context_t3971234708::get_offset_of_masterSecret_21(),
	Context_t3971234708::get_offset_of_clientWriteKey_22(),
	Context_t3971234708::get_offset_of_serverWriteKey_23(),
	Context_t3971234708::get_offset_of_clientWriteIV_24(),
	Context_t3971234708::get_offset_of_serverWriteIV_25(),
	Context_t3971234708::get_offset_of_handshakeMessages_26(),
	Context_t3971234708::get_offset_of_random_27(),
	Context_t3971234708::get_offset_of_recordProtocol_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4563 = { sizeof (ExchangeAlgorithmType_t1320888207)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4563[6] = 
{
	ExchangeAlgorithmType_t1320888207::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4564 = { sizeof (HandshakeState_t756684114)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4564[4] = 
{
	HandshakeState_t756684114::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4565 = { sizeof (HashAlgorithmType_t2376832259)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4565[4] = 
{
	HashAlgorithmType_t2376832259::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4566 = { sizeof (RSASslSignatureDeformatter_t3558097626), -1, sizeof(RSASslSignatureDeformatter_t3558097626_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4566[3] = 
{
	RSASslSignatureDeformatter_t3558097626::get_offset_of_key_0(),
	RSASslSignatureDeformatter_t3558097626::get_offset_of_hash_1(),
	RSASslSignatureDeformatter_t3558097626_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4567 = { sizeof (RSASslSignatureFormatter_t2709678515), -1, sizeof(RSASslSignatureFormatter_t2709678515_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4567[3] = 
{
	RSASslSignatureFormatter_t2709678515::get_offset_of_key_0(),
	RSASslSignatureFormatter_t2709678515::get_offset_of_hash_1(),
	RSASslSignatureFormatter_t2709678515_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4568 = { sizeof (RecordProtocol_t3759049702), -1, sizeof(RecordProtocol_t3759049702_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4568[3] = 
{
	RecordProtocol_t3759049702_StaticFields::get_offset_of_record_processing_0(),
	RecordProtocol_t3759049702::get_offset_of_innerStream_1(),
	RecordProtocol_t3759049702::get_offset_of_context_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4569 = { sizeof (ReceiveRecordAsyncResult_t3680907658), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4569[9] = 
{
	ReceiveRecordAsyncResult_t3680907658::get_offset_of_locker_0(),
	ReceiveRecordAsyncResult_t3680907658::get_offset_of__userCallback_1(),
	ReceiveRecordAsyncResult_t3680907658::get_offset_of__userState_2(),
	ReceiveRecordAsyncResult_t3680907658::get_offset_of__asyncException_3(),
	ReceiveRecordAsyncResult_t3680907658::get_offset_of_handle_4(),
	ReceiveRecordAsyncResult_t3680907658::get_offset_of__resultingBuffer_5(),
	ReceiveRecordAsyncResult_t3680907658::get_offset_of__record_6(),
	ReceiveRecordAsyncResult_t3680907658::get_offset_of_completed_7(),
	ReceiveRecordAsyncResult_t3680907658::get_offset_of__initialBuffer_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4570 = { sizeof (SendRecordAsyncResult_t3718352468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4570[7] = 
{
	SendRecordAsyncResult_t3718352468::get_offset_of_locker_0(),
	SendRecordAsyncResult_t3718352468::get_offset_of__userCallback_1(),
	SendRecordAsyncResult_t3718352468::get_offset_of__userState_2(),
	SendRecordAsyncResult_t3718352468::get_offset_of__asyncException_3(),
	SendRecordAsyncResult_t3718352468::get_offset_of_handle_4(),
	SendRecordAsyncResult_t3718352468::get_offset_of__message_5(),
	SendRecordAsyncResult_t3718352468::get_offset_of_completed_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4571 = { sizeof (SecurityCompressionType_t4242483130)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4571[3] = 
{
	SecurityCompressionType_t4242483130::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4572 = { sizeof (SecurityParameters_t2199972651), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4572[3] = 
{
	SecurityParameters_t2199972651::get_offset_of_cipher_0(),
	SecurityParameters_t2199972651::get_offset_of_clientWriteMAC_1(),
	SecurityParameters_t2199972651::get_offset_of_serverWriteMAC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4573 = { sizeof (SecurityProtocolType_t1513093310)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4573[5] = 
{
	SecurityProtocolType_t1513093310::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4574 = { sizeof (ServerContext_t3848440994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4574[2] = 
{
	ServerContext_t3848440994::get_offset_of_sslStream_29(),
	ServerContext_t3848440994::get_offset_of_clientCertificateRequired_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4575 = { sizeof (ServerRecordProtocol_t3783944361), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4576 = { sizeof (SslCipherSuite_t1981645748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4576[3] = 
{
	SslCipherSuite_t1981645748::get_offset_of_pad1_21(),
	SslCipherSuite_t1981645748::get_offset_of_pad2_22(),
	SslCipherSuite_t1981645748::get_offset_of_header_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4577 = { sizeof (SslClientStream_t3914624662), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4577[3] = 
{
	SslClientStream_t3914624662::get_offset_of_ServerCertValidation_15(),
	SslClientStream_t3914624662::get_offset_of_ClientCertSelection_16(),
	SslClientStream_t3914624662::get_offset_of_PrivateKeySelection_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4578 = { sizeof (SslHandshakeHash_t2107581773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4578[8] = 
{
	SslHandshakeHash_t2107581773::get_offset_of_md5_4(),
	SslHandshakeHash_t2107581773::get_offset_of_sha_5(),
	SslHandshakeHash_t2107581773::get_offset_of_hashing_6(),
	SslHandshakeHash_t2107581773::get_offset_of_secret_7(),
	SslHandshakeHash_t2107581773::get_offset_of_innerPadMD5_8(),
	SslHandshakeHash_t2107581773::get_offset_of_outerPadMD5_9(),
	SslHandshakeHash_t2107581773::get_offset_of_innerPadSHA_10(),
	SslHandshakeHash_t2107581773::get_offset_of_outerPadSHA_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4579 = { sizeof (SslServerStream_t875102505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4579[2] = 
{
	SslServerStream_t875102505::get_offset_of_ClientCertValidation_15(),
	SslServerStream_t875102505::get_offset_of_PrivateKeySelection_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4580 = { sizeof (SslStreamBase_t1667413408), -1, sizeof(SslStreamBase_t1667413408_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4580[13] = 
{
	SslStreamBase_t1667413408_StaticFields::get_offset_of_record_processing_2(),
	SslStreamBase_t1667413408::get_offset_of_innerStream_3(),
	SslStreamBase_t1667413408::get_offset_of_inputBuffer_4(),
	SslStreamBase_t1667413408::get_offset_of_context_5(),
	SslStreamBase_t1667413408::get_offset_of_protocol_6(),
	SslStreamBase_t1667413408::get_offset_of_ownsStream_7(),
	SslStreamBase_t1667413408::get_offset_of_disposed_8(),
	SslStreamBase_t1667413408::get_offset_of_negotiate_9(),
	SslStreamBase_t1667413408::get_offset_of_read_10(),
	SslStreamBase_t1667413408::get_offset_of_write_11(),
	SslStreamBase_t1667413408::get_offset_of_negotiationComplete_12(),
	SslStreamBase_t1667413408::get_offset_of_recbuf_13(),
	SslStreamBase_t1667413408::get_offset_of_recordStream_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4581 = { sizeof (InternalAsyncResult_t3504282821), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4581[12] = 
{
	InternalAsyncResult_t3504282821::get_offset_of_locker_0(),
	InternalAsyncResult_t3504282821::get_offset_of__userCallback_1(),
	InternalAsyncResult_t3504282821::get_offset_of__userState_2(),
	InternalAsyncResult_t3504282821::get_offset_of__asyncException_3(),
	InternalAsyncResult_t3504282821::get_offset_of_handle_4(),
	InternalAsyncResult_t3504282821::get_offset_of_completed_5(),
	InternalAsyncResult_t3504282821::get_offset_of__bytesRead_6(),
	InternalAsyncResult_t3504282821::get_offset_of__fromWrite_7(),
	InternalAsyncResult_t3504282821::get_offset_of__proceedAfterHandshake_8(),
	InternalAsyncResult_t3504282821::get_offset_of__buffer_9(),
	InternalAsyncResult_t3504282821::get_offset_of__offset_10(),
	InternalAsyncResult_t3504282821::get_offset_of__count_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4582 = { sizeof (TlsCipherSuite_t1545013224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4582[2] = 
{
	TlsCipherSuite_t1545013224::get_offset_of_header_21(),
	TlsCipherSuite_t1545013224::get_offset_of_headerLock_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4583 = { sizeof (TlsClientSettings_t2486039504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4583[4] = 
{
	TlsClientSettings_t2486039504::get_offset_of_targetHost_0(),
	TlsClientSettings_t2486039504::get_offset_of_certificates_1(),
	TlsClientSettings_t2486039504::get_offset_of_clientCertificate_2(),
	TlsClientSettings_t2486039504::get_offset_of_certificateRSA_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4584 = { sizeof (TlsException_t3534743364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4584[1] = 
{
	TlsException_t3534743364::get_offset_of_alert_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4585 = { sizeof (TlsServerSettings_t4144396433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4585[8] = 
{
	TlsServerSettings_t4144396433::get_offset_of_certificates_0(),
	TlsServerSettings_t4144396433::get_offset_of_certificateRSA_1(),
	TlsServerSettings_t4144396433::get_offset_of_rsaParameters_2(),
	TlsServerSettings_t4144396433::get_offset_of_signedParams_3(),
	TlsServerSettings_t4144396433::get_offset_of_distinguisedNames_4(),
	TlsServerSettings_t4144396433::get_offset_of_serverKeyExchange_5(),
	TlsServerSettings_t4144396433::get_offset_of_certificateRequest_6(),
	TlsServerSettings_t4144396433::get_offset_of_certificateTypes_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4586 = { sizeof (TlsStream_t2365453966), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4586[4] = 
{
	TlsStream_t2365453966::get_offset_of_canRead_2(),
	TlsStream_t2365453966::get_offset_of_canWrite_3(),
	TlsStream_t2365453966::get_offset_of_buffer_4(),
	TlsStream_t2365453966::get_offset_of_temp_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4587 = { sizeof (DTMXPathDocument2_t2335062455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4587[8] = 
{
	DTMXPathDocument2_t2335062455::get_offset_of_root_0(),
	DTMXPathDocument2_t2335062455::get_offset_of_NameTable_1(),
	DTMXPathDocument2_t2335062455::get_offset_of_Nodes_2(),
	DTMXPathDocument2_t2335062455::get_offset_of_Attributes_3(),
	DTMXPathDocument2_t2335062455::get_offset_of_Namespaces_4(),
	DTMXPathDocument2_t2335062455::get_offset_of_AtomicStringPool_5(),
	DTMXPathDocument2_t2335062455::get_offset_of_NonAtomicStringPool_6(),
	DTMXPathDocument2_t2335062455::get_offset_of_IdTable_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4588 = { sizeof (DTMXPathDocumentWriter2_t2031536403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4588[26] = 
{
	DTMXPathDocumentWriter2_t2031536403::get_offset_of_nameTable_1(),
	DTMXPathDocumentWriter2_t2031536403::get_offset_of_nodeCapacity_2(),
	DTMXPathDocumentWriter2_t2031536403::get_offset_of_attributeCapacity_3(),
	DTMXPathDocumentWriter2_t2031536403::get_offset_of_nsCapacity_4(),
	DTMXPathDocumentWriter2_t2031536403::get_offset_of_nodes_5(),
	DTMXPathDocumentWriter2_t2031536403::get_offset_of_attributes_6(),
	DTMXPathDocumentWriter2_t2031536403::get_offset_of_namespaces_7(),
	DTMXPathDocumentWriter2_t2031536403::get_offset_of_atomicStringPool_8(),
	DTMXPathDocumentWriter2_t2031536403::get_offset_of_atomicIndex_9(),
	DTMXPathDocumentWriter2_t2031536403::get_offset_of_nonAtomicStringPool_10(),
	DTMXPathDocumentWriter2_t2031536403::get_offset_of_nonAtomicIndex_11(),
	DTMXPathDocumentWriter2_t2031536403::get_offset_of_idTable_12(),
	DTMXPathDocumentWriter2_t2031536403::get_offset_of_nodeIndex_13(),
	DTMXPathDocumentWriter2_t2031536403::get_offset_of_attributeIndex_14(),
	DTMXPathDocumentWriter2_t2031536403::get_offset_of_nsIndex_15(),
	DTMXPathDocumentWriter2_t2031536403::get_offset_of_parentStack_16(),
	DTMXPathDocumentWriter2_t2031536403::get_offset_of_parentStackIndex_17(),
	DTMXPathDocumentWriter2_t2031536403::get_offset_of_hasAttributes_18(),
	DTMXPathDocumentWriter2_t2031536403::get_offset_of_hasLocalNs_19(),
	DTMXPathDocumentWriter2_t2031536403::get_offset_of_attrIndexAtStart_20(),
	DTMXPathDocumentWriter2_t2031536403::get_offset_of_nsIndexAtStart_21(),
	DTMXPathDocumentWriter2_t2031536403::get_offset_of_lastNsInScope_22(),
	DTMXPathDocumentWriter2_t2031536403::get_offset_of_prevSibling_23(),
	DTMXPathDocumentWriter2_t2031536403::get_offset_of_state_24(),
	DTMXPathDocumentWriter2_t2031536403::get_offset_of_openNamespace_25(),
	DTMXPathDocumentWriter2_t2031536403::get_offset_of_isClosed_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4589 = { sizeof (SeekableDTMXPathNavigator2_t1826748592), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4589[6] = 
{
	SeekableDTMXPathNavigator2_t1826748592::get_offset_of_document_2(),
	SeekableDTMXPathNavigator2_t1826748592::get_offset_of_currentIsNode_3(),
	SeekableDTMXPathNavigator2_t1826748592::get_offset_of_currentIsAttr_4(),
	SeekableDTMXPathNavigator2_t1826748592::get_offset_of_currentNode_5(),
	SeekableDTMXPathNavigator2_t1826748592::get_offset_of_currentAttr_6(),
	SeekableDTMXPathNavigator2_t1826748592::get_offset_of_currentNs_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4590 = { sizeof (DTMXPathLinkedNode2_t3353097824)+ sizeof (Il2CppObject), sizeof(DTMXPathLinkedNode2_t3353097824_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4590[16] = 
{
	DTMXPathLinkedNode2_t3353097824::get_offset_of_FirstChild_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097824::get_offset_of_Parent_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097824::get_offset_of_PreviousSibling_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097824::get_offset_of_NextSibling_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097824::get_offset_of_FirstAttribute_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097824::get_offset_of_FirstNamespace_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097824::get_offset_of_NodeType_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097824::get_offset_of_BaseURI_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097824::get_offset_of_IsEmptyElement_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097824::get_offset_of_LocalName_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097824::get_offset_of_NamespaceURI_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097824::get_offset_of_Prefix_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097824::get_offset_of_Value_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097824::get_offset_of_XmlLang_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097824::get_offset_of_LineNumber_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathLinkedNode2_t3353097824::get_offset_of_LinePosition_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4591 = { sizeof (DTMXPathAttributeNode2_t3707096873)+ sizeof (Il2CppObject), sizeof(DTMXPathAttributeNode2_t3707096873 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4591[8] = 
{
	DTMXPathAttributeNode2_t3707096873::get_offset_of_OwnerElement_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathAttributeNode2_t3707096873::get_offset_of_NextAttribute_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathAttributeNode2_t3707096873::get_offset_of_LocalName_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathAttributeNode2_t3707096873::get_offset_of_NamespaceURI_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathAttributeNode2_t3707096873::get_offset_of_Prefix_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathAttributeNode2_t3707096873::get_offset_of_Value_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathAttributeNode2_t3707096873::get_offset_of_LineNumber_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathAttributeNode2_t3707096873::get_offset_of_LinePosition_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4592 = { sizeof (DTMXPathNamespaceNode2_t1119120713)+ sizeof (Il2CppObject), sizeof(DTMXPathNamespaceNode2_t1119120713 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4592[4] = 
{
	DTMXPathNamespaceNode2_t1119120713::get_offset_of_DeclaredElement_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathNamespaceNode2_t1119120713::get_offset_of_NextNamespace_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathNamespaceNode2_t1119120713::get_offset_of_Name_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTMXPathNamespaceNode2_t1119120713::get_offset_of_Namespace_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4593 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4594 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4594[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4595 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4595[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4596 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4596[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4597 = { sizeof (ServiceHostFactory_t1459838541), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4598 = { sizeof (ServiceHostFactoryBase_t2614556808), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4599 = { sizeof (VirtualPathExtension_t1484037768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4599[1] = 
{
	VirtualPathExtension_t1484037768::get_offset_of_U3CVirtualPathU3Ek__BackingField_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
