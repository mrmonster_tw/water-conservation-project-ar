﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.Web.UI.ControlBuilder
struct ControlBuilder_t2523018631;
// System.Web.Compilation.AspGenerator
struct AspGenerator_t2810561191;
// System.Web.Compilation.ILocation
struct ILocation_t3961726794;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.AspGenerator/CodeRenderParser
struct  CodeRenderParser_t694444359  : public Il2CppObject
{
public:
	// System.String System.Web.Compilation.AspGenerator/CodeRenderParser::str
	String_t* ___str_0;
	// System.Web.UI.ControlBuilder System.Web.Compilation.AspGenerator/CodeRenderParser::builder
	ControlBuilder_t2523018631 * ___builder_1;
	// System.Web.Compilation.AspGenerator System.Web.Compilation.AspGenerator/CodeRenderParser::generator
	AspGenerator_t2810561191 * ___generator_2;
	// System.Web.Compilation.ILocation System.Web.Compilation.AspGenerator/CodeRenderParser::location
	Il2CppObject * ___location_3;

public:
	inline static int32_t get_offset_of_str_0() { return static_cast<int32_t>(offsetof(CodeRenderParser_t694444359, ___str_0)); }
	inline String_t* get_str_0() const { return ___str_0; }
	inline String_t** get_address_of_str_0() { return &___str_0; }
	inline void set_str_0(String_t* value)
	{
		___str_0 = value;
		Il2CppCodeGenWriteBarrier(&___str_0, value);
	}

	inline static int32_t get_offset_of_builder_1() { return static_cast<int32_t>(offsetof(CodeRenderParser_t694444359, ___builder_1)); }
	inline ControlBuilder_t2523018631 * get_builder_1() const { return ___builder_1; }
	inline ControlBuilder_t2523018631 ** get_address_of_builder_1() { return &___builder_1; }
	inline void set_builder_1(ControlBuilder_t2523018631 * value)
	{
		___builder_1 = value;
		Il2CppCodeGenWriteBarrier(&___builder_1, value);
	}

	inline static int32_t get_offset_of_generator_2() { return static_cast<int32_t>(offsetof(CodeRenderParser_t694444359, ___generator_2)); }
	inline AspGenerator_t2810561191 * get_generator_2() const { return ___generator_2; }
	inline AspGenerator_t2810561191 ** get_address_of_generator_2() { return &___generator_2; }
	inline void set_generator_2(AspGenerator_t2810561191 * value)
	{
		___generator_2 = value;
		Il2CppCodeGenWriteBarrier(&___generator_2, value);
	}

	inline static int32_t get_offset_of_location_3() { return static_cast<int32_t>(offsetof(CodeRenderParser_t694444359, ___location_3)); }
	inline Il2CppObject * get_location_3() const { return ___location_3; }
	inline Il2CppObject ** get_address_of_location_3() { return &___location_3; }
	inline void set_location_3(Il2CppObject * value)
	{
		___location_3 = value;
		Il2CppCodeGenWriteBarrier(&___location_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
