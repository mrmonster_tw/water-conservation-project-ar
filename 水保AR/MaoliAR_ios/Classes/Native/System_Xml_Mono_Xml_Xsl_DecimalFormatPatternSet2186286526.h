﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// Mono.Xml.Xsl.DecimalFormatPattern
struct DecimalFormatPattern_t3266596013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.DecimalFormatPatternSet
struct  DecimalFormatPatternSet_t2186286526  : public Il2CppObject
{
public:
	// Mono.Xml.Xsl.DecimalFormatPattern Mono.Xml.Xsl.DecimalFormatPatternSet::positivePattern
	DecimalFormatPattern_t3266596013 * ___positivePattern_0;
	// Mono.Xml.Xsl.DecimalFormatPattern Mono.Xml.Xsl.DecimalFormatPatternSet::negativePattern
	DecimalFormatPattern_t3266596013 * ___negativePattern_1;

public:
	inline static int32_t get_offset_of_positivePattern_0() { return static_cast<int32_t>(offsetof(DecimalFormatPatternSet_t2186286526, ___positivePattern_0)); }
	inline DecimalFormatPattern_t3266596013 * get_positivePattern_0() const { return ___positivePattern_0; }
	inline DecimalFormatPattern_t3266596013 ** get_address_of_positivePattern_0() { return &___positivePattern_0; }
	inline void set_positivePattern_0(DecimalFormatPattern_t3266596013 * value)
	{
		___positivePattern_0 = value;
		Il2CppCodeGenWriteBarrier(&___positivePattern_0, value);
	}

	inline static int32_t get_offset_of_negativePattern_1() { return static_cast<int32_t>(offsetof(DecimalFormatPatternSet_t2186286526, ___negativePattern_1)); }
	inline DecimalFormatPattern_t3266596013 * get_negativePattern_1() const { return ___negativePattern_1; }
	inline DecimalFormatPattern_t3266596013 ** get_address_of_negativePattern_1() { return &___negativePattern_1; }
	inline void set_negativePattern_1(DecimalFormatPattern_t3266596013 * value)
	{
		___negativePattern_1 = value;
		Il2CppCodeGenWriteBarrier(&___negativePattern_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
