﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Xml.XmlDictionary
struct XmlDictionary_t238028028;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Xml.XmlDictionaryString>
struct Dictionary_2_t2392833597;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlBinaryReaderSession
struct  XmlBinaryReaderSession_t3909229952  : public Il2CppObject
{
public:
	// System.Xml.XmlDictionary System.Xml.XmlBinaryReaderSession::dic
	XmlDictionary_t238028028 * ___dic_0;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Xml.XmlDictionaryString> System.Xml.XmlBinaryReaderSession::store
	Dictionary_2_t2392833597 * ___store_1;

public:
	inline static int32_t get_offset_of_dic_0() { return static_cast<int32_t>(offsetof(XmlBinaryReaderSession_t3909229952, ___dic_0)); }
	inline XmlDictionary_t238028028 * get_dic_0() const { return ___dic_0; }
	inline XmlDictionary_t238028028 ** get_address_of_dic_0() { return &___dic_0; }
	inline void set_dic_0(XmlDictionary_t238028028 * value)
	{
		___dic_0 = value;
		Il2CppCodeGenWriteBarrier(&___dic_0, value);
	}

	inline static int32_t get_offset_of_store_1() { return static_cast<int32_t>(offsetof(XmlBinaryReaderSession_t3909229952, ___store_1)); }
	inline Dictionary_2_t2392833597 * get_store_1() const { return ___store_1; }
	inline Dictionary_2_t2392833597 ** get_address_of_store_1() { return &___store_1; }
	inline void set_store_1(Dictionary_2_t2392833597 * value)
	{
		___store_1 = value;
		Il2CppCodeGenWriteBarrier(&___store_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
