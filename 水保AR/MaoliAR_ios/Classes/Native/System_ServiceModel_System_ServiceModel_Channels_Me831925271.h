﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Me464847589.h"

// System.Xml.XmlWriterSettings
struct XmlWriterSettings_t3314986516;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.MessageHeader
struct  MessageHeader_t831925271  : public MessageHeaderInfo_t464847589
{
public:

public:
};

struct MessageHeader_t831925271_StaticFields
{
public:
	// System.Xml.XmlWriterSettings System.ServiceModel.Channels.MessageHeader::writer_settings
	XmlWriterSettings_t3314986516 * ___writer_settings_1;
	// System.String System.ServiceModel.Channels.MessageHeader::default_actor
	String_t* ___default_actor_2;
	// System.Boolean System.ServiceModel.Channels.MessageHeader::default_is_ref
	bool ___default_is_ref_3;
	// System.Boolean System.ServiceModel.Channels.MessageHeader::default_must_understand
	bool ___default_must_understand_4;
	// System.Boolean System.ServiceModel.Channels.MessageHeader::default_relay
	bool ___default_relay_5;

public:
	inline static int32_t get_offset_of_writer_settings_1() { return static_cast<int32_t>(offsetof(MessageHeader_t831925271_StaticFields, ___writer_settings_1)); }
	inline XmlWriterSettings_t3314986516 * get_writer_settings_1() const { return ___writer_settings_1; }
	inline XmlWriterSettings_t3314986516 ** get_address_of_writer_settings_1() { return &___writer_settings_1; }
	inline void set_writer_settings_1(XmlWriterSettings_t3314986516 * value)
	{
		___writer_settings_1 = value;
		Il2CppCodeGenWriteBarrier(&___writer_settings_1, value);
	}

	inline static int32_t get_offset_of_default_actor_2() { return static_cast<int32_t>(offsetof(MessageHeader_t831925271_StaticFields, ___default_actor_2)); }
	inline String_t* get_default_actor_2() const { return ___default_actor_2; }
	inline String_t** get_address_of_default_actor_2() { return &___default_actor_2; }
	inline void set_default_actor_2(String_t* value)
	{
		___default_actor_2 = value;
		Il2CppCodeGenWriteBarrier(&___default_actor_2, value);
	}

	inline static int32_t get_offset_of_default_is_ref_3() { return static_cast<int32_t>(offsetof(MessageHeader_t831925271_StaticFields, ___default_is_ref_3)); }
	inline bool get_default_is_ref_3() const { return ___default_is_ref_3; }
	inline bool* get_address_of_default_is_ref_3() { return &___default_is_ref_3; }
	inline void set_default_is_ref_3(bool value)
	{
		___default_is_ref_3 = value;
	}

	inline static int32_t get_offset_of_default_must_understand_4() { return static_cast<int32_t>(offsetof(MessageHeader_t831925271_StaticFields, ___default_must_understand_4)); }
	inline bool get_default_must_understand_4() const { return ___default_must_understand_4; }
	inline bool* get_address_of_default_must_understand_4() { return &___default_must_understand_4; }
	inline void set_default_must_understand_4(bool value)
	{
		___default_must_understand_4 = value;
	}

	inline static int32_t get_offset_of_default_relay_5() { return static_cast<int32_t>(offsetof(MessageHeader_t831925271_StaticFields, ___default_relay_5)); }
	inline bool get_default_relay_5() const { return ___default_relay_5; }
	inline bool* get_address_of_default_relay_5() { return &___default_relay_5; }
	inline void set_default_relay_5(bool value)
	{
		___default_relay_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
