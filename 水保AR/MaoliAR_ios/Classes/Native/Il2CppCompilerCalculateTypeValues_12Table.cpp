﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherSui3316559455.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientCon2797401965.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientRec2031137796.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientSes1775821398.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientSes2353595803.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ContentTy2602934270.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Context3971234707.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ExchangeA1320888206.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_HandshakeS756684113.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_HashAlgor2376832258.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_HttpsClie1160552561.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RecordPro3759049701.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RecordPro3680907657.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RecordPro3718352467.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RSASslSig3558097625.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RSASslSig2709678514.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityC4242483129.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityP2199972650.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityP1513093309.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ServerCon3848440993.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ServerRec3783944360.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Validatio3834298736.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslClient3914624661.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslCipher1981645747.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslHandsh2107581772.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslServerS875102504.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslStream1667413407.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslStream3504282820.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsCipher1545013223.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsClient2486039503.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsExcept3534743363.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsServer4144396432.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsStream2365453965.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake1004704908.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3696583168.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3062346172.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3519510577.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake1824902654.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2486981163.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_C97965998.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_643923608.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2716496392.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3690397592.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3860330041.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3343859594.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake1850379324.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_699469151.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_501187049.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_826355380.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2156931731.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2998762695.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_507150716.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_S47429503.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2184566575.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3563249236.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3527482625.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_253764541.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake1048339864.h"
#include "Mono_Security_Mono_Math_Prime_PrimalityTest1539325943.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Certifica4091668218.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Certifica1842476440.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Certifica3743405224.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_PrivateKe3240194217.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E3057255361.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U2732071528.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U1929481982.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U1704471045.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3652892010.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U1337922363.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U2499776625.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U2490092596.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3254766644.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U1630999355.h"
#include "System_Configuration_U3CModuleU3E692745525.h"
#include "System_Configuration_System_Configuration_Provider2594774949.h"
#include "System_Configuration_System_Configuration_Provider3810574769.h"
#include "System_Configuration_System_Configuration_Callback1110469826.h"
#include "System_Configuration_System_Configuration_ClientCo3254701293.h"
#include "System_Configuration_System_Configuration_CommaDel2116787517.h"
#include "System_Configuration_System_Configuration_CommaDel2027889819.h"
#include "System_Configuration_System_Configuration_ConfigNa1573003828.h"
#include "System_Configuration_System_Configuration_ConfigIn2364721456.h"
#include "System_Configuration_System_Configuration_Configur2529364143.h"
#include "System_Configuration_System_Configuration_Configur1879228320.h"
#include "System_Configuration_System_Configuration_Configur2804732269.h"
#include "System_Configuration_System_Configuration_Configur3695308734.h"
#include "System_Configuration_System_Configuration_Configura770649384.h"
#include "System_Configuration_System_Configuration_Configur3318566633.h"
#include "System_Configuration_System_Configuration_ElementM2160633803.h"
#include "System_Configuration_System_Configuration_Configura446763386.h"
#include "System_Configuration_System_Configuration_Configur1327025031.h"
#include "System_Configuration_System_Configuration_Configur2560831360.h"
#include "System_Configuration_System_Configuration_Configura939439970.h"
#include "System_Configuration_System_Configuration_Configurat66512296.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1200 = { sizeof (CipherSuiteFactory_t3316559455), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1201 = { sizeof (ClientContext_t2797401965), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1201[2] = 
{
	ClientContext_t2797401965::get_offset_of_sslStream_30(),
	ClientContext_t2797401965::get_offset_of_clientHelloProtocol_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1202 = { sizeof (ClientRecordProtocol_t2031137796), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1203 = { sizeof (ClientSessionInfo_t1775821398), -1, sizeof(ClientSessionInfo_t1775821398_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1203[6] = 
{
	ClientSessionInfo_t1775821398_StaticFields::get_offset_of_ValidityInterval_0(),
	ClientSessionInfo_t1775821398::get_offset_of_disposed_1(),
	ClientSessionInfo_t1775821398::get_offset_of_validuntil_2(),
	ClientSessionInfo_t1775821398::get_offset_of_host_3(),
	ClientSessionInfo_t1775821398::get_offset_of_sid_4(),
	ClientSessionInfo_t1775821398::get_offset_of_masterSecret_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1204 = { sizeof (ClientSessionCache_t2353595803), -1, sizeof(ClientSessionCache_t2353595803_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1204[2] = 
{
	ClientSessionCache_t2353595803_StaticFields::get_offset_of_cache_0(),
	ClientSessionCache_t2353595803_StaticFields::get_offset_of_locker_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1205 = { sizeof (ContentType_t2602934270)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1205[5] = 
{
	ContentType_t2602934270::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1206 = { sizeof (Context_t3971234707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1206[30] = 
{
	Context_t3971234707::get_offset_of_securityProtocol_0(),
	Context_t3971234707::get_offset_of_sessionId_1(),
	Context_t3971234707::get_offset_of_compressionMethod_2(),
	Context_t3971234707::get_offset_of_serverSettings_3(),
	Context_t3971234707::get_offset_of_clientSettings_4(),
	Context_t3971234707::get_offset_of_current_5(),
	Context_t3971234707::get_offset_of_negotiating_6(),
	Context_t3971234707::get_offset_of_read_7(),
	Context_t3971234707::get_offset_of_write_8(),
	Context_t3971234707::get_offset_of_supportedCiphers_9(),
	Context_t3971234707::get_offset_of_lastHandshakeMsg_10(),
	Context_t3971234707::get_offset_of_handshakeState_11(),
	Context_t3971234707::get_offset_of_abbreviatedHandshake_12(),
	Context_t3971234707::get_offset_of_receivedConnectionEnd_13(),
	Context_t3971234707::get_offset_of_sentConnectionEnd_14(),
	Context_t3971234707::get_offset_of_protocolNegotiated_15(),
	Context_t3971234707::get_offset_of_writeSequenceNumber_16(),
	Context_t3971234707::get_offset_of_readSequenceNumber_17(),
	Context_t3971234707::get_offset_of_clientRandom_18(),
	Context_t3971234707::get_offset_of_serverRandom_19(),
	Context_t3971234707::get_offset_of_randomCS_20(),
	Context_t3971234707::get_offset_of_randomSC_21(),
	Context_t3971234707::get_offset_of_masterSecret_22(),
	Context_t3971234707::get_offset_of_clientWriteKey_23(),
	Context_t3971234707::get_offset_of_serverWriteKey_24(),
	Context_t3971234707::get_offset_of_clientWriteIV_25(),
	Context_t3971234707::get_offset_of_serverWriteIV_26(),
	Context_t3971234707::get_offset_of_handshakeMessages_27(),
	Context_t3971234707::get_offset_of_random_28(),
	Context_t3971234707::get_offset_of_recordProtocol_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1207 = { sizeof (ExchangeAlgorithmType_t1320888206)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1207[6] = 
{
	ExchangeAlgorithmType_t1320888206::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1208 = { sizeof (HandshakeState_t756684113)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1208[4] = 
{
	HandshakeState_t756684113::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1209 = { sizeof (HashAlgorithmType_t2376832258)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1209[4] = 
{
	HashAlgorithmType_t2376832258::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1210 = { sizeof (HttpsClientStream_t1160552561), -1, sizeof(HttpsClientStream_t1160552561_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1210[4] = 
{
	HttpsClientStream_t1160552561::get_offset_of__request_21(),
	HttpsClientStream_t1160552561::get_offset_of__status_22(),
	HttpsClientStream_t1160552561_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_23(),
	HttpsClientStream_t1160552561_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1211 = { sizeof (RecordProtocol_t3759049701), -1, sizeof(RecordProtocol_t3759049701_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1211[3] = 
{
	RecordProtocol_t3759049701_StaticFields::get_offset_of_record_processing_0(),
	RecordProtocol_t3759049701::get_offset_of_innerStream_1(),
	RecordProtocol_t3759049701::get_offset_of_context_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1212 = { sizeof (ReceiveRecordAsyncResult_t3680907657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1212[9] = 
{
	ReceiveRecordAsyncResult_t3680907657::get_offset_of_locker_0(),
	ReceiveRecordAsyncResult_t3680907657::get_offset_of__userCallback_1(),
	ReceiveRecordAsyncResult_t3680907657::get_offset_of__userState_2(),
	ReceiveRecordAsyncResult_t3680907657::get_offset_of__asyncException_3(),
	ReceiveRecordAsyncResult_t3680907657::get_offset_of_handle_4(),
	ReceiveRecordAsyncResult_t3680907657::get_offset_of__resultingBuffer_5(),
	ReceiveRecordAsyncResult_t3680907657::get_offset_of__record_6(),
	ReceiveRecordAsyncResult_t3680907657::get_offset_of_completed_7(),
	ReceiveRecordAsyncResult_t3680907657::get_offset_of__initialBuffer_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1213 = { sizeof (SendRecordAsyncResult_t3718352467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1213[7] = 
{
	SendRecordAsyncResult_t3718352467::get_offset_of_locker_0(),
	SendRecordAsyncResult_t3718352467::get_offset_of__userCallback_1(),
	SendRecordAsyncResult_t3718352467::get_offset_of__userState_2(),
	SendRecordAsyncResult_t3718352467::get_offset_of__asyncException_3(),
	SendRecordAsyncResult_t3718352467::get_offset_of_handle_4(),
	SendRecordAsyncResult_t3718352467::get_offset_of__message_5(),
	SendRecordAsyncResult_t3718352467::get_offset_of_completed_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1214 = { sizeof (RSASslSignatureDeformatter_t3558097625), -1, sizeof(RSASslSignatureDeformatter_t3558097625_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1214[3] = 
{
	RSASslSignatureDeformatter_t3558097625::get_offset_of_key_0(),
	RSASslSignatureDeformatter_t3558097625::get_offset_of_hash_1(),
	RSASslSignatureDeformatter_t3558097625_StaticFields::get_offset_of_U3CU3Ef__switchU24map15_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1215 = { sizeof (RSASslSignatureFormatter_t2709678514), -1, sizeof(RSASslSignatureFormatter_t2709678514_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1215[3] = 
{
	RSASslSignatureFormatter_t2709678514::get_offset_of_key_0(),
	RSASslSignatureFormatter_t2709678514::get_offset_of_hash_1(),
	RSASslSignatureFormatter_t2709678514_StaticFields::get_offset_of_U3CU3Ef__switchU24map16_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1216 = { sizeof (SecurityCompressionType_t4242483129)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1216[3] = 
{
	SecurityCompressionType_t4242483129::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1217 = { sizeof (SecurityParameters_t2199972650), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1217[3] = 
{
	SecurityParameters_t2199972650::get_offset_of_cipher_0(),
	SecurityParameters_t2199972650::get_offset_of_clientWriteMAC_1(),
	SecurityParameters_t2199972650::get_offset_of_serverWriteMAC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1218 = { sizeof (SecurityProtocolType_t1513093309)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1218[5] = 
{
	SecurityProtocolType_t1513093309::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1219 = { sizeof (ServerContext_t3848440993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1219[3] = 
{
	ServerContext_t3848440993::get_offset_of_sslStream_30(),
	ServerContext_t3848440993::get_offset_of_request_client_certificate_31(),
	ServerContext_t3848440993::get_offset_of_clientCertificateRequired_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1220 = { sizeof (ServerRecordProtocol_t3783944360), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1221 = { sizeof (ValidationResult_t3834298736), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1221[3] = 
{
	ValidationResult_t3834298736::get_offset_of_trusted_0(),
	ValidationResult_t3834298736::get_offset_of_user_denied_1(),
	ValidationResult_t3834298736::get_offset_of_error_code_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1222 = { sizeof (SslClientStream_t3914624661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1222[4] = 
{
	SslClientStream_t3914624661::get_offset_of_ServerCertValidation_17(),
	SslClientStream_t3914624661::get_offset_of_ClientCertSelection_18(),
	SslClientStream_t3914624661::get_offset_of_PrivateKeySelection_19(),
	SslClientStream_t3914624661::get_offset_of_ServerCertValidation2_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1223 = { sizeof (SslCipherSuite_t1981645747), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1223[3] = 
{
	SslCipherSuite_t1981645747::get_offset_of_pad1_21(),
	SslCipherSuite_t1981645747::get_offset_of_pad2_22(),
	SslCipherSuite_t1981645747::get_offset_of_header_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1224 = { sizeof (SslHandshakeHash_t2107581772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1224[8] = 
{
	SslHandshakeHash_t2107581772::get_offset_of_md5_4(),
	SslHandshakeHash_t2107581772::get_offset_of_sha_5(),
	SslHandshakeHash_t2107581772::get_offset_of_hashing_6(),
	SslHandshakeHash_t2107581772::get_offset_of_secret_7(),
	SslHandshakeHash_t2107581772::get_offset_of_innerPadMD5_8(),
	SslHandshakeHash_t2107581772::get_offset_of_outerPadMD5_9(),
	SslHandshakeHash_t2107581772::get_offset_of_innerPadSHA_10(),
	SslHandshakeHash_t2107581772::get_offset_of_outerPadSHA_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1225 = { sizeof (SslServerStream_t875102504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1225[3] = 
{
	SslServerStream_t875102504::get_offset_of_ClientCertValidation_17(),
	SslServerStream_t875102504::get_offset_of_PrivateKeySelection_18(),
	SslServerStream_t875102504::get_offset_of_ClientCertValidation2_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1226 = { sizeof (SslStreamBase_t1667413407), -1, sizeof(SslStreamBase_t1667413407_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1226[15] = 
{
	0,
	SslStreamBase_t1667413407_StaticFields::get_offset_of_record_processing_3(),
	SslStreamBase_t1667413407::get_offset_of_innerStream_4(),
	SslStreamBase_t1667413407::get_offset_of_inputBuffer_5(),
	SslStreamBase_t1667413407::get_offset_of_context_6(),
	SslStreamBase_t1667413407::get_offset_of_protocol_7(),
	SslStreamBase_t1667413407::get_offset_of_ownsStream_8(),
	SslStreamBase_t1667413407::get_offset_of_disposed_9(),
	SslStreamBase_t1667413407::get_offset_of_checkCertRevocationStatus_10(),
	SslStreamBase_t1667413407::get_offset_of_negotiate_11(),
	SslStreamBase_t1667413407::get_offset_of_read_12(),
	SslStreamBase_t1667413407::get_offset_of_write_13(),
	SslStreamBase_t1667413407::get_offset_of_negotiationComplete_14(),
	SslStreamBase_t1667413407::get_offset_of_recbuf_15(),
	SslStreamBase_t1667413407::get_offset_of_recordStream_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1227 = { sizeof (InternalAsyncResult_t3504282820), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1227[12] = 
{
	InternalAsyncResult_t3504282820::get_offset_of_locker_0(),
	InternalAsyncResult_t3504282820::get_offset_of__userCallback_1(),
	InternalAsyncResult_t3504282820::get_offset_of__userState_2(),
	InternalAsyncResult_t3504282820::get_offset_of__asyncException_3(),
	InternalAsyncResult_t3504282820::get_offset_of_handle_4(),
	InternalAsyncResult_t3504282820::get_offset_of_completed_5(),
	InternalAsyncResult_t3504282820::get_offset_of__bytesRead_6(),
	InternalAsyncResult_t3504282820::get_offset_of__fromWrite_7(),
	InternalAsyncResult_t3504282820::get_offset_of__proceedAfterHandshake_8(),
	InternalAsyncResult_t3504282820::get_offset_of__buffer_9(),
	InternalAsyncResult_t3504282820::get_offset_of__offset_10(),
	InternalAsyncResult_t3504282820::get_offset_of__count_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1228 = { sizeof (TlsCipherSuite_t1545013223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1228[2] = 
{
	TlsCipherSuite_t1545013223::get_offset_of_header_21(),
	TlsCipherSuite_t1545013223::get_offset_of_headerLock_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1229 = { sizeof (TlsClientSettings_t2486039503), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1229[4] = 
{
	TlsClientSettings_t2486039503::get_offset_of_targetHost_0(),
	TlsClientSettings_t2486039503::get_offset_of_certificates_1(),
	TlsClientSettings_t2486039503::get_offset_of_clientCertificate_2(),
	TlsClientSettings_t2486039503::get_offset_of_certificateRSA_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1230 = { sizeof (TlsException_t3534743363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1230[1] = 
{
	TlsException_t3534743363::get_offset_of_alert_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1231 = { sizeof (TlsServerSettings_t4144396432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1231[8] = 
{
	TlsServerSettings_t4144396432::get_offset_of_certificates_0(),
	TlsServerSettings_t4144396432::get_offset_of_certificateRSA_1(),
	TlsServerSettings_t4144396432::get_offset_of_rsaParameters_2(),
	TlsServerSettings_t4144396432::get_offset_of_signedParams_3(),
	TlsServerSettings_t4144396432::get_offset_of_distinguisedNames_4(),
	TlsServerSettings_t4144396432::get_offset_of_serverKeyExchange_5(),
	TlsServerSettings_t4144396432::get_offset_of_certificateRequest_6(),
	TlsServerSettings_t4144396432::get_offset_of_certificateTypes_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1232 = { sizeof (TlsStream_t2365453965), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1232[4] = 
{
	TlsStream_t2365453965::get_offset_of_canRead_2(),
	TlsStream_t2365453965::get_offset_of_canWrite_3(),
	TlsStream_t2365453965::get_offset_of_buffer_4(),
	TlsStream_t2365453965::get_offset_of_temp_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1233 = { sizeof (ClientCertificateType_t1004704908)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1233[6] = 
{
	ClientCertificateType_t1004704908::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1234 = { sizeof (HandshakeMessage_t3696583168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1234[4] = 
{
	HandshakeMessage_t3696583168::get_offset_of_context_6(),
	HandshakeMessage_t3696583168::get_offset_of_handshakeType_7(),
	HandshakeMessage_t3696583168::get_offset_of_contentType_8(),
	HandshakeMessage_t3696583168::get_offset_of_cache_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1235 = { sizeof (HandshakeType_t3062346172)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1235[12] = 
{
	HandshakeType_t3062346172::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1236 = { sizeof (TlsClientCertificate_t3519510577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1236[2] = 
{
	TlsClientCertificate_t3519510577::get_offset_of_clientCertSelected_10(),
	TlsClientCertificate_t3519510577::get_offset_of_clientCert_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1237 = { sizeof (TlsClientCertificateVerify_t1824902654), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1238 = { sizeof (TlsClientFinished_t2486981163), -1, sizeof(TlsClientFinished_t2486981163_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1238[1] = 
{
	TlsClientFinished_t2486981163_StaticFields::get_offset_of_Ssl3Marker_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1239 = { sizeof (TlsClientHello_t97965998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1239[1] = 
{
	TlsClientHello_t97965998::get_offset_of_random_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1240 = { sizeof (TlsClientKeyExchange_t643923608), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1241 = { sizeof (TlsServerCertificate_t2716496392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1241[1] = 
{
	TlsServerCertificate_t2716496392::get_offset_of_certificates_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1242 = { sizeof (TlsServerCertificateRequest_t3690397592), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1242[2] = 
{
	TlsServerCertificateRequest_t3690397592::get_offset_of_certificateTypes_10(),
	TlsServerCertificateRequest_t3690397592::get_offset_of_distinguisedNames_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1243 = { sizeof (TlsServerFinished_t3860330041), -1, sizeof(TlsServerFinished_t3860330041_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1243[1] = 
{
	TlsServerFinished_t3860330041_StaticFields::get_offset_of_Ssl3Marker_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1244 = { sizeof (TlsServerHello_t3343859594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1244[4] = 
{
	TlsServerHello_t3343859594::get_offset_of_compressionMethod_10(),
	TlsServerHello_t3343859594::get_offset_of_random_11(),
	TlsServerHello_t3343859594::get_offset_of_sessionId_12(),
	TlsServerHello_t3343859594::get_offset_of_cipherSuite_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1245 = { sizeof (TlsServerHelloDone_t1850379324), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1246 = { sizeof (TlsServerKeyExchange_t699469151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1246[2] = 
{
	TlsServerKeyExchange_t699469151::get_offset_of_rsaParams_10(),
	TlsServerKeyExchange_t699469151::get_offset_of_signedParams_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1247 = { sizeof (TlsClientCertificate_t501187049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1247[1] = 
{
	TlsClientCertificate_t501187049::get_offset_of_clientCertificates_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1248 = { sizeof (TlsClientCertificateVerify_t826355380), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1249 = { sizeof (TlsClientFinished_t2156931731), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1250 = { sizeof (TlsClientHello_t2998762695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1250[4] = 
{
	TlsClientHello_t2998762695::get_offset_of_random_10(),
	TlsClientHello_t2998762695::get_offset_of_sessionId_11(),
	TlsClientHello_t2998762695::get_offset_of_cipherSuites_12(),
	TlsClientHello_t2998762695::get_offset_of_compressionMethods_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1251 = { sizeof (TlsClientKeyExchange_t507150716), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1252 = { sizeof (TlsServerCertificate_t47429503), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1253 = { sizeof (TlsServerCertificateRequest_t2184566575), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1254 = { sizeof (TlsServerFinished_t3563249236), -1, sizeof(TlsServerFinished_t3563249236_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1254[1] = 
{
	TlsServerFinished_t3563249236_StaticFields::get_offset_of_Ssl3Marker_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1255 = { sizeof (TlsServerHello_t3527482625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1255[2] = 
{
	TlsServerHello_t3527482625::get_offset_of_unixTime_10(),
	TlsServerHello_t3527482625::get_offset_of_random_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1256 = { sizeof (TlsServerHelloDone_t253764541), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1257 = { sizeof (TlsServerKeyExchange_t1048339864), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1258 = { sizeof (PrimalityTest_t1539325944), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1259 = { sizeof (CertificateValidationCallback_t4091668218), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1260 = { sizeof (CertificateValidationCallback2_t1842476440), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1261 = { sizeof (CertificateSelectionCallback_t3743405224), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1262 = { sizeof (PrivateKeySelectionCallback_t3240194217), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1263 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255362), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1263[16] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U24U24fieldU2D5_1(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U24U24fieldU2D6_2(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U24U24fieldU2D7_3(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U24U24fieldU2D8_4(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U24U24fieldU2D9_5(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U24U24fieldU2D11_6(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U24U24fieldU2D12_7(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U24U24fieldU2D13_8(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U24U24fieldU2D14_9(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U24U24fieldU2D15_10(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U24U24fieldU2D16_11(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U24U24fieldU2D17_12(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U24U24fieldU2D21_13(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U24U24fieldU2D22_14(),
	U3CPrivateImplementationDetailsU3E_t3057255362_StaticFields::get_offset_of_U24U24fieldU2D23_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1264 = { sizeof (U24ArrayTypeU243132_t2732071529)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU243132_t2732071529 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1265 = { sizeof (U24ArrayTypeU24256_t1929481983)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t1929481983 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1266 = { sizeof (U24ArrayTypeU2420_t1704471046)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2420_t1704471046 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1267 = { sizeof (U24ArrayTypeU2432_t3652892011)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2432_t3652892011 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1268 = { sizeof (U24ArrayTypeU2448_t1337922364)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2448_t1337922364 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1269 = { sizeof (U24ArrayTypeU2464_t499776626)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2464_t499776626 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1270 = { sizeof (U24ArrayTypeU2412_t2490092597)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t2490092597 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1271 = { sizeof (U24ArrayTypeU2416_t3254766645)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2416_t3254766645 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1272 = { sizeof (U24ArrayTypeU244_t1630999355)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU244_t1630999355 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1273 = { sizeof (U3CModuleU3E_t692745527), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1274 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1275 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1276 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1277 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1278 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1279 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1280 = { sizeof (ProviderBase_t2594774949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1280[3] = 
{
	ProviderBase_t2594774949::get_offset_of_alreadyInitialized_0(),
	ProviderBase_t2594774949::get_offset_of__description_1(),
	ProviderBase_t2594774949::get_offset_of__name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1281 = { sizeof (ProviderCollection_t3810574769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1281[3] = 
{
	ProviderCollection_t3810574769::get_offset_of_lookup_0(),
	ProviderCollection_t3810574769::get_offset_of_readOnly_1(),
	ProviderCollection_t3810574769::get_offset_of_values_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1282 = { sizeof (CallbackValidator_t1110469826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1282[2] = 
{
	CallbackValidator_t1110469826::get_offset_of_type_0(),
	CallbackValidator_t1110469826::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1283 = { sizeof (ClientConfigurationSystem_t3254701293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1283[1] = 
{
	ClientConfigurationSystem_t3254701293::get_offset_of_cfg_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1284 = { sizeof (CommaDelimitedStringCollection_t2116787517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1284[2] = 
{
	CommaDelimitedStringCollection_t2116787517::get_offset_of_modified_1(),
	CommaDelimitedStringCollection_t2116787517::get_offset_of_readOnly_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1285 = { sizeof (CommaDelimitedStringCollectionConverter_t2027889819), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1286 = { sizeof (ConfigNameValueCollection_t1573003828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1286[1] = 
{
	ConfigNameValueCollection_t1573003828::get_offset_of_modified_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1287 = { sizeof (ConfigInfo_t2364721456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1287[6] = 
{
	ConfigInfo_t2364721456::get_offset_of_Name_0(),
	ConfigInfo_t2364721456::get_offset_of_TypeName_1(),
	ConfigInfo_t2364721456::get_offset_of_Type_2(),
	ConfigInfo_t2364721456::get_offset_of_streamName_3(),
	ConfigInfo_t2364721456::get_offset_of_Parent_4(),
	ConfigInfo_t2364721456::get_offset_of_ConfigHost_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1288 = { sizeof (Configuration_t2529364143), -1, sizeof(Configuration_t2529364143_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1288[15] = 
{
	Configuration_t2529364143::get_offset_of_parent_0(),
	Configuration_t2529364143::get_offset_of_elementData_1(),
	Configuration_t2529364143::get_offset_of_streamName_2(),
	Configuration_t2529364143::get_offset_of_rootSectionGroup_3(),
	Configuration_t2529364143::get_offset_of_locations_4(),
	Configuration_t2529364143::get_offset_of_rootGroup_5(),
	Configuration_t2529364143::get_offset_of_system_6(),
	Configuration_t2529364143::get_offset_of_hasFile_7(),
	Configuration_t2529364143::get_offset_of_rootNamespace_8(),
	Configuration_t2529364143::get_offset_of_configPath_9(),
	Configuration_t2529364143::get_offset_of_locationConfigPath_10(),
	Configuration_t2529364143::get_offset_of_locationSubPath_11(),
	Configuration_t2529364143::get_offset_of_evaluationContext_12(),
	Configuration_t2529364143_StaticFields::get_offset_of_SaveStart_13(),
	Configuration_t2529364143_StaticFields::get_offset_of_SaveEnd_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1289 = { sizeof (ConfigurationAllowDefinition_t1879228320)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1289[5] = 
{
	ConfigurationAllowDefinition_t1879228320::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1290 = { sizeof (ConfigurationAllowExeDefinition_t2804732269)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1290[5] = 
{
	ConfigurationAllowExeDefinition_t2804732269::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1291 = { sizeof (ConfigurationCollectionAttribute_t3695308734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1291[5] = 
{
	ConfigurationCollectionAttribute_t3695308734::get_offset_of_addItemName_0(),
	ConfigurationCollectionAttribute_t3695308734::get_offset_of_clearItemsName_1(),
	ConfigurationCollectionAttribute_t3695308734::get_offset_of_removeItemName_2(),
	ConfigurationCollectionAttribute_t3695308734::get_offset_of_collectionType_3(),
	ConfigurationCollectionAttribute_t3695308734::get_offset_of_itemType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1292 = { sizeof (ConfigurationConverterBase_t770649384), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1293 = { sizeof (ConfigurationElement_t3318566633), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1293[13] = 
{
	ConfigurationElement_t3318566633::get_offset_of_rawXml_0(),
	ConfigurationElement_t3318566633::get_offset_of_modified_1(),
	ConfigurationElement_t3318566633::get_offset_of_map_2(),
	ConfigurationElement_t3318566633::get_offset_of_keyProps_3(),
	ConfigurationElement_t3318566633::get_offset_of_defaultCollection_4(),
	ConfigurationElement_t3318566633::get_offset_of_readOnly_5(),
	ConfigurationElement_t3318566633::get_offset_of_elementInfo_6(),
	ConfigurationElement_t3318566633::get_offset_of__configuration_7(),
	ConfigurationElement_t3318566633::get_offset_of_lockAllAttributesExcept_8(),
	ConfigurationElement_t3318566633::get_offset_of_lockAllElementsExcept_9(),
	ConfigurationElement_t3318566633::get_offset_of_lockAttributes_10(),
	ConfigurationElement_t3318566633::get_offset_of_lockElements_11(),
	ConfigurationElement_t3318566633::get_offset_of_lockItem_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1294 = { sizeof (ElementMap_t2160633803), -1, sizeof(ElementMap_t2160633803_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1294[3] = 
{
	ElementMap_t2160633803_StaticFields::get_offset_of_elementMaps_0(),
	ElementMap_t2160633803::get_offset_of_properties_1(),
	ElementMap_t2160633803::get_offset_of_collectionAttribute_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1295 = { sizeof (ConfigurationElementCollection_t446763386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1295[10] = 
{
	ConfigurationElementCollection_t446763386::get_offset_of_list_13(),
	ConfigurationElementCollection_t446763386::get_offset_of_removed_14(),
	ConfigurationElementCollection_t446763386::get_offset_of_inherited_15(),
	ConfigurationElementCollection_t446763386::get_offset_of_emitClear_16(),
	ConfigurationElementCollection_t446763386::get_offset_of_modified_17(),
	ConfigurationElementCollection_t446763386::get_offset_of_comparer_18(),
	ConfigurationElementCollection_t446763386::get_offset_of_inheritedLimitIndex_19(),
	ConfigurationElementCollection_t446763386::get_offset_of_addElementName_20(),
	ConfigurationElementCollection_t446763386::get_offset_of_clearElementName_21(),
	ConfigurationElementCollection_t446763386::get_offset_of_removeElementName_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1296 = { sizeof (ConfigurationRemoveElement_t1327025031), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1296[3] = 
{
	ConfigurationRemoveElement_t1327025031::get_offset_of_properties_13(),
	ConfigurationRemoveElement_t1327025031::get_offset_of__origElement_14(),
	ConfigurationRemoveElement_t1327025031::get_offset_of__origCollection_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1297 = { sizeof (ConfigurationElementCollectionType_t2560831360)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1297[5] = 
{
	ConfigurationElementCollectionType_t2560831360::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1298 = { sizeof (ConfigurationElementProperty_t939439970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1298[1] = 
{
	ConfigurationElementProperty_t939439970::get_offset_of_validator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1299 = { sizeof (ConfigurationErrorsException_t66512296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1299[2] = 
{
	ConfigurationErrorsException_t66512296::get_offset_of_filename_13(),
	ConfigurationErrorsException_t66512296::get_offset_of_line_14(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
