﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.Collections.Specialized.StringDictionary
struct StringDictionary_t120437468;
// System.Char[]
struct CharU5BU5D_t3528271667;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Mime.ContentType
struct  ContentType_t768484892  : public Il2CppObject
{
public:
	// System.String System.Net.Mime.ContentType::mediaType
	String_t* ___mediaType_0;
	// System.Collections.Specialized.StringDictionary System.Net.Mime.ContentType::parameters
	StringDictionary_t120437468 * ___parameters_1;

public:
	inline static int32_t get_offset_of_mediaType_0() { return static_cast<int32_t>(offsetof(ContentType_t768484892, ___mediaType_0)); }
	inline String_t* get_mediaType_0() const { return ___mediaType_0; }
	inline String_t** get_address_of_mediaType_0() { return &___mediaType_0; }
	inline void set_mediaType_0(String_t* value)
	{
		___mediaType_0 = value;
		Il2CppCodeGenWriteBarrier(&___mediaType_0, value);
	}

	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(ContentType_t768484892, ___parameters_1)); }
	inline StringDictionary_t120437468 * get_parameters_1() const { return ___parameters_1; }
	inline StringDictionary_t120437468 ** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(StringDictionary_t120437468 * value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_1, value);
	}
};

struct ContentType_t768484892_StaticFields
{
public:
	// System.Char[] System.Net.Mime.ContentType::especials
	CharU5BU5D_t3528271667* ___especials_2;

public:
	inline static int32_t get_offset_of_especials_2() { return static_cast<int32_t>(offsetof(ContentType_t768484892_StaticFields, ___especials_2)); }
	inline CharU5BU5D_t3528271667* get_especials_2() const { return ___especials_2; }
	inline CharU5BU5D_t3528271667** get_address_of_especials_2() { return &___especials_2; }
	inline void set_especials_2(CharU5BU5D_t3528271667* value)
	{
		___especials_2 = value;
		Il2CppCodeGenWriteBarrier(&___especials_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
