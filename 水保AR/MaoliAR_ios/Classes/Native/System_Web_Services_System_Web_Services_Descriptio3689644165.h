﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_Services_System_Web_Services_Descriptio3850760970.h"

// System.Web.Services.Description.HttpBinding
struct HttpBinding_t1636923877;
// System.Xml.Serialization.SoapSchemaImporter
struct SoapSchemaImporter_t1895423347;
// System.Xml.Serialization.XmlCodeExporter
struct XmlCodeExporter_t4164579413;
// System.Xml.Serialization.XmlSchemaImporter
struct XmlSchemaImporter_t2852143304;
// System.Xml.Serialization.CodeIdentifiers
struct CodeIdentifiers_t4095039290;
// System.Xml.Serialization.XmlReflectionImporter
struct XmlReflectionImporter_t1777580912;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.HttpSimpleProtocolImporter
struct  HttpSimpleProtocolImporter_t3689644165  : public ProtocolImporter_t3850760970
{
public:
	// System.Web.Services.Description.HttpBinding System.Web.Services.Description.HttpSimpleProtocolImporter::httpBinding
	HttpBinding_t1636923877 * ___httpBinding_19;
	// System.Xml.Serialization.SoapSchemaImporter System.Web.Services.Description.HttpSimpleProtocolImporter::soapImporter
	SoapSchemaImporter_t1895423347 * ___soapImporter_20;
	// System.Xml.Serialization.XmlCodeExporter System.Web.Services.Description.HttpSimpleProtocolImporter::xmlExporter
	XmlCodeExporter_t4164579413 * ___xmlExporter_21;
	// System.Xml.Serialization.XmlSchemaImporter System.Web.Services.Description.HttpSimpleProtocolImporter::xmlImporter
	XmlSchemaImporter_t2852143304 * ___xmlImporter_22;
	// System.Xml.Serialization.CodeIdentifiers System.Web.Services.Description.HttpSimpleProtocolImporter::memberIds
	CodeIdentifiers_t4095039290 * ___memberIds_23;
	// System.Xml.Serialization.XmlReflectionImporter System.Web.Services.Description.HttpSimpleProtocolImporter::xmlReflectionImporter
	XmlReflectionImporter_t1777580912 * ___xmlReflectionImporter_24;

public:
	inline static int32_t get_offset_of_httpBinding_19() { return static_cast<int32_t>(offsetof(HttpSimpleProtocolImporter_t3689644165, ___httpBinding_19)); }
	inline HttpBinding_t1636923877 * get_httpBinding_19() const { return ___httpBinding_19; }
	inline HttpBinding_t1636923877 ** get_address_of_httpBinding_19() { return &___httpBinding_19; }
	inline void set_httpBinding_19(HttpBinding_t1636923877 * value)
	{
		___httpBinding_19 = value;
		Il2CppCodeGenWriteBarrier(&___httpBinding_19, value);
	}

	inline static int32_t get_offset_of_soapImporter_20() { return static_cast<int32_t>(offsetof(HttpSimpleProtocolImporter_t3689644165, ___soapImporter_20)); }
	inline SoapSchemaImporter_t1895423347 * get_soapImporter_20() const { return ___soapImporter_20; }
	inline SoapSchemaImporter_t1895423347 ** get_address_of_soapImporter_20() { return &___soapImporter_20; }
	inline void set_soapImporter_20(SoapSchemaImporter_t1895423347 * value)
	{
		___soapImporter_20 = value;
		Il2CppCodeGenWriteBarrier(&___soapImporter_20, value);
	}

	inline static int32_t get_offset_of_xmlExporter_21() { return static_cast<int32_t>(offsetof(HttpSimpleProtocolImporter_t3689644165, ___xmlExporter_21)); }
	inline XmlCodeExporter_t4164579413 * get_xmlExporter_21() const { return ___xmlExporter_21; }
	inline XmlCodeExporter_t4164579413 ** get_address_of_xmlExporter_21() { return &___xmlExporter_21; }
	inline void set_xmlExporter_21(XmlCodeExporter_t4164579413 * value)
	{
		___xmlExporter_21 = value;
		Il2CppCodeGenWriteBarrier(&___xmlExporter_21, value);
	}

	inline static int32_t get_offset_of_xmlImporter_22() { return static_cast<int32_t>(offsetof(HttpSimpleProtocolImporter_t3689644165, ___xmlImporter_22)); }
	inline XmlSchemaImporter_t2852143304 * get_xmlImporter_22() const { return ___xmlImporter_22; }
	inline XmlSchemaImporter_t2852143304 ** get_address_of_xmlImporter_22() { return &___xmlImporter_22; }
	inline void set_xmlImporter_22(XmlSchemaImporter_t2852143304 * value)
	{
		___xmlImporter_22 = value;
		Il2CppCodeGenWriteBarrier(&___xmlImporter_22, value);
	}

	inline static int32_t get_offset_of_memberIds_23() { return static_cast<int32_t>(offsetof(HttpSimpleProtocolImporter_t3689644165, ___memberIds_23)); }
	inline CodeIdentifiers_t4095039290 * get_memberIds_23() const { return ___memberIds_23; }
	inline CodeIdentifiers_t4095039290 ** get_address_of_memberIds_23() { return &___memberIds_23; }
	inline void set_memberIds_23(CodeIdentifiers_t4095039290 * value)
	{
		___memberIds_23 = value;
		Il2CppCodeGenWriteBarrier(&___memberIds_23, value);
	}

	inline static int32_t get_offset_of_xmlReflectionImporter_24() { return static_cast<int32_t>(offsetof(HttpSimpleProtocolImporter_t3689644165, ___xmlReflectionImporter_24)); }
	inline XmlReflectionImporter_t1777580912 * get_xmlReflectionImporter_24() const { return ___xmlReflectionImporter_24; }
	inline XmlReflectionImporter_t1777580912 ** get_address_of_xmlReflectionImporter_24() { return &___xmlReflectionImporter_24; }
	inline void set_xmlReflectionImporter_24(XmlReflectionImporter_t1777580912 * value)
	{
		___xmlReflectionImporter_24 = value;
		Il2CppCodeGenWriteBarrier(&___xmlReflectionImporter_24, value);
	}
};

struct HttpSimpleProtocolImporter_t3689644165_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Web.Services.Description.HttpSimpleProtocolImporter::<>f__switch$map0
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map0_25;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_25() { return static_cast<int32_t>(offsetof(HttpSimpleProtocolImporter_t3689644165_StaticFields, ___U3CU3Ef__switchU24map0_25)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map0_25() const { return ___U3CU3Ef__switchU24map0_25; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map0_25() { return &___U3CU3Ef__switchU24map0_25; }
	inline void set_U3CU3Ef__switchU24map0_25(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map0_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map0_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
