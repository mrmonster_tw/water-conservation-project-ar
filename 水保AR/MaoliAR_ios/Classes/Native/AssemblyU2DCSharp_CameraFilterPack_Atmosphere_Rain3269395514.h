﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Vector43319028937.h"

// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_Atmosphere_Rain_Pro
struct  CameraFilterPack_Atmosphere_Rain_Pro_t3269395514  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Shader CameraFilterPack_Atmosphere_Rain_Pro::SCShader
	Shader_t4151988712 * ___SCShader_2;
	// System.Single CameraFilterPack_Atmosphere_Rain_Pro::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Atmosphere_Rain_Pro::ScreenResolution
	Vector4_t3319028937  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Atmosphere_Rain_Pro::SCMaterial
	Material_t340375123 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Atmosphere_Rain_Pro::Fade
	float ___Fade_6;
	// System.Single CameraFilterPack_Atmosphere_Rain_Pro::Intensity
	float ___Intensity_7;
	// System.Single CameraFilterPack_Atmosphere_Rain_Pro::DirectionX
	float ___DirectionX_8;
	// System.Single CameraFilterPack_Atmosphere_Rain_Pro::Size
	float ___Size_9;
	// System.Single CameraFilterPack_Atmosphere_Rain_Pro::Speed
	float ___Speed_10;
	// System.Single CameraFilterPack_Atmosphere_Rain_Pro::Distortion
	float ___Distortion_11;
	// System.Single CameraFilterPack_Atmosphere_Rain_Pro::StormFlashOnOff
	float ___StormFlashOnOff_12;
	// System.Single CameraFilterPack_Atmosphere_Rain_Pro::DropOnOff
	float ___DropOnOff_13;
	// UnityEngine.Texture2D CameraFilterPack_Atmosphere_Rain_Pro::Texture2
	Texture2D_t3840446185 * ___Texture2_14;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_Atmosphere_Rain_Pro_t3269395514, ___SCShader_2)); }
	inline Shader_t4151988712 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t4151988712 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t4151988712 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_TimeX_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_Atmosphere_Rain_Pro_t3269395514, ___TimeX_3)); }
	inline float get_TimeX_3() const { return ___TimeX_3; }
	inline float* get_address_of_TimeX_3() { return &___TimeX_3; }
	inline void set_TimeX_3(float value)
	{
		___TimeX_3 = value;
	}

	inline static int32_t get_offset_of_ScreenResolution_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_Atmosphere_Rain_Pro_t3269395514, ___ScreenResolution_4)); }
	inline Vector4_t3319028937  get_ScreenResolution_4() const { return ___ScreenResolution_4; }
	inline Vector4_t3319028937 * get_address_of_ScreenResolution_4() { return &___ScreenResolution_4; }
	inline void set_ScreenResolution_4(Vector4_t3319028937  value)
	{
		___ScreenResolution_4 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_Atmosphere_Rain_Pro_t3269395514, ___SCMaterial_5)); }
	inline Material_t340375123 * get_SCMaterial_5() const { return ___SCMaterial_5; }
	inline Material_t340375123 ** get_address_of_SCMaterial_5() { return &___SCMaterial_5; }
	inline void set_SCMaterial_5(Material_t340375123 * value)
	{
		___SCMaterial_5 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_5, value);
	}

	inline static int32_t get_offset_of_Fade_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_Atmosphere_Rain_Pro_t3269395514, ___Fade_6)); }
	inline float get_Fade_6() const { return ___Fade_6; }
	inline float* get_address_of_Fade_6() { return &___Fade_6; }
	inline void set_Fade_6(float value)
	{
		___Fade_6 = value;
	}

	inline static int32_t get_offset_of_Intensity_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_Atmosphere_Rain_Pro_t3269395514, ___Intensity_7)); }
	inline float get_Intensity_7() const { return ___Intensity_7; }
	inline float* get_address_of_Intensity_7() { return &___Intensity_7; }
	inline void set_Intensity_7(float value)
	{
		___Intensity_7 = value;
	}

	inline static int32_t get_offset_of_DirectionX_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_Atmosphere_Rain_Pro_t3269395514, ___DirectionX_8)); }
	inline float get_DirectionX_8() const { return ___DirectionX_8; }
	inline float* get_address_of_DirectionX_8() { return &___DirectionX_8; }
	inline void set_DirectionX_8(float value)
	{
		___DirectionX_8 = value;
	}

	inline static int32_t get_offset_of_Size_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_Atmosphere_Rain_Pro_t3269395514, ___Size_9)); }
	inline float get_Size_9() const { return ___Size_9; }
	inline float* get_address_of_Size_9() { return &___Size_9; }
	inline void set_Size_9(float value)
	{
		___Size_9 = value;
	}

	inline static int32_t get_offset_of_Speed_10() { return static_cast<int32_t>(offsetof(CameraFilterPack_Atmosphere_Rain_Pro_t3269395514, ___Speed_10)); }
	inline float get_Speed_10() const { return ___Speed_10; }
	inline float* get_address_of_Speed_10() { return &___Speed_10; }
	inline void set_Speed_10(float value)
	{
		___Speed_10 = value;
	}

	inline static int32_t get_offset_of_Distortion_11() { return static_cast<int32_t>(offsetof(CameraFilterPack_Atmosphere_Rain_Pro_t3269395514, ___Distortion_11)); }
	inline float get_Distortion_11() const { return ___Distortion_11; }
	inline float* get_address_of_Distortion_11() { return &___Distortion_11; }
	inline void set_Distortion_11(float value)
	{
		___Distortion_11 = value;
	}

	inline static int32_t get_offset_of_StormFlashOnOff_12() { return static_cast<int32_t>(offsetof(CameraFilterPack_Atmosphere_Rain_Pro_t3269395514, ___StormFlashOnOff_12)); }
	inline float get_StormFlashOnOff_12() const { return ___StormFlashOnOff_12; }
	inline float* get_address_of_StormFlashOnOff_12() { return &___StormFlashOnOff_12; }
	inline void set_StormFlashOnOff_12(float value)
	{
		___StormFlashOnOff_12 = value;
	}

	inline static int32_t get_offset_of_DropOnOff_13() { return static_cast<int32_t>(offsetof(CameraFilterPack_Atmosphere_Rain_Pro_t3269395514, ___DropOnOff_13)); }
	inline float get_DropOnOff_13() const { return ___DropOnOff_13; }
	inline float* get_address_of_DropOnOff_13() { return &___DropOnOff_13; }
	inline void set_DropOnOff_13(float value)
	{
		___DropOnOff_13 = value;
	}

	inline static int32_t get_offset_of_Texture2_14() { return static_cast<int32_t>(offsetof(CameraFilterPack_Atmosphere_Rain_Pro_t3269395514, ___Texture2_14)); }
	inline Texture2D_t3840446185 * get_Texture2_14() const { return ___Texture2_14; }
	inline Texture2D_t3840446185 ** get_address_of_Texture2_14() { return &___Texture2_14; }
	inline void set_Texture2_14(Texture2D_t3840446185 * value)
	{
		___Texture2_14 = value;
		Il2CppCodeGenWriteBarrier(&___Texture2_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
