﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_MulticastDelegate157516450.h"
#include "mscorlib_System_Collections_DictionaryEntry3123975638.h"

// System.Xml.UniqueId
struct UniqueId_t1383576913;
// System.Collections.Generic.Dictionary`2<System.Xml.UniqueId,System.ServiceModel.Security.Tokens.SecurityContextSecurityToken>
struct Dictionary_2_t3105478127;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Transform`1<System.Xml.UniqueId,System.Collections.Generic.Dictionary`2<System.Xml.UniqueId,System.ServiceModel.Security.Tokens.SecurityContextSecurityToken>,System.Collections.DictionaryEntry>
struct  Transform_1_t902157514  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
