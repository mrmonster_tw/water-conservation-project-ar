﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Configurat1899919908.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.StandardBindingOptionalReliableSessionElement
struct  StandardBindingOptionalReliableSessionElement_t2857298057  : public StandardBindingReliableSessionElement_t1899919908
{
public:

public:
};

struct StandardBindingOptionalReliableSessionElement_t2857298057_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.StandardBindingOptionalReliableSessionElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_16;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.StandardBindingOptionalReliableSessionElement::enabled
	ConfigurationProperty_t3590861854 * ___enabled_17;

public:
	inline static int32_t get_offset_of_properties_16() { return static_cast<int32_t>(offsetof(StandardBindingOptionalReliableSessionElement_t2857298057_StaticFields, ___properties_16)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_16() const { return ___properties_16; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_16() { return &___properties_16; }
	inline void set_properties_16(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_16 = value;
		Il2CppCodeGenWriteBarrier(&___properties_16, value);
	}

	inline static int32_t get_offset_of_enabled_17() { return static_cast<int32_t>(offsetof(StandardBindingOptionalReliableSessionElement_t2857298057_StaticFields, ___enabled_17)); }
	inline ConfigurationProperty_t3590861854 * get_enabled_17() const { return ___enabled_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_enabled_17() { return &___enabled_17; }
	inline void set_enabled_17(ConfigurationProperty_t3590861854 * value)
	{
		___enabled_17 = value;
		Il2CppCodeGenWriteBarrier(&___enabled_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
