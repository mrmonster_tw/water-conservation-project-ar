﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Dispatcher2309470997.h"

// System.ServiceModel.DataContractFormatAttribute
struct DataContractFormatAttribute_t1176337824;
// System.Collections.Generic.Dictionary`2<System.ServiceModel.Description.MessagePartDescription,System.Runtime.Serialization.XmlObjectSerializer>
struct Dictionary_2_t2739530728;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.DataContractMessagesFormatter
struct  DataContractMessagesFormatter_t1452436551  : public BaseMessagesFormatter_t2309470997
{
public:
	// System.ServiceModel.DataContractFormatAttribute System.ServiceModel.Dispatcher.DataContractMessagesFormatter::attr
	DataContractFormatAttribute_t1176337824 * ___attr_4;
	// System.Collections.Generic.Dictionary`2<System.ServiceModel.Description.MessagePartDescription,System.Runtime.Serialization.XmlObjectSerializer> System.ServiceModel.Dispatcher.DataContractMessagesFormatter::serializers
	Dictionary_2_t2739530728 * ___serializers_5;

public:
	inline static int32_t get_offset_of_attr_4() { return static_cast<int32_t>(offsetof(DataContractMessagesFormatter_t1452436551, ___attr_4)); }
	inline DataContractFormatAttribute_t1176337824 * get_attr_4() const { return ___attr_4; }
	inline DataContractFormatAttribute_t1176337824 ** get_address_of_attr_4() { return &___attr_4; }
	inline void set_attr_4(DataContractFormatAttribute_t1176337824 * value)
	{
		___attr_4 = value;
		Il2CppCodeGenWriteBarrier(&___attr_4, value);
	}

	inline static int32_t get_offset_of_serializers_5() { return static_cast<int32_t>(offsetof(DataContractMessagesFormatter_t1452436551, ___serializers_5)); }
	inline Dictionary_2_t2739530728 * get_serializers_5() const { return ___serializers_5; }
	inline Dictionary_2_t2739530728 ** get_address_of_serializers_5() { return &___serializers_5; }
	inline void set_serializers_5(Dictionary_2_t2739530728 * value)
	{
		___serializers_5 = value;
		Il2CppCodeGenWriteBarrier(&___serializers_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
