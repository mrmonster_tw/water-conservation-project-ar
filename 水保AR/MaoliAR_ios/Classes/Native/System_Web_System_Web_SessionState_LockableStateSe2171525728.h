﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.SessionState.StateServerItem
struct StateServerItem_t1723435776;
// System.Threading.ReaderWriterLock
struct ReaderWriterLock_t367846299;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.SessionState.LockableStateServerItem
struct  LockableStateServerItem_t2171525728  : public Il2CppObject
{
public:
	// System.Web.SessionState.StateServerItem System.Web.SessionState.LockableStateServerItem::item
	StateServerItem_t1723435776 * ___item_0;
	// System.Threading.ReaderWriterLock System.Web.SessionState.LockableStateServerItem::rwlock
	ReaderWriterLock_t367846299 * ___rwlock_1;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(LockableStateServerItem_t2171525728, ___item_0)); }
	inline StateServerItem_t1723435776 * get_item_0() const { return ___item_0; }
	inline StateServerItem_t1723435776 ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(StateServerItem_t1723435776 * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier(&___item_0, value);
	}

	inline static int32_t get_offset_of_rwlock_1() { return static_cast<int32_t>(offsetof(LockableStateServerItem_t2171525728, ___rwlock_1)); }
	inline ReaderWriterLock_t367846299 * get_rwlock_1() const { return ___rwlock_1; }
	inline ReaderWriterLock_t367846299 ** get_address_of_rwlock_1() { return &___rwlock_1; }
	inline void set_rwlock_1(ReaderWriterLock_t367846299 * value)
	{
		___rwlock_1 = value;
		Il2CppCodeGenWriteBarrier(&___rwlock_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
