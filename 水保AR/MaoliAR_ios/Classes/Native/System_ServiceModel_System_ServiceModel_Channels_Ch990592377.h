﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Ch740725699.h"

// System.ServiceModel.IDefaultCommunicationTimeouts
struct IDefaultCommunicationTimeouts_t2848643016;
// System.Collections.Generic.KeyedByTypeCollection`1<System.Object>
struct KeyedByTypeCollection_1_t1429017627;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.ChannelListenerBase
struct  ChannelListenerBase_t990592377  : public ChannelManagerBase_t740725699
{
public:
	// System.ServiceModel.IDefaultCommunicationTimeouts System.ServiceModel.Channels.ChannelListenerBase::timeouts
	Il2CppObject * ___timeouts_9;
	// System.Collections.Generic.KeyedByTypeCollection`1<System.Object> System.ServiceModel.Channels.ChannelListenerBase::properties
	KeyedByTypeCollection_1_t1429017627 * ___properties_10;

public:
	inline static int32_t get_offset_of_timeouts_9() { return static_cast<int32_t>(offsetof(ChannelListenerBase_t990592377, ___timeouts_9)); }
	inline Il2CppObject * get_timeouts_9() const { return ___timeouts_9; }
	inline Il2CppObject ** get_address_of_timeouts_9() { return &___timeouts_9; }
	inline void set_timeouts_9(Il2CppObject * value)
	{
		___timeouts_9 = value;
		Il2CppCodeGenWriteBarrier(&___timeouts_9, value);
	}

	inline static int32_t get_offset_of_properties_10() { return static_cast<int32_t>(offsetof(ChannelListenerBase_t990592377, ___properties_10)); }
	inline KeyedByTypeCollection_1_t1429017627 * get_properties_10() const { return ___properties_10; }
	inline KeyedByTypeCollection_1_t1429017627 ** get_address_of_properties_10() { return &___properties_10; }
	inline void set_properties_10(KeyedByTypeCollection_1_t1429017627 * value)
	{
		___properties_10 = value;
		Il2CppCodeGenWriteBarrier(&___properties_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
