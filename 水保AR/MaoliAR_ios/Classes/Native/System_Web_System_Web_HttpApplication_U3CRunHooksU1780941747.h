﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Delegate
struct Delegate_t1188392813;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.EventHandler
struct EventHandler_t1348719766;
// System.Object
struct Il2CppObject;
// System.Web.HttpApplication
struct HttpApplication_t3359645917;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpApplication/<RunHooks>c__Iterator1
struct  U3CRunHooksU3Ec__Iterator1_t1780941747  : public Il2CppObject
{
public:
	// System.Delegate System.Web.HttpApplication/<RunHooks>c__Iterator1::list
	Delegate_t1188392813 * ___list_0;
	// System.Delegate[] System.Web.HttpApplication/<RunHooks>c__Iterator1::<delegates>__0
	DelegateU5BU5D_t1703627840* ___U3CdelegatesU3E__0_1;
	// System.Delegate[] System.Web.HttpApplication/<RunHooks>c__Iterator1::<$s_355>__1
	DelegateU5BU5D_t1703627840* ___U3CU24s_355U3E__1_2;
	// System.Int32 System.Web.HttpApplication/<RunHooks>c__Iterator1::<$s_356>__2
	int32_t ___U3CU24s_356U3E__2_3;
	// System.EventHandler System.Web.HttpApplication/<RunHooks>c__Iterator1::<d>__3
	EventHandler_t1348719766 * ___U3CdU3E__3_4;
	// System.Int32 System.Web.HttpApplication/<RunHooks>c__Iterator1::$PC
	int32_t ___U24PC_5;
	// System.Object System.Web.HttpApplication/<RunHooks>c__Iterator1::$current
	Il2CppObject * ___U24current_6;
	// System.Delegate System.Web.HttpApplication/<RunHooks>c__Iterator1::<$>list
	Delegate_t1188392813 * ___U3CU24U3Elist_7;
	// System.Web.HttpApplication System.Web.HttpApplication/<RunHooks>c__Iterator1::<>f__this
	HttpApplication_t3359645917 * ___U3CU3Ef__this_8;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(U3CRunHooksU3Ec__Iterator1_t1780941747, ___list_0)); }
	inline Delegate_t1188392813 * get_list_0() const { return ___list_0; }
	inline Delegate_t1188392813 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(Delegate_t1188392813 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier(&___list_0, value);
	}

	inline static int32_t get_offset_of_U3CdelegatesU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRunHooksU3Ec__Iterator1_t1780941747, ___U3CdelegatesU3E__0_1)); }
	inline DelegateU5BU5D_t1703627840* get_U3CdelegatesU3E__0_1() const { return ___U3CdelegatesU3E__0_1; }
	inline DelegateU5BU5D_t1703627840** get_address_of_U3CdelegatesU3E__0_1() { return &___U3CdelegatesU3E__0_1; }
	inline void set_U3CdelegatesU3E__0_1(DelegateU5BU5D_t1703627840* value)
	{
		___U3CdelegatesU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdelegatesU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CU24s_355U3E__1_2() { return static_cast<int32_t>(offsetof(U3CRunHooksU3Ec__Iterator1_t1780941747, ___U3CU24s_355U3E__1_2)); }
	inline DelegateU5BU5D_t1703627840* get_U3CU24s_355U3E__1_2() const { return ___U3CU24s_355U3E__1_2; }
	inline DelegateU5BU5D_t1703627840** get_address_of_U3CU24s_355U3E__1_2() { return &___U3CU24s_355U3E__1_2; }
	inline void set_U3CU24s_355U3E__1_2(DelegateU5BU5D_t1703627840* value)
	{
		___U3CU24s_355U3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_355U3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3CU24s_356U3E__2_3() { return static_cast<int32_t>(offsetof(U3CRunHooksU3Ec__Iterator1_t1780941747, ___U3CU24s_356U3E__2_3)); }
	inline int32_t get_U3CU24s_356U3E__2_3() const { return ___U3CU24s_356U3E__2_3; }
	inline int32_t* get_address_of_U3CU24s_356U3E__2_3() { return &___U3CU24s_356U3E__2_3; }
	inline void set_U3CU24s_356U3E__2_3(int32_t value)
	{
		___U3CU24s_356U3E__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CdU3E__3_4() { return static_cast<int32_t>(offsetof(U3CRunHooksU3Ec__Iterator1_t1780941747, ___U3CdU3E__3_4)); }
	inline EventHandler_t1348719766 * get_U3CdU3E__3_4() const { return ___U3CdU3E__3_4; }
	inline EventHandler_t1348719766 ** get_address_of_U3CdU3E__3_4() { return &___U3CdU3E__3_4; }
	inline void set_U3CdU3E__3_4(EventHandler_t1348719766 * value)
	{
		___U3CdU3E__3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdU3E__3_4, value);
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CRunHooksU3Ec__Iterator1_t1780941747, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CRunHooksU3Ec__Iterator1_t1780941747, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Elist_7() { return static_cast<int32_t>(offsetof(U3CRunHooksU3Ec__Iterator1_t1780941747, ___U3CU24U3Elist_7)); }
	inline Delegate_t1188392813 * get_U3CU24U3Elist_7() const { return ___U3CU24U3Elist_7; }
	inline Delegate_t1188392813 ** get_address_of_U3CU24U3Elist_7() { return &___U3CU24U3Elist_7; }
	inline void set_U3CU24U3Elist_7(Delegate_t1188392813 * value)
	{
		___U3CU24U3Elist_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Elist_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_8() { return static_cast<int32_t>(offsetof(U3CRunHooksU3Ec__Iterator1_t1780941747, ___U3CU3Ef__this_8)); }
	inline HttpApplication_t3359645917 * get_U3CU3Ef__this_8() const { return ___U3CU3Ef__this_8; }
	inline HttpApplication_t3359645917 ** get_address_of_U3CU3Ef__this_8() { return &___U3CU3Ef__this_8; }
	inline void set_U3CU3Ef__this_8(HttpApplication_t3359645917 * value)
	{
		___U3CU3Ef__this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
