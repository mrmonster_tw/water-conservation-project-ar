﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_Mono_CodeGeneration_CodeValueRe975266093.h"

// System.Reflection.Emit.LocalBuilder
struct LocalBuilder_t3562264111;
// System.Type
struct Type_t;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.CodeGeneration.CodeVariableReference
struct  CodeVariableReference_t2879773301  : public CodeValueReference_t975266093
{
public:
	// System.Reflection.Emit.LocalBuilder Mono.CodeGeneration.CodeVariableReference::localBuilder
	LocalBuilder_t3562264111 * ___localBuilder_0;
	// System.Type Mono.CodeGeneration.CodeVariableReference::type
	Type_t * ___type_1;
	// System.String Mono.CodeGeneration.CodeVariableReference::name
	String_t* ___name_2;

public:
	inline static int32_t get_offset_of_localBuilder_0() { return static_cast<int32_t>(offsetof(CodeVariableReference_t2879773301, ___localBuilder_0)); }
	inline LocalBuilder_t3562264111 * get_localBuilder_0() const { return ___localBuilder_0; }
	inline LocalBuilder_t3562264111 ** get_address_of_localBuilder_0() { return &___localBuilder_0; }
	inline void set_localBuilder_0(LocalBuilder_t3562264111 * value)
	{
		___localBuilder_0 = value;
		Il2CppCodeGenWriteBarrier(&___localBuilder_0, value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(CodeVariableReference_t2879773301, ___type_1)); }
	inline Type_t * get_type_1() const { return ___type_1; }
	inline Type_t ** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(Type_t * value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier(&___type_1, value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(CodeVariableReference_t2879773301, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
