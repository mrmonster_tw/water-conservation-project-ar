﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_Compilation_TemplateControlCo645890099.h"

// System.Web.UI.PageThemeParser
struct PageThemeParser_t3466356847;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.PageThemeCompiler
struct  PageThemeCompiler_t4116047890  : public TemplateControlCompiler_t645890099
{
public:
	// System.Web.UI.PageThemeParser System.Web.Compilation.PageThemeCompiler::parser
	PageThemeParser_t3466356847 * ___parser_24;

public:
	inline static int32_t get_offset_of_parser_24() { return static_cast<int32_t>(offsetof(PageThemeCompiler_t4116047890, ___parser_24)); }
	inline PageThemeParser_t3466356847 * get_parser_24() const { return ___parser_24; }
	inline PageThemeParser_t3466356847 ** get_address_of_parser_24() { return &___parser_24; }
	inline void set_parser_24(PageThemeParser_t3466356847 * value)
	{
		___parser_24 = value;
		Il2CppCodeGenWriteBarrier(&___parser_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
