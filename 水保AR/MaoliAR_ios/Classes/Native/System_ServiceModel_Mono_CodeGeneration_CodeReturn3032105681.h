﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_Mono_CodeGeneration_CodeStatemen30174410.h"

// Mono.CodeGeneration.CodeExpression
struct CodeExpression_t2163794278;
// Mono.CodeGeneration.CodeBuilder
struct CodeBuilder_t3190018006;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.CodeGeneration.CodeReturn
struct  CodeReturn_t3032105681  : public CodeStatement_t30174410
{
public:
	// Mono.CodeGeneration.CodeExpression Mono.CodeGeneration.CodeReturn::retValue
	CodeExpression_t2163794278 * ___retValue_0;
	// Mono.CodeGeneration.CodeBuilder Mono.CodeGeneration.CodeReturn::codeBuilder
	CodeBuilder_t3190018006 * ___codeBuilder_1;

public:
	inline static int32_t get_offset_of_retValue_0() { return static_cast<int32_t>(offsetof(CodeReturn_t3032105681, ___retValue_0)); }
	inline CodeExpression_t2163794278 * get_retValue_0() const { return ___retValue_0; }
	inline CodeExpression_t2163794278 ** get_address_of_retValue_0() { return &___retValue_0; }
	inline void set_retValue_0(CodeExpression_t2163794278 * value)
	{
		___retValue_0 = value;
		Il2CppCodeGenWriteBarrier(&___retValue_0, value);
	}

	inline static int32_t get_offset_of_codeBuilder_1() { return static_cast<int32_t>(offsetof(CodeReturn_t3032105681, ___codeBuilder_1)); }
	inline CodeBuilder_t3190018006 * get_codeBuilder_1() const { return ___codeBuilder_1; }
	inline CodeBuilder_t3190018006 ** get_address_of_codeBuilder_1() { return &___codeBuilder_1; }
	inline void set_codeBuilder_1(CodeBuilder_t3190018006 * value)
	{
		___codeBuilder_1 = value;
		Il2CppCodeGenWriteBarrier(&___codeBuilder_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
