﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Vector22156229523.h"

// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReadJoystick
struct  ReadJoystick_t2305976153  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32[] ReadJoystick::joystickxy
	Int32U5BU5D_t385246372* ___joystickxy_2;
	// UnityEngine.Vector2 ReadJoystick::curAccess
	Vector2_t2156229523  ___curAccess_3;
	// System.String ReadJoystick::BT
	String_t* ___BT_4;

public:
	inline static int32_t get_offset_of_joystickxy_2() { return static_cast<int32_t>(offsetof(ReadJoystick_t2305976153, ___joystickxy_2)); }
	inline Int32U5BU5D_t385246372* get_joystickxy_2() const { return ___joystickxy_2; }
	inline Int32U5BU5D_t385246372** get_address_of_joystickxy_2() { return &___joystickxy_2; }
	inline void set_joystickxy_2(Int32U5BU5D_t385246372* value)
	{
		___joystickxy_2 = value;
		Il2CppCodeGenWriteBarrier(&___joystickxy_2, value);
	}

	inline static int32_t get_offset_of_curAccess_3() { return static_cast<int32_t>(offsetof(ReadJoystick_t2305976153, ___curAccess_3)); }
	inline Vector2_t2156229523  get_curAccess_3() const { return ___curAccess_3; }
	inline Vector2_t2156229523 * get_address_of_curAccess_3() { return &___curAccess_3; }
	inline void set_curAccess_3(Vector2_t2156229523  value)
	{
		___curAccess_3 = value;
	}

	inline static int32_t get_offset_of_BT_4() { return static_cast<int32_t>(offsetof(ReadJoystick_t2305976153, ___BT_4)); }
	inline String_t* get_BT_4() const { return ___BT_4; }
	inline String_t** get_address_of_BT_4() { return &___BT_4; }
	inline void set_BT_4(String_t* value)
	{
		___BT_4 = value;
		Il2CppCodeGenWriteBarrier(&___BT_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
