﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_Control3006474639.h"

// System.Reflection.Assembly
struct Assembly_t4102432799;
// System.Object
struct Il2CppObject;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t1853889766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.TemplateControl
struct  TemplateControl_t1922940480  : public Control_t3006474639
{
public:
	// System.String System.Web.UI.TemplateControl::_appRelativeVirtualPath
	String_t* ____appRelativeVirtualPath_33;

public:
	inline static int32_t get_offset_of__appRelativeVirtualPath_33() { return static_cast<int32_t>(offsetof(TemplateControl_t1922940480, ____appRelativeVirtualPath_33)); }
	inline String_t* get__appRelativeVirtualPath_33() const { return ____appRelativeVirtualPath_33; }
	inline String_t** get_address_of__appRelativeVirtualPath_33() { return &____appRelativeVirtualPath_33; }
	inline void set__appRelativeVirtualPath_33(String_t* value)
	{
		____appRelativeVirtualPath_33 = value;
		Il2CppCodeGenWriteBarrier(&____appRelativeVirtualPath_33, value);
	}
};

struct TemplateControl_t1922940480_StaticFields
{
public:
	// System.Reflection.Assembly System.Web.UI.TemplateControl::_System_Web_Assembly
	Assembly_t4102432799 * ____System_Web_Assembly_28;
	// System.Object System.Web.UI.TemplateControl::abortTransaction
	Il2CppObject * ___abortTransaction_29;
	// System.Object System.Web.UI.TemplateControl::commitTransaction
	Il2CppObject * ___commitTransaction_30;
	// System.Object System.Web.UI.TemplateControl::error
	Il2CppObject * ___error_31;
	// System.String[] System.Web.UI.TemplateControl::methodNames
	StringU5BU5D_t1281789340* ___methodNames_32;
	// System.Collections.Hashtable System.Web.UI.TemplateControl::auto_event_info
	Hashtable_t1853889766 * ___auto_event_info_34;
	// System.Object System.Web.UI.TemplateControl::auto_event_info_monitor
	Il2CppObject * ___auto_event_info_monitor_35;

public:
	inline static int32_t get_offset_of__System_Web_Assembly_28() { return static_cast<int32_t>(offsetof(TemplateControl_t1922940480_StaticFields, ____System_Web_Assembly_28)); }
	inline Assembly_t4102432799 * get__System_Web_Assembly_28() const { return ____System_Web_Assembly_28; }
	inline Assembly_t4102432799 ** get_address_of__System_Web_Assembly_28() { return &____System_Web_Assembly_28; }
	inline void set__System_Web_Assembly_28(Assembly_t4102432799 * value)
	{
		____System_Web_Assembly_28 = value;
		Il2CppCodeGenWriteBarrier(&____System_Web_Assembly_28, value);
	}

	inline static int32_t get_offset_of_abortTransaction_29() { return static_cast<int32_t>(offsetof(TemplateControl_t1922940480_StaticFields, ___abortTransaction_29)); }
	inline Il2CppObject * get_abortTransaction_29() const { return ___abortTransaction_29; }
	inline Il2CppObject ** get_address_of_abortTransaction_29() { return &___abortTransaction_29; }
	inline void set_abortTransaction_29(Il2CppObject * value)
	{
		___abortTransaction_29 = value;
		Il2CppCodeGenWriteBarrier(&___abortTransaction_29, value);
	}

	inline static int32_t get_offset_of_commitTransaction_30() { return static_cast<int32_t>(offsetof(TemplateControl_t1922940480_StaticFields, ___commitTransaction_30)); }
	inline Il2CppObject * get_commitTransaction_30() const { return ___commitTransaction_30; }
	inline Il2CppObject ** get_address_of_commitTransaction_30() { return &___commitTransaction_30; }
	inline void set_commitTransaction_30(Il2CppObject * value)
	{
		___commitTransaction_30 = value;
		Il2CppCodeGenWriteBarrier(&___commitTransaction_30, value);
	}

	inline static int32_t get_offset_of_error_31() { return static_cast<int32_t>(offsetof(TemplateControl_t1922940480_StaticFields, ___error_31)); }
	inline Il2CppObject * get_error_31() const { return ___error_31; }
	inline Il2CppObject ** get_address_of_error_31() { return &___error_31; }
	inline void set_error_31(Il2CppObject * value)
	{
		___error_31 = value;
		Il2CppCodeGenWriteBarrier(&___error_31, value);
	}

	inline static int32_t get_offset_of_methodNames_32() { return static_cast<int32_t>(offsetof(TemplateControl_t1922940480_StaticFields, ___methodNames_32)); }
	inline StringU5BU5D_t1281789340* get_methodNames_32() const { return ___methodNames_32; }
	inline StringU5BU5D_t1281789340** get_address_of_methodNames_32() { return &___methodNames_32; }
	inline void set_methodNames_32(StringU5BU5D_t1281789340* value)
	{
		___methodNames_32 = value;
		Il2CppCodeGenWriteBarrier(&___methodNames_32, value);
	}

	inline static int32_t get_offset_of_auto_event_info_34() { return static_cast<int32_t>(offsetof(TemplateControl_t1922940480_StaticFields, ___auto_event_info_34)); }
	inline Hashtable_t1853889766 * get_auto_event_info_34() const { return ___auto_event_info_34; }
	inline Hashtable_t1853889766 ** get_address_of_auto_event_info_34() { return &___auto_event_info_34; }
	inline void set_auto_event_info_34(Hashtable_t1853889766 * value)
	{
		___auto_event_info_34 = value;
		Il2CppCodeGenWriteBarrier(&___auto_event_info_34, value);
	}

	inline static int32_t get_offset_of_auto_event_info_monitor_35() { return static_cast<int32_t>(offsetof(TemplateControl_t1922940480_StaticFields, ___auto_event_info_monitor_35)); }
	inline Il2CppObject * get_auto_event_info_monitor_35() const { return ___auto_event_info_monitor_35; }
	inline Il2CppObject ** get_address_of_auto_event_info_monitor_35() { return &___auto_event_info_monitor_35; }
	inline void set_auto_event_info_monitor_35(Il2CppObject * value)
	{
		___auto_event_info_monitor_35 = value;
		Il2CppCodeGenWriteBarrier(&___auto_event_info_monitor_35, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
