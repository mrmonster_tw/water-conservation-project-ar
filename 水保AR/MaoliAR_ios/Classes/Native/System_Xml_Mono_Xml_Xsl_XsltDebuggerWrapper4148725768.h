﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.XsltDebuggerWrapper
struct  XsltDebuggerWrapper_t4148725768  : public Il2CppObject
{
public:
	// System.Reflection.MethodInfo Mono.Xml.Xsl.XsltDebuggerWrapper::on_compile
	MethodInfo_t * ___on_compile_0;
	// System.Reflection.MethodInfo Mono.Xml.Xsl.XsltDebuggerWrapper::on_execute
	MethodInfo_t * ___on_execute_1;
	// System.Object Mono.Xml.Xsl.XsltDebuggerWrapper::impl
	Il2CppObject * ___impl_2;

public:
	inline static int32_t get_offset_of_on_compile_0() { return static_cast<int32_t>(offsetof(XsltDebuggerWrapper_t4148725768, ___on_compile_0)); }
	inline MethodInfo_t * get_on_compile_0() const { return ___on_compile_0; }
	inline MethodInfo_t ** get_address_of_on_compile_0() { return &___on_compile_0; }
	inline void set_on_compile_0(MethodInfo_t * value)
	{
		___on_compile_0 = value;
		Il2CppCodeGenWriteBarrier(&___on_compile_0, value);
	}

	inline static int32_t get_offset_of_on_execute_1() { return static_cast<int32_t>(offsetof(XsltDebuggerWrapper_t4148725768, ___on_execute_1)); }
	inline MethodInfo_t * get_on_execute_1() const { return ___on_execute_1; }
	inline MethodInfo_t ** get_address_of_on_execute_1() { return &___on_execute_1; }
	inline void set_on_execute_1(MethodInfo_t * value)
	{
		___on_execute_1 = value;
		Il2CppCodeGenWriteBarrier(&___on_execute_1, value);
	}

	inline static int32_t get_offset_of_impl_2() { return static_cast<int32_t>(offsetof(XsltDebuggerWrapper_t4148725768, ___impl_2)); }
	inline Il2CppObject * get_impl_2() const { return ___impl_2; }
	inline Il2CppObject ** get_address_of_impl_2() { return &___impl_2; }
	inline void set_impl_2(Il2CppObject * value)
	{
		___impl_2 = value;
		Il2CppCodeGenWriteBarrier(&___impl_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
