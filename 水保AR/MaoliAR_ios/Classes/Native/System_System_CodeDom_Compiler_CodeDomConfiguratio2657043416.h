﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3156163955.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;
// System.CodeDom.Compiler.CompilerCollection
struct CompilerCollection_t2531538163;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.Compiler.CodeDomConfigurationHandler
struct  CodeDomConfigurationHandler_t2657043416  : public ConfigurationSection_t3156163955
{
public:

public:
};

struct CodeDomConfigurationHandler_t2657043416_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.CodeDom.Compiler.CodeDomConfigurationHandler::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_17;
	// System.Configuration.ConfigurationProperty System.CodeDom.Compiler.CodeDomConfigurationHandler::compilersProp
	ConfigurationProperty_t3590861854 * ___compilersProp_18;
	// System.CodeDom.Compiler.CompilerCollection System.CodeDom.Compiler.CodeDomConfigurationHandler::default_compilers
	CompilerCollection_t2531538163 * ___default_compilers_19;

public:
	inline static int32_t get_offset_of_properties_17() { return static_cast<int32_t>(offsetof(CodeDomConfigurationHandler_t2657043416_StaticFields, ___properties_17)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_17() const { return ___properties_17; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_17() { return &___properties_17; }
	inline void set_properties_17(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_17 = value;
		Il2CppCodeGenWriteBarrier(&___properties_17, value);
	}

	inline static int32_t get_offset_of_compilersProp_18() { return static_cast<int32_t>(offsetof(CodeDomConfigurationHandler_t2657043416_StaticFields, ___compilersProp_18)); }
	inline ConfigurationProperty_t3590861854 * get_compilersProp_18() const { return ___compilersProp_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_compilersProp_18() { return &___compilersProp_18; }
	inline void set_compilersProp_18(ConfigurationProperty_t3590861854 * value)
	{
		___compilersProp_18 = value;
		Il2CppCodeGenWriteBarrier(&___compilersProp_18, value);
	}

	inline static int32_t get_offset_of_default_compilers_19() { return static_cast<int32_t>(offsetof(CodeDomConfigurationHandler_t2657043416_StaticFields, ___default_compilers_19)); }
	inline CompilerCollection_t2531538163 * get_default_compilers_19() const { return ___default_compilers_19; }
	inline CompilerCollection_t2531538163 ** get_address_of_default_compilers_19() { return &___default_compilers_19; }
	inline void set_default_compilers_19(CompilerCollection_t2531538163 * value)
	{
		___default_compilers_19 = value;
		Il2CppCodeGenWriteBarrier(&___default_compilers_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
