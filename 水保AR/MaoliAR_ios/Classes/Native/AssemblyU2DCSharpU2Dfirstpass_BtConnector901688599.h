﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4116647657;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BtConnector
struct  BtConnector_t901688599  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean BtConnector::stopReading
	bool ___stopReading_4;
	// System.Boolean BtConnector::mode0
	bool ___mode0_5;
	// System.Boolean BtConnector::mode2
	bool ___mode2_6;
	// System.Boolean BtConnector::mode3
	bool ___mode3_7;
	// System.Int32 BtConnector::length
	int32_t ___length_8;
	// System.Byte BtConnector::terminalByte
	uint8_t ___terminalByte_9;

public:
	inline static int32_t get_offset_of_stopReading_4() { return static_cast<int32_t>(offsetof(BtConnector_t901688599, ___stopReading_4)); }
	inline bool get_stopReading_4() const { return ___stopReading_4; }
	inline bool* get_address_of_stopReading_4() { return &___stopReading_4; }
	inline void set_stopReading_4(bool value)
	{
		___stopReading_4 = value;
	}

	inline static int32_t get_offset_of_mode0_5() { return static_cast<int32_t>(offsetof(BtConnector_t901688599, ___mode0_5)); }
	inline bool get_mode0_5() const { return ___mode0_5; }
	inline bool* get_address_of_mode0_5() { return &___mode0_5; }
	inline void set_mode0_5(bool value)
	{
		___mode0_5 = value;
	}

	inline static int32_t get_offset_of_mode2_6() { return static_cast<int32_t>(offsetof(BtConnector_t901688599, ___mode2_6)); }
	inline bool get_mode2_6() const { return ___mode2_6; }
	inline bool* get_address_of_mode2_6() { return &___mode2_6; }
	inline void set_mode2_6(bool value)
	{
		___mode2_6 = value;
	}

	inline static int32_t get_offset_of_mode3_7() { return static_cast<int32_t>(offsetof(BtConnector_t901688599, ___mode3_7)); }
	inline bool get_mode3_7() const { return ___mode3_7; }
	inline bool* get_address_of_mode3_7() { return &___mode3_7; }
	inline void set_mode3_7(bool value)
	{
		___mode3_7 = value;
	}

	inline static int32_t get_offset_of_length_8() { return static_cast<int32_t>(offsetof(BtConnector_t901688599, ___length_8)); }
	inline int32_t get_length_8() const { return ___length_8; }
	inline int32_t* get_address_of_length_8() { return &___length_8; }
	inline void set_length_8(int32_t value)
	{
		___length_8 = value;
	}

	inline static int32_t get_offset_of_terminalByte_9() { return static_cast<int32_t>(offsetof(BtConnector_t901688599, ___terminalByte_9)); }
	inline uint8_t get_terminalByte_9() const { return ___terminalByte_9; }
	inline uint8_t* get_address_of_terminalByte_9() { return &___terminalByte_9; }
	inline void set_terminalByte_9(uint8_t value)
	{
		___terminalByte_9 = value;
	}
};

struct BtConnector_t901688599_StaticFields
{
public:
	// System.String BtConnector::message
	String_t* ___message_2;
	// System.Byte[] BtConnector::byteMessage
	ByteU5BU5D_t4116647657* ___byteMessage_3;
	// System.Boolean BtConnector::dataAvailable
	bool ___dataAvailable_10;
	// System.Boolean BtConnector::connected
	bool ___connected_11;
	// System.Boolean BtConnector::isDevicePicked
	bool ___isDevicePicked_12;

public:
	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(BtConnector_t901688599_StaticFields, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier(&___message_2, value);
	}

	inline static int32_t get_offset_of_byteMessage_3() { return static_cast<int32_t>(offsetof(BtConnector_t901688599_StaticFields, ___byteMessage_3)); }
	inline ByteU5BU5D_t4116647657* get_byteMessage_3() const { return ___byteMessage_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_byteMessage_3() { return &___byteMessage_3; }
	inline void set_byteMessage_3(ByteU5BU5D_t4116647657* value)
	{
		___byteMessage_3 = value;
		Il2CppCodeGenWriteBarrier(&___byteMessage_3, value);
	}

	inline static int32_t get_offset_of_dataAvailable_10() { return static_cast<int32_t>(offsetof(BtConnector_t901688599_StaticFields, ___dataAvailable_10)); }
	inline bool get_dataAvailable_10() const { return ___dataAvailable_10; }
	inline bool* get_address_of_dataAvailable_10() { return &___dataAvailable_10; }
	inline void set_dataAvailable_10(bool value)
	{
		___dataAvailable_10 = value;
	}

	inline static int32_t get_offset_of_connected_11() { return static_cast<int32_t>(offsetof(BtConnector_t901688599_StaticFields, ___connected_11)); }
	inline bool get_connected_11() const { return ___connected_11; }
	inline bool* get_address_of_connected_11() { return &___connected_11; }
	inline void set_connected_11(bool value)
	{
		___connected_11 = value;
	}

	inline static int32_t get_offset_of_isDevicePicked_12() { return static_cast<int32_t>(offsetof(BtConnector_t901688599_StaticFields, ___isDevicePicked_12)); }
	inline bool get_isDevicePicked_12() const { return ___isDevicePicked_12; }
	inline bool* get_address_of_isDevicePicked_12() { return &___isDevicePicked_12; }
	inline void set_isDevicePicked_12(bool value)
	{
		___isDevicePicked_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
