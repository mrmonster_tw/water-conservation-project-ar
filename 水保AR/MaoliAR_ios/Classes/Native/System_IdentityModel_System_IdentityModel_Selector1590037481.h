﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Selectors284109284.h"

// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t714049126;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Selectors.X509SecurityTokenAuthenticator/X509AuthorizationPolicy
struct  X509AuthorizationPolicy_t1590037481  : public SystemIdentityAuthorizationPolicy_t284109284
{
public:
	// System.Security.Cryptography.X509Certificates.X509Certificate2 System.IdentityModel.Selectors.X509SecurityTokenAuthenticator/X509AuthorizationPolicy::cert
	X509Certificate2_t714049126 * ___cert_1;

public:
	inline static int32_t get_offset_of_cert_1() { return static_cast<int32_t>(offsetof(X509AuthorizationPolicy_t1590037481, ___cert_1)); }
	inline X509Certificate2_t714049126 * get_cert_1() const { return ___cert_1; }
	inline X509Certificate2_t714049126 ** get_address_of_cert_1() { return &___cert_1; }
	inline void set_cert_1(X509Certificate2_t714049126 * value)
	{
		___cert_1 = value;
		Il2CppCodeGenWriteBarrier(&___cert_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
