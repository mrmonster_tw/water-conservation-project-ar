﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Xml_System_Xml_XmlSpace3324193251.h"

// System.Xml.XmlReader
struct XmlReader_t3121518892;
// System.Xml.XmlValidatingReader
struct XmlValidatingReader_t1719295192;
// System.Xml.XmlNameTable
struct XmlNameTable_t71772148;
// System.Xml.IXmlLineInfo
struct IXmlLineInfo_t2353988607;
// Mono.Xml.XPath.DTMXPathLinkedNode2[]
struct DTMXPathLinkedNode2U5BU5D_t3499139398;
// Mono.Xml.XPath.DTMXPathAttributeNode2[]
struct DTMXPathAttributeNode2U5BU5D_t1081050937;
// Mono.Xml.XPath.DTMXPathNamespaceNode2[]
struct DTMXPathNamespaceNode2U5BU5D_t3587247257;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Int32[]
struct Int32U5BU5D_t385246372;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.XPath.DTMXPathDocumentBuilder2
struct  DTMXPathDocumentBuilder2_t3353543192  : public Il2CppObject
{
public:
	// System.Xml.XmlReader Mono.Xml.XPath.DTMXPathDocumentBuilder2::xmlReader
	XmlReader_t3121518892 * ___xmlReader_0;
	// System.Xml.XmlValidatingReader Mono.Xml.XPath.DTMXPathDocumentBuilder2::validatingReader
	XmlValidatingReader_t1719295192 * ___validatingReader_1;
	// System.Xml.XmlSpace Mono.Xml.XPath.DTMXPathDocumentBuilder2::xmlSpace
	int32_t ___xmlSpace_2;
	// System.Xml.XmlNameTable Mono.Xml.XPath.DTMXPathDocumentBuilder2::nameTable
	XmlNameTable_t71772148 * ___nameTable_3;
	// System.Xml.IXmlLineInfo Mono.Xml.XPath.DTMXPathDocumentBuilder2::lineInfo
	Il2CppObject * ___lineInfo_4;
	// System.Int32 Mono.Xml.XPath.DTMXPathDocumentBuilder2::nodeCapacity
	int32_t ___nodeCapacity_5;
	// System.Int32 Mono.Xml.XPath.DTMXPathDocumentBuilder2::attributeCapacity
	int32_t ___attributeCapacity_6;
	// System.Int32 Mono.Xml.XPath.DTMXPathDocumentBuilder2::nsCapacity
	int32_t ___nsCapacity_7;
	// Mono.Xml.XPath.DTMXPathLinkedNode2[] Mono.Xml.XPath.DTMXPathDocumentBuilder2::nodes
	DTMXPathLinkedNode2U5BU5D_t3499139398* ___nodes_8;
	// Mono.Xml.XPath.DTMXPathAttributeNode2[] Mono.Xml.XPath.DTMXPathDocumentBuilder2::attributes
	DTMXPathAttributeNode2U5BU5D_t1081050937* ___attributes_9;
	// Mono.Xml.XPath.DTMXPathNamespaceNode2[] Mono.Xml.XPath.DTMXPathDocumentBuilder2::namespaces
	DTMXPathNamespaceNode2U5BU5D_t3587247257* ___namespaces_10;
	// System.String[] Mono.Xml.XPath.DTMXPathDocumentBuilder2::atomicStringPool
	StringU5BU5D_t1281789340* ___atomicStringPool_11;
	// System.Int32 Mono.Xml.XPath.DTMXPathDocumentBuilder2::atomicIndex
	int32_t ___atomicIndex_12;
	// System.String[] Mono.Xml.XPath.DTMXPathDocumentBuilder2::nonAtomicStringPool
	StringU5BU5D_t1281789340* ___nonAtomicStringPool_13;
	// System.Int32 Mono.Xml.XPath.DTMXPathDocumentBuilder2::nonAtomicIndex
	int32_t ___nonAtomicIndex_14;
	// System.Collections.Hashtable Mono.Xml.XPath.DTMXPathDocumentBuilder2::idTable
	Hashtable_t1853889766 * ___idTable_15;
	// System.Int32 Mono.Xml.XPath.DTMXPathDocumentBuilder2::nodeIndex
	int32_t ___nodeIndex_16;
	// System.Int32 Mono.Xml.XPath.DTMXPathDocumentBuilder2::attributeIndex
	int32_t ___attributeIndex_17;
	// System.Int32 Mono.Xml.XPath.DTMXPathDocumentBuilder2::nsIndex
	int32_t ___nsIndex_18;
	// System.Boolean Mono.Xml.XPath.DTMXPathDocumentBuilder2::hasAttributes
	bool ___hasAttributes_19;
	// System.Boolean Mono.Xml.XPath.DTMXPathDocumentBuilder2::hasLocalNs
	bool ___hasLocalNs_20;
	// System.Int32 Mono.Xml.XPath.DTMXPathDocumentBuilder2::attrIndexAtStart
	int32_t ___attrIndexAtStart_21;
	// System.Int32 Mono.Xml.XPath.DTMXPathDocumentBuilder2::nsIndexAtStart
	int32_t ___nsIndexAtStart_22;
	// System.Int32 Mono.Xml.XPath.DTMXPathDocumentBuilder2::lastNsInScope
	int32_t ___lastNsInScope_23;
	// System.Boolean Mono.Xml.XPath.DTMXPathDocumentBuilder2::skipRead
	bool ___skipRead_24;
	// System.Int32[] Mono.Xml.XPath.DTMXPathDocumentBuilder2::parentStack
	Int32U5BU5D_t385246372* ___parentStack_25;
	// System.Int32 Mono.Xml.XPath.DTMXPathDocumentBuilder2::parentStackIndex
	int32_t ___parentStackIndex_26;

public:
	inline static int32_t get_offset_of_xmlReader_0() { return static_cast<int32_t>(offsetof(DTMXPathDocumentBuilder2_t3353543192, ___xmlReader_0)); }
	inline XmlReader_t3121518892 * get_xmlReader_0() const { return ___xmlReader_0; }
	inline XmlReader_t3121518892 ** get_address_of_xmlReader_0() { return &___xmlReader_0; }
	inline void set_xmlReader_0(XmlReader_t3121518892 * value)
	{
		___xmlReader_0 = value;
		Il2CppCodeGenWriteBarrier(&___xmlReader_0, value);
	}

	inline static int32_t get_offset_of_validatingReader_1() { return static_cast<int32_t>(offsetof(DTMXPathDocumentBuilder2_t3353543192, ___validatingReader_1)); }
	inline XmlValidatingReader_t1719295192 * get_validatingReader_1() const { return ___validatingReader_1; }
	inline XmlValidatingReader_t1719295192 ** get_address_of_validatingReader_1() { return &___validatingReader_1; }
	inline void set_validatingReader_1(XmlValidatingReader_t1719295192 * value)
	{
		___validatingReader_1 = value;
		Il2CppCodeGenWriteBarrier(&___validatingReader_1, value);
	}

	inline static int32_t get_offset_of_xmlSpace_2() { return static_cast<int32_t>(offsetof(DTMXPathDocumentBuilder2_t3353543192, ___xmlSpace_2)); }
	inline int32_t get_xmlSpace_2() const { return ___xmlSpace_2; }
	inline int32_t* get_address_of_xmlSpace_2() { return &___xmlSpace_2; }
	inline void set_xmlSpace_2(int32_t value)
	{
		___xmlSpace_2 = value;
	}

	inline static int32_t get_offset_of_nameTable_3() { return static_cast<int32_t>(offsetof(DTMXPathDocumentBuilder2_t3353543192, ___nameTable_3)); }
	inline XmlNameTable_t71772148 * get_nameTable_3() const { return ___nameTable_3; }
	inline XmlNameTable_t71772148 ** get_address_of_nameTable_3() { return &___nameTable_3; }
	inline void set_nameTable_3(XmlNameTable_t71772148 * value)
	{
		___nameTable_3 = value;
		Il2CppCodeGenWriteBarrier(&___nameTable_3, value);
	}

	inline static int32_t get_offset_of_lineInfo_4() { return static_cast<int32_t>(offsetof(DTMXPathDocumentBuilder2_t3353543192, ___lineInfo_4)); }
	inline Il2CppObject * get_lineInfo_4() const { return ___lineInfo_4; }
	inline Il2CppObject ** get_address_of_lineInfo_4() { return &___lineInfo_4; }
	inline void set_lineInfo_4(Il2CppObject * value)
	{
		___lineInfo_4 = value;
		Il2CppCodeGenWriteBarrier(&___lineInfo_4, value);
	}

	inline static int32_t get_offset_of_nodeCapacity_5() { return static_cast<int32_t>(offsetof(DTMXPathDocumentBuilder2_t3353543192, ___nodeCapacity_5)); }
	inline int32_t get_nodeCapacity_5() const { return ___nodeCapacity_5; }
	inline int32_t* get_address_of_nodeCapacity_5() { return &___nodeCapacity_5; }
	inline void set_nodeCapacity_5(int32_t value)
	{
		___nodeCapacity_5 = value;
	}

	inline static int32_t get_offset_of_attributeCapacity_6() { return static_cast<int32_t>(offsetof(DTMXPathDocumentBuilder2_t3353543192, ___attributeCapacity_6)); }
	inline int32_t get_attributeCapacity_6() const { return ___attributeCapacity_6; }
	inline int32_t* get_address_of_attributeCapacity_6() { return &___attributeCapacity_6; }
	inline void set_attributeCapacity_6(int32_t value)
	{
		___attributeCapacity_6 = value;
	}

	inline static int32_t get_offset_of_nsCapacity_7() { return static_cast<int32_t>(offsetof(DTMXPathDocumentBuilder2_t3353543192, ___nsCapacity_7)); }
	inline int32_t get_nsCapacity_7() const { return ___nsCapacity_7; }
	inline int32_t* get_address_of_nsCapacity_7() { return &___nsCapacity_7; }
	inline void set_nsCapacity_7(int32_t value)
	{
		___nsCapacity_7 = value;
	}

	inline static int32_t get_offset_of_nodes_8() { return static_cast<int32_t>(offsetof(DTMXPathDocumentBuilder2_t3353543192, ___nodes_8)); }
	inline DTMXPathLinkedNode2U5BU5D_t3499139398* get_nodes_8() const { return ___nodes_8; }
	inline DTMXPathLinkedNode2U5BU5D_t3499139398** get_address_of_nodes_8() { return &___nodes_8; }
	inline void set_nodes_8(DTMXPathLinkedNode2U5BU5D_t3499139398* value)
	{
		___nodes_8 = value;
		Il2CppCodeGenWriteBarrier(&___nodes_8, value);
	}

	inline static int32_t get_offset_of_attributes_9() { return static_cast<int32_t>(offsetof(DTMXPathDocumentBuilder2_t3353543192, ___attributes_9)); }
	inline DTMXPathAttributeNode2U5BU5D_t1081050937* get_attributes_9() const { return ___attributes_9; }
	inline DTMXPathAttributeNode2U5BU5D_t1081050937** get_address_of_attributes_9() { return &___attributes_9; }
	inline void set_attributes_9(DTMXPathAttributeNode2U5BU5D_t1081050937* value)
	{
		___attributes_9 = value;
		Il2CppCodeGenWriteBarrier(&___attributes_9, value);
	}

	inline static int32_t get_offset_of_namespaces_10() { return static_cast<int32_t>(offsetof(DTMXPathDocumentBuilder2_t3353543192, ___namespaces_10)); }
	inline DTMXPathNamespaceNode2U5BU5D_t3587247257* get_namespaces_10() const { return ___namespaces_10; }
	inline DTMXPathNamespaceNode2U5BU5D_t3587247257** get_address_of_namespaces_10() { return &___namespaces_10; }
	inline void set_namespaces_10(DTMXPathNamespaceNode2U5BU5D_t3587247257* value)
	{
		___namespaces_10 = value;
		Il2CppCodeGenWriteBarrier(&___namespaces_10, value);
	}

	inline static int32_t get_offset_of_atomicStringPool_11() { return static_cast<int32_t>(offsetof(DTMXPathDocumentBuilder2_t3353543192, ___atomicStringPool_11)); }
	inline StringU5BU5D_t1281789340* get_atomicStringPool_11() const { return ___atomicStringPool_11; }
	inline StringU5BU5D_t1281789340** get_address_of_atomicStringPool_11() { return &___atomicStringPool_11; }
	inline void set_atomicStringPool_11(StringU5BU5D_t1281789340* value)
	{
		___atomicStringPool_11 = value;
		Il2CppCodeGenWriteBarrier(&___atomicStringPool_11, value);
	}

	inline static int32_t get_offset_of_atomicIndex_12() { return static_cast<int32_t>(offsetof(DTMXPathDocumentBuilder2_t3353543192, ___atomicIndex_12)); }
	inline int32_t get_atomicIndex_12() const { return ___atomicIndex_12; }
	inline int32_t* get_address_of_atomicIndex_12() { return &___atomicIndex_12; }
	inline void set_atomicIndex_12(int32_t value)
	{
		___atomicIndex_12 = value;
	}

	inline static int32_t get_offset_of_nonAtomicStringPool_13() { return static_cast<int32_t>(offsetof(DTMXPathDocumentBuilder2_t3353543192, ___nonAtomicStringPool_13)); }
	inline StringU5BU5D_t1281789340* get_nonAtomicStringPool_13() const { return ___nonAtomicStringPool_13; }
	inline StringU5BU5D_t1281789340** get_address_of_nonAtomicStringPool_13() { return &___nonAtomicStringPool_13; }
	inline void set_nonAtomicStringPool_13(StringU5BU5D_t1281789340* value)
	{
		___nonAtomicStringPool_13 = value;
		Il2CppCodeGenWriteBarrier(&___nonAtomicStringPool_13, value);
	}

	inline static int32_t get_offset_of_nonAtomicIndex_14() { return static_cast<int32_t>(offsetof(DTMXPathDocumentBuilder2_t3353543192, ___nonAtomicIndex_14)); }
	inline int32_t get_nonAtomicIndex_14() const { return ___nonAtomicIndex_14; }
	inline int32_t* get_address_of_nonAtomicIndex_14() { return &___nonAtomicIndex_14; }
	inline void set_nonAtomicIndex_14(int32_t value)
	{
		___nonAtomicIndex_14 = value;
	}

	inline static int32_t get_offset_of_idTable_15() { return static_cast<int32_t>(offsetof(DTMXPathDocumentBuilder2_t3353543192, ___idTable_15)); }
	inline Hashtable_t1853889766 * get_idTable_15() const { return ___idTable_15; }
	inline Hashtable_t1853889766 ** get_address_of_idTable_15() { return &___idTable_15; }
	inline void set_idTable_15(Hashtable_t1853889766 * value)
	{
		___idTable_15 = value;
		Il2CppCodeGenWriteBarrier(&___idTable_15, value);
	}

	inline static int32_t get_offset_of_nodeIndex_16() { return static_cast<int32_t>(offsetof(DTMXPathDocumentBuilder2_t3353543192, ___nodeIndex_16)); }
	inline int32_t get_nodeIndex_16() const { return ___nodeIndex_16; }
	inline int32_t* get_address_of_nodeIndex_16() { return &___nodeIndex_16; }
	inline void set_nodeIndex_16(int32_t value)
	{
		___nodeIndex_16 = value;
	}

	inline static int32_t get_offset_of_attributeIndex_17() { return static_cast<int32_t>(offsetof(DTMXPathDocumentBuilder2_t3353543192, ___attributeIndex_17)); }
	inline int32_t get_attributeIndex_17() const { return ___attributeIndex_17; }
	inline int32_t* get_address_of_attributeIndex_17() { return &___attributeIndex_17; }
	inline void set_attributeIndex_17(int32_t value)
	{
		___attributeIndex_17 = value;
	}

	inline static int32_t get_offset_of_nsIndex_18() { return static_cast<int32_t>(offsetof(DTMXPathDocumentBuilder2_t3353543192, ___nsIndex_18)); }
	inline int32_t get_nsIndex_18() const { return ___nsIndex_18; }
	inline int32_t* get_address_of_nsIndex_18() { return &___nsIndex_18; }
	inline void set_nsIndex_18(int32_t value)
	{
		___nsIndex_18 = value;
	}

	inline static int32_t get_offset_of_hasAttributes_19() { return static_cast<int32_t>(offsetof(DTMXPathDocumentBuilder2_t3353543192, ___hasAttributes_19)); }
	inline bool get_hasAttributes_19() const { return ___hasAttributes_19; }
	inline bool* get_address_of_hasAttributes_19() { return &___hasAttributes_19; }
	inline void set_hasAttributes_19(bool value)
	{
		___hasAttributes_19 = value;
	}

	inline static int32_t get_offset_of_hasLocalNs_20() { return static_cast<int32_t>(offsetof(DTMXPathDocumentBuilder2_t3353543192, ___hasLocalNs_20)); }
	inline bool get_hasLocalNs_20() const { return ___hasLocalNs_20; }
	inline bool* get_address_of_hasLocalNs_20() { return &___hasLocalNs_20; }
	inline void set_hasLocalNs_20(bool value)
	{
		___hasLocalNs_20 = value;
	}

	inline static int32_t get_offset_of_attrIndexAtStart_21() { return static_cast<int32_t>(offsetof(DTMXPathDocumentBuilder2_t3353543192, ___attrIndexAtStart_21)); }
	inline int32_t get_attrIndexAtStart_21() const { return ___attrIndexAtStart_21; }
	inline int32_t* get_address_of_attrIndexAtStart_21() { return &___attrIndexAtStart_21; }
	inline void set_attrIndexAtStart_21(int32_t value)
	{
		___attrIndexAtStart_21 = value;
	}

	inline static int32_t get_offset_of_nsIndexAtStart_22() { return static_cast<int32_t>(offsetof(DTMXPathDocumentBuilder2_t3353543192, ___nsIndexAtStart_22)); }
	inline int32_t get_nsIndexAtStart_22() const { return ___nsIndexAtStart_22; }
	inline int32_t* get_address_of_nsIndexAtStart_22() { return &___nsIndexAtStart_22; }
	inline void set_nsIndexAtStart_22(int32_t value)
	{
		___nsIndexAtStart_22 = value;
	}

	inline static int32_t get_offset_of_lastNsInScope_23() { return static_cast<int32_t>(offsetof(DTMXPathDocumentBuilder2_t3353543192, ___lastNsInScope_23)); }
	inline int32_t get_lastNsInScope_23() const { return ___lastNsInScope_23; }
	inline int32_t* get_address_of_lastNsInScope_23() { return &___lastNsInScope_23; }
	inline void set_lastNsInScope_23(int32_t value)
	{
		___lastNsInScope_23 = value;
	}

	inline static int32_t get_offset_of_skipRead_24() { return static_cast<int32_t>(offsetof(DTMXPathDocumentBuilder2_t3353543192, ___skipRead_24)); }
	inline bool get_skipRead_24() const { return ___skipRead_24; }
	inline bool* get_address_of_skipRead_24() { return &___skipRead_24; }
	inline void set_skipRead_24(bool value)
	{
		___skipRead_24 = value;
	}

	inline static int32_t get_offset_of_parentStack_25() { return static_cast<int32_t>(offsetof(DTMXPathDocumentBuilder2_t3353543192, ___parentStack_25)); }
	inline Int32U5BU5D_t385246372* get_parentStack_25() const { return ___parentStack_25; }
	inline Int32U5BU5D_t385246372** get_address_of_parentStack_25() { return &___parentStack_25; }
	inline void set_parentStack_25(Int32U5BU5D_t385246372* value)
	{
		___parentStack_25 = value;
		Il2CppCodeGenWriteBarrier(&___parentStack_25, value);
	}

	inline static int32_t get_offset_of_parentStackIndex_26() { return static_cast<int32_t>(offsetof(DTMXPathDocumentBuilder2_t3353543192, ___parentStackIndex_26)); }
	inline int32_t get_parentStackIndex_26() const { return ___parentStackIndex_26; }
	inline int32_t* get_address_of_parentStackIndex_26() { return &___parentStackIndex_26; }
	inline void set_parentStackIndex_26(int32_t value)
	{
		___parentStackIndex_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
