﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Channels.Message
struct Message_t869514973;
// System.ServiceModel.Channels.MessageBuffer
struct MessageBuffer_t778472114;
// System.ServiceModel.Channels.MessageSecurityBindingSupport
struct MessageSecurityBindingSupport_t1904510157;
// System.Xml.XmlDocument
struct XmlDocument_t2837193595;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t418790500;
// System.ServiceModel.Security.SecurityMessageProperty
struct SecurityMessageProperty_t3957431664;
// System.ServiceModel.Channels.WSSecurityMessageHeader
struct WSSecurityMessageHeader_t97321761;
// System.ServiceModel.Channels.WSSecurityMessageHeaderReader
struct WSSecurityMessageHeaderReader_t3195594637;
// System.Collections.Generic.List`1<System.ServiceModel.Channels.MessageHeaderInfo>
struct List_1_t1936922331;
// System.IdentityModel.Selectors.SecurityTokenResolver
struct SecurityTokenResolver_t3905425829;
// System.Collections.Generic.List`1<System.IdentityModel.Tokens.SecurityToken>
struct List_1_t2743948282;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.SecureMessageDecryptor
struct  SecureMessageDecryptor_t2174777289  : public Il2CppObject
{
public:
	// System.ServiceModel.Channels.Message System.ServiceModel.Channels.SecureMessageDecryptor::source_message
	Message_t869514973 * ___source_message_0;
	// System.ServiceModel.Channels.MessageBuffer System.ServiceModel.Channels.SecureMessageDecryptor::buf
	MessageBuffer_t778472114 * ___buf_1;
	// System.ServiceModel.Channels.MessageSecurityBindingSupport System.ServiceModel.Channels.SecureMessageDecryptor::security
	MessageSecurityBindingSupport_t1904510157 * ___security_2;
	// System.Xml.XmlDocument System.ServiceModel.Channels.SecureMessageDecryptor::doc
	XmlDocument_t2837193595 * ___doc_3;
	// System.Xml.XmlNamespaceManager System.ServiceModel.Channels.SecureMessageDecryptor::nsmgr
	XmlNamespaceManager_t418790500 * ___nsmgr_4;
	// System.ServiceModel.Security.SecurityMessageProperty System.ServiceModel.Channels.SecureMessageDecryptor::sec_prop
	SecurityMessageProperty_t3957431664 * ___sec_prop_5;
	// System.ServiceModel.Channels.WSSecurityMessageHeader System.ServiceModel.Channels.SecureMessageDecryptor::wss_header
	WSSecurityMessageHeader_t97321761 * ___wss_header_6;
	// System.ServiceModel.Channels.WSSecurityMessageHeaderReader System.ServiceModel.Channels.SecureMessageDecryptor::wss_header_reader
	WSSecurityMessageHeaderReader_t3195594637 * ___wss_header_reader_7;
	// System.Collections.Generic.List`1<System.ServiceModel.Channels.MessageHeaderInfo> System.ServiceModel.Channels.SecureMessageDecryptor::headers
	List_1_t1936922331 * ___headers_8;
	// System.IdentityModel.Selectors.SecurityTokenResolver System.ServiceModel.Channels.SecureMessageDecryptor::token_resolver
	SecurityTokenResolver_t3905425829 * ___token_resolver_9;
	// System.Collections.Generic.List`1<System.IdentityModel.Tokens.SecurityToken> System.ServiceModel.Channels.SecureMessageDecryptor::tokens
	List_1_t2743948282 * ___tokens_10;

public:
	inline static int32_t get_offset_of_source_message_0() { return static_cast<int32_t>(offsetof(SecureMessageDecryptor_t2174777289, ___source_message_0)); }
	inline Message_t869514973 * get_source_message_0() const { return ___source_message_0; }
	inline Message_t869514973 ** get_address_of_source_message_0() { return &___source_message_0; }
	inline void set_source_message_0(Message_t869514973 * value)
	{
		___source_message_0 = value;
		Il2CppCodeGenWriteBarrier(&___source_message_0, value);
	}

	inline static int32_t get_offset_of_buf_1() { return static_cast<int32_t>(offsetof(SecureMessageDecryptor_t2174777289, ___buf_1)); }
	inline MessageBuffer_t778472114 * get_buf_1() const { return ___buf_1; }
	inline MessageBuffer_t778472114 ** get_address_of_buf_1() { return &___buf_1; }
	inline void set_buf_1(MessageBuffer_t778472114 * value)
	{
		___buf_1 = value;
		Il2CppCodeGenWriteBarrier(&___buf_1, value);
	}

	inline static int32_t get_offset_of_security_2() { return static_cast<int32_t>(offsetof(SecureMessageDecryptor_t2174777289, ___security_2)); }
	inline MessageSecurityBindingSupport_t1904510157 * get_security_2() const { return ___security_2; }
	inline MessageSecurityBindingSupport_t1904510157 ** get_address_of_security_2() { return &___security_2; }
	inline void set_security_2(MessageSecurityBindingSupport_t1904510157 * value)
	{
		___security_2 = value;
		Il2CppCodeGenWriteBarrier(&___security_2, value);
	}

	inline static int32_t get_offset_of_doc_3() { return static_cast<int32_t>(offsetof(SecureMessageDecryptor_t2174777289, ___doc_3)); }
	inline XmlDocument_t2837193595 * get_doc_3() const { return ___doc_3; }
	inline XmlDocument_t2837193595 ** get_address_of_doc_3() { return &___doc_3; }
	inline void set_doc_3(XmlDocument_t2837193595 * value)
	{
		___doc_3 = value;
		Il2CppCodeGenWriteBarrier(&___doc_3, value);
	}

	inline static int32_t get_offset_of_nsmgr_4() { return static_cast<int32_t>(offsetof(SecureMessageDecryptor_t2174777289, ___nsmgr_4)); }
	inline XmlNamespaceManager_t418790500 * get_nsmgr_4() const { return ___nsmgr_4; }
	inline XmlNamespaceManager_t418790500 ** get_address_of_nsmgr_4() { return &___nsmgr_4; }
	inline void set_nsmgr_4(XmlNamespaceManager_t418790500 * value)
	{
		___nsmgr_4 = value;
		Il2CppCodeGenWriteBarrier(&___nsmgr_4, value);
	}

	inline static int32_t get_offset_of_sec_prop_5() { return static_cast<int32_t>(offsetof(SecureMessageDecryptor_t2174777289, ___sec_prop_5)); }
	inline SecurityMessageProperty_t3957431664 * get_sec_prop_5() const { return ___sec_prop_5; }
	inline SecurityMessageProperty_t3957431664 ** get_address_of_sec_prop_5() { return &___sec_prop_5; }
	inline void set_sec_prop_5(SecurityMessageProperty_t3957431664 * value)
	{
		___sec_prop_5 = value;
		Il2CppCodeGenWriteBarrier(&___sec_prop_5, value);
	}

	inline static int32_t get_offset_of_wss_header_6() { return static_cast<int32_t>(offsetof(SecureMessageDecryptor_t2174777289, ___wss_header_6)); }
	inline WSSecurityMessageHeader_t97321761 * get_wss_header_6() const { return ___wss_header_6; }
	inline WSSecurityMessageHeader_t97321761 ** get_address_of_wss_header_6() { return &___wss_header_6; }
	inline void set_wss_header_6(WSSecurityMessageHeader_t97321761 * value)
	{
		___wss_header_6 = value;
		Il2CppCodeGenWriteBarrier(&___wss_header_6, value);
	}

	inline static int32_t get_offset_of_wss_header_reader_7() { return static_cast<int32_t>(offsetof(SecureMessageDecryptor_t2174777289, ___wss_header_reader_7)); }
	inline WSSecurityMessageHeaderReader_t3195594637 * get_wss_header_reader_7() const { return ___wss_header_reader_7; }
	inline WSSecurityMessageHeaderReader_t3195594637 ** get_address_of_wss_header_reader_7() { return &___wss_header_reader_7; }
	inline void set_wss_header_reader_7(WSSecurityMessageHeaderReader_t3195594637 * value)
	{
		___wss_header_reader_7 = value;
		Il2CppCodeGenWriteBarrier(&___wss_header_reader_7, value);
	}

	inline static int32_t get_offset_of_headers_8() { return static_cast<int32_t>(offsetof(SecureMessageDecryptor_t2174777289, ___headers_8)); }
	inline List_1_t1936922331 * get_headers_8() const { return ___headers_8; }
	inline List_1_t1936922331 ** get_address_of_headers_8() { return &___headers_8; }
	inline void set_headers_8(List_1_t1936922331 * value)
	{
		___headers_8 = value;
		Il2CppCodeGenWriteBarrier(&___headers_8, value);
	}

	inline static int32_t get_offset_of_token_resolver_9() { return static_cast<int32_t>(offsetof(SecureMessageDecryptor_t2174777289, ___token_resolver_9)); }
	inline SecurityTokenResolver_t3905425829 * get_token_resolver_9() const { return ___token_resolver_9; }
	inline SecurityTokenResolver_t3905425829 ** get_address_of_token_resolver_9() { return &___token_resolver_9; }
	inline void set_token_resolver_9(SecurityTokenResolver_t3905425829 * value)
	{
		___token_resolver_9 = value;
		Il2CppCodeGenWriteBarrier(&___token_resolver_9, value);
	}

	inline static int32_t get_offset_of_tokens_10() { return static_cast<int32_t>(offsetof(SecureMessageDecryptor_t2174777289, ___tokens_10)); }
	inline List_1_t2743948282 * get_tokens_10() const { return ___tokens_10; }
	inline List_1_t2743948282 ** get_address_of_tokens_10() { return &___tokens_10; }
	inline void set_tokens_10(List_1_t2743948282 * value)
	{
		___tokens_10 = value;
		Il2CppCodeGenWriteBarrier(&___tokens_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
