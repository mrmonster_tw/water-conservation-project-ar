﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Security_T3373317487.h"

// System.ServiceModel.Security.Tokens.SslSecurityTokenProvider
struct SslSecurityTokenProvider_t2013392034;
// System.ServiceModel.Description.WSTrustSecurityTokenServiceProxy
struct WSTrustSecurityTokenServiceProxy_t2160987012;
// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t714049126;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.SslCommunicationObject
struct  SslCommunicationObject_t3237646940  : public ProviderCommunicationObject_t3373317487
{
public:
	// System.ServiceModel.Security.Tokens.SslSecurityTokenProvider System.ServiceModel.Security.Tokens.SslCommunicationObject::owner
	SslSecurityTokenProvider_t2013392034 * ___owner_15;
	// System.ServiceModel.Description.WSTrustSecurityTokenServiceProxy System.ServiceModel.Security.Tokens.SslCommunicationObject::proxy
	WSTrustSecurityTokenServiceProxy_t2160987012 * ___proxy_16;
	// System.Security.Cryptography.X509Certificates.X509Certificate2 System.ServiceModel.Security.Tokens.SslCommunicationObject::client_certificate
	X509Certificate2_t714049126 * ___client_certificate_17;

public:
	inline static int32_t get_offset_of_owner_15() { return static_cast<int32_t>(offsetof(SslCommunicationObject_t3237646940, ___owner_15)); }
	inline SslSecurityTokenProvider_t2013392034 * get_owner_15() const { return ___owner_15; }
	inline SslSecurityTokenProvider_t2013392034 ** get_address_of_owner_15() { return &___owner_15; }
	inline void set_owner_15(SslSecurityTokenProvider_t2013392034 * value)
	{
		___owner_15 = value;
		Il2CppCodeGenWriteBarrier(&___owner_15, value);
	}

	inline static int32_t get_offset_of_proxy_16() { return static_cast<int32_t>(offsetof(SslCommunicationObject_t3237646940, ___proxy_16)); }
	inline WSTrustSecurityTokenServiceProxy_t2160987012 * get_proxy_16() const { return ___proxy_16; }
	inline WSTrustSecurityTokenServiceProxy_t2160987012 ** get_address_of_proxy_16() { return &___proxy_16; }
	inline void set_proxy_16(WSTrustSecurityTokenServiceProxy_t2160987012 * value)
	{
		___proxy_16 = value;
		Il2CppCodeGenWriteBarrier(&___proxy_16, value);
	}

	inline static int32_t get_offset_of_client_certificate_17() { return static_cast<int32_t>(offsetof(SslCommunicationObject_t3237646940, ___client_certificate_17)); }
	inline X509Certificate2_t714049126 * get_client_certificate_17() const { return ___client_certificate_17; }
	inline X509Certificate2_t714049126 ** get_address_of_client_certificate_17() { return &___client_certificate_17; }
	inline void set_client_certificate_17(X509Certificate2_t714049126 * value)
	{
		___client_certificate_17 = value;
		Il2CppCodeGenWriteBarrier(&___client_certificate_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
