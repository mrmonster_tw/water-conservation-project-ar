﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeTypeMember1555525554.h"

// System.CodeDom.CodeTypeReferenceCollection
struct CodeTypeReferenceCollection_t3857551471;
// System.CodeDom.CodeTypeReference
struct CodeTypeReference_t3809997434;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeMemberEvent
struct  CodeMemberEvent_t1243791587  : public CodeTypeMember_t1555525554
{
public:
	// System.CodeDom.CodeTypeReferenceCollection System.CodeDom.CodeMemberEvent::implementationTypes
	CodeTypeReferenceCollection_t3857551471 * ___implementationTypes_8;
	// System.CodeDom.CodeTypeReference System.CodeDom.CodeMemberEvent::privateImplementationType
	CodeTypeReference_t3809997434 * ___privateImplementationType_9;
	// System.CodeDom.CodeTypeReference System.CodeDom.CodeMemberEvent::type
	CodeTypeReference_t3809997434 * ___type_10;

public:
	inline static int32_t get_offset_of_implementationTypes_8() { return static_cast<int32_t>(offsetof(CodeMemberEvent_t1243791587, ___implementationTypes_8)); }
	inline CodeTypeReferenceCollection_t3857551471 * get_implementationTypes_8() const { return ___implementationTypes_8; }
	inline CodeTypeReferenceCollection_t3857551471 ** get_address_of_implementationTypes_8() { return &___implementationTypes_8; }
	inline void set_implementationTypes_8(CodeTypeReferenceCollection_t3857551471 * value)
	{
		___implementationTypes_8 = value;
		Il2CppCodeGenWriteBarrier(&___implementationTypes_8, value);
	}

	inline static int32_t get_offset_of_privateImplementationType_9() { return static_cast<int32_t>(offsetof(CodeMemberEvent_t1243791587, ___privateImplementationType_9)); }
	inline CodeTypeReference_t3809997434 * get_privateImplementationType_9() const { return ___privateImplementationType_9; }
	inline CodeTypeReference_t3809997434 ** get_address_of_privateImplementationType_9() { return &___privateImplementationType_9; }
	inline void set_privateImplementationType_9(CodeTypeReference_t3809997434 * value)
	{
		___privateImplementationType_9 = value;
		Il2CppCodeGenWriteBarrier(&___privateImplementationType_9, value);
	}

	inline static int32_t get_offset_of_type_10() { return static_cast<int32_t>(offsetof(CodeMemberEvent_t1243791587, ___type_10)); }
	inline CodeTypeReference_t3809997434 * get_type_10() const { return ___type_10; }
	inline CodeTypeReference_t3809997434 ** get_address_of_type_10() { return &___type_10; }
	inline void set_type_10(CodeTypeReference_t3809997434 * value)
	{
		___type_10 = value;
		Il2CppCodeGenWriteBarrier(&___type_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
