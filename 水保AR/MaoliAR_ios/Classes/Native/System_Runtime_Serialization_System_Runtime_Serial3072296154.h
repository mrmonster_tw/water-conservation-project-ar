﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"

// System.String
struct String_t;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.EnumMemberInfo
struct  EnumMemberInfo_t3072296154 
{
public:
	// System.String System.Runtime.Serialization.EnumMemberInfo::XmlName
	String_t* ___XmlName_0;
	// System.Object System.Runtime.Serialization.EnumMemberInfo::Value
	Il2CppObject * ___Value_1;

public:
	inline static int32_t get_offset_of_XmlName_0() { return static_cast<int32_t>(offsetof(EnumMemberInfo_t3072296154, ___XmlName_0)); }
	inline String_t* get_XmlName_0() const { return ___XmlName_0; }
	inline String_t** get_address_of_XmlName_0() { return &___XmlName_0; }
	inline void set_XmlName_0(String_t* value)
	{
		___XmlName_0 = value;
		Il2CppCodeGenWriteBarrier(&___XmlName_0, value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(EnumMemberInfo_t3072296154, ___Value_1)); }
	inline Il2CppObject * get_Value_1() const { return ___Value_1; }
	inline Il2CppObject ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(Il2CppObject * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier(&___Value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Serialization.EnumMemberInfo
struct EnumMemberInfo_t3072296154_marshaled_pinvoke
{
	char* ___XmlName_0;
	Il2CppIUnknown* ___Value_1;
};
// Native definition for COM marshalling of System.Runtime.Serialization.EnumMemberInfo
struct EnumMemberInfo_t3072296154_marshaled_com
{
	Il2CppChar* ___XmlName_0;
	Il2CppIUnknown* ___Value_1;
};
