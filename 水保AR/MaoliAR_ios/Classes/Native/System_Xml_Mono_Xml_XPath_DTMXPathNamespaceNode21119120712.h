﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.XPath.DTMXPathNamespaceNode2
struct  DTMXPathNamespaceNode2_t1119120712 
{
public:
	// System.Int32 Mono.Xml.XPath.DTMXPathNamespaceNode2::DeclaredElement
	int32_t ___DeclaredElement_0;
	// System.Int32 Mono.Xml.XPath.DTMXPathNamespaceNode2::NextNamespace
	int32_t ___NextNamespace_1;
	// System.Int32 Mono.Xml.XPath.DTMXPathNamespaceNode2::Name
	int32_t ___Name_2;
	// System.Int32 Mono.Xml.XPath.DTMXPathNamespaceNode2::Namespace
	int32_t ___Namespace_3;

public:
	inline static int32_t get_offset_of_DeclaredElement_0() { return static_cast<int32_t>(offsetof(DTMXPathNamespaceNode2_t1119120712, ___DeclaredElement_0)); }
	inline int32_t get_DeclaredElement_0() const { return ___DeclaredElement_0; }
	inline int32_t* get_address_of_DeclaredElement_0() { return &___DeclaredElement_0; }
	inline void set_DeclaredElement_0(int32_t value)
	{
		___DeclaredElement_0 = value;
	}

	inline static int32_t get_offset_of_NextNamespace_1() { return static_cast<int32_t>(offsetof(DTMXPathNamespaceNode2_t1119120712, ___NextNamespace_1)); }
	inline int32_t get_NextNamespace_1() const { return ___NextNamespace_1; }
	inline int32_t* get_address_of_NextNamespace_1() { return &___NextNamespace_1; }
	inline void set_NextNamespace_1(int32_t value)
	{
		___NextNamespace_1 = value;
	}

	inline static int32_t get_offset_of_Name_2() { return static_cast<int32_t>(offsetof(DTMXPathNamespaceNode2_t1119120712, ___Name_2)); }
	inline int32_t get_Name_2() const { return ___Name_2; }
	inline int32_t* get_address_of_Name_2() { return &___Name_2; }
	inline void set_Name_2(int32_t value)
	{
		___Name_2 = value;
	}

	inline static int32_t get_offset_of_Namespace_3() { return static_cast<int32_t>(offsetof(DTMXPathNamespaceNode2_t1119120712, ___Namespace_3)); }
	inline int32_t get_Namespace_3() const { return ___Namespace_3; }
	inline int32_t* get_address_of_Namespace_3() { return &___Namespace_3; }
	inline void set_Namespace_3(int32_t value)
	{
		___Namespace_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
