﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_TextWriter3478189236.h"

// System.IO.TextWriter
struct TextWriter_t3478189236;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.Compiler.IndentedTextWriter
struct  IndentedTextWriter_t2995409896  : public TextWriter_t3478189236
{
public:
	// System.IO.TextWriter System.CodeDom.Compiler.IndentedTextWriter::writer
	TextWriter_t3478189236 * ___writer_4;
	// System.String System.CodeDom.Compiler.IndentedTextWriter::tabString
	String_t* ___tabString_5;
	// System.Int32 System.CodeDom.Compiler.IndentedTextWriter::indent
	int32_t ___indent_6;
	// System.Boolean System.CodeDom.Compiler.IndentedTextWriter::newline
	bool ___newline_7;

public:
	inline static int32_t get_offset_of_writer_4() { return static_cast<int32_t>(offsetof(IndentedTextWriter_t2995409896, ___writer_4)); }
	inline TextWriter_t3478189236 * get_writer_4() const { return ___writer_4; }
	inline TextWriter_t3478189236 ** get_address_of_writer_4() { return &___writer_4; }
	inline void set_writer_4(TextWriter_t3478189236 * value)
	{
		___writer_4 = value;
		Il2CppCodeGenWriteBarrier(&___writer_4, value);
	}

	inline static int32_t get_offset_of_tabString_5() { return static_cast<int32_t>(offsetof(IndentedTextWriter_t2995409896, ___tabString_5)); }
	inline String_t* get_tabString_5() const { return ___tabString_5; }
	inline String_t** get_address_of_tabString_5() { return &___tabString_5; }
	inline void set_tabString_5(String_t* value)
	{
		___tabString_5 = value;
		Il2CppCodeGenWriteBarrier(&___tabString_5, value);
	}

	inline static int32_t get_offset_of_indent_6() { return static_cast<int32_t>(offsetof(IndentedTextWriter_t2995409896, ___indent_6)); }
	inline int32_t get_indent_6() const { return ___indent_6; }
	inline int32_t* get_address_of_indent_6() { return &___indent_6; }
	inline void set_indent_6(int32_t value)
	{
		___indent_6 = value;
	}

	inline static int32_t get_offset_of_newline_7() { return static_cast<int32_t>(offsetof(IndentedTextWriter_t2995409896, ___newline_7)); }
	inline bool get_newline_7() const { return ___newline_7; }
	inline bool* get_address_of_newline_7() { return &___newline_7; }
	inline void set_newline_7(bool value)
	{
		___newline_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
