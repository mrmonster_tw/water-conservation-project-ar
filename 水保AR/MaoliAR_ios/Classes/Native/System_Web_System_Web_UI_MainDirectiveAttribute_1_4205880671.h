﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.MainDirectiveAttribute`1<System.Object>
struct  MainDirectiveAttribute_1_t4205880671  : public Il2CppObject
{
public:
	// System.String System.Web.UI.MainDirectiveAttribute`1::unparsedValue
	String_t* ___unparsedValue_0;
	// T System.Web.UI.MainDirectiveAttribute`1::value
	Il2CppObject * ___value_1;
	// System.Boolean System.Web.UI.MainDirectiveAttribute`1::isExpression
	bool ___isExpression_2;

public:
	inline static int32_t get_offset_of_unparsedValue_0() { return static_cast<int32_t>(offsetof(MainDirectiveAttribute_1_t4205880671, ___unparsedValue_0)); }
	inline String_t* get_unparsedValue_0() const { return ___unparsedValue_0; }
	inline String_t** get_address_of_unparsedValue_0() { return &___unparsedValue_0; }
	inline void set_unparsedValue_0(String_t* value)
	{
		___unparsedValue_0 = value;
		Il2CppCodeGenWriteBarrier(&___unparsedValue_0, value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(MainDirectiveAttribute_1_t4205880671, ___value_1)); }
	inline Il2CppObject * get_value_1() const { return ___value_1; }
	inline Il2CppObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Il2CppObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}

	inline static int32_t get_offset_of_isExpression_2() { return static_cast<int32_t>(offsetof(MainDirectiveAttribute_1_t4205880671, ___isExpression_2)); }
	inline bool get_isExpression_2() const { return ___isExpression_2; }
	inline bool* get_address_of_isExpression_2() { return &___isExpression_2; }
	inline void set_isExpression_2(bool value)
	{
		___isExpression_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
