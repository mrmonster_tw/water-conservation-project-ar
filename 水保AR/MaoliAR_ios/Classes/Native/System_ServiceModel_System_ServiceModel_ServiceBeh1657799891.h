﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"
#include "System_ServiceModel_System_ServiceModel_AddressFil2335829274.h"
#include "System_ServiceModel_System_ServiceModel_InstanceCon780954075.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.ServiceBehaviorAttribute
struct  ServiceBehaviorAttribute_t1657799891  : public Attribute_t861562559
{
public:
	// System.Object System.ServiceModel.ServiceBehaviorAttribute::singleton
	Il2CppObject * ___singleton_0;
	// System.ServiceModel.AddressFilterMode System.ServiceModel.ServiceBehaviorAttribute::<AddressFilterMode>k__BackingField
	int32_t ___U3CAddressFilterModeU3Ek__BackingField_1;
	// System.ServiceModel.InstanceContextMode System.ServiceModel.ServiceBehaviorAttribute::<InstanceContextMode>k__BackingField
	int32_t ___U3CInstanceContextModeU3Ek__BackingField_2;
	// System.Boolean System.ServiceModel.ServiceBehaviorAttribute::<IncludeExceptionDetailInFaults>k__BackingField
	bool ___U3CIncludeExceptionDetailInFaultsU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_singleton_0() { return static_cast<int32_t>(offsetof(ServiceBehaviorAttribute_t1657799891, ___singleton_0)); }
	inline Il2CppObject * get_singleton_0() const { return ___singleton_0; }
	inline Il2CppObject ** get_address_of_singleton_0() { return &___singleton_0; }
	inline void set_singleton_0(Il2CppObject * value)
	{
		___singleton_0 = value;
		Il2CppCodeGenWriteBarrier(&___singleton_0, value);
	}

	inline static int32_t get_offset_of_U3CAddressFilterModeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ServiceBehaviorAttribute_t1657799891, ___U3CAddressFilterModeU3Ek__BackingField_1)); }
	inline int32_t get_U3CAddressFilterModeU3Ek__BackingField_1() const { return ___U3CAddressFilterModeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CAddressFilterModeU3Ek__BackingField_1() { return &___U3CAddressFilterModeU3Ek__BackingField_1; }
	inline void set_U3CAddressFilterModeU3Ek__BackingField_1(int32_t value)
	{
		___U3CAddressFilterModeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CInstanceContextModeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ServiceBehaviorAttribute_t1657799891, ___U3CInstanceContextModeU3Ek__BackingField_2)); }
	inline int32_t get_U3CInstanceContextModeU3Ek__BackingField_2() const { return ___U3CInstanceContextModeU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CInstanceContextModeU3Ek__BackingField_2() { return &___U3CInstanceContextModeU3Ek__BackingField_2; }
	inline void set_U3CInstanceContextModeU3Ek__BackingField_2(int32_t value)
	{
		___U3CInstanceContextModeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CIncludeExceptionDetailInFaultsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ServiceBehaviorAttribute_t1657799891, ___U3CIncludeExceptionDetailInFaultsU3Ek__BackingField_3)); }
	inline bool get_U3CIncludeExceptionDetailInFaultsU3Ek__BackingField_3() const { return ___U3CIncludeExceptionDetailInFaultsU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIncludeExceptionDetailInFaultsU3Ek__BackingField_3() { return &___U3CIncludeExceptionDetailInFaultsU3Ek__BackingField_3; }
	inline void set_U3CIncludeExceptionDetailInFaultsU3Ek__BackingField_3(bool value)
	{
		___U3CIncludeExceptionDetailInFaultsU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
