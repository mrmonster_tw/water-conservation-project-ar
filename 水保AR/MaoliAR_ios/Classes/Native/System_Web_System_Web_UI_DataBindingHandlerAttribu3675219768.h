﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"

// System.String
struct String_t;
// System.Web.UI.DataBindingHandlerAttribute
struct DataBindingHandlerAttribute_t3675219768;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.DataBindingHandlerAttribute
struct  DataBindingHandlerAttribute_t3675219768  : public Attribute_t861562559
{
public:
	// System.String System.Web.UI.DataBindingHandlerAttribute::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(DataBindingHandlerAttribute_t3675219768, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}
};

struct DataBindingHandlerAttribute_t3675219768_StaticFields
{
public:
	// System.Web.UI.DataBindingHandlerAttribute System.Web.UI.DataBindingHandlerAttribute::Default
	DataBindingHandlerAttribute_t3675219768 * ___Default_1;

public:
	inline static int32_t get_offset_of_Default_1() { return static_cast<int32_t>(offsetof(DataBindingHandlerAttribute_t3675219768_StaticFields, ___Default_1)); }
	inline DataBindingHandlerAttribute_t3675219768 * get_Default_1() const { return ___Default_1; }
	inline DataBindingHandlerAttribute_t3675219768 ** get_address_of_Default_1() { return &___Default_1; }
	inline void set_Default_1(DataBindingHandlerAttribute_t3675219768 * value)
	{
		___Default_1 = value;
		Il2CppCodeGenWriteBarrier(&___Default_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
