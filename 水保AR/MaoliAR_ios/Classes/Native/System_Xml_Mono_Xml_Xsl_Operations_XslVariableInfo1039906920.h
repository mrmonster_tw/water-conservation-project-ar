﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2760654312;
// System.Xml.XPath.XPathExpression
struct XPathExpression_t1723793351;
// Mono.Xml.Xsl.Operations.XslOperation
struct XslOperation_t2153241355;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslVariableInformation
struct  XslVariableInformation_t1039906920  : public Il2CppObject
{
public:
	// System.Xml.XmlQualifiedName Mono.Xml.Xsl.Operations.XslVariableInformation::name
	XmlQualifiedName_t2760654312 * ___name_0;
	// System.Xml.XPath.XPathExpression Mono.Xml.Xsl.Operations.XslVariableInformation::select
	XPathExpression_t1723793351 * ___select_1;
	// Mono.Xml.Xsl.Operations.XslOperation Mono.Xml.Xsl.Operations.XslVariableInformation::content
	XslOperation_t2153241355 * ___content_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(XslVariableInformation_t1039906920, ___name_0)); }
	inline XmlQualifiedName_t2760654312 * get_name_0() const { return ___name_0; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(XmlQualifiedName_t2760654312 * value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_select_1() { return static_cast<int32_t>(offsetof(XslVariableInformation_t1039906920, ___select_1)); }
	inline XPathExpression_t1723793351 * get_select_1() const { return ___select_1; }
	inline XPathExpression_t1723793351 ** get_address_of_select_1() { return &___select_1; }
	inline void set_select_1(XPathExpression_t1723793351 * value)
	{
		___select_1 = value;
		Il2CppCodeGenWriteBarrier(&___select_1, value);
	}

	inline static int32_t get_offset_of_content_2() { return static_cast<int32_t>(offsetof(XslVariableInformation_t1039906920, ___content_2)); }
	inline XslOperation_t2153241355 * get_content_2() const { return ___content_2; }
	inline XslOperation_t2153241355 ** get_address_of_content_2() { return &___content_2; }
	inline void set_content_2(XslOperation_t2153241355 * value)
	{
		___content_2 = value;
		Il2CppCodeGenWriteBarrier(&___content_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
