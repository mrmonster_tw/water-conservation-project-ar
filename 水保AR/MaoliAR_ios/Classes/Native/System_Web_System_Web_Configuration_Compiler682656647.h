﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.Compiler
struct  Compiler_t682656647  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct Compiler_t682656647_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Web.Configuration.Compiler::compilerOptionsProp
	ConfigurationProperty_t3590861854 * ___compilerOptionsProp_13;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.Compiler::extensionProp
	ConfigurationProperty_t3590861854 * ___extensionProp_14;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.Compiler::languageProp
	ConfigurationProperty_t3590861854 * ___languageProp_15;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.Compiler::typeProp
	ConfigurationProperty_t3590861854 * ___typeProp_16;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.Compiler::warningLevelProp
	ConfigurationProperty_t3590861854 * ___warningLevelProp_17;
	// System.Configuration.ConfigurationPropertyCollection System.Web.Configuration.Compiler::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_18;

public:
	inline static int32_t get_offset_of_compilerOptionsProp_13() { return static_cast<int32_t>(offsetof(Compiler_t682656647_StaticFields, ___compilerOptionsProp_13)); }
	inline ConfigurationProperty_t3590861854 * get_compilerOptionsProp_13() const { return ___compilerOptionsProp_13; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_compilerOptionsProp_13() { return &___compilerOptionsProp_13; }
	inline void set_compilerOptionsProp_13(ConfigurationProperty_t3590861854 * value)
	{
		___compilerOptionsProp_13 = value;
		Il2CppCodeGenWriteBarrier(&___compilerOptionsProp_13, value);
	}

	inline static int32_t get_offset_of_extensionProp_14() { return static_cast<int32_t>(offsetof(Compiler_t682656647_StaticFields, ___extensionProp_14)); }
	inline ConfigurationProperty_t3590861854 * get_extensionProp_14() const { return ___extensionProp_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_extensionProp_14() { return &___extensionProp_14; }
	inline void set_extensionProp_14(ConfigurationProperty_t3590861854 * value)
	{
		___extensionProp_14 = value;
		Il2CppCodeGenWriteBarrier(&___extensionProp_14, value);
	}

	inline static int32_t get_offset_of_languageProp_15() { return static_cast<int32_t>(offsetof(Compiler_t682656647_StaticFields, ___languageProp_15)); }
	inline ConfigurationProperty_t3590861854 * get_languageProp_15() const { return ___languageProp_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_languageProp_15() { return &___languageProp_15; }
	inline void set_languageProp_15(ConfigurationProperty_t3590861854 * value)
	{
		___languageProp_15 = value;
		Il2CppCodeGenWriteBarrier(&___languageProp_15, value);
	}

	inline static int32_t get_offset_of_typeProp_16() { return static_cast<int32_t>(offsetof(Compiler_t682656647_StaticFields, ___typeProp_16)); }
	inline ConfigurationProperty_t3590861854 * get_typeProp_16() const { return ___typeProp_16; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_typeProp_16() { return &___typeProp_16; }
	inline void set_typeProp_16(ConfigurationProperty_t3590861854 * value)
	{
		___typeProp_16 = value;
		Il2CppCodeGenWriteBarrier(&___typeProp_16, value);
	}

	inline static int32_t get_offset_of_warningLevelProp_17() { return static_cast<int32_t>(offsetof(Compiler_t682656647_StaticFields, ___warningLevelProp_17)); }
	inline ConfigurationProperty_t3590861854 * get_warningLevelProp_17() const { return ___warningLevelProp_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_warningLevelProp_17() { return &___warningLevelProp_17; }
	inline void set_warningLevelProp_17(ConfigurationProperty_t3590861854 * value)
	{
		___warningLevelProp_17 = value;
		Il2CppCodeGenWriteBarrier(&___warningLevelProp_17, value);
	}

	inline static int32_t get_offset_of_properties_18() { return static_cast<int32_t>(offsetof(Compiler_t682656647_StaticFields, ___properties_18)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_18() const { return ___properties_18; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_18() { return &___properties_18; }
	inline void set_properties_18(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_18 = value;
		Il2CppCodeGenWriteBarrier(&___properties_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
