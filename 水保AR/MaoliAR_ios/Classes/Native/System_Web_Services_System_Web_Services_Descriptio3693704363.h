﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_Services_System_Web_Services_Descriptio2466402210.h"

// System.Web.Services.Description.BindingCollection
struct BindingCollection_t274233127;
// System.Web.Services.Description.ServiceDescriptionFormatExtensionCollection
struct ServiceDescriptionFormatExtensionCollection_t2630153888;
// System.Web.Services.Description.ImportCollection
struct ImportCollection_t2249231492;
// System.Web.Services.Description.MessageCollection
struct MessageCollection_t1124893136;
// System.Web.Services.Description.PortTypeCollection
struct PortTypeCollection_t2738291908;
// System.Web.Services.Description.ServiceCollection
struct ServiceCollection_t3495097492;
// System.String
struct String_t;
// System.Web.Services.Description.Types
struct Types_t1892683773;
// System.Web.Services.Description.ServiceDescription/ServiceDescriptionSerializer
struct ServiceDescriptionSerializer_t4141433730;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.ServiceDescription
struct  ServiceDescription_t3693704363  : public NamedItem_t2466402210
{
public:
	// System.Web.Services.Description.BindingCollection System.Web.Services.Description.ServiceDescription::bindings
	BindingCollection_t274233127 * ___bindings_4;
	// System.Web.Services.Description.ServiceDescriptionFormatExtensionCollection System.Web.Services.Description.ServiceDescription::extensions
	ServiceDescriptionFormatExtensionCollection_t2630153888 * ___extensions_5;
	// System.Web.Services.Description.ImportCollection System.Web.Services.Description.ServiceDescription::imports
	ImportCollection_t2249231492 * ___imports_6;
	// System.Web.Services.Description.MessageCollection System.Web.Services.Description.ServiceDescription::messages
	MessageCollection_t1124893136 * ___messages_7;
	// System.Web.Services.Description.PortTypeCollection System.Web.Services.Description.ServiceDescription::portTypes
	PortTypeCollection_t2738291908 * ___portTypes_8;
	// System.Web.Services.Description.ServiceCollection System.Web.Services.Description.ServiceDescription::services
	ServiceCollection_t3495097492 * ___services_9;
	// System.String System.Web.Services.Description.ServiceDescription::targetNamespace
	String_t* ___targetNamespace_10;
	// System.Web.Services.Description.Types System.Web.Services.Description.ServiceDescription::types
	Types_t1892683773 * ___types_11;

public:
	inline static int32_t get_offset_of_bindings_4() { return static_cast<int32_t>(offsetof(ServiceDescription_t3693704363, ___bindings_4)); }
	inline BindingCollection_t274233127 * get_bindings_4() const { return ___bindings_4; }
	inline BindingCollection_t274233127 ** get_address_of_bindings_4() { return &___bindings_4; }
	inline void set_bindings_4(BindingCollection_t274233127 * value)
	{
		___bindings_4 = value;
		Il2CppCodeGenWriteBarrier(&___bindings_4, value);
	}

	inline static int32_t get_offset_of_extensions_5() { return static_cast<int32_t>(offsetof(ServiceDescription_t3693704363, ___extensions_5)); }
	inline ServiceDescriptionFormatExtensionCollection_t2630153888 * get_extensions_5() const { return ___extensions_5; }
	inline ServiceDescriptionFormatExtensionCollection_t2630153888 ** get_address_of_extensions_5() { return &___extensions_5; }
	inline void set_extensions_5(ServiceDescriptionFormatExtensionCollection_t2630153888 * value)
	{
		___extensions_5 = value;
		Il2CppCodeGenWriteBarrier(&___extensions_5, value);
	}

	inline static int32_t get_offset_of_imports_6() { return static_cast<int32_t>(offsetof(ServiceDescription_t3693704363, ___imports_6)); }
	inline ImportCollection_t2249231492 * get_imports_6() const { return ___imports_6; }
	inline ImportCollection_t2249231492 ** get_address_of_imports_6() { return &___imports_6; }
	inline void set_imports_6(ImportCollection_t2249231492 * value)
	{
		___imports_6 = value;
		Il2CppCodeGenWriteBarrier(&___imports_6, value);
	}

	inline static int32_t get_offset_of_messages_7() { return static_cast<int32_t>(offsetof(ServiceDescription_t3693704363, ___messages_7)); }
	inline MessageCollection_t1124893136 * get_messages_7() const { return ___messages_7; }
	inline MessageCollection_t1124893136 ** get_address_of_messages_7() { return &___messages_7; }
	inline void set_messages_7(MessageCollection_t1124893136 * value)
	{
		___messages_7 = value;
		Il2CppCodeGenWriteBarrier(&___messages_7, value);
	}

	inline static int32_t get_offset_of_portTypes_8() { return static_cast<int32_t>(offsetof(ServiceDescription_t3693704363, ___portTypes_8)); }
	inline PortTypeCollection_t2738291908 * get_portTypes_8() const { return ___portTypes_8; }
	inline PortTypeCollection_t2738291908 ** get_address_of_portTypes_8() { return &___portTypes_8; }
	inline void set_portTypes_8(PortTypeCollection_t2738291908 * value)
	{
		___portTypes_8 = value;
		Il2CppCodeGenWriteBarrier(&___portTypes_8, value);
	}

	inline static int32_t get_offset_of_services_9() { return static_cast<int32_t>(offsetof(ServiceDescription_t3693704363, ___services_9)); }
	inline ServiceCollection_t3495097492 * get_services_9() const { return ___services_9; }
	inline ServiceCollection_t3495097492 ** get_address_of_services_9() { return &___services_9; }
	inline void set_services_9(ServiceCollection_t3495097492 * value)
	{
		___services_9 = value;
		Il2CppCodeGenWriteBarrier(&___services_9, value);
	}

	inline static int32_t get_offset_of_targetNamespace_10() { return static_cast<int32_t>(offsetof(ServiceDescription_t3693704363, ___targetNamespace_10)); }
	inline String_t* get_targetNamespace_10() const { return ___targetNamespace_10; }
	inline String_t** get_address_of_targetNamespace_10() { return &___targetNamespace_10; }
	inline void set_targetNamespace_10(String_t* value)
	{
		___targetNamespace_10 = value;
		Il2CppCodeGenWriteBarrier(&___targetNamespace_10, value);
	}

	inline static int32_t get_offset_of_types_11() { return static_cast<int32_t>(offsetof(ServiceDescription_t3693704363, ___types_11)); }
	inline Types_t1892683773 * get_types_11() const { return ___types_11; }
	inline Types_t1892683773 ** get_address_of_types_11() { return &___types_11; }
	inline void set_types_11(Types_t1892683773 * value)
	{
		___types_11 = value;
		Il2CppCodeGenWriteBarrier(&___types_11, value);
	}
};

struct ServiceDescription_t3693704363_StaticFields
{
public:
	// System.Web.Services.Description.ServiceDescription/ServiceDescriptionSerializer System.Web.Services.Description.ServiceDescription::serializer
	ServiceDescriptionSerializer_t4141433730 * ___serializer_12;

public:
	inline static int32_t get_offset_of_serializer_12() { return static_cast<int32_t>(offsetof(ServiceDescription_t3693704363_StaticFields, ___serializer_12)); }
	inline ServiceDescriptionSerializer_t4141433730 * get_serializer_12() const { return ___serializer_12; }
	inline ServiceDescriptionSerializer_t4141433730 ** get_address_of_serializer_12() { return &___serializer_12; }
	inline void set_serializer_12(ServiceDescriptionSerializer_t4141433730 * value)
	{
		___serializer_12 = value;
		Il2CppCodeGenWriteBarrier(&___serializer_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
