﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_WebControls_WebControl2705811671.h"

// System.Web.UI.WebControls.TableRowCollection
struct TableRowCollection_t3754565154;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.WebControls.Table
struct  Table_t574440801  : public WebControl_t2705811671
{
public:
	// System.Web.UI.WebControls.TableRowCollection System.Web.UI.WebControls.Table::rows
	TableRowCollection_t3754565154 * ___rows_36;
	// System.Boolean System.Web.UI.WebControls.Table::generateTableSections
	bool ___generateTableSections_37;

public:
	inline static int32_t get_offset_of_rows_36() { return static_cast<int32_t>(offsetof(Table_t574440801, ___rows_36)); }
	inline TableRowCollection_t3754565154 * get_rows_36() const { return ___rows_36; }
	inline TableRowCollection_t3754565154 ** get_address_of_rows_36() { return &___rows_36; }
	inline void set_rows_36(TableRowCollection_t3754565154 * value)
	{
		___rows_36 = value;
		Il2CppCodeGenWriteBarrier(&___rows_36, value);
	}

	inline static int32_t get_offset_of_generateTableSections_37() { return static_cast<int32_t>(offsetof(Table_t574440801, ___generateTableSections_37)); }
	inline bool get_generateTableSections_37() const { return ___generateTableSections_37; }
	inline bool* get_address_of_generateTableSections_37() { return &___generateTableSections_37; }
	inline void set_generateTableSections_37(bool value)
	{
		___generateTableSections_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
