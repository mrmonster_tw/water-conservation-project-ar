﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Collections.ArrayList
struct ArrayList_t2718874744;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeNamespaceImportCollection
struct  CodeNamespaceImportCollection_t3818670277  : public Il2CppObject
{
public:
	// System.Collections.Hashtable System.CodeDom.CodeNamespaceImportCollection::keys
	Hashtable_t1853889766 * ___keys_0;
	// System.Collections.ArrayList System.CodeDom.CodeNamespaceImportCollection::data
	ArrayList_t2718874744 * ___data_1;

public:
	inline static int32_t get_offset_of_keys_0() { return static_cast<int32_t>(offsetof(CodeNamespaceImportCollection_t3818670277, ___keys_0)); }
	inline Hashtable_t1853889766 * get_keys_0() const { return ___keys_0; }
	inline Hashtable_t1853889766 ** get_address_of_keys_0() { return &___keys_0; }
	inline void set_keys_0(Hashtable_t1853889766 * value)
	{
		___keys_0 = value;
		Il2CppCodeGenWriteBarrier(&___keys_0, value);
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(CodeNamespaceImportCollection_t3818670277, ___data_1)); }
	inline ArrayList_t2718874744 * get_data_1() const { return ___data_1; }
	inline ArrayList_t2718874744 ** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(ArrayList_t2718874744 * value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier(&___data_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
