﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Channels.TransportBindingElement
struct TransportBindingElement_t2144904149;
// System.ServiceModel.Channels.MessageEncoder
struct MessageEncoder_t3063398011;
// System.Xml.XmlDictionaryReaderQuotas
struct XmlDictionaryReaderQuotas_t173030297;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.TcpChannelInfo
struct  TcpChannelInfo_t709592533  : public Il2CppObject
{
public:
	// System.ServiceModel.Channels.TransportBindingElement System.ServiceModel.Channels.TcpChannelInfo::<BindingElement>k__BackingField
	TransportBindingElement_t2144904149 * ___U3CBindingElementU3Ek__BackingField_0;
	// System.ServiceModel.Channels.MessageEncoder System.ServiceModel.Channels.TcpChannelInfo::<MessageEncoder>k__BackingField
	MessageEncoder_t3063398011 * ___U3CMessageEncoderU3Ek__BackingField_1;
	// System.Xml.XmlDictionaryReaderQuotas System.ServiceModel.Channels.TcpChannelInfo::<ReaderQuotas>k__BackingField
	XmlDictionaryReaderQuotas_t173030297 * ___U3CReaderQuotasU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CBindingElementU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TcpChannelInfo_t709592533, ___U3CBindingElementU3Ek__BackingField_0)); }
	inline TransportBindingElement_t2144904149 * get_U3CBindingElementU3Ek__BackingField_0() const { return ___U3CBindingElementU3Ek__BackingField_0; }
	inline TransportBindingElement_t2144904149 ** get_address_of_U3CBindingElementU3Ek__BackingField_0() { return &___U3CBindingElementU3Ek__BackingField_0; }
	inline void set_U3CBindingElementU3Ek__BackingField_0(TransportBindingElement_t2144904149 * value)
	{
		___U3CBindingElementU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CBindingElementU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CMessageEncoderU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TcpChannelInfo_t709592533, ___U3CMessageEncoderU3Ek__BackingField_1)); }
	inline MessageEncoder_t3063398011 * get_U3CMessageEncoderU3Ek__BackingField_1() const { return ___U3CMessageEncoderU3Ek__BackingField_1; }
	inline MessageEncoder_t3063398011 ** get_address_of_U3CMessageEncoderU3Ek__BackingField_1() { return &___U3CMessageEncoderU3Ek__BackingField_1; }
	inline void set_U3CMessageEncoderU3Ek__BackingField_1(MessageEncoder_t3063398011 * value)
	{
		___U3CMessageEncoderU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMessageEncoderU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CReaderQuotasU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TcpChannelInfo_t709592533, ___U3CReaderQuotasU3Ek__BackingField_2)); }
	inline XmlDictionaryReaderQuotas_t173030297 * get_U3CReaderQuotasU3Ek__BackingField_2() const { return ___U3CReaderQuotasU3Ek__BackingField_2; }
	inline XmlDictionaryReaderQuotas_t173030297 ** get_address_of_U3CReaderQuotasU3Ek__BackingField_2() { return &___U3CReaderQuotasU3Ek__BackingField_2; }
	inline void set_U3CReaderQuotasU3Ek__BackingField_2(XmlDictionaryReaderQuotas_t173030297 * value)
	{
		___U3CReaderQuotasU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CReaderQuotasU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
