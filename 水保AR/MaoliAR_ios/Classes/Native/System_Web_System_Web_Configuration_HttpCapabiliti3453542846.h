﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.BitArray
struct BitArray_t4087883509;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Collections.IDictionary
struct IDictionary_t1363984059;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.HttpCapabilitiesBase
struct  HttpCapabilitiesBase_t3453542846  : public Il2CppObject
{
public:
	// System.Collections.BitArray System.Web.Configuration.HttpCapabilitiesBase::flags
	BitArray_t4087883509 * ___flags_0;
	// System.Type System.Web.Configuration.HttpCapabilitiesBase::tagWriter
	Type_t * ___tagWriter_1;
	// System.String System.Web.Configuration.HttpCapabilitiesBase::useragent
	String_t* ___useragent_2;
	// System.Collections.IDictionary System.Web.Configuration.HttpCapabilitiesBase::capabilities
	Il2CppObject * ___capabilities_3;
	// System.Collections.IDictionary System.Web.Configuration.HttpCapabilitiesBase::adapters
	Il2CppObject * ___adapters_5;

public:
	inline static int32_t get_offset_of_flags_0() { return static_cast<int32_t>(offsetof(HttpCapabilitiesBase_t3453542846, ___flags_0)); }
	inline BitArray_t4087883509 * get_flags_0() const { return ___flags_0; }
	inline BitArray_t4087883509 ** get_address_of_flags_0() { return &___flags_0; }
	inline void set_flags_0(BitArray_t4087883509 * value)
	{
		___flags_0 = value;
		Il2CppCodeGenWriteBarrier(&___flags_0, value);
	}

	inline static int32_t get_offset_of_tagWriter_1() { return static_cast<int32_t>(offsetof(HttpCapabilitiesBase_t3453542846, ___tagWriter_1)); }
	inline Type_t * get_tagWriter_1() const { return ___tagWriter_1; }
	inline Type_t ** get_address_of_tagWriter_1() { return &___tagWriter_1; }
	inline void set_tagWriter_1(Type_t * value)
	{
		___tagWriter_1 = value;
		Il2CppCodeGenWriteBarrier(&___tagWriter_1, value);
	}

	inline static int32_t get_offset_of_useragent_2() { return static_cast<int32_t>(offsetof(HttpCapabilitiesBase_t3453542846, ___useragent_2)); }
	inline String_t* get_useragent_2() const { return ___useragent_2; }
	inline String_t** get_address_of_useragent_2() { return &___useragent_2; }
	inline void set_useragent_2(String_t* value)
	{
		___useragent_2 = value;
		Il2CppCodeGenWriteBarrier(&___useragent_2, value);
	}

	inline static int32_t get_offset_of_capabilities_3() { return static_cast<int32_t>(offsetof(HttpCapabilitiesBase_t3453542846, ___capabilities_3)); }
	inline Il2CppObject * get_capabilities_3() const { return ___capabilities_3; }
	inline Il2CppObject ** get_address_of_capabilities_3() { return &___capabilities_3; }
	inline void set_capabilities_3(Il2CppObject * value)
	{
		___capabilities_3 = value;
		Il2CppCodeGenWriteBarrier(&___capabilities_3, value);
	}

	inline static int32_t get_offset_of_adapters_5() { return static_cast<int32_t>(offsetof(HttpCapabilitiesBase_t3453542846, ___adapters_5)); }
	inline Il2CppObject * get_adapters_5() const { return ___adapters_5; }
	inline Il2CppObject ** get_address_of_adapters_5() { return &___adapters_5; }
	inline void set_adapters_5(Il2CppObject * value)
	{
		___adapters_5 = value;
		Il2CppCodeGenWriteBarrier(&___adapters_5, value);
	}
};

struct HttpCapabilitiesBase_t3453542846_StaticFields
{
public:
	// System.Boolean System.Web.Configuration.HttpCapabilitiesBase::GetConfigCapabilities_called
	bool ___GetConfigCapabilities_called_4;

public:
	inline static int32_t get_offset_of_GetConfigCapabilities_called_4() { return static_cast<int32_t>(offsetof(HttpCapabilitiesBase_t3453542846_StaticFields, ___GetConfigCapabilities_called_4)); }
	inline bool get_GetConfigCapabilities_called_4() const { return ___GetConfigCapabilities_called_4; }
	inline bool* get_address_of_GetConfigCapabilities_called_4() { return &___GetConfigCapabilities_called_4; }
	inline void set_GetConfigCapabilities_called_4(bool value)
	{
		___GetConfigCapabilities_called_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
