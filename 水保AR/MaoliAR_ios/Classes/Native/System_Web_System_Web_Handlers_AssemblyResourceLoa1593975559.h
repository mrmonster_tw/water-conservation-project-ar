﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Reflection.Assembly
struct Assembly_t4102432799;
// System.Text.RegularExpressions.Regex
struct Regex_t3657309853;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Handlers.AssemblyResourceLoader/PerformSubstitutionHelper
struct  PerformSubstitutionHelper_t1593975559  : public Il2CppObject
{
public:
	// System.Reflection.Assembly System.Web.Handlers.AssemblyResourceLoader/PerformSubstitutionHelper::_assembly
	Assembly_t4102432799 * ____assembly_0;

public:
	inline static int32_t get_offset_of__assembly_0() { return static_cast<int32_t>(offsetof(PerformSubstitutionHelper_t1593975559, ____assembly_0)); }
	inline Assembly_t4102432799 * get__assembly_0() const { return ____assembly_0; }
	inline Assembly_t4102432799 ** get_address_of__assembly_0() { return &____assembly_0; }
	inline void set__assembly_0(Assembly_t4102432799 * value)
	{
		____assembly_0 = value;
		Il2CppCodeGenWriteBarrier(&____assembly_0, value);
	}
};

struct PerformSubstitutionHelper_t1593975559_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex System.Web.Handlers.AssemblyResourceLoader/PerformSubstitutionHelper::_regex
	Regex_t3657309853 * ____regex_1;

public:
	inline static int32_t get_offset_of__regex_1() { return static_cast<int32_t>(offsetof(PerformSubstitutionHelper_t1593975559_StaticFields, ____regex_1)); }
	inline Regex_t3657309853 * get__regex_1() const { return ____regex_1; }
	inline Regex_t3657309853 ** get_address_of__regex_1() { return &____regex_1; }
	inline void set__regex_1(Regex_t3657309853 * value)
	{
		____regex_1 = value;
		Il2CppCodeGenWriteBarrier(&____regex_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
