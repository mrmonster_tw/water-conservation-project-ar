﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"
#include "System_System_IO_WatcherChangeTypes673177441.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.WaitForChangedResult
struct  WaitForChangedResult_t3377826936 
{
public:
	// System.IO.WatcherChangeTypes System.IO.WaitForChangedResult::changeType
	int32_t ___changeType_0;
	// System.String System.IO.WaitForChangedResult::name
	String_t* ___name_1;
	// System.String System.IO.WaitForChangedResult::oldName
	String_t* ___oldName_2;
	// System.Boolean System.IO.WaitForChangedResult::timedOut
	bool ___timedOut_3;

public:
	inline static int32_t get_offset_of_changeType_0() { return static_cast<int32_t>(offsetof(WaitForChangedResult_t3377826936, ___changeType_0)); }
	inline int32_t get_changeType_0() const { return ___changeType_0; }
	inline int32_t* get_address_of_changeType_0() { return &___changeType_0; }
	inline void set_changeType_0(int32_t value)
	{
		___changeType_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(WaitForChangedResult_t3377826936, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_oldName_2() { return static_cast<int32_t>(offsetof(WaitForChangedResult_t3377826936, ___oldName_2)); }
	inline String_t* get_oldName_2() const { return ___oldName_2; }
	inline String_t** get_address_of_oldName_2() { return &___oldName_2; }
	inline void set_oldName_2(String_t* value)
	{
		___oldName_2 = value;
		Il2CppCodeGenWriteBarrier(&___oldName_2, value);
	}

	inline static int32_t get_offset_of_timedOut_3() { return static_cast<int32_t>(offsetof(WaitForChangedResult_t3377826936, ___timedOut_3)); }
	inline bool get_timedOut_3() const { return ___timedOut_3; }
	inline bool* get_address_of_timedOut_3() { return &___timedOut_3; }
	inline void set_timedOut_3(bool value)
	{
		___timedOut_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.IO.WaitForChangedResult
struct WaitForChangedResult_t3377826936_marshaled_pinvoke
{
	int32_t ___changeType_0;
	char* ___name_1;
	char* ___oldName_2;
	int32_t ___timedOut_3;
};
// Native definition for COM marshalling of System.IO.WaitForChangedResult
struct WaitForChangedResult_t3377826936_marshaled_com
{
	int32_t ___changeType_0;
	Il2CppChar* ___name_1;
	Il2CppChar* ___oldName_2;
	int32_t ___timedOut_3;
};
