﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t1853889766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.CachingCompiler
struct  CachingCompiler_t3794161503  : public Il2CppObject
{
public:

public:
};

struct CachingCompiler_t3794161503_StaticFields
{
public:
	// System.String System.Web.Compilation.CachingCompiler::dynamicBase
	String_t* ___dynamicBase_0;
	// System.Collections.Hashtable System.Web.Compilation.CachingCompiler::compilationTickets
	Hashtable_t1853889766 * ___compilationTickets_1;
	// System.Collections.Hashtable System.Web.Compilation.CachingCompiler::assemblyCache
	Hashtable_t1853889766 * ___assemblyCache_2;

public:
	inline static int32_t get_offset_of_dynamicBase_0() { return static_cast<int32_t>(offsetof(CachingCompiler_t3794161503_StaticFields, ___dynamicBase_0)); }
	inline String_t* get_dynamicBase_0() const { return ___dynamicBase_0; }
	inline String_t** get_address_of_dynamicBase_0() { return &___dynamicBase_0; }
	inline void set_dynamicBase_0(String_t* value)
	{
		___dynamicBase_0 = value;
		Il2CppCodeGenWriteBarrier(&___dynamicBase_0, value);
	}

	inline static int32_t get_offset_of_compilationTickets_1() { return static_cast<int32_t>(offsetof(CachingCompiler_t3794161503_StaticFields, ___compilationTickets_1)); }
	inline Hashtable_t1853889766 * get_compilationTickets_1() const { return ___compilationTickets_1; }
	inline Hashtable_t1853889766 ** get_address_of_compilationTickets_1() { return &___compilationTickets_1; }
	inline void set_compilationTickets_1(Hashtable_t1853889766 * value)
	{
		___compilationTickets_1 = value;
		Il2CppCodeGenWriteBarrier(&___compilationTickets_1, value);
	}

	inline static int32_t get_offset_of_assemblyCache_2() { return static_cast<int32_t>(offsetof(CachingCompiler_t3794161503_StaticFields, ___assemblyCache_2)); }
	inline Hashtable_t1853889766 * get_assemblyCache_2() const { return ___assemblyCache_2; }
	inline Hashtable_t1853889766 ** get_address_of_assemblyCache_2() { return &___assemblyCache_2; }
	inline void set_assemblyCache_2(Hashtable_t1853889766 * value)
	{
		___assemblyCache_2 = value;
		Il2CppCodeGenWriteBarrier(&___assemblyCache_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
