﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeExpression2166265795.h"

// System.CodeDom.CodeMethodReferenceExpression
struct CodeMethodReferenceExpression_t2645505394;
// System.CodeDom.CodeExpressionCollection
struct CodeExpressionCollection_t2370433003;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeMethodInvokeExpression
struct  CodeMethodInvokeExpression_t967905108  : public CodeExpression_t2166265795
{
public:
	// System.CodeDom.CodeMethodReferenceExpression System.CodeDom.CodeMethodInvokeExpression::method
	CodeMethodReferenceExpression_t2645505394 * ___method_1;
	// System.CodeDom.CodeExpressionCollection System.CodeDom.CodeMethodInvokeExpression::parameters
	CodeExpressionCollection_t2370433003 * ___parameters_2;

public:
	inline static int32_t get_offset_of_method_1() { return static_cast<int32_t>(offsetof(CodeMethodInvokeExpression_t967905108, ___method_1)); }
	inline CodeMethodReferenceExpression_t2645505394 * get_method_1() const { return ___method_1; }
	inline CodeMethodReferenceExpression_t2645505394 ** get_address_of_method_1() { return &___method_1; }
	inline void set_method_1(CodeMethodReferenceExpression_t2645505394 * value)
	{
		___method_1 = value;
		Il2CppCodeGenWriteBarrier(&___method_1, value);
	}

	inline static int32_t get_offset_of_parameters_2() { return static_cast<int32_t>(offsetof(CodeMethodInvokeExpression_t967905108, ___parameters_2)); }
	inline CodeExpressionCollection_t2370433003 * get_parameters_2() const { return ___parameters_2; }
	inline CodeExpressionCollection_t2370433003 ** get_address_of_parameters_2() { return &___parameters_2; }
	inline void set_parameters_2(CodeExpressionCollection_t2370433003 * value)
	{
		___parameters_2 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
