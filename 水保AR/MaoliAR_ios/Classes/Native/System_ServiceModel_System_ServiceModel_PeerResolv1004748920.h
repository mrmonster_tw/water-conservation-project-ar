﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_Guid3193532887.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.PeerResolvers.UnregisterInfoDC
struct  UnregisterInfoDC_t1004748920  : public Il2CppObject
{
public:
	// System.String System.ServiceModel.PeerResolvers.UnregisterInfoDC::mesh_id
	String_t* ___mesh_id_0;
	// System.Guid System.ServiceModel.PeerResolvers.UnregisterInfoDC::registration_id
	Guid_t  ___registration_id_1;

public:
	inline static int32_t get_offset_of_mesh_id_0() { return static_cast<int32_t>(offsetof(UnregisterInfoDC_t1004748920, ___mesh_id_0)); }
	inline String_t* get_mesh_id_0() const { return ___mesh_id_0; }
	inline String_t** get_address_of_mesh_id_0() { return &___mesh_id_0; }
	inline void set_mesh_id_0(String_t* value)
	{
		___mesh_id_0 = value;
		Il2CppCodeGenWriteBarrier(&___mesh_id_0, value);
	}

	inline static int32_t get_offset_of_registration_id_1() { return static_cast<int32_t>(offsetof(UnregisterInfoDC_t1004748920, ___registration_id_1)); }
	inline Guid_t  get_registration_id_1() const { return ___registration_id_1; }
	inline Guid_t * get_address_of_registration_id_1() { return &___registration_id_1; }
	inline void set_registration_id_1(Guid_t  value)
	{
		___registration_id_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
