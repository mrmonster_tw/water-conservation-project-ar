﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.Compilation.AppResourceFilesCollection
struct AppResourceFilesCollection_t3801859039;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.String>>
struct Dictionary_2_t3104781730;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.AppResourcesCompiler
struct  AppResourcesCompiler_t3512896086  : public Il2CppObject
{
public:
	// System.Boolean System.Web.Compilation.AppResourcesCompiler::isGlobal
	bool ___isGlobal_0;
	// System.Web.Compilation.AppResourceFilesCollection System.Web.Compilation.AppResourcesCompiler::files
	AppResourceFilesCollection_t3801859039 * ___files_1;
	// System.String System.Web.Compilation.AppResourcesCompiler::tempDirectory
	String_t* ___tempDirectory_2;
	// System.String System.Web.Compilation.AppResourcesCompiler::virtualPath
	String_t* ___virtualPath_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.String>> System.Web.Compilation.AppResourcesCompiler::cultureFiles
	Dictionary_2_t3104781730 * ___cultureFiles_4;
	// System.Collections.Generic.List`1<System.String> System.Web.Compilation.AppResourcesCompiler::defaultCultureFiles
	List_1_t3319525431 * ___defaultCultureFiles_5;

public:
	inline static int32_t get_offset_of_isGlobal_0() { return static_cast<int32_t>(offsetof(AppResourcesCompiler_t3512896086, ___isGlobal_0)); }
	inline bool get_isGlobal_0() const { return ___isGlobal_0; }
	inline bool* get_address_of_isGlobal_0() { return &___isGlobal_0; }
	inline void set_isGlobal_0(bool value)
	{
		___isGlobal_0 = value;
	}

	inline static int32_t get_offset_of_files_1() { return static_cast<int32_t>(offsetof(AppResourcesCompiler_t3512896086, ___files_1)); }
	inline AppResourceFilesCollection_t3801859039 * get_files_1() const { return ___files_1; }
	inline AppResourceFilesCollection_t3801859039 ** get_address_of_files_1() { return &___files_1; }
	inline void set_files_1(AppResourceFilesCollection_t3801859039 * value)
	{
		___files_1 = value;
		Il2CppCodeGenWriteBarrier(&___files_1, value);
	}

	inline static int32_t get_offset_of_tempDirectory_2() { return static_cast<int32_t>(offsetof(AppResourcesCompiler_t3512896086, ___tempDirectory_2)); }
	inline String_t* get_tempDirectory_2() const { return ___tempDirectory_2; }
	inline String_t** get_address_of_tempDirectory_2() { return &___tempDirectory_2; }
	inline void set_tempDirectory_2(String_t* value)
	{
		___tempDirectory_2 = value;
		Il2CppCodeGenWriteBarrier(&___tempDirectory_2, value);
	}

	inline static int32_t get_offset_of_virtualPath_3() { return static_cast<int32_t>(offsetof(AppResourcesCompiler_t3512896086, ___virtualPath_3)); }
	inline String_t* get_virtualPath_3() const { return ___virtualPath_3; }
	inline String_t** get_address_of_virtualPath_3() { return &___virtualPath_3; }
	inline void set_virtualPath_3(String_t* value)
	{
		___virtualPath_3 = value;
		Il2CppCodeGenWriteBarrier(&___virtualPath_3, value);
	}

	inline static int32_t get_offset_of_cultureFiles_4() { return static_cast<int32_t>(offsetof(AppResourcesCompiler_t3512896086, ___cultureFiles_4)); }
	inline Dictionary_2_t3104781730 * get_cultureFiles_4() const { return ___cultureFiles_4; }
	inline Dictionary_2_t3104781730 ** get_address_of_cultureFiles_4() { return &___cultureFiles_4; }
	inline void set_cultureFiles_4(Dictionary_2_t3104781730 * value)
	{
		___cultureFiles_4 = value;
		Il2CppCodeGenWriteBarrier(&___cultureFiles_4, value);
	}

	inline static int32_t get_offset_of_defaultCultureFiles_5() { return static_cast<int32_t>(offsetof(AppResourcesCompiler_t3512896086, ___defaultCultureFiles_5)); }
	inline List_1_t3319525431 * get_defaultCultureFiles_5() const { return ___defaultCultureFiles_5; }
	inline List_1_t3319525431 ** get_address_of_defaultCultureFiles_5() { return &___defaultCultureFiles_5; }
	inline void set_defaultCultureFiles_5(List_1_t3319525431 * value)
	{
		___defaultCultureFiles_5 = value;
		Il2CppCodeGenWriteBarrier(&___defaultCultureFiles_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
