﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Generic.Dictionary`2<System.Web.Compilation.DefaultResourceProvider/ResourceManagerCacheKey,System.Resources.ResourceManager>
struct Dictionary_2_t75065667;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.DefaultResourceProvider
struct  DefaultResourceProvider_t400076996  : public Il2CppObject
{
public:
	// System.String System.Web.Compilation.DefaultResourceProvider::resource
	String_t* ___resource_1;
	// System.Boolean System.Web.Compilation.DefaultResourceProvider::isGlobal
	bool ___isGlobal_2;

public:
	inline static int32_t get_offset_of_resource_1() { return static_cast<int32_t>(offsetof(DefaultResourceProvider_t400076996, ___resource_1)); }
	inline String_t* get_resource_1() const { return ___resource_1; }
	inline String_t** get_address_of_resource_1() { return &___resource_1; }
	inline void set_resource_1(String_t* value)
	{
		___resource_1 = value;
		Il2CppCodeGenWriteBarrier(&___resource_1, value);
	}

	inline static int32_t get_offset_of_isGlobal_2() { return static_cast<int32_t>(offsetof(DefaultResourceProvider_t400076996, ___isGlobal_2)); }
	inline bool get_isGlobal_2() const { return ___isGlobal_2; }
	inline bool* get_address_of_isGlobal_2() { return &___isGlobal_2; }
	inline void set_isGlobal_2(bool value)
	{
		___isGlobal_2 = value;
	}
};

struct DefaultResourceProvider_t400076996_ThreadStaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Web.Compilation.DefaultResourceProvider/ResourceManagerCacheKey,System.Resources.ResourceManager> System.Web.Compilation.DefaultResourceProvider::resourceManagerCache
	Dictionary_2_t75065667 * ___resourceManagerCache_0;

public:
	inline static int32_t get_offset_of_resourceManagerCache_0() { return static_cast<int32_t>(offsetof(DefaultResourceProvider_t400076996_ThreadStaticFields, ___resourceManagerCache_0)); }
	inline Dictionary_2_t75065667 * get_resourceManagerCache_0() const { return ___resourceManagerCache_0; }
	inline Dictionary_2_t75065667 ** get_address_of_resourceManagerCache_0() { return &___resourceManagerCache_0; }
	inline void set_resourceManagerCache_0(Dictionary_2_t75065667 * value)
	{
		___resourceManagerCache_0 = value;
		Il2CppCodeGenWriteBarrier(&___resourceManagerCache_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
