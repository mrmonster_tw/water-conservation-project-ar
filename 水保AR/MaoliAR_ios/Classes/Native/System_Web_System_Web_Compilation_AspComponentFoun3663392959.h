﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_Compilation_AspComponentFoun1085528248.h"

// System.String
struct String_t;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.AspComponentFoundry/TagNameFoundry
struct  TagNameFoundry_t3663392959  : public Foundry_t1085528248
{
public:
	// System.String System.Web.Compilation.AspComponentFoundry/TagNameFoundry::tagName
	String_t* ___tagName_1;
	// System.Type System.Web.Compilation.AspComponentFoundry/TagNameFoundry::type
	Type_t * ___type_2;
	// System.String System.Web.Compilation.AspComponentFoundry/TagNameFoundry::source
	String_t* ___source_3;

public:
	inline static int32_t get_offset_of_tagName_1() { return static_cast<int32_t>(offsetof(TagNameFoundry_t3663392959, ___tagName_1)); }
	inline String_t* get_tagName_1() const { return ___tagName_1; }
	inline String_t** get_address_of_tagName_1() { return &___tagName_1; }
	inline void set_tagName_1(String_t* value)
	{
		___tagName_1 = value;
		Il2CppCodeGenWriteBarrier(&___tagName_1, value);
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(TagNameFoundry_t3663392959, ___type_2)); }
	inline Type_t * get_type_2() const { return ___type_2; }
	inline Type_t ** get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(Type_t * value)
	{
		___type_2 = value;
		Il2CppCodeGenWriteBarrier(&___type_2, value);
	}

	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(TagNameFoundry_t3663392959, ___source_3)); }
	inline String_t* get_source_3() const { return ___source_3; }
	inline String_t** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(String_t* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier(&___source_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
