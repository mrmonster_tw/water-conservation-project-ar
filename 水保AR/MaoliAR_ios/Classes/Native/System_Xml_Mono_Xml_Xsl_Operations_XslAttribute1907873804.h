﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Operations_XslCompiledElemen50593777.h"

// Mono.Xml.Xsl.Operations.XslAvt
struct XslAvt_t1645109359;
// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// Mono.Xml.Xsl.Operations.XslOperation
struct XslOperation_t2153241355;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslAttribute
struct  XslAttribute_t1907873804  : public XslCompiledElement_t50593777
{
public:
	// Mono.Xml.Xsl.Operations.XslAvt Mono.Xml.Xsl.Operations.XslAttribute::name
	XslAvt_t1645109359 * ___name_3;
	// Mono.Xml.Xsl.Operations.XslAvt Mono.Xml.Xsl.Operations.XslAttribute::ns
	XslAvt_t1645109359 * ___ns_4;
	// System.String Mono.Xml.Xsl.Operations.XslAttribute::calcName
	String_t* ___calcName_5;
	// System.String Mono.Xml.Xsl.Operations.XslAttribute::calcNs
	String_t* ___calcNs_6;
	// System.String Mono.Xml.Xsl.Operations.XslAttribute::calcPrefix
	String_t* ___calcPrefix_7;
	// System.Collections.Hashtable Mono.Xml.Xsl.Operations.XslAttribute::nsDecls
	Hashtable_t1853889766 * ___nsDecls_8;
	// Mono.Xml.Xsl.Operations.XslOperation Mono.Xml.Xsl.Operations.XslAttribute::value
	XslOperation_t2153241355 * ___value_9;

public:
	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(XslAttribute_t1907873804, ___name_3)); }
	inline XslAvt_t1645109359 * get_name_3() const { return ___name_3; }
	inline XslAvt_t1645109359 ** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(XslAvt_t1645109359 * value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier(&___name_3, value);
	}

	inline static int32_t get_offset_of_ns_4() { return static_cast<int32_t>(offsetof(XslAttribute_t1907873804, ___ns_4)); }
	inline XslAvt_t1645109359 * get_ns_4() const { return ___ns_4; }
	inline XslAvt_t1645109359 ** get_address_of_ns_4() { return &___ns_4; }
	inline void set_ns_4(XslAvt_t1645109359 * value)
	{
		___ns_4 = value;
		Il2CppCodeGenWriteBarrier(&___ns_4, value);
	}

	inline static int32_t get_offset_of_calcName_5() { return static_cast<int32_t>(offsetof(XslAttribute_t1907873804, ___calcName_5)); }
	inline String_t* get_calcName_5() const { return ___calcName_5; }
	inline String_t** get_address_of_calcName_5() { return &___calcName_5; }
	inline void set_calcName_5(String_t* value)
	{
		___calcName_5 = value;
		Il2CppCodeGenWriteBarrier(&___calcName_5, value);
	}

	inline static int32_t get_offset_of_calcNs_6() { return static_cast<int32_t>(offsetof(XslAttribute_t1907873804, ___calcNs_6)); }
	inline String_t* get_calcNs_6() const { return ___calcNs_6; }
	inline String_t** get_address_of_calcNs_6() { return &___calcNs_6; }
	inline void set_calcNs_6(String_t* value)
	{
		___calcNs_6 = value;
		Il2CppCodeGenWriteBarrier(&___calcNs_6, value);
	}

	inline static int32_t get_offset_of_calcPrefix_7() { return static_cast<int32_t>(offsetof(XslAttribute_t1907873804, ___calcPrefix_7)); }
	inline String_t* get_calcPrefix_7() const { return ___calcPrefix_7; }
	inline String_t** get_address_of_calcPrefix_7() { return &___calcPrefix_7; }
	inline void set_calcPrefix_7(String_t* value)
	{
		___calcPrefix_7 = value;
		Il2CppCodeGenWriteBarrier(&___calcPrefix_7, value);
	}

	inline static int32_t get_offset_of_nsDecls_8() { return static_cast<int32_t>(offsetof(XslAttribute_t1907873804, ___nsDecls_8)); }
	inline Hashtable_t1853889766 * get_nsDecls_8() const { return ___nsDecls_8; }
	inline Hashtable_t1853889766 ** get_address_of_nsDecls_8() { return &___nsDecls_8; }
	inline void set_nsDecls_8(Hashtable_t1853889766 * value)
	{
		___nsDecls_8 = value;
		Il2CppCodeGenWriteBarrier(&___nsDecls_8, value);
	}

	inline static int32_t get_offset_of_value_9() { return static_cast<int32_t>(offsetof(XslAttribute_t1907873804, ___value_9)); }
	inline XslOperation_t2153241355 * get_value_9() const { return ___value_9; }
	inline XslOperation_t2153241355 ** get_address_of_value_9() { return &___value_9; }
	inline void set_value_9(XslOperation_t2153241355 * value)
	{
		___value_9 = value;
		Il2CppCodeGenWriteBarrier(&___value_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
