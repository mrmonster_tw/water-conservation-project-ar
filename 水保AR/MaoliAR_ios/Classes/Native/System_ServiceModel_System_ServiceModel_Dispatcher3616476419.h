﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Generic.SynchronizedCollection`1<System.ServiceModel.Dispatcher.IChannelInitializer>
struct SynchronizedCollection_1_t3028954626;
// System.Collections.Generic.SynchronizedCollection`1<System.ServiceModel.Dispatcher.IInteractiveChannelInitializer>
struct SynchronizedCollection_1_t2872365154;
// System.Collections.Generic.SynchronizedCollection`1<System.ServiceModel.Dispatcher.IClientMessageInspector>
struct SynchronizedCollection_1_t2969066822;
// System.ServiceModel.Dispatcher.ClientOperation/ClientOperationCollection
struct ClientOperationCollection_t538801239;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.ServiceModel.Dispatcher.DispatchRuntime
struct DispatchRuntime_t796075230;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.ClientRuntime
struct  ClientRuntime_t3616476419  : public Il2CppObject
{
public:
	// System.Collections.Generic.SynchronizedCollection`1<System.ServiceModel.Dispatcher.IChannelInitializer> System.ServiceModel.Dispatcher.ClientRuntime::channel_initializers
	SynchronizedCollection_1_t3028954626 * ___channel_initializers_0;
	// System.Collections.Generic.SynchronizedCollection`1<System.ServiceModel.Dispatcher.IInteractiveChannelInitializer> System.ServiceModel.Dispatcher.ClientRuntime::interactive_channel_initializers
	SynchronizedCollection_1_t2872365154 * ___interactive_channel_initializers_1;
	// System.Collections.Generic.SynchronizedCollection`1<System.ServiceModel.Dispatcher.IClientMessageInspector> System.ServiceModel.Dispatcher.ClientRuntime::inspectors
	SynchronizedCollection_1_t2969066822 * ___inspectors_2;
	// System.ServiceModel.Dispatcher.ClientOperation/ClientOperationCollection System.ServiceModel.Dispatcher.ClientRuntime::operations
	ClientOperationCollection_t538801239 * ___operations_3;
	// System.String System.ServiceModel.Dispatcher.ClientRuntime::contract_name
	String_t* ___contract_name_4;
	// System.String System.ServiceModel.Dispatcher.ClientRuntime::contract_ns
	String_t* ___contract_ns_5;
	// System.Int32 System.ServiceModel.Dispatcher.ClientRuntime::max_fault_size
	int32_t ___max_fault_size_6;
	// System.Type System.ServiceModel.Dispatcher.ClientRuntime::<CallbackClientType>k__BackingField
	Type_t * ___U3CCallbackClientTypeU3Ek__BackingField_7;
	// System.Type System.ServiceModel.Dispatcher.ClientRuntime::<ContractClientType>k__BackingField
	Type_t * ___U3CContractClientTypeU3Ek__BackingField_8;
	// System.ServiceModel.Dispatcher.DispatchRuntime System.ServiceModel.Dispatcher.ClientRuntime::<CallbackDispatchRuntime>k__BackingField
	DispatchRuntime_t796075230 * ___U3CCallbackDispatchRuntimeU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_channel_initializers_0() { return static_cast<int32_t>(offsetof(ClientRuntime_t3616476419, ___channel_initializers_0)); }
	inline SynchronizedCollection_1_t3028954626 * get_channel_initializers_0() const { return ___channel_initializers_0; }
	inline SynchronizedCollection_1_t3028954626 ** get_address_of_channel_initializers_0() { return &___channel_initializers_0; }
	inline void set_channel_initializers_0(SynchronizedCollection_1_t3028954626 * value)
	{
		___channel_initializers_0 = value;
		Il2CppCodeGenWriteBarrier(&___channel_initializers_0, value);
	}

	inline static int32_t get_offset_of_interactive_channel_initializers_1() { return static_cast<int32_t>(offsetof(ClientRuntime_t3616476419, ___interactive_channel_initializers_1)); }
	inline SynchronizedCollection_1_t2872365154 * get_interactive_channel_initializers_1() const { return ___interactive_channel_initializers_1; }
	inline SynchronizedCollection_1_t2872365154 ** get_address_of_interactive_channel_initializers_1() { return &___interactive_channel_initializers_1; }
	inline void set_interactive_channel_initializers_1(SynchronizedCollection_1_t2872365154 * value)
	{
		___interactive_channel_initializers_1 = value;
		Il2CppCodeGenWriteBarrier(&___interactive_channel_initializers_1, value);
	}

	inline static int32_t get_offset_of_inspectors_2() { return static_cast<int32_t>(offsetof(ClientRuntime_t3616476419, ___inspectors_2)); }
	inline SynchronizedCollection_1_t2969066822 * get_inspectors_2() const { return ___inspectors_2; }
	inline SynchronizedCollection_1_t2969066822 ** get_address_of_inspectors_2() { return &___inspectors_2; }
	inline void set_inspectors_2(SynchronizedCollection_1_t2969066822 * value)
	{
		___inspectors_2 = value;
		Il2CppCodeGenWriteBarrier(&___inspectors_2, value);
	}

	inline static int32_t get_offset_of_operations_3() { return static_cast<int32_t>(offsetof(ClientRuntime_t3616476419, ___operations_3)); }
	inline ClientOperationCollection_t538801239 * get_operations_3() const { return ___operations_3; }
	inline ClientOperationCollection_t538801239 ** get_address_of_operations_3() { return &___operations_3; }
	inline void set_operations_3(ClientOperationCollection_t538801239 * value)
	{
		___operations_3 = value;
		Il2CppCodeGenWriteBarrier(&___operations_3, value);
	}

	inline static int32_t get_offset_of_contract_name_4() { return static_cast<int32_t>(offsetof(ClientRuntime_t3616476419, ___contract_name_4)); }
	inline String_t* get_contract_name_4() const { return ___contract_name_4; }
	inline String_t** get_address_of_contract_name_4() { return &___contract_name_4; }
	inline void set_contract_name_4(String_t* value)
	{
		___contract_name_4 = value;
		Il2CppCodeGenWriteBarrier(&___contract_name_4, value);
	}

	inline static int32_t get_offset_of_contract_ns_5() { return static_cast<int32_t>(offsetof(ClientRuntime_t3616476419, ___contract_ns_5)); }
	inline String_t* get_contract_ns_5() const { return ___contract_ns_5; }
	inline String_t** get_address_of_contract_ns_5() { return &___contract_ns_5; }
	inline void set_contract_ns_5(String_t* value)
	{
		___contract_ns_5 = value;
		Il2CppCodeGenWriteBarrier(&___contract_ns_5, value);
	}

	inline static int32_t get_offset_of_max_fault_size_6() { return static_cast<int32_t>(offsetof(ClientRuntime_t3616476419, ___max_fault_size_6)); }
	inline int32_t get_max_fault_size_6() const { return ___max_fault_size_6; }
	inline int32_t* get_address_of_max_fault_size_6() { return &___max_fault_size_6; }
	inline void set_max_fault_size_6(int32_t value)
	{
		___max_fault_size_6 = value;
	}

	inline static int32_t get_offset_of_U3CCallbackClientTypeU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ClientRuntime_t3616476419, ___U3CCallbackClientTypeU3Ek__BackingField_7)); }
	inline Type_t * get_U3CCallbackClientTypeU3Ek__BackingField_7() const { return ___U3CCallbackClientTypeU3Ek__BackingField_7; }
	inline Type_t ** get_address_of_U3CCallbackClientTypeU3Ek__BackingField_7() { return &___U3CCallbackClientTypeU3Ek__BackingField_7; }
	inline void set_U3CCallbackClientTypeU3Ek__BackingField_7(Type_t * value)
	{
		___U3CCallbackClientTypeU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCallbackClientTypeU3Ek__BackingField_7, value);
	}

	inline static int32_t get_offset_of_U3CContractClientTypeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(ClientRuntime_t3616476419, ___U3CContractClientTypeU3Ek__BackingField_8)); }
	inline Type_t * get_U3CContractClientTypeU3Ek__BackingField_8() const { return ___U3CContractClientTypeU3Ek__BackingField_8; }
	inline Type_t ** get_address_of_U3CContractClientTypeU3Ek__BackingField_8() { return &___U3CContractClientTypeU3Ek__BackingField_8; }
	inline void set_U3CContractClientTypeU3Ek__BackingField_8(Type_t * value)
	{
		___U3CContractClientTypeU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CContractClientTypeU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of_U3CCallbackDispatchRuntimeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(ClientRuntime_t3616476419, ___U3CCallbackDispatchRuntimeU3Ek__BackingField_9)); }
	inline DispatchRuntime_t796075230 * get_U3CCallbackDispatchRuntimeU3Ek__BackingField_9() const { return ___U3CCallbackDispatchRuntimeU3Ek__BackingField_9; }
	inline DispatchRuntime_t796075230 ** get_address_of_U3CCallbackDispatchRuntimeU3Ek__BackingField_9() { return &___U3CCallbackDispatchRuntimeU3Ek__BackingField_9; }
	inline void set_U3CCallbackDispatchRuntimeU3Ek__BackingField_9(DispatchRuntime_t796075230 * value)
	{
		___U3CCallbackDispatchRuntimeU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCallbackDispatchRuntimeU3Ek__BackingField_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
