﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_EasingFunctio2767217938.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_ApplyTween3327999347.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_EaseType2573404410.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_LoopType369612249.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_NamedValueCol1091574706.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_Defaults3148213711.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_CRSpline2815350084.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_U3CTweenDelay2686771544.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_U3CTweenResta1737386981.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_U3CStartU3Ec_2390838266.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_C229504867.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_4012421388.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_C741724143.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2605441282.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1594920509.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_C330505695.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_C229035171.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1580479539.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1958507577.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Ci76530769.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_C736195574.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3281073503.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Ci80388988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_C876897537.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_4115310896.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1606529363.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2221466283.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3045151141.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3718402411.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Ci59963330.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1869387270.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2985116592.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_4116788586.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1975354525.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1032325577.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1823485623.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3819547615.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1946006342.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2645519978.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_C633783710.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_4189517256.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3576012827.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_C577622288.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3609884370.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1593743734.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3841223768.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_C956974295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1379802392.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1261455774.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3943002634.h"
#include "AssemblyU2DCSharpU2Dfirstpass_TonemappingLog4031249120.h"
#include "AssemblyU2DCSharpU2Dfirstpass_TonemappingLut4032363238.h"
#include "AssemblyU2DCSharpU2Dfirstpass_TonemappingLut_Simpl3505133624.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1871701680.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1691315015.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1125654350.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_I630413071.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_I747557136.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3774419504.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2012607685.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3369172721.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_4260782719.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_I984135990.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_4271191419.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Im19712272.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2848767628.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2685819829.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1804605042.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2416258039.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1038294851.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3334654964.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1046251128.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1503422003.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3705161775.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2812046500.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_I520253047.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3742166504.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1416458051.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1159177774.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3562116199.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_I640919481.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3424449263.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1200394124.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1116783936.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3871645803.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3210294001.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_4187663194.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3636551379.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1566655669.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_I473098480.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1233703462.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_I506487406.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1984240676.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_4101461743.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_I900542613.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1707485390.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2026006575.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1214077586.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1587267364.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5600 = { sizeof (EasingFunction_t2767217938), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5601 = { sizeof (ApplyTween_t3327999347), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5602 = { sizeof (EaseType_t2573404410)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5602[34] = 
{
	EaseType_t2573404410::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5603 = { sizeof (LoopType_t369612249)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5603[4] = 
{
	LoopType_t369612249::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5604 = { sizeof (NamedValueColor_t1091574706)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5604[5] = 
{
	NamedValueColor_t1091574706::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5605 = { sizeof (Defaults_t3148213711), -1, sizeof(Defaults_t3148213711_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5605[16] = 
{
	Defaults_t3148213711_StaticFields::get_offset_of_time_0(),
	Defaults_t3148213711_StaticFields::get_offset_of_delay_1(),
	Defaults_t3148213711_StaticFields::get_offset_of_namedColorValue_2(),
	Defaults_t3148213711_StaticFields::get_offset_of_loopType_3(),
	Defaults_t3148213711_StaticFields::get_offset_of_easeType_4(),
	Defaults_t3148213711_StaticFields::get_offset_of_lookSpeed_5(),
	Defaults_t3148213711_StaticFields::get_offset_of_isLocal_6(),
	Defaults_t3148213711_StaticFields::get_offset_of_space_7(),
	Defaults_t3148213711_StaticFields::get_offset_of_orientToPath_8(),
	Defaults_t3148213711_StaticFields::get_offset_of_color_9(),
	Defaults_t3148213711_StaticFields::get_offset_of_updateTimePercentage_10(),
	Defaults_t3148213711_StaticFields::get_offset_of_updateTime_11(),
	Defaults_t3148213711_StaticFields::get_offset_of_cameraFadeDepth_12(),
	Defaults_t3148213711_StaticFields::get_offset_of_lookAhead_13(),
	Defaults_t3148213711_StaticFields::get_offset_of_useRealTime_14(),
	Defaults_t3148213711_StaticFields::get_offset_of_up_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5606 = { sizeof (CRSpline_t2815350084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5606[1] = 
{
	CRSpline_t2815350084::get_offset_of_pts_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5607 = { sizeof (U3CTweenDelayU3Ec__Iterator0_t2686771544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5607[4] = 
{
	U3CTweenDelayU3Ec__Iterator0_t2686771544::get_offset_of_U24this_0(),
	U3CTweenDelayU3Ec__Iterator0_t2686771544::get_offset_of_U24current_1(),
	U3CTweenDelayU3Ec__Iterator0_t2686771544::get_offset_of_U24disposing_2(),
	U3CTweenDelayU3Ec__Iterator0_t2686771544::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5608 = { sizeof (U3CTweenRestartU3Ec__Iterator1_t1737386981), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5608[4] = 
{
	U3CTweenRestartU3Ec__Iterator1_t1737386981::get_offset_of_U24this_0(),
	U3CTweenRestartU3Ec__Iterator1_t1737386981::get_offset_of_U24current_1(),
	U3CTweenRestartU3Ec__Iterator1_t1737386981::get_offset_of_U24disposing_2(),
	U3CTweenRestartU3Ec__Iterator1_t1737386981::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5609 = { sizeof (U3CStartU3Ec__Iterator2_t2390838266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5609[4] = 
{
	U3CStartU3Ec__Iterator2_t2390838266::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator2_t2390838266::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator2_t2390838266::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator2_t2390838266::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5610 = { sizeof (AmbientOcclusion_t229504867), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5610[6] = 
{
	AmbientOcclusion_t229504867::get_offset_of_settings_2(),
	AmbientOcclusion_t229504867::get_offset_of__aoShader_3(),
	AmbientOcclusion_t229504867::get_offset_of__aoMaterial_4(),
	AmbientOcclusion_t229504867::get_offset_of__aoCommands_5(),
	AmbientOcclusion_t229504867::get_offset_of_U3CpropertyObserverU3Ek__BackingField_6(),
	AmbientOcclusion_t229504867::get_offset_of__quadMesh_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5611 = { sizeof (PropertyObserver_t4012421388)+ sizeof (Il2CppObject), sizeof(PropertyObserver_t4012421388_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5611[6] = 
{
	PropertyObserver_t4012421388::get_offset_of__downsampling_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropertyObserver_t4012421388::get_offset_of__occlusionSource_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropertyObserver_t4012421388::get_offset_of__ambientOnly_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropertyObserver_t4012421388::get_offset_of__debug_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropertyObserver_t4012421388::get_offset_of__pixelWidth_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropertyObserver_t4012421388::get_offset_of__pixelHeight_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5612 = { sizeof (SampleCount_t741724143)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5612[6] = 
{
	SampleCount_t741724143::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5613 = { sizeof (OcclusionSource_t2605441282)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5613[4] = 
{
	OcclusionSource_t2605441282::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5614 = { sizeof (Settings_t1594920509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5614[8] = 
{
	Settings_t1594920509::get_offset_of_intensity_0(),
	Settings_t1594920509::get_offset_of_radius_1(),
	Settings_t1594920509::get_offset_of_sampleCount_2(),
	Settings_t1594920509::get_offset_of_sampleCountValue_3(),
	Settings_t1594920509::get_offset_of_downsampling_4(),
	Settings_t1594920509::get_offset_of_ambientOnly_5(),
	Settings_t1594920509::get_offset_of_occlusionSource_6(),
	Settings_t1594920509::get_offset_of_debug_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5615 = { sizeof (AntiAliasing_t330505695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5615[4] = 
{
	AntiAliasing_t330505695::get_offset_of_m_SMAA_2(),
	AntiAliasing_t330505695::get_offset_of_m_FXAA_3(),
	AntiAliasing_t330505695::get_offset_of_m_Method_4(),
	AntiAliasing_t330505695::get_offset_of_m_Camera_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5616 = { sizeof (Method_t229035171)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5616[3] = 
{
	Method_t229035171::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5617 = { sizeof (FXAA_t1580479539), -1, sizeof(FXAA_t1580479539_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5617[5] = 
{
	FXAA_t1580479539::get_offset_of_m_Shader_0(),
	FXAA_t1580479539::get_offset_of_m_Material_1(),
	FXAA_t1580479539::get_offset_of_preset_2(),
	FXAA_t1580479539_StaticFields::get_offset_of_availablePresets_3(),
	FXAA_t1580479539::get_offset_of_U3CvalidSourceFormatU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5618 = { sizeof (QualitySettings_t1958507577)+ sizeof (Il2CppObject), sizeof(QualitySettings_t1958507577 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5618[3] = 
{
	QualitySettings_t1958507577::get_offset_of_subpixelAliasingRemovalAmount_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	QualitySettings_t1958507577::get_offset_of_edgeDetectionThreshold_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	QualitySettings_t1958507577::get_offset_of_minimumRequiredLuminance_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5619 = { sizeof (ConsoleSettings_t76530769)+ sizeof (Il2CppObject), sizeof(ConsoleSettings_t76530769 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5619[4] = 
{
	ConsoleSettings_t76530769::get_offset_of_subpixelSpreadAmount_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ConsoleSettings_t76530769::get_offset_of_edgeSharpnessAmount_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ConsoleSettings_t76530769::get_offset_of_edgeDetectionThreshold_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ConsoleSettings_t76530769::get_offset_of_minimumRequiredLuminance_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5620 = { sizeof (Preset_t736195574)+ sizeof (Il2CppObject), sizeof(Preset_t736195574 ), sizeof(Preset_t736195574_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5620[7] = 
{
	Preset_t736195574::get_offset_of_qualitySettings_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Preset_t736195574::get_offset_of_consoleSettings_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Preset_t736195574_StaticFields::get_offset_of_s_ExtremePerformance_2(),
	Preset_t736195574_StaticFields::get_offset_of_s_Performance_3(),
	Preset_t736195574_StaticFields::get_offset_of_s_Default_4(),
	Preset_t736195574_StaticFields::get_offset_of_s_Quality_5(),
	Preset_t736195574_StaticFields::get_offset_of_s_ExtremeQuality_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5621 = { sizeof (LayoutAttribute_t3281073503), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5622 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5623 = { sizeof (SMAA_t80388988), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5623[12] = 
{
	SMAA_t80388988::get_offset_of_settings_0(),
	SMAA_t80388988::get_offset_of_quality_1(),
	SMAA_t80388988::get_offset_of_predication_2(),
	SMAA_t80388988::get_offset_of_temporal_3(),
	SMAA_t80388988::get_offset_of_m_ProjectionMatrix_4(),
	SMAA_t80388988::get_offset_of_m_PreviousViewProjectionMatrix_5(),
	SMAA_t80388988::get_offset_of_m_FlipFlop_6(),
	SMAA_t80388988::get_offset_of_m_Accumulation_7(),
	SMAA_t80388988::get_offset_of_m_Shader_8(),
	SMAA_t80388988::get_offset_of_m_AreaTexture_9(),
	SMAA_t80388988::get_offset_of_m_SearchTexture_10(),
	SMAA_t80388988::get_offset_of_m_Material_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5624 = { sizeof (SettingsGroup_t876897537), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5625 = { sizeof (TopLevelSettings_t4115310896), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5626 = { sizeof (ExperimentalGroup_t1606529363), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5627 = { sizeof (DebugPass_t2221466283)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5627[5] = 
{
	DebugPass_t2221466283::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5628 = { sizeof (QualityPreset_t3045151141)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5628[6] = 
{
	QualityPreset_t3045151141::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5629 = { sizeof (EdgeDetectionMethod_t3718402411)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5629[4] = 
{
	EdgeDetectionMethod_t3718402411::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5630 = { sizeof (GlobalSettings_t59963330)+ sizeof (Il2CppObject), sizeof(GlobalSettings_t59963330 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5630[3] = 
{
	GlobalSettings_t59963330::get_offset_of_debugPass_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GlobalSettings_t59963330::get_offset_of_quality_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GlobalSettings_t59963330::get_offset_of_edgeDetectionMethod_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5631 = { sizeof (QualitySettings_t1869387270)+ sizeof (Il2CppObject), sizeof(QualitySettings_t1869387270_marshaled_pinvoke), sizeof(QualitySettings_t1869387270_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5631[9] = 
{
	QualitySettings_t1869387270::get_offset_of_diagonalDetection_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	QualitySettings_t1869387270::get_offset_of_cornerDetection_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	QualitySettings_t1869387270::get_offset_of_threshold_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	QualitySettings_t1869387270::get_offset_of_depthThreshold_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	QualitySettings_t1869387270::get_offset_of_maxSearchSteps_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	QualitySettings_t1869387270::get_offset_of_maxDiagonalSearchSteps_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	QualitySettings_t1869387270::get_offset_of_cornerRounding_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	QualitySettings_t1869387270::get_offset_of_localContrastAdaptationFactor_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	QualitySettings_t1869387270_StaticFields::get_offset_of_presetQualitySettings_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5632 = { sizeof (TemporalSettings_t2985116592)+ sizeof (Il2CppObject), sizeof(TemporalSettings_t2985116592_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5632[2] = 
{
	TemporalSettings_t2985116592::get_offset_of_enabled_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TemporalSettings_t2985116592::get_offset_of_fuzzSize_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5633 = { sizeof (PredicationSettings_t4116788586)+ sizeof (Il2CppObject), sizeof(PredicationSettings_t4116788586_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5633[4] = 
{
	PredicationSettings_t4116788586::get_offset_of_enabled_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PredicationSettings_t4116788586::get_offset_of_threshold_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PredicationSettings_t4116788586::get_offset_of_scale_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PredicationSettings_t4116788586::get_offset_of_strength_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5634 = { sizeof (Bloom_t1975354525), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5634[6] = 
{
	Bloom_t1975354525::get_offset_of_settings_2(),
	Bloom_t1975354525::get_offset_of_m_Shader_3(),
	Bloom_t1975354525::get_offset_of_m_Material_4(),
	0,
	Bloom_t1975354525::get_offset_of_m_blurBuffer1_6(),
	Bloom_t1975354525::get_offset_of_m_blurBuffer2_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5635 = { sizeof (Settings_t1032325577)+ sizeof (Il2CppObject), sizeof(Settings_t1032325577_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5635[6] = 
{
	Settings_t1032325577::get_offset_of_threshold_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Settings_t1032325577::get_offset_of_softKnee_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Settings_t1032325577::get_offset_of_radius_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Settings_t1032325577::get_offset_of_intensity_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Settings_t1032325577::get_offset_of_highQuality_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Settings_t1032325577::get_offset_of_antiFlicker_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5636 = { sizeof (ImageEffectHelper_t1823485623), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5637 = { sizeof (MinAttribute_t3819547615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5637[1] = 
{
	MinAttribute_t3819547615::get_offset_of_min_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5638 = { sizeof (RenderTextureUtility_t1946006342), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5638[1] = 
{
	RenderTextureUtility_t1946006342::get_offset_of_m_TemporaryRTs_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5639 = { sizeof (DepthOfField_t2645519978), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5639[22] = 
{
	0,
	DepthOfField_t2645519978::get_offset_of_settings_3(),
	DepthOfField_t2645519978::get_offset_of_focus_4(),
	DepthOfField_t2645519978::get_offset_of_bokehTexture_5(),
	DepthOfField_t2645519978::get_offset_of_m_FilmicDepthOfFieldShader_6(),
	DepthOfField_t2645519978::get_offset_of_m_MedianFilterShader_7(),
	DepthOfField_t2645519978::get_offset_of_m_TextureBokehShader_8(),
	DepthOfField_t2645519978::get_offset_of_m_RTU_9(),
	DepthOfField_t2645519978::get_offset_of_m_FilmicDepthOfFieldMaterial_10(),
	DepthOfField_t2645519978::get_offset_of_m_MedianFilterMaterial_11(),
	DepthOfField_t2645519978::get_offset_of_m_TextureBokehMaterial_12(),
	DepthOfField_t2645519978::get_offset_of_m_ComputeBufferDrawArgs_13(),
	DepthOfField_t2645519978::get_offset_of_m_ComputeBufferPoints_14(),
	DepthOfField_t2645519978::get_offset_of_m_CurrentQualitySettings_15(),
	DepthOfField_t2645519978::get_offset_of_m_LastApertureOrientation_16(),
	DepthOfField_t2645519978::get_offset_of_m_OctogonalBokehDirection1_17(),
	DepthOfField_t2645519978::get_offset_of_m_OctogonalBokehDirection2_18(),
	DepthOfField_t2645519978::get_offset_of_m_OctogonalBokehDirection3_19(),
	DepthOfField_t2645519978::get_offset_of_m_OctogonalBokehDirection4_20(),
	DepthOfField_t2645519978::get_offset_of_m_HexagonalBokehDirection1_21(),
	DepthOfField_t2645519978::get_offset_of_m_HexagonalBokehDirection2_22(),
	DepthOfField_t2645519978::get_offset_of_m_HexagonalBokehDirection3_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5640 = { sizeof (Passes_t633783710)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5640[25] = 
{
	Passes_t633783710::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5641 = { sizeof (MedianPasses_t4189517256)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5641[3] = 
{
	MedianPasses_t4189517256::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5642 = { sizeof (BokehTexturesPasses_t3576012827)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5642[3] = 
{
	BokehTexturesPasses_t3576012827::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5643 = { sizeof (TweakMode_t577622288)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5643[3] = 
{
	TweakMode_t577622288::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5644 = { sizeof (ApertureShape_t3609884370)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5644[4] = 
{
	ApertureShape_t3609884370::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5645 = { sizeof (QualityPreset_t1593743734)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5645[4] = 
{
	QualityPreset_t1593743734::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5646 = { sizeof (FilterQuality_t3841223768)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5646[4] = 
{
	FilterQuality_t3841223768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5647 = { sizeof (GlobalSettings_t956974295)+ sizeof (Il2CppObject), sizeof(GlobalSettings_t956974295_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5647[5] = 
{
	GlobalSettings_t956974295::get_offset_of_visualizeFocus_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GlobalSettings_t956974295::get_offset_of_tweakMode_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GlobalSettings_t956974295::get_offset_of_filteringQuality_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GlobalSettings_t956974295::get_offset_of_apertureShape_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GlobalSettings_t956974295::get_offset_of_apertureOrientation_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5648 = { sizeof (QualitySettings_t1379802392)+ sizeof (Il2CppObject), sizeof(QualitySettings_t1379802392_marshaled_pinvoke), sizeof(QualitySettings_t1379802392_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5648[4] = 
{
	QualitySettings_t1379802392::get_offset_of_prefilterBlur_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	QualitySettings_t1379802392::get_offset_of_medianFilter_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	QualitySettings_t1379802392::get_offset_of_dilateNearBlur_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	QualitySettings_t1379802392_StaticFields::get_offset_of_presetQualitySettings_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5649 = { sizeof (FocusSettings_t1261455774)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5649[9] = 
{
	FocusSettings_t1261455774::get_offset_of_transform_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FocusSettings_t1261455774::get_offset_of_focusPlane_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FocusSettings_t1261455774::get_offset_of_range_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FocusSettings_t1261455774::get_offset_of_nearPlane_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FocusSettings_t1261455774::get_offset_of_nearFalloff_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FocusSettings_t1261455774::get_offset_of_farPlane_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FocusSettings_t1261455774::get_offset_of_farFalloff_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FocusSettings_t1261455774::get_offset_of_nearBlurRadius_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FocusSettings_t1261455774::get_offset_of_farBlurRadius_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5650 = { sizeof (BokehTextureSettings_t3943002634)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5650[5] = 
{
	BokehTextureSettings_t3943002634::get_offset_of_texture_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	BokehTextureSettings_t3943002634::get_offset_of_scale_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	BokehTextureSettings_t3943002634::get_offset_of_intensity_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	BokehTextureSettings_t3943002634::get_offset_of_threshold_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	BokehTextureSettings_t3943002634::get_offset_of_spawnHeuristic_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5651 = { sizeof (TonemappingLog_t4031249120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5651[21] = 
{
	TonemappingLog_t4031249120::get_offset_of_enableAdaptive_5(),
	TonemappingLog_t4031249120::get_offset_of_debugClamp_6(),
	TonemappingLog_t4031249120::get_offset_of_middleGrey_7(),
	TonemappingLog_t4031249120::get_offset_of_adaptionSpeed_8(),
	TonemappingLog_t4031249120::get_offset_of_adaptiveMin_9(),
	TonemappingLog_t4031249120::get_offset_of_adaptiveMax_10(),
	TonemappingLog_t4031249120::get_offset_of_logMid_11(),
	TonemappingLog_t4031249120::get_offset_of_linearMid_12(),
	TonemappingLog_t4031249120::get_offset_of_dynamicRange_13(),
	TonemappingLog_t4031249120::get_offset_of_remapCurve_14(),
	TonemappingLog_t4031249120::get_offset_of_curveTex_15(),
	TonemappingLog_t4031249120::get_offset_of_lutTex_16(),
	TonemappingLog_t4031249120::get_offset_of_converted3DLut_17(),
	TonemappingLog_t4031249120::get_offset_of_lutTexName_18(),
	TonemappingLog_t4031249120::get_offset_of_tonemapperLog_19(),
	TonemappingLog_t4031249120::get_offset_of_validRenderTextureFormat_20(),
	TonemappingLog_t4031249120::get_offset_of_tonemapMaterial_21(),
	TonemappingLog_t4031249120::get_offset_of_rt_22(),
	TonemappingLog_t4031249120::get_offset_of_rtFormat_23(),
	TonemappingLog_t4031249120::get_offset_of_curveLen_24(),
	TonemappingLog_t4031249120::get_offset_of_curveData_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5652 = { sizeof (TonemappingLut_t4032363238), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5652[51] = 
{
	TonemappingLut_t4032363238::get_offset_of_enableAdaptive_5(),
	TonemappingLut_t4032363238::get_offset_of_enableFilmicCurve_6(),
	TonemappingLut_t4032363238::get_offset_of_enableUserLut_7(),
	TonemappingLut_t4032363238::get_offset_of_enableColorGrading_8(),
	TonemappingLut_t4032363238::get_offset_of_enableColorCurve_9(),
	TonemappingLut_t4032363238::get_offset_of_debugClamp_10(),
	TonemappingLut_t4032363238::get_offset_of_lutTex_11(),
	TonemappingLut_t4032363238::get_offset_of_lutCurveTex1D_12(),
	TonemappingLut_t4032363238::get_offset_of_middleGrey_13(),
	TonemappingLut_t4032363238::get_offset_of_adaptionSpeed_14(),
	TonemappingLut_t4032363238::get_offset_of_adaptiveMin_15(),
	TonemappingLut_t4032363238::get_offset_of_adaptiveMax_16(),
	TonemappingLut_t4032363238::get_offset_of_adaptiveDebug_17(),
	TonemappingLut_t4032363238::get_offset_of_lutExposureBias_18(),
	TonemappingLut_t4032363238::get_offset_of_lutWhiteBalance_19(),
	TonemappingLut_t4032363238::get_offset_of_lutContrast_20(),
	TonemappingLut_t4032363238::get_offset_of_lutSaturation_21(),
	TonemappingLut_t4032363238::get_offset_of_lutGamma_22(),
	TonemappingLut_t4032363238::get_offset_of_lutToe_23(),
	TonemappingLut_t4032363238::get_offset_of_lutShoulder_24(),
	TonemappingLut_t4032363238::get_offset_of_remapCurve_25(),
	TonemappingLut_t4032363238::get_offset_of_userLutTex_26(),
	TonemappingLut_t4032363238::get_offset_of_userLutName_27(),
	TonemappingLut_t4032363238::get_offset_of_lutShadows_28(),
	TonemappingLut_t4032363238::get_offset_of_lutMidtones_29(),
	TonemappingLut_t4032363238::get_offset_of_lutHighlights_30(),
	TonemappingLut_t4032363238::get_offset_of_cache_lutContrast_31(),
	TonemappingLut_t4032363238::get_offset_of_cache_lutSaturation_32(),
	TonemappingLut_t4032363238::get_offset_of_cache_lutGamma_33(),
	TonemappingLut_t4032363238::get_offset_of_cache_lutLowY_34(),
	TonemappingLut_t4032363238::get_offset_of_cache_lutHighY_35(),
	TonemappingLut_t4032363238::get_offset_of_cache_enableAdaptive_36(),
	TonemappingLut_t4032363238::get_offset_of_cache_enableFilmicCurve_37(),
	TonemappingLut_t4032363238::get_offset_of_cache_enableColorGrading_38(),
	TonemappingLut_t4032363238::get_offset_of_cache_enableUserLut_39(),
	TonemappingLut_t4032363238::get_offset_of_cache_enableColorCurve_40(),
	TonemappingLut_t4032363238::get_offset_of_cache_lutWhiteBalance_41(),
	TonemappingLut_t4032363238::get_offset_of_cache_lutHighlights_42(),
	TonemappingLut_t4032363238::get_offset_of_cache_lutMidtones_43(),
	TonemappingLut_t4032363238::get_offset_of_cache_lutShadows_44(),
	TonemappingLut_t4032363238::get_offset_of_cache_remapCurve_45(),
	TonemappingLut_t4032363238::get_offset_of_cache_userLutName_46(),
	TonemappingLut_t4032363238::get_offset_of_tonemapperLut_47(),
	TonemappingLut_t4032363238::get_offset_of_validRenderTextureFormat_48(),
	TonemappingLut_t4032363238::get_offset_of_tonemapMaterial_49(),
	TonemappingLut_t4032363238::get_offset_of_rt_50(),
	TonemappingLut_t4032363238::get_offset_of_rtFormat_51(),
	TonemappingLut_t4032363238::get_offset_of_curveLen_52(),
	TonemappingLut_t4032363238::get_offset_of_curveData_53(),
	TonemappingLut_t4032363238::get_offset_of_userLutDim_54(),
	TonemappingLut_t4032363238::get_offset_of_userLutData_55(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5653 = { sizeof (SimplePolyFunc_t3505133624)+ sizeof (Il2CppObject), sizeof(SimplePolyFunc_t3505133624 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5653[7] = 
{
	SimplePolyFunc_t3505133624::get_offset_of_A_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SimplePolyFunc_t3505133624::get_offset_of_B_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SimplePolyFunc_t3505133624::get_offset_of_x0_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SimplePolyFunc_t3505133624::get_offset_of_y0_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SimplePolyFunc_t3505133624::get_offset_of_signX_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SimplePolyFunc_t3505133624::get_offset_of_signY_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SimplePolyFunc_t3505133624::get_offset_of_logA_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5654 = { sizeof (AAMode_t1871701680)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5654[8] = 
{
	AAMode_t1871701680::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5655 = { sizeof (Antialiasing_t1691315015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5655[22] = 
{
	Antialiasing_t1691315015::get_offset_of_mode_5(),
	Antialiasing_t1691315015::get_offset_of_showGeneratedNormals_6(),
	Antialiasing_t1691315015::get_offset_of_offsetScale_7(),
	Antialiasing_t1691315015::get_offset_of_blurRadius_8(),
	Antialiasing_t1691315015::get_offset_of_edgeThresholdMin_9(),
	Antialiasing_t1691315015::get_offset_of_edgeThreshold_10(),
	Antialiasing_t1691315015::get_offset_of_edgeSharpness_11(),
	Antialiasing_t1691315015::get_offset_of_dlaaSharp_12(),
	Antialiasing_t1691315015::get_offset_of_ssaaShader_13(),
	Antialiasing_t1691315015::get_offset_of_ssaa_14(),
	Antialiasing_t1691315015::get_offset_of_dlaaShader_15(),
	Antialiasing_t1691315015::get_offset_of_dlaa_16(),
	Antialiasing_t1691315015::get_offset_of_nfaaShader_17(),
	Antialiasing_t1691315015::get_offset_of_nfaa_18(),
	Antialiasing_t1691315015::get_offset_of_shaderFXAAPreset2_19(),
	Antialiasing_t1691315015::get_offset_of_materialFXAAPreset2_20(),
	Antialiasing_t1691315015::get_offset_of_shaderFXAAPreset3_21(),
	Antialiasing_t1691315015::get_offset_of_materialFXAAPreset3_22(),
	Antialiasing_t1691315015::get_offset_of_shaderFXAAII_23(),
	Antialiasing_t1691315015::get_offset_of_materialFXAAII_24(),
	Antialiasing_t1691315015::get_offset_of_shaderFXAAIII_25(),
	Antialiasing_t1691315015::get_offset_of_materialFXAAIII_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5656 = { sizeof (Bloom_t1125654350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5656[30] = 
{
	Bloom_t1125654350::get_offset_of_tweakMode_5(),
	Bloom_t1125654350::get_offset_of_screenBlendMode_6(),
	Bloom_t1125654350::get_offset_of_hdr_7(),
	Bloom_t1125654350::get_offset_of_doHdr_8(),
	Bloom_t1125654350::get_offset_of_sepBlurSpread_9(),
	Bloom_t1125654350::get_offset_of_quality_10(),
	Bloom_t1125654350::get_offset_of_bloomIntensity_11(),
	Bloom_t1125654350::get_offset_of_bloomThreshold_12(),
	Bloom_t1125654350::get_offset_of_bloomThresholdColor_13(),
	Bloom_t1125654350::get_offset_of_bloomBlurIterations_14(),
	Bloom_t1125654350::get_offset_of_hollywoodFlareBlurIterations_15(),
	Bloom_t1125654350::get_offset_of_flareRotation_16(),
	Bloom_t1125654350::get_offset_of_lensflareMode_17(),
	Bloom_t1125654350::get_offset_of_hollyStretchWidth_18(),
	Bloom_t1125654350::get_offset_of_lensflareIntensity_19(),
	Bloom_t1125654350::get_offset_of_lensflareThreshold_20(),
	Bloom_t1125654350::get_offset_of_lensFlareSaturation_21(),
	Bloom_t1125654350::get_offset_of_flareColorA_22(),
	Bloom_t1125654350::get_offset_of_flareColorB_23(),
	Bloom_t1125654350::get_offset_of_flareColorC_24(),
	Bloom_t1125654350::get_offset_of_flareColorD_25(),
	Bloom_t1125654350::get_offset_of_lensFlareVignetteMask_26(),
	Bloom_t1125654350::get_offset_of_lensFlareShader_27(),
	Bloom_t1125654350::get_offset_of_lensFlareMaterial_28(),
	Bloom_t1125654350::get_offset_of_screenBlendShader_29(),
	Bloom_t1125654350::get_offset_of_screenBlend_30(),
	Bloom_t1125654350::get_offset_of_blurAndFlaresShader_31(),
	Bloom_t1125654350::get_offset_of_blurAndFlaresMaterial_32(),
	Bloom_t1125654350::get_offset_of_brightPassFilterShader_33(),
	Bloom_t1125654350::get_offset_of_brightPassFilterMaterial_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5657 = { sizeof (LensFlareStyle_t630413071)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5657[4] = 
{
	LensFlareStyle_t630413071::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5658 = { sizeof (TweakMode_t747557136)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5658[3] = 
{
	TweakMode_t747557136::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5659 = { sizeof (HDRBloomMode_t3774419504)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5659[4] = 
{
	HDRBloomMode_t3774419504::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5660 = { sizeof (BloomScreenBlendMode_t2012607685)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5660[3] = 
{
	BloomScreenBlendMode_t2012607685::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5661 = { sizeof (BloomQuality_t3369172721)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5661[3] = 
{
	BloomQuality_t3369172721::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5662 = { sizeof (LensflareStyle34_t4260782719)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5662[4] = 
{
	LensflareStyle34_t4260782719::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5663 = { sizeof (TweakMode34_t984135990)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5663[3] = 
{
	TweakMode34_t984135990::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5664 = { sizeof (HDRBloomMode_t4271191419)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5664[4] = 
{
	HDRBloomMode_t4271191419::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5665 = { sizeof (BloomScreenBlendMode_t19712272)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5665[3] = 
{
	BloomScreenBlendMode_t19712272::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5666 = { sizeof (BloomAndFlares_t2848767628), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5666[34] = 
{
	BloomAndFlares_t2848767628::get_offset_of_tweakMode_5(),
	BloomAndFlares_t2848767628::get_offset_of_screenBlendMode_6(),
	BloomAndFlares_t2848767628::get_offset_of_hdr_7(),
	BloomAndFlares_t2848767628::get_offset_of_doHdr_8(),
	BloomAndFlares_t2848767628::get_offset_of_sepBlurSpread_9(),
	BloomAndFlares_t2848767628::get_offset_of_useSrcAlphaAsMask_10(),
	BloomAndFlares_t2848767628::get_offset_of_bloomIntensity_11(),
	BloomAndFlares_t2848767628::get_offset_of_bloomThreshold_12(),
	BloomAndFlares_t2848767628::get_offset_of_bloomBlurIterations_13(),
	BloomAndFlares_t2848767628::get_offset_of_lensflares_14(),
	BloomAndFlares_t2848767628::get_offset_of_hollywoodFlareBlurIterations_15(),
	BloomAndFlares_t2848767628::get_offset_of_lensflareMode_16(),
	BloomAndFlares_t2848767628::get_offset_of_hollyStretchWidth_17(),
	BloomAndFlares_t2848767628::get_offset_of_lensflareIntensity_18(),
	BloomAndFlares_t2848767628::get_offset_of_lensflareThreshold_19(),
	BloomAndFlares_t2848767628::get_offset_of_flareColorA_20(),
	BloomAndFlares_t2848767628::get_offset_of_flareColorB_21(),
	BloomAndFlares_t2848767628::get_offset_of_flareColorC_22(),
	BloomAndFlares_t2848767628::get_offset_of_flareColorD_23(),
	BloomAndFlares_t2848767628::get_offset_of_lensFlareVignetteMask_24(),
	BloomAndFlares_t2848767628::get_offset_of_lensFlareShader_25(),
	BloomAndFlares_t2848767628::get_offset_of_lensFlareMaterial_26(),
	BloomAndFlares_t2848767628::get_offset_of_vignetteShader_27(),
	BloomAndFlares_t2848767628::get_offset_of_vignetteMaterial_28(),
	BloomAndFlares_t2848767628::get_offset_of_separableBlurShader_29(),
	BloomAndFlares_t2848767628::get_offset_of_separableBlurMaterial_30(),
	BloomAndFlares_t2848767628::get_offset_of_addBrightStuffOneOneShader_31(),
	BloomAndFlares_t2848767628::get_offset_of_addBrightStuffBlendOneOneMaterial_32(),
	BloomAndFlares_t2848767628::get_offset_of_screenBlendShader_33(),
	BloomAndFlares_t2848767628::get_offset_of_screenBlend_34(),
	BloomAndFlares_t2848767628::get_offset_of_hollywoodFlaresShader_35(),
	BloomAndFlares_t2848767628::get_offset_of_hollywoodFlaresMaterial_36(),
	BloomAndFlares_t2848767628::get_offset_of_brightPassFilterShader_37(),
	BloomAndFlares_t2848767628::get_offset_of_brightPassFilterMaterial_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5667 = { sizeof (BloomOptimized_t2685819829), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5667[8] = 
{
	BloomOptimized_t2685819829::get_offset_of_threshold_5(),
	BloomOptimized_t2685819829::get_offset_of_intensity_6(),
	BloomOptimized_t2685819829::get_offset_of_blurSize_7(),
	BloomOptimized_t2685819829::get_offset_of_resolution_8(),
	BloomOptimized_t2685819829::get_offset_of_blurIterations_9(),
	BloomOptimized_t2685819829::get_offset_of_blurType_10(),
	BloomOptimized_t2685819829::get_offset_of_fastBloomShader_11(),
	BloomOptimized_t2685819829::get_offset_of_fastBloomMaterial_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5668 = { sizeof (Resolution_t1804605042)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5668[3] = 
{
	Resolution_t1804605042::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5669 = { sizeof (BlurType_t2416258039)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5669[3] = 
{
	BlurType_t2416258039::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5670 = { sizeof (Blur_t1038294851), -1, sizeof(Blur_t1038294851_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5670[4] = 
{
	Blur_t1038294851::get_offset_of_iterations_2(),
	Blur_t1038294851::get_offset_of_blurSpread_3(),
	Blur_t1038294851::get_offset_of_blurShader_4(),
	Blur_t1038294851_StaticFields::get_offset_of_m_Material_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5671 = { sizeof (BlurOptimized_t3334654964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5671[6] = 
{
	BlurOptimized_t3334654964::get_offset_of_downsample_5(),
	BlurOptimized_t3334654964::get_offset_of_blurSize_6(),
	BlurOptimized_t3334654964::get_offset_of_blurIterations_7(),
	BlurOptimized_t3334654964::get_offset_of_blurType_8(),
	BlurOptimized_t3334654964::get_offset_of_blurShader_9(),
	BlurOptimized_t3334654964::get_offset_of_blurMaterial_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5672 = { sizeof (BlurType_t1046251128)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5672[3] = 
{
	BlurType_t1046251128::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5673 = { sizeof (CamMu_t1503422003), -1, sizeof(CamMu_t1503422003_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5673[30] = 
{
	CamMu_t1503422003_StaticFields::get_offset_of_MAX_RADIUS_5(),
	CamMu_t1503422003::get_offset_of_filterType_6(),
	CamMu_t1503422003::get_offset_of_preview_7(),
	CamMu_t1503422003::get_offset_of_previewScale_8(),
	CamMu_t1503422003::get_offset_of_movementScale_9(),
	CamMu_t1503422003::get_offset_of_rotationScale_10(),
	CamMu_t1503422003::get_offset_of_maxVelocity_11(),
	CamMu_t1503422003::get_offset_of_minVelocity_12(),
	CamMu_t1503422003::get_offset_of_velocityScale_13(),
	CamMu_t1503422003::get_offset_of_softZDistance_14(),
	CamMu_t1503422003::get_offset_of_velocityDownsample_15(),
	CamMu_t1503422003::get_offset_of_excludeLayers_16(),
	CamMu_t1503422003::get_offset_of_tmpCam_17(),
	CamMu_t1503422003::get_offset_of_shader_18(),
	CamMu_t1503422003::get_offset_of_dx11MotionBlurShader_19(),
	CamMu_t1503422003::get_offset_of_replacementClear_20(),
	CamMu_t1503422003::get_offset_of_motionBlurMaterial_21(),
	CamMu_t1503422003::get_offset_of_dx11MotionBlurMaterial_22(),
	CamMu_t1503422003::get_offset_of_noiseTexture_23(),
	CamMu_t1503422003::get_offset_of_jitter_24(),
	CamMu_t1503422003::get_offset_of_showVelocity_25(),
	CamMu_t1503422003::get_offset_of_showVelocityScale_26(),
	CamMu_t1503422003::get_offset_of_currentViewProjMat_27(),
	CamMu_t1503422003::get_offset_of_prevViewProjMat_28(),
	CamMu_t1503422003::get_offset_of_prevFrameCount_29(),
	CamMu_t1503422003::get_offset_of_wasActive_30(),
	CamMu_t1503422003::get_offset_of_prevFrameForward_31(),
	CamMu_t1503422003::get_offset_of_prevFrameUp_32(),
	CamMu_t1503422003::get_offset_of_prevFramePos_33(),
	CamMu_t1503422003::get_offset_of__camera_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5674 = { sizeof (MotionBlurFilter_t3705161775)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5674[6] = 
{
	MotionBlurFilter_t3705161775::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5675 = { sizeof (CameraMotionBlur_t2812046500), -1, sizeof(CameraMotionBlur_t2812046500_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5675[30] = 
{
	CameraMotionBlur_t2812046500_StaticFields::get_offset_of_MAX_RADIUS_5(),
	CameraMotionBlur_t2812046500::get_offset_of_filterType_6(),
	CameraMotionBlur_t2812046500::get_offset_of_preview_7(),
	CameraMotionBlur_t2812046500::get_offset_of_previewScale_8(),
	CameraMotionBlur_t2812046500::get_offset_of_movementScale_9(),
	CameraMotionBlur_t2812046500::get_offset_of_rotationScale_10(),
	CameraMotionBlur_t2812046500::get_offset_of_maxVelocity_11(),
	CameraMotionBlur_t2812046500::get_offset_of_minVelocity_12(),
	CameraMotionBlur_t2812046500::get_offset_of_velocityScale_13(),
	CameraMotionBlur_t2812046500::get_offset_of_softZDistance_14(),
	CameraMotionBlur_t2812046500::get_offset_of_velocityDownsample_15(),
	CameraMotionBlur_t2812046500::get_offset_of_excludeLayers_16(),
	CameraMotionBlur_t2812046500::get_offset_of_tmpCam_17(),
	CameraMotionBlur_t2812046500::get_offset_of_shader_18(),
	CameraMotionBlur_t2812046500::get_offset_of_dx11MotionBlurShader_19(),
	CameraMotionBlur_t2812046500::get_offset_of_replacementClear_20(),
	CameraMotionBlur_t2812046500::get_offset_of_motionBlurMaterial_21(),
	CameraMotionBlur_t2812046500::get_offset_of_dx11MotionBlurMaterial_22(),
	CameraMotionBlur_t2812046500::get_offset_of_noiseTexture_23(),
	CameraMotionBlur_t2812046500::get_offset_of_jitter_24(),
	CameraMotionBlur_t2812046500::get_offset_of_showVelocity_25(),
	CameraMotionBlur_t2812046500::get_offset_of_showVelocityScale_26(),
	CameraMotionBlur_t2812046500::get_offset_of_currentViewProjMat_27(),
	CameraMotionBlur_t2812046500::get_offset_of_prevViewProjMat_28(),
	CameraMotionBlur_t2812046500::get_offset_of_prevFrameCount_29(),
	CameraMotionBlur_t2812046500::get_offset_of_wasActive_30(),
	CameraMotionBlur_t2812046500::get_offset_of_prevFrameForward_31(),
	CameraMotionBlur_t2812046500::get_offset_of_prevFrameUp_32(),
	CameraMotionBlur_t2812046500::get_offset_of_prevFramePos_33(),
	CameraMotionBlur_t2812046500::get_offset_of__camera_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5676 = { sizeof (MotionBlurFilter_t520253047)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5676[6] = 
{
	MotionBlurFilter_t520253047::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5677 = { sizeof (ColorCorrectionCurves_t3742166504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5677[24] = 
{
	ColorCorrectionCurves_t3742166504::get_offset_of_redChannel_5(),
	ColorCorrectionCurves_t3742166504::get_offset_of_greenChannel_6(),
	ColorCorrectionCurves_t3742166504::get_offset_of_blueChannel_7(),
	ColorCorrectionCurves_t3742166504::get_offset_of_useDepthCorrection_8(),
	ColorCorrectionCurves_t3742166504::get_offset_of_zCurve_9(),
	ColorCorrectionCurves_t3742166504::get_offset_of_depthRedChannel_10(),
	ColorCorrectionCurves_t3742166504::get_offset_of_depthGreenChannel_11(),
	ColorCorrectionCurves_t3742166504::get_offset_of_depthBlueChannel_12(),
	ColorCorrectionCurves_t3742166504::get_offset_of_ccMaterial_13(),
	ColorCorrectionCurves_t3742166504::get_offset_of_ccDepthMaterial_14(),
	ColorCorrectionCurves_t3742166504::get_offset_of_selectiveCcMaterial_15(),
	ColorCorrectionCurves_t3742166504::get_offset_of_rgbChannelTex_16(),
	ColorCorrectionCurves_t3742166504::get_offset_of_rgbDepthChannelTex_17(),
	ColorCorrectionCurves_t3742166504::get_offset_of_zCurveTex_18(),
	ColorCorrectionCurves_t3742166504::get_offset_of_saturation_19(),
	ColorCorrectionCurves_t3742166504::get_offset_of_selectiveCc_20(),
	ColorCorrectionCurves_t3742166504::get_offset_of_selectiveFromColor_21(),
	ColorCorrectionCurves_t3742166504::get_offset_of_selectiveToColor_22(),
	ColorCorrectionCurves_t3742166504::get_offset_of_mode_23(),
	ColorCorrectionCurves_t3742166504::get_offset_of_updateTextures_24(),
	ColorCorrectionCurves_t3742166504::get_offset_of_colorCorrectionCurvesShader_25(),
	ColorCorrectionCurves_t3742166504::get_offset_of_simpleColorCorrectionCurvesShader_26(),
	ColorCorrectionCurves_t3742166504::get_offset_of_colorCorrectionSelectiveShader_27(),
	ColorCorrectionCurves_t3742166504::get_offset_of_updateTexturesOnStartup_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5678 = { sizeof (ColorCorrectionMode_t1416458051)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5678[3] = 
{
	ColorCorrectionMode_t1416458051::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5679 = { sizeof (ColorCorrectionLookup_t1159177774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5679[4] = 
{
	ColorCorrectionLookup_t1159177774::get_offset_of_shader_5(),
	ColorCorrectionLookup_t1159177774::get_offset_of_material_6(),
	ColorCorrectionLookup_t1159177774::get_offset_of_converted3DLut_7(),
	ColorCorrectionLookup_t1159177774::get_offset_of_basedOnTempTex_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5680 = { sizeof (ColorCorrectionRamp_t3562116199), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5680[1] = 
{
	ColorCorrectionRamp_t3562116199::get_offset_of_textureRamp_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5681 = { sizeof (ContrastEnhance_t640919481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5681[7] = 
{
	ContrastEnhance_t640919481::get_offset_of_intensity_5(),
	ContrastEnhance_t640919481::get_offset_of_threshold_6(),
	ContrastEnhance_t640919481::get_offset_of_separableBlurMaterial_7(),
	ContrastEnhance_t640919481::get_offset_of_contrastCompositeMaterial_8(),
	ContrastEnhance_t640919481::get_offset_of_blurSpread_9(),
	ContrastEnhance_t640919481::get_offset_of_separableBlurShader_10(),
	ContrastEnhance_t640919481::get_offset_of_contrastCompositeShader_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5682 = { sizeof (ContrastStretch_t3424449263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5682[13] = 
{
	ContrastStretch_t3424449263::get_offset_of_adaptationSpeed_2(),
	ContrastStretch_t3424449263::get_offset_of_limitMinimum_3(),
	ContrastStretch_t3424449263::get_offset_of_limitMaximum_4(),
	ContrastStretch_t3424449263::get_offset_of_adaptRenderTex_5(),
	ContrastStretch_t3424449263::get_offset_of_curAdaptIndex_6(),
	ContrastStretch_t3424449263::get_offset_of_shaderLum_7(),
	ContrastStretch_t3424449263::get_offset_of_m_materialLum_8(),
	ContrastStretch_t3424449263::get_offset_of_shaderReduce_9(),
	ContrastStretch_t3424449263::get_offset_of_m_materialReduce_10(),
	ContrastStretch_t3424449263::get_offset_of_shaderAdapt_11(),
	ContrastStretch_t3424449263::get_offset_of_m_materialAdapt_12(),
	ContrastStretch_t3424449263::get_offset_of_shaderApply_13(),
	ContrastStretch_t3424449263::get_offset_of_m_materialApply_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5683 = { sizeof (CreaseShading_t1200394124), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5683[9] = 
{
	CreaseShading_t1200394124::get_offset_of_intensity_5(),
	CreaseShading_t1200394124::get_offset_of_softness_6(),
	CreaseShading_t1200394124::get_offset_of_spread_7(),
	CreaseShading_t1200394124::get_offset_of_blurShader_8(),
	CreaseShading_t1200394124::get_offset_of_blurMaterial_9(),
	CreaseShading_t1200394124::get_offset_of_depthFetchShader_10(),
	CreaseShading_t1200394124::get_offset_of_depthFetchMaterial_11(),
	CreaseShading_t1200394124::get_offset_of_creaseApplyShader_12(),
	CreaseShading_t1200394124::get_offset_of_creaseApplyMaterial_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5684 = { sizeof (DepthOfField_t1116783936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5684[24] = 
{
	DepthOfField_t1116783936::get_offset_of_visualizeFocus_5(),
	DepthOfField_t1116783936::get_offset_of_focalLength_6(),
	DepthOfField_t1116783936::get_offset_of_focalSize_7(),
	DepthOfField_t1116783936::get_offset_of_aperture_8(),
	DepthOfField_t1116783936::get_offset_of_focalTransform_9(),
	DepthOfField_t1116783936::get_offset_of_maxBlurSize_10(),
	DepthOfField_t1116783936::get_offset_of_highResolution_11(),
	DepthOfField_t1116783936::get_offset_of_blurType_12(),
	DepthOfField_t1116783936::get_offset_of_blurSampleCount_13(),
	DepthOfField_t1116783936::get_offset_of_nearBlur_14(),
	DepthOfField_t1116783936::get_offset_of_foregroundOverlap_15(),
	DepthOfField_t1116783936::get_offset_of_dofHdrShader_16(),
	DepthOfField_t1116783936::get_offset_of_dofHdrMaterial_17(),
	DepthOfField_t1116783936::get_offset_of_dx11BokehShader_18(),
	DepthOfField_t1116783936::get_offset_of_dx11bokehMaterial_19(),
	DepthOfField_t1116783936::get_offset_of_dx11BokehThreshold_20(),
	DepthOfField_t1116783936::get_offset_of_dx11SpawnHeuristic_21(),
	DepthOfField_t1116783936::get_offset_of_dx11BokehTexture_22(),
	DepthOfField_t1116783936::get_offset_of_dx11BokehScale_23(),
	DepthOfField_t1116783936::get_offset_of_dx11BokehIntensity_24(),
	DepthOfField_t1116783936::get_offset_of_focalDistance01_25(),
	DepthOfField_t1116783936::get_offset_of_cbDrawArgs_26(),
	DepthOfField_t1116783936::get_offset_of_cbPoints_27(),
	DepthOfField_t1116783936::get_offset_of_internalBlurWidth_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5685 = { sizeof (BlurType_t3871645803)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5685[3] = 
{
	BlurType_t3871645803::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5686 = { sizeof (BlurSampleCount_t3210294001)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5686[4] = 
{
	BlurSampleCount_t3210294001::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5687 = { sizeof (DepthOfFieldDeprecated_t4187663194), -1, sizeof(DepthOfFieldDeprecated_t4187663194_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5687[43] = 
{
	DepthOfFieldDeprecated_t4187663194_StaticFields::get_offset_of_SMOOTH_DOWNSAMPLE_PASS_5(),
	DepthOfFieldDeprecated_t4187663194_StaticFields::get_offset_of_BOKEH_EXTRA_BLUR_6(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_quality_7(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_resolution_8(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_simpleTweakMode_9(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_focalPoint_10(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_smoothness_11(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_focalZDistance_12(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_focalZStartCurve_13(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_focalZEndCurve_14(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_focalStartCurve_15(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_focalEndCurve_16(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_focalDistance01_17(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_objectFocus_18(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_focalSize_19(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_bluriness_20(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_maxBlurSpread_21(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_foregroundBlurExtrude_22(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_dofBlurShader_23(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_dofBlurMaterial_24(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_dofShader_25(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_dofMaterial_26(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_visualize_27(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_bokehDestination_28(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_widthOverHeight_29(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_oneOverBaseSize_30(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_bokeh_31(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_bokehSupport_32(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_bokehShader_33(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_bokehTexture_34(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_bokehScale_35(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_bokehIntensity_36(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_bokehThresholdContrast_37(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_bokehThresholdLuminance_38(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_bokehDownsample_39(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_bokehMaterial_40(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of__camera_41(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_foregroundTexture_42(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_mediumRezWorkTexture_43(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_finalDefocus_44(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_lowRezWorkTexture_45(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_bokehSource_46(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_bokehSource2_47(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5688 = { sizeof (Dof34QualitySetting_t3636551379)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5688[3] = 
{
	Dof34QualitySetting_t3636551379::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5689 = { sizeof (DofResolution_t1566655669)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5689[4] = 
{
	DofResolution_t1566655669::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5690 = { sizeof (DofBlurriness_t473098480)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5690[4] = 
{
	DofBlurriness_t473098480::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5691 = { sizeof (BokehDestination_t1233703462)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5691[4] = 
{
	BokehDestination_t1233703462::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5692 = { sizeof (EdgeDetection_t506487406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5692[11] = 
{
	EdgeDetection_t506487406::get_offset_of_mode_5(),
	EdgeDetection_t506487406::get_offset_of_sensitivityDepth_6(),
	EdgeDetection_t506487406::get_offset_of_sensitivityNormals_7(),
	EdgeDetection_t506487406::get_offset_of_lumThreshold_8(),
	EdgeDetection_t506487406::get_offset_of_edgeExp_9(),
	EdgeDetection_t506487406::get_offset_of_sampleDist_10(),
	EdgeDetection_t506487406::get_offset_of_edgesOnly_11(),
	EdgeDetection_t506487406::get_offset_of_edgesOnlyBgColor_12(),
	EdgeDetection_t506487406::get_offset_of_edgeDetectShader_13(),
	EdgeDetection_t506487406::get_offset_of_edgeDetectMaterial_14(),
	EdgeDetection_t506487406::get_offset_of_oldMode_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5693 = { sizeof (EdgeDetectMode_t1984240676)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5693[6] = 
{
	EdgeDetectMode_t1984240676::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5694 = { sizeof (Fisheye_t4101461743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5694[4] = 
{
	Fisheye_t4101461743::get_offset_of_strengthX_5(),
	Fisheye_t4101461743::get_offset_of_strengthY_6(),
	Fisheye_t4101461743::get_offset_of_fishEyeShader_7(),
	Fisheye_t4101461743::get_offset_of_fisheyeMaterial_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5695 = { sizeof (GlobalFog_t900542613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5695[9] = 
{
	GlobalFog_t900542613::get_offset_of_distanceFog_5(),
	GlobalFog_t900542613::get_offset_of_excludeFarPixels_6(),
	GlobalFog_t900542613::get_offset_of_useRadialDistance_7(),
	GlobalFog_t900542613::get_offset_of_heightFog_8(),
	GlobalFog_t900542613::get_offset_of_height_9(),
	GlobalFog_t900542613::get_offset_of_heightDensity_10(),
	GlobalFog_t900542613::get_offset_of_startDistance_11(),
	GlobalFog_t900542613::get_offset_of_fogShader_12(),
	GlobalFog_t900542613::get_offset_of_fogMaterial_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5696 = { sizeof (Grayscale_t1707485390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5696[2] = 
{
	Grayscale_t1707485390::get_offset_of_textureRamp_4(),
	Grayscale_t1707485390::get_offset_of_rampOffset_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5697 = { sizeof (ImageEffectBase_t2026006575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5697[2] = 
{
	ImageEffectBase_t2026006575::get_offset_of_shader_2(),
	ImageEffectBase_t2026006575::get_offset_of_m_Material_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5698 = { sizeof (ImageEffects_t1214077586), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5699 = { sizeof (MotionBlur_t1587267364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5699[3] = 
{
	MotionBlur_t1587267364::get_offset_of_blurAmount_4(),
	MotionBlur_t1587267364::get_offset_of_extraBlur_5(),
	MotionBlur_t1587267364::get_offset_of_accumTexture_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
