﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"

// System.Web.UI.PersistChildrenAttribute
struct PersistChildrenAttribute_t3161469854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.PersistChildrenAttribute
struct  PersistChildrenAttribute_t3161469854  : public Attribute_t861562559
{
public:
	// System.Boolean System.Web.UI.PersistChildrenAttribute::persist
	bool ___persist_0;
	// System.Boolean System.Web.UI.PersistChildrenAttribute::usesCustomPersistence
	bool ___usesCustomPersistence_1;

public:
	inline static int32_t get_offset_of_persist_0() { return static_cast<int32_t>(offsetof(PersistChildrenAttribute_t3161469854, ___persist_0)); }
	inline bool get_persist_0() const { return ___persist_0; }
	inline bool* get_address_of_persist_0() { return &___persist_0; }
	inline void set_persist_0(bool value)
	{
		___persist_0 = value;
	}

	inline static int32_t get_offset_of_usesCustomPersistence_1() { return static_cast<int32_t>(offsetof(PersistChildrenAttribute_t3161469854, ___usesCustomPersistence_1)); }
	inline bool get_usesCustomPersistence_1() const { return ___usesCustomPersistence_1; }
	inline bool* get_address_of_usesCustomPersistence_1() { return &___usesCustomPersistence_1; }
	inline void set_usesCustomPersistence_1(bool value)
	{
		___usesCustomPersistence_1 = value;
	}
};

struct PersistChildrenAttribute_t3161469854_StaticFields
{
public:
	// System.Web.UI.PersistChildrenAttribute System.Web.UI.PersistChildrenAttribute::Default
	PersistChildrenAttribute_t3161469854 * ___Default_2;
	// System.Web.UI.PersistChildrenAttribute System.Web.UI.PersistChildrenAttribute::Yes
	PersistChildrenAttribute_t3161469854 * ___Yes_3;
	// System.Web.UI.PersistChildrenAttribute System.Web.UI.PersistChildrenAttribute::No
	PersistChildrenAttribute_t3161469854 * ___No_4;

public:
	inline static int32_t get_offset_of_Default_2() { return static_cast<int32_t>(offsetof(PersistChildrenAttribute_t3161469854_StaticFields, ___Default_2)); }
	inline PersistChildrenAttribute_t3161469854 * get_Default_2() const { return ___Default_2; }
	inline PersistChildrenAttribute_t3161469854 ** get_address_of_Default_2() { return &___Default_2; }
	inline void set_Default_2(PersistChildrenAttribute_t3161469854 * value)
	{
		___Default_2 = value;
		Il2CppCodeGenWriteBarrier(&___Default_2, value);
	}

	inline static int32_t get_offset_of_Yes_3() { return static_cast<int32_t>(offsetof(PersistChildrenAttribute_t3161469854_StaticFields, ___Yes_3)); }
	inline PersistChildrenAttribute_t3161469854 * get_Yes_3() const { return ___Yes_3; }
	inline PersistChildrenAttribute_t3161469854 ** get_address_of_Yes_3() { return &___Yes_3; }
	inline void set_Yes_3(PersistChildrenAttribute_t3161469854 * value)
	{
		___Yes_3 = value;
		Il2CppCodeGenWriteBarrier(&___Yes_3, value);
	}

	inline static int32_t get_offset_of_No_4() { return static_cast<int32_t>(offsetof(PersistChildrenAttribute_t3161469854_StaticFields, ___No_4)); }
	inline PersistChildrenAttribute_t3161469854 * get_No_4() const { return ___No_4; }
	inline PersistChildrenAttribute_t3161469854 ** get_address_of_No_4() { return &___No_4; }
	inline void set_No_4(PersistChildrenAttribute_t3161469854 * value)
	{
		___No_4 = value;
		Il2CppCodeGenWriteBarrier(&___No_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
