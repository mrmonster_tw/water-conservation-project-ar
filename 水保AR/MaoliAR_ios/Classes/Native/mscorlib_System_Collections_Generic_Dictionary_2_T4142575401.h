﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_MulticastDelegate157516450.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23025488424.h"

// System.Type
struct Type_t;
// System.ServiceModel.Configuration.BindingElementExtensionElement
struct BindingElementExtensionElement_t2478436489;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Transform`1<System.Type,System.ServiceModel.Configuration.BindingElementExtensionElement,System.Collections.Generic.KeyValuePair`2<System.Type,System.ServiceModel.Configuration.BindingElementExtensionElement>>
struct  Transform_1_t4142575401  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
