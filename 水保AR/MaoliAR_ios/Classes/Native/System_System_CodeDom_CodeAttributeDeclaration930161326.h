﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.CodeDom.CodeAttributeArgumentCollection
struct CodeAttributeArgumentCollection_t2392619326;
// System.CodeDom.CodeTypeReference
struct CodeTypeReference_t3809997434;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeAttributeDeclaration
struct  CodeAttributeDeclaration_t930161326  : public Il2CppObject
{
public:
	// System.String System.CodeDom.CodeAttributeDeclaration::name
	String_t* ___name_0;
	// System.CodeDom.CodeAttributeArgumentCollection System.CodeDom.CodeAttributeDeclaration::arguments
	CodeAttributeArgumentCollection_t2392619326 * ___arguments_1;
	// System.CodeDom.CodeTypeReference System.CodeDom.CodeAttributeDeclaration::attribute
	CodeTypeReference_t3809997434 * ___attribute_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(CodeAttributeDeclaration_t930161326, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_arguments_1() { return static_cast<int32_t>(offsetof(CodeAttributeDeclaration_t930161326, ___arguments_1)); }
	inline CodeAttributeArgumentCollection_t2392619326 * get_arguments_1() const { return ___arguments_1; }
	inline CodeAttributeArgumentCollection_t2392619326 ** get_address_of_arguments_1() { return &___arguments_1; }
	inline void set_arguments_1(CodeAttributeArgumentCollection_t2392619326 * value)
	{
		___arguments_1 = value;
		Il2CppCodeGenWriteBarrier(&___arguments_1, value);
	}

	inline static int32_t get_offset_of_attribute_2() { return static_cast<int32_t>(offsetof(CodeAttributeDeclaration_t930161326, ___attribute_2)); }
	inline CodeTypeReference_t3809997434 * get_attribute_2() const { return ___attribute_2; }
	inline CodeTypeReference_t3809997434 ** get_address_of_attribute_2() { return &___attribute_2; }
	inline void set_attribute_2(CodeTypeReference_t3809997434 * value)
	{
		___attribute_2 = value;
		Il2CppCodeGenWriteBarrier(&___attribute_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
