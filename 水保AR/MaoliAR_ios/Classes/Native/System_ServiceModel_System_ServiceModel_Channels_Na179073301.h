﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_TimeSpan881159249.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.NamedPipeConnectionPoolSettings
struct  NamedPipeConnectionPoolSettings_t179073301  : public Il2CppObject
{
public:
	// System.String System.ServiceModel.Channels.NamedPipeConnectionPoolSettings::group_name
	String_t* ___group_name_0;
	// System.TimeSpan System.ServiceModel.Channels.NamedPipeConnectionPoolSettings::idle_timeout
	TimeSpan_t881159249  ___idle_timeout_1;
	// System.Int32 System.ServiceModel.Channels.NamedPipeConnectionPoolSettings::max_conn
	int32_t ___max_conn_2;

public:
	inline static int32_t get_offset_of_group_name_0() { return static_cast<int32_t>(offsetof(NamedPipeConnectionPoolSettings_t179073301, ___group_name_0)); }
	inline String_t* get_group_name_0() const { return ___group_name_0; }
	inline String_t** get_address_of_group_name_0() { return &___group_name_0; }
	inline void set_group_name_0(String_t* value)
	{
		___group_name_0 = value;
		Il2CppCodeGenWriteBarrier(&___group_name_0, value);
	}

	inline static int32_t get_offset_of_idle_timeout_1() { return static_cast<int32_t>(offsetof(NamedPipeConnectionPoolSettings_t179073301, ___idle_timeout_1)); }
	inline TimeSpan_t881159249  get_idle_timeout_1() const { return ___idle_timeout_1; }
	inline TimeSpan_t881159249 * get_address_of_idle_timeout_1() { return &___idle_timeout_1; }
	inline void set_idle_timeout_1(TimeSpan_t881159249  value)
	{
		___idle_timeout_1 = value;
	}

	inline static int32_t get_offset_of_max_conn_2() { return static_cast<int32_t>(offsetof(NamedPipeConnectionPoolSettings_t179073301, ___max_conn_2)); }
	inline int32_t get_max_conn_2() const { return ___max_conn_2; }
	inline int32_t* get_address_of_max_conn_2() { return &___max_conn_2; }
	inline void set_max_conn_2(int32_t value)
	{
		___max_conn_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
