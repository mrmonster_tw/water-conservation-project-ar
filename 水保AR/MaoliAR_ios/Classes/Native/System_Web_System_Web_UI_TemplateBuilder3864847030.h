﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_ControlBuilder2523018631.h"

// System.String
struct String_t;
// System.Web.UI.TemplateContainerAttribute
struct TemplateContainerAttribute_t1232723725;
// System.Web.UI.TemplateInstanceAttribute
struct TemplateInstanceAttribute_t1309238595;
// System.Collections.Generic.List`1<System.Web.UI.TemplateBinding>
struct List_1_t840632935;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.TemplateBuilder
struct  TemplateBuilder_t3864847030  : public ControlBuilder_t2523018631
{
public:
	// System.String System.Web.UI.TemplateBuilder::text
	String_t* ___text_28;
	// System.Web.UI.TemplateContainerAttribute System.Web.UI.TemplateBuilder::containerAttribute
	TemplateContainerAttribute_t1232723725 * ___containerAttribute_29;
	// System.Web.UI.TemplateInstanceAttribute System.Web.UI.TemplateBuilder::instanceAttribute
	TemplateInstanceAttribute_t1309238595 * ___instanceAttribute_30;
	// System.Collections.Generic.List`1<System.Web.UI.TemplateBinding> System.Web.UI.TemplateBuilder::bindings
	List_1_t840632935 * ___bindings_31;

public:
	inline static int32_t get_offset_of_text_28() { return static_cast<int32_t>(offsetof(TemplateBuilder_t3864847030, ___text_28)); }
	inline String_t* get_text_28() const { return ___text_28; }
	inline String_t** get_address_of_text_28() { return &___text_28; }
	inline void set_text_28(String_t* value)
	{
		___text_28 = value;
		Il2CppCodeGenWriteBarrier(&___text_28, value);
	}

	inline static int32_t get_offset_of_containerAttribute_29() { return static_cast<int32_t>(offsetof(TemplateBuilder_t3864847030, ___containerAttribute_29)); }
	inline TemplateContainerAttribute_t1232723725 * get_containerAttribute_29() const { return ___containerAttribute_29; }
	inline TemplateContainerAttribute_t1232723725 ** get_address_of_containerAttribute_29() { return &___containerAttribute_29; }
	inline void set_containerAttribute_29(TemplateContainerAttribute_t1232723725 * value)
	{
		___containerAttribute_29 = value;
		Il2CppCodeGenWriteBarrier(&___containerAttribute_29, value);
	}

	inline static int32_t get_offset_of_instanceAttribute_30() { return static_cast<int32_t>(offsetof(TemplateBuilder_t3864847030, ___instanceAttribute_30)); }
	inline TemplateInstanceAttribute_t1309238595 * get_instanceAttribute_30() const { return ___instanceAttribute_30; }
	inline TemplateInstanceAttribute_t1309238595 ** get_address_of_instanceAttribute_30() { return &___instanceAttribute_30; }
	inline void set_instanceAttribute_30(TemplateInstanceAttribute_t1309238595 * value)
	{
		___instanceAttribute_30 = value;
		Il2CppCodeGenWriteBarrier(&___instanceAttribute_30, value);
	}

	inline static int32_t get_offset_of_bindings_31() { return static_cast<int32_t>(offsetof(TemplateBuilder_t3864847030, ___bindings_31)); }
	inline List_1_t840632935 * get_bindings_31() const { return ___bindings_31; }
	inline List_1_t840632935 ** get_address_of_bindings_31() { return &___bindings_31; }
	inline void set_bindings_31(List_1_t840632935 * value)
	{
		___bindings_31 = value;
		Il2CppCodeGenWriteBarrier(&___bindings_31, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
