﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_MarshalByRefObject2760389100.h"

// System.Web.Hosting.VirtualPathProvider
struct VirtualPathProvider_t2835917181;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Hosting.VirtualPathProvider
struct  VirtualPathProvider_t2835917181  : public MarshalByRefObject_t2760389100
{
public:
	// System.Web.Hosting.VirtualPathProvider System.Web.Hosting.VirtualPathProvider::prev
	VirtualPathProvider_t2835917181 * ___prev_1;

public:
	inline static int32_t get_offset_of_prev_1() { return static_cast<int32_t>(offsetof(VirtualPathProvider_t2835917181, ___prev_1)); }
	inline VirtualPathProvider_t2835917181 * get_prev_1() const { return ___prev_1; }
	inline VirtualPathProvider_t2835917181 ** get_address_of_prev_1() { return &___prev_1; }
	inline void set_prev_1(VirtualPathProvider_t2835917181 * value)
	{
		___prev_1 = value;
		Il2CppCodeGenWriteBarrier(&___prev_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
