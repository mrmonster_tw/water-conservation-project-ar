﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_HtmlControls_HtmlInputCon1839583126.h"

// System.Web.HttpPostedFile
struct HttpPostedFile_t2439047419;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.HtmlControls.HtmlInputFile
struct  HtmlInputFile_t3149771952  : public HtmlInputControl_t1839583126
{
public:
	// System.Web.HttpPostedFile System.Web.UI.HtmlControls.HtmlInputFile::posted_file
	HttpPostedFile_t2439047419 * ___posted_file_30;

public:
	inline static int32_t get_offset_of_posted_file_30() { return static_cast<int32_t>(offsetof(HtmlInputFile_t3149771952, ___posted_file_30)); }
	inline HttpPostedFile_t2439047419 * get_posted_file_30() const { return ___posted_file_30; }
	inline HttpPostedFile_t2439047419 ** get_address_of_posted_file_30() { return &___posted_file_30; }
	inline void set_posted_file_30(HttpPostedFile_t2439047419 * value)
	{
		___posted_file_30 = value;
		Il2CppCodeGenWriteBarrier(&___posted_file_30, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
