﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.BuildManager/PreCompilationData
struct  PreCompilationData_t3220258209  : public Il2CppObject
{
public:
	// System.String System.Web.Compilation.BuildManager/PreCompilationData::VirtualPath
	String_t* ___VirtualPath_0;
	// System.String System.Web.Compilation.BuildManager/PreCompilationData::AssemblyFileName
	String_t* ___AssemblyFileName_1;
	// System.String System.Web.Compilation.BuildManager/PreCompilationData::TypeName
	String_t* ___TypeName_2;
	// System.Type System.Web.Compilation.BuildManager/PreCompilationData::Type
	Type_t * ___Type_3;

public:
	inline static int32_t get_offset_of_VirtualPath_0() { return static_cast<int32_t>(offsetof(PreCompilationData_t3220258209, ___VirtualPath_0)); }
	inline String_t* get_VirtualPath_0() const { return ___VirtualPath_0; }
	inline String_t** get_address_of_VirtualPath_0() { return &___VirtualPath_0; }
	inline void set_VirtualPath_0(String_t* value)
	{
		___VirtualPath_0 = value;
		Il2CppCodeGenWriteBarrier(&___VirtualPath_0, value);
	}

	inline static int32_t get_offset_of_AssemblyFileName_1() { return static_cast<int32_t>(offsetof(PreCompilationData_t3220258209, ___AssemblyFileName_1)); }
	inline String_t* get_AssemblyFileName_1() const { return ___AssemblyFileName_1; }
	inline String_t** get_address_of_AssemblyFileName_1() { return &___AssemblyFileName_1; }
	inline void set_AssemblyFileName_1(String_t* value)
	{
		___AssemblyFileName_1 = value;
		Il2CppCodeGenWriteBarrier(&___AssemblyFileName_1, value);
	}

	inline static int32_t get_offset_of_TypeName_2() { return static_cast<int32_t>(offsetof(PreCompilationData_t3220258209, ___TypeName_2)); }
	inline String_t* get_TypeName_2() const { return ___TypeName_2; }
	inline String_t** get_address_of_TypeName_2() { return &___TypeName_2; }
	inline void set_TypeName_2(String_t* value)
	{
		___TypeName_2 = value;
		Il2CppCodeGenWriteBarrier(&___TypeName_2, value);
	}

	inline static int32_t get_offset_of_Type_3() { return static_cast<int32_t>(offsetof(PreCompilationData_t3220258209, ___Type_3)); }
	inline Type_t * get_Type_3() const { return ___Type_3; }
	inline Type_t ** get_address_of_Type_3() { return &___Type_3; }
	inline void set_Type_3(Type_t * value)
	{
		___Type_3 = value;
		Il2CppCodeGenWriteBarrier(&___Type_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
