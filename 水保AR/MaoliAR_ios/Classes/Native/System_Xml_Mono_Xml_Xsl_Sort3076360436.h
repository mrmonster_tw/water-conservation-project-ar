﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Xml_System_Xml_XPath_XmlDataType3437356259.h"
#include "System_Xml_System_Xml_XPath_XmlSortOrder4114893492.h"
#include "System_Xml_System_Xml_XPath_XmlCaseOrder176547839.h"

// System.String
struct String_t;
// Mono.Xml.Xsl.Operations.XslAvt
struct XslAvt_t1645109359;
// System.Xml.XPath.XPathExpression
struct XPathExpression_t1723793351;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Sort
struct  Sort_t3076360436  : public Il2CppObject
{
public:
	// System.String Mono.Xml.Xsl.Sort::lang
	String_t* ___lang_0;
	// System.Xml.XPath.XmlDataType Mono.Xml.Xsl.Sort::dataType
	int32_t ___dataType_1;
	// System.Xml.XPath.XmlSortOrder Mono.Xml.Xsl.Sort::order
	int32_t ___order_2;
	// System.Xml.XPath.XmlCaseOrder Mono.Xml.Xsl.Sort::caseOrder
	int32_t ___caseOrder_3;
	// Mono.Xml.Xsl.Operations.XslAvt Mono.Xml.Xsl.Sort::langAvt
	XslAvt_t1645109359 * ___langAvt_4;
	// Mono.Xml.Xsl.Operations.XslAvt Mono.Xml.Xsl.Sort::dataTypeAvt
	XslAvt_t1645109359 * ___dataTypeAvt_5;
	// Mono.Xml.Xsl.Operations.XslAvt Mono.Xml.Xsl.Sort::orderAvt
	XslAvt_t1645109359 * ___orderAvt_6;
	// Mono.Xml.Xsl.Operations.XslAvt Mono.Xml.Xsl.Sort::caseOrderAvt
	XslAvt_t1645109359 * ___caseOrderAvt_7;
	// System.Xml.XPath.XPathExpression Mono.Xml.Xsl.Sort::expr
	XPathExpression_t1723793351 * ___expr_8;

public:
	inline static int32_t get_offset_of_lang_0() { return static_cast<int32_t>(offsetof(Sort_t3076360436, ___lang_0)); }
	inline String_t* get_lang_0() const { return ___lang_0; }
	inline String_t** get_address_of_lang_0() { return &___lang_0; }
	inline void set_lang_0(String_t* value)
	{
		___lang_0 = value;
		Il2CppCodeGenWriteBarrier(&___lang_0, value);
	}

	inline static int32_t get_offset_of_dataType_1() { return static_cast<int32_t>(offsetof(Sort_t3076360436, ___dataType_1)); }
	inline int32_t get_dataType_1() const { return ___dataType_1; }
	inline int32_t* get_address_of_dataType_1() { return &___dataType_1; }
	inline void set_dataType_1(int32_t value)
	{
		___dataType_1 = value;
	}

	inline static int32_t get_offset_of_order_2() { return static_cast<int32_t>(offsetof(Sort_t3076360436, ___order_2)); }
	inline int32_t get_order_2() const { return ___order_2; }
	inline int32_t* get_address_of_order_2() { return &___order_2; }
	inline void set_order_2(int32_t value)
	{
		___order_2 = value;
	}

	inline static int32_t get_offset_of_caseOrder_3() { return static_cast<int32_t>(offsetof(Sort_t3076360436, ___caseOrder_3)); }
	inline int32_t get_caseOrder_3() const { return ___caseOrder_3; }
	inline int32_t* get_address_of_caseOrder_3() { return &___caseOrder_3; }
	inline void set_caseOrder_3(int32_t value)
	{
		___caseOrder_3 = value;
	}

	inline static int32_t get_offset_of_langAvt_4() { return static_cast<int32_t>(offsetof(Sort_t3076360436, ___langAvt_4)); }
	inline XslAvt_t1645109359 * get_langAvt_4() const { return ___langAvt_4; }
	inline XslAvt_t1645109359 ** get_address_of_langAvt_4() { return &___langAvt_4; }
	inline void set_langAvt_4(XslAvt_t1645109359 * value)
	{
		___langAvt_4 = value;
		Il2CppCodeGenWriteBarrier(&___langAvt_4, value);
	}

	inline static int32_t get_offset_of_dataTypeAvt_5() { return static_cast<int32_t>(offsetof(Sort_t3076360436, ___dataTypeAvt_5)); }
	inline XslAvt_t1645109359 * get_dataTypeAvt_5() const { return ___dataTypeAvt_5; }
	inline XslAvt_t1645109359 ** get_address_of_dataTypeAvt_5() { return &___dataTypeAvt_5; }
	inline void set_dataTypeAvt_5(XslAvt_t1645109359 * value)
	{
		___dataTypeAvt_5 = value;
		Il2CppCodeGenWriteBarrier(&___dataTypeAvt_5, value);
	}

	inline static int32_t get_offset_of_orderAvt_6() { return static_cast<int32_t>(offsetof(Sort_t3076360436, ___orderAvt_6)); }
	inline XslAvt_t1645109359 * get_orderAvt_6() const { return ___orderAvt_6; }
	inline XslAvt_t1645109359 ** get_address_of_orderAvt_6() { return &___orderAvt_6; }
	inline void set_orderAvt_6(XslAvt_t1645109359 * value)
	{
		___orderAvt_6 = value;
		Il2CppCodeGenWriteBarrier(&___orderAvt_6, value);
	}

	inline static int32_t get_offset_of_caseOrderAvt_7() { return static_cast<int32_t>(offsetof(Sort_t3076360436, ___caseOrderAvt_7)); }
	inline XslAvt_t1645109359 * get_caseOrderAvt_7() const { return ___caseOrderAvt_7; }
	inline XslAvt_t1645109359 ** get_address_of_caseOrderAvt_7() { return &___caseOrderAvt_7; }
	inline void set_caseOrderAvt_7(XslAvt_t1645109359 * value)
	{
		___caseOrderAvt_7 = value;
		Il2CppCodeGenWriteBarrier(&___caseOrderAvt_7, value);
	}

	inline static int32_t get_offset_of_expr_8() { return static_cast<int32_t>(offsetof(Sort_t3076360436, ___expr_8)); }
	inline XPathExpression_t1723793351 * get_expr_8() const { return ___expr_8; }
	inline XPathExpression_t1723793351 ** get_address_of_expr_8() { return &___expr_8; }
	inline void set_expr_8(XPathExpression_t1723793351 * value)
	{
		___expr_8 = value;
		Il2CppCodeGenWriteBarrier(&___expr_8, value);
	}
};

struct Sort_t3076360436_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.Sort::<>f__switch$map10
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map10_9;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.Sort::<>f__switch$map11
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map11_10;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.Sort::<>f__switch$map12
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map12_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map10_9() { return static_cast<int32_t>(offsetof(Sort_t3076360436_StaticFields, ___U3CU3Ef__switchU24map10_9)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map10_9() const { return ___U3CU3Ef__switchU24map10_9; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map10_9() { return &___U3CU3Ef__switchU24map10_9; }
	inline void set_U3CU3Ef__switchU24map10_9(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map10_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map10_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map11_10() { return static_cast<int32_t>(offsetof(Sort_t3076360436_StaticFields, ___U3CU3Ef__switchU24map11_10)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map11_10() const { return ___U3CU3Ef__switchU24map11_10; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map11_10() { return &___U3CU3Ef__switchU24map11_10; }
	inline void set_U3CU3Ef__switchU24map11_10(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map11_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map11_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map12_11() { return static_cast<int32_t>(offsetof(Sort_t3076360436_StaticFields, ___U3CU3Ef__switchU24map12_11)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map12_11() const { return ___U3CU3Ef__switchU24map12_11; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map12_11() { return &___U3CU3Ef__switchU24map12_11; }
	inline void set_U3CU3Ef__switchU24map12_11(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map12_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map12_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
