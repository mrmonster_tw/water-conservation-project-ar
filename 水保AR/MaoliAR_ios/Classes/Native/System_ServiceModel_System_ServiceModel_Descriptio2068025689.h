﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.FaultDescription
struct  FaultDescription_t2068025689  : public Il2CppObject
{
public:
	// System.String System.ServiceModel.Description.FaultDescription::name
	String_t* ___name_0;
	// System.String System.ServiceModel.Description.FaultDescription::ns
	String_t* ___ns_1;
	// System.Type System.ServiceModel.Description.FaultDescription::detail_type
	Type_t * ___detail_type_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(FaultDescription_t2068025689, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_ns_1() { return static_cast<int32_t>(offsetof(FaultDescription_t2068025689, ___ns_1)); }
	inline String_t* get_ns_1() const { return ___ns_1; }
	inline String_t** get_address_of_ns_1() { return &___ns_1; }
	inline void set_ns_1(String_t* value)
	{
		___ns_1 = value;
		Il2CppCodeGenWriteBarrier(&___ns_1, value);
	}

	inline static int32_t get_offset_of_detail_type_2() { return static_cast<int32_t>(offsetof(FaultDescription_t2068025689, ___detail_type_2)); }
	inline Type_t * get_detail_type_2() const { return ___detail_type_2; }
	inline Type_t ** get_address_of_detail_type_2() { return &___detail_type_2; }
	inline void set_detail_type_2(Type_t * value)
	{
		___detail_type_2 = value;
		Il2CppCodeGenWriteBarrier(&___detail_type_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
