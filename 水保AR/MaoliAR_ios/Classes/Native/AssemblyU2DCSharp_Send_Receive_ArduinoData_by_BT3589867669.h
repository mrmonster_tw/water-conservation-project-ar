﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// BT_Mic[]
struct BT_MicU5BU5D_t4256766077;
// UnityEngine.UI.Text
struct Text_t1901882714;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Send_Receive_ArduinoData_by_BT
struct  Send_Receive_ArduinoData_by_BT_t3589867669  : public MonoBehaviour_t3962482529
{
public:
	// BT_Mic[] Send_Receive_ArduinoData_by_BT::_data
	BT_MicU5BU5D_t4256766077* ____data_2;
	// System.Boolean Send_Receive_ArduinoData_by_BT::b_debug
	bool ___b_debug_3;
	// UnityEngine.UI.Text Send_Receive_ArduinoData_by_BT::BT_state_text
	Text_t1901882714 * ___BT_state_text_4;
	// System.Char Send_Receive_ArduinoData_by_BT::a
	Il2CppChar ___a_5;
	// System.String Send_Receive_ArduinoData_by_BT::readLine
	String_t* ___readLine_6;

public:
	inline static int32_t get_offset_of__data_2() { return static_cast<int32_t>(offsetof(Send_Receive_ArduinoData_by_BT_t3589867669, ____data_2)); }
	inline BT_MicU5BU5D_t4256766077* get__data_2() const { return ____data_2; }
	inline BT_MicU5BU5D_t4256766077** get_address_of__data_2() { return &____data_2; }
	inline void set__data_2(BT_MicU5BU5D_t4256766077* value)
	{
		____data_2 = value;
		Il2CppCodeGenWriteBarrier(&____data_2, value);
	}

	inline static int32_t get_offset_of_b_debug_3() { return static_cast<int32_t>(offsetof(Send_Receive_ArduinoData_by_BT_t3589867669, ___b_debug_3)); }
	inline bool get_b_debug_3() const { return ___b_debug_3; }
	inline bool* get_address_of_b_debug_3() { return &___b_debug_3; }
	inline void set_b_debug_3(bool value)
	{
		___b_debug_3 = value;
	}

	inline static int32_t get_offset_of_BT_state_text_4() { return static_cast<int32_t>(offsetof(Send_Receive_ArduinoData_by_BT_t3589867669, ___BT_state_text_4)); }
	inline Text_t1901882714 * get_BT_state_text_4() const { return ___BT_state_text_4; }
	inline Text_t1901882714 ** get_address_of_BT_state_text_4() { return &___BT_state_text_4; }
	inline void set_BT_state_text_4(Text_t1901882714 * value)
	{
		___BT_state_text_4 = value;
		Il2CppCodeGenWriteBarrier(&___BT_state_text_4, value);
	}

	inline static int32_t get_offset_of_a_5() { return static_cast<int32_t>(offsetof(Send_Receive_ArduinoData_by_BT_t3589867669, ___a_5)); }
	inline Il2CppChar get_a_5() const { return ___a_5; }
	inline Il2CppChar* get_address_of_a_5() { return &___a_5; }
	inline void set_a_5(Il2CppChar value)
	{
		___a_5 = value;
	}

	inline static int32_t get_offset_of_readLine_6() { return static_cast<int32_t>(offsetof(Send_Receive_ArduinoData_by_BT_t3589867669, ___readLine_6)); }
	inline String_t* get_readLine_6() const { return ___readLine_6; }
	inline String_t** get_address_of_readLine_6() { return &___readLine_6; }
	inline void set_readLine_6(String_t* value)
	{
		___readLine_6 = value;
		Il2CppCodeGenWriteBarrier(&___readLine_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
