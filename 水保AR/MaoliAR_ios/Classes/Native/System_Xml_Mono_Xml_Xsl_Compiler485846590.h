﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Xml.XPath.XPathNavigator
struct XPathNavigator_t787956054;
// System.Collections.Stack
struct Stack_t2329662280;
// Mono.Xml.Xsl.XslStylesheet
struct XslStylesheet_t113441946;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t418790500;
// System.Xml.XmlResolver
struct XmlResolver_t626023767;
// System.Security.Policy.Evidence
struct Evidence_t2008144148;
// System.String
struct String_t;
// Mono.Xml.Xsl.XsltDebuggerWrapper
struct XsltDebuggerWrapper_t4148725768;
// Mono.Xml.Xsl.MSXslScriptManager
struct MSXslScriptManager_t4018040365;
// Mono.Xml.XPath.XPathParser
struct XPathParser_t4136515887;
// Mono.Xml.Xsl.XsltPatternParser
struct XsltPatternParser_t433897766;
// Mono.Xml.Xsl.VariableScope
struct VariableScope_t3522028429;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Compiler
struct  Compiler_t485846590  : public Il2CppObject
{
public:
	// System.Collections.ArrayList Mono.Xml.Xsl.Compiler::inputStack
	ArrayList_t2718874744 * ___inputStack_0;
	// System.Xml.XPath.XPathNavigator Mono.Xml.Xsl.Compiler::currentInput
	XPathNavigator_t787956054 * ___currentInput_1;
	// System.Collections.Stack Mono.Xml.Xsl.Compiler::styleStack
	Stack_t2329662280 * ___styleStack_2;
	// Mono.Xml.Xsl.XslStylesheet Mono.Xml.Xsl.Compiler::currentStyle
	XslStylesheet_t113441946 * ___currentStyle_3;
	// System.Collections.Hashtable Mono.Xml.Xsl.Compiler::keys
	Hashtable_t1853889766 * ___keys_4;
	// System.Collections.Hashtable Mono.Xml.Xsl.Compiler::globalVariables
	Hashtable_t1853889766 * ___globalVariables_5;
	// System.Collections.Hashtable Mono.Xml.Xsl.Compiler::attrSets
	Hashtable_t1853889766 * ___attrSets_6;
	// System.Xml.XmlNamespaceManager Mono.Xml.Xsl.Compiler::nsMgr
	XmlNamespaceManager_t418790500 * ___nsMgr_7;
	// System.Xml.XmlResolver Mono.Xml.Xsl.Compiler::res
	XmlResolver_t626023767 * ___res_8;
	// System.Security.Policy.Evidence Mono.Xml.Xsl.Compiler::evidence
	Evidence_t2008144148 * ___evidence_9;
	// Mono.Xml.Xsl.XslStylesheet Mono.Xml.Xsl.Compiler::rootStyle
	XslStylesheet_t113441946 * ___rootStyle_10;
	// System.Collections.Hashtable Mono.Xml.Xsl.Compiler::outputs
	Hashtable_t1853889766 * ___outputs_11;
	// System.Boolean Mono.Xml.Xsl.Compiler::keyCompilationMode
	bool ___keyCompilationMode_12;
	// System.String Mono.Xml.Xsl.Compiler::stylesheetVersion
	String_t* ___stylesheetVersion_13;
	// Mono.Xml.Xsl.XsltDebuggerWrapper Mono.Xml.Xsl.Compiler::debugger
	XsltDebuggerWrapper_t4148725768 * ___debugger_14;
	// Mono.Xml.Xsl.MSXslScriptManager Mono.Xml.Xsl.Compiler::msScripts
	MSXslScriptManager_t4018040365 * ___msScripts_15;
	// Mono.Xml.XPath.XPathParser Mono.Xml.Xsl.Compiler::xpathParser
	XPathParser_t4136515887 * ___xpathParser_16;
	// Mono.Xml.Xsl.XsltPatternParser Mono.Xml.Xsl.Compiler::patternParser
	XsltPatternParser_t433897766 * ___patternParser_17;
	// Mono.Xml.Xsl.VariableScope Mono.Xml.Xsl.Compiler::curVarScope
	VariableScope_t3522028429 * ___curVarScope_18;
	// System.Collections.Hashtable Mono.Xml.Xsl.Compiler::decimalFormats
	Hashtable_t1853889766 * ___decimalFormats_19;

public:
	inline static int32_t get_offset_of_inputStack_0() { return static_cast<int32_t>(offsetof(Compiler_t485846590, ___inputStack_0)); }
	inline ArrayList_t2718874744 * get_inputStack_0() const { return ___inputStack_0; }
	inline ArrayList_t2718874744 ** get_address_of_inputStack_0() { return &___inputStack_0; }
	inline void set_inputStack_0(ArrayList_t2718874744 * value)
	{
		___inputStack_0 = value;
		Il2CppCodeGenWriteBarrier(&___inputStack_0, value);
	}

	inline static int32_t get_offset_of_currentInput_1() { return static_cast<int32_t>(offsetof(Compiler_t485846590, ___currentInput_1)); }
	inline XPathNavigator_t787956054 * get_currentInput_1() const { return ___currentInput_1; }
	inline XPathNavigator_t787956054 ** get_address_of_currentInput_1() { return &___currentInput_1; }
	inline void set_currentInput_1(XPathNavigator_t787956054 * value)
	{
		___currentInput_1 = value;
		Il2CppCodeGenWriteBarrier(&___currentInput_1, value);
	}

	inline static int32_t get_offset_of_styleStack_2() { return static_cast<int32_t>(offsetof(Compiler_t485846590, ___styleStack_2)); }
	inline Stack_t2329662280 * get_styleStack_2() const { return ___styleStack_2; }
	inline Stack_t2329662280 ** get_address_of_styleStack_2() { return &___styleStack_2; }
	inline void set_styleStack_2(Stack_t2329662280 * value)
	{
		___styleStack_2 = value;
		Il2CppCodeGenWriteBarrier(&___styleStack_2, value);
	}

	inline static int32_t get_offset_of_currentStyle_3() { return static_cast<int32_t>(offsetof(Compiler_t485846590, ___currentStyle_3)); }
	inline XslStylesheet_t113441946 * get_currentStyle_3() const { return ___currentStyle_3; }
	inline XslStylesheet_t113441946 ** get_address_of_currentStyle_3() { return &___currentStyle_3; }
	inline void set_currentStyle_3(XslStylesheet_t113441946 * value)
	{
		___currentStyle_3 = value;
		Il2CppCodeGenWriteBarrier(&___currentStyle_3, value);
	}

	inline static int32_t get_offset_of_keys_4() { return static_cast<int32_t>(offsetof(Compiler_t485846590, ___keys_4)); }
	inline Hashtable_t1853889766 * get_keys_4() const { return ___keys_4; }
	inline Hashtable_t1853889766 ** get_address_of_keys_4() { return &___keys_4; }
	inline void set_keys_4(Hashtable_t1853889766 * value)
	{
		___keys_4 = value;
		Il2CppCodeGenWriteBarrier(&___keys_4, value);
	}

	inline static int32_t get_offset_of_globalVariables_5() { return static_cast<int32_t>(offsetof(Compiler_t485846590, ___globalVariables_5)); }
	inline Hashtable_t1853889766 * get_globalVariables_5() const { return ___globalVariables_5; }
	inline Hashtable_t1853889766 ** get_address_of_globalVariables_5() { return &___globalVariables_5; }
	inline void set_globalVariables_5(Hashtable_t1853889766 * value)
	{
		___globalVariables_5 = value;
		Il2CppCodeGenWriteBarrier(&___globalVariables_5, value);
	}

	inline static int32_t get_offset_of_attrSets_6() { return static_cast<int32_t>(offsetof(Compiler_t485846590, ___attrSets_6)); }
	inline Hashtable_t1853889766 * get_attrSets_6() const { return ___attrSets_6; }
	inline Hashtable_t1853889766 ** get_address_of_attrSets_6() { return &___attrSets_6; }
	inline void set_attrSets_6(Hashtable_t1853889766 * value)
	{
		___attrSets_6 = value;
		Il2CppCodeGenWriteBarrier(&___attrSets_6, value);
	}

	inline static int32_t get_offset_of_nsMgr_7() { return static_cast<int32_t>(offsetof(Compiler_t485846590, ___nsMgr_7)); }
	inline XmlNamespaceManager_t418790500 * get_nsMgr_7() const { return ___nsMgr_7; }
	inline XmlNamespaceManager_t418790500 ** get_address_of_nsMgr_7() { return &___nsMgr_7; }
	inline void set_nsMgr_7(XmlNamespaceManager_t418790500 * value)
	{
		___nsMgr_7 = value;
		Il2CppCodeGenWriteBarrier(&___nsMgr_7, value);
	}

	inline static int32_t get_offset_of_res_8() { return static_cast<int32_t>(offsetof(Compiler_t485846590, ___res_8)); }
	inline XmlResolver_t626023767 * get_res_8() const { return ___res_8; }
	inline XmlResolver_t626023767 ** get_address_of_res_8() { return &___res_8; }
	inline void set_res_8(XmlResolver_t626023767 * value)
	{
		___res_8 = value;
		Il2CppCodeGenWriteBarrier(&___res_8, value);
	}

	inline static int32_t get_offset_of_evidence_9() { return static_cast<int32_t>(offsetof(Compiler_t485846590, ___evidence_9)); }
	inline Evidence_t2008144148 * get_evidence_9() const { return ___evidence_9; }
	inline Evidence_t2008144148 ** get_address_of_evidence_9() { return &___evidence_9; }
	inline void set_evidence_9(Evidence_t2008144148 * value)
	{
		___evidence_9 = value;
		Il2CppCodeGenWriteBarrier(&___evidence_9, value);
	}

	inline static int32_t get_offset_of_rootStyle_10() { return static_cast<int32_t>(offsetof(Compiler_t485846590, ___rootStyle_10)); }
	inline XslStylesheet_t113441946 * get_rootStyle_10() const { return ___rootStyle_10; }
	inline XslStylesheet_t113441946 ** get_address_of_rootStyle_10() { return &___rootStyle_10; }
	inline void set_rootStyle_10(XslStylesheet_t113441946 * value)
	{
		___rootStyle_10 = value;
		Il2CppCodeGenWriteBarrier(&___rootStyle_10, value);
	}

	inline static int32_t get_offset_of_outputs_11() { return static_cast<int32_t>(offsetof(Compiler_t485846590, ___outputs_11)); }
	inline Hashtable_t1853889766 * get_outputs_11() const { return ___outputs_11; }
	inline Hashtable_t1853889766 ** get_address_of_outputs_11() { return &___outputs_11; }
	inline void set_outputs_11(Hashtable_t1853889766 * value)
	{
		___outputs_11 = value;
		Il2CppCodeGenWriteBarrier(&___outputs_11, value);
	}

	inline static int32_t get_offset_of_keyCompilationMode_12() { return static_cast<int32_t>(offsetof(Compiler_t485846590, ___keyCompilationMode_12)); }
	inline bool get_keyCompilationMode_12() const { return ___keyCompilationMode_12; }
	inline bool* get_address_of_keyCompilationMode_12() { return &___keyCompilationMode_12; }
	inline void set_keyCompilationMode_12(bool value)
	{
		___keyCompilationMode_12 = value;
	}

	inline static int32_t get_offset_of_stylesheetVersion_13() { return static_cast<int32_t>(offsetof(Compiler_t485846590, ___stylesheetVersion_13)); }
	inline String_t* get_stylesheetVersion_13() const { return ___stylesheetVersion_13; }
	inline String_t** get_address_of_stylesheetVersion_13() { return &___stylesheetVersion_13; }
	inline void set_stylesheetVersion_13(String_t* value)
	{
		___stylesheetVersion_13 = value;
		Il2CppCodeGenWriteBarrier(&___stylesheetVersion_13, value);
	}

	inline static int32_t get_offset_of_debugger_14() { return static_cast<int32_t>(offsetof(Compiler_t485846590, ___debugger_14)); }
	inline XsltDebuggerWrapper_t4148725768 * get_debugger_14() const { return ___debugger_14; }
	inline XsltDebuggerWrapper_t4148725768 ** get_address_of_debugger_14() { return &___debugger_14; }
	inline void set_debugger_14(XsltDebuggerWrapper_t4148725768 * value)
	{
		___debugger_14 = value;
		Il2CppCodeGenWriteBarrier(&___debugger_14, value);
	}

	inline static int32_t get_offset_of_msScripts_15() { return static_cast<int32_t>(offsetof(Compiler_t485846590, ___msScripts_15)); }
	inline MSXslScriptManager_t4018040365 * get_msScripts_15() const { return ___msScripts_15; }
	inline MSXslScriptManager_t4018040365 ** get_address_of_msScripts_15() { return &___msScripts_15; }
	inline void set_msScripts_15(MSXslScriptManager_t4018040365 * value)
	{
		___msScripts_15 = value;
		Il2CppCodeGenWriteBarrier(&___msScripts_15, value);
	}

	inline static int32_t get_offset_of_xpathParser_16() { return static_cast<int32_t>(offsetof(Compiler_t485846590, ___xpathParser_16)); }
	inline XPathParser_t4136515887 * get_xpathParser_16() const { return ___xpathParser_16; }
	inline XPathParser_t4136515887 ** get_address_of_xpathParser_16() { return &___xpathParser_16; }
	inline void set_xpathParser_16(XPathParser_t4136515887 * value)
	{
		___xpathParser_16 = value;
		Il2CppCodeGenWriteBarrier(&___xpathParser_16, value);
	}

	inline static int32_t get_offset_of_patternParser_17() { return static_cast<int32_t>(offsetof(Compiler_t485846590, ___patternParser_17)); }
	inline XsltPatternParser_t433897766 * get_patternParser_17() const { return ___patternParser_17; }
	inline XsltPatternParser_t433897766 ** get_address_of_patternParser_17() { return &___patternParser_17; }
	inline void set_patternParser_17(XsltPatternParser_t433897766 * value)
	{
		___patternParser_17 = value;
		Il2CppCodeGenWriteBarrier(&___patternParser_17, value);
	}

	inline static int32_t get_offset_of_curVarScope_18() { return static_cast<int32_t>(offsetof(Compiler_t485846590, ___curVarScope_18)); }
	inline VariableScope_t3522028429 * get_curVarScope_18() const { return ___curVarScope_18; }
	inline VariableScope_t3522028429 ** get_address_of_curVarScope_18() { return &___curVarScope_18; }
	inline void set_curVarScope_18(VariableScope_t3522028429 * value)
	{
		___curVarScope_18 = value;
		Il2CppCodeGenWriteBarrier(&___curVarScope_18, value);
	}

	inline static int32_t get_offset_of_decimalFormats_19() { return static_cast<int32_t>(offsetof(Compiler_t485846590, ___decimalFormats_19)); }
	inline Hashtable_t1853889766 * get_decimalFormats_19() const { return ___decimalFormats_19; }
	inline Hashtable_t1853889766 ** get_address_of_decimalFormats_19() { return &___decimalFormats_19; }
	inline void set_decimalFormats_19(Hashtable_t1853889766 * value)
	{
		___decimalFormats_19 = value;
		Il2CppCodeGenWriteBarrier(&___decimalFormats_19, value);
	}
};

struct Compiler_t485846590_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.Compiler::<>f__switch$mapE
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24mapE_20;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.Compiler::<>f__switch$mapF
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24mapF_21;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapE_20() { return static_cast<int32_t>(offsetof(Compiler_t485846590_StaticFields, ___U3CU3Ef__switchU24mapE_20)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24mapE_20() const { return ___U3CU3Ef__switchU24mapE_20; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24mapE_20() { return &___U3CU3Ef__switchU24mapE_20; }
	inline void set_U3CU3Ef__switchU24mapE_20(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24mapE_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapE_20, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapF_21() { return static_cast<int32_t>(offsetof(Compiler_t485846590_StaticFields, ___U3CU3Ef__switchU24mapF_21)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24mapF_21() const { return ___U3CU3Ef__switchU24mapF_21; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24mapF_21() { return &___U3CU3Ef__switchU24mapF_21; }
	inline void set_U3CU3Ef__switchU24mapF_21(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24mapF_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapF_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
