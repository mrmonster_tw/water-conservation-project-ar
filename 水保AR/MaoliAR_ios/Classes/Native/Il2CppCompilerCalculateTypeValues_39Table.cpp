﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit240592346.h"
#include "UnityEngine_UnityEngine_Collision4262080450.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction962663221.h"
#include "UnityEngine_UnityEngine_Physics2310948930.h"
#include "UnityEngine_UnityEngine_ContactPoint3758755253.h"
#include "UnityEngine_UnityEngine_Rigidbody3916780224.h"
#include "UnityEngine_UnityEngine_Collider1773347010.h"
#include "UnityEngine_UnityEngine_BoxCollider1640800422.h"
#include "UnityEngine_UnityEngine_MeshCollider903564387.h"
#include "UnityEngine_UnityEngine_RaycastHit1056001966.h"
#include "UnityEngine_UnityEngine_CharacterController1138636865.h"
#include "UnityEngine_UnityEngine_RaycastHit2D2279581989.h"
#include "UnityEngine_UnityEngine_Physics2D1528932956.h"
#include "UnityEngine_UnityEngine_ContactFilter2D3805203441.h"
#include "UnityEngine_UnityEngine_Rigidbody2D939494601.h"
#include "UnityEngine_UnityEngine_Collider2D2806799626.h"
#include "UnityEngine_UnityEngine_BoxCollider2D3581341831.h"
#include "UnityEngine_UnityEngine_ContactPoint2D3390240644.h"
#include "UnityEngine_UnityEngine_Collision2D2842956331.h"
#include "UnityEngine_UnityEngine_AudioSettings3587374600.h"
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigu2089929874.h"
#include "UnityEngine_UnityEngine_AudioClip3680889665.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallbac1677636661.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCa1059417452.h"
#include "UnityEngine_UnityEngine_AudioListener2734094699.h"
#include "UnityEngine_UnityEngine_AudioSource3935305588.h"
#include "UnityEngine_UnityEngine_WebCamDevice1322781432.h"
#include "UnityEngine_UnityEngine_WebCamTexture1514609158.h"
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttr2857104338.h"
#include "UnityEngine_UnityEngine_StateMachineBehaviour957311111.h"
#include "UnityEngine_UnityEngine_AnimationEventSource3045280095.h"
#include "UnityEngine_UnityEngine_AnimationEvent1536042487.h"
#include "UnityEngine_UnityEngine_AnimationClip2318505987.h"
#include "UnityEngine_UnityEngine_PlayMode3051407859.h"
#include "UnityEngine_UnityEngine_Animation3648466861.h"
#include "UnityEngine_UnityEngine_Animation_Enumerator1136361084.h"
#include "UnityEngine_UnityEngine_AnimationState1108360407.h"
#include "UnityEngine_UnityEngine_AnimatorControllerParamete3317225440.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo3156717155.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo509032636.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo2534804151.h"
#include "UnityEngine_UnityEngine_Animator434523843.h"
#include "UnityEngine_UnityEngine_AnimatorControllerParamete1758260042.h"
#include "UnityEngine_UnityEngine_SkeletonBone4134054672.h"
#include "UnityEngine_UnityEngine_HumanLimit2901552972.h"
#include "UnityEngine_UnityEngine_HumanBone2465339518.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController2933699135.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim1397024487.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim2837525843.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim1193230317.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim2591841208.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim2436992687.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim3136944237.h"
#include "UnityEngine_UnityEngine_Motion1110556653.h"
#include "UnityEngine_UnityEngine_FontStyle82229486.h"
#include "UnityEngine_UnityEngine_TextGenerationError3604799999.h"
#include "UnityEngine_UnityEngine_TextGenerationSettings1351628751.h"
#include "UnityEngine_UnityEngine_TextGenerator3211863866.h"
#include "UnityEngine_UnityEngine_TextAnchor2035777396.h"
#include "UnityEngine_UnityEngine_HorizontalWrapMode2172737147.h"
#include "UnityEngine_UnityEngine_VerticalWrapMode2936607737.h"
#include "UnityEngine_UnityEngine_GUIText402233326.h"
#include "UnityEngine_UnityEngine_CharacterInfo1228754872.h"
#include "UnityEngine_UnityEngine_Font1956802104.h"
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCal2467502454.h"
#include "UnityEngine_UnityEngine_UICharInfo75501106.h"
#include "UnityEngine_UnityEngine_UILineInfo4195266810.h"
#include "UnityEngine_UnityEngine_UIVertex4057497605.h"
#include "UnityEngine_UnityEngine_RectTransformUtility1743242446.h"
#include "UnityEngine_UnityEngine_RenderMode4077056833.h"
#include "UnityEngine_UnityEngine_Canvas3310196443.h"
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvases3309123499.h"
#include "UnityEngine_UnityEngine_CanvasGroup4083511760.h"
#include "UnityEngine_UnityEngine_CanvasRenderer2598313366.h"
#include "UnityEngine_UnityEngine_Event2956885303.h"
#include "UnityEngine_UnityEngine_EventType3528516131.h"
#include "UnityEngine_UnityEngine_EventModifiers2016417398.h"
#include "UnityEngine_UnityEngine_GUI1624858472.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction3146511083.h"
#include "UnityEngine_UnityEngine_GUIContent3050628031.h"
#include "UnityEngine_UnityEngine_FocusType718022695.h"
#include "UnityEngine_UnityEngine_GUILayout3503650450.h"
#include "UnityEngine_UnityEngine_GUILayoutOption811797299.h"
#include "UnityEngine_UnityEngine_GUILayoutOption_Type3858932131.h"
#include "UnityEngine_UnityEngine_GUILayoutGroup2157789695.h"
#include "UnityEngine_UnityEngine_GUIScrollGroup1523329021.h"
#include "UnityEngine_UnityEngine_GUILayoutEntry3214611570.h"
#include "UnityEngine_UnityEngine_GUIWordWrapSizer2043268473.h"
#include "UnityEngine_UnityEngine_GUILayoutUtility66395690.h"
#include "UnityEngine_UnityEngine_GUILayoutUtility_LayoutCache78309876.h"
#include "UnityEngine_UnityEngine_GUISettings1774757634.h"
#include "UnityEngine_UnityEngine_GUISkin1244372282.h"
#include "UnityEngine_UnityEngine_GUISkin_SkinChangedDelegat1143955295.h"
#include "UnityEngine_UnityEngine_GUIStyleState1397964415.h"
#include "UnityEngine_UnityEngine_ImagePosition641749504.h"
#include "UnityEngine_UnityEngine_GUIStyle3956901511.h"
#include "UnityEngine_UnityEngine_TextClipping865312958.h"
#include "UnityEngine_UnityEngine_GUITargetAttribute25796337.h"
#include "UnityEngine_UnityEngine_ExitGUIException133215258.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3900 = { sizeof (ControllerColliderHit_t240592346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3900[7] = 
{
	ControllerColliderHit_t240592346::get_offset_of_m_Controller_0(),
	ControllerColliderHit_t240592346::get_offset_of_m_Collider_1(),
	ControllerColliderHit_t240592346::get_offset_of_m_Point_2(),
	ControllerColliderHit_t240592346::get_offset_of_m_Normal_3(),
	ControllerColliderHit_t240592346::get_offset_of_m_MoveDirection_4(),
	ControllerColliderHit_t240592346::get_offset_of_m_MoveLength_5(),
	ControllerColliderHit_t240592346::get_offset_of_m_Push_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3901 = { sizeof (Collision_t4262080450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3901[5] = 
{
	Collision_t4262080450::get_offset_of_m_Impulse_0(),
	Collision_t4262080450::get_offset_of_m_RelativeVelocity_1(),
	Collision_t4262080450::get_offset_of_m_Rigidbody_2(),
	Collision_t4262080450::get_offset_of_m_Collider_3(),
	Collision_t4262080450::get_offset_of_m_Contacts_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3902 = { sizeof (QueryTriggerInteraction_t962663221)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3902[4] = 
{
	QueryTriggerInteraction_t962663221::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3903 = { sizeof (Physics_t2310948930), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3904 = { sizeof (ContactPoint_t3758755253)+ sizeof (Il2CppObject), sizeof(ContactPoint_t3758755253 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3904[5] = 
{
	ContactPoint_t3758755253::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t3758755253::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t3758755253::get_offset_of_m_ThisColliderInstanceID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t3758755253::get_offset_of_m_OtherColliderInstanceID_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t3758755253::get_offset_of_m_Separation_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3905 = { sizeof (Rigidbody_t3916780224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3906 = { sizeof (Collider_t1773347010), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3907 = { sizeof (BoxCollider_t1640800422), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3908 = { sizeof (MeshCollider_t903564387), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3909 = { sizeof (RaycastHit_t1056001966)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3909[6] = 
{
	RaycastHit_t1056001966::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t1056001966::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t1056001966::get_offset_of_m_FaceID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t1056001966::get_offset_of_m_Distance_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t1056001966::get_offset_of_m_UV_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t1056001966::get_offset_of_m_Collider_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3910 = { sizeof (CharacterController_t1138636865), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3911 = { sizeof (RaycastHit2D_t2279581989)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3911[6] = 
{
	RaycastHit2D_t2279581989::get_offset_of_m_Centroid_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t2279581989::get_offset_of_m_Point_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t2279581989::get_offset_of_m_Normal_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t2279581989::get_offset_of_m_Distance_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t2279581989::get_offset_of_m_Fraction_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t2279581989::get_offset_of_m_Collider_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3912 = { sizeof (Physics2D_t1528932956), -1, sizeof(Physics2D_t1528932956_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3912[1] = 
{
	Physics2D_t1528932956_StaticFields::get_offset_of_m_LastDisabledRigidbody2D_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3913 = { sizeof (ContactFilter2D_t3805203441)+ sizeof (Il2CppObject), sizeof(ContactFilter2D_t3805203441_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3913[11] = 
{
	ContactFilter2D_t3805203441::get_offset_of_useTriggers_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_useLayerMask_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_useDepth_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_useOutsideDepth_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_useNormalAngle_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_useOutsideNormalAngle_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_layerMask_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_minDepth_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_maxDepth_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_minNormalAngle_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_maxNormalAngle_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3914 = { sizeof (Rigidbody2D_t939494601), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3915 = { sizeof (Collider2D_t2806799626), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3916 = { sizeof (BoxCollider2D_t3581341831), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3917 = { sizeof (ContactPoint2D_t3390240644)+ sizeof (Il2CppObject), sizeof(ContactPoint2D_t3390240644 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3917[11] = 
{
	ContactPoint2D_t3390240644::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_RelativeVelocity_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_Separation_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_NormalImpulse_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_TangentImpulse_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_Collider_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_OtherCollider_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_Rigidbody_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_OtherRigidbody_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_Enabled_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3918 = { sizeof (Collision2D_t2842956331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3918[7] = 
{
	Collision2D_t2842956331::get_offset_of_m_Collider_0(),
	Collision2D_t2842956331::get_offset_of_m_OtherCollider_1(),
	Collision2D_t2842956331::get_offset_of_m_Rigidbody_2(),
	Collision2D_t2842956331::get_offset_of_m_OtherRigidbody_3(),
	Collision2D_t2842956331::get_offset_of_m_Contacts_4(),
	Collision2D_t2842956331::get_offset_of_m_RelativeVelocity_5(),
	Collision2D_t2842956331::get_offset_of_m_Enabled_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3919 = { sizeof (AudioSettings_t3587374600), -1, sizeof(AudioSettings_t3587374600_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3919[1] = 
{
	AudioSettings_t3587374600_StaticFields::get_offset_of_OnAudioConfigurationChanged_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3920 = { sizeof (AudioConfigurationChangeHandler_t2089929874), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3921 = { sizeof (AudioClip_t3680889665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3921[2] = 
{
	AudioClip_t3680889665::get_offset_of_m_PCMReaderCallback_2(),
	AudioClip_t3680889665::get_offset_of_m_PCMSetPositionCallback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3922 = { sizeof (PCMReaderCallback_t1677636661), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3923 = { sizeof (PCMSetPositionCallback_t1059417452), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3924 = { sizeof (AudioListener_t2734094699), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3925 = { sizeof (AudioSource_t3935305588), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3926 = { sizeof (WebCamDevice_t1322781432)+ sizeof (Il2CppObject), sizeof(WebCamDevice_t1322781432_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3926[2] = 
{
	WebCamDevice_t1322781432::get_offset_of_m_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WebCamDevice_t1322781432::get_offset_of_m_Flags_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3927 = { sizeof (WebCamTexture_t1514609158), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3928 = { sizeof (SharedBetweenAnimatorsAttribute_t2857104338), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3929 = { sizeof (StateMachineBehaviour_t957311111), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3930 = { sizeof (AnimationEventSource_t3045280095)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3930[4] = 
{
	AnimationEventSource_t3045280095::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3931 = { sizeof (AnimationEvent_t1536042487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3931[11] = 
{
	AnimationEvent_t1536042487::get_offset_of_m_Time_0(),
	AnimationEvent_t1536042487::get_offset_of_m_FunctionName_1(),
	AnimationEvent_t1536042487::get_offset_of_m_StringParameter_2(),
	AnimationEvent_t1536042487::get_offset_of_m_ObjectReferenceParameter_3(),
	AnimationEvent_t1536042487::get_offset_of_m_FloatParameter_4(),
	AnimationEvent_t1536042487::get_offset_of_m_IntParameter_5(),
	AnimationEvent_t1536042487::get_offset_of_m_MessageOptions_6(),
	AnimationEvent_t1536042487::get_offset_of_m_Source_7(),
	AnimationEvent_t1536042487::get_offset_of_m_StateSender_8(),
	AnimationEvent_t1536042487::get_offset_of_m_AnimatorStateInfo_9(),
	AnimationEvent_t1536042487::get_offset_of_m_AnimatorClipInfo_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3932 = { sizeof (AnimationClip_t2318505987), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3933 = { sizeof (PlayMode_t3051407859)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3933[3] = 
{
	PlayMode_t3051407859::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3934 = { sizeof (Animation_t3648466861), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3935 = { sizeof (Enumerator_t1136361084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3935[2] = 
{
	Enumerator_t1136361084::get_offset_of_m_Outer_0(),
	Enumerator_t1136361084::get_offset_of_m_CurrentIndex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3936 = { sizeof (AnimationState_t1108360407), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3937 = { sizeof (AnimatorControllerParameterType_t3317225440)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3937[5] = 
{
	AnimatorControllerParameterType_t3317225440::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3938 = { sizeof (AnimatorClipInfo_t3156717155)+ sizeof (Il2CppObject), sizeof(AnimatorClipInfo_t3156717155 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3938[2] = 
{
	AnimatorClipInfo_t3156717155::get_offset_of_m_ClipInstanceID_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorClipInfo_t3156717155::get_offset_of_m_Weight_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3939 = { sizeof (AnimatorStateInfo_t509032636)+ sizeof (Il2CppObject), sizeof(AnimatorStateInfo_t509032636 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3939[9] = 
{
	AnimatorStateInfo_t509032636::get_offset_of_m_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t509032636::get_offset_of_m_Path_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t509032636::get_offset_of_m_FullPath_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t509032636::get_offset_of_m_NormalizedTime_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t509032636::get_offset_of_m_Length_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t509032636::get_offset_of_m_Speed_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t509032636::get_offset_of_m_SpeedMultiplier_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t509032636::get_offset_of_m_Tag_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t509032636::get_offset_of_m_Loop_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3940 = { sizeof (AnimatorTransitionInfo_t2534804151)+ sizeof (Il2CppObject), sizeof(AnimatorTransitionInfo_t2534804151_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3940[6] = 
{
	AnimatorTransitionInfo_t2534804151::get_offset_of_m_FullPath_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2534804151::get_offset_of_m_UserName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2534804151::get_offset_of_m_Name_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2534804151::get_offset_of_m_NormalizedTime_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2534804151::get_offset_of_m_AnyState_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2534804151::get_offset_of_m_TransitionType_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3941 = { sizeof (Animator_t434523843), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3942 = { sizeof (AnimatorControllerParameter_t1758260042), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3942[5] = 
{
	AnimatorControllerParameter_t1758260042::get_offset_of_m_Name_0(),
	AnimatorControllerParameter_t1758260042::get_offset_of_m_Type_1(),
	AnimatorControllerParameter_t1758260042::get_offset_of_m_DefaultFloat_2(),
	AnimatorControllerParameter_t1758260042::get_offset_of_m_DefaultInt_3(),
	AnimatorControllerParameter_t1758260042::get_offset_of_m_DefaultBool_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3943 = { sizeof (SkeletonBone_t4134054672)+ sizeof (Il2CppObject), sizeof(SkeletonBone_t4134054672_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3943[5] = 
{
	SkeletonBone_t4134054672::get_offset_of_name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t4134054672::get_offset_of_parentName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t4134054672::get_offset_of_position_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t4134054672::get_offset_of_rotation_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t4134054672::get_offset_of_scale_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3944 = { sizeof (HumanLimit_t2901552972)+ sizeof (Il2CppObject), sizeof(HumanLimit_t2901552972 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3944[5] = 
{
	HumanLimit_t2901552972::get_offset_of_m_Min_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t2901552972::get_offset_of_m_Max_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t2901552972::get_offset_of_m_Center_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t2901552972::get_offset_of_m_AxisLength_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t2901552972::get_offset_of_m_UseDefaultValues_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3945 = { sizeof (HumanBone_t2465339518)+ sizeof (Il2CppObject), sizeof(HumanBone_t2465339518_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3945[3] = 
{
	HumanBone_t2465339518::get_offset_of_m_BoneName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanBone_t2465339518::get_offset_of_m_HumanName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanBone_t2465339518::get_offset_of_limit_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3946 = { sizeof (RuntimeAnimatorController_t2933699135), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3947 = { sizeof (AnimatorControllerPlayable_t1397024487), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3948 = { sizeof (AnimationMixerPlayable_t2837525843), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3949 = { sizeof (AnimationLayerMixerPlayable_t1193230317), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3950 = { sizeof (AnimationClipPlayable_t2591841208), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3951 = { sizeof (AnimationPlayable_t2436992687), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3952 = { sizeof (AnimationOffsetPlayable_t3136944237), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3953 = { sizeof (Motion_t1110556653), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3954 = { sizeof (FontStyle_t82229486)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3954[5] = 
{
	FontStyle_t82229486::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3955 = { sizeof (TextGenerationError_t3604799999)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3955[5] = 
{
	TextGenerationError_t3604799999::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3956 = { sizeof (TextGenerationSettings_t1351628751)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3956[18] = 
{
	TextGenerationSettings_t1351628751::get_offset_of_font_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_color_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_fontSize_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_lineSpacing_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_richText_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_scaleFactor_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_fontStyle_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_textAnchor_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_alignByGeometry_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_resizeTextForBestFit_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_resizeTextMinSize_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_resizeTextMaxSize_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_updateBounds_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_verticalOverflow_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_horizontalOverflow_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_generationExtents_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_pivot_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_generateOutOfBounds_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3957 = { sizeof (TextGenerator_t3211863866), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3957[11] = 
{
	TextGenerator_t3211863866::get_offset_of_m_Ptr_0(),
	TextGenerator_t3211863866::get_offset_of_m_LastString_1(),
	TextGenerator_t3211863866::get_offset_of_m_LastSettings_2(),
	TextGenerator_t3211863866::get_offset_of_m_HasGenerated_3(),
	TextGenerator_t3211863866::get_offset_of_m_LastValid_4(),
	TextGenerator_t3211863866::get_offset_of_m_Verts_5(),
	TextGenerator_t3211863866::get_offset_of_m_Characters_6(),
	TextGenerator_t3211863866::get_offset_of_m_Lines_7(),
	TextGenerator_t3211863866::get_offset_of_m_CachedVerts_8(),
	TextGenerator_t3211863866::get_offset_of_m_CachedCharacters_9(),
	TextGenerator_t3211863866::get_offset_of_m_CachedLines_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3958 = { sizeof (TextAnchor_t2035777396)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3958[10] = 
{
	TextAnchor_t2035777396::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3959 = { sizeof (HorizontalWrapMode_t2172737147)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3959[3] = 
{
	HorizontalWrapMode_t2172737147::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3960 = { sizeof (VerticalWrapMode_t2936607737)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3960[3] = 
{
	VerticalWrapMode_t2936607737::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3961 = { sizeof (GUIText_t402233326), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3962 = { sizeof (CharacterInfo_t1228754872)+ sizeof (Il2CppObject), sizeof(CharacterInfo_t1228754872_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3962[7] = 
{
	CharacterInfo_t1228754872::get_offset_of_index_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CharacterInfo_t1228754872::get_offset_of_uv_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CharacterInfo_t1228754872::get_offset_of_vert_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CharacterInfo_t1228754872::get_offset_of_width_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CharacterInfo_t1228754872::get_offset_of_size_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CharacterInfo_t1228754872::get_offset_of_style_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CharacterInfo_t1228754872::get_offset_of_flipped_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3963 = { sizeof (Font_t1956802104), -1, sizeof(Font_t1956802104_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3963[2] = 
{
	Font_t1956802104_StaticFields::get_offset_of_textureRebuilt_2(),
	Font_t1956802104::get_offset_of_m_FontTextureRebuildCallback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3964 = { sizeof (FontTextureRebuildCallback_t2467502454), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3965 = { sizeof (UICharInfo_t75501106)+ sizeof (Il2CppObject), sizeof(UICharInfo_t75501106 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3965[2] = 
{
	UICharInfo_t75501106::get_offset_of_cursorPos_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UICharInfo_t75501106::get_offset_of_charWidth_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3966 = { sizeof (UILineInfo_t4195266810)+ sizeof (Il2CppObject), sizeof(UILineInfo_t4195266810 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3966[4] = 
{
	UILineInfo_t4195266810::get_offset_of_startCharIdx_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UILineInfo_t4195266810::get_offset_of_height_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UILineInfo_t4195266810::get_offset_of_topY_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UILineInfo_t4195266810::get_offset_of_leading_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3967 = { sizeof (UIVertex_t4057497605)+ sizeof (Il2CppObject), sizeof(UIVertex_t4057497605 ), sizeof(UIVertex_t4057497605_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3967[11] = 
{
	UIVertex_t4057497605::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4057497605::get_offset_of_normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4057497605::get_offset_of_color_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4057497605::get_offset_of_uv0_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4057497605::get_offset_of_uv1_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4057497605::get_offset_of_uv2_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4057497605::get_offset_of_uv3_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4057497605::get_offset_of_tangent_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4057497605_StaticFields::get_offset_of_s_DefaultColor_8(),
	UIVertex_t4057497605_StaticFields::get_offset_of_s_DefaultTangent_9(),
	UIVertex_t4057497605_StaticFields::get_offset_of_simpleVert_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3968 = { sizeof (RectTransformUtility_t1743242446), -1, sizeof(RectTransformUtility_t1743242446_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3968[1] = 
{
	RectTransformUtility_t1743242446_StaticFields::get_offset_of_s_Corners_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3969 = { sizeof (RenderMode_t4077056833)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3969[4] = 
{
	RenderMode_t4077056833::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3970 = { sizeof (Canvas_t3310196443), -1, sizeof(Canvas_t3310196443_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3970[1] = 
{
	Canvas_t3310196443_StaticFields::get_offset_of_willRenderCanvases_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3971 = { sizeof (WillRenderCanvases_t3309123499), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3972 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3973 = { sizeof (CanvasGroup_t4083511760), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3974 = { sizeof (CanvasRenderer_t2598313366), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3975 = { sizeof (Event_t2956885303), sizeof(Event_t2956885303_marshaled_pinvoke), sizeof(Event_t2956885303_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3975[4] = 
{
	Event_t2956885303::get_offset_of_m_Ptr_0(),
	Event_t2956885303_StaticFields::get_offset_of_s_Current_1(),
	Event_t2956885303_StaticFields::get_offset_of_s_MasterEvent_2(),
	Event_t2956885303_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3976 = { sizeof (EventType_t3528516131)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3976[33] = 
{
	EventType_t3528516131::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3977 = { sizeof (EventModifiers_t2016417398)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3977[9] = 
{
	EventModifiers_t2016417398::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3978 = { sizeof (GUI_t1624858472), -1, sizeof(GUI_t1624858472_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3978[14] = 
{
	GUI_t1624858472_StaticFields::get_offset_of_s_ScrollStepSize_0(),
	GUI_t1624858472_StaticFields::get_offset_of_s_ScrollControlId_1(),
	GUI_t1624858472_StaticFields::get_offset_of_s_HotTextField_2(),
	GUI_t1624858472_StaticFields::get_offset_of_s_BoxHash_3(),
	GUI_t1624858472_StaticFields::get_offset_of_s_RepeatButtonHash_4(),
	GUI_t1624858472_StaticFields::get_offset_of_s_ToggleHash_5(),
	GUI_t1624858472_StaticFields::get_offset_of_s_ButtonGridHash_6(),
	GUI_t1624858472_StaticFields::get_offset_of_s_SliderHash_7(),
	GUI_t1624858472_StaticFields::get_offset_of_s_BeginGroupHash_8(),
	GUI_t1624858472_StaticFields::get_offset_of_s_ScrollviewHash_9(),
	GUI_t1624858472_StaticFields::get_offset_of_U3CscrollTroughSideU3Ek__BackingField_10(),
	GUI_t1624858472_StaticFields::get_offset_of_U3CnextScrollStepTimeU3Ek__BackingField_11(),
	GUI_t1624858472_StaticFields::get_offset_of_s_Skin_12(),
	GUI_t1624858472_StaticFields::get_offset_of_s_ScrollViewStates_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3979 = { sizeof (WindowFunction_t3146511083), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3980 = { sizeof (GUIContent_t3050628031), -1, sizeof(GUIContent_t3050628031_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3980[7] = 
{
	GUIContent_t3050628031::get_offset_of_m_Text_0(),
	GUIContent_t3050628031::get_offset_of_m_Image_1(),
	GUIContent_t3050628031::get_offset_of_m_Tooltip_2(),
	GUIContent_t3050628031_StaticFields::get_offset_of_s_Text_3(),
	GUIContent_t3050628031_StaticFields::get_offset_of_s_Image_4(),
	GUIContent_t3050628031_StaticFields::get_offset_of_s_TextImage_5(),
	GUIContent_t3050628031_StaticFields::get_offset_of_none_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3981 = { sizeof (FocusType_t718022695)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3981[4] = 
{
	FocusType_t718022695::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3982 = { sizeof (GUILayout_t3503650450), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3983 = { sizeof (GUILayoutOption_t811797299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3983[2] = 
{
	GUILayoutOption_t811797299::get_offset_of_type_0(),
	GUILayoutOption_t811797299::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3984 = { sizeof (Type_t3858932131)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3984[15] = 
{
	Type_t3858932131::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3985 = { sizeof (GUILayoutGroup_t2157789695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3985[17] = 
{
	GUILayoutGroup_t2157789695::get_offset_of_entries_10(),
	GUILayoutGroup_t2157789695::get_offset_of_isVertical_11(),
	GUILayoutGroup_t2157789695::get_offset_of_resetCoords_12(),
	GUILayoutGroup_t2157789695::get_offset_of_spacing_13(),
	GUILayoutGroup_t2157789695::get_offset_of_sameSize_14(),
	GUILayoutGroup_t2157789695::get_offset_of_isWindow_15(),
	GUILayoutGroup_t2157789695::get_offset_of_windowID_16(),
	GUILayoutGroup_t2157789695::get_offset_of_m_Cursor_17(),
	GUILayoutGroup_t2157789695::get_offset_of_m_StretchableCountX_18(),
	GUILayoutGroup_t2157789695::get_offset_of_m_StretchableCountY_19(),
	GUILayoutGroup_t2157789695::get_offset_of_m_UserSpecifiedWidth_20(),
	GUILayoutGroup_t2157789695::get_offset_of_m_UserSpecifiedHeight_21(),
	GUILayoutGroup_t2157789695::get_offset_of_m_ChildMinWidth_22(),
	GUILayoutGroup_t2157789695::get_offset_of_m_ChildMaxWidth_23(),
	GUILayoutGroup_t2157789695::get_offset_of_m_ChildMinHeight_24(),
	GUILayoutGroup_t2157789695::get_offset_of_m_ChildMaxHeight_25(),
	GUILayoutGroup_t2157789695::get_offset_of_m_Margin_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3986 = { sizeof (GUIScrollGroup_t1523329021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3986[12] = 
{
	GUIScrollGroup_t1523329021::get_offset_of_calcMinWidth_27(),
	GUIScrollGroup_t1523329021::get_offset_of_calcMaxWidth_28(),
	GUIScrollGroup_t1523329021::get_offset_of_calcMinHeight_29(),
	GUIScrollGroup_t1523329021::get_offset_of_calcMaxHeight_30(),
	GUIScrollGroup_t1523329021::get_offset_of_clientWidth_31(),
	GUIScrollGroup_t1523329021::get_offset_of_clientHeight_32(),
	GUIScrollGroup_t1523329021::get_offset_of_allowHorizontalScroll_33(),
	GUIScrollGroup_t1523329021::get_offset_of_allowVerticalScroll_34(),
	GUIScrollGroup_t1523329021::get_offset_of_needsHorizontalScrollbar_35(),
	GUIScrollGroup_t1523329021::get_offset_of_needsVerticalScrollbar_36(),
	GUIScrollGroup_t1523329021::get_offset_of_horizontalScrollbar_37(),
	GUIScrollGroup_t1523329021::get_offset_of_verticalScrollbar_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3987 = { sizeof (GUILayoutEntry_t3214611570), -1, sizeof(GUILayoutEntry_t3214611570_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3987[10] = 
{
	GUILayoutEntry_t3214611570::get_offset_of_minWidth_0(),
	GUILayoutEntry_t3214611570::get_offset_of_maxWidth_1(),
	GUILayoutEntry_t3214611570::get_offset_of_minHeight_2(),
	GUILayoutEntry_t3214611570::get_offset_of_maxHeight_3(),
	GUILayoutEntry_t3214611570::get_offset_of_rect_4(),
	GUILayoutEntry_t3214611570::get_offset_of_stretchWidth_5(),
	GUILayoutEntry_t3214611570::get_offset_of_stretchHeight_6(),
	GUILayoutEntry_t3214611570::get_offset_of_m_Style_7(),
	GUILayoutEntry_t3214611570_StaticFields::get_offset_of_kDummyRect_8(),
	GUILayoutEntry_t3214611570_StaticFields::get_offset_of_indent_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3988 = { sizeof (GUIWordWrapSizer_t2043268473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3988[3] = 
{
	GUIWordWrapSizer_t2043268473::get_offset_of_m_Content_10(),
	GUIWordWrapSizer_t2043268473::get_offset_of_m_ForcedMinHeight_11(),
	GUIWordWrapSizer_t2043268473::get_offset_of_m_ForcedMaxHeight_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3989 = { sizeof (GUILayoutUtility_t66395690), -1, sizeof(GUILayoutUtility_t66395690_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3989[5] = 
{
	GUILayoutUtility_t66395690_StaticFields::get_offset_of_s_StoredLayouts_0(),
	GUILayoutUtility_t66395690_StaticFields::get_offset_of_s_StoredWindows_1(),
	GUILayoutUtility_t66395690_StaticFields::get_offset_of_current_2(),
	GUILayoutUtility_t66395690_StaticFields::get_offset_of_kDummyRect_3(),
	GUILayoutUtility_t66395690_StaticFields::get_offset_of_s_SpaceStyle_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3990 = { sizeof (LayoutCache_t78309876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3990[3] = 
{
	LayoutCache_t78309876::get_offset_of_topLevel_0(),
	LayoutCache_t78309876::get_offset_of_layoutGroups_1(),
	LayoutCache_t78309876::get_offset_of_windows_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3991 = { sizeof (GUISettings_t1774757634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3991[5] = 
{
	GUISettings_t1774757634::get_offset_of_m_DoubleClickSelectsWord_0(),
	GUISettings_t1774757634::get_offset_of_m_TripleClickSelectsLine_1(),
	GUISettings_t1774757634::get_offset_of_m_CursorColor_2(),
	GUISettings_t1774757634::get_offset_of_m_CursorFlashSpeed_3(),
	GUISettings_t1774757634::get_offset_of_m_SelectionColor_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3992 = { sizeof (GUISkin_t1244372282), -1, sizeof(GUISkin_t1244372282_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3992[27] = 
{
	GUISkin_t1244372282::get_offset_of_m_Font_2(),
	GUISkin_t1244372282::get_offset_of_m_box_3(),
	GUISkin_t1244372282::get_offset_of_m_button_4(),
	GUISkin_t1244372282::get_offset_of_m_toggle_5(),
	GUISkin_t1244372282::get_offset_of_m_label_6(),
	GUISkin_t1244372282::get_offset_of_m_textField_7(),
	GUISkin_t1244372282::get_offset_of_m_textArea_8(),
	GUISkin_t1244372282::get_offset_of_m_window_9(),
	GUISkin_t1244372282::get_offset_of_m_horizontalSlider_10(),
	GUISkin_t1244372282::get_offset_of_m_horizontalSliderThumb_11(),
	GUISkin_t1244372282::get_offset_of_m_verticalSlider_12(),
	GUISkin_t1244372282::get_offset_of_m_verticalSliderThumb_13(),
	GUISkin_t1244372282::get_offset_of_m_horizontalScrollbar_14(),
	GUISkin_t1244372282::get_offset_of_m_horizontalScrollbarThumb_15(),
	GUISkin_t1244372282::get_offset_of_m_horizontalScrollbarLeftButton_16(),
	GUISkin_t1244372282::get_offset_of_m_horizontalScrollbarRightButton_17(),
	GUISkin_t1244372282::get_offset_of_m_verticalScrollbar_18(),
	GUISkin_t1244372282::get_offset_of_m_verticalScrollbarThumb_19(),
	GUISkin_t1244372282::get_offset_of_m_verticalScrollbarUpButton_20(),
	GUISkin_t1244372282::get_offset_of_m_verticalScrollbarDownButton_21(),
	GUISkin_t1244372282::get_offset_of_m_ScrollView_22(),
	GUISkin_t1244372282::get_offset_of_m_CustomStyles_23(),
	GUISkin_t1244372282::get_offset_of_m_Settings_24(),
	GUISkin_t1244372282_StaticFields::get_offset_of_ms_Error_25(),
	GUISkin_t1244372282::get_offset_of_m_Styles_26(),
	GUISkin_t1244372282_StaticFields::get_offset_of_m_SkinChanged_27(),
	GUISkin_t1244372282_StaticFields::get_offset_of_current_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3993 = { sizeof (SkinChangedDelegate_t1143955295), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3994 = { sizeof (GUIStyleState_t1397964415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3994[3] = 
{
	GUIStyleState_t1397964415::get_offset_of_m_Ptr_0(),
	GUIStyleState_t1397964415::get_offset_of_m_SourceStyle_1(),
	GUIStyleState_t1397964415::get_offset_of_m_Background_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3995 = { sizeof (ImagePosition_t641749504)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3995[5] = 
{
	ImagePosition_t641749504::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3996 = { sizeof (GUIStyle_t3956901511), -1, sizeof(GUIStyle_t3956901511_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3996[16] = 
{
	GUIStyle_t3956901511::get_offset_of_m_Ptr_0(),
	GUIStyle_t3956901511::get_offset_of_m_Normal_1(),
	GUIStyle_t3956901511::get_offset_of_m_Hover_2(),
	GUIStyle_t3956901511::get_offset_of_m_Active_3(),
	GUIStyle_t3956901511::get_offset_of_m_Focused_4(),
	GUIStyle_t3956901511::get_offset_of_m_OnNormal_5(),
	GUIStyle_t3956901511::get_offset_of_m_OnHover_6(),
	GUIStyle_t3956901511::get_offset_of_m_OnActive_7(),
	GUIStyle_t3956901511::get_offset_of_m_OnFocused_8(),
	GUIStyle_t3956901511::get_offset_of_m_Border_9(),
	GUIStyle_t3956901511::get_offset_of_m_Padding_10(),
	GUIStyle_t3956901511::get_offset_of_m_Margin_11(),
	GUIStyle_t3956901511::get_offset_of_m_Overflow_12(),
	GUIStyle_t3956901511::get_offset_of_m_FontInternal_13(),
	GUIStyle_t3956901511_StaticFields::get_offset_of_showKeyboardFocus_14(),
	GUIStyle_t3956901511_StaticFields::get_offset_of_s_None_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3997 = { sizeof (TextClipping_t865312958)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3997[3] = 
{
	TextClipping_t865312958::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3998 = { sizeof (GUITargetAttribute_t25796337), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3998[1] = 
{
	GUITargetAttribute_t25796337::get_offset_of_displayMask_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3999 = { sizeof (ExitGUIException_t133215258), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
