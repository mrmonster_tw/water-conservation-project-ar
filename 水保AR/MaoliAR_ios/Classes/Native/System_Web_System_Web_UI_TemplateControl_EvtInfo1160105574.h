﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Reflection.EventInfo
struct EventInfo_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.TemplateControl/EvtInfo
struct  EvtInfo_t1160105574  : public Il2CppObject
{
public:
	// System.Reflection.MethodInfo System.Web.UI.TemplateControl/EvtInfo::method
	MethodInfo_t * ___method_0;
	// System.String System.Web.UI.TemplateControl/EvtInfo::methodName
	String_t* ___methodName_1;
	// System.Reflection.EventInfo System.Web.UI.TemplateControl/EvtInfo::evt
	EventInfo_t * ___evt_2;
	// System.Boolean System.Web.UI.TemplateControl/EvtInfo::noParams
	bool ___noParams_3;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(EvtInfo_t1160105574, ___method_0)); }
	inline MethodInfo_t * get_method_0() const { return ___method_0; }
	inline MethodInfo_t ** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(MethodInfo_t * value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier(&___method_0, value);
	}

	inline static int32_t get_offset_of_methodName_1() { return static_cast<int32_t>(offsetof(EvtInfo_t1160105574, ___methodName_1)); }
	inline String_t* get_methodName_1() const { return ___methodName_1; }
	inline String_t** get_address_of_methodName_1() { return &___methodName_1; }
	inline void set_methodName_1(String_t* value)
	{
		___methodName_1 = value;
		Il2CppCodeGenWriteBarrier(&___methodName_1, value);
	}

	inline static int32_t get_offset_of_evt_2() { return static_cast<int32_t>(offsetof(EvtInfo_t1160105574, ___evt_2)); }
	inline EventInfo_t * get_evt_2() const { return ___evt_2; }
	inline EventInfo_t ** get_address_of_evt_2() { return &___evt_2; }
	inline void set_evt_2(EventInfo_t * value)
	{
		___evt_2 = value;
		Il2CppCodeGenWriteBarrier(&___evt_2, value);
	}

	inline static int32_t get_offset_of_noParams_3() { return static_cast<int32_t>(offsetof(EvtInfo_t1160105574, ___noParams_3)); }
	inline bool get_noParams_3() const { return ___noParams_3; }
	inline bool* get_address_of_noParams_3() { return &___noParams_3; }
	inline void set_noParams_3(bool value)
	{
		___noParams_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
