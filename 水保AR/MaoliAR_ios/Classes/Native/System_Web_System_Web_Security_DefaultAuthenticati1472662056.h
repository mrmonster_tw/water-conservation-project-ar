﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Object
struct Il2CppObject;
// System.Security.Principal.GenericIdentity
struct GenericIdentity_t2319019448;
// System.ComponentModel.EventHandlerList
struct EventHandlerList_t1108123056;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Security.DefaultAuthenticationModule
struct  DefaultAuthenticationModule_t1472662056  : public Il2CppObject
{
public:
	// System.ComponentModel.EventHandlerList System.Web.Security.DefaultAuthenticationModule::events
	EventHandlerList_t1108123056 * ___events_2;

public:
	inline static int32_t get_offset_of_events_2() { return static_cast<int32_t>(offsetof(DefaultAuthenticationModule_t1472662056, ___events_2)); }
	inline EventHandlerList_t1108123056 * get_events_2() const { return ___events_2; }
	inline EventHandlerList_t1108123056 ** get_address_of_events_2() { return &___events_2; }
	inline void set_events_2(EventHandlerList_t1108123056 * value)
	{
		___events_2 = value;
		Il2CppCodeGenWriteBarrier(&___events_2, value);
	}
};

struct DefaultAuthenticationModule_t1472662056_StaticFields
{
public:
	// System.Object System.Web.Security.DefaultAuthenticationModule::authenticateEvent
	Il2CppObject * ___authenticateEvent_0;
	// System.Security.Principal.GenericIdentity System.Web.Security.DefaultAuthenticationModule::defaultIdentity
	GenericIdentity_t2319019448 * ___defaultIdentity_1;

public:
	inline static int32_t get_offset_of_authenticateEvent_0() { return static_cast<int32_t>(offsetof(DefaultAuthenticationModule_t1472662056_StaticFields, ___authenticateEvent_0)); }
	inline Il2CppObject * get_authenticateEvent_0() const { return ___authenticateEvent_0; }
	inline Il2CppObject ** get_address_of_authenticateEvent_0() { return &___authenticateEvent_0; }
	inline void set_authenticateEvent_0(Il2CppObject * value)
	{
		___authenticateEvent_0 = value;
		Il2CppCodeGenWriteBarrier(&___authenticateEvent_0, value);
	}

	inline static int32_t get_offset_of_defaultIdentity_1() { return static_cast<int32_t>(offsetof(DefaultAuthenticationModule_t1472662056_StaticFields, ___defaultIdentity_1)); }
	inline GenericIdentity_t2319019448 * get_defaultIdentity_1() const { return ___defaultIdentity_1; }
	inline GenericIdentity_t2319019448 ** get_address_of_defaultIdentity_1() { return &___defaultIdentity_1; }
	inline void set_defaultIdentity_1(GenericIdentity_t2319019448 * value)
	{
		___defaultIdentity_1 = value;
		Il2CppCodeGenWriteBarrier(&___defaultIdentity_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
