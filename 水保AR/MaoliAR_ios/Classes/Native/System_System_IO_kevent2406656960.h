﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"
#include "mscorlib_System_IntPtr840150181.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.kevent
struct  kevent_t2406656960 
{
public:
	// System.Int32 System.IO.kevent::ident
	int32_t ___ident_0;
	// System.Int16 System.IO.kevent::filter
	int16_t ___filter_1;
	// System.UInt16 System.IO.kevent::flags
	uint16_t ___flags_2;
	// System.UInt32 System.IO.kevent::fflags
	uint32_t ___fflags_3;
	// System.Int32 System.IO.kevent::data
	int32_t ___data_4;
	// System.IntPtr System.IO.kevent::udata
	IntPtr_t ___udata_5;

public:
	inline static int32_t get_offset_of_ident_0() { return static_cast<int32_t>(offsetof(kevent_t2406656960, ___ident_0)); }
	inline int32_t get_ident_0() const { return ___ident_0; }
	inline int32_t* get_address_of_ident_0() { return &___ident_0; }
	inline void set_ident_0(int32_t value)
	{
		___ident_0 = value;
	}

	inline static int32_t get_offset_of_filter_1() { return static_cast<int32_t>(offsetof(kevent_t2406656960, ___filter_1)); }
	inline int16_t get_filter_1() const { return ___filter_1; }
	inline int16_t* get_address_of_filter_1() { return &___filter_1; }
	inline void set_filter_1(int16_t value)
	{
		___filter_1 = value;
	}

	inline static int32_t get_offset_of_flags_2() { return static_cast<int32_t>(offsetof(kevent_t2406656960, ___flags_2)); }
	inline uint16_t get_flags_2() const { return ___flags_2; }
	inline uint16_t* get_address_of_flags_2() { return &___flags_2; }
	inline void set_flags_2(uint16_t value)
	{
		___flags_2 = value;
	}

	inline static int32_t get_offset_of_fflags_3() { return static_cast<int32_t>(offsetof(kevent_t2406656960, ___fflags_3)); }
	inline uint32_t get_fflags_3() const { return ___fflags_3; }
	inline uint32_t* get_address_of_fflags_3() { return &___fflags_3; }
	inline void set_fflags_3(uint32_t value)
	{
		___fflags_3 = value;
	}

	inline static int32_t get_offset_of_data_4() { return static_cast<int32_t>(offsetof(kevent_t2406656960, ___data_4)); }
	inline int32_t get_data_4() const { return ___data_4; }
	inline int32_t* get_address_of_data_4() { return &___data_4; }
	inline void set_data_4(int32_t value)
	{
		___data_4 = value;
	}

	inline static int32_t get_offset_of_udata_5() { return static_cast<int32_t>(offsetof(kevent_t2406656960, ___udata_5)); }
	inline IntPtr_t get_udata_5() const { return ___udata_5; }
	inline IntPtr_t* get_address_of_udata_5() { return &___udata_5; }
	inline void set_udata_5(IntPtr_t value)
	{
		___udata_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
