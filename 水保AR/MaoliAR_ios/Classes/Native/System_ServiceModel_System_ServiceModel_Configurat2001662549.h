﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Configurat4182893664.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.WSDualHttpBindingElement
struct  WSDualHttpBindingElement_t2001662549  : public StandardBindingElement_t4182893664
{
public:

public:
};

struct WSDualHttpBindingElement_t2001662549_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.WSDualHttpBindingElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_14;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.WSDualHttpBindingElement::binding_element_type
	ConfigurationProperty_t3590861854 * ___binding_element_type_15;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.WSDualHttpBindingElement::bypass_proxy_on_local
	ConfigurationProperty_t3590861854 * ___bypass_proxy_on_local_16;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.WSDualHttpBindingElement::client_base_address
	ConfigurationProperty_t3590861854 * ___client_base_address_17;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.WSDualHttpBindingElement::host_name_comparison_mode
	ConfigurationProperty_t3590861854 * ___host_name_comparison_mode_18;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.WSDualHttpBindingElement::max_buffer_pool_size
	ConfigurationProperty_t3590861854 * ___max_buffer_pool_size_19;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.WSDualHttpBindingElement::max_received_message_size
	ConfigurationProperty_t3590861854 * ___max_received_message_size_20;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.WSDualHttpBindingElement::message_encoding
	ConfigurationProperty_t3590861854 * ___message_encoding_21;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.WSDualHttpBindingElement::proxy_address
	ConfigurationProperty_t3590861854 * ___proxy_address_22;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.WSDualHttpBindingElement::reader_quotas
	ConfigurationProperty_t3590861854 * ___reader_quotas_23;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.WSDualHttpBindingElement::reliable_session
	ConfigurationProperty_t3590861854 * ___reliable_session_24;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.WSDualHttpBindingElement::security
	ConfigurationProperty_t3590861854 * ___security_25;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.WSDualHttpBindingElement::text_encoding
	ConfigurationProperty_t3590861854 * ___text_encoding_26;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.WSDualHttpBindingElement::transaction_flow
	ConfigurationProperty_t3590861854 * ___transaction_flow_27;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.WSDualHttpBindingElement::use_default_web_proxy
	ConfigurationProperty_t3590861854 * ___use_default_web_proxy_28;

public:
	inline static int32_t get_offset_of_properties_14() { return static_cast<int32_t>(offsetof(WSDualHttpBindingElement_t2001662549_StaticFields, ___properties_14)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_14() const { return ___properties_14; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_14() { return &___properties_14; }
	inline void set_properties_14(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_14 = value;
		Il2CppCodeGenWriteBarrier(&___properties_14, value);
	}

	inline static int32_t get_offset_of_binding_element_type_15() { return static_cast<int32_t>(offsetof(WSDualHttpBindingElement_t2001662549_StaticFields, ___binding_element_type_15)); }
	inline ConfigurationProperty_t3590861854 * get_binding_element_type_15() const { return ___binding_element_type_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_binding_element_type_15() { return &___binding_element_type_15; }
	inline void set_binding_element_type_15(ConfigurationProperty_t3590861854 * value)
	{
		___binding_element_type_15 = value;
		Il2CppCodeGenWriteBarrier(&___binding_element_type_15, value);
	}

	inline static int32_t get_offset_of_bypass_proxy_on_local_16() { return static_cast<int32_t>(offsetof(WSDualHttpBindingElement_t2001662549_StaticFields, ___bypass_proxy_on_local_16)); }
	inline ConfigurationProperty_t3590861854 * get_bypass_proxy_on_local_16() const { return ___bypass_proxy_on_local_16; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_bypass_proxy_on_local_16() { return &___bypass_proxy_on_local_16; }
	inline void set_bypass_proxy_on_local_16(ConfigurationProperty_t3590861854 * value)
	{
		___bypass_proxy_on_local_16 = value;
		Il2CppCodeGenWriteBarrier(&___bypass_proxy_on_local_16, value);
	}

	inline static int32_t get_offset_of_client_base_address_17() { return static_cast<int32_t>(offsetof(WSDualHttpBindingElement_t2001662549_StaticFields, ___client_base_address_17)); }
	inline ConfigurationProperty_t3590861854 * get_client_base_address_17() const { return ___client_base_address_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_client_base_address_17() { return &___client_base_address_17; }
	inline void set_client_base_address_17(ConfigurationProperty_t3590861854 * value)
	{
		___client_base_address_17 = value;
		Il2CppCodeGenWriteBarrier(&___client_base_address_17, value);
	}

	inline static int32_t get_offset_of_host_name_comparison_mode_18() { return static_cast<int32_t>(offsetof(WSDualHttpBindingElement_t2001662549_StaticFields, ___host_name_comparison_mode_18)); }
	inline ConfigurationProperty_t3590861854 * get_host_name_comparison_mode_18() const { return ___host_name_comparison_mode_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_host_name_comparison_mode_18() { return &___host_name_comparison_mode_18; }
	inline void set_host_name_comparison_mode_18(ConfigurationProperty_t3590861854 * value)
	{
		___host_name_comparison_mode_18 = value;
		Il2CppCodeGenWriteBarrier(&___host_name_comparison_mode_18, value);
	}

	inline static int32_t get_offset_of_max_buffer_pool_size_19() { return static_cast<int32_t>(offsetof(WSDualHttpBindingElement_t2001662549_StaticFields, ___max_buffer_pool_size_19)); }
	inline ConfigurationProperty_t3590861854 * get_max_buffer_pool_size_19() const { return ___max_buffer_pool_size_19; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_max_buffer_pool_size_19() { return &___max_buffer_pool_size_19; }
	inline void set_max_buffer_pool_size_19(ConfigurationProperty_t3590861854 * value)
	{
		___max_buffer_pool_size_19 = value;
		Il2CppCodeGenWriteBarrier(&___max_buffer_pool_size_19, value);
	}

	inline static int32_t get_offset_of_max_received_message_size_20() { return static_cast<int32_t>(offsetof(WSDualHttpBindingElement_t2001662549_StaticFields, ___max_received_message_size_20)); }
	inline ConfigurationProperty_t3590861854 * get_max_received_message_size_20() const { return ___max_received_message_size_20; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_max_received_message_size_20() { return &___max_received_message_size_20; }
	inline void set_max_received_message_size_20(ConfigurationProperty_t3590861854 * value)
	{
		___max_received_message_size_20 = value;
		Il2CppCodeGenWriteBarrier(&___max_received_message_size_20, value);
	}

	inline static int32_t get_offset_of_message_encoding_21() { return static_cast<int32_t>(offsetof(WSDualHttpBindingElement_t2001662549_StaticFields, ___message_encoding_21)); }
	inline ConfigurationProperty_t3590861854 * get_message_encoding_21() const { return ___message_encoding_21; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_message_encoding_21() { return &___message_encoding_21; }
	inline void set_message_encoding_21(ConfigurationProperty_t3590861854 * value)
	{
		___message_encoding_21 = value;
		Il2CppCodeGenWriteBarrier(&___message_encoding_21, value);
	}

	inline static int32_t get_offset_of_proxy_address_22() { return static_cast<int32_t>(offsetof(WSDualHttpBindingElement_t2001662549_StaticFields, ___proxy_address_22)); }
	inline ConfigurationProperty_t3590861854 * get_proxy_address_22() const { return ___proxy_address_22; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_proxy_address_22() { return &___proxy_address_22; }
	inline void set_proxy_address_22(ConfigurationProperty_t3590861854 * value)
	{
		___proxy_address_22 = value;
		Il2CppCodeGenWriteBarrier(&___proxy_address_22, value);
	}

	inline static int32_t get_offset_of_reader_quotas_23() { return static_cast<int32_t>(offsetof(WSDualHttpBindingElement_t2001662549_StaticFields, ___reader_quotas_23)); }
	inline ConfigurationProperty_t3590861854 * get_reader_quotas_23() const { return ___reader_quotas_23; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_reader_quotas_23() { return &___reader_quotas_23; }
	inline void set_reader_quotas_23(ConfigurationProperty_t3590861854 * value)
	{
		___reader_quotas_23 = value;
		Il2CppCodeGenWriteBarrier(&___reader_quotas_23, value);
	}

	inline static int32_t get_offset_of_reliable_session_24() { return static_cast<int32_t>(offsetof(WSDualHttpBindingElement_t2001662549_StaticFields, ___reliable_session_24)); }
	inline ConfigurationProperty_t3590861854 * get_reliable_session_24() const { return ___reliable_session_24; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_reliable_session_24() { return &___reliable_session_24; }
	inline void set_reliable_session_24(ConfigurationProperty_t3590861854 * value)
	{
		___reliable_session_24 = value;
		Il2CppCodeGenWriteBarrier(&___reliable_session_24, value);
	}

	inline static int32_t get_offset_of_security_25() { return static_cast<int32_t>(offsetof(WSDualHttpBindingElement_t2001662549_StaticFields, ___security_25)); }
	inline ConfigurationProperty_t3590861854 * get_security_25() const { return ___security_25; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_security_25() { return &___security_25; }
	inline void set_security_25(ConfigurationProperty_t3590861854 * value)
	{
		___security_25 = value;
		Il2CppCodeGenWriteBarrier(&___security_25, value);
	}

	inline static int32_t get_offset_of_text_encoding_26() { return static_cast<int32_t>(offsetof(WSDualHttpBindingElement_t2001662549_StaticFields, ___text_encoding_26)); }
	inline ConfigurationProperty_t3590861854 * get_text_encoding_26() const { return ___text_encoding_26; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_text_encoding_26() { return &___text_encoding_26; }
	inline void set_text_encoding_26(ConfigurationProperty_t3590861854 * value)
	{
		___text_encoding_26 = value;
		Il2CppCodeGenWriteBarrier(&___text_encoding_26, value);
	}

	inline static int32_t get_offset_of_transaction_flow_27() { return static_cast<int32_t>(offsetof(WSDualHttpBindingElement_t2001662549_StaticFields, ___transaction_flow_27)); }
	inline ConfigurationProperty_t3590861854 * get_transaction_flow_27() const { return ___transaction_flow_27; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_transaction_flow_27() { return &___transaction_flow_27; }
	inline void set_transaction_flow_27(ConfigurationProperty_t3590861854 * value)
	{
		___transaction_flow_27 = value;
		Il2CppCodeGenWriteBarrier(&___transaction_flow_27, value);
	}

	inline static int32_t get_offset_of_use_default_web_proxy_28() { return static_cast<int32_t>(offsetof(WSDualHttpBindingElement_t2001662549_StaticFields, ___use_default_web_proxy_28)); }
	inline ConfigurationProperty_t3590861854 * get_use_default_web_proxy_28() const { return ___use_default_web_proxy_28; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_use_default_web_proxy_28() { return &___use_default_web_proxy_28; }
	inline void set_use_default_web_proxy_28(ConfigurationProperty_t3590861854 * value)
	{
		___use_default_web_proxy_28 = value;
		Il2CppCodeGenWriteBarrier(&___use_default_web_proxy_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
