﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.AspTokenizer/PutBackItem
struct  PutBackItem_t147579746  : public Il2CppObject
{
public:
	// System.String System.Web.Compilation.AspTokenizer/PutBackItem::Value
	String_t* ___Value_0;
	// System.Int32 System.Web.Compilation.AspTokenizer/PutBackItem::Position
	int32_t ___Position_1;
	// System.Int32 System.Web.Compilation.AspTokenizer/PutBackItem::CurrentToken
	int32_t ___CurrentToken_2;
	// System.Boolean System.Web.Compilation.AspTokenizer/PutBackItem::InTag
	bool ___InTag_3;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(PutBackItem_t147579746, ___Value_0)); }
	inline String_t* get_Value_0() const { return ___Value_0; }
	inline String_t** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(String_t* value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier(&___Value_0, value);
	}

	inline static int32_t get_offset_of_Position_1() { return static_cast<int32_t>(offsetof(PutBackItem_t147579746, ___Position_1)); }
	inline int32_t get_Position_1() const { return ___Position_1; }
	inline int32_t* get_address_of_Position_1() { return &___Position_1; }
	inline void set_Position_1(int32_t value)
	{
		___Position_1 = value;
	}

	inline static int32_t get_offset_of_CurrentToken_2() { return static_cast<int32_t>(offsetof(PutBackItem_t147579746, ___CurrentToken_2)); }
	inline int32_t get_CurrentToken_2() const { return ___CurrentToken_2; }
	inline int32_t* get_address_of_CurrentToken_2() { return &___CurrentToken_2; }
	inline void set_CurrentToken_2(int32_t value)
	{
		___CurrentToken_2 = value;
	}

	inline static int32_t get_offset_of_InTag_3() { return static_cast<int32_t>(offsetof(PutBackItem_t147579746, ___InTag_3)); }
	inline bool get_InTag_3() const { return ___InTag_3; }
	inline bool* get_address_of_InTag_3() { return &___InTag_3; }
	inline void set_InTag_3(bool value)
	{
		___InTag_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
