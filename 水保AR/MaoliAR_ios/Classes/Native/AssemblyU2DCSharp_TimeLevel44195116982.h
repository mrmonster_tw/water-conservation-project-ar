﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;
// UnityEngine.SpriteRenderer[]
struct SpriteRendererU5BU5D_t911335936;
// Drag_Controller[]
struct Drag_ControllerU5BU5D_t2274009765;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;
// Level_Controller
struct Level_Controller_t1433348427;
// System.Predicate`1<Level_Controller>
struct Predicate_1_t2258642551;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimeLevel4
struct  TimeLevel4_t4195116982  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TimeLevel4::Left_time
	float ___Left_time_2;
	// System.Single TimeLevel4::Left_timeMax
	float ___Left_timeMax_3;
	// UnityEngine.GameObject TimeLevel4::checkObj
	GameObject_t1113636619 * ___checkObj_4;
	// UnityEngine.UI.Image TimeLevel4::im
	Image_t2670269651 * ___im_5;
	// UnityEngine.UI.Text TimeLevel4::timeText
	Text_t1901882714 * ___timeText_6;
	// UnityEngine.GameObject TimeLevel4::Clock
	GameObject_t1113636619 * ___Clock_7;
	// UnityEngine.GameObject[] TimeLevel4::OpenIt
	GameObjectU5BU5D_t3328599146* ___OpenIt_8;
	// UnityEngine.Sprite[] TimeLevel4::boxCut
	SpriteU5BU5D_t2581906349* ___boxCut_9;
	// UnityEngine.SpriteRenderer[] TimeLevel4::boxAll
	SpriteRendererU5BU5D_t911335936* ___boxAll_10;
	// Drag_Controller[] TimeLevel4::DC_all
	Drag_ControllerU5BU5D_t2274009765* ___DC_all_11;
	// UnityEngine.Events.UnityEvent TimeLevel4::Events
	UnityEvent_t2581268647 * ___Events_12;
	// UnityEngine.GameObject TimeLevel4::ReplayTips
	GameObject_t1113636619 * ___ReplayTips_13;
	// System.Boolean TimeLevel4::once
	bool ___once_14;
	// Level_Controller TimeLevel4::Level_04
	Level_Controller_t1433348427 * ___Level_04_15;

public:
	inline static int32_t get_offset_of_Left_time_2() { return static_cast<int32_t>(offsetof(TimeLevel4_t4195116982, ___Left_time_2)); }
	inline float get_Left_time_2() const { return ___Left_time_2; }
	inline float* get_address_of_Left_time_2() { return &___Left_time_2; }
	inline void set_Left_time_2(float value)
	{
		___Left_time_2 = value;
	}

	inline static int32_t get_offset_of_Left_timeMax_3() { return static_cast<int32_t>(offsetof(TimeLevel4_t4195116982, ___Left_timeMax_3)); }
	inline float get_Left_timeMax_3() const { return ___Left_timeMax_3; }
	inline float* get_address_of_Left_timeMax_3() { return &___Left_timeMax_3; }
	inline void set_Left_timeMax_3(float value)
	{
		___Left_timeMax_3 = value;
	}

	inline static int32_t get_offset_of_checkObj_4() { return static_cast<int32_t>(offsetof(TimeLevel4_t4195116982, ___checkObj_4)); }
	inline GameObject_t1113636619 * get_checkObj_4() const { return ___checkObj_4; }
	inline GameObject_t1113636619 ** get_address_of_checkObj_4() { return &___checkObj_4; }
	inline void set_checkObj_4(GameObject_t1113636619 * value)
	{
		___checkObj_4 = value;
		Il2CppCodeGenWriteBarrier(&___checkObj_4, value);
	}

	inline static int32_t get_offset_of_im_5() { return static_cast<int32_t>(offsetof(TimeLevel4_t4195116982, ___im_5)); }
	inline Image_t2670269651 * get_im_5() const { return ___im_5; }
	inline Image_t2670269651 ** get_address_of_im_5() { return &___im_5; }
	inline void set_im_5(Image_t2670269651 * value)
	{
		___im_5 = value;
		Il2CppCodeGenWriteBarrier(&___im_5, value);
	}

	inline static int32_t get_offset_of_timeText_6() { return static_cast<int32_t>(offsetof(TimeLevel4_t4195116982, ___timeText_6)); }
	inline Text_t1901882714 * get_timeText_6() const { return ___timeText_6; }
	inline Text_t1901882714 ** get_address_of_timeText_6() { return &___timeText_6; }
	inline void set_timeText_6(Text_t1901882714 * value)
	{
		___timeText_6 = value;
		Il2CppCodeGenWriteBarrier(&___timeText_6, value);
	}

	inline static int32_t get_offset_of_Clock_7() { return static_cast<int32_t>(offsetof(TimeLevel4_t4195116982, ___Clock_7)); }
	inline GameObject_t1113636619 * get_Clock_7() const { return ___Clock_7; }
	inline GameObject_t1113636619 ** get_address_of_Clock_7() { return &___Clock_7; }
	inline void set_Clock_7(GameObject_t1113636619 * value)
	{
		___Clock_7 = value;
		Il2CppCodeGenWriteBarrier(&___Clock_7, value);
	}

	inline static int32_t get_offset_of_OpenIt_8() { return static_cast<int32_t>(offsetof(TimeLevel4_t4195116982, ___OpenIt_8)); }
	inline GameObjectU5BU5D_t3328599146* get_OpenIt_8() const { return ___OpenIt_8; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_OpenIt_8() { return &___OpenIt_8; }
	inline void set_OpenIt_8(GameObjectU5BU5D_t3328599146* value)
	{
		___OpenIt_8 = value;
		Il2CppCodeGenWriteBarrier(&___OpenIt_8, value);
	}

	inline static int32_t get_offset_of_boxCut_9() { return static_cast<int32_t>(offsetof(TimeLevel4_t4195116982, ___boxCut_9)); }
	inline SpriteU5BU5D_t2581906349* get_boxCut_9() const { return ___boxCut_9; }
	inline SpriteU5BU5D_t2581906349** get_address_of_boxCut_9() { return &___boxCut_9; }
	inline void set_boxCut_9(SpriteU5BU5D_t2581906349* value)
	{
		___boxCut_9 = value;
		Il2CppCodeGenWriteBarrier(&___boxCut_9, value);
	}

	inline static int32_t get_offset_of_boxAll_10() { return static_cast<int32_t>(offsetof(TimeLevel4_t4195116982, ___boxAll_10)); }
	inline SpriteRendererU5BU5D_t911335936* get_boxAll_10() const { return ___boxAll_10; }
	inline SpriteRendererU5BU5D_t911335936** get_address_of_boxAll_10() { return &___boxAll_10; }
	inline void set_boxAll_10(SpriteRendererU5BU5D_t911335936* value)
	{
		___boxAll_10 = value;
		Il2CppCodeGenWriteBarrier(&___boxAll_10, value);
	}

	inline static int32_t get_offset_of_DC_all_11() { return static_cast<int32_t>(offsetof(TimeLevel4_t4195116982, ___DC_all_11)); }
	inline Drag_ControllerU5BU5D_t2274009765* get_DC_all_11() const { return ___DC_all_11; }
	inline Drag_ControllerU5BU5D_t2274009765** get_address_of_DC_all_11() { return &___DC_all_11; }
	inline void set_DC_all_11(Drag_ControllerU5BU5D_t2274009765* value)
	{
		___DC_all_11 = value;
		Il2CppCodeGenWriteBarrier(&___DC_all_11, value);
	}

	inline static int32_t get_offset_of_Events_12() { return static_cast<int32_t>(offsetof(TimeLevel4_t4195116982, ___Events_12)); }
	inline UnityEvent_t2581268647 * get_Events_12() const { return ___Events_12; }
	inline UnityEvent_t2581268647 ** get_address_of_Events_12() { return &___Events_12; }
	inline void set_Events_12(UnityEvent_t2581268647 * value)
	{
		___Events_12 = value;
		Il2CppCodeGenWriteBarrier(&___Events_12, value);
	}

	inline static int32_t get_offset_of_ReplayTips_13() { return static_cast<int32_t>(offsetof(TimeLevel4_t4195116982, ___ReplayTips_13)); }
	inline GameObject_t1113636619 * get_ReplayTips_13() const { return ___ReplayTips_13; }
	inline GameObject_t1113636619 ** get_address_of_ReplayTips_13() { return &___ReplayTips_13; }
	inline void set_ReplayTips_13(GameObject_t1113636619 * value)
	{
		___ReplayTips_13 = value;
		Il2CppCodeGenWriteBarrier(&___ReplayTips_13, value);
	}

	inline static int32_t get_offset_of_once_14() { return static_cast<int32_t>(offsetof(TimeLevel4_t4195116982, ___once_14)); }
	inline bool get_once_14() const { return ___once_14; }
	inline bool* get_address_of_once_14() { return &___once_14; }
	inline void set_once_14(bool value)
	{
		___once_14 = value;
	}

	inline static int32_t get_offset_of_Level_04_15() { return static_cast<int32_t>(offsetof(TimeLevel4_t4195116982, ___Level_04_15)); }
	inline Level_Controller_t1433348427 * get_Level_04_15() const { return ___Level_04_15; }
	inline Level_Controller_t1433348427 ** get_address_of_Level_04_15() { return &___Level_04_15; }
	inline void set_Level_04_15(Level_Controller_t1433348427 * value)
	{
		___Level_04_15 = value;
		Il2CppCodeGenWriteBarrier(&___Level_04_15, value);
	}
};

struct TimeLevel4_t4195116982_StaticFields
{
public:
	// System.Predicate`1<Level_Controller> TimeLevel4::<>f__am$cache0
	Predicate_1_t2258642551 * ___U3CU3Ef__amU24cache0_16;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_16() { return static_cast<int32_t>(offsetof(TimeLevel4_t4195116982_StaticFields, ___U3CU3Ef__amU24cache0_16)); }
	inline Predicate_1_t2258642551 * get_U3CU3Ef__amU24cache0_16() const { return ___U3CU3Ef__amU24cache0_16; }
	inline Predicate_1_t2258642551 ** get_address_of_U3CU3Ef__amU24cache0_16() { return &___U3CU3Ef__amU24cache0_16; }
	inline void set_U3CU3Ef__amU24cache0_16(Predicate_1_t2258642551 * value)
	{
		___U3CU3Ef__amU24cache0_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
