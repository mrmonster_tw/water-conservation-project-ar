﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Web_U3CPrivateImplementationDetailsU3E_U24A2490092596.h"
#include "System_Web_U3CPrivateImplementationDetailsU3E_U24A3244137463.h"
#include "System_Web_U3CPrivateImplementationDetailsU3E_U24A3652892010.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255370  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255370_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType$12 <PrivateImplementationDetails>::$$field-0
	U24ArrayTypeU2412_t2490092600  ___U24U24fieldU2D0_0;
	// <PrivateImplementationDetails>/$ArrayType$8 <PrivateImplementationDetails>::$$field-2
	U24ArrayTypeU248_t3244137468  ___U24U24fieldU2D2_1;
	// <PrivateImplementationDetails>/$ArrayType$32 <PrivateImplementationDetails>::$$field-5
	U24ArrayTypeU2432_t3652892013  ___U24U24fieldU2D5_2;

public:
	inline static int32_t get_offset_of_U24U24fieldU2D0_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255370_StaticFields, ___U24U24fieldU2D0_0)); }
	inline U24ArrayTypeU2412_t2490092600  get_U24U24fieldU2D0_0() const { return ___U24U24fieldU2D0_0; }
	inline U24ArrayTypeU2412_t2490092600 * get_address_of_U24U24fieldU2D0_0() { return &___U24U24fieldU2D0_0; }
	inline void set_U24U24fieldU2D0_0(U24ArrayTypeU2412_t2490092600  value)
	{
		___U24U24fieldU2D0_0 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D2_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255370_StaticFields, ___U24U24fieldU2D2_1)); }
	inline U24ArrayTypeU248_t3244137468  get_U24U24fieldU2D2_1() const { return ___U24U24fieldU2D2_1; }
	inline U24ArrayTypeU248_t3244137468 * get_address_of_U24U24fieldU2D2_1() { return &___U24U24fieldU2D2_1; }
	inline void set_U24U24fieldU2D2_1(U24ArrayTypeU248_t3244137468  value)
	{
		___U24U24fieldU2D2_1 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D5_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255370_StaticFields, ___U24U24fieldU2D5_2)); }
	inline U24ArrayTypeU2432_t3652892013  get_U24U24fieldU2D5_2() const { return ___U24U24fieldU2D5_2; }
	inline U24ArrayTypeU2432_t3652892013 * get_address_of_U24U24fieldU2D5_2() { return &___U24U24fieldU2D5_2; }
	inline void set_U24U24fieldU2D5_2(U24ArrayTypeU2432_t3652892013  value)
	{
		___U24U24fieldU2D5_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
