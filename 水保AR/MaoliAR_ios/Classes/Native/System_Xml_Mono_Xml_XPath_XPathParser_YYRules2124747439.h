﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_MarshalByRefObject2760389100.h"

// System.String[]
struct StringU5BU5D_t1281789340;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.XPath.XPathParser/YYRules
struct  YYRules_t2124747439  : public MarshalByRefObject_t2760389100
{
public:

public:
};

struct YYRules_t2124747439_StaticFields
{
public:
	// System.String[] Mono.Xml.XPath.XPathParser/YYRules::yyRule
	StringU5BU5D_t1281789340* ___yyRule_1;

public:
	inline static int32_t get_offset_of_yyRule_1() { return static_cast<int32_t>(offsetof(YYRules_t2124747439_StaticFields, ___yyRule_1)); }
	inline StringU5BU5D_t1281789340* get_yyRule_1() const { return ___yyRule_1; }
	inline StringU5BU5D_t1281789340** get_address_of_yyRule_1() { return &___yyRule_1; }
	inline void set_yyRule_1(StringU5BU5D_t1281789340* value)
	{
		___yyRule_1 = value;
		Il2CppCodeGenWriteBarrier(&___yyRule_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
