﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeExpression2166265795.h"

// System.CodeDom.CodeTypeReference
struct CodeTypeReference_t3809997434;
// System.CodeDom.CodeExpression
struct CodeExpression_t2166265795;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeCastExpression
struct  CodeCastExpression_t2486271560  : public CodeExpression_t2166265795
{
public:
	// System.CodeDom.CodeTypeReference System.CodeDom.CodeCastExpression::targetType
	CodeTypeReference_t3809997434 * ___targetType_1;
	// System.CodeDom.CodeExpression System.CodeDom.CodeCastExpression::expression
	CodeExpression_t2166265795 * ___expression_2;

public:
	inline static int32_t get_offset_of_targetType_1() { return static_cast<int32_t>(offsetof(CodeCastExpression_t2486271560, ___targetType_1)); }
	inline CodeTypeReference_t3809997434 * get_targetType_1() const { return ___targetType_1; }
	inline CodeTypeReference_t3809997434 ** get_address_of_targetType_1() { return &___targetType_1; }
	inline void set_targetType_1(CodeTypeReference_t3809997434 * value)
	{
		___targetType_1 = value;
		Il2CppCodeGenWriteBarrier(&___targetType_1, value);
	}

	inline static int32_t get_offset_of_expression_2() { return static_cast<int32_t>(offsetof(CodeCastExpression_t2486271560, ___expression_2)); }
	inline CodeExpression_t2166265795 * get_expression_2() const { return ___expression_2; }
	inline CodeExpression_t2166265795 ** get_address_of_expression_2() { return &___expression_2; }
	inline void set_expression_2(CodeExpression_t2166265795 * value)
	{
		___expression_2 = value;
		Il2CppCodeGenWriteBarrier(&___expression_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
