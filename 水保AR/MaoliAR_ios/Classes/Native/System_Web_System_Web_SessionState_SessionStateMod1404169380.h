﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_TimeSpan881159249.h"
#include "System_Web_System_Web_SessionState_SessionStateAct2424179227.h"

// System.Object
struct Il2CppObject;
// System.Web.Configuration.SessionStateSection
struct SessionStateSection_t500006340;
// System.Web.SessionState.SessionStateStoreProviderBase
struct SessionStateStoreProviderBase_t182923317;
// System.Web.SessionState.ISessionIDManager
struct ISessionIDManager_t4103770884;
// System.Web.HttpApplication
struct HttpApplication_t3359645917;
// System.Web.SessionState.SessionStateStoreData
struct SessionStateStoreData_t4177544864;
// System.Web.SessionState.HttpSessionStateContainer
struct HttpSessionStateContainer_t50694200;
// System.ComponentModel.EventHandlerList
struct EventHandlerList_t1108123056;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.SessionState.SessionStateModule
struct  SessionStateModule_t1404169380  : public Il2CppObject
{
public:
	// System.Web.Configuration.SessionStateSection System.Web.SessionState.SessionStateModule::config
	SessionStateSection_t500006340 * ___config_2;
	// System.Web.SessionState.SessionStateStoreProviderBase System.Web.SessionState.SessionStateModule::handler
	SessionStateStoreProviderBase_t182923317 * ___handler_3;
	// System.Web.SessionState.ISessionIDManager System.Web.SessionState.SessionStateModule::idManager
	Il2CppObject * ___idManager_4;
	// System.Boolean System.Web.SessionState.SessionStateModule::supportsExpiration
	bool ___supportsExpiration_5;
	// System.Web.HttpApplication System.Web.SessionState.SessionStateModule::app
	HttpApplication_t3359645917 * ___app_6;
	// System.Boolean System.Web.SessionState.SessionStateModule::storeLocked
	bool ___storeLocked_7;
	// System.TimeSpan System.Web.SessionState.SessionStateModule::storeLockAge
	TimeSpan_t881159249  ___storeLockAge_8;
	// System.Object System.Web.SessionState.SessionStateModule::storeLockId
	Il2CppObject * ___storeLockId_9;
	// System.Web.SessionState.SessionStateActions System.Web.SessionState.SessionStateModule::storeSessionAction
	int32_t ___storeSessionAction_10;
	// System.Boolean System.Web.SessionState.SessionStateModule::storeIsNew
	bool ___storeIsNew_11;
	// System.Web.SessionState.SessionStateStoreData System.Web.SessionState.SessionStateModule::storeData
	SessionStateStoreData_t4177544864 * ___storeData_12;
	// System.Web.SessionState.HttpSessionStateContainer System.Web.SessionState.SessionStateModule::container
	HttpSessionStateContainer_t50694200 * ___container_13;
	// System.TimeSpan System.Web.SessionState.SessionStateModule::executionTimeout
	TimeSpan_t881159249  ___executionTimeout_14;
	// System.ComponentModel.EventHandlerList System.Web.SessionState.SessionStateModule::events
	EventHandlerList_t1108123056 * ___events_15;

public:
	inline static int32_t get_offset_of_config_2() { return static_cast<int32_t>(offsetof(SessionStateModule_t1404169380, ___config_2)); }
	inline SessionStateSection_t500006340 * get_config_2() const { return ___config_2; }
	inline SessionStateSection_t500006340 ** get_address_of_config_2() { return &___config_2; }
	inline void set_config_2(SessionStateSection_t500006340 * value)
	{
		___config_2 = value;
		Il2CppCodeGenWriteBarrier(&___config_2, value);
	}

	inline static int32_t get_offset_of_handler_3() { return static_cast<int32_t>(offsetof(SessionStateModule_t1404169380, ___handler_3)); }
	inline SessionStateStoreProviderBase_t182923317 * get_handler_3() const { return ___handler_3; }
	inline SessionStateStoreProviderBase_t182923317 ** get_address_of_handler_3() { return &___handler_3; }
	inline void set_handler_3(SessionStateStoreProviderBase_t182923317 * value)
	{
		___handler_3 = value;
		Il2CppCodeGenWriteBarrier(&___handler_3, value);
	}

	inline static int32_t get_offset_of_idManager_4() { return static_cast<int32_t>(offsetof(SessionStateModule_t1404169380, ___idManager_4)); }
	inline Il2CppObject * get_idManager_4() const { return ___idManager_4; }
	inline Il2CppObject ** get_address_of_idManager_4() { return &___idManager_4; }
	inline void set_idManager_4(Il2CppObject * value)
	{
		___idManager_4 = value;
		Il2CppCodeGenWriteBarrier(&___idManager_4, value);
	}

	inline static int32_t get_offset_of_supportsExpiration_5() { return static_cast<int32_t>(offsetof(SessionStateModule_t1404169380, ___supportsExpiration_5)); }
	inline bool get_supportsExpiration_5() const { return ___supportsExpiration_5; }
	inline bool* get_address_of_supportsExpiration_5() { return &___supportsExpiration_5; }
	inline void set_supportsExpiration_5(bool value)
	{
		___supportsExpiration_5 = value;
	}

	inline static int32_t get_offset_of_app_6() { return static_cast<int32_t>(offsetof(SessionStateModule_t1404169380, ___app_6)); }
	inline HttpApplication_t3359645917 * get_app_6() const { return ___app_6; }
	inline HttpApplication_t3359645917 ** get_address_of_app_6() { return &___app_6; }
	inline void set_app_6(HttpApplication_t3359645917 * value)
	{
		___app_6 = value;
		Il2CppCodeGenWriteBarrier(&___app_6, value);
	}

	inline static int32_t get_offset_of_storeLocked_7() { return static_cast<int32_t>(offsetof(SessionStateModule_t1404169380, ___storeLocked_7)); }
	inline bool get_storeLocked_7() const { return ___storeLocked_7; }
	inline bool* get_address_of_storeLocked_7() { return &___storeLocked_7; }
	inline void set_storeLocked_7(bool value)
	{
		___storeLocked_7 = value;
	}

	inline static int32_t get_offset_of_storeLockAge_8() { return static_cast<int32_t>(offsetof(SessionStateModule_t1404169380, ___storeLockAge_8)); }
	inline TimeSpan_t881159249  get_storeLockAge_8() const { return ___storeLockAge_8; }
	inline TimeSpan_t881159249 * get_address_of_storeLockAge_8() { return &___storeLockAge_8; }
	inline void set_storeLockAge_8(TimeSpan_t881159249  value)
	{
		___storeLockAge_8 = value;
	}

	inline static int32_t get_offset_of_storeLockId_9() { return static_cast<int32_t>(offsetof(SessionStateModule_t1404169380, ___storeLockId_9)); }
	inline Il2CppObject * get_storeLockId_9() const { return ___storeLockId_9; }
	inline Il2CppObject ** get_address_of_storeLockId_9() { return &___storeLockId_9; }
	inline void set_storeLockId_9(Il2CppObject * value)
	{
		___storeLockId_9 = value;
		Il2CppCodeGenWriteBarrier(&___storeLockId_9, value);
	}

	inline static int32_t get_offset_of_storeSessionAction_10() { return static_cast<int32_t>(offsetof(SessionStateModule_t1404169380, ___storeSessionAction_10)); }
	inline int32_t get_storeSessionAction_10() const { return ___storeSessionAction_10; }
	inline int32_t* get_address_of_storeSessionAction_10() { return &___storeSessionAction_10; }
	inline void set_storeSessionAction_10(int32_t value)
	{
		___storeSessionAction_10 = value;
	}

	inline static int32_t get_offset_of_storeIsNew_11() { return static_cast<int32_t>(offsetof(SessionStateModule_t1404169380, ___storeIsNew_11)); }
	inline bool get_storeIsNew_11() const { return ___storeIsNew_11; }
	inline bool* get_address_of_storeIsNew_11() { return &___storeIsNew_11; }
	inline void set_storeIsNew_11(bool value)
	{
		___storeIsNew_11 = value;
	}

	inline static int32_t get_offset_of_storeData_12() { return static_cast<int32_t>(offsetof(SessionStateModule_t1404169380, ___storeData_12)); }
	inline SessionStateStoreData_t4177544864 * get_storeData_12() const { return ___storeData_12; }
	inline SessionStateStoreData_t4177544864 ** get_address_of_storeData_12() { return &___storeData_12; }
	inline void set_storeData_12(SessionStateStoreData_t4177544864 * value)
	{
		___storeData_12 = value;
		Il2CppCodeGenWriteBarrier(&___storeData_12, value);
	}

	inline static int32_t get_offset_of_container_13() { return static_cast<int32_t>(offsetof(SessionStateModule_t1404169380, ___container_13)); }
	inline HttpSessionStateContainer_t50694200 * get_container_13() const { return ___container_13; }
	inline HttpSessionStateContainer_t50694200 ** get_address_of_container_13() { return &___container_13; }
	inline void set_container_13(HttpSessionStateContainer_t50694200 * value)
	{
		___container_13 = value;
		Il2CppCodeGenWriteBarrier(&___container_13, value);
	}

	inline static int32_t get_offset_of_executionTimeout_14() { return static_cast<int32_t>(offsetof(SessionStateModule_t1404169380, ___executionTimeout_14)); }
	inline TimeSpan_t881159249  get_executionTimeout_14() const { return ___executionTimeout_14; }
	inline TimeSpan_t881159249 * get_address_of_executionTimeout_14() { return &___executionTimeout_14; }
	inline void set_executionTimeout_14(TimeSpan_t881159249  value)
	{
		___executionTimeout_14 = value;
	}

	inline static int32_t get_offset_of_events_15() { return static_cast<int32_t>(offsetof(SessionStateModule_t1404169380, ___events_15)); }
	inline EventHandlerList_t1108123056 * get_events_15() const { return ___events_15; }
	inline EventHandlerList_t1108123056 ** get_address_of_events_15() { return &___events_15; }
	inline void set_events_15(EventHandlerList_t1108123056 * value)
	{
		___events_15 = value;
		Il2CppCodeGenWriteBarrier(&___events_15, value);
	}
};

struct SessionStateModule_t1404169380_StaticFields
{
public:
	// System.Object System.Web.SessionState.SessionStateModule::startEvent
	Il2CppObject * ___startEvent_0;
	// System.Object System.Web.SessionState.SessionStateModule::endEvent
	Il2CppObject * ___endEvent_1;

public:
	inline static int32_t get_offset_of_startEvent_0() { return static_cast<int32_t>(offsetof(SessionStateModule_t1404169380_StaticFields, ___startEvent_0)); }
	inline Il2CppObject * get_startEvent_0() const { return ___startEvent_0; }
	inline Il2CppObject ** get_address_of_startEvent_0() { return &___startEvent_0; }
	inline void set_startEvent_0(Il2CppObject * value)
	{
		___startEvent_0 = value;
		Il2CppCodeGenWriteBarrier(&___startEvent_0, value);
	}

	inline static int32_t get_offset_of_endEvent_1() { return static_cast<int32_t>(offsetof(SessionStateModule_t1404169380_StaticFields, ___endEvent_1)); }
	inline Il2CppObject * get_endEvent_1() const { return ___endEvent_1; }
	inline Il2CppObject ** get_address_of_endEvent_1() { return &___endEvent_1; }
	inline void set_endEvent_1(Il2CppObject * value)
	{
		___endEvent_1 = value;
		Il2CppCodeGenWriteBarrier(&___endEvent_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
