﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_I3108779577.h"

// System.ServiceModel.Channels.TcpReplyChannel
struct TcpReplyChannel_t2651191060;
// System.ServiceModel.Channels.Message
struct Message_t869514973;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.TcpReplyChannel/TcpRequestContext
struct  TcpRequestContext_t278260108  : public InternalRequestContext_t3108779577
{
public:
	// System.ServiceModel.Channels.TcpReplyChannel System.ServiceModel.Channels.TcpReplyChannel/TcpRequestContext::owner
	TcpReplyChannel_t2651191060 * ___owner_1;
	// System.ServiceModel.Channels.Message System.ServiceModel.Channels.TcpReplyChannel/TcpRequestContext::request
	Message_t869514973 * ___request_2;

public:
	inline static int32_t get_offset_of_owner_1() { return static_cast<int32_t>(offsetof(TcpRequestContext_t278260108, ___owner_1)); }
	inline TcpReplyChannel_t2651191060 * get_owner_1() const { return ___owner_1; }
	inline TcpReplyChannel_t2651191060 ** get_address_of_owner_1() { return &___owner_1; }
	inline void set_owner_1(TcpReplyChannel_t2651191060 * value)
	{
		___owner_1 = value;
		Il2CppCodeGenWriteBarrier(&___owner_1, value);
	}

	inline static int32_t get_offset_of_request_2() { return static_cast<int32_t>(offsetof(TcpRequestContext_t278260108, ___request_2)); }
	inline Message_t869514973 * get_request_2() const { return ___request_2; }
	inline Message_t869514973 ** get_address_of_request_2() { return &___request_2; }
	inline void set_request_2(Message_t869514973 * value)
	{
		___request_2 = value;
		Il2CppCodeGenWriteBarrier(&___request_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
