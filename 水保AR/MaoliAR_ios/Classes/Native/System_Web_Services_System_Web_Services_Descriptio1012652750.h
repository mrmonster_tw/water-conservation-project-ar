﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_Services_System_Web_Services_Descriptio2466402210.h"

// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2760654312;
// System.Web.Services.Description.Operation
struct Operation_t1010384202;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.OperationMessage
struct  OperationMessage_t1012652750  : public NamedItem_t2466402210
{
public:
	// System.Xml.XmlQualifiedName System.Web.Services.Description.OperationMessage::message
	XmlQualifiedName_t2760654312 * ___message_4;
	// System.Web.Services.Description.Operation System.Web.Services.Description.OperationMessage::operation
	Operation_t1010384202 * ___operation_5;

public:
	inline static int32_t get_offset_of_message_4() { return static_cast<int32_t>(offsetof(OperationMessage_t1012652750, ___message_4)); }
	inline XmlQualifiedName_t2760654312 * get_message_4() const { return ___message_4; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_message_4() { return &___message_4; }
	inline void set_message_4(XmlQualifiedName_t2760654312 * value)
	{
		___message_4 = value;
		Il2CppCodeGenWriteBarrier(&___message_4, value);
	}

	inline static int32_t get_offset_of_operation_5() { return static_cast<int32_t>(offsetof(OperationMessage_t1012652750, ___operation_5)); }
	inline Operation_t1010384202 * get_operation_5() const { return ___operation_5; }
	inline Operation_t1010384202 ** get_address_of_operation_5() { return &___operation_5; }
	inline void set_operation_5(Operation_t1010384202 * value)
	{
		___operation_5 = value;
		Il2CppCodeGenWriteBarrier(&___operation_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
