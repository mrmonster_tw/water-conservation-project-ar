﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Web_System_Web_Compilation_TextBlockType2260672758.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.TextBlock
struct  TextBlock_t3735829980  : public Il2CppObject
{
public:
	// System.String System.Web.Compilation.TextBlock::Content
	String_t* ___Content_0;
	// System.Web.Compilation.TextBlockType System.Web.Compilation.TextBlock::Type
	int32_t ___Type_1;
	// System.Int32 System.Web.Compilation.TextBlock::Length
	int32_t ___Length_2;

public:
	inline static int32_t get_offset_of_Content_0() { return static_cast<int32_t>(offsetof(TextBlock_t3735829980, ___Content_0)); }
	inline String_t* get_Content_0() const { return ___Content_0; }
	inline String_t** get_address_of_Content_0() { return &___Content_0; }
	inline void set_Content_0(String_t* value)
	{
		___Content_0 = value;
		Il2CppCodeGenWriteBarrier(&___Content_0, value);
	}

	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(TextBlock_t3735829980, ___Type_1)); }
	inline int32_t get_Type_1() const { return ___Type_1; }
	inline int32_t* get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(int32_t value)
	{
		___Type_1 = value;
	}

	inline static int32_t get_offset_of_Length_2() { return static_cast<int32_t>(offsetof(TextBlock_t3735829980, ___Length_2)); }
	inline int32_t get_Length_2() const { return ___Length_2; }
	inline int32_t* get_address_of_Length_2() { return &___Length_2; }
	inline void set_Length_2(int32_t value)
	{
		___Length_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
