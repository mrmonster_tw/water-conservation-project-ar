﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Xml_System_Xml_Serialization_SchemaTypes1741406581.h"

// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Xml.Serialization.TypeData
struct TypeData_t476999220;
// System.Xml.Schema.XmlSchemaPatternFacet
struct XmlSchemaPatternFacet_t3316004401;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.String[]
struct StringU5BU5D_t1281789340;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.TypeData
struct  TypeData_t476999220  : public Il2CppObject
{
public:
	// System.Type System.Xml.Serialization.TypeData::type
	Type_t * ___type_0;
	// System.String System.Xml.Serialization.TypeData::elementName
	String_t* ___elementName_1;
	// System.Xml.Serialization.SchemaTypes System.Xml.Serialization.TypeData::sType
	int32_t ___sType_2;
	// System.Type System.Xml.Serialization.TypeData::listItemType
	Type_t * ___listItemType_3;
	// System.String System.Xml.Serialization.TypeData::typeName
	String_t* ___typeName_4;
	// System.String System.Xml.Serialization.TypeData::fullTypeName
	String_t* ___fullTypeName_5;
	// System.String System.Xml.Serialization.TypeData::csharpName
	String_t* ___csharpName_6;
	// System.String System.Xml.Serialization.TypeData::csharpFullName
	String_t* ___csharpFullName_7;
	// System.Xml.Serialization.TypeData System.Xml.Serialization.TypeData::listItemTypeData
	TypeData_t476999220 * ___listItemTypeData_8;
	// System.Xml.Serialization.TypeData System.Xml.Serialization.TypeData::listTypeData
	TypeData_t476999220 * ___listTypeData_9;
	// System.Xml.Serialization.TypeData System.Xml.Serialization.TypeData::mappedType
	TypeData_t476999220 * ___mappedType_10;
	// System.Xml.Schema.XmlSchemaPatternFacet System.Xml.Serialization.TypeData::facet
	XmlSchemaPatternFacet_t3316004401 * ___facet_11;
	// System.Boolean System.Xml.Serialization.TypeData::hasPublicConstructor
	bool ___hasPublicConstructor_12;
	// System.Boolean System.Xml.Serialization.TypeData::nullableOverride
	bool ___nullableOverride_13;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(TypeData_t476999220, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier(&___type_0, value);
	}

	inline static int32_t get_offset_of_elementName_1() { return static_cast<int32_t>(offsetof(TypeData_t476999220, ___elementName_1)); }
	inline String_t* get_elementName_1() const { return ___elementName_1; }
	inline String_t** get_address_of_elementName_1() { return &___elementName_1; }
	inline void set_elementName_1(String_t* value)
	{
		___elementName_1 = value;
		Il2CppCodeGenWriteBarrier(&___elementName_1, value);
	}

	inline static int32_t get_offset_of_sType_2() { return static_cast<int32_t>(offsetof(TypeData_t476999220, ___sType_2)); }
	inline int32_t get_sType_2() const { return ___sType_2; }
	inline int32_t* get_address_of_sType_2() { return &___sType_2; }
	inline void set_sType_2(int32_t value)
	{
		___sType_2 = value;
	}

	inline static int32_t get_offset_of_listItemType_3() { return static_cast<int32_t>(offsetof(TypeData_t476999220, ___listItemType_3)); }
	inline Type_t * get_listItemType_3() const { return ___listItemType_3; }
	inline Type_t ** get_address_of_listItemType_3() { return &___listItemType_3; }
	inline void set_listItemType_3(Type_t * value)
	{
		___listItemType_3 = value;
		Il2CppCodeGenWriteBarrier(&___listItemType_3, value);
	}

	inline static int32_t get_offset_of_typeName_4() { return static_cast<int32_t>(offsetof(TypeData_t476999220, ___typeName_4)); }
	inline String_t* get_typeName_4() const { return ___typeName_4; }
	inline String_t** get_address_of_typeName_4() { return &___typeName_4; }
	inline void set_typeName_4(String_t* value)
	{
		___typeName_4 = value;
		Il2CppCodeGenWriteBarrier(&___typeName_4, value);
	}

	inline static int32_t get_offset_of_fullTypeName_5() { return static_cast<int32_t>(offsetof(TypeData_t476999220, ___fullTypeName_5)); }
	inline String_t* get_fullTypeName_5() const { return ___fullTypeName_5; }
	inline String_t** get_address_of_fullTypeName_5() { return &___fullTypeName_5; }
	inline void set_fullTypeName_5(String_t* value)
	{
		___fullTypeName_5 = value;
		Il2CppCodeGenWriteBarrier(&___fullTypeName_5, value);
	}

	inline static int32_t get_offset_of_csharpName_6() { return static_cast<int32_t>(offsetof(TypeData_t476999220, ___csharpName_6)); }
	inline String_t* get_csharpName_6() const { return ___csharpName_6; }
	inline String_t** get_address_of_csharpName_6() { return &___csharpName_6; }
	inline void set_csharpName_6(String_t* value)
	{
		___csharpName_6 = value;
		Il2CppCodeGenWriteBarrier(&___csharpName_6, value);
	}

	inline static int32_t get_offset_of_csharpFullName_7() { return static_cast<int32_t>(offsetof(TypeData_t476999220, ___csharpFullName_7)); }
	inline String_t* get_csharpFullName_7() const { return ___csharpFullName_7; }
	inline String_t** get_address_of_csharpFullName_7() { return &___csharpFullName_7; }
	inline void set_csharpFullName_7(String_t* value)
	{
		___csharpFullName_7 = value;
		Il2CppCodeGenWriteBarrier(&___csharpFullName_7, value);
	}

	inline static int32_t get_offset_of_listItemTypeData_8() { return static_cast<int32_t>(offsetof(TypeData_t476999220, ___listItemTypeData_8)); }
	inline TypeData_t476999220 * get_listItemTypeData_8() const { return ___listItemTypeData_8; }
	inline TypeData_t476999220 ** get_address_of_listItemTypeData_8() { return &___listItemTypeData_8; }
	inline void set_listItemTypeData_8(TypeData_t476999220 * value)
	{
		___listItemTypeData_8 = value;
		Il2CppCodeGenWriteBarrier(&___listItemTypeData_8, value);
	}

	inline static int32_t get_offset_of_listTypeData_9() { return static_cast<int32_t>(offsetof(TypeData_t476999220, ___listTypeData_9)); }
	inline TypeData_t476999220 * get_listTypeData_9() const { return ___listTypeData_9; }
	inline TypeData_t476999220 ** get_address_of_listTypeData_9() { return &___listTypeData_9; }
	inline void set_listTypeData_9(TypeData_t476999220 * value)
	{
		___listTypeData_9 = value;
		Il2CppCodeGenWriteBarrier(&___listTypeData_9, value);
	}

	inline static int32_t get_offset_of_mappedType_10() { return static_cast<int32_t>(offsetof(TypeData_t476999220, ___mappedType_10)); }
	inline TypeData_t476999220 * get_mappedType_10() const { return ___mappedType_10; }
	inline TypeData_t476999220 ** get_address_of_mappedType_10() { return &___mappedType_10; }
	inline void set_mappedType_10(TypeData_t476999220 * value)
	{
		___mappedType_10 = value;
		Il2CppCodeGenWriteBarrier(&___mappedType_10, value);
	}

	inline static int32_t get_offset_of_facet_11() { return static_cast<int32_t>(offsetof(TypeData_t476999220, ___facet_11)); }
	inline XmlSchemaPatternFacet_t3316004401 * get_facet_11() const { return ___facet_11; }
	inline XmlSchemaPatternFacet_t3316004401 ** get_address_of_facet_11() { return &___facet_11; }
	inline void set_facet_11(XmlSchemaPatternFacet_t3316004401 * value)
	{
		___facet_11 = value;
		Il2CppCodeGenWriteBarrier(&___facet_11, value);
	}

	inline static int32_t get_offset_of_hasPublicConstructor_12() { return static_cast<int32_t>(offsetof(TypeData_t476999220, ___hasPublicConstructor_12)); }
	inline bool get_hasPublicConstructor_12() const { return ___hasPublicConstructor_12; }
	inline bool* get_address_of_hasPublicConstructor_12() { return &___hasPublicConstructor_12; }
	inline void set_hasPublicConstructor_12(bool value)
	{
		___hasPublicConstructor_12 = value;
	}

	inline static int32_t get_offset_of_nullableOverride_13() { return static_cast<int32_t>(offsetof(TypeData_t476999220, ___nullableOverride_13)); }
	inline bool get_nullableOverride_13() const { return ___nullableOverride_13; }
	inline bool* get_address_of_nullableOverride_13() { return &___nullableOverride_13; }
	inline void set_nullableOverride_13(bool value)
	{
		___nullableOverride_13 = value;
	}
};

struct TypeData_t476999220_StaticFields
{
public:
	// System.Collections.Hashtable System.Xml.Serialization.TypeData::keywordsTable
	Hashtable_t1853889766 * ___keywordsTable_14;
	// System.String[] System.Xml.Serialization.TypeData::keywords
	StringU5BU5D_t1281789340* ___keywords_15;

public:
	inline static int32_t get_offset_of_keywordsTable_14() { return static_cast<int32_t>(offsetof(TypeData_t476999220_StaticFields, ___keywordsTable_14)); }
	inline Hashtable_t1853889766 * get_keywordsTable_14() const { return ___keywordsTable_14; }
	inline Hashtable_t1853889766 ** get_address_of_keywordsTable_14() { return &___keywordsTable_14; }
	inline void set_keywordsTable_14(Hashtable_t1853889766 * value)
	{
		___keywordsTable_14 = value;
		Il2CppCodeGenWriteBarrier(&___keywordsTable_14, value);
	}

	inline static int32_t get_offset_of_keywords_15() { return static_cast<int32_t>(offsetof(TypeData_t476999220_StaticFields, ___keywords_15)); }
	inline StringU5BU5D_t1281789340* get_keywords_15() const { return ___keywords_15; }
	inline StringU5BU5D_t1281789340** get_address_of_keywords_15() { return &___keywords_15; }
	inline void set_keywords_15(StringU5BU5D_t1281789340* value)
	{
		___keywords_15 = value;
		Il2CppCodeGenWriteBarrier(&___keywords_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
