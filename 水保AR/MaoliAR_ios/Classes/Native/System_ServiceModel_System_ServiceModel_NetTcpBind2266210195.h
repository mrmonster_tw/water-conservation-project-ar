﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Bi859993683.h"

// System.ServiceModel.NetTcpSecurity
struct NetTcpSecurity_t856315759;
// System.Xml.XmlDictionaryReaderQuotas
struct XmlDictionaryReaderQuotas_t173030297;
// System.ServiceModel.TransactionProtocol
struct TransactionProtocol_t3972232485;
// System.ServiceModel.Channels.TcpTransportBindingElement
struct TcpTransportBindingElement_t1965552937;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.NetTcpBinding
struct  NetTcpBinding_t2266210195  : public Binding_t859993683
{
public:
	// System.Int32 System.ServiceModel.NetTcpBinding::max_conn
	int32_t ___max_conn_6;
	// System.ServiceModel.NetTcpSecurity System.ServiceModel.NetTcpBinding::security
	NetTcpSecurity_t856315759 * ___security_7;
	// System.Xml.XmlDictionaryReaderQuotas System.ServiceModel.NetTcpBinding::reader_quotas
	XmlDictionaryReaderQuotas_t173030297 * ___reader_quotas_8;
	// System.Boolean System.ServiceModel.NetTcpBinding::transaction_flow
	bool ___transaction_flow_9;
	// System.ServiceModel.TransactionProtocol System.ServiceModel.NetTcpBinding::transaction_protocol
	TransactionProtocol_t3972232485 * ___transaction_protocol_10;
	// System.ServiceModel.Channels.TcpTransportBindingElement System.ServiceModel.NetTcpBinding::transport
	TcpTransportBindingElement_t1965552937 * ___transport_11;

public:
	inline static int32_t get_offset_of_max_conn_6() { return static_cast<int32_t>(offsetof(NetTcpBinding_t2266210195, ___max_conn_6)); }
	inline int32_t get_max_conn_6() const { return ___max_conn_6; }
	inline int32_t* get_address_of_max_conn_6() { return &___max_conn_6; }
	inline void set_max_conn_6(int32_t value)
	{
		___max_conn_6 = value;
	}

	inline static int32_t get_offset_of_security_7() { return static_cast<int32_t>(offsetof(NetTcpBinding_t2266210195, ___security_7)); }
	inline NetTcpSecurity_t856315759 * get_security_7() const { return ___security_7; }
	inline NetTcpSecurity_t856315759 ** get_address_of_security_7() { return &___security_7; }
	inline void set_security_7(NetTcpSecurity_t856315759 * value)
	{
		___security_7 = value;
		Il2CppCodeGenWriteBarrier(&___security_7, value);
	}

	inline static int32_t get_offset_of_reader_quotas_8() { return static_cast<int32_t>(offsetof(NetTcpBinding_t2266210195, ___reader_quotas_8)); }
	inline XmlDictionaryReaderQuotas_t173030297 * get_reader_quotas_8() const { return ___reader_quotas_8; }
	inline XmlDictionaryReaderQuotas_t173030297 ** get_address_of_reader_quotas_8() { return &___reader_quotas_8; }
	inline void set_reader_quotas_8(XmlDictionaryReaderQuotas_t173030297 * value)
	{
		___reader_quotas_8 = value;
		Il2CppCodeGenWriteBarrier(&___reader_quotas_8, value);
	}

	inline static int32_t get_offset_of_transaction_flow_9() { return static_cast<int32_t>(offsetof(NetTcpBinding_t2266210195, ___transaction_flow_9)); }
	inline bool get_transaction_flow_9() const { return ___transaction_flow_9; }
	inline bool* get_address_of_transaction_flow_9() { return &___transaction_flow_9; }
	inline void set_transaction_flow_9(bool value)
	{
		___transaction_flow_9 = value;
	}

	inline static int32_t get_offset_of_transaction_protocol_10() { return static_cast<int32_t>(offsetof(NetTcpBinding_t2266210195, ___transaction_protocol_10)); }
	inline TransactionProtocol_t3972232485 * get_transaction_protocol_10() const { return ___transaction_protocol_10; }
	inline TransactionProtocol_t3972232485 ** get_address_of_transaction_protocol_10() { return &___transaction_protocol_10; }
	inline void set_transaction_protocol_10(TransactionProtocol_t3972232485 * value)
	{
		___transaction_protocol_10 = value;
		Il2CppCodeGenWriteBarrier(&___transaction_protocol_10, value);
	}

	inline static int32_t get_offset_of_transport_11() { return static_cast<int32_t>(offsetof(NetTcpBinding_t2266210195, ___transport_11)); }
	inline TcpTransportBindingElement_t1965552937 * get_transport_11() const { return ___transport_11; }
	inline TcpTransportBindingElement_t1965552937 ** get_address_of_transport_11() { return &___transport_11; }
	inline void set_transport_11(TcpTransportBindingElement_t1965552937 * value)
	{
		___transport_11 = value;
		Il2CppCodeGenWriteBarrier(&___transport_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
