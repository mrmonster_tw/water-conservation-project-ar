﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_Collections_Generic_Syn2781623994.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.ClientOperation/ClientOperationCollection
struct  ClientOperationCollection_t538801239  : public SynchronizedKeyedCollection_2_t2781623994
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
