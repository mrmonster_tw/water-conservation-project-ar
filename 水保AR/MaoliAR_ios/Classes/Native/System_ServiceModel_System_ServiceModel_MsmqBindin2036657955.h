﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Bi859993683.h"
#include "System_ServiceModel_System_ServiceModel_DeadLetter1799023886.h"
#include "System_ServiceModel_System_ServiceModel_ReceiveErr2226770027.h"
#include "mscorlib_System_TimeSpan881159249.h"

// System.Uri
struct Uri_t100236324;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.MsmqBindingBase
struct  MsmqBindingBase_t2036657955  : public Binding_t859993683
{
public:
	// System.Uri System.ServiceModel.MsmqBindingBase::custom_dead_letter_queue
	Uri_t100236324 * ___custom_dead_letter_queue_6;
	// System.ServiceModel.DeadLetterQueue System.ServiceModel.MsmqBindingBase::dead_letter_queue
	int32_t ___dead_letter_queue_7;
	// System.Boolean System.ServiceModel.MsmqBindingBase::durable
	bool ___durable_8;
	// System.Boolean System.ServiceModel.MsmqBindingBase::exactly_once
	bool ___exactly_once_9;
	// System.Boolean System.ServiceModel.MsmqBindingBase::use_msmq_trace
	bool ___use_msmq_trace_10;
	// System.Boolean System.ServiceModel.MsmqBindingBase::use_source_journal
	bool ___use_source_journal_11;
	// System.Int32 System.ServiceModel.MsmqBindingBase::max_retry_cycles
	int32_t ___max_retry_cycles_12;
	// System.Int32 System.ServiceModel.MsmqBindingBase::receive_retry_count
	int32_t ___receive_retry_count_13;
	// System.Int64 System.ServiceModel.MsmqBindingBase::max_recv_msg_size
	int64_t ___max_recv_msg_size_14;
	// System.ServiceModel.ReceiveErrorHandling System.ServiceModel.MsmqBindingBase::receive_error_handling
	int32_t ___receive_error_handling_15;
	// System.TimeSpan System.ServiceModel.MsmqBindingBase::retry_cycle_delay
	TimeSpan_t881159249  ___retry_cycle_delay_16;
	// System.TimeSpan System.ServiceModel.MsmqBindingBase::ttl
	TimeSpan_t881159249  ___ttl_17;

public:
	inline static int32_t get_offset_of_custom_dead_letter_queue_6() { return static_cast<int32_t>(offsetof(MsmqBindingBase_t2036657955, ___custom_dead_letter_queue_6)); }
	inline Uri_t100236324 * get_custom_dead_letter_queue_6() const { return ___custom_dead_letter_queue_6; }
	inline Uri_t100236324 ** get_address_of_custom_dead_letter_queue_6() { return &___custom_dead_letter_queue_6; }
	inline void set_custom_dead_letter_queue_6(Uri_t100236324 * value)
	{
		___custom_dead_letter_queue_6 = value;
		Il2CppCodeGenWriteBarrier(&___custom_dead_letter_queue_6, value);
	}

	inline static int32_t get_offset_of_dead_letter_queue_7() { return static_cast<int32_t>(offsetof(MsmqBindingBase_t2036657955, ___dead_letter_queue_7)); }
	inline int32_t get_dead_letter_queue_7() const { return ___dead_letter_queue_7; }
	inline int32_t* get_address_of_dead_letter_queue_7() { return &___dead_letter_queue_7; }
	inline void set_dead_letter_queue_7(int32_t value)
	{
		___dead_letter_queue_7 = value;
	}

	inline static int32_t get_offset_of_durable_8() { return static_cast<int32_t>(offsetof(MsmqBindingBase_t2036657955, ___durable_8)); }
	inline bool get_durable_8() const { return ___durable_8; }
	inline bool* get_address_of_durable_8() { return &___durable_8; }
	inline void set_durable_8(bool value)
	{
		___durable_8 = value;
	}

	inline static int32_t get_offset_of_exactly_once_9() { return static_cast<int32_t>(offsetof(MsmqBindingBase_t2036657955, ___exactly_once_9)); }
	inline bool get_exactly_once_9() const { return ___exactly_once_9; }
	inline bool* get_address_of_exactly_once_9() { return &___exactly_once_9; }
	inline void set_exactly_once_9(bool value)
	{
		___exactly_once_9 = value;
	}

	inline static int32_t get_offset_of_use_msmq_trace_10() { return static_cast<int32_t>(offsetof(MsmqBindingBase_t2036657955, ___use_msmq_trace_10)); }
	inline bool get_use_msmq_trace_10() const { return ___use_msmq_trace_10; }
	inline bool* get_address_of_use_msmq_trace_10() { return &___use_msmq_trace_10; }
	inline void set_use_msmq_trace_10(bool value)
	{
		___use_msmq_trace_10 = value;
	}

	inline static int32_t get_offset_of_use_source_journal_11() { return static_cast<int32_t>(offsetof(MsmqBindingBase_t2036657955, ___use_source_journal_11)); }
	inline bool get_use_source_journal_11() const { return ___use_source_journal_11; }
	inline bool* get_address_of_use_source_journal_11() { return &___use_source_journal_11; }
	inline void set_use_source_journal_11(bool value)
	{
		___use_source_journal_11 = value;
	}

	inline static int32_t get_offset_of_max_retry_cycles_12() { return static_cast<int32_t>(offsetof(MsmqBindingBase_t2036657955, ___max_retry_cycles_12)); }
	inline int32_t get_max_retry_cycles_12() const { return ___max_retry_cycles_12; }
	inline int32_t* get_address_of_max_retry_cycles_12() { return &___max_retry_cycles_12; }
	inline void set_max_retry_cycles_12(int32_t value)
	{
		___max_retry_cycles_12 = value;
	}

	inline static int32_t get_offset_of_receive_retry_count_13() { return static_cast<int32_t>(offsetof(MsmqBindingBase_t2036657955, ___receive_retry_count_13)); }
	inline int32_t get_receive_retry_count_13() const { return ___receive_retry_count_13; }
	inline int32_t* get_address_of_receive_retry_count_13() { return &___receive_retry_count_13; }
	inline void set_receive_retry_count_13(int32_t value)
	{
		___receive_retry_count_13 = value;
	}

	inline static int32_t get_offset_of_max_recv_msg_size_14() { return static_cast<int32_t>(offsetof(MsmqBindingBase_t2036657955, ___max_recv_msg_size_14)); }
	inline int64_t get_max_recv_msg_size_14() const { return ___max_recv_msg_size_14; }
	inline int64_t* get_address_of_max_recv_msg_size_14() { return &___max_recv_msg_size_14; }
	inline void set_max_recv_msg_size_14(int64_t value)
	{
		___max_recv_msg_size_14 = value;
	}

	inline static int32_t get_offset_of_receive_error_handling_15() { return static_cast<int32_t>(offsetof(MsmqBindingBase_t2036657955, ___receive_error_handling_15)); }
	inline int32_t get_receive_error_handling_15() const { return ___receive_error_handling_15; }
	inline int32_t* get_address_of_receive_error_handling_15() { return &___receive_error_handling_15; }
	inline void set_receive_error_handling_15(int32_t value)
	{
		___receive_error_handling_15 = value;
	}

	inline static int32_t get_offset_of_retry_cycle_delay_16() { return static_cast<int32_t>(offsetof(MsmqBindingBase_t2036657955, ___retry_cycle_delay_16)); }
	inline TimeSpan_t881159249  get_retry_cycle_delay_16() const { return ___retry_cycle_delay_16; }
	inline TimeSpan_t881159249 * get_address_of_retry_cycle_delay_16() { return &___retry_cycle_delay_16; }
	inline void set_retry_cycle_delay_16(TimeSpan_t881159249  value)
	{
		___retry_cycle_delay_16 = value;
	}

	inline static int32_t get_offset_of_ttl_17() { return static_cast<int32_t>(offsetof(MsmqBindingBase_t2036657955, ___ttl_17)); }
	inline TimeSpan_t881159249  get_ttl_17() const { return ___ttl_17; }
	inline TimeSpan_t881159249 * get_address_of_ttl_17() { return &___ttl_17; }
	inline void set_ttl_17(TimeSpan_t881159249  value)
	{
		___ttl_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
