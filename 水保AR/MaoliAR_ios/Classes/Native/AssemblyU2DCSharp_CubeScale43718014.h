﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Touch1921856868.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CubeScale
struct  CubeScale_t43718014  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Touch CubeScale::oldTouch1
	Touch_t1921856868  ___oldTouch1_2;
	// UnityEngine.Touch CubeScale::oldTouch2
	Touch_t1921856868  ___oldTouch2_3;

public:
	inline static int32_t get_offset_of_oldTouch1_2() { return static_cast<int32_t>(offsetof(CubeScale_t43718014, ___oldTouch1_2)); }
	inline Touch_t1921856868  get_oldTouch1_2() const { return ___oldTouch1_2; }
	inline Touch_t1921856868 * get_address_of_oldTouch1_2() { return &___oldTouch1_2; }
	inline void set_oldTouch1_2(Touch_t1921856868  value)
	{
		___oldTouch1_2 = value;
	}

	inline static int32_t get_offset_of_oldTouch2_3() { return static_cast<int32_t>(offsetof(CubeScale_t43718014, ___oldTouch2_3)); }
	inline Touch_t1921856868  get_oldTouch2_3() const { return ___oldTouch2_3; }
	inline Touch_t1921856868 * get_address_of_oldTouch2_3() { return &___oldTouch2_3; }
	inline void set_oldTouch2_3(Touch_t1921856868  value)
	{
		___oldTouch2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
