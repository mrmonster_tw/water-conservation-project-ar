﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Dispatcher2436929862.h"
#include "mscorlib_System_Guid3193532887.h"

// System.ServiceModel.ServiceHostBase
struct ServiceHostBase_t3741910535;
// System.String
struct String_t;
// System.Collections.ObjectModel.Collection`1<System.ServiceModel.Dispatcher.IErrorHandler>
struct Collection_1_t2895781704;
// System.ServiceModel.Channels.IChannelListener
struct IChannelListener_t114797722;
// System.ServiceModel.IDefaultCommunicationTimeouts
struct IDefaultCommunicationTimeouts_t2848643016;
// System.ServiceModel.Channels.MessageVersion
struct MessageVersion_t1958933598;
// System.Collections.Generic.SynchronizedCollection`1<System.ServiceModel.Dispatcher.IChannelInitializer>
struct SynchronizedCollection_1_t3028954626;
// System.ServiceModel.Dispatcher.ServiceThrottle
struct ServiceThrottle_t3841213848;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t451242010;
// System.ServiceModel.Dispatcher.ListenerLoopManager
struct ListenerLoopManager_t4138186672;
// System.Collections.Generic.SynchronizedCollection`1<System.ServiceModel.Dispatcher.EndpointDispatcher>
struct SynchronizedCollection_1_t3023046804;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.ChannelDispatcher
struct  ChannelDispatcher_t2781106991  : public ChannelDispatcherBase_t2436929862
{
public:
	// System.ServiceModel.ServiceHostBase System.ServiceModel.Dispatcher.ChannelDispatcher::host
	ServiceHostBase_t3741910535 * ___host_9;
	// System.String System.ServiceModel.Dispatcher.ChannelDispatcher::binding_name
	String_t* ___binding_name_10;
	// System.Collections.ObjectModel.Collection`1<System.ServiceModel.Dispatcher.IErrorHandler> System.ServiceModel.Dispatcher.ChannelDispatcher::error_handlers
	Collection_1_t2895781704 * ___error_handlers_11;
	// System.ServiceModel.Channels.IChannelListener System.ServiceModel.Dispatcher.ChannelDispatcher::listener
	Il2CppObject * ___listener_12;
	// System.ServiceModel.IDefaultCommunicationTimeouts System.ServiceModel.Dispatcher.ChannelDispatcher::timeouts
	Il2CppObject * ___timeouts_13;
	// System.ServiceModel.Channels.MessageVersion System.ServiceModel.Dispatcher.ChannelDispatcher::message_version
	MessageVersion_t1958933598 * ___message_version_14;
	// System.Boolean System.ServiceModel.Dispatcher.ChannelDispatcher::receive_sync
	bool ___receive_sync_15;
	// System.Boolean System.ServiceModel.Dispatcher.ChannelDispatcher::include_exception_detail_in_faults
	bool ___include_exception_detail_in_faults_16;
	// System.Collections.Generic.SynchronizedCollection`1<System.ServiceModel.Dispatcher.IChannelInitializer> System.ServiceModel.Dispatcher.ChannelDispatcher::initializers
	SynchronizedCollection_1_t3028954626 * ___initializers_17;
	// System.ServiceModel.Dispatcher.ServiceThrottle System.ServiceModel.Dispatcher.ChannelDispatcher::throttle
	ServiceThrottle_t3841213848 * ___throttle_18;
	// System.Guid System.ServiceModel.Dispatcher.ChannelDispatcher::identifier
	Guid_t  ___identifier_19;
	// System.Threading.ManualResetEvent System.ServiceModel.Dispatcher.ChannelDispatcher::async_event
	ManualResetEvent_t451242010 * ___async_event_20;
	// System.ServiceModel.Dispatcher.ListenerLoopManager System.ServiceModel.Dispatcher.ChannelDispatcher::loop_manager
	ListenerLoopManager_t4138186672 * ___loop_manager_21;
	// System.Collections.Generic.SynchronizedCollection`1<System.ServiceModel.Dispatcher.EndpointDispatcher> System.ServiceModel.Dispatcher.ChannelDispatcher::endpoints
	SynchronizedCollection_1_t3023046804 * ___endpoints_22;

public:
	inline static int32_t get_offset_of_host_9() { return static_cast<int32_t>(offsetof(ChannelDispatcher_t2781106991, ___host_9)); }
	inline ServiceHostBase_t3741910535 * get_host_9() const { return ___host_9; }
	inline ServiceHostBase_t3741910535 ** get_address_of_host_9() { return &___host_9; }
	inline void set_host_9(ServiceHostBase_t3741910535 * value)
	{
		___host_9 = value;
		Il2CppCodeGenWriteBarrier(&___host_9, value);
	}

	inline static int32_t get_offset_of_binding_name_10() { return static_cast<int32_t>(offsetof(ChannelDispatcher_t2781106991, ___binding_name_10)); }
	inline String_t* get_binding_name_10() const { return ___binding_name_10; }
	inline String_t** get_address_of_binding_name_10() { return &___binding_name_10; }
	inline void set_binding_name_10(String_t* value)
	{
		___binding_name_10 = value;
		Il2CppCodeGenWriteBarrier(&___binding_name_10, value);
	}

	inline static int32_t get_offset_of_error_handlers_11() { return static_cast<int32_t>(offsetof(ChannelDispatcher_t2781106991, ___error_handlers_11)); }
	inline Collection_1_t2895781704 * get_error_handlers_11() const { return ___error_handlers_11; }
	inline Collection_1_t2895781704 ** get_address_of_error_handlers_11() { return &___error_handlers_11; }
	inline void set_error_handlers_11(Collection_1_t2895781704 * value)
	{
		___error_handlers_11 = value;
		Il2CppCodeGenWriteBarrier(&___error_handlers_11, value);
	}

	inline static int32_t get_offset_of_listener_12() { return static_cast<int32_t>(offsetof(ChannelDispatcher_t2781106991, ___listener_12)); }
	inline Il2CppObject * get_listener_12() const { return ___listener_12; }
	inline Il2CppObject ** get_address_of_listener_12() { return &___listener_12; }
	inline void set_listener_12(Il2CppObject * value)
	{
		___listener_12 = value;
		Il2CppCodeGenWriteBarrier(&___listener_12, value);
	}

	inline static int32_t get_offset_of_timeouts_13() { return static_cast<int32_t>(offsetof(ChannelDispatcher_t2781106991, ___timeouts_13)); }
	inline Il2CppObject * get_timeouts_13() const { return ___timeouts_13; }
	inline Il2CppObject ** get_address_of_timeouts_13() { return &___timeouts_13; }
	inline void set_timeouts_13(Il2CppObject * value)
	{
		___timeouts_13 = value;
		Il2CppCodeGenWriteBarrier(&___timeouts_13, value);
	}

	inline static int32_t get_offset_of_message_version_14() { return static_cast<int32_t>(offsetof(ChannelDispatcher_t2781106991, ___message_version_14)); }
	inline MessageVersion_t1958933598 * get_message_version_14() const { return ___message_version_14; }
	inline MessageVersion_t1958933598 ** get_address_of_message_version_14() { return &___message_version_14; }
	inline void set_message_version_14(MessageVersion_t1958933598 * value)
	{
		___message_version_14 = value;
		Il2CppCodeGenWriteBarrier(&___message_version_14, value);
	}

	inline static int32_t get_offset_of_receive_sync_15() { return static_cast<int32_t>(offsetof(ChannelDispatcher_t2781106991, ___receive_sync_15)); }
	inline bool get_receive_sync_15() const { return ___receive_sync_15; }
	inline bool* get_address_of_receive_sync_15() { return &___receive_sync_15; }
	inline void set_receive_sync_15(bool value)
	{
		___receive_sync_15 = value;
	}

	inline static int32_t get_offset_of_include_exception_detail_in_faults_16() { return static_cast<int32_t>(offsetof(ChannelDispatcher_t2781106991, ___include_exception_detail_in_faults_16)); }
	inline bool get_include_exception_detail_in_faults_16() const { return ___include_exception_detail_in_faults_16; }
	inline bool* get_address_of_include_exception_detail_in_faults_16() { return &___include_exception_detail_in_faults_16; }
	inline void set_include_exception_detail_in_faults_16(bool value)
	{
		___include_exception_detail_in_faults_16 = value;
	}

	inline static int32_t get_offset_of_initializers_17() { return static_cast<int32_t>(offsetof(ChannelDispatcher_t2781106991, ___initializers_17)); }
	inline SynchronizedCollection_1_t3028954626 * get_initializers_17() const { return ___initializers_17; }
	inline SynchronizedCollection_1_t3028954626 ** get_address_of_initializers_17() { return &___initializers_17; }
	inline void set_initializers_17(SynchronizedCollection_1_t3028954626 * value)
	{
		___initializers_17 = value;
		Il2CppCodeGenWriteBarrier(&___initializers_17, value);
	}

	inline static int32_t get_offset_of_throttle_18() { return static_cast<int32_t>(offsetof(ChannelDispatcher_t2781106991, ___throttle_18)); }
	inline ServiceThrottle_t3841213848 * get_throttle_18() const { return ___throttle_18; }
	inline ServiceThrottle_t3841213848 ** get_address_of_throttle_18() { return &___throttle_18; }
	inline void set_throttle_18(ServiceThrottle_t3841213848 * value)
	{
		___throttle_18 = value;
		Il2CppCodeGenWriteBarrier(&___throttle_18, value);
	}

	inline static int32_t get_offset_of_identifier_19() { return static_cast<int32_t>(offsetof(ChannelDispatcher_t2781106991, ___identifier_19)); }
	inline Guid_t  get_identifier_19() const { return ___identifier_19; }
	inline Guid_t * get_address_of_identifier_19() { return &___identifier_19; }
	inline void set_identifier_19(Guid_t  value)
	{
		___identifier_19 = value;
	}

	inline static int32_t get_offset_of_async_event_20() { return static_cast<int32_t>(offsetof(ChannelDispatcher_t2781106991, ___async_event_20)); }
	inline ManualResetEvent_t451242010 * get_async_event_20() const { return ___async_event_20; }
	inline ManualResetEvent_t451242010 ** get_address_of_async_event_20() { return &___async_event_20; }
	inline void set_async_event_20(ManualResetEvent_t451242010 * value)
	{
		___async_event_20 = value;
		Il2CppCodeGenWriteBarrier(&___async_event_20, value);
	}

	inline static int32_t get_offset_of_loop_manager_21() { return static_cast<int32_t>(offsetof(ChannelDispatcher_t2781106991, ___loop_manager_21)); }
	inline ListenerLoopManager_t4138186672 * get_loop_manager_21() const { return ___loop_manager_21; }
	inline ListenerLoopManager_t4138186672 ** get_address_of_loop_manager_21() { return &___loop_manager_21; }
	inline void set_loop_manager_21(ListenerLoopManager_t4138186672 * value)
	{
		___loop_manager_21 = value;
		Il2CppCodeGenWriteBarrier(&___loop_manager_21, value);
	}

	inline static int32_t get_offset_of_endpoints_22() { return static_cast<int32_t>(offsetof(ChannelDispatcher_t2781106991, ___endpoints_22)); }
	inline SynchronizedCollection_1_t3023046804 * get_endpoints_22() const { return ___endpoints_22; }
	inline SynchronizedCollection_1_t3023046804 ** get_address_of_endpoints_22() { return &___endpoints_22; }
	inline void set_endpoints_22(SynchronizedCollection_1_t3023046804 * value)
	{
		___endpoints_22 = value;
		Il2CppCodeGenWriteBarrier(&___endpoints_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
