﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_TimeSpan881159249.h"
#include "System_ServiceModel_System_ServiceModel_Security_U1078652501.h"

// System.Web.Security.MembershipProvider
struct MembershipProvider_t1994512972;
// System.IdentityModel.Selectors.UserNamePasswordValidator
struct UserNamePasswordValidator_t4240724900;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.UserNamePasswordServiceCredential
struct  UserNamePasswordServiceCredential_t3640294368  : public Il2CppObject
{
public:
	// System.Web.Security.MembershipProvider System.ServiceModel.Security.UserNamePasswordServiceCredential::provider
	MembershipProvider_t1994512972 * ___provider_0;
	// System.TimeSpan System.ServiceModel.Security.UserNamePasswordServiceCredential::lifetime
	TimeSpan_t881159249  ___lifetime_1;
	// System.Boolean System.ServiceModel.Security.UserNamePasswordServiceCredential::include_win_groups
	bool ___include_win_groups_2;
	// System.Int32 System.ServiceModel.Security.UserNamePasswordServiceCredential::max_cache_tokens
	int32_t ___max_cache_tokens_3;
	// System.IdentityModel.Selectors.UserNamePasswordValidator System.ServiceModel.Security.UserNamePasswordServiceCredential::validator
	UserNamePasswordValidator_t4240724900 * ___validator_4;
	// System.ServiceModel.Security.UserNamePasswordValidationMode System.ServiceModel.Security.UserNamePasswordServiceCredential::mode
	int32_t ___mode_5;

public:
	inline static int32_t get_offset_of_provider_0() { return static_cast<int32_t>(offsetof(UserNamePasswordServiceCredential_t3640294368, ___provider_0)); }
	inline MembershipProvider_t1994512972 * get_provider_0() const { return ___provider_0; }
	inline MembershipProvider_t1994512972 ** get_address_of_provider_0() { return &___provider_0; }
	inline void set_provider_0(MembershipProvider_t1994512972 * value)
	{
		___provider_0 = value;
		Il2CppCodeGenWriteBarrier(&___provider_0, value);
	}

	inline static int32_t get_offset_of_lifetime_1() { return static_cast<int32_t>(offsetof(UserNamePasswordServiceCredential_t3640294368, ___lifetime_1)); }
	inline TimeSpan_t881159249  get_lifetime_1() const { return ___lifetime_1; }
	inline TimeSpan_t881159249 * get_address_of_lifetime_1() { return &___lifetime_1; }
	inline void set_lifetime_1(TimeSpan_t881159249  value)
	{
		___lifetime_1 = value;
	}

	inline static int32_t get_offset_of_include_win_groups_2() { return static_cast<int32_t>(offsetof(UserNamePasswordServiceCredential_t3640294368, ___include_win_groups_2)); }
	inline bool get_include_win_groups_2() const { return ___include_win_groups_2; }
	inline bool* get_address_of_include_win_groups_2() { return &___include_win_groups_2; }
	inline void set_include_win_groups_2(bool value)
	{
		___include_win_groups_2 = value;
	}

	inline static int32_t get_offset_of_max_cache_tokens_3() { return static_cast<int32_t>(offsetof(UserNamePasswordServiceCredential_t3640294368, ___max_cache_tokens_3)); }
	inline int32_t get_max_cache_tokens_3() const { return ___max_cache_tokens_3; }
	inline int32_t* get_address_of_max_cache_tokens_3() { return &___max_cache_tokens_3; }
	inline void set_max_cache_tokens_3(int32_t value)
	{
		___max_cache_tokens_3 = value;
	}

	inline static int32_t get_offset_of_validator_4() { return static_cast<int32_t>(offsetof(UserNamePasswordServiceCredential_t3640294368, ___validator_4)); }
	inline UserNamePasswordValidator_t4240724900 * get_validator_4() const { return ___validator_4; }
	inline UserNamePasswordValidator_t4240724900 ** get_address_of_validator_4() { return &___validator_4; }
	inline void set_validator_4(UserNamePasswordValidator_t4240724900 * value)
	{
		___validator_4 = value;
		Il2CppCodeGenWriteBarrier(&___validator_4, value);
	}

	inline static int32_t get_offset_of_mode_5() { return static_cast<int32_t>(offsetof(UserNamePasswordServiceCredential_t3640294368, ___mode_5)); }
	inline int32_t get_mode_5() const { return ___mode_5; }
	inline int32_t* get_address_of_mode_5() { return &___mode_5; }
	inline void set_mode_5(int32_t value)
	{
		___mode_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
