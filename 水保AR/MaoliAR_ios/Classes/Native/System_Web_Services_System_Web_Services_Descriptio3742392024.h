﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Xml.Serialization.XmlSerializer
struct XmlSerializer_t1117804635;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.ExtensionInfo
struct  ExtensionInfo_t3742392024  : public Il2CppObject
{
public:
	// System.Collections.ArrayList System.Web.Services.Description.ExtensionInfo::_namespaceDeclarations
	ArrayList_t2718874744 * ____namespaceDeclarations_0;
	// System.String System.Web.Services.Description.ExtensionInfo::_namespace
	String_t* ____namespace_1;
	// System.String System.Web.Services.Description.ExtensionInfo::_elementName
	String_t* ____elementName_2;
	// System.Type System.Web.Services.Description.ExtensionInfo::_type
	Type_t * ____type_3;
	// System.Xml.Serialization.XmlSerializer System.Web.Services.Description.ExtensionInfo::_serializer
	XmlSerializer_t1117804635 * ____serializer_4;

public:
	inline static int32_t get_offset_of__namespaceDeclarations_0() { return static_cast<int32_t>(offsetof(ExtensionInfo_t3742392024, ____namespaceDeclarations_0)); }
	inline ArrayList_t2718874744 * get__namespaceDeclarations_0() const { return ____namespaceDeclarations_0; }
	inline ArrayList_t2718874744 ** get_address_of__namespaceDeclarations_0() { return &____namespaceDeclarations_0; }
	inline void set__namespaceDeclarations_0(ArrayList_t2718874744 * value)
	{
		____namespaceDeclarations_0 = value;
		Il2CppCodeGenWriteBarrier(&____namespaceDeclarations_0, value);
	}

	inline static int32_t get_offset_of__namespace_1() { return static_cast<int32_t>(offsetof(ExtensionInfo_t3742392024, ____namespace_1)); }
	inline String_t* get__namespace_1() const { return ____namespace_1; }
	inline String_t** get_address_of__namespace_1() { return &____namespace_1; }
	inline void set__namespace_1(String_t* value)
	{
		____namespace_1 = value;
		Il2CppCodeGenWriteBarrier(&____namespace_1, value);
	}

	inline static int32_t get_offset_of__elementName_2() { return static_cast<int32_t>(offsetof(ExtensionInfo_t3742392024, ____elementName_2)); }
	inline String_t* get__elementName_2() const { return ____elementName_2; }
	inline String_t** get_address_of__elementName_2() { return &____elementName_2; }
	inline void set__elementName_2(String_t* value)
	{
		____elementName_2 = value;
		Il2CppCodeGenWriteBarrier(&____elementName_2, value);
	}

	inline static int32_t get_offset_of__type_3() { return static_cast<int32_t>(offsetof(ExtensionInfo_t3742392024, ____type_3)); }
	inline Type_t * get__type_3() const { return ____type_3; }
	inline Type_t ** get_address_of__type_3() { return &____type_3; }
	inline void set__type_3(Type_t * value)
	{
		____type_3 = value;
		Il2CppCodeGenWriteBarrier(&____type_3, value);
	}

	inline static int32_t get_offset_of__serializer_4() { return static_cast<int32_t>(offsetof(ExtensionInfo_t3742392024, ____serializer_4)); }
	inline XmlSerializer_t1117804635 * get__serializer_4() const { return ____serializer_4; }
	inline XmlSerializer_t1117804635 ** get_address_of__serializer_4() { return &____serializer_4; }
	inline void set__serializer_4(XmlSerializer_t1117804635 * value)
	{
		____serializer_4 = value;
		Il2CppCodeGenWriteBarrier(&____serializer_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
