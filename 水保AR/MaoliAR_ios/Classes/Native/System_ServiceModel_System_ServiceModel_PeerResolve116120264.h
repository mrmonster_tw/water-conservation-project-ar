﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.PeerResolvers.RegisterResponseInfoDC
struct RegisterResponseInfoDC_t1503758284;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.PeerResolvers.RegisterResponseInfo
struct  RegisterResponseInfo_t116120264  : public Il2CppObject
{
public:
	// System.ServiceModel.PeerResolvers.RegisterResponseInfoDC System.ServiceModel.PeerResolvers.RegisterResponseInfo::body
	RegisterResponseInfoDC_t1503758284 * ___body_0;

public:
	inline static int32_t get_offset_of_body_0() { return static_cast<int32_t>(offsetof(RegisterResponseInfo_t116120264, ___body_0)); }
	inline RegisterResponseInfoDC_t1503758284 * get_body_0() const { return ___body_0; }
	inline RegisterResponseInfoDC_t1503758284 ** get_address_of_body_0() { return &___body_0; }
	inline void set_body_0(RegisterResponseInfoDC_t1503758284 * value)
	{
		___body_0 = value;
		Il2CppCodeGenWriteBarrier(&___body_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
