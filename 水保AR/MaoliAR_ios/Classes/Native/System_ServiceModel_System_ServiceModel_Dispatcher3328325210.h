﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Description.OperationDescription
struct OperationDescription_t1389309725;
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t390618515;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.DefaultOperationInvoker
struct  DefaultOperationInvoker_t3328325210  : public Il2CppObject
{
public:
	// System.ServiceModel.Description.OperationDescription System.ServiceModel.Dispatcher.DefaultOperationInvoker::od
	OperationDescription_t1389309725 * ___od_0;
	// System.Reflection.ParameterInfo[] System.ServiceModel.Dispatcher.DefaultOperationInvoker::in_params
	ParameterInfoU5BU5D_t390618515* ___in_params_1;
	// System.Reflection.ParameterInfo[] System.ServiceModel.Dispatcher.DefaultOperationInvoker::out_params
	ParameterInfoU5BU5D_t390618515* ___out_params_2;
	// System.Boolean System.ServiceModel.Dispatcher.DefaultOperationInvoker::is_synchronous
	bool ___is_synchronous_3;

public:
	inline static int32_t get_offset_of_od_0() { return static_cast<int32_t>(offsetof(DefaultOperationInvoker_t3328325210, ___od_0)); }
	inline OperationDescription_t1389309725 * get_od_0() const { return ___od_0; }
	inline OperationDescription_t1389309725 ** get_address_of_od_0() { return &___od_0; }
	inline void set_od_0(OperationDescription_t1389309725 * value)
	{
		___od_0 = value;
		Il2CppCodeGenWriteBarrier(&___od_0, value);
	}

	inline static int32_t get_offset_of_in_params_1() { return static_cast<int32_t>(offsetof(DefaultOperationInvoker_t3328325210, ___in_params_1)); }
	inline ParameterInfoU5BU5D_t390618515* get_in_params_1() const { return ___in_params_1; }
	inline ParameterInfoU5BU5D_t390618515** get_address_of_in_params_1() { return &___in_params_1; }
	inline void set_in_params_1(ParameterInfoU5BU5D_t390618515* value)
	{
		___in_params_1 = value;
		Il2CppCodeGenWriteBarrier(&___in_params_1, value);
	}

	inline static int32_t get_offset_of_out_params_2() { return static_cast<int32_t>(offsetof(DefaultOperationInvoker_t3328325210, ___out_params_2)); }
	inline ParameterInfoU5BU5D_t390618515* get_out_params_2() const { return ___out_params_2; }
	inline ParameterInfoU5BU5D_t390618515** get_address_of_out_params_2() { return &___out_params_2; }
	inline void set_out_params_2(ParameterInfoU5BU5D_t390618515* value)
	{
		___out_params_2 = value;
		Il2CppCodeGenWriteBarrier(&___out_params_2, value);
	}

	inline static int32_t get_offset_of_is_synchronous_3() { return static_cast<int32_t>(offsetof(DefaultOperationInvoker_t3328325210, ___is_synchronous_3)); }
	inline bool get_is_synchronous_3() const { return ___is_synchronous_3; }
	inline bool* get_address_of_is_synchronous_3() { return &___is_synchronous_3; }
	inline void set_is_synchronous_3(bool value)
	{
		___is_synchronous_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
