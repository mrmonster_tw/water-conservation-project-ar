﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Runtime_Serialization_System_Xml_XmlDictiona238028028.h"

// System.Xml.XmlDictionary/EmptyDictionary
struct EmptyDictionary_t1467978489;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDictionary/EmptyDictionary
struct  EmptyDictionary_t1467978489  : public XmlDictionary_t238028028
{
public:

public:
};

struct EmptyDictionary_t1467978489_StaticFields
{
public:
	// System.Xml.XmlDictionary/EmptyDictionary System.Xml.XmlDictionary/EmptyDictionary::Instance
	EmptyDictionary_t1467978489 * ___Instance_4;

public:
	inline static int32_t get_offset_of_Instance_4() { return static_cast<int32_t>(offsetof(EmptyDictionary_t1467978489_StaticFields, ___Instance_4)); }
	inline EmptyDictionary_t1467978489 * get_Instance_4() const { return ___Instance_4; }
	inline EmptyDictionary_t1467978489 ** get_address_of_Instance_4() { return &___Instance_4; }
	inline void set_Instance_4(EmptyDictionary_t1467978489 * value)
	{
		___Instance_4 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
