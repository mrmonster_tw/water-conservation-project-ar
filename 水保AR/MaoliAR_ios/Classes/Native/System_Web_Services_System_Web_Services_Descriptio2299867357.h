﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Web_Services_System_Web_Services_Description_10559386.h"

// System.Web.Services.Discovery.DiscoveryClientDocumentCollection
struct DiscoveryClientDocumentCollection_t1899304036;
// System.CodeDom.CodeNamespace
struct CodeNamespace_t2165007136;
// System.String
struct String_t;
// System.Collections.Specialized.StringCollection
struct StringCollection_t167406615;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.WebReference
struct  WebReference_t2299867357  : public Il2CppObject
{
public:
	// System.Web.Services.Discovery.DiscoveryClientDocumentCollection System.Web.Services.Description.WebReference::_documents
	DiscoveryClientDocumentCollection_t1899304036 * ____documents_0;
	// System.CodeDom.CodeNamespace System.Web.Services.Description.WebReference::_proxyCode
	CodeNamespace_t2165007136 * ____proxyCode_1;
	// System.Web.Services.Description.ServiceDescriptionImportWarnings System.Web.Services.Description.WebReference::_warnings
	int32_t ____warnings_2;
	// System.String System.Web.Services.Description.WebReference::_protocolName
	String_t* ____protocolName_3;
	// System.String System.Web.Services.Description.WebReference::_appSettingUrlKey
	String_t* ____appSettingUrlKey_4;
	// System.String System.Web.Services.Description.WebReference::_appSettingBaseUrl
	String_t* ____appSettingBaseUrl_5;
	// System.Collections.Specialized.StringCollection System.Web.Services.Description.WebReference::_validationWarnings
	StringCollection_t167406615 * ____validationWarnings_6;

public:
	inline static int32_t get_offset_of__documents_0() { return static_cast<int32_t>(offsetof(WebReference_t2299867357, ____documents_0)); }
	inline DiscoveryClientDocumentCollection_t1899304036 * get__documents_0() const { return ____documents_0; }
	inline DiscoveryClientDocumentCollection_t1899304036 ** get_address_of__documents_0() { return &____documents_0; }
	inline void set__documents_0(DiscoveryClientDocumentCollection_t1899304036 * value)
	{
		____documents_0 = value;
		Il2CppCodeGenWriteBarrier(&____documents_0, value);
	}

	inline static int32_t get_offset_of__proxyCode_1() { return static_cast<int32_t>(offsetof(WebReference_t2299867357, ____proxyCode_1)); }
	inline CodeNamespace_t2165007136 * get__proxyCode_1() const { return ____proxyCode_1; }
	inline CodeNamespace_t2165007136 ** get_address_of__proxyCode_1() { return &____proxyCode_1; }
	inline void set__proxyCode_1(CodeNamespace_t2165007136 * value)
	{
		____proxyCode_1 = value;
		Il2CppCodeGenWriteBarrier(&____proxyCode_1, value);
	}

	inline static int32_t get_offset_of__warnings_2() { return static_cast<int32_t>(offsetof(WebReference_t2299867357, ____warnings_2)); }
	inline int32_t get__warnings_2() const { return ____warnings_2; }
	inline int32_t* get_address_of__warnings_2() { return &____warnings_2; }
	inline void set__warnings_2(int32_t value)
	{
		____warnings_2 = value;
	}

	inline static int32_t get_offset_of__protocolName_3() { return static_cast<int32_t>(offsetof(WebReference_t2299867357, ____protocolName_3)); }
	inline String_t* get__protocolName_3() const { return ____protocolName_3; }
	inline String_t** get_address_of__protocolName_3() { return &____protocolName_3; }
	inline void set__protocolName_3(String_t* value)
	{
		____protocolName_3 = value;
		Il2CppCodeGenWriteBarrier(&____protocolName_3, value);
	}

	inline static int32_t get_offset_of__appSettingUrlKey_4() { return static_cast<int32_t>(offsetof(WebReference_t2299867357, ____appSettingUrlKey_4)); }
	inline String_t* get__appSettingUrlKey_4() const { return ____appSettingUrlKey_4; }
	inline String_t** get_address_of__appSettingUrlKey_4() { return &____appSettingUrlKey_4; }
	inline void set__appSettingUrlKey_4(String_t* value)
	{
		____appSettingUrlKey_4 = value;
		Il2CppCodeGenWriteBarrier(&____appSettingUrlKey_4, value);
	}

	inline static int32_t get_offset_of__appSettingBaseUrl_5() { return static_cast<int32_t>(offsetof(WebReference_t2299867357, ____appSettingBaseUrl_5)); }
	inline String_t* get__appSettingBaseUrl_5() const { return ____appSettingBaseUrl_5; }
	inline String_t** get_address_of__appSettingBaseUrl_5() { return &____appSettingBaseUrl_5; }
	inline void set__appSettingBaseUrl_5(String_t* value)
	{
		____appSettingBaseUrl_5 = value;
		Il2CppCodeGenWriteBarrier(&____appSettingBaseUrl_5, value);
	}

	inline static int32_t get_offset_of__validationWarnings_6() { return static_cast<int32_t>(offsetof(WebReference_t2299867357, ____validationWarnings_6)); }
	inline StringCollection_t167406615 * get__validationWarnings_6() const { return ____validationWarnings_6; }
	inline StringCollection_t167406615 ** get_address_of__validationWarnings_6() { return &____validationWarnings_6; }
	inline void set__validationWarnings_6(StringCollection_t167406615 * value)
	{
		____validationWarnings_6 = value;
		Il2CppCodeGenWriteBarrier(&____validationWarnings_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
