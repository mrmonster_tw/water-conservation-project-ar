﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_System_Net_Security_ProtectionLevel2559578148.h"

// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Type
struct Type_t;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.MessagePartDescription
struct  MessagePartDescription_t2026856693  : public Il2CppObject
{
public:
	// System.Int32 System.ServiceModel.Description.MessagePartDescription::index
	int32_t ___index_0;
	// System.Reflection.MemberInfo System.ServiceModel.Description.MessagePartDescription::member
	MemberInfo_t * ___member_1;
	// System.Type System.ServiceModel.Description.MessagePartDescription::type
	Type_t * ___type_2;
	// System.String System.ServiceModel.Description.MessagePartDescription::name
	String_t* ___name_3;
	// System.String System.ServiceModel.Description.MessagePartDescription::ns
	String_t* ___ns_4;
	// System.Boolean System.ServiceModel.Description.MessagePartDescription::has_protection_level
	bool ___has_protection_level_5;
	// System.Net.Security.ProtectionLevel System.ServiceModel.Description.MessagePartDescription::protection_level
	int32_t ___protection_level_6;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(MessagePartDescription_t2026856693, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_member_1() { return static_cast<int32_t>(offsetof(MessagePartDescription_t2026856693, ___member_1)); }
	inline MemberInfo_t * get_member_1() const { return ___member_1; }
	inline MemberInfo_t ** get_address_of_member_1() { return &___member_1; }
	inline void set_member_1(MemberInfo_t * value)
	{
		___member_1 = value;
		Il2CppCodeGenWriteBarrier(&___member_1, value);
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(MessagePartDescription_t2026856693, ___type_2)); }
	inline Type_t * get_type_2() const { return ___type_2; }
	inline Type_t ** get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(Type_t * value)
	{
		___type_2 = value;
		Il2CppCodeGenWriteBarrier(&___type_2, value);
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(MessagePartDescription_t2026856693, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier(&___name_3, value);
	}

	inline static int32_t get_offset_of_ns_4() { return static_cast<int32_t>(offsetof(MessagePartDescription_t2026856693, ___ns_4)); }
	inline String_t* get_ns_4() const { return ___ns_4; }
	inline String_t** get_address_of_ns_4() { return &___ns_4; }
	inline void set_ns_4(String_t* value)
	{
		___ns_4 = value;
		Il2CppCodeGenWriteBarrier(&___ns_4, value);
	}

	inline static int32_t get_offset_of_has_protection_level_5() { return static_cast<int32_t>(offsetof(MessagePartDescription_t2026856693, ___has_protection_level_5)); }
	inline bool get_has_protection_level_5() const { return ___has_protection_level_5; }
	inline bool* get_address_of_has_protection_level_5() { return &___has_protection_level_5; }
	inline void set_has_protection_level_5(bool value)
	{
		___has_protection_level_5 = value;
	}

	inline static int32_t get_offset_of_protection_level_6() { return static_cast<int32_t>(offsetof(MessagePartDescription_t2026856693, ___protection_level_6)); }
	inline int32_t get_protection_level_6() const { return ___protection_level_6; }
	inline int32_t* get_address_of_protection_level_6() { return &___protection_level_6; }
	inline void set_protection_level_6(int32_t value)
	{
		___protection_level_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
