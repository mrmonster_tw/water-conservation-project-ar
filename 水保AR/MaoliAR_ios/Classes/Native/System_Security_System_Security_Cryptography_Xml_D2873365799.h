﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Xml.XmlElement
struct XmlElement_t561603118;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.Xml.DataObject
struct  DataObject_t2873365799  : public Il2CppObject
{
public:
	// System.Xml.XmlElement System.Security.Cryptography.Xml.DataObject::element
	XmlElement_t561603118 * ___element_0;
	// System.Boolean System.Security.Cryptography.Xml.DataObject::propertyModified
	bool ___propertyModified_1;

public:
	inline static int32_t get_offset_of_element_0() { return static_cast<int32_t>(offsetof(DataObject_t2873365799, ___element_0)); }
	inline XmlElement_t561603118 * get_element_0() const { return ___element_0; }
	inline XmlElement_t561603118 ** get_address_of_element_0() { return &___element_0; }
	inline void set_element_0(XmlElement_t561603118 * value)
	{
		___element_0 = value;
		Il2CppCodeGenWriteBarrier(&___element_0, value);
	}

	inline static int32_t get_offset_of_propertyModified_1() { return static_cast<int32_t>(offsetof(DataObject_t2873365799, ___propertyModified_1)); }
	inline bool get_propertyModified_1() const { return ___propertyModified_1; }
	inline bool* get_address_of_propertyModified_1() { return &___propertyModified_1; }
	inline void set_propertyModified_1(bool value)
	{
		___propertyModified_1 = value;
	}
};

struct DataObject_t2873365799_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.Xml.DataObject::<>f__switch$map4
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map4_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4_2() { return static_cast<int32_t>(offsetof(DataObject_t2873365799_StaticFields, ___U3CU3Ef__switchU24map4_2)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map4_2() const { return ___U3CU3Ef__switchU24map4_2; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map4_2() { return &___U3CU3Ef__switchU24map4_2; }
	inline void set_U3CU3Ef__switchU24map4_2(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map4_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map4_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
