﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.Collections.ArrayList
struct ArrayList_t2718874744;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslNumber/XslNumberFormatter
struct  XslNumberFormatter_t3669835622  : public Il2CppObject
{
public:
	// System.String Mono.Xml.Xsl.Operations.XslNumber/XslNumberFormatter::firstSep
	String_t* ___firstSep_0;
	// System.String Mono.Xml.Xsl.Operations.XslNumber/XslNumberFormatter::lastSep
	String_t* ___lastSep_1;
	// System.Collections.ArrayList Mono.Xml.Xsl.Operations.XslNumber/XslNumberFormatter::fmtList
	ArrayList_t2718874744 * ___fmtList_2;

public:
	inline static int32_t get_offset_of_firstSep_0() { return static_cast<int32_t>(offsetof(XslNumberFormatter_t3669835622, ___firstSep_0)); }
	inline String_t* get_firstSep_0() const { return ___firstSep_0; }
	inline String_t** get_address_of_firstSep_0() { return &___firstSep_0; }
	inline void set_firstSep_0(String_t* value)
	{
		___firstSep_0 = value;
		Il2CppCodeGenWriteBarrier(&___firstSep_0, value);
	}

	inline static int32_t get_offset_of_lastSep_1() { return static_cast<int32_t>(offsetof(XslNumberFormatter_t3669835622, ___lastSep_1)); }
	inline String_t* get_lastSep_1() const { return ___lastSep_1; }
	inline String_t** get_address_of_lastSep_1() { return &___lastSep_1; }
	inline void set_lastSep_1(String_t* value)
	{
		___lastSep_1 = value;
		Il2CppCodeGenWriteBarrier(&___lastSep_1, value);
	}

	inline static int32_t get_offset_of_fmtList_2() { return static_cast<int32_t>(offsetof(XslNumberFormatter_t3669835622, ___fmtList_2)); }
	inline ArrayList_t2718874744 * get_fmtList_2() const { return ___fmtList_2; }
	inline ArrayList_t2718874744 ** get_address_of_fmtList_2() { return &___fmtList_2; }
	inline void set_fmtList_2(ArrayList_t2718874744 * value)
	{
		___fmtList_2 = value;
		Il2CppCodeGenWriteBarrier(&___fmtList_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
