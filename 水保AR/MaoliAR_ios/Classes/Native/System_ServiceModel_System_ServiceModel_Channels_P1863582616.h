﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_System_ServiceModel_Channels_P2823156161.h"

// System.ServiceModel.PeerNodeAddress
struct PeerNodeAddress_t2098027372;
// System.ServiceModel.Channels.PeerDuplexChannel/LocalPeerReceiver
struct LocalPeerReceiver_t2236693061;
// System.ServiceModel.Channels.PeerDuplexChannel/IPeerConnectorClient
struct IPeerConnectorClient_t888178396;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.PeerDuplexChannel/RemotePeerConnection
struct  RemotePeerConnection_t1863582616  : public Il2CppObject
{
public:
	// System.ServiceModel.PeerNodeAddress System.ServiceModel.Channels.PeerDuplexChannel/RemotePeerConnection::<Address>k__BackingField
	PeerNodeAddress_t2098027372 * ___U3CAddressU3Ek__BackingField_0;
	// System.ServiceModel.Channels.PeerDuplexChannel/RemotePeerStatus System.ServiceModel.Channels.PeerDuplexChannel/RemotePeerConnection::<Status>k__BackingField
	int32_t ___U3CStatusU3Ek__BackingField_1;
	// System.ServiceModel.Channels.PeerDuplexChannel/LocalPeerReceiver System.ServiceModel.Channels.PeerDuplexChannel/RemotePeerConnection::<Instance>k__BackingField
	LocalPeerReceiver_t2236693061 * ___U3CInstanceU3Ek__BackingField_2;
	// System.ServiceModel.Channels.PeerDuplexChannel/IPeerConnectorClient System.ServiceModel.Channels.PeerDuplexChannel/RemotePeerConnection::<Channel>k__BackingField
	Il2CppObject * ___U3CChannelU3Ek__BackingField_3;
	// System.UInt64 System.ServiceModel.Channels.PeerDuplexChannel/RemotePeerConnection::<NodeId>k__BackingField
	uint64_t ___U3CNodeIdU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CAddressU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RemotePeerConnection_t1863582616, ___U3CAddressU3Ek__BackingField_0)); }
	inline PeerNodeAddress_t2098027372 * get_U3CAddressU3Ek__BackingField_0() const { return ___U3CAddressU3Ek__BackingField_0; }
	inline PeerNodeAddress_t2098027372 ** get_address_of_U3CAddressU3Ek__BackingField_0() { return &___U3CAddressU3Ek__BackingField_0; }
	inline void set_U3CAddressU3Ek__BackingField_0(PeerNodeAddress_t2098027372 * value)
	{
		___U3CAddressU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAddressU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CStatusU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RemotePeerConnection_t1863582616, ___U3CStatusU3Ek__BackingField_1)); }
	inline int32_t get_U3CStatusU3Ek__BackingField_1() const { return ___U3CStatusU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CStatusU3Ek__BackingField_1() { return &___U3CStatusU3Ek__BackingField_1; }
	inline void set_U3CStatusU3Ek__BackingField_1(int32_t value)
	{
		___U3CStatusU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RemotePeerConnection_t1863582616, ___U3CInstanceU3Ek__BackingField_2)); }
	inline LocalPeerReceiver_t2236693061 * get_U3CInstanceU3Ek__BackingField_2() const { return ___U3CInstanceU3Ek__BackingField_2; }
	inline LocalPeerReceiver_t2236693061 ** get_address_of_U3CInstanceU3Ek__BackingField_2() { return &___U3CInstanceU3Ek__BackingField_2; }
	inline void set_U3CInstanceU3Ek__BackingField_2(LocalPeerReceiver_t2236693061 * value)
	{
		___U3CInstanceU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CInstanceU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CChannelU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RemotePeerConnection_t1863582616, ___U3CChannelU3Ek__BackingField_3)); }
	inline Il2CppObject * get_U3CChannelU3Ek__BackingField_3() const { return ___U3CChannelU3Ek__BackingField_3; }
	inline Il2CppObject ** get_address_of_U3CChannelU3Ek__BackingField_3() { return &___U3CChannelU3Ek__BackingField_3; }
	inline void set_U3CChannelU3Ek__BackingField_3(Il2CppObject * value)
	{
		___U3CChannelU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CChannelU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CNodeIdU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RemotePeerConnection_t1863582616, ___U3CNodeIdU3Ek__BackingField_4)); }
	inline uint64_t get_U3CNodeIdU3Ek__BackingField_4() const { return ___U3CNodeIdU3Ek__BackingField_4; }
	inline uint64_t* get_address_of_U3CNodeIdU3Ek__BackingField_4() { return &___U3CNodeIdU3Ek__BackingField_4; }
	inline void set_U3CNodeIdU3Ek__BackingField_4(uint64_t value)
	{
		___U3CNodeIdU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
