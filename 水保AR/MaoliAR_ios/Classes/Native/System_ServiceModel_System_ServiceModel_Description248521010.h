﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_System_ServiceModel_Descriptio1024212837.h"

// System.Web.Security.RoleProvider
struct RoleProvider_t1137021788;
// System.ServiceModel.ServiceAuthorizationManager
struct ServiceAuthorizationManager_t56182799;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IdentityModel.Policy.IAuthorizationPolicy>
struct ReadOnlyCollection_1_t778487864;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.ServiceAuthorizationBehavior
struct  ServiceAuthorizationBehavior_t248521010  : public Il2CppObject
{
public:
	// System.Boolean System.ServiceModel.Description.ServiceAuthorizationBehavior::impersonate
	bool ___impersonate_0;
	// System.ServiceModel.Description.PrincipalPermissionMode System.ServiceModel.Description.ServiceAuthorizationBehavior::perm_mode
	int32_t ___perm_mode_1;
	// System.Web.Security.RoleProvider System.ServiceModel.Description.ServiceAuthorizationBehavior::role_provider
	RoleProvider_t1137021788 * ___role_provider_2;
	// System.ServiceModel.ServiceAuthorizationManager System.ServiceModel.Description.ServiceAuthorizationBehavior::svc_auth_manager
	ServiceAuthorizationManager_t56182799 * ___svc_auth_manager_3;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IdentityModel.Policy.IAuthorizationPolicy> System.ServiceModel.Description.ServiceAuthorizationBehavior::ext_auth_policies
	ReadOnlyCollection_1_t778487864 * ___ext_auth_policies_4;

public:
	inline static int32_t get_offset_of_impersonate_0() { return static_cast<int32_t>(offsetof(ServiceAuthorizationBehavior_t248521010, ___impersonate_0)); }
	inline bool get_impersonate_0() const { return ___impersonate_0; }
	inline bool* get_address_of_impersonate_0() { return &___impersonate_0; }
	inline void set_impersonate_0(bool value)
	{
		___impersonate_0 = value;
	}

	inline static int32_t get_offset_of_perm_mode_1() { return static_cast<int32_t>(offsetof(ServiceAuthorizationBehavior_t248521010, ___perm_mode_1)); }
	inline int32_t get_perm_mode_1() const { return ___perm_mode_1; }
	inline int32_t* get_address_of_perm_mode_1() { return &___perm_mode_1; }
	inline void set_perm_mode_1(int32_t value)
	{
		___perm_mode_1 = value;
	}

	inline static int32_t get_offset_of_role_provider_2() { return static_cast<int32_t>(offsetof(ServiceAuthorizationBehavior_t248521010, ___role_provider_2)); }
	inline RoleProvider_t1137021788 * get_role_provider_2() const { return ___role_provider_2; }
	inline RoleProvider_t1137021788 ** get_address_of_role_provider_2() { return &___role_provider_2; }
	inline void set_role_provider_2(RoleProvider_t1137021788 * value)
	{
		___role_provider_2 = value;
		Il2CppCodeGenWriteBarrier(&___role_provider_2, value);
	}

	inline static int32_t get_offset_of_svc_auth_manager_3() { return static_cast<int32_t>(offsetof(ServiceAuthorizationBehavior_t248521010, ___svc_auth_manager_3)); }
	inline ServiceAuthorizationManager_t56182799 * get_svc_auth_manager_3() const { return ___svc_auth_manager_3; }
	inline ServiceAuthorizationManager_t56182799 ** get_address_of_svc_auth_manager_3() { return &___svc_auth_manager_3; }
	inline void set_svc_auth_manager_3(ServiceAuthorizationManager_t56182799 * value)
	{
		___svc_auth_manager_3 = value;
		Il2CppCodeGenWriteBarrier(&___svc_auth_manager_3, value);
	}

	inline static int32_t get_offset_of_ext_auth_policies_4() { return static_cast<int32_t>(offsetof(ServiceAuthorizationBehavior_t248521010, ___ext_auth_policies_4)); }
	inline ReadOnlyCollection_1_t778487864 * get_ext_auth_policies_4() const { return ___ext_auth_policies_4; }
	inline ReadOnlyCollection_1_t778487864 ** get_address_of_ext_auth_policies_4() { return &___ext_auth_policies_4; }
	inline void set_ext_auth_policies_4(ReadOnlyCollection_1_t778487864 * value)
	{
		___ext_auth_policies_4 = value;
		Il2CppCodeGenWriteBarrier(&___ext_auth_policies_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
