﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Selectors972969391.h"

// System.ServiceModel.Security.WSSecurityTokenSerializer
struct WSSecurityTokenSerializer_t1309166020;
// System.ServiceModel.Security.SecurityVersion
struct SecurityVersion_t3211242264;
// System.IdentityModel.Tokens.SamlSerializer
struct SamlSerializer_t985931480;
// System.ServiceModel.Security.SecurityStateEncoder
struct SecurityStateEncoder_t2009957818;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t3956019502;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.WSSecurityTokenSerializer
struct  WSSecurityTokenSerializer_t1309166020  : public SecurityTokenSerializer_t972969391
{
public:
	// System.ServiceModel.Security.SecurityVersion System.ServiceModel.Security.WSSecurityTokenSerializer::security_version
	SecurityVersion_t3211242264 * ___security_version_1;
	// System.Boolean System.ServiceModel.Security.WSSecurityTokenSerializer::emit_bsp
	bool ___emit_bsp_2;
	// System.IdentityModel.Tokens.SamlSerializer System.ServiceModel.Security.WSSecurityTokenSerializer::saml_serializer
	SamlSerializer_t985931480 * ___saml_serializer_3;
	// System.ServiceModel.Security.SecurityStateEncoder System.ServiceModel.Security.WSSecurityTokenSerializer::encoder
	SecurityStateEncoder_t2009957818 * ___encoder_4;
	// System.Collections.Generic.List`1<System.Type> System.ServiceModel.Security.WSSecurityTokenSerializer::known_types
	List_1_t3956019502 * ___known_types_5;
	// System.Int32 System.ServiceModel.Security.WSSecurityTokenSerializer::max_offset
	int32_t ___max_offset_6;
	// System.Int32 System.ServiceModel.Security.WSSecurityTokenSerializer::max_label_length
	int32_t ___max_label_length_7;
	// System.Int32 System.ServiceModel.Security.WSSecurityTokenSerializer::max_nonce_length
	int32_t ___max_nonce_length_8;

public:
	inline static int32_t get_offset_of_security_version_1() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020, ___security_version_1)); }
	inline SecurityVersion_t3211242264 * get_security_version_1() const { return ___security_version_1; }
	inline SecurityVersion_t3211242264 ** get_address_of_security_version_1() { return &___security_version_1; }
	inline void set_security_version_1(SecurityVersion_t3211242264 * value)
	{
		___security_version_1 = value;
		Il2CppCodeGenWriteBarrier(&___security_version_1, value);
	}

	inline static int32_t get_offset_of_emit_bsp_2() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020, ___emit_bsp_2)); }
	inline bool get_emit_bsp_2() const { return ___emit_bsp_2; }
	inline bool* get_address_of_emit_bsp_2() { return &___emit_bsp_2; }
	inline void set_emit_bsp_2(bool value)
	{
		___emit_bsp_2 = value;
	}

	inline static int32_t get_offset_of_saml_serializer_3() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020, ___saml_serializer_3)); }
	inline SamlSerializer_t985931480 * get_saml_serializer_3() const { return ___saml_serializer_3; }
	inline SamlSerializer_t985931480 ** get_address_of_saml_serializer_3() { return &___saml_serializer_3; }
	inline void set_saml_serializer_3(SamlSerializer_t985931480 * value)
	{
		___saml_serializer_3 = value;
		Il2CppCodeGenWriteBarrier(&___saml_serializer_3, value);
	}

	inline static int32_t get_offset_of_encoder_4() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020, ___encoder_4)); }
	inline SecurityStateEncoder_t2009957818 * get_encoder_4() const { return ___encoder_4; }
	inline SecurityStateEncoder_t2009957818 ** get_address_of_encoder_4() { return &___encoder_4; }
	inline void set_encoder_4(SecurityStateEncoder_t2009957818 * value)
	{
		___encoder_4 = value;
		Il2CppCodeGenWriteBarrier(&___encoder_4, value);
	}

	inline static int32_t get_offset_of_known_types_5() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020, ___known_types_5)); }
	inline List_1_t3956019502 * get_known_types_5() const { return ___known_types_5; }
	inline List_1_t3956019502 ** get_address_of_known_types_5() { return &___known_types_5; }
	inline void set_known_types_5(List_1_t3956019502 * value)
	{
		___known_types_5 = value;
		Il2CppCodeGenWriteBarrier(&___known_types_5, value);
	}

	inline static int32_t get_offset_of_max_offset_6() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020, ___max_offset_6)); }
	inline int32_t get_max_offset_6() const { return ___max_offset_6; }
	inline int32_t* get_address_of_max_offset_6() { return &___max_offset_6; }
	inline void set_max_offset_6(int32_t value)
	{
		___max_offset_6 = value;
	}

	inline static int32_t get_offset_of_max_label_length_7() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020, ___max_label_length_7)); }
	inline int32_t get_max_label_length_7() const { return ___max_label_length_7; }
	inline int32_t* get_address_of_max_label_length_7() { return &___max_label_length_7; }
	inline void set_max_label_length_7(int32_t value)
	{
		___max_label_length_7 = value;
	}

	inline static int32_t get_offset_of_max_nonce_length_8() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020, ___max_nonce_length_8)); }
	inline int32_t get_max_nonce_length_8() const { return ___max_nonce_length_8; }
	inline int32_t* get_address_of_max_nonce_length_8() { return &___max_nonce_length_8; }
	inline void set_max_nonce_length_8(int32_t value)
	{
		___max_nonce_length_8 = value;
	}
};

struct WSSecurityTokenSerializer_t1309166020_StaticFields
{
public:
	// System.ServiceModel.Security.WSSecurityTokenSerializer System.ServiceModel.Security.WSSecurityTokenSerializer::default_instance
	WSSecurityTokenSerializer_t1309166020 * ___default_instance_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Security.WSSecurityTokenSerializer::<>f__switch$map1D
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map1D_9;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Security.WSSecurityTokenSerializer::<>f__switch$map1E
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map1E_10;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Security.WSSecurityTokenSerializer::<>f__switch$map1F
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map1F_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Security.WSSecurityTokenSerializer::<>f__switch$map20
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map20_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Security.WSSecurityTokenSerializer::<>f__switch$map21
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map21_13;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Security.WSSecurityTokenSerializer::<>f__switch$map22
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map22_14;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Security.WSSecurityTokenSerializer::<>f__switch$map23
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map23_15;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Security.WSSecurityTokenSerializer::<>f__switch$map24
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map24_16;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Security.WSSecurityTokenSerializer::<>f__switch$map25
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map25_17;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Security.WSSecurityTokenSerializer::<>f__switch$map26
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map26_18;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Security.WSSecurityTokenSerializer::<>f__switch$map27
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map27_19;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Security.WSSecurityTokenSerializer::<>f__switch$map28
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map28_20;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Security.WSSecurityTokenSerializer::<>f__switch$map29
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map29_21;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Security.WSSecurityTokenSerializer::<>f__switch$map2A
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map2A_22;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Security.WSSecurityTokenSerializer::<>f__switch$map2B
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map2B_23;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Security.WSSecurityTokenSerializer::<>f__switch$map2C
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map2C_24;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Security.WSSecurityTokenSerializer::<>f__switch$map2D
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map2D_25;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Security.WSSecurityTokenSerializer::<>f__switch$map2E
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map2E_26;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Security.WSSecurityTokenSerializer::<>f__switch$map2F
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map2F_27;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Security.WSSecurityTokenSerializer::<>f__switch$map30
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map30_28;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Security.WSSecurityTokenSerializer::<>f__switch$map31
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map31_29;

public:
	inline static int32_t get_offset_of_default_instance_0() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020_StaticFields, ___default_instance_0)); }
	inline WSSecurityTokenSerializer_t1309166020 * get_default_instance_0() const { return ___default_instance_0; }
	inline WSSecurityTokenSerializer_t1309166020 ** get_address_of_default_instance_0() { return &___default_instance_0; }
	inline void set_default_instance_0(WSSecurityTokenSerializer_t1309166020 * value)
	{
		___default_instance_0 = value;
		Il2CppCodeGenWriteBarrier(&___default_instance_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1D_9() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020_StaticFields, ___U3CU3Ef__switchU24map1D_9)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map1D_9() const { return ___U3CU3Ef__switchU24map1D_9; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map1D_9() { return &___U3CU3Ef__switchU24map1D_9; }
	inline void set_U3CU3Ef__switchU24map1D_9(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map1D_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1D_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1E_10() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020_StaticFields, ___U3CU3Ef__switchU24map1E_10)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map1E_10() const { return ___U3CU3Ef__switchU24map1E_10; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map1E_10() { return &___U3CU3Ef__switchU24map1E_10; }
	inline void set_U3CU3Ef__switchU24map1E_10(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map1E_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1E_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1F_11() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020_StaticFields, ___U3CU3Ef__switchU24map1F_11)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map1F_11() const { return ___U3CU3Ef__switchU24map1F_11; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map1F_11() { return &___U3CU3Ef__switchU24map1F_11; }
	inline void set_U3CU3Ef__switchU24map1F_11(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map1F_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1F_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map20_12() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020_StaticFields, ___U3CU3Ef__switchU24map20_12)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map20_12() const { return ___U3CU3Ef__switchU24map20_12; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map20_12() { return &___U3CU3Ef__switchU24map20_12; }
	inline void set_U3CU3Ef__switchU24map20_12(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map20_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map20_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map21_13() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020_StaticFields, ___U3CU3Ef__switchU24map21_13)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map21_13() const { return ___U3CU3Ef__switchU24map21_13; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map21_13() { return &___U3CU3Ef__switchU24map21_13; }
	inline void set_U3CU3Ef__switchU24map21_13(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map21_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map21_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map22_14() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020_StaticFields, ___U3CU3Ef__switchU24map22_14)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map22_14() const { return ___U3CU3Ef__switchU24map22_14; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map22_14() { return &___U3CU3Ef__switchU24map22_14; }
	inline void set_U3CU3Ef__switchU24map22_14(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map22_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map22_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map23_15() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020_StaticFields, ___U3CU3Ef__switchU24map23_15)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map23_15() const { return ___U3CU3Ef__switchU24map23_15; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map23_15() { return &___U3CU3Ef__switchU24map23_15; }
	inline void set_U3CU3Ef__switchU24map23_15(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map23_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map23_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map24_16() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020_StaticFields, ___U3CU3Ef__switchU24map24_16)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map24_16() const { return ___U3CU3Ef__switchU24map24_16; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map24_16() { return &___U3CU3Ef__switchU24map24_16; }
	inline void set_U3CU3Ef__switchU24map24_16(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map24_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map24_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map25_17() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020_StaticFields, ___U3CU3Ef__switchU24map25_17)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map25_17() const { return ___U3CU3Ef__switchU24map25_17; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map25_17() { return &___U3CU3Ef__switchU24map25_17; }
	inline void set_U3CU3Ef__switchU24map25_17(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map25_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map25_17, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map26_18() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020_StaticFields, ___U3CU3Ef__switchU24map26_18)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map26_18() const { return ___U3CU3Ef__switchU24map26_18; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map26_18() { return &___U3CU3Ef__switchU24map26_18; }
	inline void set_U3CU3Ef__switchU24map26_18(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map26_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map26_18, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map27_19() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020_StaticFields, ___U3CU3Ef__switchU24map27_19)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map27_19() const { return ___U3CU3Ef__switchU24map27_19; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map27_19() { return &___U3CU3Ef__switchU24map27_19; }
	inline void set_U3CU3Ef__switchU24map27_19(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map27_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map27_19, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map28_20() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020_StaticFields, ___U3CU3Ef__switchU24map28_20)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map28_20() const { return ___U3CU3Ef__switchU24map28_20; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map28_20() { return &___U3CU3Ef__switchU24map28_20; }
	inline void set_U3CU3Ef__switchU24map28_20(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map28_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map28_20, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map29_21() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020_StaticFields, ___U3CU3Ef__switchU24map29_21)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map29_21() const { return ___U3CU3Ef__switchU24map29_21; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map29_21() { return &___U3CU3Ef__switchU24map29_21; }
	inline void set_U3CU3Ef__switchU24map29_21(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map29_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map29_21, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2A_22() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020_StaticFields, ___U3CU3Ef__switchU24map2A_22)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map2A_22() const { return ___U3CU3Ef__switchU24map2A_22; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map2A_22() { return &___U3CU3Ef__switchU24map2A_22; }
	inline void set_U3CU3Ef__switchU24map2A_22(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map2A_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map2A_22, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2B_23() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020_StaticFields, ___U3CU3Ef__switchU24map2B_23)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map2B_23() const { return ___U3CU3Ef__switchU24map2B_23; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map2B_23() { return &___U3CU3Ef__switchU24map2B_23; }
	inline void set_U3CU3Ef__switchU24map2B_23(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map2B_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map2B_23, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2C_24() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020_StaticFields, ___U3CU3Ef__switchU24map2C_24)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map2C_24() const { return ___U3CU3Ef__switchU24map2C_24; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map2C_24() { return &___U3CU3Ef__switchU24map2C_24; }
	inline void set_U3CU3Ef__switchU24map2C_24(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map2C_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map2C_24, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2D_25() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020_StaticFields, ___U3CU3Ef__switchU24map2D_25)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map2D_25() const { return ___U3CU3Ef__switchU24map2D_25; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map2D_25() { return &___U3CU3Ef__switchU24map2D_25; }
	inline void set_U3CU3Ef__switchU24map2D_25(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map2D_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map2D_25, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2E_26() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020_StaticFields, ___U3CU3Ef__switchU24map2E_26)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map2E_26() const { return ___U3CU3Ef__switchU24map2E_26; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map2E_26() { return &___U3CU3Ef__switchU24map2E_26; }
	inline void set_U3CU3Ef__switchU24map2E_26(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map2E_26 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map2E_26, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2F_27() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020_StaticFields, ___U3CU3Ef__switchU24map2F_27)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map2F_27() const { return ___U3CU3Ef__switchU24map2F_27; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map2F_27() { return &___U3CU3Ef__switchU24map2F_27; }
	inline void set_U3CU3Ef__switchU24map2F_27(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map2F_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map2F_27, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map30_28() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020_StaticFields, ___U3CU3Ef__switchU24map30_28)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map30_28() const { return ___U3CU3Ef__switchU24map30_28; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map30_28() { return &___U3CU3Ef__switchU24map30_28; }
	inline void set_U3CU3Ef__switchU24map30_28(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map30_28 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map30_28, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map31_29() { return static_cast<int32_t>(offsetof(WSSecurityTokenSerializer_t1309166020_StaticFields, ___U3CU3Ef__switchU24map31_29)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map31_29() const { return ___U3CU3Ef__switchU24map31_29; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map31_29() { return &___U3CU3Ef__switchU24map31_29; }
	inline void set_U3CU3Ef__switchU24map31_29(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map31_29 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map31_29, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
