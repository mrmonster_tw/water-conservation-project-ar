﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"

// System.Xml.UniqueId
struct UniqueId_t1383576913;
// System.ServiceModel.InstanceContext
struct InstanceContext_t3593205954;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Xml.UniqueId,System.ServiceModel.InstanceContext>
struct  KeyValuePair_2_t1176609220 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	UniqueId_t1383576913 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	InstanceContext_t3593205954 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1176609220, ___key_0)); }
	inline UniqueId_t1383576913 * get_key_0() const { return ___key_0; }
	inline UniqueId_t1383576913 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(UniqueId_t1383576913 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier(&___key_0, value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1176609220, ___value_1)); }
	inline InstanceContext_t3593205954 * get_value_1() const { return ___value_1; }
	inline InstanceContext_t3593205954 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(InstanceContext_t3593205954 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
