﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_H3854184778.h"

// System.ServiceModel.Channels.AspNetChannelListener`1<System.ServiceModel.Channels.IReplyChannel>
struct AspNetChannelListener_1_t3459437865;
// System.Collections.Generic.List`1<System.Web.HttpContext>
struct List_1_t3441333752;
// System.Web.HttpContext
struct HttpContext_t1969259010;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t1333520283;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.AspNetReplyChannel
struct  AspNetReplyChannel_t2159115947  : public HttpReplyChannel_t3854184778
{
public:
	// System.ServiceModel.Channels.AspNetChannelListener`1<System.ServiceModel.Channels.IReplyChannel> System.ServiceModel.Channels.AspNetReplyChannel::listener
	AspNetChannelListener_1_t3459437865 * ___listener_16;
	// System.Collections.Generic.List`1<System.Web.HttpContext> System.ServiceModel.Channels.AspNetReplyChannel::waiting
	List_1_t3441333752 * ___waiting_17;
	// System.Web.HttpContext System.ServiceModel.Channels.AspNetReplyChannel::http_context
	HttpContext_t1969259010 * ___http_context_18;
	// System.Threading.AutoResetEvent System.ServiceModel.Channels.AspNetReplyChannel::wait
	AutoResetEvent_t1333520283 * ___wait_19;

public:
	inline static int32_t get_offset_of_listener_16() { return static_cast<int32_t>(offsetof(AspNetReplyChannel_t2159115947, ___listener_16)); }
	inline AspNetChannelListener_1_t3459437865 * get_listener_16() const { return ___listener_16; }
	inline AspNetChannelListener_1_t3459437865 ** get_address_of_listener_16() { return &___listener_16; }
	inline void set_listener_16(AspNetChannelListener_1_t3459437865 * value)
	{
		___listener_16 = value;
		Il2CppCodeGenWriteBarrier(&___listener_16, value);
	}

	inline static int32_t get_offset_of_waiting_17() { return static_cast<int32_t>(offsetof(AspNetReplyChannel_t2159115947, ___waiting_17)); }
	inline List_1_t3441333752 * get_waiting_17() const { return ___waiting_17; }
	inline List_1_t3441333752 ** get_address_of_waiting_17() { return &___waiting_17; }
	inline void set_waiting_17(List_1_t3441333752 * value)
	{
		___waiting_17 = value;
		Il2CppCodeGenWriteBarrier(&___waiting_17, value);
	}

	inline static int32_t get_offset_of_http_context_18() { return static_cast<int32_t>(offsetof(AspNetReplyChannel_t2159115947, ___http_context_18)); }
	inline HttpContext_t1969259010 * get_http_context_18() const { return ___http_context_18; }
	inline HttpContext_t1969259010 ** get_address_of_http_context_18() { return &___http_context_18; }
	inline void set_http_context_18(HttpContext_t1969259010 * value)
	{
		___http_context_18 = value;
		Il2CppCodeGenWriteBarrier(&___http_context_18, value);
	}

	inline static int32_t get_offset_of_wait_19() { return static_cast<int32_t>(offsetof(AspNetReplyChannel_t2159115947, ___wait_19)); }
	inline AutoResetEvent_t1333520283 * get_wait_19() const { return ___wait_19; }
	inline AutoResetEvent_t1333520283 ** get_address_of_wait_19() { return &___wait_19; }
	inline void set_wait_19(AutoResetEvent_t1333520283 * value)
	{
		___wait_19 = value;
		Il2CppCodeGenWriteBarrier(&___wait_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
