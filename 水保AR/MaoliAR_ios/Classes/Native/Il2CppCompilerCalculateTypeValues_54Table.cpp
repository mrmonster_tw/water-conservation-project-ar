﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventHandle600343995.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste1003666588.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg1076084509.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3867320123.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3344766165.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigger55832929.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve3484638744.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirect1216237838.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycasterM2536340562.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastRes3360306849.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviou3495933518.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AxisEventD2331243652.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AbstractEv4171500731.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD3903027533.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3807901092.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3704011348.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3039385657.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInput3630163547.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInputM2019268878.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp3453173740.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInpu857139936.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInpu384203932.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp3190347560.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Standalone2760469101.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Standalone3382566315.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_TouchInput4248229598.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseRaycas4150874583.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Physics2DR3382992964.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PhysicsRayc437419520.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorT809614380.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Color1000778859.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Color1121741130.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Float1274330004.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Float1856710240.h"
#include "UnityEngine_UI_UnityEngine_UI_AnimationTriggers2532145056.h"
#include "UnityEngine_UI_UnityEngine_UI_Button4055032469.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_ButtonClickedEv48803504.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_U3COnFinishSu3413438900.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdate2572322932.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdateRegistry2720824592.h"
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock2139031574.h"
#include "UnityEngine_UI_UnityEngine_UI_DefaultControls4098465386.h"
#include "UnityEngine_UI_UnityEngine_UI_DefaultControls_Reso1597885468.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown2274391225.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_DropdownIte1451952895.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_OptionData3270282352.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_OptionDataL1438173104.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_DropdownEve4040729994.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_U3CShowU3Ec1106527198.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_U3CDelayedD3853912249.h"
#include "UnityEngine_UI_UnityEngine_UI_FontData746620069.h"
#include "UnityEngine_UI_UnityEngine_UI_FontUpdateTracker1839077620.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic1660335611.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster2999697109.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster_Bloc612090948.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicRegistry3479976429.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2670269651.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_Type1152881528.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_FillMethod1167457570.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_OriginHorizont1174417785.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_OriginVertical2256455259.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin901855765812.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin180325369132.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin360707706162.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField3762917431.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentTy1787303396.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_InputType1770400679.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_Character4051914437.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_LineType4214648469.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_OnValidat2355412304.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_SubmitEven648412432.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_OnChangeEv467195904.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_EditState3741896775.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_U3CCaretB2589889038.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5400 = { sizeof (EventHandle_t600343995)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5400[3] = 
{
	EventHandle_t600343995::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5401 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5402 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5403 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5404 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5405 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5406 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5407 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5408 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5409 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5410 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5411 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5412 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5413 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5414 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5415 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5416 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5417 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5418 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5419 = { sizeof (EventSystem_t1003666588), -1, sizeof(EventSystem_t1003666588_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5419[12] = 
{
	EventSystem_t1003666588::get_offset_of_m_SystemInputModules_2(),
	EventSystem_t1003666588::get_offset_of_m_CurrentInputModule_3(),
	EventSystem_t1003666588_StaticFields::get_offset_of_U3CcurrentU3Ek__BackingField_4(),
	EventSystem_t1003666588::get_offset_of_m_FirstSelected_5(),
	EventSystem_t1003666588::get_offset_of_m_sendNavigationEvents_6(),
	EventSystem_t1003666588::get_offset_of_m_DragThreshold_7(),
	EventSystem_t1003666588::get_offset_of_m_CurrentSelected_8(),
	EventSystem_t1003666588::get_offset_of_m_HasFocus_9(),
	EventSystem_t1003666588::get_offset_of_m_SelectionGuard_10(),
	EventSystem_t1003666588::get_offset_of_m_DummyData_11(),
	EventSystem_t1003666588_StaticFields::get_offset_of_s_RaycastComparer_12(),
	EventSystem_t1003666588_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5420 = { sizeof (EventTrigger_t1076084509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5420[2] = 
{
	EventTrigger_t1076084509::get_offset_of_m_Delegates_2(),
	EventTrigger_t1076084509::get_offset_of_delegates_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5421 = { sizeof (TriggerEvent_t3867320123), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5422 = { sizeof (Entry_t3344766165), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5422[2] = 
{
	Entry_t3344766165::get_offset_of_eventID_0(),
	Entry_t3344766165::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5423 = { sizeof (EventTriggerType_t55832929)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5423[18] = 
{
	EventTriggerType_t55832929::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5424 = { sizeof (ExecuteEvents_t3484638744), -1, sizeof(ExecuteEvents_t3484638744_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5424[36] = 
{
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerEnterHandler_0(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerExitHandler_1(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerDownHandler_2(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerUpHandler_3(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerClickHandler_4(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_InitializePotentialDragHandler_5(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_BeginDragHandler_6(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_DragHandler_7(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_EndDragHandler_8(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_DropHandler_9(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_ScrollHandler_10(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_UpdateSelectedHandler_11(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_SelectHandler_12(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_DeselectHandler_13(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_MoveHandler_14(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_SubmitHandler_15(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_CancelHandler_16(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_HandlerListPool_17(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_InternalTransformList_18(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_19(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_20(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_21(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_22(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_23(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_24(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_25(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_26(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_27(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_28(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_29(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_30(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_31(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_32(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_33(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_34(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5425 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5426 = { sizeof (MoveDirection_t1216237838)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5426[6] = 
{
	MoveDirection_t1216237838::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5427 = { sizeof (RaycasterManager_t2536340562), -1, sizeof(RaycasterManager_t2536340562_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5427[1] = 
{
	RaycasterManager_t2536340562_StaticFields::get_offset_of_s_Raycasters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5428 = { sizeof (RaycastResult_t3360306849)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5428[10] = 
{
	RaycastResult_t3360306849::get_offset_of_m_GameObject_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3360306849::get_offset_of_module_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3360306849::get_offset_of_distance_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3360306849::get_offset_of_index_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3360306849::get_offset_of_depth_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3360306849::get_offset_of_sortingLayer_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3360306849::get_offset_of_sortingOrder_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3360306849::get_offset_of_worldPosition_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3360306849::get_offset_of_worldNormal_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3360306849::get_offset_of_screenPosition_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5429 = { sizeof (UIBehaviour_t3495933518), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5430 = { sizeof (AxisEventData_t2331243652), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5430[2] = 
{
	AxisEventData_t2331243652::get_offset_of_U3CmoveVectorU3Ek__BackingField_2(),
	AxisEventData_t2331243652::get_offset_of_U3CmoveDirU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5431 = { sizeof (AbstractEventData_t4171500731), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5431[1] = 
{
	AbstractEventData_t4171500731::get_offset_of_m_Used_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5432 = { sizeof (BaseEventData_t3903027533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5432[1] = 
{
	BaseEventData_t3903027533::get_offset_of_m_EventSystem_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5433 = { sizeof (PointerEventData_t3807901092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5433[21] = 
{
	PointerEventData_t3807901092::get_offset_of_U3CpointerEnterU3Ek__BackingField_2(),
	PointerEventData_t3807901092::get_offset_of_m_PointerPress_3(),
	PointerEventData_t3807901092::get_offset_of_U3ClastPressU3Ek__BackingField_4(),
	PointerEventData_t3807901092::get_offset_of_U3CrawPointerPressU3Ek__BackingField_5(),
	PointerEventData_t3807901092::get_offset_of_U3CpointerDragU3Ek__BackingField_6(),
	PointerEventData_t3807901092::get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7(),
	PointerEventData_t3807901092::get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8(),
	PointerEventData_t3807901092::get_offset_of_hovered_9(),
	PointerEventData_t3807901092::get_offset_of_U3CeligibleForClickU3Ek__BackingField_10(),
	PointerEventData_t3807901092::get_offset_of_U3CpointerIdU3Ek__BackingField_11(),
	PointerEventData_t3807901092::get_offset_of_U3CpositionU3Ek__BackingField_12(),
	PointerEventData_t3807901092::get_offset_of_U3CdeltaU3Ek__BackingField_13(),
	PointerEventData_t3807901092::get_offset_of_U3CpressPositionU3Ek__BackingField_14(),
	PointerEventData_t3807901092::get_offset_of_U3CworldPositionU3Ek__BackingField_15(),
	PointerEventData_t3807901092::get_offset_of_U3CworldNormalU3Ek__BackingField_16(),
	PointerEventData_t3807901092::get_offset_of_U3CclickTimeU3Ek__BackingField_17(),
	PointerEventData_t3807901092::get_offset_of_U3CclickCountU3Ek__BackingField_18(),
	PointerEventData_t3807901092::get_offset_of_U3CscrollDeltaU3Ek__BackingField_19(),
	PointerEventData_t3807901092::get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20(),
	PointerEventData_t3807901092::get_offset_of_U3CdraggingU3Ek__BackingField_21(),
	PointerEventData_t3807901092::get_offset_of_U3CbuttonU3Ek__BackingField_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5434 = { sizeof (InputButton_t3704011348)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5434[4] = 
{
	InputButton_t3704011348::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5435 = { sizeof (FramePressState_t3039385657)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5435[5] = 
{
	FramePressState_t3039385657::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5436 = { sizeof (BaseInput_t3630163547), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5437 = { sizeof (BaseInputModule_t2019268878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5437[6] = 
{
	BaseInputModule_t2019268878::get_offset_of_m_RaycastResultCache_2(),
	BaseInputModule_t2019268878::get_offset_of_m_AxisEventData_3(),
	BaseInputModule_t2019268878::get_offset_of_m_EventSystem_4(),
	BaseInputModule_t2019268878::get_offset_of_m_BaseEventData_5(),
	BaseInputModule_t2019268878::get_offset_of_m_InputOverride_6(),
	BaseInputModule_t2019268878::get_offset_of_m_DefaultInput_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5438 = { sizeof (PointerInputModule_t3453173740), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5438[6] = 
{
	0,
	0,
	0,
	0,
	PointerInputModule_t3453173740::get_offset_of_m_PointerData_12(),
	PointerInputModule_t3453173740::get_offset_of_m_MouseState_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5439 = { sizeof (ButtonState_t857139936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5439[2] = 
{
	ButtonState_t857139936::get_offset_of_m_Button_0(),
	ButtonState_t857139936::get_offset_of_m_EventData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5440 = { sizeof (MouseState_t384203932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5440[1] = 
{
	MouseState_t384203932::get_offset_of_m_TrackedButtons_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5441 = { sizeof (MouseButtonEventData_t3190347560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5441[2] = 
{
	MouseButtonEventData_t3190347560::get_offset_of_buttonState_0(),
	MouseButtonEventData_t3190347560::get_offset_of_buttonData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5442 = { sizeof (StandaloneInputModule_t2760469101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5442[13] = 
{
	StandaloneInputModule_t2760469101::get_offset_of_m_PrevActionTime_14(),
	StandaloneInputModule_t2760469101::get_offset_of_m_LastMoveVector_15(),
	StandaloneInputModule_t2760469101::get_offset_of_m_ConsecutiveMoveCount_16(),
	StandaloneInputModule_t2760469101::get_offset_of_m_LastMousePosition_17(),
	StandaloneInputModule_t2760469101::get_offset_of_m_MousePosition_18(),
	StandaloneInputModule_t2760469101::get_offset_of_m_CurrentFocusedGameObject_19(),
	StandaloneInputModule_t2760469101::get_offset_of_m_HorizontalAxis_20(),
	StandaloneInputModule_t2760469101::get_offset_of_m_VerticalAxis_21(),
	StandaloneInputModule_t2760469101::get_offset_of_m_SubmitButton_22(),
	StandaloneInputModule_t2760469101::get_offset_of_m_CancelButton_23(),
	StandaloneInputModule_t2760469101::get_offset_of_m_InputActionsPerSecond_24(),
	StandaloneInputModule_t2760469101::get_offset_of_m_RepeatDelay_25(),
	StandaloneInputModule_t2760469101::get_offset_of_m_ForceModuleActive_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5443 = { sizeof (InputMode_t3382566315)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5443[3] = 
{
	InputMode_t3382566315::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5444 = { sizeof (TouchInputModule_t4248229598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5444[3] = 
{
	TouchInputModule_t4248229598::get_offset_of_m_LastMousePosition_14(),
	TouchInputModule_t4248229598::get_offset_of_m_MousePosition_15(),
	TouchInputModule_t4248229598::get_offset_of_m_ForceModuleActive_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5445 = { sizeof (BaseRaycaster_t4150874583), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5446 = { sizeof (Physics2DRaycaster_t3382992964), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5447 = { sizeof (PhysicsRaycaster_t437419520), -1, sizeof(PhysicsRaycaster_t437419520_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5447[4] = 
{
	0,
	PhysicsRaycaster_t437419520::get_offset_of_m_EventCamera_3(),
	PhysicsRaycaster_t437419520::get_offset_of_m_EventMask_4(),
	PhysicsRaycaster_t437419520_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5448 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5449 = { sizeof (ColorTween_t809614380)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5449[6] = 
{
	ColorTween_t809614380::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t809614380::get_offset_of_m_StartColor_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t809614380::get_offset_of_m_TargetColor_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t809614380::get_offset_of_m_TweenMode_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t809614380::get_offset_of_m_Duration_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t809614380::get_offset_of_m_IgnoreTimeScale_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5450 = { sizeof (ColorTweenMode_t1000778859)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5450[4] = 
{
	ColorTweenMode_t1000778859::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5451 = { sizeof (ColorTweenCallback_t1121741130), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5452 = { sizeof (FloatTween_t1274330004)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5452[5] = 
{
	FloatTween_t1274330004::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t1274330004::get_offset_of_m_StartValue_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t1274330004::get_offset_of_m_TargetValue_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t1274330004::get_offset_of_m_Duration_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t1274330004::get_offset_of_m_IgnoreTimeScale_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5453 = { sizeof (FloatTweenCallback_t1856710240), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5454 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5454[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5455 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5455[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5456 = { sizeof (AnimationTriggers_t2532145056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5456[8] = 
{
	0,
	0,
	0,
	0,
	AnimationTriggers_t2532145056::get_offset_of_m_NormalTrigger_4(),
	AnimationTriggers_t2532145056::get_offset_of_m_HighlightedTrigger_5(),
	AnimationTriggers_t2532145056::get_offset_of_m_PressedTrigger_6(),
	AnimationTriggers_t2532145056::get_offset_of_m_DisabledTrigger_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5457 = { sizeof (Button_t4055032469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5457[1] = 
{
	Button_t4055032469::get_offset_of_m_OnClick_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5458 = { sizeof (ButtonClickedEvent_t48803504), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5459 = { sizeof (U3COnFinishSubmitU3Ec__Iterator0_t3413438900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5459[6] = 
{
	U3COnFinishSubmitU3Ec__Iterator0_t3413438900::get_offset_of_U3CfadeTimeU3E__0_0(),
	U3COnFinishSubmitU3Ec__Iterator0_t3413438900::get_offset_of_U3CelapsedTimeU3E__0_1(),
	U3COnFinishSubmitU3Ec__Iterator0_t3413438900::get_offset_of_U24this_2(),
	U3COnFinishSubmitU3Ec__Iterator0_t3413438900::get_offset_of_U24current_3(),
	U3COnFinishSubmitU3Ec__Iterator0_t3413438900::get_offset_of_U24disposing_4(),
	U3COnFinishSubmitU3Ec__Iterator0_t3413438900::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5460 = { sizeof (CanvasUpdate_t2572322932)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5460[7] = 
{
	CanvasUpdate_t2572322932::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5461 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5462 = { sizeof (CanvasUpdateRegistry_t2720824592), -1, sizeof(CanvasUpdateRegistry_t2720824592_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5462[7] = 
{
	CanvasUpdateRegistry_t2720824592_StaticFields::get_offset_of_s_Instance_0(),
	CanvasUpdateRegistry_t2720824592::get_offset_of_m_PerformingLayoutUpdate_1(),
	CanvasUpdateRegistry_t2720824592::get_offset_of_m_PerformingGraphicUpdate_2(),
	CanvasUpdateRegistry_t2720824592::get_offset_of_m_LayoutRebuildQueue_3(),
	CanvasUpdateRegistry_t2720824592::get_offset_of_m_GraphicRebuildQueue_4(),
	CanvasUpdateRegistry_t2720824592_StaticFields::get_offset_of_s_SortLayoutFunction_5(),
	CanvasUpdateRegistry_t2720824592_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5463 = { sizeof (ColorBlock_t2139031574)+ sizeof (Il2CppObject), sizeof(ColorBlock_t2139031574 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5463[6] = 
{
	ColorBlock_t2139031574::get_offset_of_m_NormalColor_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2139031574::get_offset_of_m_HighlightedColor_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2139031574::get_offset_of_m_PressedColor_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2139031574::get_offset_of_m_DisabledColor_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2139031574::get_offset_of_m_ColorMultiplier_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2139031574::get_offset_of_m_FadeDuration_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5464 = { sizeof (DefaultControls_t4098465386), -1, sizeof(DefaultControls_t4098465386_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5464[9] = 
{
	0,
	0,
	0,
	DefaultControls_t4098465386_StaticFields::get_offset_of_s_ThickElementSize_3(),
	DefaultControls_t4098465386_StaticFields::get_offset_of_s_ThinElementSize_4(),
	DefaultControls_t4098465386_StaticFields::get_offset_of_s_ImageElementSize_5(),
	DefaultControls_t4098465386_StaticFields::get_offset_of_s_DefaultSelectableColor_6(),
	DefaultControls_t4098465386_StaticFields::get_offset_of_s_PanelColor_7(),
	DefaultControls_t4098465386_StaticFields::get_offset_of_s_TextColor_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5465 = { sizeof (Resources_t1597885468)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5465[7] = 
{
	Resources_t1597885468::get_offset_of_standard_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t1597885468::get_offset_of_background_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t1597885468::get_offset_of_inputField_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t1597885468::get_offset_of_knob_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t1597885468::get_offset_of_checkmark_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t1597885468::get_offset_of_dropdown_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t1597885468::get_offset_of_mask_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5466 = { sizeof (Dropdown_t2274391225), -1, sizeof(Dropdown_t2274391225_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5466[14] = 
{
	Dropdown_t2274391225::get_offset_of_m_Template_16(),
	Dropdown_t2274391225::get_offset_of_m_CaptionText_17(),
	Dropdown_t2274391225::get_offset_of_m_CaptionImage_18(),
	Dropdown_t2274391225::get_offset_of_m_ItemText_19(),
	Dropdown_t2274391225::get_offset_of_m_ItemImage_20(),
	Dropdown_t2274391225::get_offset_of_m_Value_21(),
	Dropdown_t2274391225::get_offset_of_m_Options_22(),
	Dropdown_t2274391225::get_offset_of_m_OnValueChanged_23(),
	Dropdown_t2274391225::get_offset_of_m_Dropdown_24(),
	Dropdown_t2274391225::get_offset_of_m_Blocker_25(),
	Dropdown_t2274391225::get_offset_of_m_Items_26(),
	Dropdown_t2274391225::get_offset_of_m_AlphaTweenRunner_27(),
	Dropdown_t2274391225::get_offset_of_validTemplate_28(),
	Dropdown_t2274391225_StaticFields::get_offset_of_s_NoOptionData_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5467 = { sizeof (DropdownItem_t1451952895), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5467[4] = 
{
	DropdownItem_t1451952895::get_offset_of_m_Text_2(),
	DropdownItem_t1451952895::get_offset_of_m_Image_3(),
	DropdownItem_t1451952895::get_offset_of_m_RectTransform_4(),
	DropdownItem_t1451952895::get_offset_of_m_Toggle_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5468 = { sizeof (OptionData_t3270282352), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5468[2] = 
{
	OptionData_t3270282352::get_offset_of_m_Text_0(),
	OptionData_t3270282352::get_offset_of_m_Image_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5469 = { sizeof (OptionDataList_t1438173104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5469[1] = 
{
	OptionDataList_t1438173104::get_offset_of_m_Options_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5470 = { sizeof (DropdownEvent_t4040729994), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5471 = { sizeof (U3CShowU3Ec__AnonStorey1_t1106527198), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5471[2] = 
{
	U3CShowU3Ec__AnonStorey1_t1106527198::get_offset_of_item_0(),
	U3CShowU3Ec__AnonStorey1_t1106527198::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5472 = { sizeof (U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3853912249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5472[5] = 
{
	U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3853912249::get_offset_of_delay_0(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3853912249::get_offset_of_U24this_1(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3853912249::get_offset_of_U24current_2(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3853912249::get_offset_of_U24disposing_3(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3853912249::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5473 = { sizeof (FontData_t746620069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5473[12] = 
{
	FontData_t746620069::get_offset_of_m_Font_0(),
	FontData_t746620069::get_offset_of_m_FontSize_1(),
	FontData_t746620069::get_offset_of_m_FontStyle_2(),
	FontData_t746620069::get_offset_of_m_BestFit_3(),
	FontData_t746620069::get_offset_of_m_MinSize_4(),
	FontData_t746620069::get_offset_of_m_MaxSize_5(),
	FontData_t746620069::get_offset_of_m_Alignment_6(),
	FontData_t746620069::get_offset_of_m_AlignByGeometry_7(),
	FontData_t746620069::get_offset_of_m_RichText_8(),
	FontData_t746620069::get_offset_of_m_HorizontalOverflow_9(),
	FontData_t746620069::get_offset_of_m_VerticalOverflow_10(),
	FontData_t746620069::get_offset_of_m_LineSpacing_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5474 = { sizeof (FontUpdateTracker_t1839077620), -1, sizeof(FontUpdateTracker_t1839077620_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5474[3] = 
{
	FontUpdateTracker_t1839077620_StaticFields::get_offset_of_m_Tracked_0(),
	FontUpdateTracker_t1839077620_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_1(),
	FontUpdateTracker_t1839077620_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5475 = { sizeof (Graphic_t1660335611), -1, sizeof(Graphic_t1660335611_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5475[17] = 
{
	Graphic_t1660335611_StaticFields::get_offset_of_s_DefaultUI_2(),
	Graphic_t1660335611_StaticFields::get_offset_of_s_WhiteTexture_3(),
	Graphic_t1660335611::get_offset_of_m_Material_4(),
	Graphic_t1660335611::get_offset_of_m_Color_5(),
	Graphic_t1660335611::get_offset_of_m_RaycastTarget_6(),
	Graphic_t1660335611::get_offset_of_m_RectTransform_7(),
	Graphic_t1660335611::get_offset_of_m_CanvasRender_8(),
	Graphic_t1660335611::get_offset_of_m_Canvas_9(),
	Graphic_t1660335611::get_offset_of_m_VertsDirty_10(),
	Graphic_t1660335611::get_offset_of_m_MaterialDirty_11(),
	Graphic_t1660335611::get_offset_of_m_OnDirtyLayoutCallback_12(),
	Graphic_t1660335611::get_offset_of_m_OnDirtyVertsCallback_13(),
	Graphic_t1660335611::get_offset_of_m_OnDirtyMaterialCallback_14(),
	Graphic_t1660335611_StaticFields::get_offset_of_s_Mesh_15(),
	Graphic_t1660335611_StaticFields::get_offset_of_s_VertexHelper_16(),
	Graphic_t1660335611::get_offset_of_m_ColorTweenRunner_17(),
	Graphic_t1660335611::get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5476 = { sizeof (GraphicRaycaster_t2999697109), -1, sizeof(GraphicRaycaster_t2999697109_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5476[8] = 
{
	0,
	GraphicRaycaster_t2999697109::get_offset_of_m_IgnoreReversedGraphics_3(),
	GraphicRaycaster_t2999697109::get_offset_of_m_BlockingObjects_4(),
	GraphicRaycaster_t2999697109::get_offset_of_m_BlockingMask_5(),
	GraphicRaycaster_t2999697109::get_offset_of_m_Canvas_6(),
	GraphicRaycaster_t2999697109::get_offset_of_m_RaycastResults_7(),
	GraphicRaycaster_t2999697109_StaticFields::get_offset_of_s_SortedGraphics_8(),
	GraphicRaycaster_t2999697109_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5477 = { sizeof (BlockingObjects_t612090948)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5477[5] = 
{
	BlockingObjects_t612090948::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5478 = { sizeof (GraphicRegistry_t3479976429), -1, sizeof(GraphicRegistry_t3479976429_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5478[3] = 
{
	GraphicRegistry_t3479976429_StaticFields::get_offset_of_s_Instance_0(),
	GraphicRegistry_t3479976429::get_offset_of_m_Graphics_1(),
	GraphicRegistry_t3479976429_StaticFields::get_offset_of_s_EmptyList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5479 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5480 = { sizeof (Image_t2670269651), -1, sizeof(Image_t2670269651_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5480[15] = 
{
	Image_t2670269651_StaticFields::get_offset_of_s_ETC1DefaultUI_28(),
	Image_t2670269651::get_offset_of_m_Sprite_29(),
	Image_t2670269651::get_offset_of_m_OverrideSprite_30(),
	Image_t2670269651::get_offset_of_m_Type_31(),
	Image_t2670269651::get_offset_of_m_PreserveAspect_32(),
	Image_t2670269651::get_offset_of_m_FillCenter_33(),
	Image_t2670269651::get_offset_of_m_FillMethod_34(),
	Image_t2670269651::get_offset_of_m_FillAmount_35(),
	Image_t2670269651::get_offset_of_m_FillClockwise_36(),
	Image_t2670269651::get_offset_of_m_FillOrigin_37(),
	Image_t2670269651::get_offset_of_m_AlphaHitTestMinimumThreshold_38(),
	Image_t2670269651_StaticFields::get_offset_of_s_VertScratch_39(),
	Image_t2670269651_StaticFields::get_offset_of_s_UVScratch_40(),
	Image_t2670269651_StaticFields::get_offset_of_s_Xy_41(),
	Image_t2670269651_StaticFields::get_offset_of_s_Uv_42(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5481 = { sizeof (Type_t1152881528)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5481[5] = 
{
	Type_t1152881528::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5482 = { sizeof (FillMethod_t1167457570)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5482[6] = 
{
	FillMethod_t1167457570::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5483 = { sizeof (OriginHorizontal_t1174417785)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5483[3] = 
{
	OriginHorizontal_t1174417785::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5484 = { sizeof (OriginVertical_t2256455259)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5484[3] = 
{
	OriginVertical_t2256455259::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5485 = { sizeof (Origin90_t1855765812)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5485[5] = 
{
	Origin90_t1855765812::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5486 = { sizeof (Origin180_t325369132)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5486[5] = 
{
	Origin180_t325369132::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5487 = { sizeof (Origin360_t707706162)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5487[5] = 
{
	Origin360_t707706162::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5488 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5489 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5490 = { sizeof (InputField_t3762917431), -1, sizeof(InputField_t3762917431_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5490[47] = 
{
	InputField_t3762917431::get_offset_of_m_Keyboard_16(),
	InputField_t3762917431_StaticFields::get_offset_of_kSeparators_17(),
	InputField_t3762917431::get_offset_of_m_TextComponent_18(),
	InputField_t3762917431::get_offset_of_m_Placeholder_19(),
	InputField_t3762917431::get_offset_of_m_ContentType_20(),
	InputField_t3762917431::get_offset_of_m_InputType_21(),
	InputField_t3762917431::get_offset_of_m_AsteriskChar_22(),
	InputField_t3762917431::get_offset_of_m_KeyboardType_23(),
	InputField_t3762917431::get_offset_of_m_LineType_24(),
	InputField_t3762917431::get_offset_of_m_HideMobileInput_25(),
	InputField_t3762917431::get_offset_of_m_CharacterValidation_26(),
	InputField_t3762917431::get_offset_of_m_CharacterLimit_27(),
	InputField_t3762917431::get_offset_of_m_OnEndEdit_28(),
	InputField_t3762917431::get_offset_of_m_OnValueChanged_29(),
	InputField_t3762917431::get_offset_of_m_OnValidateInput_30(),
	InputField_t3762917431::get_offset_of_m_CaretColor_31(),
	InputField_t3762917431::get_offset_of_m_CustomCaretColor_32(),
	InputField_t3762917431::get_offset_of_m_SelectionColor_33(),
	InputField_t3762917431::get_offset_of_m_Text_34(),
	InputField_t3762917431::get_offset_of_m_CaretBlinkRate_35(),
	InputField_t3762917431::get_offset_of_m_CaretWidth_36(),
	InputField_t3762917431::get_offset_of_m_ReadOnly_37(),
	InputField_t3762917431::get_offset_of_m_CaretPosition_38(),
	InputField_t3762917431::get_offset_of_m_CaretSelectPosition_39(),
	InputField_t3762917431::get_offset_of_caretRectTrans_40(),
	InputField_t3762917431::get_offset_of_m_CursorVerts_41(),
	InputField_t3762917431::get_offset_of_m_InputTextCache_42(),
	InputField_t3762917431::get_offset_of_m_CachedInputRenderer_43(),
	InputField_t3762917431::get_offset_of_m_PreventFontCallback_44(),
	InputField_t3762917431::get_offset_of_m_Mesh_45(),
	InputField_t3762917431::get_offset_of_m_AllowInput_46(),
	InputField_t3762917431::get_offset_of_m_ShouldActivateNextUpdate_47(),
	InputField_t3762917431::get_offset_of_m_UpdateDrag_48(),
	InputField_t3762917431::get_offset_of_m_DragPositionOutOfBounds_49(),
	0,
	0,
	InputField_t3762917431::get_offset_of_m_CaretVisible_52(),
	InputField_t3762917431::get_offset_of_m_BlinkCoroutine_53(),
	InputField_t3762917431::get_offset_of_m_BlinkStartTime_54(),
	InputField_t3762917431::get_offset_of_m_DrawStart_55(),
	InputField_t3762917431::get_offset_of_m_DrawEnd_56(),
	InputField_t3762917431::get_offset_of_m_DragCoroutine_57(),
	InputField_t3762917431::get_offset_of_m_OriginalText_58(),
	InputField_t3762917431::get_offset_of_m_WasCanceled_59(),
	InputField_t3762917431::get_offset_of_m_HasDoneFocusTransition_60(),
	0,
	InputField_t3762917431::get_offset_of_m_ProcessingEvent_62(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5491 = { sizeof (ContentType_t1787303396)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5491[11] = 
{
	ContentType_t1787303396::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5492 = { sizeof (InputType_t1770400679)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5492[4] = 
{
	InputType_t1770400679::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5493 = { sizeof (CharacterValidation_t4051914437)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5493[7] = 
{
	CharacterValidation_t4051914437::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5494 = { sizeof (LineType_t4214648469)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5494[4] = 
{
	LineType_t4214648469::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5495 = { sizeof (OnValidateInput_t2355412304), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5496 = { sizeof (SubmitEvent_t648412432), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5497 = { sizeof (OnChangeEvent_t467195904), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5498 = { sizeof (EditState_t3741896775)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5498[3] = 
{
	EditState_t3741896775::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5499 = { sizeof (U3CCaretBlinkU3Ec__Iterator0_t2589889038), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5499[6] = 
{
	U3CCaretBlinkU3Ec__Iterator0_t2589889038::get_offset_of_U3CblinkPeriodU3E__1_0(),
	U3CCaretBlinkU3Ec__Iterator0_t2589889038::get_offset_of_U3CblinkStateU3E__1_1(),
	U3CCaretBlinkU3Ec__Iterator0_t2589889038::get_offset_of_U24this_2(),
	U3CCaretBlinkU3Ec__Iterator0_t2589889038::get_offset_of_U24current_3(),
	U3CCaretBlinkU3Ec__Iterator0_t2589889038::get_offset_of_U24disposing_4(),
	U3CCaretBlinkU3Ec__Iterator0_t2589889038::get_offset_of_U24PC_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
