﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_System_ServiceModel_Security_S1263698963.h"
#include "mscorlib_System_TimeSpan881159249.h"

// System.Collections.Generic.Dictionary`2<System.Uri,System.Collections.Generic.KeyedByTypeCollection`1<System.ServiceModel.Description.IEndpointBehavior>>
struct Dictionary_2_t3399354942;
// System.Collections.Generic.KeyedByTypeCollection`1<System.ServiceModel.Description.IEndpointBehavior>
struct KeyedByTypeCollection_1_t4222441378;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.IssuedTokenClientCredential
struct  IssuedTokenClientCredential_t2331807164  : public Il2CppObject
{
public:
	// System.Boolean System.ServiceModel.Security.IssuedTokenClientCredential::cache
	bool ___cache_0;
	// System.Collections.Generic.Dictionary`2<System.Uri,System.Collections.Generic.KeyedByTypeCollection`1<System.ServiceModel.Description.IEndpointBehavior>> System.ServiceModel.Security.IssuedTokenClientCredential::behaviors
	Dictionary_2_t3399354942 * ___behaviors_1;
	// System.ServiceModel.Security.SecurityKeyEntropyMode System.ServiceModel.Security.IssuedTokenClientCredential::entropy
	int32_t ___entropy_2;
	// System.Collections.Generic.KeyedByTypeCollection`1<System.ServiceModel.Description.IEndpointBehavior> System.ServiceModel.Security.IssuedTokenClientCredential::local_behaviors
	KeyedByTypeCollection_1_t4222441378 * ___local_behaviors_3;
	// System.TimeSpan System.ServiceModel.Security.IssuedTokenClientCredential::max_cache_time
	TimeSpan_t881159249  ___max_cache_time_4;
	// System.Int32 System.ServiceModel.Security.IssuedTokenClientCredential::renewal_threshold
	int32_t ___renewal_threshold_5;

public:
	inline static int32_t get_offset_of_cache_0() { return static_cast<int32_t>(offsetof(IssuedTokenClientCredential_t2331807164, ___cache_0)); }
	inline bool get_cache_0() const { return ___cache_0; }
	inline bool* get_address_of_cache_0() { return &___cache_0; }
	inline void set_cache_0(bool value)
	{
		___cache_0 = value;
	}

	inline static int32_t get_offset_of_behaviors_1() { return static_cast<int32_t>(offsetof(IssuedTokenClientCredential_t2331807164, ___behaviors_1)); }
	inline Dictionary_2_t3399354942 * get_behaviors_1() const { return ___behaviors_1; }
	inline Dictionary_2_t3399354942 ** get_address_of_behaviors_1() { return &___behaviors_1; }
	inline void set_behaviors_1(Dictionary_2_t3399354942 * value)
	{
		___behaviors_1 = value;
		Il2CppCodeGenWriteBarrier(&___behaviors_1, value);
	}

	inline static int32_t get_offset_of_entropy_2() { return static_cast<int32_t>(offsetof(IssuedTokenClientCredential_t2331807164, ___entropy_2)); }
	inline int32_t get_entropy_2() const { return ___entropy_2; }
	inline int32_t* get_address_of_entropy_2() { return &___entropy_2; }
	inline void set_entropy_2(int32_t value)
	{
		___entropy_2 = value;
	}

	inline static int32_t get_offset_of_local_behaviors_3() { return static_cast<int32_t>(offsetof(IssuedTokenClientCredential_t2331807164, ___local_behaviors_3)); }
	inline KeyedByTypeCollection_1_t4222441378 * get_local_behaviors_3() const { return ___local_behaviors_3; }
	inline KeyedByTypeCollection_1_t4222441378 ** get_address_of_local_behaviors_3() { return &___local_behaviors_3; }
	inline void set_local_behaviors_3(KeyedByTypeCollection_1_t4222441378 * value)
	{
		___local_behaviors_3 = value;
		Il2CppCodeGenWriteBarrier(&___local_behaviors_3, value);
	}

	inline static int32_t get_offset_of_max_cache_time_4() { return static_cast<int32_t>(offsetof(IssuedTokenClientCredential_t2331807164, ___max_cache_time_4)); }
	inline TimeSpan_t881159249  get_max_cache_time_4() const { return ___max_cache_time_4; }
	inline TimeSpan_t881159249 * get_address_of_max_cache_time_4() { return &___max_cache_time_4; }
	inline void set_max_cache_time_4(TimeSpan_t881159249  value)
	{
		___max_cache_time_4 = value;
	}

	inline static int32_t get_offset_of_renewal_threshold_5() { return static_cast<int32_t>(offsetof(IssuedTokenClientCredential_t2331807164, ___renewal_threshold_5)); }
	inline int32_t get_renewal_threshold_5() const { return ___renewal_threshold_5; }
	inline int32_t* get_address_of_renewal_threshold_5() { return &___renewal_threshold_5; }
	inline void set_renewal_threshold_5(int32_t value)
	{
		___renewal_threshold_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
