﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "Mono_Web_Mono_Web_Util_SettingsMappingPlatform242231579.h"

// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Collections.Generic.List`1<Mono.Web.Util.SettingsMappingWhat>
struct List_1_t2935062715;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Web.Util.SettingsMapping
struct  SettingsMapping_t162400643  : public Il2CppObject
{
public:
	// System.String Mono.Web.Util.SettingsMapping::_sectionTypeName
	String_t* ____sectionTypeName_0;
	// System.Type Mono.Web.Util.SettingsMapping::_sectionType
	Type_t * ____sectionType_1;
	// System.String Mono.Web.Util.SettingsMapping::_mapperTypeName
	String_t* ____mapperTypeName_2;
	// System.Type Mono.Web.Util.SettingsMapping::_mapperType
	Type_t * ____mapperType_3;
	// Mono.Web.Util.SettingsMappingPlatform Mono.Web.Util.SettingsMapping::_platform
	int32_t ____platform_4;
	// System.Collections.Generic.List`1<Mono.Web.Util.SettingsMappingWhat> Mono.Web.Util.SettingsMapping::_whats
	List_1_t2935062715 * ____whats_5;

public:
	inline static int32_t get_offset_of__sectionTypeName_0() { return static_cast<int32_t>(offsetof(SettingsMapping_t162400643, ____sectionTypeName_0)); }
	inline String_t* get__sectionTypeName_0() const { return ____sectionTypeName_0; }
	inline String_t** get_address_of__sectionTypeName_0() { return &____sectionTypeName_0; }
	inline void set__sectionTypeName_0(String_t* value)
	{
		____sectionTypeName_0 = value;
		Il2CppCodeGenWriteBarrier(&____sectionTypeName_0, value);
	}

	inline static int32_t get_offset_of__sectionType_1() { return static_cast<int32_t>(offsetof(SettingsMapping_t162400643, ____sectionType_1)); }
	inline Type_t * get__sectionType_1() const { return ____sectionType_1; }
	inline Type_t ** get_address_of__sectionType_1() { return &____sectionType_1; }
	inline void set__sectionType_1(Type_t * value)
	{
		____sectionType_1 = value;
		Il2CppCodeGenWriteBarrier(&____sectionType_1, value);
	}

	inline static int32_t get_offset_of__mapperTypeName_2() { return static_cast<int32_t>(offsetof(SettingsMapping_t162400643, ____mapperTypeName_2)); }
	inline String_t* get__mapperTypeName_2() const { return ____mapperTypeName_2; }
	inline String_t** get_address_of__mapperTypeName_2() { return &____mapperTypeName_2; }
	inline void set__mapperTypeName_2(String_t* value)
	{
		____mapperTypeName_2 = value;
		Il2CppCodeGenWriteBarrier(&____mapperTypeName_2, value);
	}

	inline static int32_t get_offset_of__mapperType_3() { return static_cast<int32_t>(offsetof(SettingsMapping_t162400643, ____mapperType_3)); }
	inline Type_t * get__mapperType_3() const { return ____mapperType_3; }
	inline Type_t ** get_address_of__mapperType_3() { return &____mapperType_3; }
	inline void set__mapperType_3(Type_t * value)
	{
		____mapperType_3 = value;
		Il2CppCodeGenWriteBarrier(&____mapperType_3, value);
	}

	inline static int32_t get_offset_of__platform_4() { return static_cast<int32_t>(offsetof(SettingsMapping_t162400643, ____platform_4)); }
	inline int32_t get__platform_4() const { return ____platform_4; }
	inline int32_t* get_address_of__platform_4() { return &____platform_4; }
	inline void set__platform_4(int32_t value)
	{
		____platform_4 = value;
	}

	inline static int32_t get_offset_of__whats_5() { return static_cast<int32_t>(offsetof(SettingsMapping_t162400643, ____whats_5)); }
	inline List_1_t2935062715 * get__whats_5() const { return ____whats_5; }
	inline List_1_t2935062715 ** get_address_of__whats_5() { return &____whats_5; }
	inline void set__whats_5(List_1_t2935062715 * value)
	{
		____whats_5 = value;
		Il2CppCodeGenWriteBarrier(&____whats_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
