﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Soultion01_ch
struct  Soultion01_ch_t3285493964  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject Soultion01_ch::Soultion01_ball
	GameObject_t1113636619 * ___Soultion01_ball_2;
	// UnityEngine.GameObject Soultion01_ch::finger
	GameObject_t1113636619 * ___finger_3;
	// UnityEngine.GameObject Soultion01_ch::touchfinger
	GameObject_t1113636619 * ___touchfinger_4;
	// UnityEngine.GameObject Soultion01_ch::s_2
	GameObject_t1113636619 * ___s_2_5;

public:
	inline static int32_t get_offset_of_Soultion01_ball_2() { return static_cast<int32_t>(offsetof(Soultion01_ch_t3285493964, ___Soultion01_ball_2)); }
	inline GameObject_t1113636619 * get_Soultion01_ball_2() const { return ___Soultion01_ball_2; }
	inline GameObject_t1113636619 ** get_address_of_Soultion01_ball_2() { return &___Soultion01_ball_2; }
	inline void set_Soultion01_ball_2(GameObject_t1113636619 * value)
	{
		___Soultion01_ball_2 = value;
		Il2CppCodeGenWriteBarrier(&___Soultion01_ball_2, value);
	}

	inline static int32_t get_offset_of_finger_3() { return static_cast<int32_t>(offsetof(Soultion01_ch_t3285493964, ___finger_3)); }
	inline GameObject_t1113636619 * get_finger_3() const { return ___finger_3; }
	inline GameObject_t1113636619 ** get_address_of_finger_3() { return &___finger_3; }
	inline void set_finger_3(GameObject_t1113636619 * value)
	{
		___finger_3 = value;
		Il2CppCodeGenWriteBarrier(&___finger_3, value);
	}

	inline static int32_t get_offset_of_touchfinger_4() { return static_cast<int32_t>(offsetof(Soultion01_ch_t3285493964, ___touchfinger_4)); }
	inline GameObject_t1113636619 * get_touchfinger_4() const { return ___touchfinger_4; }
	inline GameObject_t1113636619 ** get_address_of_touchfinger_4() { return &___touchfinger_4; }
	inline void set_touchfinger_4(GameObject_t1113636619 * value)
	{
		___touchfinger_4 = value;
		Il2CppCodeGenWriteBarrier(&___touchfinger_4, value);
	}

	inline static int32_t get_offset_of_s_2_5() { return static_cast<int32_t>(offsetof(Soultion01_ch_t3285493964, ___s_2_5)); }
	inline GameObject_t1113636619 * get_s_2_5() const { return ___s_2_5; }
	inline GameObject_t1113636619 ** get_address_of_s_2_5() { return &___s_2_5; }
	inline void set_s_2_5(GameObject_t1113636619 * value)
	{
		___s_2_5 = value;
		Il2CppCodeGenWriteBarrier(&___s_2_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
