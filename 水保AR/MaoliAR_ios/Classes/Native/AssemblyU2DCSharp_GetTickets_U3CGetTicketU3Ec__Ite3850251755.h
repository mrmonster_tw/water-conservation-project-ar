﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// GetTickets
struct GetTickets_t911946028;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetTickets/<GetTicket>c__Iterator1
struct  U3CGetTicketU3Ec__Iterator1_t3850251755  : public Il2CppObject
{
public:
	// System.String GetTickets/<GetTicket>c__Iterator1::scenemode
	String_t* ___scenemode_0;
	// System.Int32 GetTickets/<GetTicket>c__Iterator1::<newNomber>__0
	int32_t ___U3CnewNomberU3E__0_1;
	// System.Int32 GetTickets/<GetTicket>c__Iterator1::nextHour
	int32_t ___nextHour_2;
	// System.Int32 GetTickets/<GetTicket>c__Iterator1::nextMinute
	int32_t ___nextMinute_3;
	// GetTickets GetTickets/<GetTicket>c__Iterator1::$this
	GetTickets_t911946028 * ___U24this_4;
	// System.Object GetTickets/<GetTicket>c__Iterator1::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean GetTickets/<GetTicket>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 GetTickets/<GetTicket>c__Iterator1::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_scenemode_0() { return static_cast<int32_t>(offsetof(U3CGetTicketU3Ec__Iterator1_t3850251755, ___scenemode_0)); }
	inline String_t* get_scenemode_0() const { return ___scenemode_0; }
	inline String_t** get_address_of_scenemode_0() { return &___scenemode_0; }
	inline void set_scenemode_0(String_t* value)
	{
		___scenemode_0 = value;
		Il2CppCodeGenWriteBarrier(&___scenemode_0, value);
	}

	inline static int32_t get_offset_of_U3CnewNomberU3E__0_1() { return static_cast<int32_t>(offsetof(U3CGetTicketU3Ec__Iterator1_t3850251755, ___U3CnewNomberU3E__0_1)); }
	inline int32_t get_U3CnewNomberU3E__0_1() const { return ___U3CnewNomberU3E__0_1; }
	inline int32_t* get_address_of_U3CnewNomberU3E__0_1() { return &___U3CnewNomberU3E__0_1; }
	inline void set_U3CnewNomberU3E__0_1(int32_t value)
	{
		___U3CnewNomberU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_nextHour_2() { return static_cast<int32_t>(offsetof(U3CGetTicketU3Ec__Iterator1_t3850251755, ___nextHour_2)); }
	inline int32_t get_nextHour_2() const { return ___nextHour_2; }
	inline int32_t* get_address_of_nextHour_2() { return &___nextHour_2; }
	inline void set_nextHour_2(int32_t value)
	{
		___nextHour_2 = value;
	}

	inline static int32_t get_offset_of_nextMinute_3() { return static_cast<int32_t>(offsetof(U3CGetTicketU3Ec__Iterator1_t3850251755, ___nextMinute_3)); }
	inline int32_t get_nextMinute_3() const { return ___nextMinute_3; }
	inline int32_t* get_address_of_nextMinute_3() { return &___nextMinute_3; }
	inline void set_nextMinute_3(int32_t value)
	{
		___nextMinute_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CGetTicketU3Ec__Iterator1_t3850251755, ___U24this_4)); }
	inline GetTickets_t911946028 * get_U24this_4() const { return ___U24this_4; }
	inline GetTickets_t911946028 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(GetTickets_t911946028 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CGetTicketU3Ec__Iterator1_t3850251755, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CGetTicketU3Ec__Iterator1_t3850251755, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CGetTicketU3Ec__Iterator1_t3850251755, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
