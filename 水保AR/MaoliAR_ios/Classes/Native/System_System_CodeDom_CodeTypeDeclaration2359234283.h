﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeTypeMember1555525554.h"
#include "mscorlib_System_Reflection_TypeAttributes113483779.h"

// System.CodeDom.CodeTypeReferenceCollection
struct CodeTypeReferenceCollection_t3857551471;
// System.CodeDom.CodeTypeMemberCollection
struct CodeTypeMemberCollection_t1296205520;
// System.CodeDom.CodeTypeParameterCollection
struct CodeTypeParameterCollection_t1882362546;
// System.EventHandler
struct EventHandler_t1348719766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeTypeDeclaration
struct  CodeTypeDeclaration_t2359234283  : public CodeTypeMember_t1555525554
{
public:
	// System.CodeDom.CodeTypeReferenceCollection System.CodeDom.CodeTypeDeclaration::baseTypes
	CodeTypeReferenceCollection_t3857551471 * ___baseTypes_8;
	// System.CodeDom.CodeTypeMemberCollection System.CodeDom.CodeTypeDeclaration::members
	CodeTypeMemberCollection_t1296205520 * ___members_9;
	// System.Reflection.TypeAttributes System.CodeDom.CodeTypeDeclaration::attributes
	int32_t ___attributes_10;
	// System.Boolean System.CodeDom.CodeTypeDeclaration::isEnum
	bool ___isEnum_11;
	// System.Boolean System.CodeDom.CodeTypeDeclaration::isStruct
	bool ___isStruct_12;
	// System.Boolean System.CodeDom.CodeTypeDeclaration::isPartial
	bool ___isPartial_13;
	// System.CodeDom.CodeTypeParameterCollection System.CodeDom.CodeTypeDeclaration::typeParameters
	CodeTypeParameterCollection_t1882362546 * ___typeParameters_14;
	// System.EventHandler System.CodeDom.CodeTypeDeclaration::PopulateBaseTypes
	EventHandler_t1348719766 * ___PopulateBaseTypes_15;
	// System.EventHandler System.CodeDom.CodeTypeDeclaration::PopulateMembers
	EventHandler_t1348719766 * ___PopulateMembers_16;

public:
	inline static int32_t get_offset_of_baseTypes_8() { return static_cast<int32_t>(offsetof(CodeTypeDeclaration_t2359234283, ___baseTypes_8)); }
	inline CodeTypeReferenceCollection_t3857551471 * get_baseTypes_8() const { return ___baseTypes_8; }
	inline CodeTypeReferenceCollection_t3857551471 ** get_address_of_baseTypes_8() { return &___baseTypes_8; }
	inline void set_baseTypes_8(CodeTypeReferenceCollection_t3857551471 * value)
	{
		___baseTypes_8 = value;
		Il2CppCodeGenWriteBarrier(&___baseTypes_8, value);
	}

	inline static int32_t get_offset_of_members_9() { return static_cast<int32_t>(offsetof(CodeTypeDeclaration_t2359234283, ___members_9)); }
	inline CodeTypeMemberCollection_t1296205520 * get_members_9() const { return ___members_9; }
	inline CodeTypeMemberCollection_t1296205520 ** get_address_of_members_9() { return &___members_9; }
	inline void set_members_9(CodeTypeMemberCollection_t1296205520 * value)
	{
		___members_9 = value;
		Il2CppCodeGenWriteBarrier(&___members_9, value);
	}

	inline static int32_t get_offset_of_attributes_10() { return static_cast<int32_t>(offsetof(CodeTypeDeclaration_t2359234283, ___attributes_10)); }
	inline int32_t get_attributes_10() const { return ___attributes_10; }
	inline int32_t* get_address_of_attributes_10() { return &___attributes_10; }
	inline void set_attributes_10(int32_t value)
	{
		___attributes_10 = value;
	}

	inline static int32_t get_offset_of_isEnum_11() { return static_cast<int32_t>(offsetof(CodeTypeDeclaration_t2359234283, ___isEnum_11)); }
	inline bool get_isEnum_11() const { return ___isEnum_11; }
	inline bool* get_address_of_isEnum_11() { return &___isEnum_11; }
	inline void set_isEnum_11(bool value)
	{
		___isEnum_11 = value;
	}

	inline static int32_t get_offset_of_isStruct_12() { return static_cast<int32_t>(offsetof(CodeTypeDeclaration_t2359234283, ___isStruct_12)); }
	inline bool get_isStruct_12() const { return ___isStruct_12; }
	inline bool* get_address_of_isStruct_12() { return &___isStruct_12; }
	inline void set_isStruct_12(bool value)
	{
		___isStruct_12 = value;
	}

	inline static int32_t get_offset_of_isPartial_13() { return static_cast<int32_t>(offsetof(CodeTypeDeclaration_t2359234283, ___isPartial_13)); }
	inline bool get_isPartial_13() const { return ___isPartial_13; }
	inline bool* get_address_of_isPartial_13() { return &___isPartial_13; }
	inline void set_isPartial_13(bool value)
	{
		___isPartial_13 = value;
	}

	inline static int32_t get_offset_of_typeParameters_14() { return static_cast<int32_t>(offsetof(CodeTypeDeclaration_t2359234283, ___typeParameters_14)); }
	inline CodeTypeParameterCollection_t1882362546 * get_typeParameters_14() const { return ___typeParameters_14; }
	inline CodeTypeParameterCollection_t1882362546 ** get_address_of_typeParameters_14() { return &___typeParameters_14; }
	inline void set_typeParameters_14(CodeTypeParameterCollection_t1882362546 * value)
	{
		___typeParameters_14 = value;
		Il2CppCodeGenWriteBarrier(&___typeParameters_14, value);
	}

	inline static int32_t get_offset_of_PopulateBaseTypes_15() { return static_cast<int32_t>(offsetof(CodeTypeDeclaration_t2359234283, ___PopulateBaseTypes_15)); }
	inline EventHandler_t1348719766 * get_PopulateBaseTypes_15() const { return ___PopulateBaseTypes_15; }
	inline EventHandler_t1348719766 ** get_address_of_PopulateBaseTypes_15() { return &___PopulateBaseTypes_15; }
	inline void set_PopulateBaseTypes_15(EventHandler_t1348719766 * value)
	{
		___PopulateBaseTypes_15 = value;
		Il2CppCodeGenWriteBarrier(&___PopulateBaseTypes_15, value);
	}

	inline static int32_t get_offset_of_PopulateMembers_16() { return static_cast<int32_t>(offsetof(CodeTypeDeclaration_t2359234283, ___PopulateMembers_16)); }
	inline EventHandler_t1348719766 * get_PopulateMembers_16() const { return ___PopulateMembers_16; }
	inline EventHandler_t1348719766 ** get_address_of_PopulateMembers_16() { return &___PopulateMembers_16; }
	inline void set_PopulateMembers_16(EventHandler_t1348719766 * value)
	{
		___PopulateMembers_16 = value;
		Il2CppCodeGenWriteBarrier(&___PopulateMembers_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
