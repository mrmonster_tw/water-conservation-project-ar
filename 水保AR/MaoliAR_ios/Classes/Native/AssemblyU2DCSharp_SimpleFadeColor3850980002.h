﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"

// SimpleFadeColor
struct SimpleFadeColor_t3850980002;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleFadeColor
struct  SimpleFadeColor_t3850980002  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color SimpleFadeColor::color1
	Color_t2555686324  ___color1_3;
	// UnityEngine.Color SimpleFadeColor::color2
	Color_t2555686324  ___color2_4;
	// UnityEngine.Color SimpleFadeColor::nowColor
	Color_t2555686324  ___nowColor_5;
	// System.Int32 SimpleFadeColor::_changeColors
	int32_t ____changeColors_6;
	// System.Int32 SimpleFadeColor::changeColor
	int32_t ___changeColor_7;

public:
	inline static int32_t get_offset_of_color1_3() { return static_cast<int32_t>(offsetof(SimpleFadeColor_t3850980002, ___color1_3)); }
	inline Color_t2555686324  get_color1_3() const { return ___color1_3; }
	inline Color_t2555686324 * get_address_of_color1_3() { return &___color1_3; }
	inline void set_color1_3(Color_t2555686324  value)
	{
		___color1_3 = value;
	}

	inline static int32_t get_offset_of_color2_4() { return static_cast<int32_t>(offsetof(SimpleFadeColor_t3850980002, ___color2_4)); }
	inline Color_t2555686324  get_color2_4() const { return ___color2_4; }
	inline Color_t2555686324 * get_address_of_color2_4() { return &___color2_4; }
	inline void set_color2_4(Color_t2555686324  value)
	{
		___color2_4 = value;
	}

	inline static int32_t get_offset_of_nowColor_5() { return static_cast<int32_t>(offsetof(SimpleFadeColor_t3850980002, ___nowColor_5)); }
	inline Color_t2555686324  get_nowColor_5() const { return ___nowColor_5; }
	inline Color_t2555686324 * get_address_of_nowColor_5() { return &___nowColor_5; }
	inline void set_nowColor_5(Color_t2555686324  value)
	{
		___nowColor_5 = value;
	}

	inline static int32_t get_offset_of__changeColors_6() { return static_cast<int32_t>(offsetof(SimpleFadeColor_t3850980002, ____changeColors_6)); }
	inline int32_t get__changeColors_6() const { return ____changeColors_6; }
	inline int32_t* get_address_of__changeColors_6() { return &____changeColors_6; }
	inline void set__changeColors_6(int32_t value)
	{
		____changeColors_6 = value;
	}

	inline static int32_t get_offset_of_changeColor_7() { return static_cast<int32_t>(offsetof(SimpleFadeColor_t3850980002, ___changeColor_7)); }
	inline int32_t get_changeColor_7() const { return ___changeColor_7; }
	inline int32_t* get_address_of_changeColor_7() { return &___changeColor_7; }
	inline void set_changeColor_7(int32_t value)
	{
		___changeColor_7 = value;
	}
};

struct SimpleFadeColor_t3850980002_StaticFields
{
public:
	// SimpleFadeColor SimpleFadeColor::instance
	SimpleFadeColor_t3850980002 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(SimpleFadeColor_t3850980002_StaticFields, ___instance_2)); }
	inline SimpleFadeColor_t3850980002 * get_instance_2() const { return ___instance_2; }
	inline SimpleFadeColor_t3850980002 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(SimpleFadeColor_t3850980002 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
