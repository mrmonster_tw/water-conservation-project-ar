﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3156163955.h"

// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.UrlMappingsSection
struct  UrlMappingsSection_t960595399  : public ConfigurationSection_t3156163955
{
public:

public:
};

struct UrlMappingsSection_t960595399_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Web.Configuration.UrlMappingsSection::enabledProp
	ConfigurationProperty_t3590861854 * ___enabledProp_17;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.UrlMappingsSection::urlMappingsProp
	ConfigurationProperty_t3590861854 * ___urlMappingsProp_18;
	// System.Configuration.ConfigurationPropertyCollection System.Web.Configuration.UrlMappingsSection::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_19;

public:
	inline static int32_t get_offset_of_enabledProp_17() { return static_cast<int32_t>(offsetof(UrlMappingsSection_t960595399_StaticFields, ___enabledProp_17)); }
	inline ConfigurationProperty_t3590861854 * get_enabledProp_17() const { return ___enabledProp_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_enabledProp_17() { return &___enabledProp_17; }
	inline void set_enabledProp_17(ConfigurationProperty_t3590861854 * value)
	{
		___enabledProp_17 = value;
		Il2CppCodeGenWriteBarrier(&___enabledProp_17, value);
	}

	inline static int32_t get_offset_of_urlMappingsProp_18() { return static_cast<int32_t>(offsetof(UrlMappingsSection_t960595399_StaticFields, ___urlMappingsProp_18)); }
	inline ConfigurationProperty_t3590861854 * get_urlMappingsProp_18() const { return ___urlMappingsProp_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_urlMappingsProp_18() { return &___urlMappingsProp_18; }
	inline void set_urlMappingsProp_18(ConfigurationProperty_t3590861854 * value)
	{
		___urlMappingsProp_18 = value;
		Il2CppCodeGenWriteBarrier(&___urlMappingsProp_18, value);
	}

	inline static int32_t get_offset_of_properties_19() { return static_cast<int32_t>(offsetof(UrlMappingsSection_t960595399_StaticFields, ___properties_19)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_19() const { return ___properties_19; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_19() { return &___properties_19; }
	inline void set_properties_19(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_19 = value;
		Il2CppCodeGenWriteBarrier(&___properties_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
