﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_TemplateControlParser3072921516.h"
#include "System_Web_System_Web_Configuration_PagesEnableSes3709987186.h"
#include "System_Web_System_Web_TraceMode4125462420.h"

// System.String
struct String_t;
// System.Web.UI.MainDirectiveAttribute`1<System.Int32>
struct MainDirectiveAttribute_1_t4076720260;
// System.Web.UI.MainDirectiveAttribute`1<System.String>
struct MainDirectiveAttribute_1_t2973225196;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.PageParser
struct  PageParser_t2342141287  : public TemplateControlParser_t3072921516
{
public:
	// System.Web.Configuration.PagesEnableSessionState System.Web.UI.PageParser::enableSessionState
	int32_t ___enableSessionState_61;
	// System.Boolean System.Web.UI.PageParser::enableViewStateMac
	bool ___enableViewStateMac_62;
	// System.Boolean System.Web.UI.PageParser::enableViewStateMacSet
	bool ___enableViewStateMacSet_63;
	// System.Boolean System.Web.UI.PageParser::smartNavigation
	bool ___smartNavigation_64;
	// System.Boolean System.Web.UI.PageParser::haveTrace
	bool ___haveTrace_65;
	// System.Boolean System.Web.UI.PageParser::trace
	bool ___trace_66;
	// System.Boolean System.Web.UI.PageParser::notBuffer
	bool ___notBuffer_67;
	// System.Web.TraceMode System.Web.UI.PageParser::tracemode
	int32_t ___tracemode_68;
	// System.String System.Web.UI.PageParser::contentType
	String_t* ___contentType_69;
	// System.Web.UI.MainDirectiveAttribute`1<System.Int32> System.Web.UI.PageParser::codepage
	MainDirectiveAttribute_1_t4076720260 * ___codepage_70;
	// System.Web.UI.MainDirectiveAttribute`1<System.String> System.Web.UI.PageParser::responseEncoding
	MainDirectiveAttribute_1_t2973225196 * ___responseEncoding_71;
	// System.Web.UI.MainDirectiveAttribute`1<System.Int32> System.Web.UI.PageParser::lcid
	MainDirectiveAttribute_1_t4076720260 * ___lcid_72;
	// System.Web.UI.MainDirectiveAttribute`1<System.String> System.Web.UI.PageParser::clientTarget
	MainDirectiveAttribute_1_t2973225196 * ___clientTarget_73;
	// System.Web.UI.MainDirectiveAttribute`1<System.String> System.Web.UI.PageParser::masterPage
	MainDirectiveAttribute_1_t2973225196 * ___masterPage_74;
	// System.Web.UI.MainDirectiveAttribute`1<System.String> System.Web.UI.PageParser::title
	MainDirectiveAttribute_1_t2973225196 * ___title_75;
	// System.Web.UI.MainDirectiveAttribute`1<System.String> System.Web.UI.PageParser::theme
	MainDirectiveAttribute_1_t2973225196 * ___theme_76;
	// System.String System.Web.UI.PageParser::culture
	String_t* ___culture_77;
	// System.String System.Web.UI.PageParser::uiculture
	String_t* ___uiculture_78;
	// System.String System.Web.UI.PageParser::errorPage
	String_t* ___errorPage_79;
	// System.Boolean System.Web.UI.PageParser::validateRequest
	bool ___validateRequest_80;
	// System.Boolean System.Web.UI.PageParser::async
	bool ___async_81;
	// System.Int32 System.Web.UI.PageParser::asyncTimeout
	int32_t ___asyncTimeout_82;
	// System.Type System.Web.UI.PageParser::masterType
	Type_t * ___masterType_83;
	// System.String System.Web.UI.PageParser::masterVirtualPath
	String_t* ___masterVirtualPath_84;
	// System.String System.Web.UI.PageParser::styleSheetTheme
	String_t* ___styleSheetTheme_85;
	// System.Boolean System.Web.UI.PageParser::enable_event_validation
	bool ___enable_event_validation_86;
	// System.Boolean System.Web.UI.PageParser::maintainScrollPositionOnPostBack
	bool ___maintainScrollPositionOnPostBack_87;
	// System.Int32 System.Web.UI.PageParser::maxPageStateFieldLength
	int32_t ___maxPageStateFieldLength_88;
	// System.Type System.Web.UI.PageParser::previousPageType
	Type_t * ___previousPageType_89;
	// System.String System.Web.UI.PageParser::previousPageVirtualPath
	String_t* ___previousPageVirtualPath_90;

public:
	inline static int32_t get_offset_of_enableSessionState_61() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___enableSessionState_61)); }
	inline int32_t get_enableSessionState_61() const { return ___enableSessionState_61; }
	inline int32_t* get_address_of_enableSessionState_61() { return &___enableSessionState_61; }
	inline void set_enableSessionState_61(int32_t value)
	{
		___enableSessionState_61 = value;
	}

	inline static int32_t get_offset_of_enableViewStateMac_62() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___enableViewStateMac_62)); }
	inline bool get_enableViewStateMac_62() const { return ___enableViewStateMac_62; }
	inline bool* get_address_of_enableViewStateMac_62() { return &___enableViewStateMac_62; }
	inline void set_enableViewStateMac_62(bool value)
	{
		___enableViewStateMac_62 = value;
	}

	inline static int32_t get_offset_of_enableViewStateMacSet_63() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___enableViewStateMacSet_63)); }
	inline bool get_enableViewStateMacSet_63() const { return ___enableViewStateMacSet_63; }
	inline bool* get_address_of_enableViewStateMacSet_63() { return &___enableViewStateMacSet_63; }
	inline void set_enableViewStateMacSet_63(bool value)
	{
		___enableViewStateMacSet_63 = value;
	}

	inline static int32_t get_offset_of_smartNavigation_64() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___smartNavigation_64)); }
	inline bool get_smartNavigation_64() const { return ___smartNavigation_64; }
	inline bool* get_address_of_smartNavigation_64() { return &___smartNavigation_64; }
	inline void set_smartNavigation_64(bool value)
	{
		___smartNavigation_64 = value;
	}

	inline static int32_t get_offset_of_haveTrace_65() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___haveTrace_65)); }
	inline bool get_haveTrace_65() const { return ___haveTrace_65; }
	inline bool* get_address_of_haveTrace_65() { return &___haveTrace_65; }
	inline void set_haveTrace_65(bool value)
	{
		___haveTrace_65 = value;
	}

	inline static int32_t get_offset_of_trace_66() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___trace_66)); }
	inline bool get_trace_66() const { return ___trace_66; }
	inline bool* get_address_of_trace_66() { return &___trace_66; }
	inline void set_trace_66(bool value)
	{
		___trace_66 = value;
	}

	inline static int32_t get_offset_of_notBuffer_67() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___notBuffer_67)); }
	inline bool get_notBuffer_67() const { return ___notBuffer_67; }
	inline bool* get_address_of_notBuffer_67() { return &___notBuffer_67; }
	inline void set_notBuffer_67(bool value)
	{
		___notBuffer_67 = value;
	}

	inline static int32_t get_offset_of_tracemode_68() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___tracemode_68)); }
	inline int32_t get_tracemode_68() const { return ___tracemode_68; }
	inline int32_t* get_address_of_tracemode_68() { return &___tracemode_68; }
	inline void set_tracemode_68(int32_t value)
	{
		___tracemode_68 = value;
	}

	inline static int32_t get_offset_of_contentType_69() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___contentType_69)); }
	inline String_t* get_contentType_69() const { return ___contentType_69; }
	inline String_t** get_address_of_contentType_69() { return &___contentType_69; }
	inline void set_contentType_69(String_t* value)
	{
		___contentType_69 = value;
		Il2CppCodeGenWriteBarrier(&___contentType_69, value);
	}

	inline static int32_t get_offset_of_codepage_70() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___codepage_70)); }
	inline MainDirectiveAttribute_1_t4076720260 * get_codepage_70() const { return ___codepage_70; }
	inline MainDirectiveAttribute_1_t4076720260 ** get_address_of_codepage_70() { return &___codepage_70; }
	inline void set_codepage_70(MainDirectiveAttribute_1_t4076720260 * value)
	{
		___codepage_70 = value;
		Il2CppCodeGenWriteBarrier(&___codepage_70, value);
	}

	inline static int32_t get_offset_of_responseEncoding_71() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___responseEncoding_71)); }
	inline MainDirectiveAttribute_1_t2973225196 * get_responseEncoding_71() const { return ___responseEncoding_71; }
	inline MainDirectiveAttribute_1_t2973225196 ** get_address_of_responseEncoding_71() { return &___responseEncoding_71; }
	inline void set_responseEncoding_71(MainDirectiveAttribute_1_t2973225196 * value)
	{
		___responseEncoding_71 = value;
		Il2CppCodeGenWriteBarrier(&___responseEncoding_71, value);
	}

	inline static int32_t get_offset_of_lcid_72() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___lcid_72)); }
	inline MainDirectiveAttribute_1_t4076720260 * get_lcid_72() const { return ___lcid_72; }
	inline MainDirectiveAttribute_1_t4076720260 ** get_address_of_lcid_72() { return &___lcid_72; }
	inline void set_lcid_72(MainDirectiveAttribute_1_t4076720260 * value)
	{
		___lcid_72 = value;
		Il2CppCodeGenWriteBarrier(&___lcid_72, value);
	}

	inline static int32_t get_offset_of_clientTarget_73() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___clientTarget_73)); }
	inline MainDirectiveAttribute_1_t2973225196 * get_clientTarget_73() const { return ___clientTarget_73; }
	inline MainDirectiveAttribute_1_t2973225196 ** get_address_of_clientTarget_73() { return &___clientTarget_73; }
	inline void set_clientTarget_73(MainDirectiveAttribute_1_t2973225196 * value)
	{
		___clientTarget_73 = value;
		Il2CppCodeGenWriteBarrier(&___clientTarget_73, value);
	}

	inline static int32_t get_offset_of_masterPage_74() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___masterPage_74)); }
	inline MainDirectiveAttribute_1_t2973225196 * get_masterPage_74() const { return ___masterPage_74; }
	inline MainDirectiveAttribute_1_t2973225196 ** get_address_of_masterPage_74() { return &___masterPage_74; }
	inline void set_masterPage_74(MainDirectiveAttribute_1_t2973225196 * value)
	{
		___masterPage_74 = value;
		Il2CppCodeGenWriteBarrier(&___masterPage_74, value);
	}

	inline static int32_t get_offset_of_title_75() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___title_75)); }
	inline MainDirectiveAttribute_1_t2973225196 * get_title_75() const { return ___title_75; }
	inline MainDirectiveAttribute_1_t2973225196 ** get_address_of_title_75() { return &___title_75; }
	inline void set_title_75(MainDirectiveAttribute_1_t2973225196 * value)
	{
		___title_75 = value;
		Il2CppCodeGenWriteBarrier(&___title_75, value);
	}

	inline static int32_t get_offset_of_theme_76() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___theme_76)); }
	inline MainDirectiveAttribute_1_t2973225196 * get_theme_76() const { return ___theme_76; }
	inline MainDirectiveAttribute_1_t2973225196 ** get_address_of_theme_76() { return &___theme_76; }
	inline void set_theme_76(MainDirectiveAttribute_1_t2973225196 * value)
	{
		___theme_76 = value;
		Il2CppCodeGenWriteBarrier(&___theme_76, value);
	}

	inline static int32_t get_offset_of_culture_77() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___culture_77)); }
	inline String_t* get_culture_77() const { return ___culture_77; }
	inline String_t** get_address_of_culture_77() { return &___culture_77; }
	inline void set_culture_77(String_t* value)
	{
		___culture_77 = value;
		Il2CppCodeGenWriteBarrier(&___culture_77, value);
	}

	inline static int32_t get_offset_of_uiculture_78() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___uiculture_78)); }
	inline String_t* get_uiculture_78() const { return ___uiculture_78; }
	inline String_t** get_address_of_uiculture_78() { return &___uiculture_78; }
	inline void set_uiculture_78(String_t* value)
	{
		___uiculture_78 = value;
		Il2CppCodeGenWriteBarrier(&___uiculture_78, value);
	}

	inline static int32_t get_offset_of_errorPage_79() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___errorPage_79)); }
	inline String_t* get_errorPage_79() const { return ___errorPage_79; }
	inline String_t** get_address_of_errorPage_79() { return &___errorPage_79; }
	inline void set_errorPage_79(String_t* value)
	{
		___errorPage_79 = value;
		Il2CppCodeGenWriteBarrier(&___errorPage_79, value);
	}

	inline static int32_t get_offset_of_validateRequest_80() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___validateRequest_80)); }
	inline bool get_validateRequest_80() const { return ___validateRequest_80; }
	inline bool* get_address_of_validateRequest_80() { return &___validateRequest_80; }
	inline void set_validateRequest_80(bool value)
	{
		___validateRequest_80 = value;
	}

	inline static int32_t get_offset_of_async_81() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___async_81)); }
	inline bool get_async_81() const { return ___async_81; }
	inline bool* get_address_of_async_81() { return &___async_81; }
	inline void set_async_81(bool value)
	{
		___async_81 = value;
	}

	inline static int32_t get_offset_of_asyncTimeout_82() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___asyncTimeout_82)); }
	inline int32_t get_asyncTimeout_82() const { return ___asyncTimeout_82; }
	inline int32_t* get_address_of_asyncTimeout_82() { return &___asyncTimeout_82; }
	inline void set_asyncTimeout_82(int32_t value)
	{
		___asyncTimeout_82 = value;
	}

	inline static int32_t get_offset_of_masterType_83() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___masterType_83)); }
	inline Type_t * get_masterType_83() const { return ___masterType_83; }
	inline Type_t ** get_address_of_masterType_83() { return &___masterType_83; }
	inline void set_masterType_83(Type_t * value)
	{
		___masterType_83 = value;
		Il2CppCodeGenWriteBarrier(&___masterType_83, value);
	}

	inline static int32_t get_offset_of_masterVirtualPath_84() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___masterVirtualPath_84)); }
	inline String_t* get_masterVirtualPath_84() const { return ___masterVirtualPath_84; }
	inline String_t** get_address_of_masterVirtualPath_84() { return &___masterVirtualPath_84; }
	inline void set_masterVirtualPath_84(String_t* value)
	{
		___masterVirtualPath_84 = value;
		Il2CppCodeGenWriteBarrier(&___masterVirtualPath_84, value);
	}

	inline static int32_t get_offset_of_styleSheetTheme_85() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___styleSheetTheme_85)); }
	inline String_t* get_styleSheetTheme_85() const { return ___styleSheetTheme_85; }
	inline String_t** get_address_of_styleSheetTheme_85() { return &___styleSheetTheme_85; }
	inline void set_styleSheetTheme_85(String_t* value)
	{
		___styleSheetTheme_85 = value;
		Il2CppCodeGenWriteBarrier(&___styleSheetTheme_85, value);
	}

	inline static int32_t get_offset_of_enable_event_validation_86() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___enable_event_validation_86)); }
	inline bool get_enable_event_validation_86() const { return ___enable_event_validation_86; }
	inline bool* get_address_of_enable_event_validation_86() { return &___enable_event_validation_86; }
	inline void set_enable_event_validation_86(bool value)
	{
		___enable_event_validation_86 = value;
	}

	inline static int32_t get_offset_of_maintainScrollPositionOnPostBack_87() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___maintainScrollPositionOnPostBack_87)); }
	inline bool get_maintainScrollPositionOnPostBack_87() const { return ___maintainScrollPositionOnPostBack_87; }
	inline bool* get_address_of_maintainScrollPositionOnPostBack_87() { return &___maintainScrollPositionOnPostBack_87; }
	inline void set_maintainScrollPositionOnPostBack_87(bool value)
	{
		___maintainScrollPositionOnPostBack_87 = value;
	}

	inline static int32_t get_offset_of_maxPageStateFieldLength_88() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___maxPageStateFieldLength_88)); }
	inline int32_t get_maxPageStateFieldLength_88() const { return ___maxPageStateFieldLength_88; }
	inline int32_t* get_address_of_maxPageStateFieldLength_88() { return &___maxPageStateFieldLength_88; }
	inline void set_maxPageStateFieldLength_88(int32_t value)
	{
		___maxPageStateFieldLength_88 = value;
	}

	inline static int32_t get_offset_of_previousPageType_89() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___previousPageType_89)); }
	inline Type_t * get_previousPageType_89() const { return ___previousPageType_89; }
	inline Type_t ** get_address_of_previousPageType_89() { return &___previousPageType_89; }
	inline void set_previousPageType_89(Type_t * value)
	{
		___previousPageType_89 = value;
		Il2CppCodeGenWriteBarrier(&___previousPageType_89, value);
	}

	inline static int32_t get_offset_of_previousPageVirtualPath_90() { return static_cast<int32_t>(offsetof(PageParser_t2342141287, ___previousPageVirtualPath_90)); }
	inline String_t* get_previousPageVirtualPath_90() const { return ___previousPageVirtualPath_90; }
	inline String_t** get_address_of_previousPageVirtualPath_90() { return &___previousPageVirtualPath_90; }
	inline void set_previousPageVirtualPath_90(String_t* value)
	{
		___previousPageVirtualPath_90 = value;
		Il2CppCodeGenWriteBarrier(&___previousPageVirtualPath_90, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
