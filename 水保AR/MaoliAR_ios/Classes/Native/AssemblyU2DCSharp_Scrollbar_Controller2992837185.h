﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.RectTransform[]
struct RectTransformU5BU5D_t107129948;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t2439009922;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Scrollbar_Controller
struct  Scrollbar_Controller_t2992837185  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform[] Scrollbar_Controller::target
	RectTransformU5BU5D_t107129948* ___target_2;
	// UnityEngine.UI.Image[] Scrollbar_Controller::dots
	ImageU5BU5D_t2439009922* ___dots_3;
	// UnityEngine.GameObject Scrollbar_Controller::grid
	GameObject_t1113636619 * ___grid_4;
	// System.Single[] Scrollbar_Controller::distance
	SingleU5BU5D_t1444911251* ___distance_5;
	// UnityEngine.AudioClip Scrollbar_Controller::swipe_sound
	AudioClip_t3680889665 * ___swipe_sound_6;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(Scrollbar_Controller_t2992837185, ___target_2)); }
	inline RectTransformU5BU5D_t107129948* get_target_2() const { return ___target_2; }
	inline RectTransformU5BU5D_t107129948** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(RectTransformU5BU5D_t107129948* value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier(&___target_2, value);
	}

	inline static int32_t get_offset_of_dots_3() { return static_cast<int32_t>(offsetof(Scrollbar_Controller_t2992837185, ___dots_3)); }
	inline ImageU5BU5D_t2439009922* get_dots_3() const { return ___dots_3; }
	inline ImageU5BU5D_t2439009922** get_address_of_dots_3() { return &___dots_3; }
	inline void set_dots_3(ImageU5BU5D_t2439009922* value)
	{
		___dots_3 = value;
		Il2CppCodeGenWriteBarrier(&___dots_3, value);
	}

	inline static int32_t get_offset_of_grid_4() { return static_cast<int32_t>(offsetof(Scrollbar_Controller_t2992837185, ___grid_4)); }
	inline GameObject_t1113636619 * get_grid_4() const { return ___grid_4; }
	inline GameObject_t1113636619 ** get_address_of_grid_4() { return &___grid_4; }
	inline void set_grid_4(GameObject_t1113636619 * value)
	{
		___grid_4 = value;
		Il2CppCodeGenWriteBarrier(&___grid_4, value);
	}

	inline static int32_t get_offset_of_distance_5() { return static_cast<int32_t>(offsetof(Scrollbar_Controller_t2992837185, ___distance_5)); }
	inline SingleU5BU5D_t1444911251* get_distance_5() const { return ___distance_5; }
	inline SingleU5BU5D_t1444911251** get_address_of_distance_5() { return &___distance_5; }
	inline void set_distance_5(SingleU5BU5D_t1444911251* value)
	{
		___distance_5 = value;
		Il2CppCodeGenWriteBarrier(&___distance_5, value);
	}

	inline static int32_t get_offset_of_swipe_sound_6() { return static_cast<int32_t>(offsetof(Scrollbar_Controller_t2992837185, ___swipe_sound_6)); }
	inline AudioClip_t3680889665 * get_swipe_sound_6() const { return ___swipe_sound_6; }
	inline AudioClip_t3680889665 ** get_address_of_swipe_sound_6() { return &___swipe_sound_6; }
	inline void set_swipe_sound_6(AudioClip_t3680889665 * value)
	{
		___swipe_sound_6 = value;
		Il2CppCodeGenWriteBarrier(&___swipe_sound_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
