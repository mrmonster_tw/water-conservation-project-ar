﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2760654312;
// Mono.Xml.XPath.Pattern
struct Pattern_t1136864796;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// Mono.Xml.Xsl.Operations.XslOperation
struct XslOperation_t2153241355;
// Mono.Xml.Xsl.XslStylesheet
struct XslStylesheet_t113441946;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.XslTemplate
struct  XslTemplate_t152263049  : public Il2CppObject
{
public:
	// System.Xml.XmlQualifiedName Mono.Xml.Xsl.XslTemplate::name
	XmlQualifiedName_t2760654312 * ___name_0;
	// Mono.Xml.XPath.Pattern Mono.Xml.Xsl.XslTemplate::match
	Pattern_t1136864796 * ___match_1;
	// System.Xml.XmlQualifiedName Mono.Xml.Xsl.XslTemplate::mode
	XmlQualifiedName_t2760654312 * ___mode_2;
	// System.Double Mono.Xml.Xsl.XslTemplate::priority
	double ___priority_3;
	// System.Collections.ArrayList Mono.Xml.Xsl.XslTemplate::parameters
	ArrayList_t2718874744 * ___parameters_4;
	// Mono.Xml.Xsl.Operations.XslOperation Mono.Xml.Xsl.XslTemplate::content
	XslOperation_t2153241355 * ___content_5;
	// System.Int32 Mono.Xml.Xsl.XslTemplate::Id
	int32_t ___Id_7;
	// Mono.Xml.Xsl.XslStylesheet Mono.Xml.Xsl.XslTemplate::style
	XslStylesheet_t113441946 * ___style_8;
	// System.Int32 Mono.Xml.Xsl.XslTemplate::stackSize
	int32_t ___stackSize_9;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(XslTemplate_t152263049, ___name_0)); }
	inline XmlQualifiedName_t2760654312 * get_name_0() const { return ___name_0; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(XmlQualifiedName_t2760654312 * value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_match_1() { return static_cast<int32_t>(offsetof(XslTemplate_t152263049, ___match_1)); }
	inline Pattern_t1136864796 * get_match_1() const { return ___match_1; }
	inline Pattern_t1136864796 ** get_address_of_match_1() { return &___match_1; }
	inline void set_match_1(Pattern_t1136864796 * value)
	{
		___match_1 = value;
		Il2CppCodeGenWriteBarrier(&___match_1, value);
	}

	inline static int32_t get_offset_of_mode_2() { return static_cast<int32_t>(offsetof(XslTemplate_t152263049, ___mode_2)); }
	inline XmlQualifiedName_t2760654312 * get_mode_2() const { return ___mode_2; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_mode_2() { return &___mode_2; }
	inline void set_mode_2(XmlQualifiedName_t2760654312 * value)
	{
		___mode_2 = value;
		Il2CppCodeGenWriteBarrier(&___mode_2, value);
	}

	inline static int32_t get_offset_of_priority_3() { return static_cast<int32_t>(offsetof(XslTemplate_t152263049, ___priority_3)); }
	inline double get_priority_3() const { return ___priority_3; }
	inline double* get_address_of_priority_3() { return &___priority_3; }
	inline void set_priority_3(double value)
	{
		___priority_3 = value;
	}

	inline static int32_t get_offset_of_parameters_4() { return static_cast<int32_t>(offsetof(XslTemplate_t152263049, ___parameters_4)); }
	inline ArrayList_t2718874744 * get_parameters_4() const { return ___parameters_4; }
	inline ArrayList_t2718874744 ** get_address_of_parameters_4() { return &___parameters_4; }
	inline void set_parameters_4(ArrayList_t2718874744 * value)
	{
		___parameters_4 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_4, value);
	}

	inline static int32_t get_offset_of_content_5() { return static_cast<int32_t>(offsetof(XslTemplate_t152263049, ___content_5)); }
	inline XslOperation_t2153241355 * get_content_5() const { return ___content_5; }
	inline XslOperation_t2153241355 ** get_address_of_content_5() { return &___content_5; }
	inline void set_content_5(XslOperation_t2153241355 * value)
	{
		___content_5 = value;
		Il2CppCodeGenWriteBarrier(&___content_5, value);
	}

	inline static int32_t get_offset_of_Id_7() { return static_cast<int32_t>(offsetof(XslTemplate_t152263049, ___Id_7)); }
	inline int32_t get_Id_7() const { return ___Id_7; }
	inline int32_t* get_address_of_Id_7() { return &___Id_7; }
	inline void set_Id_7(int32_t value)
	{
		___Id_7 = value;
	}

	inline static int32_t get_offset_of_style_8() { return static_cast<int32_t>(offsetof(XslTemplate_t152263049, ___style_8)); }
	inline XslStylesheet_t113441946 * get_style_8() const { return ___style_8; }
	inline XslStylesheet_t113441946 ** get_address_of_style_8() { return &___style_8; }
	inline void set_style_8(XslStylesheet_t113441946 * value)
	{
		___style_8 = value;
		Il2CppCodeGenWriteBarrier(&___style_8, value);
	}

	inline static int32_t get_offset_of_stackSize_9() { return static_cast<int32_t>(offsetof(XslTemplate_t152263049, ___stackSize_9)); }
	inline int32_t get_stackSize_9() const { return ___stackSize_9; }
	inline int32_t* get_address_of_stackSize_9() { return &___stackSize_9; }
	inline void set_stackSize_9(int32_t value)
	{
		___stackSize_9 = value;
	}
};

struct XslTemplate_t152263049_StaticFields
{
public:
	// System.Int32 Mono.Xml.Xsl.XslTemplate::nextId
	int32_t ___nextId_6;

public:
	inline static int32_t get_offset_of_nextId_6() { return static_cast<int32_t>(offsetof(XslTemplate_t152263049_StaticFields, ___nextId_6)); }
	inline int32_t get_nextId_6() const { return ___nextId_6; }
	inline int32_t* get_address_of_nextId_6() { return &___nextId_6; }
	inline void set_nextId_6(int32_t value)
	{
		___nextId_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
