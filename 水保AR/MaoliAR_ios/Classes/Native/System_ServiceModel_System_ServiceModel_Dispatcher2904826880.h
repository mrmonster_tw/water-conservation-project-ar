﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_Collections_Generic_Syn2751273846.h"

// System.ServiceModel.ServiceHostBase
struct ServiceHostBase_t3741910535;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.ChannelDispatcherCollection
struct  ChannelDispatcherCollection_t2904826880  : public SynchronizedCollection_1_t2751273846
{
public:
	// System.ServiceModel.ServiceHostBase System.ServiceModel.Dispatcher.ChannelDispatcherCollection::_service
	ServiceHostBase_t3741910535 * ____service_2;

public:
	inline static int32_t get_offset_of__service_2() { return static_cast<int32_t>(offsetof(ChannelDispatcherCollection_t2904826880, ____service_2)); }
	inline ServiceHostBase_t3741910535 * get__service_2() const { return ____service_2; }
	inline ServiceHostBase_t3741910535 ** get_address_of__service_2() { return &____service_2; }
	inline void set__service_2(ServiceHostBase_t3741910535 * value)
	{
		____service_2 = value;
		Il2CppCodeGenWriteBarrier(&____service_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
