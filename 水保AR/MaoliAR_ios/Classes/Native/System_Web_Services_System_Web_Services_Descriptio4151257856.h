﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_Services_System_Web_Services_Descriptio2466402210.h"

// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2760654312;
// System.Web.Services.Description.ServiceDescriptionFormatExtensionCollection
struct ServiceDescriptionFormatExtensionCollection_t2630153888;
// System.Web.Services.Description.Service
struct Service_t2672957899;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.Port
struct  Port_t4151257856  : public NamedItem_t2466402210
{
public:
	// System.Xml.XmlQualifiedName System.Web.Services.Description.Port::binding
	XmlQualifiedName_t2760654312 * ___binding_4;
	// System.Web.Services.Description.ServiceDescriptionFormatExtensionCollection System.Web.Services.Description.Port::extensions
	ServiceDescriptionFormatExtensionCollection_t2630153888 * ___extensions_5;
	// System.Web.Services.Description.Service System.Web.Services.Description.Port::service
	Service_t2672957899 * ___service_6;

public:
	inline static int32_t get_offset_of_binding_4() { return static_cast<int32_t>(offsetof(Port_t4151257856, ___binding_4)); }
	inline XmlQualifiedName_t2760654312 * get_binding_4() const { return ___binding_4; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_binding_4() { return &___binding_4; }
	inline void set_binding_4(XmlQualifiedName_t2760654312 * value)
	{
		___binding_4 = value;
		Il2CppCodeGenWriteBarrier(&___binding_4, value);
	}

	inline static int32_t get_offset_of_extensions_5() { return static_cast<int32_t>(offsetof(Port_t4151257856, ___extensions_5)); }
	inline ServiceDescriptionFormatExtensionCollection_t2630153888 * get_extensions_5() const { return ___extensions_5; }
	inline ServiceDescriptionFormatExtensionCollection_t2630153888 ** get_address_of_extensions_5() { return &___extensions_5; }
	inline void set_extensions_5(ServiceDescriptionFormatExtensionCollection_t2630153888 * value)
	{
		___extensions_5 = value;
		Il2CppCodeGenWriteBarrier(&___extensions_5, value);
	}

	inline static int32_t get_offset_of_service_6() { return static_cast<int32_t>(offsetof(Port_t4151257856, ___service_6)); }
	inline Service_t2672957899 * get_service_6() const { return ___service_6; }
	inline Service_t2672957899 ** get_address_of_service_6() { return &___service_6; }
	inline void set_service_6(Service_t2672957899 * value)
	{
		___service_6 = value;
		Il2CppCodeGenWriteBarrier(&___service_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
