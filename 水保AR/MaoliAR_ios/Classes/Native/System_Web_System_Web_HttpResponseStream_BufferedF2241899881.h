﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_HttpResponseStream_Bucket1453203164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpResponseStream/BufferedFileBucket
struct  BufferedFileBucket_t2241899881  : public Bucket_t1453203164
{
public:
	// System.String System.Web.HttpResponseStream/BufferedFileBucket::file
	String_t* ___file_1;
	// System.Int64 System.Web.HttpResponseStream/BufferedFileBucket::offset
	int64_t ___offset_2;
	// System.Int64 System.Web.HttpResponseStream/BufferedFileBucket::length
	int64_t ___length_3;

public:
	inline static int32_t get_offset_of_file_1() { return static_cast<int32_t>(offsetof(BufferedFileBucket_t2241899881, ___file_1)); }
	inline String_t* get_file_1() const { return ___file_1; }
	inline String_t** get_address_of_file_1() { return &___file_1; }
	inline void set_file_1(String_t* value)
	{
		___file_1 = value;
		Il2CppCodeGenWriteBarrier(&___file_1, value);
	}

	inline static int32_t get_offset_of_offset_2() { return static_cast<int32_t>(offsetof(BufferedFileBucket_t2241899881, ___offset_2)); }
	inline int64_t get_offset_2() const { return ___offset_2; }
	inline int64_t* get_address_of_offset_2() { return &___offset_2; }
	inline void set_offset_2(int64_t value)
	{
		___offset_2 = value;
	}

	inline static int32_t get_offset_of_length_3() { return static_cast<int32_t>(offsetof(BufferedFileBucket_t2241899881, ___length_3)); }
	inline int64_t get_length_3() const { return ___length_3; }
	inline int64_t* get_address_of_length_3() { return &___length_3; }
	inline void set_length_3(int64_t value)
	{
		___length_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
