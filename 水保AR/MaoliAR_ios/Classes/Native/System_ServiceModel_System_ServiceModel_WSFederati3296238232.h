﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_System_ServiceModel_WSFederatio751951473.h"

// System.ServiceModel.FederatedMessageSecurityOverHttp
struct FederatedMessageSecurityOverHttp_t2993240700;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.WSFederationHttpSecurity
struct  WSFederationHttpSecurity_t3296238232  : public Il2CppObject
{
public:
	// System.ServiceModel.WSFederationHttpSecurityMode System.ServiceModel.WSFederationHttpSecurity::mode
	int32_t ___mode_0;
	// System.ServiceModel.FederatedMessageSecurityOverHttp System.ServiceModel.WSFederationHttpSecurity::message
	FederatedMessageSecurityOverHttp_t2993240700 * ___message_1;

public:
	inline static int32_t get_offset_of_mode_0() { return static_cast<int32_t>(offsetof(WSFederationHttpSecurity_t3296238232, ___mode_0)); }
	inline int32_t get_mode_0() const { return ___mode_0; }
	inline int32_t* get_address_of_mode_0() { return &___mode_0; }
	inline void set_mode_0(int32_t value)
	{
		___mode_0 = value;
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(WSFederationHttpSecurity_t3296238232, ___message_1)); }
	inline FederatedMessageSecurityOverHttp_t2993240700 * get_message_1() const { return ___message_1; }
	inline FederatedMessageSecurityOverHttp_t2993240700 ** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(FederatedMessageSecurityOverHttp_t2993240700 * value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier(&___message_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
