﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// GetTickets
struct GetTickets_t911946028;
// RestFulsClass.RestFulClass/RestFul
struct RestFul_t2169253335;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestFulsClass.RestFulClass/RestFul
struct  RestFul_t2169253335  : public Il2CppObject
{
public:

public:
};

struct RestFul_t2169253335_StaticFields
{
public:
	// GetTickets RestFulsClass.RestFulClass/RestFul::This
	GetTickets_t911946028 * ___This_0;
	// RestFulsClass.RestFulClass/RestFul RestFulsClass.RestFulClass/RestFul::restFul
	RestFul_t2169253335 * ___restFul_1;
	// System.String RestFulsClass.RestFulClass/RestFul::myAddress
	String_t* ___myAddress_2;
	// System.String RestFulsClass.RestFulClass/RestFul::mySVC
	String_t* ___mySVC_3;
	// System.String RestFulsClass.RestFulClass/RestFul::FuncNormal
	String_t* ___FuncNormal_4;
	// System.String RestFulsClass.RestFulClass/RestFul::PlayerUUID
	String_t* ___PlayerUUID_5;

public:
	inline static int32_t get_offset_of_This_0() { return static_cast<int32_t>(offsetof(RestFul_t2169253335_StaticFields, ___This_0)); }
	inline GetTickets_t911946028 * get_This_0() const { return ___This_0; }
	inline GetTickets_t911946028 ** get_address_of_This_0() { return &___This_0; }
	inline void set_This_0(GetTickets_t911946028 * value)
	{
		___This_0 = value;
		Il2CppCodeGenWriteBarrier(&___This_0, value);
	}

	inline static int32_t get_offset_of_restFul_1() { return static_cast<int32_t>(offsetof(RestFul_t2169253335_StaticFields, ___restFul_1)); }
	inline RestFul_t2169253335 * get_restFul_1() const { return ___restFul_1; }
	inline RestFul_t2169253335 ** get_address_of_restFul_1() { return &___restFul_1; }
	inline void set_restFul_1(RestFul_t2169253335 * value)
	{
		___restFul_1 = value;
		Il2CppCodeGenWriteBarrier(&___restFul_1, value);
	}

	inline static int32_t get_offset_of_myAddress_2() { return static_cast<int32_t>(offsetof(RestFul_t2169253335_StaticFields, ___myAddress_2)); }
	inline String_t* get_myAddress_2() const { return ___myAddress_2; }
	inline String_t** get_address_of_myAddress_2() { return &___myAddress_2; }
	inline void set_myAddress_2(String_t* value)
	{
		___myAddress_2 = value;
		Il2CppCodeGenWriteBarrier(&___myAddress_2, value);
	}

	inline static int32_t get_offset_of_mySVC_3() { return static_cast<int32_t>(offsetof(RestFul_t2169253335_StaticFields, ___mySVC_3)); }
	inline String_t* get_mySVC_3() const { return ___mySVC_3; }
	inline String_t** get_address_of_mySVC_3() { return &___mySVC_3; }
	inline void set_mySVC_3(String_t* value)
	{
		___mySVC_3 = value;
		Il2CppCodeGenWriteBarrier(&___mySVC_3, value);
	}

	inline static int32_t get_offset_of_FuncNormal_4() { return static_cast<int32_t>(offsetof(RestFul_t2169253335_StaticFields, ___FuncNormal_4)); }
	inline String_t* get_FuncNormal_4() const { return ___FuncNormal_4; }
	inline String_t** get_address_of_FuncNormal_4() { return &___FuncNormal_4; }
	inline void set_FuncNormal_4(String_t* value)
	{
		___FuncNormal_4 = value;
		Il2CppCodeGenWriteBarrier(&___FuncNormal_4, value);
	}

	inline static int32_t get_offset_of_PlayerUUID_5() { return static_cast<int32_t>(offsetof(RestFul_t2169253335_StaticFields, ___PlayerUUID_5)); }
	inline String_t* get_PlayerUUID_5() const { return ___PlayerUUID_5; }
	inline String_t** get_address_of_PlayerUUID_5() { return &___PlayerUUID_5; }
	inline void set_PlayerUUID_5(String_t* value)
	{
		___PlayerUUID_5 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerUUID_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
