﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_C3754390252.h"

// System.ServiceModel.Channels.TcpConnectionPoolSettings
struct TcpConnectionPoolSettings_t1573330250;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.TcpTransportBindingElement
struct  TcpTransportBindingElement_t1965552937  : public ConnectionOrientedTransportBindingElement_t3754390252
{
public:
	// System.Int32 System.ServiceModel.Channels.TcpTransportBindingElement::listen_backlog
	int32_t ___listen_backlog_11;
	// System.Boolean System.ServiceModel.Channels.TcpTransportBindingElement::port_sharing_enabled
	bool ___port_sharing_enabled_12;
	// System.ServiceModel.Channels.TcpConnectionPoolSettings System.ServiceModel.Channels.TcpTransportBindingElement::pool
	TcpConnectionPoolSettings_t1573330250 * ___pool_13;

public:
	inline static int32_t get_offset_of_listen_backlog_11() { return static_cast<int32_t>(offsetof(TcpTransportBindingElement_t1965552937, ___listen_backlog_11)); }
	inline int32_t get_listen_backlog_11() const { return ___listen_backlog_11; }
	inline int32_t* get_address_of_listen_backlog_11() { return &___listen_backlog_11; }
	inline void set_listen_backlog_11(int32_t value)
	{
		___listen_backlog_11 = value;
	}

	inline static int32_t get_offset_of_port_sharing_enabled_12() { return static_cast<int32_t>(offsetof(TcpTransportBindingElement_t1965552937, ___port_sharing_enabled_12)); }
	inline bool get_port_sharing_enabled_12() const { return ___port_sharing_enabled_12; }
	inline bool* get_address_of_port_sharing_enabled_12() { return &___port_sharing_enabled_12; }
	inline void set_port_sharing_enabled_12(bool value)
	{
		___port_sharing_enabled_12 = value;
	}

	inline static int32_t get_offset_of_pool_13() { return static_cast<int32_t>(offsetof(TcpTransportBindingElement_t1965552937, ___pool_13)); }
	inline TcpConnectionPoolSettings_t1573330250 * get_pool_13() const { return ___pool_13; }
	inline TcpConnectionPoolSettings_t1573330250 ** get_address_of_pool_13() { return &___pool_13; }
	inline void set_pool_13(TcpConnectionPoolSettings_t1573330250 * value)
	{
		___pool_13 = value;
		Il2CppCodeGenWriteBarrier(&___pool_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
