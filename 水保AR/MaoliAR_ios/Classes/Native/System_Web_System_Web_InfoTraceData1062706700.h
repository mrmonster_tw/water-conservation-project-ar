﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.InfoTraceData
struct  InfoTraceData_t1062706700  : public Il2CppObject
{
public:
	// System.String System.Web.InfoTraceData::Category
	String_t* ___Category_0;
	// System.String System.Web.InfoTraceData::Message
	String_t* ___Message_1;
	// System.String System.Web.InfoTraceData::Exception
	String_t* ___Exception_2;
	// System.Double System.Web.InfoTraceData::TimeSinceFirst
	double ___TimeSinceFirst_3;
	// System.Double System.Web.InfoTraceData::TimeSinceLast
	double ___TimeSinceLast_4;
	// System.Boolean System.Web.InfoTraceData::IsWarning
	bool ___IsWarning_5;

public:
	inline static int32_t get_offset_of_Category_0() { return static_cast<int32_t>(offsetof(InfoTraceData_t1062706700, ___Category_0)); }
	inline String_t* get_Category_0() const { return ___Category_0; }
	inline String_t** get_address_of_Category_0() { return &___Category_0; }
	inline void set_Category_0(String_t* value)
	{
		___Category_0 = value;
		Il2CppCodeGenWriteBarrier(&___Category_0, value);
	}

	inline static int32_t get_offset_of_Message_1() { return static_cast<int32_t>(offsetof(InfoTraceData_t1062706700, ___Message_1)); }
	inline String_t* get_Message_1() const { return ___Message_1; }
	inline String_t** get_address_of_Message_1() { return &___Message_1; }
	inline void set_Message_1(String_t* value)
	{
		___Message_1 = value;
		Il2CppCodeGenWriteBarrier(&___Message_1, value);
	}

	inline static int32_t get_offset_of_Exception_2() { return static_cast<int32_t>(offsetof(InfoTraceData_t1062706700, ___Exception_2)); }
	inline String_t* get_Exception_2() const { return ___Exception_2; }
	inline String_t** get_address_of_Exception_2() { return &___Exception_2; }
	inline void set_Exception_2(String_t* value)
	{
		___Exception_2 = value;
		Il2CppCodeGenWriteBarrier(&___Exception_2, value);
	}

	inline static int32_t get_offset_of_TimeSinceFirst_3() { return static_cast<int32_t>(offsetof(InfoTraceData_t1062706700, ___TimeSinceFirst_3)); }
	inline double get_TimeSinceFirst_3() const { return ___TimeSinceFirst_3; }
	inline double* get_address_of_TimeSinceFirst_3() { return &___TimeSinceFirst_3; }
	inline void set_TimeSinceFirst_3(double value)
	{
		___TimeSinceFirst_3 = value;
	}

	inline static int32_t get_offset_of_TimeSinceLast_4() { return static_cast<int32_t>(offsetof(InfoTraceData_t1062706700, ___TimeSinceLast_4)); }
	inline double get_TimeSinceLast_4() const { return ___TimeSinceLast_4; }
	inline double* get_address_of_TimeSinceLast_4() { return &___TimeSinceLast_4; }
	inline void set_TimeSinceLast_4(double value)
	{
		___TimeSinceLast_4 = value;
	}

	inline static int32_t get_offset_of_IsWarning_5() { return static_cast<int32_t>(offsetof(InfoTraceData_t1062706700, ___IsWarning_5)); }
	inline bool get_IsWarning_5() const { return ___IsWarning_5; }
	inline bool* get_address_of_IsWarning_5() { return &___IsWarning_5; }
	inline void set_IsWarning_5(bool value)
	{
		___IsWarning_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
