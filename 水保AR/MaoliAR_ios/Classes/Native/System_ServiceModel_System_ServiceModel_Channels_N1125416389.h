﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_I3108779577.h"

// System.ServiceModel.Channels.NamedPipeReplyChannel
struct NamedPipeReplyChannel_t296821458;
// System.ServiceModel.Channels.Message
struct Message_t869514973;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.NamedPipeReplyChannel/NamedPipeRequestContext
struct  NamedPipeRequestContext_t1125416389  : public InternalRequestContext_t3108779577
{
public:
	// System.ServiceModel.Channels.NamedPipeReplyChannel System.ServiceModel.Channels.NamedPipeReplyChannel/NamedPipeRequestContext::owner
	NamedPipeReplyChannel_t296821458 * ___owner_1;
	// System.ServiceModel.Channels.Message System.ServiceModel.Channels.NamedPipeReplyChannel/NamedPipeRequestContext::request
	Message_t869514973 * ___request_2;

public:
	inline static int32_t get_offset_of_owner_1() { return static_cast<int32_t>(offsetof(NamedPipeRequestContext_t1125416389, ___owner_1)); }
	inline NamedPipeReplyChannel_t296821458 * get_owner_1() const { return ___owner_1; }
	inline NamedPipeReplyChannel_t296821458 ** get_address_of_owner_1() { return &___owner_1; }
	inline void set_owner_1(NamedPipeReplyChannel_t296821458 * value)
	{
		___owner_1 = value;
		Il2CppCodeGenWriteBarrier(&___owner_1, value);
	}

	inline static int32_t get_offset_of_request_2() { return static_cast<int32_t>(offsetof(NamedPipeRequestContext_t1125416389, ___request_2)); }
	inline Message_t869514973 * get_request_2() const { return ___request_2; }
	inline Message_t869514973 ** get_address_of_request_2() { return &___request_2; }
	inline void set_request_2(Message_t869514973 * value)
	{
		___request_2 = value;
		Il2CppCodeGenWriteBarrier(&___request_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
