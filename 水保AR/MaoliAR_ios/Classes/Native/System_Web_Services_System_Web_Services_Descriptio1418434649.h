﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_Services_System_Web_Services_Descriptio3177955060.h"
#include "System_Web_Services_System_Web_Services_Descriptio2844024356.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.SoapBinding
struct  SoapBinding_t1418434649  : public ServiceDescriptionFormatExtension_t3177955060
{
public:
	// System.Web.Services.Description.SoapBindingStyle System.Web.Services.Description.SoapBinding::style
	int32_t ___style_1;
	// System.String System.Web.Services.Description.SoapBinding::transport
	String_t* ___transport_2;

public:
	inline static int32_t get_offset_of_style_1() { return static_cast<int32_t>(offsetof(SoapBinding_t1418434649, ___style_1)); }
	inline int32_t get_style_1() const { return ___style_1; }
	inline int32_t* get_address_of_style_1() { return &___style_1; }
	inline void set_style_1(int32_t value)
	{
		___style_1 = value;
	}

	inline static int32_t get_offset_of_transport_2() { return static_cast<int32_t>(offsetof(SoapBinding_t1418434649, ___transport_2)); }
	inline String_t* get_transport_2() const { return ___transport_2; }
	inline String_t** get_address_of_transport_2() { return &___transport_2; }
	inline void set_transport_2(String_t* value)
	{
		___transport_2 = value;
		Il2CppCodeGenWriteBarrier(&___transport_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
