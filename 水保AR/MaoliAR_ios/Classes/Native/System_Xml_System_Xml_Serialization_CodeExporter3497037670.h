﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Xml.Serialization.MapCodeGenerator
struct MapCodeGenerator_t1608106344;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.CodeExporter
struct  CodeExporter_t3497037670  : public Il2CppObject
{
public:
	// System.Xml.Serialization.MapCodeGenerator System.Xml.Serialization.CodeExporter::codeGenerator
	MapCodeGenerator_t1608106344 * ___codeGenerator_0;

public:
	inline static int32_t get_offset_of_codeGenerator_0() { return static_cast<int32_t>(offsetof(CodeExporter_t3497037670, ___codeGenerator_0)); }
	inline MapCodeGenerator_t1608106344 * get_codeGenerator_0() const { return ___codeGenerator_0; }
	inline MapCodeGenerator_t1608106344 ** get_address_of_codeGenerator_0() { return &___codeGenerator_0; }
	inline void set_codeGenerator_0(MapCodeGenerator_t1608106344 * value)
	{
		___codeGenerator_0 = value;
		Il2CppCodeGenWriteBarrier(&___codeGenerator_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
