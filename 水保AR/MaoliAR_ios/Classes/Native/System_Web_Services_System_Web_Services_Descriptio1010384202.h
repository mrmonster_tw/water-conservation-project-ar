﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_Services_System_Web_Services_Descriptio2466402210.h"

// System.Web.Services.Description.OperationFaultCollection
struct OperationFaultCollection_t3586254674;
// System.Web.Services.Description.OperationMessageCollection
struct OperationMessageCollection_t3992193545;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Web.Services.Description.PortType
struct PortType_t3119525474;
// System.Web.Services.Description.ServiceDescriptionFormatExtensionCollection
struct ServiceDescriptionFormatExtensionCollection_t2630153888;
// System.Char[]
struct CharU5BU5D_t3528271667;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.Operation
struct  Operation_t1010384202  : public NamedItem_t2466402210
{
public:
	// System.Web.Services.Description.OperationFaultCollection System.Web.Services.Description.Operation::faults
	OperationFaultCollection_t3586254674 * ___faults_4;
	// System.Web.Services.Description.OperationMessageCollection System.Web.Services.Description.Operation::messages
	OperationMessageCollection_t3992193545 * ___messages_5;
	// System.String[] System.Web.Services.Description.Operation::parameterOrder
	StringU5BU5D_t1281789340* ___parameterOrder_6;
	// System.Web.Services.Description.PortType System.Web.Services.Description.Operation::portType
	PortType_t3119525474 * ___portType_7;
	// System.Web.Services.Description.ServiceDescriptionFormatExtensionCollection System.Web.Services.Description.Operation::extensions
	ServiceDescriptionFormatExtensionCollection_t2630153888 * ___extensions_8;

public:
	inline static int32_t get_offset_of_faults_4() { return static_cast<int32_t>(offsetof(Operation_t1010384202, ___faults_4)); }
	inline OperationFaultCollection_t3586254674 * get_faults_4() const { return ___faults_4; }
	inline OperationFaultCollection_t3586254674 ** get_address_of_faults_4() { return &___faults_4; }
	inline void set_faults_4(OperationFaultCollection_t3586254674 * value)
	{
		___faults_4 = value;
		Il2CppCodeGenWriteBarrier(&___faults_4, value);
	}

	inline static int32_t get_offset_of_messages_5() { return static_cast<int32_t>(offsetof(Operation_t1010384202, ___messages_5)); }
	inline OperationMessageCollection_t3992193545 * get_messages_5() const { return ___messages_5; }
	inline OperationMessageCollection_t3992193545 ** get_address_of_messages_5() { return &___messages_5; }
	inline void set_messages_5(OperationMessageCollection_t3992193545 * value)
	{
		___messages_5 = value;
		Il2CppCodeGenWriteBarrier(&___messages_5, value);
	}

	inline static int32_t get_offset_of_parameterOrder_6() { return static_cast<int32_t>(offsetof(Operation_t1010384202, ___parameterOrder_6)); }
	inline StringU5BU5D_t1281789340* get_parameterOrder_6() const { return ___parameterOrder_6; }
	inline StringU5BU5D_t1281789340** get_address_of_parameterOrder_6() { return &___parameterOrder_6; }
	inline void set_parameterOrder_6(StringU5BU5D_t1281789340* value)
	{
		___parameterOrder_6 = value;
		Il2CppCodeGenWriteBarrier(&___parameterOrder_6, value);
	}

	inline static int32_t get_offset_of_portType_7() { return static_cast<int32_t>(offsetof(Operation_t1010384202, ___portType_7)); }
	inline PortType_t3119525474 * get_portType_7() const { return ___portType_7; }
	inline PortType_t3119525474 ** get_address_of_portType_7() { return &___portType_7; }
	inline void set_portType_7(PortType_t3119525474 * value)
	{
		___portType_7 = value;
		Il2CppCodeGenWriteBarrier(&___portType_7, value);
	}

	inline static int32_t get_offset_of_extensions_8() { return static_cast<int32_t>(offsetof(Operation_t1010384202, ___extensions_8)); }
	inline ServiceDescriptionFormatExtensionCollection_t2630153888 * get_extensions_8() const { return ___extensions_8; }
	inline ServiceDescriptionFormatExtensionCollection_t2630153888 ** get_address_of_extensions_8() { return &___extensions_8; }
	inline void set_extensions_8(ServiceDescriptionFormatExtensionCollection_t2630153888 * value)
	{
		___extensions_8 = value;
		Il2CppCodeGenWriteBarrier(&___extensions_8, value);
	}
};

struct Operation_t1010384202_StaticFields
{
public:
	// System.Char[] System.Web.Services.Description.Operation::wsChars
	CharU5BU5D_t3528271667* ___wsChars_9;

public:
	inline static int32_t get_offset_of_wsChars_9() { return static_cast<int32_t>(offsetof(Operation_t1010384202_StaticFields, ___wsChars_9)); }
	inline CharU5BU5D_t3528271667* get_wsChars_9() const { return ___wsChars_9; }
	inline CharU5BU5D_t3528271667** get_address_of_wsChars_9() { return &___wsChars_9; }
	inline void set_wsChars_9(CharU5BU5D_t3528271667* value)
	{
		___wsChars_9 = value;
		Il2CppCodeGenWriteBarrier(&___wsChars_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
