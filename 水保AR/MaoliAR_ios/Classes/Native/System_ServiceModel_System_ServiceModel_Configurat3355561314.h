﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.ServiceModel.Channels.AddressHeaderCollection
struct AddressHeaderCollection_t331929339;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.AddressHeaderCollectionElement
struct  AddressHeaderCollectionElement_t3355561314  : public ConfigurationElement_t3318566633
{
public:
	// System.ServiceModel.Channels.AddressHeaderCollection System.ServiceModel.Configuration.AddressHeaderCollectionElement::_headers
	AddressHeaderCollection_t331929339 * ____headers_13;

public:
	inline static int32_t get_offset_of__headers_13() { return static_cast<int32_t>(offsetof(AddressHeaderCollectionElement_t3355561314, ____headers_13)); }
	inline AddressHeaderCollection_t331929339 * get__headers_13() const { return ____headers_13; }
	inline AddressHeaderCollection_t331929339 ** get_address_of__headers_13() { return &____headers_13; }
	inline void set__headers_13(AddressHeaderCollection_t331929339 * value)
	{
		____headers_13 = value;
		Il2CppCodeGenWriteBarrier(&____headers_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
