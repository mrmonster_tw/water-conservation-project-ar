﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Web_System_Web_Util_SearchPattern_OpCode642418941.h"

// System.String
struct String_t;
// System.Web.Util.SearchPattern/Op
struct Op_t1428817098;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Util.SearchPattern/Op
struct  Op_t1428817098  : public Il2CppObject
{
public:
	// System.Web.Util.SearchPattern/OpCode System.Web.Util.SearchPattern/Op::Code
	int32_t ___Code_0;
	// System.String System.Web.Util.SearchPattern/Op::Argument
	String_t* ___Argument_1;
	// System.Web.Util.SearchPattern/Op System.Web.Util.SearchPattern/Op::Next
	Op_t1428817098 * ___Next_2;

public:
	inline static int32_t get_offset_of_Code_0() { return static_cast<int32_t>(offsetof(Op_t1428817098, ___Code_0)); }
	inline int32_t get_Code_0() const { return ___Code_0; }
	inline int32_t* get_address_of_Code_0() { return &___Code_0; }
	inline void set_Code_0(int32_t value)
	{
		___Code_0 = value;
	}

	inline static int32_t get_offset_of_Argument_1() { return static_cast<int32_t>(offsetof(Op_t1428817098, ___Argument_1)); }
	inline String_t* get_Argument_1() const { return ___Argument_1; }
	inline String_t** get_address_of_Argument_1() { return &___Argument_1; }
	inline void set_Argument_1(String_t* value)
	{
		___Argument_1 = value;
		Il2CppCodeGenWriteBarrier(&___Argument_1, value);
	}

	inline static int32_t get_offset_of_Next_2() { return static_cast<int32_t>(offsetof(Op_t1428817098, ___Next_2)); }
	inline Op_t1428817098 * get_Next_2() const { return ___Next_2; }
	inline Op_t1428817098 ** get_address_of_Next_2() { return &___Next_2; }
	inline void set_Next_2(Op_t1428817098 * value)
	{
		___Next_2 = value;
		Il2CppCodeGenWriteBarrier(&___Next_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
