﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Xml.Serialization.CodeIdentifiers
struct CodeIdentifiers_t4095039290;
// System.Collections.Specialized.StringCollection
struct StringCollection_t167406615;
// System.Collections.Hashtable
struct Hashtable_t1853889766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.ImportContext
struct  ImportContext_t1801135953  : public Il2CppObject
{
public:
	// System.Boolean System.Xml.Serialization.ImportContext::_shareTypes
	bool ____shareTypes_0;
	// System.Xml.Serialization.CodeIdentifiers System.Xml.Serialization.ImportContext::_typeIdentifiers
	CodeIdentifiers_t4095039290 * ____typeIdentifiers_1;
	// System.Collections.Specialized.StringCollection System.Xml.Serialization.ImportContext::_warnings
	StringCollection_t167406615 * ____warnings_2;
	// System.Collections.Hashtable System.Xml.Serialization.ImportContext::MappedTypes
	Hashtable_t1853889766 * ___MappedTypes_3;
	// System.Collections.Hashtable System.Xml.Serialization.ImportContext::DataMappedTypes
	Hashtable_t1853889766 * ___DataMappedTypes_4;
	// System.Collections.Hashtable System.Xml.Serialization.ImportContext::SharedAnonymousTypes
	Hashtable_t1853889766 * ___SharedAnonymousTypes_5;

public:
	inline static int32_t get_offset_of__shareTypes_0() { return static_cast<int32_t>(offsetof(ImportContext_t1801135953, ____shareTypes_0)); }
	inline bool get__shareTypes_0() const { return ____shareTypes_0; }
	inline bool* get_address_of__shareTypes_0() { return &____shareTypes_0; }
	inline void set__shareTypes_0(bool value)
	{
		____shareTypes_0 = value;
	}

	inline static int32_t get_offset_of__typeIdentifiers_1() { return static_cast<int32_t>(offsetof(ImportContext_t1801135953, ____typeIdentifiers_1)); }
	inline CodeIdentifiers_t4095039290 * get__typeIdentifiers_1() const { return ____typeIdentifiers_1; }
	inline CodeIdentifiers_t4095039290 ** get_address_of__typeIdentifiers_1() { return &____typeIdentifiers_1; }
	inline void set__typeIdentifiers_1(CodeIdentifiers_t4095039290 * value)
	{
		____typeIdentifiers_1 = value;
		Il2CppCodeGenWriteBarrier(&____typeIdentifiers_1, value);
	}

	inline static int32_t get_offset_of__warnings_2() { return static_cast<int32_t>(offsetof(ImportContext_t1801135953, ____warnings_2)); }
	inline StringCollection_t167406615 * get__warnings_2() const { return ____warnings_2; }
	inline StringCollection_t167406615 ** get_address_of__warnings_2() { return &____warnings_2; }
	inline void set__warnings_2(StringCollection_t167406615 * value)
	{
		____warnings_2 = value;
		Il2CppCodeGenWriteBarrier(&____warnings_2, value);
	}

	inline static int32_t get_offset_of_MappedTypes_3() { return static_cast<int32_t>(offsetof(ImportContext_t1801135953, ___MappedTypes_3)); }
	inline Hashtable_t1853889766 * get_MappedTypes_3() const { return ___MappedTypes_3; }
	inline Hashtable_t1853889766 ** get_address_of_MappedTypes_3() { return &___MappedTypes_3; }
	inline void set_MappedTypes_3(Hashtable_t1853889766 * value)
	{
		___MappedTypes_3 = value;
		Il2CppCodeGenWriteBarrier(&___MappedTypes_3, value);
	}

	inline static int32_t get_offset_of_DataMappedTypes_4() { return static_cast<int32_t>(offsetof(ImportContext_t1801135953, ___DataMappedTypes_4)); }
	inline Hashtable_t1853889766 * get_DataMappedTypes_4() const { return ___DataMappedTypes_4; }
	inline Hashtable_t1853889766 ** get_address_of_DataMappedTypes_4() { return &___DataMappedTypes_4; }
	inline void set_DataMappedTypes_4(Hashtable_t1853889766 * value)
	{
		___DataMappedTypes_4 = value;
		Il2CppCodeGenWriteBarrier(&___DataMappedTypes_4, value);
	}

	inline static int32_t get_offset_of_SharedAnonymousTypes_5() { return static_cast<int32_t>(offsetof(ImportContext_t1801135953, ___SharedAnonymousTypes_5)); }
	inline Hashtable_t1853889766 * get_SharedAnonymousTypes_5() const { return ___SharedAnonymousTypes_5; }
	inline Hashtable_t1853889766 ** get_address_of_SharedAnonymousTypes_5() { return &___SharedAnonymousTypes_5; }
	inline void set_SharedAnonymousTypes_5(Hashtable_t1853889766 * value)
	{
		___SharedAnonymousTypes_5 = value;
		Il2CppCodeGenWriteBarrier(&___SharedAnonymousTypes_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
