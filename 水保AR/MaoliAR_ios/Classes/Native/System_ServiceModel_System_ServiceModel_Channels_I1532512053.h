﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Ch259291852.h"

// System.Uri
struct Uri_t100236324;
// System.Func`2<System.TimeSpan,System.Object>
struct Func_2_t3894548565;
// System.Threading.Thread
struct Thread_t2300836069;
// System.IAsyncResult
struct IAsyncResult_t767004451;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.InternalChannelListenerBase`1<System.Object>
struct  InternalChannelListenerBase_1_t1532512053  : public ChannelListenerBase_1_t259291852
{
public:
	// System.Uri System.ServiceModel.Channels.InternalChannelListenerBase`1::listen_uri
	Uri_t100236324 * ___listen_uri_12;
	// System.Func`2<System.TimeSpan,TChannel> System.ServiceModel.Channels.InternalChannelListenerBase`1::accept_channel_delegate
	Func_2_t3894548565 * ___accept_channel_delegate_13;
	// System.Threading.Thread System.ServiceModel.Channels.InternalChannelListenerBase`1::<CurrentAsyncThread>k__BackingField
	Thread_t2300836069 * ___U3CCurrentAsyncThreadU3Ek__BackingField_14;
	// System.IAsyncResult System.ServiceModel.Channels.InternalChannelListenerBase`1::<CurrentAsyncResult>k__BackingField
	Il2CppObject * ___U3CCurrentAsyncResultU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_listen_uri_12() { return static_cast<int32_t>(offsetof(InternalChannelListenerBase_1_t1532512053, ___listen_uri_12)); }
	inline Uri_t100236324 * get_listen_uri_12() const { return ___listen_uri_12; }
	inline Uri_t100236324 ** get_address_of_listen_uri_12() { return &___listen_uri_12; }
	inline void set_listen_uri_12(Uri_t100236324 * value)
	{
		___listen_uri_12 = value;
		Il2CppCodeGenWriteBarrier(&___listen_uri_12, value);
	}

	inline static int32_t get_offset_of_accept_channel_delegate_13() { return static_cast<int32_t>(offsetof(InternalChannelListenerBase_1_t1532512053, ___accept_channel_delegate_13)); }
	inline Func_2_t3894548565 * get_accept_channel_delegate_13() const { return ___accept_channel_delegate_13; }
	inline Func_2_t3894548565 ** get_address_of_accept_channel_delegate_13() { return &___accept_channel_delegate_13; }
	inline void set_accept_channel_delegate_13(Func_2_t3894548565 * value)
	{
		___accept_channel_delegate_13 = value;
		Il2CppCodeGenWriteBarrier(&___accept_channel_delegate_13, value);
	}

	inline static int32_t get_offset_of_U3CCurrentAsyncThreadU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(InternalChannelListenerBase_1_t1532512053, ___U3CCurrentAsyncThreadU3Ek__BackingField_14)); }
	inline Thread_t2300836069 * get_U3CCurrentAsyncThreadU3Ek__BackingField_14() const { return ___U3CCurrentAsyncThreadU3Ek__BackingField_14; }
	inline Thread_t2300836069 ** get_address_of_U3CCurrentAsyncThreadU3Ek__BackingField_14() { return &___U3CCurrentAsyncThreadU3Ek__BackingField_14; }
	inline void set_U3CCurrentAsyncThreadU3Ek__BackingField_14(Thread_t2300836069 * value)
	{
		___U3CCurrentAsyncThreadU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCurrentAsyncThreadU3Ek__BackingField_14, value);
	}

	inline static int32_t get_offset_of_U3CCurrentAsyncResultU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(InternalChannelListenerBase_1_t1532512053, ___U3CCurrentAsyncResultU3Ek__BackingField_15)); }
	inline Il2CppObject * get_U3CCurrentAsyncResultU3Ek__BackingField_15() const { return ___U3CCurrentAsyncResultU3Ek__BackingField_15; }
	inline Il2CppObject ** get_address_of_U3CCurrentAsyncResultU3Ek__BackingField_15() { return &___U3CCurrentAsyncResultU3Ek__BackingField_15; }
	inline void set_U3CCurrentAsyncResultU3Ek__BackingField_15(Il2CppObject * value)
	{
		___U3CCurrentAsyncResultU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCurrentAsyncResultU3Ek__BackingField_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
