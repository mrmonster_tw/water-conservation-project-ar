﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_HtmlControls_HtmlContainer641877197.h"

// System.Web.UI.HtmlControls.HtmlTableCellCollection
struct HtmlTableCellCollection_t1920277368;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.HtmlControls.HtmlTableRow
struct  HtmlTableRow_t3030096891  : public HtmlContainerControl_t641877197
{
public:
	// System.Web.UI.HtmlControls.HtmlTableCellCollection System.Web.UI.HtmlControls.HtmlTableRow::_cells
	HtmlTableCellCollection_t1920277368 * ____cells_30;

public:
	inline static int32_t get_offset_of__cells_30() { return static_cast<int32_t>(offsetof(HtmlTableRow_t3030096891, ____cells_30)); }
	inline HtmlTableCellCollection_t1920277368 * get__cells_30() const { return ____cells_30; }
	inline HtmlTableCellCollection_t1920277368 ** get_address_of__cells_30() { return &____cells_30; }
	inline void set__cells_30(HtmlTableCellCollection_t1920277368 * value)
	{
		____cells_30 = value;
		Il2CppCodeGenWriteBarrier(&____cells_30, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
