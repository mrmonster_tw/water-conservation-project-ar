﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeTypeDeclaration2359234283.h"

// System.CodeDom.CodeParameterDeclarationExpressionCollection
struct CodeParameterDeclarationExpressionCollection_t3391789433;
// System.CodeDom.CodeTypeReference
struct CodeTypeReference_t3809997434;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeTypeDelegate
struct  CodeTypeDelegate_t2896700083  : public CodeTypeDeclaration_t2359234283
{
public:
	// System.CodeDom.CodeParameterDeclarationExpressionCollection System.CodeDom.CodeTypeDelegate::parameters
	CodeParameterDeclarationExpressionCollection_t3391789433 * ___parameters_17;
	// System.CodeDom.CodeTypeReference System.CodeDom.CodeTypeDelegate::returnType
	CodeTypeReference_t3809997434 * ___returnType_18;

public:
	inline static int32_t get_offset_of_parameters_17() { return static_cast<int32_t>(offsetof(CodeTypeDelegate_t2896700083, ___parameters_17)); }
	inline CodeParameterDeclarationExpressionCollection_t3391789433 * get_parameters_17() const { return ___parameters_17; }
	inline CodeParameterDeclarationExpressionCollection_t3391789433 ** get_address_of_parameters_17() { return &___parameters_17; }
	inline void set_parameters_17(CodeParameterDeclarationExpressionCollection_t3391789433 * value)
	{
		___parameters_17 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_17, value);
	}

	inline static int32_t get_offset_of_returnType_18() { return static_cast<int32_t>(offsetof(CodeTypeDelegate_t2896700083, ___returnType_18)); }
	inline CodeTypeReference_t3809997434 * get_returnType_18() const { return ___returnType_18; }
	inline CodeTypeReference_t3809997434 ** get_address_of_returnType_18() { return &___returnType_18; }
	inline void set_returnType_18(CodeTypeReference_t3809997434 * value)
	{
		___returnType_18 = value;
		Il2CppCodeGenWriteBarrier(&___returnType_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
