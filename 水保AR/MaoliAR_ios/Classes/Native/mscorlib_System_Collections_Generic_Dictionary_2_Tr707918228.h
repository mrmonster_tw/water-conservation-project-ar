﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_MulticastDelegate157516450.h"

// System.Uri
struct Uri_t100236324;
// System.Collections.Generic.KeyedByTypeCollection`1<System.ServiceModel.Description.IEndpointBehavior>
struct KeyedByTypeCollection_1_t4222441378;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Transform`1<System.Uri,System.Collections.Generic.KeyedByTypeCollection`1<System.ServiceModel.Description.IEndpointBehavior>,System.Uri>
struct  Transform_1_t707918228  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
