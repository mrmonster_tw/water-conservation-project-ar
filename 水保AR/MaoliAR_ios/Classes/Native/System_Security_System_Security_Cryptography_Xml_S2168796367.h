﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Security.Cryptography.Xml.Signature
struct Signature_t754089620;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t932037087;
// System.Xml.XmlDocument
struct XmlDocument_t2837193595;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.Xml.XmlElement
struct XmlElement_t561603118;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Xml.XmlResolver
struct XmlResolver_t626023767;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Char[]
struct CharU5BU5D_t3528271667;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.Xml.SignedXml
struct  SignedXml_t2168796367  : public Il2CppObject
{
public:
	// System.Security.Cryptography.Xml.Signature System.Security.Cryptography.Xml.SignedXml::m_signature
	Signature_t754089620 * ___m_signature_0;
	// System.Security.Cryptography.AsymmetricAlgorithm System.Security.Cryptography.Xml.SignedXml::key
	AsymmetricAlgorithm_t932037087 * ___key_1;
	// System.Xml.XmlDocument System.Security.Cryptography.Xml.SignedXml::envdoc
	XmlDocument_t2837193595 * ___envdoc_2;
	// System.Collections.IEnumerator System.Security.Cryptography.Xml.SignedXml::pkEnumerator
	Il2CppObject * ___pkEnumerator_3;
	// System.Xml.XmlElement System.Security.Cryptography.Xml.SignedXml::signatureElement
	XmlElement_t561603118 * ___signatureElement_4;
	// System.Collections.Hashtable System.Security.Cryptography.Xml.SignedXml::hashes
	Hashtable_t1853889766 * ___hashes_5;
	// System.Xml.XmlResolver System.Security.Cryptography.Xml.SignedXml::xmlResolver
	XmlResolver_t626023767 * ___xmlResolver_6;
	// System.Collections.ArrayList System.Security.Cryptography.Xml.SignedXml::manifests
	ArrayList_t2718874744 * ___manifests_7;
	// System.Collections.IEnumerator System.Security.Cryptography.Xml.SignedXml::_x509Enumerator
	Il2CppObject * ____x509Enumerator_8;

public:
	inline static int32_t get_offset_of_m_signature_0() { return static_cast<int32_t>(offsetof(SignedXml_t2168796367, ___m_signature_0)); }
	inline Signature_t754089620 * get_m_signature_0() const { return ___m_signature_0; }
	inline Signature_t754089620 ** get_address_of_m_signature_0() { return &___m_signature_0; }
	inline void set_m_signature_0(Signature_t754089620 * value)
	{
		___m_signature_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_signature_0, value);
	}

	inline static int32_t get_offset_of_key_1() { return static_cast<int32_t>(offsetof(SignedXml_t2168796367, ___key_1)); }
	inline AsymmetricAlgorithm_t932037087 * get_key_1() const { return ___key_1; }
	inline AsymmetricAlgorithm_t932037087 ** get_address_of_key_1() { return &___key_1; }
	inline void set_key_1(AsymmetricAlgorithm_t932037087 * value)
	{
		___key_1 = value;
		Il2CppCodeGenWriteBarrier(&___key_1, value);
	}

	inline static int32_t get_offset_of_envdoc_2() { return static_cast<int32_t>(offsetof(SignedXml_t2168796367, ___envdoc_2)); }
	inline XmlDocument_t2837193595 * get_envdoc_2() const { return ___envdoc_2; }
	inline XmlDocument_t2837193595 ** get_address_of_envdoc_2() { return &___envdoc_2; }
	inline void set_envdoc_2(XmlDocument_t2837193595 * value)
	{
		___envdoc_2 = value;
		Il2CppCodeGenWriteBarrier(&___envdoc_2, value);
	}

	inline static int32_t get_offset_of_pkEnumerator_3() { return static_cast<int32_t>(offsetof(SignedXml_t2168796367, ___pkEnumerator_3)); }
	inline Il2CppObject * get_pkEnumerator_3() const { return ___pkEnumerator_3; }
	inline Il2CppObject ** get_address_of_pkEnumerator_3() { return &___pkEnumerator_3; }
	inline void set_pkEnumerator_3(Il2CppObject * value)
	{
		___pkEnumerator_3 = value;
		Il2CppCodeGenWriteBarrier(&___pkEnumerator_3, value);
	}

	inline static int32_t get_offset_of_signatureElement_4() { return static_cast<int32_t>(offsetof(SignedXml_t2168796367, ___signatureElement_4)); }
	inline XmlElement_t561603118 * get_signatureElement_4() const { return ___signatureElement_4; }
	inline XmlElement_t561603118 ** get_address_of_signatureElement_4() { return &___signatureElement_4; }
	inline void set_signatureElement_4(XmlElement_t561603118 * value)
	{
		___signatureElement_4 = value;
		Il2CppCodeGenWriteBarrier(&___signatureElement_4, value);
	}

	inline static int32_t get_offset_of_hashes_5() { return static_cast<int32_t>(offsetof(SignedXml_t2168796367, ___hashes_5)); }
	inline Hashtable_t1853889766 * get_hashes_5() const { return ___hashes_5; }
	inline Hashtable_t1853889766 ** get_address_of_hashes_5() { return &___hashes_5; }
	inline void set_hashes_5(Hashtable_t1853889766 * value)
	{
		___hashes_5 = value;
		Il2CppCodeGenWriteBarrier(&___hashes_5, value);
	}

	inline static int32_t get_offset_of_xmlResolver_6() { return static_cast<int32_t>(offsetof(SignedXml_t2168796367, ___xmlResolver_6)); }
	inline XmlResolver_t626023767 * get_xmlResolver_6() const { return ___xmlResolver_6; }
	inline XmlResolver_t626023767 ** get_address_of_xmlResolver_6() { return &___xmlResolver_6; }
	inline void set_xmlResolver_6(XmlResolver_t626023767 * value)
	{
		___xmlResolver_6 = value;
		Il2CppCodeGenWriteBarrier(&___xmlResolver_6, value);
	}

	inline static int32_t get_offset_of_manifests_7() { return static_cast<int32_t>(offsetof(SignedXml_t2168796367, ___manifests_7)); }
	inline ArrayList_t2718874744 * get_manifests_7() const { return ___manifests_7; }
	inline ArrayList_t2718874744 ** get_address_of_manifests_7() { return &___manifests_7; }
	inline void set_manifests_7(ArrayList_t2718874744 * value)
	{
		___manifests_7 = value;
		Il2CppCodeGenWriteBarrier(&___manifests_7, value);
	}

	inline static int32_t get_offset_of__x509Enumerator_8() { return static_cast<int32_t>(offsetof(SignedXml_t2168796367, ____x509Enumerator_8)); }
	inline Il2CppObject * get__x509Enumerator_8() const { return ____x509Enumerator_8; }
	inline Il2CppObject ** get_address_of__x509Enumerator_8() { return &____x509Enumerator_8; }
	inline void set__x509Enumerator_8(Il2CppObject * value)
	{
		____x509Enumerator_8 = value;
		Il2CppCodeGenWriteBarrier(&____x509Enumerator_8, value);
	}
};

struct SignedXml_t2168796367_StaticFields
{
public:
	// System.Char[] System.Security.Cryptography.Xml.SignedXml::whitespaceChars
	CharU5BU5D_t3528271667* ___whitespaceChars_9;

public:
	inline static int32_t get_offset_of_whitespaceChars_9() { return static_cast<int32_t>(offsetof(SignedXml_t2168796367_StaticFields, ___whitespaceChars_9)); }
	inline CharU5BU5D_t3528271667* get_whitespaceChars_9() const { return ___whitespaceChars_9; }
	inline CharU5BU5D_t3528271667** get_address_of_whitespaceChars_9() { return &___whitespaceChars_9; }
	inline void set_whitespaceChars_9(CharU5BU5D_t3528271667* value)
	{
		___whitespaceChars_9 = value;
		Il2CppCodeGenWriteBarrier(&___whitespaceChars_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
