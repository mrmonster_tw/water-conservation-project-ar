﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_M3531153504.h"

// System.ServiceModel.Channels.MessageVersion
struct MessageVersion_t1958933598;
// System.Text.Encoding
struct Encoding_t1523322056;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.MtomMessageEncodingBindingElement
struct  MtomMessageEncodingBindingElement_t3584114940  : public MessageEncodingBindingElement_t3531153504
{
public:
	// System.ServiceModel.Channels.MessageVersion System.ServiceModel.Channels.MtomMessageEncodingBindingElement::version
	MessageVersion_t1958933598 * ___version_0;
	// System.Text.Encoding System.ServiceModel.Channels.MtomMessageEncodingBindingElement::encoding
	Encoding_t1523322056 * ___encoding_1;

public:
	inline static int32_t get_offset_of_version_0() { return static_cast<int32_t>(offsetof(MtomMessageEncodingBindingElement_t3584114940, ___version_0)); }
	inline MessageVersion_t1958933598 * get_version_0() const { return ___version_0; }
	inline MessageVersion_t1958933598 ** get_address_of_version_0() { return &___version_0; }
	inline void set_version_0(MessageVersion_t1958933598 * value)
	{
		___version_0 = value;
		Il2CppCodeGenWriteBarrier(&___version_0, value);
	}

	inline static int32_t get_offset_of_encoding_1() { return static_cast<int32_t>(offsetof(MtomMessageEncodingBindingElement_t3584114940, ___encoding_1)); }
	inline Encoding_t1523322056 * get_encoding_1() const { return ___encoding_1; }
	inline Encoding_t1523322056 ** get_address_of_encoding_1() { return &___encoding_1; }
	inline void set_encoding_1(Encoding_t1523322056 * value)
	{
		___encoding_1 = value;
		Il2CppCodeGenWriteBarrier(&___encoding_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
