﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Threading.ManualResetEvent
struct ManualResetEvent_t451242010;
// System.ServiceModel.Channels.TcpChannelListener`1/<AcceptTcpClient>c__AnonStorey13<System.ServiceModel.Channels.IReplyChannel>
struct U3CAcceptTcpClientU3Ec__AnonStorey13_t3379279139;
// System.ServiceModel.Channels.TcpChannelListener`1<System.ServiceModel.Channels.IReplyChannel>
struct TcpChannelListener_1_t2995356293;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.TcpChannelListener`1/<AcceptTcpClient>c__AnonStorey14<System.ServiceModel.Channels.IReplyChannel>
struct  U3CAcceptTcpClientU3Ec__AnonStorey14_t1979495715  : public Il2CppObject
{
public:
	// System.Threading.ManualResetEvent System.ServiceModel.Channels.TcpChannelListener`1/<AcceptTcpClient>c__AnonStorey14::wait
	ManualResetEvent_t451242010 * ___wait_0;
	// System.ServiceModel.Channels.TcpChannelListener`1/<AcceptTcpClient>c__AnonStorey13<TChannel> System.ServiceModel.Channels.TcpChannelListener`1/<AcceptTcpClient>c__AnonStorey14::<>f__ref$19
	U3CAcceptTcpClientU3Ec__AnonStorey13_t3379279139 * ___U3CU3Ef__refU2419_1;
	// System.ServiceModel.Channels.TcpChannelListener`1<TChannel> System.ServiceModel.Channels.TcpChannelListener`1/<AcceptTcpClient>c__AnonStorey14::<>f__this
	TcpChannelListener_1_t2995356293 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_wait_0() { return static_cast<int32_t>(offsetof(U3CAcceptTcpClientU3Ec__AnonStorey14_t1979495715, ___wait_0)); }
	inline ManualResetEvent_t451242010 * get_wait_0() const { return ___wait_0; }
	inline ManualResetEvent_t451242010 ** get_address_of_wait_0() { return &___wait_0; }
	inline void set_wait_0(ManualResetEvent_t451242010 * value)
	{
		___wait_0 = value;
		Il2CppCodeGenWriteBarrier(&___wait_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2419_1() { return static_cast<int32_t>(offsetof(U3CAcceptTcpClientU3Ec__AnonStorey14_t1979495715, ___U3CU3Ef__refU2419_1)); }
	inline U3CAcceptTcpClientU3Ec__AnonStorey13_t3379279139 * get_U3CU3Ef__refU2419_1() const { return ___U3CU3Ef__refU2419_1; }
	inline U3CAcceptTcpClientU3Ec__AnonStorey13_t3379279139 ** get_address_of_U3CU3Ef__refU2419_1() { return &___U3CU3Ef__refU2419_1; }
	inline void set_U3CU3Ef__refU2419_1(U3CAcceptTcpClientU3Ec__AnonStorey13_t3379279139 * value)
	{
		___U3CU3Ef__refU2419_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU2419_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CAcceptTcpClientU3Ec__AnonStorey14_t1979495715, ___U3CU3Ef__this_2)); }
	inline TcpChannelListener_1_t2995356293 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline TcpChannelListener_1_t2995356293 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(TcpChannelListener_1_t2995356293 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
