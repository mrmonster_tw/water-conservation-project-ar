﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.IdentityModel.Selectors.X509CertificateValidator
struct X509CertificateValidator_t1375267704;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Selectors.X509CertificateValidator
struct  X509CertificateValidator_t1375267704  : public Il2CppObject
{
public:

public:
};

struct X509CertificateValidator_t1375267704_StaticFields
{
public:
	// System.IdentityModel.Selectors.X509CertificateValidator System.IdentityModel.Selectors.X509CertificateValidator::none
	X509CertificateValidator_t1375267704 * ___none_0;
	// System.IdentityModel.Selectors.X509CertificateValidator System.IdentityModel.Selectors.X509CertificateValidator::chain
	X509CertificateValidator_t1375267704 * ___chain_1;
	// System.IdentityModel.Selectors.X509CertificateValidator System.IdentityModel.Selectors.X509CertificateValidator::peer_or_chain
	X509CertificateValidator_t1375267704 * ___peer_or_chain_2;
	// System.IdentityModel.Selectors.X509CertificateValidator System.IdentityModel.Selectors.X509CertificateValidator::peer
	X509CertificateValidator_t1375267704 * ___peer_3;

public:
	inline static int32_t get_offset_of_none_0() { return static_cast<int32_t>(offsetof(X509CertificateValidator_t1375267704_StaticFields, ___none_0)); }
	inline X509CertificateValidator_t1375267704 * get_none_0() const { return ___none_0; }
	inline X509CertificateValidator_t1375267704 ** get_address_of_none_0() { return &___none_0; }
	inline void set_none_0(X509CertificateValidator_t1375267704 * value)
	{
		___none_0 = value;
		Il2CppCodeGenWriteBarrier(&___none_0, value);
	}

	inline static int32_t get_offset_of_chain_1() { return static_cast<int32_t>(offsetof(X509CertificateValidator_t1375267704_StaticFields, ___chain_1)); }
	inline X509CertificateValidator_t1375267704 * get_chain_1() const { return ___chain_1; }
	inline X509CertificateValidator_t1375267704 ** get_address_of_chain_1() { return &___chain_1; }
	inline void set_chain_1(X509CertificateValidator_t1375267704 * value)
	{
		___chain_1 = value;
		Il2CppCodeGenWriteBarrier(&___chain_1, value);
	}

	inline static int32_t get_offset_of_peer_or_chain_2() { return static_cast<int32_t>(offsetof(X509CertificateValidator_t1375267704_StaticFields, ___peer_or_chain_2)); }
	inline X509CertificateValidator_t1375267704 * get_peer_or_chain_2() const { return ___peer_or_chain_2; }
	inline X509CertificateValidator_t1375267704 ** get_address_of_peer_or_chain_2() { return &___peer_or_chain_2; }
	inline void set_peer_or_chain_2(X509CertificateValidator_t1375267704 * value)
	{
		___peer_or_chain_2 = value;
		Il2CppCodeGenWriteBarrier(&___peer_or_chain_2, value);
	}

	inline static int32_t get_offset_of_peer_3() { return static_cast<int32_t>(offsetof(X509CertificateValidator_t1375267704_StaticFields, ___peer_3)); }
	inline X509CertificateValidator_t1375267704 * get_peer_3() const { return ___peer_3; }
	inline X509CertificateValidator_t1375267704 ** get_address_of_peer_3() { return &___peer_3; }
	inline void set_peer_3(X509CertificateValidator_t1375267704 * value)
	{
		___peer_3 = value;
		Il2CppCodeGenWriteBarrier(&___peer_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
