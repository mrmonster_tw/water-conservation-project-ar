﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3549286319.h"
#include "System_EnterpriseServices_U3CModuleU3E692745525.h"
#include "System_EnterpriseServices_System_EnterpriseServices_25113873.h"
#include "mscorlib_System_String1847450689.h"
#include "mscorlib_System_Void1185182177.h"
#include "mscorlib_System_Guid3193532887.h"
#include "mscorlib_System_Attribute861562559.h"
#include "System_EnterpriseServices_System_EnterpriseService2032989562.h"

// System.EnterpriseServices.ApplicationIDAttribute
struct ApplicationIDAttribute_t25113873;
// System.String
struct String_t;
// System.Attribute
struct Attribute_t861562559;
// System.EnterpriseServices.ApplicationNameAttribute
struct ApplicationNameAttribute_t2032989562;




// System.Void System.Attribute::.ctor()
extern "C"  void Attribute__ctor_m1529526131 (Attribute_t861562559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Guid::.ctor(System.String)
extern "C"  void Guid__ctor_m2423264394 (Guid_t * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.EnterpriseServices.ApplicationIDAttribute::.ctor(System.String)
extern "C"  void ApplicationIDAttribute__ctor_m3659986529 (ApplicationIDAttribute_t25113873 * __this, String_t* ___guid0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___guid0;
		Guid_t  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Guid__ctor_m2423264394(&L_1, L_0, /*hidden argument*/NULL);
		__this->set_guid_0(L_1);
		return;
	}
}
// System.Void System.EnterpriseServices.ApplicationNameAttribute::.ctor(System.String)
extern "C"  void ApplicationNameAttribute__ctor_m1978542252 (ApplicationNameAttribute_t2032989562 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_name_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
