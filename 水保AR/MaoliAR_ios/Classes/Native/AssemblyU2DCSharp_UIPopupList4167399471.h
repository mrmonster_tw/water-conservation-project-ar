﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_UIWidgetContainer30162560.h"
#include "UnityEngine_UnityEngine_FontStyle82229486.h"
#include "AssemblyU2DCSharp_UIPopupList_Position1583461796.h"
#include "AssemblyU2DCSharp_NGUIText_Alignment3228070485.h"
#include "UnityEngine_UnityEngine_Vector22156229523.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"
#include "AssemblyU2DCSharp_UIPopupList_OpenOn1997085761.h"

// UIPopupList
struct UIPopupList_t4167399471;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UIAtlas
struct UIAtlas_t3195533529;
// UIFont
struct UIFont_t2766063701;
// UnityEngine.Font
struct Font_t1956802104;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.Collections.Generic.List`1<EventDelegate>
struct List_1_t4210400802;
// UIPanel
struct UIPanel_t1716472341;
// UISprite
struct UISprite_t194114938;
// UILabel
struct UILabel_t3248798549;
// System.Collections.Generic.List`1<UILabel>
struct List_1_t425905995;
// UIPopupList/LegacyEvent
struct LegacyEvent_t2749056879;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPopupList
struct  UIPopupList_t4167399471  : public UIWidgetContainer_t30162560
{
public:
	// UIAtlas UIPopupList::atlas
	UIAtlas_t3195533529 * ___atlas_6;
	// UIFont UIPopupList::bitmapFont
	UIFont_t2766063701 * ___bitmapFont_7;
	// UnityEngine.Font UIPopupList::trueTypeFont
	Font_t1956802104 * ___trueTypeFont_8;
	// System.Int32 UIPopupList::fontSize
	int32_t ___fontSize_9;
	// UnityEngine.FontStyle UIPopupList::fontStyle
	int32_t ___fontStyle_10;
	// System.String UIPopupList::backgroundSprite
	String_t* ___backgroundSprite_11;
	// System.String UIPopupList::highlightSprite
	String_t* ___highlightSprite_12;
	// UIPopupList/Position UIPopupList::position
	int32_t ___position_13;
	// NGUIText/Alignment UIPopupList::alignment
	int32_t ___alignment_14;
	// System.Collections.Generic.List`1<System.String> UIPopupList::items
	List_1_t3319525431 * ___items_15;
	// System.Collections.Generic.List`1<System.Object> UIPopupList::itemData
	List_1_t257213610 * ___itemData_16;
	// UnityEngine.Vector2 UIPopupList::padding
	Vector2_t2156229523  ___padding_17;
	// UnityEngine.Color UIPopupList::textColor
	Color_t2555686324  ___textColor_18;
	// UnityEngine.Color UIPopupList::backgroundColor
	Color_t2555686324  ___backgroundColor_19;
	// UnityEngine.Color UIPopupList::highlightColor
	Color_t2555686324  ___highlightColor_20;
	// System.Boolean UIPopupList::isAnimated
	bool ___isAnimated_21;
	// System.Boolean UIPopupList::isLocalized
	bool ___isLocalized_22;
	// UIPopupList/OpenOn UIPopupList::openOn
	int32_t ___openOn_23;
	// System.Collections.Generic.List`1<EventDelegate> UIPopupList::onChange
	List_1_t4210400802 * ___onChange_24;
	// System.String UIPopupList::mSelectedItem
	String_t* ___mSelectedItem_25;
	// UIPanel UIPopupList::mPanel
	UIPanel_t1716472341 * ___mPanel_26;
	// UISprite UIPopupList::mBackground
	UISprite_t194114938 * ___mBackground_27;
	// UISprite UIPopupList::mHighlight
	UISprite_t194114938 * ___mHighlight_28;
	// UILabel UIPopupList::mHighlightedLabel
	UILabel_t3248798549 * ___mHighlightedLabel_29;
	// System.Collections.Generic.List`1<UILabel> UIPopupList::mLabelList
	List_1_t425905995 * ___mLabelList_30;
	// System.Single UIPopupList::mBgBorder
	float ___mBgBorder_31;
	// UnityEngine.GameObject UIPopupList::mSelection
	GameObject_t1113636619 * ___mSelection_32;
	// System.Int32 UIPopupList::mOpenFrame
	int32_t ___mOpenFrame_33;
	// UnityEngine.GameObject UIPopupList::eventReceiver
	GameObject_t1113636619 * ___eventReceiver_34;
	// System.String UIPopupList::functionName
	String_t* ___functionName_35;
	// System.Single UIPopupList::textScale
	float ___textScale_36;
	// UIFont UIPopupList::font
	UIFont_t2766063701 * ___font_37;
	// UILabel UIPopupList::textLabel
	UILabel_t3248798549 * ___textLabel_38;
	// UIPopupList/LegacyEvent UIPopupList::mLegacyEvent
	LegacyEvent_t2749056879 * ___mLegacyEvent_39;
	// System.Boolean UIPopupList::mExecuting
	bool ___mExecuting_40;
	// System.Boolean UIPopupList::mUseDynamicFont
	bool ___mUseDynamicFont_41;
	// System.Boolean UIPopupList::mTweening
	bool ___mTweening_42;
	// UnityEngine.GameObject UIPopupList::source
	GameObject_t1113636619 * ___source_43;

public:
	inline static int32_t get_offset_of_atlas_6() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___atlas_6)); }
	inline UIAtlas_t3195533529 * get_atlas_6() const { return ___atlas_6; }
	inline UIAtlas_t3195533529 ** get_address_of_atlas_6() { return &___atlas_6; }
	inline void set_atlas_6(UIAtlas_t3195533529 * value)
	{
		___atlas_6 = value;
		Il2CppCodeGenWriteBarrier(&___atlas_6, value);
	}

	inline static int32_t get_offset_of_bitmapFont_7() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___bitmapFont_7)); }
	inline UIFont_t2766063701 * get_bitmapFont_7() const { return ___bitmapFont_7; }
	inline UIFont_t2766063701 ** get_address_of_bitmapFont_7() { return &___bitmapFont_7; }
	inline void set_bitmapFont_7(UIFont_t2766063701 * value)
	{
		___bitmapFont_7 = value;
		Il2CppCodeGenWriteBarrier(&___bitmapFont_7, value);
	}

	inline static int32_t get_offset_of_trueTypeFont_8() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___trueTypeFont_8)); }
	inline Font_t1956802104 * get_trueTypeFont_8() const { return ___trueTypeFont_8; }
	inline Font_t1956802104 ** get_address_of_trueTypeFont_8() { return &___trueTypeFont_8; }
	inline void set_trueTypeFont_8(Font_t1956802104 * value)
	{
		___trueTypeFont_8 = value;
		Il2CppCodeGenWriteBarrier(&___trueTypeFont_8, value);
	}

	inline static int32_t get_offset_of_fontSize_9() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___fontSize_9)); }
	inline int32_t get_fontSize_9() const { return ___fontSize_9; }
	inline int32_t* get_address_of_fontSize_9() { return &___fontSize_9; }
	inline void set_fontSize_9(int32_t value)
	{
		___fontSize_9 = value;
	}

	inline static int32_t get_offset_of_fontStyle_10() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___fontStyle_10)); }
	inline int32_t get_fontStyle_10() const { return ___fontStyle_10; }
	inline int32_t* get_address_of_fontStyle_10() { return &___fontStyle_10; }
	inline void set_fontStyle_10(int32_t value)
	{
		___fontStyle_10 = value;
	}

	inline static int32_t get_offset_of_backgroundSprite_11() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___backgroundSprite_11)); }
	inline String_t* get_backgroundSprite_11() const { return ___backgroundSprite_11; }
	inline String_t** get_address_of_backgroundSprite_11() { return &___backgroundSprite_11; }
	inline void set_backgroundSprite_11(String_t* value)
	{
		___backgroundSprite_11 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundSprite_11, value);
	}

	inline static int32_t get_offset_of_highlightSprite_12() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___highlightSprite_12)); }
	inline String_t* get_highlightSprite_12() const { return ___highlightSprite_12; }
	inline String_t** get_address_of_highlightSprite_12() { return &___highlightSprite_12; }
	inline void set_highlightSprite_12(String_t* value)
	{
		___highlightSprite_12 = value;
		Il2CppCodeGenWriteBarrier(&___highlightSprite_12, value);
	}

	inline static int32_t get_offset_of_position_13() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___position_13)); }
	inline int32_t get_position_13() const { return ___position_13; }
	inline int32_t* get_address_of_position_13() { return &___position_13; }
	inline void set_position_13(int32_t value)
	{
		___position_13 = value;
	}

	inline static int32_t get_offset_of_alignment_14() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___alignment_14)); }
	inline int32_t get_alignment_14() const { return ___alignment_14; }
	inline int32_t* get_address_of_alignment_14() { return &___alignment_14; }
	inline void set_alignment_14(int32_t value)
	{
		___alignment_14 = value;
	}

	inline static int32_t get_offset_of_items_15() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___items_15)); }
	inline List_1_t3319525431 * get_items_15() const { return ___items_15; }
	inline List_1_t3319525431 ** get_address_of_items_15() { return &___items_15; }
	inline void set_items_15(List_1_t3319525431 * value)
	{
		___items_15 = value;
		Il2CppCodeGenWriteBarrier(&___items_15, value);
	}

	inline static int32_t get_offset_of_itemData_16() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___itemData_16)); }
	inline List_1_t257213610 * get_itemData_16() const { return ___itemData_16; }
	inline List_1_t257213610 ** get_address_of_itemData_16() { return &___itemData_16; }
	inline void set_itemData_16(List_1_t257213610 * value)
	{
		___itemData_16 = value;
		Il2CppCodeGenWriteBarrier(&___itemData_16, value);
	}

	inline static int32_t get_offset_of_padding_17() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___padding_17)); }
	inline Vector2_t2156229523  get_padding_17() const { return ___padding_17; }
	inline Vector2_t2156229523 * get_address_of_padding_17() { return &___padding_17; }
	inline void set_padding_17(Vector2_t2156229523  value)
	{
		___padding_17 = value;
	}

	inline static int32_t get_offset_of_textColor_18() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___textColor_18)); }
	inline Color_t2555686324  get_textColor_18() const { return ___textColor_18; }
	inline Color_t2555686324 * get_address_of_textColor_18() { return &___textColor_18; }
	inline void set_textColor_18(Color_t2555686324  value)
	{
		___textColor_18 = value;
	}

	inline static int32_t get_offset_of_backgroundColor_19() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___backgroundColor_19)); }
	inline Color_t2555686324  get_backgroundColor_19() const { return ___backgroundColor_19; }
	inline Color_t2555686324 * get_address_of_backgroundColor_19() { return &___backgroundColor_19; }
	inline void set_backgroundColor_19(Color_t2555686324  value)
	{
		___backgroundColor_19 = value;
	}

	inline static int32_t get_offset_of_highlightColor_20() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___highlightColor_20)); }
	inline Color_t2555686324  get_highlightColor_20() const { return ___highlightColor_20; }
	inline Color_t2555686324 * get_address_of_highlightColor_20() { return &___highlightColor_20; }
	inline void set_highlightColor_20(Color_t2555686324  value)
	{
		___highlightColor_20 = value;
	}

	inline static int32_t get_offset_of_isAnimated_21() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___isAnimated_21)); }
	inline bool get_isAnimated_21() const { return ___isAnimated_21; }
	inline bool* get_address_of_isAnimated_21() { return &___isAnimated_21; }
	inline void set_isAnimated_21(bool value)
	{
		___isAnimated_21 = value;
	}

	inline static int32_t get_offset_of_isLocalized_22() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___isLocalized_22)); }
	inline bool get_isLocalized_22() const { return ___isLocalized_22; }
	inline bool* get_address_of_isLocalized_22() { return &___isLocalized_22; }
	inline void set_isLocalized_22(bool value)
	{
		___isLocalized_22 = value;
	}

	inline static int32_t get_offset_of_openOn_23() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___openOn_23)); }
	inline int32_t get_openOn_23() const { return ___openOn_23; }
	inline int32_t* get_address_of_openOn_23() { return &___openOn_23; }
	inline void set_openOn_23(int32_t value)
	{
		___openOn_23 = value;
	}

	inline static int32_t get_offset_of_onChange_24() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___onChange_24)); }
	inline List_1_t4210400802 * get_onChange_24() const { return ___onChange_24; }
	inline List_1_t4210400802 ** get_address_of_onChange_24() { return &___onChange_24; }
	inline void set_onChange_24(List_1_t4210400802 * value)
	{
		___onChange_24 = value;
		Il2CppCodeGenWriteBarrier(&___onChange_24, value);
	}

	inline static int32_t get_offset_of_mSelectedItem_25() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___mSelectedItem_25)); }
	inline String_t* get_mSelectedItem_25() const { return ___mSelectedItem_25; }
	inline String_t** get_address_of_mSelectedItem_25() { return &___mSelectedItem_25; }
	inline void set_mSelectedItem_25(String_t* value)
	{
		___mSelectedItem_25 = value;
		Il2CppCodeGenWriteBarrier(&___mSelectedItem_25, value);
	}

	inline static int32_t get_offset_of_mPanel_26() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___mPanel_26)); }
	inline UIPanel_t1716472341 * get_mPanel_26() const { return ___mPanel_26; }
	inline UIPanel_t1716472341 ** get_address_of_mPanel_26() { return &___mPanel_26; }
	inline void set_mPanel_26(UIPanel_t1716472341 * value)
	{
		___mPanel_26 = value;
		Il2CppCodeGenWriteBarrier(&___mPanel_26, value);
	}

	inline static int32_t get_offset_of_mBackground_27() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___mBackground_27)); }
	inline UISprite_t194114938 * get_mBackground_27() const { return ___mBackground_27; }
	inline UISprite_t194114938 ** get_address_of_mBackground_27() { return &___mBackground_27; }
	inline void set_mBackground_27(UISprite_t194114938 * value)
	{
		___mBackground_27 = value;
		Il2CppCodeGenWriteBarrier(&___mBackground_27, value);
	}

	inline static int32_t get_offset_of_mHighlight_28() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___mHighlight_28)); }
	inline UISprite_t194114938 * get_mHighlight_28() const { return ___mHighlight_28; }
	inline UISprite_t194114938 ** get_address_of_mHighlight_28() { return &___mHighlight_28; }
	inline void set_mHighlight_28(UISprite_t194114938 * value)
	{
		___mHighlight_28 = value;
		Il2CppCodeGenWriteBarrier(&___mHighlight_28, value);
	}

	inline static int32_t get_offset_of_mHighlightedLabel_29() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___mHighlightedLabel_29)); }
	inline UILabel_t3248798549 * get_mHighlightedLabel_29() const { return ___mHighlightedLabel_29; }
	inline UILabel_t3248798549 ** get_address_of_mHighlightedLabel_29() { return &___mHighlightedLabel_29; }
	inline void set_mHighlightedLabel_29(UILabel_t3248798549 * value)
	{
		___mHighlightedLabel_29 = value;
		Il2CppCodeGenWriteBarrier(&___mHighlightedLabel_29, value);
	}

	inline static int32_t get_offset_of_mLabelList_30() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___mLabelList_30)); }
	inline List_1_t425905995 * get_mLabelList_30() const { return ___mLabelList_30; }
	inline List_1_t425905995 ** get_address_of_mLabelList_30() { return &___mLabelList_30; }
	inline void set_mLabelList_30(List_1_t425905995 * value)
	{
		___mLabelList_30 = value;
		Il2CppCodeGenWriteBarrier(&___mLabelList_30, value);
	}

	inline static int32_t get_offset_of_mBgBorder_31() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___mBgBorder_31)); }
	inline float get_mBgBorder_31() const { return ___mBgBorder_31; }
	inline float* get_address_of_mBgBorder_31() { return &___mBgBorder_31; }
	inline void set_mBgBorder_31(float value)
	{
		___mBgBorder_31 = value;
	}

	inline static int32_t get_offset_of_mSelection_32() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___mSelection_32)); }
	inline GameObject_t1113636619 * get_mSelection_32() const { return ___mSelection_32; }
	inline GameObject_t1113636619 ** get_address_of_mSelection_32() { return &___mSelection_32; }
	inline void set_mSelection_32(GameObject_t1113636619 * value)
	{
		___mSelection_32 = value;
		Il2CppCodeGenWriteBarrier(&___mSelection_32, value);
	}

	inline static int32_t get_offset_of_mOpenFrame_33() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___mOpenFrame_33)); }
	inline int32_t get_mOpenFrame_33() const { return ___mOpenFrame_33; }
	inline int32_t* get_address_of_mOpenFrame_33() { return &___mOpenFrame_33; }
	inline void set_mOpenFrame_33(int32_t value)
	{
		___mOpenFrame_33 = value;
	}

	inline static int32_t get_offset_of_eventReceiver_34() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___eventReceiver_34)); }
	inline GameObject_t1113636619 * get_eventReceiver_34() const { return ___eventReceiver_34; }
	inline GameObject_t1113636619 ** get_address_of_eventReceiver_34() { return &___eventReceiver_34; }
	inline void set_eventReceiver_34(GameObject_t1113636619 * value)
	{
		___eventReceiver_34 = value;
		Il2CppCodeGenWriteBarrier(&___eventReceiver_34, value);
	}

	inline static int32_t get_offset_of_functionName_35() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___functionName_35)); }
	inline String_t* get_functionName_35() const { return ___functionName_35; }
	inline String_t** get_address_of_functionName_35() { return &___functionName_35; }
	inline void set_functionName_35(String_t* value)
	{
		___functionName_35 = value;
		Il2CppCodeGenWriteBarrier(&___functionName_35, value);
	}

	inline static int32_t get_offset_of_textScale_36() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___textScale_36)); }
	inline float get_textScale_36() const { return ___textScale_36; }
	inline float* get_address_of_textScale_36() { return &___textScale_36; }
	inline void set_textScale_36(float value)
	{
		___textScale_36 = value;
	}

	inline static int32_t get_offset_of_font_37() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___font_37)); }
	inline UIFont_t2766063701 * get_font_37() const { return ___font_37; }
	inline UIFont_t2766063701 ** get_address_of_font_37() { return &___font_37; }
	inline void set_font_37(UIFont_t2766063701 * value)
	{
		___font_37 = value;
		Il2CppCodeGenWriteBarrier(&___font_37, value);
	}

	inline static int32_t get_offset_of_textLabel_38() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___textLabel_38)); }
	inline UILabel_t3248798549 * get_textLabel_38() const { return ___textLabel_38; }
	inline UILabel_t3248798549 ** get_address_of_textLabel_38() { return &___textLabel_38; }
	inline void set_textLabel_38(UILabel_t3248798549 * value)
	{
		___textLabel_38 = value;
		Il2CppCodeGenWriteBarrier(&___textLabel_38, value);
	}

	inline static int32_t get_offset_of_mLegacyEvent_39() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___mLegacyEvent_39)); }
	inline LegacyEvent_t2749056879 * get_mLegacyEvent_39() const { return ___mLegacyEvent_39; }
	inline LegacyEvent_t2749056879 ** get_address_of_mLegacyEvent_39() { return &___mLegacyEvent_39; }
	inline void set_mLegacyEvent_39(LegacyEvent_t2749056879 * value)
	{
		___mLegacyEvent_39 = value;
		Il2CppCodeGenWriteBarrier(&___mLegacyEvent_39, value);
	}

	inline static int32_t get_offset_of_mExecuting_40() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___mExecuting_40)); }
	inline bool get_mExecuting_40() const { return ___mExecuting_40; }
	inline bool* get_address_of_mExecuting_40() { return &___mExecuting_40; }
	inline void set_mExecuting_40(bool value)
	{
		___mExecuting_40 = value;
	}

	inline static int32_t get_offset_of_mUseDynamicFont_41() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___mUseDynamicFont_41)); }
	inline bool get_mUseDynamicFont_41() const { return ___mUseDynamicFont_41; }
	inline bool* get_address_of_mUseDynamicFont_41() { return &___mUseDynamicFont_41; }
	inline void set_mUseDynamicFont_41(bool value)
	{
		___mUseDynamicFont_41 = value;
	}

	inline static int32_t get_offset_of_mTweening_42() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___mTweening_42)); }
	inline bool get_mTweening_42() const { return ___mTweening_42; }
	inline bool* get_address_of_mTweening_42() { return &___mTweening_42; }
	inline void set_mTweening_42(bool value)
	{
		___mTweening_42 = value;
	}

	inline static int32_t get_offset_of_source_43() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471, ___source_43)); }
	inline GameObject_t1113636619 * get_source_43() const { return ___source_43; }
	inline GameObject_t1113636619 ** get_address_of_source_43() { return &___source_43; }
	inline void set_source_43(GameObject_t1113636619 * value)
	{
		___source_43 = value;
		Il2CppCodeGenWriteBarrier(&___source_43, value);
	}
};

struct UIPopupList_t4167399471_StaticFields
{
public:
	// UIPopupList UIPopupList::current
	UIPopupList_t4167399471 * ___current_2;
	// UnityEngine.GameObject UIPopupList::mChild
	GameObject_t1113636619 * ___mChild_3;
	// System.Single UIPopupList::mFadeOutComplete
	float ___mFadeOutComplete_4;

public:
	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471_StaticFields, ___current_2)); }
	inline UIPopupList_t4167399471 * get_current_2() const { return ___current_2; }
	inline UIPopupList_t4167399471 ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(UIPopupList_t4167399471 * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier(&___current_2, value);
	}

	inline static int32_t get_offset_of_mChild_3() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471_StaticFields, ___mChild_3)); }
	inline GameObject_t1113636619 * get_mChild_3() const { return ___mChild_3; }
	inline GameObject_t1113636619 ** get_address_of_mChild_3() { return &___mChild_3; }
	inline void set_mChild_3(GameObject_t1113636619 * value)
	{
		___mChild_3 = value;
		Il2CppCodeGenWriteBarrier(&___mChild_3, value);
	}

	inline static int32_t get_offset_of_mFadeOutComplete_4() { return static_cast<int32_t>(offsetof(UIPopupList_t4167399471_StaticFields, ___mFadeOutComplete_4)); }
	inline float get_mFadeOutComplete_4() const { return ___mFadeOutComplete_4; }
	inline float* get_address_of_mFadeOutComplete_4() { return &___mFadeOutComplete_4; }
	inline void set_mFadeOutComplete_4(float value)
	{
		___mFadeOutComplete_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
