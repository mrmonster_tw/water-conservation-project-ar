﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_HtmlControls_HtmlInputCon1839583126.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.HtmlControls.HtmlInputImage
struct  HtmlInputImage_t1805692912  : public HtmlInputControl_t1839583126
{
public:
	// System.Int32 System.Web.UI.HtmlControls.HtmlInputImage::clicked_x
	int32_t ___clicked_x_31;
	// System.Int32 System.Web.UI.HtmlControls.HtmlInputImage::clicked_y
	int32_t ___clicked_y_32;

public:
	inline static int32_t get_offset_of_clicked_x_31() { return static_cast<int32_t>(offsetof(HtmlInputImage_t1805692912, ___clicked_x_31)); }
	inline int32_t get_clicked_x_31() const { return ___clicked_x_31; }
	inline int32_t* get_address_of_clicked_x_31() { return &___clicked_x_31; }
	inline void set_clicked_x_31(int32_t value)
	{
		___clicked_x_31 = value;
	}

	inline static int32_t get_offset_of_clicked_y_32() { return static_cast<int32_t>(offsetof(HtmlInputImage_t1805692912, ___clicked_y_32)); }
	inline int32_t get_clicked_y_32() const { return ___clicked_y_32; }
	inline int32_t* get_address_of_clicked_y_32() { return &___clicked_y_32; }
	inline void set_clicked_y_32(int32_t value)
	{
		___clicked_y_32 = value;
	}
};

struct HtmlInputImage_t1805692912_StaticFields
{
public:
	// System.Object System.Web.UI.HtmlControls.HtmlInputImage::ServerClickEvent
	Il2CppObject * ___ServerClickEvent_30;

public:
	inline static int32_t get_offset_of_ServerClickEvent_30() { return static_cast<int32_t>(offsetof(HtmlInputImage_t1805692912_StaticFields, ___ServerClickEvent_30)); }
	inline Il2CppObject * get_ServerClickEvent_30() const { return ___ServerClickEvent_30; }
	inline Il2CppObject ** get_address_of_ServerClickEvent_30() { return &___ServerClickEvent_30; }
	inline void set_ServerClickEvent_30(Il2CppObject * value)
	{
		___ServerClickEvent_30 = value;
		Il2CppCodeGenWriteBarrier(&___ServerClickEvent_30, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
