﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_System_ServiceModel_Descriptio1024212837.h"

// System.ServiceModel.Dispatcher.ClientRuntime
struct ClientRuntime_t3616476419;
// System.ServiceModel.Dispatcher.DispatchOperation/DispatchOperationCollection
struct DispatchOperationCollection_t3008162935;
// System.ServiceModel.Dispatcher.EndpointDispatcher
struct EndpointDispatcher_t2708702820;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IdentityModel.Policy.IAuthorizationPolicy>
struct ReadOnlyCollection_1_t778487864;
// System.Collections.Generic.SynchronizedCollection`1<System.ServiceModel.Dispatcher.IInputSessionShutdown>
struct SynchronizedCollection_1_t2851569720;
// System.Collections.Generic.SynchronizedCollection`1<System.ServiceModel.Dispatcher.IInstanceContextInitializer>
struct SynchronizedCollection_1_t2202694170;
// System.ServiceModel.Dispatcher.IInstanceProvider
struct IInstanceProvider_t267968183;
// System.ServiceModel.Dispatcher.IInstanceContextProvider
struct IInstanceContextProvider_t1424775303;
// System.Collections.Generic.SynchronizedCollection`1<System.ServiceModel.Dispatcher.IDispatchMessageInspector>
struct SynchronizedCollection_1_t569559589;
// System.ServiceModel.Dispatcher.IDispatchOperationSelector
struct IDispatchOperationSelector_t3911127062;
// System.Web.Security.RoleProvider
struct RoleProvider_t1137021788;
// System.ServiceModel.ServiceAuthorizationManager
struct ServiceAuthorizationManager_t56182799;
// System.ServiceModel.InstanceContext
struct InstanceContext_t3593205954;
// System.Type
struct Type_t;
// System.ServiceModel.Dispatcher.DispatchOperation
struct DispatchOperation_t1810306617;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.DispatchRuntime
struct  DispatchRuntime_t796075230  : public Il2CppObject
{
public:
	// System.ServiceModel.Dispatcher.ClientRuntime System.ServiceModel.Dispatcher.DispatchRuntime::callback_client_runtime
	ClientRuntime_t3616476419 * ___callback_client_runtime_0;
	// System.ServiceModel.Dispatcher.DispatchOperation/DispatchOperationCollection System.ServiceModel.Dispatcher.DispatchRuntime::operations
	DispatchOperationCollection_t3008162935 * ___operations_1;
	// System.Boolean System.ServiceModel.Dispatcher.DispatchRuntime::<AutomaticInputSessionShutdown>k__BackingField
	bool ___U3CAutomaticInputSessionShutdownU3Ek__BackingField_2;
	// System.ServiceModel.Dispatcher.EndpointDispatcher System.ServiceModel.Dispatcher.DispatchRuntime::<EndpointDispatcher>k__BackingField
	EndpointDispatcher_t2708702820 * ___U3CEndpointDispatcherU3Ek__BackingField_3;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IdentityModel.Policy.IAuthorizationPolicy> System.ServiceModel.Dispatcher.DispatchRuntime::<ExternalAuthorizationPolicies>k__BackingField
	ReadOnlyCollection_1_t778487864 * ___U3CExternalAuthorizationPoliciesU3Ek__BackingField_4;
	// System.Boolean System.ServiceModel.Dispatcher.DispatchRuntime::<ImpersonateCallerForAllOperations>k__BackingField
	bool ___U3CImpersonateCallerForAllOperationsU3Ek__BackingField_5;
	// System.Collections.Generic.SynchronizedCollection`1<System.ServiceModel.Dispatcher.IInputSessionShutdown> System.ServiceModel.Dispatcher.DispatchRuntime::<InputSessionShutdownHandlers>k__BackingField
	SynchronizedCollection_1_t2851569720 * ___U3CInputSessionShutdownHandlersU3Ek__BackingField_6;
	// System.Collections.Generic.SynchronizedCollection`1<System.ServiceModel.Dispatcher.IInstanceContextInitializer> System.ServiceModel.Dispatcher.DispatchRuntime::<InstanceContextInitializers>k__BackingField
	SynchronizedCollection_1_t2202694170 * ___U3CInstanceContextInitializersU3Ek__BackingField_7;
	// System.ServiceModel.Dispatcher.IInstanceProvider System.ServiceModel.Dispatcher.DispatchRuntime::<InstanceProvider>k__BackingField
	Il2CppObject * ___U3CInstanceProviderU3Ek__BackingField_8;
	// System.ServiceModel.Dispatcher.IInstanceContextProvider System.ServiceModel.Dispatcher.DispatchRuntime::<InstanceContextProvider>k__BackingField
	Il2CppObject * ___U3CInstanceContextProviderU3Ek__BackingField_9;
	// System.Collections.Generic.SynchronizedCollection`1<System.ServiceModel.Dispatcher.IDispatchMessageInspector> System.ServiceModel.Dispatcher.DispatchRuntime::<MessageInspectors>k__BackingField
	SynchronizedCollection_1_t569559589 * ___U3CMessageInspectorsU3Ek__BackingField_10;
	// System.ServiceModel.Dispatcher.IDispatchOperationSelector System.ServiceModel.Dispatcher.DispatchRuntime::<OperationSelector>k__BackingField
	Il2CppObject * ___U3COperationSelectorU3Ek__BackingField_11;
	// System.ServiceModel.Description.PrincipalPermissionMode System.ServiceModel.Dispatcher.DispatchRuntime::<PrincipalPermissionMode>k__BackingField
	int32_t ___U3CPrincipalPermissionModeU3Ek__BackingField_12;
	// System.Web.Security.RoleProvider System.ServiceModel.Dispatcher.DispatchRuntime::<RoleProvider>k__BackingField
	RoleProvider_t1137021788 * ___U3CRoleProviderU3Ek__BackingField_13;
	// System.ServiceModel.ServiceAuthorizationManager System.ServiceModel.Dispatcher.DispatchRuntime::<ServiceAuthorizationManager>k__BackingField
	ServiceAuthorizationManager_t56182799 * ___U3CServiceAuthorizationManagerU3Ek__BackingField_14;
	// System.ServiceModel.InstanceContext System.ServiceModel.Dispatcher.DispatchRuntime::<SingletonInstanceContext>k__BackingField
	InstanceContext_t3593205954 * ___U3CSingletonInstanceContextU3Ek__BackingField_15;
	// System.Boolean System.ServiceModel.Dispatcher.DispatchRuntime::<SuppressAuditFailure>k__BackingField
	bool ___U3CSuppressAuditFailureU3Ek__BackingField_16;
	// System.Type System.ServiceModel.Dispatcher.DispatchRuntime::<Type>k__BackingField
	Type_t * ___U3CTypeU3Ek__BackingField_17;
	// System.ServiceModel.Dispatcher.DispatchOperation System.ServiceModel.Dispatcher.DispatchRuntime::<UnhandledDispatchOperation>k__BackingField
	DispatchOperation_t1810306617 * ___U3CUnhandledDispatchOperationU3Ek__BackingField_18;
	// System.Boolean System.ServiceModel.Dispatcher.DispatchRuntime::<ValidateMustUnderstand>k__BackingField
	bool ___U3CValidateMustUnderstandU3Ek__BackingField_19;

public:
	inline static int32_t get_offset_of_callback_client_runtime_0() { return static_cast<int32_t>(offsetof(DispatchRuntime_t796075230, ___callback_client_runtime_0)); }
	inline ClientRuntime_t3616476419 * get_callback_client_runtime_0() const { return ___callback_client_runtime_0; }
	inline ClientRuntime_t3616476419 ** get_address_of_callback_client_runtime_0() { return &___callback_client_runtime_0; }
	inline void set_callback_client_runtime_0(ClientRuntime_t3616476419 * value)
	{
		___callback_client_runtime_0 = value;
		Il2CppCodeGenWriteBarrier(&___callback_client_runtime_0, value);
	}

	inline static int32_t get_offset_of_operations_1() { return static_cast<int32_t>(offsetof(DispatchRuntime_t796075230, ___operations_1)); }
	inline DispatchOperationCollection_t3008162935 * get_operations_1() const { return ___operations_1; }
	inline DispatchOperationCollection_t3008162935 ** get_address_of_operations_1() { return &___operations_1; }
	inline void set_operations_1(DispatchOperationCollection_t3008162935 * value)
	{
		___operations_1 = value;
		Il2CppCodeGenWriteBarrier(&___operations_1, value);
	}

	inline static int32_t get_offset_of_U3CAutomaticInputSessionShutdownU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DispatchRuntime_t796075230, ___U3CAutomaticInputSessionShutdownU3Ek__BackingField_2)); }
	inline bool get_U3CAutomaticInputSessionShutdownU3Ek__BackingField_2() const { return ___U3CAutomaticInputSessionShutdownU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CAutomaticInputSessionShutdownU3Ek__BackingField_2() { return &___U3CAutomaticInputSessionShutdownU3Ek__BackingField_2; }
	inline void set_U3CAutomaticInputSessionShutdownU3Ek__BackingField_2(bool value)
	{
		___U3CAutomaticInputSessionShutdownU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CEndpointDispatcherU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DispatchRuntime_t796075230, ___U3CEndpointDispatcherU3Ek__BackingField_3)); }
	inline EndpointDispatcher_t2708702820 * get_U3CEndpointDispatcherU3Ek__BackingField_3() const { return ___U3CEndpointDispatcherU3Ek__BackingField_3; }
	inline EndpointDispatcher_t2708702820 ** get_address_of_U3CEndpointDispatcherU3Ek__BackingField_3() { return &___U3CEndpointDispatcherU3Ek__BackingField_3; }
	inline void set_U3CEndpointDispatcherU3Ek__BackingField_3(EndpointDispatcher_t2708702820 * value)
	{
		___U3CEndpointDispatcherU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CEndpointDispatcherU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CExternalAuthorizationPoliciesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DispatchRuntime_t796075230, ___U3CExternalAuthorizationPoliciesU3Ek__BackingField_4)); }
	inline ReadOnlyCollection_1_t778487864 * get_U3CExternalAuthorizationPoliciesU3Ek__BackingField_4() const { return ___U3CExternalAuthorizationPoliciesU3Ek__BackingField_4; }
	inline ReadOnlyCollection_1_t778487864 ** get_address_of_U3CExternalAuthorizationPoliciesU3Ek__BackingField_4() { return &___U3CExternalAuthorizationPoliciesU3Ek__BackingField_4; }
	inline void set_U3CExternalAuthorizationPoliciesU3Ek__BackingField_4(ReadOnlyCollection_1_t778487864 * value)
	{
		___U3CExternalAuthorizationPoliciesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CExternalAuthorizationPoliciesU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CImpersonateCallerForAllOperationsU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(DispatchRuntime_t796075230, ___U3CImpersonateCallerForAllOperationsU3Ek__BackingField_5)); }
	inline bool get_U3CImpersonateCallerForAllOperationsU3Ek__BackingField_5() const { return ___U3CImpersonateCallerForAllOperationsU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CImpersonateCallerForAllOperationsU3Ek__BackingField_5() { return &___U3CImpersonateCallerForAllOperationsU3Ek__BackingField_5; }
	inline void set_U3CImpersonateCallerForAllOperationsU3Ek__BackingField_5(bool value)
	{
		___U3CImpersonateCallerForAllOperationsU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CInputSessionShutdownHandlersU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(DispatchRuntime_t796075230, ___U3CInputSessionShutdownHandlersU3Ek__BackingField_6)); }
	inline SynchronizedCollection_1_t2851569720 * get_U3CInputSessionShutdownHandlersU3Ek__BackingField_6() const { return ___U3CInputSessionShutdownHandlersU3Ek__BackingField_6; }
	inline SynchronizedCollection_1_t2851569720 ** get_address_of_U3CInputSessionShutdownHandlersU3Ek__BackingField_6() { return &___U3CInputSessionShutdownHandlersU3Ek__BackingField_6; }
	inline void set_U3CInputSessionShutdownHandlersU3Ek__BackingField_6(SynchronizedCollection_1_t2851569720 * value)
	{
		___U3CInputSessionShutdownHandlersU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CInputSessionShutdownHandlersU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CInstanceContextInitializersU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(DispatchRuntime_t796075230, ___U3CInstanceContextInitializersU3Ek__BackingField_7)); }
	inline SynchronizedCollection_1_t2202694170 * get_U3CInstanceContextInitializersU3Ek__BackingField_7() const { return ___U3CInstanceContextInitializersU3Ek__BackingField_7; }
	inline SynchronizedCollection_1_t2202694170 ** get_address_of_U3CInstanceContextInitializersU3Ek__BackingField_7() { return &___U3CInstanceContextInitializersU3Ek__BackingField_7; }
	inline void set_U3CInstanceContextInitializersU3Ek__BackingField_7(SynchronizedCollection_1_t2202694170 * value)
	{
		___U3CInstanceContextInitializersU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CInstanceContextInitializersU3Ek__BackingField_7, value);
	}

	inline static int32_t get_offset_of_U3CInstanceProviderU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(DispatchRuntime_t796075230, ___U3CInstanceProviderU3Ek__BackingField_8)); }
	inline Il2CppObject * get_U3CInstanceProviderU3Ek__BackingField_8() const { return ___U3CInstanceProviderU3Ek__BackingField_8; }
	inline Il2CppObject ** get_address_of_U3CInstanceProviderU3Ek__BackingField_8() { return &___U3CInstanceProviderU3Ek__BackingField_8; }
	inline void set_U3CInstanceProviderU3Ek__BackingField_8(Il2CppObject * value)
	{
		___U3CInstanceProviderU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CInstanceProviderU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of_U3CInstanceContextProviderU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(DispatchRuntime_t796075230, ___U3CInstanceContextProviderU3Ek__BackingField_9)); }
	inline Il2CppObject * get_U3CInstanceContextProviderU3Ek__BackingField_9() const { return ___U3CInstanceContextProviderU3Ek__BackingField_9; }
	inline Il2CppObject ** get_address_of_U3CInstanceContextProviderU3Ek__BackingField_9() { return &___U3CInstanceContextProviderU3Ek__BackingField_9; }
	inline void set_U3CInstanceContextProviderU3Ek__BackingField_9(Il2CppObject * value)
	{
		___U3CInstanceContextProviderU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CInstanceContextProviderU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3CMessageInspectorsU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(DispatchRuntime_t796075230, ___U3CMessageInspectorsU3Ek__BackingField_10)); }
	inline SynchronizedCollection_1_t569559589 * get_U3CMessageInspectorsU3Ek__BackingField_10() const { return ___U3CMessageInspectorsU3Ek__BackingField_10; }
	inline SynchronizedCollection_1_t569559589 ** get_address_of_U3CMessageInspectorsU3Ek__BackingField_10() { return &___U3CMessageInspectorsU3Ek__BackingField_10; }
	inline void set_U3CMessageInspectorsU3Ek__BackingField_10(SynchronizedCollection_1_t569559589 * value)
	{
		___U3CMessageInspectorsU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMessageInspectorsU3Ek__BackingField_10, value);
	}

	inline static int32_t get_offset_of_U3COperationSelectorU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(DispatchRuntime_t796075230, ___U3COperationSelectorU3Ek__BackingField_11)); }
	inline Il2CppObject * get_U3COperationSelectorU3Ek__BackingField_11() const { return ___U3COperationSelectorU3Ek__BackingField_11; }
	inline Il2CppObject ** get_address_of_U3COperationSelectorU3Ek__BackingField_11() { return &___U3COperationSelectorU3Ek__BackingField_11; }
	inline void set_U3COperationSelectorU3Ek__BackingField_11(Il2CppObject * value)
	{
		___U3COperationSelectorU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3COperationSelectorU3Ek__BackingField_11, value);
	}

	inline static int32_t get_offset_of_U3CPrincipalPermissionModeU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(DispatchRuntime_t796075230, ___U3CPrincipalPermissionModeU3Ek__BackingField_12)); }
	inline int32_t get_U3CPrincipalPermissionModeU3Ek__BackingField_12() const { return ___U3CPrincipalPermissionModeU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CPrincipalPermissionModeU3Ek__BackingField_12() { return &___U3CPrincipalPermissionModeU3Ek__BackingField_12; }
	inline void set_U3CPrincipalPermissionModeU3Ek__BackingField_12(int32_t value)
	{
		___U3CPrincipalPermissionModeU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CRoleProviderU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(DispatchRuntime_t796075230, ___U3CRoleProviderU3Ek__BackingField_13)); }
	inline RoleProvider_t1137021788 * get_U3CRoleProviderU3Ek__BackingField_13() const { return ___U3CRoleProviderU3Ek__BackingField_13; }
	inline RoleProvider_t1137021788 ** get_address_of_U3CRoleProviderU3Ek__BackingField_13() { return &___U3CRoleProviderU3Ek__BackingField_13; }
	inline void set_U3CRoleProviderU3Ek__BackingField_13(RoleProvider_t1137021788 * value)
	{
		___U3CRoleProviderU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRoleProviderU3Ek__BackingField_13, value);
	}

	inline static int32_t get_offset_of_U3CServiceAuthorizationManagerU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(DispatchRuntime_t796075230, ___U3CServiceAuthorizationManagerU3Ek__BackingField_14)); }
	inline ServiceAuthorizationManager_t56182799 * get_U3CServiceAuthorizationManagerU3Ek__BackingField_14() const { return ___U3CServiceAuthorizationManagerU3Ek__BackingField_14; }
	inline ServiceAuthorizationManager_t56182799 ** get_address_of_U3CServiceAuthorizationManagerU3Ek__BackingField_14() { return &___U3CServiceAuthorizationManagerU3Ek__BackingField_14; }
	inline void set_U3CServiceAuthorizationManagerU3Ek__BackingField_14(ServiceAuthorizationManager_t56182799 * value)
	{
		___U3CServiceAuthorizationManagerU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CServiceAuthorizationManagerU3Ek__BackingField_14, value);
	}

	inline static int32_t get_offset_of_U3CSingletonInstanceContextU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(DispatchRuntime_t796075230, ___U3CSingletonInstanceContextU3Ek__BackingField_15)); }
	inline InstanceContext_t3593205954 * get_U3CSingletonInstanceContextU3Ek__BackingField_15() const { return ___U3CSingletonInstanceContextU3Ek__BackingField_15; }
	inline InstanceContext_t3593205954 ** get_address_of_U3CSingletonInstanceContextU3Ek__BackingField_15() { return &___U3CSingletonInstanceContextU3Ek__BackingField_15; }
	inline void set_U3CSingletonInstanceContextU3Ek__BackingField_15(InstanceContext_t3593205954 * value)
	{
		___U3CSingletonInstanceContextU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSingletonInstanceContextU3Ek__BackingField_15, value);
	}

	inline static int32_t get_offset_of_U3CSuppressAuditFailureU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(DispatchRuntime_t796075230, ___U3CSuppressAuditFailureU3Ek__BackingField_16)); }
	inline bool get_U3CSuppressAuditFailureU3Ek__BackingField_16() const { return ___U3CSuppressAuditFailureU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CSuppressAuditFailureU3Ek__BackingField_16() { return &___U3CSuppressAuditFailureU3Ek__BackingField_16; }
	inline void set_U3CSuppressAuditFailureU3Ek__BackingField_16(bool value)
	{
		___U3CSuppressAuditFailureU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(DispatchRuntime_t796075230, ___U3CTypeU3Ek__BackingField_17)); }
	inline Type_t * get_U3CTypeU3Ek__BackingField_17() const { return ___U3CTypeU3Ek__BackingField_17; }
	inline Type_t ** get_address_of_U3CTypeU3Ek__BackingField_17() { return &___U3CTypeU3Ek__BackingField_17; }
	inline void set_U3CTypeU3Ek__BackingField_17(Type_t * value)
	{
		___U3CTypeU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTypeU3Ek__BackingField_17, value);
	}

	inline static int32_t get_offset_of_U3CUnhandledDispatchOperationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(DispatchRuntime_t796075230, ___U3CUnhandledDispatchOperationU3Ek__BackingField_18)); }
	inline DispatchOperation_t1810306617 * get_U3CUnhandledDispatchOperationU3Ek__BackingField_18() const { return ___U3CUnhandledDispatchOperationU3Ek__BackingField_18; }
	inline DispatchOperation_t1810306617 ** get_address_of_U3CUnhandledDispatchOperationU3Ek__BackingField_18() { return &___U3CUnhandledDispatchOperationU3Ek__BackingField_18; }
	inline void set_U3CUnhandledDispatchOperationU3Ek__BackingField_18(DispatchOperation_t1810306617 * value)
	{
		___U3CUnhandledDispatchOperationU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUnhandledDispatchOperationU3Ek__BackingField_18, value);
	}

	inline static int32_t get_offset_of_U3CValidateMustUnderstandU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(DispatchRuntime_t796075230, ___U3CValidateMustUnderstandU3Ek__BackingField_19)); }
	inline bool get_U3CValidateMustUnderstandU3Ek__BackingField_19() const { return ___U3CValidateMustUnderstandU3Ek__BackingField_19; }
	inline bool* get_address_of_U3CValidateMustUnderstandU3Ek__BackingField_19() { return &___U3CValidateMustUnderstandU3Ek__BackingField_19; }
	inline void set_U3CValidateMustUnderstandU3Ek__BackingField_19(bool value)
	{
		___U3CValidateMustUnderstandU3Ek__BackingField_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
