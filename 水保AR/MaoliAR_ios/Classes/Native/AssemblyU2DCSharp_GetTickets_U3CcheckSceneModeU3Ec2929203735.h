﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<System.DateTime>
struct List_1_t915637231;
// GetTickets
struct GetTickets_t911946028;
// System.Object
struct Il2CppObject;
// GetTickets/<checkSceneMode>c__Iterator0/<checkSceneMode>c__AnonStorey6
struct U3CcheckSceneModeU3Ec__AnonStorey6_t1932449374;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetTickets/<checkSceneMode>c__Iterator0
struct  U3CcheckSceneModeU3Ec__Iterator0_t2929203735  : public Il2CppObject
{
public:
	// System.Int32 GetTickets/<checkSceneMode>c__Iterator0::<getScenesMode>__0
	int32_t ___U3CgetScenesModeU3E__0_0;
	// System.Int32 GetTickets/<checkSceneMode>c__Iterator0::<Number>__0
	int32_t ___U3CNumberU3E__0_1;
	// System.String GetTickets/<checkSceneMode>c__Iterator0::url
	String_t* ___url_2;
	// System.String[] GetTickets/<checkSceneMode>c__Iterator0::<temps>__0
	StringU5BU5D_t1281789340* ___U3CtempsU3E__0_3;
	// System.Boolean GetTickets/<checkSceneMode>c__Iterator0::<Expired>__0
	bool ___U3CExpiredU3E__0_4;
	// System.Int32 GetTickets/<checkSceneMode>c__Iterator0::<number>__0
	int32_t ___U3CnumberU3E__0_5;
	// System.String[] GetTickets/<checkSceneMode>c__Iterator0::<getTypesStr>__0
	StringU5BU5D_t1281789340* ___U3CgetTypesStrU3E__0_6;
	// System.Collections.Generic.List`1<System.Int32> GetTickets/<checkSceneMode>c__Iterator0::<getunExpiredTypes>__0
	List_1_t128053199 * ___U3CgetunExpiredTypesU3E__0_7;
	// System.Int32 GetTickets/<checkSceneMode>c__Iterator0::<findNumber>__0
	int32_t ___U3CfindNumberU3E__0_8;
	// System.Int32 GetTickets/<checkSceneMode>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_9;
	// System.Int32 GetTickets/<checkSceneMode>c__Iterator0::<nowPlayerCount>__0
	int32_t ___U3CnowPlayerCountU3E__0_10;
	// System.Int32 GetTickets/<checkSceneMode>c__Iterator0::<sceneMode>__0
	int32_t ___U3CsceneModeU3E__0_11;
	// System.Collections.Generic.List`1<System.DateTime> GetTickets/<checkSceneMode>c__Iterator0::<selectDateTime>__0
	List_1_t915637231 * ___U3CselectDateTimeU3E__0_12;
	// System.Boolean GetTickets/<checkSceneMode>c__Iterator0::<fix>__2
	bool ___U3CfixU3E__2_13;
	// System.Boolean GetTickets/<checkSceneMode>c__Iterator0::<b>__3
	bool ___U3CbU3E__3_14;
	// System.Boolean GetTickets/<checkSceneMode>c__Iterator0::<b>__4
	bool ___U3CbU3E__4_15;
	// GetTickets GetTickets/<checkSceneMode>c__Iterator0::$this
	GetTickets_t911946028 * ___U24this_16;
	// System.Object GetTickets/<checkSceneMode>c__Iterator0::$current
	Il2CppObject * ___U24current_17;
	// System.Boolean GetTickets/<checkSceneMode>c__Iterator0::$disposing
	bool ___U24disposing_18;
	// System.Int32 GetTickets/<checkSceneMode>c__Iterator0::$PC
	int32_t ___U24PC_19;
	// GetTickets/<checkSceneMode>c__Iterator0/<checkSceneMode>c__AnonStorey6 GetTickets/<checkSceneMode>c__Iterator0::$locvar0
	U3CcheckSceneModeU3Ec__AnonStorey6_t1932449374 * ___U24locvar0_20;

public:
	inline static int32_t get_offset_of_U3CgetScenesModeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CcheckSceneModeU3Ec__Iterator0_t2929203735, ___U3CgetScenesModeU3E__0_0)); }
	inline int32_t get_U3CgetScenesModeU3E__0_0() const { return ___U3CgetScenesModeU3E__0_0; }
	inline int32_t* get_address_of_U3CgetScenesModeU3E__0_0() { return &___U3CgetScenesModeU3E__0_0; }
	inline void set_U3CgetScenesModeU3E__0_0(int32_t value)
	{
		___U3CgetScenesModeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CNumberU3E__0_1() { return static_cast<int32_t>(offsetof(U3CcheckSceneModeU3Ec__Iterator0_t2929203735, ___U3CNumberU3E__0_1)); }
	inline int32_t get_U3CNumberU3E__0_1() const { return ___U3CNumberU3E__0_1; }
	inline int32_t* get_address_of_U3CNumberU3E__0_1() { return &___U3CNumberU3E__0_1; }
	inline void set_U3CNumberU3E__0_1(int32_t value)
	{
		___U3CNumberU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_url_2() { return static_cast<int32_t>(offsetof(U3CcheckSceneModeU3Ec__Iterator0_t2929203735, ___url_2)); }
	inline String_t* get_url_2() const { return ___url_2; }
	inline String_t** get_address_of_url_2() { return &___url_2; }
	inline void set_url_2(String_t* value)
	{
		___url_2 = value;
		Il2CppCodeGenWriteBarrier(&___url_2, value);
	}

	inline static int32_t get_offset_of_U3CtempsU3E__0_3() { return static_cast<int32_t>(offsetof(U3CcheckSceneModeU3Ec__Iterator0_t2929203735, ___U3CtempsU3E__0_3)); }
	inline StringU5BU5D_t1281789340* get_U3CtempsU3E__0_3() const { return ___U3CtempsU3E__0_3; }
	inline StringU5BU5D_t1281789340** get_address_of_U3CtempsU3E__0_3() { return &___U3CtempsU3E__0_3; }
	inline void set_U3CtempsU3E__0_3(StringU5BU5D_t1281789340* value)
	{
		___U3CtempsU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtempsU3E__0_3, value);
	}

	inline static int32_t get_offset_of_U3CExpiredU3E__0_4() { return static_cast<int32_t>(offsetof(U3CcheckSceneModeU3Ec__Iterator0_t2929203735, ___U3CExpiredU3E__0_4)); }
	inline bool get_U3CExpiredU3E__0_4() const { return ___U3CExpiredU3E__0_4; }
	inline bool* get_address_of_U3CExpiredU3E__0_4() { return &___U3CExpiredU3E__0_4; }
	inline void set_U3CExpiredU3E__0_4(bool value)
	{
		___U3CExpiredU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CnumberU3E__0_5() { return static_cast<int32_t>(offsetof(U3CcheckSceneModeU3Ec__Iterator0_t2929203735, ___U3CnumberU3E__0_5)); }
	inline int32_t get_U3CnumberU3E__0_5() const { return ___U3CnumberU3E__0_5; }
	inline int32_t* get_address_of_U3CnumberU3E__0_5() { return &___U3CnumberU3E__0_5; }
	inline void set_U3CnumberU3E__0_5(int32_t value)
	{
		___U3CnumberU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U3CgetTypesStrU3E__0_6() { return static_cast<int32_t>(offsetof(U3CcheckSceneModeU3Ec__Iterator0_t2929203735, ___U3CgetTypesStrU3E__0_6)); }
	inline StringU5BU5D_t1281789340* get_U3CgetTypesStrU3E__0_6() const { return ___U3CgetTypesStrU3E__0_6; }
	inline StringU5BU5D_t1281789340** get_address_of_U3CgetTypesStrU3E__0_6() { return &___U3CgetTypesStrU3E__0_6; }
	inline void set_U3CgetTypesStrU3E__0_6(StringU5BU5D_t1281789340* value)
	{
		___U3CgetTypesStrU3E__0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CgetTypesStrU3E__0_6, value);
	}

	inline static int32_t get_offset_of_U3CgetunExpiredTypesU3E__0_7() { return static_cast<int32_t>(offsetof(U3CcheckSceneModeU3Ec__Iterator0_t2929203735, ___U3CgetunExpiredTypesU3E__0_7)); }
	inline List_1_t128053199 * get_U3CgetunExpiredTypesU3E__0_7() const { return ___U3CgetunExpiredTypesU3E__0_7; }
	inline List_1_t128053199 ** get_address_of_U3CgetunExpiredTypesU3E__0_7() { return &___U3CgetunExpiredTypesU3E__0_7; }
	inline void set_U3CgetunExpiredTypesU3E__0_7(List_1_t128053199 * value)
	{
		___U3CgetunExpiredTypesU3E__0_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CgetunExpiredTypesU3E__0_7, value);
	}

	inline static int32_t get_offset_of_U3CfindNumberU3E__0_8() { return static_cast<int32_t>(offsetof(U3CcheckSceneModeU3Ec__Iterator0_t2929203735, ___U3CfindNumberU3E__0_8)); }
	inline int32_t get_U3CfindNumberU3E__0_8() const { return ___U3CfindNumberU3E__0_8; }
	inline int32_t* get_address_of_U3CfindNumberU3E__0_8() { return &___U3CfindNumberU3E__0_8; }
	inline void set_U3CfindNumberU3E__0_8(int32_t value)
	{
		___U3CfindNumberU3E__0_8 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__1_9() { return static_cast<int32_t>(offsetof(U3CcheckSceneModeU3Ec__Iterator0_t2929203735, ___U3CiU3E__1_9)); }
	inline int32_t get_U3CiU3E__1_9() const { return ___U3CiU3E__1_9; }
	inline int32_t* get_address_of_U3CiU3E__1_9() { return &___U3CiU3E__1_9; }
	inline void set_U3CiU3E__1_9(int32_t value)
	{
		___U3CiU3E__1_9 = value;
	}

	inline static int32_t get_offset_of_U3CnowPlayerCountU3E__0_10() { return static_cast<int32_t>(offsetof(U3CcheckSceneModeU3Ec__Iterator0_t2929203735, ___U3CnowPlayerCountU3E__0_10)); }
	inline int32_t get_U3CnowPlayerCountU3E__0_10() const { return ___U3CnowPlayerCountU3E__0_10; }
	inline int32_t* get_address_of_U3CnowPlayerCountU3E__0_10() { return &___U3CnowPlayerCountU3E__0_10; }
	inline void set_U3CnowPlayerCountU3E__0_10(int32_t value)
	{
		___U3CnowPlayerCountU3E__0_10 = value;
	}

	inline static int32_t get_offset_of_U3CsceneModeU3E__0_11() { return static_cast<int32_t>(offsetof(U3CcheckSceneModeU3Ec__Iterator0_t2929203735, ___U3CsceneModeU3E__0_11)); }
	inline int32_t get_U3CsceneModeU3E__0_11() const { return ___U3CsceneModeU3E__0_11; }
	inline int32_t* get_address_of_U3CsceneModeU3E__0_11() { return &___U3CsceneModeU3E__0_11; }
	inline void set_U3CsceneModeU3E__0_11(int32_t value)
	{
		___U3CsceneModeU3E__0_11 = value;
	}

	inline static int32_t get_offset_of_U3CselectDateTimeU3E__0_12() { return static_cast<int32_t>(offsetof(U3CcheckSceneModeU3Ec__Iterator0_t2929203735, ___U3CselectDateTimeU3E__0_12)); }
	inline List_1_t915637231 * get_U3CselectDateTimeU3E__0_12() const { return ___U3CselectDateTimeU3E__0_12; }
	inline List_1_t915637231 ** get_address_of_U3CselectDateTimeU3E__0_12() { return &___U3CselectDateTimeU3E__0_12; }
	inline void set_U3CselectDateTimeU3E__0_12(List_1_t915637231 * value)
	{
		___U3CselectDateTimeU3E__0_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CselectDateTimeU3E__0_12, value);
	}

	inline static int32_t get_offset_of_U3CfixU3E__2_13() { return static_cast<int32_t>(offsetof(U3CcheckSceneModeU3Ec__Iterator0_t2929203735, ___U3CfixU3E__2_13)); }
	inline bool get_U3CfixU3E__2_13() const { return ___U3CfixU3E__2_13; }
	inline bool* get_address_of_U3CfixU3E__2_13() { return &___U3CfixU3E__2_13; }
	inline void set_U3CfixU3E__2_13(bool value)
	{
		___U3CfixU3E__2_13 = value;
	}

	inline static int32_t get_offset_of_U3CbU3E__3_14() { return static_cast<int32_t>(offsetof(U3CcheckSceneModeU3Ec__Iterator0_t2929203735, ___U3CbU3E__3_14)); }
	inline bool get_U3CbU3E__3_14() const { return ___U3CbU3E__3_14; }
	inline bool* get_address_of_U3CbU3E__3_14() { return &___U3CbU3E__3_14; }
	inline void set_U3CbU3E__3_14(bool value)
	{
		___U3CbU3E__3_14 = value;
	}

	inline static int32_t get_offset_of_U3CbU3E__4_15() { return static_cast<int32_t>(offsetof(U3CcheckSceneModeU3Ec__Iterator0_t2929203735, ___U3CbU3E__4_15)); }
	inline bool get_U3CbU3E__4_15() const { return ___U3CbU3E__4_15; }
	inline bool* get_address_of_U3CbU3E__4_15() { return &___U3CbU3E__4_15; }
	inline void set_U3CbU3E__4_15(bool value)
	{
		___U3CbU3E__4_15 = value;
	}

	inline static int32_t get_offset_of_U24this_16() { return static_cast<int32_t>(offsetof(U3CcheckSceneModeU3Ec__Iterator0_t2929203735, ___U24this_16)); }
	inline GetTickets_t911946028 * get_U24this_16() const { return ___U24this_16; }
	inline GetTickets_t911946028 ** get_address_of_U24this_16() { return &___U24this_16; }
	inline void set_U24this_16(GetTickets_t911946028 * value)
	{
		___U24this_16 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_16, value);
	}

	inline static int32_t get_offset_of_U24current_17() { return static_cast<int32_t>(offsetof(U3CcheckSceneModeU3Ec__Iterator0_t2929203735, ___U24current_17)); }
	inline Il2CppObject * get_U24current_17() const { return ___U24current_17; }
	inline Il2CppObject ** get_address_of_U24current_17() { return &___U24current_17; }
	inline void set_U24current_17(Il2CppObject * value)
	{
		___U24current_17 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_17, value);
	}

	inline static int32_t get_offset_of_U24disposing_18() { return static_cast<int32_t>(offsetof(U3CcheckSceneModeU3Ec__Iterator0_t2929203735, ___U24disposing_18)); }
	inline bool get_U24disposing_18() const { return ___U24disposing_18; }
	inline bool* get_address_of_U24disposing_18() { return &___U24disposing_18; }
	inline void set_U24disposing_18(bool value)
	{
		___U24disposing_18 = value;
	}

	inline static int32_t get_offset_of_U24PC_19() { return static_cast<int32_t>(offsetof(U3CcheckSceneModeU3Ec__Iterator0_t2929203735, ___U24PC_19)); }
	inline int32_t get_U24PC_19() const { return ___U24PC_19; }
	inline int32_t* get_address_of_U24PC_19() { return &___U24PC_19; }
	inline void set_U24PC_19(int32_t value)
	{
		___U24PC_19 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_20() { return static_cast<int32_t>(offsetof(U3CcheckSceneModeU3Ec__Iterator0_t2929203735, ___U24locvar0_20)); }
	inline U3CcheckSceneModeU3Ec__AnonStorey6_t1932449374 * get_U24locvar0_20() const { return ___U24locvar0_20; }
	inline U3CcheckSceneModeU3Ec__AnonStorey6_t1932449374 ** get_address_of_U24locvar0_20() { return &___U24locvar0_20; }
	inline void set_U24locvar0_20(U3CcheckSceneModeU3Ec__AnonStorey6_t1932449374 * value)
	{
		___U24locvar0_20 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
