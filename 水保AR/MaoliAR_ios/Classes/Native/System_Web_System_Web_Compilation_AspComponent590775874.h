﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Type
struct Type_t;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.AspComponent
struct  AspComponent_t590775874  : public Il2CppObject
{
public:
	// System.Type System.Web.Compilation.AspComponent::Type
	Type_t * ___Type_0;
	// System.String System.Web.Compilation.AspComponent::Prefix
	String_t* ___Prefix_1;
	// System.String System.Web.Compilation.AspComponent::Source
	String_t* ___Source_2;
	// System.Boolean System.Web.Compilation.AspComponent::FromConfig
	bool ___FromConfig_3;
	// System.String System.Web.Compilation.AspComponent::Namespace
	String_t* ___Namespace_4;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(AspComponent_t590775874, ___Type_0)); }
	inline Type_t * get_Type_0() const { return ___Type_0; }
	inline Type_t ** get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(Type_t * value)
	{
		___Type_0 = value;
		Il2CppCodeGenWriteBarrier(&___Type_0, value);
	}

	inline static int32_t get_offset_of_Prefix_1() { return static_cast<int32_t>(offsetof(AspComponent_t590775874, ___Prefix_1)); }
	inline String_t* get_Prefix_1() const { return ___Prefix_1; }
	inline String_t** get_address_of_Prefix_1() { return &___Prefix_1; }
	inline void set_Prefix_1(String_t* value)
	{
		___Prefix_1 = value;
		Il2CppCodeGenWriteBarrier(&___Prefix_1, value);
	}

	inline static int32_t get_offset_of_Source_2() { return static_cast<int32_t>(offsetof(AspComponent_t590775874, ___Source_2)); }
	inline String_t* get_Source_2() const { return ___Source_2; }
	inline String_t** get_address_of_Source_2() { return &___Source_2; }
	inline void set_Source_2(String_t* value)
	{
		___Source_2 = value;
		Il2CppCodeGenWriteBarrier(&___Source_2, value);
	}

	inline static int32_t get_offset_of_FromConfig_3() { return static_cast<int32_t>(offsetof(AspComponent_t590775874, ___FromConfig_3)); }
	inline bool get_FromConfig_3() const { return ___FromConfig_3; }
	inline bool* get_address_of_FromConfig_3() { return &___FromConfig_3; }
	inline void set_FromConfig_3(bool value)
	{
		___FromConfig_3 = value;
	}

	inline static int32_t get_offset_of_Namespace_4() { return static_cast<int32_t>(offsetof(AspComponent_t590775874, ___Namespace_4)); }
	inline String_t* get_Namespace_4() const { return ___Namespace_4; }
	inline String_t** get_address_of_Namespace_4() { return &___Namespace_4; }
	inline void set_Namespace_4(String_t* value)
	{
		___Namespace_4 = value;
		Il2CppCodeGenWriteBarrier(&___Namespace_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
