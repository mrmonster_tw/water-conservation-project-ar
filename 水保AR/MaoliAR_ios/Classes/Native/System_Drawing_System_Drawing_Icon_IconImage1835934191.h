﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"
#include "System_Drawing_System_Drawing_Icon_BitmapInfoHeade2752024591.h"

// System.UInt32[]
struct UInt32U5BU5D_t2770800703;
// System.Byte[]
struct ByteU5BU5D_t4116647657;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Drawing.Icon/IconImage
struct  IconImage_t1835934191 
{
public:
	// System.Drawing.Icon/BitmapInfoHeader System.Drawing.Icon/IconImage::iconHeader
	BitmapInfoHeader_t2752024591  ___iconHeader_0;
	// System.UInt32[] System.Drawing.Icon/IconImage::iconColors
	UInt32U5BU5D_t2770800703* ___iconColors_1;
	// System.Byte[] System.Drawing.Icon/IconImage::iconXOR
	ByteU5BU5D_t4116647657* ___iconXOR_2;
	// System.Byte[] System.Drawing.Icon/IconImage::iconAND
	ByteU5BU5D_t4116647657* ___iconAND_3;

public:
	inline static int32_t get_offset_of_iconHeader_0() { return static_cast<int32_t>(offsetof(IconImage_t1835934191, ___iconHeader_0)); }
	inline BitmapInfoHeader_t2752024591  get_iconHeader_0() const { return ___iconHeader_0; }
	inline BitmapInfoHeader_t2752024591 * get_address_of_iconHeader_0() { return &___iconHeader_0; }
	inline void set_iconHeader_0(BitmapInfoHeader_t2752024591  value)
	{
		___iconHeader_0 = value;
	}

	inline static int32_t get_offset_of_iconColors_1() { return static_cast<int32_t>(offsetof(IconImage_t1835934191, ___iconColors_1)); }
	inline UInt32U5BU5D_t2770800703* get_iconColors_1() const { return ___iconColors_1; }
	inline UInt32U5BU5D_t2770800703** get_address_of_iconColors_1() { return &___iconColors_1; }
	inline void set_iconColors_1(UInt32U5BU5D_t2770800703* value)
	{
		___iconColors_1 = value;
		Il2CppCodeGenWriteBarrier(&___iconColors_1, value);
	}

	inline static int32_t get_offset_of_iconXOR_2() { return static_cast<int32_t>(offsetof(IconImage_t1835934191, ___iconXOR_2)); }
	inline ByteU5BU5D_t4116647657* get_iconXOR_2() const { return ___iconXOR_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_iconXOR_2() { return &___iconXOR_2; }
	inline void set_iconXOR_2(ByteU5BU5D_t4116647657* value)
	{
		___iconXOR_2 = value;
		Il2CppCodeGenWriteBarrier(&___iconXOR_2, value);
	}

	inline static int32_t get_offset_of_iconAND_3() { return static_cast<int32_t>(offsetof(IconImage_t1835934191, ___iconAND_3)); }
	inline ByteU5BU5D_t4116647657* get_iconAND_3() const { return ___iconAND_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_iconAND_3() { return &___iconAND_3; }
	inline void set_iconAND_3(ByteU5BU5D_t4116647657* value)
	{
		___iconAND_3 = value;
		Il2CppCodeGenWriteBarrier(&___iconAND_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Drawing.Icon/IconImage
struct IconImage_t1835934191_marshaled_pinvoke
{
	BitmapInfoHeader_t2752024591  ___iconHeader_0;
	uint32_t* ___iconColors_1;
	uint8_t* ___iconXOR_2;
	uint8_t* ___iconAND_3;
};
// Native definition for COM marshalling of System.Drawing.Icon/IconImage
struct IconImage_t1835934191_marshaled_com
{
	BitmapInfoHeader_t2752024591  ___iconHeader_0;
	uint32_t* ___iconColors_1;
	uint8_t* ___iconXOR_2;
	uint8_t* ___iconAND_3;
};
