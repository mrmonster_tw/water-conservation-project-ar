﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Selector1981162148.h"

// System.String[]
struct StringU5BU5D_t1281789340;
// System.ServiceModel.MessageSecurityVersion/MessageSecurityTokenVersion
struct MessageSecurityTokenVersion_t2151590395;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.String>
struct ReadOnlyCollection_1_t3060026976;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.MessageSecurityVersion/MessageSecurityTokenVersion
struct  MessageSecurityTokenVersion_t2151590395  : public SecurityTokenVersion_t1981162148
{
public:
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.String> System.ServiceModel.MessageSecurityVersion/MessageSecurityTokenVersion::specs
	ReadOnlyCollection_1_t3060026976 * ___specs_6;

public:
	inline static int32_t get_offset_of_specs_6() { return static_cast<int32_t>(offsetof(MessageSecurityTokenVersion_t2151590395, ___specs_6)); }
	inline ReadOnlyCollection_1_t3060026976 * get_specs_6() const { return ___specs_6; }
	inline ReadOnlyCollection_1_t3060026976 ** get_address_of_specs_6() { return &___specs_6; }
	inline void set_specs_6(ReadOnlyCollection_1_t3060026976 * value)
	{
		___specs_6 = value;
		Il2CppCodeGenWriteBarrier(&___specs_6, value);
	}
};

struct MessageSecurityTokenVersion_t2151590395_StaticFields
{
public:
	// System.String[] System.ServiceModel.MessageSecurityVersion/MessageSecurityTokenVersion::specs10_profile_source
	StringU5BU5D_t1281789340* ___specs10_profile_source_0;
	// System.String[] System.ServiceModel.MessageSecurityVersion/MessageSecurityTokenVersion::specs11_source
	StringU5BU5D_t1281789340* ___specs11_source_1;
	// System.String[] System.ServiceModel.MessageSecurityVersion/MessageSecurityTokenVersion::specs11_profile_source
	StringU5BU5D_t1281789340* ___specs11_profile_source_2;
	// System.ServiceModel.MessageSecurityVersion/MessageSecurityTokenVersion System.ServiceModel.MessageSecurityVersion/MessageSecurityTokenVersion::wss10basic
	MessageSecurityTokenVersion_t2151590395 * ___wss10basic_3;
	// System.ServiceModel.MessageSecurityVersion/MessageSecurityTokenVersion System.ServiceModel.MessageSecurityVersion/MessageSecurityTokenVersion::wss11
	MessageSecurityTokenVersion_t2151590395 * ___wss11_4;
	// System.ServiceModel.MessageSecurityVersion/MessageSecurityTokenVersion System.ServiceModel.MessageSecurityVersion/MessageSecurityTokenVersion::wss11basic
	MessageSecurityTokenVersion_t2151590395 * ___wss11basic_5;

public:
	inline static int32_t get_offset_of_specs10_profile_source_0() { return static_cast<int32_t>(offsetof(MessageSecurityTokenVersion_t2151590395_StaticFields, ___specs10_profile_source_0)); }
	inline StringU5BU5D_t1281789340* get_specs10_profile_source_0() const { return ___specs10_profile_source_0; }
	inline StringU5BU5D_t1281789340** get_address_of_specs10_profile_source_0() { return &___specs10_profile_source_0; }
	inline void set_specs10_profile_source_0(StringU5BU5D_t1281789340* value)
	{
		___specs10_profile_source_0 = value;
		Il2CppCodeGenWriteBarrier(&___specs10_profile_source_0, value);
	}

	inline static int32_t get_offset_of_specs11_source_1() { return static_cast<int32_t>(offsetof(MessageSecurityTokenVersion_t2151590395_StaticFields, ___specs11_source_1)); }
	inline StringU5BU5D_t1281789340* get_specs11_source_1() const { return ___specs11_source_1; }
	inline StringU5BU5D_t1281789340** get_address_of_specs11_source_1() { return &___specs11_source_1; }
	inline void set_specs11_source_1(StringU5BU5D_t1281789340* value)
	{
		___specs11_source_1 = value;
		Il2CppCodeGenWriteBarrier(&___specs11_source_1, value);
	}

	inline static int32_t get_offset_of_specs11_profile_source_2() { return static_cast<int32_t>(offsetof(MessageSecurityTokenVersion_t2151590395_StaticFields, ___specs11_profile_source_2)); }
	inline StringU5BU5D_t1281789340* get_specs11_profile_source_2() const { return ___specs11_profile_source_2; }
	inline StringU5BU5D_t1281789340** get_address_of_specs11_profile_source_2() { return &___specs11_profile_source_2; }
	inline void set_specs11_profile_source_2(StringU5BU5D_t1281789340* value)
	{
		___specs11_profile_source_2 = value;
		Il2CppCodeGenWriteBarrier(&___specs11_profile_source_2, value);
	}

	inline static int32_t get_offset_of_wss10basic_3() { return static_cast<int32_t>(offsetof(MessageSecurityTokenVersion_t2151590395_StaticFields, ___wss10basic_3)); }
	inline MessageSecurityTokenVersion_t2151590395 * get_wss10basic_3() const { return ___wss10basic_3; }
	inline MessageSecurityTokenVersion_t2151590395 ** get_address_of_wss10basic_3() { return &___wss10basic_3; }
	inline void set_wss10basic_3(MessageSecurityTokenVersion_t2151590395 * value)
	{
		___wss10basic_3 = value;
		Il2CppCodeGenWriteBarrier(&___wss10basic_3, value);
	}

	inline static int32_t get_offset_of_wss11_4() { return static_cast<int32_t>(offsetof(MessageSecurityTokenVersion_t2151590395_StaticFields, ___wss11_4)); }
	inline MessageSecurityTokenVersion_t2151590395 * get_wss11_4() const { return ___wss11_4; }
	inline MessageSecurityTokenVersion_t2151590395 ** get_address_of_wss11_4() { return &___wss11_4; }
	inline void set_wss11_4(MessageSecurityTokenVersion_t2151590395 * value)
	{
		___wss11_4 = value;
		Il2CppCodeGenWriteBarrier(&___wss11_4, value);
	}

	inline static int32_t get_offset_of_wss11basic_5() { return static_cast<int32_t>(offsetof(MessageSecurityTokenVersion_t2151590395_StaticFields, ___wss11basic_5)); }
	inline MessageSecurityTokenVersion_t2151590395 * get_wss11basic_5() const { return ___wss11basic_5; }
	inline MessageSecurityTokenVersion_t2151590395 ** get_address_of_wss11basic_5() { return &___wss11basic_5; }
	inline void set_wss11basic_5(MessageSecurityTokenVersion_t2151590395 * value)
	{
		___wss11basic_5 = value;
		Il2CppCodeGenWriteBarrier(&___wss11basic_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
