﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.IO.SearchPattern2/Op
struct Op_t3134810481;
// System.String
struct String_t;
// System.Char[]
struct CharU5BU5D_t3528271667;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.SearchPattern2
struct  SearchPattern2_t2824637351  : public Il2CppObject
{
public:
	// System.IO.SearchPattern2/Op System.IO.SearchPattern2::ops
	Op_t3134810481 * ___ops_0;
	// System.Boolean System.IO.SearchPattern2::ignore
	bool ___ignore_1;
	// System.Boolean System.IO.SearchPattern2::hasWildcard
	bool ___hasWildcard_2;
	// System.String System.IO.SearchPattern2::pattern
	String_t* ___pattern_3;

public:
	inline static int32_t get_offset_of_ops_0() { return static_cast<int32_t>(offsetof(SearchPattern2_t2824637351, ___ops_0)); }
	inline Op_t3134810481 * get_ops_0() const { return ___ops_0; }
	inline Op_t3134810481 ** get_address_of_ops_0() { return &___ops_0; }
	inline void set_ops_0(Op_t3134810481 * value)
	{
		___ops_0 = value;
		Il2CppCodeGenWriteBarrier(&___ops_0, value);
	}

	inline static int32_t get_offset_of_ignore_1() { return static_cast<int32_t>(offsetof(SearchPattern2_t2824637351, ___ignore_1)); }
	inline bool get_ignore_1() const { return ___ignore_1; }
	inline bool* get_address_of_ignore_1() { return &___ignore_1; }
	inline void set_ignore_1(bool value)
	{
		___ignore_1 = value;
	}

	inline static int32_t get_offset_of_hasWildcard_2() { return static_cast<int32_t>(offsetof(SearchPattern2_t2824637351, ___hasWildcard_2)); }
	inline bool get_hasWildcard_2() const { return ___hasWildcard_2; }
	inline bool* get_address_of_hasWildcard_2() { return &___hasWildcard_2; }
	inline void set_hasWildcard_2(bool value)
	{
		___hasWildcard_2 = value;
	}

	inline static int32_t get_offset_of_pattern_3() { return static_cast<int32_t>(offsetof(SearchPattern2_t2824637351, ___pattern_3)); }
	inline String_t* get_pattern_3() const { return ___pattern_3; }
	inline String_t** get_address_of_pattern_3() { return &___pattern_3; }
	inline void set_pattern_3(String_t* value)
	{
		___pattern_3 = value;
		Il2CppCodeGenWriteBarrier(&___pattern_3, value);
	}
};

struct SearchPattern2_t2824637351_StaticFields
{
public:
	// System.Char[] System.IO.SearchPattern2::WildcardChars
	CharU5BU5D_t3528271667* ___WildcardChars_4;
	// System.Char[] System.IO.SearchPattern2::InvalidChars
	CharU5BU5D_t3528271667* ___InvalidChars_5;

public:
	inline static int32_t get_offset_of_WildcardChars_4() { return static_cast<int32_t>(offsetof(SearchPattern2_t2824637351_StaticFields, ___WildcardChars_4)); }
	inline CharU5BU5D_t3528271667* get_WildcardChars_4() const { return ___WildcardChars_4; }
	inline CharU5BU5D_t3528271667** get_address_of_WildcardChars_4() { return &___WildcardChars_4; }
	inline void set_WildcardChars_4(CharU5BU5D_t3528271667* value)
	{
		___WildcardChars_4 = value;
		Il2CppCodeGenWriteBarrier(&___WildcardChars_4, value);
	}

	inline static int32_t get_offset_of_InvalidChars_5() { return static_cast<int32_t>(offsetof(SearchPattern2_t2824637351_StaticFields, ___InvalidChars_5)); }
	inline CharU5BU5D_t3528271667* get_InvalidChars_5() const { return ___InvalidChars_5; }
	inline CharU5BU5D_t3528271667** get_address_of_InvalidChars_5() { return &___InvalidChars_5; }
	inline void set_InvalidChars_5(CharU5BU5D_t3528271667* value)
	{
		___InvalidChars_5 = value;
		Il2CppCodeGenWriteBarrier(&___InvalidChars_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
