﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Hashtable
struct Hashtable_t1853889766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpStaticObjectsCollection
struct  HttpStaticObjectsCollection_t518175362  : public Il2CppObject
{
public:
	// System.Collections.Hashtable System.Web.HttpStaticObjectsCollection::_Objects
	Hashtable_t1853889766 * ____Objects_0;

public:
	inline static int32_t get_offset_of__Objects_0() { return static_cast<int32_t>(offsetof(HttpStaticObjectsCollection_t518175362, ____Objects_0)); }
	inline Hashtable_t1853889766 * get__Objects_0() const { return ____Objects_0; }
	inline Hashtable_t1853889766 ** get_address_of__Objects_0() { return &____Objects_0; }
	inline void set__Objects_0(Hashtable_t1853889766 * value)
	{
		____Objects_0 = value;
		Il2CppCodeGenWriteBarrier(&____Objects_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
