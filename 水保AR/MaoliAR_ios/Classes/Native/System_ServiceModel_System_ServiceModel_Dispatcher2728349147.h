﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Dispatcher.BaseRequestProcessorHandler
struct BaseRequestProcessorHandler_t2537743531;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.HandlersChain
struct  HandlersChain_t2728349147  : public Il2CppObject
{
public:
	// System.ServiceModel.Dispatcher.BaseRequestProcessorHandler System.ServiceModel.Dispatcher.HandlersChain::chain
	BaseRequestProcessorHandler_t2537743531 * ___chain_0;

public:
	inline static int32_t get_offset_of_chain_0() { return static_cast<int32_t>(offsetof(HandlersChain_t2728349147, ___chain_0)); }
	inline BaseRequestProcessorHandler_t2537743531 * get_chain_0() const { return ___chain_0; }
	inline BaseRequestProcessorHandler_t2537743531 ** get_address_of_chain_0() { return &___chain_0; }
	inline void set_chain_0(BaseRequestProcessorHandler_t2537743531 * value)
	{
		___chain_0 = value;
		Il2CppCodeGenWriteBarrier(&___chain_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
