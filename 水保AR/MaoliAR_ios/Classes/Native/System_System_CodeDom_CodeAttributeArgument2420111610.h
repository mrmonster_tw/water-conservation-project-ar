﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.CodeDom.CodeExpression
struct CodeExpression_t2166265795;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeAttributeArgument
struct  CodeAttributeArgument_t2420111610  : public Il2CppObject
{
public:
	// System.String System.CodeDom.CodeAttributeArgument::name
	String_t* ___name_0;
	// System.CodeDom.CodeExpression System.CodeDom.CodeAttributeArgument::value
	CodeExpression_t2166265795 * ___value_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(CodeAttributeArgument_t2420111610, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(CodeAttributeArgument_t2420111610, ___value_1)); }
	inline CodeExpression_t2166265795 * get_value_1() const { return ___value_1; }
	inline CodeExpression_t2166265795 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(CodeExpression_t2166265795 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
