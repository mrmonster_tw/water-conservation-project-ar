﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeObject3927604602.h"
#include "System_System_CodeDom_MemberAttributes1258384723.h"

// System.String
struct String_t;
// System.CodeDom.CodeCommentStatementCollection
struct CodeCommentStatementCollection_t84489496;
// System.CodeDom.CodeAttributeDeclarationCollection
struct CodeAttributeDeclarationCollection_t3890917538;
// System.CodeDom.CodeLinePragma
struct CodeLinePragma_t3085002198;
// System.CodeDom.CodeDirectiveCollection
struct CodeDirectiveCollection_t1153190035;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeTypeMember
struct  CodeTypeMember_t1555525554  : public CodeObject_t3927604602
{
public:
	// System.String System.CodeDom.CodeTypeMember::name
	String_t* ___name_1;
	// System.CodeDom.MemberAttributes System.CodeDom.CodeTypeMember::attributes
	int32_t ___attributes_2;
	// System.CodeDom.CodeCommentStatementCollection System.CodeDom.CodeTypeMember::comments
	CodeCommentStatementCollection_t84489496 * ___comments_3;
	// System.CodeDom.CodeAttributeDeclarationCollection System.CodeDom.CodeTypeMember::customAttributes
	CodeAttributeDeclarationCollection_t3890917538 * ___customAttributes_4;
	// System.CodeDom.CodeLinePragma System.CodeDom.CodeTypeMember::linePragma
	CodeLinePragma_t3085002198 * ___linePragma_5;
	// System.CodeDom.CodeDirectiveCollection System.CodeDom.CodeTypeMember::endDirectives
	CodeDirectiveCollection_t1153190035 * ___endDirectives_6;
	// System.CodeDom.CodeDirectiveCollection System.CodeDom.CodeTypeMember::startDirectives
	CodeDirectiveCollection_t1153190035 * ___startDirectives_7;

public:
	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(CodeTypeMember_t1555525554, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_attributes_2() { return static_cast<int32_t>(offsetof(CodeTypeMember_t1555525554, ___attributes_2)); }
	inline int32_t get_attributes_2() const { return ___attributes_2; }
	inline int32_t* get_address_of_attributes_2() { return &___attributes_2; }
	inline void set_attributes_2(int32_t value)
	{
		___attributes_2 = value;
	}

	inline static int32_t get_offset_of_comments_3() { return static_cast<int32_t>(offsetof(CodeTypeMember_t1555525554, ___comments_3)); }
	inline CodeCommentStatementCollection_t84489496 * get_comments_3() const { return ___comments_3; }
	inline CodeCommentStatementCollection_t84489496 ** get_address_of_comments_3() { return &___comments_3; }
	inline void set_comments_3(CodeCommentStatementCollection_t84489496 * value)
	{
		___comments_3 = value;
		Il2CppCodeGenWriteBarrier(&___comments_3, value);
	}

	inline static int32_t get_offset_of_customAttributes_4() { return static_cast<int32_t>(offsetof(CodeTypeMember_t1555525554, ___customAttributes_4)); }
	inline CodeAttributeDeclarationCollection_t3890917538 * get_customAttributes_4() const { return ___customAttributes_4; }
	inline CodeAttributeDeclarationCollection_t3890917538 ** get_address_of_customAttributes_4() { return &___customAttributes_4; }
	inline void set_customAttributes_4(CodeAttributeDeclarationCollection_t3890917538 * value)
	{
		___customAttributes_4 = value;
		Il2CppCodeGenWriteBarrier(&___customAttributes_4, value);
	}

	inline static int32_t get_offset_of_linePragma_5() { return static_cast<int32_t>(offsetof(CodeTypeMember_t1555525554, ___linePragma_5)); }
	inline CodeLinePragma_t3085002198 * get_linePragma_5() const { return ___linePragma_5; }
	inline CodeLinePragma_t3085002198 ** get_address_of_linePragma_5() { return &___linePragma_5; }
	inline void set_linePragma_5(CodeLinePragma_t3085002198 * value)
	{
		___linePragma_5 = value;
		Il2CppCodeGenWriteBarrier(&___linePragma_5, value);
	}

	inline static int32_t get_offset_of_endDirectives_6() { return static_cast<int32_t>(offsetof(CodeTypeMember_t1555525554, ___endDirectives_6)); }
	inline CodeDirectiveCollection_t1153190035 * get_endDirectives_6() const { return ___endDirectives_6; }
	inline CodeDirectiveCollection_t1153190035 ** get_address_of_endDirectives_6() { return &___endDirectives_6; }
	inline void set_endDirectives_6(CodeDirectiveCollection_t1153190035 * value)
	{
		___endDirectives_6 = value;
		Il2CppCodeGenWriteBarrier(&___endDirectives_6, value);
	}

	inline static int32_t get_offset_of_startDirectives_7() { return static_cast<int32_t>(offsetof(CodeTypeMember_t1555525554, ___startDirectives_7)); }
	inline CodeDirectiveCollection_t1153190035 * get_startDirectives_7() const { return ___startDirectives_7; }
	inline CodeDirectiveCollection_t1153190035 ** get_address_of_startDirectives_7() { return &___startDirectives_7; }
	inline void set_startDirectives_7(CodeDirectiveCollection_t1153190035 * value)
	{
		___startDirectives_7 = value;
		Il2CppCodeGenWriteBarrier(&___startDirectives_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
