﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Generic.Dictionary`2<System.Int32,System.Xml.XmlDictionaryString>
struct Dictionary_2_t2392833597;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlBinaryWriterSession
struct  XmlBinaryWriterSession_t1730638232  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Xml.XmlDictionaryString> System.Xml.XmlBinaryWriterSession::dic
	Dictionary_2_t2392833597 * ___dic_0;

public:
	inline static int32_t get_offset_of_dic_0() { return static_cast<int32_t>(offsetof(XmlBinaryWriterSession_t1730638232, ___dic_0)); }
	inline Dictionary_2_t2392833597 * get_dic_0() const { return ___dic_0; }
	inline Dictionary_2_t2392833597 ** get_address_of_dic_0() { return &___dic_0; }
	inline void set_dic_0(Dictionary_2_t2392833597 * value)
	{
		___dic_0 = value;
		Il2CppCodeGenWriteBarrier(&___dic_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
