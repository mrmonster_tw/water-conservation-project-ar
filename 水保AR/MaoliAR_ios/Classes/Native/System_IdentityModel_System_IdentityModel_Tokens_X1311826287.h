﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Tokens_S1271873540.h"

// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t714049126;
// System.String
struct String_t;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IdentityModel.Tokens.SecurityKey>
struct ReadOnlyCollection_1_t1046284793;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Tokens.X509SecurityToken
struct  X509SecurityToken_t1311826287  : public SecurityToken_t1271873540
{
public:
	// System.Security.Cryptography.X509Certificates.X509Certificate2 System.IdentityModel.Tokens.X509SecurityToken::cert
	X509Certificate2_t714049126 * ___cert_0;
	// System.String System.IdentityModel.Tokens.X509SecurityToken::id
	String_t* ___id_1;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IdentityModel.Tokens.SecurityKey> System.IdentityModel.Tokens.X509SecurityToken::keys
	ReadOnlyCollection_1_t1046284793 * ___keys_2;

public:
	inline static int32_t get_offset_of_cert_0() { return static_cast<int32_t>(offsetof(X509SecurityToken_t1311826287, ___cert_0)); }
	inline X509Certificate2_t714049126 * get_cert_0() const { return ___cert_0; }
	inline X509Certificate2_t714049126 ** get_address_of_cert_0() { return &___cert_0; }
	inline void set_cert_0(X509Certificate2_t714049126 * value)
	{
		___cert_0 = value;
		Il2CppCodeGenWriteBarrier(&___cert_0, value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(X509SecurityToken_t1311826287, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier(&___id_1, value);
	}

	inline static int32_t get_offset_of_keys_2() { return static_cast<int32_t>(offsetof(X509SecurityToken_t1311826287, ___keys_2)); }
	inline ReadOnlyCollection_1_t1046284793 * get_keys_2() const { return ___keys_2; }
	inline ReadOnlyCollection_1_t1046284793 ** get_address_of_keys_2() { return &___keys_2; }
	inline void set_keys_2(ReadOnlyCollection_1_t1046284793 * value)
	{
		___keys_2 = value;
		Il2CppCodeGenWriteBarrier(&___keys_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
