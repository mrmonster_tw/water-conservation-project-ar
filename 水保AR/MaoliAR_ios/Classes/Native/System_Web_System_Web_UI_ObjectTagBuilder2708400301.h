﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_ControlBuilder2523018631.h"

// System.String
struct String_t;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.ObjectTagBuilder
struct  ObjectTagBuilder_t2708400301  : public ControlBuilder_t2523018631
{
public:
	// System.String System.Web.UI.ObjectTagBuilder::id
	String_t* ___id_28;
	// System.String System.Web.UI.ObjectTagBuilder::scope
	String_t* ___scope_29;
	// System.Type System.Web.UI.ObjectTagBuilder::type
	Type_t * ___type_30;

public:
	inline static int32_t get_offset_of_id_28() { return static_cast<int32_t>(offsetof(ObjectTagBuilder_t2708400301, ___id_28)); }
	inline String_t* get_id_28() const { return ___id_28; }
	inline String_t** get_address_of_id_28() { return &___id_28; }
	inline void set_id_28(String_t* value)
	{
		___id_28 = value;
		Il2CppCodeGenWriteBarrier(&___id_28, value);
	}

	inline static int32_t get_offset_of_scope_29() { return static_cast<int32_t>(offsetof(ObjectTagBuilder_t2708400301, ___scope_29)); }
	inline String_t* get_scope_29() const { return ___scope_29; }
	inline String_t** get_address_of_scope_29() { return &___scope_29; }
	inline void set_scope_29(String_t* value)
	{
		___scope_29 = value;
		Il2CppCodeGenWriteBarrier(&___scope_29, value);
	}

	inline static int32_t get_offset_of_type_30() { return static_cast<int32_t>(offsetof(ObjectTagBuilder_t2708400301, ___type_30)); }
	inline Type_t * get_type_30() const { return ___type_30; }
	inline Type_t ** get_address_of_type_30() { return &___type_30; }
	inline void set_type_30(Type_t * value)
	{
		___type_30 = value;
		Il2CppCodeGenWriteBarrier(&___type_30, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
