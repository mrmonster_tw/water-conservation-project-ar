﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Drawing_System_Drawing_Imaging_PixelFormat200935677.h"
#include "mscorlib_System_IntPtr840150181.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Drawing.Imaging.BitmapData
struct  BitmapData_t288533671  : public Il2CppObject
{
public:
	// System.Int32 System.Drawing.Imaging.BitmapData::width
	int32_t ___width_0;
	// System.Int32 System.Drawing.Imaging.BitmapData::height
	int32_t ___height_1;
	// System.Int32 System.Drawing.Imaging.BitmapData::stride
	int32_t ___stride_2;
	// System.Drawing.Imaging.PixelFormat System.Drawing.Imaging.BitmapData::pixel_format
	int32_t ___pixel_format_3;
	// System.IntPtr System.Drawing.Imaging.BitmapData::scan0
	IntPtr_t ___scan0_4;
	// System.Int32 System.Drawing.Imaging.BitmapData::reserved
	int32_t ___reserved_5;
	// System.IntPtr System.Drawing.Imaging.BitmapData::palette
	IntPtr_t ___palette_6;
	// System.Int32 System.Drawing.Imaging.BitmapData::property_count
	int32_t ___property_count_7;
	// System.IntPtr System.Drawing.Imaging.BitmapData::property
	IntPtr_t ___property_8;
	// System.Single System.Drawing.Imaging.BitmapData::dpi_horz
	float ___dpi_horz_9;
	// System.Single System.Drawing.Imaging.BitmapData::dpi_vert
	float ___dpi_vert_10;
	// System.Int32 System.Drawing.Imaging.BitmapData::image_flags
	int32_t ___image_flags_11;
	// System.Int32 System.Drawing.Imaging.BitmapData::left
	int32_t ___left_12;
	// System.Int32 System.Drawing.Imaging.BitmapData::top
	int32_t ___top_13;
	// System.Int32 System.Drawing.Imaging.BitmapData::x
	int32_t ___x_14;
	// System.Int32 System.Drawing.Imaging.BitmapData::y
	int32_t ___y_15;
	// System.Int32 System.Drawing.Imaging.BitmapData::transparent
	int32_t ___transparent_16;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(BitmapData_t288533671, ___width_0)); }
	inline int32_t get_width_0() const { return ___width_0; }
	inline int32_t* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(int32_t value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(BitmapData_t288533671, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}

	inline static int32_t get_offset_of_stride_2() { return static_cast<int32_t>(offsetof(BitmapData_t288533671, ___stride_2)); }
	inline int32_t get_stride_2() const { return ___stride_2; }
	inline int32_t* get_address_of_stride_2() { return &___stride_2; }
	inline void set_stride_2(int32_t value)
	{
		___stride_2 = value;
	}

	inline static int32_t get_offset_of_pixel_format_3() { return static_cast<int32_t>(offsetof(BitmapData_t288533671, ___pixel_format_3)); }
	inline int32_t get_pixel_format_3() const { return ___pixel_format_3; }
	inline int32_t* get_address_of_pixel_format_3() { return &___pixel_format_3; }
	inline void set_pixel_format_3(int32_t value)
	{
		___pixel_format_3 = value;
	}

	inline static int32_t get_offset_of_scan0_4() { return static_cast<int32_t>(offsetof(BitmapData_t288533671, ___scan0_4)); }
	inline IntPtr_t get_scan0_4() const { return ___scan0_4; }
	inline IntPtr_t* get_address_of_scan0_4() { return &___scan0_4; }
	inline void set_scan0_4(IntPtr_t value)
	{
		___scan0_4 = value;
	}

	inline static int32_t get_offset_of_reserved_5() { return static_cast<int32_t>(offsetof(BitmapData_t288533671, ___reserved_5)); }
	inline int32_t get_reserved_5() const { return ___reserved_5; }
	inline int32_t* get_address_of_reserved_5() { return &___reserved_5; }
	inline void set_reserved_5(int32_t value)
	{
		___reserved_5 = value;
	}

	inline static int32_t get_offset_of_palette_6() { return static_cast<int32_t>(offsetof(BitmapData_t288533671, ___palette_6)); }
	inline IntPtr_t get_palette_6() const { return ___palette_6; }
	inline IntPtr_t* get_address_of_palette_6() { return &___palette_6; }
	inline void set_palette_6(IntPtr_t value)
	{
		___palette_6 = value;
	}

	inline static int32_t get_offset_of_property_count_7() { return static_cast<int32_t>(offsetof(BitmapData_t288533671, ___property_count_7)); }
	inline int32_t get_property_count_7() const { return ___property_count_7; }
	inline int32_t* get_address_of_property_count_7() { return &___property_count_7; }
	inline void set_property_count_7(int32_t value)
	{
		___property_count_7 = value;
	}

	inline static int32_t get_offset_of_property_8() { return static_cast<int32_t>(offsetof(BitmapData_t288533671, ___property_8)); }
	inline IntPtr_t get_property_8() const { return ___property_8; }
	inline IntPtr_t* get_address_of_property_8() { return &___property_8; }
	inline void set_property_8(IntPtr_t value)
	{
		___property_8 = value;
	}

	inline static int32_t get_offset_of_dpi_horz_9() { return static_cast<int32_t>(offsetof(BitmapData_t288533671, ___dpi_horz_9)); }
	inline float get_dpi_horz_9() const { return ___dpi_horz_9; }
	inline float* get_address_of_dpi_horz_9() { return &___dpi_horz_9; }
	inline void set_dpi_horz_9(float value)
	{
		___dpi_horz_9 = value;
	}

	inline static int32_t get_offset_of_dpi_vert_10() { return static_cast<int32_t>(offsetof(BitmapData_t288533671, ___dpi_vert_10)); }
	inline float get_dpi_vert_10() const { return ___dpi_vert_10; }
	inline float* get_address_of_dpi_vert_10() { return &___dpi_vert_10; }
	inline void set_dpi_vert_10(float value)
	{
		___dpi_vert_10 = value;
	}

	inline static int32_t get_offset_of_image_flags_11() { return static_cast<int32_t>(offsetof(BitmapData_t288533671, ___image_flags_11)); }
	inline int32_t get_image_flags_11() const { return ___image_flags_11; }
	inline int32_t* get_address_of_image_flags_11() { return &___image_flags_11; }
	inline void set_image_flags_11(int32_t value)
	{
		___image_flags_11 = value;
	}

	inline static int32_t get_offset_of_left_12() { return static_cast<int32_t>(offsetof(BitmapData_t288533671, ___left_12)); }
	inline int32_t get_left_12() const { return ___left_12; }
	inline int32_t* get_address_of_left_12() { return &___left_12; }
	inline void set_left_12(int32_t value)
	{
		___left_12 = value;
	}

	inline static int32_t get_offset_of_top_13() { return static_cast<int32_t>(offsetof(BitmapData_t288533671, ___top_13)); }
	inline int32_t get_top_13() const { return ___top_13; }
	inline int32_t* get_address_of_top_13() { return &___top_13; }
	inline void set_top_13(int32_t value)
	{
		___top_13 = value;
	}

	inline static int32_t get_offset_of_x_14() { return static_cast<int32_t>(offsetof(BitmapData_t288533671, ___x_14)); }
	inline int32_t get_x_14() const { return ___x_14; }
	inline int32_t* get_address_of_x_14() { return &___x_14; }
	inline void set_x_14(int32_t value)
	{
		___x_14 = value;
	}

	inline static int32_t get_offset_of_y_15() { return static_cast<int32_t>(offsetof(BitmapData_t288533671, ___y_15)); }
	inline int32_t get_y_15() const { return ___y_15; }
	inline int32_t* get_address_of_y_15() { return &___y_15; }
	inline void set_y_15(int32_t value)
	{
		___y_15 = value;
	}

	inline static int32_t get_offset_of_transparent_16() { return static_cast<int32_t>(offsetof(BitmapData_t288533671, ___transparent_16)); }
	inline int32_t get_transparent_16() const { return ___transparent_16; }
	inline int32_t* get_address_of_transparent_16() { return &___transparent_16; }
	inline void set_transparent_16(int32_t value)
	{
		___transparent_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Drawing.Imaging.BitmapData
struct BitmapData_t288533671_marshaled_pinvoke
{
	int32_t ___width_0;
	int32_t ___height_1;
	int32_t ___stride_2;
	int32_t ___pixel_format_3;
	intptr_t ___scan0_4;
	int32_t ___reserved_5;
	intptr_t ___palette_6;
	int32_t ___property_count_7;
	intptr_t ___property_8;
	float ___dpi_horz_9;
	float ___dpi_vert_10;
	int32_t ___image_flags_11;
	int32_t ___left_12;
	int32_t ___top_13;
	int32_t ___x_14;
	int32_t ___y_15;
	int32_t ___transparent_16;
};
// Native definition for COM marshalling of System.Drawing.Imaging.BitmapData
struct BitmapData_t288533671_marshaled_com
{
	int32_t ___width_0;
	int32_t ___height_1;
	int32_t ___stride_2;
	int32_t ___pixel_format_3;
	intptr_t ___scan0_4;
	int32_t ___reserved_5;
	intptr_t ___palette_6;
	int32_t ___property_count_7;
	intptr_t ___property_8;
	float ___dpi_horz_9;
	float ___dpi_vert_10;
	int32_t ___image_flags_11;
	int32_t ___left_12;
	int32_t ___top_13;
	int32_t ___x_14;
	int32_t ___y_15;
	int32_t ___transparent_16;
};
