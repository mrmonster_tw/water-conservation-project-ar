﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Configurati551052268.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.NetMsmqBindingElement
struct  NetMsmqBindingElement_t4085042952  : public MsmqBindingElementBase_t551052268
{
public:

public:
};

struct NetMsmqBindingElement_t4085042952_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.NetMsmqBindingElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_27;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.NetMsmqBindingElement::binding_element_type
	ConfigurationProperty_t3590861854 * ___binding_element_type_28;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.NetMsmqBindingElement::max_buffer_pool_size
	ConfigurationProperty_t3590861854 * ___max_buffer_pool_size_29;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.NetMsmqBindingElement::queue_transfer_protocol
	ConfigurationProperty_t3590861854 * ___queue_transfer_protocol_30;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.NetMsmqBindingElement::reader_quotas
	ConfigurationProperty_t3590861854 * ___reader_quotas_31;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.NetMsmqBindingElement::security
	ConfigurationProperty_t3590861854 * ___security_32;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.NetMsmqBindingElement::use_active_directory
	ConfigurationProperty_t3590861854 * ___use_active_directory_33;

public:
	inline static int32_t get_offset_of_properties_27() { return static_cast<int32_t>(offsetof(NetMsmqBindingElement_t4085042952_StaticFields, ___properties_27)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_27() const { return ___properties_27; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_27() { return &___properties_27; }
	inline void set_properties_27(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_27 = value;
		Il2CppCodeGenWriteBarrier(&___properties_27, value);
	}

	inline static int32_t get_offset_of_binding_element_type_28() { return static_cast<int32_t>(offsetof(NetMsmqBindingElement_t4085042952_StaticFields, ___binding_element_type_28)); }
	inline ConfigurationProperty_t3590861854 * get_binding_element_type_28() const { return ___binding_element_type_28; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_binding_element_type_28() { return &___binding_element_type_28; }
	inline void set_binding_element_type_28(ConfigurationProperty_t3590861854 * value)
	{
		___binding_element_type_28 = value;
		Il2CppCodeGenWriteBarrier(&___binding_element_type_28, value);
	}

	inline static int32_t get_offset_of_max_buffer_pool_size_29() { return static_cast<int32_t>(offsetof(NetMsmqBindingElement_t4085042952_StaticFields, ___max_buffer_pool_size_29)); }
	inline ConfigurationProperty_t3590861854 * get_max_buffer_pool_size_29() const { return ___max_buffer_pool_size_29; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_max_buffer_pool_size_29() { return &___max_buffer_pool_size_29; }
	inline void set_max_buffer_pool_size_29(ConfigurationProperty_t3590861854 * value)
	{
		___max_buffer_pool_size_29 = value;
		Il2CppCodeGenWriteBarrier(&___max_buffer_pool_size_29, value);
	}

	inline static int32_t get_offset_of_queue_transfer_protocol_30() { return static_cast<int32_t>(offsetof(NetMsmqBindingElement_t4085042952_StaticFields, ___queue_transfer_protocol_30)); }
	inline ConfigurationProperty_t3590861854 * get_queue_transfer_protocol_30() const { return ___queue_transfer_protocol_30; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_queue_transfer_protocol_30() { return &___queue_transfer_protocol_30; }
	inline void set_queue_transfer_protocol_30(ConfigurationProperty_t3590861854 * value)
	{
		___queue_transfer_protocol_30 = value;
		Il2CppCodeGenWriteBarrier(&___queue_transfer_protocol_30, value);
	}

	inline static int32_t get_offset_of_reader_quotas_31() { return static_cast<int32_t>(offsetof(NetMsmqBindingElement_t4085042952_StaticFields, ___reader_quotas_31)); }
	inline ConfigurationProperty_t3590861854 * get_reader_quotas_31() const { return ___reader_quotas_31; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_reader_quotas_31() { return &___reader_quotas_31; }
	inline void set_reader_quotas_31(ConfigurationProperty_t3590861854 * value)
	{
		___reader_quotas_31 = value;
		Il2CppCodeGenWriteBarrier(&___reader_quotas_31, value);
	}

	inline static int32_t get_offset_of_security_32() { return static_cast<int32_t>(offsetof(NetMsmqBindingElement_t4085042952_StaticFields, ___security_32)); }
	inline ConfigurationProperty_t3590861854 * get_security_32() const { return ___security_32; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_security_32() { return &___security_32; }
	inline void set_security_32(ConfigurationProperty_t3590861854 * value)
	{
		___security_32 = value;
		Il2CppCodeGenWriteBarrier(&___security_32, value);
	}

	inline static int32_t get_offset_of_use_active_directory_33() { return static_cast<int32_t>(offsetof(NetMsmqBindingElement_t4085042952_StaticFields, ___use_active_directory_33)); }
	inline ConfigurationProperty_t3590861854 * get_use_active_directory_33() const { return ___use_active_directory_33; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_use_active_directory_33() { return &___use_active_directory_33; }
	inline void set_use_active_directory_33(ConfigurationProperty_t3590861854 * value)
	{
		___use_active_directory_33 = value;
		Il2CppCodeGenWriteBarrier(&___use_active_directory_33, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
