﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlTokenizedType3348692225.h"
#include "System_Xml_System_Xml_XmlUrlResolver817895037.h"
#include "System_Xml_System_Xml_XmlValidatingReader1719295192.h"
#include "System_Xml_System_Xml_XmlWhitespace131741354.h"
#include "System_Xml_System_Xml_XmlWriter127905479.h"
#include "System_Xml_System_Xml_XmlWriterSettings3314986516.h"
#include "System_Xml_System_Xml_XmlTextWriter2114213153.h"
#include "System_Xml_System_Xml_XmlTextWriter_XmlNodeInfo4030693883.h"
#include "System_Xml_System_Xml_XmlTextWriter_StringUtil564231417.h"
#include "System_Xml_System_Xml_XmlTextWriter_XmlDeclState1683299469.h"
#include "System_Xml_System_Xml_XmlStreamReader727818754.h"
#include "System_Xml_System_Xml_NonBlockingStreamReader2495303928.h"
#include "System_Xml_System_Xml_XmlInputStream1691369434.h"
#include "System_Xml_System_Xml_XmlParserInput2182411204.h"
#include "System_Xml_System_Xml_XmlParserInput_XmlParserInpu3533005609.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator787956054.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator_Enumera1590428576.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator_U3CEnume404642154.h"
#include "System_Xml_System_Xml_XPath_XPathExpression1723793351.h"
#include "System_Xml_System_Xml_XPath_XPathItem4250588140.h"
#include "System_Xml_System_Xml_XPath_XPathNamespaceScope4128811329.h"
#include "System_Xml_System_Xml_XPath_XPathNodeIterator3667290188.h"
#include "System_Xml_System_Xml_XPath_XPathNodeIterator_U3CGet90454087.h"
#include "System_Xml_System_Xml_XPath_XPathResultType2828988488.h"
#include "System_Xml_System_Xml_XPath_XPathNodeType3031007223.h"
#include "System_Xml_System_Xml_XPath_XmlDataType3437356259.h"
#include "System_Xml_System_Xml_XPath_XmlSortOrder4114893492.h"
#include "System_Xml_System_Xml_XPath_XmlCaseOrder176547839.h"
#include "System_Xml_System_Xml_XPath_XPathDocument1673143697.h"
#include "System_Xml_System_Xml_XPath_XPathException2107611652.h"
#include "System_Xml_System_Xml_XPath_XPathIteratorComparer1799213572.h"
#include "System_Xml_System_Xml_XPath_XPathNavigatorComparer2421573191.h"
#include "System_Xml_System_Xml_XPath_XPathFunctions1481462947.h"
#include "System_Xml_System_Xml_XPath_XPathFunction857746608.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionLast241688620.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionPosition1996792158.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionCount4174407007.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionId1670301333.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionLocalName1807446049.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionNamespace4077891083.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionName1405602168.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionString3820712894.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionConcat3351639595.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionStartsWit3734288208.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionContains678583606.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionSubstring3299707364.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionSubstring2254953802.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionSubstring3677332116.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionStringLen1686187666.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionNormalizeS379159227.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionTranslate1071508611.h"
#include "System_Xml_System_Xml_XPath_XPathBooleanFunction57991257.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionBoolean2892373420.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionNot3025979743.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionTrue3806785954.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionFalse1021066162.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionLang242671679.h"
#include "System_Xml_System_Xml_XPath_XPathNumericFunction307051317.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionNumber699684043.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionSum2229418177.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionFloor3617095014.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionCeil4176839285.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionRound2668931994.h"
#include "System_Xml_System_Xml_XPath_CompiledExpression4018285681.h"
#include "System_Xml_System_Xml_XPath_XPathSortElement4237073177.h"
#include "System_Xml_System_Xml_XPath_XPathSorters698127628.h"
#include "System_Xml_System_Xml_XPath_XPathSorter36376808.h"
#include "System_Xml_System_Xml_XPath_XPathSorter_XPathNumberCo6863540.h"
#include "System_Xml_System_Xml_XPath_XPathSorter_XPathTextC2075334572.h"
#include "System_Xml_System_Xml_XPath_Expression1452783009.h"
#include "System_Xml_System_Xml_XPath_ExprBinary2069694888.h"
#include "System_Xml_System_Xml_XPath_ExprBoolean3855188593.h"
#include "System_Xml_System_Xml_XPath_ExprOR3019581832.h"
#include "System_Xml_System_Xml_XPath_ExprAND348538276.h"
#include "System_Xml_System_Xml_XPath_EqualityExpr1646148531.h"
#include "System_Xml_System_Xml_XPath_ExprEQ1453891107.h"
#include "System_Xml_System_Xml_XPath_ExprNE4135261543.h"
#include "System_Xml_System_Xml_XPath_RelationalExpr3307137467.h"
#include "System_Xml_System_Xml_XPath_ExprGT1857306706.h"
#include "System_Xml_System_Xml_XPath_ExprGE4135851367.h"
#include "System_Xml_System_Xml_XPath_ExprLT1856585810.h"
#include "System_Xml_System_Xml_XPath_ExprLE4135130471.h"
#include "System_Xml_System_Xml_XPath_ExprNumeric370757872.h"
#include "System_Xml_System_Xml_XPath_ExprPLUS1725353634.h"
#include "System_Xml_System_Xml_XPath_ExprMINUS3125292145.h"
#include "System_Xml_System_Xml_XPath_ExprMULT118778748.h"
#include "System_Xml_System_Xml_XPath_ExprDIV1961659563.h"
#include "System_Xml_System_Xml_XPath_ExprMOD1913835785.h"
#include "System_Xml_System_Xml_XPath_ExprNEG381818215.h"
#include "System_Xml_System_Xml_XPath_NodeSet3272593155.h"
#include "System_Xml_System_Xml_XPath_ExprUNION1609753984.h"
#include "System_Xml_System_Xml_XPath_ExprSLASH1263914658.h"
#include "System_Xml_System_Xml_XPath_ExprSLASH21762680492.h"
#include "System_Xml_System_Xml_XPath_ExprRoot3091324302.h"
#include "System_Xml_System_Xml_XPath_Axes1882171014.h"
#include "System_Xml_System_Xml_XPath_AxisSpecifier40435393.h"
#include "System_Xml_System_Xml_XPath_NodeTest747859056.h"
#include "System_Xml_System_Xml_XPath_NodeTypeTest4287533341.h"
#include "System_Xml_System_Xml_XPath_NodeNameTest562814213.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (XmlTokenizedType_t3348692225)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1700[14] = 
{
	XmlTokenizedType_t3348692225::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (XmlUrlResolver_t817895037), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1701[1] = 
{
	XmlUrlResolver_t817895037::get_offset_of_credential_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (XmlValidatingReader_t1719295192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1702[12] = 
{
	XmlValidatingReader_t1719295192::get_offset_of_entityHandling_3(),
	XmlValidatingReader_t1719295192::get_offset_of_sourceReader_4(),
	XmlValidatingReader_t1719295192::get_offset_of_xmlTextReader_5(),
	XmlValidatingReader_t1719295192::get_offset_of_validatingReader_6(),
	XmlValidatingReader_t1719295192::get_offset_of_resolver_7(),
	XmlValidatingReader_t1719295192::get_offset_of_resolverSpecified_8(),
	XmlValidatingReader_t1719295192::get_offset_of_validationType_9(),
	XmlValidatingReader_t1719295192::get_offset_of_schemas_10(),
	XmlValidatingReader_t1719295192::get_offset_of_dtdReader_11(),
	XmlValidatingReader_t1719295192::get_offset_of_schemaInfo_12(),
	XmlValidatingReader_t1719295192::get_offset_of_storedCharacters_13(),
	XmlValidatingReader_t1719295192::get_offset_of_ValidationEventHandler_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (XmlWhitespace_t131741354), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (XmlWriter_t127905479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1704[1] = 
{
	XmlWriter_t127905479::get_offset_of_settings_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (XmlWriterSettings_t3314986516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1705[12] = 
{
	XmlWriterSettings_t3314986516::get_offset_of_checkCharacters_0(),
	XmlWriterSettings_t3314986516::get_offset_of_closeOutput_1(),
	XmlWriterSettings_t3314986516::get_offset_of_conformance_2(),
	XmlWriterSettings_t3314986516::get_offset_of_encoding_3(),
	XmlWriterSettings_t3314986516::get_offset_of_indent_4(),
	XmlWriterSettings_t3314986516::get_offset_of_indentChars_5(),
	XmlWriterSettings_t3314986516::get_offset_of_newLineChars_6(),
	XmlWriterSettings_t3314986516::get_offset_of_newLineOnAttributes_7(),
	XmlWriterSettings_t3314986516::get_offset_of_newLineHandling_8(),
	XmlWriterSettings_t3314986516::get_offset_of_omitXmlDeclaration_9(),
	XmlWriterSettings_t3314986516::get_offset_of_outputMethod_10(),
	XmlWriterSettings_t3314986516::get_offset_of_U3CNamespaceHandlingU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (XmlTextWriter_t2114213153), -1, sizeof(XmlTextWriter_t2114213153_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1706[35] = 
{
	XmlTextWriter_t2114213153_StaticFields::get_offset_of_unmarked_utf8encoding_1(),
	XmlTextWriter_t2114213153_StaticFields::get_offset_of_escaped_text_chars_2(),
	XmlTextWriter_t2114213153_StaticFields::get_offset_of_escaped_attr_chars_3(),
	XmlTextWriter_t2114213153::get_offset_of_base_stream_4(),
	XmlTextWriter_t2114213153::get_offset_of_source_5(),
	XmlTextWriter_t2114213153::get_offset_of_writer_6(),
	XmlTextWriter_t2114213153::get_offset_of_preserver_7(),
	XmlTextWriter_t2114213153::get_offset_of_preserved_name_8(),
	XmlTextWriter_t2114213153::get_offset_of_is_preserved_xmlns_9(),
	XmlTextWriter_t2114213153::get_offset_of_allow_doc_fragment_10(),
	XmlTextWriter_t2114213153::get_offset_of_close_output_stream_11(),
	XmlTextWriter_t2114213153::get_offset_of_ignore_encoding_12(),
	XmlTextWriter_t2114213153::get_offset_of_namespaces_13(),
	XmlTextWriter_t2114213153::get_offset_of_xmldecl_state_14(),
	XmlTextWriter_t2114213153::get_offset_of_check_character_validity_15(),
	XmlTextWriter_t2114213153::get_offset_of_newline_handling_16(),
	XmlTextWriter_t2114213153::get_offset_of_is_document_entity_17(),
	XmlTextWriter_t2114213153::get_offset_of_state_18(),
	XmlTextWriter_t2114213153::get_offset_of_node_state_19(),
	XmlTextWriter_t2114213153::get_offset_of_nsmanager_20(),
	XmlTextWriter_t2114213153::get_offset_of_open_count_21(),
	XmlTextWriter_t2114213153::get_offset_of_elements_22(),
	XmlTextWriter_t2114213153::get_offset_of_new_local_namespaces_23(),
	XmlTextWriter_t2114213153::get_offset_of_explicit_nsdecls_24(),
	XmlTextWriter_t2114213153::get_offset_of_namespace_handling_25(),
	XmlTextWriter_t2114213153::get_offset_of_indent_26(),
	XmlTextWriter_t2114213153::get_offset_of_indent_count_27(),
	XmlTextWriter_t2114213153::get_offset_of_indent_char_28(),
	XmlTextWriter_t2114213153::get_offset_of_indent_string_29(),
	XmlTextWriter_t2114213153::get_offset_of_newline_30(),
	XmlTextWriter_t2114213153::get_offset_of_indent_attributes_31(),
	XmlTextWriter_t2114213153::get_offset_of_quote_char_32(),
	XmlTextWriter_t2114213153::get_offset_of_v2_33(),
	XmlTextWriter_t2114213153_StaticFields::get_offset_of_U3CU3Ef__switchU24map3A_34(),
	XmlTextWriter_t2114213153_StaticFields::get_offset_of_U3CU3Ef__switchU24map3B_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (XmlNodeInfo_t4030693883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1707[7] = 
{
	XmlNodeInfo_t4030693883::get_offset_of_Prefix_0(),
	XmlNodeInfo_t4030693883::get_offset_of_LocalName_1(),
	XmlNodeInfo_t4030693883::get_offset_of_NS_2(),
	XmlNodeInfo_t4030693883::get_offset_of_HasSimple_3(),
	XmlNodeInfo_t4030693883::get_offset_of_HasElements_4(),
	XmlNodeInfo_t4030693883::get_offset_of_XmlLang_5(),
	XmlNodeInfo_t4030693883::get_offset_of_XmlSpace_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (StringUtil_t564231417), -1, sizeof(StringUtil_t564231417_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1708[2] = 
{
	StringUtil_t564231417_StaticFields::get_offset_of_cul_0(),
	StringUtil_t564231417_StaticFields::get_offset_of_cmp_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (XmlDeclState_t1683299469)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1709[5] = 
{
	XmlDeclState_t1683299469::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (XmlStreamReader_t727818754), -1, sizeof(XmlStreamReader_t727818754_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1710[2] = 
{
	XmlStreamReader_t727818754::get_offset_of_input_13(),
	XmlStreamReader_t727818754_StaticFields::get_offset_of_invalidDataException_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (NonBlockingStreamReader_t2495303928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1711[11] = 
{
	NonBlockingStreamReader_t2495303928::get_offset_of_input_buffer_2(),
	NonBlockingStreamReader_t2495303928::get_offset_of_decoded_buffer_3(),
	NonBlockingStreamReader_t2495303928::get_offset_of_decoded_count_4(),
	NonBlockingStreamReader_t2495303928::get_offset_of_pos_5(),
	NonBlockingStreamReader_t2495303928::get_offset_of_buffer_size_6(),
	NonBlockingStreamReader_t2495303928::get_offset_of_encoding_7(),
	NonBlockingStreamReader_t2495303928::get_offset_of_decoder_8(),
	NonBlockingStreamReader_t2495303928::get_offset_of_base_stream_9(),
	NonBlockingStreamReader_t2495303928::get_offset_of_mayBlock_10(),
	NonBlockingStreamReader_t2495303928::get_offset_of_line_builder_11(),
	NonBlockingStreamReader_t2495303928::get_offset_of_foundCR_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (XmlInputStream_t1691369434), -1, sizeof(XmlInputStream_t1691369434_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1712[7] = 
{
	XmlInputStream_t1691369434_StaticFields::get_offset_of_StrictUTF8_2(),
	XmlInputStream_t1691369434::get_offset_of_enc_3(),
	XmlInputStream_t1691369434::get_offset_of_stream_4(),
	XmlInputStream_t1691369434::get_offset_of_buffer_5(),
	XmlInputStream_t1691369434::get_offset_of_bufLength_6(),
	XmlInputStream_t1691369434::get_offset_of_bufPos_7(),
	XmlInputStream_t1691369434_StaticFields::get_offset_of_encodingException_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (XmlParserInput_t2182411204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1713[5] = 
{
	XmlParserInput_t2182411204::get_offset_of_sourceStack_0(),
	XmlParserInput_t2182411204::get_offset_of_source_1(),
	XmlParserInput_t2182411204::get_offset_of_has_peek_2(),
	XmlParserInput_t2182411204::get_offset_of_peek_char_3(),
	XmlParserInput_t2182411204::get_offset_of_allowTextDecl_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (XmlParserInputSource_t3533005609), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1714[6] = 
{
	XmlParserInputSource_t3533005609::get_offset_of_BaseURI_0(),
	XmlParserInputSource_t3533005609::get_offset_of_reader_1(),
	XmlParserInputSource_t3533005609::get_offset_of_state_2(),
	XmlParserInputSource_t3533005609::get_offset_of_isPE_3(),
	XmlParserInputSource_t3533005609::get_offset_of_line_4(),
	XmlParserInputSource_t3533005609::get_offset_of_column_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (XPathNavigator_t787956054), -1, sizeof(XPathNavigator_t787956054_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1716[2] = 
{
	XPathNavigator_t787956054_StaticFields::get_offset_of_escape_text_chars_0(),
	XPathNavigator_t787956054_StaticFields::get_offset_of_escape_attr_chars_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (EnumerableIterator_t1590428576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1717[3] = 
{
	EnumerableIterator_t1590428576::get_offset_of_source_1(),
	EnumerableIterator_t1590428576::get_offset_of_e_2(),
	EnumerableIterator_t1590428576::get_offset_of_pos_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (U3CEnumerateChildrenU3Ec__Iterator0_t404642154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1718[8] = 
{
	U3CEnumerateChildrenU3Ec__Iterator0_t404642154::get_offset_of_n_0(),
	U3CEnumerateChildrenU3Ec__Iterator0_t404642154::get_offset_of_U3CnavU3E__0_1(),
	U3CEnumerateChildrenU3Ec__Iterator0_t404642154::get_offset_of_U3Cnav2U3E__1_2(),
	U3CEnumerateChildrenU3Ec__Iterator0_t404642154::get_offset_of_type_3(),
	U3CEnumerateChildrenU3Ec__Iterator0_t404642154::get_offset_of_U24PC_4(),
	U3CEnumerateChildrenU3Ec__Iterator0_t404642154::get_offset_of_U24current_5(),
	U3CEnumerateChildrenU3Ec__Iterator0_t404642154::get_offset_of_U3CU24U3En_6(),
	U3CEnumerateChildrenU3Ec__Iterator0_t404642154::get_offset_of_U3CU24U3Etype_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (XPathExpression_t1723793351), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (XPathItem_t4250588140), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (XPathNamespaceScope_t4128811329)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1721[4] = 
{
	XPathNamespaceScope_t4128811329::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (XPathNodeIterator_t3667290188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1722[1] = 
{
	XPathNodeIterator_t3667290188::get_offset_of__count_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (U3CGetEnumeratorU3Ec__Iterator2_t90454087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1723[3] = 
{
	U3CGetEnumeratorU3Ec__Iterator2_t90454087::get_offset_of_U24PC_0(),
	U3CGetEnumeratorU3Ec__Iterator2_t90454087::get_offset_of_U24current_1(),
	U3CGetEnumeratorU3Ec__Iterator2_t90454087::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { sizeof (XPathResultType_t2828988488)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1724[8] = 
{
	XPathResultType_t2828988488::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { sizeof (XPathNodeType_t3031007223)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1725[11] = 
{
	XPathNodeType_t3031007223::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { sizeof (XmlDataType_t3437356259)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1726[3] = 
{
	XmlDataType_t3437356259::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (XmlSortOrder_t4114893492)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1727[3] = 
{
	XmlSortOrder_t4114893492::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { sizeof (XmlCaseOrder_t176547839)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1728[4] = 
{
	XmlCaseOrder_t176547839::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (XPathDocument_t1673143697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1729[1] = 
{
	XPathDocument_t1673143697::get_offset_of_document_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (XPathException_t2107611652), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (XPathIteratorComparer_t1799213572), -1, sizeof(XPathIteratorComparer_t1799213572_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1731[1] = 
{
	XPathIteratorComparer_t1799213572_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (XPathNavigatorComparer_t2421573191), -1, sizeof(XPathNavigatorComparer_t2421573191_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1732[1] = 
{
	XPathNavigatorComparer_t2421573191_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (XPathFunctions_t1481462947), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (XPathFunction_t857746608), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (XPathFunctionLast_t241688620), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (XPathFunctionPosition_t1996792158), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (XPathFunctionCount_t4174407007), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1737[1] = 
{
	XPathFunctionCount_t4174407007::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (XPathFunctionId_t1670301333), -1, sizeof(XPathFunctionId_t1670301333_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1738[2] = 
{
	XPathFunctionId_t1670301333::get_offset_of_arg0_0(),
	XPathFunctionId_t1670301333_StaticFields::get_offset_of_rgchWhitespace_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (XPathFunctionLocalName_t1807446049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1739[1] = 
{
	XPathFunctionLocalName_t1807446049::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (XPathFunctionNamespaceUri_t4077891083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1740[1] = 
{
	XPathFunctionNamespaceUri_t4077891083::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (XPathFunctionName_t1405602168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1741[1] = 
{
	XPathFunctionName_t1405602168::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (XPathFunctionString_t3820712894), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1742[1] = 
{
	XPathFunctionString_t3820712894::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { sizeof (XPathFunctionConcat_t3351639595), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1743[1] = 
{
	XPathFunctionConcat_t3351639595::get_offset_of_rgs_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (XPathFunctionStartsWith_t3734288208), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1744[2] = 
{
	XPathFunctionStartsWith_t3734288208::get_offset_of_arg0_0(),
	XPathFunctionStartsWith_t3734288208::get_offset_of_arg1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (XPathFunctionContains_t678583606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1745[2] = 
{
	XPathFunctionContains_t678583606::get_offset_of_arg0_0(),
	XPathFunctionContains_t678583606::get_offset_of_arg1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (XPathFunctionSubstringBefore_t3299707364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1746[2] = 
{
	XPathFunctionSubstringBefore_t3299707364::get_offset_of_arg0_0(),
	XPathFunctionSubstringBefore_t3299707364::get_offset_of_arg1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (XPathFunctionSubstringAfter_t2254953802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1747[2] = 
{
	XPathFunctionSubstringAfter_t2254953802::get_offset_of_arg0_0(),
	XPathFunctionSubstringAfter_t2254953802::get_offset_of_arg1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (XPathFunctionSubstring_t3677332116), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1748[3] = 
{
	XPathFunctionSubstring_t3677332116::get_offset_of_arg0_0(),
	XPathFunctionSubstring_t3677332116::get_offset_of_arg1_1(),
	XPathFunctionSubstring_t3677332116::get_offset_of_arg2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { sizeof (XPathFunctionStringLength_t1686187666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1749[1] = 
{
	XPathFunctionStringLength_t1686187666::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (XPathFunctionNormalizeSpace_t379159227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1750[1] = 
{
	XPathFunctionNormalizeSpace_t379159227::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (XPathFunctionTranslate_t1071508611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1751[3] = 
{
	XPathFunctionTranslate_t1071508611::get_offset_of_arg0_0(),
	XPathFunctionTranslate_t1071508611::get_offset_of_arg1_1(),
	XPathFunctionTranslate_t1071508611::get_offset_of_arg2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (XPathBooleanFunction_t57991257), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (XPathFunctionBoolean_t2892373420), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1753[1] = 
{
	XPathFunctionBoolean_t2892373420::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (XPathFunctionNot_t3025979743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1754[1] = 
{
	XPathFunctionNot_t3025979743::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (XPathFunctionTrue_t3806785954), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (XPathFunctionFalse_t1021066162), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (XPathFunctionLang_t242671679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1757[1] = 
{
	XPathFunctionLang_t242671679::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (XPathNumericFunction_t307051317), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (XPathFunctionNumber_t699684043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1759[1] = 
{
	XPathFunctionNumber_t699684043::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (XPathFunctionSum_t2229418177), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1760[1] = 
{
	XPathFunctionSum_t2229418177::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (XPathFunctionFloor_t3617095014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1761[1] = 
{
	XPathFunctionFloor_t3617095014::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (XPathFunctionCeil_t4176839285), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1762[1] = 
{
	XPathFunctionCeil_t4176839285::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (XPathFunctionRound_t2668931994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1763[1] = 
{
	XPathFunctionRound_t2668931994::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { sizeof (CompiledExpression_t4018285681), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1764[4] = 
{
	CompiledExpression_t4018285681::get_offset_of__nsm_0(),
	CompiledExpression_t4018285681::get_offset_of__expr_1(),
	CompiledExpression_t4018285681::get_offset_of__sorters_2(),
	CompiledExpression_t4018285681::get_offset_of_rawExpression_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (XPathSortElement_t4237073177), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1765[2] = 
{
	XPathSortElement_t4237073177::get_offset_of_Navigator_0(),
	XPathSortElement_t4237073177::get_offset_of_Values_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (XPathSorters_t698127628), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1766[1] = 
{
	XPathSorters_t698127628::get_offset_of__rgSorters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (XPathSorter_t36376808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1767[3] = 
{
	XPathSorter_t36376808::get_offset_of__expr_0(),
	XPathSorter_t36376808::get_offset_of__cmp_1(),
	XPathSorter_t36376808::get_offset_of__type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (XPathNumberComparer_t6863540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1768[1] = 
{
	XPathNumberComparer_t6863540::get_offset_of__nMulSort_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (XPathTextComparer_t2075334572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1769[4] = 
{
	XPathTextComparer_t2075334572::get_offset_of__nMulSort_0(),
	XPathTextComparer_t2075334572::get_offset_of__nMulCase_1(),
	XPathTextComparer_t2075334572::get_offset_of__orderCase_2(),
	XPathTextComparer_t2075334572::get_offset_of__ci_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (Expression_t1452783009), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (ExprBinary_t2069694888), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1771[2] = 
{
	ExprBinary_t2069694888::get_offset_of__left_0(),
	ExprBinary_t2069694888::get_offset_of__right_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { sizeof (ExprBoolean_t3855188593), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (ExprOR_t3019581832), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (ExprAND_t348538276), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (EqualityExpr_t1646148531), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1775[1] = 
{
	EqualityExpr_t1646148531::get_offset_of_trueVal_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (ExprEQ_t1453891107), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (ExprNE_t4135261543), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (RelationalExpr_t3307137467), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (ExprGT_t1857306706), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { sizeof (ExprGE_t4135851367), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { sizeof (ExprLT_t1856585810), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { sizeof (ExprLE_t4135130471), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { sizeof (ExprNumeric_t370757872), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (ExprPLUS_t1725353634), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (ExprMINUS_t3125292145), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { sizeof (ExprMULT_t118778748), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (ExprDIV_t1961659563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (ExprMOD_t1913835785), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (ExprNEG_t381818215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1789[1] = 
{
	ExprNEG_t381818215::get_offset_of__expr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (NodeSet_t3272593155), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (ExprUNION_t1609753984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1791[2] = 
{
	ExprUNION_t1609753984::get_offset_of_left_0(),
	ExprUNION_t1609753984::get_offset_of_right_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (ExprSLASH_t1263914658), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1792[2] = 
{
	ExprSLASH_t1263914658::get_offset_of_left_0(),
	ExprSLASH_t1263914658::get_offset_of_right_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { sizeof (ExprSLASH2_t1762680492), -1, sizeof(ExprSLASH2_t1762680492_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1793[3] = 
{
	ExprSLASH2_t1762680492::get_offset_of_left_0(),
	ExprSLASH2_t1762680492::get_offset_of_right_1(),
	ExprSLASH2_t1762680492_StaticFields::get_offset_of_DescendantOrSelfStar_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (ExprRoot_t3091324302), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (Axes_t1882171014)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1795[14] = 
{
	Axes_t1882171014::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (AxisSpecifier_t40435393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1796[1] = 
{
	AxisSpecifier_t40435393::get_offset_of__axis_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (NodeTest_t747859056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1797[1] = 
{
	NodeTest_t747859056::get_offset_of__axis_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (NodeTypeTest_t4287533341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1798[2] = 
{
	NodeTypeTest_t4287533341::get_offset_of_type_1(),
	NodeTypeTest_t4287533341::get_offset_of__param_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (NodeNameTest_t562814213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1799[2] = 
{
	NodeNameTest_t562814213::get_offset_of__name_1(),
	NodeNameTest_t562814213::get_offset_of_resolvedName_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
