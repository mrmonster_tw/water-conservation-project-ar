﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_I2767586507.h"





extern const Il2CppType IStream_t2767586507_0_0_0;
extern "C" void Context_t1744531130_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Context_t1744531130_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Context_t1744531130_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Context_t1744531130_0_0_0;
extern "C" void Escape_t3294788190_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Escape_t3294788190_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Escape_t3294788190_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Escape_t3294788190_0_0_0;
extern "C" void PreviousInfo_t2148130204_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PreviousInfo_t2148130204_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PreviousInfo_t2148130204_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType PreviousInfo_t2148130204_0_0_0;
extern "C" void UriScheme_t2867806342_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UriScheme_t2867806342_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UriScheme_t2867806342_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType UriScheme_t2867806342_0_0_0;
extern "C" void DelegatePInvokeWrapper_AppDomainInitializer_t682969308();
extern const Il2CppType AppDomainInitializer_t682969308_0_0_0;
extern "C" void DelegatePInvokeWrapper_Swapper_t2822380397();
extern const Il2CppType Swapper_t2822380397_0_0_0;
extern "C" void DictionaryEntry_t3123975638_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DictionaryEntry_t3123975638_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DictionaryEntry_t3123975638_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType DictionaryEntry_t3123975638_0_0_0;
extern "C" void Slot_t3975888750_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t3975888750_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t3975888750_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Slot_t3975888750_0_0_0;
extern "C" void Slot_t384495010_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t384495010_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t384495010_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Slot_t384495010_0_0_0;
extern "C" void DelegatePInvokeWrapper_InternalCancelHandler_t872516951();
extern const Il2CppType InternalCancelHandler_t872516951_0_0_0;
extern "C" void ConsoleKeyInfo_t1802691652_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ConsoleKeyInfo_t1802691652_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ConsoleKeyInfo_t1802691652_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ConsoleKeyInfo_t1802691652_0_0_0;
extern "C" void Enum_t4135868527_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Enum_t4135868527_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Enum_t4135868527_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Enum_t4135868527_0_0_0;
extern "C" void InputRecord_t2660212290_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void InputRecord_t2660212290_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void InputRecord_t2660212290_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType InputRecord_t2660212290_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t714865915();
extern const Il2CppType ReadDelegate_t714865915_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t4270993571();
extern const Il2CppType WriteDelegate_t4270993571_0_0_0;
extern "C" void MonoIOStat_t592533987_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoIOStat_t592533987_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoIOStat_t592533987_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MonoIOStat_t592533987_0_0_0;
extern "C" void MonoEnumInfo_t3694469084_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEnumInfo_t3694469084_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEnumInfo_t3694469084_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MonoEnumInfo_t3694469084_0_0_0;
extern "C" void CustomAttributeNamedArgument_t287865710_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeNamedArgument_t287865710_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeNamedArgument_t287865710_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CustomAttributeNamedArgument_t287865710_0_0_0;
extern "C" void CustomAttributeTypedArgument_t2723150157_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeTypedArgument_t2723150157_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeTypedArgument_t2723150157_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CustomAttributeTypedArgument_t2723150157_0_0_0;
extern "C" void ILTokenInfo_t2325775114_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ILTokenInfo_t2325775114_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ILTokenInfo_t2325775114_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ILTokenInfo_t2325775114_0_0_0;
extern "C" void MonoResource_t4103430009_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoResource_t4103430009_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoResource_t4103430009_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MonoResource_t4103430009_0_0_0;
extern "C" void RefEmitPermissionSet_t484390987_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RefEmitPermissionSet_t484390987_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RefEmitPermissionSet_t484390987_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RefEmitPermissionSet_t484390987_0_0_0;
extern "C" void InterfaceMapping_t1267958657_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void InterfaceMapping_t1267958657_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void InterfaceMapping_t1267958657_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType InterfaceMapping_t1267958657_0_0_0;
extern "C" void MonoEventInfo_t346866618_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEventInfo_t346866618_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEventInfo_t346866618_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MonoEventInfo_t346866618_0_0_0;
extern "C" void MonoMethodInfo_t1248819020_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoMethodInfo_t1248819020_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoMethodInfo_t1248819020_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MonoMethodInfo_t1248819020_0_0_0;
extern "C" void MonoPropertyInfo_t3087356066_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoPropertyInfo_t3087356066_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoPropertyInfo_t3087356066_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MonoPropertyInfo_t3087356066_0_0_0;
extern "C" void ParameterModifier_t1461694466_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ParameterModifier_t1461694466_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ParameterModifier_t1461694466_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ParameterModifier_t1461694466_0_0_0;
extern "C" void ResourceCacheItem_t51292791_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceCacheItem_t51292791_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceCacheItem_t51292791_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ResourceCacheItem_t51292791_0_0_0;
extern "C" void ResourceInfo_t2872965302_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceInfo_t2872965302_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceInfo_t2872965302_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ResourceInfo_t2872965302_0_0_0;
extern "C" void STATSTG_t1180006254_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void STATSTG_t1180006254_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void STATSTG_t1180006254_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType STATSTG_t1180006254_0_0_0;
extern "C" void DelegatePInvokeWrapper_CrossContextDelegate_t387175271();
extern const Il2CppType CrossContextDelegate_t387175271_0_0_0;
extern "C" void DelegatePInvokeWrapper_CallbackHandler_t3280319253();
extern const Il2CppType CallbackHandler_t3280319253_0_0_0;
extern "C" void SerializationEntry_t648286436_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SerializationEntry_t648286436_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SerializationEntry_t648286436_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SerializationEntry_t648286436_0_0_0;
extern "C" void StreamingContext_t3711869237_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StreamingContext_t3711869237_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StreamingContext_t3711869237_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType StreamingContext_t3711869237_0_0_0;
extern "C" void DSAParameters_t1885824122_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DSAParameters_t1885824122_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DSAParameters_t1885824122_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType DSAParameters_t1885824122_0_0_0;
extern "C" void RSAParameters_t1728406613_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RSAParameters_t1728406613_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RSAParameters_t1728406613_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RSAParameters_t1728406613_0_0_0;
extern "C" void SNIP_t4156255223_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SNIP_t4156255223_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SNIP_t4156255223_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SNIP_t4156255223_0_0_0;
extern "C" void SecurityFrame_t1422462475_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SecurityFrame_t1422462475_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SecurityFrame_t1422462475_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SecurityFrame_t1422462475_0_0_0;
extern "C" void DelegatePInvokeWrapper_ThreadStart_t1006689297();
extern const Il2CppType ThreadStart_t1006689297_0_0_0;
extern "C" void ValueType_t3640485471_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ValueType_t3640485471_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ValueType_t3640485471_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ValueType_t3640485471_0_0_0;
extern "C" void CRYPTPROTECT_PROMPTSTRUCT_t1592014442_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CRYPTPROTECT_PROMPTSTRUCT_t1592014442_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CRYPTPROTECT_PROMPTSTRUCT_t1592014442_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CRYPTPROTECT_PROMPTSTRUCT_t1592014442_0_0_0;
extern "C" void X509IssuerSerial_t3270105241_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void X509IssuerSerial_t3270105241_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void X509IssuerSerial_t3270105241_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType X509IssuerSerial_t3270105241_0_0_0;
extern "C" void DTMXPathLinkedNode2_t3353097823_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DTMXPathLinkedNode2_t3353097823_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DTMXPathLinkedNode2_t3353097823_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType DTMXPathLinkedNode2_t3353097823_0_0_0;
extern "C" void Attribute_t270895445_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Attribute_t270895445_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Attribute_t270895445_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Attribute_t270895445_0_0_0;
extern "C" void TagName_t2891256255_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TagName_t2891256255_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TagName_t2891256255_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TagName_t2891256255_0_0_0;
extern "C" void QNameValueType_t615604793_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void QNameValueType_t615604793_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void QNameValueType_t615604793_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType QNameValueType_t615604793_0_0_0;
extern "C" void StringArrayValueType_t3147326440_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StringArrayValueType_t3147326440_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StringArrayValueType_t3147326440_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType StringArrayValueType_t3147326440_0_0_0;
extern "C" void StringValueType_t261329552_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StringValueType_t261329552_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StringValueType_t261329552_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType StringValueType_t261329552_0_0_0;
extern "C" void UriValueType_t2866347695_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UriValueType_t2866347695_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UriValueType_t2866347695_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType UriValueType_t2866347695_0_0_0;
extern "C" void NsDecl_t3938094415_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void NsDecl_t3938094415_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void NsDecl_t3938094415_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType NsDecl_t3938094415_0_0_0;
extern "C" void NsScope_t3958624705_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void NsScope_t3958624705_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void NsScope_t3958624705_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType NsScope_t3958624705_0_0_0;
extern "C" void DelegatePInvokeWrapper_CharGetter_t1703763694();
extern const Il2CppType CharGetter_t1703763694_0_0_0;
extern "C" void NodeEnumerator_t2183475207_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void NodeEnumerator_t2183475207_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void NodeEnumerator_t2183475207_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType NodeEnumerator_t2183475207_0_0_0;
extern "C" void DelegatePInvokeWrapper_AsyncReadHandler_t1188682440();
extern const Il2CppType AsyncReadHandler_t1188682440_0_0_0;
extern "C" void ProcessAsyncReader_t337580163_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ProcessAsyncReader_t337580163_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ProcessAsyncReader_t337580163_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ProcessAsyncReader_t337580163_0_0_0;
extern "C" void ProcInfo_t2917059746_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ProcInfo_t2917059746_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ProcInfo_t2917059746_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ProcInfo_t2917059746_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadMethod_t893206259();
extern const Il2CppType ReadMethod_t893206259_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnmanagedReadOrWrite_t876388624();
extern const Il2CppType UnmanagedReadOrWrite_t876388624_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteMethod_t2538911768();
extern const Il2CppType WriteMethod_t2538911768_0_0_0;
extern "C" void InotifyEvent_t2637353984_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void InotifyEvent_t2637353984_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void InotifyEvent_t2637353984_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType InotifyEvent_t2637353984_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t2469437439();
extern const Il2CppType ReadDelegate_t2469437439_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t1613340939();
extern const Il2CppType WriteDelegate_t1613340939_0_0_0;
extern "C" void WaitForChangedResult_t3377826936_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WaitForChangedResult_t3377826936_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WaitForChangedResult_t3377826936_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType WaitForChangedResult_t3377826936_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t4266946825();
extern const Il2CppType ReadDelegate_t4266946825_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t2016697242();
extern const Il2CppType WriteDelegate_t2016697242_0_0_0;
extern "C" void DelegatePInvokeWrapper_SocketAsyncCall_t1521370843();
extern const Il2CppType SocketAsyncCall_t1521370843_0_0_0;
extern "C" void SocketAsyncResult_t2080034863_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SocketAsyncResult_t2080034863_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SocketAsyncResult_t2080034863_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SocketAsyncResult_t2080034863_0_0_0;
extern "C" void X509ChainStatus_t133602714_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void X509ChainStatus_t133602714_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void X509ChainStatus_t133602714_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType X509ChainStatus_t133602714_0_0_0;
extern "C" void IntStack_t2189327687_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void IntStack_t2189327687_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void IntStack_t2189327687_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType IntStack_t2189327687_0_0_0;
extern "C" void Interval_t1802865632_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Interval_t1802865632_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Interval_t1802865632_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Interval_t1802865632_0_0_0;
extern "C" void DelegatePInvokeWrapper_CostDelegate_t1722821004();
extern const Il2CppType CostDelegate_t1722821004_0_0_0;
extern "C" void IntStack_t561212167_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void IntStack_t561212167_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void IntStack_t561212167_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType IntStack_t561212167_0_0_0;
extern "C" void UriScheme_t722425697_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UriScheme_t722425697_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UriScheme_t722425697_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType UriScheme_t722425697_0_0_0;
extern "C" void DelegatePInvokeWrapper_SignalHandler_t1590986791();
extern const Il2CppType SignalHandler_t1590986791_0_0_0;
extern "C" void DelegatePInvokeWrapper_Action_t1264377477();
extern const Il2CppType Action_t1264377477_0_0_0;
extern "C" void SecurityAttributesHack_t2445122734_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SecurityAttributesHack_t2445122734_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SecurityAttributesHack_t2445122734_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SecurityAttributesHack_t2445122734_0_0_0;
extern "C" void Color_t1869934208_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Color_t1869934208_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Color_t1869934208_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Color_t1869934208_0_0_0;
extern "C" Il2CppIUnknown* CreateComCallableWrapperFor_ComIStreamWrapper_t1385077908(Il2CppObject* obj);
extern const Il2CppType ComIStreamWrapper_t1385077908_0_0_0;
extern "C" void DelegatePInvokeWrapper_StreamCloseDelegate_t2004792630();
extern const Il2CppType StreamCloseDelegate_t2004792630_0_0_0;
extern "C" void DelegatePInvokeWrapper_StreamGetBytesDelegate_t482941147();
extern const Il2CppType StreamGetBytesDelegate_t482941147_0_0_0;
extern "C" void DelegatePInvokeWrapper_StreamGetHeaderDelegate_t558280091();
extern const Il2CppType StreamGetHeaderDelegate_t558280091_0_0_0;
extern "C" void DelegatePInvokeWrapper_StreamPutBytesDelegate_t225654305();
extern const Il2CppType StreamPutBytesDelegate_t225654305_0_0_0;
extern "C" void DelegatePInvokeWrapper_StreamSeekDelegate_t2972861213();
extern const Il2CppType StreamSeekDelegate_t2972861213_0_0_0;
extern "C" void DelegatePInvokeWrapper_StreamSizeDelegate_t1636074383();
extern const Il2CppType StreamSizeDelegate_t1636074383_0_0_0;
extern "C" void IconDir_t4069448150_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void IconDir_t4069448150_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void IconDir_t4069448150_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType IconDir_t4069448150_0_0_0;
extern "C" void IconImage_t1835934191_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void IconImage_t1835934191_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void IconImage_t1835934191_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType IconImage_t1835934191_0_0_0;
extern "C" void BitmapData_t288533671_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void BitmapData_t288533671_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void BitmapData_t288533671_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType BitmapData_t288533671_0_0_0;
extern "C" void EncoderParameter_t138078747_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void EncoderParameter_t138078747_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void EncoderParameter_t138078747_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType EncoderParameter_t138078747_0_0_0;
extern "C" void EnumMemberInfo_t3072296154_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void EnumMemberInfo_t3072296154_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void EnumMemberInfo_t3072296154_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType EnumMemberInfo_t3072296154_0_0_0;
extern "C" void DelegatePInvokeWrapper_CheckBlockEnd_t1373994268();
extern const Il2CppType CheckBlockEnd_t1373994268_0_0_0;
extern "C" void CodeUnit_t1519973372_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CodeUnit_t1519973372_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CodeUnit_t1519973372_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CodeUnit_t1519973372_0_0_0;
extern "C" void DelegatePInvokeWrapper_ParsingCompleteHandler_t1375501938();
extern const Il2CppType ParsingCompleteHandler_t1375501938_0_0_0;
extern "C" void DelegatePInvokeWrapper_NoParamsDelegate_t1618427640();
extern const Il2CppType NoParamsDelegate_t1618427640_0_0_0;
extern "C" void AddedAttr_t2359971688_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AddedAttr_t2359971688_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AddedAttr_t2359971688_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AddedAttr_t2359971688_0_0_0;
extern "C" void AddedStyle_t611321135_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AddedStyle_t611321135_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AddedStyle_t611321135_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AddedStyle_t611321135_0_0_0;
extern "C" void AddedTag_t1198678936_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AddedTag_t1198678936_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AddedTag_t1198678936_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AddedTag_t1198678936_0_0_0;
extern "C" void FontUnit_t3688074528_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FontUnit_t3688074528_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FontUnit_t3688074528_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType FontUnit_t3688074528_0_0_0;
extern "C" void Unit_t3186727900_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Unit_t3186727900_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Unit_t3186727900_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Unit_t3186727900_0_0_0;
extern "C" void DelegatePInvokeWrapper_AndroidJavaRunnable_t377890659();
extern const Il2CppType AndroidJavaRunnable_t377890659_0_0_0;
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AnimationCurve_t3046754366_0_0_0;
extern "C" void AnimationEvent_t1536042487_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationEvent_t1536042487_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationEvent_t1536042487_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AnimationEvent_t1536042487_0_0_0;
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AnimatorTransitionInfo_t2534804151_0_0_0;
extern "C" void DelegatePInvokeWrapper_LogCallback_t3588208630();
extern const Il2CppType LogCallback_t3588208630_0_0_0;
extern "C" void DelegatePInvokeWrapper_LowMemoryCallback_t4104246196();
extern const Il2CppType LowMemoryCallback_t4104246196_0_0_0;
extern "C" void AssetBundleRequest_t699759206_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AssetBundleRequest_t699759206_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AssetBundleRequest_t699759206_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AssetBundleRequest_t699759206_0_0_0;
extern "C" void AsyncOperation_t1445031843_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AsyncOperation_t1445031843_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AsyncOperation_t1445031843_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AsyncOperation_t1445031843_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMReaderCallback_t1677636661();
extern const Il2CppType PCMReaderCallback_t1677636661_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMSetPositionCallback_t1059417452();
extern const Il2CppType PCMSetPositionCallback_t1059417452_0_0_0;
extern "C" void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t2089929874();
extern const Il2CppType AudioConfigurationChangeHandler_t2089929874_0_0_0;
extern "C" void DelegatePInvokeWrapper_WillRenderCanvases_t3309123499();
extern const Il2CppType WillRenderCanvases_t3309123499_0_0_0;
extern "C" void CharacterInfo_t1228754872_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CharacterInfo_t1228754872_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CharacterInfo_t1228754872_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CharacterInfo_t1228754872_0_0_0;
extern "C" void Collision_t4262080450_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Collision_t4262080450_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Collision_t4262080450_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Collision_t4262080450_0_0_0;
extern "C" void Collision2D_t2842956331_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Collision2D_t2842956331_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Collision2D_t2842956331_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Collision2D_t2842956331_0_0_0;
extern "C" void ContactFilter2D_t3805203441_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ContactFilter2D_t3805203441_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ContactFilter2D_t3805203441_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ContactFilter2D_t3805203441_0_0_0;
extern "C" void ControllerColliderHit_t240592346_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ControllerColliderHit_t240592346_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ControllerColliderHit_t240592346_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ControllerColliderHit_t240592346_0_0_0;
extern "C" void Coroutine_t3829159415_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Coroutine_t3829159415_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Coroutine_t3829159415_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Coroutine_t3829159415_0_0_0;
extern "C" void CullingGroup_t2096318768_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CullingGroup_t2096318768_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CullingGroup_t2096318768_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CullingGroup_t2096318768_0_0_0;
extern "C" void DelegatePInvokeWrapper_StateChanged_t2136737110();
extern const Il2CppType StateChanged_t2136737110_0_0_0;
extern "C" void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t51287044();
extern const Il2CppType DisplaysUpdatedDelegate_t51287044_0_0_0;
extern "C" void Event_t2956885303_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Event_t2956885303_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Event_t2956885303_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Event_t2956885303_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnityAction_t3245792599();
extern const Il2CppType UnityAction_t3245792599_0_0_0;
extern "C" void DelegatePInvokeWrapper_FontTextureRebuildCallback_t2467502454();
extern const Il2CppType FontTextureRebuildCallback_t2467502454_0_0_0;
extern "C" void Gradient_t3067099924_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Gradient_t3067099924_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Gradient_t3067099924_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Gradient_t3067099924_0_0_0;
extern "C" void DelegatePInvokeWrapper_WindowFunction_t3146511083();
extern const Il2CppType WindowFunction_t3146511083_0_0_0;
extern "C" void GUIContent_t3050628031_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIContent_t3050628031_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIContent_t3050628031_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GUIContent_t3050628031_0_0_0;
extern "C" void DelegatePInvokeWrapper_SkinChangedDelegate_t1143955295();
extern const Il2CppType SkinChangedDelegate_t1143955295_0_0_0;
extern "C" void GUIStyle_t3956901511_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyle_t3956901511_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyle_t3956901511_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GUIStyle_t3956901511_0_0_0;
extern "C" void GUIStyleState_t1397964415_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyleState_t1397964415_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyleState_t1397964415_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GUIStyleState_t1397964415_0_0_0;
extern "C" void HumanBone_t2465339518_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HumanBone_t2465339518_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HumanBone_t2465339518_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType HumanBone_t2465339518_0_0_0;
extern "C" void Internal_DrawTextureArguments_t1705718261_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Internal_DrawTextureArguments_t1705718261_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Internal_DrawTextureArguments_t1705718261_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Internal_DrawTextureArguments_t1705718261_0_0_0;
extern "C" void jvalue_t1372148875_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void jvalue_t1372148875_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void jvalue_t1372148875_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType jvalue_t1372148875_0_0_0;
extern "C" void DownloadHandler_t2937767557_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DownloadHandler_t2937767557_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DownloadHandler_t2937767557_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType DownloadHandler_t2937767557_0_0_0;
extern "C" void DownloadHandlerBuffer_t2928496527_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DownloadHandlerBuffer_t2928496527_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DownloadHandlerBuffer_t2928496527_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType DownloadHandlerBuffer_t2928496527_0_0_0;
extern "C" void UnityWebRequest_t463507806_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityWebRequest_t463507806_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityWebRequest_t463507806_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType UnityWebRequest_t463507806_0_0_0;
extern "C" void UploadHandler_t2993558019_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UploadHandler_t2993558019_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UploadHandler_t2993558019_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType UploadHandler_t2993558019_0_0_0;
extern "C" void Object_t631007953_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Object_t631007953_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Object_t631007953_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Object_t631007953_0_0_0;
extern "C" void RaycastHit_t1056001966_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit_t1056001966_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit_t1056001966_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RaycastHit_t1056001966_0_0_0;
extern "C" void RaycastHit2D_t2279581989_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit2D_t2279581989_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit2D_t2279581989_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RaycastHit2D_t2279581989_0_0_0;
extern "C" void RectOffset_t1369453676_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RectOffset_t1369453676_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RectOffset_t1369453676_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RectOffset_t1369453676_0_0_0;
extern "C" void DelegatePInvokeWrapper_UpdatedEventHandler_t1027848393();
extern const Il2CppType UpdatedEventHandler_t1027848393_0_0_0;
extern "C" void ResourceRequest_t3109103591_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceRequest_t3109103591_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceRequest_t3109103591_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ResourceRequest_t3109103591_0_0_0;
extern "C" void ScriptableObject_t2528358522_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ScriptableObject_t2528358522_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ScriptableObject_t2528358522_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ScriptableObject_t2528358522_0_0_0;
extern "C" void HitInfo_t3229609740_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HitInfo_t3229609740_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HitInfo_t3229609740_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType HitInfo_t3229609740_0_0_0;
extern "C" void SkeletonBone_t4134054672_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SkeletonBone_t4134054672_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SkeletonBone_t4134054672_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SkeletonBone_t4134054672_0_0_0;
extern "C" void SliderHandler_t1154919399_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SliderHandler_t1154919399_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SliderHandler_t1154919399_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SliderHandler_t1154919399_0_0_0;
extern "C" void GcAchievementData_t675222246_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementData_t675222246_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementData_t675222246_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GcAchievementData_t675222246_0_0_0;
extern "C" void GcAchievementDescriptionData_t643925653_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementDescriptionData_t643925653_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementDescriptionData_t643925653_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GcAchievementDescriptionData_t643925653_0_0_0;
extern "C" void GcLeaderboard_t4132273028_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcLeaderboard_t4132273028_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcLeaderboard_t4132273028_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GcLeaderboard_t4132273028_0_0_0;
extern "C" void GcScoreData_t2125309831_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcScoreData_t2125309831_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcScoreData_t2125309831_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GcScoreData_t2125309831_0_0_0;
extern "C" void GcUserProfileData_t2719720026_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcUserProfileData_t2719720026_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcUserProfileData_t2719720026_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GcUserProfileData_t2719720026_0_0_0;
extern "C" void TextGenerationSettings_t1351628751_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerationSettings_t1351628751_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerationSettings_t1351628751_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TextGenerationSettings_t1351628751_0_0_0;
extern "C" void TextGenerator_t3211863866_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerator_t3211863866_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerator_t3211863866_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TextGenerator_t3211863866_0_0_0;
extern "C" void TrackedReference_t1199777556_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TrackedReference_t1199777556_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TrackedReference_t1199777556_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TrackedReference_t1199777556_0_0_0;
extern "C" void WaitForSeconds_t1699091251_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WaitForSeconds_t1699091251_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WaitForSeconds_t1699091251_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType WaitForSeconds_t1699091251_0_0_0;
extern "C" void WebCamDevice_t1322781432_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WebCamDevice_t1322781432_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WebCamDevice_t1322781432_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType WebCamDevice_t1322781432_0_0_0;
extern "C" void YieldInstruction_t403091072_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void YieldInstruction_t403091072_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void YieldInstruction_t403091072_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType YieldInstruction_t403091072_0_0_0;
extern "C" void CameraField_t1483002240_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CameraField_t1483002240_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CameraField_t1483002240_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CameraField_t1483002240_0_0_0;
extern "C" void CameraFieldData_t409245341_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CameraFieldData_t409245341_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CameraFieldData_t409245341_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CameraFieldData_t409245341_0_0_0;
extern "C" void EyewearCalibrationReading_t664929988_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void EyewearCalibrationReading_t664929988_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void EyewearCalibrationReading_t664929988_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType EyewearCalibrationReading_t664929988_0_0_0;
extern "C" void TargetSearchResult_t3441982613_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TargetSearchResult_t3441982613_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TargetSearchResult_t3441982613_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TargetSearchResult_t3441982613_0_0_0;
extern "C" void AutoRotationState_t946804616_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AutoRotationState_t946804616_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AutoRotationState_t946804616_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AutoRotationState_t946804616_0_0_0;
extern "C" void ProfileCollection_t901995765_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ProfileCollection_t901995765_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ProfileCollection_t901995765_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ProfileCollection_t901995765_0_0_0;
extern "C" void DelegatePInvokeWrapper_DispatcherFactory_t1014155341();
extern const Il2CppType DispatcherFactory_t1014155341_0_0_0;
extern "C" void DTMXPathLinkedNode2_t3353097824_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DTMXPathLinkedNode2_t3353097824_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DTMXPathLinkedNode2_t3353097824_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType DTMXPathLinkedNode2_t3353097824_0_0_0;
extern "C" void DelegatePInvokeWrapper_AsyncWaitForMessageHandler_t1048830322();
extern const Il2CppType AsyncWaitForMessageHandler_t1048830322_0_0_0;
extern "C" void DelegatePInvokeWrapper_AsyncHandler_t217889813();
extern const Il2CppType AsyncHandler_t217889813_0_0_0;
extern "C" void RaycastResult_t3360306849_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastResult_t3360306849_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastResult_t3360306849_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RaycastResult_t3360306849_0_0_0;
extern "C" void ColorTween_t809614380_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ColorTween_t809614380_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ColorTween_t809614380_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ColorTween_t809614380_0_0_0;
extern "C" void FloatTween_t1274330004_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FloatTween_t1274330004_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FloatTween_t1274330004_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType FloatTween_t1274330004_0_0_0;
extern "C" void Resources_t1597885468_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Resources_t1597885468_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Resources_t1597885468_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Resources_t1597885468_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnValidateInput_t2355412304();
extern const Il2CppType OnValidateInput_t2355412304_0_0_0;
extern "C" void Navigation_t3049316579_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Navigation_t3049316579_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Navigation_t3049316579_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Navigation_t3049316579_0_0_0;
extern "C" void SpriteState_t1362986479_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SpriteState_t1362986479_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SpriteState_t1362986479_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SpriteState_t1362986479_0_0_0;
extern "C" void Data_t1588725102_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Data_t1588725102_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Data_t1588725102_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Data_t1588725102_0_0_0;
extern "C" void DelegatePInvokeWrapper_ApplyTween_t3327999347();
extern const Il2CppType ApplyTween_t3327999347_0_0_0;
extern "C" void DelegatePInvokeWrapper_EasingFunction_t2767217938();
extern const Il2CppType EasingFunction_t2767217938_0_0_0;
extern "C" void PropertyObserver_t4012421388_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PropertyObserver_t4012421388_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PropertyObserver_t4012421388_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType PropertyObserver_t4012421388_0_0_0;
extern "C" void Settings_t1032325577_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Settings_t1032325577_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Settings_t1032325577_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Settings_t1032325577_0_0_0;
extern "C" void BokehTextureSettings_t3943002634_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void BokehTextureSettings_t3943002634_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void BokehTextureSettings_t3943002634_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType BokehTextureSettings_t3943002634_0_0_0;
extern "C" void FocusSettings_t1261455774_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FocusSettings_t1261455774_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FocusSettings_t1261455774_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType FocusSettings_t1261455774_0_0_0;
extern "C" void GlobalSettings_t956974295_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GlobalSettings_t956974295_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GlobalSettings_t956974295_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GlobalSettings_t956974295_0_0_0;
extern "C" void QualitySettings_t1379802392_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void QualitySettings_t1379802392_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void QualitySettings_t1379802392_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType QualitySettings_t1379802392_0_0_0;
extern "C" void ChromaticAberrationSettings_t3807027201_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ChromaticAberrationSettings_t3807027201_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ChromaticAberrationSettings_t3807027201_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ChromaticAberrationSettings_t3807027201_0_0_0;
extern "C" void DistortionSettings_t3193728992_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DistortionSettings_t3193728992_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DistortionSettings_t3193728992_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType DistortionSettings_t3193728992_0_0_0;
extern "C" void VignetteSettings_t2342083791_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void VignetteSettings_t2342083791_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void VignetteSettings_t2342083791_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType VignetteSettings_t2342083791_0_0_0;
extern "C" void Frame_t3511111948_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Frame_t3511111948_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Frame_t3511111948_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Frame_t3511111948_0_0_0;
extern "C" void ReflectionSettings_t499321483_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ReflectionSettings_t499321483_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ReflectionSettings_t499321483_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ReflectionSettings_t499321483_0_0_0;
extern "C" void SSRSettings_t899296535_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SSRSettings_t899296535_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SSRSettings_t899296535_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SSRSettings_t899296535_0_0_0;
extern "C" void PredicationSettings_t4116788586_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PredicationSettings_t4116788586_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PredicationSettings_t4116788586_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType PredicationSettings_t4116788586_0_0_0;
extern "C" void QualitySettings_t1869387270_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void QualitySettings_t1869387270_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void QualitySettings_t1869387270_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType QualitySettings_t1869387270_0_0_0;
extern "C" void TemporalSettings_t2985116592_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TemporalSettings_t2985116592_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TemporalSettings_t2985116592_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TemporalSettings_t2985116592_0_0_0;
extern "C" void ChannelMixerSettings_t491321292_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ChannelMixerSettings_t491321292_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ChannelMixerSettings_t491321292_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ChannelMixerSettings_t491321292_0_0_0;
extern "C" void ColorGradingSettings_t1923563914_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ColorGradingSettings_t1923563914_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ColorGradingSettings_t1923563914_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ColorGradingSettings_t1923563914_0_0_0;
extern "C" void CurvesSettings_t2932745847_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CurvesSettings_t2932745847_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CurvesSettings_t2932745847_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CurvesSettings_t2932745847_0_0_0;
extern "C" void EyeAdaptationSettings_t3234830401_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void EyeAdaptationSettings_t3234830401_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void EyeAdaptationSettings_t3234830401_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType EyeAdaptationSettings_t3234830401_0_0_0;
extern "C" void LUTSettings_t2339616782_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void LUTSettings_t2339616782_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void LUTSettings_t2339616782_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType LUTSettings_t2339616782_0_0_0;
extern "C" void TonemappingSettings_t1161324624_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TonemappingSettings_t1161324624_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TonemappingSettings_t1161324624_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TonemappingSettings_t1161324624_0_0_0;
extern "C" void DelegatePInvokeWrapper_Callback_t3139336517();
extern const Il2CppType Callback_t3139336517_0_0_0;
extern "C" void NormalList_t1320820545_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void NormalList_t1320820545_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void NormalList_t1320820545_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType NormalList_t1320820545_0_0_0;
extern "C" void SureList_t2832800709_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SureList_t2832800709_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SureList_t2832800709_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SureList_t2832800709_0_0_0;
extern "C" void ComponetFuncScript_t187458_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ComponetFuncScript_t187458_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ComponetFuncScript_t187458_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ComponetFuncScript_t187458_0_0_0;
extern "C" void PlayerSetting_t3195188508_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PlayerSetting_t3195188508_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PlayerSetting_t3195188508_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType PlayerSetting_t3195188508_0_0_0;
extern "C" void DelegatePInvokeWrapper_LoadFunction_t2078002637();
extern const Il2CppType LoadFunction_t2078002637_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnLocalizeNotification_t3391620158();
extern const Il2CppType OnLocalizeNotification_t3391620158_0_0_0;
extern "C" void Preset_t2972107632_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Preset_t2972107632_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Preset_t2972107632_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Preset_t2972107632_0_0_0;
extern "C" void Imgs_t1080690280_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Imgs_t1080690280_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Imgs_t1080690280_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Imgs_t1080690280_0_0_0;
extern "C" void Questions_t2798307503_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Questions_t2798307503_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Questions_t2798307503_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Questions_t2798307503_0_0_0;
extern "C" void SetAnimInfo_t2127528194_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SetAnimInfo_t2127528194_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SetAnimInfo_t2127528194_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SetAnimInfo_t2127528194_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnFinished_t3778785451();
extern const Il2CppType OnFinished_t3778785451_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnFinished_t3364492952();
extern const Il2CppType OnFinished_t3364492952_0_0_0;
extern "C" void FadeEntry_t639421133_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FadeEntry_t639421133_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FadeEntry_t639421133_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType FadeEntry_t639421133_0_0_0;
extern "C" void DepthEntry_t628749918_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DepthEntry_t628749918_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DepthEntry_t628749918_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType DepthEntry_t628749918_0_0_0;
extern "C" void DelegatePInvokeWrapper_GetAnyKeyFunc_t1761480072();
extern const Il2CppType GetAnyKeyFunc_t1761480072_0_0_0;
extern "C" void DelegatePInvokeWrapper_GetAxisFunc_t2592608932();
extern const Il2CppType GetAxisFunc_t2592608932_0_0_0;
extern "C" void DelegatePInvokeWrapper_GetKeyStateFunc_t2810275146();
extern const Il2CppType GetKeyStateFunc_t2810275146_0_0_0;
extern "C" void DelegatePInvokeWrapper_GetTouchCountCallback_t3185863032();
extern const Il2CppType GetTouchCountCallback_t3185863032_0_0_0;
extern "C" void DelegatePInvokeWrapper_MoveDelegate_t16019400();
extern const Il2CppType MoveDelegate_t16019400_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnCustomInput_t3508588789();
extern const Il2CppType OnCustomInput_t3508588789_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnSchemeChange_t1701155603();
extern const Il2CppType OnSchemeChange_t1701155603_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnScreenResize_t2279991692();
extern const Il2CppType OnScreenResize_t2279991692_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnReposition_t1372889220();
extern const Il2CppType OnReposition_t1372889220_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnValidate_t1246632601();
extern const Il2CppType OnValidate_t1246632601_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnGeometryUpdated_t2462438111();
extern const Il2CppType OnGeometryUpdated_t2462438111_0_0_0;
extern "C" void DelegatePInvokeWrapper_LegacyEvent_t2749056879();
extern const Il2CppType LegacyEvent_t2749056879_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnDragFinished_t3715779777();
extern const Il2CppType OnDragFinished_t3715779777_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnDragNotification_t1437737811();
extern const Il2CppType OnDragNotification_t1437737811_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnReposition_t3913508630();
extern const Il2CppType OnReposition_t3913508630_0_0_0;
extern "C" void DelegatePInvokeWrapper_Validate_t3702293971();
extern const Il2CppType Validate_t3702293971_0_0_0;
extern "C" void DelegatePInvokeWrapper_HitCheck_t2300079615();
extern const Il2CppType HitCheck_t2300079615_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnDimensionsChanged_t3101921181();
extern const Il2CppType OnDimensionsChanged_t3101921181_0_0_0;
extern Il2CppInteropData g_Il2CppInteropData[232] = 
{
	{ NULL, NULL, NULL, NULL, NULL, &IStream_t2767586507::IID, &IStream_t2767586507_0_0_0 } /* System.Runtime.InteropServices.ComTypes.IStream */,
	{ NULL, Context_t1744531130_marshal_pinvoke, Context_t1744531130_marshal_pinvoke_back, Context_t1744531130_marshal_pinvoke_cleanup, NULL, NULL, &Context_t1744531130_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Context */,
	{ NULL, Escape_t3294788190_marshal_pinvoke, Escape_t3294788190_marshal_pinvoke_back, Escape_t3294788190_marshal_pinvoke_cleanup, NULL, NULL, &Escape_t3294788190_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Escape */,
	{ NULL, PreviousInfo_t2148130204_marshal_pinvoke, PreviousInfo_t2148130204_marshal_pinvoke_back, PreviousInfo_t2148130204_marshal_pinvoke_cleanup, NULL, NULL, &PreviousInfo_t2148130204_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/PreviousInfo */,
	{ NULL, UriScheme_t2867806342_marshal_pinvoke, UriScheme_t2867806342_marshal_pinvoke_back, UriScheme_t2867806342_marshal_pinvoke_cleanup, NULL, NULL, &UriScheme_t2867806342_0_0_0 } /* Mono.Security.Uri/UriScheme */,
	{ DelegatePInvokeWrapper_AppDomainInitializer_t682969308, NULL, NULL, NULL, NULL, NULL, &AppDomainInitializer_t682969308_0_0_0 } /* System.AppDomainInitializer */,
	{ DelegatePInvokeWrapper_Swapper_t2822380397, NULL, NULL, NULL, NULL, NULL, &Swapper_t2822380397_0_0_0 } /* System.Array/Swapper */,
	{ NULL, DictionaryEntry_t3123975638_marshal_pinvoke, DictionaryEntry_t3123975638_marshal_pinvoke_back, DictionaryEntry_t3123975638_marshal_pinvoke_cleanup, NULL, NULL, &DictionaryEntry_t3123975638_0_0_0 } /* System.Collections.DictionaryEntry */,
	{ NULL, Slot_t3975888750_marshal_pinvoke, Slot_t3975888750_marshal_pinvoke_back, Slot_t3975888750_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t3975888750_0_0_0 } /* System.Collections.Hashtable/Slot */,
	{ NULL, Slot_t384495010_marshal_pinvoke, Slot_t384495010_marshal_pinvoke_back, Slot_t384495010_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t384495010_0_0_0 } /* System.Collections.SortedList/Slot */,
	{ DelegatePInvokeWrapper_InternalCancelHandler_t872516951, NULL, NULL, NULL, NULL, NULL, &InternalCancelHandler_t872516951_0_0_0 } /* System.Console/InternalCancelHandler */,
	{ NULL, ConsoleKeyInfo_t1802691652_marshal_pinvoke, ConsoleKeyInfo_t1802691652_marshal_pinvoke_back, ConsoleKeyInfo_t1802691652_marshal_pinvoke_cleanup, NULL, NULL, &ConsoleKeyInfo_t1802691652_0_0_0 } /* System.ConsoleKeyInfo */,
	{ NULL, Enum_t4135868527_marshal_pinvoke, Enum_t4135868527_marshal_pinvoke_back, Enum_t4135868527_marshal_pinvoke_cleanup, NULL, NULL, &Enum_t4135868527_0_0_0 } /* System.Enum */,
	{ NULL, InputRecord_t2660212290_marshal_pinvoke, InputRecord_t2660212290_marshal_pinvoke_back, InputRecord_t2660212290_marshal_pinvoke_cleanup, NULL, NULL, &InputRecord_t2660212290_0_0_0 } /* System.InputRecord */,
	{ DelegatePInvokeWrapper_ReadDelegate_t714865915, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t714865915_0_0_0 } /* System.IO.FileStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t4270993571, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t4270993571_0_0_0 } /* System.IO.FileStream/WriteDelegate */,
	{ NULL, MonoIOStat_t592533987_marshal_pinvoke, MonoIOStat_t592533987_marshal_pinvoke_back, MonoIOStat_t592533987_marshal_pinvoke_cleanup, NULL, NULL, &MonoIOStat_t592533987_0_0_0 } /* System.IO.MonoIOStat */,
	{ NULL, MonoEnumInfo_t3694469084_marshal_pinvoke, MonoEnumInfo_t3694469084_marshal_pinvoke_back, MonoEnumInfo_t3694469084_marshal_pinvoke_cleanup, NULL, NULL, &MonoEnumInfo_t3694469084_0_0_0 } /* System.MonoEnumInfo */,
	{ NULL, CustomAttributeNamedArgument_t287865710_marshal_pinvoke, CustomAttributeNamedArgument_t287865710_marshal_pinvoke_back, CustomAttributeNamedArgument_t287865710_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeNamedArgument_t287865710_0_0_0 } /* System.Reflection.CustomAttributeNamedArgument */,
	{ NULL, CustomAttributeTypedArgument_t2723150157_marshal_pinvoke, CustomAttributeTypedArgument_t2723150157_marshal_pinvoke_back, CustomAttributeTypedArgument_t2723150157_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeTypedArgument_t2723150157_0_0_0 } /* System.Reflection.CustomAttributeTypedArgument */,
	{ NULL, ILTokenInfo_t2325775114_marshal_pinvoke, ILTokenInfo_t2325775114_marshal_pinvoke_back, ILTokenInfo_t2325775114_marshal_pinvoke_cleanup, NULL, NULL, &ILTokenInfo_t2325775114_0_0_0 } /* System.Reflection.Emit.ILTokenInfo */,
	{ NULL, MonoResource_t4103430009_marshal_pinvoke, MonoResource_t4103430009_marshal_pinvoke_back, MonoResource_t4103430009_marshal_pinvoke_cleanup, NULL, NULL, &MonoResource_t4103430009_0_0_0 } /* System.Reflection.Emit.MonoResource */,
	{ NULL, RefEmitPermissionSet_t484390987_marshal_pinvoke, RefEmitPermissionSet_t484390987_marshal_pinvoke_back, RefEmitPermissionSet_t484390987_marshal_pinvoke_cleanup, NULL, NULL, &RefEmitPermissionSet_t484390987_0_0_0 } /* System.Reflection.Emit.RefEmitPermissionSet */,
	{ NULL, InterfaceMapping_t1267958657_marshal_pinvoke, InterfaceMapping_t1267958657_marshal_pinvoke_back, InterfaceMapping_t1267958657_marshal_pinvoke_cleanup, NULL, NULL, &InterfaceMapping_t1267958657_0_0_0 } /* System.Reflection.InterfaceMapping */,
	{ NULL, MonoEventInfo_t346866618_marshal_pinvoke, MonoEventInfo_t346866618_marshal_pinvoke_back, MonoEventInfo_t346866618_marshal_pinvoke_cleanup, NULL, NULL, &MonoEventInfo_t346866618_0_0_0 } /* System.Reflection.MonoEventInfo */,
	{ NULL, MonoMethodInfo_t1248819020_marshal_pinvoke, MonoMethodInfo_t1248819020_marshal_pinvoke_back, MonoMethodInfo_t1248819020_marshal_pinvoke_cleanup, NULL, NULL, &MonoMethodInfo_t1248819020_0_0_0 } /* System.Reflection.MonoMethodInfo */,
	{ NULL, MonoPropertyInfo_t3087356066_marshal_pinvoke, MonoPropertyInfo_t3087356066_marshal_pinvoke_back, MonoPropertyInfo_t3087356066_marshal_pinvoke_cleanup, NULL, NULL, &MonoPropertyInfo_t3087356066_0_0_0 } /* System.Reflection.MonoPropertyInfo */,
	{ NULL, ParameterModifier_t1461694466_marshal_pinvoke, ParameterModifier_t1461694466_marshal_pinvoke_back, ParameterModifier_t1461694466_marshal_pinvoke_cleanup, NULL, NULL, &ParameterModifier_t1461694466_0_0_0 } /* System.Reflection.ParameterModifier */,
	{ NULL, ResourceCacheItem_t51292791_marshal_pinvoke, ResourceCacheItem_t51292791_marshal_pinvoke_back, ResourceCacheItem_t51292791_marshal_pinvoke_cleanup, NULL, NULL, &ResourceCacheItem_t51292791_0_0_0 } /* System.Resources.ResourceReader/ResourceCacheItem */,
	{ NULL, ResourceInfo_t2872965302_marshal_pinvoke, ResourceInfo_t2872965302_marshal_pinvoke_back, ResourceInfo_t2872965302_marshal_pinvoke_cleanup, NULL, NULL, &ResourceInfo_t2872965302_0_0_0 } /* System.Resources.ResourceReader/ResourceInfo */,
	{ NULL, STATSTG_t1180006254_marshal_pinvoke, STATSTG_t1180006254_marshal_pinvoke_back, STATSTG_t1180006254_marshal_pinvoke_cleanup, NULL, NULL, &STATSTG_t1180006254_0_0_0 } /* System.Runtime.InteropServices.ComTypes.STATSTG */,
	{ DelegatePInvokeWrapper_CrossContextDelegate_t387175271, NULL, NULL, NULL, NULL, NULL, &CrossContextDelegate_t387175271_0_0_0 } /* System.Runtime.Remoting.Contexts.CrossContextDelegate */,
	{ DelegatePInvokeWrapper_CallbackHandler_t3280319253, NULL, NULL, NULL, NULL, NULL, &CallbackHandler_t3280319253_0_0_0 } /* System.Runtime.Serialization.SerializationCallbacks/CallbackHandler */,
	{ NULL, SerializationEntry_t648286436_marshal_pinvoke, SerializationEntry_t648286436_marshal_pinvoke_back, SerializationEntry_t648286436_marshal_pinvoke_cleanup, NULL, NULL, &SerializationEntry_t648286436_0_0_0 } /* System.Runtime.Serialization.SerializationEntry */,
	{ NULL, StreamingContext_t3711869237_marshal_pinvoke, StreamingContext_t3711869237_marshal_pinvoke_back, StreamingContext_t3711869237_marshal_pinvoke_cleanup, NULL, NULL, &StreamingContext_t3711869237_0_0_0 } /* System.Runtime.Serialization.StreamingContext */,
	{ NULL, DSAParameters_t1885824122_marshal_pinvoke, DSAParameters_t1885824122_marshal_pinvoke_back, DSAParameters_t1885824122_marshal_pinvoke_cleanup, NULL, NULL, &DSAParameters_t1885824122_0_0_0 } /* System.Security.Cryptography.DSAParameters */,
	{ NULL, RSAParameters_t1728406613_marshal_pinvoke, RSAParameters_t1728406613_marshal_pinvoke_back, RSAParameters_t1728406613_marshal_pinvoke_cleanup, NULL, NULL, &RSAParameters_t1728406613_0_0_0 } /* System.Security.Cryptography.RSAParameters */,
	{ NULL, SNIP_t4156255223_marshal_pinvoke, SNIP_t4156255223_marshal_pinvoke_back, SNIP_t4156255223_marshal_pinvoke_cleanup, NULL, NULL, &SNIP_t4156255223_0_0_0 } /* System.Security.Permissions.StrongNameIdentityPermission/SNIP */,
	{ NULL, SecurityFrame_t1422462475_marshal_pinvoke, SecurityFrame_t1422462475_marshal_pinvoke_back, SecurityFrame_t1422462475_marshal_pinvoke_cleanup, NULL, NULL, &SecurityFrame_t1422462475_0_0_0 } /* System.Security.SecurityFrame */,
	{ DelegatePInvokeWrapper_ThreadStart_t1006689297, NULL, NULL, NULL, NULL, NULL, &ThreadStart_t1006689297_0_0_0 } /* System.Threading.ThreadStart */,
	{ NULL, ValueType_t3640485471_marshal_pinvoke, ValueType_t3640485471_marshal_pinvoke_back, ValueType_t3640485471_marshal_pinvoke_cleanup, NULL, NULL, &ValueType_t3640485471_0_0_0 } /* System.ValueType */,
	{ NULL, CRYPTPROTECT_PROMPTSTRUCT_t1592014442_marshal_pinvoke, CRYPTPROTECT_PROMPTSTRUCT_t1592014442_marshal_pinvoke_back, CRYPTPROTECT_PROMPTSTRUCT_t1592014442_marshal_pinvoke_cleanup, NULL, NULL, &CRYPTPROTECT_PROMPTSTRUCT_t1592014442_0_0_0 } /* Mono.Security.Cryptography.NativeDapiProtection/CRYPTPROTECT_PROMPTSTRUCT */,
	{ NULL, X509IssuerSerial_t3270105241_marshal_pinvoke, X509IssuerSerial_t3270105241_marshal_pinvoke_back, X509IssuerSerial_t3270105241_marshal_pinvoke_cleanup, NULL, NULL, &X509IssuerSerial_t3270105241_0_0_0 } /* System.Security.Cryptography.Xml.X509IssuerSerial */,
	{ NULL, DTMXPathLinkedNode2_t3353097823_marshal_pinvoke, DTMXPathLinkedNode2_t3353097823_marshal_pinvoke_back, DTMXPathLinkedNode2_t3353097823_marshal_pinvoke_cleanup, NULL, NULL, &DTMXPathLinkedNode2_t3353097823_0_0_0 } /* Mono.Xml.XPath.DTMXPathLinkedNode2 */,
	{ NULL, Attribute_t270895445_marshal_pinvoke, Attribute_t270895445_marshal_pinvoke_back, Attribute_t270895445_marshal_pinvoke_cleanup, NULL, NULL, &Attribute_t270895445_0_0_0 } /* Mono.Xml.Xsl.Attribute */,
	{ NULL, TagName_t2891256255_marshal_pinvoke, TagName_t2891256255_marshal_pinvoke_back, TagName_t2891256255_marshal_pinvoke_cleanup, NULL, NULL, &TagName_t2891256255_0_0_0 } /* Mono.Xml2.XmlTextReader/TagName */,
	{ NULL, QNameValueType_t615604793_marshal_pinvoke, QNameValueType_t615604793_marshal_pinvoke_back, QNameValueType_t615604793_marshal_pinvoke_cleanup, NULL, NULL, &QNameValueType_t615604793_0_0_0 } /* System.Xml.Schema.QNameValueType */,
	{ NULL, StringArrayValueType_t3147326440_marshal_pinvoke, StringArrayValueType_t3147326440_marshal_pinvoke_back, StringArrayValueType_t3147326440_marshal_pinvoke_cleanup, NULL, NULL, &StringArrayValueType_t3147326440_0_0_0 } /* System.Xml.Schema.StringArrayValueType */,
	{ NULL, StringValueType_t261329552_marshal_pinvoke, StringValueType_t261329552_marshal_pinvoke_back, StringValueType_t261329552_marshal_pinvoke_cleanup, NULL, NULL, &StringValueType_t261329552_0_0_0 } /* System.Xml.Schema.StringValueType */,
	{ NULL, UriValueType_t2866347695_marshal_pinvoke, UriValueType_t2866347695_marshal_pinvoke_back, UriValueType_t2866347695_marshal_pinvoke_cleanup, NULL, NULL, &UriValueType_t2866347695_0_0_0 } /* System.Xml.Schema.UriValueType */,
	{ NULL, NsDecl_t3938094415_marshal_pinvoke, NsDecl_t3938094415_marshal_pinvoke_back, NsDecl_t3938094415_marshal_pinvoke_cleanup, NULL, NULL, &NsDecl_t3938094415_0_0_0 } /* System.Xml.XmlNamespaceManager/NsDecl */,
	{ NULL, NsScope_t3958624705_marshal_pinvoke, NsScope_t3958624705_marshal_pinvoke_back, NsScope_t3958624705_marshal_pinvoke_cleanup, NULL, NULL, &NsScope_t3958624705_0_0_0 } /* System.Xml.XmlNamespaceManager/NsScope */,
	{ DelegatePInvokeWrapper_CharGetter_t1703763694, NULL, NULL, NULL, NULL, NULL, &CharGetter_t1703763694_0_0_0 } /* System.Xml.XmlReaderBinarySupport/CharGetter */,
	{ NULL, NodeEnumerator_t2183475207_marshal_pinvoke, NodeEnumerator_t2183475207_marshal_pinvoke_back, NodeEnumerator_t2183475207_marshal_pinvoke_cleanup, NULL, NULL, &NodeEnumerator_t2183475207_0_0_0 } /* System.Collections.Generic.RBTree/NodeEnumerator */,
	{ DelegatePInvokeWrapper_AsyncReadHandler_t1188682440, NULL, NULL, NULL, NULL, NULL, &AsyncReadHandler_t1188682440_0_0_0 } /* System.Diagnostics.Process/AsyncReadHandler */,
	{ NULL, ProcessAsyncReader_t337580163_marshal_pinvoke, ProcessAsyncReader_t337580163_marshal_pinvoke_back, ProcessAsyncReader_t337580163_marshal_pinvoke_cleanup, NULL, NULL, &ProcessAsyncReader_t337580163_0_0_0 } /* System.Diagnostics.Process/ProcessAsyncReader */,
	{ NULL, ProcInfo_t2917059746_marshal_pinvoke, ProcInfo_t2917059746_marshal_pinvoke_back, ProcInfo_t2917059746_marshal_pinvoke_cleanup, NULL, NULL, &ProcInfo_t2917059746_0_0_0 } /* System.Diagnostics.Process/ProcInfo */,
	{ DelegatePInvokeWrapper_ReadMethod_t893206259, NULL, NULL, NULL, NULL, NULL, &ReadMethod_t893206259_0_0_0 } /* System.IO.Compression.DeflateStream/ReadMethod */,
	{ DelegatePInvokeWrapper_UnmanagedReadOrWrite_t876388624, NULL, NULL, NULL, NULL, NULL, &UnmanagedReadOrWrite_t876388624_0_0_0 } /* System.IO.Compression.DeflateStream/UnmanagedReadOrWrite */,
	{ DelegatePInvokeWrapper_WriteMethod_t2538911768, NULL, NULL, NULL, NULL, NULL, &WriteMethod_t2538911768_0_0_0 } /* System.IO.Compression.DeflateStream/WriteMethod */,
	{ NULL, InotifyEvent_t2637353984_marshal_pinvoke, InotifyEvent_t2637353984_marshal_pinvoke_back, InotifyEvent_t2637353984_marshal_pinvoke_cleanup, NULL, NULL, &InotifyEvent_t2637353984_0_0_0 } /* System.IO.InotifyEvent */,
	{ DelegatePInvokeWrapper_ReadDelegate_t2469437439, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t2469437439_0_0_0 } /* System.IO.MonoSyncFileStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t1613340939, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t1613340939_0_0_0 } /* System.IO.MonoSyncFileStream/WriteDelegate */,
	{ NULL, WaitForChangedResult_t3377826936_marshal_pinvoke, WaitForChangedResult_t3377826936_marshal_pinvoke_back, WaitForChangedResult_t3377826936_marshal_pinvoke_cleanup, NULL, NULL, &WaitForChangedResult_t3377826936_0_0_0 } /* System.IO.WaitForChangedResult */,
	{ DelegatePInvokeWrapper_ReadDelegate_t4266946825, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t4266946825_0_0_0 } /* System.Net.FtpDataStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t2016697242, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t2016697242_0_0_0 } /* System.Net.FtpDataStream/WriteDelegate */,
	{ DelegatePInvokeWrapper_SocketAsyncCall_t1521370843, NULL, NULL, NULL, NULL, NULL, &SocketAsyncCall_t1521370843_0_0_0 } /* System.Net.Sockets.Socket/SocketAsyncCall */,
	{ NULL, SocketAsyncResult_t2080034863_marshal_pinvoke, SocketAsyncResult_t2080034863_marshal_pinvoke_back, SocketAsyncResult_t2080034863_marshal_pinvoke_cleanup, NULL, NULL, &SocketAsyncResult_t2080034863_0_0_0 } /* System.Net.Sockets.Socket/SocketAsyncResult */,
	{ NULL, X509ChainStatus_t133602714_marshal_pinvoke, X509ChainStatus_t133602714_marshal_pinvoke_back, X509ChainStatus_t133602714_marshal_pinvoke_cleanup, NULL, NULL, &X509ChainStatus_t133602714_0_0_0 } /* System.Security.Cryptography.X509Certificates.X509ChainStatus */,
	{ NULL, IntStack_t2189327687_marshal_pinvoke, IntStack_t2189327687_marshal_pinvoke_back, IntStack_t2189327687_marshal_pinvoke_cleanup, NULL, NULL, &IntStack_t2189327687_0_0_0 } /* System.Text.RegularExpressions.Interpreter/IntStack */,
	{ NULL, Interval_t1802865632_marshal_pinvoke, Interval_t1802865632_marshal_pinvoke_back, Interval_t1802865632_marshal_pinvoke_cleanup, NULL, NULL, &Interval_t1802865632_0_0_0 } /* System.Text.RegularExpressions.Interval */,
	{ DelegatePInvokeWrapper_CostDelegate_t1722821004, NULL, NULL, NULL, NULL, NULL, &CostDelegate_t1722821004_0_0_0 } /* System.Text.RegularExpressions.IntervalCollection/CostDelegate */,
	{ NULL, IntStack_t561212167_marshal_pinvoke, IntStack_t561212167_marshal_pinvoke_back, IntStack_t561212167_marshal_pinvoke_cleanup, NULL, NULL, &IntStack_t561212167_0_0_0 } /* System.Text.RegularExpressions.RxInterpreter/IntStack */,
	{ NULL, UriScheme_t722425697_marshal_pinvoke, UriScheme_t722425697_marshal_pinvoke_back, UriScheme_t722425697_marshal_pinvoke_cleanup, NULL, NULL, &UriScheme_t722425697_0_0_0 } /* System.Uri/UriScheme */,
	{ DelegatePInvokeWrapper_SignalHandler_t1590986791, NULL, NULL, NULL, NULL, NULL, &SignalHandler_t1590986791_0_0_0 } /* Mono.Unix.Native.SignalHandler */,
	{ DelegatePInvokeWrapper_Action_t1264377477, NULL, NULL, NULL, NULL, NULL, &Action_t1264377477_0_0_0 } /* System.Action */,
	{ NULL, SecurityAttributesHack_t2445122734_marshal_pinvoke, SecurityAttributesHack_t2445122734_marshal_pinvoke_back, SecurityAttributesHack_t2445122734_marshal_pinvoke_cleanup, NULL, NULL, &SecurityAttributesHack_t2445122734_0_0_0 } /* System.IO.Pipes.SecurityAttributesHack */,
	{ NULL, Color_t1869934208_marshal_pinvoke, Color_t1869934208_marshal_pinvoke_back, Color_t1869934208_marshal_pinvoke_cleanup, NULL, NULL, &Color_t1869934208_0_0_0 } /* System.Drawing.Color */,
	{ NULL, NULL, NULL, NULL, CreateComCallableWrapperFor_ComIStreamWrapper_t1385077908, NULL, &ComIStreamWrapper_t1385077908_0_0_0 } /* System.Drawing.ComIStreamWrapper */,
	{ DelegatePInvokeWrapper_StreamCloseDelegate_t2004792630, NULL, NULL, NULL, NULL, NULL, &StreamCloseDelegate_t2004792630_0_0_0 } /* System.Drawing.GDIPlus/StreamCloseDelegate */,
	{ DelegatePInvokeWrapper_StreamGetBytesDelegate_t482941147, NULL, NULL, NULL, NULL, NULL, &StreamGetBytesDelegate_t482941147_0_0_0 } /* System.Drawing.GDIPlus/StreamGetBytesDelegate */,
	{ DelegatePInvokeWrapper_StreamGetHeaderDelegate_t558280091, NULL, NULL, NULL, NULL, NULL, &StreamGetHeaderDelegate_t558280091_0_0_0 } /* System.Drawing.GDIPlus/StreamGetHeaderDelegate */,
	{ DelegatePInvokeWrapper_StreamPutBytesDelegate_t225654305, NULL, NULL, NULL, NULL, NULL, &StreamPutBytesDelegate_t225654305_0_0_0 } /* System.Drawing.GDIPlus/StreamPutBytesDelegate */,
	{ DelegatePInvokeWrapper_StreamSeekDelegate_t2972861213, NULL, NULL, NULL, NULL, NULL, &StreamSeekDelegate_t2972861213_0_0_0 } /* System.Drawing.GDIPlus/StreamSeekDelegate */,
	{ DelegatePInvokeWrapper_StreamSizeDelegate_t1636074383, NULL, NULL, NULL, NULL, NULL, &StreamSizeDelegate_t1636074383_0_0_0 } /* System.Drawing.GDIPlus/StreamSizeDelegate */,
	{ NULL, IconDir_t4069448150_marshal_pinvoke, IconDir_t4069448150_marshal_pinvoke_back, IconDir_t4069448150_marshal_pinvoke_cleanup, NULL, NULL, &IconDir_t4069448150_0_0_0 } /* System.Drawing.Icon/IconDir */,
	{ NULL, IconImage_t1835934191_marshal_pinvoke, IconImage_t1835934191_marshal_pinvoke_back, IconImage_t1835934191_marshal_pinvoke_cleanup, NULL, NULL, &IconImage_t1835934191_0_0_0 } /* System.Drawing.Icon/IconImage */,
	{ NULL, BitmapData_t288533671_marshal_pinvoke, BitmapData_t288533671_marshal_pinvoke_back, BitmapData_t288533671_marshal_pinvoke_cleanup, NULL, NULL, &BitmapData_t288533671_0_0_0 } /* System.Drawing.Imaging.BitmapData */,
	{ NULL, EncoderParameter_t138078747_marshal_pinvoke, EncoderParameter_t138078747_marshal_pinvoke_back, EncoderParameter_t138078747_marshal_pinvoke_cleanup, NULL, NULL, &EncoderParameter_t138078747_0_0_0 } /* System.Drawing.Imaging.EncoderParameter */,
	{ NULL, EnumMemberInfo_t3072296154_marshal_pinvoke, EnumMemberInfo_t3072296154_marshal_pinvoke_back, EnumMemberInfo_t3072296154_marshal_pinvoke_cleanup, NULL, NULL, &EnumMemberInfo_t3072296154_0_0_0 } /* System.Runtime.Serialization.EnumMemberInfo */,
	{ DelegatePInvokeWrapper_CheckBlockEnd_t1373994268, NULL, NULL, NULL, NULL, NULL, &CheckBlockEnd_t1373994268_0_0_0 } /* System.Web.Compilation.AspGenerator/CheckBlockEnd */,
	{ NULL, CodeUnit_t1519973372_marshal_pinvoke, CodeUnit_t1519973372_marshal_pinvoke_back, CodeUnit_t1519973372_marshal_pinvoke_cleanup, NULL, NULL, &CodeUnit_t1519973372_0_0_0 } /* System.Web.Compilation.AssemblyBuilder/CodeUnit */,
	{ DelegatePInvokeWrapper_ParsingCompleteHandler_t1375501938, NULL, NULL, NULL, NULL, NULL, &ParsingCompleteHandler_t1375501938_0_0_0 } /* System.Web.Compilation.ParsingCompleteHandler */,
	{ DelegatePInvokeWrapper_NoParamsDelegate_t1618427640, NULL, NULL, NULL, NULL, NULL, &NoParamsDelegate_t1618427640_0_0_0 } /* System.Web.NoParamsDelegate */,
	{ NULL, AddedAttr_t2359971688_marshal_pinvoke, AddedAttr_t2359971688_marshal_pinvoke_back, AddedAttr_t2359971688_marshal_pinvoke_cleanup, NULL, NULL, &AddedAttr_t2359971688_0_0_0 } /* System.Web.UI.HtmlTextWriter/AddedAttr */,
	{ NULL, AddedStyle_t611321135_marshal_pinvoke, AddedStyle_t611321135_marshal_pinvoke_back, AddedStyle_t611321135_marshal_pinvoke_cleanup, NULL, NULL, &AddedStyle_t611321135_0_0_0 } /* System.Web.UI.HtmlTextWriter/AddedStyle */,
	{ NULL, AddedTag_t1198678936_marshal_pinvoke, AddedTag_t1198678936_marshal_pinvoke_back, AddedTag_t1198678936_marshal_pinvoke_cleanup, NULL, NULL, &AddedTag_t1198678936_0_0_0 } /* System.Web.UI.HtmlTextWriter/AddedTag */,
	{ NULL, FontUnit_t3688074528_marshal_pinvoke, FontUnit_t3688074528_marshal_pinvoke_back, FontUnit_t3688074528_marshal_pinvoke_cleanup, NULL, NULL, &FontUnit_t3688074528_0_0_0 } /* System.Web.UI.WebControls.FontUnit */,
	{ NULL, Unit_t3186727900_marshal_pinvoke, Unit_t3186727900_marshal_pinvoke_back, Unit_t3186727900_marshal_pinvoke_cleanup, NULL, NULL, &Unit_t3186727900_0_0_0 } /* System.Web.UI.WebControls.Unit */,
	{ DelegatePInvokeWrapper_AndroidJavaRunnable_t377890659, NULL, NULL, NULL, NULL, NULL, &AndroidJavaRunnable_t377890659_0_0_0 } /* UnityEngine.AndroidJavaRunnable */,
	{ NULL, AnimationCurve_t3046754366_marshal_pinvoke, AnimationCurve_t3046754366_marshal_pinvoke_back, AnimationCurve_t3046754366_marshal_pinvoke_cleanup, NULL, NULL, &AnimationCurve_t3046754366_0_0_0 } /* UnityEngine.AnimationCurve */,
	{ NULL, AnimationEvent_t1536042487_marshal_pinvoke, AnimationEvent_t1536042487_marshal_pinvoke_back, AnimationEvent_t1536042487_marshal_pinvoke_cleanup, NULL, NULL, &AnimationEvent_t1536042487_0_0_0 } /* UnityEngine.AnimationEvent */,
	{ NULL, AnimatorTransitionInfo_t2534804151_marshal_pinvoke, AnimatorTransitionInfo_t2534804151_marshal_pinvoke_back, AnimatorTransitionInfo_t2534804151_marshal_pinvoke_cleanup, NULL, NULL, &AnimatorTransitionInfo_t2534804151_0_0_0 } /* UnityEngine.AnimatorTransitionInfo */,
	{ DelegatePInvokeWrapper_LogCallback_t3588208630, NULL, NULL, NULL, NULL, NULL, &LogCallback_t3588208630_0_0_0 } /* UnityEngine.Application/LogCallback */,
	{ DelegatePInvokeWrapper_LowMemoryCallback_t4104246196, NULL, NULL, NULL, NULL, NULL, &LowMemoryCallback_t4104246196_0_0_0 } /* UnityEngine.Application/LowMemoryCallback */,
	{ NULL, AssetBundleRequest_t699759206_marshal_pinvoke, AssetBundleRequest_t699759206_marshal_pinvoke_back, AssetBundleRequest_t699759206_marshal_pinvoke_cleanup, NULL, NULL, &AssetBundleRequest_t699759206_0_0_0 } /* UnityEngine.AssetBundleRequest */,
	{ NULL, AsyncOperation_t1445031843_marshal_pinvoke, AsyncOperation_t1445031843_marshal_pinvoke_back, AsyncOperation_t1445031843_marshal_pinvoke_cleanup, NULL, NULL, &AsyncOperation_t1445031843_0_0_0 } /* UnityEngine.AsyncOperation */,
	{ DelegatePInvokeWrapper_PCMReaderCallback_t1677636661, NULL, NULL, NULL, NULL, NULL, &PCMReaderCallback_t1677636661_0_0_0 } /* UnityEngine.AudioClip/PCMReaderCallback */,
	{ DelegatePInvokeWrapper_PCMSetPositionCallback_t1059417452, NULL, NULL, NULL, NULL, NULL, &PCMSetPositionCallback_t1059417452_0_0_0 } /* UnityEngine.AudioClip/PCMSetPositionCallback */,
	{ DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t2089929874, NULL, NULL, NULL, NULL, NULL, &AudioConfigurationChangeHandler_t2089929874_0_0_0 } /* UnityEngine.AudioSettings/AudioConfigurationChangeHandler */,
	{ DelegatePInvokeWrapper_WillRenderCanvases_t3309123499, NULL, NULL, NULL, NULL, NULL, &WillRenderCanvases_t3309123499_0_0_0 } /* UnityEngine.Canvas/WillRenderCanvases */,
	{ NULL, CharacterInfo_t1228754872_marshal_pinvoke, CharacterInfo_t1228754872_marshal_pinvoke_back, CharacterInfo_t1228754872_marshal_pinvoke_cleanup, NULL, NULL, &CharacterInfo_t1228754872_0_0_0 } /* UnityEngine.CharacterInfo */,
	{ NULL, Collision_t4262080450_marshal_pinvoke, Collision_t4262080450_marshal_pinvoke_back, Collision_t4262080450_marshal_pinvoke_cleanup, NULL, NULL, &Collision_t4262080450_0_0_0 } /* UnityEngine.Collision */,
	{ NULL, Collision2D_t2842956331_marshal_pinvoke, Collision2D_t2842956331_marshal_pinvoke_back, Collision2D_t2842956331_marshal_pinvoke_cleanup, NULL, NULL, &Collision2D_t2842956331_0_0_0 } /* UnityEngine.Collision2D */,
	{ NULL, ContactFilter2D_t3805203441_marshal_pinvoke, ContactFilter2D_t3805203441_marshal_pinvoke_back, ContactFilter2D_t3805203441_marshal_pinvoke_cleanup, NULL, NULL, &ContactFilter2D_t3805203441_0_0_0 } /* UnityEngine.ContactFilter2D */,
	{ NULL, ControllerColliderHit_t240592346_marshal_pinvoke, ControllerColliderHit_t240592346_marshal_pinvoke_back, ControllerColliderHit_t240592346_marshal_pinvoke_cleanup, NULL, NULL, &ControllerColliderHit_t240592346_0_0_0 } /* UnityEngine.ControllerColliderHit */,
	{ NULL, Coroutine_t3829159415_marshal_pinvoke, Coroutine_t3829159415_marshal_pinvoke_back, Coroutine_t3829159415_marshal_pinvoke_cleanup, NULL, NULL, &Coroutine_t3829159415_0_0_0 } /* UnityEngine.Coroutine */,
	{ NULL, CullingGroup_t2096318768_marshal_pinvoke, CullingGroup_t2096318768_marshal_pinvoke_back, CullingGroup_t2096318768_marshal_pinvoke_cleanup, NULL, NULL, &CullingGroup_t2096318768_0_0_0 } /* UnityEngine.CullingGroup */,
	{ DelegatePInvokeWrapper_StateChanged_t2136737110, NULL, NULL, NULL, NULL, NULL, &StateChanged_t2136737110_0_0_0 } /* UnityEngine.CullingGroup/StateChanged */,
	{ DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t51287044, NULL, NULL, NULL, NULL, NULL, &DisplaysUpdatedDelegate_t51287044_0_0_0 } /* UnityEngine.Display/DisplaysUpdatedDelegate */,
	{ NULL, Event_t2956885303_marshal_pinvoke, Event_t2956885303_marshal_pinvoke_back, Event_t2956885303_marshal_pinvoke_cleanup, NULL, NULL, &Event_t2956885303_0_0_0 } /* UnityEngine.Event */,
	{ DelegatePInvokeWrapper_UnityAction_t3245792599, NULL, NULL, NULL, NULL, NULL, &UnityAction_t3245792599_0_0_0 } /* UnityEngine.Events.UnityAction */,
	{ DelegatePInvokeWrapper_FontTextureRebuildCallback_t2467502454, NULL, NULL, NULL, NULL, NULL, &FontTextureRebuildCallback_t2467502454_0_0_0 } /* UnityEngine.Font/FontTextureRebuildCallback */,
	{ NULL, Gradient_t3067099924_marshal_pinvoke, Gradient_t3067099924_marshal_pinvoke_back, Gradient_t3067099924_marshal_pinvoke_cleanup, NULL, NULL, &Gradient_t3067099924_0_0_0 } /* UnityEngine.Gradient */,
	{ DelegatePInvokeWrapper_WindowFunction_t3146511083, NULL, NULL, NULL, NULL, NULL, &WindowFunction_t3146511083_0_0_0 } /* UnityEngine.GUI/WindowFunction */,
	{ NULL, GUIContent_t3050628031_marshal_pinvoke, GUIContent_t3050628031_marshal_pinvoke_back, GUIContent_t3050628031_marshal_pinvoke_cleanup, NULL, NULL, &GUIContent_t3050628031_0_0_0 } /* UnityEngine.GUIContent */,
	{ DelegatePInvokeWrapper_SkinChangedDelegate_t1143955295, NULL, NULL, NULL, NULL, NULL, &SkinChangedDelegate_t1143955295_0_0_0 } /* UnityEngine.GUISkin/SkinChangedDelegate */,
	{ NULL, GUIStyle_t3956901511_marshal_pinvoke, GUIStyle_t3956901511_marshal_pinvoke_back, GUIStyle_t3956901511_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyle_t3956901511_0_0_0 } /* UnityEngine.GUIStyle */,
	{ NULL, GUIStyleState_t1397964415_marshal_pinvoke, GUIStyleState_t1397964415_marshal_pinvoke_back, GUIStyleState_t1397964415_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyleState_t1397964415_0_0_0 } /* UnityEngine.GUIStyleState */,
	{ NULL, HumanBone_t2465339518_marshal_pinvoke, HumanBone_t2465339518_marshal_pinvoke_back, HumanBone_t2465339518_marshal_pinvoke_cleanup, NULL, NULL, &HumanBone_t2465339518_0_0_0 } /* UnityEngine.HumanBone */,
	{ NULL, Internal_DrawTextureArguments_t1705718261_marshal_pinvoke, Internal_DrawTextureArguments_t1705718261_marshal_pinvoke_back, Internal_DrawTextureArguments_t1705718261_marshal_pinvoke_cleanup, NULL, NULL, &Internal_DrawTextureArguments_t1705718261_0_0_0 } /* UnityEngine.Internal_DrawTextureArguments */,
	{ NULL, jvalue_t1372148875_marshal_pinvoke, jvalue_t1372148875_marshal_pinvoke_back, jvalue_t1372148875_marshal_pinvoke_cleanup, NULL, NULL, &jvalue_t1372148875_0_0_0 } /* UnityEngine.jvalue */,
	{ NULL, DownloadHandler_t2937767557_marshal_pinvoke, DownloadHandler_t2937767557_marshal_pinvoke_back, DownloadHandler_t2937767557_marshal_pinvoke_cleanup, NULL, NULL, &DownloadHandler_t2937767557_0_0_0 } /* UnityEngine.Networking.DownloadHandler */,
	{ NULL, DownloadHandlerBuffer_t2928496527_marshal_pinvoke, DownloadHandlerBuffer_t2928496527_marshal_pinvoke_back, DownloadHandlerBuffer_t2928496527_marshal_pinvoke_cleanup, NULL, NULL, &DownloadHandlerBuffer_t2928496527_0_0_0 } /* UnityEngine.Networking.DownloadHandlerBuffer */,
	{ NULL, UnityWebRequest_t463507806_marshal_pinvoke, UnityWebRequest_t463507806_marshal_pinvoke_back, UnityWebRequest_t463507806_marshal_pinvoke_cleanup, NULL, NULL, &UnityWebRequest_t463507806_0_0_0 } /* UnityEngine.Networking.UnityWebRequest */,
	{ NULL, UploadHandler_t2993558019_marshal_pinvoke, UploadHandler_t2993558019_marshal_pinvoke_back, UploadHandler_t2993558019_marshal_pinvoke_cleanup, NULL, NULL, &UploadHandler_t2993558019_0_0_0 } /* UnityEngine.Networking.UploadHandler */,
	{ NULL, Object_t631007953_marshal_pinvoke, Object_t631007953_marshal_pinvoke_back, Object_t631007953_marshal_pinvoke_cleanup, NULL, NULL, &Object_t631007953_0_0_0 } /* UnityEngine.Object */,
	{ NULL, RaycastHit_t1056001966_marshal_pinvoke, RaycastHit_t1056001966_marshal_pinvoke_back, RaycastHit_t1056001966_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit_t1056001966_0_0_0 } /* UnityEngine.RaycastHit */,
	{ NULL, RaycastHit2D_t2279581989_marshal_pinvoke, RaycastHit2D_t2279581989_marshal_pinvoke_back, RaycastHit2D_t2279581989_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit2D_t2279581989_0_0_0 } /* UnityEngine.RaycastHit2D */,
	{ NULL, RectOffset_t1369453676_marshal_pinvoke, RectOffset_t1369453676_marshal_pinvoke_back, RectOffset_t1369453676_marshal_pinvoke_cleanup, NULL, NULL, &RectOffset_t1369453676_0_0_0 } /* UnityEngine.RectOffset */,
	{ DelegatePInvokeWrapper_UpdatedEventHandler_t1027848393, NULL, NULL, NULL, NULL, NULL, &UpdatedEventHandler_t1027848393_0_0_0 } /* UnityEngine.RemoteSettings/UpdatedEventHandler */,
	{ NULL, ResourceRequest_t3109103591_marshal_pinvoke, ResourceRequest_t3109103591_marshal_pinvoke_back, ResourceRequest_t3109103591_marshal_pinvoke_cleanup, NULL, NULL, &ResourceRequest_t3109103591_0_0_0 } /* UnityEngine.ResourceRequest */,
	{ NULL, ScriptableObject_t2528358522_marshal_pinvoke, ScriptableObject_t2528358522_marshal_pinvoke_back, ScriptableObject_t2528358522_marshal_pinvoke_cleanup, NULL, NULL, &ScriptableObject_t2528358522_0_0_0 } /* UnityEngine.ScriptableObject */,
	{ NULL, HitInfo_t3229609740_marshal_pinvoke, HitInfo_t3229609740_marshal_pinvoke_back, HitInfo_t3229609740_marshal_pinvoke_cleanup, NULL, NULL, &HitInfo_t3229609740_0_0_0 } /* UnityEngine.SendMouseEvents/HitInfo */,
	{ NULL, SkeletonBone_t4134054672_marshal_pinvoke, SkeletonBone_t4134054672_marshal_pinvoke_back, SkeletonBone_t4134054672_marshal_pinvoke_cleanup, NULL, NULL, &SkeletonBone_t4134054672_0_0_0 } /* UnityEngine.SkeletonBone */,
	{ NULL, SliderHandler_t1154919399_marshal_pinvoke, SliderHandler_t1154919399_marshal_pinvoke_back, SliderHandler_t1154919399_marshal_pinvoke_cleanup, NULL, NULL, &SliderHandler_t1154919399_0_0_0 } /* UnityEngine.SliderHandler */,
	{ NULL, GcAchievementData_t675222246_marshal_pinvoke, GcAchievementData_t675222246_marshal_pinvoke_back, GcAchievementData_t675222246_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementData_t675222246_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementData */,
	{ NULL, GcAchievementDescriptionData_t643925653_marshal_pinvoke, GcAchievementDescriptionData_t643925653_marshal_pinvoke_back, GcAchievementDescriptionData_t643925653_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementDescriptionData_t643925653_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData */,
	{ NULL, GcLeaderboard_t4132273028_marshal_pinvoke, GcLeaderboard_t4132273028_marshal_pinvoke_back, GcLeaderboard_t4132273028_marshal_pinvoke_cleanup, NULL, NULL, &GcLeaderboard_t4132273028_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard */,
	{ NULL, GcScoreData_t2125309831_marshal_pinvoke, GcScoreData_t2125309831_marshal_pinvoke_back, GcScoreData_t2125309831_marshal_pinvoke_cleanup, NULL, NULL, &GcScoreData_t2125309831_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcScoreData */,
	{ NULL, GcUserProfileData_t2719720026_marshal_pinvoke, GcUserProfileData_t2719720026_marshal_pinvoke_back, GcUserProfileData_t2719720026_marshal_pinvoke_cleanup, NULL, NULL, &GcUserProfileData_t2719720026_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData */,
	{ NULL, TextGenerationSettings_t1351628751_marshal_pinvoke, TextGenerationSettings_t1351628751_marshal_pinvoke_back, TextGenerationSettings_t1351628751_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerationSettings_t1351628751_0_0_0 } /* UnityEngine.TextGenerationSettings */,
	{ NULL, TextGenerator_t3211863866_marshal_pinvoke, TextGenerator_t3211863866_marshal_pinvoke_back, TextGenerator_t3211863866_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerator_t3211863866_0_0_0 } /* UnityEngine.TextGenerator */,
	{ NULL, TrackedReference_t1199777556_marshal_pinvoke, TrackedReference_t1199777556_marshal_pinvoke_back, TrackedReference_t1199777556_marshal_pinvoke_cleanup, NULL, NULL, &TrackedReference_t1199777556_0_0_0 } /* UnityEngine.TrackedReference */,
	{ NULL, WaitForSeconds_t1699091251_marshal_pinvoke, WaitForSeconds_t1699091251_marshal_pinvoke_back, WaitForSeconds_t1699091251_marshal_pinvoke_cleanup, NULL, NULL, &WaitForSeconds_t1699091251_0_0_0 } /* UnityEngine.WaitForSeconds */,
	{ NULL, WebCamDevice_t1322781432_marshal_pinvoke, WebCamDevice_t1322781432_marshal_pinvoke_back, WebCamDevice_t1322781432_marshal_pinvoke_cleanup, NULL, NULL, &WebCamDevice_t1322781432_0_0_0 } /* UnityEngine.WebCamDevice */,
	{ NULL, YieldInstruction_t403091072_marshal_pinvoke, YieldInstruction_t403091072_marshal_pinvoke_back, YieldInstruction_t403091072_marshal_pinvoke_cleanup, NULL, NULL, &YieldInstruction_t403091072_0_0_0 } /* UnityEngine.YieldInstruction */,
	{ NULL, CameraField_t1483002240_marshal_pinvoke, CameraField_t1483002240_marshal_pinvoke_back, CameraField_t1483002240_marshal_pinvoke_cleanup, NULL, NULL, &CameraField_t1483002240_0_0_0 } /* Vuforia.CameraDevice/CameraField */,
	{ NULL, CameraFieldData_t409245341_marshal_pinvoke, CameraFieldData_t409245341_marshal_pinvoke_back, CameraFieldData_t409245341_marshal_pinvoke_cleanup, NULL, NULL, &CameraFieldData_t409245341_0_0_0 } /* Vuforia.CameraDeviceImpl/CameraFieldData */,
	{ NULL, EyewearCalibrationReading_t664929988_marshal_pinvoke, EyewearCalibrationReading_t664929988_marshal_pinvoke_back, EyewearCalibrationReading_t664929988_marshal_pinvoke_cleanup, NULL, NULL, &EyewearCalibrationReading_t664929988_0_0_0 } /* Vuforia.EyewearDevice/EyewearCalibrationReading */,
	{ NULL, TargetSearchResult_t3441982613_marshal_pinvoke, TargetSearchResult_t3441982613_marshal_pinvoke_back, TargetSearchResult_t3441982613_marshal_pinvoke_cleanup, NULL, NULL, &TargetSearchResult_t3441982613_0_0_0 } /* Vuforia.TargetFinder/TargetSearchResult */,
	{ NULL, AutoRotationState_t946804616_marshal_pinvoke, AutoRotationState_t946804616_marshal_pinvoke_back, AutoRotationState_t946804616_marshal_pinvoke_cleanup, NULL, NULL, &AutoRotationState_t946804616_0_0_0 } /* Vuforia.VuforiaManagerImpl/AutoRotationState */,
	{ NULL, ProfileCollection_t901995765_marshal_pinvoke, ProfileCollection_t901995765_marshal_pinvoke_back, ProfileCollection_t901995765_marshal_pinvoke_cleanup, NULL, NULL, &ProfileCollection_t901995765_0_0_0 } /* Vuforia.WebCamProfile/ProfileCollection */,
	{ DelegatePInvokeWrapper_DispatcherFactory_t1014155341, NULL, NULL, NULL, NULL, NULL, &DispatcherFactory_t1014155341_0_0_0 } /* Boo.Lang.Runtime.DynamicDispatching.DispatcherCache/DispatcherFactory */,
	{ NULL, DTMXPathLinkedNode2_t3353097824_marshal_pinvoke, DTMXPathLinkedNode2_t3353097824_marshal_pinvoke_back, DTMXPathLinkedNode2_t3353097824_marshal_pinvoke_cleanup, NULL, NULL, &DTMXPathLinkedNode2_t3353097824_0_0_0 } /* Mono.Xml.XPath.DTMXPathLinkedNode2 */,
	{ DelegatePInvokeWrapper_AsyncWaitForMessageHandler_t1048830322, NULL, NULL, NULL, NULL, NULL, &AsyncWaitForMessageHandler_t1048830322_0_0_0 } /* System.ServiceModel.Channels.DuplexChannelBase/AsyncWaitForMessageHandler */,
	{ DelegatePInvokeWrapper_AsyncHandler_t217889813, NULL, NULL, NULL, NULL, NULL, &AsyncHandler_t217889813_0_0_0 } /* System.ServiceModel.Channels.DuplexSessionBase/AsyncHandler */,
	{ NULL, RaycastResult_t3360306849_marshal_pinvoke, RaycastResult_t3360306849_marshal_pinvoke_back, RaycastResult_t3360306849_marshal_pinvoke_cleanup, NULL, NULL, &RaycastResult_t3360306849_0_0_0 } /* UnityEngine.EventSystems.RaycastResult */,
	{ NULL, ColorTween_t809614380_marshal_pinvoke, ColorTween_t809614380_marshal_pinvoke_back, ColorTween_t809614380_marshal_pinvoke_cleanup, NULL, NULL, &ColorTween_t809614380_0_0_0 } /* UnityEngine.UI.CoroutineTween.ColorTween */,
	{ NULL, FloatTween_t1274330004_marshal_pinvoke, FloatTween_t1274330004_marshal_pinvoke_back, FloatTween_t1274330004_marshal_pinvoke_cleanup, NULL, NULL, &FloatTween_t1274330004_0_0_0 } /* UnityEngine.UI.CoroutineTween.FloatTween */,
	{ NULL, Resources_t1597885468_marshal_pinvoke, Resources_t1597885468_marshal_pinvoke_back, Resources_t1597885468_marshal_pinvoke_cleanup, NULL, NULL, &Resources_t1597885468_0_0_0 } /* UnityEngine.UI.DefaultControls/Resources */,
	{ DelegatePInvokeWrapper_OnValidateInput_t2355412304, NULL, NULL, NULL, NULL, NULL, &OnValidateInput_t2355412304_0_0_0 } /* UnityEngine.UI.InputField/OnValidateInput */,
	{ NULL, Navigation_t3049316579_marshal_pinvoke, Navigation_t3049316579_marshal_pinvoke_back, Navigation_t3049316579_marshal_pinvoke_cleanup, NULL, NULL, &Navigation_t3049316579_0_0_0 } /* UnityEngine.UI.Navigation */,
	{ NULL, SpriteState_t1362986479_marshal_pinvoke, SpriteState_t1362986479_marshal_pinvoke_back, SpriteState_t1362986479_marshal_pinvoke_cleanup, NULL, NULL, &SpriteState_t1362986479_0_0_0 } /* UnityEngine.UI.SpriteState */,
	{ NULL, Data_t1588725102_marshal_pinvoke, Data_t1588725102_marshal_pinvoke_back, Data_t1588725102_marshal_pinvoke_cleanup, NULL, NULL, &Data_t1588725102_0_0_0 } /* HighlightingSystem.Highlighter/RendererCache/Data */,
	{ DelegatePInvokeWrapper_ApplyTween_t3327999347, NULL, NULL, NULL, NULL, NULL, &ApplyTween_t3327999347_0_0_0 } /* iTween/ApplyTween */,
	{ DelegatePInvokeWrapper_EasingFunction_t2767217938, NULL, NULL, NULL, NULL, NULL, &EasingFunction_t2767217938_0_0_0 } /* iTween/EasingFunction */,
	{ NULL, PropertyObserver_t4012421388_marshal_pinvoke, PropertyObserver_t4012421388_marshal_pinvoke_back, PropertyObserver_t4012421388_marshal_pinvoke_cleanup, NULL, NULL, &PropertyObserver_t4012421388_0_0_0 } /* UnityStandardAssets.CinematicEffects.AmbientOcclusion/PropertyObserver */,
	{ NULL, Settings_t1032325577_marshal_pinvoke, Settings_t1032325577_marshal_pinvoke_back, Settings_t1032325577_marshal_pinvoke_cleanup, NULL, NULL, &Settings_t1032325577_0_0_0 } /* UnityStandardAssets.CinematicEffects.Bloom/Settings */,
	{ NULL, BokehTextureSettings_t3943002634_marshal_pinvoke, BokehTextureSettings_t3943002634_marshal_pinvoke_back, BokehTextureSettings_t3943002634_marshal_pinvoke_cleanup, NULL, NULL, &BokehTextureSettings_t3943002634_0_0_0 } /* UnityStandardAssets.CinematicEffects.DepthOfField/BokehTextureSettings */,
	{ NULL, FocusSettings_t1261455774_marshal_pinvoke, FocusSettings_t1261455774_marshal_pinvoke_back, FocusSettings_t1261455774_marshal_pinvoke_cleanup, NULL, NULL, &FocusSettings_t1261455774_0_0_0 } /* UnityStandardAssets.CinematicEffects.DepthOfField/FocusSettings */,
	{ NULL, GlobalSettings_t956974295_marshal_pinvoke, GlobalSettings_t956974295_marshal_pinvoke_back, GlobalSettings_t956974295_marshal_pinvoke_cleanup, NULL, NULL, &GlobalSettings_t956974295_0_0_0 } /* UnityStandardAssets.CinematicEffects.DepthOfField/GlobalSettings */,
	{ NULL, QualitySettings_t1379802392_marshal_pinvoke, QualitySettings_t1379802392_marshal_pinvoke_back, QualitySettings_t1379802392_marshal_pinvoke_cleanup, NULL, NULL, &QualitySettings_t1379802392_0_0_0 } /* UnityStandardAssets.CinematicEffects.DepthOfField/QualitySettings */,
	{ NULL, ChromaticAberrationSettings_t3807027201_marshal_pinvoke, ChromaticAberrationSettings_t3807027201_marshal_pinvoke_back, ChromaticAberrationSettings_t3807027201_marshal_pinvoke_cleanup, NULL, NULL, &ChromaticAberrationSettings_t3807027201_0_0_0 } /* UnityStandardAssets.CinematicEffects.LensAberrations/ChromaticAberrationSettings */,
	{ NULL, DistortionSettings_t3193728992_marshal_pinvoke, DistortionSettings_t3193728992_marshal_pinvoke_back, DistortionSettings_t3193728992_marshal_pinvoke_cleanup, NULL, NULL, &DistortionSettings_t3193728992_0_0_0 } /* UnityStandardAssets.CinematicEffects.LensAberrations/DistortionSettings */,
	{ NULL, VignetteSettings_t2342083791_marshal_pinvoke, VignetteSettings_t2342083791_marshal_pinvoke_back, VignetteSettings_t2342083791_marshal_pinvoke_cleanup, NULL, NULL, &VignetteSettings_t2342083791_0_0_0 } /* UnityStandardAssets.CinematicEffects.LensAberrations/VignetteSettings */,
	{ NULL, Frame_t3511111948_marshal_pinvoke, Frame_t3511111948_marshal_pinvoke_back, Frame_t3511111948_marshal_pinvoke_cleanup, NULL, NULL, &Frame_t3511111948_0_0_0 } /* UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter/Frame */,
	{ NULL, ReflectionSettings_t499321483_marshal_pinvoke, ReflectionSettings_t499321483_marshal_pinvoke_back, ReflectionSettings_t499321483_marshal_pinvoke_cleanup, NULL, NULL, &ReflectionSettings_t499321483_0_0_0 } /* UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/ReflectionSettings */,
	{ NULL, SSRSettings_t899296535_marshal_pinvoke, SSRSettings_t899296535_marshal_pinvoke_back, SSRSettings_t899296535_marshal_pinvoke_cleanup, NULL, NULL, &SSRSettings_t899296535_0_0_0 } /* UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/SSRSettings */,
	{ NULL, PredicationSettings_t4116788586_marshal_pinvoke, PredicationSettings_t4116788586_marshal_pinvoke_back, PredicationSettings_t4116788586_marshal_pinvoke_cleanup, NULL, NULL, &PredicationSettings_t4116788586_0_0_0 } /* UnityStandardAssets.CinematicEffects.SMAA/PredicationSettings */,
	{ NULL, QualitySettings_t1869387270_marshal_pinvoke, QualitySettings_t1869387270_marshal_pinvoke_back, QualitySettings_t1869387270_marshal_pinvoke_cleanup, NULL, NULL, &QualitySettings_t1869387270_0_0_0 } /* UnityStandardAssets.CinematicEffects.SMAA/QualitySettings */,
	{ NULL, TemporalSettings_t2985116592_marshal_pinvoke, TemporalSettings_t2985116592_marshal_pinvoke_back, TemporalSettings_t2985116592_marshal_pinvoke_cleanup, NULL, NULL, &TemporalSettings_t2985116592_0_0_0 } /* UnityStandardAssets.CinematicEffects.SMAA/TemporalSettings */,
	{ NULL, ChannelMixerSettings_t491321292_marshal_pinvoke, ChannelMixerSettings_t491321292_marshal_pinvoke_back, ChannelMixerSettings_t491321292_marshal_pinvoke_cleanup, NULL, NULL, &ChannelMixerSettings_t491321292_0_0_0 } /* UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ChannelMixerSettings */,
	{ NULL, ColorGradingSettings_t1923563914_marshal_pinvoke, ColorGradingSettings_t1923563914_marshal_pinvoke_back, ColorGradingSettings_t1923563914_marshal_pinvoke_cleanup, NULL, NULL, &ColorGradingSettings_t1923563914_0_0_0 } /* UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingSettings */,
	{ NULL, CurvesSettings_t2932745847_marshal_pinvoke, CurvesSettings_t2932745847_marshal_pinvoke_back, CurvesSettings_t2932745847_marshal_pinvoke_cleanup, NULL, NULL, &CurvesSettings_t2932745847_0_0_0 } /* UnityStandardAssets.CinematicEffects.TonemappingColorGrading/CurvesSettings */,
	{ NULL, EyeAdaptationSettings_t3234830401_marshal_pinvoke, EyeAdaptationSettings_t3234830401_marshal_pinvoke_back, EyeAdaptationSettings_t3234830401_marshal_pinvoke_cleanup, NULL, NULL, &EyeAdaptationSettings_t3234830401_0_0_0 } /* UnityStandardAssets.CinematicEffects.TonemappingColorGrading/EyeAdaptationSettings */,
	{ NULL, LUTSettings_t2339616782_marshal_pinvoke, LUTSettings_t2339616782_marshal_pinvoke_back, LUTSettings_t2339616782_marshal_pinvoke_cleanup, NULL, NULL, &LUTSettings_t2339616782_0_0_0 } /* UnityStandardAssets.CinematicEffects.TonemappingColorGrading/LUTSettings */,
	{ NULL, TonemappingSettings_t1161324624_marshal_pinvoke, TonemappingSettings_t1161324624_marshal_pinvoke_back, TonemappingSettings_t1161324624_marshal_pinvoke_cleanup, NULL, NULL, &TonemappingSettings_t1161324624_0_0_0 } /* UnityStandardAssets.CinematicEffects.TonemappingColorGrading/TonemappingSettings */,
	{ DelegatePInvokeWrapper_Callback_t3139336517, NULL, NULL, NULL, NULL, NULL, &Callback_t3139336517_0_0_0 } /* EventDelegate/Callback */,
	{ NULL, NormalList_t1320820545_marshal_pinvoke, NormalList_t1320820545_marshal_pinvoke_back, NormalList_t1320820545_marshal_pinvoke_cleanup, NULL, NULL, &NormalList_t1320820545_0_0_0 } /* ExpiredComponets/NormalList */,
	{ NULL, SureList_t2832800709_marshal_pinvoke, SureList_t2832800709_marshal_pinvoke_back, SureList_t2832800709_marshal_pinvoke_cleanup, NULL, NULL, &SureList_t2832800709_0_0_0 } /* ExpiredComponets/SureList */,
	{ NULL, ComponetFuncScript_t187458_marshal_pinvoke, ComponetFuncScript_t187458_marshal_pinvoke_back, ComponetFuncScript_t187458_marshal_pinvoke_cleanup, NULL, NULL, &ComponetFuncScript_t187458_0_0_0 } /* GetTickets/ComponetFuncScript */,
	{ NULL, PlayerSetting_t3195188508_marshal_pinvoke, PlayerSetting_t3195188508_marshal_pinvoke_back, PlayerSetting_t3195188508_marshal_pinvoke_cleanup, NULL, NULL, &PlayerSetting_t3195188508_0_0_0 } /* GetTickets/PlayerSetting */,
	{ DelegatePInvokeWrapper_LoadFunction_t2078002637, NULL, NULL, NULL, NULL, NULL, &LoadFunction_t2078002637_0_0_0 } /* Localization/LoadFunction */,
	{ DelegatePInvokeWrapper_OnLocalizeNotification_t3391620158, NULL, NULL, NULL, NULL, NULL, &OnLocalizeNotification_t3391620158_0_0_0 } /* Localization/OnLocalizeNotification */,
	{ NULL, Preset_t2972107632_marshal_pinvoke, Preset_t2972107632_marshal_pinvoke_back, Preset_t2972107632_marshal_pinvoke_cleanup, NULL, NULL, &Preset_t2972107632_0_0_0 } /* PresetSelector/Preset */,
	{ NULL, Imgs_t1080690280_marshal_pinvoke, Imgs_t1080690280_marshal_pinvoke_back, Imgs_t1080690280_marshal_pinvoke_cleanup, NULL, NULL, &Imgs_t1080690280_0_0_0 } /* QuestionedComponets/Imgs */,
	{ NULL, Questions_t2798307503_marshal_pinvoke, Questions_t2798307503_marshal_pinvoke_back, Questions_t2798307503_marshal_pinvoke_cleanup, NULL, NULL, &Questions_t2798307503_0_0_0 } /* QuestionedComponets/Questions */,
	{ NULL, SetAnimInfo_t2127528194_marshal_pinvoke, SetAnimInfo_t2127528194_marshal_pinvoke_back, SetAnimInfo_t2127528194_marshal_pinvoke_cleanup, NULL, NULL, &SetAnimInfo_t2127528194_0_0_0 } /* SetAnimArray/SetAnimInfo */,
	{ DelegatePInvokeWrapper_OnFinished_t3778785451, NULL, NULL, NULL, NULL, NULL, &OnFinished_t3778785451_0_0_0 } /* SpringPanel/OnFinished */,
	{ DelegatePInvokeWrapper_OnFinished_t3364492952, NULL, NULL, NULL, NULL, NULL, &OnFinished_t3364492952_0_0_0 } /* SpringPosition/OnFinished */,
	{ NULL, FadeEntry_t639421133_marshal_pinvoke, FadeEntry_t639421133_marshal_pinvoke_back, FadeEntry_t639421133_marshal_pinvoke_cleanup, NULL, NULL, &FadeEntry_t639421133_0_0_0 } /* TypewriterEffect/FadeEntry */,
	{ NULL, DepthEntry_t628749918_marshal_pinvoke, DepthEntry_t628749918_marshal_pinvoke_back, DepthEntry_t628749918_marshal_pinvoke_cleanup, NULL, NULL, &DepthEntry_t628749918_0_0_0 } /* UICamera/DepthEntry */,
	{ DelegatePInvokeWrapper_GetAnyKeyFunc_t1761480072, NULL, NULL, NULL, NULL, NULL, &GetAnyKeyFunc_t1761480072_0_0_0 } /* UICamera/GetAnyKeyFunc */,
	{ DelegatePInvokeWrapper_GetAxisFunc_t2592608932, NULL, NULL, NULL, NULL, NULL, &GetAxisFunc_t2592608932_0_0_0 } /* UICamera/GetAxisFunc */,
	{ DelegatePInvokeWrapper_GetKeyStateFunc_t2810275146, NULL, NULL, NULL, NULL, NULL, &GetKeyStateFunc_t2810275146_0_0_0 } /* UICamera/GetKeyStateFunc */,
	{ DelegatePInvokeWrapper_GetTouchCountCallback_t3185863032, NULL, NULL, NULL, NULL, NULL, &GetTouchCountCallback_t3185863032_0_0_0 } /* UICamera/GetTouchCountCallback */,
	{ DelegatePInvokeWrapper_MoveDelegate_t16019400, NULL, NULL, NULL, NULL, NULL, &MoveDelegate_t16019400_0_0_0 } /* UICamera/MoveDelegate */,
	{ DelegatePInvokeWrapper_OnCustomInput_t3508588789, NULL, NULL, NULL, NULL, NULL, &OnCustomInput_t3508588789_0_0_0 } /* UICamera/OnCustomInput */,
	{ DelegatePInvokeWrapper_OnSchemeChange_t1701155603, NULL, NULL, NULL, NULL, NULL, &OnSchemeChange_t1701155603_0_0_0 } /* UICamera/OnSchemeChange */,
	{ DelegatePInvokeWrapper_OnScreenResize_t2279991692, NULL, NULL, NULL, NULL, NULL, &OnScreenResize_t2279991692_0_0_0 } /* UICamera/OnScreenResize */,
	{ DelegatePInvokeWrapper_OnReposition_t1372889220, NULL, NULL, NULL, NULL, NULL, &OnReposition_t1372889220_0_0_0 } /* UIGrid/OnReposition */,
	{ DelegatePInvokeWrapper_OnValidate_t1246632601, NULL, NULL, NULL, NULL, NULL, &OnValidate_t1246632601_0_0_0 } /* UIInput/OnValidate */,
	{ DelegatePInvokeWrapper_OnGeometryUpdated_t2462438111, NULL, NULL, NULL, NULL, NULL, &OnGeometryUpdated_t2462438111_0_0_0 } /* UIPanel/OnGeometryUpdated */,
	{ DelegatePInvokeWrapper_LegacyEvent_t2749056879, NULL, NULL, NULL, NULL, NULL, &LegacyEvent_t2749056879_0_0_0 } /* UIPopupList/LegacyEvent */,
	{ DelegatePInvokeWrapper_OnDragFinished_t3715779777, NULL, NULL, NULL, NULL, NULL, &OnDragFinished_t3715779777_0_0_0 } /* UIProgressBar/OnDragFinished */,
	{ DelegatePInvokeWrapper_OnDragNotification_t1437737811, NULL, NULL, NULL, NULL, NULL, &OnDragNotification_t1437737811_0_0_0 } /* UIScrollView/OnDragNotification */,
	{ DelegatePInvokeWrapper_OnReposition_t3913508630, NULL, NULL, NULL, NULL, NULL, &OnReposition_t3913508630_0_0_0 } /* UITable/OnReposition */,
	{ DelegatePInvokeWrapper_Validate_t3702293971, NULL, NULL, NULL, NULL, NULL, &Validate_t3702293971_0_0_0 } /* UIToggle/Validate */,
	{ DelegatePInvokeWrapper_HitCheck_t2300079615, NULL, NULL, NULL, NULL, NULL, &HitCheck_t2300079615_0_0_0 } /* UIWidget/HitCheck */,
	{ DelegatePInvokeWrapper_OnDimensionsChanged_t3101921181, NULL, NULL, NULL, NULL, NULL, &OnDimensionsChanged_t3101921181_0_0_0 } /* UIWidget/OnDimensionsChanged */,
	NULL,
};
