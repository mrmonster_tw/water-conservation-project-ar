﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.PeerCredentialElement
struct  PeerCredentialElement_t3226431640  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct PeerCredentialElement_t3226431640_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.PeerCredentialElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.PeerCredentialElement::certificate
	ConfigurationProperty_t3590861854 * ___certificate_14;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.PeerCredentialElement::message_sender_authentication
	ConfigurationProperty_t3590861854 * ___message_sender_authentication_15;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.PeerCredentialElement::peer_authentication
	ConfigurationProperty_t3590861854 * ___peer_authentication_16;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(PeerCredentialElement_t3226431640_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier(&___properties_13, value);
	}

	inline static int32_t get_offset_of_certificate_14() { return static_cast<int32_t>(offsetof(PeerCredentialElement_t3226431640_StaticFields, ___certificate_14)); }
	inline ConfigurationProperty_t3590861854 * get_certificate_14() const { return ___certificate_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_certificate_14() { return &___certificate_14; }
	inline void set_certificate_14(ConfigurationProperty_t3590861854 * value)
	{
		___certificate_14 = value;
		Il2CppCodeGenWriteBarrier(&___certificate_14, value);
	}

	inline static int32_t get_offset_of_message_sender_authentication_15() { return static_cast<int32_t>(offsetof(PeerCredentialElement_t3226431640_StaticFields, ___message_sender_authentication_15)); }
	inline ConfigurationProperty_t3590861854 * get_message_sender_authentication_15() const { return ___message_sender_authentication_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_message_sender_authentication_15() { return &___message_sender_authentication_15; }
	inline void set_message_sender_authentication_15(ConfigurationProperty_t3590861854 * value)
	{
		___message_sender_authentication_15 = value;
		Il2CppCodeGenWriteBarrier(&___message_sender_authentication_15, value);
	}

	inline static int32_t get_offset_of_peer_authentication_16() { return static_cast<int32_t>(offsetof(PeerCredentialElement_t3226431640_StaticFields, ___peer_authentication_16)); }
	inline ConfigurationProperty_t3590861854 * get_peer_authentication_16() const { return ___peer_authentication_16; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_peer_authentication_16() { return &___peer_authentication_16; }
	inline void set_peer_authentication_16(ConfigurationProperty_t3590861854 * value)
	{
		___peer_authentication_16 = value;
		Il2CppCodeGenWriteBarrier(&___peer_authentication_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
