﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<System.CodeDom.CodeCompileUnit>
struct List_1_t3999693657;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.AppCodeAssembly
struct  AppCodeAssembly_t1261384177  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.String> System.Web.Compilation.AppCodeAssembly::files
	List_1_t3319525431 * ___files_0;
	// System.Collections.Generic.List`1<System.CodeDom.CodeCompileUnit> System.Web.Compilation.AppCodeAssembly::units
	List_1_t3999693657 * ___units_1;
	// System.String System.Web.Compilation.AppCodeAssembly::name
	String_t* ___name_2;
	// System.String System.Web.Compilation.AppCodeAssembly::path
	String_t* ___path_3;
	// System.Boolean System.Web.Compilation.AppCodeAssembly::validAssembly
	bool ___validAssembly_4;
	// System.String System.Web.Compilation.AppCodeAssembly::outputAssemblyName
	String_t* ___outputAssemblyName_5;

public:
	inline static int32_t get_offset_of_files_0() { return static_cast<int32_t>(offsetof(AppCodeAssembly_t1261384177, ___files_0)); }
	inline List_1_t3319525431 * get_files_0() const { return ___files_0; }
	inline List_1_t3319525431 ** get_address_of_files_0() { return &___files_0; }
	inline void set_files_0(List_1_t3319525431 * value)
	{
		___files_0 = value;
		Il2CppCodeGenWriteBarrier(&___files_0, value);
	}

	inline static int32_t get_offset_of_units_1() { return static_cast<int32_t>(offsetof(AppCodeAssembly_t1261384177, ___units_1)); }
	inline List_1_t3999693657 * get_units_1() const { return ___units_1; }
	inline List_1_t3999693657 ** get_address_of_units_1() { return &___units_1; }
	inline void set_units_1(List_1_t3999693657 * value)
	{
		___units_1 = value;
		Il2CppCodeGenWriteBarrier(&___units_1, value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(AppCodeAssembly_t1261384177, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}

	inline static int32_t get_offset_of_path_3() { return static_cast<int32_t>(offsetof(AppCodeAssembly_t1261384177, ___path_3)); }
	inline String_t* get_path_3() const { return ___path_3; }
	inline String_t** get_address_of_path_3() { return &___path_3; }
	inline void set_path_3(String_t* value)
	{
		___path_3 = value;
		Il2CppCodeGenWriteBarrier(&___path_3, value);
	}

	inline static int32_t get_offset_of_validAssembly_4() { return static_cast<int32_t>(offsetof(AppCodeAssembly_t1261384177, ___validAssembly_4)); }
	inline bool get_validAssembly_4() const { return ___validAssembly_4; }
	inline bool* get_address_of_validAssembly_4() { return &___validAssembly_4; }
	inline void set_validAssembly_4(bool value)
	{
		___validAssembly_4 = value;
	}

	inline static int32_t get_offset_of_outputAssemblyName_5() { return static_cast<int32_t>(offsetof(AppCodeAssembly_t1261384177, ___outputAssemblyName_5)); }
	inline String_t* get_outputAssemblyName_5() const { return ___outputAssemblyName_5; }
	inline String_t** get_address_of_outputAssemblyName_5() { return &___outputAssemblyName_5; }
	inline void set_outputAssemblyName_5(String_t* value)
	{
		___outputAssemblyName_5 = value;
		Il2CppCodeGenWriteBarrier(&___outputAssemblyName_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
