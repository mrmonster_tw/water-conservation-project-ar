﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_ComponentModel_Component3620823400.h"
#include "System_System_IO_NotifyFilters3825601669.h"
#include "System_System_IO_WaitForChangedResult3377826936.h"

// System.String
struct String_t;
// System.ComponentModel.ISynchronizeInvoke
struct ISynchronizeInvoke_t1357618335;
// System.IO.SearchPattern2
struct SearchPattern2_t2824637351;
// System.IO.IFileWatcher
struct IFileWatcher_t3899097327;
// System.Object
struct Il2CppObject;
// System.IO.FileSystemEventHandler
struct FileSystemEventHandler_t1806121106;
// System.IO.RenamedEventHandler
struct RenamedEventHandler_t3047461033;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileSystemWatcher
struct  FileSystemWatcher_t416760199  : public Component_t3620823400
{
public:
	// System.Boolean System.IO.FileSystemWatcher::enableRaisingEvents
	bool ___enableRaisingEvents_4;
	// System.String System.IO.FileSystemWatcher::filter
	String_t* ___filter_5;
	// System.Boolean System.IO.FileSystemWatcher::includeSubdirectories
	bool ___includeSubdirectories_6;
	// System.Int32 System.IO.FileSystemWatcher::internalBufferSize
	int32_t ___internalBufferSize_7;
	// System.IO.NotifyFilters System.IO.FileSystemWatcher::notifyFilter
	int32_t ___notifyFilter_8;
	// System.String System.IO.FileSystemWatcher::path
	String_t* ___path_9;
	// System.String System.IO.FileSystemWatcher::fullpath
	String_t* ___fullpath_10;
	// System.ComponentModel.ISynchronizeInvoke System.IO.FileSystemWatcher::synchronizingObject
	Il2CppObject * ___synchronizingObject_11;
	// System.IO.WaitForChangedResult System.IO.FileSystemWatcher::lastData
	WaitForChangedResult_t3377826936  ___lastData_12;
	// System.Boolean System.IO.FileSystemWatcher::waiting
	bool ___waiting_13;
	// System.IO.SearchPattern2 System.IO.FileSystemWatcher::pattern
	SearchPattern2_t2824637351 * ___pattern_14;
	// System.Boolean System.IO.FileSystemWatcher::disposed
	bool ___disposed_15;
	// System.String System.IO.FileSystemWatcher::mangledFilter
	String_t* ___mangledFilter_16;
	// System.IO.FileSystemEventHandler System.IO.FileSystemWatcher::Changed
	FileSystemEventHandler_t1806121106 * ___Changed_19;
	// System.IO.FileSystemEventHandler System.IO.FileSystemWatcher::Created
	FileSystemEventHandler_t1806121106 * ___Created_20;
	// System.IO.FileSystemEventHandler System.IO.FileSystemWatcher::Deleted
	FileSystemEventHandler_t1806121106 * ___Deleted_21;
	// System.IO.RenamedEventHandler System.IO.FileSystemWatcher::Renamed
	RenamedEventHandler_t3047461033 * ___Renamed_22;

public:
	inline static int32_t get_offset_of_enableRaisingEvents_4() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___enableRaisingEvents_4)); }
	inline bool get_enableRaisingEvents_4() const { return ___enableRaisingEvents_4; }
	inline bool* get_address_of_enableRaisingEvents_4() { return &___enableRaisingEvents_4; }
	inline void set_enableRaisingEvents_4(bool value)
	{
		___enableRaisingEvents_4 = value;
	}

	inline static int32_t get_offset_of_filter_5() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___filter_5)); }
	inline String_t* get_filter_5() const { return ___filter_5; }
	inline String_t** get_address_of_filter_5() { return &___filter_5; }
	inline void set_filter_5(String_t* value)
	{
		___filter_5 = value;
		Il2CppCodeGenWriteBarrier(&___filter_5, value);
	}

	inline static int32_t get_offset_of_includeSubdirectories_6() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___includeSubdirectories_6)); }
	inline bool get_includeSubdirectories_6() const { return ___includeSubdirectories_6; }
	inline bool* get_address_of_includeSubdirectories_6() { return &___includeSubdirectories_6; }
	inline void set_includeSubdirectories_6(bool value)
	{
		___includeSubdirectories_6 = value;
	}

	inline static int32_t get_offset_of_internalBufferSize_7() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___internalBufferSize_7)); }
	inline int32_t get_internalBufferSize_7() const { return ___internalBufferSize_7; }
	inline int32_t* get_address_of_internalBufferSize_7() { return &___internalBufferSize_7; }
	inline void set_internalBufferSize_7(int32_t value)
	{
		___internalBufferSize_7 = value;
	}

	inline static int32_t get_offset_of_notifyFilter_8() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___notifyFilter_8)); }
	inline int32_t get_notifyFilter_8() const { return ___notifyFilter_8; }
	inline int32_t* get_address_of_notifyFilter_8() { return &___notifyFilter_8; }
	inline void set_notifyFilter_8(int32_t value)
	{
		___notifyFilter_8 = value;
	}

	inline static int32_t get_offset_of_path_9() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___path_9)); }
	inline String_t* get_path_9() const { return ___path_9; }
	inline String_t** get_address_of_path_9() { return &___path_9; }
	inline void set_path_9(String_t* value)
	{
		___path_9 = value;
		Il2CppCodeGenWriteBarrier(&___path_9, value);
	}

	inline static int32_t get_offset_of_fullpath_10() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___fullpath_10)); }
	inline String_t* get_fullpath_10() const { return ___fullpath_10; }
	inline String_t** get_address_of_fullpath_10() { return &___fullpath_10; }
	inline void set_fullpath_10(String_t* value)
	{
		___fullpath_10 = value;
		Il2CppCodeGenWriteBarrier(&___fullpath_10, value);
	}

	inline static int32_t get_offset_of_synchronizingObject_11() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___synchronizingObject_11)); }
	inline Il2CppObject * get_synchronizingObject_11() const { return ___synchronizingObject_11; }
	inline Il2CppObject ** get_address_of_synchronizingObject_11() { return &___synchronizingObject_11; }
	inline void set_synchronizingObject_11(Il2CppObject * value)
	{
		___synchronizingObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___synchronizingObject_11, value);
	}

	inline static int32_t get_offset_of_lastData_12() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___lastData_12)); }
	inline WaitForChangedResult_t3377826936  get_lastData_12() const { return ___lastData_12; }
	inline WaitForChangedResult_t3377826936 * get_address_of_lastData_12() { return &___lastData_12; }
	inline void set_lastData_12(WaitForChangedResult_t3377826936  value)
	{
		___lastData_12 = value;
	}

	inline static int32_t get_offset_of_waiting_13() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___waiting_13)); }
	inline bool get_waiting_13() const { return ___waiting_13; }
	inline bool* get_address_of_waiting_13() { return &___waiting_13; }
	inline void set_waiting_13(bool value)
	{
		___waiting_13 = value;
	}

	inline static int32_t get_offset_of_pattern_14() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___pattern_14)); }
	inline SearchPattern2_t2824637351 * get_pattern_14() const { return ___pattern_14; }
	inline SearchPattern2_t2824637351 ** get_address_of_pattern_14() { return &___pattern_14; }
	inline void set_pattern_14(SearchPattern2_t2824637351 * value)
	{
		___pattern_14 = value;
		Il2CppCodeGenWriteBarrier(&___pattern_14, value);
	}

	inline static int32_t get_offset_of_disposed_15() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___disposed_15)); }
	inline bool get_disposed_15() const { return ___disposed_15; }
	inline bool* get_address_of_disposed_15() { return &___disposed_15; }
	inline void set_disposed_15(bool value)
	{
		___disposed_15 = value;
	}

	inline static int32_t get_offset_of_mangledFilter_16() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___mangledFilter_16)); }
	inline String_t* get_mangledFilter_16() const { return ___mangledFilter_16; }
	inline String_t** get_address_of_mangledFilter_16() { return &___mangledFilter_16; }
	inline void set_mangledFilter_16(String_t* value)
	{
		___mangledFilter_16 = value;
		Il2CppCodeGenWriteBarrier(&___mangledFilter_16, value);
	}

	inline static int32_t get_offset_of_Changed_19() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___Changed_19)); }
	inline FileSystemEventHandler_t1806121106 * get_Changed_19() const { return ___Changed_19; }
	inline FileSystemEventHandler_t1806121106 ** get_address_of_Changed_19() { return &___Changed_19; }
	inline void set_Changed_19(FileSystemEventHandler_t1806121106 * value)
	{
		___Changed_19 = value;
		Il2CppCodeGenWriteBarrier(&___Changed_19, value);
	}

	inline static int32_t get_offset_of_Created_20() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___Created_20)); }
	inline FileSystemEventHandler_t1806121106 * get_Created_20() const { return ___Created_20; }
	inline FileSystemEventHandler_t1806121106 ** get_address_of_Created_20() { return &___Created_20; }
	inline void set_Created_20(FileSystemEventHandler_t1806121106 * value)
	{
		___Created_20 = value;
		Il2CppCodeGenWriteBarrier(&___Created_20, value);
	}

	inline static int32_t get_offset_of_Deleted_21() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___Deleted_21)); }
	inline FileSystemEventHandler_t1806121106 * get_Deleted_21() const { return ___Deleted_21; }
	inline FileSystemEventHandler_t1806121106 ** get_address_of_Deleted_21() { return &___Deleted_21; }
	inline void set_Deleted_21(FileSystemEventHandler_t1806121106 * value)
	{
		___Deleted_21 = value;
		Il2CppCodeGenWriteBarrier(&___Deleted_21, value);
	}

	inline static int32_t get_offset_of_Renamed_22() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199, ___Renamed_22)); }
	inline RenamedEventHandler_t3047461033 * get_Renamed_22() const { return ___Renamed_22; }
	inline RenamedEventHandler_t3047461033 ** get_address_of_Renamed_22() { return &___Renamed_22; }
	inline void set_Renamed_22(RenamedEventHandler_t3047461033 * value)
	{
		___Renamed_22 = value;
		Il2CppCodeGenWriteBarrier(&___Renamed_22, value);
	}
};

struct FileSystemWatcher_t416760199_StaticFields
{
public:
	// System.IO.IFileWatcher System.IO.FileSystemWatcher::watcher
	Il2CppObject * ___watcher_17;
	// System.Object System.IO.FileSystemWatcher::lockobj
	Il2CppObject * ___lockobj_18;

public:
	inline static int32_t get_offset_of_watcher_17() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199_StaticFields, ___watcher_17)); }
	inline Il2CppObject * get_watcher_17() const { return ___watcher_17; }
	inline Il2CppObject ** get_address_of_watcher_17() { return &___watcher_17; }
	inline void set_watcher_17(Il2CppObject * value)
	{
		___watcher_17 = value;
		Il2CppCodeGenWriteBarrier(&___watcher_17, value);
	}

	inline static int32_t get_offset_of_lockobj_18() { return static_cast<int32_t>(offsetof(FileSystemWatcher_t416760199_StaticFields, ___lockobj_18)); }
	inline Il2CppObject * get_lockobj_18() const { return ___lockobj_18; }
	inline Il2CppObject ** get_address_of_lockobj_18() { return &___lockobj_18; }
	inline void set_lockobj_18(Il2CppObject * value)
	{
		___lockobj_18 = value;
		Il2CppCodeGenWriteBarrier(&___lockobj_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
