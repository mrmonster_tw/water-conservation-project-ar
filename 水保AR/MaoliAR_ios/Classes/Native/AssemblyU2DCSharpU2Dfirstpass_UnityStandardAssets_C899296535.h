﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_C499321483.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3790221060.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3779436815.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_C899296535.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/SSRSettings
struct  SSRSettings_t899296535 
{
public:
	// UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/ReflectionSettings UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/SSRSettings::reflectionSettings
	ReflectionSettings_t499321483  ___reflectionSettings_0;
	// UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/IntensitySettings UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/SSRSettings::intensitySettings
	IntensitySettings_t3790221060  ___intensitySettings_1;
	// UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/ScreenEdgeMask UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/SSRSettings::screenEdgeMask
	ScreenEdgeMask_t3779436815  ___screenEdgeMask_2;

public:
	inline static int32_t get_offset_of_reflectionSettings_0() { return static_cast<int32_t>(offsetof(SSRSettings_t899296535, ___reflectionSettings_0)); }
	inline ReflectionSettings_t499321483  get_reflectionSettings_0() const { return ___reflectionSettings_0; }
	inline ReflectionSettings_t499321483 * get_address_of_reflectionSettings_0() { return &___reflectionSettings_0; }
	inline void set_reflectionSettings_0(ReflectionSettings_t499321483  value)
	{
		___reflectionSettings_0 = value;
	}

	inline static int32_t get_offset_of_intensitySettings_1() { return static_cast<int32_t>(offsetof(SSRSettings_t899296535, ___intensitySettings_1)); }
	inline IntensitySettings_t3790221060  get_intensitySettings_1() const { return ___intensitySettings_1; }
	inline IntensitySettings_t3790221060 * get_address_of_intensitySettings_1() { return &___intensitySettings_1; }
	inline void set_intensitySettings_1(IntensitySettings_t3790221060  value)
	{
		___intensitySettings_1 = value;
	}

	inline static int32_t get_offset_of_screenEdgeMask_2() { return static_cast<int32_t>(offsetof(SSRSettings_t899296535, ___screenEdgeMask_2)); }
	inline ScreenEdgeMask_t3779436815  get_screenEdgeMask_2() const { return ___screenEdgeMask_2; }
	inline ScreenEdgeMask_t3779436815 * get_address_of_screenEdgeMask_2() { return &___screenEdgeMask_2; }
	inline void set_screenEdgeMask_2(ScreenEdgeMask_t3779436815  value)
	{
		___screenEdgeMask_2 = value;
	}
};

struct SSRSettings_t899296535_StaticFields
{
public:
	// UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/SSRSettings UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/SSRSettings::s_Default
	SSRSettings_t899296535  ___s_Default_3;

public:
	inline static int32_t get_offset_of_s_Default_3() { return static_cast<int32_t>(offsetof(SSRSettings_t899296535_StaticFields, ___s_Default_3)); }
	inline SSRSettings_t899296535  get_s_Default_3() const { return ___s_Default_3; }
	inline SSRSettings_t899296535 * get_address_of_s_Default_3() { return &___s_Default_3; }
	inline void set_s_Default_3(SSRSettings_t899296535  value)
	{
		___s_Default_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/SSRSettings
struct SSRSettings_t899296535_marshaled_pinvoke
{
	ReflectionSettings_t499321483_marshaled_pinvoke ___reflectionSettings_0;
	IntensitySettings_t3790221060  ___intensitySettings_1;
	ScreenEdgeMask_t3779436815  ___screenEdgeMask_2;
};
// Native definition for COM marshalling of UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/SSRSettings
struct SSRSettings_t899296535_marshaled_com
{
	ReflectionSettings_t499321483_marshaled_com ___reflectionSettings_0;
	IntensitySettings_t3790221060  ___intensitySettings_1;
	ScreenEdgeMask_t3779436815  ___screenEdgeMask_2;
};
