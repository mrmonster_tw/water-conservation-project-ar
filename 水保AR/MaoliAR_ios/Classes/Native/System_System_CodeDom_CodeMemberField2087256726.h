﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeTypeMember1555525554.h"

// System.CodeDom.CodeExpression
struct CodeExpression_t2166265795;
// System.CodeDom.CodeTypeReference
struct CodeTypeReference_t3809997434;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeMemberField
struct  CodeMemberField_t2087256726  : public CodeTypeMember_t1555525554
{
public:
	// System.CodeDom.CodeExpression System.CodeDom.CodeMemberField::initExpression
	CodeExpression_t2166265795 * ___initExpression_8;
	// System.CodeDom.CodeTypeReference System.CodeDom.CodeMemberField::type
	CodeTypeReference_t3809997434 * ___type_9;

public:
	inline static int32_t get_offset_of_initExpression_8() { return static_cast<int32_t>(offsetof(CodeMemberField_t2087256726, ___initExpression_8)); }
	inline CodeExpression_t2166265795 * get_initExpression_8() const { return ___initExpression_8; }
	inline CodeExpression_t2166265795 ** get_address_of_initExpression_8() { return &___initExpression_8; }
	inline void set_initExpression_8(CodeExpression_t2166265795 * value)
	{
		___initExpression_8 = value;
		Il2CppCodeGenWriteBarrier(&___initExpression_8, value);
	}

	inline static int32_t get_offset_of_type_9() { return static_cast<int32_t>(offsetof(CodeMemberField_t2087256726, ___type_9)); }
	inline CodeTypeReference_t3809997434 * get_type_9() const { return ___type_9; }
	inline CodeTypeReference_t3809997434 ** get_address_of_type_9() { return &___type_9; }
	inline void set_type_9(CodeTypeReference_t3809997434 * value)
	{
		___type_9 = value;
		Il2CppCodeGenWriteBarrier(&___type_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
