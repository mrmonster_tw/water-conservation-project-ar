﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.UI.Image[]
struct ImageU5BU5D_t2439009922;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Level_05_Controller_ICON
struct  Level_05_Controller_ICON_t491541153  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image[] Level_05_Controller_ICON::image
	ImageU5BU5D_t2439009922* ___image_2;

public:
	inline static int32_t get_offset_of_image_2() { return static_cast<int32_t>(offsetof(Level_05_Controller_ICON_t491541153, ___image_2)); }
	inline ImageU5BU5D_t2439009922* get_image_2() const { return ___image_2; }
	inline ImageU5BU5D_t2439009922** get_address_of_image_2() { return &___image_2; }
	inline void set_image_2(ImageU5BU5D_t2439009922* value)
	{
		___image_2 = value;
		Il2CppCodeGenWriteBarrier(&___image_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
