﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.UI.Page
struct Page_t770747966;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t1432317219;
// System.Byte[]
struct ByteU5BU5D_t4116647657;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.ObjectStateFormatter
struct  ObjectStateFormatter_t1543808709  : public Il2CppObject
{
public:
	// System.Web.UI.Page System.Web.UI.ObjectStateFormatter::page
	Page_t770747966 * ___page_0;
	// System.Security.Cryptography.HashAlgorithm System.Web.UI.ObjectStateFormatter::algo
	HashAlgorithm_t1432317219 * ___algo_1;
	// System.Byte[] System.Web.UI.ObjectStateFormatter::vkey
	ByteU5BU5D_t4116647657* ___vkey_2;

public:
	inline static int32_t get_offset_of_page_0() { return static_cast<int32_t>(offsetof(ObjectStateFormatter_t1543808709, ___page_0)); }
	inline Page_t770747966 * get_page_0() const { return ___page_0; }
	inline Page_t770747966 ** get_address_of_page_0() { return &___page_0; }
	inline void set_page_0(Page_t770747966 * value)
	{
		___page_0 = value;
		Il2CppCodeGenWriteBarrier(&___page_0, value);
	}

	inline static int32_t get_offset_of_algo_1() { return static_cast<int32_t>(offsetof(ObjectStateFormatter_t1543808709, ___algo_1)); }
	inline HashAlgorithm_t1432317219 * get_algo_1() const { return ___algo_1; }
	inline HashAlgorithm_t1432317219 ** get_address_of_algo_1() { return &___algo_1; }
	inline void set_algo_1(HashAlgorithm_t1432317219 * value)
	{
		___algo_1 = value;
		Il2CppCodeGenWriteBarrier(&___algo_1, value);
	}

	inline static int32_t get_offset_of_vkey_2() { return static_cast<int32_t>(offsetof(ObjectStateFormatter_t1543808709, ___vkey_2)); }
	inline ByteU5BU5D_t4116647657* get_vkey_2() const { return ___vkey_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_vkey_2() { return &___vkey_2; }
	inline void set_vkey_2(ByteU5BU5D_t4116647657* value)
	{
		___vkey_2 = value;
		Il2CppCodeGenWriteBarrier(&___vkey_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
