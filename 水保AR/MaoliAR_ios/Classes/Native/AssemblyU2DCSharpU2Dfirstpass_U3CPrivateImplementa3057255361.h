﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa2467506693.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa3253128244.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa2488454196.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255374  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A
	U24ArrayTypeU3D24_t2467506693  ___U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_0;
	// <PrivateImplementationDetails>/$ArrayType=16 <PrivateImplementationDetails>::$field-8D0C1DCA7F35F40B810D754C5F5EC7C4D6110D41
	U24ArrayTypeU3D16_t3253128244  ___U24fieldU2D8D0C1DCA7F35F40B810D754C5F5EC7C4D6110D41_1;
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-51A7A390CD6DE245186881400B18C9D822EFE240
	U24ArrayTypeU3D12_t2488454197  ___U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_2;

public:
	inline static int32_t get_offset_of_U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_0)); }
	inline U24ArrayTypeU3D24_t2467506693  get_U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_0() const { return ___U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_0; }
	inline U24ArrayTypeU3D24_t2467506693 * get_address_of_U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_0() { return &___U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_0; }
	inline void set_U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_0(U24ArrayTypeU3D24_t2467506693  value)
	{
		___U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8D0C1DCA7F35F40B810D754C5F5EC7C4D6110D41_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___U24fieldU2D8D0C1DCA7F35F40B810D754C5F5EC7C4D6110D41_1)); }
	inline U24ArrayTypeU3D16_t3253128244  get_U24fieldU2D8D0C1DCA7F35F40B810D754C5F5EC7C4D6110D41_1() const { return ___U24fieldU2D8D0C1DCA7F35F40B810D754C5F5EC7C4D6110D41_1; }
	inline U24ArrayTypeU3D16_t3253128244 * get_address_of_U24fieldU2D8D0C1DCA7F35F40B810D754C5F5EC7C4D6110D41_1() { return &___U24fieldU2D8D0C1DCA7F35F40B810D754C5F5EC7C4D6110D41_1; }
	inline void set_U24fieldU2D8D0C1DCA7F35F40B810D754C5F5EC7C4D6110D41_1(U24ArrayTypeU3D16_t3253128244  value)
	{
		___U24fieldU2D8D0C1DCA7F35F40B810D754C5F5EC7C4D6110D41_1 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_2)); }
	inline U24ArrayTypeU3D12_t2488454197  get_U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_2() const { return ___U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_2; }
	inline U24ArrayTypeU3D12_t2488454197 * get_address_of_U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_2() { return &___U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_2; }
	inline void set_U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_2(U24ArrayTypeU3D12_t2488454197  value)
	{
		___U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
