﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerato34418657.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_968067334.h"

// System.Object
struct Il2CppObject;
// System.ServiceModel.Channels.MessageProperties/ParameterValueCollection
struct ParameterValueCollection_t3369516869;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.MessageProperties/ParameterValueCollection/<GetEnumerator>c__Iterator3
struct  U3CGetEnumeratorU3Ec__Iterator3_t2203724713  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>> System.ServiceModel.Channels.MessageProperties/ParameterValueCollection/<GetEnumerator>c__Iterator3::<$s_180>__0
	Enumerator_t34418657  ___U3CU24s_180U3E__0_0;
	// System.Collections.Generic.KeyValuePair`2<System.String,System.Object> System.ServiceModel.Channels.MessageProperties/ParameterValueCollection/<GetEnumerator>c__Iterator3::<p>__1
	KeyValuePair_2_t968067334  ___U3CpU3E__1_1;
	// System.Int32 System.ServiceModel.Channels.MessageProperties/ParameterValueCollection/<GetEnumerator>c__Iterator3::$PC
	int32_t ___U24PC_2;
	// System.Object System.ServiceModel.Channels.MessageProperties/ParameterValueCollection/<GetEnumerator>c__Iterator3::$current
	Il2CppObject * ___U24current_3;
	// System.ServiceModel.Channels.MessageProperties/ParameterValueCollection System.ServiceModel.Channels.MessageProperties/ParameterValueCollection/<GetEnumerator>c__Iterator3::<>f__this
	ParameterValueCollection_t3369516869 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CU24s_180U3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator3_t2203724713, ___U3CU24s_180U3E__0_0)); }
	inline Enumerator_t34418657  get_U3CU24s_180U3E__0_0() const { return ___U3CU24s_180U3E__0_0; }
	inline Enumerator_t34418657 * get_address_of_U3CU24s_180U3E__0_0() { return &___U3CU24s_180U3E__0_0; }
	inline void set_U3CU24s_180U3E__0_0(Enumerator_t34418657  value)
	{
		___U3CU24s_180U3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CpU3E__1_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator3_t2203724713, ___U3CpU3E__1_1)); }
	inline KeyValuePair_2_t968067334  get_U3CpU3E__1_1() const { return ___U3CpU3E__1_1; }
	inline KeyValuePair_2_t968067334 * get_address_of_U3CpU3E__1_1() { return &___U3CpU3E__1_1; }
	inline void set_U3CpU3E__1_1(KeyValuePair_2_t968067334  value)
	{
		___U3CpU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator3_t2203724713, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator3_t2203724713, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator3_t2203724713, ___U3CU3Ef__this_4)); }
	inline ParameterValueCollection_t3369516869 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline ParameterValueCollection_t3369516869 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(ParameterValueCollection_t3369516869 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
