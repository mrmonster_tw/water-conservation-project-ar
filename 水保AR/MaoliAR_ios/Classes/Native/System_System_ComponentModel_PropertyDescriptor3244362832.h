﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_ComponentModel_MemberDescriptor3815403747.h"

// System.ComponentModel.TypeConverter
struct TypeConverter_t2249118273;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.PropertyDescriptor
struct  PropertyDescriptor_t3244362832  : public MemberDescriptor_t3815403747
{
public:
	// System.ComponentModel.TypeConverter System.ComponentModel.PropertyDescriptor::converter
	TypeConverter_t2249118273 * ___converter_3;

public:
	inline static int32_t get_offset_of_converter_3() { return static_cast<int32_t>(offsetof(PropertyDescriptor_t3244362832, ___converter_3)); }
	inline TypeConverter_t2249118273 * get_converter_3() const { return ___converter_3; }
	inline TypeConverter_t2249118273 ** get_address_of_converter_3() { return &___converter_3; }
	inline void set_converter_3(TypeConverter_t2249118273 * value)
	{
		___converter_3 = value;
		Il2CppCodeGenWriteBarrier(&___converter_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
