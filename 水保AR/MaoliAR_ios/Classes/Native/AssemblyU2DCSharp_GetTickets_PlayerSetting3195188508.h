﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetTickets/PlayerSetting
struct  PlayerSetting_t3195188508 
{
public:
	// System.String GetTickets/PlayerSetting::myAddress
	String_t* ___myAddress_0;
	// System.String GetTickets/PlayerSetting::mySVC
	String_t* ___mySVC_1;
	// System.String GetTickets/PlayerSetting::FuncNormal
	String_t* ___FuncNormal_2;

public:
	inline static int32_t get_offset_of_myAddress_0() { return static_cast<int32_t>(offsetof(PlayerSetting_t3195188508, ___myAddress_0)); }
	inline String_t* get_myAddress_0() const { return ___myAddress_0; }
	inline String_t** get_address_of_myAddress_0() { return &___myAddress_0; }
	inline void set_myAddress_0(String_t* value)
	{
		___myAddress_0 = value;
		Il2CppCodeGenWriteBarrier(&___myAddress_0, value);
	}

	inline static int32_t get_offset_of_mySVC_1() { return static_cast<int32_t>(offsetof(PlayerSetting_t3195188508, ___mySVC_1)); }
	inline String_t* get_mySVC_1() const { return ___mySVC_1; }
	inline String_t** get_address_of_mySVC_1() { return &___mySVC_1; }
	inline void set_mySVC_1(String_t* value)
	{
		___mySVC_1 = value;
		Il2CppCodeGenWriteBarrier(&___mySVC_1, value);
	}

	inline static int32_t get_offset_of_FuncNormal_2() { return static_cast<int32_t>(offsetof(PlayerSetting_t3195188508, ___FuncNormal_2)); }
	inline String_t* get_FuncNormal_2() const { return ___FuncNormal_2; }
	inline String_t** get_address_of_FuncNormal_2() { return &___FuncNormal_2; }
	inline void set_FuncNormal_2(String_t* value)
	{
		___FuncNormal_2 = value;
		Il2CppCodeGenWriteBarrier(&___FuncNormal_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GetTickets/PlayerSetting
struct PlayerSetting_t3195188508_marshaled_pinvoke
{
	char* ___myAddress_0;
	char* ___mySVC_1;
	char* ___FuncNormal_2;
};
// Native definition for COM marshalling of GetTickets/PlayerSetting
struct PlayerSetting_t3195188508_marshaled_com
{
	Il2CppChar* ___myAddress_0;
	Il2CppChar* ___mySVC_1;
	Il2CppChar* ___FuncNormal_2;
};
