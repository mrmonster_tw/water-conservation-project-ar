﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_ExprFilter2551926938.h"
#include "System_Xml_System_Xml_XPath_ExprNumber3483239576.h"
#include "System_Xml_System_Xml_XPath_ExprLiteral2264229068.h"
#include "System_Xml_System_Xml_XPath_ExprVariable1387751146.h"
#include "System_Xml_System_Xml_XPath_ExprParens795575662.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments3573450773.h"
#include "System_Xml_System_Xml_XPath_ExprFunctionCall607199437.h"
#include "System_Xml_System_Xml_XPath_BaseIterator4168896842.h"
#include "System_Xml_System_Xml_XPath_WrapperIterator1258076988.h"
#include "System_Xml_System_Xml_XPath_SimpleIterator809567201.h"
#include "System_Xml_System_Xml_XPath_SelfIterator907214.h"
#include "System_Xml_System_Xml_XPath_NullIterator2630925529.h"
#include "System_Xml_System_Xml_XPath_ParensIterator4020310733.h"
#include "System_Xml_System_Xml_XPath_ParentIterator3536730964.h"
#include "System_Xml_System_Xml_XPath_ChildIterator3598849435.h"
#include "System_Xml_System_Xml_XPath_FollowingSiblingIterat3042855407.h"
#include "System_Xml_System_Xml_XPath_PrecedingSiblingIterat3231156185.h"
#include "System_Xml_System_Xml_XPath_AncestorIterator1850150082.h"
#include "System_Xml_System_Xml_XPath_AncestorOrSelfIterator2300071220.h"
#include "System_Xml_System_Xml_XPath_DescendantIterator2685366878.h"
#include "System_Xml_System_Xml_XPath_DescendantOrSelfIterat4256219478.h"
#include "System_Xml_System_Xml_XPath_FollowingIterator3658643279.h"
#include "System_Xml_System_Xml_XPath_PrecedingIterator2452812266.h"
#include "System_Xml_System_Xml_XPath_NamespaceIterator3101604877.h"
#include "System_Xml_System_Xml_XPath_AttributeIterator3778175417.h"
#include "System_Xml_System_Xml_XPath_AxisIterator3073253660.h"
#include "System_Xml_System_Xml_XPath_SimpleSlashIterator3611200333.h"
#include "System_Xml_System_Xml_XPath_SortedIterator2904376427.h"
#include "System_Xml_System_Xml_XPath_SlashIterator2421034408.h"
#include "System_Xml_System_Xml_XPath_PredicateIterator4231391432.h"
#include "System_Xml_System_Xml_XPath_ListIterator3066609410.h"
#include "System_Xml_System_Xml_XPath_UnionIterator312972106.h"
#include "System_Xml_Mono_Xml_XPath_Tokenizer1517940593.h"
#include "System_Xml_System_Xml_Xsl_SimpleXsltDebugger937136423.h"
#include "System_Xml_System_Xml_Xsl_XslTransform2320758400.h"
#include "System_Xml_System_Xml_Xsl_XsltArgumentList741251601.h"
#include "System_Xml_System_Xml_Xsl_XsltCompileException1322447047.h"
#include "System_Xml_System_Xml_Xsl_XsltContext2039362735.h"
#include "System_Xml_System_Xml_Xsl_XsltException1876063340.h"
#include "System_Xml_Mono_Xml_Schema_XsdWhitespaceFacet376308449.h"
#include "System_Xml_Mono_Xml_Schema_XsdOrdering789960802.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnySimpleType1257864485.h"
#include "System_Xml_Mono_Xml_Schema_XdtAnyAtomicType269366253.h"
#include "System_Xml_Mono_Xml_Schema_XdtUntypedAtomic1388131523.h"
#include "System_Xml_Mono_Xml_Schema_XsdString3049094358.h"
#include "System_Xml_Mono_Xml_Schema_XsdNormalizedString3260789355.h"
#include "System_Xml_Mono_Xml_Schema_XsdToken1239036978.h"
#include "System_Xml_Mono_Xml_Schema_XsdLanguage1876291273.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMToken834691671.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMTokens4246953255.h"
#include "System_Xml_Mono_Xml_Schema_XsdName2755146808.h"
#include "System_Xml_Mono_Xml_Schema_XsdNCName3943159043.h"
#include "System_Xml_Mono_Xml_Schema_XsdID34704195.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRef2913612829.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRefs16099206.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntity3956505874.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntities1477210398.h"
#include "System_Xml_Mono_Xml_Schema_XsdNotation2827634056.h"
#include "System_Xml_Mono_Xml_Schema_XsdDecimal1288601093.h"
#include "System_Xml_Mono_Xml_Schema_XsdInteger2044766898.h"
#include "System_Xml_Mono_Xml_Schema_XsdLong1324632828.h"
#include "System_Xml_Mono_Xml_Schema_XsdInt33917785.h"
#include "System_Xml_Mono_Xml_Schema_XsdShort3489811876.h"
#include "System_Xml_Mono_Xml_Schema_XsdByte2221093920.h"
#include "System_Xml_Mono_Xml_Schema_XsdNonNegativeInteger308064234.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedLong1409593434.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedInt72105793.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedShort3654069686.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedByte2304219558.h"
#include "System_Xml_Mono_Xml_Schema_XsdPositiveInteger1704031413.h"
#include "System_Xml_Mono_Xml_Schema_XsdNonPositiveInteger1029055398.h"
#include "System_Xml_Mono_Xml_Schema_XsdNegativeInteger2178753546.h"
#include "System_Xml_Mono_Xml_Schema_XsdFloat3181928905.h"
#include "System_Xml_Mono_Xml_Schema_XsdDouble3324344982.h"
#include "System_Xml_Mono_Xml_Schema_XsdBase64Binary3360383190.h"
#include "System_Xml_Mono_Xml_Schema_XsdHexBinary882812470.h"
#include "System_Xml_Mono_Xml_Schema_XsdQName2385631467.h"
#include "System_Xml_Mono_Xml_Schema_XsdBoolean380164876.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnyURI2755748070.h"
#include "System_Xml_Mono_Xml_Schema_XmlSchemaUri3948303260.h"
#include "System_Xml_Mono_Xml_Schema_XsdDuration1555973170.h"
#include "System_Xml_Mono_Xml_Schema_XdtDayTimeDuration268779858.h"
#include "System_Xml_Mono_Xml_Schema_XdtYearMonthDuration1503718519.h"
#include "System_Xml_Mono_Xml_Schema_XsdDateTime2563698975.h"
#include "System_Xml_Mono_Xml_Schema_XsdDate1417753656.h"
#include "System_Xml_Mono_Xml_Schema_XsdTime3558487088.h"
#include "System_Xml_Mono_Xml_Schema_XsdGYearMonth3399073121.h"
#include "System_Xml_Mono_Xml_Schema_XsdGMonthDay2605134399.h"
#include "System_Xml_Mono_Xml_Schema_XsdGYear3316212116.h"
#include "System_Xml_Mono_Xml_Schema_XsdGMonth3913018815.h"
#include "System_Xml_Mono_Xml_Schema_XsdGDay293490745.h"
#include "System_Xml_System_Xml_Schema_QNameValueType615604793.h"
#include "System_Xml_System_Xml_Schema_StringValueType261329552.h"
#include "System_Xml_System_Xml_Schema_UriValueType2866347695.h"
#include "System_Xml_System_Xml_Schema_StringArrayValueType3147326440.h"
#include "System_Xml_System_Xml_Schema_ValidationEventArgs2784773869.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (ExprFilter_t2551926938), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1800[2] = 
{
	ExprFilter_t2551926938::get_offset_of_expr_0(),
	ExprFilter_t2551926938::get_offset_of_pred_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (ExprNumber_t3483239576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1801[1] = 
{
	ExprNumber_t3483239576::get_offset_of__value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (ExprLiteral_t2264229068), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1802[1] = 
{
	ExprLiteral_t2264229068::get_offset_of__value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (ExprVariable_t1387751146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1803[2] = 
{
	ExprVariable_t1387751146::get_offset_of__name_0(),
	ExprVariable_t1387751146::get_offset_of_resolvedName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (ExprParens_t795575662), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1804[1] = 
{
	ExprParens_t795575662::get_offset_of__expr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (FunctionArguments_t3573450773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1805[2] = 
{
	FunctionArguments_t3573450773::get_offset_of__arg_0(),
	FunctionArguments_t3573450773::get_offset_of__tail_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (ExprFunctionCall_t607199437), -1, sizeof(ExprFunctionCall_t607199437_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1806[4] = 
{
	ExprFunctionCall_t607199437::get_offset_of__name_0(),
	ExprFunctionCall_t607199437::get_offset_of_resolvedName_1(),
	ExprFunctionCall_t607199437::get_offset_of__args_2(),
	ExprFunctionCall_t607199437_StaticFields::get_offset_of_U3CU3Ef__switchU24map3C_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (BaseIterator_t4168896842), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1807[2] = 
{
	BaseIterator_t4168896842::get_offset_of__nsm_1(),
	BaseIterator_t4168896842::get_offset_of_position_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (WrapperIterator_t1258076988), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1808[1] = 
{
	WrapperIterator_t1258076988::get_offset_of_iter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (SimpleIterator_t809567201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1809[3] = 
{
	SimpleIterator_t809567201::get_offset_of__nav_3(),
	SimpleIterator_t809567201::get_offset_of__current_4(),
	SimpleIterator_t809567201::get_offset_of_skipfirst_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (SelfIterator_t907214), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (NullIterator_t2630925529), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (ParensIterator_t4020310733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1812[1] = 
{
	ParensIterator_t4020310733::get_offset_of__iter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (ParentIterator_t3536730964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1813[1] = 
{
	ParentIterator_t3536730964::get_offset_of_canMove_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (ChildIterator_t3598849435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1814[1] = 
{
	ChildIterator_t3598849435::get_offset_of__nav_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (FollowingSiblingIterator_t3042855407), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (PrecedingSiblingIterator_t3231156185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1816[3] = 
{
	PrecedingSiblingIterator_t3231156185::get_offset_of_finished_6(),
	PrecedingSiblingIterator_t3231156185::get_offset_of_started_7(),
	PrecedingSiblingIterator_t3231156185::get_offset_of_startPosition_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (AncestorIterator_t1850150082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1817[3] = 
{
	AncestorIterator_t1850150082::get_offset_of_currentPosition_6(),
	AncestorIterator_t1850150082::get_offset_of_navigators_7(),
	AncestorIterator_t1850150082::get_offset_of_startPosition_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (AncestorOrSelfIterator_t2300071220), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1818[3] = 
{
	AncestorOrSelfIterator_t2300071220::get_offset_of_currentPosition_6(),
	AncestorOrSelfIterator_t2300071220::get_offset_of_navigators_7(),
	AncestorOrSelfIterator_t2300071220::get_offset_of_startPosition_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (DescendantIterator_t2685366878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1819[2] = 
{
	DescendantIterator_t2685366878::get_offset_of__depth_6(),
	DescendantIterator_t2685366878::get_offset_of__finished_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (DescendantOrSelfIterator_t4256219478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1820[2] = 
{
	DescendantOrSelfIterator_t4256219478::get_offset_of__depth_6(),
	DescendantOrSelfIterator_t4256219478::get_offset_of__finished_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (FollowingIterator_t3658643279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1821[1] = 
{
	FollowingIterator_t3658643279::get_offset_of__finished_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (PrecedingIterator_t2452812266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1822[3] = 
{
	PrecedingIterator_t2452812266::get_offset_of_finished_6(),
	PrecedingIterator_t2452812266::get_offset_of_started_7(),
	PrecedingIterator_t2452812266::get_offset_of_startPosition_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (NamespaceIterator_t3101604877), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (AttributeIterator_t3778175417), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (AxisIterator_t3073253660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1825[2] = 
{
	AxisIterator_t3073253660::get_offset_of__iter_3(),
	AxisIterator_t3073253660::get_offset_of__test_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (SimpleSlashIterator_t3611200333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1826[4] = 
{
	SimpleSlashIterator_t3611200333::get_offset_of__expr_3(),
	SimpleSlashIterator_t3611200333::get_offset_of__left_4(),
	SimpleSlashIterator_t3611200333::get_offset_of__right_5(),
	SimpleSlashIterator_t3611200333::get_offset_of__current_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (SortedIterator_t2904376427), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1827[1] = 
{
	SortedIterator_t2904376427::get_offset_of_list_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (SlashIterator_t2421034408), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1828[6] = 
{
	SlashIterator_t2421034408::get_offset_of__iterLeft_3(),
	SlashIterator_t2421034408::get_offset_of__iterRight_4(),
	SlashIterator_t2421034408::get_offset_of__expr_5(),
	SlashIterator_t2421034408::get_offset_of__iterList_6(),
	SlashIterator_t2421034408::get_offset_of__finished_7(),
	SlashIterator_t2421034408::get_offset_of__nextIterRight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (PredicateIterator_t4231391432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1829[4] = 
{
	PredicateIterator_t4231391432::get_offset_of__iter_3(),
	PredicateIterator_t4231391432::get_offset_of__pred_4(),
	PredicateIterator_t4231391432::get_offset_of_resType_5(),
	PredicateIterator_t4231391432::get_offset_of_finished_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (ListIterator_t3066609410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1830[1] = 
{
	ListIterator_t3066609410::get_offset_of__list_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (UnionIterator_t312972106), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1831[5] = 
{
	UnionIterator_t312972106::get_offset_of__left_3(),
	UnionIterator_t312972106::get_offset_of__right_4(),
	UnionIterator_t312972106::get_offset_of_keepLeft_5(),
	UnionIterator_t312972106::get_offset_of_keepRight_6(),
	UnionIterator_t312972106::get_offset_of__current_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (Tokenizer_t1517940593), -1, sizeof(Tokenizer_t1517940593_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1832[10] = 
{
	Tokenizer_t1517940593::get_offset_of_m_rgchInput_0(),
	Tokenizer_t1517940593::get_offset_of_m_ich_1(),
	Tokenizer_t1517940593::get_offset_of_m_cch_2(),
	Tokenizer_t1517940593::get_offset_of_m_iToken_3(),
	Tokenizer_t1517940593::get_offset_of_m_iTokenPrev_4(),
	Tokenizer_t1517940593::get_offset_of_m_objToken_5(),
	Tokenizer_t1517940593::get_offset_of_m_fPrevWasOperator_6(),
	Tokenizer_t1517940593::get_offset_of_m_fThisIsOperator_7(),
	Tokenizer_t1517940593_StaticFields::get_offset_of_s_mapTokens_8(),
	Tokenizer_t1517940593_StaticFields::get_offset_of_s_rgTokenMap_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (SimpleXsltDebugger_t937136423), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (XslTransform_t2320758400), -1, sizeof(XslTransform_t2320758400_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1836[6] = 
{
	XslTransform_t2320758400_StaticFields::get_offset_of_TemplateStackFrameError_0(),
	XslTransform_t2320758400_StaticFields::get_offset_of_TemplateStackFrameOutput_1(),
	XslTransform_t2320758400::get_offset_of_debugger_2(),
	XslTransform_t2320758400::get_offset_of_s_3(),
	XslTransform_t2320758400::get_offset_of_xmlResolver_4(),
	XslTransform_t2320758400_StaticFields::get_offset_of_U3CU3Ef__switchU24map3D_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (XsltArgumentList_t741251601), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1837[2] = 
{
	XsltArgumentList_t741251601::get_offset_of_extensionObjects_0(),
	XsltArgumentList_t741251601::get_offset_of_parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (XsltCompileException_t1322447047), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (XsltContext_t2039362735), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (XsltException_t1876063340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1841[4] = 
{
	XsltException_t1876063340::get_offset_of_lineNumber_11(),
	XsltException_t1876063340::get_offset_of_linePosition_12(),
	XsltException_t1876063340::get_offset_of_sourceUri_13(),
	XsltException_t1876063340::get_offset_of_templateFrames_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (XsdWhitespaceFacet_t376308449)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1842[4] = 
{
	XsdWhitespaceFacet_t376308449::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (XsdOrdering_t789960802)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1843[5] = 
{
	XsdOrdering_t789960802::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (XsdAnySimpleType_t1257864485), -1, sizeof(XsdAnySimpleType_t1257864485_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1844[6] = 
{
	XsdAnySimpleType_t1257864485_StaticFields::get_offset_of_instance_55(),
	XsdAnySimpleType_t1257864485_StaticFields::get_offset_of_whitespaceArray_56(),
	XsdAnySimpleType_t1257864485_StaticFields::get_offset_of_booleanAllowedFacets_57(),
	XsdAnySimpleType_t1257864485_StaticFields::get_offset_of_decimalAllowedFacets_58(),
	XsdAnySimpleType_t1257864485_StaticFields::get_offset_of_durationAllowedFacets_59(),
	XsdAnySimpleType_t1257864485_StaticFields::get_offset_of_stringAllowedFacets_60(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (XdtAnyAtomicType_t269366253), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (XdtUntypedAtomic_t1388131523), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (XsdString_t3049094358), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (XsdNormalizedString_t3260789355), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (XsdToken_t1239036978), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (XsdLanguage_t1876291273), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (XsdNMToken_t834691671), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (XsdNMTokens_t4246953255), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (XsdName_t2755146808), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (XsdNCName_t3943159043), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (XsdID_t34704195), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (XsdIDRef_t2913612829), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (XsdIDRefs_t16099206), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (XsdEntity_t3956505874), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (XsdEntities_t1477210398), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (XsdNotation_t2827634056), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (XsdDecimal_t1288601093), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (XsdInteger_t2044766898), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (XsdLong_t1324632828), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (XsdInt_t33917785), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (XsdShort_t3489811876), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (XsdByte_t2221093920), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (XsdNonNegativeInteger_t308064234), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (XsdUnsignedLong_t1409593434), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (XsdUnsignedInt_t72105793), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (XsdUnsignedShort_t3654069686), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (XsdUnsignedByte_t2304219558), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (XsdPositiveInteger_t1704031413), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (XsdNonPositiveInteger_t1029055398), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (XsdNegativeInteger_t2178753546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (XsdFloat_t3181928905), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (XsdDouble_t3324344982), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (XsdBase64Binary_t3360383190), -1, sizeof(XsdBase64Binary_t3360383190_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1877[2] = 
{
	XsdBase64Binary_t3360383190_StaticFields::get_offset_of_ALPHABET_61(),
	XsdBase64Binary_t3360383190_StaticFields::get_offset_of_decodeTable_62(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (XsdHexBinary_t882812470), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (XsdQName_t2385631467), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (XsdBoolean_t380164876), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (XsdAnyURI_t2755748070), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (XmlSchemaUri_t3948303260), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1882[1] = 
{
	XmlSchemaUri_t3948303260::get_offset_of_value_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (XsdDuration_t1555973170), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (XdtDayTimeDuration_t268779858), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (XdtYearMonthDuration_t1503718519), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (XsdDateTime_t2563698975), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (XsdDate_t1417753656), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (XsdTime_t3558487088), -1, sizeof(XsdTime_t3558487088_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1888[1] = 
{
	XsdTime_t3558487088_StaticFields::get_offset_of_timeFormats_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (XsdGYearMonth_t3399073121), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (XsdGMonthDay_t2605134399), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (XsdGYear_t3316212116), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (XsdGMonth_t3913018815), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (XsdGDay_t293490745), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (QNameValueType_t615604793)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1895[1] = 
{
	QNameValueType_t615604793::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { sizeof (StringValueType_t261329552)+ sizeof (Il2CppObject), sizeof(StringValueType_t261329552_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1896[1] = 
{
	StringValueType_t261329552::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (UriValueType_t2866347695)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1897[1] = 
{
	UriValueType_t2866347695::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (StringArrayValueType_t3147326440)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1898[1] = 
{
	StringArrayValueType_t3147326440::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (ValidationEventArgs_t2784773869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1899[3] = 
{
	ValidationEventArgs_t2784773869::get_offset_of_exception_1(),
	ValidationEventArgs_t2784773869::get_offset_of_message_2(),
	ValidationEventArgs_t2784773869::get_offset_of_severity_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
