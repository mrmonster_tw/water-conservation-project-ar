﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.ParserError
struct  ParserError_t3362544569  : public Il2CppObject
{
public:
	// System.String System.Web.ParserError::_errorText
	String_t* ____errorText_0;
	// System.String System.Web.ParserError::_virtualPath
	String_t* ____virtualPath_1;
	// System.Int32 System.Web.ParserError::_line
	int32_t ____line_2;

public:
	inline static int32_t get_offset_of__errorText_0() { return static_cast<int32_t>(offsetof(ParserError_t3362544569, ____errorText_0)); }
	inline String_t* get__errorText_0() const { return ____errorText_0; }
	inline String_t** get_address_of__errorText_0() { return &____errorText_0; }
	inline void set__errorText_0(String_t* value)
	{
		____errorText_0 = value;
		Il2CppCodeGenWriteBarrier(&____errorText_0, value);
	}

	inline static int32_t get_offset_of__virtualPath_1() { return static_cast<int32_t>(offsetof(ParserError_t3362544569, ____virtualPath_1)); }
	inline String_t* get__virtualPath_1() const { return ____virtualPath_1; }
	inline String_t** get_address_of__virtualPath_1() { return &____virtualPath_1; }
	inline void set__virtualPath_1(String_t* value)
	{
		____virtualPath_1 = value;
		Il2CppCodeGenWriteBarrier(&____virtualPath_1, value);
	}

	inline static int32_t get_offset_of__line_2() { return static_cast<int32_t>(offsetof(ParserError_t3362544569, ____line_2)); }
	inline int32_t get__line_2() const { return ____line_2; }
	inline int32_t* get_address_of__line_2() { return &____line_2; }
	inline void set__line_2(int32_t value)
	{
		____line_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
