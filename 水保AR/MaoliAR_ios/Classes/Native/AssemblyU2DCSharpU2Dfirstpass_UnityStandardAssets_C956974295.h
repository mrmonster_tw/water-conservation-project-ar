﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_C577622288.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1593743734.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3609884370.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.DepthOfField/GlobalSettings
struct  GlobalSettings_t956974295 
{
public:
	// System.Boolean UnityStandardAssets.CinematicEffects.DepthOfField/GlobalSettings::visualizeFocus
	bool ___visualizeFocus_0;
	// UnityStandardAssets.CinematicEffects.DepthOfField/TweakMode UnityStandardAssets.CinematicEffects.DepthOfField/GlobalSettings::tweakMode
	int32_t ___tweakMode_1;
	// UnityStandardAssets.CinematicEffects.DepthOfField/QualityPreset UnityStandardAssets.CinematicEffects.DepthOfField/GlobalSettings::filteringQuality
	int32_t ___filteringQuality_2;
	// UnityStandardAssets.CinematicEffects.DepthOfField/ApertureShape UnityStandardAssets.CinematicEffects.DepthOfField/GlobalSettings::apertureShape
	int32_t ___apertureShape_3;
	// System.Single UnityStandardAssets.CinematicEffects.DepthOfField/GlobalSettings::apertureOrientation
	float ___apertureOrientation_4;

public:
	inline static int32_t get_offset_of_visualizeFocus_0() { return static_cast<int32_t>(offsetof(GlobalSettings_t956974295, ___visualizeFocus_0)); }
	inline bool get_visualizeFocus_0() const { return ___visualizeFocus_0; }
	inline bool* get_address_of_visualizeFocus_0() { return &___visualizeFocus_0; }
	inline void set_visualizeFocus_0(bool value)
	{
		___visualizeFocus_0 = value;
	}

	inline static int32_t get_offset_of_tweakMode_1() { return static_cast<int32_t>(offsetof(GlobalSettings_t956974295, ___tweakMode_1)); }
	inline int32_t get_tweakMode_1() const { return ___tweakMode_1; }
	inline int32_t* get_address_of_tweakMode_1() { return &___tweakMode_1; }
	inline void set_tweakMode_1(int32_t value)
	{
		___tweakMode_1 = value;
	}

	inline static int32_t get_offset_of_filteringQuality_2() { return static_cast<int32_t>(offsetof(GlobalSettings_t956974295, ___filteringQuality_2)); }
	inline int32_t get_filteringQuality_2() const { return ___filteringQuality_2; }
	inline int32_t* get_address_of_filteringQuality_2() { return &___filteringQuality_2; }
	inline void set_filteringQuality_2(int32_t value)
	{
		___filteringQuality_2 = value;
	}

	inline static int32_t get_offset_of_apertureShape_3() { return static_cast<int32_t>(offsetof(GlobalSettings_t956974295, ___apertureShape_3)); }
	inline int32_t get_apertureShape_3() const { return ___apertureShape_3; }
	inline int32_t* get_address_of_apertureShape_3() { return &___apertureShape_3; }
	inline void set_apertureShape_3(int32_t value)
	{
		___apertureShape_3 = value;
	}

	inline static int32_t get_offset_of_apertureOrientation_4() { return static_cast<int32_t>(offsetof(GlobalSettings_t956974295, ___apertureOrientation_4)); }
	inline float get_apertureOrientation_4() const { return ___apertureOrientation_4; }
	inline float* get_address_of_apertureOrientation_4() { return &___apertureOrientation_4; }
	inline void set_apertureOrientation_4(float value)
	{
		___apertureOrientation_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityStandardAssets.CinematicEffects.DepthOfField/GlobalSettings
struct GlobalSettings_t956974295_marshaled_pinvoke
{
	int32_t ___visualizeFocus_0;
	int32_t ___tweakMode_1;
	int32_t ___filteringQuality_2;
	int32_t ___apertureShape_3;
	float ___apertureOrientation_4;
};
// Native definition for COM marshalling of UnityStandardAssets.CinematicEffects.DepthOfField/GlobalSettings
struct GlobalSettings_t956974295_marshaled_com
{
	int32_t ___visualizeFocus_0;
	int32_t ___tweakMode_1;
	int32_t ___filteringQuality_2;
	int32_t ___apertureShape_3;
	float ___apertureOrientation_4;
};
