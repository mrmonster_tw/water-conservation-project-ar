﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_I2309025479.h"

// System.ServiceModel.Channels.TcpChannelInfo
struct TcpChannelInfo_t709592533;
// System.Net.Sockets.TcpListener
struct TcpListener_t3499576757;
// System.Collections.Generic.List`1<System.Threading.ManualResetEvent>
struct List_1_t1923316752;
// System.Collections.Generic.List`1<System.ServiceModel.Channels.IReplyChannel>
struct List_1_t1033727036;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.TcpChannelListener`1<System.ServiceModel.Channels.IReplyChannel>
struct  TcpChannelListener_1_t2995356293  : public InternalChannelListenerBase_1_t2309025479
{
public:
	// System.ServiceModel.Channels.TcpChannelInfo System.ServiceModel.Channels.TcpChannelListener`1::info
	TcpChannelInfo_t709592533 * ___info_16;
	// System.Net.Sockets.TcpListener System.ServiceModel.Channels.TcpChannelListener`1::tcp_listener
	TcpListener_t3499576757 * ___tcp_listener_17;
	// System.Collections.Generic.List`1<System.Threading.ManualResetEvent> System.ServiceModel.Channels.TcpChannelListener`1::accept_handles
	List_1_t1923316752 * ___accept_handles_18;
	// System.Collections.Generic.List`1<TChannel> System.ServiceModel.Channels.TcpChannelListener`1::accepted_channels
	List_1_t1033727036 * ___accepted_channels_19;

public:
	inline static int32_t get_offset_of_info_16() { return static_cast<int32_t>(offsetof(TcpChannelListener_1_t2995356293, ___info_16)); }
	inline TcpChannelInfo_t709592533 * get_info_16() const { return ___info_16; }
	inline TcpChannelInfo_t709592533 ** get_address_of_info_16() { return &___info_16; }
	inline void set_info_16(TcpChannelInfo_t709592533 * value)
	{
		___info_16 = value;
		Il2CppCodeGenWriteBarrier(&___info_16, value);
	}

	inline static int32_t get_offset_of_tcp_listener_17() { return static_cast<int32_t>(offsetof(TcpChannelListener_1_t2995356293, ___tcp_listener_17)); }
	inline TcpListener_t3499576757 * get_tcp_listener_17() const { return ___tcp_listener_17; }
	inline TcpListener_t3499576757 ** get_address_of_tcp_listener_17() { return &___tcp_listener_17; }
	inline void set_tcp_listener_17(TcpListener_t3499576757 * value)
	{
		___tcp_listener_17 = value;
		Il2CppCodeGenWriteBarrier(&___tcp_listener_17, value);
	}

	inline static int32_t get_offset_of_accept_handles_18() { return static_cast<int32_t>(offsetof(TcpChannelListener_1_t2995356293, ___accept_handles_18)); }
	inline List_1_t1923316752 * get_accept_handles_18() const { return ___accept_handles_18; }
	inline List_1_t1923316752 ** get_address_of_accept_handles_18() { return &___accept_handles_18; }
	inline void set_accept_handles_18(List_1_t1923316752 * value)
	{
		___accept_handles_18 = value;
		Il2CppCodeGenWriteBarrier(&___accept_handles_18, value);
	}

	inline static int32_t get_offset_of_accepted_channels_19() { return static_cast<int32_t>(offsetof(TcpChannelListener_1_t2995356293, ___accepted_channels_19)); }
	inline List_1_t1033727036 * get_accepted_channels_19() const { return ___accepted_channels_19; }
	inline List_1_t1033727036 ** get_address_of_accepted_channels_19() { return &___accepted_channels_19; }
	inline void set_accepted_channels_19(List_1_t1033727036 * value)
	{
		___accepted_channels_19 = value;
		Il2CppCodeGenWriteBarrier(&___accepted_channels_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
