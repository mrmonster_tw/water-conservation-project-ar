﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Configuration.SoapExtensionTypeElement
struct  SoapExtensionTypeElement_t432867097  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct SoapExtensionTypeElement_t432867097_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Web.Services.Configuration.SoapExtensionTypeElement::groupProp
	ConfigurationProperty_t3590861854 * ___groupProp_13;
	// System.Configuration.ConfigurationProperty System.Web.Services.Configuration.SoapExtensionTypeElement::priorityProp
	ConfigurationProperty_t3590861854 * ___priorityProp_14;
	// System.Configuration.ConfigurationProperty System.Web.Services.Configuration.SoapExtensionTypeElement::typeProp
	ConfigurationProperty_t3590861854 * ___typeProp_15;
	// System.Configuration.ConfigurationPropertyCollection System.Web.Services.Configuration.SoapExtensionTypeElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_16;

public:
	inline static int32_t get_offset_of_groupProp_13() { return static_cast<int32_t>(offsetof(SoapExtensionTypeElement_t432867097_StaticFields, ___groupProp_13)); }
	inline ConfigurationProperty_t3590861854 * get_groupProp_13() const { return ___groupProp_13; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_groupProp_13() { return &___groupProp_13; }
	inline void set_groupProp_13(ConfigurationProperty_t3590861854 * value)
	{
		___groupProp_13 = value;
		Il2CppCodeGenWriteBarrier(&___groupProp_13, value);
	}

	inline static int32_t get_offset_of_priorityProp_14() { return static_cast<int32_t>(offsetof(SoapExtensionTypeElement_t432867097_StaticFields, ___priorityProp_14)); }
	inline ConfigurationProperty_t3590861854 * get_priorityProp_14() const { return ___priorityProp_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_priorityProp_14() { return &___priorityProp_14; }
	inline void set_priorityProp_14(ConfigurationProperty_t3590861854 * value)
	{
		___priorityProp_14 = value;
		Il2CppCodeGenWriteBarrier(&___priorityProp_14, value);
	}

	inline static int32_t get_offset_of_typeProp_15() { return static_cast<int32_t>(offsetof(SoapExtensionTypeElement_t432867097_StaticFields, ___typeProp_15)); }
	inline ConfigurationProperty_t3590861854 * get_typeProp_15() const { return ___typeProp_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_typeProp_15() { return &___typeProp_15; }
	inline void set_typeProp_15(ConfigurationProperty_t3590861854 * value)
	{
		___typeProp_15 = value;
		Il2CppCodeGenWriteBarrier(&___typeProp_15, value);
	}

	inline static int32_t get_offset_of_properties_16() { return static_cast<int32_t>(offsetof(SoapExtensionTypeElement_t432867097_StaticFields, ___properties_16)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_16() const { return ___properties_16; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_16() { return &___properties_16; }
	inline void set_properties_16(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_16 = value;
		Il2CppCodeGenWriteBarrier(&___properties_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
