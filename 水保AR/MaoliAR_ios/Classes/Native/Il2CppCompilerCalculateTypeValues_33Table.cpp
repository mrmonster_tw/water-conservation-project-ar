﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Web_System_Web_Handlers_AssemblyResourceLoa1593975559.h"
#include "System_Web_System_Web_Hosting_ApplicationHost2589283385.h"
#include "System_Web_System_Web_Hosting_DefaultVirtualDirect1528118306.h"
#include "System_Web_System_Web_Hosting_DefaultVirtualFile1678122423.h"
#include "System_Web_System_Web_Hosting_DefaultVirtualPathPr3381095642.h"
#include "System_Web_System_Web_Hosting_HostingEnvironment3287307516.h"
#include "System_Web_System_Web_Hosting_SimpleWorkerRequest1223921380.h"
#include "System_Web_System_Web_Hosting_VirtualDirectory3182525004.h"
#include "System_Web_System_Web_Hosting_VirtualFileBase3533200965.h"
#include "System_Web_System_Web_Hosting_VirtualFile2726832715.h"
#include "System_Web_System_Web_Hosting_VirtualPathProvider2835917181.h"
#include "System_Web_System_Web_BaseParamsCollection196591189.h"
#include "System_Web_System_Web_HeadersCollection2753901807.h"
#include "System_Web_System_Web_HtmlizedException2999156009.h"
#include "System_Web_System_Web_HttpApplication3359645917.h"
#include "System_Web_System_Web_HttpApplication_U3CRunHooksU1780941747.h"
#include "System_Web_System_Web_HttpApplication_U3CPipelineU2410703380.h"
#include "System_Web_System_Web_AsyncRequestState2506828551.h"
#include "System_Web_System_Web_AsyncInvoker1593358297.h"
#include "System_Web_System_Web_HttpApplicationFactory4230706105.h"
#include "System_Web_System_Web_HttpApplicationState2618076950.h"
#include "System_Web_System_Web_HttpBrowserCapabilities4010224613.h"
#include "System_Web_System_Web_HttpCacheability811143856.h"
#include "System_Web_System_Web_HttpCachePolicy379126981.h"
#include "System_Web_System_Web_HttpCacheVaryByContentEncodi1137512671.h"
#include "System_Web_System_Web_HttpCacheVaryByHeaders2217784971.h"
#include "System_Web_System_Web_HttpCacheVaryByParams2818058713.h"
#include "System_Web_System_Web_HttpContext1969259010.h"
#include "System_Web_System_Web_StepTimeout2723136115.h"
#include "System_Web_System_Web_HttpCookieCollection1178559666.h"
#include "System_Web_System_Web_CookieFlags3398431624.h"
#include "System_Web_System_Web_HttpCookie2077767070.h"
#include "System_Web_System_Web_HttpCookie_CookieNVC2003193884.h"
#include "System_Web_System_Web_HttpCookieMode2360372362.h"
#include "System_Web_System_Web_HttpException2907797370.h"
#include "System_Web_System_Web_HttpFileCollection355459549.h"
#include "System_Web_System_Web_HttpModuleCollection38260146.h"
#include "System_Web_System_Web_HttpParseException2218887192.h"
#include "System_Web_System_Web_HttpPostedFile2439047419.h"
#include "System_Web_System_Web_HttpPostedFile_ReadSubStream1898399504.h"
#include "System_Web_System_Web_HttpRequest809700260.h"
#include "System_Web_System_Web_HttpMultipart476075009.h"
#include "System_Web_System_Web_HttpMultipart_Element3560275022.h"
#include "System_Web_System_Web_HttpRequestValidationExcepti2551422396.h"
#include "System_Web_System_Web_HttpResponseStream38354555.h"
#include "System_Web_System_Web_HttpResponseStream_BlockManag141966076.h"
#include "System_Web_System_Web_HttpResponseStream_Bucket1453203164.h"
#include "System_Web_System_Web_HttpResponseStream_ByteBucke3684624432.h"
#include "System_Web_System_Web_HttpResponseStream_BufferedF2241899881.h"
#include "System_Web_System_Web_HttpResponse1542361753.h"
#include "System_Web_System_Web_FlagEnd3393771211.h"
#include "System_Web_System_Web_HttpRuntime1480753741.h"
#include "System_Web_System_Web_HttpServerUtility3850680178.h"
#include "System_Web_System_Web_HttpStaticObjectsCollection518175362.h"
#include "System_Web_System_Web_HttpStaticObjectsCollection_3669534551.h"
#include "System_Web_System_Web_HttpUnhandledException2437890896.h"
#include "System_Web_System_Web_HttpUtility1255642059.h"
#include "System_Web_System_Web_HttpWorkerRequest998871775.h"
#include "System_Web_System_Web_HttpWriter2060157611.h"
#include "System_Web_System_Web_IntPtrStream1988557108.h"
#include "System_Web_System_Web_InputFilterStream4129443631.h"
#include "System_Web_System_Web_NoParamsInvoker2754751357.h"
#include "System_Web_System_Web_ParserErrorCollection485049528.h"
#include "System_Web_System_Web_ParserError3362544569.h"
#include "System_Web_System_Web_Profile_ProfileBase2370935045.h"
#include "System_Web_System_Web_QueueManager3850794979.h"
#include "System_Web_System_Web_Security_DefaultAuthenticatio645436702.h"
#include "System_Web_System_Web_Security_DefaultAuthenticati1472662056.h"
#include "System_Web_System_Web_Security_MembershipProvider1994512972.h"
#include "System_Web_System_Web_Security_RoleProvider1137021788.h"
#include "System_Web_System_Web_ServerVariablesCollection3116558453.h"
#include "System_Web_System_Web_SessionState_SessionId738760722.h"
#include "System_Web_System_Web_SessionState_SessionStateMod3002328156.h"
#include "System_Web_System_Web_SessionState_HttpSessionStateC50694200.h"
#include "System_Web_System_Web_SessionState_HttpSessionStat3146594024.h"
#include "System_Web_System_Web_SessionState_LockableStateSe2171525728.h"
#include "System_Web_System_Web_SessionState_RemoteStateServ3071674104.h"
#include "System_Web_System_Web_SessionState_SessionIDManage3564083178.h"
#include "System_Web_System_Web_SessionState_InProcSessionIt3251217927.h"
#include "System_Web_System_Web_SessionState_SessionInProcHa2831147200.h"
#include "System_Web_System_Web_SessionState_SessionStateAct2424179227.h"
#include "System_Web_System_Web_SessionState_SessionStateIte3993009625.h"
#include "System_Web_System_Web_SessionState_SessionStateMod1404169380.h"
#include "System_Web_System_Web_SessionState_SessionStateModu123990870.h"
#include "System_Web_System_Web_SessionState_SessionStateServ103977851.h"
#include "System_Web_System_Web_SessionState_SessionStateSto4177544864.h"
#include "System_Web_System_Web_SessionState_SessionStateStor182923317.h"
#include "System_Web_System_Web_SessionState_SessionStateUti3346029770.h"
#include "System_Web_System_Web_SessionState_StateServerItem1723435776.h"
#include "System_Web_System_Web_TempFileStream420614451.h"
#include "System_Web_System_Web_TraceContext3886895271.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3300 = { sizeof (PerformSubstitutionHelper_t1593975559), -1, sizeof(PerformSubstitutionHelper_t1593975559_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3300[2] = 
{
	PerformSubstitutionHelper_t1593975559::get_offset_of__assembly_0(),
	PerformSubstitutionHelper_t1593975559_StaticFields::get_offset_of__regex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3301 = { sizeof (ApplicationHost_t2589283385), -1, sizeof(ApplicationHost_t2589283385_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3301[3] = 
{
	ApplicationHost_t2589283385_StaticFields::get_offset_of_MonoHostedDataKey_0(),
	ApplicationHost_t2589283385_StaticFields::get_offset_of_WebConfigFileNames_1(),
	ApplicationHost_t2589283385_StaticFields::get_offset_of_create_dir_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3302 = { sizeof (DefaultVirtualDirectory_t1528118306), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3302[2] = 
{
	DefaultVirtualDirectory_t1528118306::get_offset_of_phys_dir_2(),
	DefaultVirtualDirectory_t1528118306::get_offset_of_virtual_dir_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3303 = { sizeof (DefaultVirtualFile_t1678122423), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3304 = { sizeof (DefaultVirtualPathProvider_t3381095642), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3305 = { sizeof (HostingEnvironment_t3287307516), -1, sizeof(HostingEnvironment_t3287307516_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3305[1] = 
{
	HostingEnvironment_t3287307516_StaticFields::get_offset_of_vpath_provider_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3306 = { sizeof (SimpleWorkerRequest_t1223921380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3306[7] = 
{
	SimpleWorkerRequest_t1223921380::get_offset_of_page_3(),
	SimpleWorkerRequest_t1223921380::get_offset_of_query_4(),
	SimpleWorkerRequest_t1223921380::get_offset_of_app_virtual_dir_5(),
	SimpleWorkerRequest_t1223921380::get_offset_of_app_physical_dir_6(),
	SimpleWorkerRequest_t1223921380::get_offset_of_path_info_7(),
	SimpleWorkerRequest_t1223921380::get_offset_of_output_8(),
	SimpleWorkerRequest_t1223921380::get_offset_of_hosted_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3307 = { sizeof (VirtualDirectory_t3182525004), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3308 = { sizeof (VirtualFileBase_t3533200965), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3308[1] = 
{
	VirtualFileBase_t3533200965::get_offset_of_vpath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3309 = { sizeof (VirtualFile_t2726832715), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3310 = { sizeof (VirtualPathProvider_t2835917181), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3310[1] = 
{
	VirtualPathProvider_t2835917181::get_offset_of_prev_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3311 = { sizeof (BaseParamsCollection_t196591189), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3311[2] = 
{
	BaseParamsCollection_t196591189::get_offset_of__request_14(),
	BaseParamsCollection_t196591189::get_offset_of__loaded_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3312 = { sizeof (HeadersCollection_t2753901807), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3313 = { sizeof (HtmlizedException_t2999156009), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3314 = { sizeof (HttpApplication_t3359645917), -1, sizeof(HttpApplication_t3359645917_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3314[55] = 
{
	HttpApplication_t3359645917_StaticFields::get_offset_of_disposedEvent_0(),
	HttpApplication_t3359645917_StaticFields::get_offset_of_errorEvent_1(),
	HttpApplication_t3359645917_StaticFields::get_offset_of_requests_total_counter_2(),
	HttpApplication_t3359645917_StaticFields::get_offset_of_BinDirs_3(),
	HttpApplication_t3359645917::get_offset_of_this_lock_4(),
	HttpApplication_t3359645917::get_offset_of_context_5(),
	HttpApplication_t3359645917::get_offset_of_session_6(),
	HttpApplication_t3359645917::get_offset_of_isite_7(),
	HttpApplication_t3359645917::get_offset_of_modcoll_8(),
	HttpApplication_t3359645917::get_offset_of_assemblyLocation_9(),
	HttpApplication_t3359645917::get_offset_of_factory_10(),
	HttpApplication_t3359645917::get_offset_of_autoCulture_11(),
	HttpApplication_t3359645917::get_offset_of_autoUICulture_12(),
	HttpApplication_t3359645917::get_offset_of_stop_processing_13(),
	HttpApplication_t3359645917::get_offset_of_in_application_start_14(),
	HttpApplication_t3359645917::get_offset_of_pipeline_15(),
	HttpApplication_t3359645917::get_offset_of_done_16(),
	HttpApplication_t3359645917::get_offset_of_begin_iar_17(),
	HttpApplication_t3359645917::get_offset_of_current_ai_18(),
	HttpApplication_t3359645917::get_offset_of_events_19(),
	HttpApplication_t3359645917::get_offset_of_nonApplicationEvents_20(),
	HttpApplication_t3359645917::get_offset_of_app_culture_21(),
	HttpApplication_t3359645917::get_offset_of_appui_culture_22(),
	HttpApplication_t3359645917::get_offset_of_prev_app_culture_23(),
	HttpApplication_t3359645917::get_offset_of_prev_appui_culture_24(),
	HttpApplication_t3359645917::get_offset_of_prev_user_25(),
	HttpApplication_t3359645917_StaticFields::get_offset_of_binDirectory_26(),
	HttpApplication_t3359645917_StaticFields::get_offset_of_initialization_exception_27(),
	HttpApplication_t3359645917::get_offset_of_removeConfigurationFromCache_28(),
	HttpApplication_t3359645917::get_offset_of_fullInitComplete_29(),
	HttpApplication_t3359645917::get_offset_of_must_yield_30(),
	HttpApplication_t3359645917::get_offset_of_in_begin_31(),
	HttpApplication_t3359645917_StaticFields::get_offset_of_PreSendRequestHeadersEvent_32(),
	HttpApplication_t3359645917_StaticFields::get_offset_of_PreSendRequestContentEvent_33(),
	HttpApplication_t3359645917_StaticFields::get_offset_of_AcquireRequestStateEvent_34(),
	HttpApplication_t3359645917_StaticFields::get_offset_of_AuthenticateRequestEvent_35(),
	HttpApplication_t3359645917_StaticFields::get_offset_of_AuthorizeRequestEvent_36(),
	HttpApplication_t3359645917_StaticFields::get_offset_of_BeginRequestEvent_37(),
	HttpApplication_t3359645917_StaticFields::get_offset_of_EndRequestEvent_38(),
	HttpApplication_t3359645917_StaticFields::get_offset_of_PostRequestHandlerExecuteEvent_39(),
	HttpApplication_t3359645917_StaticFields::get_offset_of_PreRequestHandlerExecuteEvent_40(),
	HttpApplication_t3359645917_StaticFields::get_offset_of_ReleaseRequestStateEvent_41(),
	HttpApplication_t3359645917_StaticFields::get_offset_of_ResolveRequestCacheEvent_42(),
	HttpApplication_t3359645917_StaticFields::get_offset_of_UpdateRequestCacheEvent_43(),
	HttpApplication_t3359645917_StaticFields::get_offset_of_PostAuthenticateRequestEvent_44(),
	HttpApplication_t3359645917_StaticFields::get_offset_of_PostAuthorizeRequestEvent_45(),
	HttpApplication_t3359645917_StaticFields::get_offset_of_PostResolveRequestCacheEvent_46(),
	HttpApplication_t3359645917_StaticFields::get_offset_of_PostMapRequestHandlerEvent_47(),
	HttpApplication_t3359645917_StaticFields::get_offset_of_PostAcquireRequestStateEvent_48(),
	HttpApplication_t3359645917_StaticFields::get_offset_of_PostReleaseRequestStateEvent_49(),
	HttpApplication_t3359645917_StaticFields::get_offset_of_PostUpdateRequestCacheEvent_50(),
	HttpApplication_t3359645917_StaticFields::get_offset_of_LogRequestEvent_51(),
	HttpApplication_t3359645917_StaticFields::get_offset_of_MapRequestHandlerEvent_52(),
	HttpApplication_t3359645917_StaticFields::get_offset_of_PostLogRequestEvent_53(),
	HttpApplication_t3359645917::get_offset_of_DefaultAuthentication_54(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3315 = { sizeof (U3CRunHooksU3Ec__Iterator1_t1780941747), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3315[9] = 
{
	U3CRunHooksU3Ec__Iterator1_t1780941747::get_offset_of_list_0(),
	U3CRunHooksU3Ec__Iterator1_t1780941747::get_offset_of_U3CdelegatesU3E__0_1(),
	U3CRunHooksU3Ec__Iterator1_t1780941747::get_offset_of_U3CU24s_355U3E__1_2(),
	U3CRunHooksU3Ec__Iterator1_t1780941747::get_offset_of_U3CU24s_356U3E__2_3(),
	U3CRunHooksU3Ec__Iterator1_t1780941747::get_offset_of_U3CdU3E__3_4(),
	U3CRunHooksU3Ec__Iterator1_t1780941747::get_offset_of_U24PC_5(),
	U3CRunHooksU3Ec__Iterator1_t1780941747::get_offset_of_U24current_6(),
	U3CRunHooksU3Ec__Iterator1_t1780941747::get_offset_of_U3CU24U3Elist_7(),
	U3CRunHooksU3Ec__Iterator1_t1780941747::get_offset_of_U3CU3Ef__this_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3316 = { sizeof (U3CPipelineU3Ec__Iterator2_t2410703380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3316[50] = 
{
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CeventHandlerU3E__0_0(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CU24s_357U3E__1_1(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CstopU3E__2_2(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CU24s_358U3E__3_3(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CstopU3E__4_4(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CU24s_359U3E__5_5(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CstopU3E__6_6(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CU24s_360U3E__7_7(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CstopU3E__8_8(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CU24s_361U3E__9_9(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CstopU3E__10_10(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CU24s_362U3E__11_11(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CstopU3E__12_12(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CU24s_363U3E__13_13(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CstopU3E__14_14(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CU24s_364U3E__15_15(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CstopU3E__16_16(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CU24s_365U3E__17_17(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CstopU3E__18_18(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3ChandlerU3E__19_19(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CfnfU3E__20_20(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CdnfU3E__21_21(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CeU3E__22_22(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CU24s_366U3E__23_23(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CstopU3E__24_24(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CU24s_367U3E__25_25(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CstopU3E__26_26(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CU24s_368U3E__27_27(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CstopU3E__28_28(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CU24s_369U3E__29_29(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CstopU3E__30_30(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CctxHandlerU3E__31_31(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3Casync_handlerU3E__32_32(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CU24s_370U3E__33_33(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CstopU3E__34_34(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CU24s_371U3E__35_35(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CstopU3E__36_36(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CU24s_372U3E__37_37(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CstopU3E__38_38(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CU24s_373U3E__39_39(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CstopU3E__40_40(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CU24s_374U3E__41_41(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CstopU3E__42_42(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CU24s_375U3E__43_43(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CstopU3E__44_44(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CU24s_376U3E__45_45(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CstopU3E__46_46(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U24PC_47(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U24current_48(),
	U3CPipelineU3Ec__Iterator2_t2410703380::get_offset_of_U3CU3Ef__this_49(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3317 = { sizeof (AsyncRequestState_t2506828551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3317[4] = 
{
	AsyncRequestState_t2506828551::get_offset_of_cb_0(),
	AsyncRequestState_t2506828551::get_offset_of_cb_data_1(),
	AsyncRequestState_t2506828551::get_offset_of_completed_2(),
	AsyncRequestState_t2506828551::get_offset_of_complete_event_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3318 = { sizeof (AsyncInvoker_t1593358297), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3318[3] = 
{
	AsyncInvoker_t1593358297::get_offset_of_begin_0(),
	AsyncInvoker_t1593358297::get_offset_of_end_1(),
	AsyncInvoker_t1593358297::get_offset_of_data_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3319 = { sizeof (HttpApplicationFactory_t4230706105), -1, sizeof(HttpApplicationFactory_t4230706105_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3319[21] = 
{
	HttpApplicationFactory_t4230706105::get_offset_of_this_lock_0(),
	HttpApplicationFactory_t4230706105_StaticFields::get_offset_of_theFactory_1(),
	HttpApplicationFactory_t4230706105::get_offset_of_session_end_2(),
	HttpApplicationFactory_t4230706105::get_offset_of_needs_init_3(),
	HttpApplicationFactory_t4230706105::get_offset_of_app_start_needed_4(),
	HttpApplicationFactory_t4230706105::get_offset_of_have_app_events_5(),
	HttpApplicationFactory_t4230706105::get_offset_of_app_type_6(),
	HttpApplicationFactory_t4230706105::get_offset_of_app_state_7(),
	HttpApplicationFactory_t4230706105::get_offset_of_app_event_handlers_8(),
	HttpApplicationFactory_t4230706105_StaticFields::get_offset_of_watchers_9(),
	HttpApplicationFactory_t4230706105_StaticFields::get_offset_of_watchers_lock_10(),
	HttpApplicationFactory_t4230706105_StaticFields::get_offset_of_app_shutdown_11(),
	HttpApplicationFactory_t4230706105_StaticFields::get_offset_of_app_disabled_12(),
	HttpApplicationFactory_t4230706105_StaticFields::get_offset_of_app_browsers_files_13(),
	HttpApplicationFactory_t4230706105_StaticFields::get_offset_of_default_machine_browsers_files_14(),
	HttpApplicationFactory_t4230706105_StaticFields::get_offset_of_app_mono_machine_browsers_files_15(),
	HttpApplicationFactory_t4230706105::get_offset_of_available_16(),
	HttpApplicationFactory_t4230706105::get_offset_of_next_free_17(),
	HttpApplicationFactory_t4230706105::get_offset_of_available_for_end_18(),
	HttpApplicationFactory_t4230706105_StaticFields::get_offset_of_capabilities_processor_19(),
	HttpApplicationFactory_t4230706105_StaticFields::get_offset_of_capabilities_processor_lock_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3320 = { sizeof (HttpApplicationState_t2618076950), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3320[3] = 
{
	HttpApplicationState_t2618076950::get_offset_of__AppObjects_10(),
	HttpApplicationState_t2618076950::get_offset_of__SessionObjects_11(),
	HttpApplicationState_t2618076950::get_offset_of__Lock_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3321 = { sizeof (HttpBrowserCapabilities_t4010224613), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3322 = { sizeof (HttpCacheability_t811143856)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3322[7] = 
{
	HttpCacheability_t811143856::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3323 = { sizeof (HttpCachePolicy_t379126981), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3323[18] = 
{
	HttpCachePolicy_t379126981::get_offset_of_vary_by_content_encodings_0(),
	HttpCachePolicy_t379126981::get_offset_of_vary_by_headers_1(),
	HttpCachePolicy_t379126981::get_offset_of_vary_by_params_2(),
	HttpCachePolicy_t379126981::get_offset_of_Cacheability_3(),
	HttpCachePolicy_t379126981::get_offset_of_etag_4(),
	HttpCachePolicy_t379126981::get_offset_of_etag_from_file_dependencies_5(),
	HttpCachePolicy_t379126981::get_offset_of_last_modified_from_file_dependencies_6(),
	HttpCachePolicy_t379126981::get_offset_of_have_expire_date_7(),
	HttpCachePolicy_t379126981::get_offset_of_expire_date_8(),
	HttpCachePolicy_t379126981::get_offset_of_have_last_modified_9(),
	HttpCachePolicy_t379126981::get_offset_of_last_modified_10(),
	HttpCachePolicy_t379126981::get_offset_of_HaveMaxAge_11(),
	HttpCachePolicy_t379126981::get_offset_of_MaxAge_12(),
	HttpCachePolicy_t379126981::get_offset_of_allow_response_in_browser_history_13(),
	HttpCachePolicy_t379126981::get_offset_of_allow_server_caching_14(),
	HttpCachePolicy_t379126981::get_offset_of_set_no_store_15(),
	HttpCachePolicy_t379126981::get_offset_of_set_no_transform_16(),
	HttpCachePolicy_t379126981::get_offset_of_valid_until_expires_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3324 = { sizeof (HttpCacheVaryByContentEncodings_t1137512671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3324[1] = 
{
	HttpCacheVaryByContentEncodings_t1137512671::get_offset_of_encodings_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3325 = { sizeof (HttpCacheVaryByHeaders_t2217784971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3325[1] = 
{
	HttpCacheVaryByHeaders_t2217784971::get_offset_of_fields_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3326 = { sizeof (HttpCacheVaryByParams_t2818058713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3326[2] = 
{
	HttpCacheVaryByParams_t2818058713::get_offset_of_ignore_parms_0(),
	HttpCacheVaryByParams_t2818058713::get_offset_of_parms_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3327 = { sizeof (HttpContext_t1969259010), -1, sizeof(HttpContext_t1969259010_StaticFields), sizeof(HttpContext_t1969259010_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable3327[24] = 
{
	HttpContext_t1969259010::get_offset_of_WorkerRequest_0(),
	HttpContext_t1969259010::get_offset_of_app_instance_1(),
	HttpContext_t1969259010::get_offset_of_request_2(),
	HttpContext_t1969259010::get_offset_of_response_3(),
	HttpContext_t1969259010::get_offset_of_session_state_4(),
	HttpContext_t1969259010::get_offset_of_server_5(),
	HttpContext_t1969259010::get_offset_of_trace_context_6(),
	HttpContext_t1969259010::get_offset_of_handler_7(),
	HttpContext_t1969259010::get_offset_of_error_page_8(),
	HttpContext_t1969259010::get_offset_of_user_9(),
	HttpContext_t1969259010::get_offset_of_errors_10(),
	HttpContext_t1969259010::get_offset_of_items_11(),
	HttpContext_t1969259010::get_offset_of_config_timeout_12(),
	HttpContext_t1969259010::get_offset_of_timeout_possible_13(),
	HttpContext_t1969259010::get_offset_of_time_stamp_14(),
	HttpContext_t1969259010::get_offset_of_timer_15(),
	HttpContext_t1969259010::get_offset_of_thread_16(),
	HttpContext_t1969259010::get_offset_of__isProcessingInclude_17(),
	THREAD_STATIC_FIELD_OFFSET,
	THREAD_STATIC_FIELD_OFFSET,
	THREAD_STATIC_FIELD_OFFSET,
	HttpContext_t1969259010_StaticFields::get_offset_of_AppGlobalResourcesAssembly_21(),
	HttpContext_t1969259010::get_offset_of_handlers_22(),
	HttpContext_t1969259010::get_offset_of_U3CMapRequestHandlerDoneU3Ek__BackingField_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3328 = { sizeof (StepTimeout_t2723136115), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3329 = { sizeof (HttpCookieCollection_t1178559666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3329[1] = 
{
	HttpCookieCollection_t1178559666::get_offset_of_auto_fill_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3330 = { sizeof (CookieFlags_t3398431624)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3330[3] = 
{
	CookieFlags_t3398431624::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3331 = { sizeof (HttpCookie_t2077767070), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3331[6] = 
{
	HttpCookie_t2077767070::get_offset_of_path_0(),
	HttpCookie_t2077767070::get_offset_of_domain_1(),
	HttpCookie_t2077767070::get_offset_of_expires_2(),
	HttpCookie_t2077767070::get_offset_of_name_3(),
	HttpCookie_t2077767070::get_offset_of_flags_4(),
	HttpCookie_t2077767070::get_offset_of_values_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3332 = { sizeof (CookieNVC_t2003193884), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3333 = { sizeof (HttpCookieMode_t2360372362)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3333[5] = 
{
	HttpCookieMode_t2360372362::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3334 = { sizeof (HttpException_t2907797370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3334[3] = 
{
	HttpException_t2907797370::get_offset_of_http_code_11(),
	HttpException_t2907797370::get_offset_of_resource_name_12(),
	HttpException_t2907797370::get_offset_of_description_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3335 = { sizeof (HttpFileCollection_t355459549), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3336 = { sizeof (HttpModuleCollection_t38260146), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3337 = { sizeof (HttpParseException_t2218887192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3337[3] = 
{
	HttpParseException_t2218887192::get_offset_of_line_14(),
	HttpParseException_t2218887192::get_offset_of_virtualPath_15(),
	HttpParseException_t2218887192::get_offset_of_errors_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3338 = { sizeof (HttpPostedFile_t2439047419), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3338[3] = 
{
	HttpPostedFile_t2439047419::get_offset_of_name_0(),
	HttpPostedFile_t2439047419::get_offset_of_content_type_1(),
	HttpPostedFile_t2439047419::get_offset_of_stream_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3339 = { sizeof (ReadSubStream_t1898399504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3339[4] = 
{
	ReadSubStream_t1898399504::get_offset_of_s_2(),
	ReadSubStream_t1898399504::get_offset_of_offset_3(),
	ReadSubStream_t1898399504::get_offset_of_end_4(),
	ReadSubStream_t1898399504::get_offset_of_position_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3340 = { sizeof (HttpRequest_t809700260), -1, sizeof(HttpRequest_t809700260_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3340[40] = 
{
	HttpRequest_t809700260::get_offset_of_worker_request_0(),
	HttpRequest_t809700260::get_offset_of_context_1(),
	HttpRequest_t809700260::get_offset_of_query_string_nvc_2(),
	HttpRequest_t809700260::get_offset_of_orig_url_3(),
	HttpRequest_t809700260::get_offset_of_url_components_4(),
	HttpRequest_t809700260::get_offset_of_client_target_5(),
	HttpRequest_t809700260::get_offset_of_browser_capabilities_6(),
	HttpRequest_t809700260::get_offset_of_file_path_7(),
	HttpRequest_t809700260::get_offset_of_base_virtual_dir_8(),
	HttpRequest_t809700260::get_offset_of_root_virtual_dir_9(),
	HttpRequest_t809700260::get_offset_of_client_file_path_10(),
	HttpRequest_t809700260::get_offset_of_content_type_11(),
	HttpRequest_t809700260::get_offset_of_content_length_12(),
	HttpRequest_t809700260::get_offset_of_encoding_13(),
	HttpRequest_t809700260::get_offset_of_current_exe_path_14(),
	HttpRequest_t809700260::get_offset_of_physical_path_15(),
	HttpRequest_t809700260::get_offset_of_unescaped_path_16(),
	HttpRequest_t809700260::get_offset_of_path_info_17(),
	HttpRequest_t809700260::get_offset_of_headers_18(),
	HttpRequest_t809700260::get_offset_of_input_stream_19(),
	HttpRequest_t809700260::get_offset_of_input_filter_20(),
	HttpRequest_t809700260::get_offset_of_filter_21(),
	HttpRequest_t809700260::get_offset_of_cookies_22(),
	HttpRequest_t809700260::get_offset_of_http_method_23(),
	HttpRequest_t809700260::get_offset_of_form_24(),
	HttpRequest_t809700260::get_offset_of_files_25(),
	HttpRequest_t809700260::get_offset_of_server_variables_26(),
	HttpRequest_t809700260::get_offset_of_request_type_27(),
	HttpRequest_t809700260::get_offset_of_user_languages_28(),
	HttpRequest_t809700260::get_offset_of_cached_url_29(),
	HttpRequest_t809700260::get_offset_of_request_file_30(),
	HttpRequest_t809700260_StaticFields::get_offset_of_host_addresses_31(),
	HttpRequest_t809700260::get_offset_of_validate_cookies_32(),
	HttpRequest_t809700260::get_offset_of_validate_query_string_33(),
	HttpRequest_t809700260::get_offset_of_validate_form_34(),
	HttpRequest_t809700260::get_offset_of_checked_cookies_35(),
	HttpRequest_t809700260::get_offset_of_checked_query_string_36(),
	HttpRequest_t809700260::get_offset_of_checked_form_37(),
	HttpRequest_t809700260_StaticFields::get_offset_of_urlMappings_38(),
	HttpRequest_t809700260_StaticFields::get_offset_of_queryTrimChars_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3341 = { sizeof (HttpMultipart_t476075009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3341[7] = 
{
	HttpMultipart_t476075009::get_offset_of_data_0(),
	HttpMultipart_t476075009::get_offset_of_boundary_1(),
	HttpMultipart_t476075009::get_offset_of_boundary_bytes_2(),
	HttpMultipart_t476075009::get_offset_of_buffer_3(),
	HttpMultipart_t476075009::get_offset_of_at_eof_4(),
	HttpMultipart_t476075009::get_offset_of_encoding_5(),
	HttpMultipart_t476075009::get_offset_of_sb_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3342 = { sizeof (Element_t3560275022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3342[5] = 
{
	Element_t3560275022::get_offset_of_ContentType_0(),
	Element_t3560275022::get_offset_of_Name_1(),
	Element_t3560275022::get_offset_of_Filename_2(),
	Element_t3560275022::get_offset_of_Start_3(),
	Element_t3560275022::get_offset_of_Length_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3343 = { sizeof (HttpRequestValidationException_t2551422396), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3344 = { sizeof (HttpResponseStream_t38354555), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3344[7] = 
{
	HttpResponseStream_t38354555::get_offset_of_first_bucket_2(),
	HttpResponseStream_t38354555::get_offset_of_cur_bucket_3(),
	HttpResponseStream_t38354555::get_offset_of_response_4(),
	HttpResponseStream_t38354555::get_offset_of_total_5(),
	HttpResponseStream_t38354555::get_offset_of_filter_6(),
	HttpResponseStream_t38354555::get_offset_of_chunk_buffer_7(),
	HttpResponseStream_t38354555::get_offset_of_filtering_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3345 = { sizeof (BlockManager_t141966076), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3345[3] = 
{
	BlockManager_t141966076::get_offset_of_data_0(),
	BlockManager_t141966076::get_offset_of_position_1(),
	BlockManager_t141966076::get_offset_of_block_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3346 = { sizeof (Bucket_t1453203164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3346[1] = 
{
	Bucket_t1453203164::get_offset_of_Next_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3347 = { sizeof (ByteBucket_t3684624432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3347[4] = 
{
	ByteBucket_t3684624432::get_offset_of_start_1(),
	ByteBucket_t3684624432::get_offset_of_length_2(),
	ByteBucket_t3684624432::get_offset_of_blocks_3(),
	ByteBucket_t3684624432::get_offset_of_Expandable_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3348 = { sizeof (BufferedFileBucket_t2241899881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3348[3] = 
{
	BufferedFileBucket_t2241899881::get_offset_of_file_1(),
	BufferedFileBucket_t2241899881::get_offset_of_offset_2(),
	BufferedFileBucket_t2241899881::get_offset_of_length_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3349 = { sizeof (HttpResponse_t1542361753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3349[29] = 
{
	HttpResponse_t1542361753::get_offset_of_WorkerRequest_0(),
	HttpResponse_t1542361753::get_offset_of_output_stream_1(),
	HttpResponse_t1542361753::get_offset_of_buffer_2(),
	HttpResponse_t1542361753::get_offset_of_fileDependencies_3(),
	HttpResponse_t1542361753::get_offset_of_context_4(),
	HttpResponse_t1542361753::get_offset_of_writer_5(),
	HttpResponse_t1542361753::get_offset_of_cache_policy_6(),
	HttpResponse_t1542361753::get_offset_of_encoding_7(),
	HttpResponse_t1542361753::get_offset_of_cookies_8(),
	HttpResponse_t1542361753::get_offset_of_status_code_9(),
	HttpResponse_t1542361753::get_offset_of_status_description_10(),
	HttpResponse_t1542361753::get_offset_of_content_type_11(),
	HttpResponse_t1542361753::get_offset_of_charset_12(),
	HttpResponse_t1542361753::get_offset_of_charset_set_13(),
	HttpResponse_t1542361753::get_offset_of_cached_response_14(),
	HttpResponse_t1542361753::get_offset_of_user_cache_control_15(),
	HttpResponse_t1542361753::get_offset_of_redirect_location_16(),
	HttpResponse_t1542361753::get_offset_of_version_header_17(),
	HttpResponse_t1542361753::get_offset_of_version_header_checked_18(),
	HttpResponse_t1542361753::get_offset_of_content_length_19(),
	HttpResponse_t1542361753::get_offset_of_headers_20(),
	HttpResponse_t1542361753::get_offset_of_headers_sent_21(),
	HttpResponse_t1542361753::get_offset_of_cached_headers_22(),
	HttpResponse_t1542361753::get_offset_of_transfer_encoding_23(),
	HttpResponse_t1542361753::get_offset_of_use_chunked_24(),
	HttpResponse_t1542361753::get_offset_of_closed_25(),
	HttpResponse_t1542361753::get_offset_of_suppress_content_26(),
	HttpResponse_t1542361753::get_offset_of_app_path_mod_27(),
	HttpResponse_t1542361753::get_offset_of_is_request_being_redirected_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3350 = { sizeof (FlagEnd_t3393771211), -1, sizeof(FlagEnd_t3393771211_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3350[1] = 
{
	FlagEnd_t3393771211_StaticFields::get_offset_of_Value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3351 = { sizeof (HttpRuntime_t1480753741), -1, sizeof(HttpRuntime_t1480753741_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3351[19] = 
{
	HttpRuntime_t1480753741_StaticFields::get_offset_of_caseInsensitive_0(),
	HttpRuntime_t1480753741_StaticFields::get_offset_of_runningOnWindows_1(),
	HttpRuntime_t1480753741_StaticFields::get_offset_of_isunc_2(),
	HttpRuntime_t1480753741_StaticFields::get_offset_of_monoVersion_3(),
	HttpRuntime_t1480753741_StaticFields::get_offset_of_domainUnloading_4(),
	HttpRuntime_t1480753741_StaticFields::get_offset_of_queue_manager_5(),
	HttpRuntime_t1480753741_StaticFields::get_offset_of_trace_manager_6(),
	HttpRuntime_t1480753741_StaticFields::get_offset_of_cache_7(),
	HttpRuntime_t1480753741_StaticFields::get_offset_of_internalCache_8(),
	HttpRuntime_t1480753741_StaticFields::get_offset_of_do_RealProcessRequest_9(),
	HttpRuntime_t1480753741_StaticFields::get_offset_of_initialException_10(),
	HttpRuntime_t1480753741_StaticFields::get_offset_of_firstRun_11(),
	HttpRuntime_t1480753741_StaticFields::get_offset_of_assemblyMappingEnabled_12(),
	HttpRuntime_t1480753741_StaticFields::get_offset_of_assemblyMappingLock_13(),
	HttpRuntime_t1480753741_StaticFields::get_offset_of_appOfflineLock_14(),
	HttpRuntime_t1480753741_StaticFields::get_offset_of__actual_bin_directory_15(),
	HttpRuntime_t1480753741_StaticFields::get_offset_of_app_offline_files_16(),
	HttpRuntime_t1480753741_StaticFields::get_offset_of_app_offline_file_17(),
	HttpRuntime_t1480753741_StaticFields::get_offset_of_content503_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3352 = { sizeof (HttpServerUtility_t3850680178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3352[1] = 
{
	HttpServerUtility_t3850680178::get_offset_of_context_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3353 = { sizeof (HttpStaticObjectsCollection_t518175362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3353[1] = 
{
	HttpStaticObjectsCollection_t518175362::get_offset_of__Objects_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3354 = { sizeof (StaticItem_t3669534551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3354[2] = 
{
	StaticItem_t3669534551::get_offset_of_this_lock_0(),
	StaticItem_t3669534551::get_offset_of_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3355 = { sizeof (HttpUnhandledException_t2437890896), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3356 = { sizeof (HttpUtility_t1255642059), -1, sizeof(HttpUtility_t1255642059_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3356[3] = 
{
	HttpUtility_t1255642059_StaticFields::get_offset_of_entities_0(),
	HttpUtility_t1255642059_StaticFields::get_offset_of_lock__1(),
	HttpUtility_t1255642059_StaticFields::get_offset_of_hexChars_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3357 = { sizeof (HttpWorkerRequest_t998871775), -1, sizeof(HttpWorkerRequest_t998871775_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3357[3] = 
{
	HttpWorkerRequest_t998871775_StaticFields::get_offset_of_RequestHeaderIndexer_0(),
	HttpWorkerRequest_t998871775_StaticFields::get_offset_of_ResponseHeaderIndexer_1(),
	HttpWorkerRequest_t998871775::get_offset_of_started_internally_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3358 = { sizeof (HttpWriter_t2060157611), -1, sizeof(HttpWriter_t2060157611_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3358[6] = 
{
	HttpWriter_t2060157611::get_offset_of_output_stream_4(),
	HttpWriter_t2060157611::get_offset_of_response_5(),
	HttpWriter_t2060157611::get_offset_of_encoding_6(),
	HttpWriter_t2060157611::get_offset_of__bytebuffer_7(),
	HttpWriter_t2060157611::get_offset_of_chars_8(),
	HttpWriter_t2060157611_StaticFields::get_offset_of_newline_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3359 = { sizeof (IntPtrStream_t1988557108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3359[4] = 
{
	IntPtrStream_t1988557108::get_offset_of_base_address_2(),
	IntPtrStream_t1988557108::get_offset_of_size_3(),
	IntPtrStream_t1988557108::get_offset_of_position_4(),
	IntPtrStream_t1988557108::get_offset_of_owns_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3360 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3361 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3362 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3363 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3364 = { sizeof (InputFilterStream_t4129443631), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3364[1] = 
{
	InputFilterStream_t4129443631::get_offset_of_stream_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3365 = { sizeof (NoParamsInvoker_t2754751357), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3365[2] = 
{
	NoParamsInvoker_t2754751357::get_offset_of_faked_0(),
	NoParamsInvoker_t2754751357::get_offset_of_real_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3366 = { sizeof (ParserErrorCollection_t485049528), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3367 = { sizeof (ParserError_t3362544569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3367[3] = 
{
	ParserError_t3362544569::get_offset_of__errorText_0(),
	ParserError_t3362544569::get_offset_of__virtualPath_1(),
	ParserError_t3362544569::get_offset_of__line_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3368 = { sizeof (ProfileBase_t2370935045), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3369 = { sizeof (QueueManager_t3850794979), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3369[7] = 
{
	QueueManager_t3850794979::get_offset_of_minFree_0(),
	QueueManager_t3850794979::get_offset_of_minLocalFree_1(),
	QueueManager_t3850794979::get_offset_of_queueLimit_2(),
	QueueManager_t3850794979::get_offset_of_queue_3(),
	QueueManager_t3850794979::get_offset_of_disposing_4(),
	QueueManager_t3850794979::get_offset_of_initialException_5(),
	QueueManager_t3850794979::get_offset_of_requestsQueuedCounter_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3370 = { sizeof (DefaultAuthenticationEventArgs_t645436702), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3370[1] = 
{
	DefaultAuthenticationEventArgs_t645436702::get_offset_of__context_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3371 = { sizeof (DefaultAuthenticationModule_t1472662056), -1, sizeof(DefaultAuthenticationModule_t1472662056_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3371[3] = 
{
	DefaultAuthenticationModule_t1472662056_StaticFields::get_offset_of_authenticateEvent_0(),
	DefaultAuthenticationModule_t1472662056_StaticFields::get_offset_of_defaultIdentity_1(),
	DefaultAuthenticationModule_t1472662056::get_offset_of_events_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3372 = { sizeof (MembershipProvider_t1994512972), -1, sizeof(MembershipProvider_t1994512972_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3372[1] = 
{
	MembershipProvider_t1994512972_StaticFields::get_offset_of_validatingPasswordEvent_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3373 = { sizeof (RoleProvider_t1137021788), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3374 = { sizeof (ServerVariablesCollection_t3116558453), -1, sizeof(ServerVariablesCollection_t3116558453_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3374[3] = 
{
	ServerVariablesCollection_t3116558453::get_offset_of_request_16(),
	ServerVariablesCollection_t3116558453::get_offset_of_loaded_17(),
	ServerVariablesCollection_t3116558453_StaticFields::get_offset_of_U3CU3Ef__switchU24mapE_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3375 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3376 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3377 = { sizeof (SessionId_t738760722), -1, sizeof(SessionId_t738760722_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3377[2] = 
{
	SessionId_t738760722_StaticFields::get_offset_of_allowed_0(),
	SessionId_t738760722_StaticFields::get_offset_of_rng_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3378 = { sizeof (SessionStateMode_t3002328156)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3378[6] = 
{
	SessionStateMode_t3002328156::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3379 = { sizeof (HttpSessionStateContainer_t50694200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3379[10] = 
{
	HttpSessionStateContainer_t50694200::get_offset_of_id_0(),
	HttpSessionStateContainer_t50694200::get_offset_of_staticObjects_1(),
	HttpSessionStateContainer_t50694200::get_offset_of_timeout_2(),
	HttpSessionStateContainer_t50694200::get_offset_of_newSession_3(),
	HttpSessionStateContainer_t50694200::get_offset_of_isCookieless_4(),
	HttpSessionStateContainer_t50694200::get_offset_of_mode_5(),
	HttpSessionStateContainer_t50694200::get_offset_of_isReadOnly_6(),
	HttpSessionStateContainer_t50694200::get_offset_of_abandoned_7(),
	HttpSessionStateContainer_t50694200::get_offset_of_sessionItems_8(),
	HttpSessionStateContainer_t50694200::get_offset_of_cookieMode_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3380 = { sizeof (HttpSessionState_t3146594024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3380[1] = 
{
	HttpSessionState_t3146594024::get_offset_of_container_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3381 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3382 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3383 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3384 = { sizeof (LockableStateServerItem_t2171525728), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3384[2] = 
{
	LockableStateServerItem_t2171525728::get_offset_of_item_0(),
	LockableStateServerItem_t2171525728::get_offset_of_rwlock_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3385 = { sizeof (RemoteStateServer_t3071674104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3385[1] = 
{
	RemoteStateServer_t3071674104::get_offset_of_cache_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3386 = { sizeof (SessionIDManager_t3564083178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3386[1] = 
{
	SessionIDManager_t3564083178::get_offset_of_config_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3387 = { sizeof (InProcSessionItem_t3251217927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3387[10] = 
{
	InProcSessionItem_t3251217927::get_offset_of_locked_0(),
	InProcSessionItem_t3251217927::get_offset_of_cookieless_1(),
	InProcSessionItem_t3251217927::get_offset_of_items_2(),
	InProcSessionItem_t3251217927::get_offset_of_lockedTime_3(),
	InProcSessionItem_t3251217927::get_offset_of_expiresAt_4(),
	InProcSessionItem_t3251217927::get_offset_of_rwlock_5(),
	InProcSessionItem_t3251217927::get_offset_of_lockId_6(),
	InProcSessionItem_t3251217927::get_offset_of_timeout_7(),
	InProcSessionItem_t3251217927::get_offset_of_resettingTimeout_8(),
	InProcSessionItem_t3251217927::get_offset_of_staticItems_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3388 = { sizeof (SessionInProcHandler_t2831147200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3388[3] = 
{
	SessionInProcHandler_t2831147200::get_offset_of_removedCB_3(),
	SessionInProcHandler_t2831147200::get_offset_of_expireCallback_4(),
	SessionInProcHandler_t2831147200::get_offset_of_staticObjects_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3389 = { sizeof (SessionStateActions_t2424179227)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3389[3] = 
{
	SessionStateActions_t2424179227::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3390 = { sizeof (SessionStateItemCollection_t3993009625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3390[1] = 
{
	SessionStateItemCollection_t3993009625::get_offset_of_is_dirty_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3391 = { sizeof (SessionStateModule_t1404169380), -1, sizeof(SessionStateModule_t1404169380_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3391[16] = 
{
	SessionStateModule_t1404169380_StaticFields::get_offset_of_startEvent_0(),
	SessionStateModule_t1404169380_StaticFields::get_offset_of_endEvent_1(),
	SessionStateModule_t1404169380::get_offset_of_config_2(),
	SessionStateModule_t1404169380::get_offset_of_handler_3(),
	SessionStateModule_t1404169380::get_offset_of_idManager_4(),
	SessionStateModule_t1404169380::get_offset_of_supportsExpiration_5(),
	SessionStateModule_t1404169380::get_offset_of_app_6(),
	SessionStateModule_t1404169380::get_offset_of_storeLocked_7(),
	SessionStateModule_t1404169380::get_offset_of_storeLockAge_8(),
	SessionStateModule_t1404169380::get_offset_of_storeLockId_9(),
	SessionStateModule_t1404169380::get_offset_of_storeSessionAction_10(),
	SessionStateModule_t1404169380::get_offset_of_storeIsNew_11(),
	SessionStateModule_t1404169380::get_offset_of_storeData_12(),
	SessionStateModule_t1404169380::get_offset_of_container_13(),
	SessionStateModule_t1404169380::get_offset_of_executionTimeout_14(),
	SessionStateModule_t1404169380::get_offset_of_events_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3392 = { sizeof (CallbackState_t123990870), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3392[4] = 
{
	CallbackState_t123990870::get_offset_of_Context_0(),
	CallbackState_t123990870::get_offset_of_AutoEvent_1(),
	CallbackState_t123990870::get_offset_of_SessionId_2(),
	CallbackState_t123990870::get_offset_of_IsReadOnly_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3393 = { sizeof (SessionStateServerHandler_t103977851), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3393[2] = 
{
	SessionStateServerHandler_t103977851::get_offset_of_config_3(),
	SessionStateServerHandler_t103977851::get_offset_of_stateServer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3394 = { sizeof (SessionStateStoreData_t4177544864), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3394[3] = 
{
	SessionStateStoreData_t4177544864::get_offset_of_sessionItems_0(),
	SessionStateStoreData_t4177544864::get_offset_of_staticObjects_1(),
	SessionStateStoreData_t4177544864::get_offset_of_timeout_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3395 = { sizeof (SessionStateStoreProviderBase_t182923317), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3396 = { sizeof (SessionStateUtility_t3346029770), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3397 = { sizeof (StateServerItem_t1723435776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3397[8] = 
{
	StateServerItem_t1723435776::get_offset_of_CollectionData_0(),
	StateServerItem_t1723435776::get_offset_of_StaticObjectsData_1(),
	StateServerItem_t1723435776::get_offset_of_last_access_2(),
	StateServerItem_t1723435776::get_offset_of_Timeout_3(),
	StateServerItem_t1723435776::get_offset_of_LockId_4(),
	StateServerItem_t1723435776::get_offset_of_Locked_5(),
	StateServerItem_t1723435776::get_offset_of_LockedTime_6(),
	StateServerItem_t1723435776::get_offset_of_Action_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3398 = { sizeof (TempFileStream_t420614451), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3398[3] = 
{
	TempFileStream_t420614451::get_offset_of_read_mode_16(),
	TempFileStream_t420614451::get_offset_of_disposed_17(),
	TempFileStream_t420614451::get_offset_of_saved_position_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3399 = { sizeof (TraceContext_t3886895271), -1, sizeof(TraceContext_t3886895271_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3399[12] = 
{
	TraceContext_t3886895271_StaticFields::get_offset_of_traceFinishedEvent_0(),
	TraceContext_t3886895271::get_offset_of__Context_1(),
	TraceContext_t3886895271::get_offset_of__traceManager_2(),
	TraceContext_t3886895271::get_offset_of__Enabled_3(),
	TraceContext_t3886895271::get_offset_of__Mode_4(),
	TraceContext_t3886895271::get_offset_of_data_5(),
	TraceContext_t3886895271::get_offset_of_data_saved_6(),
	TraceContext_t3886895271::get_offset_of__haveTrace_7(),
	TraceContext_t3886895271::get_offset_of_view_states_8(),
	TraceContext_t3886895271::get_offset_of_control_states_9(),
	TraceContext_t3886895271::get_offset_of_sizes_10(),
	TraceContext_t3886895271::get_offset_of_events_11(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
