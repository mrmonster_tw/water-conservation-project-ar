﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Operations_XslCompiledElemen50593777.h"

// Mono.Xml.Xsl.Operations.XslVariableInformation
struct XslVariableInformation_t1039906920;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslGeneralVariable
struct  XslGeneralVariable_t1456221871  : public XslCompiledElement_t50593777
{
public:
	// Mono.Xml.Xsl.Operations.XslVariableInformation Mono.Xml.Xsl.Operations.XslGeneralVariable::var
	XslVariableInformation_t1039906920 * ___var_3;

public:
	inline static int32_t get_offset_of_var_3() { return static_cast<int32_t>(offsetof(XslGeneralVariable_t1456221871, ___var_3)); }
	inline XslVariableInformation_t1039906920 * get_var_3() const { return ___var_3; }
	inline XslVariableInformation_t1039906920 ** get_address_of_var_3() { return &___var_3; }
	inline void set_var_3(XslVariableInformation_t1039906920 * value)
	{
		___var_3 = value;
		Il2CppCodeGenWriteBarrier(&___var_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
