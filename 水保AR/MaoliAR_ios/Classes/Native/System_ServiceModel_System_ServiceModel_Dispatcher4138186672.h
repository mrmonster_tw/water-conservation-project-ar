﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_DateTime3738529785.h"
#include "mscorlib_System_TimeSpan881159249.h"
#include "System_ServiceModel_System_ServiceModel_AddressFil2335829274.h"

// System.ServiceModel.Dispatcher.ChannelDispatcher
struct ChannelDispatcher_t2781106991;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t1333520283;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t451242010;
// System.Threading.Thread
struct Thread_t2300836069;
// System.Func`1<System.IAsyncResult>
struct Func_1_t196751098;
// System.Collections.Generic.List`1<System.ServiceModel.Channels.IChannel>
struct List_1_t2409282287;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.ListenerLoopManager
struct  ListenerLoopManager_t4138186672  : public Il2CppObject
{
public:
	// System.ServiceModel.Dispatcher.ChannelDispatcher System.ServiceModel.Dispatcher.ListenerLoopManager::owner
	ChannelDispatcher_t2781106991 * ___owner_0;
	// System.Threading.AutoResetEvent System.ServiceModel.Dispatcher.ListenerLoopManager::throttle_wait_handle
	AutoResetEvent_t1333520283 * ___throttle_wait_handle_1;
	// System.Threading.AutoResetEvent System.ServiceModel.Dispatcher.ListenerLoopManager::creator_handle
	AutoResetEvent_t1333520283 * ___creator_handle_2;
	// System.Threading.ManualResetEvent System.ServiceModel.Dispatcher.ListenerLoopManager::stop_handle
	ManualResetEvent_t451242010 * ___stop_handle_3;
	// System.Boolean System.ServiceModel.Dispatcher.ListenerLoopManager::loop
	bool ___loop_4;
	// System.Threading.Thread System.ServiceModel.Dispatcher.ListenerLoopManager::loop_thread
	Thread_t2300836069 * ___loop_thread_5;
	// System.DateTime System.ServiceModel.Dispatcher.ListenerLoopManager::close_started
	DateTime_t3738529785  ___close_started_6;
	// System.TimeSpan System.ServiceModel.Dispatcher.ListenerLoopManager::close_timeout
	TimeSpan_t881159249  ___close_timeout_7;
	// System.Func`1<System.IAsyncResult> System.ServiceModel.Dispatcher.ListenerLoopManager::channel_acceptor
	Func_1_t196751098 * ___channel_acceptor_8;
	// System.Collections.Generic.List`1<System.ServiceModel.Channels.IChannel> System.ServiceModel.Dispatcher.ListenerLoopManager::channels
	List_1_t2409282287 * ___channels_9;
	// System.ServiceModel.AddressFilterMode System.ServiceModel.Dispatcher.ListenerLoopManager::address_filter_mode
	int32_t ___address_filter_mode_10;

public:
	inline static int32_t get_offset_of_owner_0() { return static_cast<int32_t>(offsetof(ListenerLoopManager_t4138186672, ___owner_0)); }
	inline ChannelDispatcher_t2781106991 * get_owner_0() const { return ___owner_0; }
	inline ChannelDispatcher_t2781106991 ** get_address_of_owner_0() { return &___owner_0; }
	inline void set_owner_0(ChannelDispatcher_t2781106991 * value)
	{
		___owner_0 = value;
		Il2CppCodeGenWriteBarrier(&___owner_0, value);
	}

	inline static int32_t get_offset_of_throttle_wait_handle_1() { return static_cast<int32_t>(offsetof(ListenerLoopManager_t4138186672, ___throttle_wait_handle_1)); }
	inline AutoResetEvent_t1333520283 * get_throttle_wait_handle_1() const { return ___throttle_wait_handle_1; }
	inline AutoResetEvent_t1333520283 ** get_address_of_throttle_wait_handle_1() { return &___throttle_wait_handle_1; }
	inline void set_throttle_wait_handle_1(AutoResetEvent_t1333520283 * value)
	{
		___throttle_wait_handle_1 = value;
		Il2CppCodeGenWriteBarrier(&___throttle_wait_handle_1, value);
	}

	inline static int32_t get_offset_of_creator_handle_2() { return static_cast<int32_t>(offsetof(ListenerLoopManager_t4138186672, ___creator_handle_2)); }
	inline AutoResetEvent_t1333520283 * get_creator_handle_2() const { return ___creator_handle_2; }
	inline AutoResetEvent_t1333520283 ** get_address_of_creator_handle_2() { return &___creator_handle_2; }
	inline void set_creator_handle_2(AutoResetEvent_t1333520283 * value)
	{
		___creator_handle_2 = value;
		Il2CppCodeGenWriteBarrier(&___creator_handle_2, value);
	}

	inline static int32_t get_offset_of_stop_handle_3() { return static_cast<int32_t>(offsetof(ListenerLoopManager_t4138186672, ___stop_handle_3)); }
	inline ManualResetEvent_t451242010 * get_stop_handle_3() const { return ___stop_handle_3; }
	inline ManualResetEvent_t451242010 ** get_address_of_stop_handle_3() { return &___stop_handle_3; }
	inline void set_stop_handle_3(ManualResetEvent_t451242010 * value)
	{
		___stop_handle_3 = value;
		Il2CppCodeGenWriteBarrier(&___stop_handle_3, value);
	}

	inline static int32_t get_offset_of_loop_4() { return static_cast<int32_t>(offsetof(ListenerLoopManager_t4138186672, ___loop_4)); }
	inline bool get_loop_4() const { return ___loop_4; }
	inline bool* get_address_of_loop_4() { return &___loop_4; }
	inline void set_loop_4(bool value)
	{
		___loop_4 = value;
	}

	inline static int32_t get_offset_of_loop_thread_5() { return static_cast<int32_t>(offsetof(ListenerLoopManager_t4138186672, ___loop_thread_5)); }
	inline Thread_t2300836069 * get_loop_thread_5() const { return ___loop_thread_5; }
	inline Thread_t2300836069 ** get_address_of_loop_thread_5() { return &___loop_thread_5; }
	inline void set_loop_thread_5(Thread_t2300836069 * value)
	{
		___loop_thread_5 = value;
		Il2CppCodeGenWriteBarrier(&___loop_thread_5, value);
	}

	inline static int32_t get_offset_of_close_started_6() { return static_cast<int32_t>(offsetof(ListenerLoopManager_t4138186672, ___close_started_6)); }
	inline DateTime_t3738529785  get_close_started_6() const { return ___close_started_6; }
	inline DateTime_t3738529785 * get_address_of_close_started_6() { return &___close_started_6; }
	inline void set_close_started_6(DateTime_t3738529785  value)
	{
		___close_started_6 = value;
	}

	inline static int32_t get_offset_of_close_timeout_7() { return static_cast<int32_t>(offsetof(ListenerLoopManager_t4138186672, ___close_timeout_7)); }
	inline TimeSpan_t881159249  get_close_timeout_7() const { return ___close_timeout_7; }
	inline TimeSpan_t881159249 * get_address_of_close_timeout_7() { return &___close_timeout_7; }
	inline void set_close_timeout_7(TimeSpan_t881159249  value)
	{
		___close_timeout_7 = value;
	}

	inline static int32_t get_offset_of_channel_acceptor_8() { return static_cast<int32_t>(offsetof(ListenerLoopManager_t4138186672, ___channel_acceptor_8)); }
	inline Func_1_t196751098 * get_channel_acceptor_8() const { return ___channel_acceptor_8; }
	inline Func_1_t196751098 ** get_address_of_channel_acceptor_8() { return &___channel_acceptor_8; }
	inline void set_channel_acceptor_8(Func_1_t196751098 * value)
	{
		___channel_acceptor_8 = value;
		Il2CppCodeGenWriteBarrier(&___channel_acceptor_8, value);
	}

	inline static int32_t get_offset_of_channels_9() { return static_cast<int32_t>(offsetof(ListenerLoopManager_t4138186672, ___channels_9)); }
	inline List_1_t2409282287 * get_channels_9() const { return ___channels_9; }
	inline List_1_t2409282287 ** get_address_of_channels_9() { return &___channels_9; }
	inline void set_channels_9(List_1_t2409282287 * value)
	{
		___channels_9 = value;
		Il2CppCodeGenWriteBarrier(&___channels_9, value);
	}

	inline static int32_t get_offset_of_address_filter_mode_10() { return static_cast<int32_t>(offsetof(ListenerLoopManager_t4138186672, ___address_filter_mode_10)); }
	inline int32_t get_address_filter_mode_10() const { return ___address_filter_mode_10; }
	inline int32_t* get_address_of_address_filter_mode_10() { return &___address_filter_mode_10; }
	inline void set_address_filter_mode_10(int32_t value)
	{
		___address_filter_mode_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
