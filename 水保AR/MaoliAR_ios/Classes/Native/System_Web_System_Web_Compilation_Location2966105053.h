﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.Web.Compilation.ILocation
struct ILocation_t3961726794;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.Location
struct  Location_t2966105053  : public Il2CppObject
{
public:
	// System.Int32 System.Web.Compilation.Location::beginLine
	int32_t ___beginLine_0;
	// System.Int32 System.Web.Compilation.Location::endLine
	int32_t ___endLine_1;
	// System.Int32 System.Web.Compilation.Location::beginColumn
	int32_t ___beginColumn_2;
	// System.Int32 System.Web.Compilation.Location::endColumn
	int32_t ___endColumn_3;
	// System.String System.Web.Compilation.Location::fileName
	String_t* ___fileName_4;
	// System.String System.Web.Compilation.Location::plainText
	String_t* ___plainText_5;
	// System.Web.Compilation.ILocation System.Web.Compilation.Location::location
	Il2CppObject * ___location_6;

public:
	inline static int32_t get_offset_of_beginLine_0() { return static_cast<int32_t>(offsetof(Location_t2966105053, ___beginLine_0)); }
	inline int32_t get_beginLine_0() const { return ___beginLine_0; }
	inline int32_t* get_address_of_beginLine_0() { return &___beginLine_0; }
	inline void set_beginLine_0(int32_t value)
	{
		___beginLine_0 = value;
	}

	inline static int32_t get_offset_of_endLine_1() { return static_cast<int32_t>(offsetof(Location_t2966105053, ___endLine_1)); }
	inline int32_t get_endLine_1() const { return ___endLine_1; }
	inline int32_t* get_address_of_endLine_1() { return &___endLine_1; }
	inline void set_endLine_1(int32_t value)
	{
		___endLine_1 = value;
	}

	inline static int32_t get_offset_of_beginColumn_2() { return static_cast<int32_t>(offsetof(Location_t2966105053, ___beginColumn_2)); }
	inline int32_t get_beginColumn_2() const { return ___beginColumn_2; }
	inline int32_t* get_address_of_beginColumn_2() { return &___beginColumn_2; }
	inline void set_beginColumn_2(int32_t value)
	{
		___beginColumn_2 = value;
	}

	inline static int32_t get_offset_of_endColumn_3() { return static_cast<int32_t>(offsetof(Location_t2966105053, ___endColumn_3)); }
	inline int32_t get_endColumn_3() const { return ___endColumn_3; }
	inline int32_t* get_address_of_endColumn_3() { return &___endColumn_3; }
	inline void set_endColumn_3(int32_t value)
	{
		___endColumn_3 = value;
	}

	inline static int32_t get_offset_of_fileName_4() { return static_cast<int32_t>(offsetof(Location_t2966105053, ___fileName_4)); }
	inline String_t* get_fileName_4() const { return ___fileName_4; }
	inline String_t** get_address_of_fileName_4() { return &___fileName_4; }
	inline void set_fileName_4(String_t* value)
	{
		___fileName_4 = value;
		Il2CppCodeGenWriteBarrier(&___fileName_4, value);
	}

	inline static int32_t get_offset_of_plainText_5() { return static_cast<int32_t>(offsetof(Location_t2966105053, ___plainText_5)); }
	inline String_t* get_plainText_5() const { return ___plainText_5; }
	inline String_t** get_address_of_plainText_5() { return &___plainText_5; }
	inline void set_plainText_5(String_t* value)
	{
		___plainText_5 = value;
		Il2CppCodeGenWriteBarrier(&___plainText_5, value);
	}

	inline static int32_t get_offset_of_location_6() { return static_cast<int32_t>(offsetof(Location_t2966105053, ___location_6)); }
	inline Il2CppObject * get_location_6() const { return ___location_6; }
	inline Il2CppObject ** get_address_of_location_6() { return &___location_6; }
	inline void set_location_6(Il2CppObject * value)
	{
		___location_6 = value;
		Il2CppCodeGenWriteBarrier(&___location_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
