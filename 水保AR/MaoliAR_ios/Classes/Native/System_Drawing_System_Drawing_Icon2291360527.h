﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_MarshalByRefObject2760389100.h"
#include "System_Drawing_System_Drawing_Size1711122972.h"
#include "mscorlib_System_IntPtr840150181.h"
#include "System_Drawing_System_Drawing_Icon_IconDir4069448150.h"

// System.Drawing.Icon/IconImage[]
struct IconImageU5BU5D_t2518041974;
// System.Drawing.Bitmap
struct Bitmap_t3858959443;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Drawing.Icon
struct  Icon_t2291360527  : public MarshalByRefObject_t2760389100
{
public:
	// System.Drawing.Size System.Drawing.Icon::iconSize
	Size_t1711122972  ___iconSize_1;
	// System.IntPtr System.Drawing.Icon::handle
	IntPtr_t ___handle_2;
	// System.Drawing.Icon/IconDir System.Drawing.Icon::iconDir
	IconDir_t4069448150  ___iconDir_3;
	// System.UInt16 System.Drawing.Icon::id
	uint16_t ___id_4;
	// System.Drawing.Icon/IconImage[] System.Drawing.Icon::imageData
	IconImageU5BU5D_t2518041974* ___imageData_5;
	// System.Boolean System.Drawing.Icon::undisposable
	bool ___undisposable_6;
	// System.Boolean System.Drawing.Icon::disposed
	bool ___disposed_7;
	// System.Drawing.Bitmap System.Drawing.Icon::bitmap
	Bitmap_t3858959443 * ___bitmap_8;

public:
	inline static int32_t get_offset_of_iconSize_1() { return static_cast<int32_t>(offsetof(Icon_t2291360527, ___iconSize_1)); }
	inline Size_t1711122972  get_iconSize_1() const { return ___iconSize_1; }
	inline Size_t1711122972 * get_address_of_iconSize_1() { return &___iconSize_1; }
	inline void set_iconSize_1(Size_t1711122972  value)
	{
		___iconSize_1 = value;
	}

	inline static int32_t get_offset_of_handle_2() { return static_cast<int32_t>(offsetof(Icon_t2291360527, ___handle_2)); }
	inline IntPtr_t get_handle_2() const { return ___handle_2; }
	inline IntPtr_t* get_address_of_handle_2() { return &___handle_2; }
	inline void set_handle_2(IntPtr_t value)
	{
		___handle_2 = value;
	}

	inline static int32_t get_offset_of_iconDir_3() { return static_cast<int32_t>(offsetof(Icon_t2291360527, ___iconDir_3)); }
	inline IconDir_t4069448150  get_iconDir_3() const { return ___iconDir_3; }
	inline IconDir_t4069448150 * get_address_of_iconDir_3() { return &___iconDir_3; }
	inline void set_iconDir_3(IconDir_t4069448150  value)
	{
		___iconDir_3 = value;
	}

	inline static int32_t get_offset_of_id_4() { return static_cast<int32_t>(offsetof(Icon_t2291360527, ___id_4)); }
	inline uint16_t get_id_4() const { return ___id_4; }
	inline uint16_t* get_address_of_id_4() { return &___id_4; }
	inline void set_id_4(uint16_t value)
	{
		___id_4 = value;
	}

	inline static int32_t get_offset_of_imageData_5() { return static_cast<int32_t>(offsetof(Icon_t2291360527, ___imageData_5)); }
	inline IconImageU5BU5D_t2518041974* get_imageData_5() const { return ___imageData_5; }
	inline IconImageU5BU5D_t2518041974** get_address_of_imageData_5() { return &___imageData_5; }
	inline void set_imageData_5(IconImageU5BU5D_t2518041974* value)
	{
		___imageData_5 = value;
		Il2CppCodeGenWriteBarrier(&___imageData_5, value);
	}

	inline static int32_t get_offset_of_undisposable_6() { return static_cast<int32_t>(offsetof(Icon_t2291360527, ___undisposable_6)); }
	inline bool get_undisposable_6() const { return ___undisposable_6; }
	inline bool* get_address_of_undisposable_6() { return &___undisposable_6; }
	inline void set_undisposable_6(bool value)
	{
		___undisposable_6 = value;
	}

	inline static int32_t get_offset_of_disposed_7() { return static_cast<int32_t>(offsetof(Icon_t2291360527, ___disposed_7)); }
	inline bool get_disposed_7() const { return ___disposed_7; }
	inline bool* get_address_of_disposed_7() { return &___disposed_7; }
	inline void set_disposed_7(bool value)
	{
		___disposed_7 = value;
	}

	inline static int32_t get_offset_of_bitmap_8() { return static_cast<int32_t>(offsetof(Icon_t2291360527, ___bitmap_8)); }
	inline Bitmap_t3858959443 * get_bitmap_8() const { return ___bitmap_8; }
	inline Bitmap_t3858959443 ** get_address_of_bitmap_8() { return &___bitmap_8; }
	inline void set_bitmap_8(Bitmap_t3858959443 * value)
	{
		___bitmap_8 = value;
		Il2CppCodeGenWriteBarrier(&___bitmap_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
