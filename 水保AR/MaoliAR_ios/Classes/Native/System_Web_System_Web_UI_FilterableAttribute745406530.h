﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"

// System.Web.UI.FilterableAttribute
struct FilterableAttribute_t745406530;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.FilterableAttribute
struct  FilterableAttribute_t745406530  : public Attribute_t861562559
{
public:
	// System.Boolean System.Web.UI.FilterableAttribute::filterable
	bool ___filterable_0;

public:
	inline static int32_t get_offset_of_filterable_0() { return static_cast<int32_t>(offsetof(FilterableAttribute_t745406530, ___filterable_0)); }
	inline bool get_filterable_0() const { return ___filterable_0; }
	inline bool* get_address_of_filterable_0() { return &___filterable_0; }
	inline void set_filterable_0(bool value)
	{
		___filterable_0 = value;
	}
};

struct FilterableAttribute_t745406530_StaticFields
{
public:
	// System.Web.UI.FilterableAttribute System.Web.UI.FilterableAttribute::Default
	FilterableAttribute_t745406530 * ___Default_1;
	// System.Web.UI.FilterableAttribute System.Web.UI.FilterableAttribute::No
	FilterableAttribute_t745406530 * ___No_2;
	// System.Web.UI.FilterableAttribute System.Web.UI.FilterableAttribute::Yes
	FilterableAttribute_t745406530 * ___Yes_3;

public:
	inline static int32_t get_offset_of_Default_1() { return static_cast<int32_t>(offsetof(FilterableAttribute_t745406530_StaticFields, ___Default_1)); }
	inline FilterableAttribute_t745406530 * get_Default_1() const { return ___Default_1; }
	inline FilterableAttribute_t745406530 ** get_address_of_Default_1() { return &___Default_1; }
	inline void set_Default_1(FilterableAttribute_t745406530 * value)
	{
		___Default_1 = value;
		Il2CppCodeGenWriteBarrier(&___Default_1, value);
	}

	inline static int32_t get_offset_of_No_2() { return static_cast<int32_t>(offsetof(FilterableAttribute_t745406530_StaticFields, ___No_2)); }
	inline FilterableAttribute_t745406530 * get_No_2() const { return ___No_2; }
	inline FilterableAttribute_t745406530 ** get_address_of_No_2() { return &___No_2; }
	inline void set_No_2(FilterableAttribute_t745406530 * value)
	{
		___No_2 = value;
		Il2CppCodeGenWriteBarrier(&___No_2, value);
	}

	inline static int32_t get_offset_of_Yes_3() { return static_cast<int32_t>(offsetof(FilterableAttribute_t745406530_StaticFields, ___Yes_3)); }
	inline FilterableAttribute_t745406530 * get_Yes_3() const { return ___Yes_3; }
	inline FilterableAttribute_t745406530 ** get_address_of_Yes_3() { return &___Yes_3; }
	inline void set_Yes_3(FilterableAttribute_t745406530 * value)
	{
		___Yes_3 = value;
		Il2CppCodeGenWriteBarrier(&___Yes_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
