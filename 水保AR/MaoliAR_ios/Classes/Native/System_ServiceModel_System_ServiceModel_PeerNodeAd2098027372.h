﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.EndpointAddress
struct EndpointAddress_t3119842923;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.IPAddress>
struct ReadOnlyCollection_1_t1454353877;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.PeerNodeAddress
struct  PeerNodeAddress_t2098027372  : public Il2CppObject
{
public:
	// System.ServiceModel.EndpointAddress System.ServiceModel.PeerNodeAddress::endpoint
	EndpointAddress_t3119842923 * ___endpoint_0;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.IPAddress> System.ServiceModel.PeerNodeAddress::peer_addresses
	ReadOnlyCollection_1_t1454353877 * ___peer_addresses_1;

public:
	inline static int32_t get_offset_of_endpoint_0() { return static_cast<int32_t>(offsetof(PeerNodeAddress_t2098027372, ___endpoint_0)); }
	inline EndpointAddress_t3119842923 * get_endpoint_0() const { return ___endpoint_0; }
	inline EndpointAddress_t3119842923 ** get_address_of_endpoint_0() { return &___endpoint_0; }
	inline void set_endpoint_0(EndpointAddress_t3119842923 * value)
	{
		___endpoint_0 = value;
		Il2CppCodeGenWriteBarrier(&___endpoint_0, value);
	}

	inline static int32_t get_offset_of_peer_addresses_1() { return static_cast<int32_t>(offsetof(PeerNodeAddress_t2098027372, ___peer_addresses_1)); }
	inline ReadOnlyCollection_1_t1454353877 * get_peer_addresses_1() const { return ___peer_addresses_1; }
	inline ReadOnlyCollection_1_t1454353877 ** get_address_of_peer_addresses_1() { return &___peer_addresses_1; }
	inline void set_peer_addresses_1(ReadOnlyCollection_1_t1454353877 * value)
	{
		___peer_addresses_1 = value;
		Il2CppCodeGenWriteBarrier(&___peer_addresses_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
