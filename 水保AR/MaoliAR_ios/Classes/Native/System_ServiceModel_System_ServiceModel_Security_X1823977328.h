﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t714049126;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.X509CertificateRecipientServiceCredential
struct  X509CertificateRecipientServiceCredential_t1823977328  : public Il2CppObject
{
public:
	// System.Security.Cryptography.X509Certificates.X509Certificate2 System.ServiceModel.Security.X509CertificateRecipientServiceCredential::certificate
	X509Certificate2_t714049126 * ___certificate_0;

public:
	inline static int32_t get_offset_of_certificate_0() { return static_cast<int32_t>(offsetof(X509CertificateRecipientServiceCredential_t1823977328, ___certificate_0)); }
	inline X509Certificate2_t714049126 * get_certificate_0() const { return ___certificate_0; }
	inline X509Certificate2_t714049126 ** get_address_of_certificate_0() { return &___certificate_0; }
	inline void set_certificate_0(X509Certificate2_t714049126 * value)
	{
		___certificate_0 = value;
		Il2CppCodeGenWriteBarrier(&___certificate_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
