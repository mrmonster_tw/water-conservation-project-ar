﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeExpression2166265795.h"

// System.CodeDom.CodeExpressionCollection
struct CodeExpressionCollection_t2370433003;
// System.CodeDom.CodeExpression
struct CodeExpression_t2166265795;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeArrayIndexerExpression
struct  CodeArrayIndexerExpression_t2029945469  : public CodeExpression_t2166265795
{
public:
	// System.CodeDom.CodeExpressionCollection System.CodeDom.CodeArrayIndexerExpression::indices
	CodeExpressionCollection_t2370433003 * ___indices_1;
	// System.CodeDom.CodeExpression System.CodeDom.CodeArrayIndexerExpression::targetObject
	CodeExpression_t2166265795 * ___targetObject_2;

public:
	inline static int32_t get_offset_of_indices_1() { return static_cast<int32_t>(offsetof(CodeArrayIndexerExpression_t2029945469, ___indices_1)); }
	inline CodeExpressionCollection_t2370433003 * get_indices_1() const { return ___indices_1; }
	inline CodeExpressionCollection_t2370433003 ** get_address_of_indices_1() { return &___indices_1; }
	inline void set_indices_1(CodeExpressionCollection_t2370433003 * value)
	{
		___indices_1 = value;
		Il2CppCodeGenWriteBarrier(&___indices_1, value);
	}

	inline static int32_t get_offset_of_targetObject_2() { return static_cast<int32_t>(offsetof(CodeArrayIndexerExpression_t2029945469, ___targetObject_2)); }
	inline CodeExpression_t2166265795 * get_targetObject_2() const { return ___targetObject_2; }
	inline CodeExpression_t2166265795 ** get_address_of_targetObject_2() { return &___targetObject_2; }
	inline void set_targetObject_2(CodeExpression_t2166265795 * value)
	{
		___targetObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___targetObject_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
