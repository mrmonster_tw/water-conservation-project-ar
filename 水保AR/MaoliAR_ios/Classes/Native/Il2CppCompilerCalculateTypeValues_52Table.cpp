﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_ServiceModel_System_ServiceModel_Security_Id614541315.h"
#include "System_ServiceModel_System_ServiceModel_Security_Id634240690.h"
#include "System_ServiceModel_System_ServiceModel_Security_I2331807164.h"
#include "System_ServiceModel_System_ServiceModel_Security_Is922425120.h"
#include "System_ServiceModel_System_ServiceModel_Security_S1263698963.h"
#include "System_ServiceModel_System_ServiceModel_Security_M2368875147.h"
#include "System_ServiceModel_System_ServiceModel_Security_M4027609129.h"
#include "System_ServiceModel_System_ServiceModel_Security_M4107551044.h"
#include "System_ServiceModel_System_ServiceModel_Security_P4076012648.h"
#include "System_ServiceModel_System_ServiceModel_Security_Sc187359123.h"
#include "System_ServiceModel_System_ServiceModel_Security_S1277180297.h"
#include "System_ServiceModel_System_ServiceModel_Security_S2971294059.h"
#include "System_ServiceModel_System_ServiceModel_Security_Se354233970.h"
#include "System_ServiceModel_System_ServiceModel_Security_S2980594242.h"
#include "System_ServiceModel_System_ServiceModel_Security_Se749792453.h"
#include "System_ServiceModel_System_ServiceModel_Security_S2324234340.h"
#include "System_ServiceModel_System_ServiceModel_Security_S1875199571.h"
#include "System_ServiceModel_System_ServiceModel_Security_Se698951267.h"
#include "System_ServiceModel_System_ServiceModel_Security_Se807295854.h"
#include "System_ServiceModel_System_ServiceModel_Security_S3957431664.h"
#include "System_ServiceModel_System_ServiceModel_Security_S2241932240.h"
#include "System_ServiceModel_System_ServiceModel_Security_S2009957818.h"
#include "System_ServiceModel_System_ServiceModel_Security_S2144301020.h"
#include "System_ServiceModel_System_ServiceModel_Security_S3911394864.h"
#include "System_ServiceModel_System_ServiceModel_Security_S3211242264.h"
#include "System_ServiceModel_System_ServiceModel_Security_S4142693030.h"
#include "System_ServiceModel_System_ServiceModel_Security_S4142627494.h"
#include "System_ServiceModel_System_ServiceModel_Security_Se557944679.h"
#include "System_ServiceModel_System_ServiceModel_Security_S3961793461.h"
#include "System_ServiceModel_System_ServiceModel_Security_T3017348835.h"
#include "System_ServiceModel_System_ServiceModel_Security_T3734519520.h"
#include "System_ServiceModel_System_ServiceModel_Channels_U4128541908.h"
#include "System_ServiceModel_System_ServiceModel_Security_U4222120774.h"
#include "System_ServiceModel_System_ServiceModel_Security_U3640294368.h"
#include "System_ServiceModel_System_ServiceModel_Security_W1309166020.h"
#include "System_ServiceModel_System_ServiceModel_Security_W2902646674.h"
#include "System_ServiceModel_System_ServiceModel_Security_W1594975448.h"
#include "System_ServiceModel_System_ServiceModel_Security_X5679231977.h"
#include "System_ServiceModel_System_ServiceModel_Security_X3275324517.h"
#include "System_ServiceModel_System_ServiceModel_Security_X5850579047.h"
#include "System_ServiceModel_System_ServiceModel_Security_X1823977328.h"
#include "System_ServiceModel_System_ServiceModel_Security_X3667333716.h"
#include "System_ServiceModel_System_ServiceModel_Security_X5406335588.h"
#include "System_ServiceModel_System_ServiceModel_Security_X3406728790.h"
#include "System_ServiceModel_System_ServiceModel_ActionNotS2864863127.h"
#include "System_ServiceModel_System_ServiceModel_AddressFil2335829274.h"
#include "System_ServiceModel_System_ServiceModel_BasicHttpM3060458099.h"
#include "System_ServiceModel_System_ServiceModel_BasicHttpS2873933683.h"
#include "System_ServiceModel_System_ServiceModel_Communicat1609160389.h"
#include "System_ServiceModel_System_ServiceModel_HostNameCo1351856580.h"
#include "System_ServiceModel_System_ServiceModel_Impersonat2275648231.h"
#include "System_ServiceModel_System_ServiceModel_InstanceCon780954075.h"
#include "System_ServiceModel_System_ServiceModel_NetMsmqSec2233070459.h"
#include "System_ServiceModel_System_ServiceModel_NetNamedPi1407634332.h"
#include "System_ServiceModel_System_ServiceModel_PeerTransp3378031475.h"
#include "System_ServiceModel_System_ServiceModel_ReceiveErr2226770027.h"
#include "System_ServiceModel_System_ServiceModel_ReleaseIns1178280783.h"
#include "System_ServiceModel_System_ServiceModel_SessionMode513474935.h"
#include "System_ServiceModel_System_ServiceModel_WSDualHttpS221882749.h"
#include "System_ServiceModel_System_ServiceModel_WSFederatio751951473.h"
#include "System_ServiceModel_System_ServiceModel_WSMessageE2322128963.h"
#include "System_ServiceModel_System_ServiceModel_DeadLetter1799023886.h"
#include "System_ServiceModel_System_ServiceModel_HttpClientC856074172.h"
#include "System_ServiceModel_System_ServiceModel_HttpProxyC3209968900.h"
#include "System_ServiceModel_System_ServiceModel_MessageCre1579346758.h"
#include "System_ServiceModel_System_ServiceModel_MsmqAuthen2816778259.h"
#include "System_ServiceModel_System_ServiceModel_MsmqEncrypt395927193.h"
#include "System_ServiceModel_System_ServiceModel_MsmqSecure3964215293.h"
#include "System_ServiceModel_System_ServiceModel_QueueTrans3996023584.h"
#include "System_ServiceModel_System_ServiceModel_SecurityMo1299321141.h"
#include "System_ServiceModel_System_ServiceModel_TcpClientC3522739476.h"
#include "System_ServiceModel_System_ServiceModel_Channels_M3863555980.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Se713688608.h"
#include "System_ServiceModel_System_ServiceModel_Descriptio1024212837.h"
#include "System_ServiceModel_System_ServiceModel_Descriptio1296153790.h"
#include "System_ServiceModel_System_ServiceModel_Descriptio1965852526.h"
#include "System_ServiceModel_System_ServiceModel_MsmqIntegr2765665404.h"
#include "System_ServiceModel_System_ServiceModel_MsmqIntegr2623259356.h"
#include "System_ServiceModel_System_ServiceModel_Security_U1078652501.h"
#include "System_ServiceModel_System_ServiceModel_Security_X3346525935.h"
#include "System_ServiceModel_System_ServiceModel_Security_T4097770718.h"
#include "System_ServiceModel_System_ServiceModel_Security_To681329718.h"
#include "System_ServiceModel_System_ServiceModel_BasicHttpB4145755420.h"
#include "System_ServiceModel_System_ServiceModel_BasicHttpM1555777380.h"
#include "System_ServiceModel_System_ServiceModel_BasicHttpSe548302097.h"
#include "System_ServiceModel_System_ServiceModel_ChannelFact628250694.h"
#include "System_ServiceModel_System_ServiceModel_ClientCred1905807029.h"
#include "System_ServiceModel_System_ServiceModel_ClientProx3350985928.h"
#include "System_ServiceModel_System_ServiceModel_ProxyGener3584545969.h"
#include "System_ServiceModel_System_ServiceModel_ClientRunti398573209.h"
#include "System_ServiceModel_System_ServiceModel_Communicati147767313.h"
#include "System_ServiceModel_System_ServiceModel_Communicat1022779118.h"
#include "System_ServiceModel_System_ServiceModel_Constants3917698844.h"
#include "System_ServiceModel_System_ServiceModel_DataContra1176337824.h"
#include "System_ServiceModel_System_ServiceModel_DefaultCom1749927852.h"
#include "System_ServiceModel_System_ServiceModel_DnsEndpoin1764324158.h"
#include "System_ServiceModel_System_ServiceModel_Channels_P3228922052.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5200 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5201 = { sizeof (IdentityVerifier_t614541315), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5202 = { sizeof (DefaultIdentityVerifier_t634240690), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5203 = { sizeof (IssuedTokenClientCredential_t2331807164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5203[6] = 
{
	IssuedTokenClientCredential_t2331807164::get_offset_of_cache_0(),
	IssuedTokenClientCredential_t2331807164::get_offset_of_behaviors_1(),
	IssuedTokenClientCredential_t2331807164::get_offset_of_entropy_2(),
	IssuedTokenClientCredential_t2331807164::get_offset_of_local_behaviors_3(),
	IssuedTokenClientCredential_t2331807164::get_offset_of_max_cache_time_4(),
	IssuedTokenClientCredential_t2331807164::get_offset_of_renewal_threshold_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5204 = { sizeof (IssuedTokenServiceCredential_t922425120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5204[4] = 
{
	IssuedTokenServiceCredential_t922425120::get_offset_of_cert_verify_mode_0(),
	IssuedTokenServiceCredential_t922425120::get_offset_of_known_certs_1(),
	IssuedTokenServiceCredential_t922425120::get_offset_of_revocation_mode_2(),
	IssuedTokenServiceCredential_t922425120::get_offset_of_store_location_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5205 = { sizeof (SecurityKeyEntropyMode_t1263698963)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5205[4] = 
{
	SecurityKeyEntropyMode_t1263698963::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5206 = { sizeof (MessagePartSpecification_t2368875147), -1, sizeof(MessagePartSpecification_t2368875147_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5206[4] = 
{
	MessagePartSpecification_t2368875147_StaticFields::get_offset_of_empty_0(),
	MessagePartSpecification_t2368875147_StaticFields::get_offset_of_no_parts_1(),
	MessagePartSpecification_t2368875147::get_offset_of_body_2(),
	MessagePartSpecification_t2368875147::get_offset_of_header_types_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5207 = { sizeof (MessageProtectionOrder_t4027609129)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5207[4] = 
{
	MessageProtectionOrder_t4027609129::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5208 = { sizeof (MessageSecurityException_t4107551044), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5209 = { sizeof (PeerCredential_t4076012648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5209[2] = 
{
	PeerCredential_t4076012648::get_offset_of_cert_auth_0(),
	PeerCredential_t4076012648::get_offset_of_peer_auth_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5210 = { sizeof (ScopedMessagePartSpecification_t187359123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5210[3] = 
{
	ScopedMessagePartSpecification_t187359123::get_offset_of_table_0(),
	ScopedMessagePartSpecification_t187359123::get_offset_of_parts_1(),
	ScopedMessagePartSpecification_t187359123::get_offset_of_is_readonly_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5211 = { sizeof (SecureConversationServiceCredential_t1277180297), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5211[2] = 
{
	SecureConversationServiceCredential_t1277180297::get_offset_of_ctx_claim_types_0(),
	SecureConversationServiceCredential_t1277180297::get_offset_of_encoder_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5212 = { sizeof (SecureConversationVersion_t2971294059), -1, sizeof(SecureConversationVersion_t2971294059_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5212[5] = 
{
	SecureConversationVersion_t2971294059_StaticFields::get_offset_of_U3CDefaultU3Ek__BackingField_0(),
	SecureConversationVersion_t2971294059_StaticFields::get_offset_of_U3CWSSecureConversation13U3Ek__BackingField_1(),
	SecureConversationVersion_t2971294059_StaticFields::get_offset_of_U3CWSSecureConversationFeb2005U3Ek__BackingField_2(),
	SecureConversationVersion_t2971294059::get_offset_of_U3CNamespaceU3Ek__BackingField_3(),
	SecureConversationVersion_t2971294059::get_offset_of_U3CPrefixU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5213 = { sizeof (SecureConversationVersionImpl_t354233970), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5214 = { sizeof (SecurityAlgorithmSuite_t2980594242), -1, sizeof(SecurityAlgorithmSuite_t2980594242_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5214[16] = 
{
	SecurityAlgorithmSuite_t2980594242_StaticFields::get_offset_of_b128_0(),
	SecurityAlgorithmSuite_t2980594242_StaticFields::get_offset_of_b128r_1(),
	SecurityAlgorithmSuite_t2980594242_StaticFields::get_offset_of_b128s_2(),
	SecurityAlgorithmSuite_t2980594242_StaticFields::get_offset_of_b128sr_3(),
	SecurityAlgorithmSuite_t2980594242_StaticFields::get_offset_of_b192_4(),
	SecurityAlgorithmSuite_t2980594242_StaticFields::get_offset_of_b192r_5(),
	SecurityAlgorithmSuite_t2980594242_StaticFields::get_offset_of_b192s_6(),
	SecurityAlgorithmSuite_t2980594242_StaticFields::get_offset_of_b192sr_7(),
	SecurityAlgorithmSuite_t2980594242_StaticFields::get_offset_of_b256_8(),
	SecurityAlgorithmSuite_t2980594242_StaticFields::get_offset_of_b256r_9(),
	SecurityAlgorithmSuite_t2980594242_StaticFields::get_offset_of_b256s_10(),
	SecurityAlgorithmSuite_t2980594242_StaticFields::get_offset_of_b256sr_11(),
	SecurityAlgorithmSuite_t2980594242_StaticFields::get_offset_of_tdes_12(),
	SecurityAlgorithmSuite_t2980594242_StaticFields::get_offset_of_tdes_r_13(),
	SecurityAlgorithmSuite_t2980594242_StaticFields::get_offset_of_tdes_s_14(),
	SecurityAlgorithmSuite_t2980594242_StaticFields::get_offset_of_tdes_sr_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5215 = { sizeof (BasicSecurityAlgorithmSuite_t749792453), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5216 = { sizeof (TripleDESSecurityAlgorithmSuite_t2324234340), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5217 = { sizeof (SecurityAlgorithmSuiteImplBase_t1875199571), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5217[4] = 
{
	SecurityAlgorithmSuiteImplBase_t1875199571::get_offset_of_size_16(),
	SecurityAlgorithmSuiteImplBase_t1875199571::get_offset_of_rsa15_17(),
	SecurityAlgorithmSuiteImplBase_t1875199571::get_offset_of_sha256_18(),
	SecurityAlgorithmSuiteImplBase_t1875199571::get_offset_of_tdes_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5218 = { sizeof (SecurityContextKeyIdentifierClause_t698951267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5218[2] = 
{
	SecurityContextKeyIdentifierClause_t698951267::get_offset_of_context_3(),
	SecurityContextKeyIdentifierClause_t698951267::get_offset_of_generation_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5219 = { sizeof (SecurityCredentialsManager_t807295854), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5220 = { sizeof (SecurityMessageProperty_t3957431664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5220[8] = 
{
	SecurityMessageProperty_t3957431664::get_offset_of_initiator_token_0(),
	SecurityMessageProperty_t3957431664::get_offset_of_protection_token_1(),
	SecurityMessageProperty_t3957431664::get_offset_of_incoming_supp_tokens_2(),
	SecurityMessageProperty_t3957431664::get_offset_of_policies_3(),
	SecurityMessageProperty_t3957431664::get_offset_of_sender_id_prefix_4(),
	SecurityMessageProperty_t3957431664::get_offset_of_context_5(),
	SecurityMessageProperty_t3957431664::get_offset_of_ConfirmedSignatures_6(),
	SecurityMessageProperty_t3957431664::get_offset_of_EncryptionKey_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5221 = { sizeof (SecurityNegotiationException_t2241932240), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5222 = { sizeof (SecurityStateEncoder_t2009957818), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5223 = { sizeof (SecurityTokenAttachmentMode_t2144301020)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5223[5] = 
{
	SecurityTokenAttachmentMode_t2144301020::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5224 = { sizeof (SecurityTokenSpecification_t3911394864), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5224[2] = 
{
	SecurityTokenSpecification_t3911394864::get_offset_of_token_0(),
	SecurityTokenSpecification_t3911394864::get_offset_of_policies_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5225 = { sizeof (SecurityVersion_t3211242264), -1, sizeof(SecurityVersion_t3211242264_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5225[2] = 
{
	SecurityVersion_t3211242264_StaticFields::get_offset_of_wss10_0(),
	SecurityVersion_t3211242264_StaticFields::get_offset_of_wss11_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5226 = { sizeof (SecurityVersion10_t4142693030), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5227 = { sizeof (SecurityVersion11_t4142627494), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5228 = { sizeof (ServiceCredentialsSecurityTokenManager_t557944679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5228[1] = 
{
	ServiceCredentialsSecurityTokenManager_t557944679::get_offset_of_credentials_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5229 = { sizeof (SupportingTokenSpecification_t3961793461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5229[1] = 
{
	SupportingTokenSpecification_t3961793461::get_offset_of_mode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5230 = { sizeof (TrustVersion_t3017348835), -1, sizeof(TrustVersion_t3017348835_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5230[5] = 
{
	TrustVersion_t3017348835_StaticFields::get_offset_of_U3CDefaultU3Ek__BackingField_0(),
	TrustVersion_t3017348835_StaticFields::get_offset_of_U3CWSTrust13U3Ek__BackingField_1(),
	TrustVersion_t3017348835_StaticFields::get_offset_of_U3CWSTrustFeb2005U3Ek__BackingField_2(),
	TrustVersion_t3017348835::get_offset_of_U3CNamespaceU3Ek__BackingField_3(),
	TrustVersion_t3017348835::get_offset_of_U3CPrefixU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5231 = { sizeof (TrustVersionImpl_t3734519520), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5232 = { sizeof (UnionSecurityTokenResolver_t4128541908), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5232[1] = 
{
	UnionSecurityTokenResolver_t4128541908::get_offset_of_resolvers_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5233 = { sizeof (UserNamePasswordClientCredential_t4222120774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5233[2] = 
{
	UserNamePasswordClientCredential_t4222120774::get_offset_of_username_0(),
	UserNamePasswordClientCredential_t4222120774::get_offset_of_password_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5234 = { sizeof (UserNamePasswordServiceCredential_t3640294368), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5234[6] = 
{
	UserNamePasswordServiceCredential_t3640294368::get_offset_of_provider_0(),
	UserNamePasswordServiceCredential_t3640294368::get_offset_of_lifetime_1(),
	UserNamePasswordServiceCredential_t3640294368::get_offset_of_include_win_groups_2(),
	UserNamePasswordServiceCredential_t3640294368::get_offset_of_max_cache_tokens_3(),
	UserNamePasswordServiceCredential_t3640294368::get_offset_of_validator_4(),
	UserNamePasswordServiceCredential_t3640294368::get_offset_of_mode_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5235 = { sizeof (WSSecurityTokenSerializer_t1309166020), -1, sizeof(WSSecurityTokenSerializer_t1309166020_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5235[30] = 
{
	WSSecurityTokenSerializer_t1309166020_StaticFields::get_offset_of_default_instance_0(),
	WSSecurityTokenSerializer_t1309166020::get_offset_of_security_version_1(),
	WSSecurityTokenSerializer_t1309166020::get_offset_of_emit_bsp_2(),
	WSSecurityTokenSerializer_t1309166020::get_offset_of_saml_serializer_3(),
	WSSecurityTokenSerializer_t1309166020::get_offset_of_encoder_4(),
	WSSecurityTokenSerializer_t1309166020::get_offset_of_known_types_5(),
	WSSecurityTokenSerializer_t1309166020::get_offset_of_max_offset_6(),
	WSSecurityTokenSerializer_t1309166020::get_offset_of_max_label_length_7(),
	WSSecurityTokenSerializer_t1309166020::get_offset_of_max_nonce_length_8(),
	WSSecurityTokenSerializer_t1309166020_StaticFields::get_offset_of_U3CU3Ef__switchU24map1D_9(),
	WSSecurityTokenSerializer_t1309166020_StaticFields::get_offset_of_U3CU3Ef__switchU24map1E_10(),
	WSSecurityTokenSerializer_t1309166020_StaticFields::get_offset_of_U3CU3Ef__switchU24map1F_11(),
	WSSecurityTokenSerializer_t1309166020_StaticFields::get_offset_of_U3CU3Ef__switchU24map20_12(),
	WSSecurityTokenSerializer_t1309166020_StaticFields::get_offset_of_U3CU3Ef__switchU24map21_13(),
	WSSecurityTokenSerializer_t1309166020_StaticFields::get_offset_of_U3CU3Ef__switchU24map22_14(),
	WSSecurityTokenSerializer_t1309166020_StaticFields::get_offset_of_U3CU3Ef__switchU24map23_15(),
	WSSecurityTokenSerializer_t1309166020_StaticFields::get_offset_of_U3CU3Ef__switchU24map24_16(),
	WSSecurityTokenSerializer_t1309166020_StaticFields::get_offset_of_U3CU3Ef__switchU24map25_17(),
	WSSecurityTokenSerializer_t1309166020_StaticFields::get_offset_of_U3CU3Ef__switchU24map26_18(),
	WSSecurityTokenSerializer_t1309166020_StaticFields::get_offset_of_U3CU3Ef__switchU24map27_19(),
	WSSecurityTokenSerializer_t1309166020_StaticFields::get_offset_of_U3CU3Ef__switchU24map28_20(),
	WSSecurityTokenSerializer_t1309166020_StaticFields::get_offset_of_U3CU3Ef__switchU24map29_21(),
	WSSecurityTokenSerializer_t1309166020_StaticFields::get_offset_of_U3CU3Ef__switchU24map2A_22(),
	WSSecurityTokenSerializer_t1309166020_StaticFields::get_offset_of_U3CU3Ef__switchU24map2B_23(),
	WSSecurityTokenSerializer_t1309166020_StaticFields::get_offset_of_U3CU3Ef__switchU24map2C_24(),
	WSSecurityTokenSerializer_t1309166020_StaticFields::get_offset_of_U3CU3Ef__switchU24map2D_25(),
	WSSecurityTokenSerializer_t1309166020_StaticFields::get_offset_of_U3CU3Ef__switchU24map2E_26(),
	WSSecurityTokenSerializer_t1309166020_StaticFields::get_offset_of_U3CU3Ef__switchU24map2F_27(),
	WSSecurityTokenSerializer_t1309166020_StaticFields::get_offset_of_U3CU3Ef__switchU24map30_28(),
	WSSecurityTokenSerializer_t1309166020_StaticFields::get_offset_of_U3CU3Ef__switchU24map31_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5236 = { sizeof (WindowsClientCredential_t2902646674), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5236[3] = 
{
	WindowsClientCredential_t2902646674::get_offset_of_allow_ntlm_0(),
	WindowsClientCredential_t2902646674::get_offset_of_impersonation_level_1(),
	WindowsClientCredential_t2902646674::get_offset_of_client_credential_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5237 = { sizeof (WindowsServiceCredential_t1594975448), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5238 = { sizeof (X509CertificateInitiatorClientCredential_t679231977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5238[1] = 
{
	X509CertificateInitiatorClientCredential_t679231977::get_offset_of_certificate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5239 = { sizeof (X509CertificateInitiatorServiceCredential_t3275324517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5239[2] = 
{
	X509CertificateInitiatorServiceCredential_t3275324517::get_offset_of_auth_0(),
	X509CertificateInitiatorServiceCredential_t3275324517::get_offset_of_certificate_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5240 = { sizeof (X509CertificateRecipientClientCredential_t850579047), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5240[3] = 
{
	X509CertificateRecipientClientCredential_t850579047::get_offset_of_auth_0(),
	X509CertificateRecipientClientCredential_t850579047::get_offset_of_certificate_1(),
	X509CertificateRecipientClientCredential_t850579047::get_offset_of_scoped_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5241 = { sizeof (X509CertificateRecipientServiceCredential_t1823977328), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5241[1] = 
{
	X509CertificateRecipientServiceCredential_t1823977328::get_offset_of_certificate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5242 = { sizeof (X509ClientCertificateAuthentication_t3667333716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5242[5] = 
{
	X509ClientCertificateAuthentication_t3667333716::get_offset_of_validation_mode_0(),
	X509ClientCertificateAuthentication_t3667333716::get_offset_of_custom_validator_1(),
	X509ClientCertificateAuthentication_t3667333716::get_offset_of_include_windows_groups_2(),
	X509ClientCertificateAuthentication_t3667333716::get_offset_of_revocation_mode_3(),
	X509ClientCertificateAuthentication_t3667333716::get_offset_of_trusted_store_loc_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5243 = { sizeof (X509PeerCertificateAuthentication_t406335588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5243[3] = 
{
	X509PeerCertificateAuthentication_t406335588::get_offset_of_revocation_mode_0(),
	X509PeerCertificateAuthentication_t406335588::get_offset_of_store_loc_1(),
	X509PeerCertificateAuthentication_t406335588::get_offset_of_validation_mode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5244 = { sizeof (X509ServiceCertificateAuthentication_t3406728790), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5244[2] = 
{
	X509ServiceCertificateAuthentication_t3406728790::get_offset_of_validation_mode_0(),
	X509ServiceCertificateAuthentication_t3406728790::get_offset_of_custom_validator_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5245 = { sizeof (ActionNotSupportedException_t2864863127), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5246 = { sizeof (AddressFilterMode_t2335829274)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5246[4] = 
{
	AddressFilterMode_t2335829274::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5247 = { sizeof (BasicHttpMessageCredentialType_t3060458099)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5247[3] = 
{
	BasicHttpMessageCredentialType_t3060458099::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5248 = { sizeof (BasicHttpSecurityMode_t2873933683)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5248[6] = 
{
	BasicHttpSecurityMode_t2873933683::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5249 = { sizeof (CommunicationState_t1609160389)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5249[7] = 
{
	CommunicationState_t1609160389::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5250 = { sizeof (HostNameComparisonMode_t1351856580)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5250[4] = 
{
	HostNameComparisonMode_t1351856580::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5251 = { sizeof (ImpersonationOption_t2275648231)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5251[4] = 
{
	ImpersonationOption_t2275648231::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5252 = { sizeof (InstanceContextMode_t780954075)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5252[4] = 
{
	InstanceContextMode_t780954075::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5253 = { sizeof (NetMsmqSecurityMode_t2233070459)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5253[5] = 
{
	NetMsmqSecurityMode_t2233070459::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5254 = { sizeof (NetNamedPipeSecurityMode_t1407634332)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5254[3] = 
{
	NetNamedPipeSecurityMode_t1407634332::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5255 = { sizeof (PeerTransportCredentialType_t3378031475)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5255[3] = 
{
	PeerTransportCredentialType_t3378031475::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5256 = { sizeof (ReceiveErrorHandling_t2226770027)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5256[5] = 
{
	ReceiveErrorHandling_t2226770027::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5257 = { sizeof (ReleaseInstanceMode_t1178280783)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5257[5] = 
{
	ReleaseInstanceMode_t1178280783::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5258 = { sizeof (SessionMode_t513474935)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5258[4] = 
{
	SessionMode_t513474935::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5259 = { sizeof (WSDualHttpSecurityMode_t221882749)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5259[3] = 
{
	WSDualHttpSecurityMode_t221882749::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5260 = { sizeof (WSFederationHttpSecurityMode_t751951473)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5260[4] = 
{
	WSFederationHttpSecurityMode_t751951473::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5261 = { sizeof (WSMessageEncoding_t2322128963)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5261[3] = 
{
	WSMessageEncoding_t2322128963::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5262 = { sizeof (DeadLetterQueue_t1799023886)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5262[4] = 
{
	DeadLetterQueue_t1799023886::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5263 = { sizeof (HttpClientCredentialType_t856074172)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5263[7] = 
{
	HttpClientCredentialType_t856074172::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5264 = { sizeof (HttpProxyCredentialType_t3209968900)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5264[6] = 
{
	HttpProxyCredentialType_t3209968900::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5265 = { sizeof (MessageCredentialType_t1579346758)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5265[6] = 
{
	MessageCredentialType_t1579346758::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5266 = { sizeof (MsmqAuthenticationMode_t2816778259)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5266[4] = 
{
	MsmqAuthenticationMode_t2816778259::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5267 = { sizeof (MsmqEncryptionAlgorithm_t395927193)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5267[3] = 
{
	MsmqEncryptionAlgorithm_t395927193::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5268 = { sizeof (MsmqSecureHashAlgorithm_t3964215293)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5268[5] = 
{
	MsmqSecureHashAlgorithm_t3964215293::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5269 = { sizeof (QueueTransferProtocol_t3996023584)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5269[4] = 
{
	QueueTransferProtocol_t3996023584::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5270 = { sizeof (SecurityMode_t1299321141)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5270[5] = 
{
	SecurityMode_t1299321141::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5271 = { sizeof (TcpClientCredentialType_t3522739476)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5271[4] = 
{
	TcpClientCredentialType_t3522739476::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5272 = { sizeof (MessageState_t3863555980)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5272[6] = 
{
	MessageState_t3863555980::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5273 = { sizeof (SecurityHeaderLayout_t713688608)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5273[5] = 
{
	SecurityHeaderLayout_t713688608::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5274 = { sizeof (PrincipalPermissionMode_t1024212837)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5274[5] = 
{
	PrincipalPermissionMode_t1024212837::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5275 = { sizeof (MessageDirection_t1296153790)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5275[3] = 
{
	MessageDirection_t1296153790::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5276 = { sizeof (ListenUriMode_t1965852526)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5276[3] = 
{
	ListenUriMode_t1965852526::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5277 = { sizeof (MsmqIntegrationSecurityMode_t2765665404)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5277[3] = 
{
	MsmqIntegrationSecurityMode_t2765665404::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5278 = { sizeof (MsmqMessageSerializationFormat_t2623259356)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5278[6] = 
{
	MsmqMessageSerializationFormat_t2623259356::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5279 = { sizeof (UserNamePasswordValidationMode_t1078652501)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5279[4] = 
{
	UserNamePasswordValidationMode_t1078652501::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5280 = { sizeof (X509CertificateValidationMode_t3346525935)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5280[6] = 
{
	X509CertificateValidationMode_t3346525935::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5281 = { sizeof (SecurityTokenInclusionMode_t4097770718)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5281[5] = 
{
	SecurityTokenInclusionMode_t4097770718::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5282 = { sizeof (X509KeyIdentifierClauseType_t681329718)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5282[6] = 
{
	X509KeyIdentifierClauseType_t681329718::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5283 = { sizeof (BasicHttpBinding_t4145755420), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5283[14] = 
{
	BasicHttpBinding_t4145755420::get_offset_of_allow_cookies_6(),
	BasicHttpBinding_t4145755420::get_offset_of_bypass_proxy_on_local_7(),
	BasicHttpBinding_t4145755420::get_offset_of_host_name_comparison_mode_8(),
	BasicHttpBinding_t4145755420::get_offset_of_max_buffer_pool_size_9(),
	BasicHttpBinding_t4145755420::get_offset_of_max_buffer_size_10(),
	BasicHttpBinding_t4145755420::get_offset_of_max_recv_message_size_11(),
	BasicHttpBinding_t4145755420::get_offset_of_message_encoding_12(),
	BasicHttpBinding_t4145755420::get_offset_of_proxy_address_13(),
	BasicHttpBinding_t4145755420::get_offset_of_reader_quotas_14(),
	BasicHttpBinding_t4145755420::get_offset_of_env_version_15(),
	BasicHttpBinding_t4145755420::get_offset_of_text_encoding_16(),
	BasicHttpBinding_t4145755420::get_offset_of_transfer_mode_17(),
	BasicHttpBinding_t4145755420::get_offset_of_use_default_web_proxy_18(),
	BasicHttpBinding_t4145755420::get_offset_of_security_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5284 = { sizeof (BasicHttpMessageSecurity_t1555777380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5284[1] = 
{
	BasicHttpMessageSecurity_t1555777380::get_offset_of_ctype_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5285 = { sizeof (BasicHttpSecurity_t548302097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5285[3] = 
{
	BasicHttpSecurity_t548302097::get_offset_of_message_0(),
	BasicHttpSecurity_t548302097::get_offset_of_mode_1(),
	BasicHttpSecurity_t548302097::get_offset_of_transport_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5286 = { sizeof (ChannelFactory_t628250694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5286[2] = 
{
	ChannelFactory_t628250694::get_offset_of_service_endpoint_9(),
	ChannelFactory_t628250694::get_offset_of_factory_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5287 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5287[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5288 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5288[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5289 = { sizeof (ClientCredentialsSecurityTokenManager_t1905807029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5289[1] = 
{
	ClientCredentialsSecurityTokenManager_t1905807029::get_offset_of_credentials_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5290 = { sizeof (ClientProxyGenerator_t3350985928), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5291 = { sizeof (ProxyGeneratorBase_t3584545969), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5292 = { sizeof (ClientRuntimeChannel_t398573209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5292[7] = 
{
	ClientRuntimeChannel_t398573209::get_offset_of_runtime_9(),
	ClientRuntimeChannel_t398573209::get_offset_of_default_open_timeout_10(),
	ClientRuntimeChannel_t398573209::get_offset_of_default_close_timeout_11(),
	ClientRuntimeChannel_t398573209::get_offset_of_channel_12(),
	ClientRuntimeChannel_t398573209::get_offset_of_factory_13(),
	ClientRuntimeChannel_t398573209::get_offset_of_did_interactive_initialization_14(),
	ClientRuntimeChannel_t398573209::get_offset_of_U3COperationTimeoutU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5293 = { sizeof (CommunicationException_t147767313), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5294 = { sizeof (CommunicationObjectFaultedException_t1022779118), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5295 = { sizeof (Constants_t3917698844), -1, sizeof(Constants_t3917698844_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5295[2] = 
{
	Constants_t3917698844_StaticFields::get_offset_of_dict_strings_0(),
	Constants_t3917698844_StaticFields::get_offset_of_U3CSoapDictionaryU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5296 = { sizeof (DataContractFormatAttribute_t1176337824), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5297 = { sizeof (DefaultCommunicationTimeouts_t1749927852), -1, sizeof(DefaultCommunicationTimeouts_t1749927852_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5297[5] = 
{
	DefaultCommunicationTimeouts_t1749927852_StaticFields::get_offset_of_Instance_0(),
	DefaultCommunicationTimeouts_t1749927852::get_offset_of_close_1(),
	DefaultCommunicationTimeouts_t1749927852::get_offset_of_open_2(),
	DefaultCommunicationTimeouts_t1749927852::get_offset_of_receive_3(),
	DefaultCommunicationTimeouts_t1749927852::get_offset_of_send_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5298 = { sizeof (DnsEndpointIdentity_t1764324158), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5299 = { sizeof (PrivacyNoticeBindingElementImporter_t3228922052), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
