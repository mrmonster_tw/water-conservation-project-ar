﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.UI.Control
struct Control_t3006474639;
// System.Web.UI.Control[]
struct ControlU5BU5D_t3870755670;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.ControlCollection
struct  ControlCollection_t4212191938  : public Il2CppObject
{
public:
	// System.Web.UI.Control System.Web.UI.ControlCollection::owner
	Control_t3006474639 * ___owner_0;
	// System.Web.UI.Control[] System.Web.UI.ControlCollection::controls
	ControlU5BU5D_t3870755670* ___controls_1;
	// System.Int32 System.Web.UI.ControlCollection::version
	int32_t ___version_2;
	// System.Int32 System.Web.UI.ControlCollection::count
	int32_t ___count_3;
	// System.Boolean System.Web.UI.ControlCollection::readOnly
	bool ___readOnly_4;

public:
	inline static int32_t get_offset_of_owner_0() { return static_cast<int32_t>(offsetof(ControlCollection_t4212191938, ___owner_0)); }
	inline Control_t3006474639 * get_owner_0() const { return ___owner_0; }
	inline Control_t3006474639 ** get_address_of_owner_0() { return &___owner_0; }
	inline void set_owner_0(Control_t3006474639 * value)
	{
		___owner_0 = value;
		Il2CppCodeGenWriteBarrier(&___owner_0, value);
	}

	inline static int32_t get_offset_of_controls_1() { return static_cast<int32_t>(offsetof(ControlCollection_t4212191938, ___controls_1)); }
	inline ControlU5BU5D_t3870755670* get_controls_1() const { return ___controls_1; }
	inline ControlU5BU5D_t3870755670** get_address_of_controls_1() { return &___controls_1; }
	inline void set_controls_1(ControlU5BU5D_t3870755670* value)
	{
		___controls_1 = value;
		Il2CppCodeGenWriteBarrier(&___controls_1, value);
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(ControlCollection_t4212191938, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_count_3() { return static_cast<int32_t>(offsetof(ControlCollection_t4212191938, ___count_3)); }
	inline int32_t get_count_3() const { return ___count_3; }
	inline int32_t* get_address_of_count_3() { return &___count_3; }
	inline void set_count_3(int32_t value)
	{
		___count_3 = value;
	}

	inline static int32_t get_offset_of_readOnly_4() { return static_cast<int32_t>(offsetof(ControlCollection_t4212191938, ___readOnly_4)); }
	inline bool get_readOnly_4() const { return ___readOnly_4; }
	inline bool* get_address_of_readOnly_4() { return &___readOnly_4; }
	inline void set_readOnly_4(bool value)
	{
		___readOnly_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
