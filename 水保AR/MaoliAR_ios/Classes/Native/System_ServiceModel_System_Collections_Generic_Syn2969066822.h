﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Object
struct Il2CppObject;
// System.Collections.Generic.List`1<System.ServiceModel.Dispatcher.IClientMessageInspector>
struct List_1_t4126797580;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.SynchronizedCollection`1<System.ServiceModel.Dispatcher.IClientMessageInspector>
struct  SynchronizedCollection_1_t2969066822  : public Il2CppObject
{
public:
	// System.Object System.Collections.Generic.SynchronizedCollection`1::root
	Il2CppObject * ___root_0;
	// System.Collections.Generic.List`1<T> System.Collections.Generic.SynchronizedCollection`1::list
	List_1_t4126797580 * ___list_1;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(SynchronizedCollection_1_t2969066822, ___root_0)); }
	inline Il2CppObject * get_root_0() const { return ___root_0; }
	inline Il2CppObject ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(Il2CppObject * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier(&___root_0, value);
	}

	inline static int32_t get_offset_of_list_1() { return static_cast<int32_t>(offsetof(SynchronizedCollection_1_t2969066822, ___list_1)); }
	inline List_1_t4126797580 * get_list_1() const { return ___list_1; }
	inline List_1_t4126797580 ** get_address_of_list_1() { return &___list_1; }
	inline void set_list_1(List_1_t4126797580 * value)
	{
		___list_1 = value;
		Il2CppCodeGenWriteBarrier(&___list_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
