﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3010527243.h"

// System.Web.Configuration.VirtualDirectoryMappingCollection
struct VirtualDirectoryMappingCollection_t3325528393;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.WebConfigurationFileMap
struct  WebConfigurationFileMap_t225230154  : public ConfigurationFileMap_t3010527243
{
public:
	// System.Web.Configuration.VirtualDirectoryMappingCollection System.Web.Configuration.WebConfigurationFileMap::virtualDirectories
	VirtualDirectoryMappingCollection_t3325528393 * ___virtualDirectories_1;

public:
	inline static int32_t get_offset_of_virtualDirectories_1() { return static_cast<int32_t>(offsetof(WebConfigurationFileMap_t225230154, ___virtualDirectories_1)); }
	inline VirtualDirectoryMappingCollection_t3325528393 * get_virtualDirectories_1() const { return ___virtualDirectories_1; }
	inline VirtualDirectoryMappingCollection_t3325528393 ** get_address_of_virtualDirectories_1() { return &___virtualDirectories_1; }
	inline void set_virtualDirectories_1(VirtualDirectoryMappingCollection_t3325528393 * value)
	{
		___virtualDirectories_1 = value;
		Il2CppCodeGenWriteBarrier(&___virtualDirectories_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
