﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_ControlBuilder2523018631.h"

// System.Type[]
struct TypeU5BU5D_t3940880105;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.CollectionBuilder
struct  CollectionBuilder_t4289422969  : public ControlBuilder_t2523018631
{
public:
	// System.Type[] System.Web.UI.CollectionBuilder::possibleElementTypes
	TypeU5BU5D_t3940880105* ___possibleElementTypes_28;

public:
	inline static int32_t get_offset_of_possibleElementTypes_28() { return static_cast<int32_t>(offsetof(CollectionBuilder_t4289422969, ___possibleElementTypes_28)); }
	inline TypeU5BU5D_t3940880105* get_possibleElementTypes_28() const { return ___possibleElementTypes_28; }
	inline TypeU5BU5D_t3940880105** get_address_of_possibleElementTypes_28() { return &___possibleElementTypes_28; }
	inline void set_possibleElementTypes_28(TypeU5BU5D_t3940880105* value)
	{
		___possibleElementTypes_28 = value;
		Il2CppCodeGenWriteBarrier(&___possibleElementTypes_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
