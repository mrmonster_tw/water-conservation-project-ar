﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_XPath_LocationPathPattern3015773238.h"

// Mono.Xml.Xsl.XsltKey
struct XsltKey_t826370162;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.XPath.KeyPattern
struct  KeyPattern_t747253878  : public LocationPathPattern_t3015773238
{
public:
	// Mono.Xml.Xsl.XsltKey Mono.Xml.XPath.KeyPattern::key
	XsltKey_t826370162 * ___key_4;

public:
	inline static int32_t get_offset_of_key_4() { return static_cast<int32_t>(offsetof(KeyPattern_t747253878, ___key_4)); }
	inline XsltKey_t826370162 * get_key_4() const { return ___key_4; }
	inline XsltKey_t826370162 ** get_address_of_key_4() { return &___key_4; }
	inline void set_key_4(XsltKey_t826370162 * value)
	{
		___key_4 = value;
		Il2CppCodeGenWriteBarrier(&___key_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
