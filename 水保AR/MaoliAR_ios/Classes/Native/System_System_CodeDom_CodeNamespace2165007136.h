﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeObject3927604602.h"

// System.CodeDom.CodeCommentStatementCollection
struct CodeCommentStatementCollection_t84489496;
// System.CodeDom.CodeNamespaceImportCollection
struct CodeNamespaceImportCollection_t3818670277;
// System.CodeDom.CodeTypeDeclarationCollection
struct CodeTypeDeclarationCollection_t2229959800;
// System.String
struct String_t;
// System.EventHandler
struct EventHandler_t1348719766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeNamespace
struct  CodeNamespace_t2165007136  : public CodeObject_t3927604602
{
public:
	// System.CodeDom.CodeCommentStatementCollection System.CodeDom.CodeNamespace::comments
	CodeCommentStatementCollection_t84489496 * ___comments_1;
	// System.CodeDom.CodeNamespaceImportCollection System.CodeDom.CodeNamespace::imports
	CodeNamespaceImportCollection_t3818670277 * ___imports_2;
	// System.CodeDom.CodeTypeDeclarationCollection System.CodeDom.CodeNamespace::classes
	CodeTypeDeclarationCollection_t2229959800 * ___classes_3;
	// System.String System.CodeDom.CodeNamespace::name
	String_t* ___name_4;
	// System.EventHandler System.CodeDom.CodeNamespace::PopulateComments
	EventHandler_t1348719766 * ___PopulateComments_5;
	// System.EventHandler System.CodeDom.CodeNamespace::PopulateImports
	EventHandler_t1348719766 * ___PopulateImports_6;
	// System.EventHandler System.CodeDom.CodeNamespace::PopulateTypes
	EventHandler_t1348719766 * ___PopulateTypes_7;

public:
	inline static int32_t get_offset_of_comments_1() { return static_cast<int32_t>(offsetof(CodeNamespace_t2165007136, ___comments_1)); }
	inline CodeCommentStatementCollection_t84489496 * get_comments_1() const { return ___comments_1; }
	inline CodeCommentStatementCollection_t84489496 ** get_address_of_comments_1() { return &___comments_1; }
	inline void set_comments_1(CodeCommentStatementCollection_t84489496 * value)
	{
		___comments_1 = value;
		Il2CppCodeGenWriteBarrier(&___comments_1, value);
	}

	inline static int32_t get_offset_of_imports_2() { return static_cast<int32_t>(offsetof(CodeNamespace_t2165007136, ___imports_2)); }
	inline CodeNamespaceImportCollection_t3818670277 * get_imports_2() const { return ___imports_2; }
	inline CodeNamespaceImportCollection_t3818670277 ** get_address_of_imports_2() { return &___imports_2; }
	inline void set_imports_2(CodeNamespaceImportCollection_t3818670277 * value)
	{
		___imports_2 = value;
		Il2CppCodeGenWriteBarrier(&___imports_2, value);
	}

	inline static int32_t get_offset_of_classes_3() { return static_cast<int32_t>(offsetof(CodeNamespace_t2165007136, ___classes_3)); }
	inline CodeTypeDeclarationCollection_t2229959800 * get_classes_3() const { return ___classes_3; }
	inline CodeTypeDeclarationCollection_t2229959800 ** get_address_of_classes_3() { return &___classes_3; }
	inline void set_classes_3(CodeTypeDeclarationCollection_t2229959800 * value)
	{
		___classes_3 = value;
		Il2CppCodeGenWriteBarrier(&___classes_3, value);
	}

	inline static int32_t get_offset_of_name_4() { return static_cast<int32_t>(offsetof(CodeNamespace_t2165007136, ___name_4)); }
	inline String_t* get_name_4() const { return ___name_4; }
	inline String_t** get_address_of_name_4() { return &___name_4; }
	inline void set_name_4(String_t* value)
	{
		___name_4 = value;
		Il2CppCodeGenWriteBarrier(&___name_4, value);
	}

	inline static int32_t get_offset_of_PopulateComments_5() { return static_cast<int32_t>(offsetof(CodeNamespace_t2165007136, ___PopulateComments_5)); }
	inline EventHandler_t1348719766 * get_PopulateComments_5() const { return ___PopulateComments_5; }
	inline EventHandler_t1348719766 ** get_address_of_PopulateComments_5() { return &___PopulateComments_5; }
	inline void set_PopulateComments_5(EventHandler_t1348719766 * value)
	{
		___PopulateComments_5 = value;
		Il2CppCodeGenWriteBarrier(&___PopulateComments_5, value);
	}

	inline static int32_t get_offset_of_PopulateImports_6() { return static_cast<int32_t>(offsetof(CodeNamespace_t2165007136, ___PopulateImports_6)); }
	inline EventHandler_t1348719766 * get_PopulateImports_6() const { return ___PopulateImports_6; }
	inline EventHandler_t1348719766 ** get_address_of_PopulateImports_6() { return &___PopulateImports_6; }
	inline void set_PopulateImports_6(EventHandler_t1348719766 * value)
	{
		___PopulateImports_6 = value;
		Il2CppCodeGenWriteBarrier(&___PopulateImports_6, value);
	}

	inline static int32_t get_offset_of_PopulateTypes_7() { return static_cast<int32_t>(offsetof(CodeNamespace_t2165007136, ___PopulateTypes_7)); }
	inline EventHandler_t1348719766 * get_PopulateTypes_7() const { return ___PopulateTypes_7; }
	inline EventHandler_t1348719766 ** get_address_of_PopulateTypes_7() { return &___PopulateTypes_7; }
	inline void set_PopulateTypes_7(EventHandler_t1348719766 * value)
	{
		___PopulateTypes_7 = value;
		Il2CppCodeGenWriteBarrier(&___PopulateTypes_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
