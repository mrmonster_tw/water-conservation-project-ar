﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Evaporation_1_ch
struct  Evaporation_1_ch_t1042206845  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject Evaporation_1_ch::Evaporation_1_ball
	GameObject_t1113636619 * ___Evaporation_1_ball_2;
	// UnityEngine.GameObject Evaporation_1_ch::finger
	GameObject_t1113636619 * ___finger_3;
	// UnityEngine.GameObject Evaporation_1_ch::touchfinger
	GameObject_t1113636619 * ___touchfinger_4;
	// UnityEngine.GameObject Evaporation_1_ch::s_1
	GameObject_t1113636619 * ___s_1_5;
	// UnityEngine.GameObject Evaporation_1_ch::s_2
	GameObject_t1113636619 * ___s_2_6;

public:
	inline static int32_t get_offset_of_Evaporation_1_ball_2() { return static_cast<int32_t>(offsetof(Evaporation_1_ch_t1042206845, ___Evaporation_1_ball_2)); }
	inline GameObject_t1113636619 * get_Evaporation_1_ball_2() const { return ___Evaporation_1_ball_2; }
	inline GameObject_t1113636619 ** get_address_of_Evaporation_1_ball_2() { return &___Evaporation_1_ball_2; }
	inline void set_Evaporation_1_ball_2(GameObject_t1113636619 * value)
	{
		___Evaporation_1_ball_2 = value;
		Il2CppCodeGenWriteBarrier(&___Evaporation_1_ball_2, value);
	}

	inline static int32_t get_offset_of_finger_3() { return static_cast<int32_t>(offsetof(Evaporation_1_ch_t1042206845, ___finger_3)); }
	inline GameObject_t1113636619 * get_finger_3() const { return ___finger_3; }
	inline GameObject_t1113636619 ** get_address_of_finger_3() { return &___finger_3; }
	inline void set_finger_3(GameObject_t1113636619 * value)
	{
		___finger_3 = value;
		Il2CppCodeGenWriteBarrier(&___finger_3, value);
	}

	inline static int32_t get_offset_of_touchfinger_4() { return static_cast<int32_t>(offsetof(Evaporation_1_ch_t1042206845, ___touchfinger_4)); }
	inline GameObject_t1113636619 * get_touchfinger_4() const { return ___touchfinger_4; }
	inline GameObject_t1113636619 ** get_address_of_touchfinger_4() { return &___touchfinger_4; }
	inline void set_touchfinger_4(GameObject_t1113636619 * value)
	{
		___touchfinger_4 = value;
		Il2CppCodeGenWriteBarrier(&___touchfinger_4, value);
	}

	inline static int32_t get_offset_of_s_1_5() { return static_cast<int32_t>(offsetof(Evaporation_1_ch_t1042206845, ___s_1_5)); }
	inline GameObject_t1113636619 * get_s_1_5() const { return ___s_1_5; }
	inline GameObject_t1113636619 ** get_address_of_s_1_5() { return &___s_1_5; }
	inline void set_s_1_5(GameObject_t1113636619 * value)
	{
		___s_1_5 = value;
		Il2CppCodeGenWriteBarrier(&___s_1_5, value);
	}

	inline static int32_t get_offset_of_s_2_6() { return static_cast<int32_t>(offsetof(Evaporation_1_ch_t1042206845, ___s_2_6)); }
	inline GameObject_t1113636619 * get_s_2_6() const { return ___s_2_6; }
	inline GameObject_t1113636619 ** get_address_of_s_2_6() { return &___s_2_6; }
	inline void set_s_2_6(GameObject_t1113636619 * value)
	{
		___s_2_6 = value;
		Il2CppCodeGenWriteBarrier(&___s_2_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
