﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.SortedList
struct SortedList_t2427694641;
// System.IO.Stream
struct Stream_t1273022909;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Resources.ResourceWriter
struct  ResourceWriter_t1892498018  : public Il2CppObject
{
public:
	// System.Collections.SortedList System.Resources.ResourceWriter::resources
	SortedList_t2427694641 * ___resources_0;
	// System.IO.Stream System.Resources.ResourceWriter::stream
	Stream_t1273022909 * ___stream_1;

public:
	inline static int32_t get_offset_of_resources_0() { return static_cast<int32_t>(offsetof(ResourceWriter_t1892498018, ___resources_0)); }
	inline SortedList_t2427694641 * get_resources_0() const { return ___resources_0; }
	inline SortedList_t2427694641 ** get_address_of_resources_0() { return &___resources_0; }
	inline void set_resources_0(SortedList_t2427694641 * value)
	{
		___resources_0 = value;
		Il2CppCodeGenWriteBarrier(&___resources_0, value);
	}

	inline static int32_t get_offset_of_stream_1() { return static_cast<int32_t>(offsetof(ResourceWriter_t1892498018, ___stream_1)); }
	inline Stream_t1273022909 * get_stream_1() const { return ___stream_1; }
	inline Stream_t1273022909 ** get_address_of_stream_1() { return &___stream_1; }
	inline void set_stream_1(Stream_t1273022909 * value)
	{
		___stream_1 = value;
		Il2CppCodeGenWriteBarrier(&___stream_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
