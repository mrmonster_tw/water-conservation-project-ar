﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Drawing_System_Drawing_Point4030882587.h"

// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Resources.ResXDataNode
struct  ResXDataNode_t2895009953  : public Il2CppObject
{
public:
	// System.String System.Resources.ResXDataNode::name
	String_t* ___name_0;
	// System.Object System.Resources.ResXDataNode::value
	Il2CppObject * ___value_1;
	// System.Type System.Resources.ResXDataNode::type
	Type_t * ___type_2;
	// System.String System.Resources.ResXDataNode::comment
	String_t* ___comment_3;
	// System.Drawing.Point System.Resources.ResXDataNode::pos
	Point_t4030882587  ___pos_4;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(ResXDataNode_t2895009953, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ResXDataNode_t2895009953, ___value_1)); }
	inline Il2CppObject * get_value_1() const { return ___value_1; }
	inline Il2CppObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Il2CppObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(ResXDataNode_t2895009953, ___type_2)); }
	inline Type_t * get_type_2() const { return ___type_2; }
	inline Type_t ** get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(Type_t * value)
	{
		___type_2 = value;
		Il2CppCodeGenWriteBarrier(&___type_2, value);
	}

	inline static int32_t get_offset_of_comment_3() { return static_cast<int32_t>(offsetof(ResXDataNode_t2895009953, ___comment_3)); }
	inline String_t* get_comment_3() const { return ___comment_3; }
	inline String_t** get_address_of_comment_3() { return &___comment_3; }
	inline void set_comment_3(String_t* value)
	{
		___comment_3 = value;
		Il2CppCodeGenWriteBarrier(&___comment_3, value);
	}

	inline static int32_t get_offset_of_pos_4() { return static_cast<int32_t>(offsetof(ResXDataNode_t2895009953, ___pos_4)); }
	inline Point_t4030882587  get_pos_4() const { return ___pos_4; }
	inline Point_t4030882587 * get_address_of_pos_4() { return &___pos_4; }
	inline void set_pos_4(Point_t4030882587  value)
	{
		___pos_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
