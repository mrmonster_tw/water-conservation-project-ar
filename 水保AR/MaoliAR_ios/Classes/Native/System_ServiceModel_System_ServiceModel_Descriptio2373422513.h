﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_B3441673553.h"

// System.Collections.ObjectModel.Collection`1<System.ServiceModel.Description.WstRequestSecurityTokenResponse>
struct Collection_1_t4128587481;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.WstRequestSecurityTokenResponseCollection
struct  WstRequestSecurityTokenResponseCollection_t2373422513  : public BodyWriter_t3441673553
{
public:
	// System.Collections.ObjectModel.Collection`1<System.ServiceModel.Description.WstRequestSecurityTokenResponse> System.ServiceModel.Description.WstRequestSecurityTokenResponseCollection::responses
	Collection_1_t4128587481 * ___responses_1;

public:
	inline static int32_t get_offset_of_responses_1() { return static_cast<int32_t>(offsetof(WstRequestSecurityTokenResponseCollection_t2373422513, ___responses_1)); }
	inline Collection_1_t4128587481 * get_responses_1() const { return ___responses_1; }
	inline Collection_1_t4128587481 ** get_address_of_responses_1() { return &___responses_1; }
	inline void set_responses_1(Collection_1_t4128587481 * value)
	{
		___responses_1 = value;
		Il2CppCodeGenWriteBarrier(&___responses_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
