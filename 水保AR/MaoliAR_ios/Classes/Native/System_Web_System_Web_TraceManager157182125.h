﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Web_System_Web_TraceMode4125462420.h"

// System.Web.TraceData[]
struct TraceDataU5BU5D_t1531861082;
// System.Exception
struct Exception_t1436737249;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.TraceManager
struct  TraceManager_t157182125  : public Il2CppObject
{
public:
	// System.Boolean System.Web.TraceManager::enabled
	bool ___enabled_0;
	// System.Boolean System.Web.TraceManager::local_only
	bool ___local_only_1;
	// System.Boolean System.Web.TraceManager::page_output
	bool ___page_output_2;
	// System.Web.TraceMode System.Web.TraceManager::mode
	int32_t ___mode_3;
	// System.Int32 System.Web.TraceManager::request_limit
	int32_t ___request_limit_4;
	// System.Int32 System.Web.TraceManager::cur_item
	int32_t ___cur_item_5;
	// System.Web.TraceData[] System.Web.TraceManager::data
	TraceDataU5BU5D_t1531861082* ___data_6;
	// System.Exception System.Web.TraceManager::initialException
	Exception_t1436737249 * ___initialException_7;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(TraceManager_t157182125, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}

	inline static int32_t get_offset_of_local_only_1() { return static_cast<int32_t>(offsetof(TraceManager_t157182125, ___local_only_1)); }
	inline bool get_local_only_1() const { return ___local_only_1; }
	inline bool* get_address_of_local_only_1() { return &___local_only_1; }
	inline void set_local_only_1(bool value)
	{
		___local_only_1 = value;
	}

	inline static int32_t get_offset_of_page_output_2() { return static_cast<int32_t>(offsetof(TraceManager_t157182125, ___page_output_2)); }
	inline bool get_page_output_2() const { return ___page_output_2; }
	inline bool* get_address_of_page_output_2() { return &___page_output_2; }
	inline void set_page_output_2(bool value)
	{
		___page_output_2 = value;
	}

	inline static int32_t get_offset_of_mode_3() { return static_cast<int32_t>(offsetof(TraceManager_t157182125, ___mode_3)); }
	inline int32_t get_mode_3() const { return ___mode_3; }
	inline int32_t* get_address_of_mode_3() { return &___mode_3; }
	inline void set_mode_3(int32_t value)
	{
		___mode_3 = value;
	}

	inline static int32_t get_offset_of_request_limit_4() { return static_cast<int32_t>(offsetof(TraceManager_t157182125, ___request_limit_4)); }
	inline int32_t get_request_limit_4() const { return ___request_limit_4; }
	inline int32_t* get_address_of_request_limit_4() { return &___request_limit_4; }
	inline void set_request_limit_4(int32_t value)
	{
		___request_limit_4 = value;
	}

	inline static int32_t get_offset_of_cur_item_5() { return static_cast<int32_t>(offsetof(TraceManager_t157182125, ___cur_item_5)); }
	inline int32_t get_cur_item_5() const { return ___cur_item_5; }
	inline int32_t* get_address_of_cur_item_5() { return &___cur_item_5; }
	inline void set_cur_item_5(int32_t value)
	{
		___cur_item_5 = value;
	}

	inline static int32_t get_offset_of_data_6() { return static_cast<int32_t>(offsetof(TraceManager_t157182125, ___data_6)); }
	inline TraceDataU5BU5D_t1531861082* get_data_6() const { return ___data_6; }
	inline TraceDataU5BU5D_t1531861082** get_address_of_data_6() { return &___data_6; }
	inline void set_data_6(TraceDataU5BU5D_t1531861082* value)
	{
		___data_6 = value;
		Il2CppCodeGenWriteBarrier(&___data_6, value);
	}

	inline static int32_t get_offset_of_initialException_7() { return static_cast<int32_t>(offsetof(TraceManager_t157182125, ___initialException_7)); }
	inline Exception_t1436737249 * get_initialException_7() const { return ___initialException_7; }
	inline Exception_t1436737249 ** get_address_of_initialException_7() { return &___initialException_7; }
	inline void set_initialException_7(Exception_t1436737249 * value)
	{
		___initialException_7 = value;
		Il2CppCodeGenWriteBarrier(&___initialException_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
