﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"

// System.Collections.Generic.List`1<System.Type>
struct List_1_t3956019502;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.Collections.Generic.List`1<HighlightingSystem.Highlighter/RendererCache>
struct List_1_t3376144042;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.Material
struct Material_t340375123;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HighlightingSystem.Highlighter
struct  Highlighter_t672210414  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color HighlightingSystem.Highlighter::occluderColor
	Color_t2555686324  ___occluderColor_8;
	// System.Boolean HighlightingSystem.Highlighter::<highlighted>k__BackingField
	bool ___U3ChighlightedU3Ek__BackingField_15;
	// UnityEngine.Transform HighlightingSystem.Highlighter::tr
	Transform_t3600365921 * ___tr_16;
	// System.Collections.Generic.List`1<HighlightingSystem.Highlighter/RendererCache> HighlightingSystem.Highlighter::highlightableRenderers
	List_1_t3376144042 * ___highlightableRenderers_17;
	// System.Int32 HighlightingSystem.Highlighter::visibilityCheckFrame
	int32_t ___visibilityCheckFrame_18;
	// System.Boolean HighlightingSystem.Highlighter::visibilityChanged
	bool ___visibilityChanged_19;
	// System.Boolean HighlightingSystem.Highlighter::visible
	bool ___visible_20;
	// System.Boolean HighlightingSystem.Highlighter::renderersDirty
	bool ___renderersDirty_21;
	// UnityEngine.Color HighlightingSystem.Highlighter::currentColor
	Color_t2555686324  ___currentColor_22;
	// System.Boolean HighlightingSystem.Highlighter::transitionActive
	bool ___transitionActive_23;
	// System.Single HighlightingSystem.Highlighter::transitionValue
	float ___transitionValue_24;
	// System.Single HighlightingSystem.Highlighter::flashingFreq
	float ___flashingFreq_25;
	// System.Int32 HighlightingSystem.Highlighter::_once
	int32_t ____once_26;
	// UnityEngine.Color HighlightingSystem.Highlighter::onceColor
	Color_t2555686324  ___onceColor_27;
	// System.Boolean HighlightingSystem.Highlighter::flashing
	bool ___flashing_28;
	// UnityEngine.Color HighlightingSystem.Highlighter::flashingColorMin
	Color_t2555686324  ___flashingColorMin_29;
	// UnityEngine.Color HighlightingSystem.Highlighter::flashingColorMax
	Color_t2555686324  ___flashingColorMax_30;
	// System.Boolean HighlightingSystem.Highlighter::constantly
	bool ___constantly_31;
	// UnityEngine.Color HighlightingSystem.Highlighter::constantColor
	Color_t2555686324  ___constantColor_32;
	// System.Boolean HighlightingSystem.Highlighter::occluder
	bool ___occluder_33;
	// System.Boolean HighlightingSystem.Highlighter::seeThrough
	bool ___seeThrough_34;
	// System.Int32 HighlightingSystem.Highlighter::renderQueue
	int32_t ___renderQueue_35;
	// System.Boolean HighlightingSystem.Highlighter::zTest
	bool ___zTest_36;
	// System.Boolean HighlightingSystem.Highlighter::stencilRef
	bool ___stencilRef_37;
	// UnityEngine.Material HighlightingSystem.Highlighter::_opaqueMaterial
	Material_t340375123 * ____opaqueMaterial_40;

public:
	inline static int32_t get_offset_of_occluderColor_8() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___occluderColor_8)); }
	inline Color_t2555686324  get_occluderColor_8() const { return ___occluderColor_8; }
	inline Color_t2555686324 * get_address_of_occluderColor_8() { return &___occluderColor_8; }
	inline void set_occluderColor_8(Color_t2555686324  value)
	{
		___occluderColor_8 = value;
	}

	inline static int32_t get_offset_of_U3ChighlightedU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___U3ChighlightedU3Ek__BackingField_15)); }
	inline bool get_U3ChighlightedU3Ek__BackingField_15() const { return ___U3ChighlightedU3Ek__BackingField_15; }
	inline bool* get_address_of_U3ChighlightedU3Ek__BackingField_15() { return &___U3ChighlightedU3Ek__BackingField_15; }
	inline void set_U3ChighlightedU3Ek__BackingField_15(bool value)
	{
		___U3ChighlightedU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_tr_16() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___tr_16)); }
	inline Transform_t3600365921 * get_tr_16() const { return ___tr_16; }
	inline Transform_t3600365921 ** get_address_of_tr_16() { return &___tr_16; }
	inline void set_tr_16(Transform_t3600365921 * value)
	{
		___tr_16 = value;
		Il2CppCodeGenWriteBarrier(&___tr_16, value);
	}

	inline static int32_t get_offset_of_highlightableRenderers_17() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___highlightableRenderers_17)); }
	inline List_1_t3376144042 * get_highlightableRenderers_17() const { return ___highlightableRenderers_17; }
	inline List_1_t3376144042 ** get_address_of_highlightableRenderers_17() { return &___highlightableRenderers_17; }
	inline void set_highlightableRenderers_17(List_1_t3376144042 * value)
	{
		___highlightableRenderers_17 = value;
		Il2CppCodeGenWriteBarrier(&___highlightableRenderers_17, value);
	}

	inline static int32_t get_offset_of_visibilityCheckFrame_18() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___visibilityCheckFrame_18)); }
	inline int32_t get_visibilityCheckFrame_18() const { return ___visibilityCheckFrame_18; }
	inline int32_t* get_address_of_visibilityCheckFrame_18() { return &___visibilityCheckFrame_18; }
	inline void set_visibilityCheckFrame_18(int32_t value)
	{
		___visibilityCheckFrame_18 = value;
	}

	inline static int32_t get_offset_of_visibilityChanged_19() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___visibilityChanged_19)); }
	inline bool get_visibilityChanged_19() const { return ___visibilityChanged_19; }
	inline bool* get_address_of_visibilityChanged_19() { return &___visibilityChanged_19; }
	inline void set_visibilityChanged_19(bool value)
	{
		___visibilityChanged_19 = value;
	}

	inline static int32_t get_offset_of_visible_20() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___visible_20)); }
	inline bool get_visible_20() const { return ___visible_20; }
	inline bool* get_address_of_visible_20() { return &___visible_20; }
	inline void set_visible_20(bool value)
	{
		___visible_20 = value;
	}

	inline static int32_t get_offset_of_renderersDirty_21() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___renderersDirty_21)); }
	inline bool get_renderersDirty_21() const { return ___renderersDirty_21; }
	inline bool* get_address_of_renderersDirty_21() { return &___renderersDirty_21; }
	inline void set_renderersDirty_21(bool value)
	{
		___renderersDirty_21 = value;
	}

	inline static int32_t get_offset_of_currentColor_22() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___currentColor_22)); }
	inline Color_t2555686324  get_currentColor_22() const { return ___currentColor_22; }
	inline Color_t2555686324 * get_address_of_currentColor_22() { return &___currentColor_22; }
	inline void set_currentColor_22(Color_t2555686324  value)
	{
		___currentColor_22 = value;
	}

	inline static int32_t get_offset_of_transitionActive_23() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___transitionActive_23)); }
	inline bool get_transitionActive_23() const { return ___transitionActive_23; }
	inline bool* get_address_of_transitionActive_23() { return &___transitionActive_23; }
	inline void set_transitionActive_23(bool value)
	{
		___transitionActive_23 = value;
	}

	inline static int32_t get_offset_of_transitionValue_24() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___transitionValue_24)); }
	inline float get_transitionValue_24() const { return ___transitionValue_24; }
	inline float* get_address_of_transitionValue_24() { return &___transitionValue_24; }
	inline void set_transitionValue_24(float value)
	{
		___transitionValue_24 = value;
	}

	inline static int32_t get_offset_of_flashingFreq_25() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___flashingFreq_25)); }
	inline float get_flashingFreq_25() const { return ___flashingFreq_25; }
	inline float* get_address_of_flashingFreq_25() { return &___flashingFreq_25; }
	inline void set_flashingFreq_25(float value)
	{
		___flashingFreq_25 = value;
	}

	inline static int32_t get_offset_of__once_26() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ____once_26)); }
	inline int32_t get__once_26() const { return ____once_26; }
	inline int32_t* get_address_of__once_26() { return &____once_26; }
	inline void set__once_26(int32_t value)
	{
		____once_26 = value;
	}

	inline static int32_t get_offset_of_onceColor_27() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___onceColor_27)); }
	inline Color_t2555686324  get_onceColor_27() const { return ___onceColor_27; }
	inline Color_t2555686324 * get_address_of_onceColor_27() { return &___onceColor_27; }
	inline void set_onceColor_27(Color_t2555686324  value)
	{
		___onceColor_27 = value;
	}

	inline static int32_t get_offset_of_flashing_28() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___flashing_28)); }
	inline bool get_flashing_28() const { return ___flashing_28; }
	inline bool* get_address_of_flashing_28() { return &___flashing_28; }
	inline void set_flashing_28(bool value)
	{
		___flashing_28 = value;
	}

	inline static int32_t get_offset_of_flashingColorMin_29() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___flashingColorMin_29)); }
	inline Color_t2555686324  get_flashingColorMin_29() const { return ___flashingColorMin_29; }
	inline Color_t2555686324 * get_address_of_flashingColorMin_29() { return &___flashingColorMin_29; }
	inline void set_flashingColorMin_29(Color_t2555686324  value)
	{
		___flashingColorMin_29 = value;
	}

	inline static int32_t get_offset_of_flashingColorMax_30() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___flashingColorMax_30)); }
	inline Color_t2555686324  get_flashingColorMax_30() const { return ___flashingColorMax_30; }
	inline Color_t2555686324 * get_address_of_flashingColorMax_30() { return &___flashingColorMax_30; }
	inline void set_flashingColorMax_30(Color_t2555686324  value)
	{
		___flashingColorMax_30 = value;
	}

	inline static int32_t get_offset_of_constantly_31() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___constantly_31)); }
	inline bool get_constantly_31() const { return ___constantly_31; }
	inline bool* get_address_of_constantly_31() { return &___constantly_31; }
	inline void set_constantly_31(bool value)
	{
		___constantly_31 = value;
	}

	inline static int32_t get_offset_of_constantColor_32() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___constantColor_32)); }
	inline Color_t2555686324  get_constantColor_32() const { return ___constantColor_32; }
	inline Color_t2555686324 * get_address_of_constantColor_32() { return &___constantColor_32; }
	inline void set_constantColor_32(Color_t2555686324  value)
	{
		___constantColor_32 = value;
	}

	inline static int32_t get_offset_of_occluder_33() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___occluder_33)); }
	inline bool get_occluder_33() const { return ___occluder_33; }
	inline bool* get_address_of_occluder_33() { return &___occluder_33; }
	inline void set_occluder_33(bool value)
	{
		___occluder_33 = value;
	}

	inline static int32_t get_offset_of_seeThrough_34() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___seeThrough_34)); }
	inline bool get_seeThrough_34() const { return ___seeThrough_34; }
	inline bool* get_address_of_seeThrough_34() { return &___seeThrough_34; }
	inline void set_seeThrough_34(bool value)
	{
		___seeThrough_34 = value;
	}

	inline static int32_t get_offset_of_renderQueue_35() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___renderQueue_35)); }
	inline int32_t get_renderQueue_35() const { return ___renderQueue_35; }
	inline int32_t* get_address_of_renderQueue_35() { return &___renderQueue_35; }
	inline void set_renderQueue_35(int32_t value)
	{
		___renderQueue_35 = value;
	}

	inline static int32_t get_offset_of_zTest_36() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___zTest_36)); }
	inline bool get_zTest_36() const { return ___zTest_36; }
	inline bool* get_address_of_zTest_36() { return &___zTest_36; }
	inline void set_zTest_36(bool value)
	{
		___zTest_36 = value;
	}

	inline static int32_t get_offset_of_stencilRef_37() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___stencilRef_37)); }
	inline bool get_stencilRef_37() const { return ___stencilRef_37; }
	inline bool* get_address_of_stencilRef_37() { return &___stencilRef_37; }
	inline void set_stencilRef_37(bool value)
	{
		___stencilRef_37 = value;
	}

	inline static int32_t get_offset_of__opaqueMaterial_40() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ____opaqueMaterial_40)); }
	inline Material_t340375123 * get__opaqueMaterial_40() const { return ____opaqueMaterial_40; }
	inline Material_t340375123 ** get_address_of__opaqueMaterial_40() { return &____opaqueMaterial_40; }
	inline void set__opaqueMaterial_40(Material_t340375123 * value)
	{
		____opaqueMaterial_40 = value;
		Il2CppCodeGenWriteBarrier(&____opaqueMaterial_40, value);
	}
};

struct Highlighter_t672210414_StaticFields
{
public:
	// System.Single HighlightingSystem.Highlighter::constantOnSpeed
	float ___constantOnSpeed_2;
	// System.Single HighlightingSystem.Highlighter::constantOffSpeed
	float ___constantOffSpeed_3;
	// System.Single HighlightingSystem.Highlighter::transparentCutoff
	float ___transparentCutoff_4;
	// System.Collections.Generic.List`1<System.Type> HighlightingSystem.Highlighter::types
	List_1_t3956019502 * ___types_6;
	// System.Single HighlightingSystem.Highlighter::zWrite
	float ___zWrite_12;
	// System.Single HighlightingSystem.Highlighter::offsetFactor
	float ___offsetFactor_13;
	// System.Single HighlightingSystem.Highlighter::offsetUnits
	float ___offsetUnits_14;
	// UnityEngine.Shader HighlightingSystem.Highlighter::_opaqueShader
	Shader_t4151988712 * ____opaqueShader_38;
	// UnityEngine.Shader HighlightingSystem.Highlighter::_transparentShader
	Shader_t4151988712 * ____transparentShader_39;

public:
	inline static int32_t get_offset_of_constantOnSpeed_2() { return static_cast<int32_t>(offsetof(Highlighter_t672210414_StaticFields, ___constantOnSpeed_2)); }
	inline float get_constantOnSpeed_2() const { return ___constantOnSpeed_2; }
	inline float* get_address_of_constantOnSpeed_2() { return &___constantOnSpeed_2; }
	inline void set_constantOnSpeed_2(float value)
	{
		___constantOnSpeed_2 = value;
	}

	inline static int32_t get_offset_of_constantOffSpeed_3() { return static_cast<int32_t>(offsetof(Highlighter_t672210414_StaticFields, ___constantOffSpeed_3)); }
	inline float get_constantOffSpeed_3() const { return ___constantOffSpeed_3; }
	inline float* get_address_of_constantOffSpeed_3() { return &___constantOffSpeed_3; }
	inline void set_constantOffSpeed_3(float value)
	{
		___constantOffSpeed_3 = value;
	}

	inline static int32_t get_offset_of_transparentCutoff_4() { return static_cast<int32_t>(offsetof(Highlighter_t672210414_StaticFields, ___transparentCutoff_4)); }
	inline float get_transparentCutoff_4() const { return ___transparentCutoff_4; }
	inline float* get_address_of_transparentCutoff_4() { return &___transparentCutoff_4; }
	inline void set_transparentCutoff_4(float value)
	{
		___transparentCutoff_4 = value;
	}

	inline static int32_t get_offset_of_types_6() { return static_cast<int32_t>(offsetof(Highlighter_t672210414_StaticFields, ___types_6)); }
	inline List_1_t3956019502 * get_types_6() const { return ___types_6; }
	inline List_1_t3956019502 ** get_address_of_types_6() { return &___types_6; }
	inline void set_types_6(List_1_t3956019502 * value)
	{
		___types_6 = value;
		Il2CppCodeGenWriteBarrier(&___types_6, value);
	}

	inline static int32_t get_offset_of_zWrite_12() { return static_cast<int32_t>(offsetof(Highlighter_t672210414_StaticFields, ___zWrite_12)); }
	inline float get_zWrite_12() const { return ___zWrite_12; }
	inline float* get_address_of_zWrite_12() { return &___zWrite_12; }
	inline void set_zWrite_12(float value)
	{
		___zWrite_12 = value;
	}

	inline static int32_t get_offset_of_offsetFactor_13() { return static_cast<int32_t>(offsetof(Highlighter_t672210414_StaticFields, ___offsetFactor_13)); }
	inline float get_offsetFactor_13() const { return ___offsetFactor_13; }
	inline float* get_address_of_offsetFactor_13() { return &___offsetFactor_13; }
	inline void set_offsetFactor_13(float value)
	{
		___offsetFactor_13 = value;
	}

	inline static int32_t get_offset_of_offsetUnits_14() { return static_cast<int32_t>(offsetof(Highlighter_t672210414_StaticFields, ___offsetUnits_14)); }
	inline float get_offsetUnits_14() const { return ___offsetUnits_14; }
	inline float* get_address_of_offsetUnits_14() { return &___offsetUnits_14; }
	inline void set_offsetUnits_14(float value)
	{
		___offsetUnits_14 = value;
	}

	inline static int32_t get_offset_of__opaqueShader_38() { return static_cast<int32_t>(offsetof(Highlighter_t672210414_StaticFields, ____opaqueShader_38)); }
	inline Shader_t4151988712 * get__opaqueShader_38() const { return ____opaqueShader_38; }
	inline Shader_t4151988712 ** get_address_of__opaqueShader_38() { return &____opaqueShader_38; }
	inline void set__opaqueShader_38(Shader_t4151988712 * value)
	{
		____opaqueShader_38 = value;
		Il2CppCodeGenWriteBarrier(&____opaqueShader_38, value);
	}

	inline static int32_t get_offset_of__transparentShader_39() { return static_cast<int32_t>(offsetof(Highlighter_t672210414_StaticFields, ____transparentShader_39)); }
	inline Shader_t4151988712 * get__transparentShader_39() const { return ____transparentShader_39; }
	inline Shader_t4151988712 ** get_address_of__transparentShader_39() { return &____transparentShader_39; }
	inline void set__transparentShader_39(Shader_t4151988712 * value)
	{
		____transparentShader_39 = value;
		Il2CppCodeGenWriteBarrier(&____transparentShader_39, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
