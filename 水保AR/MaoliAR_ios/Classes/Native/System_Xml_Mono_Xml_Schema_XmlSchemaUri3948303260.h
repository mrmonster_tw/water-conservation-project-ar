﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Uri100236324.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XmlSchemaUri
struct  XmlSchemaUri_t3948303260  : public Uri_t100236324
{
public:
	// System.String Mono.Xml.Schema.XmlSchemaUri::value
	String_t* ___value_37;

public:
	inline static int32_t get_offset_of_value_37() { return static_cast<int32_t>(offsetof(XmlSchemaUri_t3948303260, ___value_37)); }
	inline String_t* get_value_37() const { return ___value_37; }
	inline String_t** get_address_of_value_37() { return &___value_37; }
	inline void set_value_37(String_t* value)
	{
		___value_37 = value;
		Il2CppCodeGenWriteBarrier(&___value_37, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
