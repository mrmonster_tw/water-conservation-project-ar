﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Collections_ObjectModel_KeyedColle3485127696.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.MessageHeaderDescriptionCollection
struct  MessageHeaderDescriptionCollection_t4093932642  : public KeyedCollection_2_t3485127696
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
