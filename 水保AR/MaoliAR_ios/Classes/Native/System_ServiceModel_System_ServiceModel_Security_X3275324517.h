﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Security.X509ClientCertificateAuthentication
struct X509ClientCertificateAuthentication_t3667333716;
// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t714049126;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.X509CertificateInitiatorServiceCredential
struct  X509CertificateInitiatorServiceCredential_t3275324517  : public Il2CppObject
{
public:
	// System.ServiceModel.Security.X509ClientCertificateAuthentication System.ServiceModel.Security.X509CertificateInitiatorServiceCredential::auth
	X509ClientCertificateAuthentication_t3667333716 * ___auth_0;
	// System.Security.Cryptography.X509Certificates.X509Certificate2 System.ServiceModel.Security.X509CertificateInitiatorServiceCredential::certificate
	X509Certificate2_t714049126 * ___certificate_1;

public:
	inline static int32_t get_offset_of_auth_0() { return static_cast<int32_t>(offsetof(X509CertificateInitiatorServiceCredential_t3275324517, ___auth_0)); }
	inline X509ClientCertificateAuthentication_t3667333716 * get_auth_0() const { return ___auth_0; }
	inline X509ClientCertificateAuthentication_t3667333716 ** get_address_of_auth_0() { return &___auth_0; }
	inline void set_auth_0(X509ClientCertificateAuthentication_t3667333716 * value)
	{
		___auth_0 = value;
		Il2CppCodeGenWriteBarrier(&___auth_0, value);
	}

	inline static int32_t get_offset_of_certificate_1() { return static_cast<int32_t>(offsetof(X509CertificateInitiatorServiceCredential_t3275324517, ___certificate_1)); }
	inline X509Certificate2_t714049126 * get_certificate_1() const { return ___certificate_1; }
	inline X509Certificate2_t714049126 ** get_address_of_certificate_1() { return &___certificate_1; }
	inline void set_certificate_1(X509Certificate2_t714049126 * value)
	{
		___certificate_1 = value;
		Il2CppCodeGenWriteBarrier(&___certificate_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
