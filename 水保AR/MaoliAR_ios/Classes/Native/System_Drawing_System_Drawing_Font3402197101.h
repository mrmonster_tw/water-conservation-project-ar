﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_MarshalByRefObject2760389100.h"
#include "mscorlib_System_IntPtr840150181.h"
#include "System_Drawing_System_Drawing_FontStyle2978677421.h"
#include "System_Drawing_System_Drawing_GraphicsUnit4033181596.h"

// System.String
struct String_t;
// System.Drawing.FontFamily
struct FontFamily_t13778311;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Drawing.Font
struct  Font_t3402197101  : public MarshalByRefObject_t2760389100
{
public:
	// System.IntPtr System.Drawing.Font::fontObject
	IntPtr_t ___fontObject_1;
	// System.String System.Drawing.Font::originalFontName
	String_t* ___originalFontName_2;
	// System.Single System.Drawing.Font::_size
	float ____size_3;
	// System.Boolean System.Drawing.Font::_bold
	bool ____bold_5;
	// System.Drawing.FontFamily System.Drawing.Font::_fontFamily
	FontFamily_t13778311 * ____fontFamily_6;
	// System.Byte System.Drawing.Font::_gdiCharSet
	uint8_t ____gdiCharSet_7;
	// System.Boolean System.Drawing.Font::_gdiVerticalFont
	bool ____gdiVerticalFont_8;
	// System.Boolean System.Drawing.Font::_italic
	bool ____italic_9;
	// System.String System.Drawing.Font::_name
	String_t* ____name_10;
	// System.Single System.Drawing.Font::_sizeInPoints
	float ____sizeInPoints_11;
	// System.Boolean System.Drawing.Font::_strikeout
	bool ____strikeout_12;
	// System.Drawing.FontStyle System.Drawing.Font::_style
	int32_t ____style_13;
	// System.Boolean System.Drawing.Font::_underline
	bool ____underline_14;
	// System.Drawing.GraphicsUnit System.Drawing.Font::_unit
	int32_t ____unit_15;

public:
	inline static int32_t get_offset_of_fontObject_1() { return static_cast<int32_t>(offsetof(Font_t3402197101, ___fontObject_1)); }
	inline IntPtr_t get_fontObject_1() const { return ___fontObject_1; }
	inline IntPtr_t* get_address_of_fontObject_1() { return &___fontObject_1; }
	inline void set_fontObject_1(IntPtr_t value)
	{
		___fontObject_1 = value;
	}

	inline static int32_t get_offset_of_originalFontName_2() { return static_cast<int32_t>(offsetof(Font_t3402197101, ___originalFontName_2)); }
	inline String_t* get_originalFontName_2() const { return ___originalFontName_2; }
	inline String_t** get_address_of_originalFontName_2() { return &___originalFontName_2; }
	inline void set_originalFontName_2(String_t* value)
	{
		___originalFontName_2 = value;
		Il2CppCodeGenWriteBarrier(&___originalFontName_2, value);
	}

	inline static int32_t get_offset_of__size_3() { return static_cast<int32_t>(offsetof(Font_t3402197101, ____size_3)); }
	inline float get__size_3() const { return ____size_3; }
	inline float* get_address_of__size_3() { return &____size_3; }
	inline void set__size_3(float value)
	{
		____size_3 = value;
	}

	inline static int32_t get_offset_of__bold_5() { return static_cast<int32_t>(offsetof(Font_t3402197101, ____bold_5)); }
	inline bool get__bold_5() const { return ____bold_5; }
	inline bool* get_address_of__bold_5() { return &____bold_5; }
	inline void set__bold_5(bool value)
	{
		____bold_5 = value;
	}

	inline static int32_t get_offset_of__fontFamily_6() { return static_cast<int32_t>(offsetof(Font_t3402197101, ____fontFamily_6)); }
	inline FontFamily_t13778311 * get__fontFamily_6() const { return ____fontFamily_6; }
	inline FontFamily_t13778311 ** get_address_of__fontFamily_6() { return &____fontFamily_6; }
	inline void set__fontFamily_6(FontFamily_t13778311 * value)
	{
		____fontFamily_6 = value;
		Il2CppCodeGenWriteBarrier(&____fontFamily_6, value);
	}

	inline static int32_t get_offset_of__gdiCharSet_7() { return static_cast<int32_t>(offsetof(Font_t3402197101, ____gdiCharSet_7)); }
	inline uint8_t get__gdiCharSet_7() const { return ____gdiCharSet_7; }
	inline uint8_t* get_address_of__gdiCharSet_7() { return &____gdiCharSet_7; }
	inline void set__gdiCharSet_7(uint8_t value)
	{
		____gdiCharSet_7 = value;
	}

	inline static int32_t get_offset_of__gdiVerticalFont_8() { return static_cast<int32_t>(offsetof(Font_t3402197101, ____gdiVerticalFont_8)); }
	inline bool get__gdiVerticalFont_8() const { return ____gdiVerticalFont_8; }
	inline bool* get_address_of__gdiVerticalFont_8() { return &____gdiVerticalFont_8; }
	inline void set__gdiVerticalFont_8(bool value)
	{
		____gdiVerticalFont_8 = value;
	}

	inline static int32_t get_offset_of__italic_9() { return static_cast<int32_t>(offsetof(Font_t3402197101, ____italic_9)); }
	inline bool get__italic_9() const { return ____italic_9; }
	inline bool* get_address_of__italic_9() { return &____italic_9; }
	inline void set__italic_9(bool value)
	{
		____italic_9 = value;
	}

	inline static int32_t get_offset_of__name_10() { return static_cast<int32_t>(offsetof(Font_t3402197101, ____name_10)); }
	inline String_t* get__name_10() const { return ____name_10; }
	inline String_t** get_address_of__name_10() { return &____name_10; }
	inline void set__name_10(String_t* value)
	{
		____name_10 = value;
		Il2CppCodeGenWriteBarrier(&____name_10, value);
	}

	inline static int32_t get_offset_of__sizeInPoints_11() { return static_cast<int32_t>(offsetof(Font_t3402197101, ____sizeInPoints_11)); }
	inline float get__sizeInPoints_11() const { return ____sizeInPoints_11; }
	inline float* get_address_of__sizeInPoints_11() { return &____sizeInPoints_11; }
	inline void set__sizeInPoints_11(float value)
	{
		____sizeInPoints_11 = value;
	}

	inline static int32_t get_offset_of__strikeout_12() { return static_cast<int32_t>(offsetof(Font_t3402197101, ____strikeout_12)); }
	inline bool get__strikeout_12() const { return ____strikeout_12; }
	inline bool* get_address_of__strikeout_12() { return &____strikeout_12; }
	inline void set__strikeout_12(bool value)
	{
		____strikeout_12 = value;
	}

	inline static int32_t get_offset_of__style_13() { return static_cast<int32_t>(offsetof(Font_t3402197101, ____style_13)); }
	inline int32_t get__style_13() const { return ____style_13; }
	inline int32_t* get_address_of__style_13() { return &____style_13; }
	inline void set__style_13(int32_t value)
	{
		____style_13 = value;
	}

	inline static int32_t get_offset_of__underline_14() { return static_cast<int32_t>(offsetof(Font_t3402197101, ____underline_14)); }
	inline bool get__underline_14() const { return ____underline_14; }
	inline bool* get_address_of__underline_14() { return &____underline_14; }
	inline void set__underline_14(bool value)
	{
		____underline_14 = value;
	}

	inline static int32_t get_offset_of__unit_15() { return static_cast<int32_t>(offsetof(Font_t3402197101, ____unit_15)); }
	inline int32_t get__unit_15() const { return ____unit_15; }
	inline int32_t* get_address_of__unit_15() { return &____unit_15; }
	inline void set__unit_15(int32_t value)
	{
		____unit_15 = value;
	}
};

struct Font_t3402197101_StaticFields
{
public:
	// System.Int32 System.Drawing.Font::CharSetOffset
	int32_t ___CharSetOffset_4;

public:
	inline static int32_t get_offset_of_CharSetOffset_4() { return static_cast<int32_t>(offsetof(Font_t3402197101_StaticFields, ___CharSetOffset_4)); }
	inline int32_t get_CharSetOffset_4() const { return ___CharSetOffset_4; }
	inline int32_t* get_address_of_CharSetOffset_4() { return &___CharSetOffset_4; }
	inline void set_CharSetOffset_4(int32_t value)
	{
		___CharSetOffset_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
