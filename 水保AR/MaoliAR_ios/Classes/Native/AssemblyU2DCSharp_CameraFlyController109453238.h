﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Vector33722313464.h"

// UnityEngine.Transform
struct Transform_t3600365921;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFlyController
struct  CameraFlyController_t109453238  : public MonoBehaviour_t3962482529
{
public:
	// System.Single CameraFlyController::speed
	float ___speed_2;
	// UnityEngine.Transform CameraFlyController::tr
	Transform_t3600365921 * ___tr_3;
	// UnityEngine.Vector3 CameraFlyController::mpStart
	Vector3_t3722313464  ___mpStart_4;
	// UnityEngine.Vector3 CameraFlyController::originalRotation
	Vector3_t3722313464  ___originalRotation_5;
	// System.Single CameraFlyController::t
	float ___t_6;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_tr_3() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238, ___tr_3)); }
	inline Transform_t3600365921 * get_tr_3() const { return ___tr_3; }
	inline Transform_t3600365921 ** get_address_of_tr_3() { return &___tr_3; }
	inline void set_tr_3(Transform_t3600365921 * value)
	{
		___tr_3 = value;
		Il2CppCodeGenWriteBarrier(&___tr_3, value);
	}

	inline static int32_t get_offset_of_mpStart_4() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238, ___mpStart_4)); }
	inline Vector3_t3722313464  get_mpStart_4() const { return ___mpStart_4; }
	inline Vector3_t3722313464 * get_address_of_mpStart_4() { return &___mpStart_4; }
	inline void set_mpStart_4(Vector3_t3722313464  value)
	{
		___mpStart_4 = value;
	}

	inline static int32_t get_offset_of_originalRotation_5() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238, ___originalRotation_5)); }
	inline Vector3_t3722313464  get_originalRotation_5() const { return ___originalRotation_5; }
	inline Vector3_t3722313464 * get_address_of_originalRotation_5() { return &___originalRotation_5; }
	inline void set_originalRotation_5(Vector3_t3722313464  value)
	{
		___originalRotation_5 = value;
	}

	inline static int32_t get_offset_of_t_6() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238, ___t_6)); }
	inline float get_t_6() const { return ___t_6; }
	inline float* get_address_of_t_6() { return &___t_6; }
	inline void set_t_6(float value)
	{
		___t_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
