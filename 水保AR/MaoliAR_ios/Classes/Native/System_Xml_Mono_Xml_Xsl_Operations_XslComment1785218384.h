﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Operations_XslCompiledElemen50593777.h"

// Mono.Xml.Xsl.Operations.XslOperation
struct XslOperation_t2153241355;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslComment
struct  XslComment_t1785218384  : public XslCompiledElement_t50593777
{
public:
	// Mono.Xml.Xsl.Operations.XslOperation Mono.Xml.Xsl.Operations.XslComment::value
	XslOperation_t2153241355 * ___value_3;

public:
	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(XslComment_t1785218384, ___value_3)); }
	inline XslOperation_t2153241355 * get_value_3() const { return ___value_3; }
	inline XslOperation_t2153241355 ** get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(XslOperation_t2153241355 * value)
	{
		___value_3 = value;
		Il2CppCodeGenWriteBarrier(&___value_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
