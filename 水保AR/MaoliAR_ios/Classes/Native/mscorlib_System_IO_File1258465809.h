﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_Nullable_1_gen1166124571.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.File
struct  File_t1258465809  : public Il2CppObject
{
public:

public:
};

struct File_t1258465809_StaticFields
{
public:
	// System.Nullable`1<System.DateTime> System.IO.File::defaultLocalFileTime
	Nullable_1_t1166124571  ___defaultLocalFileTime_0;

public:
	inline static int32_t get_offset_of_defaultLocalFileTime_0() { return static_cast<int32_t>(offsetof(File_t1258465809_StaticFields, ___defaultLocalFileTime_0)); }
	inline Nullable_1_t1166124571  get_defaultLocalFileTime_0() const { return ___defaultLocalFileTime_0; }
	inline Nullable_1_t1166124571 * get_address_of_defaultLocalFileTime_0() { return &___defaultLocalFileTime_0; }
	inline void set_defaultLocalFileTime_0(Nullable_1_t1166124571  value)
	{
		___defaultLocalFileTime_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
