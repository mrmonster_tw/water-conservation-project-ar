﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_Text_RegularExpressions_IntervalColl1722821004.h"
#include "System_System_Text_RegularExpressions_MatchCollect1395363720.h"
#include "System_System_Text_RegularExpressions_MatchCollect2645102469.h"
#include "System_System_Text_RegularExpressions_Match3408321083.h"
#include "System_System_Text_RegularExpressions_Syntax_Parse2430509383.h"
#include "System_System_Text_RegularExpressions_QuickSearch2588090110.h"
#include "System_System_Text_RegularExpressions_Regex3657309853.h"
#include "System_System_Text_RegularExpressions_Regex_Adapte3525834651.h"
#include "System_System_Text_RegularExpressions_RegexOptions92845595.h"
#include "System_System_Text_RegularExpressions_RxInterprete3591201055.h"
#include "System_System_Text_RegularExpressions_RxInterpreter561212167.h"
#include "System_System_Text_RegularExpressions_RxInterprete2643093670.h"
#include "System_System_Text_RegularExpressions_RxLinkRef3349372210.h"
#include "System_System_Text_RegularExpressions_RxCompiler80015800.h"
#include "System_System_Text_RegularExpressions_RxInterprete3897605822.h"
#include "System_System_Text_RegularExpressions_RxOp349969367.h"
#include "System_System_Text_RegularExpressions_ReplacementE1065029019.h"
#include "System_System_Text_RegularExpressions_Syntax_Expre1810289389.h"
#include "System_System_Text_RegularExpressions_Syntax_Expre2722445759.h"
#include "System_System_Text_RegularExpressions_Syntax_Compo1252229802.h"
#include "System_System_Text_RegularExpressions_Syntax_Group1458537008.h"
#include "System_System_Text_RegularExpressions_Syntax_Regul3834220169.h"
#include "System_System_Text_RegularExpressions_Syntax_Captur751358689.h"
#include "System_System_Text_RegularExpressions_Syntax_Balan2395658894.h"
#include "System_System_Text_RegularExpressions_Syntax_NonBa3074098547.h"
#include "System_System_Text_RegularExpressions_Syntax_Repet2393242404.h"
#include "System_System_Text_RegularExpressions_Syntax_Asser3267412828.h"
#include "System_System_Text_RegularExpressions_Syntax_Captu3786084589.h"
#include "System_System_Text_RegularExpressions_Syntax_Expre1861210811.h"
#include "System_System_Text_RegularExpressions_Syntax_Altern625481451.h"
#include "System_System_Text_RegularExpressions_Syntax_Litera434143540.h"
#include "System_System_Text_RegularExpressions_Syntax_Posit3339288061.h"
#include "System_System_Text_RegularExpressions_Syntax_Refer1799410108.h"
#include "System_System_Text_RegularExpressions_Syntax_Backs3656518667.h"
#include "System_System_Text_RegularExpressions_Syntax_Charac839120860.h"
#include "System_System_Text_RegularExpressions_Syntax_Ancho3387011151.h"
#include "System_System_UriBuilder579353065.h"
#include "System_System_UriComponents814099658.h"
#include "System_System_Uri100236324.h"
#include "System_System_Uri_UriScheme722425697.h"
#include "System_System_UriFormat2031163398.h"
#include "System_System_UriFormatException953270471.h"
#include "System_System_UriHostNameType881866241.h"
#include "System_System_UriKind3816567336.h"
#include "System_System_UriParser3890150400.h"
#include "System_System_UriPartial1736313903.h"
#include "System_System_UriTypeConverter3695916615.h"
#include "System_System_Runtime_InteropServices_HandleCollec2917059441.h"
#include "System_System_Runtime_InteropServices_StandardOleM3279439477.h"
#include "System_System_Diagnostics_DataReceivedEventHandler2795960821.h"
#include "System_System_IO_ErrorEventHandler2621677363.h"
#include "System_System_IO_FileSystemEventHandler1806121106.h"
#include "System_System_IO_RenamedEventHandler3047461033.h"
#include "System_System_Net_AuthenticationSchemeSelector375327801.h"
#include "System_System_Net_BindIPEndPoint1029027275.h"
#include "System_System_Net_HttpContinueDelegate3009151163.h"
#include "System_System_Net_Security_LocalCertificateSelecti2354453884.h"
#include "System_System_Net_Security_RemoteCertificateValida3014364904.h"
#include "System_System_Text_RegularExpressions_MatchEvaluato632122704.h"
#include "System_System_Text_RegularExpressions_EvalDelegate1206020564.h"
#include "System_U3CPrivateImplementationDetailsU3E3057255361.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array3254766644.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array3652892010.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array4289081659.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array2490092596.h"
#include "System_Transactions_U3CModuleU3E692745525.h"
#include "System_Transactions_System_MonoTODOAttribute4131080581.h"
#include "System_Transactions_System_Transactions_Committable152884219.h"
#include "System_Transactions_System_Transactions_Enlistment1988529039.h"
#include "System_Transactions_System_Transactions_EnterpriseS695190331.h"
#include "System_Transactions_System_Transactions_IsolationL4247150849.h"
#include "System_Transactions_System_Transactions_PreparingE4199633836.h"
#include "System_Transactions_System_Transactions_SinglePhas2412016327.h"
#include "System_Transactions_System_Transactions_Transactio3472000926.h"
#include "System_Transactions_System_Transactions_Transactio2116885608.h"
#include "System_Transactions_System_Transactions_Transactio1281392668.h"
#include "System_Transactions_System_Transactions_Transactio2459298917.h"
#include "System_Transactions_System_Transactions_Transactio2760750674.h"
#include "System_Transactions_System_Transactions_Transactio2865697824.h"
#include "System_Transactions_System_Transactions_Transactio3249669472.h"
#include "System_Transactions_System_Transactions_Transactio1495782207.h"
#include "System_Transactions_System_Transactions_Transaction192984806.h"
#include "Mono_Data_Tds_U3CModuleU3E692745525.h"
#include "System_EnterpriseServices_U3CModuleU3E692745525.h"
#include "System_EnterpriseServices_System_EnterpriseServices_25113873.h"
#include "System_EnterpriseServices_System_EnterpriseService2032989562.h"
#include "Mono_Posix_U3CModuleU3E692745525.h"
#include "Mono_Posix_Mono_Unix_Native_FileNameMarshaler4011664002.h"
#include "Mono_Posix_MapAttribute440548554.h"
#include "Mono_Posix_Mono_Unix_Native_Stdlib3308777473.h"
#include "Mono_Posix_Mono_Unix_Native_FilePermissions1971995193.h"
#include "Mono_Posix_Mono_Unix_Native_Signum4209417157.h"
#include "Mono_Posix_Mono_Unix_Native_Syscall2623000133.h"
#include "Mono_Posix_Mono_Unix_Native_SignalHandler1590986791.h"
#include "System_Data_U3CModuleU3E692745525.h"
#include "Mono_Data_Sqlite_U3CModuleU3E692745525.h"
#include "System_Core_U3CModuleU3E692745525.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (CostDelegate_t1722821004), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (MatchCollection_t1395363720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2701[2] = 
{
	MatchCollection_t1395363720::get_offset_of_current_0(),
	MatchCollection_t1395363720::get_offset_of_list_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (Enumerator_t2645102469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2702[2] = 
{
	Enumerator_t2645102469::get_offset_of_index_0(),
	Enumerator_t2645102469::get_offset_of_coll_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (Match_t3408321083), -1, sizeof(Match_t3408321083_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2703[5] = 
{
	Match_t3408321083::get_offset_of_regex_6(),
	Match_t3408321083::get_offset_of_machine_7(),
	Match_t3408321083::get_offset_of_text_length_8(),
	Match_t3408321083::get_offset_of_groups_9(),
	Match_t3408321083_StaticFields::get_offset_of_empty_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (Parser_t2430509383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2704[6] = 
{
	Parser_t2430509383::get_offset_of_pattern_0(),
	Parser_t2430509383::get_offset_of_ptr_1(),
	Parser_t2430509383::get_offset_of_caps_2(),
	Parser_t2430509383::get_offset_of_refs_3(),
	Parser_t2430509383::get_offset_of_num_groups_4(),
	Parser_t2430509383::get_offset_of_gap_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { sizeof (QuickSearch_t2588090110), -1, sizeof(QuickSearch_t2588090110_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2705[7] = 
{
	QuickSearch_t2588090110::get_offset_of_str_0(),
	QuickSearch_t2588090110::get_offset_of_len_1(),
	QuickSearch_t2588090110::get_offset_of_ignore_2(),
	QuickSearch_t2588090110::get_offset_of_reverse_3(),
	QuickSearch_t2588090110::get_offset_of_shift_4(),
	QuickSearch_t2588090110::get_offset_of_shiftExtended_5(),
	QuickSearch_t2588090110_StaticFields::get_offset_of_THRESHOLD_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (Regex_t3657309853), -1, sizeof(Regex_t3657309853_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2706[10] = 
{
	Regex_t3657309853_StaticFields::get_offset_of_cache_0(),
	Regex_t3657309853_StaticFields::get_offset_of_old_rx_1(),
	Regex_t3657309853::get_offset_of_machineFactory_2(),
	Regex_t3657309853::get_offset_of_mapping_3(),
	Regex_t3657309853::get_offset_of_group_count_4(),
	Regex_t3657309853::get_offset_of_gap_5(),
	Regex_t3657309853::get_offset_of_group_names_6(),
	Regex_t3657309853::get_offset_of_group_numbers_7(),
	Regex_t3657309853::get_offset_of_pattern_8(),
	Regex_t3657309853::get_offset_of_roptions_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (Adapter_t3525834651), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2707[1] = 
{
	Adapter_t3525834651::get_offset_of_ev_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (RegexOptions_t92845595)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2708[11] = 
{
	RegexOptions_t92845595::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (RxInterpreter_t3591201055), -1, sizeof(RxInterpreter_t3591201055_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2709[14] = 
{
	RxInterpreter_t3591201055::get_offset_of_program_1(),
	RxInterpreter_t3591201055::get_offset_of_str_2(),
	RxInterpreter_t3591201055::get_offset_of_string_start_3(),
	RxInterpreter_t3591201055::get_offset_of_string_end_4(),
	RxInterpreter_t3591201055::get_offset_of_group_count_5(),
	RxInterpreter_t3591201055::get_offset_of_groups_6(),
	RxInterpreter_t3591201055::get_offset_of_eval_del_7(),
	RxInterpreter_t3591201055::get_offset_of_marks_8(),
	RxInterpreter_t3591201055::get_offset_of_mark_start_9(),
	RxInterpreter_t3591201055::get_offset_of_mark_end_10(),
	RxInterpreter_t3591201055::get_offset_of_stack_11(),
	RxInterpreter_t3591201055::get_offset_of_repeat_12(),
	RxInterpreter_t3591201055::get_offset_of_deep_13(),
	RxInterpreter_t3591201055_StaticFields::get_offset_of_trace_rx_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { sizeof (IntStack_t561212167)+ sizeof (Il2CppObject), sizeof(IntStack_t561212167_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2710[2] = 
{
	IntStack_t561212167::get_offset_of_values_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IntStack_t561212167::get_offset_of_count_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (RepeatContext_t2643093670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2711[7] = 
{
	RepeatContext_t2643093670::get_offset_of_start_0(),
	RepeatContext_t2643093670::get_offset_of_min_1(),
	RepeatContext_t2643093670::get_offset_of_max_2(),
	RepeatContext_t2643093670::get_offset_of_lazy_3(),
	RepeatContext_t2643093670::get_offset_of_expr_pc_4(),
	RepeatContext_t2643093670::get_offset_of_previous_5(),
	RepeatContext_t2643093670::get_offset_of_count_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (RxLinkRef_t3349372210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2712[2] = 
{
	RxLinkRef_t3349372210::get_offset_of_offsets_0(),
	RxLinkRef_t3349372210::get_offset_of_current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (RxCompiler_t80015800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2713[2] = 
{
	RxCompiler_t80015800::get_offset_of_program_0(),
	RxCompiler_t80015800::get_offset_of_curpos_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (RxInterpreterFactory_t3897605822), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2714[5] = 
{
	RxInterpreterFactory_t3897605822::get_offset_of_mapping_0(),
	RxInterpreterFactory_t3897605822::get_offset_of_program_1(),
	RxInterpreterFactory_t3897605822::get_offset_of_eval_del_2(),
	RxInterpreterFactory_t3897605822::get_offset_of_namesMapping_3(),
	RxInterpreterFactory_t3897605822::get_offset_of_gap_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (RxOp_t349969367)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2715[160] = 
{
	RxOp_t349969367::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (ReplacementEvaluator_t1065029019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2716[4] = 
{
	ReplacementEvaluator_t1065029019::get_offset_of_regex_0(),
	ReplacementEvaluator_t1065029019::get_offset_of_n_pieces_1(),
	ReplacementEvaluator_t1065029019::get_offset_of_pieces_2(),
	ReplacementEvaluator_t1065029019::get_offset_of_replacement_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (ExpressionCollection_t1810289389), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (Expression_t2722445759), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (CompositeExpression_t1252229802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2719[1] = 
{
	CompositeExpression_t1252229802::get_offset_of_expressions_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (Group_t1458537008), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (RegularExpression_t3834220169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2721[1] = 
{
	RegularExpression_t3834220169::get_offset_of_group_count_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (CapturingGroup_t751358689), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2722[2] = 
{
	CapturingGroup_t751358689::get_offset_of_gid_1(),
	CapturingGroup_t751358689::get_offset_of_name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (BalancingGroup_t2395658894), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2723[1] = 
{
	BalancingGroup_t2395658894::get_offset_of_balance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (NonBacktrackingGroup_t3074098547), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (Repetition_t2393242404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2725[3] = 
{
	Repetition_t2393242404::get_offset_of_min_1(),
	Repetition_t2393242404::get_offset_of_max_2(),
	Repetition_t2393242404::get_offset_of_lazy_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (Assertion_t3267412828), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (CaptureAssertion_t3786084589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2727[3] = 
{
	CaptureAssertion_t3786084589::get_offset_of_alternate_1(),
	CaptureAssertion_t3786084589::get_offset_of_group_2(),
	CaptureAssertion_t3786084589::get_offset_of_literal_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (ExpressionAssertion_t1861210811), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2728[2] = 
{
	ExpressionAssertion_t1861210811::get_offset_of_reverse_1(),
	ExpressionAssertion_t1861210811::get_offset_of_negate_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (Alternation_t625481451), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (Literal_t434143540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2730[2] = 
{
	Literal_t434143540::get_offset_of_str_0(),
	Literal_t434143540::get_offset_of_ignore_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (PositionAssertion_t3339288061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2731[1] = 
{
	PositionAssertion_t3339288061::get_offset_of_pos_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (Reference_t1799410108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2732[2] = 
{
	Reference_t1799410108::get_offset_of_group_0(),
	Reference_t1799410108::get_offset_of_ignore_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (BackslashNumber_t3656518667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2733[2] = 
{
	BackslashNumber_t3656518667::get_offset_of_literal_2(),
	BackslashNumber_t3656518667::get_offset_of_ecma_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (CharacterClass_t839120860), -1, sizeof(CharacterClass_t839120860_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2734[6] = 
{
	CharacterClass_t839120860_StaticFields::get_offset_of_upper_case_characters_0(),
	CharacterClass_t839120860::get_offset_of_negate_1(),
	CharacterClass_t839120860::get_offset_of_ignore_2(),
	CharacterClass_t839120860::get_offset_of_pos_cats_3(),
	CharacterClass_t839120860::get_offset_of_neg_cats_4(),
	CharacterClass_t839120860::get_offset_of_intervals_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (AnchorInfo_t3387011151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2735[6] = 
{
	AnchorInfo_t3387011151::get_offset_of_expr_0(),
	AnchorInfo_t3387011151::get_offset_of_pos_1(),
	AnchorInfo_t3387011151::get_offset_of_offset_2(),
	AnchorInfo_t3387011151::get_offset_of_str_3(),
	AnchorInfo_t3387011151::get_offset_of_width_4(),
	AnchorInfo_t3387011151::get_offset_of_ignore_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (UriBuilder_t579353065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2736[10] = 
{
	UriBuilder_t579353065::get_offset_of_scheme_0(),
	UriBuilder_t579353065::get_offset_of_host_1(),
	UriBuilder_t579353065::get_offset_of_port_2(),
	UriBuilder_t579353065::get_offset_of_path_3(),
	UriBuilder_t579353065::get_offset_of_query_4(),
	UriBuilder_t579353065::get_offset_of_fragment_5(),
	UriBuilder_t579353065::get_offset_of_username_6(),
	UriBuilder_t579353065::get_offset_of_password_7(),
	UriBuilder_t579353065::get_offset_of_uri_8(),
	UriBuilder_t579353065::get_offset_of_modified_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (UriComponents_t814099658)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2737[17] = 
{
	UriComponents_t814099658::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (Uri_t100236324), -1, sizeof(Uri_t100236324_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2738[37] = 
{
	Uri_t100236324::get_offset_of_isUnixFilePath_0(),
	Uri_t100236324::get_offset_of_source_1(),
	Uri_t100236324::get_offset_of_scheme_2(),
	Uri_t100236324::get_offset_of_host_3(),
	Uri_t100236324::get_offset_of_port_4(),
	Uri_t100236324::get_offset_of_path_5(),
	Uri_t100236324::get_offset_of_query_6(),
	Uri_t100236324::get_offset_of_fragment_7(),
	Uri_t100236324::get_offset_of_userinfo_8(),
	Uri_t100236324::get_offset_of_isUnc_9(),
	Uri_t100236324::get_offset_of_isOpaquePart_10(),
	Uri_t100236324::get_offset_of_isAbsoluteUri_11(),
	Uri_t100236324::get_offset_of_segments_12(),
	Uri_t100236324::get_offset_of_userEscaped_13(),
	Uri_t100236324::get_offset_of_cachedAbsoluteUri_14(),
	Uri_t100236324::get_offset_of_cachedToString_15(),
	Uri_t100236324::get_offset_of_cachedLocalPath_16(),
	Uri_t100236324::get_offset_of_cachedHashCode_17(),
	Uri_t100236324_StaticFields::get_offset_of_hexUpperChars_18(),
	Uri_t100236324_StaticFields::get_offset_of_SchemeDelimiter_19(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeFile_20(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeFtp_21(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeGopher_22(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeHttp_23(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeHttps_24(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeMailto_25(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeNews_26(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeNntp_27(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeNetPipe_28(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeNetTcp_29(),
	Uri_t100236324_StaticFields::get_offset_of_schemes_30(),
	Uri_t100236324::get_offset_of_parser_31(),
	Uri_t100236324_StaticFields::get_offset_of_U3CU3Ef__switchU24map1C_32(),
	Uri_t100236324_StaticFields::get_offset_of_U3CU3Ef__switchU24map1D_33(),
	Uri_t100236324_StaticFields::get_offset_of_U3CU3Ef__switchU24map1E_34(),
	Uri_t100236324_StaticFields::get_offset_of_U3CU3Ef__switchU24map1F_35(),
	Uri_t100236324_StaticFields::get_offset_of_U3CU3Ef__switchU24map20_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (UriScheme_t722425697)+ sizeof (Il2CppObject), sizeof(UriScheme_t722425697_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2739[3] = 
{
	UriScheme_t722425697::get_offset_of_scheme_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UriScheme_t722425697::get_offset_of_delimiter_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UriScheme_t722425697::get_offset_of_defaultPort_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (UriFormat_t2031163398)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2740[4] = 
{
	UriFormat_t2031163398::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (UriFormatException_t953270471), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (UriHostNameType_t881866241)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2742[6] = 
{
	UriHostNameType_t881866241::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (UriKind_t3816567336)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2743[4] = 
{
	UriKind_t3816567336::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (UriParser_t3890150400), -1, sizeof(UriParser_t3890150400_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2744[6] = 
{
	UriParser_t3890150400_StaticFields::get_offset_of_lock_object_0(),
	UriParser_t3890150400_StaticFields::get_offset_of_table_1(),
	UriParser_t3890150400::get_offset_of_scheme_name_2(),
	UriParser_t3890150400::get_offset_of_default_port_3(),
	UriParser_t3890150400_StaticFields::get_offset_of_uri_regex_4(),
	UriParser_t3890150400_StaticFields::get_offset_of_auth_regex_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (UriPartial_t1736313903)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2745[5] = 
{
	UriPartial_t1736313903::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (UriTypeConverter_t3695916615), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (HandleCollector_t2917059441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2747[5] = 
{
	HandleCollector_t2917059441::get_offset_of_count_0(),
	HandleCollector_t2917059441::get_offset_of_init_1(),
	HandleCollector_t2917059441::get_offset_of_max_2(),
	HandleCollector_t2917059441::get_offset_of_name_3(),
	HandleCollector_t2917059441::get_offset_of_previous_collection_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { sizeof (StandardOleMarshalObject_t3279439477), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { sizeof (DataReceivedEventHandler_t2795960821), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { sizeof (ErrorEventHandler_t2621677363), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { sizeof (FileSystemEventHandler_t1806121106), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { sizeof (RenamedEventHandler_t3047461033), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { sizeof (AuthenticationSchemeSelector_t375327801), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { sizeof (BindIPEndPoint_t1029027275), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { sizeof (HttpContinueDelegate_t3009151163), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { sizeof (LocalCertificateSelectionCallback_t2354453884), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (RemoteCertificateValidationCallback_t3014364904), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (MatchEvaluator_t632122704), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { sizeof (EvalDelegate_t1206020564), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255365), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2760[5] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U24U24fieldU2D1_0(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U24U24fieldU2D2_1(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U24U24fieldU2D3_2(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U24U24fieldU2D4_3(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U24U24fieldU2D5_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { sizeof (U24ArrayTypeU2416_t3254766646)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2416_t3254766646 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { sizeof (U24ArrayTypeU2432_t3652892012)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2432_t3652892012 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { sizeof (U24ArrayTypeU24128_t4289081660)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24128_t4289081660 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { sizeof (U24ArrayTypeU2412_t2490092599)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t2490092599 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { sizeof (U3CModuleU3E_t692745531), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2766 = { sizeof (MonoTODOAttribute_t4131080586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2766[1] = 
{
	MonoTODOAttribute_t4131080586::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2767 = { sizeof (CommittableTransaction_t152884219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2767[3] = 
{
	CommittableTransaction_t152884219::get_offset_of_options_11(),
	CommittableTransaction_t152884219::get_offset_of_user_defined_state_12(),
	CommittableTransaction_t152884219::get_offset_of_asyncResult_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2768 = { sizeof (Enlistment_t1988529039), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2768[1] = 
{
	Enlistment_t1988529039::get_offset_of_done_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2769 = { sizeof (EnterpriseServicesInteropOption_t695190331)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2769[4] = 
{
	EnterpriseServicesInteropOption_t695190331::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2770 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2771 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2772 = { sizeof (IsolationLevel_t4247150849)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2772[8] = 
{
	IsolationLevel_t4247150849::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2773 = { sizeof (PreparingEnlistment_t4199633836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2773[3] = 
{
	PreparingEnlistment_t4199633836::get_offset_of_prepared_1(),
	PreparingEnlistment_t4199633836::get_offset_of_tx_2(),
	PreparingEnlistment_t4199633836::get_offset_of_enlisted_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2774 = { sizeof (SinglePhaseEnlistment_t2412016327), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2774[2] = 
{
	SinglePhaseEnlistment_t2412016327::get_offset_of_tx_1(),
	SinglePhaseEnlistment_t2412016327::get_offset_of_enlisted_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2775 = { sizeof (Transaction_t3472000926), -1, 0, sizeof(Transaction_t3472000926_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable2775[11] = 
{
	THREAD_STATIC_FIELD_OFFSET,
	Transaction_t3472000926::get_offset_of_level_1(),
	Transaction_t3472000926::get_offset_of_info_2(),
	Transaction_t3472000926::get_offset_of_dependents_3(),
	Transaction_t3472000926::get_offset_of_volatiles_4(),
	Transaction_t3472000926::get_offset_of_durables_5(),
	Transaction_t3472000926::get_offset_of_committing_6(),
	Transaction_t3472000926::get_offset_of_committed_7(),
	Transaction_t3472000926::get_offset_of_aborted_8(),
	Transaction_t3472000926::get_offset_of_scope_9(),
	Transaction_t3472000926::get_offset_of_innerException_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2776 = { sizeof (TransactionAbortedException_t2116885608), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2777 = { sizeof (TransactionException_t1281392668), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2778 = { sizeof (TransactionInformation_t2459298917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2778[4] = 
{
	TransactionInformation_t2459298917::get_offset_of_local_id_0(),
	TransactionInformation_t2459298917::get_offset_of_dtcId_1(),
	TransactionInformation_t2459298917::get_offset_of_creation_time_2(),
	TransactionInformation_t2459298917::get_offset_of_status_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2779 = { sizeof (TransactionManager_t2760750674), -1, sizeof(TransactionManager_t2760750674_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2779[2] = 
{
	TransactionManager_t2760750674_StaticFields::get_offset_of_defaultTimeout_0(),
	TransactionManager_t2760750674_StaticFields::get_offset_of_maxTimeout_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2780 = { sizeof (TransactionOptions_t2865697824)+ sizeof (Il2CppObject), sizeof(TransactionOptions_t2865697824 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2780[2] = 
{
	TransactionOptions_t2865697824::get_offset_of_level_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TransactionOptions_t2865697824::get_offset_of_timeout_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2781 = { sizeof (TransactionScope_t3249669472), -1, sizeof(TransactionScope_t3249669472_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2781[8] = 
{
	TransactionScope_t3249669472_StaticFields::get_offset_of_defaultOptions_0(),
	TransactionScope_t3249669472::get_offset_of_transaction_1(),
	TransactionScope_t3249669472::get_offset_of_oldTransaction_2(),
	TransactionScope_t3249669472::get_offset_of_parentScope_3(),
	TransactionScope_t3249669472::get_offset_of_nested_4(),
	TransactionScope_t3249669472::get_offset_of_disposed_5(),
	TransactionScope_t3249669472::get_offset_of_completed_6(),
	TransactionScope_t3249669472::get_offset_of_isRoot_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2782 = { sizeof (TransactionScopeOption_t1495782207)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2782[4] = 
{
	TransactionScopeOption_t1495782207::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2783 = { sizeof (TransactionStatus_t192984806)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2783[5] = 
{
	TransactionStatus_t192984806::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2784 = { sizeof (U3CModuleU3E_t692745532), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2785 = { sizeof (U3CModuleU3E_t692745533), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2786 = { sizeof (ApplicationIDAttribute_t25113873), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2786[1] = 
{
	ApplicationIDAttribute_t25113873::get_offset_of_guid_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2787 = { sizeof (ApplicationNameAttribute_t2032989562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2787[1] = 
{
	ApplicationNameAttribute_t2032989562::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2788 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2789 = { sizeof (U3CModuleU3E_t692745534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2790 = { sizeof (FileNameMarshaler_t4011664002), -1, sizeof(FileNameMarshaler_t4011664002_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2790[1] = 
{
	FileNameMarshaler_t4011664002_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2791 = { sizeof (MapAttribute_t440548554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2791[1] = 
{
	MapAttribute_t440548554::get_offset_of_suppressFlags_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2792 = { sizeof (Stdlib_t3308777473), -1, sizeof(Stdlib_t3308777473_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2792[25] = 
{
	Stdlib_t3308777473_StaticFields::get_offset_of__SIG_DFL_0(),
	Stdlib_t3308777473_StaticFields::get_offset_of__SIG_ERR_1(),
	Stdlib_t3308777473_StaticFields::get_offset_of__SIG_IGN_2(),
	Stdlib_t3308777473_StaticFields::get_offset_of_SIG_DFL_3(),
	Stdlib_t3308777473_StaticFields::get_offset_of_SIG_ERR_4(),
	Stdlib_t3308777473_StaticFields::get_offset_of_SIG_IGN_5(),
	Stdlib_t3308777473_StaticFields::get_offset_of_registered_signals_6(),
	Stdlib_t3308777473_StaticFields::get_offset_of__IOFBF_7(),
	Stdlib_t3308777473_StaticFields::get_offset_of__IOLBF_8(),
	Stdlib_t3308777473_StaticFields::get_offset_of__IONBF_9(),
	Stdlib_t3308777473_StaticFields::get_offset_of_BUFSIZ_10(),
	Stdlib_t3308777473_StaticFields::get_offset_of_EOF_11(),
	Stdlib_t3308777473_StaticFields::get_offset_of_FOPEN_MAX_12(),
	Stdlib_t3308777473_StaticFields::get_offset_of_FILENAME_MAX_13(),
	Stdlib_t3308777473_StaticFields::get_offset_of_L_tmpnam_14(),
	Stdlib_t3308777473_StaticFields::get_offset_of_stderr_15(),
	Stdlib_t3308777473_StaticFields::get_offset_of_stdin_16(),
	Stdlib_t3308777473_StaticFields::get_offset_of_stdout_17(),
	Stdlib_t3308777473_StaticFields::get_offset_of_TMP_MAX_18(),
	Stdlib_t3308777473_StaticFields::get_offset_of_tmpnam_lock_19(),
	Stdlib_t3308777473_StaticFields::get_offset_of_EXIT_FAILURE_20(),
	Stdlib_t3308777473_StaticFields::get_offset_of_EXIT_SUCCESS_21(),
	Stdlib_t3308777473_StaticFields::get_offset_of_MB_CUR_MAX_22(),
	Stdlib_t3308777473_StaticFields::get_offset_of_RAND_MAX_23(),
	Stdlib_t3308777473_StaticFields::get_offset_of_strerror_lock_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2793 = { sizeof (FilePermissions_t1971995193)+ sizeof (Il2CppObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2793[27] = 
{
	FilePermissions_t1971995193::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2794 = { sizeof (Signum_t4209417157)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2794[36] = 
{
	Signum_t4209417157::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2795 = { sizeof (Syscall_t2623000133), -1, sizeof(Syscall_t2623000133_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2795[11] = 
{
	Syscall_t2623000133_StaticFields::get_offset_of_readdir_lock_25(),
	Syscall_t2623000133_StaticFields::get_offset_of_fstab_lock_26(),
	Syscall_t2623000133_StaticFields::get_offset_of_grp_lock_27(),
	Syscall_t2623000133_StaticFields::get_offset_of_pwd_lock_28(),
	Syscall_t2623000133_StaticFields::get_offset_of_signal_lock_29(),
	Syscall_t2623000133_StaticFields::get_offset_of_L_ctermid_30(),
	Syscall_t2623000133_StaticFields::get_offset_of_L_cuserid_31(),
	Syscall_t2623000133_StaticFields::get_offset_of_getlogin_lock_32(),
	Syscall_t2623000133_StaticFields::get_offset_of_MAP_FAILED_33(),
	Syscall_t2623000133_StaticFields::get_offset_of_tty_lock_34(),
	Syscall_t2623000133_StaticFields::get_offset_of_usershell_lock_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2796 = { sizeof (SignalHandler_t1590986791), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2797 = { sizeof (U3CModuleU3E_t692745535), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2798 = { sizeof (U3CModuleU3E_t692745536), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2799 = { sizeof (U3CModuleU3E_t692745537), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
