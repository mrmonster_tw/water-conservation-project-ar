﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Operations_XslCompiledElemen50593777.h"

// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2760654312;
// System.Collections.ArrayList
struct ArrayList_t2718874744;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.XslAttributeSet
struct  XslAttributeSet_t4274455021  : public XslCompiledElement_t50593777
{
public:
	// System.Xml.XmlQualifiedName Mono.Xml.Xsl.XslAttributeSet::name
	XmlQualifiedName_t2760654312 * ___name_3;
	// System.Collections.ArrayList Mono.Xml.Xsl.XslAttributeSet::usedAttributeSets
	ArrayList_t2718874744 * ___usedAttributeSets_4;
	// System.Collections.ArrayList Mono.Xml.Xsl.XslAttributeSet::attributes
	ArrayList_t2718874744 * ___attributes_5;

public:
	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(XslAttributeSet_t4274455021, ___name_3)); }
	inline XmlQualifiedName_t2760654312 * get_name_3() const { return ___name_3; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(XmlQualifiedName_t2760654312 * value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier(&___name_3, value);
	}

	inline static int32_t get_offset_of_usedAttributeSets_4() { return static_cast<int32_t>(offsetof(XslAttributeSet_t4274455021, ___usedAttributeSets_4)); }
	inline ArrayList_t2718874744 * get_usedAttributeSets_4() const { return ___usedAttributeSets_4; }
	inline ArrayList_t2718874744 ** get_address_of_usedAttributeSets_4() { return &___usedAttributeSets_4; }
	inline void set_usedAttributeSets_4(ArrayList_t2718874744 * value)
	{
		___usedAttributeSets_4 = value;
		Il2CppCodeGenWriteBarrier(&___usedAttributeSets_4, value);
	}

	inline static int32_t get_offset_of_attributes_5() { return static_cast<int32_t>(offsetof(XslAttributeSet_t4274455021, ___attributes_5)); }
	inline ArrayList_t2718874744 * get_attributes_5() const { return ___attributes_5; }
	inline ArrayList_t2718874744 ** get_address_of_attributes_5() { return &___attributes_5; }
	inline void set_attributes_5(ArrayList_t2718874744 * value)
	{
		___attributes_5 = value;
		Il2CppCodeGenWriteBarrier(&___attributes_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
