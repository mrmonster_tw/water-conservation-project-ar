﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Xml.Serialization.XmlAnyAttributeAttribute
struct XmlAnyAttributeAttribute_t1449326428;
// System.Xml.Serialization.XmlAnyElementAttributes
struct XmlAnyElementAttributes_t1381893315;
// System.Xml.Serialization.XmlArrayAttribute
struct XmlArrayAttribute_t1484124842;
// System.Xml.Serialization.XmlArrayItemAttributes
struct XmlArrayItemAttributes_t3998742014;
// System.Xml.Serialization.XmlAttributeAttribute
struct XmlAttributeAttribute_t2511360870;
// System.Xml.Serialization.XmlChoiceIdentifierAttribute
struct XmlChoiceIdentifierAttribute_t1913802258;
// System.Object
struct Il2CppObject;
// System.Xml.Serialization.XmlElementAttributes
struct XmlElementAttributes_t2920573420;
// System.Xml.Serialization.XmlEnumAttribute
struct XmlEnumAttribute_t106705320;
// System.Xml.Serialization.XmlRootAttribute
struct XmlRootAttribute_t2306097217;
// System.Xml.Serialization.XmlTextAttribute
struct XmlTextAttribute_t499390083;
// System.Xml.Serialization.XmlTypeAttribute
struct XmlTypeAttribute_t945254369;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlAttributes
struct  XmlAttributes_t3116019767  : public Il2CppObject
{
public:
	// System.Xml.Serialization.XmlAnyAttributeAttribute System.Xml.Serialization.XmlAttributes::xmlAnyAttribute
	XmlAnyAttributeAttribute_t1449326428 * ___xmlAnyAttribute_0;
	// System.Xml.Serialization.XmlAnyElementAttributes System.Xml.Serialization.XmlAttributes::xmlAnyElements
	XmlAnyElementAttributes_t1381893315 * ___xmlAnyElements_1;
	// System.Xml.Serialization.XmlArrayAttribute System.Xml.Serialization.XmlAttributes::xmlArray
	XmlArrayAttribute_t1484124842 * ___xmlArray_2;
	// System.Xml.Serialization.XmlArrayItemAttributes System.Xml.Serialization.XmlAttributes::xmlArrayItems
	XmlArrayItemAttributes_t3998742014 * ___xmlArrayItems_3;
	// System.Xml.Serialization.XmlAttributeAttribute System.Xml.Serialization.XmlAttributes::xmlAttribute
	XmlAttributeAttribute_t2511360870 * ___xmlAttribute_4;
	// System.Xml.Serialization.XmlChoiceIdentifierAttribute System.Xml.Serialization.XmlAttributes::xmlChoiceIdentifier
	XmlChoiceIdentifierAttribute_t1913802258 * ___xmlChoiceIdentifier_5;
	// System.Object System.Xml.Serialization.XmlAttributes::xmlDefaultValue
	Il2CppObject * ___xmlDefaultValue_6;
	// System.Xml.Serialization.XmlElementAttributes System.Xml.Serialization.XmlAttributes::xmlElements
	XmlElementAttributes_t2920573420 * ___xmlElements_7;
	// System.Xml.Serialization.XmlEnumAttribute System.Xml.Serialization.XmlAttributes::xmlEnum
	XmlEnumAttribute_t106705320 * ___xmlEnum_8;
	// System.Boolean System.Xml.Serialization.XmlAttributes::xmlIgnore
	bool ___xmlIgnore_9;
	// System.Boolean System.Xml.Serialization.XmlAttributes::xmlns
	bool ___xmlns_10;
	// System.Xml.Serialization.XmlRootAttribute System.Xml.Serialization.XmlAttributes::xmlRoot
	XmlRootAttribute_t2306097217 * ___xmlRoot_11;
	// System.Xml.Serialization.XmlTextAttribute System.Xml.Serialization.XmlAttributes::xmlText
	XmlTextAttribute_t499390083 * ___xmlText_12;
	// System.Xml.Serialization.XmlTypeAttribute System.Xml.Serialization.XmlAttributes::xmlType
	XmlTypeAttribute_t945254369 * ___xmlType_13;

public:
	inline static int32_t get_offset_of_xmlAnyAttribute_0() { return static_cast<int32_t>(offsetof(XmlAttributes_t3116019767, ___xmlAnyAttribute_0)); }
	inline XmlAnyAttributeAttribute_t1449326428 * get_xmlAnyAttribute_0() const { return ___xmlAnyAttribute_0; }
	inline XmlAnyAttributeAttribute_t1449326428 ** get_address_of_xmlAnyAttribute_0() { return &___xmlAnyAttribute_0; }
	inline void set_xmlAnyAttribute_0(XmlAnyAttributeAttribute_t1449326428 * value)
	{
		___xmlAnyAttribute_0 = value;
		Il2CppCodeGenWriteBarrier(&___xmlAnyAttribute_0, value);
	}

	inline static int32_t get_offset_of_xmlAnyElements_1() { return static_cast<int32_t>(offsetof(XmlAttributes_t3116019767, ___xmlAnyElements_1)); }
	inline XmlAnyElementAttributes_t1381893315 * get_xmlAnyElements_1() const { return ___xmlAnyElements_1; }
	inline XmlAnyElementAttributes_t1381893315 ** get_address_of_xmlAnyElements_1() { return &___xmlAnyElements_1; }
	inline void set_xmlAnyElements_1(XmlAnyElementAttributes_t1381893315 * value)
	{
		___xmlAnyElements_1 = value;
		Il2CppCodeGenWriteBarrier(&___xmlAnyElements_1, value);
	}

	inline static int32_t get_offset_of_xmlArray_2() { return static_cast<int32_t>(offsetof(XmlAttributes_t3116019767, ___xmlArray_2)); }
	inline XmlArrayAttribute_t1484124842 * get_xmlArray_2() const { return ___xmlArray_2; }
	inline XmlArrayAttribute_t1484124842 ** get_address_of_xmlArray_2() { return &___xmlArray_2; }
	inline void set_xmlArray_2(XmlArrayAttribute_t1484124842 * value)
	{
		___xmlArray_2 = value;
		Il2CppCodeGenWriteBarrier(&___xmlArray_2, value);
	}

	inline static int32_t get_offset_of_xmlArrayItems_3() { return static_cast<int32_t>(offsetof(XmlAttributes_t3116019767, ___xmlArrayItems_3)); }
	inline XmlArrayItemAttributes_t3998742014 * get_xmlArrayItems_3() const { return ___xmlArrayItems_3; }
	inline XmlArrayItemAttributes_t3998742014 ** get_address_of_xmlArrayItems_3() { return &___xmlArrayItems_3; }
	inline void set_xmlArrayItems_3(XmlArrayItemAttributes_t3998742014 * value)
	{
		___xmlArrayItems_3 = value;
		Il2CppCodeGenWriteBarrier(&___xmlArrayItems_3, value);
	}

	inline static int32_t get_offset_of_xmlAttribute_4() { return static_cast<int32_t>(offsetof(XmlAttributes_t3116019767, ___xmlAttribute_4)); }
	inline XmlAttributeAttribute_t2511360870 * get_xmlAttribute_4() const { return ___xmlAttribute_4; }
	inline XmlAttributeAttribute_t2511360870 ** get_address_of_xmlAttribute_4() { return &___xmlAttribute_4; }
	inline void set_xmlAttribute_4(XmlAttributeAttribute_t2511360870 * value)
	{
		___xmlAttribute_4 = value;
		Il2CppCodeGenWriteBarrier(&___xmlAttribute_4, value);
	}

	inline static int32_t get_offset_of_xmlChoiceIdentifier_5() { return static_cast<int32_t>(offsetof(XmlAttributes_t3116019767, ___xmlChoiceIdentifier_5)); }
	inline XmlChoiceIdentifierAttribute_t1913802258 * get_xmlChoiceIdentifier_5() const { return ___xmlChoiceIdentifier_5; }
	inline XmlChoiceIdentifierAttribute_t1913802258 ** get_address_of_xmlChoiceIdentifier_5() { return &___xmlChoiceIdentifier_5; }
	inline void set_xmlChoiceIdentifier_5(XmlChoiceIdentifierAttribute_t1913802258 * value)
	{
		___xmlChoiceIdentifier_5 = value;
		Il2CppCodeGenWriteBarrier(&___xmlChoiceIdentifier_5, value);
	}

	inline static int32_t get_offset_of_xmlDefaultValue_6() { return static_cast<int32_t>(offsetof(XmlAttributes_t3116019767, ___xmlDefaultValue_6)); }
	inline Il2CppObject * get_xmlDefaultValue_6() const { return ___xmlDefaultValue_6; }
	inline Il2CppObject ** get_address_of_xmlDefaultValue_6() { return &___xmlDefaultValue_6; }
	inline void set_xmlDefaultValue_6(Il2CppObject * value)
	{
		___xmlDefaultValue_6 = value;
		Il2CppCodeGenWriteBarrier(&___xmlDefaultValue_6, value);
	}

	inline static int32_t get_offset_of_xmlElements_7() { return static_cast<int32_t>(offsetof(XmlAttributes_t3116019767, ___xmlElements_7)); }
	inline XmlElementAttributes_t2920573420 * get_xmlElements_7() const { return ___xmlElements_7; }
	inline XmlElementAttributes_t2920573420 ** get_address_of_xmlElements_7() { return &___xmlElements_7; }
	inline void set_xmlElements_7(XmlElementAttributes_t2920573420 * value)
	{
		___xmlElements_7 = value;
		Il2CppCodeGenWriteBarrier(&___xmlElements_7, value);
	}

	inline static int32_t get_offset_of_xmlEnum_8() { return static_cast<int32_t>(offsetof(XmlAttributes_t3116019767, ___xmlEnum_8)); }
	inline XmlEnumAttribute_t106705320 * get_xmlEnum_8() const { return ___xmlEnum_8; }
	inline XmlEnumAttribute_t106705320 ** get_address_of_xmlEnum_8() { return &___xmlEnum_8; }
	inline void set_xmlEnum_8(XmlEnumAttribute_t106705320 * value)
	{
		___xmlEnum_8 = value;
		Il2CppCodeGenWriteBarrier(&___xmlEnum_8, value);
	}

	inline static int32_t get_offset_of_xmlIgnore_9() { return static_cast<int32_t>(offsetof(XmlAttributes_t3116019767, ___xmlIgnore_9)); }
	inline bool get_xmlIgnore_9() const { return ___xmlIgnore_9; }
	inline bool* get_address_of_xmlIgnore_9() { return &___xmlIgnore_9; }
	inline void set_xmlIgnore_9(bool value)
	{
		___xmlIgnore_9 = value;
	}

	inline static int32_t get_offset_of_xmlns_10() { return static_cast<int32_t>(offsetof(XmlAttributes_t3116019767, ___xmlns_10)); }
	inline bool get_xmlns_10() const { return ___xmlns_10; }
	inline bool* get_address_of_xmlns_10() { return &___xmlns_10; }
	inline void set_xmlns_10(bool value)
	{
		___xmlns_10 = value;
	}

	inline static int32_t get_offset_of_xmlRoot_11() { return static_cast<int32_t>(offsetof(XmlAttributes_t3116019767, ___xmlRoot_11)); }
	inline XmlRootAttribute_t2306097217 * get_xmlRoot_11() const { return ___xmlRoot_11; }
	inline XmlRootAttribute_t2306097217 ** get_address_of_xmlRoot_11() { return &___xmlRoot_11; }
	inline void set_xmlRoot_11(XmlRootAttribute_t2306097217 * value)
	{
		___xmlRoot_11 = value;
		Il2CppCodeGenWriteBarrier(&___xmlRoot_11, value);
	}

	inline static int32_t get_offset_of_xmlText_12() { return static_cast<int32_t>(offsetof(XmlAttributes_t3116019767, ___xmlText_12)); }
	inline XmlTextAttribute_t499390083 * get_xmlText_12() const { return ___xmlText_12; }
	inline XmlTextAttribute_t499390083 ** get_address_of_xmlText_12() { return &___xmlText_12; }
	inline void set_xmlText_12(XmlTextAttribute_t499390083 * value)
	{
		___xmlText_12 = value;
		Il2CppCodeGenWriteBarrier(&___xmlText_12, value);
	}

	inline static int32_t get_offset_of_xmlType_13() { return static_cast<int32_t>(offsetof(XmlAttributes_t3116019767, ___xmlType_13)); }
	inline XmlTypeAttribute_t945254369 * get_xmlType_13() const { return ___xmlType_13; }
	inline XmlTypeAttribute_t945254369 ** get_address_of_xmlType_13() { return &___xmlType_13; }
	inline void set_xmlType_13(XmlTypeAttribute_t945254369 * value)
	{
		___xmlType_13 = value;
		Il2CppCodeGenWriteBarrier(&___xmlType_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
