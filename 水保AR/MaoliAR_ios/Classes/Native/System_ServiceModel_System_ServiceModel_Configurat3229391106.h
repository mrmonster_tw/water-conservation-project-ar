﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.ChannelEndpointElement
struct  ChannelEndpointElement_t3229391106  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct ChannelEndpointElement_t3229391106_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.ChannelEndpointElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.ChannelEndpointElement::address
	ConfigurationProperty_t3590861854 * ___address_14;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.ChannelEndpointElement::behavior_configuration
	ConfigurationProperty_t3590861854 * ___behavior_configuration_15;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.ChannelEndpointElement::binding
	ConfigurationProperty_t3590861854 * ___binding_16;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.ChannelEndpointElement::binding_configuration
	ConfigurationProperty_t3590861854 * ___binding_configuration_17;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.ChannelEndpointElement::contract
	ConfigurationProperty_t3590861854 * ___contract_18;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.ChannelEndpointElement::headers
	ConfigurationProperty_t3590861854 * ___headers_19;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.ChannelEndpointElement::identity
	ConfigurationProperty_t3590861854 * ___identity_20;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.ChannelEndpointElement::name
	ConfigurationProperty_t3590861854 * ___name_21;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(ChannelEndpointElement_t3229391106_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier(&___properties_13, value);
	}

	inline static int32_t get_offset_of_address_14() { return static_cast<int32_t>(offsetof(ChannelEndpointElement_t3229391106_StaticFields, ___address_14)); }
	inline ConfigurationProperty_t3590861854 * get_address_14() const { return ___address_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_address_14() { return &___address_14; }
	inline void set_address_14(ConfigurationProperty_t3590861854 * value)
	{
		___address_14 = value;
		Il2CppCodeGenWriteBarrier(&___address_14, value);
	}

	inline static int32_t get_offset_of_behavior_configuration_15() { return static_cast<int32_t>(offsetof(ChannelEndpointElement_t3229391106_StaticFields, ___behavior_configuration_15)); }
	inline ConfigurationProperty_t3590861854 * get_behavior_configuration_15() const { return ___behavior_configuration_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_behavior_configuration_15() { return &___behavior_configuration_15; }
	inline void set_behavior_configuration_15(ConfigurationProperty_t3590861854 * value)
	{
		___behavior_configuration_15 = value;
		Il2CppCodeGenWriteBarrier(&___behavior_configuration_15, value);
	}

	inline static int32_t get_offset_of_binding_16() { return static_cast<int32_t>(offsetof(ChannelEndpointElement_t3229391106_StaticFields, ___binding_16)); }
	inline ConfigurationProperty_t3590861854 * get_binding_16() const { return ___binding_16; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_binding_16() { return &___binding_16; }
	inline void set_binding_16(ConfigurationProperty_t3590861854 * value)
	{
		___binding_16 = value;
		Il2CppCodeGenWriteBarrier(&___binding_16, value);
	}

	inline static int32_t get_offset_of_binding_configuration_17() { return static_cast<int32_t>(offsetof(ChannelEndpointElement_t3229391106_StaticFields, ___binding_configuration_17)); }
	inline ConfigurationProperty_t3590861854 * get_binding_configuration_17() const { return ___binding_configuration_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_binding_configuration_17() { return &___binding_configuration_17; }
	inline void set_binding_configuration_17(ConfigurationProperty_t3590861854 * value)
	{
		___binding_configuration_17 = value;
		Il2CppCodeGenWriteBarrier(&___binding_configuration_17, value);
	}

	inline static int32_t get_offset_of_contract_18() { return static_cast<int32_t>(offsetof(ChannelEndpointElement_t3229391106_StaticFields, ___contract_18)); }
	inline ConfigurationProperty_t3590861854 * get_contract_18() const { return ___contract_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_contract_18() { return &___contract_18; }
	inline void set_contract_18(ConfigurationProperty_t3590861854 * value)
	{
		___contract_18 = value;
		Il2CppCodeGenWriteBarrier(&___contract_18, value);
	}

	inline static int32_t get_offset_of_headers_19() { return static_cast<int32_t>(offsetof(ChannelEndpointElement_t3229391106_StaticFields, ___headers_19)); }
	inline ConfigurationProperty_t3590861854 * get_headers_19() const { return ___headers_19; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_headers_19() { return &___headers_19; }
	inline void set_headers_19(ConfigurationProperty_t3590861854 * value)
	{
		___headers_19 = value;
		Il2CppCodeGenWriteBarrier(&___headers_19, value);
	}

	inline static int32_t get_offset_of_identity_20() { return static_cast<int32_t>(offsetof(ChannelEndpointElement_t3229391106_StaticFields, ___identity_20)); }
	inline ConfigurationProperty_t3590861854 * get_identity_20() const { return ___identity_20; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_identity_20() { return &___identity_20; }
	inline void set_identity_20(ConfigurationProperty_t3590861854 * value)
	{
		___identity_20 = value;
		Il2CppCodeGenWriteBarrier(&___identity_20, value);
	}

	inline static int32_t get_offset_of_name_21() { return static_cast<int32_t>(offsetof(ChannelEndpointElement_t3229391106_StaticFields, ___name_21)); }
	inline ConfigurationProperty_t3590861854 * get_name_21() const { return ___name_21; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_name_21() { return &___name_21; }
	inline void set_name_21(ConfigurationProperty_t3590861854 * value)
	{
		___name_21 = value;
		Il2CppCodeGenWriteBarrier(&___name_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
