﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.ArrayList
struct ArrayList_t2718874744;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.ObjectStateFormatter/ReaderContext
struct  ReaderContext_t3753404210  : public Il2CppObject
{
public:
	// System.Collections.ArrayList System.Web.UI.ObjectStateFormatter/ReaderContext::cache
	ArrayList_t2718874744 * ___cache_0;

public:
	inline static int32_t get_offset_of_cache_0() { return static_cast<int32_t>(offsetof(ReaderContext_t3753404210, ___cache_0)); }
	inline ArrayList_t2718874744 * get_cache_0() const { return ___cache_0; }
	inline ArrayList_t2718874744 ** get_address_of_cache_0() { return &___cache_0; }
	inline void set_cache_0(ArrayList_t2718874744 * value)
	{
		___cache_0 = value;
		Il2CppCodeGenWriteBarrier(&___cache_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
