﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_I1686189603.h"

// System.ServiceModel.Channels.PeerTransportBindingElement
struct PeerTransportBindingElement_t261693216;
// System.ServiceModel.Channels.IDuplexChannel
struct IDuplexChannel_t3233783714;
// System.ServiceModel.Channels.MessageEncoder
struct MessageEncoder_t3063398011;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t1333520283;
// System.ServiceModel.PeerResolver
struct PeerResolver_t1567980581;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.PeerChannelListener`1<System.ServiceModel.Channels.IDuplexChannel>
struct  PeerChannelListener_1_t2667659230  : public InternalChannelListenerBase_1_t1686189603
{
public:
	// System.ServiceModel.Channels.PeerTransportBindingElement System.ServiceModel.Channels.PeerChannelListener`1::source
	PeerTransportBindingElement_t261693216 * ___source_16;
	// TChannel System.ServiceModel.Channels.PeerChannelListener`1::channel
	Il2CppObject * ___channel_17;
	// System.ServiceModel.Channels.MessageEncoder System.ServiceModel.Channels.PeerChannelListener`1::encoder
	MessageEncoder_t3063398011 * ___encoder_18;
	// System.Threading.AutoResetEvent System.ServiceModel.Channels.PeerChannelListener`1::accept_handle
	AutoResetEvent_t1333520283 * ___accept_handle_19;
	// System.ServiceModel.PeerResolver System.ServiceModel.Channels.PeerChannelListener`1::<Resolver>k__BackingField
	PeerResolver_t1567980581 * ___U3CResolverU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_source_16() { return static_cast<int32_t>(offsetof(PeerChannelListener_1_t2667659230, ___source_16)); }
	inline PeerTransportBindingElement_t261693216 * get_source_16() const { return ___source_16; }
	inline PeerTransportBindingElement_t261693216 ** get_address_of_source_16() { return &___source_16; }
	inline void set_source_16(PeerTransportBindingElement_t261693216 * value)
	{
		___source_16 = value;
		Il2CppCodeGenWriteBarrier(&___source_16, value);
	}

	inline static int32_t get_offset_of_channel_17() { return static_cast<int32_t>(offsetof(PeerChannelListener_1_t2667659230, ___channel_17)); }
	inline Il2CppObject * get_channel_17() const { return ___channel_17; }
	inline Il2CppObject ** get_address_of_channel_17() { return &___channel_17; }
	inline void set_channel_17(Il2CppObject * value)
	{
		___channel_17 = value;
		Il2CppCodeGenWriteBarrier(&___channel_17, value);
	}

	inline static int32_t get_offset_of_encoder_18() { return static_cast<int32_t>(offsetof(PeerChannelListener_1_t2667659230, ___encoder_18)); }
	inline MessageEncoder_t3063398011 * get_encoder_18() const { return ___encoder_18; }
	inline MessageEncoder_t3063398011 ** get_address_of_encoder_18() { return &___encoder_18; }
	inline void set_encoder_18(MessageEncoder_t3063398011 * value)
	{
		___encoder_18 = value;
		Il2CppCodeGenWriteBarrier(&___encoder_18, value);
	}

	inline static int32_t get_offset_of_accept_handle_19() { return static_cast<int32_t>(offsetof(PeerChannelListener_1_t2667659230, ___accept_handle_19)); }
	inline AutoResetEvent_t1333520283 * get_accept_handle_19() const { return ___accept_handle_19; }
	inline AutoResetEvent_t1333520283 ** get_address_of_accept_handle_19() { return &___accept_handle_19; }
	inline void set_accept_handle_19(AutoResetEvent_t1333520283 * value)
	{
		___accept_handle_19 = value;
		Il2CppCodeGenWriteBarrier(&___accept_handle_19, value);
	}

	inline static int32_t get_offset_of_U3CResolverU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(PeerChannelListener_1_t2667659230, ___U3CResolverU3Ek__BackingField_20)); }
	inline PeerResolver_t1567980581 * get_U3CResolverU3Ek__BackingField_20() const { return ___U3CResolverU3Ek__BackingField_20; }
	inline PeerResolver_t1567980581 ** get_address_of_U3CResolverU3Ek__BackingField_20() { return &___U3CResolverU3Ek__BackingField_20; }
	inline void set_U3CResolverU3Ek__BackingField_20(PeerResolver_t1567980581 * value)
	{
		___U3CResolverU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CResolverU3Ek__BackingField_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
