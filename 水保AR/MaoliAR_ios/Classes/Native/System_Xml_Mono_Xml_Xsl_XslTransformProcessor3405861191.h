﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// Mono.Xml.Xsl.XsltDebuggerWrapper
struct XsltDebuggerWrapper_t4148725768;
// Mono.Xml.Xsl.CompiledStylesheet
struct CompiledStylesheet_t759457236;
// Mono.Xml.Xsl.XslStylesheet
struct XslStylesheet_t113441946;
// System.Collections.Stack
struct Stack_t2329662280;
// System.Xml.XPath.XPathNavigator
struct XPathNavigator_t787956054;
// System.Xml.Xsl.XsltArgumentList
struct XsltArgumentList_t741251601;
// System.Xml.XmlResolver
struct XmlResolver_t626023767;
// System.String
struct String_t;
// Mono.Xml.Xsl.XsltCompiledContext
struct XsltCompiledContext_t2295645487;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Text.StringBuilder
struct StringBuilder_t1712802186;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.XslTransformProcessor
struct  XslTransformProcessor_t3405861191  : public Il2CppObject
{
public:
	// Mono.Xml.Xsl.XsltDebuggerWrapper Mono.Xml.Xsl.XslTransformProcessor::debugger
	XsltDebuggerWrapper_t4148725768 * ___debugger_0;
	// Mono.Xml.Xsl.CompiledStylesheet Mono.Xml.Xsl.XslTransformProcessor::compiledStyle
	CompiledStylesheet_t759457236 * ___compiledStyle_1;
	// Mono.Xml.Xsl.XslStylesheet Mono.Xml.Xsl.XslTransformProcessor::style
	XslStylesheet_t113441946 * ___style_2;
	// System.Collections.Stack Mono.Xml.Xsl.XslTransformProcessor::currentTemplateStack
	Stack_t2329662280 * ___currentTemplateStack_3;
	// System.Xml.XPath.XPathNavigator Mono.Xml.Xsl.XslTransformProcessor::root
	XPathNavigator_t787956054 * ___root_4;
	// System.Xml.Xsl.XsltArgumentList Mono.Xml.Xsl.XslTransformProcessor::args
	XsltArgumentList_t741251601 * ___args_5;
	// System.Xml.XmlResolver Mono.Xml.Xsl.XslTransformProcessor::resolver
	XmlResolver_t626023767 * ___resolver_6;
	// System.String Mono.Xml.Xsl.XslTransformProcessor::currentOutputUri
	String_t* ___currentOutputUri_7;
	// Mono.Xml.Xsl.XsltCompiledContext Mono.Xml.Xsl.XslTransformProcessor::XPathContext
	XsltCompiledContext_t2295645487 * ___XPathContext_8;
	// System.Collections.Hashtable Mono.Xml.Xsl.XslTransformProcessor::globalVariableTable
	Hashtable_t1853889766 * ___globalVariableTable_9;
	// System.Collections.Hashtable Mono.Xml.Xsl.XslTransformProcessor::docCache
	Hashtable_t1853889766 * ___docCache_10;
	// System.Collections.Stack Mono.Xml.Xsl.XslTransformProcessor::outputStack
	Stack_t2329662280 * ___outputStack_11;
	// System.Text.StringBuilder Mono.Xml.Xsl.XslTransformProcessor::avtSB
	StringBuilder_t1712802186 * ___avtSB_12;
	// System.Collections.Stack Mono.Xml.Xsl.XslTransformProcessor::paramPassingCache
	Stack_t2329662280 * ___paramPassingCache_13;
	// System.Collections.ArrayList Mono.Xml.Xsl.XslTransformProcessor::nodesetStack
	ArrayList_t2718874744 * ___nodesetStack_14;
	// System.Collections.Stack Mono.Xml.Xsl.XslTransformProcessor::variableStack
	Stack_t2329662280 * ___variableStack_15;
	// System.Object[] Mono.Xml.Xsl.XslTransformProcessor::currentStack
	ObjectU5BU5D_t2843939325* ___currentStack_16;
	// System.Collections.Hashtable Mono.Xml.Xsl.XslTransformProcessor::busyTable
	Hashtable_t1853889766 * ___busyTable_17;

public:
	inline static int32_t get_offset_of_debugger_0() { return static_cast<int32_t>(offsetof(XslTransformProcessor_t3405861191, ___debugger_0)); }
	inline XsltDebuggerWrapper_t4148725768 * get_debugger_0() const { return ___debugger_0; }
	inline XsltDebuggerWrapper_t4148725768 ** get_address_of_debugger_0() { return &___debugger_0; }
	inline void set_debugger_0(XsltDebuggerWrapper_t4148725768 * value)
	{
		___debugger_0 = value;
		Il2CppCodeGenWriteBarrier(&___debugger_0, value);
	}

	inline static int32_t get_offset_of_compiledStyle_1() { return static_cast<int32_t>(offsetof(XslTransformProcessor_t3405861191, ___compiledStyle_1)); }
	inline CompiledStylesheet_t759457236 * get_compiledStyle_1() const { return ___compiledStyle_1; }
	inline CompiledStylesheet_t759457236 ** get_address_of_compiledStyle_1() { return &___compiledStyle_1; }
	inline void set_compiledStyle_1(CompiledStylesheet_t759457236 * value)
	{
		___compiledStyle_1 = value;
		Il2CppCodeGenWriteBarrier(&___compiledStyle_1, value);
	}

	inline static int32_t get_offset_of_style_2() { return static_cast<int32_t>(offsetof(XslTransformProcessor_t3405861191, ___style_2)); }
	inline XslStylesheet_t113441946 * get_style_2() const { return ___style_2; }
	inline XslStylesheet_t113441946 ** get_address_of_style_2() { return &___style_2; }
	inline void set_style_2(XslStylesheet_t113441946 * value)
	{
		___style_2 = value;
		Il2CppCodeGenWriteBarrier(&___style_2, value);
	}

	inline static int32_t get_offset_of_currentTemplateStack_3() { return static_cast<int32_t>(offsetof(XslTransformProcessor_t3405861191, ___currentTemplateStack_3)); }
	inline Stack_t2329662280 * get_currentTemplateStack_3() const { return ___currentTemplateStack_3; }
	inline Stack_t2329662280 ** get_address_of_currentTemplateStack_3() { return &___currentTemplateStack_3; }
	inline void set_currentTemplateStack_3(Stack_t2329662280 * value)
	{
		___currentTemplateStack_3 = value;
		Il2CppCodeGenWriteBarrier(&___currentTemplateStack_3, value);
	}

	inline static int32_t get_offset_of_root_4() { return static_cast<int32_t>(offsetof(XslTransformProcessor_t3405861191, ___root_4)); }
	inline XPathNavigator_t787956054 * get_root_4() const { return ___root_4; }
	inline XPathNavigator_t787956054 ** get_address_of_root_4() { return &___root_4; }
	inline void set_root_4(XPathNavigator_t787956054 * value)
	{
		___root_4 = value;
		Il2CppCodeGenWriteBarrier(&___root_4, value);
	}

	inline static int32_t get_offset_of_args_5() { return static_cast<int32_t>(offsetof(XslTransformProcessor_t3405861191, ___args_5)); }
	inline XsltArgumentList_t741251601 * get_args_5() const { return ___args_5; }
	inline XsltArgumentList_t741251601 ** get_address_of_args_5() { return &___args_5; }
	inline void set_args_5(XsltArgumentList_t741251601 * value)
	{
		___args_5 = value;
		Il2CppCodeGenWriteBarrier(&___args_5, value);
	}

	inline static int32_t get_offset_of_resolver_6() { return static_cast<int32_t>(offsetof(XslTransformProcessor_t3405861191, ___resolver_6)); }
	inline XmlResolver_t626023767 * get_resolver_6() const { return ___resolver_6; }
	inline XmlResolver_t626023767 ** get_address_of_resolver_6() { return &___resolver_6; }
	inline void set_resolver_6(XmlResolver_t626023767 * value)
	{
		___resolver_6 = value;
		Il2CppCodeGenWriteBarrier(&___resolver_6, value);
	}

	inline static int32_t get_offset_of_currentOutputUri_7() { return static_cast<int32_t>(offsetof(XslTransformProcessor_t3405861191, ___currentOutputUri_7)); }
	inline String_t* get_currentOutputUri_7() const { return ___currentOutputUri_7; }
	inline String_t** get_address_of_currentOutputUri_7() { return &___currentOutputUri_7; }
	inline void set_currentOutputUri_7(String_t* value)
	{
		___currentOutputUri_7 = value;
		Il2CppCodeGenWriteBarrier(&___currentOutputUri_7, value);
	}

	inline static int32_t get_offset_of_XPathContext_8() { return static_cast<int32_t>(offsetof(XslTransformProcessor_t3405861191, ___XPathContext_8)); }
	inline XsltCompiledContext_t2295645487 * get_XPathContext_8() const { return ___XPathContext_8; }
	inline XsltCompiledContext_t2295645487 ** get_address_of_XPathContext_8() { return &___XPathContext_8; }
	inline void set_XPathContext_8(XsltCompiledContext_t2295645487 * value)
	{
		___XPathContext_8 = value;
		Il2CppCodeGenWriteBarrier(&___XPathContext_8, value);
	}

	inline static int32_t get_offset_of_globalVariableTable_9() { return static_cast<int32_t>(offsetof(XslTransformProcessor_t3405861191, ___globalVariableTable_9)); }
	inline Hashtable_t1853889766 * get_globalVariableTable_9() const { return ___globalVariableTable_9; }
	inline Hashtable_t1853889766 ** get_address_of_globalVariableTable_9() { return &___globalVariableTable_9; }
	inline void set_globalVariableTable_9(Hashtable_t1853889766 * value)
	{
		___globalVariableTable_9 = value;
		Il2CppCodeGenWriteBarrier(&___globalVariableTable_9, value);
	}

	inline static int32_t get_offset_of_docCache_10() { return static_cast<int32_t>(offsetof(XslTransformProcessor_t3405861191, ___docCache_10)); }
	inline Hashtable_t1853889766 * get_docCache_10() const { return ___docCache_10; }
	inline Hashtable_t1853889766 ** get_address_of_docCache_10() { return &___docCache_10; }
	inline void set_docCache_10(Hashtable_t1853889766 * value)
	{
		___docCache_10 = value;
		Il2CppCodeGenWriteBarrier(&___docCache_10, value);
	}

	inline static int32_t get_offset_of_outputStack_11() { return static_cast<int32_t>(offsetof(XslTransformProcessor_t3405861191, ___outputStack_11)); }
	inline Stack_t2329662280 * get_outputStack_11() const { return ___outputStack_11; }
	inline Stack_t2329662280 ** get_address_of_outputStack_11() { return &___outputStack_11; }
	inline void set_outputStack_11(Stack_t2329662280 * value)
	{
		___outputStack_11 = value;
		Il2CppCodeGenWriteBarrier(&___outputStack_11, value);
	}

	inline static int32_t get_offset_of_avtSB_12() { return static_cast<int32_t>(offsetof(XslTransformProcessor_t3405861191, ___avtSB_12)); }
	inline StringBuilder_t1712802186 * get_avtSB_12() const { return ___avtSB_12; }
	inline StringBuilder_t1712802186 ** get_address_of_avtSB_12() { return &___avtSB_12; }
	inline void set_avtSB_12(StringBuilder_t1712802186 * value)
	{
		___avtSB_12 = value;
		Il2CppCodeGenWriteBarrier(&___avtSB_12, value);
	}

	inline static int32_t get_offset_of_paramPassingCache_13() { return static_cast<int32_t>(offsetof(XslTransformProcessor_t3405861191, ___paramPassingCache_13)); }
	inline Stack_t2329662280 * get_paramPassingCache_13() const { return ___paramPassingCache_13; }
	inline Stack_t2329662280 ** get_address_of_paramPassingCache_13() { return &___paramPassingCache_13; }
	inline void set_paramPassingCache_13(Stack_t2329662280 * value)
	{
		___paramPassingCache_13 = value;
		Il2CppCodeGenWriteBarrier(&___paramPassingCache_13, value);
	}

	inline static int32_t get_offset_of_nodesetStack_14() { return static_cast<int32_t>(offsetof(XslTransformProcessor_t3405861191, ___nodesetStack_14)); }
	inline ArrayList_t2718874744 * get_nodesetStack_14() const { return ___nodesetStack_14; }
	inline ArrayList_t2718874744 ** get_address_of_nodesetStack_14() { return &___nodesetStack_14; }
	inline void set_nodesetStack_14(ArrayList_t2718874744 * value)
	{
		___nodesetStack_14 = value;
		Il2CppCodeGenWriteBarrier(&___nodesetStack_14, value);
	}

	inline static int32_t get_offset_of_variableStack_15() { return static_cast<int32_t>(offsetof(XslTransformProcessor_t3405861191, ___variableStack_15)); }
	inline Stack_t2329662280 * get_variableStack_15() const { return ___variableStack_15; }
	inline Stack_t2329662280 ** get_address_of_variableStack_15() { return &___variableStack_15; }
	inline void set_variableStack_15(Stack_t2329662280 * value)
	{
		___variableStack_15 = value;
		Il2CppCodeGenWriteBarrier(&___variableStack_15, value);
	}

	inline static int32_t get_offset_of_currentStack_16() { return static_cast<int32_t>(offsetof(XslTransformProcessor_t3405861191, ___currentStack_16)); }
	inline ObjectU5BU5D_t2843939325* get_currentStack_16() const { return ___currentStack_16; }
	inline ObjectU5BU5D_t2843939325** get_address_of_currentStack_16() { return &___currentStack_16; }
	inline void set_currentStack_16(ObjectU5BU5D_t2843939325* value)
	{
		___currentStack_16 = value;
		Il2CppCodeGenWriteBarrier(&___currentStack_16, value);
	}

	inline static int32_t get_offset_of_busyTable_17() { return static_cast<int32_t>(offsetof(XslTransformProcessor_t3405861191, ___busyTable_17)); }
	inline Hashtable_t1853889766 * get_busyTable_17() const { return ___busyTable_17; }
	inline Hashtable_t1853889766 ** get_address_of_busyTable_17() { return &___busyTable_17; }
	inline void set_busyTable_17(Hashtable_t1853889766 * value)
	{
		___busyTable_17 = value;
		Il2CppCodeGenWriteBarrier(&___busyTable_17, value);
	}
};

struct XslTransformProcessor_t3405861191_StaticFields
{
public:
	// System.Object Mono.Xml.Xsl.XslTransformProcessor::busyObject
	Il2CppObject * ___busyObject_18;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.XslTransformProcessor::<>f__switch$map24
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map24_19;

public:
	inline static int32_t get_offset_of_busyObject_18() { return static_cast<int32_t>(offsetof(XslTransformProcessor_t3405861191_StaticFields, ___busyObject_18)); }
	inline Il2CppObject * get_busyObject_18() const { return ___busyObject_18; }
	inline Il2CppObject ** get_address_of_busyObject_18() { return &___busyObject_18; }
	inline void set_busyObject_18(Il2CppObject * value)
	{
		___busyObject_18 = value;
		Il2CppCodeGenWriteBarrier(&___busyObject_18, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map24_19() { return static_cast<int32_t>(offsetof(XslTransformProcessor_t3405861191_StaticFields, ___U3CU3Ef__switchU24map24_19)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map24_19() const { return ___U3CU3Ef__switchU24map24_19; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map24_19() { return &___U3CU3Ef__switchU24map24_19; }
	inline void set_U3CU3Ef__switchU24map24_19(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map24_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map24_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
