﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_I1191478253.h"

// System.IO.Pipes.NamedPipeServerStream
struct NamedPipeServerStream_t125618231;
// System.ServiceModel.Channels.TcpBinaryFrameManager
struct TcpBinaryFrameManager_t2884120632;
// System.ServiceModel.Channels.MessageEncoder
struct MessageEncoder_t3063398011;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.NamedPipeReplyChannel
struct  NamedPipeReplyChannel_t296821458  : public InternalReplyChannelBase_t1191478253
{
public:
	// System.IO.Pipes.NamedPipeServerStream System.ServiceModel.Channels.NamedPipeReplyChannel::server
	NamedPipeServerStream_t125618231 * ___server_15;
	// System.ServiceModel.Channels.TcpBinaryFrameManager System.ServiceModel.Channels.NamedPipeReplyChannel::frame
	TcpBinaryFrameManager_t2884120632 * ___frame_16;
	// System.ServiceModel.Channels.MessageEncoder System.ServiceModel.Channels.NamedPipeReplyChannel::<Encoder>k__BackingField
	MessageEncoder_t3063398011 * ___U3CEncoderU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_server_15() { return static_cast<int32_t>(offsetof(NamedPipeReplyChannel_t296821458, ___server_15)); }
	inline NamedPipeServerStream_t125618231 * get_server_15() const { return ___server_15; }
	inline NamedPipeServerStream_t125618231 ** get_address_of_server_15() { return &___server_15; }
	inline void set_server_15(NamedPipeServerStream_t125618231 * value)
	{
		___server_15 = value;
		Il2CppCodeGenWriteBarrier(&___server_15, value);
	}

	inline static int32_t get_offset_of_frame_16() { return static_cast<int32_t>(offsetof(NamedPipeReplyChannel_t296821458, ___frame_16)); }
	inline TcpBinaryFrameManager_t2884120632 * get_frame_16() const { return ___frame_16; }
	inline TcpBinaryFrameManager_t2884120632 ** get_address_of_frame_16() { return &___frame_16; }
	inline void set_frame_16(TcpBinaryFrameManager_t2884120632 * value)
	{
		___frame_16 = value;
		Il2CppCodeGenWriteBarrier(&___frame_16, value);
	}

	inline static int32_t get_offset_of_U3CEncoderU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(NamedPipeReplyChannel_t296821458, ___U3CEncoderU3Ek__BackingField_17)); }
	inline MessageEncoder_t3063398011 * get_U3CEncoderU3Ek__BackingField_17() const { return ___U3CEncoderU3Ek__BackingField_17; }
	inline MessageEncoder_t3063398011 ** get_address_of_U3CEncoderU3Ek__BackingField_17() { return &___U3CEncoderU3Ek__BackingField_17; }
	inline void set_U3CEncoderU3Ek__BackingField_17(MessageEncoder_t3063398011 * value)
	{
		___U3CEncoderU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CEncoderU3Ek__BackingField_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
