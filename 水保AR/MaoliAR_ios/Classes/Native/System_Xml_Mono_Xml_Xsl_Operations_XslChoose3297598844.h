﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Operations_XslCompiledElemen50593777.h"

// Mono.Xml.Xsl.Operations.XslOperation
struct XslOperation_t2153241355;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslChoose
struct  XslChoose_t3297598844  : public XslCompiledElement_t50593777
{
public:
	// Mono.Xml.Xsl.Operations.XslOperation Mono.Xml.Xsl.Operations.XslChoose::defaultChoice
	XslOperation_t2153241355 * ___defaultChoice_3;
	// System.Collections.ArrayList Mono.Xml.Xsl.Operations.XslChoose::conditions
	ArrayList_t2718874744 * ___conditions_4;

public:
	inline static int32_t get_offset_of_defaultChoice_3() { return static_cast<int32_t>(offsetof(XslChoose_t3297598844, ___defaultChoice_3)); }
	inline XslOperation_t2153241355 * get_defaultChoice_3() const { return ___defaultChoice_3; }
	inline XslOperation_t2153241355 ** get_address_of_defaultChoice_3() { return &___defaultChoice_3; }
	inline void set_defaultChoice_3(XslOperation_t2153241355 * value)
	{
		___defaultChoice_3 = value;
		Il2CppCodeGenWriteBarrier(&___defaultChoice_3, value);
	}

	inline static int32_t get_offset_of_conditions_4() { return static_cast<int32_t>(offsetof(XslChoose_t3297598844, ___conditions_4)); }
	inline ArrayList_t2718874744 * get_conditions_4() const { return ___conditions_4; }
	inline ArrayList_t2718874744 ** get_address_of_conditions_4() { return &___conditions_4; }
	inline void set_conditions_4(ArrayList_t2718874744 * value)
	{
		___conditions_4 = value;
		Il2CppCodeGenWriteBarrier(&___conditions_4, value);
	}
};

struct XslChoose_t3297598844_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.Operations.XslChoose::<>f__switch$map9
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map9_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map9_5() { return static_cast<int32_t>(offsetof(XslChoose_t3297598844_StaticFields, ___U3CU3Ef__switchU24map9_5)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map9_5() const { return ___U3CU3Ef__switchU24map9_5; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map9_5() { return &___U3CU3Ef__switchU24map9_5; }
	inline void set_U3CU3Ef__switchU24map9_5(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map9_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map9_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
