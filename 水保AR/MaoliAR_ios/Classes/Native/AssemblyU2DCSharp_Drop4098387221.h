﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Vector33722313464.h"
#include "UnityEngine_UnityEngine_LayerMask3493934918.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Camera
struct Camera_t4157153871;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Drop
struct  Drop_t4098387221  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject Drop::Image
	GameObject_t1113636619 * ___Image_2;
	// UnityEngine.GameObject Drop::Cube
	GameObject_t1113636619 * ___Cube_3;
	// UnityEngine.Vector3 Drop::Position
	Vector3_t3722313464  ___Position_4;
	// UnityEngine.Camera Drop::ThisCamera
	Camera_t4157153871 * ___ThisCamera_5;
	// UnityEngine.LayerMask Drop::Targetlayer
	LayerMask_t3493934918  ___Targetlayer_6;

public:
	inline static int32_t get_offset_of_Image_2() { return static_cast<int32_t>(offsetof(Drop_t4098387221, ___Image_2)); }
	inline GameObject_t1113636619 * get_Image_2() const { return ___Image_2; }
	inline GameObject_t1113636619 ** get_address_of_Image_2() { return &___Image_2; }
	inline void set_Image_2(GameObject_t1113636619 * value)
	{
		___Image_2 = value;
		Il2CppCodeGenWriteBarrier(&___Image_2, value);
	}

	inline static int32_t get_offset_of_Cube_3() { return static_cast<int32_t>(offsetof(Drop_t4098387221, ___Cube_3)); }
	inline GameObject_t1113636619 * get_Cube_3() const { return ___Cube_3; }
	inline GameObject_t1113636619 ** get_address_of_Cube_3() { return &___Cube_3; }
	inline void set_Cube_3(GameObject_t1113636619 * value)
	{
		___Cube_3 = value;
		Il2CppCodeGenWriteBarrier(&___Cube_3, value);
	}

	inline static int32_t get_offset_of_Position_4() { return static_cast<int32_t>(offsetof(Drop_t4098387221, ___Position_4)); }
	inline Vector3_t3722313464  get_Position_4() const { return ___Position_4; }
	inline Vector3_t3722313464 * get_address_of_Position_4() { return &___Position_4; }
	inline void set_Position_4(Vector3_t3722313464  value)
	{
		___Position_4 = value;
	}

	inline static int32_t get_offset_of_ThisCamera_5() { return static_cast<int32_t>(offsetof(Drop_t4098387221, ___ThisCamera_5)); }
	inline Camera_t4157153871 * get_ThisCamera_5() const { return ___ThisCamera_5; }
	inline Camera_t4157153871 ** get_address_of_ThisCamera_5() { return &___ThisCamera_5; }
	inline void set_ThisCamera_5(Camera_t4157153871 * value)
	{
		___ThisCamera_5 = value;
		Il2CppCodeGenWriteBarrier(&___ThisCamera_5, value);
	}

	inline static int32_t get_offset_of_Targetlayer_6() { return static_cast<int32_t>(offsetof(Drop_t4098387221, ___Targetlayer_6)); }
	inline LayerMask_t3493934918  get_Targetlayer_6() const { return ___Targetlayer_6; }
	inline LayerMask_t3493934918 * get_address_of_Targetlayer_6() { return &___Targetlayer_6; }
	inline void set_Targetlayer_6(LayerMask_t3493934918  value)
	{
		___Targetlayer_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
