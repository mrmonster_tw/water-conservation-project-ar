﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.UI.Text
struct Text_t1901882714;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextCheck
struct  TextCheck_t3911857046  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text TextCheck::請點擊任意處以繼續遊戲
	Text_t1901882714 * ___U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_2;
	// System.Boolean TextCheck::Tmp
	bool ___Tmp_3;

public:
	inline static int32_t get_offset_of_U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_2() { return static_cast<int32_t>(offsetof(TextCheck_t3911857046, ___U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_2)); }
	inline Text_t1901882714 * get_U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_2() const { return ___U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_2; }
	inline Text_t1901882714 ** get_address_of_U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_2() { return &___U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_2; }
	inline void set_U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_2(Text_t1901882714 * value)
	{
		___U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_2 = value;
		Il2CppCodeGenWriteBarrier(&___U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_2, value);
	}

	inline static int32_t get_offset_of_Tmp_3() { return static_cast<int32_t>(offsetof(TextCheck_t3911857046, ___Tmp_3)); }
	inline bool get_Tmp_3() const { return ___Tmp_3; }
	inline bool* get_address_of_Tmp_3() { return &___Tmp_3; }
	inline void set_Tmp_3(bool value)
	{
		___Tmp_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
