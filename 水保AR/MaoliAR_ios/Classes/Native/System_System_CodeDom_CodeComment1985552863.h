﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeObject3927604602.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeComment
struct  CodeComment_t1985552863  : public CodeObject_t3927604602
{
public:
	// System.Boolean System.CodeDom.CodeComment::docComment
	bool ___docComment_1;
	// System.String System.CodeDom.CodeComment::text
	String_t* ___text_2;

public:
	inline static int32_t get_offset_of_docComment_1() { return static_cast<int32_t>(offsetof(CodeComment_t1985552863, ___docComment_1)); }
	inline bool get_docComment_1() const { return ___docComment_1; }
	inline bool* get_address_of_docComment_1() { return &___docComment_1; }
	inline void set_docComment_1(bool value)
	{
		___docComment_1 = value;
	}

	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(CodeComment_t1985552863, ___text_2)); }
	inline String_t* get_text_2() const { return ___text_2; }
	inline String_t** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(String_t* value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier(&___text_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
