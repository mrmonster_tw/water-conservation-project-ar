﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"
#include "mscorlib_System_IntPtr840150181.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.NativeDapiProtection/CRYPTPROTECT_PROMPTSTRUCT
struct  CRYPTPROTECT_PROMPTSTRUCT_t1592014442 
{
public:
	// System.Int32 Mono.Security.Cryptography.NativeDapiProtection/CRYPTPROTECT_PROMPTSTRUCT::cbSize
	int32_t ___cbSize_0;
	// System.UInt32 Mono.Security.Cryptography.NativeDapiProtection/CRYPTPROTECT_PROMPTSTRUCT::dwPromptFlags
	uint32_t ___dwPromptFlags_1;
	// System.IntPtr Mono.Security.Cryptography.NativeDapiProtection/CRYPTPROTECT_PROMPTSTRUCT::hwndApp
	IntPtr_t ___hwndApp_2;
	// System.String Mono.Security.Cryptography.NativeDapiProtection/CRYPTPROTECT_PROMPTSTRUCT::szPrompt
	String_t* ___szPrompt_3;

public:
	inline static int32_t get_offset_of_cbSize_0() { return static_cast<int32_t>(offsetof(CRYPTPROTECT_PROMPTSTRUCT_t1592014442, ___cbSize_0)); }
	inline int32_t get_cbSize_0() const { return ___cbSize_0; }
	inline int32_t* get_address_of_cbSize_0() { return &___cbSize_0; }
	inline void set_cbSize_0(int32_t value)
	{
		___cbSize_0 = value;
	}

	inline static int32_t get_offset_of_dwPromptFlags_1() { return static_cast<int32_t>(offsetof(CRYPTPROTECT_PROMPTSTRUCT_t1592014442, ___dwPromptFlags_1)); }
	inline uint32_t get_dwPromptFlags_1() const { return ___dwPromptFlags_1; }
	inline uint32_t* get_address_of_dwPromptFlags_1() { return &___dwPromptFlags_1; }
	inline void set_dwPromptFlags_1(uint32_t value)
	{
		___dwPromptFlags_1 = value;
	}

	inline static int32_t get_offset_of_hwndApp_2() { return static_cast<int32_t>(offsetof(CRYPTPROTECT_PROMPTSTRUCT_t1592014442, ___hwndApp_2)); }
	inline IntPtr_t get_hwndApp_2() const { return ___hwndApp_2; }
	inline IntPtr_t* get_address_of_hwndApp_2() { return &___hwndApp_2; }
	inline void set_hwndApp_2(IntPtr_t value)
	{
		___hwndApp_2 = value;
	}

	inline static int32_t get_offset_of_szPrompt_3() { return static_cast<int32_t>(offsetof(CRYPTPROTECT_PROMPTSTRUCT_t1592014442, ___szPrompt_3)); }
	inline String_t* get_szPrompt_3() const { return ___szPrompt_3; }
	inline String_t** get_address_of_szPrompt_3() { return &___szPrompt_3; }
	inline void set_szPrompt_3(String_t* value)
	{
		___szPrompt_3 = value;
		Il2CppCodeGenWriteBarrier(&___szPrompt_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Mono.Security.Cryptography.NativeDapiProtection/CRYPTPROTECT_PROMPTSTRUCT
struct CRYPTPROTECT_PROMPTSTRUCT_t1592014442_marshaled_pinvoke
{
	int32_t ___cbSize_0;
	uint32_t ___dwPromptFlags_1;
	intptr_t ___hwndApp_2;
	char* ___szPrompt_3;
};
// Native definition for COM marshalling of Mono.Security.Cryptography.NativeDapiProtection/CRYPTPROTECT_PROMPTSTRUCT
struct CRYPTPROTECT_PROMPTSTRUCT_t1592014442_marshaled_com
{
	int32_t ___cbSize_0;
	uint32_t ___dwPromptFlags_1;
	intptr_t ___hwndApp_2;
	Il2CppChar* ___szPrompt_3;
};
