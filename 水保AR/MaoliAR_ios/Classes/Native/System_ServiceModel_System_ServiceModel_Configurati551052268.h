﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Configurat4182893664.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.MsmqBindingElementBase
struct  MsmqBindingElementBase_t551052268  : public StandardBindingElement_t4182893664
{
public:

public:
};

struct MsmqBindingElementBase_t551052268_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.MsmqBindingElementBase::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_14;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.MsmqBindingElementBase::custom_dead_letter_queue
	ConfigurationProperty_t3590861854 * ___custom_dead_letter_queue_15;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.MsmqBindingElementBase::dead_letter_queue
	ConfigurationProperty_t3590861854 * ___dead_letter_queue_16;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.MsmqBindingElementBase::durable
	ConfigurationProperty_t3590861854 * ___durable_17;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.MsmqBindingElementBase::exactly_once
	ConfigurationProperty_t3590861854 * ___exactly_once_18;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.MsmqBindingElementBase::max_received_message_size
	ConfigurationProperty_t3590861854 * ___max_received_message_size_19;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.MsmqBindingElementBase::max_retry_cycles
	ConfigurationProperty_t3590861854 * ___max_retry_cycles_20;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.MsmqBindingElementBase::receive_error_handling
	ConfigurationProperty_t3590861854 * ___receive_error_handling_21;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.MsmqBindingElementBase::receive_retry_count
	ConfigurationProperty_t3590861854 * ___receive_retry_count_22;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.MsmqBindingElementBase::retry_cycle_delay
	ConfigurationProperty_t3590861854 * ___retry_cycle_delay_23;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.MsmqBindingElementBase::time_to_live
	ConfigurationProperty_t3590861854 * ___time_to_live_24;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.MsmqBindingElementBase::use_msmq_tracing
	ConfigurationProperty_t3590861854 * ___use_msmq_tracing_25;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.MsmqBindingElementBase::use_source_journal
	ConfigurationProperty_t3590861854 * ___use_source_journal_26;

public:
	inline static int32_t get_offset_of_properties_14() { return static_cast<int32_t>(offsetof(MsmqBindingElementBase_t551052268_StaticFields, ___properties_14)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_14() const { return ___properties_14; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_14() { return &___properties_14; }
	inline void set_properties_14(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_14 = value;
		Il2CppCodeGenWriteBarrier(&___properties_14, value);
	}

	inline static int32_t get_offset_of_custom_dead_letter_queue_15() { return static_cast<int32_t>(offsetof(MsmqBindingElementBase_t551052268_StaticFields, ___custom_dead_letter_queue_15)); }
	inline ConfigurationProperty_t3590861854 * get_custom_dead_letter_queue_15() const { return ___custom_dead_letter_queue_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_custom_dead_letter_queue_15() { return &___custom_dead_letter_queue_15; }
	inline void set_custom_dead_letter_queue_15(ConfigurationProperty_t3590861854 * value)
	{
		___custom_dead_letter_queue_15 = value;
		Il2CppCodeGenWriteBarrier(&___custom_dead_letter_queue_15, value);
	}

	inline static int32_t get_offset_of_dead_letter_queue_16() { return static_cast<int32_t>(offsetof(MsmqBindingElementBase_t551052268_StaticFields, ___dead_letter_queue_16)); }
	inline ConfigurationProperty_t3590861854 * get_dead_letter_queue_16() const { return ___dead_letter_queue_16; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_dead_letter_queue_16() { return &___dead_letter_queue_16; }
	inline void set_dead_letter_queue_16(ConfigurationProperty_t3590861854 * value)
	{
		___dead_letter_queue_16 = value;
		Il2CppCodeGenWriteBarrier(&___dead_letter_queue_16, value);
	}

	inline static int32_t get_offset_of_durable_17() { return static_cast<int32_t>(offsetof(MsmqBindingElementBase_t551052268_StaticFields, ___durable_17)); }
	inline ConfigurationProperty_t3590861854 * get_durable_17() const { return ___durable_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_durable_17() { return &___durable_17; }
	inline void set_durable_17(ConfigurationProperty_t3590861854 * value)
	{
		___durable_17 = value;
		Il2CppCodeGenWriteBarrier(&___durable_17, value);
	}

	inline static int32_t get_offset_of_exactly_once_18() { return static_cast<int32_t>(offsetof(MsmqBindingElementBase_t551052268_StaticFields, ___exactly_once_18)); }
	inline ConfigurationProperty_t3590861854 * get_exactly_once_18() const { return ___exactly_once_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_exactly_once_18() { return &___exactly_once_18; }
	inline void set_exactly_once_18(ConfigurationProperty_t3590861854 * value)
	{
		___exactly_once_18 = value;
		Il2CppCodeGenWriteBarrier(&___exactly_once_18, value);
	}

	inline static int32_t get_offset_of_max_received_message_size_19() { return static_cast<int32_t>(offsetof(MsmqBindingElementBase_t551052268_StaticFields, ___max_received_message_size_19)); }
	inline ConfigurationProperty_t3590861854 * get_max_received_message_size_19() const { return ___max_received_message_size_19; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_max_received_message_size_19() { return &___max_received_message_size_19; }
	inline void set_max_received_message_size_19(ConfigurationProperty_t3590861854 * value)
	{
		___max_received_message_size_19 = value;
		Il2CppCodeGenWriteBarrier(&___max_received_message_size_19, value);
	}

	inline static int32_t get_offset_of_max_retry_cycles_20() { return static_cast<int32_t>(offsetof(MsmqBindingElementBase_t551052268_StaticFields, ___max_retry_cycles_20)); }
	inline ConfigurationProperty_t3590861854 * get_max_retry_cycles_20() const { return ___max_retry_cycles_20; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_max_retry_cycles_20() { return &___max_retry_cycles_20; }
	inline void set_max_retry_cycles_20(ConfigurationProperty_t3590861854 * value)
	{
		___max_retry_cycles_20 = value;
		Il2CppCodeGenWriteBarrier(&___max_retry_cycles_20, value);
	}

	inline static int32_t get_offset_of_receive_error_handling_21() { return static_cast<int32_t>(offsetof(MsmqBindingElementBase_t551052268_StaticFields, ___receive_error_handling_21)); }
	inline ConfigurationProperty_t3590861854 * get_receive_error_handling_21() const { return ___receive_error_handling_21; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_receive_error_handling_21() { return &___receive_error_handling_21; }
	inline void set_receive_error_handling_21(ConfigurationProperty_t3590861854 * value)
	{
		___receive_error_handling_21 = value;
		Il2CppCodeGenWriteBarrier(&___receive_error_handling_21, value);
	}

	inline static int32_t get_offset_of_receive_retry_count_22() { return static_cast<int32_t>(offsetof(MsmqBindingElementBase_t551052268_StaticFields, ___receive_retry_count_22)); }
	inline ConfigurationProperty_t3590861854 * get_receive_retry_count_22() const { return ___receive_retry_count_22; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_receive_retry_count_22() { return &___receive_retry_count_22; }
	inline void set_receive_retry_count_22(ConfigurationProperty_t3590861854 * value)
	{
		___receive_retry_count_22 = value;
		Il2CppCodeGenWriteBarrier(&___receive_retry_count_22, value);
	}

	inline static int32_t get_offset_of_retry_cycle_delay_23() { return static_cast<int32_t>(offsetof(MsmqBindingElementBase_t551052268_StaticFields, ___retry_cycle_delay_23)); }
	inline ConfigurationProperty_t3590861854 * get_retry_cycle_delay_23() const { return ___retry_cycle_delay_23; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_retry_cycle_delay_23() { return &___retry_cycle_delay_23; }
	inline void set_retry_cycle_delay_23(ConfigurationProperty_t3590861854 * value)
	{
		___retry_cycle_delay_23 = value;
		Il2CppCodeGenWriteBarrier(&___retry_cycle_delay_23, value);
	}

	inline static int32_t get_offset_of_time_to_live_24() { return static_cast<int32_t>(offsetof(MsmqBindingElementBase_t551052268_StaticFields, ___time_to_live_24)); }
	inline ConfigurationProperty_t3590861854 * get_time_to_live_24() const { return ___time_to_live_24; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_time_to_live_24() { return &___time_to_live_24; }
	inline void set_time_to_live_24(ConfigurationProperty_t3590861854 * value)
	{
		___time_to_live_24 = value;
		Il2CppCodeGenWriteBarrier(&___time_to_live_24, value);
	}

	inline static int32_t get_offset_of_use_msmq_tracing_25() { return static_cast<int32_t>(offsetof(MsmqBindingElementBase_t551052268_StaticFields, ___use_msmq_tracing_25)); }
	inline ConfigurationProperty_t3590861854 * get_use_msmq_tracing_25() const { return ___use_msmq_tracing_25; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_use_msmq_tracing_25() { return &___use_msmq_tracing_25; }
	inline void set_use_msmq_tracing_25(ConfigurationProperty_t3590861854 * value)
	{
		___use_msmq_tracing_25 = value;
		Il2CppCodeGenWriteBarrier(&___use_msmq_tracing_25, value);
	}

	inline static int32_t get_offset_of_use_source_journal_26() { return static_cast<int32_t>(offsetof(MsmqBindingElementBase_t551052268_StaticFields, ___use_source_journal_26)); }
	inline ConfigurationProperty_t3590861854 * get_use_source_journal_26() const { return ___use_source_journal_26; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_use_source_journal_26() { return &___use_source_journal_26; }
	inline void set_use_source_journal_26(ConfigurationProperty_t3590861854 * value)
	{
		___use_source_journal_26 = value;
		Il2CppCodeGenWriteBarrier(&___use_source_journal_26, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
