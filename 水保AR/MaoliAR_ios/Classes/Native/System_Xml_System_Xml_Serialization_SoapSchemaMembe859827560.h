﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2760654312;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.SoapSchemaMember
struct  SoapSchemaMember_t859827560  : public Il2CppObject
{
public:
	// System.String System.Xml.Serialization.SoapSchemaMember::memberName
	String_t* ___memberName_0;
	// System.Xml.XmlQualifiedName System.Xml.Serialization.SoapSchemaMember::memberType
	XmlQualifiedName_t2760654312 * ___memberType_1;

public:
	inline static int32_t get_offset_of_memberName_0() { return static_cast<int32_t>(offsetof(SoapSchemaMember_t859827560, ___memberName_0)); }
	inline String_t* get_memberName_0() const { return ___memberName_0; }
	inline String_t** get_address_of_memberName_0() { return &___memberName_0; }
	inline void set_memberName_0(String_t* value)
	{
		___memberName_0 = value;
		Il2CppCodeGenWriteBarrier(&___memberName_0, value);
	}

	inline static int32_t get_offset_of_memberType_1() { return static_cast<int32_t>(offsetof(SoapSchemaMember_t859827560, ___memberType_1)); }
	inline XmlQualifiedName_t2760654312 * get_memberType_1() const { return ___memberType_1; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_memberType_1() { return &___memberType_1; }
	inline void set_memberType_1(XmlQualifiedName_t2760654312 * value)
	{
		___memberType_1 = value;
		Il2CppCodeGenWriteBarrier(&___memberType_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
