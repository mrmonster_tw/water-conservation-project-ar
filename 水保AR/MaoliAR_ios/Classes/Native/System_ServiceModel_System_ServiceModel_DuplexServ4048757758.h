﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_ServiceRun1028057095.h"

// System.ServiceModel.ClientRuntimeChannel
struct ClientRuntimeChannel_t398573209;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.DuplexServiceRuntimeChannel
struct  DuplexServiceRuntimeChannel_t4048757758  : public ServiceRuntimeChannel_t1028057095
{
public:
	// System.ServiceModel.ClientRuntimeChannel System.ServiceModel.DuplexServiceRuntimeChannel::client
	ClientRuntimeChannel_t398573209 * ___client_12;

public:
	inline static int32_t get_offset_of_client_12() { return static_cast<int32_t>(offsetof(DuplexServiceRuntimeChannel_t4048757758, ___client_12)); }
	inline ClientRuntimeChannel_t398573209 * get_client_12() const { return ___client_12; }
	inline ClientRuntimeChannel_t398573209 ** get_address_of_client_12() { return &___client_12; }
	inline void set_client_12(ClientRuntimeChannel_t398573209 * value)
	{
		___client_12 = value;
		Il2CppCodeGenWriteBarrier(&___client_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
