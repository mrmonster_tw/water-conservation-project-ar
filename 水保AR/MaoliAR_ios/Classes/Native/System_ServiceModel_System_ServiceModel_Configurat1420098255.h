﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_A3096310965.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.AddressHeaderCollectionElement/ConfiguredAddressHeader
struct  ConfiguredAddressHeader_t1420098255  : public AddressHeader_t3096310965
{
public:
	// System.String System.ServiceModel.Configuration.AddressHeaderCollectionElement/ConfiguredAddressHeader::_name
	String_t* ____name_0;
	// System.String System.ServiceModel.Configuration.AddressHeaderCollectionElement/ConfiguredAddressHeader::_namespace
	String_t* ____namespace_1;
	// System.String System.ServiceModel.Configuration.AddressHeaderCollectionElement/ConfiguredAddressHeader::_outerXml
	String_t* ____outerXml_2;

public:
	inline static int32_t get_offset_of__name_0() { return static_cast<int32_t>(offsetof(ConfiguredAddressHeader_t1420098255, ____name_0)); }
	inline String_t* get__name_0() const { return ____name_0; }
	inline String_t** get_address_of__name_0() { return &____name_0; }
	inline void set__name_0(String_t* value)
	{
		____name_0 = value;
		Il2CppCodeGenWriteBarrier(&____name_0, value);
	}

	inline static int32_t get_offset_of__namespace_1() { return static_cast<int32_t>(offsetof(ConfiguredAddressHeader_t1420098255, ____namespace_1)); }
	inline String_t* get__namespace_1() const { return ____namespace_1; }
	inline String_t** get_address_of__namespace_1() { return &____namespace_1; }
	inline void set__namespace_1(String_t* value)
	{
		____namespace_1 = value;
		Il2CppCodeGenWriteBarrier(&____namespace_1, value);
	}

	inline static int32_t get_offset_of__outerXml_2() { return static_cast<int32_t>(offsetof(ConfiguredAddressHeader_t1420098255, ____outerXml_2)); }
	inline String_t* get__outerXml_2() const { return ____outerXml_2; }
	inline String_t** get_address_of__outerXml_2() { return &____outerXml_2; }
	inline void set__outerXml_2(String_t* value)
	{
		____outerXml_2 = value;
		Il2CppCodeGenWriteBarrier(&____outerXml_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
