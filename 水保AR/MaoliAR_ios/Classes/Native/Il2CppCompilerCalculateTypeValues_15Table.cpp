﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslFallback2816057531.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslForEach3423108485.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslIf3169476703.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslLiteralEleme1817810350.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslLiteralEleme3203716779.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslMessage3346364917.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslNotSupported2607115657.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslNumber2577579437.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslNumber_XslNu3669835622.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslNumber_XslNu2819324807.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslNumber_XslNu2815695937.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslNumber_XslNu2219098400.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslNumber_XslNu2026450318.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslNumber_XslNu2549330676.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslNumberingLev1604989502.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslOperation2153241355.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslProcessingIn3271313310.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslTemplateCont3070616797.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslText705431422.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslValueOf3484759627.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslVariableInfo1039906920.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslGeneralVaria1456221871.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslGlobalVariab2754044456.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslGlobalParam955387569.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslLocalVariable667984012.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XslLocalParam872751518.h"
#include "System_Xml_Mono_Xml_Xsl_Operations_XPathVariableBi3684345436.h"
#include "System_Xml_Mono_Xml_Xsl_Attribute270895445.h"
#include "System_Xml_Mono_Xml_Xsl_CompiledStylesheet759457236.h"
#include "System_Xml_Mono_Xml_Xsl_Compiler485846590.h"
#include "System_Xml_Mono_Xml_Xsl_VariableScope3522028429.h"
#include "System_Xml_Mono_Xml_Xsl_Sort3076360436.h"
#include "System_Xml_Mono_Xml_Xsl_XslNameUtil3173730477.h"
#include "System_Xml_Mono_Xml_Xsl_Emitter942443340.h"
#include "System_Xml_Mono_Xml_Xsl_GenericOutputter1694878872.h"
#include "System_Xml_Mono_Xml_Xsl_HtmlEmitter3325938161.h"
#include "System_Xml_Mono_Xml_Xsl_HtmlEmitter_HtmlUriEscape2290855028.h"
#include "System_Xml_Mono_Xml_Xsl_MSXslScriptManager4018040365.h"
#include "System_Xml_Mono_Xml_Xsl_MSXslScriptManager_Scriptin290502390.h"
#include "System_Xml_Mono_Xml_Xsl_MSXslScriptManager_MSXslSc2775088162.h"
#include "System_Xml_Mono_Xml_Xsl_Outputter889105973.h"
#include "System_Xml_Mono_Xml_Xsl_ScriptCompilerInfo3765441597.h"
#include "System_Xml_Mono_Xml_Xsl_CSharpCompilerInfo996425946.h"
#include "System_Xml_Mono_Xml_Xsl_VBCompilerInfo2386552770.h"
#include "System_Xml_Mono_Xml_Xsl_JScriptCompilerInfo2884625701.h"
#include "System_Xml_Mono_Xml_Xsl_TextEmitter2742298062.h"
#include "System_Xml_Mono_Xml_Xsl_TextOutputter2693241571.h"
#include "System_Xml_Mono_Xml_Xsl_XmlWriterEmitter503944025.h"
#include "System_Xml_Mono_Xml_Xsl_XslAttributeSet4274455021.h"
#include "System_Xml_Mono_Xml_Xsl_XslDecimalFormat2341217942.h"
#include "System_Xml_Mono_Xml_Xsl_DecimalFormatPatternSet2186286526.h"
#include "System_Xml_Mono_Xml_Xsl_DecimalFormatPattern3266596013.h"
#include "System_Xml_Mono_Xml_Xsl_ExprKeyContainer480641962.h"
#include "System_Xml_Mono_Xml_Xsl_XslKey2738464697.h"
#include "System_Xml_Mono_Xml_Xsl_KeyIndexTable1422728481.h"
#include "System_Xml_Mono_Xml_Xsl_OutputMethod245858122.h"
#include "System_Xml_Mono_Xml_Xsl_StandaloneType1639935827.h"
#include "System_Xml_Mono_Xml_Xsl_XslOutput472631936.h"
#include "System_Xml_Mono_Xml_Xsl_XslSortEvaluator824821660.h"
#include "System_Xml_Mono_Xml_Xsl_XslStylesheet113441946.h"
#include "System_Xml_Mono_Xml_Xsl_XslModedTemplateTable914225669.h"
#include "System_Xml_Mono_Xml_Xsl_XslModedTemplateTable_Templ796810083.h"
#include "System_Xml_Mono_Xml_Xsl_XslTemplateTable625046267.h"
#include "System_Xml_Mono_Xml_Xsl_XslTemplate152263049.h"
#include "System_Xml_Mono_Xml_Xsl_XslDefaultNodeTemplate4014140780.h"
#include "System_Xml_Mono_Xml_Xsl_XslEmptyTemplate1993620519.h"
#include "System_Xml_Mono_Xml_Xsl_XslDefaultTextTemplate2363894394.h"
#include "System_Xml_Mono_Xml_Xsl_XslTransformProcessor3405861191.h"
#include "System_Xml_Mono_Xml_Xsl_XsltCompiledContext2295645487.h"
#include "System_Xml_Mono_Xml_Xsl_XsltCompiledContext_XsltCo2905292101.h"
#include "System_Xml_Mono_Xml_Xsl_XsltDebuggerWrapper4148725768.h"
#include "System_Xml_Mono_Xml_Xsl_XPFuncImpl459957616.h"
#include "System_Xml_Mono_Xml_Xsl_XsltExtensionFunction863890153.h"
#include "System_Xml_Mono_Xml_Xsl_XsltCurrent1577851673.h"
#include "System_Xml_Mono_Xml_Xsl_XsltDocument841609188.h"
#include "System_Xml_Mono_Xml_Xsl_XsltElementAvailable1740333384.h"
#include "System_Xml_Mono_Xml_Xsl_XsltFormatNumber539498555.h"
#include "System_Xml_Mono_Xml_Xsl_XsltFunctionAvailable2598188313.h"
#include "System_Xml_Mono_Xml_Xsl_XsltGenerateId1800457284.h"
#include "System_Xml_Mono_Xml_Xsl_XsltKey826370162.h"
#include "System_Xml_Mono_Xml_Xsl_XsltSystemProperty2954510946.h"
#include "System_Xml_Mono_Xml_Xsl_XsltUnparsedEntityUri1001948404.h"
#include "System_Xml_Mono_Xml_Xsl_MSXslNodeSet3928430336.h"
#include "System_Xml_Mono_Xml_XmlFilterReader953863161.h"
#include "System_Xml_System_Xml_ConformanceLevel3899847875.h"
#include "System_Xml_Mono_Xml_DTDAutomataFactory2958275022.h"
#include "System_Xml_Mono_Xml_DTDAutomata781538777.h"
#include "System_Xml_Mono_Xml_DTDElementAutomata1050190167.h"
#include "System_Xml_Mono_Xml_DTDChoiceAutomata3129343723.h"
#include "System_Xml_Mono_Xml_DTDSequenceAutomata2865847115.h"
#include "System_Xml_Mono_Xml_DTDOneOrMoreAutomata1192707047.h"
#include "System_Xml_Mono_Xml_DTDEmptyAutomata465590953.h"
#include "System_Xml_Mono_Xml_DTDAnyAutomata3633486160.h"
#include "System_Xml_Mono_Xml_DTDInvalidAutomata1406553220.h"
#include "System_Xml_Mono_Xml_DTDObjectModel1729680289.h"
#include "System_Xml_Mono_Xml_DictionaryBase52754249.h"
#include "System_Xml_Mono_Xml_DictionaryBase_U3CU3Ec__Iterat2072931442.h"
#include "System_Xml_Mono_Xml_DTDCollectionBase3926218464.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1500 = { sizeof (XslFallback_t2816057531), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1500[1] = 
{
	XslFallback_t2816057531::get_offset_of_children_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1501 = { sizeof (XslForEach_t3423108485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1501[3] = 
{
	XslForEach_t3423108485::get_offset_of_select_3(),
	XslForEach_t3423108485::get_offset_of_children_4(),
	XslForEach_t3423108485::get_offset_of_sortEvaluator_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1502 = { sizeof (XslIf_t3169476703), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1502[2] = 
{
	XslIf_t3169476703::get_offset_of_test_3(),
	XslIf_t3169476703::get_offset_of_children_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1503 = { sizeof (XslLiteralElement_t1817810350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1503[8] = 
{
	XslLiteralElement_t1817810350::get_offset_of_children_3(),
	XslLiteralElement_t1817810350::get_offset_of_localname_4(),
	XslLiteralElement_t1817810350::get_offset_of_prefix_5(),
	XslLiteralElement_t1817810350::get_offset_of_nsUri_6(),
	XslLiteralElement_t1817810350::get_offset_of_isEmptyElement_7(),
	XslLiteralElement_t1817810350::get_offset_of_attrs_8(),
	XslLiteralElement_t1817810350::get_offset_of_useAttributeSets_9(),
	XslLiteralElement_t1817810350::get_offset_of_nsDecls_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1504 = { sizeof (XslLiteralAttribute_t3203716779), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1504[4] = 
{
	XslLiteralAttribute_t3203716779::get_offset_of_localname_0(),
	XslLiteralAttribute_t3203716779::get_offset_of_prefix_1(),
	XslLiteralAttribute_t3203716779::get_offset_of_nsUri_2(),
	XslLiteralAttribute_t3203716779::get_offset_of_val_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1505 = { sizeof (XslMessage_t3346364917), -1, sizeof(XslMessage_t3346364917_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1505[4] = 
{
	XslMessage_t3346364917_StaticFields::get_offset_of_output_3(),
	XslMessage_t3346364917::get_offset_of_terminate_4(),
	XslMessage_t3346364917::get_offset_of_children_5(),
	XslMessage_t3346364917_StaticFields::get_offset_of_U3CU3Ef__switchU24mapA_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1506 = { sizeof (XslNotSupportedOperation_t2607115657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1506[2] = 
{
	XslNotSupportedOperation_t2607115657::get_offset_of_name_3(),
	XslNotSupportedOperation_t2607115657::get_offset_of_fallbacks_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1507 = { sizeof (XslNumber_t2577579437), -1, sizeof(XslNumber_t2577579437_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1507[10] = 
{
	XslNumber_t2577579437::get_offset_of_level_3(),
	XslNumber_t2577579437::get_offset_of_count_4(),
	XslNumber_t2577579437::get_offset_of_from_5(),
	XslNumber_t2577579437::get_offset_of_value_6(),
	XslNumber_t2577579437::get_offset_of_format_7(),
	XslNumber_t2577579437::get_offset_of_lang_8(),
	XslNumber_t2577579437::get_offset_of_letterValue_9(),
	XslNumber_t2577579437::get_offset_of_groupingSeparator_10(),
	XslNumber_t2577579437::get_offset_of_groupingSize_11(),
	XslNumber_t2577579437_StaticFields::get_offset_of_U3CU3Ef__switchU24mapB_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1508 = { sizeof (XslNumberFormatter_t3669835622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1508[3] = 
{
	XslNumberFormatter_t3669835622::get_offset_of_firstSep_0(),
	XslNumberFormatter_t3669835622::get_offset_of_lastSep_1(),
	XslNumberFormatter_t3669835622::get_offset_of_fmtList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1509 = { sizeof (NumberFormatterScanner_t2819324807), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1509[3] = 
{
	NumberFormatterScanner_t2819324807::get_offset_of_pos_0(),
	NumberFormatterScanner_t2819324807::get_offset_of_len_1(),
	NumberFormatterScanner_t2819324807::get_offset_of_fmt_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1510 = { sizeof (FormatItem_t2815695937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1510[1] = 
{
	FormatItem_t2815695937::get_offset_of_sep_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1511 = { sizeof (AlphaItem_t2219098400), -1, sizeof(AlphaItem_t2219098400_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1511[3] = 
{
	AlphaItem_t2219098400::get_offset_of_uc_1(),
	AlphaItem_t2219098400_StaticFields::get_offset_of_ucl_2(),
	AlphaItem_t2219098400_StaticFields::get_offset_of_lcl_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1512 = { sizeof (RomanItem_t2026450318), -1, sizeof(RomanItem_t2026450318_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1512[4] = 
{
	RomanItem_t2026450318::get_offset_of_uc_1(),
	RomanItem_t2026450318_StaticFields::get_offset_of_ucrDigits_2(),
	RomanItem_t2026450318_StaticFields::get_offset_of_lcrDigits_3(),
	RomanItem_t2026450318_StaticFields::get_offset_of_decValues_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1513 = { sizeof (DigitItem_t2549330676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1513[3] = 
{
	DigitItem_t2549330676::get_offset_of_nfi_1(),
	DigitItem_t2549330676::get_offset_of_decimalSectionLength_2(),
	DigitItem_t2549330676::get_offset_of_numberBuilder_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1514 = { sizeof (XslNumberingLevel_t1604989502)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1514[4] = 
{
	XslNumberingLevel_t1604989502::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1515 = { sizeof (XslOperation_t2153241355), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1516 = { sizeof (XslProcessingInstruction_t3271313310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1516[2] = 
{
	XslProcessingInstruction_t3271313310::get_offset_of_name_3(),
	XslProcessingInstruction_t3271313310::get_offset_of_value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1517 = { sizeof (XslTemplateContent_t3070616797), -1, sizeof(XslTemplateContent_t3070616797_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1517[7] = 
{
	XslTemplateContent_t3070616797::get_offset_of_content_3(),
	XslTemplateContent_t3070616797::get_offset_of_hasStack_4(),
	XslTemplateContent_t3070616797::get_offset_of_stackSize_5(),
	XslTemplateContent_t3070616797::get_offset_of_parentType_6(),
	XslTemplateContent_t3070616797::get_offset_of_xslForEach_7(),
	XslTemplateContent_t3070616797_StaticFields::get_offset_of_U3CU3Ef__switchU24mapC_8(),
	XslTemplateContent_t3070616797_StaticFields::get_offset_of_U3CU3Ef__switchU24mapD_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1518 = { sizeof (XslText_t705431422), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1518[3] = 
{
	XslText_t705431422::get_offset_of_disableOutputEscaping_3(),
	XslText_t705431422::get_offset_of_text_4(),
	XslText_t705431422::get_offset_of_isWhitespace_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1519 = { sizeof (XslValueOf_t3484759627), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1519[2] = 
{
	XslValueOf_t3484759627::get_offset_of_select_3(),
	XslValueOf_t3484759627::get_offset_of_disableOutputEscaping_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1520 = { sizeof (XslVariableInformation_t1039906920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1520[3] = 
{
	XslVariableInformation_t1039906920::get_offset_of_name_0(),
	XslVariableInformation_t1039906920::get_offset_of_select_1(),
	XslVariableInformation_t1039906920::get_offset_of_content_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1521 = { sizeof (XslGeneralVariable_t1456221871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1521[1] = 
{
	XslGeneralVariable_t1456221871::get_offset_of_var_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1522 = { sizeof (XslGlobalVariable_t2754044456), -1, sizeof(XslGlobalVariable_t2754044456_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1522[1] = 
{
	XslGlobalVariable_t2754044456_StaticFields::get_offset_of_busyObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1523 = { sizeof (XslGlobalParam_t955387569), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1524 = { sizeof (XslLocalVariable_t667984012), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1524[1] = 
{
	XslLocalVariable_t667984012::get_offset_of_slot_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1525 = { sizeof (XslLocalParam_t872751518), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1526 = { sizeof (XPathVariableBinding_t3684345436), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1526[1] = 
{
	XPathVariableBinding_t3684345436::get_offset_of_v_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1527 = { sizeof (Attribute_t270895445)+ sizeof (Il2CppObject), sizeof(Attribute_t270895445_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1527[4] = 
{
	Attribute_t270895445::get_offset_of_Prefix_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Attribute_t270895445::get_offset_of_Namespace_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Attribute_t270895445::get_offset_of_LocalName_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Attribute_t270895445::get_offset_of_Value_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1528 = { sizeof (CompiledStylesheet_t759457236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1528[8] = 
{
	CompiledStylesheet_t759457236::get_offset_of_style_0(),
	CompiledStylesheet_t759457236::get_offset_of_globalVariables_1(),
	CompiledStylesheet_t759457236::get_offset_of_attrSets_2(),
	CompiledStylesheet_t759457236::get_offset_of_nsMgr_3(),
	CompiledStylesheet_t759457236::get_offset_of_keys_4(),
	CompiledStylesheet_t759457236::get_offset_of_outputs_5(),
	CompiledStylesheet_t759457236::get_offset_of_decimalFormats_6(),
	CompiledStylesheet_t759457236::get_offset_of_msScripts_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1529 = { sizeof (Compiler_t485846590), -1, sizeof(Compiler_t485846590_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1529[22] = 
{
	Compiler_t485846590::get_offset_of_inputStack_0(),
	Compiler_t485846590::get_offset_of_currentInput_1(),
	Compiler_t485846590::get_offset_of_styleStack_2(),
	Compiler_t485846590::get_offset_of_currentStyle_3(),
	Compiler_t485846590::get_offset_of_keys_4(),
	Compiler_t485846590::get_offset_of_globalVariables_5(),
	Compiler_t485846590::get_offset_of_attrSets_6(),
	Compiler_t485846590::get_offset_of_nsMgr_7(),
	Compiler_t485846590::get_offset_of_res_8(),
	Compiler_t485846590::get_offset_of_evidence_9(),
	Compiler_t485846590::get_offset_of_rootStyle_10(),
	Compiler_t485846590::get_offset_of_outputs_11(),
	Compiler_t485846590::get_offset_of_keyCompilationMode_12(),
	Compiler_t485846590::get_offset_of_stylesheetVersion_13(),
	Compiler_t485846590::get_offset_of_debugger_14(),
	Compiler_t485846590::get_offset_of_msScripts_15(),
	Compiler_t485846590::get_offset_of_xpathParser_16(),
	Compiler_t485846590::get_offset_of_patternParser_17(),
	Compiler_t485846590::get_offset_of_curVarScope_18(),
	Compiler_t485846590::get_offset_of_decimalFormats_19(),
	Compiler_t485846590_StaticFields::get_offset_of_U3CU3Ef__switchU24mapE_20(),
	Compiler_t485846590_StaticFields::get_offset_of_U3CU3Ef__switchU24mapF_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1530 = { sizeof (VariableScope_t3522028429), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1530[5] = 
{
	VariableScope_t3522028429::get_offset_of_variableNames_0(),
	VariableScope_t3522028429::get_offset_of_variables_1(),
	VariableScope_t3522028429::get_offset_of_parent_2(),
	VariableScope_t3522028429::get_offset_of_nextSlot_3(),
	VariableScope_t3522028429::get_offset_of_highTide_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1531 = { sizeof (Sort_t3076360436), -1, sizeof(Sort_t3076360436_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1531[12] = 
{
	Sort_t3076360436::get_offset_of_lang_0(),
	Sort_t3076360436::get_offset_of_dataType_1(),
	Sort_t3076360436::get_offset_of_order_2(),
	Sort_t3076360436::get_offset_of_caseOrder_3(),
	Sort_t3076360436::get_offset_of_langAvt_4(),
	Sort_t3076360436::get_offset_of_dataTypeAvt_5(),
	Sort_t3076360436::get_offset_of_orderAvt_6(),
	Sort_t3076360436::get_offset_of_caseOrderAvt_7(),
	Sort_t3076360436::get_offset_of_expr_8(),
	Sort_t3076360436_StaticFields::get_offset_of_U3CU3Ef__switchU24map10_9(),
	Sort_t3076360436_StaticFields::get_offset_of_U3CU3Ef__switchU24map11_10(),
	Sort_t3076360436_StaticFields::get_offset_of_U3CU3Ef__switchU24map12_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1532 = { sizeof (XslNameUtil_t3173730477), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1533 = { sizeof (Emitter_t942443340), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1534 = { sizeof (GenericOutputter_t1694878872), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1534[17] = 
{
	GenericOutputter_t1694878872::get_offset_of__outputs_0(),
	GenericOutputter_t1694878872::get_offset_of__currentOutput_1(),
	GenericOutputter_t1694878872::get_offset_of__emitter_2(),
	GenericOutputter_t1694878872::get_offset_of_pendingTextWriter_3(),
	GenericOutputter_t1694878872::get_offset_of_pendingFirstSpaces_4(),
	GenericOutputter_t1694878872::get_offset_of__state_5(),
	GenericOutputter_t1694878872::get_offset_of_pendingAttributes_6(),
	GenericOutputter_t1694878872::get_offset_of_pendingAttributesPos_7(),
	GenericOutputter_t1694878872::get_offset_of__nsManager_8(),
	GenericOutputter_t1694878872::get_offset_of__currentNamespaceDecls_9(),
	GenericOutputter_t1694878872::get_offset_of_newNamespaces_10(),
	GenericOutputter_t1694878872::get_offset_of__nt_11(),
	GenericOutputter_t1694878872::get_offset_of__encoding_12(),
	GenericOutputter_t1694878872::get_offset_of__canProcessAttributes_13(),
	GenericOutputter_t1694878872::get_offset_of__insideCData_14(),
	GenericOutputter_t1694878872::get_offset_of__omitXmlDeclaration_15(),
	GenericOutputter_t1694878872::get_offset_of__xpCount_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1535 = { sizeof (HtmlEmitter_t3325938161), -1, sizeof(HtmlEmitter_t3325938161_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1535[14] = 
{
	HtmlEmitter_t3325938161::get_offset_of_writer_0(),
	HtmlEmitter_t3325938161::get_offset_of_elementNameStack_1(),
	HtmlEmitter_t3325938161::get_offset_of_openElement_2(),
	HtmlEmitter_t3325938161::get_offset_of_openAttribute_3(),
	HtmlEmitter_t3325938161::get_offset_of_nonHtmlDepth_4(),
	HtmlEmitter_t3325938161::get_offset_of_indent_5(),
	HtmlEmitter_t3325938161::get_offset_of_outputEncoding_6(),
	HtmlEmitter_t3325938161::get_offset_of_mediaType_7(),
	HtmlEmitter_t3325938161_StaticFields::get_offset_of_U3CU3Ef__switchU24map13_8(),
	HtmlEmitter_t3325938161_StaticFields::get_offset_of_U3CU3Ef__switchU24map14_9(),
	HtmlEmitter_t3325938161_StaticFields::get_offset_of_U3CU3Ef__switchU24map15_10(),
	HtmlEmitter_t3325938161_StaticFields::get_offset_of_U3CU3Ef__switchU24map16_11(),
	HtmlEmitter_t3325938161_StaticFields::get_offset_of_U3CU3Ef__switchU24map17_12(),
	HtmlEmitter_t3325938161_StaticFields::get_offset_of_U3CU3Ef__switchU24map18_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1536 = { sizeof (HtmlUriEscape_t2290855028), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1537 = { sizeof (MSXslScriptManager_t4018040365), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1537[1] = 
{
	MSXslScriptManager_t4018040365::get_offset_of_scripts_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1538 = { sizeof (ScriptingLanguage_t290502390)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1538[4] = 
{
	ScriptingLanguage_t290502390::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1539 = { sizeof (MSXslScript_t2775088162), -1, sizeof(MSXslScript_t2775088162_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1539[6] = 
{
	MSXslScript_t2775088162::get_offset_of_language_0(),
	MSXslScript_t2775088162::get_offset_of_implementsPrefix_1(),
	MSXslScript_t2775088162::get_offset_of_code_2(),
	MSXslScript_t2775088162::get_offset_of_evidence_3(),
	MSXslScript_t2775088162_StaticFields::get_offset_of_U3CU3Ef__switchU24map19_4(),
	MSXslScript_t2775088162_StaticFields::get_offset_of_U3CU3Ef__switchU24map1A_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1540 = { sizeof (Outputter_t889105973), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1541 = { sizeof (ScriptCompilerInfo_t3765441597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1541[2] = 
{
	ScriptCompilerInfo_t3765441597::get_offset_of_compilerCommand_0(),
	ScriptCompilerInfo_t3765441597::get_offset_of_defaultCompilerOptions_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1542 = { sizeof (CSharpCompilerInfo_t996425946), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1543 = { sizeof (VBCompilerInfo_t2386552770), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1544 = { sizeof (JScriptCompilerInfo_t2884625701), -1, sizeof(JScriptCompilerInfo_t2884625701_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1544[1] = 
{
	JScriptCompilerInfo_t2884625701_StaticFields::get_offset_of_providerType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1545 = { sizeof (TextEmitter_t2742298062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1545[1] = 
{
	TextEmitter_t2742298062::get_offset_of_writer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1546 = { sizeof (TextOutputter_t2693241571), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1546[3] = 
{
	TextOutputter_t2693241571::get_offset_of__writer_0(),
	TextOutputter_t2693241571::get_offset_of__depth_1(),
	TextOutputter_t2693241571::get_offset_of__ignoreNestedText_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1547 = { sizeof (XmlWriterEmitter_t503944025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1547[1] = 
{
	XmlWriterEmitter_t503944025::get_offset_of_writer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1548 = { sizeof (XslAttributeSet_t4274455021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1548[3] = 
{
	XslAttributeSet_t4274455021::get_offset_of_name_3(),
	XslAttributeSet_t4274455021::get_offset_of_usedAttributeSets_4(),
	XslAttributeSet_t4274455021::get_offset_of_attributes_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1549 = { sizeof (XslDecimalFormat_t2341217942), -1, sizeof(XslDecimalFormat_t2341217942_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1549[9] = 
{
	XslDecimalFormat_t2341217942::get_offset_of_info_0(),
	XslDecimalFormat_t2341217942::get_offset_of_digit_1(),
	XslDecimalFormat_t2341217942::get_offset_of_zeroDigit_2(),
	XslDecimalFormat_t2341217942::get_offset_of_patternSeparator_3(),
	XslDecimalFormat_t2341217942::get_offset_of_baseUri_4(),
	XslDecimalFormat_t2341217942::get_offset_of_lineNumber_5(),
	XslDecimalFormat_t2341217942::get_offset_of_linePosition_6(),
	XslDecimalFormat_t2341217942_StaticFields::get_offset_of_Default_7(),
	XslDecimalFormat_t2341217942_StaticFields::get_offset_of_U3CU3Ef__switchU24map1B_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1550 = { sizeof (DecimalFormatPatternSet_t2186286526), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1550[2] = 
{
	DecimalFormatPatternSet_t2186286526::get_offset_of_positivePattern_0(),
	DecimalFormatPatternSet_t2186286526::get_offset_of_negativePattern_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1551 = { sizeof (DecimalFormatPattern_t3266596013), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1551[5] = 
{
	DecimalFormatPattern_t3266596013::get_offset_of_Prefix_0(),
	DecimalFormatPattern_t3266596013::get_offset_of_Suffix_1(),
	DecimalFormatPattern_t3266596013::get_offset_of_NumberPart_2(),
	DecimalFormatPattern_t3266596013::get_offset_of_info_3(),
	DecimalFormatPattern_t3266596013::get_offset_of_builder_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1552 = { sizeof (ExprKeyContainer_t480641962), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1552[1] = 
{
	ExprKeyContainer_t480641962::get_offset_of_expr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1553 = { sizeof (XslKey_t2738464697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1553[3] = 
{
	XslKey_t2738464697::get_offset_of_name_0(),
	XslKey_t2738464697::get_offset_of_useExpr_1(),
	XslKey_t2738464697::get_offset_of_matchPattern_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1554 = { sizeof (KeyIndexTable_t1422728481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1554[3] = 
{
	KeyIndexTable_t1422728481::get_offset_of_ctx_0(),
	KeyIndexTable_t1422728481::get_offset_of_keys_1(),
	KeyIndexTable_t1422728481::get_offset_of_mappedDocuments_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1555 = { sizeof (OutputMethod_t245858122)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1555[6] = 
{
	OutputMethod_t245858122::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1556 = { sizeof (StandaloneType_t1639935827)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1556[4] = 
{
	StandaloneType_t1639935827::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1557 = { sizeof (XslOutput_t472631936), -1, sizeof(XslOutput_t472631936_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1557[19] = 
{
	XslOutput_t472631936::get_offset_of_uri_0(),
	XslOutput_t472631936::get_offset_of_customMethod_1(),
	XslOutput_t472631936::get_offset_of_method_2(),
	XslOutput_t472631936::get_offset_of_version_3(),
	XslOutput_t472631936::get_offset_of_encoding_4(),
	XslOutput_t472631936::get_offset_of_omitXmlDeclaration_5(),
	XslOutput_t472631936::get_offset_of_standalone_6(),
	XslOutput_t472631936::get_offset_of_doctypePublic_7(),
	XslOutput_t472631936::get_offset_of_doctypeSystem_8(),
	XslOutput_t472631936::get_offset_of_cdataSectionElements_9(),
	XslOutput_t472631936::get_offset_of_indent_10(),
	XslOutput_t472631936::get_offset_of_mediaType_11(),
	XslOutput_t472631936::get_offset_of_stylesheetVersion_12(),
	XslOutput_t472631936::get_offset_of_cdSectsList_13(),
	XslOutput_t472631936_StaticFields::get_offset_of_U3CU3Ef__switchU24map1C_14(),
	XslOutput_t472631936_StaticFields::get_offset_of_U3CU3Ef__switchU24map1D_15(),
	XslOutput_t472631936_StaticFields::get_offset_of_U3CU3Ef__switchU24map1E_16(),
	XslOutput_t472631936_StaticFields::get_offset_of_U3CU3Ef__switchU24map1F_17(),
	XslOutput_t472631936_StaticFields::get_offset_of_U3CU3Ef__switchU24map20_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1558 = { sizeof (XslSortEvaluator_t824821660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1558[5] = 
{
	XslSortEvaluator_t824821660::get_offset_of_select_0(),
	XslSortEvaluator_t824821660::get_offset_of_sorterTemplates_1(),
	XslSortEvaluator_t824821660::get_offset_of_sorters_2(),
	XslSortEvaluator_t824821660::get_offset_of_sortRunner_3(),
	XslSortEvaluator_t824821660::get_offset_of_isSorterContextDependent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1559 = { sizeof (XslStylesheet_t113441946), -1, sizeof(XslStylesheet_t113441946_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1559[21] = 
{
	XslStylesheet_t113441946::get_offset_of_imports_0(),
	XslStylesheet_t113441946::get_offset_of_spaceControls_1(),
	XslStylesheet_t113441946::get_offset_of_namespaceAliases_2(),
	XslStylesheet_t113441946::get_offset_of_parameters_3(),
	XslStylesheet_t113441946::get_offset_of_keys_4(),
	XslStylesheet_t113441946::get_offset_of_variables_5(),
	XslStylesheet_t113441946::get_offset_of_templates_6(),
	XslStylesheet_t113441946::get_offset_of_baseURI_7(),
	XslStylesheet_t113441946::get_offset_of_version_8(),
	XslStylesheet_t113441946::get_offset_of_extensionElementPrefixes_9(),
	XslStylesheet_t113441946::get_offset_of_excludeResultPrefixes_10(),
	XslStylesheet_t113441946::get_offset_of_stylesheetNamespaces_11(),
	XslStylesheet_t113441946::get_offset_of_inProcessIncludes_12(),
	XslStylesheet_t113441946::get_offset_of_countedSpaceControlExistence_13(),
	XslStylesheet_t113441946::get_offset_of_cachedHasSpaceControls_14(),
	XslStylesheet_t113441946_StaticFields::get_offset_of_allMatchName_15(),
	XslStylesheet_t113441946::get_offset_of_countedNamespaceAliases_16(),
	XslStylesheet_t113441946::get_offset_of_cachedHasNamespaceAliases_17(),
	XslStylesheet_t113441946_StaticFields::get_offset_of_U3CU3Ef__switchU24map21_18(),
	XslStylesheet_t113441946_StaticFields::get_offset_of_U3CU3Ef__switchU24map22_19(),
	XslStylesheet_t113441946_StaticFields::get_offset_of_U3CU3Ef__switchU24map23_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1560 = { sizeof (XslModedTemplateTable_t914225669), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1560[3] = 
{
	XslModedTemplateTable_t914225669::get_offset_of_unnamedTemplates_0(),
	XslModedTemplateTable_t914225669::get_offset_of_mode_1(),
	XslModedTemplateTable_t914225669::get_offset_of_sorted_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1561 = { sizeof (TemplateWithPriority_t796810083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1561[4] = 
{
	TemplateWithPriority_t796810083::get_offset_of_Priority_0(),
	TemplateWithPriority_t796810083::get_offset_of_Template_1(),
	TemplateWithPriority_t796810083::get_offset_of_Pattern_2(),
	TemplateWithPriority_t796810083::get_offset_of_TemplateID_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1562 = { sizeof (XslTemplateTable_t625046267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1562[3] = 
{
	XslTemplateTable_t625046267::get_offset_of_templateTables_0(),
	XslTemplateTable_t625046267::get_offset_of_namedTemplates_1(),
	XslTemplateTable_t625046267::get_offset_of_parent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1563 = { sizeof (XslTemplate_t152263049), -1, sizeof(XslTemplate_t152263049_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1563[10] = 
{
	XslTemplate_t152263049::get_offset_of_name_0(),
	XslTemplate_t152263049::get_offset_of_match_1(),
	XslTemplate_t152263049::get_offset_of_mode_2(),
	XslTemplate_t152263049::get_offset_of_priority_3(),
	XslTemplate_t152263049::get_offset_of_parameters_4(),
	XslTemplate_t152263049::get_offset_of_content_5(),
	XslTemplate_t152263049_StaticFields::get_offset_of_nextId_6(),
	XslTemplate_t152263049::get_offset_of_Id_7(),
	XslTemplate_t152263049::get_offset_of_style_8(),
	XslTemplate_t152263049::get_offset_of_stackSize_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1564 = { sizeof (XslDefaultNodeTemplate_t4014140780), -1, sizeof(XslDefaultNodeTemplate_t4014140780_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1564[2] = 
{
	XslDefaultNodeTemplate_t4014140780::get_offset_of_mode_10(),
	XslDefaultNodeTemplate_t4014140780_StaticFields::get_offset_of_instance_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1565 = { sizeof (XslEmptyTemplate_t1993620519), -1, sizeof(XslEmptyTemplate_t1993620519_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1565[1] = 
{
	XslEmptyTemplate_t1993620519_StaticFields::get_offset_of_instance_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1566 = { sizeof (XslDefaultTextTemplate_t2363894394), -1, sizeof(XslDefaultTextTemplate_t2363894394_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1566[1] = 
{
	XslDefaultTextTemplate_t2363894394_StaticFields::get_offset_of_instance_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1567 = { sizeof (XslTransformProcessor_t3405861191), -1, sizeof(XslTransformProcessor_t3405861191_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1567[20] = 
{
	XslTransformProcessor_t3405861191::get_offset_of_debugger_0(),
	XslTransformProcessor_t3405861191::get_offset_of_compiledStyle_1(),
	XslTransformProcessor_t3405861191::get_offset_of_style_2(),
	XslTransformProcessor_t3405861191::get_offset_of_currentTemplateStack_3(),
	XslTransformProcessor_t3405861191::get_offset_of_root_4(),
	XslTransformProcessor_t3405861191::get_offset_of_args_5(),
	XslTransformProcessor_t3405861191::get_offset_of_resolver_6(),
	XslTransformProcessor_t3405861191::get_offset_of_currentOutputUri_7(),
	XslTransformProcessor_t3405861191::get_offset_of_XPathContext_8(),
	XslTransformProcessor_t3405861191::get_offset_of_globalVariableTable_9(),
	XslTransformProcessor_t3405861191::get_offset_of_docCache_10(),
	XslTransformProcessor_t3405861191::get_offset_of_outputStack_11(),
	XslTransformProcessor_t3405861191::get_offset_of_avtSB_12(),
	XslTransformProcessor_t3405861191::get_offset_of_paramPassingCache_13(),
	XslTransformProcessor_t3405861191::get_offset_of_nodesetStack_14(),
	XslTransformProcessor_t3405861191::get_offset_of_variableStack_15(),
	XslTransformProcessor_t3405861191::get_offset_of_currentStack_16(),
	XslTransformProcessor_t3405861191::get_offset_of_busyTable_17(),
	XslTransformProcessor_t3405861191_StaticFields::get_offset_of_busyObject_18(),
	XslTransformProcessor_t3405861191_StaticFields::get_offset_of_U3CU3Ef__switchU24map24_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1568 = { sizeof (XsltCompiledContext_t2295645487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1568[6] = 
{
	XsltCompiledContext_t2295645487::get_offset_of_keyNameCache_9(),
	XsltCompiledContext_t2295645487::get_offset_of_keyIndexTables_10(),
	XsltCompiledContext_t2295645487::get_offset_of_patternNavCaches_11(),
	XsltCompiledContext_t2295645487::get_offset_of_p_12(),
	XsltCompiledContext_t2295645487::get_offset_of_scopes_13(),
	XsltCompiledContext_t2295645487::get_offset_of_scopeAt_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1569 = { sizeof (XsltContextInfo_t2905292101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1569[4] = 
{
	XsltContextInfo_t2905292101::get_offset_of_IsCData_0(),
	XsltContextInfo_t2905292101::get_offset_of_PreserveWhitespace_1(),
	XsltContextInfo_t2905292101::get_offset_of_ElementPrefix_2(),
	XsltContextInfo_t2905292101::get_offset_of_ElementNamespace_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1570 = { sizeof (XsltDebuggerWrapper_t4148725768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1570[3] = 
{
	XsltDebuggerWrapper_t4148725768::get_offset_of_on_compile_0(),
	XsltDebuggerWrapper_t4148725768::get_offset_of_on_execute_1(),
	XsltDebuggerWrapper_t4148725768::get_offset_of_impl_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1571 = { sizeof (XPFuncImpl_t459957616), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1571[4] = 
{
	XPFuncImpl_t459957616::get_offset_of_minargs_0(),
	XPFuncImpl_t459957616::get_offset_of_maxargs_1(),
	XPFuncImpl_t459957616::get_offset_of_returnType_2(),
	XPFuncImpl_t459957616::get_offset_of_argTypes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1572 = { sizeof (XsltExtensionFunction_t863890153), -1, sizeof(XsltExtensionFunction_t863890153_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1572[5] = 
{
	XsltExtensionFunction_t863890153::get_offset_of_extension_4(),
	XsltExtensionFunction_t863890153::get_offset_of_method_5(),
	XsltExtensionFunction_t863890153::get_offset_of_typeCodes_6(),
	XsltExtensionFunction_t863890153_StaticFields::get_offset_of_U3CU3Ef__switchU24map26_7(),
	XsltExtensionFunction_t863890153_StaticFields::get_offset_of_U3CU3Ef__switchU24map27_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1573 = { sizeof (XsltCurrent_t1577851673), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1574 = { sizeof (XsltDocument_t841609188), -1, sizeof(XsltDocument_t841609188_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1574[4] = 
{
	XsltDocument_t841609188::get_offset_of_arg0_0(),
	XsltDocument_t841609188::get_offset_of_arg1_1(),
	XsltDocument_t841609188::get_offset_of_doc_2(),
	XsltDocument_t841609188_StaticFields::get_offset_of_VoidBaseUriFlag_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1575 = { sizeof (XsltElementAvailable_t1740333384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1575[2] = 
{
	XsltElementAvailable_t1740333384::get_offset_of_arg0_0(),
	XsltElementAvailable_t1740333384::get_offset_of_ctx_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1576 = { sizeof (XsltFormatNumber_t539498555), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1576[4] = 
{
	XsltFormatNumber_t539498555::get_offset_of_arg0_0(),
	XsltFormatNumber_t539498555::get_offset_of_arg1_1(),
	XsltFormatNumber_t539498555::get_offset_of_arg2_2(),
	XsltFormatNumber_t539498555::get_offset_of_ctx_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1577 = { sizeof (XsltFunctionAvailable_t2598188313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1577[2] = 
{
	XsltFunctionAvailable_t2598188313::get_offset_of_arg0_0(),
	XsltFunctionAvailable_t2598188313::get_offset_of_ctx_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1578 = { sizeof (XsltGenerateId_t1800457284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1578[1] = 
{
	XsltGenerateId_t1800457284::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1579 = { sizeof (XsltKey_t826370162), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1579[3] = 
{
	XsltKey_t826370162::get_offset_of_arg0_0(),
	XsltKey_t826370162::get_offset_of_arg1_1(),
	XsltKey_t826370162::get_offset_of_staticContext_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1580 = { sizeof (XsltSystemProperty_t2954510946), -1, sizeof(XsltSystemProperty_t2954510946_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1580[3] = 
{
	XsltSystemProperty_t2954510946::get_offset_of_arg0_0(),
	XsltSystemProperty_t2954510946::get_offset_of_ctx_1(),
	XsltSystemProperty_t2954510946_StaticFields::get_offset_of_U3CU3Ef__switchU24map28_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1581 = { sizeof (XsltUnparsedEntityUri_t1001948404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1581[1] = 
{
	XsltUnparsedEntityUri_t1001948404::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1582 = { sizeof (MSXslNodeSet_t3928430336), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1582[1] = 
{
	MSXslNodeSet_t3928430336::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1583 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1584 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1585 = { sizeof (XmlFilterReader_t953863161), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1585[3] = 
{
	XmlFilterReader_t953863161::get_offset_of_reader_3(),
	XmlFilterReader_t953863161::get_offset_of_settings_4(),
	XmlFilterReader_t953863161::get_offset_of_lineInfo_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1586 = { sizeof (ConformanceLevel_t3899847875)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1586[4] = 
{
	ConformanceLevel_t3899847875::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1587 = { sizeof (DTDAutomataFactory_t2958275022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1587[3] = 
{
	DTDAutomataFactory_t2958275022::get_offset_of_root_0(),
	DTDAutomataFactory_t2958275022::get_offset_of_choiceTable_1(),
	DTDAutomataFactory_t2958275022::get_offset_of_sequenceTable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1588 = { sizeof (DTDAutomata_t781538777), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1588[1] = 
{
	DTDAutomata_t781538777::get_offset_of_root_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1589 = { sizeof (DTDElementAutomata_t1050190167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1589[1] = 
{
	DTDElementAutomata_t1050190167::get_offset_of_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1590 = { sizeof (DTDChoiceAutomata_t3129343723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1590[4] = 
{
	DTDChoiceAutomata_t3129343723::get_offset_of_left_1(),
	DTDChoiceAutomata_t3129343723::get_offset_of_right_2(),
	DTDChoiceAutomata_t3129343723::get_offset_of_hasComputedEmptiable_3(),
	DTDChoiceAutomata_t3129343723::get_offset_of_cachedEmptiable_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1591 = { sizeof (DTDSequenceAutomata_t2865847115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1591[4] = 
{
	DTDSequenceAutomata_t2865847115::get_offset_of_left_1(),
	DTDSequenceAutomata_t2865847115::get_offset_of_right_2(),
	DTDSequenceAutomata_t2865847115::get_offset_of_hasComputedEmptiable_3(),
	DTDSequenceAutomata_t2865847115::get_offset_of_cachedEmptiable_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1592 = { sizeof (DTDOneOrMoreAutomata_t1192707047), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1592[1] = 
{
	DTDOneOrMoreAutomata_t1192707047::get_offset_of_children_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1593 = { sizeof (DTDEmptyAutomata_t465590953), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1594 = { sizeof (DTDAnyAutomata_t3633486160), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1595 = { sizeof (DTDInvalidAutomata_t1406553220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1596 = { sizeof (DTDObjectModel_t1729680289), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1596[23] = 
{
	DTDObjectModel_t1729680289::get_offset_of_factory_0(),
	DTDObjectModel_t1729680289::get_offset_of_rootAutomata_1(),
	DTDObjectModel_t1729680289::get_offset_of_emptyAutomata_2(),
	DTDObjectModel_t1729680289::get_offset_of_anyAutomata_3(),
	DTDObjectModel_t1729680289::get_offset_of_invalidAutomata_4(),
	DTDObjectModel_t1729680289::get_offset_of_elementDecls_5(),
	DTDObjectModel_t1729680289::get_offset_of_attListDecls_6(),
	DTDObjectModel_t1729680289::get_offset_of_peDecls_7(),
	DTDObjectModel_t1729680289::get_offset_of_entityDecls_8(),
	DTDObjectModel_t1729680289::get_offset_of_notationDecls_9(),
	DTDObjectModel_t1729680289::get_offset_of_validationErrors_10(),
	DTDObjectModel_t1729680289::get_offset_of_resolver_11(),
	DTDObjectModel_t1729680289::get_offset_of_nameTable_12(),
	DTDObjectModel_t1729680289::get_offset_of_externalResources_13(),
	DTDObjectModel_t1729680289::get_offset_of_baseURI_14(),
	DTDObjectModel_t1729680289::get_offset_of_name_15(),
	DTDObjectModel_t1729680289::get_offset_of_publicId_16(),
	DTDObjectModel_t1729680289::get_offset_of_systemId_17(),
	DTDObjectModel_t1729680289::get_offset_of_intSubset_18(),
	DTDObjectModel_t1729680289::get_offset_of_intSubsetHasPERef_19(),
	DTDObjectModel_t1729680289::get_offset_of_isStandalone_20(),
	DTDObjectModel_t1729680289::get_offset_of_lineNumber_21(),
	DTDObjectModel_t1729680289::get_offset_of_linePosition_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1597 = { sizeof (DictionaryBase_t52754249), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1598 = { sizeof (U3CU3Ec__Iterator3_t2072931442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1598[5] = 
{
	U3CU3Ec__Iterator3_t2072931442::get_offset_of_U3CU24s_50U3E__0_0(),
	U3CU3Ec__Iterator3_t2072931442::get_offset_of_U3CpU3E__1_1(),
	U3CU3Ec__Iterator3_t2072931442::get_offset_of_U24PC_2(),
	U3CU3Ec__Iterator3_t2072931442::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator3_t2072931442::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1599 = { sizeof (DTDCollectionBase_t3926218464), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1599[1] = 
{
	DTDCollectionBase_t3926218464::get_offset_of_root_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
