﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_MulticastDelegate157516450.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22448405263.h"

// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2760654312;
// System.ServiceModel.Description.MessageHeaderDescription
struct MessageHeaderDescription_t75735776;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Transform`1<System.Xml.XmlQualifiedName,System.ServiceModel.Description.MessageHeaderDescription,System.Collections.Generic.KeyValuePair`2<System.Xml.XmlQualifiedName,System.ServiceModel.Description.MessageHeaderDescription>>
struct  Transform_1_t3549758381  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
