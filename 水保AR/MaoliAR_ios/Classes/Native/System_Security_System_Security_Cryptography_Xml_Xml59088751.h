﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Xml.XPath.XPathResultType[]
struct XPathResultTypeU5BU5D_t1515527577;
// System.Xml.XPath.XPathNodeIterator
struct XPathNodeIterator_t3667290188;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.Xml.XmlDsigXPathTransform/XmlDsigXPathFunctionHere
struct  XmlDsigXPathFunctionHere_t59088751  : public Il2CppObject
{
public:
	// System.Xml.XPath.XPathNodeIterator System.Security.Cryptography.Xml.XmlDsigXPathTransform/XmlDsigXPathFunctionHere::xpathNode
	XPathNodeIterator_t3667290188 * ___xpathNode_1;

public:
	inline static int32_t get_offset_of_xpathNode_1() { return static_cast<int32_t>(offsetof(XmlDsigXPathFunctionHere_t59088751, ___xpathNode_1)); }
	inline XPathNodeIterator_t3667290188 * get_xpathNode_1() const { return ___xpathNode_1; }
	inline XPathNodeIterator_t3667290188 ** get_address_of_xpathNode_1() { return &___xpathNode_1; }
	inline void set_xpathNode_1(XPathNodeIterator_t3667290188 * value)
	{
		___xpathNode_1 = value;
		Il2CppCodeGenWriteBarrier(&___xpathNode_1, value);
	}
};

struct XmlDsigXPathFunctionHere_t59088751_StaticFields
{
public:
	// System.Xml.XPath.XPathResultType[] System.Security.Cryptography.Xml.XmlDsigXPathTransform/XmlDsigXPathFunctionHere::types
	XPathResultTypeU5BU5D_t1515527577* ___types_0;

public:
	inline static int32_t get_offset_of_types_0() { return static_cast<int32_t>(offsetof(XmlDsigXPathFunctionHere_t59088751_StaticFields, ___types_0)); }
	inline XPathResultTypeU5BU5D_t1515527577* get_types_0() const { return ___types_0; }
	inline XPathResultTypeU5BU5D_t1515527577** get_address_of_types_0() { return &___types_0; }
	inline void set_types_0(XPathResultTypeU5BU5D_t1515527577* value)
	{
		___types_0 = value;
		Il2CppCodeGenWriteBarrier(&___types_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
