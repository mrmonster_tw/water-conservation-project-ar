﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.ScriptCompilerInfo
struct  ScriptCompilerInfo_t3765441597  : public Il2CppObject
{
public:
	// System.String Mono.Xml.Xsl.ScriptCompilerInfo::compilerCommand
	String_t* ___compilerCommand_0;
	// System.String Mono.Xml.Xsl.ScriptCompilerInfo::defaultCompilerOptions
	String_t* ___defaultCompilerOptions_1;

public:
	inline static int32_t get_offset_of_compilerCommand_0() { return static_cast<int32_t>(offsetof(ScriptCompilerInfo_t3765441597, ___compilerCommand_0)); }
	inline String_t* get_compilerCommand_0() const { return ___compilerCommand_0; }
	inline String_t** get_address_of_compilerCommand_0() { return &___compilerCommand_0; }
	inline void set_compilerCommand_0(String_t* value)
	{
		___compilerCommand_0 = value;
		Il2CppCodeGenWriteBarrier(&___compilerCommand_0, value);
	}

	inline static int32_t get_offset_of_defaultCompilerOptions_1() { return static_cast<int32_t>(offsetof(ScriptCompilerInfo_t3765441597, ___defaultCompilerOptions_1)); }
	inline String_t* get_defaultCompilerOptions_1() const { return ___defaultCompilerOptions_1; }
	inline String_t** get_address_of_defaultCompilerOptions_1() { return &___defaultCompilerOptions_1; }
	inline void set_defaultCompilerOptions_1(String_t* value)
	{
		___defaultCompilerOptions_1 = value;
		Il2CppCodeGenWriteBarrier(&___defaultCompilerOptions_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
