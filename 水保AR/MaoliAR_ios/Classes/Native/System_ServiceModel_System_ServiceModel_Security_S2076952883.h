﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Security_S1506236276.h"

// System.String
struct String_t;
// Mono.Security.Protocol.Ntlm.Type1Message
struct Type1Message_t2139513923;
// Mono.Security.Protocol.Ntlm.Type2Message
struct Type2Message_t2139513824;
// Mono.Security.Protocol.Ntlm.Type3Message
struct Type3Message_t2139513857;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.SspiServerSession
struct  SspiServerSession_t2076952883  : public SspiSession_t1506236276
{
public:
	// System.String System.ServiceModel.Security.SspiServerSession::TargetName
	String_t* ___TargetName_8;
	// Mono.Security.Protocol.Ntlm.Type1Message System.ServiceModel.Security.SspiServerSession::type1
	Type1Message_t2139513923 * ___type1_9;
	// Mono.Security.Protocol.Ntlm.Type2Message System.ServiceModel.Security.SspiServerSession::type2
	Type2Message_t2139513824 * ___type2_10;
	// Mono.Security.Protocol.Ntlm.Type3Message System.ServiceModel.Security.SspiServerSession::type3
	Type3Message_t2139513857 * ___type3_11;

public:
	inline static int32_t get_offset_of_TargetName_8() { return static_cast<int32_t>(offsetof(SspiServerSession_t2076952883, ___TargetName_8)); }
	inline String_t* get_TargetName_8() const { return ___TargetName_8; }
	inline String_t** get_address_of_TargetName_8() { return &___TargetName_8; }
	inline void set_TargetName_8(String_t* value)
	{
		___TargetName_8 = value;
		Il2CppCodeGenWriteBarrier(&___TargetName_8, value);
	}

	inline static int32_t get_offset_of_type1_9() { return static_cast<int32_t>(offsetof(SspiServerSession_t2076952883, ___type1_9)); }
	inline Type1Message_t2139513923 * get_type1_9() const { return ___type1_9; }
	inline Type1Message_t2139513923 ** get_address_of_type1_9() { return &___type1_9; }
	inline void set_type1_9(Type1Message_t2139513923 * value)
	{
		___type1_9 = value;
		Il2CppCodeGenWriteBarrier(&___type1_9, value);
	}

	inline static int32_t get_offset_of_type2_10() { return static_cast<int32_t>(offsetof(SspiServerSession_t2076952883, ___type2_10)); }
	inline Type2Message_t2139513824 * get_type2_10() const { return ___type2_10; }
	inline Type2Message_t2139513824 ** get_address_of_type2_10() { return &___type2_10; }
	inline void set_type2_10(Type2Message_t2139513824 * value)
	{
		___type2_10 = value;
		Il2CppCodeGenWriteBarrier(&___type2_10, value);
	}

	inline static int32_t get_offset_of_type3_11() { return static_cast<int32_t>(offsetof(SspiServerSession_t2076952883, ___type3_11)); }
	inline Type3Message_t2139513857 * get_type3_11() const { return ___type3_11; }
	inline Type3Message_t2139513857 ** get_address_of_type3_11() { return &___type3_11; }
	inline void set_type3_11(Type3Message_t2139513857 * value)
	{
		___type3_11 = value;
		Il2CppCodeGenWriteBarrier(&___type3_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
