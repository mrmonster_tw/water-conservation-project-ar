﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.OperationContext
struct OperationContext_t2829644788;
// System.ServiceModel.Channels.Message
struct Message_t869514973;
// System.ServiceModel.Dispatcher.EndpointDispatcher
struct EndpointDispatcher_t2708702820;
// System.ServiceModel.IContextChannel
struct IContextChannel_t910994827;
// System.ServiceModel.Channels.RequestContext
struct RequestContext_t1473296135;
// System.ServiceModel.Channels.MessageHeaders
struct MessageHeaders_t4050072634;
// System.ServiceModel.Channels.MessageProperties
struct MessageProperties_t4101341573;
// System.ServiceModel.InstanceContext
struct InstanceContext_t3593205954;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.OperationContext
struct  OperationContext_t2829644788  : public Il2CppObject
{
public:
	// System.ServiceModel.Channels.Message System.ServiceModel.OperationContext::incoming_message
	Message_t869514973 * ___incoming_message_1;
	// System.ServiceModel.Dispatcher.EndpointDispatcher System.ServiceModel.OperationContext::dispatcher
	EndpointDispatcher_t2708702820 * ___dispatcher_2;
	// System.ServiceModel.IContextChannel System.ServiceModel.OperationContext::channel
	Il2CppObject * ___channel_3;
	// System.ServiceModel.Channels.RequestContext System.ServiceModel.OperationContext::request_ctx
	RequestContext_t1473296135 * ___request_ctx_4;
	// System.ServiceModel.Channels.MessageHeaders System.ServiceModel.OperationContext::outgoing_headers
	MessageHeaders_t4050072634 * ___outgoing_headers_5;
	// System.ServiceModel.Channels.MessageProperties System.ServiceModel.OperationContext::outgoing_properties
	MessageProperties_t4101341573 * ___outgoing_properties_6;
	// System.ServiceModel.InstanceContext System.ServiceModel.OperationContext::instance_context
	InstanceContext_t3593205954 * ___instance_context_7;
	// System.Boolean System.ServiceModel.OperationContext::<IsUserContext>k__BackingField
	bool ___U3CIsUserContextU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_incoming_message_1() { return static_cast<int32_t>(offsetof(OperationContext_t2829644788, ___incoming_message_1)); }
	inline Message_t869514973 * get_incoming_message_1() const { return ___incoming_message_1; }
	inline Message_t869514973 ** get_address_of_incoming_message_1() { return &___incoming_message_1; }
	inline void set_incoming_message_1(Message_t869514973 * value)
	{
		___incoming_message_1 = value;
		Il2CppCodeGenWriteBarrier(&___incoming_message_1, value);
	}

	inline static int32_t get_offset_of_dispatcher_2() { return static_cast<int32_t>(offsetof(OperationContext_t2829644788, ___dispatcher_2)); }
	inline EndpointDispatcher_t2708702820 * get_dispatcher_2() const { return ___dispatcher_2; }
	inline EndpointDispatcher_t2708702820 ** get_address_of_dispatcher_2() { return &___dispatcher_2; }
	inline void set_dispatcher_2(EndpointDispatcher_t2708702820 * value)
	{
		___dispatcher_2 = value;
		Il2CppCodeGenWriteBarrier(&___dispatcher_2, value);
	}

	inline static int32_t get_offset_of_channel_3() { return static_cast<int32_t>(offsetof(OperationContext_t2829644788, ___channel_3)); }
	inline Il2CppObject * get_channel_3() const { return ___channel_3; }
	inline Il2CppObject ** get_address_of_channel_3() { return &___channel_3; }
	inline void set_channel_3(Il2CppObject * value)
	{
		___channel_3 = value;
		Il2CppCodeGenWriteBarrier(&___channel_3, value);
	}

	inline static int32_t get_offset_of_request_ctx_4() { return static_cast<int32_t>(offsetof(OperationContext_t2829644788, ___request_ctx_4)); }
	inline RequestContext_t1473296135 * get_request_ctx_4() const { return ___request_ctx_4; }
	inline RequestContext_t1473296135 ** get_address_of_request_ctx_4() { return &___request_ctx_4; }
	inline void set_request_ctx_4(RequestContext_t1473296135 * value)
	{
		___request_ctx_4 = value;
		Il2CppCodeGenWriteBarrier(&___request_ctx_4, value);
	}

	inline static int32_t get_offset_of_outgoing_headers_5() { return static_cast<int32_t>(offsetof(OperationContext_t2829644788, ___outgoing_headers_5)); }
	inline MessageHeaders_t4050072634 * get_outgoing_headers_5() const { return ___outgoing_headers_5; }
	inline MessageHeaders_t4050072634 ** get_address_of_outgoing_headers_5() { return &___outgoing_headers_5; }
	inline void set_outgoing_headers_5(MessageHeaders_t4050072634 * value)
	{
		___outgoing_headers_5 = value;
		Il2CppCodeGenWriteBarrier(&___outgoing_headers_5, value);
	}

	inline static int32_t get_offset_of_outgoing_properties_6() { return static_cast<int32_t>(offsetof(OperationContext_t2829644788, ___outgoing_properties_6)); }
	inline MessageProperties_t4101341573 * get_outgoing_properties_6() const { return ___outgoing_properties_6; }
	inline MessageProperties_t4101341573 ** get_address_of_outgoing_properties_6() { return &___outgoing_properties_6; }
	inline void set_outgoing_properties_6(MessageProperties_t4101341573 * value)
	{
		___outgoing_properties_6 = value;
		Il2CppCodeGenWriteBarrier(&___outgoing_properties_6, value);
	}

	inline static int32_t get_offset_of_instance_context_7() { return static_cast<int32_t>(offsetof(OperationContext_t2829644788, ___instance_context_7)); }
	inline InstanceContext_t3593205954 * get_instance_context_7() const { return ___instance_context_7; }
	inline InstanceContext_t3593205954 ** get_address_of_instance_context_7() { return &___instance_context_7; }
	inline void set_instance_context_7(InstanceContext_t3593205954 * value)
	{
		___instance_context_7 = value;
		Il2CppCodeGenWriteBarrier(&___instance_context_7, value);
	}

	inline static int32_t get_offset_of_U3CIsUserContextU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(OperationContext_t2829644788, ___U3CIsUserContextU3Ek__BackingField_8)); }
	inline bool get_U3CIsUserContextU3Ek__BackingField_8() const { return ___U3CIsUserContextU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIsUserContextU3Ek__BackingField_8() { return &___U3CIsUserContextU3Ek__BackingField_8; }
	inline void set_U3CIsUserContextU3Ek__BackingField_8(bool value)
	{
		___U3CIsUserContextU3Ek__BackingField_8 = value;
	}
};

struct OperationContext_t2829644788_ThreadStaticFields
{
public:
	// System.ServiceModel.OperationContext System.ServiceModel.OperationContext::current
	OperationContext_t2829644788 * ___current_0;

public:
	inline static int32_t get_offset_of_current_0() { return static_cast<int32_t>(offsetof(OperationContext_t2829644788_ThreadStaticFields, ___current_0)); }
	inline OperationContext_t2829644788 * get_current_0() const { return ___current_0; }
	inline OperationContext_t2829644788 ** get_address_of_current_0() { return &___current_0; }
	inline void set_current_0(OperationContext_t2829644788 * value)
	{
		___current_0 = value;
		Il2CppCodeGenWriteBarrier(&___current_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
