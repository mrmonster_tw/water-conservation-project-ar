﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Cha19627360.h"

// System.ServiceModel.Channels.IChannelFactory`1<System.Object>
struct IChannelFactory_1_t139447917;
// System.ServiceModel.Channels.InitiatorMessageSecurityBindingSupport
struct InitiatorMessageSecurityBindingSupport_t3267128365;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.SecurityChannelFactory`1<System.Object>
struct  SecurityChannelFactory_1_t2771041013  : public ChannelFactoryBase_1_t19627360
{
public:
	// System.ServiceModel.Channels.IChannelFactory`1<TChannel> System.ServiceModel.Channels.SecurityChannelFactory`1::inner
	Il2CppObject* ___inner_14;
	// System.ServiceModel.Channels.InitiatorMessageSecurityBindingSupport System.ServiceModel.Channels.SecurityChannelFactory`1::security
	InitiatorMessageSecurityBindingSupport_t3267128365 * ___security_15;

public:
	inline static int32_t get_offset_of_inner_14() { return static_cast<int32_t>(offsetof(SecurityChannelFactory_1_t2771041013, ___inner_14)); }
	inline Il2CppObject* get_inner_14() const { return ___inner_14; }
	inline Il2CppObject** get_address_of_inner_14() { return &___inner_14; }
	inline void set_inner_14(Il2CppObject* value)
	{
		___inner_14 = value;
		Il2CppCodeGenWriteBarrier(&___inner_14, value);
	}

	inline static int32_t get_offset_of_security_15() { return static_cast<int32_t>(offsetof(SecurityChannelFactory_1_t2771041013, ___security_15)); }
	inline InitiatorMessageSecurityBindingSupport_t3267128365 * get_security_15() const { return ___security_15; }
	inline InitiatorMessageSecurityBindingSupport_t3267128365 ** get_address_of_security_15() { return &___security_15; }
	inline void set_security_15(InitiatorMessageSecurityBindingSupport_t3267128365 * value)
	{
		___security_15 = value;
		Il2CppCodeGenWriteBarrier(&___security_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
