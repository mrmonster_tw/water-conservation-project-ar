﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Runtime_Serialization_System_Xml_XmlDiction1958650860.h"

// System.Xml.XmlWriter
struct XmlWriter_t127905479;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlSimpleDictionaryWriter
struct  XmlSimpleDictionaryWriter_t1777131310  : public XmlDictionaryWriter_t1958650860
{
public:
	// System.Xml.XmlWriter System.Xml.XmlSimpleDictionaryWriter::writer
	XmlWriter_t127905479 * ___writer_3;

public:
	inline static int32_t get_offset_of_writer_3() { return static_cast<int32_t>(offsetof(XmlSimpleDictionaryWriter_t1777131310, ___writer_3)); }
	inline XmlWriter_t127905479 * get_writer_3() const { return ___writer_3; }
	inline XmlWriter_t127905479 ** get_address_of_writer_3() { return &___writer_3; }
	inline void set_writer_3(XmlWriter_t127905479 * value)
	{
		___writer_3 = value;
		Il2CppCodeGenWriteBarrier(&___writer_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
