﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Drawing.GdiColorPalette
struct  GdiColorPalette_t993082498 
{
public:
	// System.Int32 System.Drawing.GdiColorPalette::Flags
	int32_t ___Flags_0;
	// System.Int32 System.Drawing.GdiColorPalette::Count
	int32_t ___Count_1;

public:
	inline static int32_t get_offset_of_Flags_0() { return static_cast<int32_t>(offsetof(GdiColorPalette_t993082498, ___Flags_0)); }
	inline int32_t get_Flags_0() const { return ___Flags_0; }
	inline int32_t* get_address_of_Flags_0() { return &___Flags_0; }
	inline void set_Flags_0(int32_t value)
	{
		___Flags_0 = value;
	}

	inline static int32_t get_offset_of_Count_1() { return static_cast<int32_t>(offsetof(GdiColorPalette_t993082498, ___Count_1)); }
	inline int32_t get_Count_1() const { return ___Count_1; }
	inline int32_t* get_address_of_Count_1() { return &___Count_1; }
	inline void set_Count_1(int32_t value)
	{
		___Count_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
