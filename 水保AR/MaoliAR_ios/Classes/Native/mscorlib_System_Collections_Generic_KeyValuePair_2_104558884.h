﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"

// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2760654312;
// System.ServiceModel.Description.MessagePartDescription
struct MessagePartDescription_t2026856693;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Xml.XmlQualifiedName,System.ServiceModel.Description.MessagePartDescription>
struct  KeyValuePair_2_t104558884 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	XmlQualifiedName_t2760654312 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	MessagePartDescription_t2026856693 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t104558884, ___key_0)); }
	inline XmlQualifiedName_t2760654312 * get_key_0() const { return ___key_0; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(XmlQualifiedName_t2760654312 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier(&___key_0, value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t104558884, ___value_1)); }
	inline MessagePartDescription_t2026856693 * get_value_1() const { return ___value_1; }
	inline MessagePartDescription_t2026856693 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(MessagePartDescription_t2026856693 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
