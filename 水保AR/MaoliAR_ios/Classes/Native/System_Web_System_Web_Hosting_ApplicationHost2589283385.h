﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Hosting.ApplicationHost
struct  ApplicationHost_t2589283385  : public Il2CppObject
{
public:

public:
};

struct ApplicationHost_t2589283385_StaticFields
{
public:
	// System.String System.Web.Hosting.ApplicationHost::MonoHostedDataKey
	String_t* ___MonoHostedDataKey_0;
	// System.String[] System.Web.Hosting.ApplicationHost::WebConfigFileNames
	StringU5BU5D_t1281789340* ___WebConfigFileNames_1;
	// System.Object System.Web.Hosting.ApplicationHost::create_dir
	Il2CppObject * ___create_dir_2;

public:
	inline static int32_t get_offset_of_MonoHostedDataKey_0() { return static_cast<int32_t>(offsetof(ApplicationHost_t2589283385_StaticFields, ___MonoHostedDataKey_0)); }
	inline String_t* get_MonoHostedDataKey_0() const { return ___MonoHostedDataKey_0; }
	inline String_t** get_address_of_MonoHostedDataKey_0() { return &___MonoHostedDataKey_0; }
	inline void set_MonoHostedDataKey_0(String_t* value)
	{
		___MonoHostedDataKey_0 = value;
		Il2CppCodeGenWriteBarrier(&___MonoHostedDataKey_0, value);
	}

	inline static int32_t get_offset_of_WebConfigFileNames_1() { return static_cast<int32_t>(offsetof(ApplicationHost_t2589283385_StaticFields, ___WebConfigFileNames_1)); }
	inline StringU5BU5D_t1281789340* get_WebConfigFileNames_1() const { return ___WebConfigFileNames_1; }
	inline StringU5BU5D_t1281789340** get_address_of_WebConfigFileNames_1() { return &___WebConfigFileNames_1; }
	inline void set_WebConfigFileNames_1(StringU5BU5D_t1281789340* value)
	{
		___WebConfigFileNames_1 = value;
		Il2CppCodeGenWriteBarrier(&___WebConfigFileNames_1, value);
	}

	inline static int32_t get_offset_of_create_dir_2() { return static_cast<int32_t>(offsetof(ApplicationHost_t2589283385_StaticFields, ___create_dir_2)); }
	inline Il2CppObject * get_create_dir_2() const { return ___create_dir_2; }
	inline Il2CppObject ** get_address_of_create_dir_2() { return &___create_dir_2; }
	inline void set_create_dir_2(Il2CppObject * value)
	{
		___create_dir_2 = value;
		Il2CppCodeGenWriteBarrier(&___create_dir_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
