﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslNumber/XslNumberFormatter/NumberFormatterScanner
struct  NumberFormatterScanner_t2819324807  : public Il2CppObject
{
public:
	// System.Int32 Mono.Xml.Xsl.Operations.XslNumber/XslNumberFormatter/NumberFormatterScanner::pos
	int32_t ___pos_0;
	// System.Int32 Mono.Xml.Xsl.Operations.XslNumber/XslNumberFormatter/NumberFormatterScanner::len
	int32_t ___len_1;
	// System.String Mono.Xml.Xsl.Operations.XslNumber/XslNumberFormatter/NumberFormatterScanner::fmt
	String_t* ___fmt_2;

public:
	inline static int32_t get_offset_of_pos_0() { return static_cast<int32_t>(offsetof(NumberFormatterScanner_t2819324807, ___pos_0)); }
	inline int32_t get_pos_0() const { return ___pos_0; }
	inline int32_t* get_address_of_pos_0() { return &___pos_0; }
	inline void set_pos_0(int32_t value)
	{
		___pos_0 = value;
	}

	inline static int32_t get_offset_of_len_1() { return static_cast<int32_t>(offsetof(NumberFormatterScanner_t2819324807, ___len_1)); }
	inline int32_t get_len_1() const { return ___len_1; }
	inline int32_t* get_address_of_len_1() { return &___len_1; }
	inline void set_len_1(int32_t value)
	{
		___len_1 = value;
	}

	inline static int32_t get_offset_of_fmt_2() { return static_cast<int32_t>(offsetof(NumberFormatterScanner_t2819324807, ___fmt_2)); }
	inline String_t* get_fmt_2() const { return ___fmt_2; }
	inline String_t** get_address_of_fmt_2() { return &___fmt_2; }
	inline void set_fmt_2(String_t* value)
	{
		___fmt_2 = value;
		Il2CppCodeGenWriteBarrier(&___fmt_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
