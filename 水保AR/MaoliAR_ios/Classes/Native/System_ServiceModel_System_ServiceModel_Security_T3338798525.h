﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Security_T2685676710.h"

// Mono.Security.Protocol.Tls.SslServerStream
struct SslServerStream_t875102505;
// System.IO.MemoryStream
struct MemoryStream_t94973147;
// Mono.Security.Protocol.Tls.CertificateValidationCallback
struct CertificateValidationCallback_t4091668219;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.TlsServerSession
struct  TlsServerSession_t3338798525  : public TlsSession_t2685676710
{
public:
	// Mono.Security.Protocol.Tls.SslServerStream System.ServiceModel.Security.Tokens.TlsServerSession::ssl
	SslServerStream_t875102505 * ___ssl_0;
	// System.IO.MemoryStream System.ServiceModel.Security.Tokens.TlsServerSession::stream
	MemoryStream_t94973147 * ___stream_1;
	// System.Boolean System.ServiceModel.Security.Tokens.TlsServerSession::mutual
	bool ___mutual_2;

public:
	inline static int32_t get_offset_of_ssl_0() { return static_cast<int32_t>(offsetof(TlsServerSession_t3338798525, ___ssl_0)); }
	inline SslServerStream_t875102505 * get_ssl_0() const { return ___ssl_0; }
	inline SslServerStream_t875102505 ** get_address_of_ssl_0() { return &___ssl_0; }
	inline void set_ssl_0(SslServerStream_t875102505 * value)
	{
		___ssl_0 = value;
		Il2CppCodeGenWriteBarrier(&___ssl_0, value);
	}

	inline static int32_t get_offset_of_stream_1() { return static_cast<int32_t>(offsetof(TlsServerSession_t3338798525, ___stream_1)); }
	inline MemoryStream_t94973147 * get_stream_1() const { return ___stream_1; }
	inline MemoryStream_t94973147 ** get_address_of_stream_1() { return &___stream_1; }
	inline void set_stream_1(MemoryStream_t94973147 * value)
	{
		___stream_1 = value;
		Il2CppCodeGenWriteBarrier(&___stream_1, value);
	}

	inline static int32_t get_offset_of_mutual_2() { return static_cast<int32_t>(offsetof(TlsServerSession_t3338798525, ___mutual_2)); }
	inline bool get_mutual_2() const { return ___mutual_2; }
	inline bool* get_address_of_mutual_2() { return &___mutual_2; }
	inline void set_mutual_2(bool value)
	{
		___mutual_2 = value;
	}
};

struct TlsServerSession_t3338798525_StaticFields
{
public:
	// Mono.Security.Protocol.Tls.CertificateValidationCallback System.ServiceModel.Security.Tokens.TlsServerSession::<>f__am$cache3
	CertificateValidationCallback_t4091668219 * ___U3CU3Ef__amU24cache3_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(TlsServerSession_t3338798525_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline CertificateValidationCallback_t4091668219 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline CertificateValidationCallback_t4091668219 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(CertificateValidationCallback_t4091668219 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
