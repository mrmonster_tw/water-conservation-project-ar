﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_F1517933559.h"

// System.ServiceModel.Channels.MessageVersion
struct MessageVersion_t1958933598;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.FaultConverter/SimpleFaultConverter
struct  SimpleFaultConverter_t768171908  : public FaultConverter_t1517933559
{
public:
	// System.ServiceModel.Channels.MessageVersion System.ServiceModel.Channels.FaultConverter/SimpleFaultConverter::version
	MessageVersion_t1958933598 * ___version_0;

public:
	inline static int32_t get_offset_of_version_0() { return static_cast<int32_t>(offsetof(SimpleFaultConverter_t768171908, ___version_0)); }
	inline MessageVersion_t1958933598 * get_version_0() const { return ___version_0; }
	inline MessageVersion_t1958933598 ** get_address_of_version_0() { return &___version_0; }
	inline void set_version_0(MessageVersion_t1958933598 * value)
	{
		___version_0 = value;
		Il2CppCodeGenWriteBarrier(&___version_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
