﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_Compilation_BuildProvider3736381005.h"

// System.Web.Compilation.CompilerType
struct CompilerType_t2533482247;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.WsdlBuildProvider
struct  WsdlBuildProvider_t589850398  : public BuildProvider_t3736381005
{
public:
	// System.Web.Compilation.CompilerType System.Web.Compilation.WsdlBuildProvider::_compilerType
	CompilerType_t2533482247 * ____compilerType_3;

public:
	inline static int32_t get_offset_of__compilerType_3() { return static_cast<int32_t>(offsetof(WsdlBuildProvider_t589850398, ____compilerType_3)); }
	inline CompilerType_t2533482247 * get__compilerType_3() const { return ____compilerType_3; }
	inline CompilerType_t2533482247 ** get_address_of__compilerType_3() { return &____compilerType_3; }
	inline void set__compilerType_3(CompilerType_t2533482247 * value)
	{
		____compilerType_3 = value;
		Il2CppCodeGenWriteBarrier(&____compilerType_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
