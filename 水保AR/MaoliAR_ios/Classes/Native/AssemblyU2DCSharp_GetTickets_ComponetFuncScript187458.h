﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"

// ExpiredComponets
struct ExpiredComponets_t4043685999;
// QuestionedComponets
struct QuestionedComponets_t1753441808;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetTickets/ComponetFuncScript
struct  ComponetFuncScript_t187458 
{
public:
	// ExpiredComponets GetTickets/ComponetFuncScript::expiredComponets
	ExpiredComponets_t4043685999 * ___expiredComponets_0;
	// QuestionedComponets GetTickets/ComponetFuncScript::questionedComponets
	QuestionedComponets_t1753441808 * ___questionedComponets_1;

public:
	inline static int32_t get_offset_of_expiredComponets_0() { return static_cast<int32_t>(offsetof(ComponetFuncScript_t187458, ___expiredComponets_0)); }
	inline ExpiredComponets_t4043685999 * get_expiredComponets_0() const { return ___expiredComponets_0; }
	inline ExpiredComponets_t4043685999 ** get_address_of_expiredComponets_0() { return &___expiredComponets_0; }
	inline void set_expiredComponets_0(ExpiredComponets_t4043685999 * value)
	{
		___expiredComponets_0 = value;
		Il2CppCodeGenWriteBarrier(&___expiredComponets_0, value);
	}

	inline static int32_t get_offset_of_questionedComponets_1() { return static_cast<int32_t>(offsetof(ComponetFuncScript_t187458, ___questionedComponets_1)); }
	inline QuestionedComponets_t1753441808 * get_questionedComponets_1() const { return ___questionedComponets_1; }
	inline QuestionedComponets_t1753441808 ** get_address_of_questionedComponets_1() { return &___questionedComponets_1; }
	inline void set_questionedComponets_1(QuestionedComponets_t1753441808 * value)
	{
		___questionedComponets_1 = value;
		Il2CppCodeGenWriteBarrier(&___questionedComponets_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GetTickets/ComponetFuncScript
struct ComponetFuncScript_t187458_marshaled_pinvoke
{
	ExpiredComponets_t4043685999 * ___expiredComponets_0;
	QuestionedComponets_t1753441808 * ___questionedComponets_1;
};
// Native definition for COM marshalling of GetTickets/ComponetFuncScript
struct ComponetFuncScript_t187458_marshaled_com
{
	ExpiredComponets_t4043685999 * ___expiredComponets_0;
	QuestionedComponets_t1753441808 * ___questionedComponets_1;
};
