﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.ObjectModel.Collection`1<System.ServiceModel.Description.MetadataSection>
struct Collection_1_t3011753773;
// System.Collections.ObjectModel.Collection`1<System.Xml.XmlAttribute>
struct Collection_1_t118208177;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.MetadataSet
struct  MetadataSet_t160646782  : public Il2CppObject
{
public:
	// System.Collections.ObjectModel.Collection`1<System.ServiceModel.Description.MetadataSection> System.ServiceModel.Description.MetadataSet::sections
	Collection_1_t3011753773 * ___sections_0;
	// System.Collections.ObjectModel.Collection`1<System.Xml.XmlAttribute> System.ServiceModel.Description.MetadataSet::attributes
	Collection_1_t118208177 * ___attributes_1;

public:
	inline static int32_t get_offset_of_sections_0() { return static_cast<int32_t>(offsetof(MetadataSet_t160646782, ___sections_0)); }
	inline Collection_1_t3011753773 * get_sections_0() const { return ___sections_0; }
	inline Collection_1_t3011753773 ** get_address_of_sections_0() { return &___sections_0; }
	inline void set_sections_0(Collection_1_t3011753773 * value)
	{
		___sections_0 = value;
		Il2CppCodeGenWriteBarrier(&___sections_0, value);
	}

	inline static int32_t get_offset_of_attributes_1() { return static_cast<int32_t>(offsetof(MetadataSet_t160646782, ___attributes_1)); }
	inline Collection_1_t118208177 * get_attributes_1() const { return ___attributes_1; }
	inline Collection_1_t118208177 ** get_address_of_attributes_1() { return &___attributes_1; }
	inline void set_attributes_1(Collection_1_t118208177 * value)
	{
		___attributes_1 = value;
		Il2CppCodeGenWriteBarrier(&___attributes_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
