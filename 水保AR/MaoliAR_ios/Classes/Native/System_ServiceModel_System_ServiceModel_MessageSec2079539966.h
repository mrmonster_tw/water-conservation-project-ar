﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.MessageSecurityVersion
struct MessageSecurityVersion_t2079539966;
// System.ServiceModel.Security.SecureConversationVersion
struct SecureConversationVersion_t2971294059;
// System.ServiceModel.Security.TrustVersion
struct TrustVersion_t3017348835;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.MessageSecurityVersion
struct  MessageSecurityVersion_t2079539966  : public Il2CppObject
{
public:
	// System.ServiceModel.Security.SecureConversationVersion System.ServiceModel.MessageSecurityVersion::<SecureConversationVersion>k__BackingField
	SecureConversationVersion_t2971294059 * ___U3CSecureConversationVersionU3Ek__BackingField_6;
	// System.ServiceModel.Security.TrustVersion System.ServiceModel.MessageSecurityVersion::<TrustVersion>k__BackingField
	TrustVersion_t3017348835 * ___U3CTrustVersionU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CSecureConversationVersionU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(MessageSecurityVersion_t2079539966, ___U3CSecureConversationVersionU3Ek__BackingField_6)); }
	inline SecureConversationVersion_t2971294059 * get_U3CSecureConversationVersionU3Ek__BackingField_6() const { return ___U3CSecureConversationVersionU3Ek__BackingField_6; }
	inline SecureConversationVersion_t2971294059 ** get_address_of_U3CSecureConversationVersionU3Ek__BackingField_6() { return &___U3CSecureConversationVersionU3Ek__BackingField_6; }
	inline void set_U3CSecureConversationVersionU3Ek__BackingField_6(SecureConversationVersion_t2971294059 * value)
	{
		___U3CSecureConversationVersionU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSecureConversationVersionU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CTrustVersionU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(MessageSecurityVersion_t2079539966, ___U3CTrustVersionU3Ek__BackingField_7)); }
	inline TrustVersion_t3017348835 * get_U3CTrustVersionU3Ek__BackingField_7() const { return ___U3CTrustVersionU3Ek__BackingField_7; }
	inline TrustVersion_t3017348835 ** get_address_of_U3CTrustVersionU3Ek__BackingField_7() { return &___U3CTrustVersionU3Ek__BackingField_7; }
	inline void set_U3CTrustVersionU3Ek__BackingField_7(TrustVersion_t3017348835 * value)
	{
		___U3CTrustVersionU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTrustVersionU3Ek__BackingField_7, value);
	}
};

struct MessageSecurityVersion_t2079539966_StaticFields
{
public:
	// System.ServiceModel.MessageSecurityVersion System.ServiceModel.MessageSecurityVersion::wss10_basic
	MessageSecurityVersion_t2079539966 * ___wss10_basic_0;
	// System.ServiceModel.MessageSecurityVersion System.ServiceModel.MessageSecurityVersion::wss11
	MessageSecurityVersion_t2079539966 * ___wss11_1;
	// System.ServiceModel.MessageSecurityVersion System.ServiceModel.MessageSecurityVersion::wss11_basic
	MessageSecurityVersion_t2079539966 * ___wss11_basic_2;
	// System.ServiceModel.MessageSecurityVersion System.ServiceModel.MessageSecurityVersion::wss10_2007_basic
	MessageSecurityVersion_t2079539966 * ___wss10_2007_basic_3;
	// System.ServiceModel.MessageSecurityVersion System.ServiceModel.MessageSecurityVersion::wss11_2007_basic
	MessageSecurityVersion_t2079539966 * ___wss11_2007_basic_4;
	// System.ServiceModel.MessageSecurityVersion System.ServiceModel.MessageSecurityVersion::wss11_2007
	MessageSecurityVersion_t2079539966 * ___wss11_2007_5;

public:
	inline static int32_t get_offset_of_wss10_basic_0() { return static_cast<int32_t>(offsetof(MessageSecurityVersion_t2079539966_StaticFields, ___wss10_basic_0)); }
	inline MessageSecurityVersion_t2079539966 * get_wss10_basic_0() const { return ___wss10_basic_0; }
	inline MessageSecurityVersion_t2079539966 ** get_address_of_wss10_basic_0() { return &___wss10_basic_0; }
	inline void set_wss10_basic_0(MessageSecurityVersion_t2079539966 * value)
	{
		___wss10_basic_0 = value;
		Il2CppCodeGenWriteBarrier(&___wss10_basic_0, value);
	}

	inline static int32_t get_offset_of_wss11_1() { return static_cast<int32_t>(offsetof(MessageSecurityVersion_t2079539966_StaticFields, ___wss11_1)); }
	inline MessageSecurityVersion_t2079539966 * get_wss11_1() const { return ___wss11_1; }
	inline MessageSecurityVersion_t2079539966 ** get_address_of_wss11_1() { return &___wss11_1; }
	inline void set_wss11_1(MessageSecurityVersion_t2079539966 * value)
	{
		___wss11_1 = value;
		Il2CppCodeGenWriteBarrier(&___wss11_1, value);
	}

	inline static int32_t get_offset_of_wss11_basic_2() { return static_cast<int32_t>(offsetof(MessageSecurityVersion_t2079539966_StaticFields, ___wss11_basic_2)); }
	inline MessageSecurityVersion_t2079539966 * get_wss11_basic_2() const { return ___wss11_basic_2; }
	inline MessageSecurityVersion_t2079539966 ** get_address_of_wss11_basic_2() { return &___wss11_basic_2; }
	inline void set_wss11_basic_2(MessageSecurityVersion_t2079539966 * value)
	{
		___wss11_basic_2 = value;
		Il2CppCodeGenWriteBarrier(&___wss11_basic_2, value);
	}

	inline static int32_t get_offset_of_wss10_2007_basic_3() { return static_cast<int32_t>(offsetof(MessageSecurityVersion_t2079539966_StaticFields, ___wss10_2007_basic_3)); }
	inline MessageSecurityVersion_t2079539966 * get_wss10_2007_basic_3() const { return ___wss10_2007_basic_3; }
	inline MessageSecurityVersion_t2079539966 ** get_address_of_wss10_2007_basic_3() { return &___wss10_2007_basic_3; }
	inline void set_wss10_2007_basic_3(MessageSecurityVersion_t2079539966 * value)
	{
		___wss10_2007_basic_3 = value;
		Il2CppCodeGenWriteBarrier(&___wss10_2007_basic_3, value);
	}

	inline static int32_t get_offset_of_wss11_2007_basic_4() { return static_cast<int32_t>(offsetof(MessageSecurityVersion_t2079539966_StaticFields, ___wss11_2007_basic_4)); }
	inline MessageSecurityVersion_t2079539966 * get_wss11_2007_basic_4() const { return ___wss11_2007_basic_4; }
	inline MessageSecurityVersion_t2079539966 ** get_address_of_wss11_2007_basic_4() { return &___wss11_2007_basic_4; }
	inline void set_wss11_2007_basic_4(MessageSecurityVersion_t2079539966 * value)
	{
		___wss11_2007_basic_4 = value;
		Il2CppCodeGenWriteBarrier(&___wss11_2007_basic_4, value);
	}

	inline static int32_t get_offset_of_wss11_2007_5() { return static_cast<int32_t>(offsetof(MessageSecurityVersion_t2079539966_StaticFields, ___wss11_2007_5)); }
	inline MessageSecurityVersion_t2079539966 * get_wss11_2007_5() const { return ___wss11_2007_5; }
	inline MessageSecurityVersion_t2079539966 ** get_address_of_wss11_2007_5() { return &___wss11_2007_5; }
	inline void set_wss11_2007_5(MessageSecurityVersion_t2079539966 * value)
	{
		___wss11_2007_5 = value;
		Il2CppCodeGenWriteBarrier(&___wss11_2007_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
