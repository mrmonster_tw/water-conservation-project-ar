﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// GetTickets/<checkSceneMode>c__Iterator0
struct U3CcheckSceneModeU3Ec__Iterator0_t2929203735;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetTickets/<checkSceneMode>c__Iterator0/<checkSceneMode>c__AnonStorey6
struct  U3CcheckSceneModeU3Ec__AnonStorey6_t1932449374  : public Il2CppObject
{
public:
	// System.Int32 GetTickets/<checkSceneMode>c__Iterator0/<checkSceneMode>c__AnonStorey6::nowHour
	int32_t ___nowHour_0;
	// System.Int32 GetTickets/<checkSceneMode>c__Iterator0/<checkSceneMode>c__AnonStorey6::nowMinute
	int32_t ___nowMinute_1;
	// GetTickets/<checkSceneMode>c__Iterator0 GetTickets/<checkSceneMode>c__Iterator0/<checkSceneMode>c__AnonStorey6::<>f__ref$0
	U3CcheckSceneModeU3Ec__Iterator0_t2929203735 * ___U3CU3Ef__refU240_2;

public:
	inline static int32_t get_offset_of_nowHour_0() { return static_cast<int32_t>(offsetof(U3CcheckSceneModeU3Ec__AnonStorey6_t1932449374, ___nowHour_0)); }
	inline int32_t get_nowHour_0() const { return ___nowHour_0; }
	inline int32_t* get_address_of_nowHour_0() { return &___nowHour_0; }
	inline void set_nowHour_0(int32_t value)
	{
		___nowHour_0 = value;
	}

	inline static int32_t get_offset_of_nowMinute_1() { return static_cast<int32_t>(offsetof(U3CcheckSceneModeU3Ec__AnonStorey6_t1932449374, ___nowMinute_1)); }
	inline int32_t get_nowMinute_1() const { return ___nowMinute_1; }
	inline int32_t* get_address_of_nowMinute_1() { return &___nowMinute_1; }
	inline void set_nowMinute_1(int32_t value)
	{
		___nowMinute_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_2() { return static_cast<int32_t>(offsetof(U3CcheckSceneModeU3Ec__AnonStorey6_t1932449374, ___U3CU3Ef__refU240_2)); }
	inline U3CcheckSceneModeU3Ec__Iterator0_t2929203735 * get_U3CU3Ef__refU240_2() const { return ___U3CU3Ef__refU240_2; }
	inline U3CcheckSceneModeU3Ec__Iterator0_t2929203735 ** get_address_of_U3CU3Ef__refU240_2() { return &___U3CU3Ef__refU240_2; }
	inline void set_U3CU3Ef__refU240_2(U3CcheckSceneModeU3Ec__Iterator0_t2929203735 * value)
	{
		___U3CU3Ef__refU240_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU240_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
