﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.PeerResolvers.WelcomeInfoDC
struct WelcomeInfoDC_t2704353376;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.PeerResolvers.WelcomeInfo
struct  WelcomeInfo_t1049730577  : public Il2CppObject
{
public:
	// System.ServiceModel.PeerResolvers.WelcomeInfoDC System.ServiceModel.PeerResolvers.WelcomeInfo::dc
	WelcomeInfoDC_t2704353376 * ___dc_0;

public:
	inline static int32_t get_offset_of_dc_0() { return static_cast<int32_t>(offsetof(WelcomeInfo_t1049730577, ___dc_0)); }
	inline WelcomeInfoDC_t2704353376 * get_dc_0() const { return ___dc_0; }
	inline WelcomeInfoDC_t2704353376 ** get_address_of_dc_0() { return &___dc_0; }
	inline void set_dc_0(WelcomeInfoDC_t2704353376 * value)
	{
		___dc_0 = value;
		Il2CppCodeGenWriteBarrier(&___dc_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
