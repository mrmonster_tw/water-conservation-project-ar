﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Object
struct Il2CppObject;
// System.Security.Cryptography.MD5
struct MD5_t3177620429;
// System.Web.Compilation.AspTokenizer
struct AspTokenizer_t992095833;
// System.String
struct String_t;
// System.IO.StringReader
struct StringReader_t3465604688;
// System.Web.Compilation.AspParser
struct AspParser_t4001025751;
// System.ComponentModel.EventHandlerList
struct EventHandlerList_t1108123056;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.AspParser
struct  AspParser_t4001025751  : public Il2CppObject
{
public:
	// System.Security.Cryptography.MD5 System.Web.Compilation.AspParser::checksum
	MD5_t3177620429 * ___checksum_4;
	// System.Web.Compilation.AspTokenizer System.Web.Compilation.AspParser::tokenizer
	AspTokenizer_t992095833 * ___tokenizer_5;
	// System.Int32 System.Web.Compilation.AspParser::beginLine
	int32_t ___beginLine_6;
	// System.Int32 System.Web.Compilation.AspParser::endLine
	int32_t ___endLine_7;
	// System.Int32 System.Web.Compilation.AspParser::beginColumn
	int32_t ___beginColumn_8;
	// System.Int32 System.Web.Compilation.AspParser::endColumn
	int32_t ___endColumn_9;
	// System.Int32 System.Web.Compilation.AspParser::beginPosition
	int32_t ___beginPosition_10;
	// System.Int32 System.Web.Compilation.AspParser::endPosition
	int32_t ___endPosition_11;
	// System.String System.Web.Compilation.AspParser::filename
	String_t* ___filename_12;
	// System.String System.Web.Compilation.AspParser::verbatimID
	String_t* ___verbatimID_13;
	// System.String System.Web.Compilation.AspParser::fileText
	String_t* ___fileText_14;
	// System.IO.StringReader System.Web.Compilation.AspParser::fileReader
	StringReader_t3465604688 * ___fileReader_15;
	// System.Boolean System.Web.Compilation.AspParser::_internal
	bool ____internal_16;
	// System.Int32 System.Web.Compilation.AspParser::_internalLineOffset
	int32_t ____internalLineOffset_17;
	// System.Int32 System.Web.Compilation.AspParser::_internalPositionOffset
	int32_t ____internalPositionOffset_18;
	// System.Web.Compilation.AspParser System.Web.Compilation.AspParser::outer
	AspParser_t4001025751 * ___outer_19;
	// System.ComponentModel.EventHandlerList System.Web.Compilation.AspParser::events
	EventHandlerList_t1108123056 * ___events_20;

public:
	inline static int32_t get_offset_of_checksum_4() { return static_cast<int32_t>(offsetof(AspParser_t4001025751, ___checksum_4)); }
	inline MD5_t3177620429 * get_checksum_4() const { return ___checksum_4; }
	inline MD5_t3177620429 ** get_address_of_checksum_4() { return &___checksum_4; }
	inline void set_checksum_4(MD5_t3177620429 * value)
	{
		___checksum_4 = value;
		Il2CppCodeGenWriteBarrier(&___checksum_4, value);
	}

	inline static int32_t get_offset_of_tokenizer_5() { return static_cast<int32_t>(offsetof(AspParser_t4001025751, ___tokenizer_5)); }
	inline AspTokenizer_t992095833 * get_tokenizer_5() const { return ___tokenizer_5; }
	inline AspTokenizer_t992095833 ** get_address_of_tokenizer_5() { return &___tokenizer_5; }
	inline void set_tokenizer_5(AspTokenizer_t992095833 * value)
	{
		___tokenizer_5 = value;
		Il2CppCodeGenWriteBarrier(&___tokenizer_5, value);
	}

	inline static int32_t get_offset_of_beginLine_6() { return static_cast<int32_t>(offsetof(AspParser_t4001025751, ___beginLine_6)); }
	inline int32_t get_beginLine_6() const { return ___beginLine_6; }
	inline int32_t* get_address_of_beginLine_6() { return &___beginLine_6; }
	inline void set_beginLine_6(int32_t value)
	{
		___beginLine_6 = value;
	}

	inline static int32_t get_offset_of_endLine_7() { return static_cast<int32_t>(offsetof(AspParser_t4001025751, ___endLine_7)); }
	inline int32_t get_endLine_7() const { return ___endLine_7; }
	inline int32_t* get_address_of_endLine_7() { return &___endLine_7; }
	inline void set_endLine_7(int32_t value)
	{
		___endLine_7 = value;
	}

	inline static int32_t get_offset_of_beginColumn_8() { return static_cast<int32_t>(offsetof(AspParser_t4001025751, ___beginColumn_8)); }
	inline int32_t get_beginColumn_8() const { return ___beginColumn_8; }
	inline int32_t* get_address_of_beginColumn_8() { return &___beginColumn_8; }
	inline void set_beginColumn_8(int32_t value)
	{
		___beginColumn_8 = value;
	}

	inline static int32_t get_offset_of_endColumn_9() { return static_cast<int32_t>(offsetof(AspParser_t4001025751, ___endColumn_9)); }
	inline int32_t get_endColumn_9() const { return ___endColumn_9; }
	inline int32_t* get_address_of_endColumn_9() { return &___endColumn_9; }
	inline void set_endColumn_9(int32_t value)
	{
		___endColumn_9 = value;
	}

	inline static int32_t get_offset_of_beginPosition_10() { return static_cast<int32_t>(offsetof(AspParser_t4001025751, ___beginPosition_10)); }
	inline int32_t get_beginPosition_10() const { return ___beginPosition_10; }
	inline int32_t* get_address_of_beginPosition_10() { return &___beginPosition_10; }
	inline void set_beginPosition_10(int32_t value)
	{
		___beginPosition_10 = value;
	}

	inline static int32_t get_offset_of_endPosition_11() { return static_cast<int32_t>(offsetof(AspParser_t4001025751, ___endPosition_11)); }
	inline int32_t get_endPosition_11() const { return ___endPosition_11; }
	inline int32_t* get_address_of_endPosition_11() { return &___endPosition_11; }
	inline void set_endPosition_11(int32_t value)
	{
		___endPosition_11 = value;
	}

	inline static int32_t get_offset_of_filename_12() { return static_cast<int32_t>(offsetof(AspParser_t4001025751, ___filename_12)); }
	inline String_t* get_filename_12() const { return ___filename_12; }
	inline String_t** get_address_of_filename_12() { return &___filename_12; }
	inline void set_filename_12(String_t* value)
	{
		___filename_12 = value;
		Il2CppCodeGenWriteBarrier(&___filename_12, value);
	}

	inline static int32_t get_offset_of_verbatimID_13() { return static_cast<int32_t>(offsetof(AspParser_t4001025751, ___verbatimID_13)); }
	inline String_t* get_verbatimID_13() const { return ___verbatimID_13; }
	inline String_t** get_address_of_verbatimID_13() { return &___verbatimID_13; }
	inline void set_verbatimID_13(String_t* value)
	{
		___verbatimID_13 = value;
		Il2CppCodeGenWriteBarrier(&___verbatimID_13, value);
	}

	inline static int32_t get_offset_of_fileText_14() { return static_cast<int32_t>(offsetof(AspParser_t4001025751, ___fileText_14)); }
	inline String_t* get_fileText_14() const { return ___fileText_14; }
	inline String_t** get_address_of_fileText_14() { return &___fileText_14; }
	inline void set_fileText_14(String_t* value)
	{
		___fileText_14 = value;
		Il2CppCodeGenWriteBarrier(&___fileText_14, value);
	}

	inline static int32_t get_offset_of_fileReader_15() { return static_cast<int32_t>(offsetof(AspParser_t4001025751, ___fileReader_15)); }
	inline StringReader_t3465604688 * get_fileReader_15() const { return ___fileReader_15; }
	inline StringReader_t3465604688 ** get_address_of_fileReader_15() { return &___fileReader_15; }
	inline void set_fileReader_15(StringReader_t3465604688 * value)
	{
		___fileReader_15 = value;
		Il2CppCodeGenWriteBarrier(&___fileReader_15, value);
	}

	inline static int32_t get_offset_of__internal_16() { return static_cast<int32_t>(offsetof(AspParser_t4001025751, ____internal_16)); }
	inline bool get__internal_16() const { return ____internal_16; }
	inline bool* get_address_of__internal_16() { return &____internal_16; }
	inline void set__internal_16(bool value)
	{
		____internal_16 = value;
	}

	inline static int32_t get_offset_of__internalLineOffset_17() { return static_cast<int32_t>(offsetof(AspParser_t4001025751, ____internalLineOffset_17)); }
	inline int32_t get__internalLineOffset_17() const { return ____internalLineOffset_17; }
	inline int32_t* get_address_of__internalLineOffset_17() { return &____internalLineOffset_17; }
	inline void set__internalLineOffset_17(int32_t value)
	{
		____internalLineOffset_17 = value;
	}

	inline static int32_t get_offset_of__internalPositionOffset_18() { return static_cast<int32_t>(offsetof(AspParser_t4001025751, ____internalPositionOffset_18)); }
	inline int32_t get__internalPositionOffset_18() const { return ____internalPositionOffset_18; }
	inline int32_t* get_address_of__internalPositionOffset_18() { return &____internalPositionOffset_18; }
	inline void set__internalPositionOffset_18(int32_t value)
	{
		____internalPositionOffset_18 = value;
	}

	inline static int32_t get_offset_of_outer_19() { return static_cast<int32_t>(offsetof(AspParser_t4001025751, ___outer_19)); }
	inline AspParser_t4001025751 * get_outer_19() const { return ___outer_19; }
	inline AspParser_t4001025751 ** get_address_of_outer_19() { return &___outer_19; }
	inline void set_outer_19(AspParser_t4001025751 * value)
	{
		___outer_19 = value;
		Il2CppCodeGenWriteBarrier(&___outer_19, value);
	}

	inline static int32_t get_offset_of_events_20() { return static_cast<int32_t>(offsetof(AspParser_t4001025751, ___events_20)); }
	inline EventHandlerList_t1108123056 * get_events_20() const { return ___events_20; }
	inline EventHandlerList_t1108123056 ** get_address_of_events_20() { return &___events_20; }
	inline void set_events_20(EventHandlerList_t1108123056 * value)
	{
		___events_20 = value;
		Il2CppCodeGenWriteBarrier(&___events_20, value);
	}
};

struct AspParser_t4001025751_StaticFields
{
public:
	// System.Object System.Web.Compilation.AspParser::errorEvent
	Il2CppObject * ___errorEvent_0;
	// System.Object System.Web.Compilation.AspParser::tagParsedEvent
	Il2CppObject * ___tagParsedEvent_1;
	// System.Object System.Web.Compilation.AspParser::textParsedEvent
	Il2CppObject * ___textParsedEvent_2;
	// System.Object System.Web.Compilation.AspParser::parsingCompleteEvent
	Il2CppObject * ___parsingCompleteEvent_3;

public:
	inline static int32_t get_offset_of_errorEvent_0() { return static_cast<int32_t>(offsetof(AspParser_t4001025751_StaticFields, ___errorEvent_0)); }
	inline Il2CppObject * get_errorEvent_0() const { return ___errorEvent_0; }
	inline Il2CppObject ** get_address_of_errorEvent_0() { return &___errorEvent_0; }
	inline void set_errorEvent_0(Il2CppObject * value)
	{
		___errorEvent_0 = value;
		Il2CppCodeGenWriteBarrier(&___errorEvent_0, value);
	}

	inline static int32_t get_offset_of_tagParsedEvent_1() { return static_cast<int32_t>(offsetof(AspParser_t4001025751_StaticFields, ___tagParsedEvent_1)); }
	inline Il2CppObject * get_tagParsedEvent_1() const { return ___tagParsedEvent_1; }
	inline Il2CppObject ** get_address_of_tagParsedEvent_1() { return &___tagParsedEvent_1; }
	inline void set_tagParsedEvent_1(Il2CppObject * value)
	{
		___tagParsedEvent_1 = value;
		Il2CppCodeGenWriteBarrier(&___tagParsedEvent_1, value);
	}

	inline static int32_t get_offset_of_textParsedEvent_2() { return static_cast<int32_t>(offsetof(AspParser_t4001025751_StaticFields, ___textParsedEvent_2)); }
	inline Il2CppObject * get_textParsedEvent_2() const { return ___textParsedEvent_2; }
	inline Il2CppObject ** get_address_of_textParsedEvent_2() { return &___textParsedEvent_2; }
	inline void set_textParsedEvent_2(Il2CppObject * value)
	{
		___textParsedEvent_2 = value;
		Il2CppCodeGenWriteBarrier(&___textParsedEvent_2, value);
	}

	inline static int32_t get_offset_of_parsingCompleteEvent_3() { return static_cast<int32_t>(offsetof(AspParser_t4001025751_StaticFields, ___parsingCompleteEvent_3)); }
	inline Il2CppObject * get_parsingCompleteEvent_3() const { return ___parsingCompleteEvent_3; }
	inline Il2CppObject ** get_address_of_parsingCompleteEvent_3() { return &___parsingCompleteEvent_3; }
	inline void set_parsingCompleteEvent_3(Il2CppObject * value)
	{
		___parsingCompleteEvent_3 = value;
		Il2CppCodeGenWriteBarrier(&___parsingCompleteEvent_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
