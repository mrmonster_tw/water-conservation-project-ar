﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.IDictionary
struct IDictionary_t1363984059;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.Compiler.CodeGeneratorOptions
struct  CodeGeneratorOptions_t1601321324  : public Il2CppObject
{
public:
	// System.Collections.IDictionary System.CodeDom.Compiler.CodeGeneratorOptions::properties
	Il2CppObject * ___properties_0;

public:
	inline static int32_t get_offset_of_properties_0() { return static_cast<int32_t>(offsetof(CodeGeneratorOptions_t1601321324, ___properties_0)); }
	inline Il2CppObject * get_properties_0() const { return ___properties_0; }
	inline Il2CppObject ** get_address_of_properties_0() { return &___properties_0; }
	inline void set_properties_0(Il2CppObject * value)
	{
		___properties_0 = value;
		Il2CppCodeGenWriteBarrier(&___properties_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
