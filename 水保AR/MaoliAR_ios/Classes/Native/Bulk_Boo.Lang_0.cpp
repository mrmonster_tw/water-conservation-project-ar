﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3549286319.h"
#include "Boo_Lang_U3CModuleU3E692745525.h"
#include "Boo_Lang_Boo_Lang_Builtins286914603.h"
#include "mscorlib_System_String1847450689.h"
#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_Text_StringBuilder1712802186.h"
#include "mscorlib_System_Void1185182177.h"
#include "mscorlib_System_Boolean97287965.h"
#include "Boo_Lang_Boo_Lang_Runtime_DynamicDispatching_Dispat684365006.h"
#include "mscorlib_System_IntPtr840150181.h"
#include "mscorlib_System_AsyncCallback3962456242.h"
#include "Boo_Lang_Boo_Lang_Runtime_DynamicDispatching_Dispa1010326087.h"
#include "Boo_Lang_Boo_Lang_Runtime_DynamicDispatching_Dispa2110064572.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge866247282.h"
#include "Boo_Lang_Boo_Lang_Runtime_DynamicDispatching_Dispa1014155341.h"
#include "mscorlib_System_Type2483944760.h"
#include "Boo_Lang_Boo_Lang_Runtime_DynamicDispatching_Dispat965810167.h"
#include "mscorlib_System_Int322950945753.h"
#include "Boo_Lang_Boo_Lang_Runtime_DynamicDispatching_Numer3533651679.h"
#include "Boo_Lang_Boo_Lang_Runtime_ExtensionRegistry2424660641.h"
#include "Boo_Lang_Boo_Lang_List_1_gen1242660502.h"
#include "Boo_Lang_Boo_Lang_Runtime_RuntimeServices2098243569.h"
#include "mscorlib_System_RuntimeTypeHandle3027515415.h"
#include "Boo_Lang_Boo_Lang_Runtime_RuntimeServices_U3CCoerce572148199.h"
#include "mscorlib_System_Reflection_MethodInfo1877626248.h"
#include "mscorlib_System_TypeCode2987224087.h"
#include "mscorlib_System_Delegate1188392813.h"
#include "Boo_Lang_Boo_Lang_Runtime_RuntimeServices_U3CEmitI4009522197.h"
#include "mscorlib_System_ArgumentException132251570.h"
#include "mscorlib_System_Decimal2948259380.h"
#include "mscorlib_System_Double594665363.h"
#include "mscorlib_System_Single1397266774.h"
#include "mscorlib_System_UInt644134040092.h"
#include "mscorlib_System_Int643736567304.h"
#include "mscorlib_System_UInt322560061978.h"
#include "mscorlib_System_Reflection_BindingFlags2721792723.h"
#include "Boo_Lang_Boo_Lang_Runtime_RuntimeServices_U3CGetEx3368530435.h"
#include "mscorlib_System_Reflection_ParameterInfo1861056598.h"
#include "mscorlib_System_Reflection_MemberInfo3380001741.h"
#include "mscorlib_System_Reflection_MethodBase609368412.h"
#include "mscorlib_System_Reflection_MemberTypes3790569052.h"
#include "mscorlib_System_NotSupportedException1314879016.h"

// System.String
struct String_t;
// System.Collections.IEnumerable
struct IEnumerable_t1941168011;
// System.Text.StringBuilder
struct StringBuilder_t1712802186;
// System.Object
struct Il2CppObject;
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher
struct Dispatcher_t684365006;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// Boo.Lang.Runtime.DynamicDispatching.DispatcherCache
struct DispatcherCache_t1010326087;
// System.Collections.Generic.Dictionary`2<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>
struct Dictionary_2_t866247282;
// System.Collections.Generic.IEqualityComparer`1<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey>
struct IEqualityComparer_1_t4217396590;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t132545152;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t892470886;
// Boo.Lang.Runtime.DynamicDispatching.DispatcherKey
struct DispatcherKey_t2110064572;
// Boo.Lang.Runtime.DynamicDispatching.DispatcherCache/DispatcherFactory
struct DispatcherFactory_t1014155341;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// Boo.Lang.Runtime.DynamicDispatching.DispatcherKey/_EqualityComparer
struct _EqualityComparer_t965810167;
// Boo.Lang.Runtime.ExtensionRegistry
struct ExtensionRegistry_t2424660641;
// Boo.Lang.List`1<System.Reflection.MemberInfo>
struct List_1_t1242660502;
// Boo.Lang.List`1<System.Object>
struct List_1_t942764925;
// System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>
struct IEnumerable_1_t2359854630;
// Boo.Lang.Runtime.RuntimeServices/<Coerce>c__AnonStorey1D
struct U3CCoerceU3Ec__AnonStorey1D_t572148199;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Delegate
struct Delegate_t1188392813;
// Boo.Lang.Runtime.RuntimeServices/<EmitImplicitConversionDispatcher>c__AnonStorey1E
struct U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t4009522197;
// System.Array
struct Il2CppArray;
// System.ArgumentException
struct ArgumentException_t132251570;
// System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>
struct IEnumerable_1_t857479137;
// Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC
struct U3CGetExtensionMethodsU3Ec__IteratorC_t3368530435;
// System.Reflection.ParameterInfo
struct ParameterInfo_t1861056598;
// System.Reflection.MethodBase
struct MethodBase_t609368412;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.Collections.Generic.IEnumerator`1<System.Reflection.MethodInfo>
struct IEnumerator_1_t2310196716;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
extern Il2CppClass* StringBuilder_t1712802186_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t1941168011_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t3640265483_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1853284238_il2cpp_TypeInfo_var;
extern const uint32_t Builtins_join_m864932387_MetadataUsageId;
extern Il2CppClass* DispatcherKey_t2110064572_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t866247282_il2cpp_TypeInfo_var;
extern Il2CppClass* DispatcherCache_t1010326087_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1815469916_MethodInfo_var;
extern const uint32_t DispatcherCache__cctor_m62575632_MetadataUsageId;
extern const MethodInfo* Dictionary_2_TryGetValue_m1990775459_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m163959252_MethodInfo_var;
extern const uint32_t DispatcherCache_Get_m3609792288_MetadataUsageId;
extern Il2CppClass* Dispatcher_t684365006_il2cpp_TypeInfo_var;
extern const uint32_t DelegatePInvokeWrapper_DispatcherFactory_t1014155341_MetadataUsageId;
extern Il2CppClass* _EqualityComparer_t965810167_il2cpp_TypeInfo_var;
extern const uint32_t DispatcherKey__cctor_m1082726794_MetadataUsageId;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t _EqualityComparer_Equals_m2225820911_MetadataUsageId;
extern Il2CppClass* List_1_t1242660502_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1115569667_MethodInfo_var;
extern const uint32_t ExtensionRegistry__ctor_m323844458_MetadataUsageId;
extern const Il2CppType* RuntimeServices_t2098243569_0_0_0_var;
extern Il2CppClass* ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var;
extern Il2CppClass* RuntimeServices_t2098243569_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ExtensionRegistry_t2424660641_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t97287965_il2cpp_TypeInfo_var;
extern const uint32_t RuntimeServices__cctor_m1381641241_MetadataUsageId;
extern const uint32_t RuntimeServices_GetDispatcher_m3922777458_MetadataUsageId;
extern Il2CppClass* U3CCoerceU3Ec__AnonStorey1D_t572148199_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var;
extern Il2CppClass* DispatcherFactory_t1014155341_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CCoerceU3Ec__AnonStorey1D_U3CU3Em__15_m4262864175_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4123921170;
extern const uint32_t RuntimeServices_Coerce_m1663619687_MetadataUsageId;
extern Il2CppClass* ICoercible_t1130343077_il2cpp_TypeInfo_var;
extern const MethodInfo* RuntimeServices_IdentityDispatcher_m1725786149_MethodInfo_var;
extern const MethodInfo* RuntimeServices_CoercibleDispatcher_m4058208960_MethodInfo_var;
extern const uint32_t RuntimeServices_CreateCoerceDispatcher_m661106792_MetadataUsageId;
extern const Il2CppType* Dispatcher_t684365006_0_0_0_var;
extern const Il2CppType* NumericPromotions_t3533651679_0_0_0_var;
extern Il2CppClass* TypeCode_t2987224087_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2755855817;
extern Il2CppCodeGenString* _stringLiteral3454777324;
extern const uint32_t RuntimeServices_EmitPromotionDispatcher_m734050861_MetadataUsageId;
extern const uint32_t RuntimeServices_IsPromotableNumeric_m2095247129_MetadataUsageId;
extern Il2CppClass* U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t4009522197_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_U3CU3Em__16_m940011970_MethodInfo_var;
extern const uint32_t RuntimeServices_EmitImplicitConversionDispatcher_m4072191859_MetadataUsageId;
extern const uint32_t RuntimeServices_CoercibleDispatcher_m4058208960_MetadataUsageId;
extern const uint32_t RuntimeServices_op_Addition_m583005490_MetadataUsageId;
extern Il2CppClass* Il2CppArray_il2cpp_TypeInfo_var;
extern const uint32_t RuntimeServices_EqualityOperator_m2384872086_MetadataUsageId;
extern Il2CppClass* ArgumentException_t132251570_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral181402784;
extern const uint32_t RuntimeServices_ArrayEqualityImpl_m3673667702_MetadataUsageId;
extern Il2CppClass* IConvertible_t2977365677_il2cpp_TypeInfo_var;
extern Il2CppClass* Decimal_t2948259380_il2cpp_TypeInfo_var;
extern const uint32_t RuntimeServices_EqualityOperator_m2541246096_MetadataUsageId;
extern const uint32_t RuntimeServices_FindImplicitConversionOperator_m2560683559_MetadataUsageId;
extern Il2CppClass* U3CGetExtensionMethodsU3Ec__IteratorC_t3368530435_il2cpp_TypeInfo_var;
extern const uint32_t RuntimeServices_GetExtensionMethods_m2018979233_MetadataUsageId;
extern Il2CppClass* IEnumerable_1_t857479137_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t2310196716_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3306367446;
extern const uint32_t RuntimeServices_FindImplicitConversionMethod_m4176567206_MetadataUsageId;
extern const uint32_t U3CCoerceU3Ec__AnonStorey1D_U3CU3Em__15_m4262864175_MetadataUsageId;
extern const uint32_t U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_U3CU3Em__16_m940011970_MetadataUsageId;
extern const uint32_t U3CGetExtensionMethodsU3Ec__IteratorC_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MethodInfoU3E_GetEnumerator_m1148048048_MetadataUsageId;
extern Il2CppClass* IEnumerable_1_t2359854630_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t3812572209_il2cpp_TypeInfo_var;
extern Il2CppClass* MethodInfo_t_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetExtensionMethodsU3Ec__IteratorC_MoveNext_m4162423597_MetadataUsageId;
extern const uint32_t U3CGetExtensionMethodsU3Ec__IteratorC_Dispose_m3192498793_MetadataUsageId;
extern Il2CppClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetExtensionMethodsU3Ec__IteratorC_Reset_m865776842_MetadataUsageId;

// System.Object[]
struct ObjectU5BU5D_t2843939325  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Type[]
struct TypeU5BU5D_t3940880105  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t2572182361  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MethodInfo_t * m_Items[1];

public:
	inline MethodInfo_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline MethodInfo_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, MethodInfo_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline MethodInfo_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline MethodInfo_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, MethodInfo_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t390618515  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ParameterInfo_t1861056598 * m_Items[1];

public:
	inline ParameterInfo_t1861056598 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ParameterInfo_t1861056598 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ParameterInfo_t1861056598 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ParameterInfo_t1861056598 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ParameterInfo_t1861056598 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ParameterInfo_t1861056598 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<!0>)
extern "C"  void Dictionary_2__ctor_m3072481003_gshared (Dictionary_2_t132545152 * __this, Il2CppObject* p0, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&)
extern "C"  bool Dictionary_2_TryGetValue_m3280774074_gshared (Dictionary_2_t132545152 * __this, Il2CppObject * p0, Il2CppObject ** p1, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m2387223709_gshared (Dictionary_2_t132545152 * __this, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method);
// System.Void Boo.Lang.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2774066863_gshared (List_1_t942764925 * __this, const MethodInfo* method);

// System.Void System.Text.StringBuilder::.ctor()
extern "C"  void StringBuilder__ctor_m3121283359 (StringBuilder_t1712802186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Object)
extern "C"  StringBuilder_t1712802186 * StringBuilder_Append_m1640838743 (StringBuilder_t1712802186 * __this, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
extern "C"  StringBuilder_t1712802186 * StringBuilder_Append_m1965104174 (StringBuilder_t1712802186 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.StringBuilder::ToString()
extern "C"  String_t* StringBuilder_ToString_m3317489284 (StringBuilder_t1712802186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Boo.Lang.Runtime.DynamicDispatching.Dispatcher::Invoke(System.Object,System.Object[])
extern "C"  Il2CppObject * Dispatcher_Invoke_m1639676350 (Dispatcher_t684365006 * __this, Il2CppObject * ___target0, ObjectU5BU5D_t2843939325* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>::.ctor(System.Collections.Generic.IEqualityComparer`1<!0>)
#define Dictionary_2__ctor_m1815469916(__this, p0, method) ((  void (*) (Dictionary_2_t866247282 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3072481003_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m1990775459(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t866247282 *, DispatcherKey_t2110064572 *, Dispatcher_t684365006 **, const MethodInfo*))Dictionary_2_TryGetValue_m3280774074_gshared)(__this, p0, p1, method)
// System.Void System.Threading.Monitor::Enter(System.Object)
extern "C"  void Monitor_Enter_m2249409497 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher Boo.Lang.Runtime.DynamicDispatching.DispatcherCache/DispatcherFactory::Invoke()
extern "C"  Dispatcher_t684365006 * DispatcherFactory_Invoke_m2833957726 (DispatcherFactory_t1014155341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>::Add(!0,!1)
#define Dictionary_2_Add_m163959252(__this, p0, p1, method) ((  void (*) (Dictionary_2_t866247282 *, DispatcherKey_t2110064572 *, Dispatcher_t684365006 *, const MethodInfo*))Dictionary_2_Add_m2387223709_gshared)(__this, p0, p1, method)
// System.Void System.Threading.Monitor::Exit(System.Object)
extern "C"  void Monitor_Exit_m3585316909 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boo.Lang.Runtime.DynamicDispatching.DispatcherKey/_EqualityComparer::.ctor()
extern "C"  void _EqualityComparer__ctor_m1412158449 (_EqualityComparer_t965810167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::GetHashCode()
extern "C"  int32_t String_GetHashCode_m1906374149 (String_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Inequality(System.String,System.String)
extern "C"  bool String_op_Inequality_m215368492 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boo.Lang.List`1<System.Reflection.MemberInfo>::.ctor()
#define List_1__ctor_m1115569667(__this, method) ((  void (*) (List_1_t1242660502 *, const MethodInfo*))List_1__ctor_m2774066863_gshared)(__this, method)
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m1620074514 (Il2CppObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boo.Lang.Runtime.DynamicDispatching.DispatcherCache::.ctor()
extern "C"  void DispatcherCache__ctor_m2156165103 (DispatcherCache_t1010326087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boo.Lang.Runtime.ExtensionRegistry::.ctor()
extern "C"  void ExtensionRegistry__ctor_m323844458 (ExtensionRegistry_t2424660641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m88164663 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boo.Lang.Runtime.DynamicDispatching.DispatcherKey::.ctor(System.Type,System.String,System.Type[])
extern "C"  void DispatcherKey__ctor_m1810669420 (DispatcherKey_t2110064572 * __this, Type_t * ___type0, String_t* ___name1, TypeU5BU5D_t3940880105* ___arguments2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher Boo.Lang.Runtime.DynamicDispatching.DispatcherCache::Get(Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.DispatcherCache/DispatcherFactory)
extern "C"  Dispatcher_t684365006 * DispatcherCache_Get_m3609792288 (DispatcherCache_t1010326087 * __this, DispatcherKey_t2110064572 * ___key0, DispatcherFactory_t1014155341 * ___factory1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boo.Lang.Runtime.RuntimeServices/<Coerce>c__AnonStorey1D::.ctor()
extern "C"  void U3CCoerceU3Ec__AnonStorey1D__ctor_m2227718426 (U3CCoerceU3Ec__AnonStorey1D_t572148199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boo.Lang.Runtime.DynamicDispatching.DispatcherCache/DispatcherFactory::.ctor(System.Object,System.IntPtr)
extern "C"  void DispatcherFactory__ctor_m3649217775 (DispatcherFactory_t1014155341 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher Boo.Lang.Runtime.RuntimeServices::GetDispatcher(System.Object,System.String,System.Type[],Boo.Lang.Runtime.DynamicDispatching.DispatcherCache/DispatcherFactory)
extern "C"  Dispatcher_t684365006 * RuntimeServices_GetDispatcher_m3922777458 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___target0, String_t* ___cacheKeyName1, TypeU5BU5D_t3940880105* ___cacheKeyTypes2, DispatcherFactory_t1014155341 * ___factory3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boo.Lang.Runtime.DynamicDispatching.Dispatcher::.ctor(System.Object,System.IntPtr)
extern "C"  void Dispatcher__ctor_m4145559729 (Dispatcher_t684365006 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Boo.Lang.Runtime.RuntimeServices::IsPromotableNumeric(System.Type)
extern "C"  bool RuntimeServices_IsPromotableNumeric_m2095247129 (Il2CppObject * __this /* static, unused */, Type_t * ___fromType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher Boo.Lang.Runtime.RuntimeServices::EmitPromotionDispatcher(System.Type,System.Type)
extern "C"  Dispatcher_t684365006 * RuntimeServices_EmitPromotionDispatcher_m734050861 (Il2CppObject * __this /* static, unused */, Type_t * ___fromType0, Type_t * ___toType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo Boo.Lang.Runtime.RuntimeServices::FindImplicitConversionOperator(System.Type,System.Type)
extern "C"  MethodInfo_t * RuntimeServices_FindImplicitConversionOperator_m2560683559 (Il2CppObject * __this /* static, unused */, Type_t * ___from0, Type_t * ___to1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher Boo.Lang.Runtime.RuntimeServices::EmitImplicitConversionDispatcher(System.Reflection.MethodInfo)
extern "C"  Dispatcher_t684365006 * RuntimeServices_EmitImplicitConversionDispatcher_m4072191859 (Il2CppObject * __this /* static, unused */, MethodInfo_t * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TypeCode System.Type::GetTypeCode(System.Type)
extern "C"  int32_t Type_GetTypeCode_m480753082 (Il2CppObject * __this /* static, unused */, Type_t * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object[])
extern "C"  String_t* String_Concat_m2971454694 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t2843939325* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Type::GetMethod(System.String)
extern "C"  MethodInfo_t * Type_GetMethod_m2019726356 (Type_t * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::CreateDelegate(System.Type,System.Reflection.MethodInfo)
extern "C"  Delegate_t1188392813 * Delegate_CreateDelegate_m2396489936 (Il2CppObject * __this /* static, unused */, Type_t * p0, MethodInfo_t * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Boo.Lang.Runtime.RuntimeServices::IsPromotableNumeric(System.TypeCode)
extern "C"  bool RuntimeServices_IsPromotableNumeric_m3673263760 (Il2CppObject * __this /* static, unused */, int32_t ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boo.Lang.Runtime.RuntimeServices/<EmitImplicitConversionDispatcher>c__AnonStorey1E::.ctor()
extern "C"  void U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E__ctor_m1951951183 (U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t4009522197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m3937257545 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Boo.Lang.Runtime.RuntimeServices::IsNumeric(System.TypeCode)
extern "C"  bool RuntimeServices_IsNumeric_m2879598828 (Il2CppObject * __this /* static, unused */, int32_t ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Boo.Lang.Runtime.RuntimeServices::EqualityOperator(System.Object,System.TypeCode,System.Object,System.TypeCode)
extern "C"  bool RuntimeServices_EqualityOperator_m2541246096 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___lhs0, int32_t ___lhsTypeCode1, Il2CppObject * ___rhs2, int32_t ___rhsTypeCode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Boo.Lang.Runtime.RuntimeServices::ArrayEqualityImpl(System.Array,System.Array)
extern "C"  bool RuntimeServices_ArrayEqualityImpl_m3673667702 (Il2CppObject * __this /* static, unused */, Il2CppArray * ___lhs0, Il2CppArray * ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::get_Rank()
extern "C"  int32_t Array_get_Rank_m3448755881 (Il2CppArray * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String)
extern "C"  void ArgumentException__ctor_m1312628991 (ArgumentException_t132251570 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::get_Length()
extern "C"  int32_t Array_get_Length_m21610649 (Il2CppArray * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array::GetValue(System.Int32)
extern "C"  Il2CppObject * Array_GetValue_m2528546681 (Il2CppArray * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Boo.Lang.Runtime.RuntimeServices::EqualityOperator(System.Object,System.Object)
extern "C"  bool RuntimeServices_EqualityOperator_m2384872086 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___lhs0, Il2CppObject * ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TypeCode Boo.Lang.Runtime.RuntimeServices::GetConvertTypeCode(System.TypeCode,System.TypeCode)
extern "C"  int32_t RuntimeServices_GetConvertTypeCode_m597524256 (Il2CppObject * __this /* static, unused */, int32_t ___lhsTypeCode0, int32_t ___rhsTypeCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Decimal::op_Equality(System.Decimal,System.Decimal)
extern "C"  bool Decimal_op_Equality_m77262825 (Il2CppObject * __this /* static, unused */, Decimal_t2948259380  p0, Decimal_t2948259380  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo Boo.Lang.Runtime.RuntimeServices::FindImplicitConversionMethod(System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>,System.Type,System.Type)
extern "C"  MethodInfo_t * RuntimeServices_FindImplicitConversionMethod_m4176567206 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___candidates0, Type_t * ___from1, Type_t * ___to2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo> Boo.Lang.Runtime.RuntimeServices::GetExtensionMethods()
extern "C"  Il2CppObject* RuntimeServices_GetExtensionMethods_m2018979233 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC::.ctor()
extern "C"  void U3CGetExtensionMethodsU3Ec__IteratorC__ctor_m1217043206 (U3CGetExtensionMethodsU3Ec__IteratorC_t3368530435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher Boo.Lang.Runtime.RuntimeServices::CreateCoerceDispatcher(System.Object,System.Type)
extern "C"  Dispatcher_t684365006 * RuntimeServices_CreateCoerceDispatcher_m661106792 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, Type_t * ___toType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.MethodBase::Invoke(System.Object,System.Object[])
extern "C"  Il2CppObject * MethodBase_Invoke_m1776411915 (MethodBase_t609368412 * __this, Il2CppObject * p0, ObjectU5BU5D_t2843939325* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Reflection.MethodInfo> Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC::System.Collections.Generic.IEnumerable<System.Reflection.MethodInfo>.GetEnumerator()
extern "C"  Il2CppObject* U3CGetExtensionMethodsU3Ec__IteratorC_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MethodInfoU3E_GetEnumerator_m1148048048 (U3CGetExtensionMethodsU3Ec__IteratorC_t3368530435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Interlocked::CompareExchange(System.Int32&,System.Int32,System.Int32)
extern "C"  int32_t Interlocked_CompareExchange_m3023855514 (Il2CppObject * __this /* static, unused */, int32_t* p0, int32_t p1, int32_t p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo> Boo.Lang.Runtime.ExtensionRegistry::get_Extensions()
extern "C"  Il2CppObject* ExtensionRegistry_get_Extensions_m3605511872 (ExtensionRegistry_t2424660641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m2730133172 (NotSupportedException_t1314879016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Boo.Lang.Builtins::join(System.Collections.IEnumerable,System.String)
extern "C"  String_t* Builtins_join_m864932387 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___enumerable0, String_t* ___separator1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builtins_join_m864932387_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1712802186 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringBuilder_t1712802186 * L_0 = (StringBuilder_t1712802186 *)il2cpp_codegen_object_new(StringBuilder_t1712802186_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3121283359(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Il2CppObject * L_1 = ___enumerable0;
		NullCheck(L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t1941168011_il2cpp_TypeInfo_var, L_1);
		V_1 = L_2;
		Il2CppObject * L_3 = V_1;
		V_2 = ((Il2CppObject *)IsInst(L_3, IDisposable_t3640265483_il2cpp_TypeInfo_var));
	}

IL_0014:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_4 = V_1;
			NullCheck(L_4);
			bool L_5 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_4);
			if (!L_5)
			{
				goto IL_0051;
			}
		}

IL_001f:
		{
			StringBuilder_t1712802186 * L_6 = V_0;
			Il2CppObject * L_7 = V_1;
			NullCheck(L_7);
			Il2CppObject * L_8 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_7);
			NullCheck(L_6);
			StringBuilder_Append_m1640838743(L_6, L_8, /*hidden argument*/NULL);
			goto IL_0046;
		}

IL_0031:
		{
			StringBuilder_t1712802186 * L_9 = V_0;
			String_t* L_10 = ___separator1;
			NullCheck(L_9);
			StringBuilder_Append_m1965104174(L_9, L_10, /*hidden argument*/NULL);
			StringBuilder_t1712802186 * L_11 = V_0;
			Il2CppObject * L_12 = V_1;
			NullCheck(L_12);
			Il2CppObject * L_13 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_12);
			NullCheck(L_11);
			StringBuilder_Append_m1640838743(L_11, L_13, /*hidden argument*/NULL);
		}

IL_0046:
		{
			Il2CppObject * L_14 = V_1;
			NullCheck(L_14);
			bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_14);
			if (L_15)
			{
				goto IL_0031;
			}
		}

IL_0051:
		{
			IL2CPP_LEAVE(0x63, FINALLY_0056);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0056;
	}

FINALLY_0056:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_16 = V_2;
			if (!L_16)
			{
				goto IL_0062;
			}
		}

IL_005c:
		{
			Il2CppObject * L_17 = V_2;
			NullCheck(L_17);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_17);
		}

IL_0062:
		{
			IL2CPP_END_FINALLY(86)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(86)
	{
		IL2CPP_JUMP_TBL(0x63, IL_0063)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0063:
	{
		StringBuilder_t1712802186 * L_18 = V_0;
		NullCheck(L_18);
		String_t* L_19 = StringBuilder_ToString_m3317489284(L_18, /*hidden argument*/NULL);
		return L_19;
	}
}
// System.Void Boo.Lang.Runtime.DynamicDispatching.Dispatcher::.ctor(System.Object,System.IntPtr)
extern "C"  void Dispatcher__ctor_m4145559729 (Dispatcher_t684365006 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Object Boo.Lang.Runtime.DynamicDispatching.Dispatcher::Invoke(System.Object,System.Object[])
extern "C"  Il2CppObject * Dispatcher_Invoke_m1639676350 (Dispatcher_t684365006 * __this, Il2CppObject * ___target0, ObjectU5BU5D_t2843939325* ___args1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Dispatcher_Invoke_m1639676350((Dispatcher_t684365006 *)__this->get_prev_9(),___target0, ___args1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___target0, ObjectU5BU5D_t2843939325* ___args1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___target0, ___args1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___target0, ObjectU5BU5D_t2843939325* ___args1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___target0, ___args1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, ObjectU5BU5D_t2843939325* ___args1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___target0, ___args1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult Boo.Lang.Runtime.DynamicDispatching.Dispatcher::BeginInvoke(System.Object,System.Object[],System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Dispatcher_BeginInvoke_m1986273823 (Dispatcher_t684365006 * __this, Il2CppObject * ___target0, ObjectU5BU5D_t2843939325* ___args1, AsyncCallback_t3962456242 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___target0;
	__d_args[1] = ___args1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Object Boo.Lang.Runtime.DynamicDispatching.Dispatcher::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Dispatcher_EndInvoke_m1934666275 (Dispatcher_t684365006 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void Boo.Lang.Runtime.DynamicDispatching.DispatcherCache::.ctor()
extern "C"  void DispatcherCache__ctor_m2156165103 (DispatcherCache_t1010326087 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Boo.Lang.Runtime.DynamicDispatching.DispatcherCache::.cctor()
extern "C"  void DispatcherCache__cctor_m62575632 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DispatcherCache__cctor_m62575632_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DispatcherKey_t2110064572_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((DispatcherKey_t2110064572_StaticFields*)DispatcherKey_t2110064572_il2cpp_TypeInfo_var->static_fields)->get_EqualityComparer_0();
		Dictionary_2_t866247282 * L_1 = (Dictionary_2_t866247282 *)il2cpp_codegen_object_new(Dictionary_2_t866247282_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1815469916(L_1, L_0, /*hidden argument*/Dictionary_2__ctor_m1815469916_MethodInfo_var);
		((DispatcherCache_t1010326087_StaticFields*)DispatcherCache_t1010326087_il2cpp_TypeInfo_var->static_fields)->set__cache_0(L_1);
		return;
	}
}
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher Boo.Lang.Runtime.DynamicDispatching.DispatcherCache::Get(Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.DispatcherCache/DispatcherFactory)
extern "C"  Dispatcher_t684365006 * DispatcherCache_Get_m3609792288 (DispatcherCache_t1010326087 * __this, DispatcherKey_t2110064572 * ___key0, DispatcherFactory_t1014155341 * ___factory1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DispatcherCache_Get_m3609792288_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dispatcher_t684365006 * V_0 = NULL;
	Dictionary_2_t866247282 * V_1 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(DispatcherCache_t1010326087_il2cpp_TypeInfo_var);
		Dictionary_2_t866247282 * L_0 = ((DispatcherCache_t1010326087_StaticFields*)DispatcherCache_t1010326087_il2cpp_TypeInfo_var->static_fields)->get__cache_0();
		DispatcherKey_t2110064572 * L_1 = ___key0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_TryGetValue_m1990775459(L_0, L_1, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m1990775459_MethodInfo_var);
		if (L_2)
		{
			goto IL_004f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DispatcherCache_t1010326087_il2cpp_TypeInfo_var);
		Dictionary_2_t866247282 * L_3 = ((DispatcherCache_t1010326087_StaticFields*)DispatcherCache_t1010326087_il2cpp_TypeInfo_var->static_fields)->get__cache_0();
		V_1 = L_3;
		Dictionary_2_t866247282 * L_4 = V_1;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_001e:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(DispatcherCache_t1010326087_il2cpp_TypeInfo_var);
			Dictionary_2_t866247282 * L_5 = ((DispatcherCache_t1010326087_StaticFields*)DispatcherCache_t1010326087_il2cpp_TypeInfo_var->static_fields)->get__cache_0();
			DispatcherKey_t2110064572 * L_6 = ___key0;
			NullCheck(L_5);
			bool L_7 = Dictionary_2_TryGetValue_m1990775459(L_5, L_6, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m1990775459_MethodInfo_var);
			if (L_7)
			{
				goto IL_0043;
			}
		}

IL_0030:
		{
			DispatcherFactory_t1014155341 * L_8 = ___factory1;
			NullCheck(L_8);
			Dispatcher_t684365006 * L_9 = DispatcherFactory_Invoke_m2833957726(L_8, /*hidden argument*/NULL);
			V_0 = L_9;
			IL2CPP_RUNTIME_CLASS_INIT(DispatcherCache_t1010326087_il2cpp_TypeInfo_var);
			Dictionary_2_t866247282 * L_10 = ((DispatcherCache_t1010326087_StaticFields*)DispatcherCache_t1010326087_il2cpp_TypeInfo_var->static_fields)->get__cache_0();
			DispatcherKey_t2110064572 * L_11 = ___key0;
			Dispatcher_t684365006 * L_12 = V_0;
			NullCheck(L_10);
			Dictionary_2_Add_m163959252(L_10, L_11, L_12, /*hidden argument*/Dictionary_2_Add_m163959252_MethodInfo_var);
		}

IL_0043:
		{
			IL2CPP_LEAVE(0x4F, FINALLY_0048);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0048;
	}

FINALLY_0048:
	{ // begin finally (depth: 1)
		Dictionary_2_t866247282 * L_13 = V_1;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(72)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(72)
	{
		IL2CPP_JUMP_TBL(0x4F, IL_004f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_004f:
	{
		Dispatcher_t684365006 * L_14 = V_0;
		return L_14;
	}
}
extern "C"  Dispatcher_t684365006 * DelegatePInvokeWrapper_DispatcherFactory_t1014155341 (DispatcherFactory_t1014155341 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DelegatePInvokeWrapper_DispatcherFactory_t1014155341_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	typedef Il2CppMethodPointer (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	Il2CppMethodPointer returnValue = il2cppPInvokeFunc();

	// Marshaling of return value back from native representation
	Dispatcher_t684365006 * _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_function_ptr_to_delegate<Dispatcher_t684365006>(returnValue, Dispatcher_t684365006_il2cpp_TypeInfo_var);

	return _returnValue_unmarshaled;
}
// System.Void Boo.Lang.Runtime.DynamicDispatching.DispatcherCache/DispatcherFactory::.ctor(System.Object,System.IntPtr)
extern "C"  void DispatcherFactory__ctor_m3649217775 (DispatcherFactory_t1014155341 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher Boo.Lang.Runtime.DynamicDispatching.DispatcherCache/DispatcherFactory::Invoke()
extern "C"  Dispatcher_t684365006 * DispatcherFactory_Invoke_m2833957726 (DispatcherFactory_t1014155341 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DispatcherFactory_Invoke_m2833957726((DispatcherFactory_t1014155341 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Dispatcher_t684365006 * (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Dispatcher_t684365006 * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult Boo.Lang.Runtime.DynamicDispatching.DispatcherCache/DispatcherFactory::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DispatcherFactory_BeginInvoke_m1351536112 (DispatcherFactory_t1014155341 * __this, AsyncCallback_t3962456242 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher Boo.Lang.Runtime.DynamicDispatching.DispatcherCache/DispatcherFactory::EndInvoke(System.IAsyncResult)
extern "C"  Dispatcher_t684365006 * DispatcherFactory_EndInvoke_m2974203098 (DispatcherFactory_t1014155341 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Dispatcher_t684365006 *)__result;
}
// System.Void Boo.Lang.Runtime.DynamicDispatching.DispatcherKey::.ctor(System.Type,System.String,System.Type[])
extern "C"  void DispatcherKey__ctor_m1810669420 (DispatcherKey_t2110064572 * __this, Type_t * ___type0, String_t* ___name1, TypeU5BU5D_t3940880105* ___arguments2, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___type0;
		__this->set__type_1(L_0);
		String_t* L_1 = ___name1;
		__this->set__name_2(L_1);
		TypeU5BU5D_t3940880105* L_2 = ___arguments2;
		__this->set__arguments_3(L_2);
		return;
	}
}
// System.Void Boo.Lang.Runtime.DynamicDispatching.DispatcherKey::.cctor()
extern "C"  void DispatcherKey__cctor_m1082726794 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DispatcherKey__cctor_m1082726794_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		_EqualityComparer_t965810167 * L_0 = (_EqualityComparer_t965810167 *)il2cpp_codegen_object_new(_EqualityComparer_t965810167_il2cpp_TypeInfo_var);
		_EqualityComparer__ctor_m1412158449(L_0, /*hidden argument*/NULL);
		((DispatcherKey_t2110064572_StaticFields*)DispatcherKey_t2110064572_il2cpp_TypeInfo_var->static_fields)->set_EqualityComparer_0(L_0);
		return;
	}
}
// System.Void Boo.Lang.Runtime.DynamicDispatching.DispatcherKey/_EqualityComparer::.ctor()
extern "C"  void _EqualityComparer__ctor_m1412158449 (_EqualityComparer_t965810167 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Boo.Lang.Runtime.DynamicDispatching.DispatcherKey/_EqualityComparer::GetHashCode(Boo.Lang.Runtime.DynamicDispatching.DispatcherKey)
extern "C"  int32_t _EqualityComparer_GetHashCode_m3745681409 (_EqualityComparer_t965810167 * __this, DispatcherKey_t2110064572 * ___key0, const MethodInfo* method)
{
	{
		DispatcherKey_t2110064572 * L_0 = ___key0;
		NullCheck(L_0);
		Type_t * L_1 = L_0->get__type_1();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Type::GetHashCode() */, L_1);
		DispatcherKey_t2110064572 * L_3 = ___key0;
		NullCheck(L_3);
		String_t* L_4 = L_3->get__name_2();
		NullCheck(L_4);
		int32_t L_5 = String_GetHashCode_m1906374149(L_4, /*hidden argument*/NULL);
		DispatcherKey_t2110064572 * L_6 = ___key0;
		NullCheck(L_6);
		TypeU5BU5D_t3940880105* L_7 = L_6->get__arguments_3();
		NullCheck(L_7);
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_2^(int32_t)L_5))^(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))));
	}
}
// System.Boolean Boo.Lang.Runtime.DynamicDispatching.DispatcherKey/_EqualityComparer::Equals(Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.DispatcherKey)
extern "C"  bool _EqualityComparer_Equals_m2225820911 (_EqualityComparer_t965810167 * __this, DispatcherKey_t2110064572 * ___x0, DispatcherKey_t2110064572 * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_EqualityComparer_Equals_m2225820911_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		DispatcherKey_t2110064572 * L_0 = ___x0;
		NullCheck(L_0);
		Type_t * L_1 = L_0->get__type_1();
		DispatcherKey_t2110064572 * L_2 = ___y1;
		NullCheck(L_2);
		Type_t * L_3 = L_2->get__type_1();
		if ((((Il2CppObject*)(Type_t *)L_1) == ((Il2CppObject*)(Type_t *)L_3)))
		{
			goto IL_0013;
		}
	}
	{
		return (bool)0;
	}

IL_0013:
	{
		DispatcherKey_t2110064572 * L_4 = ___x0;
		NullCheck(L_4);
		TypeU5BU5D_t3940880105* L_5 = L_4->get__arguments_3();
		NullCheck(L_5);
		DispatcherKey_t2110064572 * L_6 = ___y1;
		NullCheck(L_6);
		TypeU5BU5D_t3940880105* L_7 = L_6->get__arguments_3();
		NullCheck(L_7);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))))) == ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_002a;
		}
	}
	{
		return (bool)0;
	}

IL_002a:
	{
		DispatcherKey_t2110064572 * L_8 = ___x0;
		NullCheck(L_8);
		String_t* L_9 = L_8->get__name_2();
		DispatcherKey_t2110064572 * L_10 = ___y1;
		NullCheck(L_10);
		String_t* L_11 = L_10->get__name_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0042;
		}
	}
	{
		return (bool)0;
	}

IL_0042:
	{
		V_0 = 0;
		goto IL_0064;
	}

IL_0049:
	{
		DispatcherKey_t2110064572 * L_13 = ___x0;
		NullCheck(L_13);
		TypeU5BU5D_t3940880105* L_14 = L_13->get__arguments_3();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Type_t * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		DispatcherKey_t2110064572 * L_18 = ___y1;
		NullCheck(L_18);
		TypeU5BU5D_t3940880105* L_19 = L_18->get__arguments_3();
		int32_t L_20 = V_0;
		NullCheck(L_19);
		int32_t L_21 = L_20;
		Type_t * L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_22)))
		{
			goto IL_0060;
		}
	}
	{
		return (bool)0;
	}

IL_0060:
	{
		int32_t L_23 = V_0;
		V_0 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_0064:
	{
		int32_t L_24 = V_0;
		DispatcherKey_t2110064572 * L_25 = ___x0;
		NullCheck(L_25);
		TypeU5BU5D_t3940880105* L_26 = L_25->get__arguments_3();
		NullCheck(L_26);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_26)->max_length)))))))
		{
			goto IL_0049;
		}
	}
	{
		return (bool)1;
	}
}
// System.Void Boo.Lang.Runtime.ExtensionRegistry::.ctor()
extern "C"  void ExtensionRegistry__ctor_m323844458 (ExtensionRegistry_t2424660641 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExtensionRegistry__ctor_m323844458_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1242660502 * L_0 = (List_1_t1242660502 *)il2cpp_codegen_object_new(List_1_t1242660502_il2cpp_TypeInfo_var);
		List_1__ctor_m1115569667(L_0, /*hidden argument*/List_1__ctor_m1115569667_MethodInfo_var);
		__this->set__extensions_0(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m297566312(L_1, /*hidden argument*/NULL);
		__this->set__classLock_1(L_1);
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo> Boo.Lang.Runtime.ExtensionRegistry::get_Extensions()
extern "C"  Il2CppObject* ExtensionRegistry_get_Extensions_m3605511872 (ExtensionRegistry_t2424660641 * __this, const MethodInfo* method)
{
	{
		List_1_t1242660502 * L_0 = __this->get__extensions_0();
		return L_0;
	}
}
// System.Void Boo.Lang.Runtime.RuntimeServices::.cctor()
extern "C"  void RuntimeServices__cctor_m1381641241 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices__cctor_m1381641241_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((RuntimeServices_t2098243569_StaticFields*)RuntimeServices_t2098243569_il2cpp_TypeInfo_var->static_fields)->set_NoArguments_0(((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)0)));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(RuntimeServices_t2098243569_0_0_0_var), /*hidden argument*/NULL);
		((RuntimeServices_t2098243569_StaticFields*)RuntimeServices_t2098243569_il2cpp_TypeInfo_var->static_fields)->set_RuntimeServicesType_1(L_0);
		DispatcherCache_t1010326087 * L_1 = (DispatcherCache_t1010326087 *)il2cpp_codegen_object_new(DispatcherCache_t1010326087_il2cpp_TypeInfo_var);
		DispatcherCache__ctor_m2156165103(L_1, /*hidden argument*/NULL);
		((RuntimeServices_t2098243569_StaticFields*)RuntimeServices_t2098243569_il2cpp_TypeInfo_var->static_fields)->set__cache_2(L_1);
		ExtensionRegistry_t2424660641 * L_2 = (ExtensionRegistry_t2424660641 *)il2cpp_codegen_object_new(ExtensionRegistry_t2424660641_il2cpp_TypeInfo_var);
		ExtensionRegistry__ctor_m323844458(L_2, /*hidden argument*/NULL);
		((RuntimeServices_t2098243569_StaticFields*)RuntimeServices_t2098243569_il2cpp_TypeInfo_var->static_fields)->set__extensions_3(L_2);
		bool L_3 = ((bool)1);
		Il2CppObject * L_4 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_3);
		((RuntimeServices_t2098243569_StaticFields*)RuntimeServices_t2098243569_il2cpp_TypeInfo_var->static_fields)->set_True_4(L_4);
		return;
	}
}
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher Boo.Lang.Runtime.RuntimeServices::GetDispatcher(System.Object,System.String,System.Type[],Boo.Lang.Runtime.DynamicDispatching.DispatcherCache/DispatcherFactory)
extern "C"  Dispatcher_t684365006 * RuntimeServices_GetDispatcher_m3922777458 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___target0, String_t* ___cacheKeyName1, TypeU5BU5D_t3940880105* ___cacheKeyTypes2, DispatcherFactory_t1014155341 * ___factory3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_GetDispatcher_m3922777458_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	DispatcherKey_t2110064572 * V_1 = NULL;
	Type_t * G_B2_0 = NULL;
	Type_t * G_B1_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		Type_t * L_1 = ((Type_t *)IsInstClass(L_0, Type_t_il2cpp_TypeInfo_var));
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0013;
		}
	}
	{
		Il2CppObject * L_2 = ___target0;
		NullCheck(L_2);
		Type_t * L_3 = Object_GetType_m88164663(L_2, /*hidden argument*/NULL);
		G_B2_0 = L_3;
	}

IL_0013:
	{
		V_0 = G_B2_0;
		Type_t * L_4 = V_0;
		String_t* L_5 = ___cacheKeyName1;
		TypeU5BU5D_t3940880105* L_6 = ___cacheKeyTypes2;
		DispatcherKey_t2110064572 * L_7 = (DispatcherKey_t2110064572 *)il2cpp_codegen_object_new(DispatcherKey_t2110064572_il2cpp_TypeInfo_var);
		DispatcherKey__ctor_m1810669420(L_7, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t2098243569_il2cpp_TypeInfo_var);
		DispatcherCache_t1010326087 * L_8 = ((RuntimeServices_t2098243569_StaticFields*)RuntimeServices_t2098243569_il2cpp_TypeInfo_var->static_fields)->get__cache_2();
		DispatcherKey_t2110064572 * L_9 = V_1;
		DispatcherFactory_t1014155341 * L_10 = ___factory3;
		NullCheck(L_8);
		Dispatcher_t684365006 * L_11 = DispatcherCache_Get_m3609792288(L_8, L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Object Boo.Lang.Runtime.RuntimeServices::Coerce(System.Object,System.Type)
extern "C"  Il2CppObject * RuntimeServices_Coerce_m1663619687 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, Type_t * ___toType1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_Coerce_m1663619687_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	Dispatcher_t684365006 * V_1 = NULL;
	U3CCoerceU3Ec__AnonStorey1D_t572148199 * V_2 = NULL;
	{
		U3CCoerceU3Ec__AnonStorey1D_t572148199 * L_0 = (U3CCoerceU3Ec__AnonStorey1D_t572148199 *)il2cpp_codegen_object_new(U3CCoerceU3Ec__AnonStorey1D_t572148199_il2cpp_TypeInfo_var);
		U3CCoerceU3Ec__AnonStorey1D__ctor_m2227718426(L_0, /*hidden argument*/NULL);
		V_2 = L_0;
		U3CCoerceU3Ec__AnonStorey1D_t572148199 * L_1 = V_2;
		Il2CppObject * L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_value_0(L_2);
		U3CCoerceU3Ec__AnonStorey1D_t572148199 * L_3 = V_2;
		Type_t * L_4 = ___toType1;
		NullCheck(L_3);
		L_3->set_toType_1(L_4);
		U3CCoerceU3Ec__AnonStorey1D_t572148199 * L_5 = V_2;
		NullCheck(L_5);
		Il2CppObject * L_6 = L_5->get_value_0();
		if (L_6)
		{
			goto IL_0021;
		}
	}
	{
		return NULL;
	}

IL_0021:
	{
		ObjectU5BU5D_t2843939325* L_7 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		U3CCoerceU3Ec__AnonStorey1D_t572148199 * L_8 = V_2;
		NullCheck(L_8);
		Type_t * L_9 = L_8->get_toType_1();
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_9);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_9);
		V_0 = L_7;
		U3CCoerceU3Ec__AnonStorey1D_t572148199 * L_10 = V_2;
		NullCheck(L_10);
		Il2CppObject * L_11 = L_10->get_value_0();
		TypeU5BU5D_t3940880105* L_12 = ((TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)1));
		U3CCoerceU3Ec__AnonStorey1D_t572148199 * L_13 = V_2;
		NullCheck(L_13);
		Type_t * L_14 = L_13->get_toType_1();
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_14);
		U3CCoerceU3Ec__AnonStorey1D_t572148199 * L_15 = V_2;
		IntPtr_t L_16;
		L_16.set_m_value_0((void*)(void*)U3CCoerceU3Ec__AnonStorey1D_U3CU3Em__15_m4262864175_MethodInfo_var);
		DispatcherFactory_t1014155341 * L_17 = (DispatcherFactory_t1014155341 *)il2cpp_codegen_object_new(DispatcherFactory_t1014155341_il2cpp_TypeInfo_var);
		DispatcherFactory__ctor_m3649217775(L_17, L_15, L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t2098243569_il2cpp_TypeInfo_var);
		Dispatcher_t684365006 * L_18 = RuntimeServices_GetDispatcher_m3922777458(NULL /*static, unused*/, L_11, _stringLiteral4123921170, L_12, L_17, /*hidden argument*/NULL);
		V_1 = L_18;
		Dispatcher_t684365006 * L_19 = V_1;
		U3CCoerceU3Ec__AnonStorey1D_t572148199 * L_20 = V_2;
		NullCheck(L_20);
		Il2CppObject * L_21 = L_20->get_value_0();
		ObjectU5BU5D_t2843939325* L_22 = V_0;
		NullCheck(L_19);
		Il2CppObject * L_23 = Dispatcher_Invoke_m1639676350(L_19, L_21, L_22, /*hidden argument*/NULL);
		return L_23;
	}
}
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher Boo.Lang.Runtime.RuntimeServices::CreateCoerceDispatcher(System.Object,System.Type)
extern "C"  Dispatcher_t684365006 * RuntimeServices_CreateCoerceDispatcher_m661106792 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, Type_t * ___toType1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_CreateCoerceDispatcher_m661106792_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	MethodInfo_t * V_1 = NULL;
	{
		Type_t * L_0 = ___toType1;
		Il2CppObject * L_1 = ___value0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(48 /* System.Boolean System.Type::IsInstanceOfType(System.Object) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)RuntimeServices_IdentityDispatcher_m1725786149_MethodInfo_var);
		Dispatcher_t684365006 * L_4 = (Dispatcher_t684365006 *)il2cpp_codegen_object_new(Dispatcher_t684365006_il2cpp_TypeInfo_var);
		Dispatcher__ctor_m4145559729(L_4, NULL, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0019:
	{
		Il2CppObject * L_5 = ___value0;
		if (!((Il2CppObject *)IsInst(L_5, ICoercible_t1130343077_il2cpp_TypeInfo_var)))
		{
			goto IL_0031;
		}
	}
	{
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)RuntimeServices_CoercibleDispatcher_m4058208960_MethodInfo_var);
		Dispatcher_t684365006 * L_7 = (Dispatcher_t684365006 *)il2cpp_codegen_object_new(Dispatcher_t684365006_il2cpp_TypeInfo_var);
		Dispatcher__ctor_m4145559729(L_7, NULL, L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0031:
	{
		Il2CppObject * L_8 = ___value0;
		NullCheck(L_8);
		Type_t * L_9 = Object_GetType_m88164663(L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		Type_t * L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t2098243569_il2cpp_TypeInfo_var);
		bool L_11 = RuntimeServices_IsPromotableNumeric_m2095247129(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0056;
		}
	}
	{
		Type_t * L_12 = ___toType1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t2098243569_il2cpp_TypeInfo_var);
		bool L_13 = RuntimeServices_IsPromotableNumeric_m2095247129(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0056;
		}
	}
	{
		Type_t * L_14 = V_0;
		Type_t * L_15 = ___toType1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t2098243569_il2cpp_TypeInfo_var);
		Dispatcher_t684365006 * L_16 = RuntimeServices_EmitPromotionDispatcher_m734050861(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		return L_16;
	}

IL_0056:
	{
		Type_t * L_17 = V_0;
		Type_t * L_18 = ___toType1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t2098243569_il2cpp_TypeInfo_var);
		MethodInfo_t * L_19 = RuntimeServices_FindImplicitConversionOperator_m2560683559(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		V_1 = L_19;
		MethodInfo_t * L_20 = V_1;
		if (L_20)
		{
			goto IL_0071;
		}
	}
	{
		IntPtr_t L_21;
		L_21.set_m_value_0((void*)(void*)RuntimeServices_IdentityDispatcher_m1725786149_MethodInfo_var);
		Dispatcher_t684365006 * L_22 = (Dispatcher_t684365006 *)il2cpp_codegen_object_new(Dispatcher_t684365006_il2cpp_TypeInfo_var);
		Dispatcher__ctor_m4145559729(L_22, NULL, L_21, /*hidden argument*/NULL);
		return L_22;
	}

IL_0071:
	{
		MethodInfo_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t2098243569_il2cpp_TypeInfo_var);
		Dispatcher_t684365006 * L_24 = RuntimeServices_EmitImplicitConversionDispatcher_m4072191859(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		return L_24;
	}
}
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher Boo.Lang.Runtime.RuntimeServices::EmitPromotionDispatcher(System.Type,System.Type)
extern "C"  Dispatcher_t684365006 * RuntimeServices_EmitPromotionDispatcher_m734050861 (Il2CppObject * __this /* static, unused */, Type_t * ___fromType0, Type_t * ___toType1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_EmitPromotionDispatcher_m734050861_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Dispatcher_t684365006_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(NumericPromotions_t3533651679_0_0_0_var), /*hidden argument*/NULL);
		ObjectU5BU5D_t2843939325* L_2 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral2755855817);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2755855817);
		ObjectU5BU5D_t2843939325* L_3 = L_2;
		Type_t * L_4 = ___fromType0;
		int32_t L_5 = Type_GetTypeCode_m480753082(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(TypeCode_t2987224087_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_7);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t2843939325* L_8 = L_3;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral3454777324);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3454777324);
		ObjectU5BU5D_t2843939325* L_9 = L_8;
		Type_t * L_10 = ___toType1;
		int32_t L_11 = Type_GetTypeCode_m480753082(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(TypeCode_t2987224087_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_13);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m2971454694(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		NullCheck(L_1);
		MethodInfo_t * L_15 = Type_GetMethod_m2019726356(L_1, L_14, /*hidden argument*/NULL);
		Delegate_t1188392813 * L_16 = Delegate_CreateDelegate_m2396489936(NULL /*static, unused*/, L_0, L_15, /*hidden argument*/NULL);
		return ((Dispatcher_t684365006 *)CastclassSealed(L_16, Dispatcher_t684365006_il2cpp_TypeInfo_var));
	}
}
// System.Boolean Boo.Lang.Runtime.RuntimeServices::IsPromotableNumeric(System.Type)
extern "C"  bool RuntimeServices_IsPromotableNumeric_m2095247129 (Il2CppObject * __this /* static, unused */, Type_t * ___fromType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_IsPromotableNumeric_m2095247129_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t * L_0 = ___fromType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_1 = Type_GetTypeCode_m480753082(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t2098243569_il2cpp_TypeInfo_var);
		bool L_2 = RuntimeServices_IsPromotableNumeric_m3673263760(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher Boo.Lang.Runtime.RuntimeServices::EmitImplicitConversionDispatcher(System.Reflection.MethodInfo)
extern "C"  Dispatcher_t684365006 * RuntimeServices_EmitImplicitConversionDispatcher_m4072191859 (Il2CppObject * __this /* static, unused */, MethodInfo_t * ___method0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_EmitImplicitConversionDispatcher_m4072191859_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t4009522197 * V_0 = NULL;
	{
		U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t4009522197 * L_0 = (U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t4009522197 *)il2cpp_codegen_object_new(U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t4009522197_il2cpp_TypeInfo_var);
		U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E__ctor_m1951951183(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t4009522197 * L_1 = V_0;
		MethodInfo_t * L_2 = ___method0;
		NullCheck(L_1);
		L_1->set_method_0(L_2);
		U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t4009522197 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_U3CU3Em__16_m940011970_MethodInfo_var);
		Dispatcher_t684365006 * L_5 = (Dispatcher_t684365006 *)il2cpp_codegen_object_new(Dispatcher_t684365006_il2cpp_TypeInfo_var);
		Dispatcher__ctor_m4145559729(L_5, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Object Boo.Lang.Runtime.RuntimeServices::CoercibleDispatcher(System.Object,System.Object[])
extern "C"  Il2CppObject * RuntimeServices_CoercibleDispatcher_m4058208960 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, ObjectU5BU5D_t2843939325* ___args1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_CoercibleDispatcher_m4058208960_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___o0;
		ObjectU5BU5D_t2843939325* L_1 = ___args1;
		NullCheck(L_1);
		int32_t L_2 = 0;
		Il2CppObject * L_3 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(((Il2CppObject *)Castclass(L_0, ICoercible_t1130343077_il2cpp_TypeInfo_var)));
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Type_t * >::Invoke(0 /* System.Object Boo.Lang.Runtime.ICoercible::Coerce(System.Type) */, ICoercible_t1130343077_il2cpp_TypeInfo_var, ((Il2CppObject *)Castclass(L_0, ICoercible_t1130343077_il2cpp_TypeInfo_var)), ((Type_t *)CastclassClass(L_3, Type_t_il2cpp_TypeInfo_var)));
		return L_4;
	}
}
// System.Object Boo.Lang.Runtime.RuntimeServices::IdentityDispatcher(System.Object,System.Object[])
extern "C"  Il2CppObject * RuntimeServices_IdentityDispatcher_m1725786149 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, ObjectU5BU5D_t2843939325* ___args1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___o0;
		return L_0;
	}
}
// System.Boolean Boo.Lang.Runtime.RuntimeServices::IsNumeric(System.TypeCode)
extern "C"  bool RuntimeServices_IsNumeric_m2879598828 (Il2CppObject * __this /* static, unused */, int32_t ___code0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___code0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (((int32_t)((int32_t)L_1-(int32_t)5)))
		{
			case 0:
			{
				goto IL_003d;
			}
			case 1:
			{
				goto IL_003b;
			}
			case 2:
			{
				goto IL_003f;
			}
			case 3:
			{
				goto IL_0045;
			}
			case 4:
			{
				goto IL_0041;
			}
			case 5:
			{
				goto IL_0047;
			}
			case 6:
			{
				goto IL_0043;
			}
			case 7:
			{
				goto IL_0049;
			}
			case 8:
			{
				goto IL_004b;
			}
			case 9:
			{
				goto IL_004d;
			}
			case 10:
			{
				goto IL_004f;
			}
		}
	}
	{
		goto IL_0051;
	}

IL_003b:
	{
		return (bool)1;
	}

IL_003d:
	{
		return (bool)1;
	}

IL_003f:
	{
		return (bool)1;
	}

IL_0041:
	{
		return (bool)1;
	}

IL_0043:
	{
		return (bool)1;
	}

IL_0045:
	{
		return (bool)1;
	}

IL_0047:
	{
		return (bool)1;
	}

IL_0049:
	{
		return (bool)1;
	}

IL_004b:
	{
		return (bool)1;
	}

IL_004d:
	{
		return (bool)1;
	}

IL_004f:
	{
		return (bool)1;
	}

IL_0051:
	{
		return (bool)0;
	}
}
// System.String Boo.Lang.Runtime.RuntimeServices::op_Addition(System.String,System.String)
extern "C"  String_t* RuntimeServices_op_Addition_m583005490 (Il2CppObject * __this /* static, unused */, String_t* ___lhs0, String_t* ___rhs1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_op_Addition_m583005490_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___lhs0;
		String_t* L_1 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m3937257545(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean Boo.Lang.Runtime.RuntimeServices::EqualityOperator(System.Object,System.Object)
extern "C"  bool RuntimeServices_EqualityOperator_m2384872086 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___lhs0, Il2CppObject * ___rhs1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_EqualityOperator_m2384872086_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Il2CppArray * V_2 = NULL;
	Il2CppArray * V_3 = NULL;
	int32_t G_B15_0 = 0;
	{
		Il2CppObject * L_0 = ___lhs0;
		Il2CppObject * L_1 = ___rhs1;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Il2CppObject * L_2 = ___lhs0;
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject * L_3 = ___rhs1;
		Il2CppObject * L_4 = ___lhs0;
		NullCheck(L_3);
		bool L_5 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_3, L_4);
		return L_5;
	}

IL_0017:
	{
		Il2CppObject * L_6 = ___rhs1;
		if (L_6)
		{
			goto IL_0025;
		}
	}
	{
		Il2CppObject * L_7 = ___lhs0;
		Il2CppObject * L_8 = ___rhs1;
		NullCheck(L_7);
		bool L_9 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_7, L_8);
		return L_9;
	}

IL_0025:
	{
		Il2CppObject * L_10 = ___lhs0;
		NullCheck(L_10);
		Type_t * L_11 = Object_GetType_m88164663(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_12 = Type_GetTypeCode_m480753082(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		Il2CppObject * L_13 = ___rhs1;
		NullCheck(L_13);
		Type_t * L_14 = Object_GetType_m88164663(L_13, /*hidden argument*/NULL);
		int32_t L_15 = Type_GetTypeCode_m480753082(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		int32_t L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t2098243569_il2cpp_TypeInfo_var);
		bool L_17 = RuntimeServices_IsNumeric_m2879598828(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_005d;
		}
	}
	{
		int32_t L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t2098243569_il2cpp_TypeInfo_var);
		bool L_19 = RuntimeServices_IsNumeric_m2879598828(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_005d;
		}
	}
	{
		Il2CppObject * L_20 = ___lhs0;
		int32_t L_21 = V_0;
		Il2CppObject * L_22 = ___rhs1;
		int32_t L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t2098243569_il2cpp_TypeInfo_var);
		bool L_24 = RuntimeServices_EqualityOperator_m2541246096(NULL /*static, unused*/, L_20, L_21, L_22, L_23, /*hidden argument*/NULL);
		return L_24;
	}

IL_005d:
	{
		Il2CppObject * L_25 = ___lhs0;
		V_2 = ((Il2CppArray *)IsInstClass(L_25, Il2CppArray_il2cpp_TypeInfo_var));
		Il2CppArray * L_26 = V_2;
		if (!L_26)
		{
			goto IL_007f;
		}
	}
	{
		Il2CppObject * L_27 = ___rhs1;
		V_3 = ((Il2CppArray *)IsInstClass(L_27, Il2CppArray_il2cpp_TypeInfo_var));
		Il2CppArray * L_28 = V_3;
		if (!L_28)
		{
			goto IL_007f;
		}
	}
	{
		Il2CppArray * L_29 = V_2;
		Il2CppArray * L_30 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t2098243569_il2cpp_TypeInfo_var);
		bool L_31 = RuntimeServices_ArrayEqualityImpl_m3673667702(NULL /*static, unused*/, L_29, L_30, /*hidden argument*/NULL);
		return L_31;
	}

IL_007f:
	{
		Il2CppObject * L_32 = ___lhs0;
		Il2CppObject * L_33 = ___rhs1;
		NullCheck(L_32);
		bool L_34 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_32, L_33);
		if (L_34)
		{
			goto IL_0094;
		}
	}
	{
		Il2CppObject * L_35 = ___rhs1;
		Il2CppObject * L_36 = ___lhs0;
		NullCheck(L_35);
		bool L_37 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_35, L_36);
		G_B15_0 = ((int32_t)(L_37));
		goto IL_0095;
	}

IL_0094:
	{
		G_B15_0 = 1;
	}

IL_0095:
	{
		return (bool)G_B15_0;
	}
}
// System.Boolean Boo.Lang.Runtime.RuntimeServices::ArrayEqualityImpl(System.Array,System.Array)
extern "C"  bool RuntimeServices_ArrayEqualityImpl_m3673667702 (Il2CppObject * __this /* static, unused */, Il2CppArray * ___lhs0, Il2CppArray * ___rhs1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_ArrayEqualityImpl_m3673667702_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Il2CppArray * L_0 = ___lhs0;
		NullCheck(L_0);
		int32_t L_1 = Array_get_Rank_m3448755881(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0018;
		}
	}
	{
		Il2CppArray * L_2 = ___rhs1;
		NullCheck(L_2);
		int32_t L_3 = Array_get_Rank_m3448755881(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		ArgumentException_t132251570 * L_4 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_4, _stringLiteral181402784, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		Il2CppArray * L_5 = ___lhs0;
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m21610649(L_5, /*hidden argument*/NULL);
		Il2CppArray * L_7 = ___rhs1;
		NullCheck(L_7);
		int32_t L_8 = Array_get_Length_m21610649(L_7, /*hidden argument*/NULL);
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0036;
		}
	}
	{
		return (bool)0;
	}

IL_0036:
	{
		V_0 = 0;
		goto IL_005b;
	}

IL_003d:
	{
		Il2CppArray * L_9 = ___lhs0;
		int32_t L_10 = V_0;
		NullCheck(L_9);
		Il2CppObject * L_11 = Array_GetValue_m2528546681(L_9, L_10, /*hidden argument*/NULL);
		Il2CppArray * L_12 = ___rhs1;
		int32_t L_13 = V_0;
		NullCheck(L_12);
		Il2CppObject * L_14 = Array_GetValue_m2528546681(L_12, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t2098243569_il2cpp_TypeInfo_var);
		bool L_15 = RuntimeServices_EqualityOperator_m2384872086(NULL /*static, unused*/, L_11, L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0057;
		}
	}
	{
		return (bool)0;
	}

IL_0057:
	{
		int32_t L_16 = V_0;
		V_0 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_17 = V_0;
		Il2CppArray * L_18 = ___lhs0;
		NullCheck(L_18);
		int32_t L_19 = Array_get_Length_m21610649(L_18, /*hidden argument*/NULL);
		if ((((int32_t)L_17) < ((int32_t)L_19)))
		{
			goto IL_003d;
		}
	}
	{
		return (bool)1;
	}
}
// System.TypeCode Boo.Lang.Runtime.RuntimeServices::GetConvertTypeCode(System.TypeCode,System.TypeCode)
extern "C"  int32_t RuntimeServices_GetConvertTypeCode_m597524256 (Il2CppObject * __this /* static, unused */, int32_t ___lhsTypeCode0, int32_t ___rhsTypeCode1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___lhsTypeCode0;
		if ((((int32_t)L_0) == ((int32_t)((int32_t)15))))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_1 = ___rhsTypeCode1;
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_0013;
		}
	}

IL_0010:
	{
		return (int32_t)(((int32_t)15));
	}

IL_0013:
	{
		int32_t L_2 = ___lhsTypeCode0;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)14))))
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_3 = ___rhsTypeCode1;
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)14)))))
		{
			goto IL_0026;
		}
	}

IL_0023:
	{
		return (int32_t)(((int32_t)14));
	}

IL_0026:
	{
		int32_t L_4 = ___lhsTypeCode0;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)13))))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_5 = ___rhsTypeCode1;
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_0039;
		}
	}

IL_0036:
	{
		return (int32_t)(((int32_t)13));
	}

IL_0039:
	{
		int32_t L_6 = ___lhsTypeCode0;
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_0065;
		}
	}
	{
		int32_t L_7 = ___rhsTypeCode1;
		if ((((int32_t)L_7) == ((int32_t)5)))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_8 = ___rhsTypeCode1;
		if ((((int32_t)L_8) == ((int32_t)7)))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_9 = ___rhsTypeCode1;
		if ((((int32_t)L_9) == ((int32_t)((int32_t)9))))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_10 = ___rhsTypeCode1;
		if ((!(((uint32_t)L_10) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0062;
		}
	}

IL_005f:
	{
		return (int32_t)(((int32_t)11));
	}

IL_0062:
	{
		return (int32_t)(((int32_t)12));
	}

IL_0065:
	{
		int32_t L_11 = ___rhsTypeCode1;
		if ((!(((uint32_t)L_11) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_0091;
		}
	}
	{
		int32_t L_12 = ___lhsTypeCode0;
		if ((((int32_t)L_12) == ((int32_t)5)))
		{
			goto IL_008b;
		}
	}
	{
		int32_t L_13 = ___lhsTypeCode0;
		if ((((int32_t)L_13) == ((int32_t)7)))
		{
			goto IL_008b;
		}
	}
	{
		int32_t L_14 = ___lhsTypeCode0;
		if ((((int32_t)L_14) == ((int32_t)((int32_t)9))))
		{
			goto IL_008b;
		}
	}
	{
		int32_t L_15 = ___lhsTypeCode0;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_008e;
		}
	}

IL_008b:
	{
		return (int32_t)(((int32_t)11));
	}

IL_008e:
	{
		return (int32_t)(((int32_t)12));
	}

IL_0091:
	{
		int32_t L_16 = ___lhsTypeCode0;
		if ((((int32_t)L_16) == ((int32_t)((int32_t)11))))
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_17 = ___rhsTypeCode1;
		if ((!(((uint32_t)L_17) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_00a4;
		}
	}

IL_00a1:
	{
		return (int32_t)(((int32_t)11));
	}

IL_00a4:
	{
		int32_t L_18 = ___lhsTypeCode0;
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_00c8;
		}
	}
	{
		int32_t L_19 = ___rhsTypeCode1;
		if ((((int32_t)L_19) == ((int32_t)5)))
		{
			goto IL_00c2;
		}
	}
	{
		int32_t L_20 = ___rhsTypeCode1;
		if ((((int32_t)L_20) == ((int32_t)7)))
		{
			goto IL_00c2;
		}
	}
	{
		int32_t L_21 = ___rhsTypeCode1;
		if ((!(((uint32_t)L_21) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_00c5;
		}
	}

IL_00c2:
	{
		return (int32_t)(((int32_t)11));
	}

IL_00c5:
	{
		return (int32_t)(((int32_t)10));
	}

IL_00c8:
	{
		int32_t L_22 = ___rhsTypeCode1;
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_00ec;
		}
	}
	{
		int32_t L_23 = ___lhsTypeCode0;
		if ((((int32_t)L_23) == ((int32_t)5)))
		{
			goto IL_00e6;
		}
	}
	{
		int32_t L_24 = ___lhsTypeCode0;
		if ((((int32_t)L_24) == ((int32_t)7)))
		{
			goto IL_00e6;
		}
	}
	{
		int32_t L_25 = ___lhsTypeCode0;
		if ((!(((uint32_t)L_25) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_00e9;
		}
	}

IL_00e6:
	{
		return (int32_t)(((int32_t)11));
	}

IL_00e9:
	{
		return (int32_t)(((int32_t)10));
	}

IL_00ec:
	{
		return (int32_t)(((int32_t)9));
	}
}
// System.Boolean Boo.Lang.Runtime.RuntimeServices::EqualityOperator(System.Object,System.TypeCode,System.Object,System.TypeCode)
extern "C"  bool RuntimeServices_EqualityOperator_m2541246096 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___lhs0, int32_t ___lhsTypeCode1, Il2CppObject * ___rhs2, int32_t ___rhsTypeCode3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_EqualityOperator_m2541246096_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	int32_t V_2 = 0;
	{
		Il2CppObject * L_0 = ___lhs0;
		V_0 = ((Il2CppObject *)Castclass(L_0, IConvertible_t2977365677_il2cpp_TypeInfo_var));
		Il2CppObject * L_1 = ___rhs2;
		V_1 = ((Il2CppObject *)Castclass(L_1, IConvertible_t2977365677_il2cpp_TypeInfo_var));
		int32_t L_2 = ___lhsTypeCode1;
		int32_t L_3 = ___rhsTypeCode3;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t2098243569_il2cpp_TypeInfo_var);
		int32_t L_4 = RuntimeServices_GetConvertTypeCode_m597524256(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = V_2;
		switch (((int32_t)((int32_t)L_5-(int32_t)((int32_t)10))))
		{
			case 0:
			{
				goto IL_0094;
			}
			case 1:
			{
				goto IL_0083;
			}
			case 2:
			{
				goto IL_0072;
			}
			case 3:
			{
				goto IL_0061;
			}
			case 4:
			{
				goto IL_0050;
			}
			case 5:
			{
				goto IL_003c;
			}
		}
	}
	{
		goto IL_00a5;
	}

IL_003c:
	{
		Il2CppObject * L_6 = V_0;
		NullCheck(L_6);
		Decimal_t2948259380  L_7 = InterfaceFuncInvoker1< Decimal_t2948259380 , Il2CppObject * >::Invoke(4 /* System.Decimal System.IConvertible::ToDecimal(System.IFormatProvider) */, IConvertible_t2977365677_il2cpp_TypeInfo_var, L_6, (Il2CppObject *)NULL);
		Il2CppObject * L_8 = V_1;
		NullCheck(L_8);
		Decimal_t2948259380  L_9 = InterfaceFuncInvoker1< Decimal_t2948259380 , Il2CppObject * >::Invoke(4 /* System.Decimal System.IConvertible::ToDecimal(System.IFormatProvider) */, IConvertible_t2977365677_il2cpp_TypeInfo_var, L_8, (Il2CppObject *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t2948259380_il2cpp_TypeInfo_var);
		bool L_10 = Decimal_op_Equality_m77262825(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/NULL);
		return L_10;
	}

IL_0050:
	{
		Il2CppObject * L_11 = V_0;
		NullCheck(L_11);
		double L_12 = InterfaceFuncInvoker1< double, Il2CppObject * >::Invoke(5 /* System.Double System.IConvertible::ToDouble(System.IFormatProvider) */, IConvertible_t2977365677_il2cpp_TypeInfo_var, L_11, (Il2CppObject *)NULL);
		Il2CppObject * L_13 = V_1;
		NullCheck(L_13);
		double L_14 = InterfaceFuncInvoker1< double, Il2CppObject * >::Invoke(5 /* System.Double System.IConvertible::ToDouble(System.IFormatProvider) */, IConvertible_t2977365677_il2cpp_TypeInfo_var, L_13, (Il2CppObject *)NULL);
		return (bool)((((double)L_12) == ((double)L_14))? 1 : 0);
	}

IL_0061:
	{
		Il2CppObject * L_15 = V_0;
		NullCheck(L_15);
		float L_16 = InterfaceFuncInvoker1< float, Il2CppObject * >::Invoke(10 /* System.Single System.IConvertible::ToSingle(System.IFormatProvider) */, IConvertible_t2977365677_il2cpp_TypeInfo_var, L_15, (Il2CppObject *)NULL);
		Il2CppObject * L_17 = V_1;
		NullCheck(L_17);
		float L_18 = InterfaceFuncInvoker1< float, Il2CppObject * >::Invoke(10 /* System.Single System.IConvertible::ToSingle(System.IFormatProvider) */, IConvertible_t2977365677_il2cpp_TypeInfo_var, L_17, (Il2CppObject *)NULL);
		return (bool)((((float)L_16) == ((float)L_18))? 1 : 0);
	}

IL_0072:
	{
		Il2CppObject * L_19 = V_0;
		NullCheck(L_19);
		uint64_t L_20 = InterfaceFuncInvoker1< uint64_t, Il2CppObject * >::Invoke(15 /* System.UInt64 System.IConvertible::ToUInt64(System.IFormatProvider) */, IConvertible_t2977365677_il2cpp_TypeInfo_var, L_19, (Il2CppObject *)NULL);
		Il2CppObject * L_21 = V_1;
		NullCheck(L_21);
		uint64_t L_22 = InterfaceFuncInvoker1< uint64_t, Il2CppObject * >::Invoke(15 /* System.UInt64 System.IConvertible::ToUInt64(System.IFormatProvider) */, IConvertible_t2977365677_il2cpp_TypeInfo_var, L_21, (Il2CppObject *)NULL);
		return (bool)((((int64_t)L_20) == ((int64_t)L_22))? 1 : 0);
	}

IL_0083:
	{
		Il2CppObject * L_23 = V_0;
		NullCheck(L_23);
		int64_t L_24 = InterfaceFuncInvoker1< int64_t, Il2CppObject * >::Invoke(8 /* System.Int64 System.IConvertible::ToInt64(System.IFormatProvider) */, IConvertible_t2977365677_il2cpp_TypeInfo_var, L_23, (Il2CppObject *)NULL);
		Il2CppObject * L_25 = V_1;
		NullCheck(L_25);
		int64_t L_26 = InterfaceFuncInvoker1< int64_t, Il2CppObject * >::Invoke(8 /* System.Int64 System.IConvertible::ToInt64(System.IFormatProvider) */, IConvertible_t2977365677_il2cpp_TypeInfo_var, L_25, (Il2CppObject *)NULL);
		return (bool)((((int64_t)L_24) == ((int64_t)L_26))? 1 : 0);
	}

IL_0094:
	{
		Il2CppObject * L_27 = V_0;
		NullCheck(L_27);
		uint32_t L_28 = InterfaceFuncInvoker1< uint32_t, Il2CppObject * >::Invoke(14 /* System.UInt32 System.IConvertible::ToUInt32(System.IFormatProvider) */, IConvertible_t2977365677_il2cpp_TypeInfo_var, L_27, (Il2CppObject *)NULL);
		Il2CppObject * L_29 = V_1;
		NullCheck(L_29);
		uint32_t L_30 = InterfaceFuncInvoker1< uint32_t, Il2CppObject * >::Invoke(14 /* System.UInt32 System.IConvertible::ToUInt32(System.IFormatProvider) */, IConvertible_t2977365677_il2cpp_TypeInfo_var, L_29, (Il2CppObject *)NULL);
		return (bool)((((int32_t)L_28) == ((int32_t)L_30))? 1 : 0);
	}

IL_00a5:
	{
		Il2CppObject * L_31 = V_0;
		NullCheck(L_31);
		int32_t L_32 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(7 /* System.Int32 System.IConvertible::ToInt32(System.IFormatProvider) */, IConvertible_t2977365677_il2cpp_TypeInfo_var, L_31, (Il2CppObject *)NULL);
		Il2CppObject * L_33 = V_1;
		NullCheck(L_33);
		int32_t L_34 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(7 /* System.Int32 System.IConvertible::ToInt32(System.IFormatProvider) */, IConvertible_t2977365677_il2cpp_TypeInfo_var, L_33, (Il2CppObject *)NULL);
		return (bool)((((int32_t)L_32) == ((int32_t)L_34))? 1 : 0);
	}
}
// System.Boolean Boo.Lang.Runtime.RuntimeServices::IsPromotableNumeric(System.TypeCode)
extern "C"  bool RuntimeServices_IsPromotableNumeric_m3673263760 (Il2CppObject * __this /* static, unused */, int32_t ___code0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___code0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (((int32_t)((int32_t)L_1-(int32_t)3)))
		{
			case 0:
			{
				goto IL_0057;
			}
			case 1:
			{
				goto IL_005b;
			}
			case 2:
			{
				goto IL_0045;
			}
			case 3:
			{
				goto IL_0043;
			}
			case 4:
			{
				goto IL_0047;
			}
			case 5:
			{
				goto IL_004d;
			}
			case 6:
			{
				goto IL_0049;
			}
			case 7:
			{
				goto IL_004f;
			}
			case 8:
			{
				goto IL_004b;
			}
			case 9:
			{
				goto IL_0051;
			}
			case 10:
			{
				goto IL_0053;
			}
			case 11:
			{
				goto IL_0055;
			}
			case 12:
			{
				goto IL_0059;
			}
		}
	}
	{
		goto IL_005d;
	}

IL_0043:
	{
		return (bool)1;
	}

IL_0045:
	{
		return (bool)1;
	}

IL_0047:
	{
		return (bool)1;
	}

IL_0049:
	{
		return (bool)1;
	}

IL_004b:
	{
		return (bool)1;
	}

IL_004d:
	{
		return (bool)1;
	}

IL_004f:
	{
		return (bool)1;
	}

IL_0051:
	{
		return (bool)1;
	}

IL_0053:
	{
		return (bool)1;
	}

IL_0055:
	{
		return (bool)1;
	}

IL_0057:
	{
		return (bool)1;
	}

IL_0059:
	{
		return (bool)1;
	}

IL_005b:
	{
		return (bool)1;
	}

IL_005d:
	{
		return (bool)0;
	}
}
// System.Reflection.MethodInfo Boo.Lang.Runtime.RuntimeServices::FindImplicitConversionOperator(System.Type,System.Type)
extern "C"  MethodInfo_t * RuntimeServices_FindImplicitConversionOperator_m2560683559 (Il2CppObject * __this /* static, unused */, Type_t * ___from0, Type_t * ___to1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_FindImplicitConversionOperator_m2560683559_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	MethodInfo_t * G_B3_0 = NULL;
	MethodInfo_t * G_B1_0 = NULL;
	MethodInfo_t * G_B2_0 = NULL;
	{
		Type_t * L_0 = ___from0;
		NullCheck(L_0);
		MethodInfoU5BU5D_t2572182361* L_1 = VirtFuncInvoker1< MethodInfoU5BU5D_t2572182361*, int32_t >::Invoke(70 /* System.Reflection.MethodInfo[] System.Type::GetMethods(System.Reflection.BindingFlags) */, L_0, ((int32_t)88));
		Type_t * L_2 = ___from0;
		Type_t * L_3 = ___to1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t2098243569_il2cpp_TypeInfo_var);
		MethodInfo_t * L_4 = RuntimeServices_FindImplicitConversionMethod_m4176567206(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_1, L_2, L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = L_4;
		G_B1_0 = L_5;
		if (L_5)
		{
			G_B3_0 = L_5;
			goto IL_0038;
		}
	}
	{
		Type_t * L_6 = ___to1;
		NullCheck(L_6);
		MethodInfoU5BU5D_t2572182361* L_7 = VirtFuncInvoker1< MethodInfoU5BU5D_t2572182361*, int32_t >::Invoke(70 /* System.Reflection.MethodInfo[] System.Type::GetMethods(System.Reflection.BindingFlags) */, L_6, ((int32_t)88));
		Type_t * L_8 = ___from0;
		Type_t * L_9 = ___to1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t2098243569_il2cpp_TypeInfo_var);
		MethodInfo_t * L_10 = RuntimeServices_FindImplicitConversionMethod_m4176567206(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_7, L_8, L_9, /*hidden argument*/NULL);
		MethodInfo_t * L_11 = L_10;
		G_B2_0 = L_11;
		if (L_11)
		{
			G_B3_0 = L_11;
			goto IL_0038;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t2098243569_il2cpp_TypeInfo_var);
		Il2CppObject* L_12 = RuntimeServices_GetExtensionMethods_m2018979233(NULL /*static, unused*/, /*hidden argument*/NULL);
		Type_t * L_13 = ___from0;
		Type_t * L_14 = ___to1;
		MethodInfo_t * L_15 = RuntimeServices_FindImplicitConversionMethod_m4176567206(NULL /*static, unused*/, L_12, L_13, L_14, /*hidden argument*/NULL);
		G_B3_0 = L_15;
	}

IL_0038:
	{
		return G_B3_0;
	}
}
// System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo> Boo.Lang.Runtime.RuntimeServices::GetExtensionMethods()
extern "C"  Il2CppObject* RuntimeServices_GetExtensionMethods_m2018979233 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_GetExtensionMethods_m2018979233_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetExtensionMethodsU3Ec__IteratorC_t3368530435 * V_0 = NULL;
	{
		U3CGetExtensionMethodsU3Ec__IteratorC_t3368530435 * L_0 = (U3CGetExtensionMethodsU3Ec__IteratorC_t3368530435 *)il2cpp_codegen_object_new(U3CGetExtensionMethodsU3Ec__IteratorC_t3368530435_il2cpp_TypeInfo_var);
		U3CGetExtensionMethodsU3Ec__IteratorC__ctor_m1217043206(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetExtensionMethodsU3Ec__IteratorC_t3368530435 * L_1 = V_0;
		U3CGetExtensionMethodsU3Ec__IteratorC_t3368530435 * L_2 = L_1;
		NullCheck(L_2);
		L_2->set_U24PC_2(((int32_t)-2));
		return L_2;
	}
}
// System.Reflection.MethodInfo Boo.Lang.Runtime.RuntimeServices::FindImplicitConversionMethod(System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>,System.Type,System.Type)
extern "C"  MethodInfo_t * RuntimeServices_FindImplicitConversionMethod_m4176567206 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___candidates0, Type_t * ___from1, Type_t * ___to2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_FindImplicitConversionMethod_m4176567206_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	ParameterInfoU5BU5D_t390618515* V_2 = NULL;
	MethodInfo_t * V_3 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___candidates0;
		NullCheck(L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>::GetEnumerator() */, IEnumerable_1_t857479137_il2cpp_TypeInfo_var, L_0);
		V_1 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0072;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck(L_2);
			MethodInfo_t * L_3 = InterfaceFuncInvoker0< MethodInfo_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Reflection.MethodInfo>::get_Current() */, IEnumerator_1_t2310196716_il2cpp_TypeInfo_var, L_2);
			V_0 = L_3;
			MethodInfo_t * L_4 = V_0;
			NullCheck(L_4);
			String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, L_4);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_6 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_5, _stringLiteral3306367446, /*hidden argument*/NULL);
			if (!L_6)
			{
				goto IL_002d;
			}
		}

IL_0028:
		{
			goto IL_0072;
		}

IL_002d:
		{
			MethodInfo_t * L_7 = V_0;
			NullCheck(L_7);
			Type_t * L_8 = VirtFuncInvoker0< Type_t * >::Invoke(36 /* System.Type System.Reflection.MethodInfo::get_ReturnType() */, L_7);
			Type_t * L_9 = ___to2;
			if ((((Il2CppObject*)(Type_t *)L_8) == ((Il2CppObject*)(Type_t *)L_9)))
			{
				goto IL_003e;
			}
		}

IL_0039:
		{
			goto IL_0072;
		}

IL_003e:
		{
			MethodInfo_t * L_10 = V_0;
			NullCheck(L_10);
			ParameterInfoU5BU5D_t390618515* L_11 = VirtFuncInvoker0< ParameterInfoU5BU5D_t390618515* >::Invoke(16 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_10);
			V_2 = L_11;
			ParameterInfoU5BU5D_t390618515* L_12 = V_2;
			NullCheck(L_12);
			if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length))))) == ((int32_t)1)))
			{
				goto IL_0053;
			}
		}

IL_004e:
		{
			goto IL_0072;
		}

IL_0053:
		{
			ParameterInfoU5BU5D_t390618515* L_13 = V_2;
			NullCheck(L_13);
			int32_t L_14 = 0;
			ParameterInfo_t1861056598 * L_15 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
			NullCheck(L_15);
			Type_t * L_16 = VirtFuncInvoker0< Type_t * >::Invoke(7 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_15);
			Type_t * L_17 = ___from1;
			NullCheck(L_16);
			bool L_18 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_16, L_17);
			if (L_18)
			{
				goto IL_006b;
			}
		}

IL_0066:
		{
			goto IL_0072;
		}

IL_006b:
		{
			MethodInfo_t * L_19 = V_0;
			V_3 = L_19;
			IL2CPP_LEAVE(0x8F, FINALLY_0082);
		}

IL_0072:
		{
			Il2CppObject* L_20 = V_1;
			NullCheck(L_20);
			bool L_21 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_20);
			if (L_21)
			{
				goto IL_000c;
			}
		}

IL_007d:
		{
			IL2CPP_LEAVE(0x8D, FINALLY_0082);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0082;
	}

FINALLY_0082:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_22 = V_1;
			if (L_22)
			{
				goto IL_0086;
			}
		}

IL_0085:
		{
			IL2CPP_END_FINALLY(130)
		}

IL_0086:
		{
			Il2CppObject* L_23 = V_1;
			NullCheck(L_23);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_23);
			IL2CPP_END_FINALLY(130)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(130)
	{
		IL2CPP_JUMP_TBL(0x8F, IL_008f)
		IL2CPP_JUMP_TBL(0x8D, IL_008d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_008d:
	{
		return (MethodInfo_t *)NULL;
	}

IL_008f:
	{
		MethodInfo_t * L_24 = V_3;
		return L_24;
	}
}
// System.Void Boo.Lang.Runtime.RuntimeServices/<Coerce>c__AnonStorey1D::.ctor()
extern "C"  void U3CCoerceU3Ec__AnonStorey1D__ctor_m2227718426 (U3CCoerceU3Ec__AnonStorey1D_t572148199 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher Boo.Lang.Runtime.RuntimeServices/<Coerce>c__AnonStorey1D::<>m__15()
extern "C"  Dispatcher_t684365006 * U3CCoerceU3Ec__AnonStorey1D_U3CU3Em__15_m4262864175 (U3CCoerceU3Ec__AnonStorey1D_t572148199 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCoerceU3Ec__AnonStorey1D_U3CU3Em__15_m4262864175_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_value_0();
		Type_t * L_1 = __this->get_toType_1();
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t2098243569_il2cpp_TypeInfo_var);
		Dispatcher_t684365006 * L_2 = RuntimeServices_CreateCoerceDispatcher_m661106792(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void Boo.Lang.Runtime.RuntimeServices/<EmitImplicitConversionDispatcher>c__AnonStorey1E::.ctor()
extern "C"  void U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E__ctor_m1951951183 (U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t4009522197 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Boo.Lang.Runtime.RuntimeServices/<EmitImplicitConversionDispatcher>c__AnonStorey1E::<>m__16(System.Object,System.Object[])
extern "C"  Il2CppObject * U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_U3CU3Em__16_m940011970 (U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t4009522197 * __this, Il2CppObject * ___target0, ObjectU5BU5D_t2843939325* ___args1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_U3CU3Em__16_m940011970_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MethodInfo_t * L_0 = __this->get_method_0();
		ObjectU5BU5D_t2843939325* L_1 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_2 = ___target0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
		NullCheck(L_0);
		Il2CppObject * L_3 = MethodBase_Invoke_m1776411915(L_0, NULL, L_1, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC::.ctor()
extern "C"  void U3CGetExtensionMethodsU3Ec__IteratorC__ctor_m1217043206 (U3CGetExtensionMethodsU3Ec__IteratorC_t3368530435 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC::System.Collections.Generic.IEnumerator<System.Reflection.MethodInfo>.get_Current()
extern "C"  MethodInfo_t * U3CGetExtensionMethodsU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MethodInfoU3E_get_Current_m3252140135 (U3CGetExtensionMethodsU3Ec__IteratorC_t3368530435 * __this, const MethodInfo* method)
{
	{
		MethodInfo_t * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetExtensionMethodsU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m4085587551 (U3CGetExtensionMethodsU3Ec__IteratorC_t3368530435 * __this, const MethodInfo* method)
{
	{
		MethodInfo_t * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Collections.IEnumerator Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CGetExtensionMethodsU3Ec__IteratorC_System_Collections_IEnumerable_GetEnumerator_m907415348 (U3CGetExtensionMethodsU3Ec__IteratorC_t3368530435 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CGetExtensionMethodsU3Ec__IteratorC_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MethodInfoU3E_GetEnumerator_m1148048048(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Reflection.MethodInfo> Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC::System.Collections.Generic.IEnumerable<System.Reflection.MethodInfo>.GetEnumerator()
extern "C"  Il2CppObject* U3CGetExtensionMethodsU3Ec__IteratorC_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MethodInfoU3E_GetEnumerator_m1148048048 (U3CGetExtensionMethodsU3Ec__IteratorC_t3368530435 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetExtensionMethodsU3Ec__IteratorC_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MethodInfoU3E_GetEnumerator_m1148048048_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t* L_0 = __this->get_address_of_U24PC_2();
		int32_t L_1 = Interlocked_CompareExchange_m3023855514(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CGetExtensionMethodsU3Ec__IteratorC_t3368530435 * L_2 = (U3CGetExtensionMethodsU3Ec__IteratorC_t3368530435 *)il2cpp_codegen_object_new(U3CGetExtensionMethodsU3Ec__IteratorC_t3368530435_il2cpp_TypeInfo_var);
		U3CGetExtensionMethodsU3Ec__IteratorC__ctor_m1217043206(L_2, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC::MoveNext()
extern "C"  bool U3CGetExtensionMethodsU3Ec__IteratorC_MoveNext_m4162423597 (U3CGetExtensionMethodsU3Ec__IteratorC_t3368530435 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetExtensionMethodsU3Ec__IteratorC_MoveNext_m4162423597_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0023;
			}
			case 1:
			{
				goto IL_003b;
			}
		}
	}
	{
		goto IL_00c2;
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t2098243569_il2cpp_TypeInfo_var);
		ExtensionRegistry_t2424660641 * L_2 = ((RuntimeServices_t2098243569_StaticFields*)RuntimeServices_t2098243569_il2cpp_TypeInfo_var->static_fields)->get__extensions_3();
		NullCheck(L_2);
		Il2CppObject* L_3 = ExtensionRegistry_get_Extensions_m3605511872(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Il2CppObject* L_4 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>::GetEnumerator() */, IEnumerable_1_t2359854630_il2cpp_TypeInfo_var, L_3);
		__this->set_U3CU24s_49U3E__0_0(L_4);
		V_0 = ((int32_t)-3);
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_5 = V_0;
			switch (((int32_t)((int32_t)L_5-(int32_t)1)))
			{
				case 0:
				{
					goto IL_008d;
				}
			}
		}

IL_0047:
		{
			goto IL_008d;
		}

IL_004c:
		{
			Il2CppObject* L_6 = __this->get_U3CU24s_49U3E__0_0();
			NullCheck(L_6);
			MemberInfo_t * L_7 = InterfaceFuncInvoker0< MemberInfo_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Reflection.MemberInfo>::get_Current() */, IEnumerator_1_t3812572209_il2cpp_TypeInfo_var, L_6);
			__this->set_U3CmemberU3E__1_1(L_7);
			MemberInfo_t * L_8 = __this->get_U3CmemberU3E__1_1();
			NullCheck(L_8);
			int32_t L_9 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Reflection.MemberTypes System.Reflection.MemberInfo::get_MemberType() */, L_8);
			if ((!(((uint32_t)L_9) == ((uint32_t)8))))
			{
				goto IL_008d;
			}
		}

IL_006e:
		{
			MemberInfo_t * L_10 = __this->get_U3CmemberU3E__1_1();
			__this->set_U24current_3(((MethodInfo_t *)CastclassClass(L_10, MethodInfo_t_il2cpp_TypeInfo_var)));
			__this->set_U24PC_2(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xC4, FINALLY_00a2);
		}

IL_008d:
		{
			Il2CppObject* L_11 = __this->get_U3CU24s_49U3E__0_0();
			NullCheck(L_11);
			bool L_12 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_11);
			if (L_12)
			{
				goto IL_004c;
			}
		}

IL_009d:
		{
			IL2CPP_LEAVE(0xBB, FINALLY_00a2);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_00a2;
	}

FINALLY_00a2:
	{ // begin finally (depth: 1)
		{
			bool L_13 = V_1;
			if (!L_13)
			{
				goto IL_00a6;
			}
		}

IL_00a5:
		{
			IL2CPP_END_FINALLY(162)
		}

IL_00a6:
		{
			Il2CppObject* L_14 = __this->get_U3CU24s_49U3E__0_0();
			if (L_14)
			{
				goto IL_00af;
			}
		}

IL_00ae:
		{
			IL2CPP_END_FINALLY(162)
		}

IL_00af:
		{
			Il2CppObject* L_15 = __this->get_U3CU24s_49U3E__0_0();
			NullCheck(L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_15);
			IL2CPP_END_FINALLY(162)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(162)
	{
		IL2CPP_JUMP_TBL(0xC4, IL_00c4)
		IL2CPP_JUMP_TBL(0xBB, IL_00bb)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_00bb:
	{
		__this->set_U24PC_2((-1));
	}

IL_00c2:
	{
		return (bool)0;
	}

IL_00c4:
	{
		return (bool)1;
	}
	// Dead block : IL_00c6: ldloc.2
}
// System.Void Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC::Dispose()
extern "C"  void U3CGetExtensionMethodsU3Ec__IteratorC_Dispose_m3192498793 (U3CGetExtensionMethodsU3Ec__IteratorC_t3368530435 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetExtensionMethodsU3Ec__IteratorC_Dispose_m3192498793_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_003b;
			}
			case 1:
			{
				goto IL_0021;
			}
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = __this->get_U3CU24s_49U3E__0_0();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = __this->get_U3CU24s_49U3E__0_0();
			NullCheck(L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC::Reset()
extern "C"  void U3CGetExtensionMethodsU3Ec__IteratorC_Reset_m865776842 (U3CGetExtensionMethodsU3Ec__IteratorC_t3368530435 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetExtensionMethodsU3Ec__IteratorC_Reset_m865776842_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
