﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Description.WstRequestSecurityToken
struct WstRequestSecurityToken_t4025507634;
// System.Xml.XmlDictionaryReader
struct XmlDictionaryReader_t1044334689;
// System.IdentityModel.Selectors.SecurityTokenSerializer
struct SecurityTokenSerializer_t972969391;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.WSTrustRequestSecurityTokenReader
struct  WSTrustRequestSecurityTokenReader_t3796747390  : public Il2CppObject
{
public:
	// System.ServiceModel.Description.WstRequestSecurityToken System.ServiceModel.Description.WSTrustRequestSecurityTokenReader::req
	WstRequestSecurityToken_t4025507634 * ___req_0;
	// System.Xml.XmlDictionaryReader System.ServiceModel.Description.WSTrustRequestSecurityTokenReader::reader
	XmlDictionaryReader_t1044334689 * ___reader_1;
	// System.IdentityModel.Selectors.SecurityTokenSerializer System.ServiceModel.Description.WSTrustRequestSecurityTokenReader::serializer
	SecurityTokenSerializer_t972969391 * ___serializer_2;

public:
	inline static int32_t get_offset_of_req_0() { return static_cast<int32_t>(offsetof(WSTrustRequestSecurityTokenReader_t3796747390, ___req_0)); }
	inline WstRequestSecurityToken_t4025507634 * get_req_0() const { return ___req_0; }
	inline WstRequestSecurityToken_t4025507634 ** get_address_of_req_0() { return &___req_0; }
	inline void set_req_0(WstRequestSecurityToken_t4025507634 * value)
	{
		___req_0 = value;
		Il2CppCodeGenWriteBarrier(&___req_0, value);
	}

	inline static int32_t get_offset_of_reader_1() { return static_cast<int32_t>(offsetof(WSTrustRequestSecurityTokenReader_t3796747390, ___reader_1)); }
	inline XmlDictionaryReader_t1044334689 * get_reader_1() const { return ___reader_1; }
	inline XmlDictionaryReader_t1044334689 ** get_address_of_reader_1() { return &___reader_1; }
	inline void set_reader_1(XmlDictionaryReader_t1044334689 * value)
	{
		___reader_1 = value;
		Il2CppCodeGenWriteBarrier(&___reader_1, value);
	}

	inline static int32_t get_offset_of_serializer_2() { return static_cast<int32_t>(offsetof(WSTrustRequestSecurityTokenReader_t3796747390, ___serializer_2)); }
	inline SecurityTokenSerializer_t972969391 * get_serializer_2() const { return ___serializer_2; }
	inline SecurityTokenSerializer_t972969391 ** get_address_of_serializer_2() { return &___serializer_2; }
	inline void set_serializer_2(SecurityTokenSerializer_t972969391 * value)
	{
		___serializer_2 = value;
		Il2CppCodeGenWriteBarrier(&___serializer_2, value);
	}
};

struct WSTrustRequestSecurityTokenReader_t3796747390_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Description.WSTrustRequestSecurityTokenReader::<>f__switch$map16
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map16_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Description.WSTrustRequestSecurityTokenReader::<>f__switch$map17
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map17_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Description.WSTrustRequestSecurityTokenReader::<>f__switch$map18
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map18_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map16_3() { return static_cast<int32_t>(offsetof(WSTrustRequestSecurityTokenReader_t3796747390_StaticFields, ___U3CU3Ef__switchU24map16_3)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map16_3() const { return ___U3CU3Ef__switchU24map16_3; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map16_3() { return &___U3CU3Ef__switchU24map16_3; }
	inline void set_U3CU3Ef__switchU24map16_3(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map16_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map16_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map17_4() { return static_cast<int32_t>(offsetof(WSTrustRequestSecurityTokenReader_t3796747390_StaticFields, ___U3CU3Ef__switchU24map17_4)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map17_4() const { return ___U3CU3Ef__switchU24map17_4; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map17_4() { return &___U3CU3Ef__switchU24map17_4; }
	inline void set_U3CU3Ef__switchU24map17_4(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map17_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map17_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map18_5() { return static_cast<int32_t>(offsetof(WSTrustRequestSecurityTokenReader_t3796747390_StaticFields, ___U3CU3Ef__switchU24map18_5)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map18_5() const { return ___U3CU3Ef__switchU24map18_5; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map18_5() { return &___U3CU3Ef__switchU24map18_5; }
	inline void set_U3CU3Ef__switchU24map18_5(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map18_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map18_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
