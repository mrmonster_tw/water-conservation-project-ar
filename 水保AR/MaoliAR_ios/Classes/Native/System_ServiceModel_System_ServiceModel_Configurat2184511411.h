﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3156163955.h"

// System.Type[]
struct TypeU5BU5D_t3940880105;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.ClientSection
struct  ClientSection_t2184511411  : public ConfigurationSection_t3156163955
{
public:

public:
};

struct ClientSection_t2184511411_StaticFields
{
public:
	// System.Type[] System.ServiceModel.Configuration.ClientSection::_defaultPolicyImporters
	TypeU5BU5D_t3940880105* ____defaultPolicyImporters_17;
	// System.Type[] System.ServiceModel.Configuration.ClientSection::_defaultWsdlImporters
	TypeU5BU5D_t3940880105* ____defaultWsdlImporters_18;

public:
	inline static int32_t get_offset_of__defaultPolicyImporters_17() { return static_cast<int32_t>(offsetof(ClientSection_t2184511411_StaticFields, ____defaultPolicyImporters_17)); }
	inline TypeU5BU5D_t3940880105* get__defaultPolicyImporters_17() const { return ____defaultPolicyImporters_17; }
	inline TypeU5BU5D_t3940880105** get_address_of__defaultPolicyImporters_17() { return &____defaultPolicyImporters_17; }
	inline void set__defaultPolicyImporters_17(TypeU5BU5D_t3940880105* value)
	{
		____defaultPolicyImporters_17 = value;
		Il2CppCodeGenWriteBarrier(&____defaultPolicyImporters_17, value);
	}

	inline static int32_t get_offset_of__defaultWsdlImporters_18() { return static_cast<int32_t>(offsetof(ClientSection_t2184511411_StaticFields, ____defaultWsdlImporters_18)); }
	inline TypeU5BU5D_t3940880105* get__defaultWsdlImporters_18() const { return ____defaultWsdlImporters_18; }
	inline TypeU5BU5D_t3940880105** get_address_of__defaultWsdlImporters_18() { return &____defaultWsdlImporters_18; }
	inline void set__defaultWsdlImporters_18(TypeU5BU5D_t3940880105* value)
	{
		____defaultWsdlImporters_18 = value;
		Il2CppCodeGenWriteBarrier(&____defaultWsdlImporters_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
