﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Descriptio1978373106.h"

// System.IdentityModel.Selectors.SecurityTokenSerializer
struct SecurityTokenSerializer_t972969391;
// System.String
struct String_t;
// System.ServiceModel.Security.Tokens.SecurityContextSecurityToken
struct SecurityContextSecurityToken_t3624779732;
// System.IdentityModel.Tokens.SecurityKeyIdentifierClause
struct SecurityKeyIdentifierClause_t1943429813;
// System.Object
struct Il2CppObject;
// System.ServiceModel.Description.WstLifetime
struct WstLifetime_t4147158307;
// System.Byte[]
struct ByteU5BU5D_t4116647657;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.WstRequestSecurityTokenResponse
struct  WstRequestSecurityTokenResponse_t889264267  : public WstRequestSecurityTokenBase_t1978373106
{
public:
	// System.IdentityModel.Selectors.SecurityTokenSerializer System.ServiceModel.Description.WstRequestSecurityTokenResponse::serializer
	SecurityTokenSerializer_t972969391 * ___serializer_9;
	// System.String System.ServiceModel.Description.WstRequestSecurityTokenResponse::TokenType
	String_t* ___TokenType_10;
	// System.ServiceModel.Security.Tokens.SecurityContextSecurityToken System.ServiceModel.Description.WstRequestSecurityTokenResponse::RequestedSecurityToken
	SecurityContextSecurityToken_t3624779732 * ___RequestedSecurityToken_11;
	// System.IdentityModel.Tokens.SecurityKeyIdentifierClause System.ServiceModel.Description.WstRequestSecurityTokenResponse::RequestedAttachedReference
	SecurityKeyIdentifierClause_t1943429813 * ___RequestedAttachedReference_12;
	// System.IdentityModel.Tokens.SecurityKeyIdentifierClause System.ServiceModel.Description.WstRequestSecurityTokenResponse::RequestedUnattachedReference
	SecurityKeyIdentifierClause_t1943429813 * ___RequestedUnattachedReference_13;
	// System.Object System.ServiceModel.Description.WstRequestSecurityTokenResponse::RequestedProofToken
	Il2CppObject * ___RequestedProofToken_14;
	// System.ServiceModel.Description.WstLifetime System.ServiceModel.Description.WstRequestSecurityTokenResponse::Lifetime
	WstLifetime_t4147158307 * ___Lifetime_15;
	// System.Byte[] System.ServiceModel.Description.WstRequestSecurityTokenResponse::Authenticator
	ByteU5BU5D_t4116647657* ___Authenticator_16;

public:
	inline static int32_t get_offset_of_serializer_9() { return static_cast<int32_t>(offsetof(WstRequestSecurityTokenResponse_t889264267, ___serializer_9)); }
	inline SecurityTokenSerializer_t972969391 * get_serializer_9() const { return ___serializer_9; }
	inline SecurityTokenSerializer_t972969391 ** get_address_of_serializer_9() { return &___serializer_9; }
	inline void set_serializer_9(SecurityTokenSerializer_t972969391 * value)
	{
		___serializer_9 = value;
		Il2CppCodeGenWriteBarrier(&___serializer_9, value);
	}

	inline static int32_t get_offset_of_TokenType_10() { return static_cast<int32_t>(offsetof(WstRequestSecurityTokenResponse_t889264267, ___TokenType_10)); }
	inline String_t* get_TokenType_10() const { return ___TokenType_10; }
	inline String_t** get_address_of_TokenType_10() { return &___TokenType_10; }
	inline void set_TokenType_10(String_t* value)
	{
		___TokenType_10 = value;
		Il2CppCodeGenWriteBarrier(&___TokenType_10, value);
	}

	inline static int32_t get_offset_of_RequestedSecurityToken_11() { return static_cast<int32_t>(offsetof(WstRequestSecurityTokenResponse_t889264267, ___RequestedSecurityToken_11)); }
	inline SecurityContextSecurityToken_t3624779732 * get_RequestedSecurityToken_11() const { return ___RequestedSecurityToken_11; }
	inline SecurityContextSecurityToken_t3624779732 ** get_address_of_RequestedSecurityToken_11() { return &___RequestedSecurityToken_11; }
	inline void set_RequestedSecurityToken_11(SecurityContextSecurityToken_t3624779732 * value)
	{
		___RequestedSecurityToken_11 = value;
		Il2CppCodeGenWriteBarrier(&___RequestedSecurityToken_11, value);
	}

	inline static int32_t get_offset_of_RequestedAttachedReference_12() { return static_cast<int32_t>(offsetof(WstRequestSecurityTokenResponse_t889264267, ___RequestedAttachedReference_12)); }
	inline SecurityKeyIdentifierClause_t1943429813 * get_RequestedAttachedReference_12() const { return ___RequestedAttachedReference_12; }
	inline SecurityKeyIdentifierClause_t1943429813 ** get_address_of_RequestedAttachedReference_12() { return &___RequestedAttachedReference_12; }
	inline void set_RequestedAttachedReference_12(SecurityKeyIdentifierClause_t1943429813 * value)
	{
		___RequestedAttachedReference_12 = value;
		Il2CppCodeGenWriteBarrier(&___RequestedAttachedReference_12, value);
	}

	inline static int32_t get_offset_of_RequestedUnattachedReference_13() { return static_cast<int32_t>(offsetof(WstRequestSecurityTokenResponse_t889264267, ___RequestedUnattachedReference_13)); }
	inline SecurityKeyIdentifierClause_t1943429813 * get_RequestedUnattachedReference_13() const { return ___RequestedUnattachedReference_13; }
	inline SecurityKeyIdentifierClause_t1943429813 ** get_address_of_RequestedUnattachedReference_13() { return &___RequestedUnattachedReference_13; }
	inline void set_RequestedUnattachedReference_13(SecurityKeyIdentifierClause_t1943429813 * value)
	{
		___RequestedUnattachedReference_13 = value;
		Il2CppCodeGenWriteBarrier(&___RequestedUnattachedReference_13, value);
	}

	inline static int32_t get_offset_of_RequestedProofToken_14() { return static_cast<int32_t>(offsetof(WstRequestSecurityTokenResponse_t889264267, ___RequestedProofToken_14)); }
	inline Il2CppObject * get_RequestedProofToken_14() const { return ___RequestedProofToken_14; }
	inline Il2CppObject ** get_address_of_RequestedProofToken_14() { return &___RequestedProofToken_14; }
	inline void set_RequestedProofToken_14(Il2CppObject * value)
	{
		___RequestedProofToken_14 = value;
		Il2CppCodeGenWriteBarrier(&___RequestedProofToken_14, value);
	}

	inline static int32_t get_offset_of_Lifetime_15() { return static_cast<int32_t>(offsetof(WstRequestSecurityTokenResponse_t889264267, ___Lifetime_15)); }
	inline WstLifetime_t4147158307 * get_Lifetime_15() const { return ___Lifetime_15; }
	inline WstLifetime_t4147158307 ** get_address_of_Lifetime_15() { return &___Lifetime_15; }
	inline void set_Lifetime_15(WstLifetime_t4147158307 * value)
	{
		___Lifetime_15 = value;
		Il2CppCodeGenWriteBarrier(&___Lifetime_15, value);
	}

	inline static int32_t get_offset_of_Authenticator_16() { return static_cast<int32_t>(offsetof(WstRequestSecurityTokenResponse_t889264267, ___Authenticator_16)); }
	inline ByteU5BU5D_t4116647657* get_Authenticator_16() const { return ___Authenticator_16; }
	inline ByteU5BU5D_t4116647657** get_address_of_Authenticator_16() { return &___Authenticator_16; }
	inline void set_Authenticator_16(ByteU5BU5D_t4116647657* value)
	{
		___Authenticator_16 = value;
		Il2CppCodeGenWriteBarrier(&___Authenticator_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
