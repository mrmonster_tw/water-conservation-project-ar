﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Security.ScopedMessagePartSpecification
struct ScopedMessagePartSpecification_t187359123;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.ChannelProtectionRequirements
struct  ChannelProtectionRequirements_t2141883287  : public Il2CppObject
{
public:
	// System.Boolean System.ServiceModel.Security.ChannelProtectionRequirements::is_readonly
	bool ___is_readonly_0;
	// System.ServiceModel.Security.ScopedMessagePartSpecification System.ServiceModel.Security.ChannelProtectionRequirements::in_enc
	ScopedMessagePartSpecification_t187359123 * ___in_enc_1;
	// System.ServiceModel.Security.ScopedMessagePartSpecification System.ServiceModel.Security.ChannelProtectionRequirements::in_sign
	ScopedMessagePartSpecification_t187359123 * ___in_sign_2;
	// System.ServiceModel.Security.ScopedMessagePartSpecification System.ServiceModel.Security.ChannelProtectionRequirements::out_enc
	ScopedMessagePartSpecification_t187359123 * ___out_enc_3;
	// System.ServiceModel.Security.ScopedMessagePartSpecification System.ServiceModel.Security.ChannelProtectionRequirements::out_sign
	ScopedMessagePartSpecification_t187359123 * ___out_sign_4;

public:
	inline static int32_t get_offset_of_is_readonly_0() { return static_cast<int32_t>(offsetof(ChannelProtectionRequirements_t2141883287, ___is_readonly_0)); }
	inline bool get_is_readonly_0() const { return ___is_readonly_0; }
	inline bool* get_address_of_is_readonly_0() { return &___is_readonly_0; }
	inline void set_is_readonly_0(bool value)
	{
		___is_readonly_0 = value;
	}

	inline static int32_t get_offset_of_in_enc_1() { return static_cast<int32_t>(offsetof(ChannelProtectionRequirements_t2141883287, ___in_enc_1)); }
	inline ScopedMessagePartSpecification_t187359123 * get_in_enc_1() const { return ___in_enc_1; }
	inline ScopedMessagePartSpecification_t187359123 ** get_address_of_in_enc_1() { return &___in_enc_1; }
	inline void set_in_enc_1(ScopedMessagePartSpecification_t187359123 * value)
	{
		___in_enc_1 = value;
		Il2CppCodeGenWriteBarrier(&___in_enc_1, value);
	}

	inline static int32_t get_offset_of_in_sign_2() { return static_cast<int32_t>(offsetof(ChannelProtectionRequirements_t2141883287, ___in_sign_2)); }
	inline ScopedMessagePartSpecification_t187359123 * get_in_sign_2() const { return ___in_sign_2; }
	inline ScopedMessagePartSpecification_t187359123 ** get_address_of_in_sign_2() { return &___in_sign_2; }
	inline void set_in_sign_2(ScopedMessagePartSpecification_t187359123 * value)
	{
		___in_sign_2 = value;
		Il2CppCodeGenWriteBarrier(&___in_sign_2, value);
	}

	inline static int32_t get_offset_of_out_enc_3() { return static_cast<int32_t>(offsetof(ChannelProtectionRequirements_t2141883287, ___out_enc_3)); }
	inline ScopedMessagePartSpecification_t187359123 * get_out_enc_3() const { return ___out_enc_3; }
	inline ScopedMessagePartSpecification_t187359123 ** get_address_of_out_enc_3() { return &___out_enc_3; }
	inline void set_out_enc_3(ScopedMessagePartSpecification_t187359123 * value)
	{
		___out_enc_3 = value;
		Il2CppCodeGenWriteBarrier(&___out_enc_3, value);
	}

	inline static int32_t get_offset_of_out_sign_4() { return static_cast<int32_t>(offsetof(ChannelProtectionRequirements_t2141883287, ___out_sign_4)); }
	inline ScopedMessagePartSpecification_t187359123 * get_out_sign_4() const { return ___out_sign_4; }
	inline ScopedMessagePartSpecification_t187359123 ** get_address_of_out_sign_4() { return &___out_sign_4; }
	inline void set_out_sign_4(ScopedMessagePartSpecification_t187359123 * value)
	{
		___out_sign_4 = value;
		Il2CppCodeGenWriteBarrier(&___out_sign_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
