﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Operations_XslNumber_XslNu2815695937.h"

// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t435877138;
// System.Text.StringBuilder
struct StringBuilder_t1712802186;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslNumber/XslNumberFormatter/DigitItem
struct  DigitItem_t2549330676  : public FormatItem_t2815695937
{
public:
	// System.Globalization.NumberFormatInfo Mono.Xml.Xsl.Operations.XslNumber/XslNumberFormatter/DigitItem::nfi
	NumberFormatInfo_t435877138 * ___nfi_1;
	// System.Int32 Mono.Xml.Xsl.Operations.XslNumber/XslNumberFormatter/DigitItem::decimalSectionLength
	int32_t ___decimalSectionLength_2;
	// System.Text.StringBuilder Mono.Xml.Xsl.Operations.XslNumber/XslNumberFormatter/DigitItem::numberBuilder
	StringBuilder_t1712802186 * ___numberBuilder_3;

public:
	inline static int32_t get_offset_of_nfi_1() { return static_cast<int32_t>(offsetof(DigitItem_t2549330676, ___nfi_1)); }
	inline NumberFormatInfo_t435877138 * get_nfi_1() const { return ___nfi_1; }
	inline NumberFormatInfo_t435877138 ** get_address_of_nfi_1() { return &___nfi_1; }
	inline void set_nfi_1(NumberFormatInfo_t435877138 * value)
	{
		___nfi_1 = value;
		Il2CppCodeGenWriteBarrier(&___nfi_1, value);
	}

	inline static int32_t get_offset_of_decimalSectionLength_2() { return static_cast<int32_t>(offsetof(DigitItem_t2549330676, ___decimalSectionLength_2)); }
	inline int32_t get_decimalSectionLength_2() const { return ___decimalSectionLength_2; }
	inline int32_t* get_address_of_decimalSectionLength_2() { return &___decimalSectionLength_2; }
	inline void set_decimalSectionLength_2(int32_t value)
	{
		___decimalSectionLength_2 = value;
	}

	inline static int32_t get_offset_of_numberBuilder_3() { return static_cast<int32_t>(offsetof(DigitItem_t2549330676, ___numberBuilder_3)); }
	inline StringBuilder_t1712802186 * get_numberBuilder_3() const { return ___numberBuilder_3; }
	inline StringBuilder_t1712802186 ** get_address_of_numberBuilder_3() { return &___numberBuilder_3; }
	inline void set_numberBuilder_3(StringBuilder_t1712802186 * value)
	{
		___numberBuilder_3 = value;
		Il2CppCodeGenWriteBarrier(&___numberBuilder_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
