﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String[]
struct StringU5BU5D_t1281789340;
// System.Xml.XmlReaderSettings
struct XmlReaderSettings_t2186285234;
// System.Collections.Generic.List`1<System.ServiceModel.Channels.MessageHeaderInfo>
struct List_1_t1936922331;
// System.Collections.Generic.Dictionary`2<System.Type,System.Runtime.Serialization.XmlObjectSerializer>
struct Dictionary_2_t2116681529;
// System.ServiceModel.Channels.MessageVersion
struct MessageVersion_t1958933598;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.MessageHeaders
struct  MessageHeaders_t4050072634  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.ServiceModel.Channels.MessageHeaderInfo> System.ServiceModel.Channels.MessageHeaders::l
	List_1_t1936922331 * ___l_2;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Runtime.Serialization.XmlObjectSerializer> System.ServiceModel.Channels.MessageHeaders::serializers
	Dictionary_2_t2116681529 * ___serializers_3;
	// System.ServiceModel.Channels.MessageVersion System.ServiceModel.Channels.MessageHeaders::version
	MessageVersion_t1958933598 * ___version_4;

public:
	inline static int32_t get_offset_of_l_2() { return static_cast<int32_t>(offsetof(MessageHeaders_t4050072634, ___l_2)); }
	inline List_1_t1936922331 * get_l_2() const { return ___l_2; }
	inline List_1_t1936922331 ** get_address_of_l_2() { return &___l_2; }
	inline void set_l_2(List_1_t1936922331 * value)
	{
		___l_2 = value;
		Il2CppCodeGenWriteBarrier(&___l_2, value);
	}

	inline static int32_t get_offset_of_serializers_3() { return static_cast<int32_t>(offsetof(MessageHeaders_t4050072634, ___serializers_3)); }
	inline Dictionary_2_t2116681529 * get_serializers_3() const { return ___serializers_3; }
	inline Dictionary_2_t2116681529 ** get_address_of_serializers_3() { return &___serializers_3; }
	inline void set_serializers_3(Dictionary_2_t2116681529 * value)
	{
		___serializers_3 = value;
		Il2CppCodeGenWriteBarrier(&___serializers_3, value);
	}

	inline static int32_t get_offset_of_version_4() { return static_cast<int32_t>(offsetof(MessageHeaders_t4050072634, ___version_4)); }
	inline MessageVersion_t1958933598 * get_version_4() const { return ___version_4; }
	inline MessageVersion_t1958933598 ** get_address_of_version_4() { return &___version_4; }
	inline void set_version_4(MessageVersion_t1958933598 * value)
	{
		___version_4 = value;
		Il2CppCodeGenWriteBarrier(&___version_4, value);
	}
};

struct MessageHeaders_t4050072634_StaticFields
{
public:
	// System.String[] System.ServiceModel.Channels.MessageHeaders::empty_strings
	StringU5BU5D_t1281789340* ___empty_strings_0;
	// System.Xml.XmlReaderSettings System.ServiceModel.Channels.MessageHeaders::reader_settings
	XmlReaderSettings_t2186285234 * ___reader_settings_1;

public:
	inline static int32_t get_offset_of_empty_strings_0() { return static_cast<int32_t>(offsetof(MessageHeaders_t4050072634_StaticFields, ___empty_strings_0)); }
	inline StringU5BU5D_t1281789340* get_empty_strings_0() const { return ___empty_strings_0; }
	inline StringU5BU5D_t1281789340** get_address_of_empty_strings_0() { return &___empty_strings_0; }
	inline void set_empty_strings_0(StringU5BU5D_t1281789340* value)
	{
		___empty_strings_0 = value;
		Il2CppCodeGenWriteBarrier(&___empty_strings_0, value);
	}

	inline static int32_t get_offset_of_reader_settings_1() { return static_cast<int32_t>(offsetof(MessageHeaders_t4050072634_StaticFields, ___reader_settings_1)); }
	inline XmlReaderSettings_t2186285234 * get_reader_settings_1() const { return ___reader_settings_1; }
	inline XmlReaderSettings_t2186285234 ** get_address_of_reader_settings_1() { return &___reader_settings_1; }
	inline void set_reader_settings_1(XmlReaderSettings_t2186285234 * value)
	{
		___reader_settings_1 = value;
		Il2CppCodeGenWriteBarrier(&___reader_settings_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
