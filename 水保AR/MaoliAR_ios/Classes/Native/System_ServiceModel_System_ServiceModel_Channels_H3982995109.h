﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Channels.HttpTransportBindingElement
struct HttpTransportBindingElement_t2392894562;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.HttpTransportBindingElement/HttpBindingProperties
struct  HttpBindingProperties_t3982995109  : public Il2CppObject
{
public:
	// System.ServiceModel.Channels.HttpTransportBindingElement System.ServiceModel.Channels.HttpTransportBindingElement/HttpBindingProperties::source
	HttpTransportBindingElement_t2392894562 * ___source_0;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(HttpBindingProperties_t3982995109, ___source_0)); }
	inline HttpTransportBindingElement_t2392894562 * get_source_0() const { return ___source_0; }
	inline HttpTransportBindingElement_t2392894562 ** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(HttpTransportBindingElement_t2392894562 * value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier(&___source_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
