﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.FederatedMessageSecurityOverHttpElement
struct  FederatedMessageSecurityOverHttpElement_t458989593  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct FederatedMessageSecurityOverHttpElement_t458989593_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.FederatedMessageSecurityOverHttpElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.FederatedMessageSecurityOverHttpElement::algorithm_suite
	ConfigurationProperty_t3590861854 * ___algorithm_suite_14;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.FederatedMessageSecurityOverHttpElement::claim_type_requirements
	ConfigurationProperty_t3590861854 * ___claim_type_requirements_15;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.FederatedMessageSecurityOverHttpElement::issued_key_type
	ConfigurationProperty_t3590861854 * ___issued_key_type_16;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.FederatedMessageSecurityOverHttpElement::issued_token_type
	ConfigurationProperty_t3590861854 * ___issued_token_type_17;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.FederatedMessageSecurityOverHttpElement::issuer
	ConfigurationProperty_t3590861854 * ___issuer_18;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.FederatedMessageSecurityOverHttpElement::issuer_metadata
	ConfigurationProperty_t3590861854 * ___issuer_metadata_19;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.FederatedMessageSecurityOverHttpElement::negotiate_service_credential
	ConfigurationProperty_t3590861854 * ___negotiate_service_credential_20;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.FederatedMessageSecurityOverHttpElement::token_request_parameters
	ConfigurationProperty_t3590861854 * ___token_request_parameters_21;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(FederatedMessageSecurityOverHttpElement_t458989593_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier(&___properties_13, value);
	}

	inline static int32_t get_offset_of_algorithm_suite_14() { return static_cast<int32_t>(offsetof(FederatedMessageSecurityOverHttpElement_t458989593_StaticFields, ___algorithm_suite_14)); }
	inline ConfigurationProperty_t3590861854 * get_algorithm_suite_14() const { return ___algorithm_suite_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_algorithm_suite_14() { return &___algorithm_suite_14; }
	inline void set_algorithm_suite_14(ConfigurationProperty_t3590861854 * value)
	{
		___algorithm_suite_14 = value;
		Il2CppCodeGenWriteBarrier(&___algorithm_suite_14, value);
	}

	inline static int32_t get_offset_of_claim_type_requirements_15() { return static_cast<int32_t>(offsetof(FederatedMessageSecurityOverHttpElement_t458989593_StaticFields, ___claim_type_requirements_15)); }
	inline ConfigurationProperty_t3590861854 * get_claim_type_requirements_15() const { return ___claim_type_requirements_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_claim_type_requirements_15() { return &___claim_type_requirements_15; }
	inline void set_claim_type_requirements_15(ConfigurationProperty_t3590861854 * value)
	{
		___claim_type_requirements_15 = value;
		Il2CppCodeGenWriteBarrier(&___claim_type_requirements_15, value);
	}

	inline static int32_t get_offset_of_issued_key_type_16() { return static_cast<int32_t>(offsetof(FederatedMessageSecurityOverHttpElement_t458989593_StaticFields, ___issued_key_type_16)); }
	inline ConfigurationProperty_t3590861854 * get_issued_key_type_16() const { return ___issued_key_type_16; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_issued_key_type_16() { return &___issued_key_type_16; }
	inline void set_issued_key_type_16(ConfigurationProperty_t3590861854 * value)
	{
		___issued_key_type_16 = value;
		Il2CppCodeGenWriteBarrier(&___issued_key_type_16, value);
	}

	inline static int32_t get_offset_of_issued_token_type_17() { return static_cast<int32_t>(offsetof(FederatedMessageSecurityOverHttpElement_t458989593_StaticFields, ___issued_token_type_17)); }
	inline ConfigurationProperty_t3590861854 * get_issued_token_type_17() const { return ___issued_token_type_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_issued_token_type_17() { return &___issued_token_type_17; }
	inline void set_issued_token_type_17(ConfigurationProperty_t3590861854 * value)
	{
		___issued_token_type_17 = value;
		Il2CppCodeGenWriteBarrier(&___issued_token_type_17, value);
	}

	inline static int32_t get_offset_of_issuer_18() { return static_cast<int32_t>(offsetof(FederatedMessageSecurityOverHttpElement_t458989593_StaticFields, ___issuer_18)); }
	inline ConfigurationProperty_t3590861854 * get_issuer_18() const { return ___issuer_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_issuer_18() { return &___issuer_18; }
	inline void set_issuer_18(ConfigurationProperty_t3590861854 * value)
	{
		___issuer_18 = value;
		Il2CppCodeGenWriteBarrier(&___issuer_18, value);
	}

	inline static int32_t get_offset_of_issuer_metadata_19() { return static_cast<int32_t>(offsetof(FederatedMessageSecurityOverHttpElement_t458989593_StaticFields, ___issuer_metadata_19)); }
	inline ConfigurationProperty_t3590861854 * get_issuer_metadata_19() const { return ___issuer_metadata_19; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_issuer_metadata_19() { return &___issuer_metadata_19; }
	inline void set_issuer_metadata_19(ConfigurationProperty_t3590861854 * value)
	{
		___issuer_metadata_19 = value;
		Il2CppCodeGenWriteBarrier(&___issuer_metadata_19, value);
	}

	inline static int32_t get_offset_of_negotiate_service_credential_20() { return static_cast<int32_t>(offsetof(FederatedMessageSecurityOverHttpElement_t458989593_StaticFields, ___negotiate_service_credential_20)); }
	inline ConfigurationProperty_t3590861854 * get_negotiate_service_credential_20() const { return ___negotiate_service_credential_20; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_negotiate_service_credential_20() { return &___negotiate_service_credential_20; }
	inline void set_negotiate_service_credential_20(ConfigurationProperty_t3590861854 * value)
	{
		___negotiate_service_credential_20 = value;
		Il2CppCodeGenWriteBarrier(&___negotiate_service_credential_20, value);
	}

	inline static int32_t get_offset_of_token_request_parameters_21() { return static_cast<int32_t>(offsetof(FederatedMessageSecurityOverHttpElement_t458989593_StaticFields, ___token_request_parameters_21)); }
	inline ConfigurationProperty_t3590861854 * get_token_request_parameters_21() const { return ___token_request_parameters_21; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_token_request_parameters_21() { return &___token_request_parameters_21; }
	inline void set_token_request_parameters_21(ConfigurationProperty_t3590861854 * value)
	{
		___token_request_parameters_21 = value;
		Il2CppCodeGenWriteBarrier(&___token_request_parameters_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
