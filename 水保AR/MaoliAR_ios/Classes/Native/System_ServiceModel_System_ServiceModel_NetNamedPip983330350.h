﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Bi859993683.h"

// System.Xml.XmlDictionaryReaderQuotas
struct XmlDictionaryReaderQuotas_t173030297;
// System.ServiceModel.Channels.NamedPipeTransportBindingElement
struct NamedPipeTransportBindingElement_t654821915;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.NetNamedPipeBinding
struct  NetNamedPipeBinding_t983330350  : public Binding_t859993683
{
public:
	// System.Xml.XmlDictionaryReaderQuotas System.ServiceModel.NetNamedPipeBinding::reader_quotas
	XmlDictionaryReaderQuotas_t173030297 * ___reader_quotas_6;
	// System.ServiceModel.Channels.NamedPipeTransportBindingElement System.ServiceModel.NetNamedPipeBinding::transport
	NamedPipeTransportBindingElement_t654821915 * ___transport_7;

public:
	inline static int32_t get_offset_of_reader_quotas_6() { return static_cast<int32_t>(offsetof(NetNamedPipeBinding_t983330350, ___reader_quotas_6)); }
	inline XmlDictionaryReaderQuotas_t173030297 * get_reader_quotas_6() const { return ___reader_quotas_6; }
	inline XmlDictionaryReaderQuotas_t173030297 ** get_address_of_reader_quotas_6() { return &___reader_quotas_6; }
	inline void set_reader_quotas_6(XmlDictionaryReaderQuotas_t173030297 * value)
	{
		___reader_quotas_6 = value;
		Il2CppCodeGenWriteBarrier(&___reader_quotas_6, value);
	}

	inline static int32_t get_offset_of_transport_7() { return static_cast<int32_t>(offsetof(NetNamedPipeBinding_t983330350, ___transport_7)); }
	inline NamedPipeTransportBindingElement_t654821915 * get_transport_7() const { return ___transport_7; }
	inline NamedPipeTransportBindingElement_t654821915 ** get_address_of_transport_7() { return &___transport_7; }
	inline void set_transport_7(NamedPipeTransportBindingElement_t654821915 * value)
	{
		___transport_7 = value;
		Il2CppCodeGenWriteBarrier(&___transport_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
