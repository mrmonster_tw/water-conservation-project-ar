﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_System_ServiceModel_SecurityMo1299321141.h"

// System.ServiceModel.PeerTransportSecuritySettings
struct PeerTransportSecuritySettings_t3643429466;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.PeerSecuritySettings
struct  PeerSecuritySettings_t2125602112  : public Il2CppObject
{
public:
	// System.ServiceModel.SecurityMode System.ServiceModel.PeerSecuritySettings::mode
	int32_t ___mode_0;
	// System.ServiceModel.PeerTransportSecuritySettings System.ServiceModel.PeerSecuritySettings::<Transport>k__BackingField
	PeerTransportSecuritySettings_t3643429466 * ___U3CTransportU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_mode_0() { return static_cast<int32_t>(offsetof(PeerSecuritySettings_t2125602112, ___mode_0)); }
	inline int32_t get_mode_0() const { return ___mode_0; }
	inline int32_t* get_address_of_mode_0() { return &___mode_0; }
	inline void set_mode_0(int32_t value)
	{
		___mode_0 = value;
	}

	inline static int32_t get_offset_of_U3CTransportU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PeerSecuritySettings_t2125602112, ___U3CTransportU3Ek__BackingField_1)); }
	inline PeerTransportSecuritySettings_t3643429466 * get_U3CTransportU3Ek__BackingField_1() const { return ___U3CTransportU3Ek__BackingField_1; }
	inline PeerTransportSecuritySettings_t3643429466 ** get_address_of_U3CTransportU3Ek__BackingField_1() { return &___U3CTransportU3Ek__BackingField_1; }
	inline void set_U3CTransportU3Ek__BackingField_1(PeerTransportSecuritySettings_t3643429466 * value)
	{
		___U3CTransportU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTransportU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
