﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_System_Security_Cryptography_X509Certificat2571829933.h"
#include "System_System_Security_Cryptography_X509Certificat2864310644.h"
#include "System_ServiceModel_System_ServiceModel_Security_X3346525935.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.X509PeerCertificateAuthentication
struct  X509PeerCertificateAuthentication_t406335588  : public Il2CppObject
{
public:
	// System.Security.Cryptography.X509Certificates.X509RevocationMode System.ServiceModel.Security.X509PeerCertificateAuthentication::revocation_mode
	int32_t ___revocation_mode_0;
	// System.Security.Cryptography.X509Certificates.StoreLocation System.ServiceModel.Security.X509PeerCertificateAuthentication::store_loc
	int32_t ___store_loc_1;
	// System.ServiceModel.Security.X509CertificateValidationMode System.ServiceModel.Security.X509PeerCertificateAuthentication::validation_mode
	int32_t ___validation_mode_2;

public:
	inline static int32_t get_offset_of_revocation_mode_0() { return static_cast<int32_t>(offsetof(X509PeerCertificateAuthentication_t406335588, ___revocation_mode_0)); }
	inline int32_t get_revocation_mode_0() const { return ___revocation_mode_0; }
	inline int32_t* get_address_of_revocation_mode_0() { return &___revocation_mode_0; }
	inline void set_revocation_mode_0(int32_t value)
	{
		___revocation_mode_0 = value;
	}

	inline static int32_t get_offset_of_store_loc_1() { return static_cast<int32_t>(offsetof(X509PeerCertificateAuthentication_t406335588, ___store_loc_1)); }
	inline int32_t get_store_loc_1() const { return ___store_loc_1; }
	inline int32_t* get_address_of_store_loc_1() { return &___store_loc_1; }
	inline void set_store_loc_1(int32_t value)
	{
		___store_loc_1 = value;
	}

	inline static int32_t get_offset_of_validation_mode_2() { return static_cast<int32_t>(offsetof(X509PeerCertificateAuthentication_t406335588, ___validation_mode_2)); }
	inline int32_t get_validation_mode_2() const { return ___validation_mode_2; }
	inline int32_t* get_address_of_validation_mode_2() { return &___validation_mode_2; }
	inline void set_validation_mode_2(int32_t value)
	{
		___validation_mode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
