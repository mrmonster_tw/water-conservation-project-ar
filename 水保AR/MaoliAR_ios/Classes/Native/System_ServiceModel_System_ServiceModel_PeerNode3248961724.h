﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.EventHandler
struct EventHandler_t1348719766;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.PeerNode
struct  PeerNode_t3248961724  : public Il2CppObject
{
public:
	// System.EventHandler System.ServiceModel.PeerNode::Offline
	EventHandler_t1348719766 * ___Offline_0;
	// System.EventHandler System.ServiceModel.PeerNode::Online
	EventHandler_t1348719766 * ___Online_1;
	// System.Boolean System.ServiceModel.PeerNode::<IsOnline>k__BackingField
	bool ___U3CIsOnlineU3Ek__BackingField_2;
	// System.String System.ServiceModel.PeerNode::<MeshId>k__BackingField
	String_t* ___U3CMeshIdU3Ek__BackingField_3;
	// System.UInt64 System.ServiceModel.PeerNode::<NodeId>k__BackingField
	uint64_t ___U3CNodeIdU3Ek__BackingField_4;
	// System.Object System.ServiceModel.PeerNode::<RegisteredId>k__BackingField
	Il2CppObject * ___U3CRegisteredIdU3Ek__BackingField_5;
	// System.Int32 System.ServiceModel.PeerNode::<Port>k__BackingField
	int32_t ___U3CPortU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_Offline_0() { return static_cast<int32_t>(offsetof(PeerNode_t3248961724, ___Offline_0)); }
	inline EventHandler_t1348719766 * get_Offline_0() const { return ___Offline_0; }
	inline EventHandler_t1348719766 ** get_address_of_Offline_0() { return &___Offline_0; }
	inline void set_Offline_0(EventHandler_t1348719766 * value)
	{
		___Offline_0 = value;
		Il2CppCodeGenWriteBarrier(&___Offline_0, value);
	}

	inline static int32_t get_offset_of_Online_1() { return static_cast<int32_t>(offsetof(PeerNode_t3248961724, ___Online_1)); }
	inline EventHandler_t1348719766 * get_Online_1() const { return ___Online_1; }
	inline EventHandler_t1348719766 ** get_address_of_Online_1() { return &___Online_1; }
	inline void set_Online_1(EventHandler_t1348719766 * value)
	{
		___Online_1 = value;
		Il2CppCodeGenWriteBarrier(&___Online_1, value);
	}

	inline static int32_t get_offset_of_U3CIsOnlineU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PeerNode_t3248961724, ___U3CIsOnlineU3Ek__BackingField_2)); }
	inline bool get_U3CIsOnlineU3Ek__BackingField_2() const { return ___U3CIsOnlineU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CIsOnlineU3Ek__BackingField_2() { return &___U3CIsOnlineU3Ek__BackingField_2; }
	inline void set_U3CIsOnlineU3Ek__BackingField_2(bool value)
	{
		___U3CIsOnlineU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CMeshIdU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PeerNode_t3248961724, ___U3CMeshIdU3Ek__BackingField_3)); }
	inline String_t* get_U3CMeshIdU3Ek__BackingField_3() const { return ___U3CMeshIdU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CMeshIdU3Ek__BackingField_3() { return &___U3CMeshIdU3Ek__BackingField_3; }
	inline void set_U3CMeshIdU3Ek__BackingField_3(String_t* value)
	{
		___U3CMeshIdU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMeshIdU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CNodeIdU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PeerNode_t3248961724, ___U3CNodeIdU3Ek__BackingField_4)); }
	inline uint64_t get_U3CNodeIdU3Ek__BackingField_4() const { return ___U3CNodeIdU3Ek__BackingField_4; }
	inline uint64_t* get_address_of_U3CNodeIdU3Ek__BackingField_4() { return &___U3CNodeIdU3Ek__BackingField_4; }
	inline void set_U3CNodeIdU3Ek__BackingField_4(uint64_t value)
	{
		___U3CNodeIdU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CRegisteredIdU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PeerNode_t3248961724, ___U3CRegisteredIdU3Ek__BackingField_5)); }
	inline Il2CppObject * get_U3CRegisteredIdU3Ek__BackingField_5() const { return ___U3CRegisteredIdU3Ek__BackingField_5; }
	inline Il2CppObject ** get_address_of_U3CRegisteredIdU3Ek__BackingField_5() { return &___U3CRegisteredIdU3Ek__BackingField_5; }
	inline void set_U3CRegisteredIdU3Ek__BackingField_5(Il2CppObject * value)
	{
		___U3CRegisteredIdU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRegisteredIdU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CPortU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PeerNode_t3248961724, ___U3CPortU3Ek__BackingField_6)); }
	inline int32_t get_U3CPortU3Ek__BackingField_6() const { return ___U3CPortU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CPortU3Ek__BackingField_6() { return &___U3CPortU3Ek__BackingField_6; }
	inline void set_U3CPortU3Ek__BackingField_6(int32_t value)
	{
		___U3CPortU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
