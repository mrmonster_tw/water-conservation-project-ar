﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Serialization_XmlSerializati1652069793.h"

// System.Reflection.MethodInfo
struct MethodInfo_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.ServiceDescriptionReaderBase
struct  ServiceDescriptionReaderBase_t1842510776  : public XmlSerializationReader_t1652069793
{
public:

public:
};

struct ServiceDescriptionReaderBase_t1842510776_StaticFields
{
public:
	// System.Reflection.MethodInfo System.Web.Services.Description.ServiceDescriptionReaderBase::fromBinHexStringMethod
	MethodInfo_t * ___fromBinHexStringMethod_25;

public:
	inline static int32_t get_offset_of_fromBinHexStringMethod_25() { return static_cast<int32_t>(offsetof(ServiceDescriptionReaderBase_t1842510776_StaticFields, ___fromBinHexStringMethod_25)); }
	inline MethodInfo_t * get_fromBinHexStringMethod_25() const { return ___fromBinHexStringMethod_25; }
	inline MethodInfo_t ** get_address_of_fromBinHexStringMethod_25() { return &___fromBinHexStringMethod_25; }
	inline void set_fromBinHexStringMethod_25(MethodInfo_t * value)
	{
		___fromBinHexStringMethod_25 = value;
		Il2CppCodeGenWriteBarrier(&___fromBinHexStringMethod_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
