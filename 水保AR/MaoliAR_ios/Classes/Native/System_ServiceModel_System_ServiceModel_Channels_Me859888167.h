﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Channels.Message
struct Message_t869514973;
// System.ServiceModel.Security.SecurityMessageProperty
struct SecurityMessageProperty_t3957431664;
// System.ServiceModel.Channels.MessageSecurityBindingSupport
struct MessageSecurityBindingSupport_t1904510157;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.MessageSecurityGenerator
struct  MessageSecurityGenerator_t859888167  : public Il2CppObject
{
public:
	// System.ServiceModel.Channels.Message System.ServiceModel.Channels.MessageSecurityGenerator::msg
	Message_t869514973 * ___msg_0;
	// System.ServiceModel.Security.SecurityMessageProperty System.ServiceModel.Channels.MessageSecurityGenerator::secprop
	SecurityMessageProperty_t3957431664 * ___secprop_1;
	// System.ServiceModel.Channels.MessageSecurityBindingSupport System.ServiceModel.Channels.MessageSecurityGenerator::security
	MessageSecurityBindingSupport_t1904510157 * ___security_2;
	// System.Int32 System.ServiceModel.Channels.MessageSecurityGenerator::idbase
	int32_t ___idbase_3;

public:
	inline static int32_t get_offset_of_msg_0() { return static_cast<int32_t>(offsetof(MessageSecurityGenerator_t859888167, ___msg_0)); }
	inline Message_t869514973 * get_msg_0() const { return ___msg_0; }
	inline Message_t869514973 ** get_address_of_msg_0() { return &___msg_0; }
	inline void set_msg_0(Message_t869514973 * value)
	{
		___msg_0 = value;
		Il2CppCodeGenWriteBarrier(&___msg_0, value);
	}

	inline static int32_t get_offset_of_secprop_1() { return static_cast<int32_t>(offsetof(MessageSecurityGenerator_t859888167, ___secprop_1)); }
	inline SecurityMessageProperty_t3957431664 * get_secprop_1() const { return ___secprop_1; }
	inline SecurityMessageProperty_t3957431664 ** get_address_of_secprop_1() { return &___secprop_1; }
	inline void set_secprop_1(SecurityMessageProperty_t3957431664 * value)
	{
		___secprop_1 = value;
		Il2CppCodeGenWriteBarrier(&___secprop_1, value);
	}

	inline static int32_t get_offset_of_security_2() { return static_cast<int32_t>(offsetof(MessageSecurityGenerator_t859888167, ___security_2)); }
	inline MessageSecurityBindingSupport_t1904510157 * get_security_2() const { return ___security_2; }
	inline MessageSecurityBindingSupport_t1904510157 ** get_address_of_security_2() { return &___security_2; }
	inline void set_security_2(MessageSecurityBindingSupport_t1904510157 * value)
	{
		___security_2 = value;
		Il2CppCodeGenWriteBarrier(&___security_2, value);
	}

	inline static int32_t get_offset_of_idbase_3() { return static_cast<int32_t>(offsetof(MessageSecurityGenerator_t859888167, ___idbase_3)); }
	inline int32_t get_idbase_3() const { return ___idbase_3; }
	inline int32_t* get_address_of_idbase_3() { return &___idbase_3; }
	inline void set_idbase_3(int32_t value)
	{
		___idbase_3 = value;
	}
};

struct MessageSecurityGenerator_t859888167_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Channels.MessageSecurityGenerator::<>f__switch$map6
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map6_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map6_4() { return static_cast<int32_t>(offsetof(MessageSecurityGenerator_t859888167_StaticFields, ___U3CU3Ef__switchU24map6_4)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map6_4() const { return ___U3CU3Ef__switchU24map6_4; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map6_4() { return &___U3CU3Ef__switchU24map6_4; }
	inline void set_U3CU3Ef__switchU24map6_4(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map6_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map6_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
