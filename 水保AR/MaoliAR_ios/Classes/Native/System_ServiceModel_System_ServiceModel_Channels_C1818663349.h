﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Ht684206763.h"

// System.String
struct String_t;
// System.CodeDom.Compiler.CompilerErrorCollection
struct CompilerErrorCollection_t2383307460;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.CompilationException
struct  CompilationException_t1818663349  : public HtmlizedException_t684206763
{
public:
	// System.String System.ServiceModel.Channels.CompilationException::filename
	String_t* ___filename_11;
	// System.CodeDom.Compiler.CompilerErrorCollection System.ServiceModel.Channels.CompilationException::errors
	CompilerErrorCollection_t2383307460 * ___errors_12;
	// System.String System.ServiceModel.Channels.CompilationException::fileText
	String_t* ___fileText_13;

public:
	inline static int32_t get_offset_of_filename_11() { return static_cast<int32_t>(offsetof(CompilationException_t1818663349, ___filename_11)); }
	inline String_t* get_filename_11() const { return ___filename_11; }
	inline String_t** get_address_of_filename_11() { return &___filename_11; }
	inline void set_filename_11(String_t* value)
	{
		___filename_11 = value;
		Il2CppCodeGenWriteBarrier(&___filename_11, value);
	}

	inline static int32_t get_offset_of_errors_12() { return static_cast<int32_t>(offsetof(CompilationException_t1818663349, ___errors_12)); }
	inline CompilerErrorCollection_t2383307460 * get_errors_12() const { return ___errors_12; }
	inline CompilerErrorCollection_t2383307460 ** get_address_of_errors_12() { return &___errors_12; }
	inline void set_errors_12(CompilerErrorCollection_t2383307460 * value)
	{
		___errors_12 = value;
		Il2CppCodeGenWriteBarrier(&___errors_12, value);
	}

	inline static int32_t get_offset_of_fileText_13() { return static_cast<int32_t>(offsetof(CompilationException_t1818663349, ___fileText_13)); }
	inline String_t* get_fileText_13() const { return ___fileText_13; }
	inline String_t** get_address_of_fileText_13() { return &___fileText_13; }
	inline void set_fileText_13(String_t* value)
	{
		___fileText_13 = value;
		Il2CppCodeGenWriteBarrier(&___fileText_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
