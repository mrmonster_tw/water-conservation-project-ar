﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_Guid3193532887.h"
#include "mscorlib_System_Reflection_BindingFlags2721792723.h"

// System.Web.UI.TemplateParser
struct TemplateParser_t24149626;
// System.CodeDom.CodeCompileUnit
struct CodeCompileUnit_t2527618915;
// System.CodeDom.CodeNamespace
struct CodeNamespace_t2165007136;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.CodeDom.CodeTypeDeclaration
struct CodeTypeDeclaration_t2359234283;
// System.CodeDom.CodeTypeReferenceExpression
struct CodeTypeReferenceExpression_t793901702;
// System.CodeDom.CodeThisReferenceExpression
struct CodeThisReferenceExpression_t1372480056;
// System.Web.VirtualPath
struct VirtualPath_t4270372584;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.BaseCompiler
struct  BaseCompiler_t69158820  : public Il2CppObject
{
public:
	// System.Web.UI.TemplateParser System.Web.Compilation.BaseCompiler::parser
	TemplateParser_t24149626 * ___parser_2;
	// System.CodeDom.CodeCompileUnit System.Web.Compilation.BaseCompiler::unit
	CodeCompileUnit_t2527618915 * ___unit_3;
	// System.CodeDom.CodeNamespace System.Web.Compilation.BaseCompiler::mainNS
	CodeNamespace_t2165007136 * ___mainNS_4;
	// System.Collections.Hashtable System.Web.Compilation.BaseCompiler::partialNameOverride
	Hashtable_t1853889766 * ___partialNameOverride_5;
	// System.CodeDom.CodeTypeDeclaration System.Web.Compilation.BaseCompiler::partialClass
	CodeTypeDeclaration_t2359234283 * ___partialClass_6;
	// System.CodeDom.CodeTypeReferenceExpression System.Web.Compilation.BaseCompiler::partialClassExpr
	CodeTypeReferenceExpression_t793901702 * ___partialClassExpr_7;
	// System.CodeDom.CodeTypeDeclaration System.Web.Compilation.BaseCompiler::mainClass
	CodeTypeDeclaration_t2359234283 * ___mainClass_8;
	// System.CodeDom.CodeTypeReferenceExpression System.Web.Compilation.BaseCompiler::mainClassExpr
	CodeTypeReferenceExpression_t793901702 * ___mainClassExpr_9;
	// System.Web.VirtualPath System.Web.Compilation.BaseCompiler::inputVirtualPath
	VirtualPath_t4270372584 * ___inputVirtualPath_11;

public:
	inline static int32_t get_offset_of_parser_2() { return static_cast<int32_t>(offsetof(BaseCompiler_t69158820, ___parser_2)); }
	inline TemplateParser_t24149626 * get_parser_2() const { return ___parser_2; }
	inline TemplateParser_t24149626 ** get_address_of_parser_2() { return &___parser_2; }
	inline void set_parser_2(TemplateParser_t24149626 * value)
	{
		___parser_2 = value;
		Il2CppCodeGenWriteBarrier(&___parser_2, value);
	}

	inline static int32_t get_offset_of_unit_3() { return static_cast<int32_t>(offsetof(BaseCompiler_t69158820, ___unit_3)); }
	inline CodeCompileUnit_t2527618915 * get_unit_3() const { return ___unit_3; }
	inline CodeCompileUnit_t2527618915 ** get_address_of_unit_3() { return &___unit_3; }
	inline void set_unit_3(CodeCompileUnit_t2527618915 * value)
	{
		___unit_3 = value;
		Il2CppCodeGenWriteBarrier(&___unit_3, value);
	}

	inline static int32_t get_offset_of_mainNS_4() { return static_cast<int32_t>(offsetof(BaseCompiler_t69158820, ___mainNS_4)); }
	inline CodeNamespace_t2165007136 * get_mainNS_4() const { return ___mainNS_4; }
	inline CodeNamespace_t2165007136 ** get_address_of_mainNS_4() { return &___mainNS_4; }
	inline void set_mainNS_4(CodeNamespace_t2165007136 * value)
	{
		___mainNS_4 = value;
		Il2CppCodeGenWriteBarrier(&___mainNS_4, value);
	}

	inline static int32_t get_offset_of_partialNameOverride_5() { return static_cast<int32_t>(offsetof(BaseCompiler_t69158820, ___partialNameOverride_5)); }
	inline Hashtable_t1853889766 * get_partialNameOverride_5() const { return ___partialNameOverride_5; }
	inline Hashtable_t1853889766 ** get_address_of_partialNameOverride_5() { return &___partialNameOverride_5; }
	inline void set_partialNameOverride_5(Hashtable_t1853889766 * value)
	{
		___partialNameOverride_5 = value;
		Il2CppCodeGenWriteBarrier(&___partialNameOverride_5, value);
	}

	inline static int32_t get_offset_of_partialClass_6() { return static_cast<int32_t>(offsetof(BaseCompiler_t69158820, ___partialClass_6)); }
	inline CodeTypeDeclaration_t2359234283 * get_partialClass_6() const { return ___partialClass_6; }
	inline CodeTypeDeclaration_t2359234283 ** get_address_of_partialClass_6() { return &___partialClass_6; }
	inline void set_partialClass_6(CodeTypeDeclaration_t2359234283 * value)
	{
		___partialClass_6 = value;
		Il2CppCodeGenWriteBarrier(&___partialClass_6, value);
	}

	inline static int32_t get_offset_of_partialClassExpr_7() { return static_cast<int32_t>(offsetof(BaseCompiler_t69158820, ___partialClassExpr_7)); }
	inline CodeTypeReferenceExpression_t793901702 * get_partialClassExpr_7() const { return ___partialClassExpr_7; }
	inline CodeTypeReferenceExpression_t793901702 ** get_address_of_partialClassExpr_7() { return &___partialClassExpr_7; }
	inline void set_partialClassExpr_7(CodeTypeReferenceExpression_t793901702 * value)
	{
		___partialClassExpr_7 = value;
		Il2CppCodeGenWriteBarrier(&___partialClassExpr_7, value);
	}

	inline static int32_t get_offset_of_mainClass_8() { return static_cast<int32_t>(offsetof(BaseCompiler_t69158820, ___mainClass_8)); }
	inline CodeTypeDeclaration_t2359234283 * get_mainClass_8() const { return ___mainClass_8; }
	inline CodeTypeDeclaration_t2359234283 ** get_address_of_mainClass_8() { return &___mainClass_8; }
	inline void set_mainClass_8(CodeTypeDeclaration_t2359234283 * value)
	{
		___mainClass_8 = value;
		Il2CppCodeGenWriteBarrier(&___mainClass_8, value);
	}

	inline static int32_t get_offset_of_mainClassExpr_9() { return static_cast<int32_t>(offsetof(BaseCompiler_t69158820, ___mainClassExpr_9)); }
	inline CodeTypeReferenceExpression_t793901702 * get_mainClassExpr_9() const { return ___mainClassExpr_9; }
	inline CodeTypeReferenceExpression_t793901702 ** get_address_of_mainClassExpr_9() { return &___mainClassExpr_9; }
	inline void set_mainClassExpr_9(CodeTypeReferenceExpression_t793901702 * value)
	{
		___mainClassExpr_9 = value;
		Il2CppCodeGenWriteBarrier(&___mainClassExpr_9, value);
	}

	inline static int32_t get_offset_of_inputVirtualPath_11() { return static_cast<int32_t>(offsetof(BaseCompiler_t69158820, ___inputVirtualPath_11)); }
	inline VirtualPath_t4270372584 * get_inputVirtualPath_11() const { return ___inputVirtualPath_11; }
	inline VirtualPath_t4270372584 ** get_address_of_inputVirtualPath_11() { return &___inputVirtualPath_11; }
	inline void set_inputVirtualPath_11(VirtualPath_t4270372584 * value)
	{
		___inputVirtualPath_11 = value;
		Il2CppCodeGenWriteBarrier(&___inputVirtualPath_11, value);
	}
};

struct BaseCompiler_t69158820_StaticFields
{
public:
	// System.Guid System.Web.Compilation.BaseCompiler::HashMD5
	Guid_t  ___HashMD5_0;
	// System.Reflection.BindingFlags System.Web.Compilation.BaseCompiler::replaceableFlags
	int32_t ___replaceableFlags_1;
	// System.CodeDom.CodeThisReferenceExpression System.Web.Compilation.BaseCompiler::thisRef
	CodeThisReferenceExpression_t1372480056 * ___thisRef_10;

public:
	inline static int32_t get_offset_of_HashMD5_0() { return static_cast<int32_t>(offsetof(BaseCompiler_t69158820_StaticFields, ___HashMD5_0)); }
	inline Guid_t  get_HashMD5_0() const { return ___HashMD5_0; }
	inline Guid_t * get_address_of_HashMD5_0() { return &___HashMD5_0; }
	inline void set_HashMD5_0(Guid_t  value)
	{
		___HashMD5_0 = value;
	}

	inline static int32_t get_offset_of_replaceableFlags_1() { return static_cast<int32_t>(offsetof(BaseCompiler_t69158820_StaticFields, ___replaceableFlags_1)); }
	inline int32_t get_replaceableFlags_1() const { return ___replaceableFlags_1; }
	inline int32_t* get_address_of_replaceableFlags_1() { return &___replaceableFlags_1; }
	inline void set_replaceableFlags_1(int32_t value)
	{
		___replaceableFlags_1 = value;
	}

	inline static int32_t get_offset_of_thisRef_10() { return static_cast<int32_t>(offsetof(BaseCompiler_t69158820_StaticFields, ___thisRef_10)); }
	inline CodeThisReferenceExpression_t1372480056 * get_thisRef_10() const { return ___thisRef_10; }
	inline CodeThisReferenceExpression_t1372480056 ** get_address_of_thisRef_10() { return &___thisRef_10; }
	inline void set_thisRef_10(CodeThisReferenceExpression_t1372480056 * value)
	{
		___thisRef_10 = value;
		Il2CppCodeGenWriteBarrier(&___thisRef_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
