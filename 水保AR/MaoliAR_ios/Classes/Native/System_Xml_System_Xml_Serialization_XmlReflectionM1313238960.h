﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Xml.Serialization.SoapAttributes
struct SoapAttributes_t929401768;
// System.Xml.Serialization.XmlAttributes
struct XmlAttributes_t3116019767;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlReflectionMember
struct  XmlReflectionMember_t1313238960  : public Il2CppObject
{
public:
	// System.Boolean System.Xml.Serialization.XmlReflectionMember::isReturnValue
	bool ___isReturnValue_0;
	// System.String System.Xml.Serialization.XmlReflectionMember::memberName
	String_t* ___memberName_1;
	// System.Type System.Xml.Serialization.XmlReflectionMember::memberType
	Type_t * ___memberType_2;
	// System.Boolean System.Xml.Serialization.XmlReflectionMember::overrideIsNullable
	bool ___overrideIsNullable_3;
	// System.Xml.Serialization.SoapAttributes System.Xml.Serialization.XmlReflectionMember::soapAttributes
	SoapAttributes_t929401768 * ___soapAttributes_4;
	// System.Xml.Serialization.XmlAttributes System.Xml.Serialization.XmlReflectionMember::xmlAttributes
	XmlAttributes_t3116019767 * ___xmlAttributes_5;
	// System.Type System.Xml.Serialization.XmlReflectionMember::declaringType
	Type_t * ___declaringType_6;

public:
	inline static int32_t get_offset_of_isReturnValue_0() { return static_cast<int32_t>(offsetof(XmlReflectionMember_t1313238960, ___isReturnValue_0)); }
	inline bool get_isReturnValue_0() const { return ___isReturnValue_0; }
	inline bool* get_address_of_isReturnValue_0() { return &___isReturnValue_0; }
	inline void set_isReturnValue_0(bool value)
	{
		___isReturnValue_0 = value;
	}

	inline static int32_t get_offset_of_memberName_1() { return static_cast<int32_t>(offsetof(XmlReflectionMember_t1313238960, ___memberName_1)); }
	inline String_t* get_memberName_1() const { return ___memberName_1; }
	inline String_t** get_address_of_memberName_1() { return &___memberName_1; }
	inline void set_memberName_1(String_t* value)
	{
		___memberName_1 = value;
		Il2CppCodeGenWriteBarrier(&___memberName_1, value);
	}

	inline static int32_t get_offset_of_memberType_2() { return static_cast<int32_t>(offsetof(XmlReflectionMember_t1313238960, ___memberType_2)); }
	inline Type_t * get_memberType_2() const { return ___memberType_2; }
	inline Type_t ** get_address_of_memberType_2() { return &___memberType_2; }
	inline void set_memberType_2(Type_t * value)
	{
		___memberType_2 = value;
		Il2CppCodeGenWriteBarrier(&___memberType_2, value);
	}

	inline static int32_t get_offset_of_overrideIsNullable_3() { return static_cast<int32_t>(offsetof(XmlReflectionMember_t1313238960, ___overrideIsNullable_3)); }
	inline bool get_overrideIsNullable_3() const { return ___overrideIsNullable_3; }
	inline bool* get_address_of_overrideIsNullable_3() { return &___overrideIsNullable_3; }
	inline void set_overrideIsNullable_3(bool value)
	{
		___overrideIsNullable_3 = value;
	}

	inline static int32_t get_offset_of_soapAttributes_4() { return static_cast<int32_t>(offsetof(XmlReflectionMember_t1313238960, ___soapAttributes_4)); }
	inline SoapAttributes_t929401768 * get_soapAttributes_4() const { return ___soapAttributes_4; }
	inline SoapAttributes_t929401768 ** get_address_of_soapAttributes_4() { return &___soapAttributes_4; }
	inline void set_soapAttributes_4(SoapAttributes_t929401768 * value)
	{
		___soapAttributes_4 = value;
		Il2CppCodeGenWriteBarrier(&___soapAttributes_4, value);
	}

	inline static int32_t get_offset_of_xmlAttributes_5() { return static_cast<int32_t>(offsetof(XmlReflectionMember_t1313238960, ___xmlAttributes_5)); }
	inline XmlAttributes_t3116019767 * get_xmlAttributes_5() const { return ___xmlAttributes_5; }
	inline XmlAttributes_t3116019767 ** get_address_of_xmlAttributes_5() { return &___xmlAttributes_5; }
	inline void set_xmlAttributes_5(XmlAttributes_t3116019767 * value)
	{
		___xmlAttributes_5 = value;
		Il2CppCodeGenWriteBarrier(&___xmlAttributes_5, value);
	}

	inline static int32_t get_offset_of_declaringType_6() { return static_cast<int32_t>(offsetof(XmlReflectionMember_t1313238960, ___declaringType_6)); }
	inline Type_t * get_declaringType_6() const { return ___declaringType_6; }
	inline Type_t ** get_address_of_declaringType_6() { return &___declaringType_6; }
	inline void set_declaringType_6(Type_t * value)
	{
		___declaringType_6 = value;
		Il2CppCodeGenWriteBarrier(&___declaringType_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
