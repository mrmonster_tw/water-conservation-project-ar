﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Security_S2980594242.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.SecurityAlgorithmSuite/SecurityAlgorithmSuiteImplBase
struct  SecurityAlgorithmSuiteImplBase_t1875199571  : public SecurityAlgorithmSuite_t2980594242
{
public:
	// System.Int32 System.ServiceModel.Security.SecurityAlgorithmSuite/SecurityAlgorithmSuiteImplBase::size
	int32_t ___size_16;
	// System.Boolean System.ServiceModel.Security.SecurityAlgorithmSuite/SecurityAlgorithmSuiteImplBase::rsa15
	bool ___rsa15_17;
	// System.Boolean System.ServiceModel.Security.SecurityAlgorithmSuite/SecurityAlgorithmSuiteImplBase::sha256
	bool ___sha256_18;
	// System.Boolean System.ServiceModel.Security.SecurityAlgorithmSuite/SecurityAlgorithmSuiteImplBase::tdes
	bool ___tdes_19;

public:
	inline static int32_t get_offset_of_size_16() { return static_cast<int32_t>(offsetof(SecurityAlgorithmSuiteImplBase_t1875199571, ___size_16)); }
	inline int32_t get_size_16() const { return ___size_16; }
	inline int32_t* get_address_of_size_16() { return &___size_16; }
	inline void set_size_16(int32_t value)
	{
		___size_16 = value;
	}

	inline static int32_t get_offset_of_rsa15_17() { return static_cast<int32_t>(offsetof(SecurityAlgorithmSuiteImplBase_t1875199571, ___rsa15_17)); }
	inline bool get_rsa15_17() const { return ___rsa15_17; }
	inline bool* get_address_of_rsa15_17() { return &___rsa15_17; }
	inline void set_rsa15_17(bool value)
	{
		___rsa15_17 = value;
	}

	inline static int32_t get_offset_of_sha256_18() { return static_cast<int32_t>(offsetof(SecurityAlgorithmSuiteImplBase_t1875199571, ___sha256_18)); }
	inline bool get_sha256_18() const { return ___sha256_18; }
	inline bool* get_address_of_sha256_18() { return &___sha256_18; }
	inline void set_sha256_18(bool value)
	{
		___sha256_18 = value;
	}

	inline static int32_t get_offset_of_tdes_19() { return static_cast<int32_t>(offsetof(SecurityAlgorithmSuiteImplBase_t1875199571, ___tdes_19)); }
	inline bool get_tdes_19() const { return ___tdes_19; }
	inline bool* get_address_of_tdes_19() { return &___tdes_19; }
	inline void set_tdes_19(bool value)
	{
		___tdes_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
