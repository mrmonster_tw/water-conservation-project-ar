﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3156163955.h"

// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationElementProperty
struct ConfigurationElementProperty_t939439970;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.SessionStateSection
struct  SessionStateSection_t500006340  : public ConfigurationSection_t3156163955
{
public:

public:
};

struct SessionStateSection_t500006340_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Web.Configuration.SessionStateSection::allowCustomSqlDatabaseProp
	ConfigurationProperty_t3590861854 * ___allowCustomSqlDatabaseProp_17;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.SessionStateSection::cookielessProp
	ConfigurationProperty_t3590861854 * ___cookielessProp_18;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.SessionStateSection::cookieNameProp
	ConfigurationProperty_t3590861854 * ___cookieNameProp_19;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.SessionStateSection::customProviderProp
	ConfigurationProperty_t3590861854 * ___customProviderProp_20;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.SessionStateSection::modeProp
	ConfigurationProperty_t3590861854 * ___modeProp_21;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.SessionStateSection::partitionResolverTypeProp
	ConfigurationProperty_t3590861854 * ___partitionResolverTypeProp_22;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.SessionStateSection::providersProp
	ConfigurationProperty_t3590861854 * ___providersProp_23;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.SessionStateSection::regenerateExpiredSessionIdProp
	ConfigurationProperty_t3590861854 * ___regenerateExpiredSessionIdProp_24;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.SessionStateSection::sessionIDManagerTypeProp
	ConfigurationProperty_t3590861854 * ___sessionIDManagerTypeProp_25;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.SessionStateSection::sqlCommandTimeoutProp
	ConfigurationProperty_t3590861854 * ___sqlCommandTimeoutProp_26;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.SessionStateSection::sqlConnectionStringProp
	ConfigurationProperty_t3590861854 * ___sqlConnectionStringProp_27;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.SessionStateSection::stateConnectionStringProp
	ConfigurationProperty_t3590861854 * ___stateConnectionStringProp_28;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.SessionStateSection::stateNetworkTimeoutProp
	ConfigurationProperty_t3590861854 * ___stateNetworkTimeoutProp_29;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.SessionStateSection::timeoutProp
	ConfigurationProperty_t3590861854 * ___timeoutProp_30;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.SessionStateSection::useHostingIdentityProp
	ConfigurationProperty_t3590861854 * ___useHostingIdentityProp_31;
	// System.Configuration.ConfigurationPropertyCollection System.Web.Configuration.SessionStateSection::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_32;
	// System.Configuration.ConfigurationElementProperty System.Web.Configuration.SessionStateSection::elementProperty
	ConfigurationElementProperty_t939439970 * ___elementProperty_33;

public:
	inline static int32_t get_offset_of_allowCustomSqlDatabaseProp_17() { return static_cast<int32_t>(offsetof(SessionStateSection_t500006340_StaticFields, ___allowCustomSqlDatabaseProp_17)); }
	inline ConfigurationProperty_t3590861854 * get_allowCustomSqlDatabaseProp_17() const { return ___allowCustomSqlDatabaseProp_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_allowCustomSqlDatabaseProp_17() { return &___allowCustomSqlDatabaseProp_17; }
	inline void set_allowCustomSqlDatabaseProp_17(ConfigurationProperty_t3590861854 * value)
	{
		___allowCustomSqlDatabaseProp_17 = value;
		Il2CppCodeGenWriteBarrier(&___allowCustomSqlDatabaseProp_17, value);
	}

	inline static int32_t get_offset_of_cookielessProp_18() { return static_cast<int32_t>(offsetof(SessionStateSection_t500006340_StaticFields, ___cookielessProp_18)); }
	inline ConfigurationProperty_t3590861854 * get_cookielessProp_18() const { return ___cookielessProp_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_cookielessProp_18() { return &___cookielessProp_18; }
	inline void set_cookielessProp_18(ConfigurationProperty_t3590861854 * value)
	{
		___cookielessProp_18 = value;
		Il2CppCodeGenWriteBarrier(&___cookielessProp_18, value);
	}

	inline static int32_t get_offset_of_cookieNameProp_19() { return static_cast<int32_t>(offsetof(SessionStateSection_t500006340_StaticFields, ___cookieNameProp_19)); }
	inline ConfigurationProperty_t3590861854 * get_cookieNameProp_19() const { return ___cookieNameProp_19; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_cookieNameProp_19() { return &___cookieNameProp_19; }
	inline void set_cookieNameProp_19(ConfigurationProperty_t3590861854 * value)
	{
		___cookieNameProp_19 = value;
		Il2CppCodeGenWriteBarrier(&___cookieNameProp_19, value);
	}

	inline static int32_t get_offset_of_customProviderProp_20() { return static_cast<int32_t>(offsetof(SessionStateSection_t500006340_StaticFields, ___customProviderProp_20)); }
	inline ConfigurationProperty_t3590861854 * get_customProviderProp_20() const { return ___customProviderProp_20; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_customProviderProp_20() { return &___customProviderProp_20; }
	inline void set_customProviderProp_20(ConfigurationProperty_t3590861854 * value)
	{
		___customProviderProp_20 = value;
		Il2CppCodeGenWriteBarrier(&___customProviderProp_20, value);
	}

	inline static int32_t get_offset_of_modeProp_21() { return static_cast<int32_t>(offsetof(SessionStateSection_t500006340_StaticFields, ___modeProp_21)); }
	inline ConfigurationProperty_t3590861854 * get_modeProp_21() const { return ___modeProp_21; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_modeProp_21() { return &___modeProp_21; }
	inline void set_modeProp_21(ConfigurationProperty_t3590861854 * value)
	{
		___modeProp_21 = value;
		Il2CppCodeGenWriteBarrier(&___modeProp_21, value);
	}

	inline static int32_t get_offset_of_partitionResolverTypeProp_22() { return static_cast<int32_t>(offsetof(SessionStateSection_t500006340_StaticFields, ___partitionResolverTypeProp_22)); }
	inline ConfigurationProperty_t3590861854 * get_partitionResolverTypeProp_22() const { return ___partitionResolverTypeProp_22; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_partitionResolverTypeProp_22() { return &___partitionResolverTypeProp_22; }
	inline void set_partitionResolverTypeProp_22(ConfigurationProperty_t3590861854 * value)
	{
		___partitionResolverTypeProp_22 = value;
		Il2CppCodeGenWriteBarrier(&___partitionResolverTypeProp_22, value);
	}

	inline static int32_t get_offset_of_providersProp_23() { return static_cast<int32_t>(offsetof(SessionStateSection_t500006340_StaticFields, ___providersProp_23)); }
	inline ConfigurationProperty_t3590861854 * get_providersProp_23() const { return ___providersProp_23; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_providersProp_23() { return &___providersProp_23; }
	inline void set_providersProp_23(ConfigurationProperty_t3590861854 * value)
	{
		___providersProp_23 = value;
		Il2CppCodeGenWriteBarrier(&___providersProp_23, value);
	}

	inline static int32_t get_offset_of_regenerateExpiredSessionIdProp_24() { return static_cast<int32_t>(offsetof(SessionStateSection_t500006340_StaticFields, ___regenerateExpiredSessionIdProp_24)); }
	inline ConfigurationProperty_t3590861854 * get_regenerateExpiredSessionIdProp_24() const { return ___regenerateExpiredSessionIdProp_24; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_regenerateExpiredSessionIdProp_24() { return &___regenerateExpiredSessionIdProp_24; }
	inline void set_regenerateExpiredSessionIdProp_24(ConfigurationProperty_t3590861854 * value)
	{
		___regenerateExpiredSessionIdProp_24 = value;
		Il2CppCodeGenWriteBarrier(&___regenerateExpiredSessionIdProp_24, value);
	}

	inline static int32_t get_offset_of_sessionIDManagerTypeProp_25() { return static_cast<int32_t>(offsetof(SessionStateSection_t500006340_StaticFields, ___sessionIDManagerTypeProp_25)); }
	inline ConfigurationProperty_t3590861854 * get_sessionIDManagerTypeProp_25() const { return ___sessionIDManagerTypeProp_25; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_sessionIDManagerTypeProp_25() { return &___sessionIDManagerTypeProp_25; }
	inline void set_sessionIDManagerTypeProp_25(ConfigurationProperty_t3590861854 * value)
	{
		___sessionIDManagerTypeProp_25 = value;
		Il2CppCodeGenWriteBarrier(&___sessionIDManagerTypeProp_25, value);
	}

	inline static int32_t get_offset_of_sqlCommandTimeoutProp_26() { return static_cast<int32_t>(offsetof(SessionStateSection_t500006340_StaticFields, ___sqlCommandTimeoutProp_26)); }
	inline ConfigurationProperty_t3590861854 * get_sqlCommandTimeoutProp_26() const { return ___sqlCommandTimeoutProp_26; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_sqlCommandTimeoutProp_26() { return &___sqlCommandTimeoutProp_26; }
	inline void set_sqlCommandTimeoutProp_26(ConfigurationProperty_t3590861854 * value)
	{
		___sqlCommandTimeoutProp_26 = value;
		Il2CppCodeGenWriteBarrier(&___sqlCommandTimeoutProp_26, value);
	}

	inline static int32_t get_offset_of_sqlConnectionStringProp_27() { return static_cast<int32_t>(offsetof(SessionStateSection_t500006340_StaticFields, ___sqlConnectionStringProp_27)); }
	inline ConfigurationProperty_t3590861854 * get_sqlConnectionStringProp_27() const { return ___sqlConnectionStringProp_27; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_sqlConnectionStringProp_27() { return &___sqlConnectionStringProp_27; }
	inline void set_sqlConnectionStringProp_27(ConfigurationProperty_t3590861854 * value)
	{
		___sqlConnectionStringProp_27 = value;
		Il2CppCodeGenWriteBarrier(&___sqlConnectionStringProp_27, value);
	}

	inline static int32_t get_offset_of_stateConnectionStringProp_28() { return static_cast<int32_t>(offsetof(SessionStateSection_t500006340_StaticFields, ___stateConnectionStringProp_28)); }
	inline ConfigurationProperty_t3590861854 * get_stateConnectionStringProp_28() const { return ___stateConnectionStringProp_28; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_stateConnectionStringProp_28() { return &___stateConnectionStringProp_28; }
	inline void set_stateConnectionStringProp_28(ConfigurationProperty_t3590861854 * value)
	{
		___stateConnectionStringProp_28 = value;
		Il2CppCodeGenWriteBarrier(&___stateConnectionStringProp_28, value);
	}

	inline static int32_t get_offset_of_stateNetworkTimeoutProp_29() { return static_cast<int32_t>(offsetof(SessionStateSection_t500006340_StaticFields, ___stateNetworkTimeoutProp_29)); }
	inline ConfigurationProperty_t3590861854 * get_stateNetworkTimeoutProp_29() const { return ___stateNetworkTimeoutProp_29; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_stateNetworkTimeoutProp_29() { return &___stateNetworkTimeoutProp_29; }
	inline void set_stateNetworkTimeoutProp_29(ConfigurationProperty_t3590861854 * value)
	{
		___stateNetworkTimeoutProp_29 = value;
		Il2CppCodeGenWriteBarrier(&___stateNetworkTimeoutProp_29, value);
	}

	inline static int32_t get_offset_of_timeoutProp_30() { return static_cast<int32_t>(offsetof(SessionStateSection_t500006340_StaticFields, ___timeoutProp_30)); }
	inline ConfigurationProperty_t3590861854 * get_timeoutProp_30() const { return ___timeoutProp_30; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_timeoutProp_30() { return &___timeoutProp_30; }
	inline void set_timeoutProp_30(ConfigurationProperty_t3590861854 * value)
	{
		___timeoutProp_30 = value;
		Il2CppCodeGenWriteBarrier(&___timeoutProp_30, value);
	}

	inline static int32_t get_offset_of_useHostingIdentityProp_31() { return static_cast<int32_t>(offsetof(SessionStateSection_t500006340_StaticFields, ___useHostingIdentityProp_31)); }
	inline ConfigurationProperty_t3590861854 * get_useHostingIdentityProp_31() const { return ___useHostingIdentityProp_31; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_useHostingIdentityProp_31() { return &___useHostingIdentityProp_31; }
	inline void set_useHostingIdentityProp_31(ConfigurationProperty_t3590861854 * value)
	{
		___useHostingIdentityProp_31 = value;
		Il2CppCodeGenWriteBarrier(&___useHostingIdentityProp_31, value);
	}

	inline static int32_t get_offset_of_properties_32() { return static_cast<int32_t>(offsetof(SessionStateSection_t500006340_StaticFields, ___properties_32)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_32() const { return ___properties_32; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_32() { return &___properties_32; }
	inline void set_properties_32(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_32 = value;
		Il2CppCodeGenWriteBarrier(&___properties_32, value);
	}

	inline static int32_t get_offset_of_elementProperty_33() { return static_cast<int32_t>(offsetof(SessionStateSection_t500006340_StaticFields, ___elementProperty_33)); }
	inline ConfigurationElementProperty_t939439970 * get_elementProperty_33() const { return ___elementProperty_33; }
	inline ConfigurationElementProperty_t939439970 ** get_address_of_elementProperty_33() { return &___elementProperty_33; }
	inline void set_elementProperty_33(ConfigurationElementProperty_t939439970 * value)
	{
		___elementProperty_33 = value;
		Il2CppCodeGenWriteBarrier(&___elementProperty_33, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
