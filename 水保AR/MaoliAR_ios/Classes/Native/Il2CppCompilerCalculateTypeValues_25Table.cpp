﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_Configuration_PerformanceCounter4093363992.h"
#include "System_System_Net_Configuration_ProxyElement3214064751.h"
#include "System_System_Net_Configuration_ProxyElement_Bypass945670496.h"
#include "System_System_Net_Configuration_ProxyElement_UseSy2711047072.h"
#include "System_System_Net_Configuration_ProxyElement_AutoD1649618618.h"
#include "System_System_Net_Configuration_RequestCachingSect1693238435.h"
#include "System_System_Net_Configuration_ServicePointManage2768640361.h"
#include "System_System_Net_Configuration_SettingsSection1259474535.h"
#include "System_System_Net_Configuration_SocketElement3329874080.h"
#include "System_System_Net_Configuration_WebProxyScriptEleme477406598.h"
#include "System_System_Net_Configuration_WebRequestModuleEle925190782.h"
#include "System_System_Net_Configuration_WebRequestModuleEl1406085120.h"
#include "System_System_Net_Configuration_WebRequestModuleHan914399239.h"
#include "System_System_Net_Configuration_WebRequestModulesS4132732301.h"
#include "System_System_Net_CookieCollection3881042616.h"
#include "System_System_Net_CookieCollection_CookieCollectio1373927847.h"
#include "System_System_Net_CookieContainer2331592909.h"
#include "System_System_Net_Cookie993873397.h"
#include "System_System_Net_CookieException2325395694.h"
#include "System_System_Net_DecompressionMethods1612219745.h"
#include "System_System_Net_DefaultCertificatePolicy3607119947.h"
#include "System_System_Net_Dns384099571.h"
#include "System_System_Net_EndPoint982345378.h"
#include "System_System_Net_EndPointListener2984434924.h"
#include "System_System_Net_EndPointManager1428684201.h"
#include "System_System_Net_FileWebRequestCreator1781329382.h"
#include "System_System_Net_FileWebRequest591858885.h"
#include "System_System_Net_FileWebRequest_FileWebStream586107972.h"
#include "System_System_Net_FileWebRequest_GetResponseCallba2326689408.h"
#include "System_System_Net_FileWebResponse544571260.h"
#include "System_System_Net_FtpAsyncResult3265664217.h"
#include "System_System_Net_FtpDataStream1366729715.h"
#include "System_System_Net_FtpDataStream_WriteDelegate2016697242.h"
#include "System_System_Net_FtpDataStream_ReadDelegate4266946825.h"
#include "System_System_Net_FtpRequestCreator2926281497.h"
#include "System_System_Net_FtpStatusCode58879933.h"
#include "System_System_Net_FtpWebRequest1577818305.h"
#include "System_System_Net_FtpWebRequest_RequestState4091696808.h"
#include "System_System_Net_FtpStatus2376455776.h"
#include "System_System_Net_FtpWebResponse3940763575.h"
#include "System_System_Net_GlobalProxySelection1166292522.h"
#include "System_System_Net_HttpConnection269576101.h"
#include "System_System_Net_HttpConnection_InputState4051538957.h"
#include "System_System_Net_HttpConnection_LineState768597569.h"
#include "System_System_Net_HttpListenerBasicIdentity3019963659.h"
#include "System_System_Net_HttpListenerContext424880822.h"
#include "System_System_Net_HttpListener988452056.h"
#include "System_System_Net_HttpListenerException1795897741.h"
#include "System_System_Net_HttpListenerPrefixCollection2963430373.h"
#include "System_System_Net_HttpListenerRequest630699488.h"
#include "System_System_Net_HttpListenerResponse3502667045.h"
#include "System_System_Net_HttpRequestCreator1984314013.h"
#include "System_System_Net_HttpStatusCode3035121829.h"
#include "System_System_Net_HttpStreamAsyncResult1178010344.h"
#include "System_System_Net_HttpUtility2559916872.h"
#include "System_System_Net_HttpVersion346520293.h"
#include "System_System_Net_HttpWebRequest1669436515.h"
#include "System_System_Net_HttpWebResponse3286585418.h"
#include "System_System_Net_CookieParser2349142305.h"
#include "System_System_Net_IPAddress241777590.h"
#include "System_System_Net_IPEndPoint3791887218.h"
#include "System_System_Net_IPHostEntry263743900.h"
#include "System_System_Net_IPv6Address2709566769.h"
#include "System_System_Net_ListenerAsyncResult871495091.h"
#include "System_System_Net_ListenerPrefix3570496559.h"
#include "System_System_Net_Mime_ContentType768484892.h"
#include "System_System_Net_NetConfig2828594564.h"
#include "System_System_Net_NetworkCredential3282608323.h"
#include "System_System_Net_ProtocolViolationException4144007430.h"
#include "System_System_Net_RequestStream762880582.h"
#include "System_System_Net_ResponseStream3810703494.h"
#include "System_System_Net_Security_AuthenticatedStream3415418016.h"
#include "System_System_Net_Security_AuthenticationLevel1236753641.h"
#include "System_System_Net_Security_ProtectionLevel2559578148.h"
#include "System_System_Net_SecurityProtocolType2721465497.h"
#include "System_System_Net_Security_SslStream2700741536.h"
#include "System_System_Net_Security_SslStream_U3CBeginAuthe1222040293.h"
#include "System_System_Net_Security_SslPolicyErrors2205227823.h"
#include "System_System_Net_ServicePoint2786966844.h"
#include "System_System_Net_ServicePointManager170559685.h"
#include "System_System_Net_ServicePointManager_SPKey3654231119.h"
#include "System_System_Net_ServicePointManager_ChainValidati320539540.h"
#include "System_System_Net_SocketAddress3739769427.h"
#include "System_System_Net_Sockets_AddressFamily2612549059.h"
#include "System_System_Net_Sockets_LingerOption2688985448.h"
#include "System_System_Net_Sockets_MulticastOption3861143239.h"
#include "System_System_Net_Sockets_NetworkStream4071955934.h"
#include "System_System_Net_Sockets_ProtocolType303635025.h"
#include "System_System_Net_Sockets_SelectMode1123767949.h"
#include "System_System_Net_Sockets_Socket1119025450.h"
#include "System_System_Net_Sockets_Socket_SocketOperation1288882297.h"
#include "System_System_Net_Sockets_Socket_SocketAsyncResult2080034863.h"
#include "System_System_Net_Sockets_Socket_Worker2051517921.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (PerformanceCountersElement_t4093363992), -1, sizeof(PerformanceCountersElement_t4093363992_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2500[2] = 
{
	PerformanceCountersElement_t4093363992_StaticFields::get_offset_of_enabledProp_13(),
	PerformanceCountersElement_t4093363992_StaticFields::get_offset_of_properties_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (ProxyElement_t3214064751), -1, sizeof(ProxyElement_t3214064751_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2501[6] = 
{
	ProxyElement_t3214064751_StaticFields::get_offset_of_properties_13(),
	ProxyElement_t3214064751_StaticFields::get_offset_of_autoDetectProp_14(),
	ProxyElement_t3214064751_StaticFields::get_offset_of_bypassOnLocalProp_15(),
	ProxyElement_t3214064751_StaticFields::get_offset_of_proxyAddressProp_16(),
	ProxyElement_t3214064751_StaticFields::get_offset_of_scriptLocationProp_17(),
	ProxyElement_t3214064751_StaticFields::get_offset_of_useSystemDefaultProp_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (BypassOnLocalValues_t945670496)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2502[4] = 
{
	BypassOnLocalValues_t945670496::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (UseSystemDefaultValues_t2711047072)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2503[4] = 
{
	UseSystemDefaultValues_t2711047072::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (AutoDetectValues_t1649618618)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2504[4] = 
{
	AutoDetectValues_t1649618618::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (RequestCachingSection_t1693238435), -1, sizeof(RequestCachingSection_t1693238435_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2505[7] = 
{
	RequestCachingSection_t1693238435_StaticFields::get_offset_of_properties_17(),
	RequestCachingSection_t1693238435_StaticFields::get_offset_of_defaultFtpCachePolicyProp_18(),
	RequestCachingSection_t1693238435_StaticFields::get_offset_of_defaultHttpCachePolicyProp_19(),
	RequestCachingSection_t1693238435_StaticFields::get_offset_of_defaultPolicyLevelProp_20(),
	RequestCachingSection_t1693238435_StaticFields::get_offset_of_disableAllCachingProp_21(),
	RequestCachingSection_t1693238435_StaticFields::get_offset_of_isPrivateCacheProp_22(),
	RequestCachingSection_t1693238435_StaticFields::get_offset_of_unspecifiedMaximumAgeProp_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (ServicePointManagerElement_t2768640361), -1, sizeof(ServicePointManagerElement_t2768640361_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2506[7] = 
{
	ServicePointManagerElement_t2768640361_StaticFields::get_offset_of_properties_13(),
	ServicePointManagerElement_t2768640361_StaticFields::get_offset_of_checkCertificateNameProp_14(),
	ServicePointManagerElement_t2768640361_StaticFields::get_offset_of_checkCertificateRevocationListProp_15(),
	ServicePointManagerElement_t2768640361_StaticFields::get_offset_of_dnsRefreshTimeoutProp_16(),
	ServicePointManagerElement_t2768640361_StaticFields::get_offset_of_enableDnsRoundRobinProp_17(),
	ServicePointManagerElement_t2768640361_StaticFields::get_offset_of_expect100ContinueProp_18(),
	ServicePointManagerElement_t2768640361_StaticFields::get_offset_of_useNagleAlgorithmProp_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (SettingsSection_t1259474535), -1, sizeof(SettingsSection_t1259474535_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2507[7] = 
{
	SettingsSection_t1259474535_StaticFields::get_offset_of_properties_17(),
	SettingsSection_t1259474535_StaticFields::get_offset_of_httpWebRequestProp_18(),
	SettingsSection_t1259474535_StaticFields::get_offset_of_ipv6Prop_19(),
	SettingsSection_t1259474535_StaticFields::get_offset_of_performanceCountersProp_20(),
	SettingsSection_t1259474535_StaticFields::get_offset_of_servicePointManagerProp_21(),
	SettingsSection_t1259474535_StaticFields::get_offset_of_webProxyScriptProp_22(),
	SettingsSection_t1259474535_StaticFields::get_offset_of_socketProp_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (SocketElement_t3329874080), -1, sizeof(SocketElement_t3329874080_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2508[3] = 
{
	SocketElement_t3329874080_StaticFields::get_offset_of_properties_13(),
	SocketElement_t3329874080_StaticFields::get_offset_of_alwaysUseCompletionPortsForAcceptProp_14(),
	SocketElement_t3329874080_StaticFields::get_offset_of_alwaysUseCompletionPortsForConnectProp_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (WebProxyScriptElement_t477406598), -1, sizeof(WebProxyScriptElement_t477406598_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2509[2] = 
{
	WebProxyScriptElement_t477406598_StaticFields::get_offset_of_downloadTimeoutProp_13(),
	WebProxyScriptElement_t477406598_StaticFields::get_offset_of_properties_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (WebRequestModuleElementCollection_t925190782), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (WebRequestModuleElement_t1406085120), -1, sizeof(WebRequestModuleElement_t1406085120_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2511[3] = 
{
	WebRequestModuleElement_t1406085120_StaticFields::get_offset_of_properties_13(),
	WebRequestModuleElement_t1406085120_StaticFields::get_offset_of_prefixProp_14(),
	WebRequestModuleElement_t1406085120_StaticFields::get_offset_of_typeProp_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (WebRequestModuleHandler_t914399239), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (WebRequestModulesSection_t4132732301), -1, sizeof(WebRequestModulesSection_t4132732301_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2513[2] = 
{
	WebRequestModulesSection_t4132732301_StaticFields::get_offset_of_properties_17(),
	WebRequestModulesSection_t4132732301_StaticFields::get_offset_of_webRequestModulesProp_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (CookieCollection_t3881042616), -1, sizeof(CookieCollection_t3881042616_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2514[2] = 
{
	CookieCollection_t3881042616::get_offset_of_list_0(),
	CookieCollection_t3881042616_StaticFields::get_offset_of_Comparer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (CookieCollectionComparer_t1373927847), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (CookieContainer_t2331592909), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2516[4] = 
{
	CookieContainer_t2331592909::get_offset_of_capacity_0(),
	CookieContainer_t2331592909::get_offset_of_perDomainCapacity_1(),
	CookieContainer_t2331592909::get_offset_of_maxCookieSize_2(),
	CookieContainer_t2331592909::get_offset_of_cookies_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (Cookie_t993873397), -1, sizeof(Cookie_t993873397_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2517[18] = 
{
	Cookie_t993873397::get_offset_of_comment_0(),
	Cookie_t993873397::get_offset_of_commentUri_1(),
	Cookie_t993873397::get_offset_of_discard_2(),
	Cookie_t993873397::get_offset_of_domain_3(),
	Cookie_t993873397::get_offset_of_expires_4(),
	Cookie_t993873397::get_offset_of_httpOnly_5(),
	Cookie_t993873397::get_offset_of_name_6(),
	Cookie_t993873397::get_offset_of_path_7(),
	Cookie_t993873397::get_offset_of_port_8(),
	Cookie_t993873397::get_offset_of_ports_9(),
	Cookie_t993873397::get_offset_of_secure_10(),
	Cookie_t993873397::get_offset_of_timestamp_11(),
	Cookie_t993873397::get_offset_of_val_12(),
	Cookie_t993873397::get_offset_of_version_13(),
	Cookie_t993873397_StaticFields::get_offset_of_reservedCharsName_14(),
	Cookie_t993873397_StaticFields::get_offset_of_portSeparators_15(),
	Cookie_t993873397_StaticFields::get_offset_of_tspecials_16(),
	Cookie_t993873397::get_offset_of_exact_domain_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (CookieException_t2325395694), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (DecompressionMethods_t1612219745)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2519[4] = 
{
	DecompressionMethods_t1612219745::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (DefaultCertificatePolicy_t3607119947), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (Dns_t384099571), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (EndPoint_t982345378), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (EndPointListener_t2984434924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2523[8] = 
{
	EndPointListener_t2984434924::get_offset_of_endpoint_0(),
	EndPointListener_t2984434924::get_offset_of_sock_1(),
	EndPointListener_t2984434924::get_offset_of_prefixes_2(),
	EndPointListener_t2984434924::get_offset_of_unhandled_3(),
	EndPointListener_t2984434924::get_offset_of_all_4(),
	EndPointListener_t2984434924::get_offset_of_cert_5(),
	EndPointListener_t2984434924::get_offset_of_key_6(),
	EndPointListener_t2984434924::get_offset_of_secure_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (EndPointManager_t1428684201), -1, sizeof(EndPointManager_t1428684201_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2524[1] = 
{
	EndPointManager_t1428684201_StaticFields::get_offset_of_ip_to_endpoints_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (FileWebRequestCreator_t1781329382), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (FileWebRequest_t591858885), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2526[14] = 
{
	FileWebRequest_t591858885::get_offset_of_uri_6(),
	FileWebRequest_t591858885::get_offset_of_webHeaders_7(),
	FileWebRequest_t591858885::get_offset_of_credentials_8(),
	FileWebRequest_t591858885::get_offset_of_connectionGroup_9(),
	FileWebRequest_t591858885::get_offset_of_contentLength_10(),
	FileWebRequest_t591858885::get_offset_of_fileAccess_11(),
	FileWebRequest_t591858885::get_offset_of_method_12(),
	FileWebRequest_t591858885::get_offset_of_proxy_13(),
	FileWebRequest_t591858885::get_offset_of_preAuthenticate_14(),
	FileWebRequest_t591858885::get_offset_of_timeout_15(),
	FileWebRequest_t591858885::get_offset_of_webResponse_16(),
	FileWebRequest_t591858885::get_offset_of_requestEndEvent_17(),
	FileWebRequest_t591858885::get_offset_of_requesting_18(),
	FileWebRequest_t591858885::get_offset_of_asyncResponding_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (FileWebStream_t586107972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2527[1] = 
{
	FileWebStream_t586107972::get_offset_of_webRequest_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (GetResponseCallback_t2326689408), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (FileWebResponse_t544571260), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2529[5] = 
{
	FileWebResponse_t544571260::get_offset_of_responseUri_1(),
	FileWebResponse_t544571260::get_offset_of_fileStream_2(),
	FileWebResponse_t544571260::get_offset_of_contentLength_3(),
	FileWebResponse_t544571260::get_offset_of_webHeaders_4(),
	FileWebResponse_t544571260::get_offset_of_disposed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (FtpAsyncResult_t3265664217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2530[9] = 
{
	FtpAsyncResult_t3265664217::get_offset_of_response_0(),
	FtpAsyncResult_t3265664217::get_offset_of_waitHandle_1(),
	FtpAsyncResult_t3265664217::get_offset_of_exception_2(),
	FtpAsyncResult_t3265664217::get_offset_of_callback_3(),
	FtpAsyncResult_t3265664217::get_offset_of_stream_4(),
	FtpAsyncResult_t3265664217::get_offset_of_state_5(),
	FtpAsyncResult_t3265664217::get_offset_of_completed_6(),
	FtpAsyncResult_t3265664217::get_offset_of_synch_7(),
	FtpAsyncResult_t3265664217::get_offset_of_locker_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (FtpDataStream_t1366729715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2531[5] = 
{
	FtpDataStream_t1366729715::get_offset_of_request_2(),
	FtpDataStream_t1366729715::get_offset_of_networkStream_3(),
	FtpDataStream_t1366729715::get_offset_of_disposed_4(),
	FtpDataStream_t1366729715::get_offset_of_isRead_5(),
	FtpDataStream_t1366729715::get_offset_of_totalRead_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (WriteDelegate_t2016697242), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (ReadDelegate_t4266946825), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (FtpRequestCreator_t2926281497), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (FtpStatusCode_t58879933)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2535[38] = 
{
	FtpStatusCode_t58879933::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (FtpWebRequest_t1577818305), -1, sizeof(FtpWebRequest_t1577818305_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2536[31] = 
{
	FtpWebRequest_t1577818305::get_offset_of_requestUri_6(),
	FtpWebRequest_t1577818305::get_offset_of_file_name_7(),
	FtpWebRequest_t1577818305::get_offset_of_servicePoint_8(),
	FtpWebRequest_t1577818305::get_offset_of_origDataStream_9(),
	FtpWebRequest_t1577818305::get_offset_of_dataStream_10(),
	FtpWebRequest_t1577818305::get_offset_of_controlStream_11(),
	FtpWebRequest_t1577818305::get_offset_of_controlReader_12(),
	FtpWebRequest_t1577818305::get_offset_of_credentials_13(),
	FtpWebRequest_t1577818305::get_offset_of_hostEntry_14(),
	FtpWebRequest_t1577818305::get_offset_of_localEndPoint_15(),
	FtpWebRequest_t1577818305::get_offset_of_proxy_16(),
	FtpWebRequest_t1577818305::get_offset_of_timeout_17(),
	FtpWebRequest_t1577818305::get_offset_of_rwTimeout_18(),
	FtpWebRequest_t1577818305::get_offset_of_offset_19(),
	FtpWebRequest_t1577818305::get_offset_of_binary_20(),
	FtpWebRequest_t1577818305::get_offset_of_enableSsl_21(),
	FtpWebRequest_t1577818305::get_offset_of_usePassive_22(),
	FtpWebRequest_t1577818305::get_offset_of_keepAlive_23(),
	FtpWebRequest_t1577818305::get_offset_of_method_24(),
	FtpWebRequest_t1577818305::get_offset_of_renameTo_25(),
	FtpWebRequest_t1577818305::get_offset_of_locker_26(),
	FtpWebRequest_t1577818305::get_offset_of_requestState_27(),
	FtpWebRequest_t1577818305::get_offset_of_asyncResult_28(),
	FtpWebRequest_t1577818305::get_offset_of_ftpResponse_29(),
	FtpWebRequest_t1577818305::get_offset_of_requestStream_30(),
	FtpWebRequest_t1577818305::get_offset_of_initial_path_31(),
	FtpWebRequest_t1577818305_StaticFields::get_offset_of_supportedCommands_32(),
	FtpWebRequest_t1577818305::get_offset_of_callback_33(),
	FtpWebRequest_t1577818305_StaticFields::get_offset_of_U3CU3Ef__amU24cache1C_34(),
	FtpWebRequest_t1577818305_StaticFields::get_offset_of_U3CU3Ef__switchU24mapA_35(),
	FtpWebRequest_t1577818305_StaticFields::get_offset_of_U3CU3Ef__switchU24mapB_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (RequestState_t4091696808)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2537[10] = 
{
	RequestState_t4091696808::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (FtpStatus_t2376455776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2538[2] = 
{
	FtpStatus_t2376455776::get_offset_of_statusCode_0(),
	FtpStatus_t2376455776::get_offset_of_statusDescription_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (FtpWebResponse_t3940763575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2539[12] = 
{
	FtpWebResponse_t3940763575::get_offset_of_stream_1(),
	FtpWebResponse_t3940763575::get_offset_of_uri_2(),
	FtpWebResponse_t3940763575::get_offset_of_statusCode_3(),
	FtpWebResponse_t3940763575::get_offset_of_lastModified_4(),
	FtpWebResponse_t3940763575::get_offset_of_bannerMessage_5(),
	FtpWebResponse_t3940763575::get_offset_of_welcomeMessage_6(),
	FtpWebResponse_t3940763575::get_offset_of_exitMessage_7(),
	FtpWebResponse_t3940763575::get_offset_of_statusDescription_8(),
	FtpWebResponse_t3940763575::get_offset_of_method_9(),
	FtpWebResponse_t3940763575::get_offset_of_disposed_10(),
	FtpWebResponse_t3940763575::get_offset_of_request_11(),
	FtpWebResponse_t3940763575::get_offset_of_contentLength_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (GlobalProxySelection_t1166292522), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (HttpConnection_t269576101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2541[18] = 
{
	HttpConnection_t269576101::get_offset_of_sock_0(),
	HttpConnection_t269576101::get_offset_of_stream_1(),
	HttpConnection_t269576101::get_offset_of_epl_2(),
	HttpConnection_t269576101::get_offset_of_ms_3(),
	HttpConnection_t269576101::get_offset_of_buffer_4(),
	HttpConnection_t269576101::get_offset_of_context_5(),
	HttpConnection_t269576101::get_offset_of_current_line_6(),
	HttpConnection_t269576101::get_offset_of_prefix_7(),
	HttpConnection_t269576101::get_offset_of_i_stream_8(),
	HttpConnection_t269576101::get_offset_of_o_stream_9(),
	HttpConnection_t269576101::get_offset_of_chunked_10(),
	HttpConnection_t269576101::get_offset_of_chunked_uses_11(),
	HttpConnection_t269576101::get_offset_of_context_bound_12(),
	HttpConnection_t269576101::get_offset_of_secure_13(),
	HttpConnection_t269576101::get_offset_of_key_14(),
	HttpConnection_t269576101::get_offset_of_input_state_15(),
	HttpConnection_t269576101::get_offset_of_line_state_16(),
	HttpConnection_t269576101::get_offset_of_position_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (InputState_t4051538957)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2542[3] = 
{
	InputState_t4051538957::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (LineState_t768597569)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2543[4] = 
{
	LineState_t768597569::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (HttpListenerBasicIdentity_t3019963659), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2544[1] = 
{
	HttpListenerBasicIdentity_t3019963659::get_offset_of_password_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { sizeof (HttpListenerContext_t424880822), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2545[7] = 
{
	HttpListenerContext_t424880822::get_offset_of_request_0(),
	HttpListenerContext_t424880822::get_offset_of_response_1(),
	HttpListenerContext_t424880822::get_offset_of_user_2(),
	HttpListenerContext_t424880822::get_offset_of_cnc_3(),
	HttpListenerContext_t424880822::get_offset_of_error_4(),
	HttpListenerContext_t424880822::get_offset_of_err_status_5(),
	HttpListenerContext_t424880822::get_offset_of_Listener_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (HttpListener_t988452056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2546[11] = 
{
	HttpListener_t988452056::get_offset_of_auth_schemes_0(),
	HttpListener_t988452056::get_offset_of_prefixes_1(),
	HttpListener_t988452056::get_offset_of_auth_selector_2(),
	HttpListener_t988452056::get_offset_of_realm_3(),
	HttpListener_t988452056::get_offset_of_ignore_write_exceptions_4(),
	HttpListener_t988452056::get_offset_of_unsafe_ntlm_auth_5(),
	HttpListener_t988452056::get_offset_of_listening_6(),
	HttpListener_t988452056::get_offset_of_disposed_7(),
	HttpListener_t988452056::get_offset_of_registry_8(),
	HttpListener_t988452056::get_offset_of_ctx_queue_9(),
	HttpListener_t988452056::get_offset_of_wait_queue_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (HttpListenerException_t1795897741), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (HttpListenerPrefixCollection_t2963430373), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2548[2] = 
{
	HttpListenerPrefixCollection_t2963430373::get_offset_of_prefixes_0(),
	HttpListenerPrefixCollection_t2963430373::get_offset_of_listener_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (HttpListenerRequest_t630699488), -1, sizeof(HttpListenerRequest_t630699488_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2549[19] = 
{
	HttpListenerRequest_t630699488::get_offset_of_accept_types_0(),
	HttpListenerRequest_t630699488::get_offset_of_content_length_1(),
	HttpListenerRequest_t630699488::get_offset_of_cl_set_2(),
	HttpListenerRequest_t630699488::get_offset_of_cookies_3(),
	HttpListenerRequest_t630699488::get_offset_of_headers_4(),
	HttpListenerRequest_t630699488::get_offset_of_method_5(),
	HttpListenerRequest_t630699488::get_offset_of_input_stream_6(),
	HttpListenerRequest_t630699488::get_offset_of_version_7(),
	HttpListenerRequest_t630699488::get_offset_of_query_string_8(),
	HttpListenerRequest_t630699488::get_offset_of_raw_url_9(),
	HttpListenerRequest_t630699488::get_offset_of_url_10(),
	HttpListenerRequest_t630699488::get_offset_of_referrer_11(),
	HttpListenerRequest_t630699488::get_offset_of_user_languages_12(),
	HttpListenerRequest_t630699488::get_offset_of_context_13(),
	HttpListenerRequest_t630699488::get_offset_of_is_chunked_14(),
	HttpListenerRequest_t630699488_StaticFields::get_offset_of__100continue_15(),
	HttpListenerRequest_t630699488_StaticFields::get_offset_of_no_body_methods_16(),
	HttpListenerRequest_t630699488_StaticFields::get_offset_of_separators_17(),
	HttpListenerRequest_t630699488_StaticFields::get_offset_of_U3CU3Ef__switchU24mapC_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (HttpListenerResponse_t3502667045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2550[17] = 
{
	HttpListenerResponse_t3502667045::get_offset_of_disposed_0(),
	HttpListenerResponse_t3502667045::get_offset_of_content_encoding_1(),
	HttpListenerResponse_t3502667045::get_offset_of_content_length_2(),
	HttpListenerResponse_t3502667045::get_offset_of_cl_set_3(),
	HttpListenerResponse_t3502667045::get_offset_of_content_type_4(),
	HttpListenerResponse_t3502667045::get_offset_of_cookies_5(),
	HttpListenerResponse_t3502667045::get_offset_of_headers_6(),
	HttpListenerResponse_t3502667045::get_offset_of_keep_alive_7(),
	HttpListenerResponse_t3502667045::get_offset_of_output_stream_8(),
	HttpListenerResponse_t3502667045::get_offset_of_version_9(),
	HttpListenerResponse_t3502667045::get_offset_of_location_10(),
	HttpListenerResponse_t3502667045::get_offset_of_status_code_11(),
	HttpListenerResponse_t3502667045::get_offset_of_status_description_12(),
	HttpListenerResponse_t3502667045::get_offset_of_chunked_13(),
	HttpListenerResponse_t3502667045::get_offset_of_context_14(),
	HttpListenerResponse_t3502667045::get_offset_of_HeadersSent_15(),
	HttpListenerResponse_t3502667045::get_offset_of_force_close_chunked_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { sizeof (HttpRequestCreator_t1984314013), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (HttpStatusCode_t3035121829)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2552[47] = 
{
	HttpStatusCode_t3035121829::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { sizeof (HttpStreamAsyncResult_t1178010344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2553[10] = 
{
	HttpStreamAsyncResult_t1178010344::get_offset_of_locker_0(),
	HttpStreamAsyncResult_t1178010344::get_offset_of_handle_1(),
	HttpStreamAsyncResult_t1178010344::get_offset_of_completed_2(),
	HttpStreamAsyncResult_t1178010344::get_offset_of_Buffer_3(),
	HttpStreamAsyncResult_t1178010344::get_offset_of_Offset_4(),
	HttpStreamAsyncResult_t1178010344::get_offset_of_Count_5(),
	HttpStreamAsyncResult_t1178010344::get_offset_of_Callback_6(),
	HttpStreamAsyncResult_t1178010344::get_offset_of_State_7(),
	HttpStreamAsyncResult_t1178010344::get_offset_of_SynchRead_8(),
	HttpStreamAsyncResult_t1178010344::get_offset_of_Error_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { sizeof (HttpUtility_t2559916872), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { sizeof (HttpVersion_t346520293), -1, sizeof(HttpVersion_t346520293_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2555[2] = 
{
	HttpVersion_t346520293_StaticFields::get_offset_of_Version10_0(),
	HttpVersion_t346520293_StaticFields::get_offset_of_Version11_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { sizeof (HttpWebRequest_t1669436515), -1, sizeof(HttpWebRequest_t1669436515_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2556[50] = 
{
	HttpWebRequest_t1669436515::get_offset_of_requestUri_6(),
	HttpWebRequest_t1669436515::get_offset_of_actualUri_7(),
	HttpWebRequest_t1669436515::get_offset_of_hostChanged_8(),
	HttpWebRequest_t1669436515::get_offset_of_allowAutoRedirect_9(),
	HttpWebRequest_t1669436515::get_offset_of_allowBuffering_10(),
	HttpWebRequest_t1669436515::get_offset_of_certificates_11(),
	HttpWebRequest_t1669436515::get_offset_of_connectionGroup_12(),
	HttpWebRequest_t1669436515::get_offset_of_contentLength_13(),
	HttpWebRequest_t1669436515::get_offset_of_continueDelegate_14(),
	HttpWebRequest_t1669436515::get_offset_of_cookieContainer_15(),
	HttpWebRequest_t1669436515::get_offset_of_credentials_16(),
	HttpWebRequest_t1669436515::get_offset_of_haveResponse_17(),
	HttpWebRequest_t1669436515::get_offset_of_haveRequest_18(),
	HttpWebRequest_t1669436515::get_offset_of_requestSent_19(),
	HttpWebRequest_t1669436515::get_offset_of_webHeaders_20(),
	HttpWebRequest_t1669436515::get_offset_of_keepAlive_21(),
	HttpWebRequest_t1669436515::get_offset_of_maxAutoRedirect_22(),
	HttpWebRequest_t1669436515::get_offset_of_mediaType_23(),
	HttpWebRequest_t1669436515::get_offset_of_method_24(),
	HttpWebRequest_t1669436515::get_offset_of_initialMethod_25(),
	HttpWebRequest_t1669436515::get_offset_of_pipelined_26(),
	HttpWebRequest_t1669436515::get_offset_of_preAuthenticate_27(),
	HttpWebRequest_t1669436515::get_offset_of_usedPreAuth_28(),
	HttpWebRequest_t1669436515::get_offset_of_version_29(),
	HttpWebRequest_t1669436515::get_offset_of_actualVersion_30(),
	HttpWebRequest_t1669436515::get_offset_of_proxy_31(),
	HttpWebRequest_t1669436515::get_offset_of_sendChunked_32(),
	HttpWebRequest_t1669436515::get_offset_of_servicePoint_33(),
	HttpWebRequest_t1669436515::get_offset_of_timeout_34(),
	HttpWebRequest_t1669436515::get_offset_of_writeStream_35(),
	HttpWebRequest_t1669436515::get_offset_of_webResponse_36(),
	HttpWebRequest_t1669436515::get_offset_of_asyncWrite_37(),
	HttpWebRequest_t1669436515::get_offset_of_asyncRead_38(),
	HttpWebRequest_t1669436515::get_offset_of_abortHandler_39(),
	HttpWebRequest_t1669436515::get_offset_of_aborted_40(),
	HttpWebRequest_t1669436515::get_offset_of_redirects_41(),
	HttpWebRequest_t1669436515::get_offset_of_expectContinue_42(),
	HttpWebRequest_t1669436515::get_offset_of_authCompleted_43(),
	HttpWebRequest_t1669436515::get_offset_of_bodyBuffer_44(),
	HttpWebRequest_t1669436515::get_offset_of_bodyBufferLength_45(),
	HttpWebRequest_t1669436515::get_offset_of_getResponseCalled_46(),
	HttpWebRequest_t1669436515::get_offset_of_saved_exc_47(),
	HttpWebRequest_t1669436515::get_offset_of_locker_48(),
	HttpWebRequest_t1669436515::get_offset_of_is_ntlm_auth_49(),
	HttpWebRequest_t1669436515::get_offset_of_finished_reading_50(),
	HttpWebRequest_t1669436515::get_offset_of_WebConnection_51(),
	HttpWebRequest_t1669436515::get_offset_of_auto_decomp_52(),
	HttpWebRequest_t1669436515_StaticFields::get_offset_of_defaultMaxResponseHeadersLength_53(),
	HttpWebRequest_t1669436515::get_offset_of_readWriteTimeout_54(),
	HttpWebRequest_t1669436515::get_offset_of_unsafe_auth_blah_55(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { sizeof (HttpWebResponse_t3286585418), -1, sizeof(HttpWebResponse_t3286585418_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2557[14] = 
{
	HttpWebResponse_t3286585418::get_offset_of_uri_1(),
	HttpWebResponse_t3286585418::get_offset_of_webHeaders_2(),
	HttpWebResponse_t3286585418::get_offset_of_cookieCollection_3(),
	HttpWebResponse_t3286585418::get_offset_of_method_4(),
	HttpWebResponse_t3286585418::get_offset_of_version_5(),
	HttpWebResponse_t3286585418::get_offset_of_statusCode_6(),
	HttpWebResponse_t3286585418::get_offset_of_statusDescription_7(),
	HttpWebResponse_t3286585418::get_offset_of_contentLength_8(),
	HttpWebResponse_t3286585418::get_offset_of_contentType_9(),
	HttpWebResponse_t3286585418::get_offset_of_cookie_container_10(),
	HttpWebResponse_t3286585418::get_offset_of_disposed_11(),
	HttpWebResponse_t3286585418::get_offset_of_stream_12(),
	HttpWebResponse_t3286585418::get_offset_of_cookieExpiresFormats_13(),
	HttpWebResponse_t3286585418_StaticFields::get_offset_of_U3CU3Ef__switchU24mapD_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (CookieParser_t2349142305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2558[3] = 
{
	CookieParser_t2349142305::get_offset_of_header_0(),
	CookieParser_t2349142305::get_offset_of_pos_1(),
	CookieParser_t2349142305::get_offset_of_length_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (IPAddress_t241777590), -1, sizeof(IPAddress_t241777590_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2564[12] = 
{
	IPAddress_t241777590::get_offset_of_m_Address_0(),
	IPAddress_t241777590::get_offset_of_m_Family_1(),
	IPAddress_t241777590::get_offset_of_m_Numbers_2(),
	IPAddress_t241777590::get_offset_of_m_ScopeId_3(),
	IPAddress_t241777590_StaticFields::get_offset_of_Any_4(),
	IPAddress_t241777590_StaticFields::get_offset_of_Broadcast_5(),
	IPAddress_t241777590_StaticFields::get_offset_of_Loopback_6(),
	IPAddress_t241777590_StaticFields::get_offset_of_None_7(),
	IPAddress_t241777590_StaticFields::get_offset_of_IPv6Any_8(),
	IPAddress_t241777590_StaticFields::get_offset_of_IPv6Loopback_9(),
	IPAddress_t241777590_StaticFields::get_offset_of_IPv6None_10(),
	IPAddress_t241777590::get_offset_of_m_HashCode_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (IPEndPoint_t3791887218), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2565[2] = 
{
	IPEndPoint_t3791887218::get_offset_of_address_0(),
	IPEndPoint_t3791887218::get_offset_of_port_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (IPHostEntry_t263743900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2566[3] = 
{
	IPHostEntry_t263743900::get_offset_of_addressList_0(),
	IPHostEntry_t263743900::get_offset_of_aliases_1(),
	IPHostEntry_t263743900::get_offset_of_hostName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (IPv6Address_t2709566769), -1, sizeof(IPv6Address_t2709566769_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2567[5] = 
{
	IPv6Address_t2709566769::get_offset_of_address_0(),
	IPv6Address_t2709566769::get_offset_of_prefixLength_1(),
	IPv6Address_t2709566769::get_offset_of_scopeId_2(),
	IPv6Address_t2709566769_StaticFields::get_offset_of_Loopback_3(),
	IPv6Address_t2709566769_StaticFields::get_offset_of_Unspecified_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (ListenerAsyncResult_t871495091), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2570[9] = 
{
	ListenerAsyncResult_t871495091::get_offset_of_handle_0(),
	ListenerAsyncResult_t871495091::get_offset_of_synch_1(),
	ListenerAsyncResult_t871495091::get_offset_of_completed_2(),
	ListenerAsyncResult_t871495091::get_offset_of_cb_3(),
	ListenerAsyncResult_t871495091::get_offset_of_state_4(),
	ListenerAsyncResult_t871495091::get_offset_of_exception_5(),
	ListenerAsyncResult_t871495091::get_offset_of_context_6(),
	ListenerAsyncResult_t871495091::get_offset_of_locker_7(),
	ListenerAsyncResult_t871495091::get_offset_of_forward_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (ListenerPrefix_t3570496559), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2571[6] = 
{
	ListenerPrefix_t3570496559::get_offset_of_original_0(),
	ListenerPrefix_t3570496559::get_offset_of_host_1(),
	ListenerPrefix_t3570496559::get_offset_of_port_2(),
	ListenerPrefix_t3570496559::get_offset_of_path_3(),
	ListenerPrefix_t3570496559::get_offset_of_secure_4(),
	ListenerPrefix_t3570496559::get_offset_of_Listener_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { sizeof (ContentType_t768484892), -1, sizeof(ContentType_t768484892_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2572[3] = 
{
	ContentType_t768484892::get_offset_of_mediaType_0(),
	ContentType_t768484892::get_offset_of_parameters_1(),
	ContentType_t768484892_StaticFields::get_offset_of_especials_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (NetConfig_t2828594564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2573[2] = 
{
	NetConfig_t2828594564::get_offset_of_ipv6Enabled_0(),
	NetConfig_t2828594564::get_offset_of_MaxResponseHeadersLength_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (NetworkCredential_t3282608323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2574[3] = 
{
	NetworkCredential_t3282608323::get_offset_of_userName_0(),
	NetworkCredential_t3282608323::get_offset_of_password_1(),
	NetworkCredential_t3282608323::get_offset_of_domain_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { sizeof (ProtocolViolationException_t4144007430), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (RequestStream_t762880582), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2576[6] = 
{
	RequestStream_t762880582::get_offset_of_buffer_2(),
	RequestStream_t762880582::get_offset_of_offset_3(),
	RequestStream_t762880582::get_offset_of_length_4(),
	RequestStream_t762880582::get_offset_of_remaining_body_5(),
	RequestStream_t762880582::get_offset_of_disposed_6(),
	RequestStream_t762880582::get_offset_of_stream_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (ResponseStream_t3810703494), -1, sizeof(ResponseStream_t3810703494_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2577[6] = 
{
	ResponseStream_t3810703494::get_offset_of_response_2(),
	ResponseStream_t3810703494::get_offset_of_ignore_errors_3(),
	ResponseStream_t3810703494::get_offset_of_disposed_4(),
	ResponseStream_t3810703494::get_offset_of_trailer_sent_5(),
	ResponseStream_t3810703494::get_offset_of_stream_6(),
	ResponseStream_t3810703494_StaticFields::get_offset_of_crlf_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { sizeof (AuthenticatedStream_t3415418016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2578[2] = 
{
	AuthenticatedStream_t3415418016::get_offset_of_innerStream_2(),
	AuthenticatedStream_t3415418016::get_offset_of_leaveStreamOpen_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { sizeof (AuthenticationLevel_t1236753641)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2579[4] = 
{
	AuthenticationLevel_t1236753641::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (ProtectionLevel_t2559578148)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2580[4] = 
{
	ProtectionLevel_t2559578148::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (SecurityProtocolType_t2721465497)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2581[3] = 
{
	SecurityProtocolType_t2721465497::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (SslStream_t2700741536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2582[3] = 
{
	SslStream_t2700741536::get_offset_of_ssl_stream_4(),
	SslStream_t2700741536::get_offset_of_validation_callback_5(),
	SslStream_t2700741536::get_offset_of_selection_callback_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (U3CBeginAuthenticateAsClientU3Ec__AnonStorey7_t1222040293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2583[2] = 
{
	U3CBeginAuthenticateAsClientU3Ec__AnonStorey7_t1222040293::get_offset_of_clientCertificates_0(),
	U3CBeginAuthenticateAsClientU3Ec__AnonStorey7_t1222040293::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (SslPolicyErrors_t2205227823)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2584[5] = 
{
	SslPolicyErrors_t2205227823::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (ServicePoint_t2786966844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2585[17] = 
{
	ServicePoint_t2786966844::get_offset_of_uri_0(),
	ServicePoint_t2786966844::get_offset_of_connectionLimit_1(),
	ServicePoint_t2786966844::get_offset_of_maxIdleTime_2(),
	ServicePoint_t2786966844::get_offset_of_currentConnections_3(),
	ServicePoint_t2786966844::get_offset_of_idleSince_4(),
	ServicePoint_t2786966844::get_offset_of_protocolVersion_5(),
	ServicePoint_t2786966844::get_offset_of_certificate_6(),
	ServicePoint_t2786966844::get_offset_of_clientCertificate_7(),
	ServicePoint_t2786966844::get_offset_of_host_8(),
	ServicePoint_t2786966844::get_offset_of_usesProxy_9(),
	ServicePoint_t2786966844::get_offset_of_groups_10(),
	ServicePoint_t2786966844::get_offset_of_sendContinue_11(),
	ServicePoint_t2786966844::get_offset_of_useConnect_12(),
	ServicePoint_t2786966844::get_offset_of_locker_13(),
	ServicePoint_t2786966844::get_offset_of_hostE_14(),
	ServicePoint_t2786966844::get_offset_of_useNagle_15(),
	ServicePoint_t2786966844::get_offset_of_endPointCallback_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (ServicePointManager_t170559685), -1, sizeof(ServicePointManager_t170559685_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2586[11] = 
{
	ServicePointManager_t170559685_StaticFields::get_offset_of_servicePoints_0(),
	ServicePointManager_t170559685_StaticFields::get_offset_of_policy_1(),
	ServicePointManager_t170559685_StaticFields::get_offset_of_defaultConnectionLimit_2(),
	ServicePointManager_t170559685_StaticFields::get_offset_of_maxServicePointIdleTime_3(),
	ServicePointManager_t170559685_StaticFields::get_offset_of_maxServicePoints_4(),
	ServicePointManager_t170559685_StaticFields::get_offset_of__checkCRL_5(),
	ServicePointManager_t170559685_StaticFields::get_offset_of__securityProtocol_6(),
	ServicePointManager_t170559685_StaticFields::get_offset_of_expectContinue_7(),
	ServicePointManager_t170559685_StaticFields::get_offset_of_useNagle_8(),
	ServicePointManager_t170559685_StaticFields::get_offset_of_server_cert_cb_9(),
	ServicePointManager_t170559685_StaticFields::get_offset_of_manager_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (SPKey_t3654231119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2587[2] = 
{
	SPKey_t3654231119::get_offset_of_uri_0(),
	SPKey_t3654231119::get_offset_of_use_connect_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (ChainValidationHelper_t320539540), -1, sizeof(ChainValidationHelper_t320539540_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2588[4] = 
{
	ChainValidationHelper_t320539540::get_offset_of_sender_0(),
	ChainValidationHelper_t320539540::get_offset_of_host_1(),
	ChainValidationHelper_t320539540_StaticFields::get_offset_of_is_macosx_2(),
	ChainValidationHelper_t320539540_StaticFields::get_offset_of_s_flags_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (SocketAddress_t3739769427), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2589[1] = 
{
	SocketAddress_t3739769427::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (AddressFamily_t2612549059)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2590[32] = 
{
	AddressFamily_t2612549059::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (LingerOption_t2688985448), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2591[2] = 
{
	LingerOption_t2688985448::get_offset_of_enabled_0(),
	LingerOption_t2688985448::get_offset_of_seconds_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (MulticastOption_t3861143239), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (NetworkStream_t4071955934), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2593[6] = 
{
	NetworkStream_t4071955934::get_offset_of_access_2(),
	NetworkStream_t4071955934::get_offset_of_socket_3(),
	NetworkStream_t4071955934::get_offset_of_owns_socket_4(),
	NetworkStream_t4071955934::get_offset_of_readable_5(),
	NetworkStream_t4071955934::get_offset_of_writeable_6(),
	NetworkStream_t4071955934::get_offset_of_disposed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (ProtocolType_t303635025)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2594[26] = 
{
	ProtocolType_t303635025::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (SelectMode_t1123767949)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2595[4] = 
{
	SelectMode_t1123767949::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (Socket_t1119025450), -1, sizeof(Socket_t1119025450_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2596[22] = 
{
	Socket_t1119025450::get_offset_of_readQ_0(),
	Socket_t1119025450::get_offset_of_writeQ_1(),
	Socket_t1119025450::get_offset_of_islistening_2(),
	Socket_t1119025450::get_offset_of_MinListenPort_3(),
	Socket_t1119025450::get_offset_of_MaxListenPort_4(),
	Socket_t1119025450_StaticFields::get_offset_of_ipv4Supported_5(),
	Socket_t1119025450_StaticFields::get_offset_of_ipv6Supported_6(),
	Socket_t1119025450::get_offset_of_linger_timeout_7(),
	Socket_t1119025450::get_offset_of_socket_8(),
	Socket_t1119025450::get_offset_of_address_family_9(),
	Socket_t1119025450::get_offset_of_socket_type_10(),
	Socket_t1119025450::get_offset_of_protocol_type_11(),
	Socket_t1119025450::get_offset_of_blocking_12(),
	Socket_t1119025450::get_offset_of_blocking_thread_13(),
	Socket_t1119025450::get_offset_of_isbound_14(),
	Socket_t1119025450_StaticFields::get_offset_of_current_bind_count_15(),
	Socket_t1119025450::get_offset_of_max_bind_count_16(),
	Socket_t1119025450::get_offset_of_connected_17(),
	Socket_t1119025450::get_offset_of_closed_18(),
	Socket_t1119025450::get_offset_of_disposed_19(),
	Socket_t1119025450::get_offset_of_seed_endpoint_20(),
	Socket_t1119025450_StaticFields::get_offset_of_check_socket_policy_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (SocketOperation_t1288882297)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2597[15] = 
{
	SocketOperation_t1288882297::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (SocketAsyncResult_t2080034863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2598[25] = 
{
	SocketAsyncResult_t2080034863::get_offset_of_Sock_0(),
	SocketAsyncResult_t2080034863::get_offset_of_handle_1(),
	SocketAsyncResult_t2080034863::get_offset_of_state_2(),
	SocketAsyncResult_t2080034863::get_offset_of_callback_3(),
	SocketAsyncResult_t2080034863::get_offset_of_waithandle_4(),
	SocketAsyncResult_t2080034863::get_offset_of_delayedException_5(),
	SocketAsyncResult_t2080034863::get_offset_of_EndPoint_6(),
	SocketAsyncResult_t2080034863::get_offset_of_Buffer_7(),
	SocketAsyncResult_t2080034863::get_offset_of_Offset_8(),
	SocketAsyncResult_t2080034863::get_offset_of_Size_9(),
	SocketAsyncResult_t2080034863::get_offset_of_SockFlags_10(),
	SocketAsyncResult_t2080034863::get_offset_of_AcceptSocket_11(),
	SocketAsyncResult_t2080034863::get_offset_of_Addresses_12(),
	SocketAsyncResult_t2080034863::get_offset_of_Port_13(),
	SocketAsyncResult_t2080034863::get_offset_of_Buffers_14(),
	SocketAsyncResult_t2080034863::get_offset_of_ReuseSocket_15(),
	SocketAsyncResult_t2080034863::get_offset_of_acc_socket_16(),
	SocketAsyncResult_t2080034863::get_offset_of_total_17(),
	SocketAsyncResult_t2080034863::get_offset_of_completed_sync_18(),
	SocketAsyncResult_t2080034863::get_offset_of_completed_19(),
	SocketAsyncResult_t2080034863::get_offset_of_blocking_20(),
	SocketAsyncResult_t2080034863::get_offset_of_error_21(),
	SocketAsyncResult_t2080034863::get_offset_of_operation_22(),
	SocketAsyncResult_t2080034863::get_offset_of_ares_23(),
	SocketAsyncResult_t2080034863::get_offset_of_EndCalled_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (Worker_t2051517921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2599[3] = 
{
	Worker_t2051517921::get_offset_of_result_0(),
	Worker_t2051517921::get_offset_of_requireSocketSecurity_1(),
	Worker_t2051517921::get_offset_of_send_so_far_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
