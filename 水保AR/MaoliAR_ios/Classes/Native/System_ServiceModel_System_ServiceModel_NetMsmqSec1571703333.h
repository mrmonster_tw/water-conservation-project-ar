﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.MsmqTransportSecurity
struct MsmqTransportSecurity_t2605141420;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.NetMsmqSecurity
struct  NetMsmqSecurity_t1571703333  : public Il2CppObject
{
public:
	// System.ServiceModel.MsmqTransportSecurity System.ServiceModel.NetMsmqSecurity::transport
	MsmqTransportSecurity_t2605141420 * ___transport_0;

public:
	inline static int32_t get_offset_of_transport_0() { return static_cast<int32_t>(offsetof(NetMsmqSecurity_t1571703333, ___transport_0)); }
	inline MsmqTransportSecurity_t2605141420 * get_transport_0() const { return ___transport_0; }
	inline MsmqTransportSecurity_t2605141420 ** get_address_of_transport_0() { return &___transport_0; }
	inline void set_transport_0(MsmqTransportSecurity_t2605141420 * value)
	{
		___transport_0 = value;
		Il2CppCodeGenWriteBarrier(&___transport_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
