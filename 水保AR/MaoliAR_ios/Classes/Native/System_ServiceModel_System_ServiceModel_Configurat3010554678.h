﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.ServiceElement
struct  ServiceElement_t3010554678  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct ServiceElement_t3010554678_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.ServiceElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.ServiceElement::behavior_configuration
	ConfigurationProperty_t3590861854 * ___behavior_configuration_14;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.ServiceElement::endpoints
	ConfigurationProperty_t3590861854 * ___endpoints_15;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.ServiceElement::host
	ConfigurationProperty_t3590861854 * ___host_16;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.ServiceElement::name
	ConfigurationProperty_t3590861854 * ___name_17;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(ServiceElement_t3010554678_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier(&___properties_13, value);
	}

	inline static int32_t get_offset_of_behavior_configuration_14() { return static_cast<int32_t>(offsetof(ServiceElement_t3010554678_StaticFields, ___behavior_configuration_14)); }
	inline ConfigurationProperty_t3590861854 * get_behavior_configuration_14() const { return ___behavior_configuration_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_behavior_configuration_14() { return &___behavior_configuration_14; }
	inline void set_behavior_configuration_14(ConfigurationProperty_t3590861854 * value)
	{
		___behavior_configuration_14 = value;
		Il2CppCodeGenWriteBarrier(&___behavior_configuration_14, value);
	}

	inline static int32_t get_offset_of_endpoints_15() { return static_cast<int32_t>(offsetof(ServiceElement_t3010554678_StaticFields, ___endpoints_15)); }
	inline ConfigurationProperty_t3590861854 * get_endpoints_15() const { return ___endpoints_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_endpoints_15() { return &___endpoints_15; }
	inline void set_endpoints_15(ConfigurationProperty_t3590861854 * value)
	{
		___endpoints_15 = value;
		Il2CppCodeGenWriteBarrier(&___endpoints_15, value);
	}

	inline static int32_t get_offset_of_host_16() { return static_cast<int32_t>(offsetof(ServiceElement_t3010554678_StaticFields, ___host_16)); }
	inline ConfigurationProperty_t3590861854 * get_host_16() const { return ___host_16; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_host_16() { return &___host_16; }
	inline void set_host_16(ConfigurationProperty_t3590861854 * value)
	{
		___host_16 = value;
		Il2CppCodeGenWriteBarrier(&___host_16, value);
	}

	inline static int32_t get_offset_of_name_17() { return static_cast<int32_t>(offsetof(ServiceElement_t3010554678_StaticFields, ___name_17)); }
	inline ConfigurationProperty_t3590861854 * get_name_17() const { return ___name_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_name_17() { return &___name_17; }
	inline void set_name_17(ConfigurationProperty_t3590861854 * value)
	{
		___name_17 = value;
		Il2CppCodeGenWriteBarrier(&___name_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
