﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_TemplateControlParser3072921516.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.UserControlParser
struct  UserControlParser_t2039383077  : public TemplateControlParser_t3072921516
{
public:
	// System.String System.Web.UI.UserControlParser::masterPage
	String_t* ___masterPage_61;

public:
	inline static int32_t get_offset_of_masterPage_61() { return static_cast<int32_t>(offsetof(UserControlParser_t2039383077, ___masterPage_61)); }
	inline String_t* get_masterPage_61() const { return ___masterPage_61; }
	inline String_t** get_address_of_masterPage_61() { return &___masterPage_61; }
	inline void set_masterPage_61(String_t* value)
	{
		___masterPage_61 = value;
		Il2CppCodeGenWriteBarrier(&___masterPage_61, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
