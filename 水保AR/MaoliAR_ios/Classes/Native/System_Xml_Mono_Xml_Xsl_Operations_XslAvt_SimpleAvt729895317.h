﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Operations_XslAvt_AvtPart3412049665.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslAvt/SimpleAvtPart
struct  SimpleAvtPart_t729895317  : public AvtPart_t3412049665
{
public:
	// System.String Mono.Xml.Xsl.Operations.XslAvt/SimpleAvtPart::val
	String_t* ___val_0;

public:
	inline static int32_t get_offset_of_val_0() { return static_cast<int32_t>(offsetof(SimpleAvtPart_t729895317, ___val_0)); }
	inline String_t* get_val_0() const { return ___val_0; }
	inline String_t** get_address_of_val_0() { return &___val_0; }
	inline void set_val_0(String_t* value)
	{
		___val_0 = value;
		Il2CppCodeGenWriteBarrier(&___val_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
