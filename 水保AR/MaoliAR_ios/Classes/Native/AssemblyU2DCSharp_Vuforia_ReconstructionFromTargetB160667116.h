﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Vuforia_UnityExtensions_Vuforia_ReconstructionFrom2785373562.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ReconstructionFromTargetBehaviour
struct  ReconstructionFromTargetBehaviour_t160667116  : public ReconstructionFromTargetAbstractBehaviour_t2785373562
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
