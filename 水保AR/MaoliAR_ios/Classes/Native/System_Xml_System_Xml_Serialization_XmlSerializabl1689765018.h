﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Serialization_XmlTypeMapping3915382669.h"

// System.Xml.Schema.XmlSchema
struct XmlSchema_t3742557897;
// System.Xml.Schema.XmlSchemaComplexType
struct XmlSchemaComplexType_t3740801802;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2760654312;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializableMapping
struct  XmlSerializableMapping_t1689765018  : public XmlTypeMapping_t3915382669
{
public:
	// System.Xml.Schema.XmlSchema System.Xml.Serialization.XmlSerializableMapping::_schema
	XmlSchema_t3742557897 * ____schema_17;
	// System.Xml.Schema.XmlSchemaComplexType System.Xml.Serialization.XmlSerializableMapping::_schemaType
	XmlSchemaComplexType_t3740801802 * ____schemaType_18;
	// System.Xml.XmlQualifiedName System.Xml.Serialization.XmlSerializableMapping::_schemaTypeName
	XmlQualifiedName_t2760654312 * ____schemaTypeName_19;

public:
	inline static int32_t get_offset_of__schema_17() { return static_cast<int32_t>(offsetof(XmlSerializableMapping_t1689765018, ____schema_17)); }
	inline XmlSchema_t3742557897 * get__schema_17() const { return ____schema_17; }
	inline XmlSchema_t3742557897 ** get_address_of__schema_17() { return &____schema_17; }
	inline void set__schema_17(XmlSchema_t3742557897 * value)
	{
		____schema_17 = value;
		Il2CppCodeGenWriteBarrier(&____schema_17, value);
	}

	inline static int32_t get_offset_of__schemaType_18() { return static_cast<int32_t>(offsetof(XmlSerializableMapping_t1689765018, ____schemaType_18)); }
	inline XmlSchemaComplexType_t3740801802 * get__schemaType_18() const { return ____schemaType_18; }
	inline XmlSchemaComplexType_t3740801802 ** get_address_of__schemaType_18() { return &____schemaType_18; }
	inline void set__schemaType_18(XmlSchemaComplexType_t3740801802 * value)
	{
		____schemaType_18 = value;
		Il2CppCodeGenWriteBarrier(&____schemaType_18, value);
	}

	inline static int32_t get_offset_of__schemaTypeName_19() { return static_cast<int32_t>(offsetof(XmlSerializableMapping_t1689765018, ____schemaTypeName_19)); }
	inline XmlQualifiedName_t2760654312 * get__schemaTypeName_19() const { return ____schemaTypeName_19; }
	inline XmlQualifiedName_t2760654312 ** get_address_of__schemaTypeName_19() { return &____schemaTypeName_19; }
	inline void set__schemaTypeName_19(XmlQualifiedName_t2760654312 * value)
	{
		____schemaTypeName_19 = value;
		Il2CppCodeGenWriteBarrier(&____schemaTypeName_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
