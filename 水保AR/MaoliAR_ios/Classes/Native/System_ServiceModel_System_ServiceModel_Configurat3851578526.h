﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Configurat1070033771.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.MsmqTransportElement
struct  MsmqTransportElement_t3851578526  : public MsmqElementBase_t1070033771
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.MsmqTransportElement::_properties
	ConfigurationPropertyCollection_t2852175726 * ____properties_15;

public:
	inline static int32_t get_offset_of__properties_15() { return static_cast<int32_t>(offsetof(MsmqTransportElement_t3851578526, ____properties_15)); }
	inline ConfigurationPropertyCollection_t2852175726 * get__properties_15() const { return ____properties_15; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of__properties_15() { return &____properties_15; }
	inline void set__properties_15(ConfigurationPropertyCollection_t2852175726 * value)
	{
		____properties_15 = value;
		Il2CppCodeGenWriteBarrier(&____properties_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
