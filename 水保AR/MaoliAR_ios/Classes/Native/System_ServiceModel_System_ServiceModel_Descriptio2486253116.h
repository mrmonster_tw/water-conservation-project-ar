﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.ServiceModel.Description.WstRequestSecurityTokenResponse
struct WstRequestSecurityTokenResponse_t889264267;
// System.Xml.XmlDictionaryReader
struct XmlDictionaryReader_t1044334689;
// System.IdentityModel.Selectors.SecurityTokenSerializer
struct SecurityTokenSerializer_t972969391;
// System.IdentityModel.Selectors.SecurityTokenResolver
struct SecurityTokenResolver_t3905425829;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.WSTrustRequestSecurityTokenResponseReader
struct  WSTrustRequestSecurityTokenResponseReader_t2486253116  : public Il2CppObject
{
public:
	// System.String System.ServiceModel.Description.WSTrustRequestSecurityTokenResponseReader::negotiation_type
	String_t* ___negotiation_type_0;
	// System.ServiceModel.Description.WstRequestSecurityTokenResponse System.ServiceModel.Description.WSTrustRequestSecurityTokenResponseReader::res
	WstRequestSecurityTokenResponse_t889264267 * ___res_1;
	// System.Xml.XmlDictionaryReader System.ServiceModel.Description.WSTrustRequestSecurityTokenResponseReader::reader
	XmlDictionaryReader_t1044334689 * ___reader_2;
	// System.IdentityModel.Selectors.SecurityTokenSerializer System.ServiceModel.Description.WSTrustRequestSecurityTokenResponseReader::serializer
	SecurityTokenSerializer_t972969391 * ___serializer_3;
	// System.IdentityModel.Selectors.SecurityTokenResolver System.ServiceModel.Description.WSTrustRequestSecurityTokenResponseReader::resolver
	SecurityTokenResolver_t3905425829 * ___resolver_4;

public:
	inline static int32_t get_offset_of_negotiation_type_0() { return static_cast<int32_t>(offsetof(WSTrustRequestSecurityTokenResponseReader_t2486253116, ___negotiation_type_0)); }
	inline String_t* get_negotiation_type_0() const { return ___negotiation_type_0; }
	inline String_t** get_address_of_negotiation_type_0() { return &___negotiation_type_0; }
	inline void set_negotiation_type_0(String_t* value)
	{
		___negotiation_type_0 = value;
		Il2CppCodeGenWriteBarrier(&___negotiation_type_0, value);
	}

	inline static int32_t get_offset_of_res_1() { return static_cast<int32_t>(offsetof(WSTrustRequestSecurityTokenResponseReader_t2486253116, ___res_1)); }
	inline WstRequestSecurityTokenResponse_t889264267 * get_res_1() const { return ___res_1; }
	inline WstRequestSecurityTokenResponse_t889264267 ** get_address_of_res_1() { return &___res_1; }
	inline void set_res_1(WstRequestSecurityTokenResponse_t889264267 * value)
	{
		___res_1 = value;
		Il2CppCodeGenWriteBarrier(&___res_1, value);
	}

	inline static int32_t get_offset_of_reader_2() { return static_cast<int32_t>(offsetof(WSTrustRequestSecurityTokenResponseReader_t2486253116, ___reader_2)); }
	inline XmlDictionaryReader_t1044334689 * get_reader_2() const { return ___reader_2; }
	inline XmlDictionaryReader_t1044334689 ** get_address_of_reader_2() { return &___reader_2; }
	inline void set_reader_2(XmlDictionaryReader_t1044334689 * value)
	{
		___reader_2 = value;
		Il2CppCodeGenWriteBarrier(&___reader_2, value);
	}

	inline static int32_t get_offset_of_serializer_3() { return static_cast<int32_t>(offsetof(WSTrustRequestSecurityTokenResponseReader_t2486253116, ___serializer_3)); }
	inline SecurityTokenSerializer_t972969391 * get_serializer_3() const { return ___serializer_3; }
	inline SecurityTokenSerializer_t972969391 ** get_address_of_serializer_3() { return &___serializer_3; }
	inline void set_serializer_3(SecurityTokenSerializer_t972969391 * value)
	{
		___serializer_3 = value;
		Il2CppCodeGenWriteBarrier(&___serializer_3, value);
	}

	inline static int32_t get_offset_of_resolver_4() { return static_cast<int32_t>(offsetof(WSTrustRequestSecurityTokenResponseReader_t2486253116, ___resolver_4)); }
	inline SecurityTokenResolver_t3905425829 * get_resolver_4() const { return ___resolver_4; }
	inline SecurityTokenResolver_t3905425829 ** get_address_of_resolver_4() { return &___resolver_4; }
	inline void set_resolver_4(SecurityTokenResolver_t3905425829 * value)
	{
		___resolver_4 = value;
		Il2CppCodeGenWriteBarrier(&___resolver_4, value);
	}
};

struct WSTrustRequestSecurityTokenResponseReader_t2486253116_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Description.WSTrustRequestSecurityTokenResponseReader::<>f__switch$map19
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map19_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Description.WSTrustRequestSecurityTokenResponseReader::<>f__switch$map1A
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map1A_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Description.WSTrustRequestSecurityTokenResponseReader::<>f__switch$map1B
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map1B_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map19_5() { return static_cast<int32_t>(offsetof(WSTrustRequestSecurityTokenResponseReader_t2486253116_StaticFields, ___U3CU3Ef__switchU24map19_5)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map19_5() const { return ___U3CU3Ef__switchU24map19_5; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map19_5() { return &___U3CU3Ef__switchU24map19_5; }
	inline void set_U3CU3Ef__switchU24map19_5(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map19_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map19_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1A_6() { return static_cast<int32_t>(offsetof(WSTrustRequestSecurityTokenResponseReader_t2486253116_StaticFields, ___U3CU3Ef__switchU24map1A_6)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map1A_6() const { return ___U3CU3Ef__switchU24map1A_6; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map1A_6() { return &___U3CU3Ef__switchU24map1A_6; }
	inline void set_U3CU3Ef__switchU24map1A_6(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map1A_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1A_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1B_7() { return static_cast<int32_t>(offsetof(WSTrustRequestSecurityTokenResponseReader_t2486253116_StaticFields, ___U3CU3Ef__switchU24map1B_7)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map1B_7() const { return ___U3CU3Ef__switchU24map1B_7; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map1B_7() { return &___U3CU3Ef__switchU24map1B_7; }
	inline void set_U3CU3Ef__switchU24map1B_7(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map1B_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1B_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
