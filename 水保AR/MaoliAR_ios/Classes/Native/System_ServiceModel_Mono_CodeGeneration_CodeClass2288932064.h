﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t1073948154;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// Mono.CodeGeneration.CodeMethod
struct CodeMethod_t1546731557;
// Mono.CodeGeneration.CodeBuilder
struct CodeBuilder_t3190018006;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.CodeGeneration.CodeClass
struct  CodeClass_t2288932064  : public Il2CppObject
{
public:
	// System.Reflection.Emit.TypeBuilder Mono.CodeGeneration.CodeClass::typeBuilder
	TypeBuilder_t1073948154 * ___typeBuilder_0;
	// System.Collections.ArrayList Mono.CodeGeneration.CodeClass::customAttributes
	ArrayList_t2718874744 * ___customAttributes_1;
	// System.Collections.ArrayList Mono.CodeGeneration.CodeClass::methods
	ArrayList_t2718874744 * ___methods_2;
	// System.Collections.ArrayList Mono.CodeGeneration.CodeClass::properties
	ArrayList_t2718874744 * ___properties_3;
	// System.Collections.ArrayList Mono.CodeGeneration.CodeClass::fields
	ArrayList_t2718874744 * ___fields_4;
	// System.Collections.Hashtable Mono.CodeGeneration.CodeClass::fieldAttributes
	Hashtable_t1853889766 * ___fieldAttributes_5;
	// System.Type Mono.CodeGeneration.CodeClass::baseType
	Type_t * ___baseType_6;
	// System.Type[] Mono.CodeGeneration.CodeClass::interfaces
	TypeU5BU5D_t3940880105* ___interfaces_7;
	// Mono.CodeGeneration.CodeMethod Mono.CodeGeneration.CodeClass::ctor
	CodeMethod_t1546731557 * ___ctor_8;
	// Mono.CodeGeneration.CodeBuilder Mono.CodeGeneration.CodeClass::instanceInit
	CodeBuilder_t3190018006 * ___instanceInit_9;

public:
	inline static int32_t get_offset_of_typeBuilder_0() { return static_cast<int32_t>(offsetof(CodeClass_t2288932064, ___typeBuilder_0)); }
	inline TypeBuilder_t1073948154 * get_typeBuilder_0() const { return ___typeBuilder_0; }
	inline TypeBuilder_t1073948154 ** get_address_of_typeBuilder_0() { return &___typeBuilder_0; }
	inline void set_typeBuilder_0(TypeBuilder_t1073948154 * value)
	{
		___typeBuilder_0 = value;
		Il2CppCodeGenWriteBarrier(&___typeBuilder_0, value);
	}

	inline static int32_t get_offset_of_customAttributes_1() { return static_cast<int32_t>(offsetof(CodeClass_t2288932064, ___customAttributes_1)); }
	inline ArrayList_t2718874744 * get_customAttributes_1() const { return ___customAttributes_1; }
	inline ArrayList_t2718874744 ** get_address_of_customAttributes_1() { return &___customAttributes_1; }
	inline void set_customAttributes_1(ArrayList_t2718874744 * value)
	{
		___customAttributes_1 = value;
		Il2CppCodeGenWriteBarrier(&___customAttributes_1, value);
	}

	inline static int32_t get_offset_of_methods_2() { return static_cast<int32_t>(offsetof(CodeClass_t2288932064, ___methods_2)); }
	inline ArrayList_t2718874744 * get_methods_2() const { return ___methods_2; }
	inline ArrayList_t2718874744 ** get_address_of_methods_2() { return &___methods_2; }
	inline void set_methods_2(ArrayList_t2718874744 * value)
	{
		___methods_2 = value;
		Il2CppCodeGenWriteBarrier(&___methods_2, value);
	}

	inline static int32_t get_offset_of_properties_3() { return static_cast<int32_t>(offsetof(CodeClass_t2288932064, ___properties_3)); }
	inline ArrayList_t2718874744 * get_properties_3() const { return ___properties_3; }
	inline ArrayList_t2718874744 ** get_address_of_properties_3() { return &___properties_3; }
	inline void set_properties_3(ArrayList_t2718874744 * value)
	{
		___properties_3 = value;
		Il2CppCodeGenWriteBarrier(&___properties_3, value);
	}

	inline static int32_t get_offset_of_fields_4() { return static_cast<int32_t>(offsetof(CodeClass_t2288932064, ___fields_4)); }
	inline ArrayList_t2718874744 * get_fields_4() const { return ___fields_4; }
	inline ArrayList_t2718874744 ** get_address_of_fields_4() { return &___fields_4; }
	inline void set_fields_4(ArrayList_t2718874744 * value)
	{
		___fields_4 = value;
		Il2CppCodeGenWriteBarrier(&___fields_4, value);
	}

	inline static int32_t get_offset_of_fieldAttributes_5() { return static_cast<int32_t>(offsetof(CodeClass_t2288932064, ___fieldAttributes_5)); }
	inline Hashtable_t1853889766 * get_fieldAttributes_5() const { return ___fieldAttributes_5; }
	inline Hashtable_t1853889766 ** get_address_of_fieldAttributes_5() { return &___fieldAttributes_5; }
	inline void set_fieldAttributes_5(Hashtable_t1853889766 * value)
	{
		___fieldAttributes_5 = value;
		Il2CppCodeGenWriteBarrier(&___fieldAttributes_5, value);
	}

	inline static int32_t get_offset_of_baseType_6() { return static_cast<int32_t>(offsetof(CodeClass_t2288932064, ___baseType_6)); }
	inline Type_t * get_baseType_6() const { return ___baseType_6; }
	inline Type_t ** get_address_of_baseType_6() { return &___baseType_6; }
	inline void set_baseType_6(Type_t * value)
	{
		___baseType_6 = value;
		Il2CppCodeGenWriteBarrier(&___baseType_6, value);
	}

	inline static int32_t get_offset_of_interfaces_7() { return static_cast<int32_t>(offsetof(CodeClass_t2288932064, ___interfaces_7)); }
	inline TypeU5BU5D_t3940880105* get_interfaces_7() const { return ___interfaces_7; }
	inline TypeU5BU5D_t3940880105** get_address_of_interfaces_7() { return &___interfaces_7; }
	inline void set_interfaces_7(TypeU5BU5D_t3940880105* value)
	{
		___interfaces_7 = value;
		Il2CppCodeGenWriteBarrier(&___interfaces_7, value);
	}

	inline static int32_t get_offset_of_ctor_8() { return static_cast<int32_t>(offsetof(CodeClass_t2288932064, ___ctor_8)); }
	inline CodeMethod_t1546731557 * get_ctor_8() const { return ___ctor_8; }
	inline CodeMethod_t1546731557 ** get_address_of_ctor_8() { return &___ctor_8; }
	inline void set_ctor_8(CodeMethod_t1546731557 * value)
	{
		___ctor_8 = value;
		Il2CppCodeGenWriteBarrier(&___ctor_8, value);
	}

	inline static int32_t get_offset_of_instanceInit_9() { return static_cast<int32_t>(offsetof(CodeClass_t2288932064, ___instanceInit_9)); }
	inline CodeBuilder_t3190018006 * get_instanceInit_9() const { return ___instanceInit_9; }
	inline CodeBuilder_t3190018006 ** get_address_of_instanceInit_9() { return &___instanceInit_9; }
	inline void set_instanceInit_9(CodeBuilder_t3190018006 * value)
	{
		___instanceInit_9 = value;
		Il2CppCodeGenWriteBarrier(&___instanceInit_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
