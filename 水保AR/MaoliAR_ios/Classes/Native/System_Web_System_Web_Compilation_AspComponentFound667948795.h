﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_Compilation_AspComponentFoun1085528248.h"

// System.Web.Compilation.AspComponentFoundry/AssemblyFoundry
struct AssemblyFoundry_t1662405986;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.AspComponentFoundry/CompoundFoundry
struct  CompoundFoundry_t667948795  : public Foundry_t1085528248
{
public:
	// System.Web.Compilation.AspComponentFoundry/AssemblyFoundry System.Web.Compilation.AspComponentFoundry/CompoundFoundry::assemblyFoundry
	AssemblyFoundry_t1662405986 * ___assemblyFoundry_1;
	// System.Collections.Hashtable System.Web.Compilation.AspComponentFoundry/CompoundFoundry::tagnames
	Hashtable_t1853889766 * ___tagnames_2;
	// System.String System.Web.Compilation.AspComponentFoundry/CompoundFoundry::tagPrefix
	String_t* ___tagPrefix_3;

public:
	inline static int32_t get_offset_of_assemblyFoundry_1() { return static_cast<int32_t>(offsetof(CompoundFoundry_t667948795, ___assemblyFoundry_1)); }
	inline AssemblyFoundry_t1662405986 * get_assemblyFoundry_1() const { return ___assemblyFoundry_1; }
	inline AssemblyFoundry_t1662405986 ** get_address_of_assemblyFoundry_1() { return &___assemblyFoundry_1; }
	inline void set_assemblyFoundry_1(AssemblyFoundry_t1662405986 * value)
	{
		___assemblyFoundry_1 = value;
		Il2CppCodeGenWriteBarrier(&___assemblyFoundry_1, value);
	}

	inline static int32_t get_offset_of_tagnames_2() { return static_cast<int32_t>(offsetof(CompoundFoundry_t667948795, ___tagnames_2)); }
	inline Hashtable_t1853889766 * get_tagnames_2() const { return ___tagnames_2; }
	inline Hashtable_t1853889766 ** get_address_of_tagnames_2() { return &___tagnames_2; }
	inline void set_tagnames_2(Hashtable_t1853889766 * value)
	{
		___tagnames_2 = value;
		Il2CppCodeGenWriteBarrier(&___tagnames_2, value);
	}

	inline static int32_t get_offset_of_tagPrefix_3() { return static_cast<int32_t>(offsetof(CompoundFoundry_t667948795, ___tagPrefix_3)); }
	inline String_t* get_tagPrefix_3() const { return ___tagPrefix_3; }
	inline String_t** get_address_of_tagPrefix_3() { return &___tagPrefix_3; }
	inline void set_tagPrefix_3(String_t* value)
	{
		___tagPrefix_3 = value;
		Il2CppCodeGenWriteBarrier(&___tagPrefix_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
