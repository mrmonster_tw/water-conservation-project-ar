﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractBeh3510878193.h"

// Vuforia.VuforiaBehaviour
struct VuforiaBehaviour_t2151848540;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaBehaviour
struct  VuforiaBehaviour_t2151848540  : public VuforiaAbstractBehaviour_t3510878193
{
public:

public:
};

struct VuforiaBehaviour_t2151848540_StaticFields
{
public:
	// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::mVuforiaBehaviour
	VuforiaBehaviour_t2151848540 * ___mVuforiaBehaviour_19;

public:
	inline static int32_t get_offset_of_mVuforiaBehaviour_19() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t2151848540_StaticFields, ___mVuforiaBehaviour_19)); }
	inline VuforiaBehaviour_t2151848540 * get_mVuforiaBehaviour_19() const { return ___mVuforiaBehaviour_19; }
	inline VuforiaBehaviour_t2151848540 ** get_address_of_mVuforiaBehaviour_19() { return &___mVuforiaBehaviour_19; }
	inline void set_mVuforiaBehaviour_19(VuforiaBehaviour_t2151848540 * value)
	{
		___mVuforiaBehaviour_19 = value;
		Il2CppCodeGenWriteBarrier(&___mVuforiaBehaviour_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
