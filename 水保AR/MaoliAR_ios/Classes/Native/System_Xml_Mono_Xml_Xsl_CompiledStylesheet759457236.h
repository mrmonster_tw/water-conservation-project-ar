﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// Mono.Xml.Xsl.XslStylesheet
struct XslStylesheet_t113441946;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t418790500;
// Mono.Xml.Xsl.MSXslScriptManager
struct MSXslScriptManager_t4018040365;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.CompiledStylesheet
struct  CompiledStylesheet_t759457236  : public Il2CppObject
{
public:
	// Mono.Xml.Xsl.XslStylesheet Mono.Xml.Xsl.CompiledStylesheet::style
	XslStylesheet_t113441946 * ___style_0;
	// System.Collections.Hashtable Mono.Xml.Xsl.CompiledStylesheet::globalVariables
	Hashtable_t1853889766 * ___globalVariables_1;
	// System.Collections.Hashtable Mono.Xml.Xsl.CompiledStylesheet::attrSets
	Hashtable_t1853889766 * ___attrSets_2;
	// System.Xml.XmlNamespaceManager Mono.Xml.Xsl.CompiledStylesheet::nsMgr
	XmlNamespaceManager_t418790500 * ___nsMgr_3;
	// System.Collections.Hashtable Mono.Xml.Xsl.CompiledStylesheet::keys
	Hashtable_t1853889766 * ___keys_4;
	// System.Collections.Hashtable Mono.Xml.Xsl.CompiledStylesheet::outputs
	Hashtable_t1853889766 * ___outputs_5;
	// System.Collections.Hashtable Mono.Xml.Xsl.CompiledStylesheet::decimalFormats
	Hashtable_t1853889766 * ___decimalFormats_6;
	// Mono.Xml.Xsl.MSXslScriptManager Mono.Xml.Xsl.CompiledStylesheet::msScripts
	MSXslScriptManager_t4018040365 * ___msScripts_7;

public:
	inline static int32_t get_offset_of_style_0() { return static_cast<int32_t>(offsetof(CompiledStylesheet_t759457236, ___style_0)); }
	inline XslStylesheet_t113441946 * get_style_0() const { return ___style_0; }
	inline XslStylesheet_t113441946 ** get_address_of_style_0() { return &___style_0; }
	inline void set_style_0(XslStylesheet_t113441946 * value)
	{
		___style_0 = value;
		Il2CppCodeGenWriteBarrier(&___style_0, value);
	}

	inline static int32_t get_offset_of_globalVariables_1() { return static_cast<int32_t>(offsetof(CompiledStylesheet_t759457236, ___globalVariables_1)); }
	inline Hashtable_t1853889766 * get_globalVariables_1() const { return ___globalVariables_1; }
	inline Hashtable_t1853889766 ** get_address_of_globalVariables_1() { return &___globalVariables_1; }
	inline void set_globalVariables_1(Hashtable_t1853889766 * value)
	{
		___globalVariables_1 = value;
		Il2CppCodeGenWriteBarrier(&___globalVariables_1, value);
	}

	inline static int32_t get_offset_of_attrSets_2() { return static_cast<int32_t>(offsetof(CompiledStylesheet_t759457236, ___attrSets_2)); }
	inline Hashtable_t1853889766 * get_attrSets_2() const { return ___attrSets_2; }
	inline Hashtable_t1853889766 ** get_address_of_attrSets_2() { return &___attrSets_2; }
	inline void set_attrSets_2(Hashtable_t1853889766 * value)
	{
		___attrSets_2 = value;
		Il2CppCodeGenWriteBarrier(&___attrSets_2, value);
	}

	inline static int32_t get_offset_of_nsMgr_3() { return static_cast<int32_t>(offsetof(CompiledStylesheet_t759457236, ___nsMgr_3)); }
	inline XmlNamespaceManager_t418790500 * get_nsMgr_3() const { return ___nsMgr_3; }
	inline XmlNamespaceManager_t418790500 ** get_address_of_nsMgr_3() { return &___nsMgr_3; }
	inline void set_nsMgr_3(XmlNamespaceManager_t418790500 * value)
	{
		___nsMgr_3 = value;
		Il2CppCodeGenWriteBarrier(&___nsMgr_3, value);
	}

	inline static int32_t get_offset_of_keys_4() { return static_cast<int32_t>(offsetof(CompiledStylesheet_t759457236, ___keys_4)); }
	inline Hashtable_t1853889766 * get_keys_4() const { return ___keys_4; }
	inline Hashtable_t1853889766 ** get_address_of_keys_4() { return &___keys_4; }
	inline void set_keys_4(Hashtable_t1853889766 * value)
	{
		___keys_4 = value;
		Il2CppCodeGenWriteBarrier(&___keys_4, value);
	}

	inline static int32_t get_offset_of_outputs_5() { return static_cast<int32_t>(offsetof(CompiledStylesheet_t759457236, ___outputs_5)); }
	inline Hashtable_t1853889766 * get_outputs_5() const { return ___outputs_5; }
	inline Hashtable_t1853889766 ** get_address_of_outputs_5() { return &___outputs_5; }
	inline void set_outputs_5(Hashtable_t1853889766 * value)
	{
		___outputs_5 = value;
		Il2CppCodeGenWriteBarrier(&___outputs_5, value);
	}

	inline static int32_t get_offset_of_decimalFormats_6() { return static_cast<int32_t>(offsetof(CompiledStylesheet_t759457236, ___decimalFormats_6)); }
	inline Hashtable_t1853889766 * get_decimalFormats_6() const { return ___decimalFormats_6; }
	inline Hashtable_t1853889766 ** get_address_of_decimalFormats_6() { return &___decimalFormats_6; }
	inline void set_decimalFormats_6(Hashtable_t1853889766 * value)
	{
		___decimalFormats_6 = value;
		Il2CppCodeGenWriteBarrier(&___decimalFormats_6, value);
	}

	inline static int32_t get_offset_of_msScripts_7() { return static_cast<int32_t>(offsetof(CompiledStylesheet_t759457236, ___msScripts_7)); }
	inline MSXslScriptManager_t4018040365 * get_msScripts_7() const { return ___msScripts_7; }
	inline MSXslScriptManager_t4018040365 ** get_address_of_msScripts_7() { return &___msScripts_7; }
	inline void set_msScripts_7(MSXslScriptManager_t4018040365 * value)
	{
		___msScripts_7 = value;
		Il2CppCodeGenWriteBarrier(&___msScripts_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
