﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_System_Net_Security_ProtectionLevel2559578148.h"

// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.ServiceModel.Description.FaultDescriptionCollection
struct FaultDescriptionCollection_t2464923874;
// System.ServiceModel.Description.ContractDescription
struct ContractDescription_t1411178514;
// System.Collections.Generic.KeyedByTypeCollection`1<System.ServiceModel.Description.IOperationBehavior>
struct KeyedByTypeCollection_1_t852781470;
// System.Collections.ObjectModel.Collection`1<System.Type>
struct Collection_1_t1428300678;
// System.ServiceModel.Description.MessageDescriptionCollection
struct MessageDescriptionCollection_t3177516813;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.OperationDescription
struct  OperationDescription_t1389309725  : public Il2CppObject
{
public:
	// System.Reflection.MethodInfo System.ServiceModel.Description.OperationDescription::begin_method
	MethodInfo_t * ___begin_method_0;
	// System.Reflection.MethodInfo System.ServiceModel.Description.OperationDescription::end_method
	MethodInfo_t * ___end_method_1;
	// System.Reflection.MethodInfo System.ServiceModel.Description.OperationDescription::sync_method
	MethodInfo_t * ___sync_method_2;
	// System.ServiceModel.Description.FaultDescriptionCollection System.ServiceModel.Description.OperationDescription::faults
	FaultDescriptionCollection_t2464923874 * ___faults_3;
	// System.ServiceModel.Description.ContractDescription System.ServiceModel.Description.OperationDescription::contract
	ContractDescription_t1411178514 * ___contract_4;
	// System.Collections.Generic.KeyedByTypeCollection`1<System.ServiceModel.Description.IOperationBehavior> System.ServiceModel.Description.OperationDescription::behaviors
	KeyedByTypeCollection_1_t852781470 * ___behaviors_5;
	// System.Boolean System.ServiceModel.Description.OperationDescription::is_initiating
	bool ___is_initiating_6;
	// System.Boolean System.ServiceModel.Description.OperationDescription::is_oneway
	bool ___is_oneway_7;
	// System.Boolean System.ServiceModel.Description.OperationDescription::is_terminating
	bool ___is_terminating_8;
	// System.Collections.ObjectModel.Collection`1<System.Type> System.ServiceModel.Description.OperationDescription::known_types
	Collection_1_t1428300678 * ___known_types_9;
	// System.ServiceModel.Description.MessageDescriptionCollection System.ServiceModel.Description.OperationDescription::messages
	MessageDescriptionCollection_t3177516813 * ___messages_10;
	// System.String System.ServiceModel.Description.OperationDescription::name
	String_t* ___name_11;
	// System.Net.Security.ProtectionLevel System.ServiceModel.Description.OperationDescription::protection_level
	int32_t ___protection_level_12;
	// System.Boolean System.ServiceModel.Description.OperationDescription::has_protection_level
	bool ___has_protection_level_13;

public:
	inline static int32_t get_offset_of_begin_method_0() { return static_cast<int32_t>(offsetof(OperationDescription_t1389309725, ___begin_method_0)); }
	inline MethodInfo_t * get_begin_method_0() const { return ___begin_method_0; }
	inline MethodInfo_t ** get_address_of_begin_method_0() { return &___begin_method_0; }
	inline void set_begin_method_0(MethodInfo_t * value)
	{
		___begin_method_0 = value;
		Il2CppCodeGenWriteBarrier(&___begin_method_0, value);
	}

	inline static int32_t get_offset_of_end_method_1() { return static_cast<int32_t>(offsetof(OperationDescription_t1389309725, ___end_method_1)); }
	inline MethodInfo_t * get_end_method_1() const { return ___end_method_1; }
	inline MethodInfo_t ** get_address_of_end_method_1() { return &___end_method_1; }
	inline void set_end_method_1(MethodInfo_t * value)
	{
		___end_method_1 = value;
		Il2CppCodeGenWriteBarrier(&___end_method_1, value);
	}

	inline static int32_t get_offset_of_sync_method_2() { return static_cast<int32_t>(offsetof(OperationDescription_t1389309725, ___sync_method_2)); }
	inline MethodInfo_t * get_sync_method_2() const { return ___sync_method_2; }
	inline MethodInfo_t ** get_address_of_sync_method_2() { return &___sync_method_2; }
	inline void set_sync_method_2(MethodInfo_t * value)
	{
		___sync_method_2 = value;
		Il2CppCodeGenWriteBarrier(&___sync_method_2, value);
	}

	inline static int32_t get_offset_of_faults_3() { return static_cast<int32_t>(offsetof(OperationDescription_t1389309725, ___faults_3)); }
	inline FaultDescriptionCollection_t2464923874 * get_faults_3() const { return ___faults_3; }
	inline FaultDescriptionCollection_t2464923874 ** get_address_of_faults_3() { return &___faults_3; }
	inline void set_faults_3(FaultDescriptionCollection_t2464923874 * value)
	{
		___faults_3 = value;
		Il2CppCodeGenWriteBarrier(&___faults_3, value);
	}

	inline static int32_t get_offset_of_contract_4() { return static_cast<int32_t>(offsetof(OperationDescription_t1389309725, ___contract_4)); }
	inline ContractDescription_t1411178514 * get_contract_4() const { return ___contract_4; }
	inline ContractDescription_t1411178514 ** get_address_of_contract_4() { return &___contract_4; }
	inline void set_contract_4(ContractDescription_t1411178514 * value)
	{
		___contract_4 = value;
		Il2CppCodeGenWriteBarrier(&___contract_4, value);
	}

	inline static int32_t get_offset_of_behaviors_5() { return static_cast<int32_t>(offsetof(OperationDescription_t1389309725, ___behaviors_5)); }
	inline KeyedByTypeCollection_1_t852781470 * get_behaviors_5() const { return ___behaviors_5; }
	inline KeyedByTypeCollection_1_t852781470 ** get_address_of_behaviors_5() { return &___behaviors_5; }
	inline void set_behaviors_5(KeyedByTypeCollection_1_t852781470 * value)
	{
		___behaviors_5 = value;
		Il2CppCodeGenWriteBarrier(&___behaviors_5, value);
	}

	inline static int32_t get_offset_of_is_initiating_6() { return static_cast<int32_t>(offsetof(OperationDescription_t1389309725, ___is_initiating_6)); }
	inline bool get_is_initiating_6() const { return ___is_initiating_6; }
	inline bool* get_address_of_is_initiating_6() { return &___is_initiating_6; }
	inline void set_is_initiating_6(bool value)
	{
		___is_initiating_6 = value;
	}

	inline static int32_t get_offset_of_is_oneway_7() { return static_cast<int32_t>(offsetof(OperationDescription_t1389309725, ___is_oneway_7)); }
	inline bool get_is_oneway_7() const { return ___is_oneway_7; }
	inline bool* get_address_of_is_oneway_7() { return &___is_oneway_7; }
	inline void set_is_oneway_7(bool value)
	{
		___is_oneway_7 = value;
	}

	inline static int32_t get_offset_of_is_terminating_8() { return static_cast<int32_t>(offsetof(OperationDescription_t1389309725, ___is_terminating_8)); }
	inline bool get_is_terminating_8() const { return ___is_terminating_8; }
	inline bool* get_address_of_is_terminating_8() { return &___is_terminating_8; }
	inline void set_is_terminating_8(bool value)
	{
		___is_terminating_8 = value;
	}

	inline static int32_t get_offset_of_known_types_9() { return static_cast<int32_t>(offsetof(OperationDescription_t1389309725, ___known_types_9)); }
	inline Collection_1_t1428300678 * get_known_types_9() const { return ___known_types_9; }
	inline Collection_1_t1428300678 ** get_address_of_known_types_9() { return &___known_types_9; }
	inline void set_known_types_9(Collection_1_t1428300678 * value)
	{
		___known_types_9 = value;
		Il2CppCodeGenWriteBarrier(&___known_types_9, value);
	}

	inline static int32_t get_offset_of_messages_10() { return static_cast<int32_t>(offsetof(OperationDescription_t1389309725, ___messages_10)); }
	inline MessageDescriptionCollection_t3177516813 * get_messages_10() const { return ___messages_10; }
	inline MessageDescriptionCollection_t3177516813 ** get_address_of_messages_10() { return &___messages_10; }
	inline void set_messages_10(MessageDescriptionCollection_t3177516813 * value)
	{
		___messages_10 = value;
		Il2CppCodeGenWriteBarrier(&___messages_10, value);
	}

	inline static int32_t get_offset_of_name_11() { return static_cast<int32_t>(offsetof(OperationDescription_t1389309725, ___name_11)); }
	inline String_t* get_name_11() const { return ___name_11; }
	inline String_t** get_address_of_name_11() { return &___name_11; }
	inline void set_name_11(String_t* value)
	{
		___name_11 = value;
		Il2CppCodeGenWriteBarrier(&___name_11, value);
	}

	inline static int32_t get_offset_of_protection_level_12() { return static_cast<int32_t>(offsetof(OperationDescription_t1389309725, ___protection_level_12)); }
	inline int32_t get_protection_level_12() const { return ___protection_level_12; }
	inline int32_t* get_address_of_protection_level_12() { return &___protection_level_12; }
	inline void set_protection_level_12(int32_t value)
	{
		___protection_level_12 = value;
	}

	inline static int32_t get_offset_of_has_protection_level_13() { return static_cast<int32_t>(offsetof(OperationDescription_t1389309725, ___has_protection_level_13)); }
	inline bool get_has_protection_level_13() const { return ___has_protection_level_13; }
	inline bool* get_address_of_has_protection_level_13() { return &___has_protection_level_13; }
	inline void set_has_protection_level_13(bool value)
	{
		___has_protection_level_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
