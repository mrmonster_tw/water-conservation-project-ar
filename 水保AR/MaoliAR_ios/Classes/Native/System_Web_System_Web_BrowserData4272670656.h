﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Object
struct Il2CppObject;
// System.Web.BrowserData
struct BrowserData_t4272670656;
// System.String
struct String_t;
// System.Text.RegularExpressions.Regex
struct Regex_t3657309853;
// System.Collections.Specialized.ListDictionary
struct ListDictionary_t1624492310;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.BrowserData
struct  BrowserData_t4272670656  : public Il2CppObject
{
public:
	// System.Object System.Web.BrowserData::this_lock
	Il2CppObject * ___this_lock_1;
	// System.Web.BrowserData System.Web.BrowserData::parent
	BrowserData_t4272670656 * ___parent_2;
	// System.String System.Web.BrowserData::text
	String_t* ___text_3;
	// System.String System.Web.BrowserData::pattern
	String_t* ___pattern_4;
	// System.Text.RegularExpressions.Regex System.Web.BrowserData::regex
	Regex_t3657309853 * ___regex_5;
	// System.Collections.Specialized.ListDictionary System.Web.BrowserData::data
	ListDictionary_t1624492310 * ___data_6;

public:
	inline static int32_t get_offset_of_this_lock_1() { return static_cast<int32_t>(offsetof(BrowserData_t4272670656, ___this_lock_1)); }
	inline Il2CppObject * get_this_lock_1() const { return ___this_lock_1; }
	inline Il2CppObject ** get_address_of_this_lock_1() { return &___this_lock_1; }
	inline void set_this_lock_1(Il2CppObject * value)
	{
		___this_lock_1 = value;
		Il2CppCodeGenWriteBarrier(&___this_lock_1, value);
	}

	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(BrowserData_t4272670656, ___parent_2)); }
	inline BrowserData_t4272670656 * get_parent_2() const { return ___parent_2; }
	inline BrowserData_t4272670656 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(BrowserData_t4272670656 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_text_3() { return static_cast<int32_t>(offsetof(BrowserData_t4272670656, ___text_3)); }
	inline String_t* get_text_3() const { return ___text_3; }
	inline String_t** get_address_of_text_3() { return &___text_3; }
	inline void set_text_3(String_t* value)
	{
		___text_3 = value;
		Il2CppCodeGenWriteBarrier(&___text_3, value);
	}

	inline static int32_t get_offset_of_pattern_4() { return static_cast<int32_t>(offsetof(BrowserData_t4272670656, ___pattern_4)); }
	inline String_t* get_pattern_4() const { return ___pattern_4; }
	inline String_t** get_address_of_pattern_4() { return &___pattern_4; }
	inline void set_pattern_4(String_t* value)
	{
		___pattern_4 = value;
		Il2CppCodeGenWriteBarrier(&___pattern_4, value);
	}

	inline static int32_t get_offset_of_regex_5() { return static_cast<int32_t>(offsetof(BrowserData_t4272670656, ___regex_5)); }
	inline Regex_t3657309853 * get_regex_5() const { return ___regex_5; }
	inline Regex_t3657309853 ** get_address_of_regex_5() { return &___regex_5; }
	inline void set_regex_5(Regex_t3657309853 * value)
	{
		___regex_5 = value;
		Il2CppCodeGenWriteBarrier(&___regex_5, value);
	}

	inline static int32_t get_offset_of_data_6() { return static_cast<int32_t>(offsetof(BrowserData_t4272670656, ___data_6)); }
	inline ListDictionary_t1624492310 * get_data_6() const { return ___data_6; }
	inline ListDictionary_t1624492310 ** get_address_of_data_6() { return &___data_6; }
	inline void set_data_6(ListDictionary_t1624492310 * value)
	{
		___data_6 = value;
		Il2CppCodeGenWriteBarrier(&___data_6, value);
	}
};

struct BrowserData_t4272670656_StaticFields
{
public:
	// System.Char[] System.Web.BrowserData::wildchars
	CharU5BU5D_t3528271667* ___wildchars_0;

public:
	inline static int32_t get_offset_of_wildchars_0() { return static_cast<int32_t>(offsetof(BrowserData_t4272670656_StaticFields, ___wildchars_0)); }
	inline CharU5BU5D_t3528271667* get_wildchars_0() const { return ___wildchars_0; }
	inline CharU5BU5D_t3528271667** get_address_of_wildchars_0() { return &___wildchars_0; }
	inline void set_wildchars_0(CharU5BU5D_t3528271667* value)
	{
		___wildchars_0 = value;
		Il2CppCodeGenWriteBarrier(&___wildchars_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
