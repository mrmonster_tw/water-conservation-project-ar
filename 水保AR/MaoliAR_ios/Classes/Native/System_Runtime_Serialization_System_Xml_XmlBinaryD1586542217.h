﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Runtime_Serialization_System_Xml_XmlBinaryD2956564772.h"

// System.Xml.XmlBinaryDictionaryReader
struct XmlBinaryDictionaryReader_t2034204785;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlBinaryDictionaryReader/AttrNodeInfo
struct  AttrNodeInfo_t1586542217  : public NodeInfo_t2956564772
{
public:
	// System.Xml.XmlBinaryDictionaryReader System.Xml.XmlBinaryDictionaryReader/AttrNodeInfo::owner
	XmlBinaryDictionaryReader_t2034204785 * ___owner_14;
	// System.Int32 System.Xml.XmlBinaryDictionaryReader/AttrNodeInfo::ValueIndex
	int32_t ___ValueIndex_15;

public:
	inline static int32_t get_offset_of_owner_14() { return static_cast<int32_t>(offsetof(AttrNodeInfo_t1586542217, ___owner_14)); }
	inline XmlBinaryDictionaryReader_t2034204785 * get_owner_14() const { return ___owner_14; }
	inline XmlBinaryDictionaryReader_t2034204785 ** get_address_of_owner_14() { return &___owner_14; }
	inline void set_owner_14(XmlBinaryDictionaryReader_t2034204785 * value)
	{
		___owner_14 = value;
		Il2CppCodeGenWriteBarrier(&___owner_14, value);
	}

	inline static int32_t get_offset_of_ValueIndex_15() { return static_cast<int32_t>(offsetof(AttrNodeInfo_t1586542217, ___ValueIndex_15)); }
	inline int32_t get_ValueIndex_15() const { return ___ValueIndex_15; }
	inline int32_t* get_address_of_ValueIndex_15() { return &___ValueIndex_15; }
	inline void set_ValueIndex_15(int32_t value)
	{
		___ValueIndex_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
