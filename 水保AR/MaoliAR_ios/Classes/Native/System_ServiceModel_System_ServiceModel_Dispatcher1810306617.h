﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_System_ServiceModel_Impersonat2275648231.h"

// System.ServiceModel.Dispatcher.DispatchRuntime
struct DispatchRuntime_t796075230;
// System.String
struct String_t;
// System.ServiceModel.Dispatcher.IDispatchMessageFormatter
struct IDispatchMessageFormatter_t824213586;
// System.ServiceModel.Dispatcher.IOperationInvoker
struct IOperationInvoker_t2744064448;
// System.Collections.Generic.SynchronizedCollection`1<System.ServiceModel.Dispatcher.IParameterInspector>
struct SynchronizedCollection_1_t1032157755;
// System.Collections.Generic.SynchronizedCollection`1<System.ServiceModel.Dispatcher.ICallContextInitializer>
struct SynchronizedCollection_1_t2753887244;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.DispatchOperation
struct  DispatchOperation_t1810306617  : public Il2CppObject
{
public:
	// System.ServiceModel.Dispatcher.DispatchRuntime System.ServiceModel.Dispatcher.DispatchOperation::parent
	DispatchRuntime_t796075230 * ___parent_0;
	// System.String System.ServiceModel.Dispatcher.DispatchOperation::name
	String_t* ___name_1;
	// System.String System.ServiceModel.Dispatcher.DispatchOperation::action
	String_t* ___action_2;
	// System.String System.ServiceModel.Dispatcher.DispatchOperation::reply_action
	String_t* ___reply_action_3;
	// System.Boolean System.ServiceModel.Dispatcher.DispatchOperation::serialize_reply
	bool ___serialize_reply_4;
	// System.Boolean System.ServiceModel.Dispatcher.DispatchOperation::deserialize_request
	bool ___deserialize_request_5;
	// System.Boolean System.ServiceModel.Dispatcher.DispatchOperation::is_oneway
	bool ___is_oneway_6;
	// System.Boolean System.ServiceModel.Dispatcher.DispatchOperation::release_after_call
	bool ___release_after_call_7;
	// System.Boolean System.ServiceModel.Dispatcher.DispatchOperation::release_before_call
	bool ___release_before_call_8;
	// System.Boolean System.ServiceModel.Dispatcher.DispatchOperation::tx_auto_complete
	bool ___tx_auto_complete_9;
	// System.Boolean System.ServiceModel.Dispatcher.DispatchOperation::tx_required
	bool ___tx_required_10;
	// System.Boolean System.ServiceModel.Dispatcher.DispatchOperation::auto_dispose_params
	bool ___auto_dispose_params_11;
	// System.ServiceModel.ImpersonationOption System.ServiceModel.Dispatcher.DispatchOperation::impersonation
	int32_t ___impersonation_12;
	// System.ServiceModel.Dispatcher.IDispatchMessageFormatter System.ServiceModel.Dispatcher.DispatchOperation::formatter
	Il2CppObject * ___formatter_13;
	// System.ServiceModel.Dispatcher.IDispatchMessageFormatter System.ServiceModel.Dispatcher.DispatchOperation::actual_formatter
	Il2CppObject * ___actual_formatter_14;
	// System.ServiceModel.Dispatcher.IOperationInvoker System.ServiceModel.Dispatcher.DispatchOperation::invoker
	Il2CppObject * ___invoker_15;
	// System.Collections.Generic.SynchronizedCollection`1<System.ServiceModel.Dispatcher.IParameterInspector> System.ServiceModel.Dispatcher.DispatchOperation::inspectors
	SynchronizedCollection_1_t1032157755 * ___inspectors_16;
	// System.Collections.Generic.SynchronizedCollection`1<System.ServiceModel.Dispatcher.ICallContextInitializer> System.ServiceModel.Dispatcher.DispatchOperation::ctx_initializers
	SynchronizedCollection_1_t2753887244 * ___ctx_initializers_17;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(DispatchOperation_t1810306617, ___parent_0)); }
	inline DispatchRuntime_t796075230 * get_parent_0() const { return ___parent_0; }
	inline DispatchRuntime_t796075230 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(DispatchRuntime_t796075230 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___parent_0, value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(DispatchOperation_t1810306617, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_action_2() { return static_cast<int32_t>(offsetof(DispatchOperation_t1810306617, ___action_2)); }
	inline String_t* get_action_2() const { return ___action_2; }
	inline String_t** get_address_of_action_2() { return &___action_2; }
	inline void set_action_2(String_t* value)
	{
		___action_2 = value;
		Il2CppCodeGenWriteBarrier(&___action_2, value);
	}

	inline static int32_t get_offset_of_reply_action_3() { return static_cast<int32_t>(offsetof(DispatchOperation_t1810306617, ___reply_action_3)); }
	inline String_t* get_reply_action_3() const { return ___reply_action_3; }
	inline String_t** get_address_of_reply_action_3() { return &___reply_action_3; }
	inline void set_reply_action_3(String_t* value)
	{
		___reply_action_3 = value;
		Il2CppCodeGenWriteBarrier(&___reply_action_3, value);
	}

	inline static int32_t get_offset_of_serialize_reply_4() { return static_cast<int32_t>(offsetof(DispatchOperation_t1810306617, ___serialize_reply_4)); }
	inline bool get_serialize_reply_4() const { return ___serialize_reply_4; }
	inline bool* get_address_of_serialize_reply_4() { return &___serialize_reply_4; }
	inline void set_serialize_reply_4(bool value)
	{
		___serialize_reply_4 = value;
	}

	inline static int32_t get_offset_of_deserialize_request_5() { return static_cast<int32_t>(offsetof(DispatchOperation_t1810306617, ___deserialize_request_5)); }
	inline bool get_deserialize_request_5() const { return ___deserialize_request_5; }
	inline bool* get_address_of_deserialize_request_5() { return &___deserialize_request_5; }
	inline void set_deserialize_request_5(bool value)
	{
		___deserialize_request_5 = value;
	}

	inline static int32_t get_offset_of_is_oneway_6() { return static_cast<int32_t>(offsetof(DispatchOperation_t1810306617, ___is_oneway_6)); }
	inline bool get_is_oneway_6() const { return ___is_oneway_6; }
	inline bool* get_address_of_is_oneway_6() { return &___is_oneway_6; }
	inline void set_is_oneway_6(bool value)
	{
		___is_oneway_6 = value;
	}

	inline static int32_t get_offset_of_release_after_call_7() { return static_cast<int32_t>(offsetof(DispatchOperation_t1810306617, ___release_after_call_7)); }
	inline bool get_release_after_call_7() const { return ___release_after_call_7; }
	inline bool* get_address_of_release_after_call_7() { return &___release_after_call_7; }
	inline void set_release_after_call_7(bool value)
	{
		___release_after_call_7 = value;
	}

	inline static int32_t get_offset_of_release_before_call_8() { return static_cast<int32_t>(offsetof(DispatchOperation_t1810306617, ___release_before_call_8)); }
	inline bool get_release_before_call_8() const { return ___release_before_call_8; }
	inline bool* get_address_of_release_before_call_8() { return &___release_before_call_8; }
	inline void set_release_before_call_8(bool value)
	{
		___release_before_call_8 = value;
	}

	inline static int32_t get_offset_of_tx_auto_complete_9() { return static_cast<int32_t>(offsetof(DispatchOperation_t1810306617, ___tx_auto_complete_9)); }
	inline bool get_tx_auto_complete_9() const { return ___tx_auto_complete_9; }
	inline bool* get_address_of_tx_auto_complete_9() { return &___tx_auto_complete_9; }
	inline void set_tx_auto_complete_9(bool value)
	{
		___tx_auto_complete_9 = value;
	}

	inline static int32_t get_offset_of_tx_required_10() { return static_cast<int32_t>(offsetof(DispatchOperation_t1810306617, ___tx_required_10)); }
	inline bool get_tx_required_10() const { return ___tx_required_10; }
	inline bool* get_address_of_tx_required_10() { return &___tx_required_10; }
	inline void set_tx_required_10(bool value)
	{
		___tx_required_10 = value;
	}

	inline static int32_t get_offset_of_auto_dispose_params_11() { return static_cast<int32_t>(offsetof(DispatchOperation_t1810306617, ___auto_dispose_params_11)); }
	inline bool get_auto_dispose_params_11() const { return ___auto_dispose_params_11; }
	inline bool* get_address_of_auto_dispose_params_11() { return &___auto_dispose_params_11; }
	inline void set_auto_dispose_params_11(bool value)
	{
		___auto_dispose_params_11 = value;
	}

	inline static int32_t get_offset_of_impersonation_12() { return static_cast<int32_t>(offsetof(DispatchOperation_t1810306617, ___impersonation_12)); }
	inline int32_t get_impersonation_12() const { return ___impersonation_12; }
	inline int32_t* get_address_of_impersonation_12() { return &___impersonation_12; }
	inline void set_impersonation_12(int32_t value)
	{
		___impersonation_12 = value;
	}

	inline static int32_t get_offset_of_formatter_13() { return static_cast<int32_t>(offsetof(DispatchOperation_t1810306617, ___formatter_13)); }
	inline Il2CppObject * get_formatter_13() const { return ___formatter_13; }
	inline Il2CppObject ** get_address_of_formatter_13() { return &___formatter_13; }
	inline void set_formatter_13(Il2CppObject * value)
	{
		___formatter_13 = value;
		Il2CppCodeGenWriteBarrier(&___formatter_13, value);
	}

	inline static int32_t get_offset_of_actual_formatter_14() { return static_cast<int32_t>(offsetof(DispatchOperation_t1810306617, ___actual_formatter_14)); }
	inline Il2CppObject * get_actual_formatter_14() const { return ___actual_formatter_14; }
	inline Il2CppObject ** get_address_of_actual_formatter_14() { return &___actual_formatter_14; }
	inline void set_actual_formatter_14(Il2CppObject * value)
	{
		___actual_formatter_14 = value;
		Il2CppCodeGenWriteBarrier(&___actual_formatter_14, value);
	}

	inline static int32_t get_offset_of_invoker_15() { return static_cast<int32_t>(offsetof(DispatchOperation_t1810306617, ___invoker_15)); }
	inline Il2CppObject * get_invoker_15() const { return ___invoker_15; }
	inline Il2CppObject ** get_address_of_invoker_15() { return &___invoker_15; }
	inline void set_invoker_15(Il2CppObject * value)
	{
		___invoker_15 = value;
		Il2CppCodeGenWriteBarrier(&___invoker_15, value);
	}

	inline static int32_t get_offset_of_inspectors_16() { return static_cast<int32_t>(offsetof(DispatchOperation_t1810306617, ___inspectors_16)); }
	inline SynchronizedCollection_1_t1032157755 * get_inspectors_16() const { return ___inspectors_16; }
	inline SynchronizedCollection_1_t1032157755 ** get_address_of_inspectors_16() { return &___inspectors_16; }
	inline void set_inspectors_16(SynchronizedCollection_1_t1032157755 * value)
	{
		___inspectors_16 = value;
		Il2CppCodeGenWriteBarrier(&___inspectors_16, value);
	}

	inline static int32_t get_offset_of_ctx_initializers_17() { return static_cast<int32_t>(offsetof(DispatchOperation_t1810306617, ___ctx_initializers_17)); }
	inline SynchronizedCollection_1_t2753887244 * get_ctx_initializers_17() const { return ___ctx_initializers_17; }
	inline SynchronizedCollection_1_t2753887244 ** get_address_of_ctx_initializers_17() { return &___ctx_initializers_17; }
	inline void set_ctx_initializers_17(SynchronizedCollection_1_t2753887244 * value)
	{
		___ctx_initializers_17 = value;
		Il2CppCodeGenWriteBarrier(&___ctx_initializers_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
