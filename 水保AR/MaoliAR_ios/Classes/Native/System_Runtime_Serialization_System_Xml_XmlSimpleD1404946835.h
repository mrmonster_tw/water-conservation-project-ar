﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Runtime_Serialization_System_Xml_XmlDiction1044334689.h"

// System.Xml.XmlDictionary
struct XmlDictionary_t238028028;
// System.Xml.XmlReader
struct XmlReader_t3121518892;
// System.Xml.XmlDictionaryReader
struct XmlDictionaryReader_t1044334689;
// System.Xml.IXmlLineInfo
struct IXmlLineInfo_t2353988607;
// System.Xml.OnXmlDictionaryReaderClose
struct OnXmlDictionaryReaderClose_t3234185550;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlSimpleDictionaryReader
struct  XmlSimpleDictionaryReader_t1404946835  : public XmlDictionaryReader_t1044334689
{
public:
	// System.Xml.XmlDictionary System.Xml.XmlSimpleDictionaryReader::dict
	XmlDictionary_t238028028 * ___dict_6;
	// System.Xml.XmlReader System.Xml.XmlSimpleDictionaryReader::reader
	XmlReader_t3121518892 * ___reader_7;
	// System.Xml.XmlDictionaryReader System.Xml.XmlSimpleDictionaryReader::as_dict_reader
	XmlDictionaryReader_t1044334689 * ___as_dict_reader_8;
	// System.Xml.IXmlLineInfo System.Xml.XmlSimpleDictionaryReader::as_line_info
	Il2CppObject * ___as_line_info_9;
	// System.Xml.OnXmlDictionaryReaderClose System.Xml.XmlSimpleDictionaryReader::onClose
	OnXmlDictionaryReaderClose_t3234185550 * ___onClose_10;

public:
	inline static int32_t get_offset_of_dict_6() { return static_cast<int32_t>(offsetof(XmlSimpleDictionaryReader_t1404946835, ___dict_6)); }
	inline XmlDictionary_t238028028 * get_dict_6() const { return ___dict_6; }
	inline XmlDictionary_t238028028 ** get_address_of_dict_6() { return &___dict_6; }
	inline void set_dict_6(XmlDictionary_t238028028 * value)
	{
		___dict_6 = value;
		Il2CppCodeGenWriteBarrier(&___dict_6, value);
	}

	inline static int32_t get_offset_of_reader_7() { return static_cast<int32_t>(offsetof(XmlSimpleDictionaryReader_t1404946835, ___reader_7)); }
	inline XmlReader_t3121518892 * get_reader_7() const { return ___reader_7; }
	inline XmlReader_t3121518892 ** get_address_of_reader_7() { return &___reader_7; }
	inline void set_reader_7(XmlReader_t3121518892 * value)
	{
		___reader_7 = value;
		Il2CppCodeGenWriteBarrier(&___reader_7, value);
	}

	inline static int32_t get_offset_of_as_dict_reader_8() { return static_cast<int32_t>(offsetof(XmlSimpleDictionaryReader_t1404946835, ___as_dict_reader_8)); }
	inline XmlDictionaryReader_t1044334689 * get_as_dict_reader_8() const { return ___as_dict_reader_8; }
	inline XmlDictionaryReader_t1044334689 ** get_address_of_as_dict_reader_8() { return &___as_dict_reader_8; }
	inline void set_as_dict_reader_8(XmlDictionaryReader_t1044334689 * value)
	{
		___as_dict_reader_8 = value;
		Il2CppCodeGenWriteBarrier(&___as_dict_reader_8, value);
	}

	inline static int32_t get_offset_of_as_line_info_9() { return static_cast<int32_t>(offsetof(XmlSimpleDictionaryReader_t1404946835, ___as_line_info_9)); }
	inline Il2CppObject * get_as_line_info_9() const { return ___as_line_info_9; }
	inline Il2CppObject ** get_address_of_as_line_info_9() { return &___as_line_info_9; }
	inline void set_as_line_info_9(Il2CppObject * value)
	{
		___as_line_info_9 = value;
		Il2CppCodeGenWriteBarrier(&___as_line_info_9, value);
	}

	inline static int32_t get_offset_of_onClose_10() { return static_cast<int32_t>(offsetof(XmlSimpleDictionaryReader_t1404946835, ___onClose_10)); }
	inline OnXmlDictionaryReaderClose_t3234185550 * get_onClose_10() const { return ___onClose_10; }
	inline OnXmlDictionaryReaderClose_t3234185550 ** get_address_of_onClose_10() { return &___onClose_10; }
	inline void set_onClose_10(OnXmlDictionaryReaderClose_t3234185550 * value)
	{
		___onClose_10 = value;
		Il2CppCodeGenWriteBarrier(&___onClose_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
