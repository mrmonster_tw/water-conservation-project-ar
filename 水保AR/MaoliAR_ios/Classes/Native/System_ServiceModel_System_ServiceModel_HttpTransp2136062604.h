﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_System_ServiceModel_HttpClientC856074172.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.HttpTransportSecurity
struct  HttpTransportSecurity_t2136062604  : public Il2CppObject
{
public:
	// System.ServiceModel.HttpClientCredentialType System.ServiceModel.HttpTransportSecurity::client
	int32_t ___client_0;

public:
	inline static int32_t get_offset_of_client_0() { return static_cast<int32_t>(offsetof(HttpTransportSecurity_t2136062604, ___client_0)); }
	inline int32_t get_client_0() const { return ___client_0; }
	inline int32_t* get_address_of_client_0() { return &___client_0; }
	inline void set_client_0(int32_t value)
	{
		___client_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
