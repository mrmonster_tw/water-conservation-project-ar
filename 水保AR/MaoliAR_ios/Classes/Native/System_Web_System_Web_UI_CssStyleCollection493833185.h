﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.UI.StateBag
struct StateBag_t282928164;
// System.Collections.Specialized.ListDictionary
struct ListDictionary_t1624492310;
// System.Text.StringBuilder
struct StringBuilder_t1712802186;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.CssStyleCollection
struct  CssStyleCollection_t493833185  : public Il2CppObject
{
public:
	// System.Web.UI.StateBag System.Web.UI.CssStyleCollection::bag
	StateBag_t282928164 * ___bag_0;
	// System.Collections.Specialized.ListDictionary System.Web.UI.CssStyleCollection::style
	ListDictionary_t1624492310 * ___style_1;
	// System.Text.StringBuilder System.Web.UI.CssStyleCollection::_value
	StringBuilder_t1712802186 * ____value_2;

public:
	inline static int32_t get_offset_of_bag_0() { return static_cast<int32_t>(offsetof(CssStyleCollection_t493833185, ___bag_0)); }
	inline StateBag_t282928164 * get_bag_0() const { return ___bag_0; }
	inline StateBag_t282928164 ** get_address_of_bag_0() { return &___bag_0; }
	inline void set_bag_0(StateBag_t282928164 * value)
	{
		___bag_0 = value;
		Il2CppCodeGenWriteBarrier(&___bag_0, value);
	}

	inline static int32_t get_offset_of_style_1() { return static_cast<int32_t>(offsetof(CssStyleCollection_t493833185, ___style_1)); }
	inline ListDictionary_t1624492310 * get_style_1() const { return ___style_1; }
	inline ListDictionary_t1624492310 ** get_address_of_style_1() { return &___style_1; }
	inline void set_style_1(ListDictionary_t1624492310 * value)
	{
		___style_1 = value;
		Il2CppCodeGenWriteBarrier(&___style_1, value);
	}

	inline static int32_t get_offset_of__value_2() { return static_cast<int32_t>(offsetof(CssStyleCollection_t493833185, ____value_2)); }
	inline StringBuilder_t1712802186 * get__value_2() const { return ____value_2; }
	inline StringBuilder_t1712802186 ** get_address_of__value_2() { return &____value_2; }
	inline void set__value_2(StringBuilder_t1712802186 * value)
	{
		____value_2 = value;
		Il2CppCodeGenWriteBarrier(&____value_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
