﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Operations_XslCompiledElemen50593777.h"

// System.Xml.XPath.XPathExpression
struct XPathExpression_t1723793351;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2760654312;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// Mono.Xml.Xsl.XslSortEvaluator
struct XslSortEvaluator_t824821660;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslApplyTemplates
struct  XslApplyTemplates_t2247116683  : public XslCompiledElement_t50593777
{
public:
	// System.Xml.XPath.XPathExpression Mono.Xml.Xsl.Operations.XslApplyTemplates::select
	XPathExpression_t1723793351 * ___select_3;
	// System.Xml.XmlQualifiedName Mono.Xml.Xsl.Operations.XslApplyTemplates::mode
	XmlQualifiedName_t2760654312 * ___mode_4;
	// System.Collections.ArrayList Mono.Xml.Xsl.Operations.XslApplyTemplates::withParams
	ArrayList_t2718874744 * ___withParams_5;
	// Mono.Xml.Xsl.XslSortEvaluator Mono.Xml.Xsl.Operations.XslApplyTemplates::sortEvaluator
	XslSortEvaluator_t824821660 * ___sortEvaluator_6;

public:
	inline static int32_t get_offset_of_select_3() { return static_cast<int32_t>(offsetof(XslApplyTemplates_t2247116683, ___select_3)); }
	inline XPathExpression_t1723793351 * get_select_3() const { return ___select_3; }
	inline XPathExpression_t1723793351 ** get_address_of_select_3() { return &___select_3; }
	inline void set_select_3(XPathExpression_t1723793351 * value)
	{
		___select_3 = value;
		Il2CppCodeGenWriteBarrier(&___select_3, value);
	}

	inline static int32_t get_offset_of_mode_4() { return static_cast<int32_t>(offsetof(XslApplyTemplates_t2247116683, ___mode_4)); }
	inline XmlQualifiedName_t2760654312 * get_mode_4() const { return ___mode_4; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_mode_4() { return &___mode_4; }
	inline void set_mode_4(XmlQualifiedName_t2760654312 * value)
	{
		___mode_4 = value;
		Il2CppCodeGenWriteBarrier(&___mode_4, value);
	}

	inline static int32_t get_offset_of_withParams_5() { return static_cast<int32_t>(offsetof(XslApplyTemplates_t2247116683, ___withParams_5)); }
	inline ArrayList_t2718874744 * get_withParams_5() const { return ___withParams_5; }
	inline ArrayList_t2718874744 ** get_address_of_withParams_5() { return &___withParams_5; }
	inline void set_withParams_5(ArrayList_t2718874744 * value)
	{
		___withParams_5 = value;
		Il2CppCodeGenWriteBarrier(&___withParams_5, value);
	}

	inline static int32_t get_offset_of_sortEvaluator_6() { return static_cast<int32_t>(offsetof(XslApplyTemplates_t2247116683, ___sortEvaluator_6)); }
	inline XslSortEvaluator_t824821660 * get_sortEvaluator_6() const { return ___sortEvaluator_6; }
	inline XslSortEvaluator_t824821660 ** get_address_of_sortEvaluator_6() { return &___sortEvaluator_6; }
	inline void set_sortEvaluator_6(XslSortEvaluator_t824821660 * value)
	{
		___sortEvaluator_6 = value;
		Il2CppCodeGenWriteBarrier(&___sortEvaluator_6, value);
	}
};

struct XslApplyTemplates_t2247116683_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.Operations.XslApplyTemplates::<>f__switch$map7
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map7_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map7_7() { return static_cast<int32_t>(offsetof(XslApplyTemplates_t2247116683_StaticFields, ___U3CU3Ef__switchU24map7_7)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map7_7() const { return ___U3CU3Ef__switchU24map7_7; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map7_7() { return &___U3CU3Ef__switchU24map7_7; }
	inline void set_U3CU3Ef__switchU24map7_7(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map7_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
