﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_MarshalByRefObject2760389100.h"
#include "mscorlib_System_IntPtr840150181.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Drawing.FontFamily
struct  FontFamily_t13778311  : public MarshalByRefObject_t2760389100
{
public:
	// System.String System.Drawing.FontFamily::name
	String_t* ___name_1;
	// System.IntPtr System.Drawing.FontFamily::nativeFontFamily
	IntPtr_t ___nativeFontFamily_2;

public:
	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(FontFamily_t13778311, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_nativeFontFamily_2() { return static_cast<int32_t>(offsetof(FontFamily_t13778311, ___nativeFontFamily_2)); }
	inline IntPtr_t get_nativeFontFamily_2() const { return ___nativeFontFamily_2; }
	inline IntPtr_t* get_address_of_nativeFontFamily_2() { return &___nativeFontFamily_2; }
	inline void set_nativeFontFamily_2(IntPtr_t value)
	{
		___nativeFontFamily_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
