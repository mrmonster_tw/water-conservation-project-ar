﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Generic.List`1<System.IdentityModel.Tokens.SecurityKeyIdentifierClause>
struct List_1_t3415504555;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Tokens.SecurityKeyIdentifier
struct  SecurityKeyIdentifier_t806165896  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.IdentityModel.Tokens.SecurityKeyIdentifierClause> System.IdentityModel.Tokens.SecurityKeyIdentifier::list
	List_1_t3415504555 * ___list_0;
	// System.Boolean System.IdentityModel.Tokens.SecurityKeyIdentifier::is_readonly
	bool ___is_readonly_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(SecurityKeyIdentifier_t806165896, ___list_0)); }
	inline List_1_t3415504555 * get_list_0() const { return ___list_0; }
	inline List_1_t3415504555 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t3415504555 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier(&___list_0, value);
	}

	inline static int32_t get_offset_of_is_readonly_1() { return static_cast<int32_t>(offsetof(SecurityKeyIdentifier_t806165896, ___is_readonly_1)); }
	inline bool get_is_readonly_1() const { return ___is_readonly_1; }
	inline bool* get_address_of_is_readonly_1() { return &___is_readonly_1; }
	inline void set_is_readonly_1(bool value)
	{
		___is_readonly_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
