﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeExpression2166265795.h"

// System.CodeDom.CodeExpression
struct CodeExpression_t2166265795;
// System.CodeDom.CodeExpressionCollection
struct CodeExpressionCollection_t2370433003;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeIndexerExpression
struct  CodeIndexerExpression_t356393666  : public CodeExpression_t2166265795
{
public:
	// System.CodeDom.CodeExpression System.CodeDom.CodeIndexerExpression::targetObject
	CodeExpression_t2166265795 * ___targetObject_1;
	// System.CodeDom.CodeExpressionCollection System.CodeDom.CodeIndexerExpression::indices
	CodeExpressionCollection_t2370433003 * ___indices_2;

public:
	inline static int32_t get_offset_of_targetObject_1() { return static_cast<int32_t>(offsetof(CodeIndexerExpression_t356393666, ___targetObject_1)); }
	inline CodeExpression_t2166265795 * get_targetObject_1() const { return ___targetObject_1; }
	inline CodeExpression_t2166265795 ** get_address_of_targetObject_1() { return &___targetObject_1; }
	inline void set_targetObject_1(CodeExpression_t2166265795 * value)
	{
		___targetObject_1 = value;
		Il2CppCodeGenWriteBarrier(&___targetObject_1, value);
	}

	inline static int32_t get_offset_of_indices_2() { return static_cast<int32_t>(offsetof(CodeIndexerExpression_t356393666, ___indices_2)); }
	inline CodeExpressionCollection_t2370433003 * get_indices_2() const { return ___indices_2; }
	inline CodeExpressionCollection_t2370433003 ** get_address_of_indices_2() { return &___indices_2; }
	inline void set_indices_2(CodeExpressionCollection_t2370433003 * value)
	{
		___indices_2 = value;
		Il2CppCodeGenWriteBarrier(&___indices_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
