﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.WebResourceAttribute
struct  WebResourceAttribute_t4132945045  : public Attribute_t861562559
{
public:
	// System.Boolean System.Web.UI.WebResourceAttribute::performSubstitution
	bool ___performSubstitution_0;
	// System.String System.Web.UI.WebResourceAttribute::webResource
	String_t* ___webResource_1;
	// System.String System.Web.UI.WebResourceAttribute::contentType
	String_t* ___contentType_2;

public:
	inline static int32_t get_offset_of_performSubstitution_0() { return static_cast<int32_t>(offsetof(WebResourceAttribute_t4132945045, ___performSubstitution_0)); }
	inline bool get_performSubstitution_0() const { return ___performSubstitution_0; }
	inline bool* get_address_of_performSubstitution_0() { return &___performSubstitution_0; }
	inline void set_performSubstitution_0(bool value)
	{
		___performSubstitution_0 = value;
	}

	inline static int32_t get_offset_of_webResource_1() { return static_cast<int32_t>(offsetof(WebResourceAttribute_t4132945045, ___webResource_1)); }
	inline String_t* get_webResource_1() const { return ___webResource_1; }
	inline String_t** get_address_of_webResource_1() { return &___webResource_1; }
	inline void set_webResource_1(String_t* value)
	{
		___webResource_1 = value;
		Il2CppCodeGenWriteBarrier(&___webResource_1, value);
	}

	inline static int32_t get_offset_of_contentType_2() { return static_cast<int32_t>(offsetof(WebResourceAttribute_t4132945045, ___contentType_2)); }
	inline String_t* get_contentType_2() const { return ___contentType_2; }
	inline String_t** get_address_of_contentType_2() { return &___contentType_2; }
	inline void set_contentType_2(String_t* value)
	{
		___contentType_2 = value;
		Il2CppCodeGenWriteBarrier(&___contentType_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
