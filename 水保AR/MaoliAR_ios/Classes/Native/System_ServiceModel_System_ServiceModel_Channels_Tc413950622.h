﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_D3887558294.h"

// System.ServiceModel.Channels.TcpDuplexSessionChannel
struct TcpDuplexSessionChannel_t645469680;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.TcpDuplexSessionChannel/TcpDuplexSession
struct  TcpDuplexSession_t413950622  : public DuplexSessionBase_t3887558294
{
public:
	// System.ServiceModel.Channels.TcpDuplexSessionChannel System.ServiceModel.Channels.TcpDuplexSessionChannel/TcpDuplexSession::owner
	TcpDuplexSessionChannel_t645469680 * ___owner_2;

public:
	inline static int32_t get_offset_of_owner_2() { return static_cast<int32_t>(offsetof(TcpDuplexSession_t413950622, ___owner_2)); }
	inline TcpDuplexSessionChannel_t645469680 * get_owner_2() const { return ___owner_2; }
	inline TcpDuplexSessionChannel_t645469680 ** get_address_of_owner_2() { return &___owner_2; }
	inline void set_owner_2(TcpDuplexSessionChannel_t645469680 * value)
	{
		___owner_2 = value;
		Il2CppCodeGenWriteBarrier(&___owner_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
