﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Threading.ReaderWriterLock
struct ReaderWriterLock_t367846299;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.LockQueue
struct  LockQueue_t2679652224  : public Il2CppObject
{
public:
	// System.Threading.ReaderWriterLock System.Threading.LockQueue::rwlock
	ReaderWriterLock_t367846299 * ___rwlock_0;
	// System.Int32 System.Threading.LockQueue::lockCount
	int32_t ___lockCount_1;

public:
	inline static int32_t get_offset_of_rwlock_0() { return static_cast<int32_t>(offsetof(LockQueue_t2679652224, ___rwlock_0)); }
	inline ReaderWriterLock_t367846299 * get_rwlock_0() const { return ___rwlock_0; }
	inline ReaderWriterLock_t367846299 ** get_address_of_rwlock_0() { return &___rwlock_0; }
	inline void set_rwlock_0(ReaderWriterLock_t367846299 * value)
	{
		___rwlock_0 = value;
		Il2CppCodeGenWriteBarrier(&___rwlock_0, value);
	}

	inline static int32_t get_offset_of_lockCount_1() { return static_cast<int32_t>(offsetof(LockQueue_t2679652224, ___lockCount_1)); }
	inline int32_t get_lockCount_1() const { return ___lockCount_1; }
	inline int32_t* get_address_of_lockCount_1() { return &___lockCount_1; }
	inline void set_lockCount_1(int32_t value)
	{
		___lockCount_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
