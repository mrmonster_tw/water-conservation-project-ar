﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat962350765.h"

// UnityEngine.Material
struct Material_t340375123;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.MotionBlur/ReconstructionFilter
struct  ReconstructionFilter_t4011033762  : public Il2CppObject
{
public:
	// UnityEngine.Material UnityStandardAssets.CinematicEffects.MotionBlur/ReconstructionFilter::_material
	Material_t340375123 * ____material_1;
	// System.Boolean UnityStandardAssets.CinematicEffects.MotionBlur/ReconstructionFilter::_unroll
	bool ____unroll_2;
	// UnityEngine.RenderTextureFormat UnityStandardAssets.CinematicEffects.MotionBlur/ReconstructionFilter::_vectorRTFormat
	int32_t ____vectorRTFormat_3;
	// UnityEngine.RenderTextureFormat UnityStandardAssets.CinematicEffects.MotionBlur/ReconstructionFilter::_packedRTFormat
	int32_t ____packedRTFormat_4;

public:
	inline static int32_t get_offset_of__material_1() { return static_cast<int32_t>(offsetof(ReconstructionFilter_t4011033762, ____material_1)); }
	inline Material_t340375123 * get__material_1() const { return ____material_1; }
	inline Material_t340375123 ** get_address_of__material_1() { return &____material_1; }
	inline void set__material_1(Material_t340375123 * value)
	{
		____material_1 = value;
		Il2CppCodeGenWriteBarrier(&____material_1, value);
	}

	inline static int32_t get_offset_of__unroll_2() { return static_cast<int32_t>(offsetof(ReconstructionFilter_t4011033762, ____unroll_2)); }
	inline bool get__unroll_2() const { return ____unroll_2; }
	inline bool* get_address_of__unroll_2() { return &____unroll_2; }
	inline void set__unroll_2(bool value)
	{
		____unroll_2 = value;
	}

	inline static int32_t get_offset_of__vectorRTFormat_3() { return static_cast<int32_t>(offsetof(ReconstructionFilter_t4011033762, ____vectorRTFormat_3)); }
	inline int32_t get__vectorRTFormat_3() const { return ____vectorRTFormat_3; }
	inline int32_t* get_address_of__vectorRTFormat_3() { return &____vectorRTFormat_3; }
	inline void set__vectorRTFormat_3(int32_t value)
	{
		____vectorRTFormat_3 = value;
	}

	inline static int32_t get_offset_of__packedRTFormat_4() { return static_cast<int32_t>(offsetof(ReconstructionFilter_t4011033762, ____packedRTFormat_4)); }
	inline int32_t get__packedRTFormat_4() const { return ____packedRTFormat_4; }
	inline int32_t* get_address_of__packedRTFormat_4() { return &____packedRTFormat_4; }
	inline void set__packedRTFormat_4(int32_t value)
	{
		____packedRTFormat_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
