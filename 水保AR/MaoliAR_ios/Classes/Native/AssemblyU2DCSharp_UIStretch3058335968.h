﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "AssemblyU2DCSharp_UIStretch_Style3184300279.h"
#include "UnityEngine_UnityEngine_Vector22156229523.h"
#include "UnityEngine_UnityEngine_Rect2360479859.h"

// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UIWidget
struct UIWidget_t3538521925;
// UnityEngine.Transform
struct Transform_t3600365921;
// UISprite
struct UISprite_t194114938;
// UIPanel
struct UIPanel_t1716472341;
// UIRoot
struct UIRoot_t4022971450;
// UnityEngine.Animation
struct Animation_t3648466861;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIStretch
struct  UIStretch_t3058335968  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera UIStretch::uiCamera
	Camera_t4157153871 * ___uiCamera_2;
	// UnityEngine.GameObject UIStretch::container
	GameObject_t1113636619 * ___container_3;
	// UIStretch/Style UIStretch::style
	int32_t ___style_4;
	// System.Boolean UIStretch::runOnlyOnce
	bool ___runOnlyOnce_5;
	// UnityEngine.Vector2 UIStretch::relativeSize
	Vector2_t2156229523  ___relativeSize_6;
	// UnityEngine.Vector2 UIStretch::initialSize
	Vector2_t2156229523  ___initialSize_7;
	// UnityEngine.Vector2 UIStretch::borderPadding
	Vector2_t2156229523  ___borderPadding_8;
	// UIWidget UIStretch::widgetContainer
	UIWidget_t3538521925 * ___widgetContainer_9;
	// UnityEngine.Transform UIStretch::mTrans
	Transform_t3600365921 * ___mTrans_10;
	// UIWidget UIStretch::mWidget
	UIWidget_t3538521925 * ___mWidget_11;
	// UISprite UIStretch::mSprite
	UISprite_t194114938 * ___mSprite_12;
	// UIPanel UIStretch::mPanel
	UIPanel_t1716472341 * ___mPanel_13;
	// UIRoot UIStretch::mRoot
	UIRoot_t4022971450 * ___mRoot_14;
	// UnityEngine.Animation UIStretch::mAnim
	Animation_t3648466861 * ___mAnim_15;
	// UnityEngine.Rect UIStretch::mRect
	Rect_t2360479859  ___mRect_16;
	// System.Boolean UIStretch::mStarted
	bool ___mStarted_17;

public:
	inline static int32_t get_offset_of_uiCamera_2() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___uiCamera_2)); }
	inline Camera_t4157153871 * get_uiCamera_2() const { return ___uiCamera_2; }
	inline Camera_t4157153871 ** get_address_of_uiCamera_2() { return &___uiCamera_2; }
	inline void set_uiCamera_2(Camera_t4157153871 * value)
	{
		___uiCamera_2 = value;
		Il2CppCodeGenWriteBarrier(&___uiCamera_2, value);
	}

	inline static int32_t get_offset_of_container_3() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___container_3)); }
	inline GameObject_t1113636619 * get_container_3() const { return ___container_3; }
	inline GameObject_t1113636619 ** get_address_of_container_3() { return &___container_3; }
	inline void set_container_3(GameObject_t1113636619 * value)
	{
		___container_3 = value;
		Il2CppCodeGenWriteBarrier(&___container_3, value);
	}

	inline static int32_t get_offset_of_style_4() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___style_4)); }
	inline int32_t get_style_4() const { return ___style_4; }
	inline int32_t* get_address_of_style_4() { return &___style_4; }
	inline void set_style_4(int32_t value)
	{
		___style_4 = value;
	}

	inline static int32_t get_offset_of_runOnlyOnce_5() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___runOnlyOnce_5)); }
	inline bool get_runOnlyOnce_5() const { return ___runOnlyOnce_5; }
	inline bool* get_address_of_runOnlyOnce_5() { return &___runOnlyOnce_5; }
	inline void set_runOnlyOnce_5(bool value)
	{
		___runOnlyOnce_5 = value;
	}

	inline static int32_t get_offset_of_relativeSize_6() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___relativeSize_6)); }
	inline Vector2_t2156229523  get_relativeSize_6() const { return ___relativeSize_6; }
	inline Vector2_t2156229523 * get_address_of_relativeSize_6() { return &___relativeSize_6; }
	inline void set_relativeSize_6(Vector2_t2156229523  value)
	{
		___relativeSize_6 = value;
	}

	inline static int32_t get_offset_of_initialSize_7() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___initialSize_7)); }
	inline Vector2_t2156229523  get_initialSize_7() const { return ___initialSize_7; }
	inline Vector2_t2156229523 * get_address_of_initialSize_7() { return &___initialSize_7; }
	inline void set_initialSize_7(Vector2_t2156229523  value)
	{
		___initialSize_7 = value;
	}

	inline static int32_t get_offset_of_borderPadding_8() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___borderPadding_8)); }
	inline Vector2_t2156229523  get_borderPadding_8() const { return ___borderPadding_8; }
	inline Vector2_t2156229523 * get_address_of_borderPadding_8() { return &___borderPadding_8; }
	inline void set_borderPadding_8(Vector2_t2156229523  value)
	{
		___borderPadding_8 = value;
	}

	inline static int32_t get_offset_of_widgetContainer_9() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___widgetContainer_9)); }
	inline UIWidget_t3538521925 * get_widgetContainer_9() const { return ___widgetContainer_9; }
	inline UIWidget_t3538521925 ** get_address_of_widgetContainer_9() { return &___widgetContainer_9; }
	inline void set_widgetContainer_9(UIWidget_t3538521925 * value)
	{
		___widgetContainer_9 = value;
		Il2CppCodeGenWriteBarrier(&___widgetContainer_9, value);
	}

	inline static int32_t get_offset_of_mTrans_10() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___mTrans_10)); }
	inline Transform_t3600365921 * get_mTrans_10() const { return ___mTrans_10; }
	inline Transform_t3600365921 ** get_address_of_mTrans_10() { return &___mTrans_10; }
	inline void set_mTrans_10(Transform_t3600365921 * value)
	{
		___mTrans_10 = value;
		Il2CppCodeGenWriteBarrier(&___mTrans_10, value);
	}

	inline static int32_t get_offset_of_mWidget_11() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___mWidget_11)); }
	inline UIWidget_t3538521925 * get_mWidget_11() const { return ___mWidget_11; }
	inline UIWidget_t3538521925 ** get_address_of_mWidget_11() { return &___mWidget_11; }
	inline void set_mWidget_11(UIWidget_t3538521925 * value)
	{
		___mWidget_11 = value;
		Il2CppCodeGenWriteBarrier(&___mWidget_11, value);
	}

	inline static int32_t get_offset_of_mSprite_12() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___mSprite_12)); }
	inline UISprite_t194114938 * get_mSprite_12() const { return ___mSprite_12; }
	inline UISprite_t194114938 ** get_address_of_mSprite_12() { return &___mSprite_12; }
	inline void set_mSprite_12(UISprite_t194114938 * value)
	{
		___mSprite_12 = value;
		Il2CppCodeGenWriteBarrier(&___mSprite_12, value);
	}

	inline static int32_t get_offset_of_mPanel_13() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___mPanel_13)); }
	inline UIPanel_t1716472341 * get_mPanel_13() const { return ___mPanel_13; }
	inline UIPanel_t1716472341 ** get_address_of_mPanel_13() { return &___mPanel_13; }
	inline void set_mPanel_13(UIPanel_t1716472341 * value)
	{
		___mPanel_13 = value;
		Il2CppCodeGenWriteBarrier(&___mPanel_13, value);
	}

	inline static int32_t get_offset_of_mRoot_14() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___mRoot_14)); }
	inline UIRoot_t4022971450 * get_mRoot_14() const { return ___mRoot_14; }
	inline UIRoot_t4022971450 ** get_address_of_mRoot_14() { return &___mRoot_14; }
	inline void set_mRoot_14(UIRoot_t4022971450 * value)
	{
		___mRoot_14 = value;
		Il2CppCodeGenWriteBarrier(&___mRoot_14, value);
	}

	inline static int32_t get_offset_of_mAnim_15() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___mAnim_15)); }
	inline Animation_t3648466861 * get_mAnim_15() const { return ___mAnim_15; }
	inline Animation_t3648466861 ** get_address_of_mAnim_15() { return &___mAnim_15; }
	inline void set_mAnim_15(Animation_t3648466861 * value)
	{
		___mAnim_15 = value;
		Il2CppCodeGenWriteBarrier(&___mAnim_15, value);
	}

	inline static int32_t get_offset_of_mRect_16() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___mRect_16)); }
	inline Rect_t2360479859  get_mRect_16() const { return ___mRect_16; }
	inline Rect_t2360479859 * get_address_of_mRect_16() { return &___mRect_16; }
	inline void set_mRect_16(Rect_t2360479859  value)
	{
		___mRect_16 = value;
	}

	inline static int32_t get_offset_of_mStarted_17() { return static_cast<int32_t>(offsetof(UIStretch_t3058335968, ___mStarted_17)); }
	inline bool get_mStarted_17() const { return ___mStarted_17; }
	inline bool* get_address_of_mStarted_17() { return &___mStarted_17; }
	inline void set_mStarted_17(bool value)
	{
		___mStarted_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
