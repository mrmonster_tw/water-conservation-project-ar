﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Security_S1506236276.h"

// Mono.Security.Protocol.Ntlm.Type2Message
struct Type2Message_t2139513824;
// Mono.Security.Protocol.Ntlm.Type3Message
struct Type3Message_t2139513857;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.SspiClientSession
struct  SspiClientSession_t1775079126  : public SspiSession_t1506236276
{
public:
	// Mono.Security.Protocol.Ntlm.Type2Message System.ServiceModel.Security.SspiClientSession::type2
	Type2Message_t2139513824 * ___type2_8;
	// Mono.Security.Protocol.Ntlm.Type3Message System.ServiceModel.Security.SspiClientSession::type3
	Type3Message_t2139513857 * ___type3_9;
	// System.String System.ServiceModel.Security.SspiClientSession::TargetName
	String_t* ___TargetName_10;

public:
	inline static int32_t get_offset_of_type2_8() { return static_cast<int32_t>(offsetof(SspiClientSession_t1775079126, ___type2_8)); }
	inline Type2Message_t2139513824 * get_type2_8() const { return ___type2_8; }
	inline Type2Message_t2139513824 ** get_address_of_type2_8() { return &___type2_8; }
	inline void set_type2_8(Type2Message_t2139513824 * value)
	{
		___type2_8 = value;
		Il2CppCodeGenWriteBarrier(&___type2_8, value);
	}

	inline static int32_t get_offset_of_type3_9() { return static_cast<int32_t>(offsetof(SspiClientSession_t1775079126, ___type3_9)); }
	inline Type3Message_t2139513857 * get_type3_9() const { return ___type3_9; }
	inline Type3Message_t2139513857 ** get_address_of_type3_9() { return &___type3_9; }
	inline void set_type3_9(Type3Message_t2139513857 * value)
	{
		___type3_9 = value;
		Il2CppCodeGenWriteBarrier(&___type3_9, value);
	}

	inline static int32_t get_offset_of_TargetName_10() { return static_cast<int32_t>(offsetof(SspiClientSession_t1775079126, ___TargetName_10)); }
	inline String_t* get_TargetName_10() const { return ___TargetName_10; }
	inline String_t** get_address_of_TargetName_10() { return &___TargetName_10; }
	inline void set_TargetName_10(String_t* value)
	{
		___TargetName_10 = value;
		Il2CppCodeGenWriteBarrier(&___TargetName_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
