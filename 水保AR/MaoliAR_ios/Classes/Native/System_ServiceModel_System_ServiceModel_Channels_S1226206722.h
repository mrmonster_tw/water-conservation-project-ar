﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Security_System_Security_Cryptography_Xml_K4210275625.h"

// System.IdentityModel.Tokens.SecurityKeyIdentifierClause
struct SecurityKeyIdentifierClause_t1943429813;
// System.IdentityModel.Selectors.SecurityTokenSerializer
struct SecurityTokenSerializer_t972969391;
// System.Xml.XmlDocument
struct XmlDocument_t2837193595;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.SecurityTokenReferenceKeyInfo
struct  SecurityTokenReferenceKeyInfo_t1226206722  : public KeyInfoClause_t4210275625
{
public:
	// System.IdentityModel.Tokens.SecurityKeyIdentifierClause System.ServiceModel.Channels.SecurityTokenReferenceKeyInfo::clause
	SecurityKeyIdentifierClause_t1943429813 * ___clause_0;
	// System.IdentityModel.Selectors.SecurityTokenSerializer System.ServiceModel.Channels.SecurityTokenReferenceKeyInfo::serializer
	SecurityTokenSerializer_t972969391 * ___serializer_1;
	// System.Xml.XmlDocument System.ServiceModel.Channels.SecurityTokenReferenceKeyInfo::doc
	XmlDocument_t2837193595 * ___doc_2;

public:
	inline static int32_t get_offset_of_clause_0() { return static_cast<int32_t>(offsetof(SecurityTokenReferenceKeyInfo_t1226206722, ___clause_0)); }
	inline SecurityKeyIdentifierClause_t1943429813 * get_clause_0() const { return ___clause_0; }
	inline SecurityKeyIdentifierClause_t1943429813 ** get_address_of_clause_0() { return &___clause_0; }
	inline void set_clause_0(SecurityKeyIdentifierClause_t1943429813 * value)
	{
		___clause_0 = value;
		Il2CppCodeGenWriteBarrier(&___clause_0, value);
	}

	inline static int32_t get_offset_of_serializer_1() { return static_cast<int32_t>(offsetof(SecurityTokenReferenceKeyInfo_t1226206722, ___serializer_1)); }
	inline SecurityTokenSerializer_t972969391 * get_serializer_1() const { return ___serializer_1; }
	inline SecurityTokenSerializer_t972969391 ** get_address_of_serializer_1() { return &___serializer_1; }
	inline void set_serializer_1(SecurityTokenSerializer_t972969391 * value)
	{
		___serializer_1 = value;
		Il2CppCodeGenWriteBarrier(&___serializer_1, value);
	}

	inline static int32_t get_offset_of_doc_2() { return static_cast<int32_t>(offsetof(SecurityTokenReferenceKeyInfo_t1226206722, ___doc_2)); }
	inline XmlDocument_t2837193595 * get_doc_2() const { return ___doc_2; }
	inline XmlDocument_t2837193595 ** get_address_of_doc_2() { return &___doc_2; }
	inline void set_doc_2(XmlDocument_t2837193595 * value)
	{
		___doc_2 = value;
		Il2CppCodeGenWriteBarrier(&___doc_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
