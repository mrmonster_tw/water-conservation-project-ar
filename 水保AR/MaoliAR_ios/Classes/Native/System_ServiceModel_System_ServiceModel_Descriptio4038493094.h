﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_System_ServiceModel_Descriptio1965852526.h"

// System.ServiceModel.Description.ContractDescription
struct ContractDescription_t1411178514;
// System.ServiceModel.Channels.Binding
struct Binding_t859993683;
// System.ServiceModel.EndpointAddress
struct EndpointAddress_t3119842923;
// System.Collections.Generic.KeyedByTypeCollection`1<System.ServiceModel.Description.IEndpointBehavior>
struct KeyedByTypeCollection_1_t4222441378;
// System.Uri
struct Uri_t100236324;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.ServiceEndpoint
struct  ServiceEndpoint_t4038493094  : public Il2CppObject
{
public:
	// System.ServiceModel.Description.ContractDescription System.ServiceModel.Description.ServiceEndpoint::contract
	ContractDescription_t1411178514 * ___contract_0;
	// System.ServiceModel.Channels.Binding System.ServiceModel.Description.ServiceEndpoint::binding
	Binding_t859993683 * ___binding_1;
	// System.ServiceModel.EndpointAddress System.ServiceModel.Description.ServiceEndpoint::address
	EndpointAddress_t3119842923 * ___address_2;
	// System.Collections.Generic.KeyedByTypeCollection`1<System.ServiceModel.Description.IEndpointBehavior> System.ServiceModel.Description.ServiceEndpoint::behaviors
	KeyedByTypeCollection_1_t4222441378 * ___behaviors_3;
	// System.Uri System.ServiceModel.Description.ServiceEndpoint::listen_uri
	Uri_t100236324 * ___listen_uri_4;
	// System.ServiceModel.Description.ListenUriMode System.ServiceModel.Description.ServiceEndpoint::listen_mode
	int32_t ___listen_mode_5;

public:
	inline static int32_t get_offset_of_contract_0() { return static_cast<int32_t>(offsetof(ServiceEndpoint_t4038493094, ___contract_0)); }
	inline ContractDescription_t1411178514 * get_contract_0() const { return ___contract_0; }
	inline ContractDescription_t1411178514 ** get_address_of_contract_0() { return &___contract_0; }
	inline void set_contract_0(ContractDescription_t1411178514 * value)
	{
		___contract_0 = value;
		Il2CppCodeGenWriteBarrier(&___contract_0, value);
	}

	inline static int32_t get_offset_of_binding_1() { return static_cast<int32_t>(offsetof(ServiceEndpoint_t4038493094, ___binding_1)); }
	inline Binding_t859993683 * get_binding_1() const { return ___binding_1; }
	inline Binding_t859993683 ** get_address_of_binding_1() { return &___binding_1; }
	inline void set_binding_1(Binding_t859993683 * value)
	{
		___binding_1 = value;
		Il2CppCodeGenWriteBarrier(&___binding_1, value);
	}

	inline static int32_t get_offset_of_address_2() { return static_cast<int32_t>(offsetof(ServiceEndpoint_t4038493094, ___address_2)); }
	inline EndpointAddress_t3119842923 * get_address_2() const { return ___address_2; }
	inline EndpointAddress_t3119842923 ** get_address_of_address_2() { return &___address_2; }
	inline void set_address_2(EndpointAddress_t3119842923 * value)
	{
		___address_2 = value;
		Il2CppCodeGenWriteBarrier(&___address_2, value);
	}

	inline static int32_t get_offset_of_behaviors_3() { return static_cast<int32_t>(offsetof(ServiceEndpoint_t4038493094, ___behaviors_3)); }
	inline KeyedByTypeCollection_1_t4222441378 * get_behaviors_3() const { return ___behaviors_3; }
	inline KeyedByTypeCollection_1_t4222441378 ** get_address_of_behaviors_3() { return &___behaviors_3; }
	inline void set_behaviors_3(KeyedByTypeCollection_1_t4222441378 * value)
	{
		___behaviors_3 = value;
		Il2CppCodeGenWriteBarrier(&___behaviors_3, value);
	}

	inline static int32_t get_offset_of_listen_uri_4() { return static_cast<int32_t>(offsetof(ServiceEndpoint_t4038493094, ___listen_uri_4)); }
	inline Uri_t100236324 * get_listen_uri_4() const { return ___listen_uri_4; }
	inline Uri_t100236324 ** get_address_of_listen_uri_4() { return &___listen_uri_4; }
	inline void set_listen_uri_4(Uri_t100236324 * value)
	{
		___listen_uri_4 = value;
		Il2CppCodeGenWriteBarrier(&___listen_uri_4, value);
	}

	inline static int32_t get_offset_of_listen_mode_5() { return static_cast<int32_t>(offsetof(ServiceEndpoint_t4038493094, ___listen_mode_5)); }
	inline int32_t get_listen_mode_5() const { return ___listen_mode_5; }
	inline int32_t* get_address_of_listen_mode_5() { return &___listen_mode_5; }
	inline void set_listen_mode_5(int32_t value)
	{
		___listen_mode_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
