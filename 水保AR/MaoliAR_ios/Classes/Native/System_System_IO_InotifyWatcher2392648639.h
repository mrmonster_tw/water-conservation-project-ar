﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_IntPtr840150181.h"
#include "System_System_IO_InotifyMask3323194736.h"

// System.IO.InotifyWatcher
struct InotifyWatcher_t2392648639;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Threading.Thread
struct Thread_t2300836069;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.InotifyWatcher
struct  InotifyWatcher_t2392648639  : public Il2CppObject
{
public:

public:
};

struct InotifyWatcher_t2392648639_StaticFields
{
public:
	// System.Boolean System.IO.InotifyWatcher::failed
	bool ___failed_0;
	// System.IO.InotifyWatcher System.IO.InotifyWatcher::instance
	InotifyWatcher_t2392648639 * ___instance_1;
	// System.Collections.Hashtable System.IO.InotifyWatcher::watches
	Hashtable_t1853889766 * ___watches_2;
	// System.Collections.Hashtable System.IO.InotifyWatcher::requests
	Hashtable_t1853889766 * ___requests_3;
	// System.IntPtr System.IO.InotifyWatcher::FD
	IntPtr_t ___FD_4;
	// System.Threading.Thread System.IO.InotifyWatcher::thread
	Thread_t2300836069 * ___thread_5;
	// System.Boolean System.IO.InotifyWatcher::stop
	bool ___stop_6;
	// System.IO.InotifyMask System.IO.InotifyWatcher::Interesting
	uint32_t ___Interesting_7;

public:
	inline static int32_t get_offset_of_failed_0() { return static_cast<int32_t>(offsetof(InotifyWatcher_t2392648639_StaticFields, ___failed_0)); }
	inline bool get_failed_0() const { return ___failed_0; }
	inline bool* get_address_of_failed_0() { return &___failed_0; }
	inline void set_failed_0(bool value)
	{
		___failed_0 = value;
	}

	inline static int32_t get_offset_of_instance_1() { return static_cast<int32_t>(offsetof(InotifyWatcher_t2392648639_StaticFields, ___instance_1)); }
	inline InotifyWatcher_t2392648639 * get_instance_1() const { return ___instance_1; }
	inline InotifyWatcher_t2392648639 ** get_address_of_instance_1() { return &___instance_1; }
	inline void set_instance_1(InotifyWatcher_t2392648639 * value)
	{
		___instance_1 = value;
		Il2CppCodeGenWriteBarrier(&___instance_1, value);
	}

	inline static int32_t get_offset_of_watches_2() { return static_cast<int32_t>(offsetof(InotifyWatcher_t2392648639_StaticFields, ___watches_2)); }
	inline Hashtable_t1853889766 * get_watches_2() const { return ___watches_2; }
	inline Hashtable_t1853889766 ** get_address_of_watches_2() { return &___watches_2; }
	inline void set_watches_2(Hashtable_t1853889766 * value)
	{
		___watches_2 = value;
		Il2CppCodeGenWriteBarrier(&___watches_2, value);
	}

	inline static int32_t get_offset_of_requests_3() { return static_cast<int32_t>(offsetof(InotifyWatcher_t2392648639_StaticFields, ___requests_3)); }
	inline Hashtable_t1853889766 * get_requests_3() const { return ___requests_3; }
	inline Hashtable_t1853889766 ** get_address_of_requests_3() { return &___requests_3; }
	inline void set_requests_3(Hashtable_t1853889766 * value)
	{
		___requests_3 = value;
		Il2CppCodeGenWriteBarrier(&___requests_3, value);
	}

	inline static int32_t get_offset_of_FD_4() { return static_cast<int32_t>(offsetof(InotifyWatcher_t2392648639_StaticFields, ___FD_4)); }
	inline IntPtr_t get_FD_4() const { return ___FD_4; }
	inline IntPtr_t* get_address_of_FD_4() { return &___FD_4; }
	inline void set_FD_4(IntPtr_t value)
	{
		___FD_4 = value;
	}

	inline static int32_t get_offset_of_thread_5() { return static_cast<int32_t>(offsetof(InotifyWatcher_t2392648639_StaticFields, ___thread_5)); }
	inline Thread_t2300836069 * get_thread_5() const { return ___thread_5; }
	inline Thread_t2300836069 ** get_address_of_thread_5() { return &___thread_5; }
	inline void set_thread_5(Thread_t2300836069 * value)
	{
		___thread_5 = value;
		Il2CppCodeGenWriteBarrier(&___thread_5, value);
	}

	inline static int32_t get_offset_of_stop_6() { return static_cast<int32_t>(offsetof(InotifyWatcher_t2392648639_StaticFields, ___stop_6)); }
	inline bool get_stop_6() const { return ___stop_6; }
	inline bool* get_address_of_stop_6() { return &___stop_6; }
	inline void set_stop_6(bool value)
	{
		___stop_6 = value;
	}

	inline static int32_t get_offset_of_Interesting_7() { return static_cast<int32_t>(offsetof(InotifyWatcher_t2392648639_StaticFields, ___Interesting_7)); }
	inline uint32_t get_Interesting_7() const { return ___Interesting_7; }
	inline uint32_t* get_address_of_Interesting_7() { return &___Interesting_7; }
	inline void set_Interesting_7(uint32_t value)
	{
		___Interesting_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
