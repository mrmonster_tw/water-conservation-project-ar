﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.CollectionDataContractAttribute
struct  CollectionDataContractAttribute_t3671020647  : public Attribute_t861562559
{
public:
	// System.String System.Runtime.Serialization.CollectionDataContractAttribute::name
	String_t* ___name_0;
	// System.String System.Runtime.Serialization.CollectionDataContractAttribute::ns
	String_t* ___ns_1;
	// System.String System.Runtime.Serialization.CollectionDataContractAttribute::item_name
	String_t* ___item_name_2;
	// System.String System.Runtime.Serialization.CollectionDataContractAttribute::key_name
	String_t* ___key_name_3;
	// System.String System.Runtime.Serialization.CollectionDataContractAttribute::value_name
	String_t* ___value_name_4;
	// System.Boolean System.Runtime.Serialization.CollectionDataContractAttribute::<IsReference>k__BackingField
	bool ___U3CIsReferenceU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(CollectionDataContractAttribute_t3671020647, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_ns_1() { return static_cast<int32_t>(offsetof(CollectionDataContractAttribute_t3671020647, ___ns_1)); }
	inline String_t* get_ns_1() const { return ___ns_1; }
	inline String_t** get_address_of_ns_1() { return &___ns_1; }
	inline void set_ns_1(String_t* value)
	{
		___ns_1 = value;
		Il2CppCodeGenWriteBarrier(&___ns_1, value);
	}

	inline static int32_t get_offset_of_item_name_2() { return static_cast<int32_t>(offsetof(CollectionDataContractAttribute_t3671020647, ___item_name_2)); }
	inline String_t* get_item_name_2() const { return ___item_name_2; }
	inline String_t** get_address_of_item_name_2() { return &___item_name_2; }
	inline void set_item_name_2(String_t* value)
	{
		___item_name_2 = value;
		Il2CppCodeGenWriteBarrier(&___item_name_2, value);
	}

	inline static int32_t get_offset_of_key_name_3() { return static_cast<int32_t>(offsetof(CollectionDataContractAttribute_t3671020647, ___key_name_3)); }
	inline String_t* get_key_name_3() const { return ___key_name_3; }
	inline String_t** get_address_of_key_name_3() { return &___key_name_3; }
	inline void set_key_name_3(String_t* value)
	{
		___key_name_3 = value;
		Il2CppCodeGenWriteBarrier(&___key_name_3, value);
	}

	inline static int32_t get_offset_of_value_name_4() { return static_cast<int32_t>(offsetof(CollectionDataContractAttribute_t3671020647, ___value_name_4)); }
	inline String_t* get_value_name_4() const { return ___value_name_4; }
	inline String_t** get_address_of_value_name_4() { return &___value_name_4; }
	inline void set_value_name_4(String_t* value)
	{
		___value_name_4 = value;
		Il2CppCodeGenWriteBarrier(&___value_name_4, value);
	}

	inline static int32_t get_offset_of_U3CIsReferenceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CollectionDataContractAttribute_t3671020647, ___U3CIsReferenceU3Ek__BackingField_5)); }
	inline bool get_U3CIsReferenceU3Ek__BackingField_5() const { return ___U3CIsReferenceU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CIsReferenceU3Ek__BackingField_5() { return &___U3CIsReferenceU3Ek__BackingField_5; }
	inline void set_U3CIsReferenceU3Ek__BackingField_5(bool value)
	{
		___U3CIsReferenceU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
