﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Me831925271.h"

// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Runtime.Serialization.XmlObjectSerializer
struct XmlObjectSerializer_t3967301761;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.MessageHeader/DefaultMessageHeader
struct  DefaultMessageHeader_t3682274810  : public MessageHeader_t831925271
{
public:
	// System.String System.ServiceModel.Channels.MessageHeader/DefaultMessageHeader::actor
	String_t* ___actor_6;
	// System.String System.ServiceModel.Channels.MessageHeader/DefaultMessageHeader::name
	String_t* ___name_7;
	// System.String System.ServiceModel.Channels.MessageHeader/DefaultMessageHeader::ns
	String_t* ___ns_8;
	// System.Object System.ServiceModel.Channels.MessageHeader/DefaultMessageHeader::value
	Il2CppObject * ___value_9;
	// System.Runtime.Serialization.XmlObjectSerializer System.ServiceModel.Channels.MessageHeader/DefaultMessageHeader::formatter
	XmlObjectSerializer_t3967301761 * ___formatter_10;
	// System.Boolean System.ServiceModel.Channels.MessageHeader/DefaultMessageHeader::is_ref
	bool ___is_ref_11;
	// System.Boolean System.ServiceModel.Channels.MessageHeader/DefaultMessageHeader::must_understand
	bool ___must_understand_12;
	// System.Boolean System.ServiceModel.Channels.MessageHeader/DefaultMessageHeader::relay
	bool ___relay_13;

public:
	inline static int32_t get_offset_of_actor_6() { return static_cast<int32_t>(offsetof(DefaultMessageHeader_t3682274810, ___actor_6)); }
	inline String_t* get_actor_6() const { return ___actor_6; }
	inline String_t** get_address_of_actor_6() { return &___actor_6; }
	inline void set_actor_6(String_t* value)
	{
		___actor_6 = value;
		Il2CppCodeGenWriteBarrier(&___actor_6, value);
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(DefaultMessageHeader_t3682274810, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier(&___name_7, value);
	}

	inline static int32_t get_offset_of_ns_8() { return static_cast<int32_t>(offsetof(DefaultMessageHeader_t3682274810, ___ns_8)); }
	inline String_t* get_ns_8() const { return ___ns_8; }
	inline String_t** get_address_of_ns_8() { return &___ns_8; }
	inline void set_ns_8(String_t* value)
	{
		___ns_8 = value;
		Il2CppCodeGenWriteBarrier(&___ns_8, value);
	}

	inline static int32_t get_offset_of_value_9() { return static_cast<int32_t>(offsetof(DefaultMessageHeader_t3682274810, ___value_9)); }
	inline Il2CppObject * get_value_9() const { return ___value_9; }
	inline Il2CppObject ** get_address_of_value_9() { return &___value_9; }
	inline void set_value_9(Il2CppObject * value)
	{
		___value_9 = value;
		Il2CppCodeGenWriteBarrier(&___value_9, value);
	}

	inline static int32_t get_offset_of_formatter_10() { return static_cast<int32_t>(offsetof(DefaultMessageHeader_t3682274810, ___formatter_10)); }
	inline XmlObjectSerializer_t3967301761 * get_formatter_10() const { return ___formatter_10; }
	inline XmlObjectSerializer_t3967301761 ** get_address_of_formatter_10() { return &___formatter_10; }
	inline void set_formatter_10(XmlObjectSerializer_t3967301761 * value)
	{
		___formatter_10 = value;
		Il2CppCodeGenWriteBarrier(&___formatter_10, value);
	}

	inline static int32_t get_offset_of_is_ref_11() { return static_cast<int32_t>(offsetof(DefaultMessageHeader_t3682274810, ___is_ref_11)); }
	inline bool get_is_ref_11() const { return ___is_ref_11; }
	inline bool* get_address_of_is_ref_11() { return &___is_ref_11; }
	inline void set_is_ref_11(bool value)
	{
		___is_ref_11 = value;
	}

	inline static int32_t get_offset_of_must_understand_12() { return static_cast<int32_t>(offsetof(DefaultMessageHeader_t3682274810, ___must_understand_12)); }
	inline bool get_must_understand_12() const { return ___must_understand_12; }
	inline bool* get_address_of_must_understand_12() { return &___must_understand_12; }
	inline void set_must_understand_12(bool value)
	{
		___must_understand_12 = value;
	}

	inline static int32_t get_offset_of_relay_13() { return static_cast<int32_t>(offsetof(DefaultMessageHeader_t3682274810, ___relay_13)); }
	inline bool get_relay_13() const { return ___relay_13; }
	inline bool* get_address_of_relay_13() { return &___relay_13; }
	inline void set_relay_13(bool value)
	{
		___relay_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
