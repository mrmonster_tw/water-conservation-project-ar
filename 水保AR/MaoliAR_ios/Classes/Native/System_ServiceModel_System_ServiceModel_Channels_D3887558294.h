﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Channels.DuplexSessionBase/AsyncHandler
struct AsyncHandler_t217889813;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.DuplexSessionBase
struct  DuplexSessionBase_t3887558294  : public Il2CppObject
{
public:
	// System.ServiceModel.Channels.DuplexSessionBase/AsyncHandler System.ServiceModel.Channels.DuplexSessionBase::async_method
	AsyncHandler_t217889813 * ___async_method_0;
	// System.String System.ServiceModel.Channels.DuplexSessionBase::<Id>k__BackingField
	String_t* ___U3CIdU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_async_method_0() { return static_cast<int32_t>(offsetof(DuplexSessionBase_t3887558294, ___async_method_0)); }
	inline AsyncHandler_t217889813 * get_async_method_0() const { return ___async_method_0; }
	inline AsyncHandler_t217889813 ** get_address_of_async_method_0() { return &___async_method_0; }
	inline void set_async_method_0(AsyncHandler_t217889813 * value)
	{
		___async_method_0 = value;
		Il2CppCodeGenWriteBarrier(&___async_method_0, value);
	}

	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DuplexSessionBase_t3887558294, ___U3CIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CIdU3Ek__BackingField_1() const { return ___U3CIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CIdU3Ek__BackingField_1() { return &___U3CIdU3Ek__BackingField_1; }
	inline void set_U3CIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CIdU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
