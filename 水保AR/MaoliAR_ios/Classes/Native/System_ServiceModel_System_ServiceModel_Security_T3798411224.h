﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Tokens_S1271873540.h"
#include "mscorlib_System_DateTime3738529785.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IdentityModel.Tokens.SecurityKey>
struct ReadOnlyCollection_1_t1046284793;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4116647657;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.BinarySecretSecurityToken
struct  BinarySecretSecurityToken_t3798411224  : public SecurityToken_t1271873540
{
public:
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IdentityModel.Tokens.SecurityKey> System.ServiceModel.Security.Tokens.BinarySecretSecurityToken::keys
	ReadOnlyCollection_1_t1046284793 * ___keys_0;
	// System.String System.ServiceModel.Security.Tokens.BinarySecretSecurityToken::id
	String_t* ___id_1;
	// System.Byte[] System.ServiceModel.Security.Tokens.BinarySecretSecurityToken::key
	ByteU5BU5D_t4116647657* ___key_2;
	// System.Boolean System.ServiceModel.Security.Tokens.BinarySecretSecurityToken::allow_crypto
	bool ___allow_crypto_3;
	// System.DateTime System.ServiceModel.Security.Tokens.BinarySecretSecurityToken::valid_from
	DateTime_t3738529785  ___valid_from_4;

public:
	inline static int32_t get_offset_of_keys_0() { return static_cast<int32_t>(offsetof(BinarySecretSecurityToken_t3798411224, ___keys_0)); }
	inline ReadOnlyCollection_1_t1046284793 * get_keys_0() const { return ___keys_0; }
	inline ReadOnlyCollection_1_t1046284793 ** get_address_of_keys_0() { return &___keys_0; }
	inline void set_keys_0(ReadOnlyCollection_1_t1046284793 * value)
	{
		___keys_0 = value;
		Il2CppCodeGenWriteBarrier(&___keys_0, value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(BinarySecretSecurityToken_t3798411224, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier(&___id_1, value);
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(BinarySecretSecurityToken_t3798411224, ___key_2)); }
	inline ByteU5BU5D_t4116647657* get_key_2() const { return ___key_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(ByteU5BU5D_t4116647657* value)
	{
		___key_2 = value;
		Il2CppCodeGenWriteBarrier(&___key_2, value);
	}

	inline static int32_t get_offset_of_allow_crypto_3() { return static_cast<int32_t>(offsetof(BinarySecretSecurityToken_t3798411224, ___allow_crypto_3)); }
	inline bool get_allow_crypto_3() const { return ___allow_crypto_3; }
	inline bool* get_address_of_allow_crypto_3() { return &___allow_crypto_3; }
	inline void set_allow_crypto_3(bool value)
	{
		___allow_crypto_3 = value;
	}

	inline static int32_t get_offset_of_valid_from_4() { return static_cast<int32_t>(offsetof(BinarySecretSecurityToken_t3798411224, ___valid_from_4)); }
	inline DateTime_t3738529785  get_valid_from_4() const { return ___valid_from_4; }
	inline DateTime_t3738529785 * get_address_of_valid_from_4() { return &___valid_from_4; }
	inline void set_valid_from_4(DateTime_t3738529785  value)
	{
		___valid_from_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
