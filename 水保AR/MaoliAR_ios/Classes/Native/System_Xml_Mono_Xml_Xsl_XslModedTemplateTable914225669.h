﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2760654312;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.XslModedTemplateTable
struct  XslModedTemplateTable_t914225669  : public Il2CppObject
{
public:
	// System.Collections.ArrayList Mono.Xml.Xsl.XslModedTemplateTable::unnamedTemplates
	ArrayList_t2718874744 * ___unnamedTemplates_0;
	// System.Xml.XmlQualifiedName Mono.Xml.Xsl.XslModedTemplateTable::mode
	XmlQualifiedName_t2760654312 * ___mode_1;
	// System.Boolean Mono.Xml.Xsl.XslModedTemplateTable::sorted
	bool ___sorted_2;

public:
	inline static int32_t get_offset_of_unnamedTemplates_0() { return static_cast<int32_t>(offsetof(XslModedTemplateTable_t914225669, ___unnamedTemplates_0)); }
	inline ArrayList_t2718874744 * get_unnamedTemplates_0() const { return ___unnamedTemplates_0; }
	inline ArrayList_t2718874744 ** get_address_of_unnamedTemplates_0() { return &___unnamedTemplates_0; }
	inline void set_unnamedTemplates_0(ArrayList_t2718874744 * value)
	{
		___unnamedTemplates_0 = value;
		Il2CppCodeGenWriteBarrier(&___unnamedTemplates_0, value);
	}

	inline static int32_t get_offset_of_mode_1() { return static_cast<int32_t>(offsetof(XslModedTemplateTable_t914225669, ___mode_1)); }
	inline XmlQualifiedName_t2760654312 * get_mode_1() const { return ___mode_1; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_mode_1() { return &___mode_1; }
	inline void set_mode_1(XmlQualifiedName_t2760654312 * value)
	{
		___mode_1 = value;
		Il2CppCodeGenWriteBarrier(&___mode_1, value);
	}

	inline static int32_t get_offset_of_sorted_2() { return static_cast<int32_t>(offsetof(XslModedTemplateTable_t914225669, ___sorted_2)); }
	inline bool get_sorted_2() const { return ___sorted_2; }
	inline bool* get_address_of_sorted_2() { return &___sorted_2; }
	inline void set_sorted_2(bool value)
	{
		___sorted_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
