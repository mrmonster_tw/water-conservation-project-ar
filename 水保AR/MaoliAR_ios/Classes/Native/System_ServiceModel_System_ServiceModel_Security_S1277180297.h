﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.ObjectModel.Collection`1<System.Type>
struct Collection_1_t1428300678;
// System.ServiceModel.Security.SecurityStateEncoder
struct SecurityStateEncoder_t2009957818;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.SecureConversationServiceCredential
struct  SecureConversationServiceCredential_t1277180297  : public Il2CppObject
{
public:
	// System.Collections.ObjectModel.Collection`1<System.Type> System.ServiceModel.Security.SecureConversationServiceCredential::ctx_claim_types
	Collection_1_t1428300678 * ___ctx_claim_types_0;
	// System.ServiceModel.Security.SecurityStateEncoder System.ServiceModel.Security.SecureConversationServiceCredential::encoder
	SecurityStateEncoder_t2009957818 * ___encoder_1;

public:
	inline static int32_t get_offset_of_ctx_claim_types_0() { return static_cast<int32_t>(offsetof(SecureConversationServiceCredential_t1277180297, ___ctx_claim_types_0)); }
	inline Collection_1_t1428300678 * get_ctx_claim_types_0() const { return ___ctx_claim_types_0; }
	inline Collection_1_t1428300678 ** get_address_of_ctx_claim_types_0() { return &___ctx_claim_types_0; }
	inline void set_ctx_claim_types_0(Collection_1_t1428300678 * value)
	{
		___ctx_claim_types_0 = value;
		Il2CppCodeGenWriteBarrier(&___ctx_claim_types_0, value);
	}

	inline static int32_t get_offset_of_encoder_1() { return static_cast<int32_t>(offsetof(SecureConversationServiceCredential_t1277180297, ___encoder_1)); }
	inline SecurityStateEncoder_t2009957818 * get_encoder_1() const { return ___encoder_1; }
	inline SecurityStateEncoder_t2009957818 ** get_address_of_encoder_1() { return &___encoder_1; }
	inline void set_encoder_1(SecurityStateEncoder_t2009957818 * value)
	{
		___encoder_1 = value;
		Il2CppCodeGenWriteBarrier(&___encoder_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
