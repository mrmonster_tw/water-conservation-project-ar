﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Ch740725699.h"
#include "mscorlib_System_TimeSpan881159249.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.ChannelFactoryBase
struct  ChannelFactoryBase_t715356738  : public ChannelManagerBase_t740725699
{
public:
	// System.TimeSpan System.ServiceModel.Channels.ChannelFactoryBase::open_timeout
	TimeSpan_t881159249  ___open_timeout_9;
	// System.TimeSpan System.ServiceModel.Channels.ChannelFactoryBase::close_timeout
	TimeSpan_t881159249  ___close_timeout_10;
	// System.TimeSpan System.ServiceModel.Channels.ChannelFactoryBase::receive_timeout
	TimeSpan_t881159249  ___receive_timeout_11;
	// System.TimeSpan System.ServiceModel.Channels.ChannelFactoryBase::send_timeout
	TimeSpan_t881159249  ___send_timeout_12;

public:
	inline static int32_t get_offset_of_open_timeout_9() { return static_cast<int32_t>(offsetof(ChannelFactoryBase_t715356738, ___open_timeout_9)); }
	inline TimeSpan_t881159249  get_open_timeout_9() const { return ___open_timeout_9; }
	inline TimeSpan_t881159249 * get_address_of_open_timeout_9() { return &___open_timeout_9; }
	inline void set_open_timeout_9(TimeSpan_t881159249  value)
	{
		___open_timeout_9 = value;
	}

	inline static int32_t get_offset_of_close_timeout_10() { return static_cast<int32_t>(offsetof(ChannelFactoryBase_t715356738, ___close_timeout_10)); }
	inline TimeSpan_t881159249  get_close_timeout_10() const { return ___close_timeout_10; }
	inline TimeSpan_t881159249 * get_address_of_close_timeout_10() { return &___close_timeout_10; }
	inline void set_close_timeout_10(TimeSpan_t881159249  value)
	{
		___close_timeout_10 = value;
	}

	inline static int32_t get_offset_of_receive_timeout_11() { return static_cast<int32_t>(offsetof(ChannelFactoryBase_t715356738, ___receive_timeout_11)); }
	inline TimeSpan_t881159249  get_receive_timeout_11() const { return ___receive_timeout_11; }
	inline TimeSpan_t881159249 * get_address_of_receive_timeout_11() { return &___receive_timeout_11; }
	inline void set_receive_timeout_11(TimeSpan_t881159249  value)
	{
		___receive_timeout_11 = value;
	}

	inline static int32_t get_offset_of_send_timeout_12() { return static_cast<int32_t>(offsetof(ChannelFactoryBase_t715356738, ___send_timeout_12)); }
	inline TimeSpan_t881159249  get_send_timeout_12() const { return ___send_timeout_12; }
	inline TimeSpan_t881159249 * get_address_of_send_timeout_12() { return &___send_timeout_12; }
	inline void set_send_timeout_12(TimeSpan_t881159249  value)
	{
		___send_timeout_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
