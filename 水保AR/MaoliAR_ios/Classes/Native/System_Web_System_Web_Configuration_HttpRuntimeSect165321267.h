﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3156163955.h"

// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.HttpRuntimeSection
struct  HttpRuntimeSection_t165321267  : public ConfigurationSection_t3156163955
{
public:

public:
};

struct HttpRuntimeSection_t165321267_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Web.Configuration.HttpRuntimeSection::apartmentThreadingProp
	ConfigurationProperty_t3590861854 * ___apartmentThreadingProp_17;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.HttpRuntimeSection::appRequestQueueLimitProp
	ConfigurationProperty_t3590861854 * ___appRequestQueueLimitProp_18;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.HttpRuntimeSection::delayNotificationTimeoutProp
	ConfigurationProperty_t3590861854 * ___delayNotificationTimeoutProp_19;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.HttpRuntimeSection::enableProp
	ConfigurationProperty_t3590861854 * ___enableProp_20;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.HttpRuntimeSection::enableHeaderCheckingProp
	ConfigurationProperty_t3590861854 * ___enableHeaderCheckingProp_21;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.HttpRuntimeSection::enableKernelOutputCacheProp
	ConfigurationProperty_t3590861854 * ___enableKernelOutputCacheProp_22;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.HttpRuntimeSection::enableVersionHeaderProp
	ConfigurationProperty_t3590861854 * ___enableVersionHeaderProp_23;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.HttpRuntimeSection::executionTimeoutProp
	ConfigurationProperty_t3590861854 * ___executionTimeoutProp_24;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.HttpRuntimeSection::maxRequestLengthProp
	ConfigurationProperty_t3590861854 * ___maxRequestLengthProp_25;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.HttpRuntimeSection::maxWaitChangeNotificationProp
	ConfigurationProperty_t3590861854 * ___maxWaitChangeNotificationProp_26;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.HttpRuntimeSection::minFreeThreadsProp
	ConfigurationProperty_t3590861854 * ___minFreeThreadsProp_27;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.HttpRuntimeSection::minLocalRequestFreeThreadsProp
	ConfigurationProperty_t3590861854 * ___minLocalRequestFreeThreadsProp_28;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.HttpRuntimeSection::requestLengthDiskThresholdProp
	ConfigurationProperty_t3590861854 * ___requestLengthDiskThresholdProp_29;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.HttpRuntimeSection::requireRootedSaveAsPathProp
	ConfigurationProperty_t3590861854 * ___requireRootedSaveAsPathProp_30;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.HttpRuntimeSection::sendCacheControlHeaderProp
	ConfigurationProperty_t3590861854 * ___sendCacheControlHeaderProp_31;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.HttpRuntimeSection::shutdownTimeoutProp
	ConfigurationProperty_t3590861854 * ___shutdownTimeoutProp_32;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.HttpRuntimeSection::useFullyQualifiedRedirectUrlProp
	ConfigurationProperty_t3590861854 * ___useFullyQualifiedRedirectUrlProp_33;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.HttpRuntimeSection::waitChangeNotificationProp
	ConfigurationProperty_t3590861854 * ___waitChangeNotificationProp_34;
	// System.Configuration.ConfigurationPropertyCollection System.Web.Configuration.HttpRuntimeSection::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_35;

public:
	inline static int32_t get_offset_of_apartmentThreadingProp_17() { return static_cast<int32_t>(offsetof(HttpRuntimeSection_t165321267_StaticFields, ___apartmentThreadingProp_17)); }
	inline ConfigurationProperty_t3590861854 * get_apartmentThreadingProp_17() const { return ___apartmentThreadingProp_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_apartmentThreadingProp_17() { return &___apartmentThreadingProp_17; }
	inline void set_apartmentThreadingProp_17(ConfigurationProperty_t3590861854 * value)
	{
		___apartmentThreadingProp_17 = value;
		Il2CppCodeGenWriteBarrier(&___apartmentThreadingProp_17, value);
	}

	inline static int32_t get_offset_of_appRequestQueueLimitProp_18() { return static_cast<int32_t>(offsetof(HttpRuntimeSection_t165321267_StaticFields, ___appRequestQueueLimitProp_18)); }
	inline ConfigurationProperty_t3590861854 * get_appRequestQueueLimitProp_18() const { return ___appRequestQueueLimitProp_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_appRequestQueueLimitProp_18() { return &___appRequestQueueLimitProp_18; }
	inline void set_appRequestQueueLimitProp_18(ConfigurationProperty_t3590861854 * value)
	{
		___appRequestQueueLimitProp_18 = value;
		Il2CppCodeGenWriteBarrier(&___appRequestQueueLimitProp_18, value);
	}

	inline static int32_t get_offset_of_delayNotificationTimeoutProp_19() { return static_cast<int32_t>(offsetof(HttpRuntimeSection_t165321267_StaticFields, ___delayNotificationTimeoutProp_19)); }
	inline ConfigurationProperty_t3590861854 * get_delayNotificationTimeoutProp_19() const { return ___delayNotificationTimeoutProp_19; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_delayNotificationTimeoutProp_19() { return &___delayNotificationTimeoutProp_19; }
	inline void set_delayNotificationTimeoutProp_19(ConfigurationProperty_t3590861854 * value)
	{
		___delayNotificationTimeoutProp_19 = value;
		Il2CppCodeGenWriteBarrier(&___delayNotificationTimeoutProp_19, value);
	}

	inline static int32_t get_offset_of_enableProp_20() { return static_cast<int32_t>(offsetof(HttpRuntimeSection_t165321267_StaticFields, ___enableProp_20)); }
	inline ConfigurationProperty_t3590861854 * get_enableProp_20() const { return ___enableProp_20; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_enableProp_20() { return &___enableProp_20; }
	inline void set_enableProp_20(ConfigurationProperty_t3590861854 * value)
	{
		___enableProp_20 = value;
		Il2CppCodeGenWriteBarrier(&___enableProp_20, value);
	}

	inline static int32_t get_offset_of_enableHeaderCheckingProp_21() { return static_cast<int32_t>(offsetof(HttpRuntimeSection_t165321267_StaticFields, ___enableHeaderCheckingProp_21)); }
	inline ConfigurationProperty_t3590861854 * get_enableHeaderCheckingProp_21() const { return ___enableHeaderCheckingProp_21; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_enableHeaderCheckingProp_21() { return &___enableHeaderCheckingProp_21; }
	inline void set_enableHeaderCheckingProp_21(ConfigurationProperty_t3590861854 * value)
	{
		___enableHeaderCheckingProp_21 = value;
		Il2CppCodeGenWriteBarrier(&___enableHeaderCheckingProp_21, value);
	}

	inline static int32_t get_offset_of_enableKernelOutputCacheProp_22() { return static_cast<int32_t>(offsetof(HttpRuntimeSection_t165321267_StaticFields, ___enableKernelOutputCacheProp_22)); }
	inline ConfigurationProperty_t3590861854 * get_enableKernelOutputCacheProp_22() const { return ___enableKernelOutputCacheProp_22; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_enableKernelOutputCacheProp_22() { return &___enableKernelOutputCacheProp_22; }
	inline void set_enableKernelOutputCacheProp_22(ConfigurationProperty_t3590861854 * value)
	{
		___enableKernelOutputCacheProp_22 = value;
		Il2CppCodeGenWriteBarrier(&___enableKernelOutputCacheProp_22, value);
	}

	inline static int32_t get_offset_of_enableVersionHeaderProp_23() { return static_cast<int32_t>(offsetof(HttpRuntimeSection_t165321267_StaticFields, ___enableVersionHeaderProp_23)); }
	inline ConfigurationProperty_t3590861854 * get_enableVersionHeaderProp_23() const { return ___enableVersionHeaderProp_23; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_enableVersionHeaderProp_23() { return &___enableVersionHeaderProp_23; }
	inline void set_enableVersionHeaderProp_23(ConfigurationProperty_t3590861854 * value)
	{
		___enableVersionHeaderProp_23 = value;
		Il2CppCodeGenWriteBarrier(&___enableVersionHeaderProp_23, value);
	}

	inline static int32_t get_offset_of_executionTimeoutProp_24() { return static_cast<int32_t>(offsetof(HttpRuntimeSection_t165321267_StaticFields, ___executionTimeoutProp_24)); }
	inline ConfigurationProperty_t3590861854 * get_executionTimeoutProp_24() const { return ___executionTimeoutProp_24; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_executionTimeoutProp_24() { return &___executionTimeoutProp_24; }
	inline void set_executionTimeoutProp_24(ConfigurationProperty_t3590861854 * value)
	{
		___executionTimeoutProp_24 = value;
		Il2CppCodeGenWriteBarrier(&___executionTimeoutProp_24, value);
	}

	inline static int32_t get_offset_of_maxRequestLengthProp_25() { return static_cast<int32_t>(offsetof(HttpRuntimeSection_t165321267_StaticFields, ___maxRequestLengthProp_25)); }
	inline ConfigurationProperty_t3590861854 * get_maxRequestLengthProp_25() const { return ___maxRequestLengthProp_25; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_maxRequestLengthProp_25() { return &___maxRequestLengthProp_25; }
	inline void set_maxRequestLengthProp_25(ConfigurationProperty_t3590861854 * value)
	{
		___maxRequestLengthProp_25 = value;
		Il2CppCodeGenWriteBarrier(&___maxRequestLengthProp_25, value);
	}

	inline static int32_t get_offset_of_maxWaitChangeNotificationProp_26() { return static_cast<int32_t>(offsetof(HttpRuntimeSection_t165321267_StaticFields, ___maxWaitChangeNotificationProp_26)); }
	inline ConfigurationProperty_t3590861854 * get_maxWaitChangeNotificationProp_26() const { return ___maxWaitChangeNotificationProp_26; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_maxWaitChangeNotificationProp_26() { return &___maxWaitChangeNotificationProp_26; }
	inline void set_maxWaitChangeNotificationProp_26(ConfigurationProperty_t3590861854 * value)
	{
		___maxWaitChangeNotificationProp_26 = value;
		Il2CppCodeGenWriteBarrier(&___maxWaitChangeNotificationProp_26, value);
	}

	inline static int32_t get_offset_of_minFreeThreadsProp_27() { return static_cast<int32_t>(offsetof(HttpRuntimeSection_t165321267_StaticFields, ___minFreeThreadsProp_27)); }
	inline ConfigurationProperty_t3590861854 * get_minFreeThreadsProp_27() const { return ___minFreeThreadsProp_27; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_minFreeThreadsProp_27() { return &___minFreeThreadsProp_27; }
	inline void set_minFreeThreadsProp_27(ConfigurationProperty_t3590861854 * value)
	{
		___minFreeThreadsProp_27 = value;
		Il2CppCodeGenWriteBarrier(&___minFreeThreadsProp_27, value);
	}

	inline static int32_t get_offset_of_minLocalRequestFreeThreadsProp_28() { return static_cast<int32_t>(offsetof(HttpRuntimeSection_t165321267_StaticFields, ___minLocalRequestFreeThreadsProp_28)); }
	inline ConfigurationProperty_t3590861854 * get_minLocalRequestFreeThreadsProp_28() const { return ___minLocalRequestFreeThreadsProp_28; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_minLocalRequestFreeThreadsProp_28() { return &___minLocalRequestFreeThreadsProp_28; }
	inline void set_minLocalRequestFreeThreadsProp_28(ConfigurationProperty_t3590861854 * value)
	{
		___minLocalRequestFreeThreadsProp_28 = value;
		Il2CppCodeGenWriteBarrier(&___minLocalRequestFreeThreadsProp_28, value);
	}

	inline static int32_t get_offset_of_requestLengthDiskThresholdProp_29() { return static_cast<int32_t>(offsetof(HttpRuntimeSection_t165321267_StaticFields, ___requestLengthDiskThresholdProp_29)); }
	inline ConfigurationProperty_t3590861854 * get_requestLengthDiskThresholdProp_29() const { return ___requestLengthDiskThresholdProp_29; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_requestLengthDiskThresholdProp_29() { return &___requestLengthDiskThresholdProp_29; }
	inline void set_requestLengthDiskThresholdProp_29(ConfigurationProperty_t3590861854 * value)
	{
		___requestLengthDiskThresholdProp_29 = value;
		Il2CppCodeGenWriteBarrier(&___requestLengthDiskThresholdProp_29, value);
	}

	inline static int32_t get_offset_of_requireRootedSaveAsPathProp_30() { return static_cast<int32_t>(offsetof(HttpRuntimeSection_t165321267_StaticFields, ___requireRootedSaveAsPathProp_30)); }
	inline ConfigurationProperty_t3590861854 * get_requireRootedSaveAsPathProp_30() const { return ___requireRootedSaveAsPathProp_30; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_requireRootedSaveAsPathProp_30() { return &___requireRootedSaveAsPathProp_30; }
	inline void set_requireRootedSaveAsPathProp_30(ConfigurationProperty_t3590861854 * value)
	{
		___requireRootedSaveAsPathProp_30 = value;
		Il2CppCodeGenWriteBarrier(&___requireRootedSaveAsPathProp_30, value);
	}

	inline static int32_t get_offset_of_sendCacheControlHeaderProp_31() { return static_cast<int32_t>(offsetof(HttpRuntimeSection_t165321267_StaticFields, ___sendCacheControlHeaderProp_31)); }
	inline ConfigurationProperty_t3590861854 * get_sendCacheControlHeaderProp_31() const { return ___sendCacheControlHeaderProp_31; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_sendCacheControlHeaderProp_31() { return &___sendCacheControlHeaderProp_31; }
	inline void set_sendCacheControlHeaderProp_31(ConfigurationProperty_t3590861854 * value)
	{
		___sendCacheControlHeaderProp_31 = value;
		Il2CppCodeGenWriteBarrier(&___sendCacheControlHeaderProp_31, value);
	}

	inline static int32_t get_offset_of_shutdownTimeoutProp_32() { return static_cast<int32_t>(offsetof(HttpRuntimeSection_t165321267_StaticFields, ___shutdownTimeoutProp_32)); }
	inline ConfigurationProperty_t3590861854 * get_shutdownTimeoutProp_32() const { return ___shutdownTimeoutProp_32; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_shutdownTimeoutProp_32() { return &___shutdownTimeoutProp_32; }
	inline void set_shutdownTimeoutProp_32(ConfigurationProperty_t3590861854 * value)
	{
		___shutdownTimeoutProp_32 = value;
		Il2CppCodeGenWriteBarrier(&___shutdownTimeoutProp_32, value);
	}

	inline static int32_t get_offset_of_useFullyQualifiedRedirectUrlProp_33() { return static_cast<int32_t>(offsetof(HttpRuntimeSection_t165321267_StaticFields, ___useFullyQualifiedRedirectUrlProp_33)); }
	inline ConfigurationProperty_t3590861854 * get_useFullyQualifiedRedirectUrlProp_33() const { return ___useFullyQualifiedRedirectUrlProp_33; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_useFullyQualifiedRedirectUrlProp_33() { return &___useFullyQualifiedRedirectUrlProp_33; }
	inline void set_useFullyQualifiedRedirectUrlProp_33(ConfigurationProperty_t3590861854 * value)
	{
		___useFullyQualifiedRedirectUrlProp_33 = value;
		Il2CppCodeGenWriteBarrier(&___useFullyQualifiedRedirectUrlProp_33, value);
	}

	inline static int32_t get_offset_of_waitChangeNotificationProp_34() { return static_cast<int32_t>(offsetof(HttpRuntimeSection_t165321267_StaticFields, ___waitChangeNotificationProp_34)); }
	inline ConfigurationProperty_t3590861854 * get_waitChangeNotificationProp_34() const { return ___waitChangeNotificationProp_34; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_waitChangeNotificationProp_34() { return &___waitChangeNotificationProp_34; }
	inline void set_waitChangeNotificationProp_34(ConfigurationProperty_t3590861854 * value)
	{
		___waitChangeNotificationProp_34 = value;
		Il2CppCodeGenWriteBarrier(&___waitChangeNotificationProp_34, value);
	}

	inline static int32_t get_offset_of_properties_35() { return static_cast<int32_t>(offsetof(HttpRuntimeSection_t165321267_StaticFields, ___properties_35)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_35() const { return ___properties_35; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_35() { return &___properties_35; }
	inline void set_properties_35(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_35 = value;
		Il2CppCodeGenWriteBarrier(&___properties_35, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
