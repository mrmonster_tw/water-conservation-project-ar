﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.HttpContext
struct HttpContext_t1969259010;
// System.String
struct String_t;
// System.Web.Compilation.ILocation
struct ILocation_t3961726794;
// System.Web.VirtualPath
struct VirtualPath_t4270372584;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.BaseParser
struct  BaseParser_t4057001241  : public Il2CppObject
{
public:
	// System.Web.HttpContext System.Web.UI.BaseParser::context
	HttpContext_t1969259010 * ___context_0;
	// System.String System.Web.UI.BaseParser::baseDir
	String_t* ___baseDir_1;
	// System.String System.Web.UI.BaseParser::baseVDir
	String_t* ___baseVDir_2;
	// System.Web.Compilation.ILocation System.Web.UI.BaseParser::location
	Il2CppObject * ___location_3;
	// System.Web.VirtualPath System.Web.UI.BaseParser::<VirtualPath>k__BackingField
	VirtualPath_t4270372584 * ___U3CVirtualPathU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(BaseParser_t4057001241, ___context_0)); }
	inline HttpContext_t1969259010 * get_context_0() const { return ___context_0; }
	inline HttpContext_t1969259010 ** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(HttpContext_t1969259010 * value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier(&___context_0, value);
	}

	inline static int32_t get_offset_of_baseDir_1() { return static_cast<int32_t>(offsetof(BaseParser_t4057001241, ___baseDir_1)); }
	inline String_t* get_baseDir_1() const { return ___baseDir_1; }
	inline String_t** get_address_of_baseDir_1() { return &___baseDir_1; }
	inline void set_baseDir_1(String_t* value)
	{
		___baseDir_1 = value;
		Il2CppCodeGenWriteBarrier(&___baseDir_1, value);
	}

	inline static int32_t get_offset_of_baseVDir_2() { return static_cast<int32_t>(offsetof(BaseParser_t4057001241, ___baseVDir_2)); }
	inline String_t* get_baseVDir_2() const { return ___baseVDir_2; }
	inline String_t** get_address_of_baseVDir_2() { return &___baseVDir_2; }
	inline void set_baseVDir_2(String_t* value)
	{
		___baseVDir_2 = value;
		Il2CppCodeGenWriteBarrier(&___baseVDir_2, value);
	}

	inline static int32_t get_offset_of_location_3() { return static_cast<int32_t>(offsetof(BaseParser_t4057001241, ___location_3)); }
	inline Il2CppObject * get_location_3() const { return ___location_3; }
	inline Il2CppObject ** get_address_of_location_3() { return &___location_3; }
	inline void set_location_3(Il2CppObject * value)
	{
		___location_3 = value;
		Il2CppCodeGenWriteBarrier(&___location_3, value);
	}

	inline static int32_t get_offset_of_U3CVirtualPathU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BaseParser_t4057001241, ___U3CVirtualPathU3Ek__BackingField_4)); }
	inline VirtualPath_t4270372584 * get_U3CVirtualPathU3Ek__BackingField_4() const { return ___U3CVirtualPathU3Ek__BackingField_4; }
	inline VirtualPath_t4270372584 ** get_address_of_U3CVirtualPathU3Ek__BackingField_4() { return &___U3CVirtualPathU3Ek__BackingField_4; }
	inline void set_U3CVirtualPathU3Ek__BackingField_4(VirtualPath_t4270372584 * value)
	{
		___U3CVirtualPathU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CVirtualPathU3Ek__BackingField_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
