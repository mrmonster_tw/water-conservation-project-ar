﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.ServiceModel.EnvelopeVersion
struct EnvelopeVersion_t2487833488;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.EnvelopeVersion
struct  EnvelopeVersion_t2487833488  : public Il2CppObject
{
public:
	// System.String System.ServiceModel.EnvelopeVersion::name
	String_t* ___name_0;
	// System.String System.ServiceModel.EnvelopeVersion::uri
	String_t* ___uri_1;
	// System.String System.ServiceModel.EnvelopeVersion::next_destination
	String_t* ___next_destination_2;
	// System.String[] System.ServiceModel.EnvelopeVersion::ultimate_destination
	StringU5BU5D_t1281789340* ___ultimate_destination_3;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(EnvelopeVersion_t2487833488, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_uri_1() { return static_cast<int32_t>(offsetof(EnvelopeVersion_t2487833488, ___uri_1)); }
	inline String_t* get_uri_1() const { return ___uri_1; }
	inline String_t** get_address_of_uri_1() { return &___uri_1; }
	inline void set_uri_1(String_t* value)
	{
		___uri_1 = value;
		Il2CppCodeGenWriteBarrier(&___uri_1, value);
	}

	inline static int32_t get_offset_of_next_destination_2() { return static_cast<int32_t>(offsetof(EnvelopeVersion_t2487833488, ___next_destination_2)); }
	inline String_t* get_next_destination_2() const { return ___next_destination_2; }
	inline String_t** get_address_of_next_destination_2() { return &___next_destination_2; }
	inline void set_next_destination_2(String_t* value)
	{
		___next_destination_2 = value;
		Il2CppCodeGenWriteBarrier(&___next_destination_2, value);
	}

	inline static int32_t get_offset_of_ultimate_destination_3() { return static_cast<int32_t>(offsetof(EnvelopeVersion_t2487833488, ___ultimate_destination_3)); }
	inline StringU5BU5D_t1281789340* get_ultimate_destination_3() const { return ___ultimate_destination_3; }
	inline StringU5BU5D_t1281789340** get_address_of_ultimate_destination_3() { return &___ultimate_destination_3; }
	inline void set_ultimate_destination_3(StringU5BU5D_t1281789340* value)
	{
		___ultimate_destination_3 = value;
		Il2CppCodeGenWriteBarrier(&___ultimate_destination_3, value);
	}
};

struct EnvelopeVersion_t2487833488_StaticFields
{
public:
	// System.ServiceModel.EnvelopeVersion System.ServiceModel.EnvelopeVersion::soap11
	EnvelopeVersion_t2487833488 * ___soap11_4;
	// System.ServiceModel.EnvelopeVersion System.ServiceModel.EnvelopeVersion::soap12
	EnvelopeVersion_t2487833488 * ___soap12_5;
	// System.ServiceModel.EnvelopeVersion System.ServiceModel.EnvelopeVersion::none
	EnvelopeVersion_t2487833488 * ___none_6;

public:
	inline static int32_t get_offset_of_soap11_4() { return static_cast<int32_t>(offsetof(EnvelopeVersion_t2487833488_StaticFields, ___soap11_4)); }
	inline EnvelopeVersion_t2487833488 * get_soap11_4() const { return ___soap11_4; }
	inline EnvelopeVersion_t2487833488 ** get_address_of_soap11_4() { return &___soap11_4; }
	inline void set_soap11_4(EnvelopeVersion_t2487833488 * value)
	{
		___soap11_4 = value;
		Il2CppCodeGenWriteBarrier(&___soap11_4, value);
	}

	inline static int32_t get_offset_of_soap12_5() { return static_cast<int32_t>(offsetof(EnvelopeVersion_t2487833488_StaticFields, ___soap12_5)); }
	inline EnvelopeVersion_t2487833488 * get_soap12_5() const { return ___soap12_5; }
	inline EnvelopeVersion_t2487833488 ** get_address_of_soap12_5() { return &___soap12_5; }
	inline void set_soap12_5(EnvelopeVersion_t2487833488 * value)
	{
		___soap12_5 = value;
		Il2CppCodeGenWriteBarrier(&___soap12_5, value);
	}

	inline static int32_t get_offset_of_none_6() { return static_cast<int32_t>(offsetof(EnvelopeVersion_t2487833488_StaticFields, ___none_6)); }
	inline EnvelopeVersion_t2487833488 * get_none_6() const { return ___none_6; }
	inline EnvelopeVersion_t2487833488 ** get_address_of_none_6() { return &___none_6; }
	inline void set_none_6(EnvelopeVersion_t2487833488 * value)
	{
		___none_6 = value;
		Il2CppCodeGenWriteBarrier(&___none_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
