﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Security_T2868958784.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.UserNameSecurityTokenParameters
struct  UserNameSecurityTokenParameters_t4004515376  : public SecurityTokenParameters_t2868958784
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
