﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeObject3927604602.h"

// System.CodeDom.CodeTypeReferenceCollection
struct CodeTypeReferenceCollection_t3857551471;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeTypeParameter
struct  CodeTypeParameter_t3345688804  : public CodeObject_t3927604602
{
public:
	// System.CodeDom.CodeTypeReferenceCollection System.CodeDom.CodeTypeParameter::constraints
	CodeTypeReferenceCollection_t3857551471 * ___constraints_1;
	// System.Boolean System.CodeDom.CodeTypeParameter::hasConstructorConstraint
	bool ___hasConstructorConstraint_2;
	// System.String System.CodeDom.CodeTypeParameter::name
	String_t* ___name_3;

public:
	inline static int32_t get_offset_of_constraints_1() { return static_cast<int32_t>(offsetof(CodeTypeParameter_t3345688804, ___constraints_1)); }
	inline CodeTypeReferenceCollection_t3857551471 * get_constraints_1() const { return ___constraints_1; }
	inline CodeTypeReferenceCollection_t3857551471 ** get_address_of_constraints_1() { return &___constraints_1; }
	inline void set_constraints_1(CodeTypeReferenceCollection_t3857551471 * value)
	{
		___constraints_1 = value;
		Il2CppCodeGenWriteBarrier(&___constraints_1, value);
	}

	inline static int32_t get_offset_of_hasConstructorConstraint_2() { return static_cast<int32_t>(offsetof(CodeTypeParameter_t3345688804, ___hasConstructorConstraint_2)); }
	inline bool get_hasConstructorConstraint_2() const { return ___hasConstructorConstraint_2; }
	inline bool* get_address_of_hasConstructorConstraint_2() { return &___hasConstructorConstraint_2; }
	inline void set_hasConstructorConstraint_2(bool value)
	{
		___hasConstructorConstraint_2 = value;
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(CodeTypeParameter_t3345688804, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier(&___name_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
