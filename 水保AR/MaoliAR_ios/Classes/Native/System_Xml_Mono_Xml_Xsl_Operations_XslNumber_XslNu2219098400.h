﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Operations_XslNumber_XslNu2815695937.h"

// System.Char[]
struct CharU5BU5D_t3528271667;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslNumber/XslNumberFormatter/AlphaItem
struct  AlphaItem_t2219098400  : public FormatItem_t2815695937
{
public:
	// System.Boolean Mono.Xml.Xsl.Operations.XslNumber/XslNumberFormatter/AlphaItem::uc
	bool ___uc_1;

public:
	inline static int32_t get_offset_of_uc_1() { return static_cast<int32_t>(offsetof(AlphaItem_t2219098400, ___uc_1)); }
	inline bool get_uc_1() const { return ___uc_1; }
	inline bool* get_address_of_uc_1() { return &___uc_1; }
	inline void set_uc_1(bool value)
	{
		___uc_1 = value;
	}
};

struct AlphaItem_t2219098400_StaticFields
{
public:
	// System.Char[] Mono.Xml.Xsl.Operations.XslNumber/XslNumberFormatter/AlphaItem::ucl
	CharU5BU5D_t3528271667* ___ucl_2;
	// System.Char[] Mono.Xml.Xsl.Operations.XslNumber/XslNumberFormatter/AlphaItem::lcl
	CharU5BU5D_t3528271667* ___lcl_3;

public:
	inline static int32_t get_offset_of_ucl_2() { return static_cast<int32_t>(offsetof(AlphaItem_t2219098400_StaticFields, ___ucl_2)); }
	inline CharU5BU5D_t3528271667* get_ucl_2() const { return ___ucl_2; }
	inline CharU5BU5D_t3528271667** get_address_of_ucl_2() { return &___ucl_2; }
	inline void set_ucl_2(CharU5BU5D_t3528271667* value)
	{
		___ucl_2 = value;
		Il2CppCodeGenWriteBarrier(&___ucl_2, value);
	}

	inline static int32_t get_offset_of_lcl_3() { return static_cast<int32_t>(offsetof(AlphaItem_t2219098400_StaticFields, ___lcl_3)); }
	inline CharU5BU5D_t3528271667* get_lcl_3() const { return ___lcl_3; }
	inline CharU5BU5D_t3528271667** get_address_of_lcl_3() { return &___lcl_3; }
	inline void set_lcl_3(CharU5BU5D_t3528271667* value)
	{
		___lcl_3 = value;
		Il2CppCodeGenWriteBarrier(&___lcl_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
