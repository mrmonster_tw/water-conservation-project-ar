﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_M3561136435.h"

// System.ServiceModel.Channels.TextMessageEncodingBindingElement
struct TextMessageEncodingBindingElement_t2133327734;
// System.ServiceModel.Channels.TextMessageEncoder
struct TextMessageEncoder_t2992495264;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.TextMessageEncoderFactory
struct  TextMessageEncoderFactory_t3191039922  : public MessageEncoderFactory_t3561136435
{
public:
	// System.ServiceModel.Channels.TextMessageEncodingBindingElement System.ServiceModel.Channels.TextMessageEncoderFactory::owner
	TextMessageEncodingBindingElement_t2133327734 * ___owner_0;
	// System.ServiceModel.Channels.TextMessageEncoder System.ServiceModel.Channels.TextMessageEncoderFactory::encoder
	TextMessageEncoder_t2992495264 * ___encoder_1;

public:
	inline static int32_t get_offset_of_owner_0() { return static_cast<int32_t>(offsetof(TextMessageEncoderFactory_t3191039922, ___owner_0)); }
	inline TextMessageEncodingBindingElement_t2133327734 * get_owner_0() const { return ___owner_0; }
	inline TextMessageEncodingBindingElement_t2133327734 ** get_address_of_owner_0() { return &___owner_0; }
	inline void set_owner_0(TextMessageEncodingBindingElement_t2133327734 * value)
	{
		___owner_0 = value;
		Il2CppCodeGenWriteBarrier(&___owner_0, value);
	}

	inline static int32_t get_offset_of_encoder_1() { return static_cast<int32_t>(offsetof(TextMessageEncoderFactory_t3191039922, ___encoder_1)); }
	inline TextMessageEncoder_t2992495264 * get_encoder_1() const { return ___encoder_1; }
	inline TextMessageEncoder_t2992495264 ** get_address_of_encoder_1() { return &___encoder_1; }
	inline void set_encoder_1(TextMessageEncoder_t2992495264 * value)
	{
		___encoder_1 = value;
		Il2CppCodeGenWriteBarrier(&___encoder_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
