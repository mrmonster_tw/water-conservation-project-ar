﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_Collections_Generic_Syn3394450148.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t132545152;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.SynchronizedKeyedCollection`2<System.Object,System.Object>
struct  SynchronizedKeyedCollection_2_t697926863  : public SynchronizedCollection_1_t3394450148
{
public:
	// System.Collections.Generic.Dictionary`2<K,T> System.Collections.Generic.SynchronizedKeyedCollection`2::dict
	Dictionary_2_t132545152 * ___dict_2;

public:
	inline static int32_t get_offset_of_dict_2() { return static_cast<int32_t>(offsetof(SynchronizedKeyedCollection_2_t697926863, ___dict_2)); }
	inline Dictionary_2_t132545152 * get_dict_2() const { return ___dict_2; }
	inline Dictionary_2_t132545152 ** get_address_of_dict_2() { return &___dict_2; }
	inline void set_dict_2(Dictionary_2_t132545152 * value)
	{
		___dict_2 = value;
		Il2CppCodeGenWriteBarrier(&___dict_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
