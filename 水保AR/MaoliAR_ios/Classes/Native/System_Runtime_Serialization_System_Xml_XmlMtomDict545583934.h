﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Runtime_Serialization_System_Xml_XmlDiction1958650860.h"

// System.IO.TextWriter
struct TextWriter_t3478189236;
// System.Xml.XmlWriterSettings
struct XmlWriterSettings_t3314986516;
// System.Net.Mime.ContentType
struct ContentType_t768484892;
// System.Xml.XmlWriter
struct XmlWriter_t127905479;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlMtomDictionaryWriter
struct  XmlMtomDictionaryWriter_t545583934  : public XmlDictionaryWriter_t1958650860
{
public:
	// System.IO.TextWriter System.Xml.XmlMtomDictionaryWriter::writer
	TextWriter_t3478189236 * ___writer_3;
	// System.Xml.XmlWriterSettings System.Xml.XmlMtomDictionaryWriter::xml_writer_settings
	XmlWriterSettings_t3314986516 * ___xml_writer_settings_4;
	// System.Int32 System.Xml.XmlMtomDictionaryWriter::max_bytes
	int32_t ___max_bytes_5;
	// System.Boolean System.Xml.XmlMtomDictionaryWriter::write_headers
	bool ___write_headers_6;
	// System.Boolean System.Xml.XmlMtomDictionaryWriter::owns_stream
	bool ___owns_stream_7;
	// System.Net.Mime.ContentType System.Xml.XmlMtomDictionaryWriter::content_type
	ContentType_t768484892 * ___content_type_8;
	// System.Xml.XmlWriter System.Xml.XmlMtomDictionaryWriter::w
	XmlWriter_t127905479 * ___w_9;
	// System.Int32 System.Xml.XmlMtomDictionaryWriter::depth
	int32_t ___depth_10;
	// System.Int32 System.Xml.XmlMtomDictionaryWriter::section_count
	int32_t ___section_count_11;

public:
	inline static int32_t get_offset_of_writer_3() { return static_cast<int32_t>(offsetof(XmlMtomDictionaryWriter_t545583934, ___writer_3)); }
	inline TextWriter_t3478189236 * get_writer_3() const { return ___writer_3; }
	inline TextWriter_t3478189236 ** get_address_of_writer_3() { return &___writer_3; }
	inline void set_writer_3(TextWriter_t3478189236 * value)
	{
		___writer_3 = value;
		Il2CppCodeGenWriteBarrier(&___writer_3, value);
	}

	inline static int32_t get_offset_of_xml_writer_settings_4() { return static_cast<int32_t>(offsetof(XmlMtomDictionaryWriter_t545583934, ___xml_writer_settings_4)); }
	inline XmlWriterSettings_t3314986516 * get_xml_writer_settings_4() const { return ___xml_writer_settings_4; }
	inline XmlWriterSettings_t3314986516 ** get_address_of_xml_writer_settings_4() { return &___xml_writer_settings_4; }
	inline void set_xml_writer_settings_4(XmlWriterSettings_t3314986516 * value)
	{
		___xml_writer_settings_4 = value;
		Il2CppCodeGenWriteBarrier(&___xml_writer_settings_4, value);
	}

	inline static int32_t get_offset_of_max_bytes_5() { return static_cast<int32_t>(offsetof(XmlMtomDictionaryWriter_t545583934, ___max_bytes_5)); }
	inline int32_t get_max_bytes_5() const { return ___max_bytes_5; }
	inline int32_t* get_address_of_max_bytes_5() { return &___max_bytes_5; }
	inline void set_max_bytes_5(int32_t value)
	{
		___max_bytes_5 = value;
	}

	inline static int32_t get_offset_of_write_headers_6() { return static_cast<int32_t>(offsetof(XmlMtomDictionaryWriter_t545583934, ___write_headers_6)); }
	inline bool get_write_headers_6() const { return ___write_headers_6; }
	inline bool* get_address_of_write_headers_6() { return &___write_headers_6; }
	inline void set_write_headers_6(bool value)
	{
		___write_headers_6 = value;
	}

	inline static int32_t get_offset_of_owns_stream_7() { return static_cast<int32_t>(offsetof(XmlMtomDictionaryWriter_t545583934, ___owns_stream_7)); }
	inline bool get_owns_stream_7() const { return ___owns_stream_7; }
	inline bool* get_address_of_owns_stream_7() { return &___owns_stream_7; }
	inline void set_owns_stream_7(bool value)
	{
		___owns_stream_7 = value;
	}

	inline static int32_t get_offset_of_content_type_8() { return static_cast<int32_t>(offsetof(XmlMtomDictionaryWriter_t545583934, ___content_type_8)); }
	inline ContentType_t768484892 * get_content_type_8() const { return ___content_type_8; }
	inline ContentType_t768484892 ** get_address_of_content_type_8() { return &___content_type_8; }
	inline void set_content_type_8(ContentType_t768484892 * value)
	{
		___content_type_8 = value;
		Il2CppCodeGenWriteBarrier(&___content_type_8, value);
	}

	inline static int32_t get_offset_of_w_9() { return static_cast<int32_t>(offsetof(XmlMtomDictionaryWriter_t545583934, ___w_9)); }
	inline XmlWriter_t127905479 * get_w_9() const { return ___w_9; }
	inline XmlWriter_t127905479 ** get_address_of_w_9() { return &___w_9; }
	inline void set_w_9(XmlWriter_t127905479 * value)
	{
		___w_9 = value;
		Il2CppCodeGenWriteBarrier(&___w_9, value);
	}

	inline static int32_t get_offset_of_depth_10() { return static_cast<int32_t>(offsetof(XmlMtomDictionaryWriter_t545583934, ___depth_10)); }
	inline int32_t get_depth_10() const { return ___depth_10; }
	inline int32_t* get_address_of_depth_10() { return &___depth_10; }
	inline void set_depth_10(int32_t value)
	{
		___depth_10 = value;
	}

	inline static int32_t get_offset_of_section_count_11() { return static_cast<int32_t>(offsetof(XmlMtomDictionaryWriter_t545583934, ___section_count_11)); }
	inline int32_t get_section_count_11() const { return ___section_count_11; }
	inline int32_t* get_address_of_section_count_11() { return &___section_count_11; }
	inline void set_section_count_11(int32_t value)
	{
		___section_count_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
