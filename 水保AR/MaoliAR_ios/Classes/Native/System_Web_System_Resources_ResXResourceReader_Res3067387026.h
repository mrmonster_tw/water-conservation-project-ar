﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Resources.ResXResourceReader/ResXHeader
struct  ResXHeader_t3067387026  : public Il2CppObject
{
public:
	// System.String System.Resources.ResXResourceReader/ResXHeader::resMimeType
	String_t* ___resMimeType_0;
	// System.String System.Resources.ResXResourceReader/ResXHeader::reader
	String_t* ___reader_1;
	// System.String System.Resources.ResXResourceReader/ResXHeader::version
	String_t* ___version_2;
	// System.String System.Resources.ResXResourceReader/ResXHeader::writer
	String_t* ___writer_3;

public:
	inline static int32_t get_offset_of_resMimeType_0() { return static_cast<int32_t>(offsetof(ResXHeader_t3067387026, ___resMimeType_0)); }
	inline String_t* get_resMimeType_0() const { return ___resMimeType_0; }
	inline String_t** get_address_of_resMimeType_0() { return &___resMimeType_0; }
	inline void set_resMimeType_0(String_t* value)
	{
		___resMimeType_0 = value;
		Il2CppCodeGenWriteBarrier(&___resMimeType_0, value);
	}

	inline static int32_t get_offset_of_reader_1() { return static_cast<int32_t>(offsetof(ResXHeader_t3067387026, ___reader_1)); }
	inline String_t* get_reader_1() const { return ___reader_1; }
	inline String_t** get_address_of_reader_1() { return &___reader_1; }
	inline void set_reader_1(String_t* value)
	{
		___reader_1 = value;
		Il2CppCodeGenWriteBarrier(&___reader_1, value);
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(ResXHeader_t3067387026, ___version_2)); }
	inline String_t* get_version_2() const { return ___version_2; }
	inline String_t** get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(String_t* value)
	{
		___version_2 = value;
		Il2CppCodeGenWriteBarrier(&___version_2, value);
	}

	inline static int32_t get_offset_of_writer_3() { return static_cast<int32_t>(offsetof(ResXHeader_t3067387026, ___writer_3)); }
	inline String_t* get_writer_3() const { return ___writer_3; }
	inline String_t** get_address_of_writer_3() { return &___writer_3; }
	inline void set_writer_3(String_t* value)
	{
		___writer_3 = value;
		Il2CppCodeGenWriteBarrier(&___writer_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
