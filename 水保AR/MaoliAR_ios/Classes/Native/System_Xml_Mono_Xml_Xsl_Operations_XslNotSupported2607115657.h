﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Operations_XslCompiledElemen50593777.h"

// System.String
struct String_t;
// System.Collections.ArrayList
struct ArrayList_t2718874744;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslNotSupportedOperation
struct  XslNotSupportedOperation_t2607115657  : public XslCompiledElement_t50593777
{
public:
	// System.String Mono.Xml.Xsl.Operations.XslNotSupportedOperation::name
	String_t* ___name_3;
	// System.Collections.ArrayList Mono.Xml.Xsl.Operations.XslNotSupportedOperation::fallbacks
	ArrayList_t2718874744 * ___fallbacks_4;

public:
	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(XslNotSupportedOperation_t2607115657, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier(&___name_3, value);
	}

	inline static int32_t get_offset_of_fallbacks_4() { return static_cast<int32_t>(offsetof(XslNotSupportedOperation_t2607115657, ___fallbacks_4)); }
	inline ArrayList_t2718874744 * get_fallbacks_4() const { return ___fallbacks_4; }
	inline ArrayList_t2718874744 ** get_address_of_fallbacks_4() { return &___fallbacks_4; }
	inline void set_fallbacks_4(ArrayList_t2718874744 * value)
	{
		___fallbacks_4 = value;
		Il2CppCodeGenWriteBarrier(&___fallbacks_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
