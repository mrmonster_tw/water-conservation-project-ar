﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "AssemblyU2DCSharp_ExpiredComponets_NormalList1320820545.h"
#include "AssemblyU2DCSharp_ExpiredComponets_SureList2832800709.h"

// ExpiredComponets
struct ExpiredComponets_t4043685999;
// UnityEngine.UI.Text
struct Text_t1901882714;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExpiredComponets
struct  ExpiredComponets_t4043685999  : public MonoBehaviour_t3962482529
{
public:
	// ExpiredComponets/NormalList ExpiredComponets::normalList
	NormalList_t1320820545  ___normalList_3;
	// ExpiredComponets/SureList ExpiredComponets::sureList
	SureList_t2832800709  ___sureList_4;
	// UnityEngine.UI.Text ExpiredComponets::all_Number
	Text_t1901882714 * ___all_Number_9;

public:
	inline static int32_t get_offset_of_normalList_3() { return static_cast<int32_t>(offsetof(ExpiredComponets_t4043685999, ___normalList_3)); }
	inline NormalList_t1320820545  get_normalList_3() const { return ___normalList_3; }
	inline NormalList_t1320820545 * get_address_of_normalList_3() { return &___normalList_3; }
	inline void set_normalList_3(NormalList_t1320820545  value)
	{
		___normalList_3 = value;
	}

	inline static int32_t get_offset_of_sureList_4() { return static_cast<int32_t>(offsetof(ExpiredComponets_t4043685999, ___sureList_4)); }
	inline SureList_t2832800709  get_sureList_4() const { return ___sureList_4; }
	inline SureList_t2832800709 * get_address_of_sureList_4() { return &___sureList_4; }
	inline void set_sureList_4(SureList_t2832800709  value)
	{
		___sureList_4 = value;
	}

	inline static int32_t get_offset_of_all_Number_9() { return static_cast<int32_t>(offsetof(ExpiredComponets_t4043685999, ___all_Number_9)); }
	inline Text_t1901882714 * get_all_Number_9() const { return ___all_Number_9; }
	inline Text_t1901882714 ** get_address_of_all_Number_9() { return &___all_Number_9; }
	inline void set_all_Number_9(Text_t1901882714 * value)
	{
		___all_Number_9 = value;
		Il2CppCodeGenWriteBarrier(&___all_Number_9, value);
	}
};

struct ExpiredComponets_t4043685999_StaticFields
{
public:
	// ExpiredComponets ExpiredComponets::This
	ExpiredComponets_t4043685999 * ___This_2;

public:
	inline static int32_t get_offset_of_This_2() { return static_cast<int32_t>(offsetof(ExpiredComponets_t4043685999_StaticFields, ___This_2)); }
	inline ExpiredComponets_t4043685999 * get_This_2() const { return ___This_2; }
	inline ExpiredComponets_t4043685999 ** get_address_of_This_2() { return &___This_2; }
	inline void set_This_2(ExpiredComponets_t4043685999 * value)
	{
		___This_2 = value;
		Il2CppCodeGenWriteBarrier(&___This_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
