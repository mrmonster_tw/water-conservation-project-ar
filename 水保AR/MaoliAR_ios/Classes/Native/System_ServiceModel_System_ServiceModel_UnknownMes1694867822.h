﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_EventArgs3591816995.h"

// System.ServiceModel.Channels.Message
struct Message_t869514973;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.UnknownMessageReceivedEventArgs
struct  UnknownMessageReceivedEventArgs_t1694867822  : public EventArgs_t3591816995
{
public:
	// System.ServiceModel.Channels.Message System.ServiceModel.UnknownMessageReceivedEventArgs::message
	Message_t869514973 * ___message_1;

public:
	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(UnknownMessageReceivedEventArgs_t1694867822, ___message_1)); }
	inline Message_t869514973 * get_message_1() const { return ___message_1; }
	inline Message_t869514973 ** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(Message_t869514973 * value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier(&___message_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
