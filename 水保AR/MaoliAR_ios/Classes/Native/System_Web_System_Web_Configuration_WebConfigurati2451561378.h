﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Object
struct Il2CppObject;
// System.Threading.ReaderWriterLockSlim
struct ReaderWriterLockSlim_t1938317233;
// System.Configuration.Internal.IInternalConfigConfigurationFactory
struct IInternalConfigConfigurationFactory_t1509880797;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1968819495;
// System.Collections.Generic.Dictionary`2<System.String,System.DateTime>
struct Dictionary_2_t3523786084;
// System.Threading.Timer
struct Timer_t716671026;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Configuration.IConfigurationSystem
struct IConfigurationSystem_t1184708823;
// System.Web.Configuration.Web20DefaultConfig
struct Web20DefaultConfig_t163121754;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.WebConfigurationManager
struct  WebConfigurationManager_t2451561378  : public Il2CppObject
{
public:

public:
};

struct WebConfigurationManager_t2451561378_StaticFields
{
public:
	// System.Char[] System.Web.Configuration.WebConfigurationManager::pathTrimChars
	CharU5BU5D_t3528271667* ___pathTrimChars_0;
	// System.Object System.Web.Configuration.WebConfigurationManager::suppressAppReloadLock
	Il2CppObject * ___suppressAppReloadLock_1;
	// System.Object System.Web.Configuration.WebConfigurationManager::saveLocationsCacheLock
	Il2CppObject * ___saveLocationsCacheLock_2;
	// System.Threading.ReaderWriterLockSlim System.Web.Configuration.WebConfigurationManager::sectionCacheLock
	ReaderWriterLockSlim_t1938317233 * ___sectionCacheLock_3;
	// System.Configuration.Internal.IInternalConfigConfigurationFactory System.Web.Configuration.WebConfigurationManager::configFactory
	Il2CppObject * ___configFactory_4;
	// System.Collections.Hashtable System.Web.Configuration.WebConfigurationManager::configurations
	Hashtable_t1853889766 * ___configurations_5;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Object> System.Web.Configuration.WebConfigurationManager::sectionCache
	Dictionary_2_t1968819495 * ___sectionCache_6;
	// System.Collections.Hashtable System.Web.Configuration.WebConfigurationManager::configPaths
	Hashtable_t1853889766 * ___configPaths_7;
	// System.Boolean System.Web.Configuration.WebConfigurationManager::suppressAppReload
	bool ___suppressAppReload_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.DateTime> System.Web.Configuration.WebConfigurationManager::saveLocationsCache
	Dictionary_2_t3523786084 * ___saveLocationsCache_9;
	// System.Threading.Timer System.Web.Configuration.WebConfigurationManager::saveLocationsTimer
	Timer_t716671026 * ___saveLocationsTimer_10;
	// System.Collections.ArrayList System.Web.Configuration.WebConfigurationManager::extra_assemblies
	ArrayList_t2718874744 * ___extra_assemblies_11;
	// System.Boolean System.Web.Configuration.WebConfigurationManager::hasConfigErrors
	bool ___hasConfigErrors_12;
	// System.Object System.Web.Configuration.WebConfigurationManager::hasConfigErrorsLock
	Il2CppObject * ___hasConfigErrorsLock_13;
	// System.Reflection.MethodInfo System.Web.Configuration.WebConfigurationManager::get_runtime_object
	MethodInfo_t * ___get_runtime_object_14;
	// System.Configuration.IConfigurationSystem System.Web.Configuration.WebConfigurationManager::oldConfig
	Il2CppObject * ___oldConfig_15;
	// System.Web.Configuration.Web20DefaultConfig System.Web.Configuration.WebConfigurationManager::config
	Web20DefaultConfig_t163121754 * ___config_16;
	// System.Object System.Web.Configuration.WebConfigurationManager::lockobj
	Il2CppObject * ___lockobj_17;

public:
	inline static int32_t get_offset_of_pathTrimChars_0() { return static_cast<int32_t>(offsetof(WebConfigurationManager_t2451561378_StaticFields, ___pathTrimChars_0)); }
	inline CharU5BU5D_t3528271667* get_pathTrimChars_0() const { return ___pathTrimChars_0; }
	inline CharU5BU5D_t3528271667** get_address_of_pathTrimChars_0() { return &___pathTrimChars_0; }
	inline void set_pathTrimChars_0(CharU5BU5D_t3528271667* value)
	{
		___pathTrimChars_0 = value;
		Il2CppCodeGenWriteBarrier(&___pathTrimChars_0, value);
	}

	inline static int32_t get_offset_of_suppressAppReloadLock_1() { return static_cast<int32_t>(offsetof(WebConfigurationManager_t2451561378_StaticFields, ___suppressAppReloadLock_1)); }
	inline Il2CppObject * get_suppressAppReloadLock_1() const { return ___suppressAppReloadLock_1; }
	inline Il2CppObject ** get_address_of_suppressAppReloadLock_1() { return &___suppressAppReloadLock_1; }
	inline void set_suppressAppReloadLock_1(Il2CppObject * value)
	{
		___suppressAppReloadLock_1 = value;
		Il2CppCodeGenWriteBarrier(&___suppressAppReloadLock_1, value);
	}

	inline static int32_t get_offset_of_saveLocationsCacheLock_2() { return static_cast<int32_t>(offsetof(WebConfigurationManager_t2451561378_StaticFields, ___saveLocationsCacheLock_2)); }
	inline Il2CppObject * get_saveLocationsCacheLock_2() const { return ___saveLocationsCacheLock_2; }
	inline Il2CppObject ** get_address_of_saveLocationsCacheLock_2() { return &___saveLocationsCacheLock_2; }
	inline void set_saveLocationsCacheLock_2(Il2CppObject * value)
	{
		___saveLocationsCacheLock_2 = value;
		Il2CppCodeGenWriteBarrier(&___saveLocationsCacheLock_2, value);
	}

	inline static int32_t get_offset_of_sectionCacheLock_3() { return static_cast<int32_t>(offsetof(WebConfigurationManager_t2451561378_StaticFields, ___sectionCacheLock_3)); }
	inline ReaderWriterLockSlim_t1938317233 * get_sectionCacheLock_3() const { return ___sectionCacheLock_3; }
	inline ReaderWriterLockSlim_t1938317233 ** get_address_of_sectionCacheLock_3() { return &___sectionCacheLock_3; }
	inline void set_sectionCacheLock_3(ReaderWriterLockSlim_t1938317233 * value)
	{
		___sectionCacheLock_3 = value;
		Il2CppCodeGenWriteBarrier(&___sectionCacheLock_3, value);
	}

	inline static int32_t get_offset_of_configFactory_4() { return static_cast<int32_t>(offsetof(WebConfigurationManager_t2451561378_StaticFields, ___configFactory_4)); }
	inline Il2CppObject * get_configFactory_4() const { return ___configFactory_4; }
	inline Il2CppObject ** get_address_of_configFactory_4() { return &___configFactory_4; }
	inline void set_configFactory_4(Il2CppObject * value)
	{
		___configFactory_4 = value;
		Il2CppCodeGenWriteBarrier(&___configFactory_4, value);
	}

	inline static int32_t get_offset_of_configurations_5() { return static_cast<int32_t>(offsetof(WebConfigurationManager_t2451561378_StaticFields, ___configurations_5)); }
	inline Hashtable_t1853889766 * get_configurations_5() const { return ___configurations_5; }
	inline Hashtable_t1853889766 ** get_address_of_configurations_5() { return &___configurations_5; }
	inline void set_configurations_5(Hashtable_t1853889766 * value)
	{
		___configurations_5 = value;
		Il2CppCodeGenWriteBarrier(&___configurations_5, value);
	}

	inline static int32_t get_offset_of_sectionCache_6() { return static_cast<int32_t>(offsetof(WebConfigurationManager_t2451561378_StaticFields, ___sectionCache_6)); }
	inline Dictionary_2_t1968819495 * get_sectionCache_6() const { return ___sectionCache_6; }
	inline Dictionary_2_t1968819495 ** get_address_of_sectionCache_6() { return &___sectionCache_6; }
	inline void set_sectionCache_6(Dictionary_2_t1968819495 * value)
	{
		___sectionCache_6 = value;
		Il2CppCodeGenWriteBarrier(&___sectionCache_6, value);
	}

	inline static int32_t get_offset_of_configPaths_7() { return static_cast<int32_t>(offsetof(WebConfigurationManager_t2451561378_StaticFields, ___configPaths_7)); }
	inline Hashtable_t1853889766 * get_configPaths_7() const { return ___configPaths_7; }
	inline Hashtable_t1853889766 ** get_address_of_configPaths_7() { return &___configPaths_7; }
	inline void set_configPaths_7(Hashtable_t1853889766 * value)
	{
		___configPaths_7 = value;
		Il2CppCodeGenWriteBarrier(&___configPaths_7, value);
	}

	inline static int32_t get_offset_of_suppressAppReload_8() { return static_cast<int32_t>(offsetof(WebConfigurationManager_t2451561378_StaticFields, ___suppressAppReload_8)); }
	inline bool get_suppressAppReload_8() const { return ___suppressAppReload_8; }
	inline bool* get_address_of_suppressAppReload_8() { return &___suppressAppReload_8; }
	inline void set_suppressAppReload_8(bool value)
	{
		___suppressAppReload_8 = value;
	}

	inline static int32_t get_offset_of_saveLocationsCache_9() { return static_cast<int32_t>(offsetof(WebConfigurationManager_t2451561378_StaticFields, ___saveLocationsCache_9)); }
	inline Dictionary_2_t3523786084 * get_saveLocationsCache_9() const { return ___saveLocationsCache_9; }
	inline Dictionary_2_t3523786084 ** get_address_of_saveLocationsCache_9() { return &___saveLocationsCache_9; }
	inline void set_saveLocationsCache_9(Dictionary_2_t3523786084 * value)
	{
		___saveLocationsCache_9 = value;
		Il2CppCodeGenWriteBarrier(&___saveLocationsCache_9, value);
	}

	inline static int32_t get_offset_of_saveLocationsTimer_10() { return static_cast<int32_t>(offsetof(WebConfigurationManager_t2451561378_StaticFields, ___saveLocationsTimer_10)); }
	inline Timer_t716671026 * get_saveLocationsTimer_10() const { return ___saveLocationsTimer_10; }
	inline Timer_t716671026 ** get_address_of_saveLocationsTimer_10() { return &___saveLocationsTimer_10; }
	inline void set_saveLocationsTimer_10(Timer_t716671026 * value)
	{
		___saveLocationsTimer_10 = value;
		Il2CppCodeGenWriteBarrier(&___saveLocationsTimer_10, value);
	}

	inline static int32_t get_offset_of_extra_assemblies_11() { return static_cast<int32_t>(offsetof(WebConfigurationManager_t2451561378_StaticFields, ___extra_assemblies_11)); }
	inline ArrayList_t2718874744 * get_extra_assemblies_11() const { return ___extra_assemblies_11; }
	inline ArrayList_t2718874744 ** get_address_of_extra_assemblies_11() { return &___extra_assemblies_11; }
	inline void set_extra_assemblies_11(ArrayList_t2718874744 * value)
	{
		___extra_assemblies_11 = value;
		Il2CppCodeGenWriteBarrier(&___extra_assemblies_11, value);
	}

	inline static int32_t get_offset_of_hasConfigErrors_12() { return static_cast<int32_t>(offsetof(WebConfigurationManager_t2451561378_StaticFields, ___hasConfigErrors_12)); }
	inline bool get_hasConfigErrors_12() const { return ___hasConfigErrors_12; }
	inline bool* get_address_of_hasConfigErrors_12() { return &___hasConfigErrors_12; }
	inline void set_hasConfigErrors_12(bool value)
	{
		___hasConfigErrors_12 = value;
	}

	inline static int32_t get_offset_of_hasConfigErrorsLock_13() { return static_cast<int32_t>(offsetof(WebConfigurationManager_t2451561378_StaticFields, ___hasConfigErrorsLock_13)); }
	inline Il2CppObject * get_hasConfigErrorsLock_13() const { return ___hasConfigErrorsLock_13; }
	inline Il2CppObject ** get_address_of_hasConfigErrorsLock_13() { return &___hasConfigErrorsLock_13; }
	inline void set_hasConfigErrorsLock_13(Il2CppObject * value)
	{
		___hasConfigErrorsLock_13 = value;
		Il2CppCodeGenWriteBarrier(&___hasConfigErrorsLock_13, value);
	}

	inline static int32_t get_offset_of_get_runtime_object_14() { return static_cast<int32_t>(offsetof(WebConfigurationManager_t2451561378_StaticFields, ___get_runtime_object_14)); }
	inline MethodInfo_t * get_get_runtime_object_14() const { return ___get_runtime_object_14; }
	inline MethodInfo_t ** get_address_of_get_runtime_object_14() { return &___get_runtime_object_14; }
	inline void set_get_runtime_object_14(MethodInfo_t * value)
	{
		___get_runtime_object_14 = value;
		Il2CppCodeGenWriteBarrier(&___get_runtime_object_14, value);
	}

	inline static int32_t get_offset_of_oldConfig_15() { return static_cast<int32_t>(offsetof(WebConfigurationManager_t2451561378_StaticFields, ___oldConfig_15)); }
	inline Il2CppObject * get_oldConfig_15() const { return ___oldConfig_15; }
	inline Il2CppObject ** get_address_of_oldConfig_15() { return &___oldConfig_15; }
	inline void set_oldConfig_15(Il2CppObject * value)
	{
		___oldConfig_15 = value;
		Il2CppCodeGenWriteBarrier(&___oldConfig_15, value);
	}

	inline static int32_t get_offset_of_config_16() { return static_cast<int32_t>(offsetof(WebConfigurationManager_t2451561378_StaticFields, ___config_16)); }
	inline Web20DefaultConfig_t163121754 * get_config_16() const { return ___config_16; }
	inline Web20DefaultConfig_t163121754 ** get_address_of_config_16() { return &___config_16; }
	inline void set_config_16(Web20DefaultConfig_t163121754 * value)
	{
		___config_16 = value;
		Il2CppCodeGenWriteBarrier(&___config_16, value);
	}

	inline static int32_t get_offset_of_lockobj_17() { return static_cast<int32_t>(offsetof(WebConfigurationManager_t2451561378_StaticFields, ___lockobj_17)); }
	inline Il2CppObject * get_lockobj_17() const { return ___lockobj_17; }
	inline Il2CppObject ** get_address_of_lockobj_17() { return &___lockobj_17; }
	inline void set_lockobj_17(Il2CppObject * value)
	{
		___lockobj_17 = value;
		Il2CppCodeGenWriteBarrier(&___lockobj_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
