﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.MsmqTransportSecurityElement
struct  MsmqTransportSecurityElement_t2400348769  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct MsmqTransportSecurityElement_t2400348769_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.MsmqTransportSecurityElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.MsmqTransportSecurityElement::msmq_authentication_mode
	ConfigurationProperty_t3590861854 * ___msmq_authentication_mode_14;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.MsmqTransportSecurityElement::msmq_encryption_algorithm
	ConfigurationProperty_t3590861854 * ___msmq_encryption_algorithm_15;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.MsmqTransportSecurityElement::msmq_protection_level
	ConfigurationProperty_t3590861854 * ___msmq_protection_level_16;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.MsmqTransportSecurityElement::msmq_secure_hash_algorithm
	ConfigurationProperty_t3590861854 * ___msmq_secure_hash_algorithm_17;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(MsmqTransportSecurityElement_t2400348769_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier(&___properties_13, value);
	}

	inline static int32_t get_offset_of_msmq_authentication_mode_14() { return static_cast<int32_t>(offsetof(MsmqTransportSecurityElement_t2400348769_StaticFields, ___msmq_authentication_mode_14)); }
	inline ConfigurationProperty_t3590861854 * get_msmq_authentication_mode_14() const { return ___msmq_authentication_mode_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_msmq_authentication_mode_14() { return &___msmq_authentication_mode_14; }
	inline void set_msmq_authentication_mode_14(ConfigurationProperty_t3590861854 * value)
	{
		___msmq_authentication_mode_14 = value;
		Il2CppCodeGenWriteBarrier(&___msmq_authentication_mode_14, value);
	}

	inline static int32_t get_offset_of_msmq_encryption_algorithm_15() { return static_cast<int32_t>(offsetof(MsmqTransportSecurityElement_t2400348769_StaticFields, ___msmq_encryption_algorithm_15)); }
	inline ConfigurationProperty_t3590861854 * get_msmq_encryption_algorithm_15() const { return ___msmq_encryption_algorithm_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_msmq_encryption_algorithm_15() { return &___msmq_encryption_algorithm_15; }
	inline void set_msmq_encryption_algorithm_15(ConfigurationProperty_t3590861854 * value)
	{
		___msmq_encryption_algorithm_15 = value;
		Il2CppCodeGenWriteBarrier(&___msmq_encryption_algorithm_15, value);
	}

	inline static int32_t get_offset_of_msmq_protection_level_16() { return static_cast<int32_t>(offsetof(MsmqTransportSecurityElement_t2400348769_StaticFields, ___msmq_protection_level_16)); }
	inline ConfigurationProperty_t3590861854 * get_msmq_protection_level_16() const { return ___msmq_protection_level_16; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_msmq_protection_level_16() { return &___msmq_protection_level_16; }
	inline void set_msmq_protection_level_16(ConfigurationProperty_t3590861854 * value)
	{
		___msmq_protection_level_16 = value;
		Il2CppCodeGenWriteBarrier(&___msmq_protection_level_16, value);
	}

	inline static int32_t get_offset_of_msmq_secure_hash_algorithm_17() { return static_cast<int32_t>(offsetof(MsmqTransportSecurityElement_t2400348769_StaticFields, ___msmq_secure_hash_algorithm_17)); }
	inline ConfigurationProperty_t3590861854 * get_msmq_secure_hash_algorithm_17() const { return ___msmq_secure_hash_algorithm_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_msmq_secure_hash_algorithm_17() { return &___msmq_secure_hash_algorithm_17; }
	inline void set_msmq_secure_hash_algorithm_17(ConfigurationProperty_t3590861854 * value)
	{
		___msmq_secure_hash_algorithm_17 = value;
		Il2CppCodeGenWriteBarrier(&___msmq_secure_hash_algorithm_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
