﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Channels.IReplyChannel
struct IReplyChannel_t3856619590;
// System.ServiceModel.Channels.TcpChannelListener`1<System.ServiceModel.Channels.IReplyChannel>
struct TcpChannelListener_1_t2995356293;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.TcpChannelListener`1/<OnAcceptChannel>c__AnonStorey12<System.ServiceModel.Channels.IReplyChannel>
struct  U3COnAcceptChannelU3Ec__AnonStorey12_t748475310  : public Il2CppObject
{
public:
	// TChannel System.ServiceModel.Channels.TcpChannelListener`1/<OnAcceptChannel>c__AnonStorey12::ch
	Il2CppObject * ___ch_0;
	// System.ServiceModel.Channels.TcpChannelListener`1<TChannel> System.ServiceModel.Channels.TcpChannelListener`1/<OnAcceptChannel>c__AnonStorey12::<>f__this
	TcpChannelListener_1_t2995356293 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_ch_0() { return static_cast<int32_t>(offsetof(U3COnAcceptChannelU3Ec__AnonStorey12_t748475310, ___ch_0)); }
	inline Il2CppObject * get_ch_0() const { return ___ch_0; }
	inline Il2CppObject ** get_address_of_ch_0() { return &___ch_0; }
	inline void set_ch_0(Il2CppObject * value)
	{
		___ch_0 = value;
		Il2CppCodeGenWriteBarrier(&___ch_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3COnAcceptChannelU3Ec__AnonStorey12_t748475310, ___U3CU3Ef__this_1)); }
	inline TcpChannelListener_1_t2995356293 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline TcpChannelListener_1_t2995356293 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(TcpChannelListener_1_t2995356293 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
