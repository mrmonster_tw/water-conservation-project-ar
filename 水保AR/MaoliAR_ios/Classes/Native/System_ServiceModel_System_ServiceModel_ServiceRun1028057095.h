﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Co518829156.h"
#include "mscorlib_System_TimeSpan881159249.h"

// System.ServiceModel.Channels.IChannel
struct IChannel_t937207545;
// System.ServiceModel.Dispatcher.DispatchRuntime
struct DispatchRuntime_t796075230;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.ServiceRuntimeChannel
struct  ServiceRuntimeChannel_t1028057095  : public CommunicationObject_t518829156
{
public:
	// System.ServiceModel.Channels.IChannel System.ServiceModel.ServiceRuntimeChannel::channel
	Il2CppObject * ___channel_9;
	// System.ServiceModel.Dispatcher.DispatchRuntime System.ServiceModel.ServiceRuntimeChannel::runtime
	DispatchRuntime_t796075230 * ___runtime_10;
	// System.TimeSpan System.ServiceModel.ServiceRuntimeChannel::<OperationTimeout>k__BackingField
	TimeSpan_t881159249  ___U3COperationTimeoutU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_channel_9() { return static_cast<int32_t>(offsetof(ServiceRuntimeChannel_t1028057095, ___channel_9)); }
	inline Il2CppObject * get_channel_9() const { return ___channel_9; }
	inline Il2CppObject ** get_address_of_channel_9() { return &___channel_9; }
	inline void set_channel_9(Il2CppObject * value)
	{
		___channel_9 = value;
		Il2CppCodeGenWriteBarrier(&___channel_9, value);
	}

	inline static int32_t get_offset_of_runtime_10() { return static_cast<int32_t>(offsetof(ServiceRuntimeChannel_t1028057095, ___runtime_10)); }
	inline DispatchRuntime_t796075230 * get_runtime_10() const { return ___runtime_10; }
	inline DispatchRuntime_t796075230 ** get_address_of_runtime_10() { return &___runtime_10; }
	inline void set_runtime_10(DispatchRuntime_t796075230 * value)
	{
		___runtime_10 = value;
		Il2CppCodeGenWriteBarrier(&___runtime_10, value);
	}

	inline static int32_t get_offset_of_U3COperationTimeoutU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ServiceRuntimeChannel_t1028057095, ___U3COperationTimeoutU3Ek__BackingField_11)); }
	inline TimeSpan_t881159249  get_U3COperationTimeoutU3Ek__BackingField_11() const { return ___U3COperationTimeoutU3Ek__BackingField_11; }
	inline TimeSpan_t881159249 * get_address_of_U3COperationTimeoutU3Ek__BackingField_11() { return &___U3COperationTimeoutU3Ek__BackingField_11; }
	inline void set_U3COperationTimeoutU3Ek__BackingField_11(TimeSpan_t881159249  value)
	{
		___U3COperationTimeoutU3Ek__BackingField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
