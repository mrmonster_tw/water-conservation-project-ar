﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Generic.SortedList`2<System.String,System.String>
struct SortedList_2_t2974774816;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.SortedList`2/ListKeys<System.String,System.String>
struct  ListKeys_t1548240357  : public Il2CppObject
{
public:
	// System.Collections.Generic.SortedList`2<TKey,TValue> System.Collections.Generic.SortedList`2/ListKeys::host
	SortedList_2_t2974774816 * ___host_0;

public:
	inline static int32_t get_offset_of_host_0() { return static_cast<int32_t>(offsetof(ListKeys_t1548240357, ___host_0)); }
	inline SortedList_2_t2974774816 * get_host_0() const { return ___host_0; }
	inline SortedList_2_t2974774816 ** get_address_of_host_0() { return &___host_0; }
	inline void set_host_0(SortedList_2_t2974774816 * value)
	{
		___host_0 = value;
		Il2CppCodeGenWriteBarrier(&___host_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
