﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Attribute
struct  Attribute_t270895445 
{
public:
	// System.String Mono.Xml.Xsl.Attribute::Prefix
	String_t* ___Prefix_0;
	// System.String Mono.Xml.Xsl.Attribute::Namespace
	String_t* ___Namespace_1;
	// System.String Mono.Xml.Xsl.Attribute::LocalName
	String_t* ___LocalName_2;
	// System.String Mono.Xml.Xsl.Attribute::Value
	String_t* ___Value_3;

public:
	inline static int32_t get_offset_of_Prefix_0() { return static_cast<int32_t>(offsetof(Attribute_t270895445, ___Prefix_0)); }
	inline String_t* get_Prefix_0() const { return ___Prefix_0; }
	inline String_t** get_address_of_Prefix_0() { return &___Prefix_0; }
	inline void set_Prefix_0(String_t* value)
	{
		___Prefix_0 = value;
		Il2CppCodeGenWriteBarrier(&___Prefix_0, value);
	}

	inline static int32_t get_offset_of_Namespace_1() { return static_cast<int32_t>(offsetof(Attribute_t270895445, ___Namespace_1)); }
	inline String_t* get_Namespace_1() const { return ___Namespace_1; }
	inline String_t** get_address_of_Namespace_1() { return &___Namespace_1; }
	inline void set_Namespace_1(String_t* value)
	{
		___Namespace_1 = value;
		Il2CppCodeGenWriteBarrier(&___Namespace_1, value);
	}

	inline static int32_t get_offset_of_LocalName_2() { return static_cast<int32_t>(offsetof(Attribute_t270895445, ___LocalName_2)); }
	inline String_t* get_LocalName_2() const { return ___LocalName_2; }
	inline String_t** get_address_of_LocalName_2() { return &___LocalName_2; }
	inline void set_LocalName_2(String_t* value)
	{
		___LocalName_2 = value;
		Il2CppCodeGenWriteBarrier(&___LocalName_2, value);
	}

	inline static int32_t get_offset_of_Value_3() { return static_cast<int32_t>(offsetof(Attribute_t270895445, ___Value_3)); }
	inline String_t* get_Value_3() const { return ___Value_3; }
	inline String_t** get_address_of_Value_3() { return &___Value_3; }
	inline void set_Value_3(String_t* value)
	{
		___Value_3 = value;
		Il2CppCodeGenWriteBarrier(&___Value_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Mono.Xml.Xsl.Attribute
struct Attribute_t270895445_marshaled_pinvoke
{
	char* ___Prefix_0;
	char* ___Namespace_1;
	char* ___LocalName_2;
	char* ___Value_3;
};
// Native definition for COM marshalling of Mono.Xml.Xsl.Attribute
struct Attribute_t270895445_marshaled_com
{
	Il2CppChar* ___Prefix_0;
	Il2CppChar* ___Namespace_1;
	Il2CppChar* ___LocalName_2;
	Il2CppChar* ___Value_3;
};
