﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Hashtable
struct Hashtable_t1853889766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.ObjectStateFormatter/WriterContext
struct  WriterContext_t4048875981  : public Il2CppObject
{
public:
	// System.Collections.Hashtable System.Web.UI.ObjectStateFormatter/WriterContext::cache
	Hashtable_t1853889766 * ___cache_0;
	// System.Int16 System.Web.UI.ObjectStateFormatter/WriterContext::nextKey
	int16_t ___nextKey_1;
	// System.Int16 System.Web.UI.ObjectStateFormatter/WriterContext::key
	int16_t ___key_2;

public:
	inline static int32_t get_offset_of_cache_0() { return static_cast<int32_t>(offsetof(WriterContext_t4048875981, ___cache_0)); }
	inline Hashtable_t1853889766 * get_cache_0() const { return ___cache_0; }
	inline Hashtable_t1853889766 ** get_address_of_cache_0() { return &___cache_0; }
	inline void set_cache_0(Hashtable_t1853889766 * value)
	{
		___cache_0 = value;
		Il2CppCodeGenWriteBarrier(&___cache_0, value);
	}

	inline static int32_t get_offset_of_nextKey_1() { return static_cast<int32_t>(offsetof(WriterContext_t4048875981, ___nextKey_1)); }
	inline int16_t get_nextKey_1() const { return ___nextKey_1; }
	inline int16_t* get_address_of_nextKey_1() { return &___nextKey_1; }
	inline void set_nextKey_1(int16_t value)
	{
		___nextKey_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(WriterContext_t4048875981, ___key_2)); }
	inline int16_t get_key_2() const { return ___key_2; }
	inline int16_t* get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(int16_t value)
	{
		___key_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
