﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3651253610.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3353960863.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3242499063.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255375  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=32 <PrivateImplementationDetails>::$field-68F00C41318114691E02CD7532ACF69A8DBE23C2
	U24ArrayTypeU3D32_t3651253610  ___U24fieldU2D68F00C41318114691E02CD7532ACF69A8DBE23C2_0;
	// <PrivateImplementationDetails>/$ArrayType=580 <PrivateImplementationDetails>::$field-7FB9790B49277F6151D3EB5D555CCF105904DB43
	U24ArrayTypeU3D580_t353960863  ___U24fieldU2D7FB9790B49277F6151D3EB5D555CCF105904DB43_1;
	// <PrivateImplementationDetails>/$ArrayType=8 <PrivateImplementationDetails>::$field-D26A27B5531D6252D57917C90488F9C3F7AF8F98
	U24ArrayTypeU3D8_t3242499063  ___U24fieldU2DD26A27B5531D6252D57917C90488F9C3F7AF8F98_2;

public:
	inline static int32_t get_offset_of_U24fieldU2D68F00C41318114691E02CD7532ACF69A8DBE23C2_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields, ___U24fieldU2D68F00C41318114691E02CD7532ACF69A8DBE23C2_0)); }
	inline U24ArrayTypeU3D32_t3651253610  get_U24fieldU2D68F00C41318114691E02CD7532ACF69A8DBE23C2_0() const { return ___U24fieldU2D68F00C41318114691E02CD7532ACF69A8DBE23C2_0; }
	inline U24ArrayTypeU3D32_t3651253610 * get_address_of_U24fieldU2D68F00C41318114691E02CD7532ACF69A8DBE23C2_0() { return &___U24fieldU2D68F00C41318114691E02CD7532ACF69A8DBE23C2_0; }
	inline void set_U24fieldU2D68F00C41318114691E02CD7532ACF69A8DBE23C2_0(U24ArrayTypeU3D32_t3651253610  value)
	{
		___U24fieldU2D68F00C41318114691E02CD7532ACF69A8DBE23C2_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D7FB9790B49277F6151D3EB5D555CCF105904DB43_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields, ___U24fieldU2D7FB9790B49277F6151D3EB5D555CCF105904DB43_1)); }
	inline U24ArrayTypeU3D580_t353960863  get_U24fieldU2D7FB9790B49277F6151D3EB5D555CCF105904DB43_1() const { return ___U24fieldU2D7FB9790B49277F6151D3EB5D555CCF105904DB43_1; }
	inline U24ArrayTypeU3D580_t353960863 * get_address_of_U24fieldU2D7FB9790B49277F6151D3EB5D555CCF105904DB43_1() { return &___U24fieldU2D7FB9790B49277F6151D3EB5D555CCF105904DB43_1; }
	inline void set_U24fieldU2D7FB9790B49277F6151D3EB5D555CCF105904DB43_1(U24ArrayTypeU3D580_t353960863  value)
	{
		___U24fieldU2D7FB9790B49277F6151D3EB5D555CCF105904DB43_1 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD26A27B5531D6252D57917C90488F9C3F7AF8F98_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields, ___U24fieldU2DD26A27B5531D6252D57917C90488F9C3F7AF8F98_2)); }
	inline U24ArrayTypeU3D8_t3242499063  get_U24fieldU2DD26A27B5531D6252D57917C90488F9C3F7AF8F98_2() const { return ___U24fieldU2DD26A27B5531D6252D57917C90488F9C3F7AF8F98_2; }
	inline U24ArrayTypeU3D8_t3242499063 * get_address_of_U24fieldU2DD26A27B5531D6252D57917C90488F9C3F7AF8F98_2() { return &___U24fieldU2DD26A27B5531D6252D57917C90488F9C3F7AF8F98_2; }
	inline void set_U24fieldU2DD26A27B5531D6252D57917C90488F9C3F7AF8F98_2(U24ArrayTypeU3D8_t3242499063  value)
	{
		___U24fieldU2DD26A27B5531D6252D57917C90488F9C3F7AF8F98_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
