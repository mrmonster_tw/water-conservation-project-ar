﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Runtime_CompilerServices_Extens1723066603.h"
#include "System_Core_Locale4128636107.h"
#include "System_Core_System_MonoTODOAttribute4131080581.h"
#include "System_Core_System_MonoNotSupportedAttribute2563528020.h"
#include "System_Core_Mono_Security_Cryptography_KeyBuilder2049230354.h"
#include "System_Core_Mono_Security_Cryptography_SymmetricTr3802591842.h"
#include "System_Core_System_Linq_Check192468399.h"
#include "System_Core_System_Linq_Enumerable538148348.h"
#include "System_Core_System_Linq_Enumerable_Fallback3495999270.h"
#include "System_Core_System_Security_Cryptography_Aes1218282760.h"
#include "System_Core_System_Security_Cryptography_AesManage1129950597.h"
#include "System_Core_System_Security_Cryptography_AesCryptoS345478893.h"
#include "System_Core_System_Security_Cryptography_AesTransf2957123611.h"
#include "System_Core_System_Threading_LockRecursionExceptio2413788283.h"
#include "System_Core_System_Threading_ReaderWriterLockSlim1938317233.h"
#include "System_Core_System_Threading_ReaderWriterLockSlim_2068764019.h"
#include "System_Core_Microsoft_Win32_SafeHandles_SafePipeHa1989113880.h"
#include "System_Core_System_IO_Pipes_NamedPipeServerStream125618231.h"
#include "System_Core_System_IO_Pipes_PipeAccessRights4039352439.h"
#include "System_Core_System_IO_Pipes_PipeDirection2244332536.h"
#include "System_Core_System_IO_Pipes_PipeOptions3458221681.h"
#include "System_Core_System_IO_Pipes_PipeSecurity3265965420.h"
#include "System_Core_System_IO_Pipes_PipeStream871053121.h"
#include "System_Core_System_IO_Pipes_PipeTransmissionMode3285869980.h"
#include "System_Core_System_IO_Pipes_UnixNamedPipe2806256776.h"
#include "System_Core_System_IO_Pipes_UnixNamedPipeServer2839746932.h"
#include "System_Core_System_IO_Pipes_Win32NamedPipe1933459707.h"
#include "System_Core_System_IO_Pipes_Win32NamedPipeServer774293249.h"
#include "System_Core_System_IO_Pipes_SecurityAttributesHack2445122734.h"
#include "System_Core_System_IO_Pipes_Win32Marshal3620203595.h"
#include "System_Core_System_IO_HandleInheritability208753816.h"
#include "System_Core_System_Action1264377477.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E3057255361.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U241950429485.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U244289081651.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U241929481982.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U243907531057.h"
#include "System_Drawing_U3CModuleU3E692745525.h"
#include "System_Drawing_Locale4128636107.h"
#include "System_Drawing_System_Drawing_Bitmap3858959443.h"
#include "System_Drawing_System_Drawing_Carbon3931103330.h"
#include "System_Drawing_System_Drawing_CarbonContext3570584123.h"
#include "System_Drawing_System_Drawing_ColorConverter2126274855.h"
#include "System_Drawing_System_Drawing_ColorConverter_Compa2817450177.h"
#include "System_Drawing_System_Drawing_Color1869934208.h"
#include "System_Drawing_System_Drawing_ColorTranslator1362899606.h"
#include "System_Drawing_System_Drawing_ComIStreamMarshaler1972832677.h"
#include "System_Drawing_System_Drawing_ComIStreamWrapper1385077908.h"
#include "System_Drawing_System_Drawing_Font3402197101.h"
#include "System_Drawing_System_Drawing_FontConverter2762192330.h"
#include "System_Drawing_System_Drawing_FontConverter_FontNa3052059787.h"
#include "System_Drawing_System_Drawing_FontConverter_FontUn3646088898.h"
#include "System_Drawing_System_Drawing_FontFamily13778311.h"
#include "System_Drawing_System_Drawing_FontStyle2978677421.h"
#include "System_Drawing_System_Drawing_Graphics2557146384.h"
#include "System_Drawing_System_Drawing_GraphicsUnit4033181596.h"
#include "System_Drawing_System_Drawing_Status3438243046.h"
#include "System_Drawing_System_Drawing_ImageType223047490.h"
#include "System_Drawing_System_Drawing_GetSysColorIndex869702789.h"
#include "System_Drawing_System_Drawing_GDIPlus2533009916.h"
#include "System_Drawing_System_Drawing_GDIPlus_GdiPlusStrea2754160014.h"
#include "System_Drawing_System_Drawing_GDIPlus_StreamGetHead558280091.h"
#include "System_Drawing_System_Drawing_GDIPlus_StreamGetByte482941147.h"
#include "System_Drawing_System_Drawing_GDIPlus_StreamSeekDe2972861213.h"
#include "System_Drawing_System_Drawing_GDIPlus_StreamPutByte225654305.h"
#include "System_Drawing_System_Drawing_GDIPlus_StreamCloseD2004792630.h"
#include "System_Drawing_System_Drawing_GDIPlus_StreamSizeDe1636074383.h"
#include "System_Drawing_System_Drawing_GdiplusStartupInput2136305947.h"
#include "System_Drawing_System_Drawing_GdiplusStartupOutput1824520718.h"
#include "System_Drawing_System_Drawing_GdiColorPalette993082498.h"
#include "System_Drawing_System_Drawing_GdipImageCodecInfo2577501090.h"
#include "System_Drawing_System_Drawing_GdipEncoderParameter4122932783.h"
#include "System_Drawing_System_Drawing_KnownColor146469075.h"
#include "System_Drawing_System_Drawing_KnownColors138932435.h"
#include "System_Drawing_System_Drawing_IconConverter3169545787.h"
#include "System_Drawing_System_Drawing_Icon2291360527.h"
#include "System_Drawing_System_Drawing_Icon_IconDirEntry3003987536.h"
#include "System_Drawing_System_Drawing_Icon_IconDir4069448150.h"
#include "System_Drawing_System_Drawing_Icon_BitmapInfoHeade2752024591.h"
#include "System_Drawing_System_Drawing_Icon_IconImage1835934191.h"
#include "System_Drawing_System_Drawing_ImageConverter3574741128.h"
#include "System_Drawing_System_Drawing_Image2932642646.h"
#include "System_Drawing_System_Drawing_ImageFormatConverter3531050035.h"
#include "System_Drawing_System_Drawing_PointConverter4230562297.h"
#include "System_Drawing_System_Drawing_Point4030882587.h"
#include "System_Drawing_System_Drawing_RectangleConverter2189829998.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2800 = { sizeof (ExtensionAttribute_t1723066603), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2801 = { sizeof (Locale_t4128636112), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2802 = { sizeof (MonoTODOAttribute_t4131080587), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2802[1] = 
{
	MonoTODOAttribute_t4131080587::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2803 = { sizeof (MonoNotSupportedAttribute_t2563528020), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2804 = { sizeof (KeyBuilder_t2049230356), -1, sizeof(KeyBuilder_t2049230356_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2804[1] = 
{
	KeyBuilder_t2049230356_StaticFields::get_offset_of_rng_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2805 = { sizeof (SymmetricTransform_t3802591843), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2805[12] = 
{
	SymmetricTransform_t3802591843::get_offset_of_algo_0(),
	SymmetricTransform_t3802591843::get_offset_of_encrypt_1(),
	SymmetricTransform_t3802591843::get_offset_of_BlockSizeByte_2(),
	SymmetricTransform_t3802591843::get_offset_of_temp_3(),
	SymmetricTransform_t3802591843::get_offset_of_temp2_4(),
	SymmetricTransform_t3802591843::get_offset_of_workBuff_5(),
	SymmetricTransform_t3802591843::get_offset_of_workout_6(),
	SymmetricTransform_t3802591843::get_offset_of_FeedBackByte_7(),
	SymmetricTransform_t3802591843::get_offset_of_FeedBackIter_8(),
	SymmetricTransform_t3802591843::get_offset_of_m_disposed_9(),
	SymmetricTransform_t3802591843::get_offset_of_lastBlock_10(),
	SymmetricTransform_t3802591843::get_offset_of__rng_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2806 = { sizeof (Check_t192468399), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2807 = { sizeof (Enumerable_t538148348), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2808 = { sizeof (Fallback_t3495999270)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2808[3] = 
{
	Fallback_t3495999270::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2809 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2809[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2810 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2810[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2811 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2811[14] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2812 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2812[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2813 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2813[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2814 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2814[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2815 = { sizeof (Aes_t1218282760), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2816 = { sizeof (AesManaged_t1129950597), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2817 = { sizeof (AesCryptoServiceProvider_t345478893), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2818 = { sizeof (AesTransform_t2957123611), -1, sizeof(AesTransform_t2957123611_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2818[14] = 
{
	AesTransform_t2957123611::get_offset_of_expandedKey_12(),
	AesTransform_t2957123611::get_offset_of_Nk_13(),
	AesTransform_t2957123611::get_offset_of_Nr_14(),
	AesTransform_t2957123611_StaticFields::get_offset_of_Rcon_15(),
	AesTransform_t2957123611_StaticFields::get_offset_of_SBox_16(),
	AesTransform_t2957123611_StaticFields::get_offset_of_iSBox_17(),
	AesTransform_t2957123611_StaticFields::get_offset_of_T0_18(),
	AesTransform_t2957123611_StaticFields::get_offset_of_T1_19(),
	AesTransform_t2957123611_StaticFields::get_offset_of_T2_20(),
	AesTransform_t2957123611_StaticFields::get_offset_of_T3_21(),
	AesTransform_t2957123611_StaticFields::get_offset_of_iT0_22(),
	AesTransform_t2957123611_StaticFields::get_offset_of_iT1_23(),
	AesTransform_t2957123611_StaticFields::get_offset_of_iT2_24(),
	AesTransform_t2957123611_StaticFields::get_offset_of_iT3_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2819 = { sizeof (LockRecursionException_t2413788283), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2820 = { sizeof (ReaderWriterLockSlim_t1938317233), -1, sizeof(ReaderWriterLockSlim_t1938317233_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2820[12] = 
{
	ReaderWriterLockSlim_t1938317233_StaticFields::get_offset_of_smp_0(),
	ReaderWriterLockSlim_t1938317233::get_offset_of_myLock_1(),
	ReaderWriterLockSlim_t1938317233::get_offset_of_owners_2(),
	ReaderWriterLockSlim_t1938317233::get_offset_of_upgradable_thread_3(),
	ReaderWriterLockSlim_t1938317233::get_offset_of_write_thread_4(),
	ReaderWriterLockSlim_t1938317233::get_offset_of_numWriteWaiters_5(),
	ReaderWriterLockSlim_t1938317233::get_offset_of_numReadWaiters_6(),
	ReaderWriterLockSlim_t1938317233::get_offset_of_numUpgradeWaiters_7(),
	ReaderWriterLockSlim_t1938317233::get_offset_of_writeEvent_8(),
	ReaderWriterLockSlim_t1938317233::get_offset_of_readEvent_9(),
	ReaderWriterLockSlim_t1938317233::get_offset_of_upgradeEvent_10(),
	ReaderWriterLockSlim_t1938317233::get_offset_of_read_locks_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2821 = { sizeof (LockDetails_t2068764019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2821[2] = 
{
	LockDetails_t2068764019::get_offset_of_ThreadId_0(),
	LockDetails_t2068764019::get_offset_of_ReadLocks_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2822 = { sizeof (SafePipeHandle_t1989113880), sizeof(void*), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2823 = { sizeof (NamedPipeServerStream_t125618231), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2823[1] = 
{
	NamedPipeServerStream_t125618231::get_offset_of_impl_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2824 = { sizeof (PipeAccessRights_t4039352439)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2824[18] = 
{
	PipeAccessRights_t4039352439::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2825 = { sizeof (PipeDirection_t2244332536)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2825[4] = 
{
	PipeDirection_t2244332536::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2826 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2827 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2828 = { sizeof (PipeOptions_t3458221681)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2828[4] = 
{
	PipeOptions_t3458221681::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2829 = { sizeof (PipeSecurity_t3265965420), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2830 = { sizeof (PipeStream_t871053121), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2830[11] = 
{
	PipeStream_t871053121::get_offset_of_direction_2(),
	PipeStream_t871053121::get_offset_of_transmission_mode_3(),
	PipeStream_t871053121::get_offset_of_read_trans_mode_4(),
	PipeStream_t871053121::get_offset_of_buffer_size_5(),
	PipeStream_t871053121::get_offset_of_handle_6(),
	PipeStream_t871053121::get_offset_of_stream_7(),
	PipeStream_t871053121::get_offset_of_read_delegate_8(),
	PipeStream_t871053121::get_offset_of_write_delegate_9(),
	PipeStream_t871053121::get_offset_of_U3CIsAsyncU3Ek__BackingField_10(),
	PipeStream_t871053121::get_offset_of_U3CIsConnectedU3Ek__BackingField_11(),
	PipeStream_t871053121::get_offset_of_U3CIsHandleExposedU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2831 = { sizeof (PipeTransmissionMode_t3285869980)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2831[3] = 
{
	PipeTransmissionMode_t3285869980::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2832 = { sizeof (UnixNamedPipe_t2806256776), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2833 = { sizeof (UnixNamedPipeServer_t2839746932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2833[2] = 
{
	UnixNamedPipeServer_t2839746932::get_offset_of_handle_0(),
	UnixNamedPipeServer_t2839746932::get_offset_of_should_close_handle_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2834 = { sizeof (Win32NamedPipe_t1933459707), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2835 = { sizeof (Win32NamedPipeServer_t774293249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2835[1] = 
{
	Win32NamedPipeServer_t774293249::get_offset_of_handle_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2836 = { sizeof (SecurityAttributesHack_t2445122734)+ sizeof (Il2CppObject), sizeof(SecurityAttributesHack_t2445122734_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2836[3] = 
{
	SecurityAttributesHack_t2445122734::get_offset_of_Length_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SecurityAttributesHack_t2445122734::get_offset_of_SecurityDescriptor_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SecurityAttributesHack_t2445122734::get_offset_of_Inheritable_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2837 = { sizeof (Win32Marshal_t3620203595), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2838 = { sizeof (HandleInheritability_t208753816)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2838[3] = 
{
	HandleInheritability_t208753816::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2839 = { sizeof (Action_t1264377477), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2840 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2841 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2842 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2843 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2844 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2845 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255366), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2845[12] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24U24fieldU2D1_1(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24U24fieldU2D2_2(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24U24fieldU2D3_3(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24U24fieldU2D4_4(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24U24fieldU2D5_5(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24U24fieldU2D6_6(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24U24fieldU2D7_7(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24U24fieldU2D8_8(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24U24fieldU2D9_9(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24U24fieldU2D10_10(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24U24fieldU2D11_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2846 = { sizeof (U24ArrayTypeU24136_t1950429486)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24136_t1950429486 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2847 = { sizeof (U24ArrayTypeU24120_t4289081652)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24120_t4289081652 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2848 = { sizeof (U24ArrayTypeU24256_t1929481985)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t1929481985 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2849 = { sizeof (U24ArrayTypeU241024_t3907531058)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241024_t3907531058 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2850 = { sizeof (U3CModuleU3E_t692745538), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2851 = { sizeof (Locale_t4128636113), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2852 = { sizeof (Bitmap_t3858959443), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2853 = { sizeof (Carbon_t3931103330), -1, sizeof(Carbon_t3931103330_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2853[3] = 
{
	Carbon_t3931103330_StaticFields::get_offset_of_contextReference_0(),
	Carbon_t3931103330_StaticFields::get_offset_of_lockobj_1(),
	Carbon_t3931103330_StaticFields::get_offset_of_hwnd_delegate_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2854 = { sizeof (CarbonContext_t3570584123)+ sizeof (Il2CppObject), sizeof(CarbonContext_t3570584123 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2854[4] = 
{
	CarbonContext_t3570584123::get_offset_of_port_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CarbonContext_t3570584123::get_offset_of_ctx_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CarbonContext_t3570584123::get_offset_of_width_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CarbonContext_t3570584123::get_offset_of_height_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2855 = { sizeof (ColorConverter_t2126274855), -1, sizeof(ColorConverter_t2126274855_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2855[2] = 
{
	ColorConverter_t2126274855_StaticFields::get_offset_of_cached_0(),
	ColorConverter_t2126274855_StaticFields::get_offset_of_creatingCached_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2856 = { sizeof (CompareColors_t2817450177), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2857 = { sizeof (Color_t1869934208)+ sizeof (Il2CppObject), sizeof(Color_t1869934208_marshaled_pinvoke), sizeof(Color_t1869934208_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2857[5] = 
{
	Color_t1869934208::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color_t1869934208::get_offset_of_state_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color_t1869934208::get_offset_of_knownColor_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color_t1869934208::get_offset_of_name_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color_t1869934208_StaticFields::get_offset_of_Empty_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2858 = { sizeof (ColorTranslator_t1362899606), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2859 = { sizeof (ComIStreamMarshaler_t1972832677), -1, sizeof(ComIStreamMarshaler_t1972832677_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2859[1] = 
{
	ComIStreamMarshaler_t1972832677_StaticFields::get_offset_of_defaultInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2860 = { sizeof (ComIStreamWrapper_t1385077908), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2860[2] = 
{
	ComIStreamWrapper_t1385077908::get_offset_of_baseStream_0(),
	ComIStreamWrapper_t1385077908::get_offset_of_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2861 = { sizeof (Font_t3402197101), -1, sizeof(Font_t3402197101_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2861[15] = 
{
	Font_t3402197101::get_offset_of_fontObject_1(),
	Font_t3402197101::get_offset_of_originalFontName_2(),
	Font_t3402197101::get_offset_of__size_3(),
	Font_t3402197101_StaticFields::get_offset_of_CharSetOffset_4(),
	Font_t3402197101::get_offset_of__bold_5(),
	Font_t3402197101::get_offset_of__fontFamily_6(),
	Font_t3402197101::get_offset_of__gdiCharSet_7(),
	Font_t3402197101::get_offset_of__gdiVerticalFont_8(),
	Font_t3402197101::get_offset_of__italic_9(),
	Font_t3402197101::get_offset_of__name_10(),
	Font_t3402197101::get_offset_of__sizeInPoints_11(),
	Font_t3402197101::get_offset_of__strikeout_12(),
	Font_t3402197101::get_offset_of__style_13(),
	Font_t3402197101::get_offset_of__underline_14(),
	Font_t3402197101::get_offset_of__unit_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2862 = { sizeof (FontConverter_t2762192330), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2863 = { sizeof (FontNameConverter_t3052059787), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2863[1] = 
{
	FontNameConverter_t3052059787::get_offset_of_fonts_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2864 = { sizeof (FontUnitConverter_t3646088898), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2865 = { sizeof (FontFamily_t13778311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2865[2] = 
{
	FontFamily_t13778311::get_offset_of_name_1(),
	FontFamily_t13778311::get_offset_of_nativeFontFamily_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2866 = { sizeof (FontStyle_t2978677421)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2866[6] = 
{
	FontStyle_t2978677421::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2867 = { sizeof (Graphics_t2557146384), -1, sizeof(Graphics_t2557146384_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2867[5] = 
{
	Graphics_t2557146384::get_offset_of_nativeObject_1(),
	Graphics_t2557146384::get_offset_of_context_2(),
	Graphics_t2557146384::get_offset_of_disposed_3(),
	Graphics_t2557146384_StaticFields::get_offset_of_defDpiX_4(),
	Graphics_t2557146384_StaticFields::get_offset_of_defDpiY_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2868 = { sizeof (GraphicsUnit_t4033181596)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2868[8] = 
{
	GraphicsUnit_t4033181596::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2869 = { sizeof (Status_t3438243046)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2869[23] = 
{
	Status_t3438243046::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2870 = { sizeof (ImageType_t223047490)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2870[4] = 
{
	ImageType_t223047490::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2871 = { sizeof (GetSysColorIndex_t869702789)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2871[38] = 
{
	GetSysColorIndex_t869702789::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2872 = { sizeof (GDIPlus_t2533009916), -1, sizeof(GDIPlus_t2533009916_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2872[4] = 
{
	GDIPlus_t2533009916_StaticFields::get_offset_of_Display_0(),
	GDIPlus_t2533009916_StaticFields::get_offset_of_UseX11Drawable_1(),
	GDIPlus_t2533009916_StaticFields::get_offset_of_UseCarbonDrawable_2(),
	GDIPlus_t2533009916_StaticFields::get_offset_of_GdiPlusToken_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2873 = { sizeof (GdiPlusStreamHelper_t2754160014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2873[11] = 
{
	GdiPlusStreamHelper_t2754160014::get_offset_of_stream_0(),
	GdiPlusStreamHelper_t2754160014::get_offset_of_sghd_1(),
	GdiPlusStreamHelper_t2754160014::get_offset_of_sgbd_2(),
	GdiPlusStreamHelper_t2754160014::get_offset_of_skd_3(),
	GdiPlusStreamHelper_t2754160014::get_offset_of_spbd_4(),
	GdiPlusStreamHelper_t2754160014::get_offset_of_scd_5(),
	GdiPlusStreamHelper_t2754160014::get_offset_of_ssd_6(),
	GdiPlusStreamHelper_t2754160014::get_offset_of_start_buf_7(),
	GdiPlusStreamHelper_t2754160014::get_offset_of_start_buf_pos_8(),
	GdiPlusStreamHelper_t2754160014::get_offset_of_start_buf_len_9(),
	GdiPlusStreamHelper_t2754160014::get_offset_of_managedBuf_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2874 = { sizeof (StreamGetHeaderDelegate_t558280091), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2875 = { sizeof (StreamGetBytesDelegate_t482941147), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2876 = { sizeof (StreamSeekDelegate_t2972861213), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2877 = { sizeof (StreamPutBytesDelegate_t225654305), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2878 = { sizeof (StreamCloseDelegate_t2004792630), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2879 = { sizeof (StreamSizeDelegate_t1636074383), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2880 = { sizeof (GdiplusStartupInput_t2136305947)+ sizeof (Il2CppObject), sizeof(GdiplusStartupInput_t2136305947 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2880[4] = 
{
	GdiplusStartupInput_t2136305947::get_offset_of_GdiplusVersion_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GdiplusStartupInput_t2136305947::get_offset_of_DebugEventCallback_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GdiplusStartupInput_t2136305947::get_offset_of_SuppressBackgroundThread_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GdiplusStartupInput_t2136305947::get_offset_of_SuppressExternalCodecs_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2881 = { sizeof (GdiplusStartupOutput_t1824520718)+ sizeof (Il2CppObject), sizeof(GdiplusStartupOutput_t1824520718 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2881[2] = 
{
	GdiplusStartupOutput_t1824520718::get_offset_of_NotificationHook_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GdiplusStartupOutput_t1824520718::get_offset_of_NotificationUnhook_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2882 = { sizeof (GdiColorPalette_t993082498)+ sizeof (Il2CppObject), sizeof(GdiColorPalette_t993082498 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2882[2] = 
{
	GdiColorPalette_t993082498::get_offset_of_Flags_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GdiColorPalette_t993082498::get_offset_of_Count_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2883 = { sizeof (GdipImageCodecInfo_t2577501090)+ sizeof (Il2CppObject), sizeof(GdipImageCodecInfo_t2577501090 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2883[13] = 
{
	GdipImageCodecInfo_t2577501090::get_offset_of_Clsid_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GdipImageCodecInfo_t2577501090::get_offset_of_FormatID_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GdipImageCodecInfo_t2577501090::get_offset_of_CodecName_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GdipImageCodecInfo_t2577501090::get_offset_of_DllName_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GdipImageCodecInfo_t2577501090::get_offset_of_FormatDescription_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GdipImageCodecInfo_t2577501090::get_offset_of_FilenameExtension_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GdipImageCodecInfo_t2577501090::get_offset_of_MimeType_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GdipImageCodecInfo_t2577501090::get_offset_of_Flags_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GdipImageCodecInfo_t2577501090::get_offset_of_Version_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GdipImageCodecInfo_t2577501090::get_offset_of_SigCount_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GdipImageCodecInfo_t2577501090::get_offset_of_SigSize_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GdipImageCodecInfo_t2577501090::get_offset_of_SigPattern_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GdipImageCodecInfo_t2577501090::get_offset_of_SigMask_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2884 = { sizeof (GdipEncoderParameter_t4122932783)+ sizeof (Il2CppObject), sizeof(GdipEncoderParameter_t4122932783 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2884[4] = 
{
	GdipEncoderParameter_t4122932783::get_offset_of_guid_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GdipEncoderParameter_t4122932783::get_offset_of_numberOfValues_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GdipEncoderParameter_t4122932783::get_offset_of_type_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GdipEncoderParameter_t4122932783::get_offset_of_value_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2885 = { sizeof (KnownColor_t146469075)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2885[175] = 
{
	KnownColor_t146469075::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2886 = { sizeof (KnownColors_t138932435), -1, sizeof(KnownColors_t138932435_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2886[1] = 
{
	KnownColors_t138932435_StaticFields::get_offset_of_ArgbValues_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2887 = { sizeof (IconConverter_t3169545787), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2888 = { sizeof (Icon_t2291360527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2888[8] = 
{
	Icon_t2291360527::get_offset_of_iconSize_1(),
	Icon_t2291360527::get_offset_of_handle_2(),
	Icon_t2291360527::get_offset_of_iconDir_3(),
	Icon_t2291360527::get_offset_of_id_4(),
	Icon_t2291360527::get_offset_of_imageData_5(),
	Icon_t2291360527::get_offset_of_undisposable_6(),
	Icon_t2291360527::get_offset_of_disposed_7(),
	Icon_t2291360527::get_offset_of_bitmap_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2889 = { sizeof (IconDirEntry_t3003987536)+ sizeof (Il2CppObject), sizeof(IconDirEntry_t3003987536 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2889[8] = 
{
	IconDirEntry_t3003987536::get_offset_of_width_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IconDirEntry_t3003987536::get_offset_of_height_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IconDirEntry_t3003987536::get_offset_of_colorCount_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IconDirEntry_t3003987536::get_offset_of_reserved_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IconDirEntry_t3003987536::get_offset_of_planes_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IconDirEntry_t3003987536::get_offset_of_bitCount_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IconDirEntry_t3003987536::get_offset_of_bytesInRes_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IconDirEntry_t3003987536::get_offset_of_imageOffset_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2890 = { sizeof (IconDir_t4069448150)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2890[4] = 
{
	IconDir_t4069448150::get_offset_of_idReserved_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IconDir_t4069448150::get_offset_of_idType_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IconDir_t4069448150::get_offset_of_idCount_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IconDir_t4069448150::get_offset_of_idEntries_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2891 = { sizeof (BitmapInfoHeader_t2752024591)+ sizeof (Il2CppObject), sizeof(BitmapInfoHeader_t2752024591 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2891[11] = 
{
	BitmapInfoHeader_t2752024591::get_offset_of_biSize_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	BitmapInfoHeader_t2752024591::get_offset_of_biWidth_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	BitmapInfoHeader_t2752024591::get_offset_of_biHeight_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	BitmapInfoHeader_t2752024591::get_offset_of_biPlanes_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	BitmapInfoHeader_t2752024591::get_offset_of_biBitCount_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	BitmapInfoHeader_t2752024591::get_offset_of_biCompression_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	BitmapInfoHeader_t2752024591::get_offset_of_biSizeImage_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	BitmapInfoHeader_t2752024591::get_offset_of_biXPelsPerMeter_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	BitmapInfoHeader_t2752024591::get_offset_of_biYPelsPerMeter_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	BitmapInfoHeader_t2752024591::get_offset_of_biClrUsed_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	BitmapInfoHeader_t2752024591::get_offset_of_biClrImportant_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2892 = { sizeof (IconImage_t1835934191)+ sizeof (Il2CppObject), sizeof(IconImage_t1835934191_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2892[4] = 
{
	IconImage_t1835934191::get_offset_of_iconHeader_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IconImage_t1835934191::get_offset_of_iconColors_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IconImage_t1835934191::get_offset_of_iconXOR_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IconImage_t1835934191::get_offset_of_iconAND_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2893 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2894 = { sizeof (ImageConverter_t3574741128), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2895 = { sizeof (Image_t2932642646), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2895[2] = 
{
	Image_t2932642646::get_offset_of_nativeObject_1(),
	Image_t2932642646::get_offset_of_stream_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2896 = { sizeof (ImageFormatConverter_t3531050035), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2897 = { sizeof (PointConverter_t4230562297), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2898 = { sizeof (Point_t4030882587)+ sizeof (Il2CppObject), sizeof(Point_t4030882587 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2898[2] = 
{
	Point_t4030882587::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Point_t4030882587::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2899 = { sizeof (RectangleConverter_t2189829998), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
