﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XPath_XPathNavigator787956054.h"

// Mono.Xml.XPath.DTMXPathDocument2
struct DTMXPathDocument2_t2335062454;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.XPath.DTMXPathNavigator2
struct  DTMXPathNavigator2_t4118996007  : public XPathNavigator_t787956054
{
public:
	// Mono.Xml.XPath.DTMXPathDocument2 Mono.Xml.XPath.DTMXPathNavigator2::document
	DTMXPathDocument2_t2335062454 * ___document_2;
	// System.Boolean Mono.Xml.XPath.DTMXPathNavigator2::currentIsNode
	bool ___currentIsNode_3;
	// System.Boolean Mono.Xml.XPath.DTMXPathNavigator2::currentIsAttr
	bool ___currentIsAttr_4;
	// System.Int32 Mono.Xml.XPath.DTMXPathNavigator2::currentNode
	int32_t ___currentNode_5;
	// System.Int32 Mono.Xml.XPath.DTMXPathNavigator2::currentAttr
	int32_t ___currentAttr_6;
	// System.Int32 Mono.Xml.XPath.DTMXPathNavigator2::currentNs
	int32_t ___currentNs_7;

public:
	inline static int32_t get_offset_of_document_2() { return static_cast<int32_t>(offsetof(DTMXPathNavigator2_t4118996007, ___document_2)); }
	inline DTMXPathDocument2_t2335062454 * get_document_2() const { return ___document_2; }
	inline DTMXPathDocument2_t2335062454 ** get_address_of_document_2() { return &___document_2; }
	inline void set_document_2(DTMXPathDocument2_t2335062454 * value)
	{
		___document_2 = value;
		Il2CppCodeGenWriteBarrier(&___document_2, value);
	}

	inline static int32_t get_offset_of_currentIsNode_3() { return static_cast<int32_t>(offsetof(DTMXPathNavigator2_t4118996007, ___currentIsNode_3)); }
	inline bool get_currentIsNode_3() const { return ___currentIsNode_3; }
	inline bool* get_address_of_currentIsNode_3() { return &___currentIsNode_3; }
	inline void set_currentIsNode_3(bool value)
	{
		___currentIsNode_3 = value;
	}

	inline static int32_t get_offset_of_currentIsAttr_4() { return static_cast<int32_t>(offsetof(DTMXPathNavigator2_t4118996007, ___currentIsAttr_4)); }
	inline bool get_currentIsAttr_4() const { return ___currentIsAttr_4; }
	inline bool* get_address_of_currentIsAttr_4() { return &___currentIsAttr_4; }
	inline void set_currentIsAttr_4(bool value)
	{
		___currentIsAttr_4 = value;
	}

	inline static int32_t get_offset_of_currentNode_5() { return static_cast<int32_t>(offsetof(DTMXPathNavigator2_t4118996007, ___currentNode_5)); }
	inline int32_t get_currentNode_5() const { return ___currentNode_5; }
	inline int32_t* get_address_of_currentNode_5() { return &___currentNode_5; }
	inline void set_currentNode_5(int32_t value)
	{
		___currentNode_5 = value;
	}

	inline static int32_t get_offset_of_currentAttr_6() { return static_cast<int32_t>(offsetof(DTMXPathNavigator2_t4118996007, ___currentAttr_6)); }
	inline int32_t get_currentAttr_6() const { return ___currentAttr_6; }
	inline int32_t* get_address_of_currentAttr_6() { return &___currentAttr_6; }
	inline void set_currentAttr_6(int32_t value)
	{
		___currentAttr_6 = value;
	}

	inline static int32_t get_offset_of_currentNs_7() { return static_cast<int32_t>(offsetof(DTMXPathNavigator2_t4118996007, ___currentNs_7)); }
	inline int32_t get_currentNs_7() const { return ___currentNs_7; }
	inline int32_t* get_address_of_currentNs_7() { return &___currentNs_7; }
	inline void set_currentNs_7(int32_t value)
	{
		___currentNs_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
