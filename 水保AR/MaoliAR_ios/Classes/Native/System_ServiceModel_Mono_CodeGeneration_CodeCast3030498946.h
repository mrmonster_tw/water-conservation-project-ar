﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_Mono_CodeGeneration_CodeExpres2163794278.h"

// System.Type
struct Type_t;
// Mono.CodeGeneration.CodeExpression
struct CodeExpression_t2163794278;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.CodeGeneration.CodeCast
struct  CodeCast_t3030498946  : public CodeExpression_t2163794278
{
public:
	// System.Type Mono.CodeGeneration.CodeCast::type
	Type_t * ___type_0;
	// Mono.CodeGeneration.CodeExpression Mono.CodeGeneration.CodeCast::exp
	CodeExpression_t2163794278 * ___exp_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(CodeCast_t3030498946, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier(&___type_0, value);
	}

	inline static int32_t get_offset_of_exp_1() { return static_cast<int32_t>(offsetof(CodeCast_t3030498946, ___exp_1)); }
	inline CodeExpression_t2163794278 * get_exp_1() const { return ___exp_1; }
	inline CodeExpression_t2163794278 ** get_address_of_exp_1() { return &___exp_1; }
	inline void set_exp_1(CodeExpression_t2163794278 * value)
	{
		___exp_1 = value;
		Il2CppCodeGenWriteBarrier(&___exp_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
