﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_Reflection_Emit_Label2281661643.h"

// Mono.CodeGeneration.CodeBlock
struct CodeBlock_t2718634139;
// System.Collections.Stack
struct Stack_t2329662280;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// Mono.CodeGeneration.CodeClass
struct CodeClass_t2288932064;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.CodeGeneration.CodeBuilder
struct  CodeBuilder_t3190018006  : public Il2CppObject
{
public:
	// Mono.CodeGeneration.CodeBlock Mono.CodeGeneration.CodeBuilder::mainBlock
	CodeBlock_t2718634139 * ___mainBlock_0;
	// Mono.CodeGeneration.CodeBlock Mono.CodeGeneration.CodeBuilder::currentBlock
	CodeBlock_t2718634139 * ___currentBlock_1;
	// System.Collections.Stack Mono.CodeGeneration.CodeBuilder::blockStack
	Stack_t2329662280 * ___blockStack_2;
	// System.Reflection.Emit.Label Mono.CodeGeneration.CodeBuilder::returnLabel
	Label_t2281661643  ___returnLabel_3;
	// System.Collections.ArrayList Mono.CodeGeneration.CodeBuilder::nestedIfs
	ArrayList_t2718874744 * ___nestedIfs_4;
	// System.Int32 Mono.CodeGeneration.CodeBuilder::currentIfSerie
	int32_t ___currentIfSerie_5;
	// Mono.CodeGeneration.CodeClass Mono.CodeGeneration.CodeBuilder::codeClass
	CodeClass_t2288932064 * ___codeClass_6;

public:
	inline static int32_t get_offset_of_mainBlock_0() { return static_cast<int32_t>(offsetof(CodeBuilder_t3190018006, ___mainBlock_0)); }
	inline CodeBlock_t2718634139 * get_mainBlock_0() const { return ___mainBlock_0; }
	inline CodeBlock_t2718634139 ** get_address_of_mainBlock_0() { return &___mainBlock_0; }
	inline void set_mainBlock_0(CodeBlock_t2718634139 * value)
	{
		___mainBlock_0 = value;
		Il2CppCodeGenWriteBarrier(&___mainBlock_0, value);
	}

	inline static int32_t get_offset_of_currentBlock_1() { return static_cast<int32_t>(offsetof(CodeBuilder_t3190018006, ___currentBlock_1)); }
	inline CodeBlock_t2718634139 * get_currentBlock_1() const { return ___currentBlock_1; }
	inline CodeBlock_t2718634139 ** get_address_of_currentBlock_1() { return &___currentBlock_1; }
	inline void set_currentBlock_1(CodeBlock_t2718634139 * value)
	{
		___currentBlock_1 = value;
		Il2CppCodeGenWriteBarrier(&___currentBlock_1, value);
	}

	inline static int32_t get_offset_of_blockStack_2() { return static_cast<int32_t>(offsetof(CodeBuilder_t3190018006, ___blockStack_2)); }
	inline Stack_t2329662280 * get_blockStack_2() const { return ___blockStack_2; }
	inline Stack_t2329662280 ** get_address_of_blockStack_2() { return &___blockStack_2; }
	inline void set_blockStack_2(Stack_t2329662280 * value)
	{
		___blockStack_2 = value;
		Il2CppCodeGenWriteBarrier(&___blockStack_2, value);
	}

	inline static int32_t get_offset_of_returnLabel_3() { return static_cast<int32_t>(offsetof(CodeBuilder_t3190018006, ___returnLabel_3)); }
	inline Label_t2281661643  get_returnLabel_3() const { return ___returnLabel_3; }
	inline Label_t2281661643 * get_address_of_returnLabel_3() { return &___returnLabel_3; }
	inline void set_returnLabel_3(Label_t2281661643  value)
	{
		___returnLabel_3 = value;
	}

	inline static int32_t get_offset_of_nestedIfs_4() { return static_cast<int32_t>(offsetof(CodeBuilder_t3190018006, ___nestedIfs_4)); }
	inline ArrayList_t2718874744 * get_nestedIfs_4() const { return ___nestedIfs_4; }
	inline ArrayList_t2718874744 ** get_address_of_nestedIfs_4() { return &___nestedIfs_4; }
	inline void set_nestedIfs_4(ArrayList_t2718874744 * value)
	{
		___nestedIfs_4 = value;
		Il2CppCodeGenWriteBarrier(&___nestedIfs_4, value);
	}

	inline static int32_t get_offset_of_currentIfSerie_5() { return static_cast<int32_t>(offsetof(CodeBuilder_t3190018006, ___currentIfSerie_5)); }
	inline int32_t get_currentIfSerie_5() const { return ___currentIfSerie_5; }
	inline int32_t* get_address_of_currentIfSerie_5() { return &___currentIfSerie_5; }
	inline void set_currentIfSerie_5(int32_t value)
	{
		___currentIfSerie_5 = value;
	}

	inline static int32_t get_offset_of_codeClass_6() { return static_cast<int32_t>(offsetof(CodeBuilder_t3190018006, ___codeClass_6)); }
	inline CodeClass_t2288932064 * get_codeClass_6() const { return ___codeClass_6; }
	inline CodeClass_t2288932064 ** get_address_of_codeClass_6() { return &___codeClass_6; }
	inline void set_codeClass_6(CodeClass_t2288932064 * value)
	{
		___codeClass_6 = value;
		Il2CppCodeGenWriteBarrier(&___codeClass_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
