﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_TextReader283511965.h"

// System.IO.TextReader
struct TextReader_t283511965;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.Xml.XmlSignatureStreamReader
struct  XmlSignatureStreamReader_t1967805337  : public TextReader_t283511965
{
public:
	// System.IO.TextReader System.Security.Cryptography.Xml.XmlSignatureStreamReader::source
	TextReader_t283511965 * ___source_2;
	// System.Int32 System.Security.Cryptography.Xml.XmlSignatureStreamReader::cache
	int32_t ___cache_3;

public:
	inline static int32_t get_offset_of_source_2() { return static_cast<int32_t>(offsetof(XmlSignatureStreamReader_t1967805337, ___source_2)); }
	inline TextReader_t283511965 * get_source_2() const { return ___source_2; }
	inline TextReader_t283511965 ** get_address_of_source_2() { return &___source_2; }
	inline void set_source_2(TextReader_t283511965 * value)
	{
		___source_2 = value;
		Il2CppCodeGenWriteBarrier(&___source_2, value);
	}

	inline static int32_t get_offset_of_cache_3() { return static_cast<int32_t>(offsetof(XmlSignatureStreamReader_t1967805337, ___cache_3)); }
	inline int32_t get_cache_3() const { return ___cache_3; }
	inline int32_t* get_address_of_cache_3() { return &___cache_3; }
	inline void set_cache_3(int32_t value)
	{
		___cache_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
