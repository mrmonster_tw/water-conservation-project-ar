﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Col_to_add
struct  Col_to_add_t3844769183  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Events.UnityEvent Col_to_add::Events
	UnityEvent_t2581268647 * ___Events_2;

public:
	inline static int32_t get_offset_of_Events_2() { return static_cast<int32_t>(offsetof(Col_to_add_t3844769183, ___Events_2)); }
	inline UnityEvent_t2581268647 * get_Events_2() const { return ___Events_2; }
	inline UnityEvent_t2581268647 ** get_address_of_Events_2() { return &___Events_2; }
	inline void set_Events_2(UnityEvent_t2581268647 * value)
	{
		___Events_2 = value;
		Il2CppCodeGenWriteBarrier(&___Events_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
