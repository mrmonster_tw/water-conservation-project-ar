﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_ObjectStateFormatter_Stri3828688682.h"

// System.ComponentModel.TypeConverter
struct TypeConverter_t2249118273;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.ObjectStateFormatter/TypeConverterFormatter
struct  TypeConverterFormatter_t1198826992  : public StringFormatter_t3828688682
{
public:
	// System.ComponentModel.TypeConverter System.Web.UI.ObjectStateFormatter/TypeConverterFormatter::converter
	TypeConverter_t2249118273 * ___converter_11;

public:
	inline static int32_t get_offset_of_converter_11() { return static_cast<int32_t>(offsetof(TypeConverterFormatter_t1198826992, ___converter_11)); }
	inline TypeConverter_t2249118273 * get_converter_11() const { return ___converter_11; }
	inline TypeConverter_t2249118273 ** get_address_of_converter_11() { return &___converter_11; }
	inline void set_converter_11(TypeConverter_t2249118273 * value)
	{
		___converter_11 = value;
		Il2CppCodeGenWriteBarrier(&___converter_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
