﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Ch990592377.h"

// System.ServiceModel.IDefaultCommunicationTimeouts
struct IDefaultCommunicationTimeouts_t2848643016;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.ChannelListenerBase`1<System.ServiceModel.Channels.IInputChannel>
struct  ChannelListenerBase_1_t650784192  : public ChannelListenerBase_t990592377
{
public:
	// System.ServiceModel.IDefaultCommunicationTimeouts System.ServiceModel.Channels.ChannelListenerBase`1::timeouts
	Il2CppObject * ___timeouts_11;

public:
	inline static int32_t get_offset_of_timeouts_11() { return static_cast<int32_t>(offsetof(ChannelListenerBase_1_t650784192, ___timeouts_11)); }
	inline Il2CppObject * get_timeouts_11() const { return ___timeouts_11; }
	inline Il2CppObject ** get_address_of_timeouts_11() { return &___timeouts_11; }
	inline void set_timeouts_11(Il2CppObject * value)
	{
		___timeouts_11 = value;
		Il2CppCodeGenWriteBarrier(&___timeouts_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
