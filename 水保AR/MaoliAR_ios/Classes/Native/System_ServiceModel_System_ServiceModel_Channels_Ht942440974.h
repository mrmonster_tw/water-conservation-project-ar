﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_H3854184778.h"

// System.ServiceModel.Channels.HttpSimpleChannelListener`1<System.ServiceModel.Channels.IReplyChannel>
struct HttpSimpleChannelListener_1_t3945900668;
// System.Collections.Generic.List`1<System.Net.HttpListenerContext>
struct List_1_t1896955564;
// System.ServiceModel.Channels.RequestContext
struct RequestContext_t1473296135;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t1333520283;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.HttpSimpleReplyChannel
struct  HttpSimpleReplyChannel_t942440974  : public HttpReplyChannel_t3854184778
{
public:
	// System.ServiceModel.Channels.HttpSimpleChannelListener`1<System.ServiceModel.Channels.IReplyChannel> System.ServiceModel.Channels.HttpSimpleReplyChannel::source
	HttpSimpleChannelListener_1_t3945900668 * ___source_16;
	// System.Collections.Generic.List`1<System.Net.HttpListenerContext> System.ServiceModel.Channels.HttpSimpleReplyChannel::waiting
	List_1_t1896955564 * ___waiting_17;
	// System.ServiceModel.Channels.RequestContext System.ServiceModel.Channels.HttpSimpleReplyChannel::reqctx
	RequestContext_t1473296135 * ___reqctx_18;
	// System.Threading.AutoResetEvent System.ServiceModel.Channels.HttpSimpleReplyChannel::wait
	AutoResetEvent_t1333520283 * ___wait_19;

public:
	inline static int32_t get_offset_of_source_16() { return static_cast<int32_t>(offsetof(HttpSimpleReplyChannel_t942440974, ___source_16)); }
	inline HttpSimpleChannelListener_1_t3945900668 * get_source_16() const { return ___source_16; }
	inline HttpSimpleChannelListener_1_t3945900668 ** get_address_of_source_16() { return &___source_16; }
	inline void set_source_16(HttpSimpleChannelListener_1_t3945900668 * value)
	{
		___source_16 = value;
		Il2CppCodeGenWriteBarrier(&___source_16, value);
	}

	inline static int32_t get_offset_of_waiting_17() { return static_cast<int32_t>(offsetof(HttpSimpleReplyChannel_t942440974, ___waiting_17)); }
	inline List_1_t1896955564 * get_waiting_17() const { return ___waiting_17; }
	inline List_1_t1896955564 ** get_address_of_waiting_17() { return &___waiting_17; }
	inline void set_waiting_17(List_1_t1896955564 * value)
	{
		___waiting_17 = value;
		Il2CppCodeGenWriteBarrier(&___waiting_17, value);
	}

	inline static int32_t get_offset_of_reqctx_18() { return static_cast<int32_t>(offsetof(HttpSimpleReplyChannel_t942440974, ___reqctx_18)); }
	inline RequestContext_t1473296135 * get_reqctx_18() const { return ___reqctx_18; }
	inline RequestContext_t1473296135 ** get_address_of_reqctx_18() { return &___reqctx_18; }
	inline void set_reqctx_18(RequestContext_t1473296135 * value)
	{
		___reqctx_18 = value;
		Il2CppCodeGenWriteBarrier(&___reqctx_18, value);
	}

	inline static int32_t get_offset_of_wait_19() { return static_cast<int32_t>(offsetof(HttpSimpleReplyChannel_t942440974, ___wait_19)); }
	inline AutoResetEvent_t1333520283 * get_wait_19() const { return ___wait_19; }
	inline AutoResetEvent_t1333520283 ** get_address_of_wait_19() { return &___wait_19; }
	inline void set_wait_19(AutoResetEvent_t1333520283 * value)
	{
		___wait_19 = value;
		Il2CppCodeGenWriteBarrier(&___wait_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
