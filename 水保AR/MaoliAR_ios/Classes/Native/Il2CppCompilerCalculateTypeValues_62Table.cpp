﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BT_Mic2341817396.h"
#include "AssemblyU2DCSharp_Send_Receive_ArduinoData_by_BT3589867669.h"
#include "AssemblyU2DCSharp_Send_Receive_ArduinoData_by_BT_U3181719613.h"
#include "AssemblyU2DCSharp_Send_Receive_ArduinoData_by_BT_U1361298989.h"
#include "AssemblyU2DCSharp_Drag_Controller1925480236.h"
#include "AssemblyU2DCSharp_Drag_Controller_U3CU9EDEU5C0DU4E1244618071.h"
#include "AssemblyU2DCSharp_Drag_Controller_U3CU9EDEU932FU4E2446876263.h"
#include "AssemblyU2DCSharp_Level_01_Controller3780315112.h"
#include "AssemblyU2DCSharp_Level_02_Controller1742187031.h"
#include "AssemblyU2DCSharp_Level_03_Controller697859203.h"
#include "AssemblyU2DCSharp_Level_03_Controller_ICON1624219477.h"
#include "AssemblyU2DCSharp_Level_04_Controller985329950.h"
#include "AssemblyU2DCSharp_Level_05_Controller1454764318.h"
#include "AssemblyU2DCSharp_Level_05_Controller_U3CdoWinEffec374873640.h"
#include "AssemblyU2DCSharp_Level_05_Controller_ICON491541153.h"
#include "AssemblyU2DCSharp_Level_06_Controller3711603533.h"
#include "AssemblyU2DCSharp_Level_06_Controller_U3CStartImag1459609615.h"
#include "AssemblyU2DCSharp_Level_06_Controller_U3CFadeToBoo3508591351.h"
#include "AssemblyU2DCSharp_LookAt2949065266.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Atmosphere_Rain3269395514.h"
#include "AssemblyU2DCSharp_Col_to_add3844769183.h"
#include "AssemblyU2DCSharp_Dc164372262.h"
#include "AssemblyU2DCSharp_Drop4098387221.h"
#include "AssemblyU2DCSharp_SpriteOutline3043537775.h"
#include "AssemblyU2DCSharp_TimeGame3115909209.h"
#include "AssemblyU2DCSharp_TimeGame_U3CRockStartU3Ec__Itera2048428861.h"
#include "AssemblyU2DCSharp_TimeGame_U3CResetU3Ec__Iterator13538958262.h"
#include "AssemblyU2DCSharp_TimeLevel44195116982.h"
#include "AssemblyU2DCSharp_TimeLevel4_U3CResetU3Ec__Iterato3688093126.h"
#include "AssemblyU2DCSharp_WaitForClose2325699134.h"
#include "AssemblyU2DCSharp_WaitForClose_U3CwaitCloseU3Ec__I3687100374.h"
#include "AssemblyU2DCSharp_drawBox3241306425.h"
#include "AssemblyU2DCSharp_fade_InOut787920925.h"
#include "AssemblyU2DCSharp_leve4To2D1062310264.h"
#include "AssemblyU2DCSharp_Play_Level_Music2585779503.h"
#include "AssemblyU2DCSharp_Point_Controller869836145.h"
#include "AssemblyU2DCSharp_bt_sound3531492350.h"
#include "AssemblyU2DCSharp_notMobile_MapController3835353441.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3057255361.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3651253610.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3353960863.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3242499063.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E692745525.h"
#include "AssemblyU2DUnityScript_JSHighlighterController967495899.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_U3CModuleU3E692745525.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_PostEffectsBase3718631875.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_SunShaftsResolut277016971.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_ShaftsScreenBle2434095160.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_SunShafts3552083306.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6200 = { sizeof (BT_Mic_t2341817396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6200[2] = 
{
	BT_Mic_t2341817396::get_offset_of_SW_0(),
	BT_Mic_t2341817396::get_offset_of_MIC_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6201 = { sizeof (Send_Receive_ArduinoData_by_BT_t3589867669), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6201[5] = 
{
	Send_Receive_ArduinoData_by_BT_t3589867669::get_offset_of__data_2(),
	Send_Receive_ArduinoData_by_BT_t3589867669::get_offset_of_b_debug_3(),
	Send_Receive_ArduinoData_by_BT_t3589867669::get_offset_of_BT_state_text_4(),
	Send_Receive_ArduinoData_by_BT_t3589867669::get_offset_of_a_5(),
	Send_Receive_ArduinoData_by_BT_t3589867669::get_offset_of_readLine_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6202 = { sizeof (U3ConScanedU3Ec__Iterator0_t3181719613), -1, sizeof(U3ConScanedU3Ec__Iterator0_t3181719613_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6202[8] = 
{
	U3ConScanedU3Ec__Iterator0_t3181719613::get_offset_of_U3CtU3E__0_0(),
	U3ConScanedU3Ec__Iterator0_t3181719613::get_offset_of_U3CnowSceU3E__0_1(),
	U3ConScanedU3Ec__Iterator0_t3181719613::get_offset_of_U3CcontanntU3E__1_2(),
	U3ConScanedU3Ec__Iterator0_t3181719613::get_offset_of_U24this_3(),
	U3ConScanedU3Ec__Iterator0_t3181719613::get_offset_of_U24current_4(),
	U3ConScanedU3Ec__Iterator0_t3181719613::get_offset_of_U24disposing_5(),
	U3ConScanedU3Ec__Iterator0_t3181719613::get_offset_of_U24PC_6(),
	U3ConScanedU3Ec__Iterator0_t3181719613_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6203 = { sizeof (U3CwaitU3Ec__Iterator1_t1361298989), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6203[5] = 
{
	U3CwaitU3Ec__Iterator1_t1361298989::get_offset_of_U3CnowTimeU3E__0_0(),
	U3CwaitU3Ec__Iterator1_t1361298989::get_offset_of_time_1(),
	U3CwaitU3Ec__Iterator1_t1361298989::get_offset_of_U24current_2(),
	U3CwaitU3Ec__Iterator1_t1361298989::get_offset_of_U24disposing_3(),
	U3CwaitU3Ec__Iterator1_t1361298989::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6204 = { sizeof (Drag_Controller_t1925480236), -1, sizeof(Drag_Controller_t1925480236_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6204[23] = 
{
	Drag_Controller_t1925480236::get_offset_of_LOCKING_2(),
	Drag_Controller_t1925480236::get_offset_of_complete_3(),
	Drag_Controller_t1925480236::get_offset_of_useClick_4(),
	Drag_Controller_t1925480236::get_offset_of_dontFixPosition_5(),
	Drag_Controller_t1925480236::get_offset_of_NeedToDestroy_6(),
	Drag_Controller_t1925480236::get_offset_of_already_did_7(),
	Drag_Controller_t1925480236::get_offset_of_Events_8(),
	Drag_Controller_t1925480236::get_offset_of_reset_pos_9(),
	Drag_Controller_t1925480236::get_offset_of_target_10(),
	Drag_Controller_t1925480236::get_offset_of_screenPoint_11(),
	Drag_Controller_t1925480236::get_offset_of_offset_12(),
	Drag_Controller_t1925480236::get_offset_of_do_Once_13(),
	Drag_Controller_t1925480236::get_offset_of__highlighter_14(),
	Drag_Controller_t1925480236::get_offset_of_SR_15(),
	Drag_Controller_t1925480236::get_offset_of_MySprite_16(),
	Drag_Controller_t1925480236::get_offset_of_level4background_17(),
	Drag_Controller_t1925480236::get_offset_of_level4backgroundSpriteArray_18(),
	Drag_Controller_t1925480236::get_offset_of_level5TrueImage_19(),
	Drag_Controller_t1925480236::get_offset_of_level5FalseImage_20(),
	Drag_Controller_t1925480236::get_offset_of_sw_21(),
	Drag_Controller_t1925480236::get_offset_of_myCount_22(),
	Drag_Controller_t1925480236_StaticFields::get_offset_of_nowPicCount_23(),
	Drag_Controller_t1925480236::get_offset_of_picking_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6205 = { sizeof (U3CU9EDEU5C0DU4E86U3Ec__Iterator0_t1244618071), -1, sizeof(U3CU9EDEU5C0DU4E86U3Ec__Iterator0_t1244618071_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6205[7] = 
{
	U3CU9EDEU5C0DU4E86U3Ec__Iterator0_t1244618071::get_offset_of_U24this_0(),
	U3CU9EDEU5C0DU4E86U3Ec__Iterator0_t1244618071::get_offset_of_U24current_1(),
	U3CU9EDEU5C0DU4E86U3Ec__Iterator0_t1244618071::get_offset_of_U24disposing_2(),
	U3CU9EDEU5C0DU4E86U3Ec__Iterator0_t1244618071::get_offset_of_U24PC_3(),
	U3CU9EDEU5C0DU4E86U3Ec__Iterator0_t1244618071_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	U3CU9EDEU5C0DU4E86U3Ec__Iterator0_t1244618071_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	U3CU9EDEU5C0DU4E86U3Ec__Iterator0_t1244618071_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6206 = { sizeof (U3CU9EDEU932FU4E86U3Ec__Iterator1_t2446876263), -1, sizeof(U3CU9EDEU932FU4E86U3Ec__Iterator1_t2446876263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6206[7] = 
{
	U3CU9EDEU932FU4E86U3Ec__Iterator1_t2446876263::get_offset_of_U3CsU3E__0_0(),
	U3CU9EDEU932FU4E86U3Ec__Iterator1_t2446876263::get_offset_of_U24this_1(),
	U3CU9EDEU932FU4E86U3Ec__Iterator1_t2446876263::get_offset_of_U24current_2(),
	U3CU9EDEU932FU4E86U3Ec__Iterator1_t2446876263::get_offset_of_U24disposing_3(),
	U3CU9EDEU932FU4E86U3Ec__Iterator1_t2446876263::get_offset_of_U24PC_4(),
	U3CU9EDEU932FU4E86U3Ec__Iterator1_t2446876263_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
	U3CU9EDEU932FU4E86U3Ec__Iterator1_t2446876263_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6207 = { sizeof (Level_01_Controller_t3780315112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6207[16] = 
{
	Level_01_Controller_t3780315112::get_offset_of_lvManager_2(),
	Level_01_Controller_t3780315112::get_offset_of_all_unShow_Objs_3(),
	Level_01_Controller_t3780315112::get_offset_of_all_Objs_4(),
	Level_01_Controller_t3780315112::get_offset_of_chapter_01_5(),
	Level_01_Controller_t3780315112::get_offset_of_chapter_02_6(),
	Level_01_Controller_t3780315112::get_offset_of_LV1tips_7(),
	Level_01_Controller_t3780315112::get_offset_of_win_panel_8(),
	Level_01_Controller_t3780315112::get_offset_of_good_road_9(),
	Level_01_Controller_t3780315112::get_offset_of_good_water_10(),
	Level_01_Controller_t3780315112::get_offset_of_Evaporation_11(),
	Level_01_Controller_t3780315112::get_offset_of_Evaportranspiration_12(),
	Level_01_Controller_t3780315112::get_offset_of_Infiltration_13(),
	Level_01_Controller_t3780315112::get_offset_of_tip_1_14(),
	Level_01_Controller_t3780315112::get_offset_of_tip_2_15(),
	Level_01_Controller_t3780315112::get_offset_of_do_once_16(),
	Level_01_Controller_t3780315112::get_offset_of_do_once2_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6208 = { sizeof (Level_02_Controller_t1742187031), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6208[10] = 
{
	Level_02_Controller_t1742187031::get_offset_of_lvManager_2(),
	Level_02_Controller_t1742187031::get_offset_of_all_unShow_Objs_3(),
	Level_02_Controller_t1742187031::get_offset_of_all_Objs_4(),
	Level_02_Controller_t1742187031::get_offset_of_chapter_01_5(),
	Level_02_Controller_t1742187031::get_offset_of_win_panel_6(),
	Level_02_Controller_t1742187031::get_offset_of_good_1_7(),
	Level_02_Controller_t1742187031::get_offset_of_good_2_8(),
	Level_02_Controller_t1742187031::get_offset_of_good_3_9(),
	Level_02_Controller_t1742187031::get_offset_of_tip_1_10(),
	Level_02_Controller_t1742187031::get_offset_of_do_once_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6209 = { sizeof (Level_03_Controller_t697859203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6209[4] = 
{
	Level_03_Controller_t697859203::get_offset_of_lvManager_2(),
	Level_03_Controller_t697859203::get_offset_of_all_bad_car_3(),
	Level_03_Controller_t697859203::get_offset_of_do_once_4(),
	Level_03_Controller_t697859203::get_offset_of_win_panel_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6210 = { sizeof (Level_03_Controller_ICON_t1624219477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6210[1] = 
{
	Level_03_Controller_ICON_t1624219477::get_offset_of_cars_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6211 = { sizeof (Level_04_Controller_t985329950), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6211[5] = 
{
	Level_04_Controller_t985329950::get_offset_of_lvManager_2(),
	Level_04_Controller_t985329950::get_offset_of_all_word_3(),
	Level_04_Controller_t985329950::get_offset_of_all_line_4(),
	Level_04_Controller_t985329950::get_offset_of_do_once_5(),
	Level_04_Controller_t985329950::get_offset_of_win_panel_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6212 = { sizeof (Level_05_Controller_t1454764318), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6212[5] = 
{
	Level_05_Controller_t1454764318::get_offset_of_lvManager_2(),
	Level_05_Controller_t1454764318::get_offset_of_completeParticle_3(),
	Level_05_Controller_t1454764318::get_offset_of_all_image_4(),
	Level_05_Controller_t1454764318::get_offset_of_do_once_5(),
	Level_05_Controller_t1454764318::get_offset_of_win_panel_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6213 = { sizeof (U3CdoWinEffectU3Ec__Iterator0_t374873640), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6213[4] = 
{
	U3CdoWinEffectU3Ec__Iterator0_t374873640::get_offset_of_U24this_0(),
	U3CdoWinEffectU3Ec__Iterator0_t374873640::get_offset_of_U24current_1(),
	U3CdoWinEffectU3Ec__Iterator0_t374873640::get_offset_of_U24disposing_2(),
	U3CdoWinEffectU3Ec__Iterator0_t374873640::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6214 = { sizeof (Level_05_Controller_ICON_t491541153), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6214[1] = 
{
	Level_05_Controller_ICON_t491541153::get_offset_of_image_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6215 = { sizeof (Level_06_Controller_t3711603533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6215[6] = 
{
	Level_06_Controller_t3711603533::get_offset_of_star_Images_2(),
	Level_06_Controller_t3711603533::get_offset_of_Parent_obj_3(),
	Level_06_Controller_t3711603533::get_offset_of_Shinning_4(),
	Level_06_Controller_t3711603533::get_offset_of_Shinning_speed_5(),
	Level_06_Controller_t3711603533::get_offset_of_black_6(),
	Level_06_Controller_t3711603533::get_offset_of_op_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6216 = { sizeof (U3CStartImageU3Ec__Iterator0_t1459609615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6216[5] = 
{
	U3CStartImageU3Ec__Iterator0_t1459609615::get_offset_of_U3CiU3E__1_0(),
	U3CStartImageU3Ec__Iterator0_t1459609615::get_offset_of_U24this_1(),
	U3CStartImageU3Ec__Iterator0_t1459609615::get_offset_of_U24current_2(),
	U3CStartImageU3Ec__Iterator0_t1459609615::get_offset_of_U24disposing_3(),
	U3CStartImageU3Ec__Iterator0_t1459609615::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6217 = { sizeof (U3CFadeToBookingU3Ec__Iterator1_t3508591351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6217[4] = 
{
	U3CFadeToBookingU3Ec__Iterator1_t3508591351::get_offset_of_U24this_0(),
	U3CFadeToBookingU3Ec__Iterator1_t3508591351::get_offset_of_U24current_1(),
	U3CFadeToBookingU3Ec__Iterator1_t3508591351::get_offset_of_U24disposing_2(),
	U3CFadeToBookingU3Ec__Iterator1_t3508591351::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6218 = { sizeof (LookAt_t2949065266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6218[1] = 
{
	LookAt_t2949065266::get_offset_of_target_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6219 = { sizeof (CameraFilterPack_Atmosphere_Rain_Pro_t3269395514), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6219[13] = 
{
	CameraFilterPack_Atmosphere_Rain_Pro_t3269395514::get_offset_of_SCShader_2(),
	CameraFilterPack_Atmosphere_Rain_Pro_t3269395514::get_offset_of_TimeX_3(),
	CameraFilterPack_Atmosphere_Rain_Pro_t3269395514::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Atmosphere_Rain_Pro_t3269395514::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Atmosphere_Rain_Pro_t3269395514::get_offset_of_Fade_6(),
	CameraFilterPack_Atmosphere_Rain_Pro_t3269395514::get_offset_of_Intensity_7(),
	CameraFilterPack_Atmosphere_Rain_Pro_t3269395514::get_offset_of_DirectionX_8(),
	CameraFilterPack_Atmosphere_Rain_Pro_t3269395514::get_offset_of_Size_9(),
	CameraFilterPack_Atmosphere_Rain_Pro_t3269395514::get_offset_of_Speed_10(),
	CameraFilterPack_Atmosphere_Rain_Pro_t3269395514::get_offset_of_Distortion_11(),
	CameraFilterPack_Atmosphere_Rain_Pro_t3269395514::get_offset_of_StormFlashOnOff_12(),
	CameraFilterPack_Atmosphere_Rain_Pro_t3269395514::get_offset_of_DropOnOff_13(),
	CameraFilterPack_Atmosphere_Rain_Pro_t3269395514::get_offset_of_Texture2_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6220 = { sizeof (Col_to_add_t3844769183), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6220[1] = 
{
	Col_to_add_t3844769183::get_offset_of_Events_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6221 = { sizeof (Dc_t164372262), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6222 = { sizeof (Drop_t4098387221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6222[5] = 
{
	Drop_t4098387221::get_offset_of_Image_2(),
	Drop_t4098387221::get_offset_of_Cube_3(),
	Drop_t4098387221::get_offset_of_Position_4(),
	Drop_t4098387221::get_offset_of_ThisCamera_5(),
	Drop_t4098387221::get_offset_of_Targetlayer_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6223 = { sizeof (SpriteOutline_t3043537775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6223[2] = 
{
	SpriteOutline_t3043537775::get_offset_of_color_2(),
	SpriteOutline_t3043537775::get_offset_of_spriteRenderer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6224 = { sizeof (TimeGame_t3115909209), -1, sizeof(TimeGame_t3115909209_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6224[13] = 
{
	TimeGame_t3115909209::get_offset_of_Left_time_2(),
	TimeGame_t3115909209::get_offset_of_Left_timeMax_3(),
	TimeGame_t3115909209::get_offset_of_checkObj_4(),
	TimeGame_t3115909209::get_offset_of_im_5(),
	TimeGame_t3115909209::get_offset_of_timeText_6(),
	TimeGame_t3115909209::get_offset_of_Clock_7(),
	TimeGame_t3115909209::get_offset_of_OpenIt_8(),
	TimeGame_t3115909209::get_offset_of_Events_9(),
	TimeGame_t3115909209::get_offset_of_ReplayTips_10(),
	TimeGame_t3115909209::get_offset_of_RC_Pos_11(),
	TimeGame_t3115909209::get_offset_of_Rock_Once_12(),
	TimeGame_t3115909209::get_offset_of_Level_05_13(),
	TimeGame_t3115909209_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6225 = { sizeof (U3CRockStartU3Ec__Iterator0_t2048428861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6225[4] = 
{
	U3CRockStartU3Ec__Iterator0_t2048428861::get_offset_of_U24this_0(),
	U3CRockStartU3Ec__Iterator0_t2048428861::get_offset_of_U24current_1(),
	U3CRockStartU3Ec__Iterator0_t2048428861::get_offset_of_U24disposing_2(),
	U3CRockStartU3Ec__Iterator0_t2048428861::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6226 = { sizeof (U3CResetU3Ec__Iterator1_t3538958262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6226[4] = 
{
	U3CResetU3Ec__Iterator1_t3538958262::get_offset_of_U24this_0(),
	U3CResetU3Ec__Iterator1_t3538958262::get_offset_of_U24current_1(),
	U3CResetU3Ec__Iterator1_t3538958262::get_offset_of_U24disposing_2(),
	U3CResetU3Ec__Iterator1_t3538958262::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6227 = { sizeof (TimeLevel4_t4195116982), -1, sizeof(TimeLevel4_t4195116982_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6227[15] = 
{
	TimeLevel4_t4195116982::get_offset_of_Left_time_2(),
	TimeLevel4_t4195116982::get_offset_of_Left_timeMax_3(),
	TimeLevel4_t4195116982::get_offset_of_checkObj_4(),
	TimeLevel4_t4195116982::get_offset_of_im_5(),
	TimeLevel4_t4195116982::get_offset_of_timeText_6(),
	TimeLevel4_t4195116982::get_offset_of_Clock_7(),
	TimeLevel4_t4195116982::get_offset_of_OpenIt_8(),
	TimeLevel4_t4195116982::get_offset_of_boxCut_9(),
	TimeLevel4_t4195116982::get_offset_of_boxAll_10(),
	TimeLevel4_t4195116982::get_offset_of_DC_all_11(),
	TimeLevel4_t4195116982::get_offset_of_Events_12(),
	TimeLevel4_t4195116982::get_offset_of_ReplayTips_13(),
	TimeLevel4_t4195116982::get_offset_of_once_14(),
	TimeLevel4_t4195116982::get_offset_of_Level_04_15(),
	TimeLevel4_t4195116982_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6228 = { sizeof (U3CResetU3Ec__Iterator0_t3688093126), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6228[4] = 
{
	U3CResetU3Ec__Iterator0_t3688093126::get_offset_of_U24this_0(),
	U3CResetU3Ec__Iterator0_t3688093126::get_offset_of_U24current_1(),
	U3CResetU3Ec__Iterator0_t3688093126::get_offset_of_U24disposing_2(),
	U3CResetU3Ec__Iterator0_t3688093126::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6229 = { sizeof (WaitForClose_t2325699134), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6230 = { sizeof (U3CwaitCloseU3Ec__Iterator0_t3687100374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6230[4] = 
{
	U3CwaitCloseU3Ec__Iterator0_t3687100374::get_offset_of_U24this_0(),
	U3CwaitCloseU3Ec__Iterator0_t3687100374::get_offset_of_U24current_1(),
	U3CwaitCloseU3Ec__Iterator0_t3687100374::get_offset_of_U24disposing_2(),
	U3CwaitCloseU3Ec__Iterator0_t3687100374::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6231 = { sizeof (drawBox_t3241306425), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6232 = { sizeof (fade_InOut_t787920925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6232[5] = 
{
	fade_InOut_t787920925::get_offset_of_Black_2(),
	fade_InOut_t787920925::get_offset_of_Run_3(),
	fade_InOut_t787920925::get_offset_of_Speed_4(),
	fade_InOut_t787920925::get_offset_of_Max_5(),
	fade_InOut_t787920925::get_offset_of_temp_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6233 = { sizeof (leve4To2D_t1062310264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6233[5] = 
{
	leve4To2D_t1062310264::get_offset_of_NowChange_2(),
	leve4To2D_t1062310264::get_offset_of_bloodUI_3(),
	leve4To2D_t1062310264::get_offset_of_SR_4(),
	leve4To2D_t1062310264::get_offset_of_MC_5(),
	leve4To2D_t1062310264::get_offset_of_once_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6234 = { sizeof (Play_Level_Music_t2585779503), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6234[1] = 
{
	Play_Level_Music_t2585779503::get_offset_of_audio_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6235 = { sizeof (Point_Controller_t869836145), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6235[5] = 
{
	Point_Controller_t869836145::get_offset_of_IS_level03_2(),
	Point_Controller_t869836145::get_offset_of_already_did_3(),
	Point_Controller_t869836145::get_offset_of_Events_4(),
	Point_Controller_t869836145::get_offset_of_tg_5(),
	Point_Controller_t869836145::get_offset_of_level5Finger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6236 = { sizeof (bt_sound_t3531492350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6236[1] = 
{
	bt_sound_t3531492350::get_offset_of_bt_s_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6237 = { sizeof (notMobile_MapController_t3835353441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6237[2] = 
{
	notMobile_MapController_t3835353441::get_offset_of_map_m_2(),
	notMobile_MapController_t3835353441::get_offset_of_spriteIndex_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6238 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255375), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6238[3] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields::get_offset_of_U24fieldU2D68F00C41318114691E02CD7532ACF69A8DBE23C2_0(),
	U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields::get_offset_of_U24fieldU2D7FB9790B49277F6151D3EB5D555CCF105904DB43_1(),
	U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields::get_offset_of_U24fieldU2DD26A27B5531D6252D57917C90488F9C3F7AF8F98_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6239 = { sizeof (U24ArrayTypeU3D32_t3651253610)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D32_t3651253610 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6240 = { sizeof (U24ArrayTypeU3D580_t353960863)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D580_t353960863 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6241 = { sizeof (U24ArrayTypeU3D8_t3242499063)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D8_t3242499063 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6242 = { sizeof (U3CModuleU3E_t692745559), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6243 = { sizeof (JSHighlighterController_t967495899), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6243[1] = 
{
	JSHighlighterController_t967495899::get_offset_of_h_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6244 = { sizeof (U3CModuleU3E_t692745560), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6245 = { sizeof (PostEffectsBase_t3718631875), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6245[3] = 
{
	PostEffectsBase_t3718631875::get_offset_of_supportHDRTextures_2(),
	PostEffectsBase_t3718631875::get_offset_of_supportDX11_3(),
	PostEffectsBase_t3718631875::get_offset_of_isSupported_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6246 = { sizeof (SunShaftsResolution_t277016971)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6246[4] = 
{
	SunShaftsResolution_t277016971::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6247 = { sizeof (ShaftsScreenBlendMode_t2434095160)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6247[3] = 
{
	ShaftsScreenBlendMode_t2434095160::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6248 = { sizeof (SunShafts_t3552083306), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6248[14] = 
{
	SunShafts_t3552083306::get_offset_of_resolution_5(),
	SunShafts_t3552083306::get_offset_of_screenBlendMode_6(),
	SunShafts_t3552083306::get_offset_of_sunTransform_7(),
	SunShafts_t3552083306::get_offset_of_radialBlurIterations_8(),
	SunShafts_t3552083306::get_offset_of_sunColor_9(),
	SunShafts_t3552083306::get_offset_of_sunShaftBlurRadius_10(),
	SunShafts_t3552083306::get_offset_of_sunShaftIntensity_11(),
	SunShafts_t3552083306::get_offset_of_useSkyBoxAlpha_12(),
	SunShafts_t3552083306::get_offset_of_maxRadius_13(),
	SunShafts_t3552083306::get_offset_of_useDepthTexture_14(),
	SunShafts_t3552083306::get_offset_of_sunShaftsShader_15(),
	SunShafts_t3552083306::get_offset_of_sunShaftsMaterial_16(),
	SunShafts_t3552083306::get_offset_of_simpleClearShader_17(),
	SunShafts_t3552083306::get_offset_of_simpleClearMaterial_18(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
