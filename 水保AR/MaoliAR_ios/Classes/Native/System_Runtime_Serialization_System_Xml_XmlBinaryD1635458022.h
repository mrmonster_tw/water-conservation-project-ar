﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Xml.XmlBinaryDictionaryWriter/<CreateNewPrefix>c__AnonStorey2
struct U3CCreateNewPrefixU3Ec__AnonStorey2_t3974110182;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlBinaryDictionaryWriter/<CreateNewPrefix>c__AnonStorey3
struct  U3CCreateNewPrefixU3Ec__AnonStorey3_t1635458022  : public Il2CppObject
{
public:
	// System.Char System.Xml.XmlBinaryDictionaryWriter/<CreateNewPrefix>c__AnonStorey3::c
	Il2CppChar ___c_0;
	// System.Xml.XmlBinaryDictionaryWriter/<CreateNewPrefix>c__AnonStorey2 System.Xml.XmlBinaryDictionaryWriter/<CreateNewPrefix>c__AnonStorey3::<>f__ref$2
	U3CCreateNewPrefixU3Ec__AnonStorey2_t3974110182 * ___U3CU3Ef__refU242_1;

public:
	inline static int32_t get_offset_of_c_0() { return static_cast<int32_t>(offsetof(U3CCreateNewPrefixU3Ec__AnonStorey3_t1635458022, ___c_0)); }
	inline Il2CppChar get_c_0() const { return ___c_0; }
	inline Il2CppChar* get_address_of_c_0() { return &___c_0; }
	inline void set_c_0(Il2CppChar value)
	{
		___c_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU242_1() { return static_cast<int32_t>(offsetof(U3CCreateNewPrefixU3Ec__AnonStorey3_t1635458022, ___U3CU3Ef__refU242_1)); }
	inline U3CCreateNewPrefixU3Ec__AnonStorey2_t3974110182 * get_U3CU3Ef__refU242_1() const { return ___U3CU3Ef__refU242_1; }
	inline U3CCreateNewPrefixU3Ec__AnonStorey2_t3974110182 ** get_address_of_U3CU3Ef__refU242_1() { return &___U3CU3Ef__refU242_1; }
	inline void set_U3CU3Ef__refU242_1(U3CCreateNewPrefixU3Ec__AnonStorey2_t3974110182 * value)
	{
		___U3CU3Ef__refU242_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU242_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
