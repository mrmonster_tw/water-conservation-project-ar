﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeExpression2166265795.h"

// System.CodeDom.CodeTypeReference
struct CodeTypeReference_t3809997434;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeTypeOfExpression
struct  CodeTypeOfExpression_t2266700964  : public CodeExpression_t2166265795
{
public:
	// System.CodeDom.CodeTypeReference System.CodeDom.CodeTypeOfExpression::type
	CodeTypeReference_t3809997434 * ___type_1;

public:
	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(CodeTypeOfExpression_t2266700964, ___type_1)); }
	inline CodeTypeReference_t3809997434 * get_type_1() const { return ___type_1; }
	inline CodeTypeReference_t3809997434 ** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(CodeTypeReference_t3809997434 * value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier(&___type_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
