﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_DateTime3738529785.h"

// System.IO.FileSystemWatcher
struct FileSystemWatcher_t416760199;
// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t1853889766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.DefaultWatcherData
struct  DefaultWatcherData_t3310779826  : public Il2CppObject
{
public:
	// System.IO.FileSystemWatcher System.IO.DefaultWatcherData::FSW
	FileSystemWatcher_t416760199 * ___FSW_0;
	// System.String System.IO.DefaultWatcherData::Directory
	String_t* ___Directory_1;
	// System.String System.IO.DefaultWatcherData::FileMask
	String_t* ___FileMask_2;
	// System.Boolean System.IO.DefaultWatcherData::IncludeSubdirs
	bool ___IncludeSubdirs_3;
	// System.Boolean System.IO.DefaultWatcherData::Enabled
	bool ___Enabled_4;
	// System.Boolean System.IO.DefaultWatcherData::NoWildcards
	bool ___NoWildcards_5;
	// System.DateTime System.IO.DefaultWatcherData::DisabledTime
	DateTime_t3738529785  ___DisabledTime_6;
	// System.Collections.Hashtable System.IO.DefaultWatcherData::Files
	Hashtable_t1853889766 * ___Files_7;

public:
	inline static int32_t get_offset_of_FSW_0() { return static_cast<int32_t>(offsetof(DefaultWatcherData_t3310779826, ___FSW_0)); }
	inline FileSystemWatcher_t416760199 * get_FSW_0() const { return ___FSW_0; }
	inline FileSystemWatcher_t416760199 ** get_address_of_FSW_0() { return &___FSW_0; }
	inline void set_FSW_0(FileSystemWatcher_t416760199 * value)
	{
		___FSW_0 = value;
		Il2CppCodeGenWriteBarrier(&___FSW_0, value);
	}

	inline static int32_t get_offset_of_Directory_1() { return static_cast<int32_t>(offsetof(DefaultWatcherData_t3310779826, ___Directory_1)); }
	inline String_t* get_Directory_1() const { return ___Directory_1; }
	inline String_t** get_address_of_Directory_1() { return &___Directory_1; }
	inline void set_Directory_1(String_t* value)
	{
		___Directory_1 = value;
		Il2CppCodeGenWriteBarrier(&___Directory_1, value);
	}

	inline static int32_t get_offset_of_FileMask_2() { return static_cast<int32_t>(offsetof(DefaultWatcherData_t3310779826, ___FileMask_2)); }
	inline String_t* get_FileMask_2() const { return ___FileMask_2; }
	inline String_t** get_address_of_FileMask_2() { return &___FileMask_2; }
	inline void set_FileMask_2(String_t* value)
	{
		___FileMask_2 = value;
		Il2CppCodeGenWriteBarrier(&___FileMask_2, value);
	}

	inline static int32_t get_offset_of_IncludeSubdirs_3() { return static_cast<int32_t>(offsetof(DefaultWatcherData_t3310779826, ___IncludeSubdirs_3)); }
	inline bool get_IncludeSubdirs_3() const { return ___IncludeSubdirs_3; }
	inline bool* get_address_of_IncludeSubdirs_3() { return &___IncludeSubdirs_3; }
	inline void set_IncludeSubdirs_3(bool value)
	{
		___IncludeSubdirs_3 = value;
	}

	inline static int32_t get_offset_of_Enabled_4() { return static_cast<int32_t>(offsetof(DefaultWatcherData_t3310779826, ___Enabled_4)); }
	inline bool get_Enabled_4() const { return ___Enabled_4; }
	inline bool* get_address_of_Enabled_4() { return &___Enabled_4; }
	inline void set_Enabled_4(bool value)
	{
		___Enabled_4 = value;
	}

	inline static int32_t get_offset_of_NoWildcards_5() { return static_cast<int32_t>(offsetof(DefaultWatcherData_t3310779826, ___NoWildcards_5)); }
	inline bool get_NoWildcards_5() const { return ___NoWildcards_5; }
	inline bool* get_address_of_NoWildcards_5() { return &___NoWildcards_5; }
	inline void set_NoWildcards_5(bool value)
	{
		___NoWildcards_5 = value;
	}

	inline static int32_t get_offset_of_DisabledTime_6() { return static_cast<int32_t>(offsetof(DefaultWatcherData_t3310779826, ___DisabledTime_6)); }
	inline DateTime_t3738529785  get_DisabledTime_6() const { return ___DisabledTime_6; }
	inline DateTime_t3738529785 * get_address_of_DisabledTime_6() { return &___DisabledTime_6; }
	inline void set_DisabledTime_6(DateTime_t3738529785  value)
	{
		___DisabledTime_6 = value;
	}

	inline static int32_t get_offset_of_Files_7() { return static_cast<int32_t>(offsetof(DefaultWatcherData_t3310779826, ___Files_7)); }
	inline Hashtable_t1853889766 * get_Files_7() const { return ___Files_7; }
	inline Hashtable_t1853889766 ** get_address_of_Files_7() { return &___Files_7; }
	inline void set_Files_7(Hashtable_t1853889766 * value)
	{
		___Files_7 = value;
		Il2CppCodeGenWriteBarrier(&___Files_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
