﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"
#include "mscorlib_System_IntPtr840150181.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FAMConnection
struct  FAMConnection_t1678998633 
{
public:
	// System.Int32 System.IO.FAMConnection::FD
	int32_t ___FD_0;
	// System.IntPtr System.IO.FAMConnection::opaque
	IntPtr_t ___opaque_1;

public:
	inline static int32_t get_offset_of_FD_0() { return static_cast<int32_t>(offsetof(FAMConnection_t1678998633, ___FD_0)); }
	inline int32_t get_FD_0() const { return ___FD_0; }
	inline int32_t* get_address_of_FD_0() { return &___FD_0; }
	inline void set_FD_0(int32_t value)
	{
		___FD_0 = value;
	}

	inline static int32_t get_offset_of_opaque_1() { return static_cast<int32_t>(offsetof(FAMConnection_t1678998633, ___opaque_1)); }
	inline IntPtr_t get_opaque_1() const { return ___opaque_1; }
	inline IntPtr_t* get_address_of_opaque_1() { return &___opaque_1; }
	inline void set_opaque_1(IntPtr_t value)
	{
		___opaque_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
