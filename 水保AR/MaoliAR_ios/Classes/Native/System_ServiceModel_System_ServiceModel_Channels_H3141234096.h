﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_R1473296135.h"

// System.ServiceModel.Channels.Message
struct Message_t869514973;
// System.ServiceModel.Channels.HttpReplyChannel
struct HttpReplyChannel_t3854184778;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.HttpRequestContextBase
struct  HttpRequestContextBase_t3141234096  : public RequestContext_t1473296135
{
public:
	// System.ServiceModel.Channels.Message System.ServiceModel.Channels.HttpRequestContextBase::request
	Message_t869514973 * ___request_0;
	// System.ServiceModel.Channels.HttpReplyChannel System.ServiceModel.Channels.HttpRequestContextBase::channel
	HttpReplyChannel_t3854184778 * ___channel_1;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(HttpRequestContextBase_t3141234096, ___request_0)); }
	inline Message_t869514973 * get_request_0() const { return ___request_0; }
	inline Message_t869514973 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(Message_t869514973 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier(&___request_0, value);
	}

	inline static int32_t get_offset_of_channel_1() { return static_cast<int32_t>(offsetof(HttpRequestContextBase_t3141234096, ___channel_1)); }
	inline HttpReplyChannel_t3854184778 * get_channel_1() const { return ___channel_1; }
	inline HttpReplyChannel_t3854184778 ** get_address_of_channel_1() { return &___channel_1; }
	inline void set_channel_1(HttpReplyChannel_t3854184778 * value)
	{
		___channel_1 = value;
		Il2CppCodeGenWriteBarrier(&___channel_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
