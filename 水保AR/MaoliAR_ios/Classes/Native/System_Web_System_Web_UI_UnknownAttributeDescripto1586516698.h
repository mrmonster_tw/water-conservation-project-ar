﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.UnknownAttributeDescriptor
struct  UnknownAttributeDescriptor_t1586516698  : public Il2CppObject
{
public:
	// System.Reflection.MemberInfo System.Web.UI.UnknownAttributeDescriptor::Info
	MemberInfo_t * ___Info_0;
	// System.Object System.Web.UI.UnknownAttributeDescriptor::Value
	Il2CppObject * ___Value_1;

public:
	inline static int32_t get_offset_of_Info_0() { return static_cast<int32_t>(offsetof(UnknownAttributeDescriptor_t1586516698, ___Info_0)); }
	inline MemberInfo_t * get_Info_0() const { return ___Info_0; }
	inline MemberInfo_t ** get_address_of_Info_0() { return &___Info_0; }
	inline void set_Info_0(MemberInfo_t * value)
	{
		___Info_0 = value;
		Il2CppCodeGenWriteBarrier(&___Info_0, value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(UnknownAttributeDescriptor_t1586516698, ___Value_1)); }
	inline Il2CppObject * get_Value_1() const { return ___Value_1; }
	inline Il2CppObject ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(Il2CppObject * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier(&___Value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
