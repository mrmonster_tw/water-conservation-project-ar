﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.MessageFault
struct  MessageFault_t929740066  : public Il2CppObject
{
public:

public:
};

struct MessageFault_t929740066_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Channels.MessageFault::<>f__switch$map3
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map3_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Channels.MessageFault::<>f__switch$map4
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map4_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.Channels.MessageFault::<>f__switch$map5
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map5_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map3_0() { return static_cast<int32_t>(offsetof(MessageFault_t929740066_StaticFields, ___U3CU3Ef__switchU24map3_0)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map3_0() const { return ___U3CU3Ef__switchU24map3_0; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map3_0() { return &___U3CU3Ef__switchU24map3_0; }
	inline void set_U3CU3Ef__switchU24map3_0(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map3_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map3_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4_1() { return static_cast<int32_t>(offsetof(MessageFault_t929740066_StaticFields, ___U3CU3Ef__switchU24map4_1)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map4_1() const { return ___U3CU3Ef__switchU24map4_1; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map4_1() { return &___U3CU3Ef__switchU24map4_1; }
	inline void set_U3CU3Ef__switchU24map4_1(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map4_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map4_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map5_2() { return static_cast<int32_t>(offsetof(MessageFault_t929740066_StaticFields, ___U3CU3Ef__switchU24map5_2)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map5_2() const { return ___U3CU3Ef__switchU24map5_2; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map5_2() { return &___U3CU3Ef__switchU24map5_2; }
	inline void set_U3CU3Ef__switchU24map5_2(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map5_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map5_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
