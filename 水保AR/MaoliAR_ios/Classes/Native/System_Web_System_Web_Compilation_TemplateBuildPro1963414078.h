﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_Compilation_GenericBuildProvi244322090.h"

// System.Collections.Generic.SortedDictionary`2<System.String,System.Web.Compilation.TemplateBuildProvider/ExtractDirectiveDependencies>
struct SortedDictionary_2_t3232237297;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.SortedDictionary`2<System.String,System.Boolean>
struct SortedDictionary_2_t1305064559;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.TemplateBuildProvider
struct  TemplateBuildProvider_t1963414078  : public GenericBuildProvider_1_t244322090
{
public:
	// System.Collections.Generic.SortedDictionary`2<System.String,System.Boolean> System.Web.Compilation.TemplateBuildProvider::dependencies
	SortedDictionary_2_t1305064559 * ___dependencies_11;
	// System.String System.Web.Compilation.TemplateBuildProvider::compilationLanguage
	String_t* ___compilationLanguage_12;

public:
	inline static int32_t get_offset_of_dependencies_11() { return static_cast<int32_t>(offsetof(TemplateBuildProvider_t1963414078, ___dependencies_11)); }
	inline SortedDictionary_2_t1305064559 * get_dependencies_11() const { return ___dependencies_11; }
	inline SortedDictionary_2_t1305064559 ** get_address_of_dependencies_11() { return &___dependencies_11; }
	inline void set_dependencies_11(SortedDictionary_2_t1305064559 * value)
	{
		___dependencies_11 = value;
		Il2CppCodeGenWriteBarrier(&___dependencies_11, value);
	}

	inline static int32_t get_offset_of_compilationLanguage_12() { return static_cast<int32_t>(offsetof(TemplateBuildProvider_t1963414078, ___compilationLanguage_12)); }
	inline String_t* get_compilationLanguage_12() const { return ___compilationLanguage_12; }
	inline String_t** get_address_of_compilationLanguage_12() { return &___compilationLanguage_12; }
	inline void set_compilationLanguage_12(String_t* value)
	{
		___compilationLanguage_12 = value;
		Il2CppCodeGenWriteBarrier(&___compilationLanguage_12, value);
	}
};

struct TemplateBuildProvider_t1963414078_StaticFields
{
public:
	// System.Collections.Generic.SortedDictionary`2<System.String,System.Web.Compilation.TemplateBuildProvider/ExtractDirectiveDependencies> System.Web.Compilation.TemplateBuildProvider::directiveAttributes
	SortedDictionary_2_t3232237297 * ___directiveAttributes_9;
	// System.Char[] System.Web.Compilation.TemplateBuildProvider::directiveValueTrimChars
	CharU5BU5D_t3528271667* ___directiveValueTrimChars_10;

public:
	inline static int32_t get_offset_of_directiveAttributes_9() { return static_cast<int32_t>(offsetof(TemplateBuildProvider_t1963414078_StaticFields, ___directiveAttributes_9)); }
	inline SortedDictionary_2_t3232237297 * get_directiveAttributes_9() const { return ___directiveAttributes_9; }
	inline SortedDictionary_2_t3232237297 ** get_address_of_directiveAttributes_9() { return &___directiveAttributes_9; }
	inline void set_directiveAttributes_9(SortedDictionary_2_t3232237297 * value)
	{
		___directiveAttributes_9 = value;
		Il2CppCodeGenWriteBarrier(&___directiveAttributes_9, value);
	}

	inline static int32_t get_offset_of_directiveValueTrimChars_10() { return static_cast<int32_t>(offsetof(TemplateBuildProvider_t1963414078_StaticFields, ___directiveValueTrimChars_10)); }
	inline CharU5BU5D_t3528271667* get_directiveValueTrimChars_10() const { return ___directiveValueTrimChars_10; }
	inline CharU5BU5D_t3528271667** get_address_of_directiveValueTrimChars_10() { return &___directiveValueTrimChars_10; }
	inline void set_directiveValueTrimChars_10(CharU5BU5D_t3528271667* value)
	{
		___directiveValueTrimChars_10 = value;
		Il2CppCodeGenWriteBarrier(&___directiveValueTrimChars_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
