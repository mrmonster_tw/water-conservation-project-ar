﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Tokens_B3487952943.h"

// System.String
struct String_t;
// System.IdentityModel.Tokens.SecurityKeyIdentifier
struct SecurityKeyIdentifier_t806165896;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Tokens.EncryptedKeyIdentifierClause
struct  EncryptedKeyIdentifierClause_t2037270612  : public BinaryKeyIdentifierClause_t3487952943
{
public:
	// System.String System.IdentityModel.Tokens.EncryptedKeyIdentifierClause::carried_key_name
	String_t* ___carried_key_name_4;
	// System.String System.IdentityModel.Tokens.EncryptedKeyIdentifierClause::enc_method
	String_t* ___enc_method_5;
	// System.IdentityModel.Tokens.SecurityKeyIdentifier System.IdentityModel.Tokens.EncryptedKeyIdentifierClause::identifier
	SecurityKeyIdentifier_t806165896 * ___identifier_6;

public:
	inline static int32_t get_offset_of_carried_key_name_4() { return static_cast<int32_t>(offsetof(EncryptedKeyIdentifierClause_t2037270612, ___carried_key_name_4)); }
	inline String_t* get_carried_key_name_4() const { return ___carried_key_name_4; }
	inline String_t** get_address_of_carried_key_name_4() { return &___carried_key_name_4; }
	inline void set_carried_key_name_4(String_t* value)
	{
		___carried_key_name_4 = value;
		Il2CppCodeGenWriteBarrier(&___carried_key_name_4, value);
	}

	inline static int32_t get_offset_of_enc_method_5() { return static_cast<int32_t>(offsetof(EncryptedKeyIdentifierClause_t2037270612, ___enc_method_5)); }
	inline String_t* get_enc_method_5() const { return ___enc_method_5; }
	inline String_t** get_address_of_enc_method_5() { return &___enc_method_5; }
	inline void set_enc_method_5(String_t* value)
	{
		___enc_method_5 = value;
		Il2CppCodeGenWriteBarrier(&___enc_method_5, value);
	}

	inline static int32_t get_offset_of_identifier_6() { return static_cast<int32_t>(offsetof(EncryptedKeyIdentifierClause_t2037270612, ___identifier_6)); }
	inline SecurityKeyIdentifier_t806165896 * get_identifier_6() const { return ___identifier_6; }
	inline SecurityKeyIdentifier_t806165896 ** get_address_of_identifier_6() { return &___identifier_6; }
	inline void set_identifier_6(SecurityKeyIdentifier_t806165896 * value)
	{
		___identifier_6 = value;
		Il2CppCodeGenWriteBarrier(&___identifier_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
