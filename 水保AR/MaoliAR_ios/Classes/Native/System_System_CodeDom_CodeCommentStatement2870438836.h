﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeStatement371410868.h"

// System.CodeDom.CodeComment
struct CodeComment_t1985552863;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeCommentStatement
struct  CodeCommentStatement_t2870438836  : public CodeStatement_t371410868
{
public:
	// System.CodeDom.CodeComment System.CodeDom.CodeCommentStatement::comment
	CodeComment_t1985552863 * ___comment_4;

public:
	inline static int32_t get_offset_of_comment_4() { return static_cast<int32_t>(offsetof(CodeCommentStatement_t2870438836, ___comment_4)); }
	inline CodeComment_t1985552863 * get_comment_4() const { return ___comment_4; }
	inline CodeComment_t1985552863 ** get_address_of_comment_4() { return &___comment_4; }
	inline void set_comment_4(CodeComment_t1985552863 * value)
	{
		___comment_4 = value;
		Il2CppCodeGenWriteBarrier(&___comment_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
