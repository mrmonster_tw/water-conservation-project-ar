﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.CodeDom.Compiler.IndentedTextWriter
struct IndentedTextWriter_t2995409896;
// System.CodeDom.Compiler.CodeGeneratorOptions
struct CodeGeneratorOptions_t1601321324;
// System.CodeDom.CodeTypeMember
struct CodeTypeMember_t1555525554;
// System.CodeDom.CodeTypeDeclaration
struct CodeTypeDeclaration_t2359234283;
// System.CodeDom.Compiler.CodeGenerator/Visitor
struct Visitor_t4039484693;
// System.Type[]
struct TypeU5BU5D_t3940880105;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.Compiler.CodeGenerator
struct  CodeGenerator_t1606279797  : public Il2CppObject
{
public:
	// System.CodeDom.Compiler.IndentedTextWriter System.CodeDom.Compiler.CodeGenerator::output
	IndentedTextWriter_t2995409896 * ___output_0;
	// System.CodeDom.Compiler.CodeGeneratorOptions System.CodeDom.Compiler.CodeGenerator::options
	CodeGeneratorOptions_t1601321324 * ___options_1;
	// System.CodeDom.CodeTypeMember System.CodeDom.Compiler.CodeGenerator::currentMember
	CodeTypeMember_t1555525554 * ___currentMember_2;
	// System.CodeDom.CodeTypeDeclaration System.CodeDom.Compiler.CodeGenerator::currentType
	CodeTypeDeclaration_t2359234283 * ___currentType_3;
	// System.CodeDom.Compiler.CodeGenerator/Visitor System.CodeDom.Compiler.CodeGenerator::visitor
	Visitor_t4039484693 * ___visitor_4;

public:
	inline static int32_t get_offset_of_output_0() { return static_cast<int32_t>(offsetof(CodeGenerator_t1606279797, ___output_0)); }
	inline IndentedTextWriter_t2995409896 * get_output_0() const { return ___output_0; }
	inline IndentedTextWriter_t2995409896 ** get_address_of_output_0() { return &___output_0; }
	inline void set_output_0(IndentedTextWriter_t2995409896 * value)
	{
		___output_0 = value;
		Il2CppCodeGenWriteBarrier(&___output_0, value);
	}

	inline static int32_t get_offset_of_options_1() { return static_cast<int32_t>(offsetof(CodeGenerator_t1606279797, ___options_1)); }
	inline CodeGeneratorOptions_t1601321324 * get_options_1() const { return ___options_1; }
	inline CodeGeneratorOptions_t1601321324 ** get_address_of_options_1() { return &___options_1; }
	inline void set_options_1(CodeGeneratorOptions_t1601321324 * value)
	{
		___options_1 = value;
		Il2CppCodeGenWriteBarrier(&___options_1, value);
	}

	inline static int32_t get_offset_of_currentMember_2() { return static_cast<int32_t>(offsetof(CodeGenerator_t1606279797, ___currentMember_2)); }
	inline CodeTypeMember_t1555525554 * get_currentMember_2() const { return ___currentMember_2; }
	inline CodeTypeMember_t1555525554 ** get_address_of_currentMember_2() { return &___currentMember_2; }
	inline void set_currentMember_2(CodeTypeMember_t1555525554 * value)
	{
		___currentMember_2 = value;
		Il2CppCodeGenWriteBarrier(&___currentMember_2, value);
	}

	inline static int32_t get_offset_of_currentType_3() { return static_cast<int32_t>(offsetof(CodeGenerator_t1606279797, ___currentType_3)); }
	inline CodeTypeDeclaration_t2359234283 * get_currentType_3() const { return ___currentType_3; }
	inline CodeTypeDeclaration_t2359234283 ** get_address_of_currentType_3() { return &___currentType_3; }
	inline void set_currentType_3(CodeTypeDeclaration_t2359234283 * value)
	{
		___currentType_3 = value;
		Il2CppCodeGenWriteBarrier(&___currentType_3, value);
	}

	inline static int32_t get_offset_of_visitor_4() { return static_cast<int32_t>(offsetof(CodeGenerator_t1606279797, ___visitor_4)); }
	inline Visitor_t4039484693 * get_visitor_4() const { return ___visitor_4; }
	inline Visitor_t4039484693 ** get_address_of_visitor_4() { return &___visitor_4; }
	inline void set_visitor_4(Visitor_t4039484693 * value)
	{
		___visitor_4 = value;
		Il2CppCodeGenWriteBarrier(&___visitor_4, value);
	}
};

struct CodeGenerator_t1606279797_StaticFields
{
public:
	// System.Type[] System.CodeDom.Compiler.CodeGenerator::memberTypes
	TypeU5BU5D_t3940880105* ___memberTypes_5;

public:
	inline static int32_t get_offset_of_memberTypes_5() { return static_cast<int32_t>(offsetof(CodeGenerator_t1606279797_StaticFields, ___memberTypes_5)); }
	inline TypeU5BU5D_t3940880105* get_memberTypes_5() const { return ___memberTypes_5; }
	inline TypeU5BU5D_t3940880105** get_address_of_memberTypes_5() { return &___memberTypes_5; }
	inline void set_memberTypes_5(TypeU5BU5D_t3940880105* value)
	{
		___memberTypes_5 = value;
		Il2CppCodeGenWriteBarrier(&___memberTypes_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
