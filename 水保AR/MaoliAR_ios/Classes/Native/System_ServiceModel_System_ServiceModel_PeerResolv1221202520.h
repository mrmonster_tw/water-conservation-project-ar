﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.PeerResolvers.ConnectInfoDC
struct ConnectInfoDC_t2606082360;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.PeerResolvers.ConnectInfo
struct  ConnectInfo_t1221202520  : public Il2CppObject
{
public:
	// System.ServiceModel.PeerResolvers.ConnectInfoDC System.ServiceModel.PeerResolvers.ConnectInfo::dc
	ConnectInfoDC_t2606082360 * ___dc_0;

public:
	inline static int32_t get_offset_of_dc_0() { return static_cast<int32_t>(offsetof(ConnectInfo_t1221202520, ___dc_0)); }
	inline ConnectInfoDC_t2606082360 * get_dc_0() const { return ___dc_0; }
	inline ConnectInfoDC_t2606082360 ** get_address_of_dc_0() { return &___dc_0; }
	inline void set_dc_0(ConnectInfoDC_t2606082360 * value)
	{
		___dc_0 = value;
		Il2CppCodeGenWriteBarrier(&___dc_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
