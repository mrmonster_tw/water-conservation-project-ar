﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_System_IO_FAMConnection1678998633.h"

// System.IO.FAMWatcher
struct FAMWatcher_t3228827479;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Threading.Thread
struct Thread_t2300836069;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FAMWatcher
struct  FAMWatcher_t3228827479  : public Il2CppObject
{
public:

public:
};

struct FAMWatcher_t3228827479_StaticFields
{
public:
	// System.Boolean System.IO.FAMWatcher::failed
	bool ___failed_0;
	// System.IO.FAMWatcher System.IO.FAMWatcher::instance
	FAMWatcher_t3228827479 * ___instance_1;
	// System.Collections.Hashtable System.IO.FAMWatcher::watches
	Hashtable_t1853889766 * ___watches_2;
	// System.Collections.Hashtable System.IO.FAMWatcher::requests
	Hashtable_t1853889766 * ___requests_3;
	// System.IO.FAMConnection System.IO.FAMWatcher::conn
	FAMConnection_t1678998633  ___conn_4;
	// System.Threading.Thread System.IO.FAMWatcher::thread
	Thread_t2300836069 * ___thread_5;
	// System.Boolean System.IO.FAMWatcher::stop
	bool ___stop_6;
	// System.Boolean System.IO.FAMWatcher::use_gamin
	bool ___use_gamin_7;

public:
	inline static int32_t get_offset_of_failed_0() { return static_cast<int32_t>(offsetof(FAMWatcher_t3228827479_StaticFields, ___failed_0)); }
	inline bool get_failed_0() const { return ___failed_0; }
	inline bool* get_address_of_failed_0() { return &___failed_0; }
	inline void set_failed_0(bool value)
	{
		___failed_0 = value;
	}

	inline static int32_t get_offset_of_instance_1() { return static_cast<int32_t>(offsetof(FAMWatcher_t3228827479_StaticFields, ___instance_1)); }
	inline FAMWatcher_t3228827479 * get_instance_1() const { return ___instance_1; }
	inline FAMWatcher_t3228827479 ** get_address_of_instance_1() { return &___instance_1; }
	inline void set_instance_1(FAMWatcher_t3228827479 * value)
	{
		___instance_1 = value;
		Il2CppCodeGenWriteBarrier(&___instance_1, value);
	}

	inline static int32_t get_offset_of_watches_2() { return static_cast<int32_t>(offsetof(FAMWatcher_t3228827479_StaticFields, ___watches_2)); }
	inline Hashtable_t1853889766 * get_watches_2() const { return ___watches_2; }
	inline Hashtable_t1853889766 ** get_address_of_watches_2() { return &___watches_2; }
	inline void set_watches_2(Hashtable_t1853889766 * value)
	{
		___watches_2 = value;
		Il2CppCodeGenWriteBarrier(&___watches_2, value);
	}

	inline static int32_t get_offset_of_requests_3() { return static_cast<int32_t>(offsetof(FAMWatcher_t3228827479_StaticFields, ___requests_3)); }
	inline Hashtable_t1853889766 * get_requests_3() const { return ___requests_3; }
	inline Hashtable_t1853889766 ** get_address_of_requests_3() { return &___requests_3; }
	inline void set_requests_3(Hashtable_t1853889766 * value)
	{
		___requests_3 = value;
		Il2CppCodeGenWriteBarrier(&___requests_3, value);
	}

	inline static int32_t get_offset_of_conn_4() { return static_cast<int32_t>(offsetof(FAMWatcher_t3228827479_StaticFields, ___conn_4)); }
	inline FAMConnection_t1678998633  get_conn_4() const { return ___conn_4; }
	inline FAMConnection_t1678998633 * get_address_of_conn_4() { return &___conn_4; }
	inline void set_conn_4(FAMConnection_t1678998633  value)
	{
		___conn_4 = value;
	}

	inline static int32_t get_offset_of_thread_5() { return static_cast<int32_t>(offsetof(FAMWatcher_t3228827479_StaticFields, ___thread_5)); }
	inline Thread_t2300836069 * get_thread_5() const { return ___thread_5; }
	inline Thread_t2300836069 ** get_address_of_thread_5() { return &___thread_5; }
	inline void set_thread_5(Thread_t2300836069 * value)
	{
		___thread_5 = value;
		Il2CppCodeGenWriteBarrier(&___thread_5, value);
	}

	inline static int32_t get_offset_of_stop_6() { return static_cast<int32_t>(offsetof(FAMWatcher_t3228827479_StaticFields, ___stop_6)); }
	inline bool get_stop_6() const { return ___stop_6; }
	inline bool* get_address_of_stop_6() { return &___stop_6; }
	inline void set_stop_6(bool value)
	{
		___stop_6 = value;
	}

	inline static int32_t get_offset_of_use_gamin_7() { return static_cast<int32_t>(offsetof(FAMWatcher_t3228827479_StaticFields, ___use_gamin_7)); }
	inline bool get_use_gamin_7() const { return ___use_gamin_7; }
	inline bool* get_address_of_use_gamin_7() { return &___use_gamin_7; }
	inline void set_use_gamin_7(bool value)
	{
		___use_gamin_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
