﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Web_System_Web_UI_WebControls_AssociatedCon1758314542.h"
#include "System_Web_System_Web_UI_WebControls_BaseValidator3559083609.h"
#include "System_Web_System_Web_UI_WebControls_BorderStyle3334519566.h"
#include "System_Web_System_Web_UI_WebControls_ContentBuilder247220722.h"
#include "System_Web_System_Web_UI_WebControls_Content4294182870.h"
#include "System_Web_System_Web_UI_WebControls_ContentPlaceH2996035435.h"
#include "System_Web_System_Web_UI_WebControls_ContentPlaceHo328560363.h"
#include "System_Web_System_Web_UI_WebControls_ControlIDConv1795750277.h"
#include "System_Web_System_Web_UI_WebControls_FontInfo1360706239.h"
#include "System_Web_System_Web_UI_WebControls_FontNamesConve837404227.h"
#include "System_Web_System_Web_UI_WebControls_FontSize1746433605.h"
#include "System_Web_System_Web_UI_WebControls_FontUnitConve1077013631.h"
#include "System_Web_System_Web_UI_WebControls_FontUnit3688074528.h"
#include "System_Web_System_Web_UI_WebControls_GridLines4132949328.h"
#include "System_Web_System_Web_UI_WebControls_HorizontalAlig533117342.h"
#include "System_Web_System_Web_UI_WebControls_HorizontalAli1887897725.h"
#include "System_Web_System_Web_UI_WebControls_LabelControlBui54476227.h"
#include "System_Web_System_Web_UI_WebControls_Label3626644934.h"
#include "System_Web_System_Web_UI_WebControls_ListItemCollec682790300.h"
#include "System_Web_System_Web_UI_WebControls_ListItemContr1128536273.h"
#include "System_Web_System_Web_UI_WebControls_ListItem67066227.h"
#include "System_Web_System_Web_UI_WebControls_StringArrayCo1835590627.h"
#include "System_Web_System_Web_UI_WebControls_Style3589053173.h"
#include "System_Web_System_Web_UI_WebControls_TableCaptionA1204160605.h"
#include "System_Web_System_Web_UI_WebControls_TableCellColl3482415303.h"
#include "System_Web_System_Web_UI_WebControls_TableCellCont3488491678.h"
#include "System_Web_System_Web_UI_WebControls_TableCell423125296.h"
#include "System_Web_System_Web_UI_WebControls_Table574440801.h"
#include "System_Web_System_Web_UI_WebControls_Table_RowCont2421930286.h"
#include "System_Web_System_Web_UI_WebControls_TableHeaderCe3148554236.h"
#include "System_Web_System_Web_UI_WebControls_TableHeaderSco374879970.h"
#include "System_Web_System_Web_UI_WebControls_TableItemStyl3907927463.h"
#include "System_Web_System_Web_UI_WebControls_TableRowColle3754565154.h"
#include "System_Web_System_Web_UI_WebControls_TableRow1154691476.h"
#include "System_Web_System_Web_UI_WebControls_TableRow_Cell2950217935.h"
#include "System_Web_System_Web_UI_WebControls_TableRowSecti1448447860.h"
#include "System_Web_System_Web_UI_WebControls_TableStyle1026318625.h"
#include "System_Web_System_Web_UI_WebControls_UnitConverter1266597561.h"
#include "System_Web_System_Web_UI_WebControls_Unit3186727900.h"
#include "System_Web_System_Web_UI_WebControls_Unit_ParsingS3610471096.h"
#include "System_Web_System_Web_UI_WebControls_UnitType3617768156.h"
#include "System_Web_System_Web_UI_WebControls_ValidatedCont1434589485.h"
#include "System_Web_System_Web_UI_WebControls_ValidatorDisp3259174783.h"
#include "System_Web_System_Web_UI_WebControls_VerticalAlign1776218746.h"
#include "System_Web_System_Web_UI_WebControls_VerticalAlign2849038207.h"
#include "System_Web_System_Web_UI_WebControls_WebControl2705811671.h"
#include "System_Web_System_Web_UI_WebResourceAttribute4132945045.h"
#include "System_Web_System_Web_Util_AltSerialization1561118416.h"
#include "System_Web_System_Web_Util_DataSourceResolver2150809228.h"
#include "System_Web_System_Web_Util_FileUtils3174566191.h"
#include "System_Web_System_Web_Util_FileUtils_CreateTempFile844781177.h"
#include "System_Web_System_Web_Util_Helpers1105960530.h"
#include "System_Web_System_Web_Util_ICalls4071149221.h"
#include "System_Web_System_Web_Util_SearchPattern3593873567.h"
#include "System_Web_System_Web_Util_SearchPattern_Op1428817098.h"
#include "System_Web_System_Web_Util_SearchPattern_OpCode642418941.h"
#include "System_Web_System_Web_Util_StrUtils435421409.h"
#include "System_Web_System_Web_Util_TimeUtil4122492887.h"
#include "System_Web_System_Web_Util_UrlUtils1214298665.h"
#include "System_Web_System_Web_Util_WebEncoding373074341.h"
#include "System_Web_System_Web_VirtualPath4270372584.h"
#include "System_Web_System_Web_VirtualPathUtility3131300229.h"
#include "System_Web_System_Web_WebCategoryAttribute1186646113.h"
#include "System_Web_System_Web_WebROCollection4065660147.h"
#include "System_Web_System_Web_WebSysDescriptionAttribute2907340736.h"
#include "System_Web_System_Web_BeginEventHandler3912883583.h"
#include "System_Web_System_Web_Caching_CacheItemRemovedCall3206551617.h"
#include "System_Web_System_Web_Compilation_ParseErrorHandler967647811.h"
#include "System_Web_System_Web_Compilation_TextParsedHandle4012621828.h"
#include "System_Web_System_Web_Compilation_TagParsedHandler2123538485.h"
#include "System_Web_System_Web_Compilation_ParsingCompleteH1375501938.h"
#include "System_Web_System_Web_Compilation_BuildManagerRemo2403258536.h"
#include "System_Web_System_Web_EndEventHandler957584525.h"
#include "System_Web_System_Web_NoParamsDelegate1618427640.h"
#include "System_Web_System_Web_Security_DefaultAuthenticati3408095196.h"
#include "System_Web_System_Web_SessionState_SessionStateItem619504878.h"
#include "System_Web_System_Web_UI_BuildMethod1930575983.h"
#include "System_Web_System_Web_UI_BuildTemplateMethod1250634990.h"
#include "System_Web_System_Web_UI_ControlSkinDelegate1165826654.h"
#include "System_Web_System_Web_UI_DataSourceViewSelectCallba767443522.h"
#include "System_Web_System_Web_UI_ExtractTemplateValuesMeth1264402007.h"
#include "System_Web_System_Web_UI_ImageClickEventHandler72956396.h"
#include "System_Web_System_Web_UI_RenderMethod2959628882.h"
#include "System_Web_U3CPrivateImplementationDetailsU3E3057255361.h"
#include "System_Web_U3CPrivateImplementationDetailsU3E_U24A2490092596.h"
#include "System_Web_U3CPrivateImplementationDetailsU3E_U24A3244137463.h"
#include "System_Web_U3CPrivateImplementationDetailsU3E_U24A3652892010.h"
#include "Mono_Messaging_U3CModuleU3E692745525.h"
#include "System_Configuration_Install_U3CModuleU3E692745525.h"
#include "System_IdentityModel_U3CModuleU3E692745525.h"
#include "System_IdentityModel_System_MonoTODOAttribute4131080581.h"
#include "System_IdentityModel_System_IdentityModel_Claims_C2327046348.h"
#include "System_IdentityModel_System_IdentityModel_Claims_Cl870884883.h"
#include "System_IdentityModel_System_IdentityModel_Claims_C3529661467.h"
#include "System_IdentityModel_System_IdentityModel_Claims_Cl988662189.h"
#include "System_IdentityModel_System_IdentityModel_Claims_D3476487852.h"
#include "System_IdentityModel_System_IdentityModel_Claims_R3119251884.h"
#include "System_IdentityModel_System_IdentityModel_Policy_A2521326227.h"
#include "System_IdentityModel_System_IdentityModel_Policy_A3136051856.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3600 = { sizeof (AssociatedControlConverter_t1758314542), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3601 = { sizeof (BaseValidator_t3559083609), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3601[3] = 
{
	BaseValidator_t3559083609::get_offset_of_render_uplevel_36(),
	BaseValidator_t3559083609::get_offset_of_valid_37(),
	BaseValidator_t3559083609::get_offset_of_pre_render_called_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3602 = { sizeof (BorderStyle_t3334519566)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3602[11] = 
{
	BorderStyle_t3334519566::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3603 = { sizeof (ContentBuilderInternal_t247220722), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3603[1] = 
{
	ContentBuilderInternal_t247220722::get_offset_of_placeHolderID_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3604 = { sizeof (Content_t4294182870), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3605 = { sizeof (ContentPlaceHolderBuilder_t2996035435), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3606 = { sizeof (ContentPlaceHolder_t328560363), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3607 = { sizeof (ControlIDConverter_t1795750277), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3608 = { sizeof (FontInfo_t1360706239), -1, sizeof(FontInfo_t1360706239_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3608[3] = 
{
	FontInfo_t1360706239_StaticFields::get_offset_of_empty_names_0(),
	FontInfo_t1360706239::get_offset_of_bag_1(),
	FontInfo_t1360706239::get_offset_of__owner_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3609 = { sizeof (FontNamesConverter_t837404227), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3610 = { sizeof (FontSize_t1746433605)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3610[12] = 
{
	FontSize_t1746433605::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3611 = { sizeof (FontUnitConverter_t1077013631), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3612 = { sizeof (FontUnit_t3688074528)+ sizeof (Il2CppObject), sizeof(FontUnit_t3688074528_marshaled_pinvoke), sizeof(FontUnit_t3688074528_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3612[14] = 
{
	FontUnit_t3688074528::get_offset_of_type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FontUnit_t3688074528::get_offset_of_unit_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FontUnit_t3688074528_StaticFields::get_offset_of_Empty_2(),
	FontUnit_t3688074528_StaticFields::get_offset_of_Smaller_3(),
	FontUnit_t3688074528_StaticFields::get_offset_of_Larger_4(),
	FontUnit_t3688074528_StaticFields::get_offset_of_XXSmall_5(),
	FontUnit_t3688074528_StaticFields::get_offset_of_XSmall_6(),
	FontUnit_t3688074528_StaticFields::get_offset_of_Small_7(),
	FontUnit_t3688074528_StaticFields::get_offset_of_Medium_8(),
	FontUnit_t3688074528_StaticFields::get_offset_of_Large_9(),
	FontUnit_t3688074528_StaticFields::get_offset_of_XLarge_10(),
	FontUnit_t3688074528_StaticFields::get_offset_of_XXLarge_11(),
	FontUnit_t3688074528_StaticFields::get_offset_of_font_size_names_12(),
	FontUnit_t3688074528_StaticFields::get_offset_of_U3CU3Ef__switchU24map15_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3613 = { sizeof (GridLines_t4132949328)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3613[5] = 
{
	GridLines_t4132949328::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3614 = { sizeof (HorizontalAlignConverter_t533117342), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3615 = { sizeof (HorizontalAlign_t1887897725)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3615[6] = 
{
	HorizontalAlign_t1887897725::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3616 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3617 = { sizeof (LabelControlBuilder_t54476227), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3618 = { sizeof (Label_t3626644934), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3619 = { sizeof (ListItemCollection_t682790300), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3619[4] = 
{
	ListItemCollection_t682790300::get_offset_of_items_0(),
	ListItemCollection_t682790300::get_offset_of_tracking_1(),
	ListItemCollection_t682790300::get_offset_of_dirty_2(),
	ListItemCollection_t682790300::get_offset_of_lastDirty_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3620 = { sizeof (ListItemControlBuilder_t1128536273), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3621 = { sizeof (ListItem_t67066227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3621[7] = 
{
	ListItem_t67066227::get_offset_of_text_0(),
	ListItem_t67066227::get_offset_of_value_1(),
	ListItem_t67066227::get_offset_of_selected_2(),
	ListItem_t67066227::get_offset_of_dirty_3(),
	ListItem_t67066227::get_offset_of_enabled_4(),
	ListItem_t67066227::get_offset_of_tracking_5(),
	ListItem_t67066227::get_offset_of_sb_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3622 = { sizeof (StringArrayConverter_t1835590627), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3623 = { sizeof (Style_t3589053173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3623[7] = 
{
	Style_t3589053173::get_offset_of_styles_4(),
	Style_t3589053173::get_offset_of_stylesTraked_5(),
	Style_t3589053173::get_offset_of_viewstate_6(),
	Style_t3589053173::get_offset_of_fontinfo_7(),
	Style_t3589053173::get_offset_of_tracking_8(),
	Style_t3589053173::get_offset_of__isSharedViewState_9(),
	Style_t3589053173::get_offset_of_registered_class_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3624 = { sizeof (TableCaptionAlign_t1204160605)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3624[6] = 
{
	TableCaptionAlign_t1204160605::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3625 = { sizeof (TableCellCollection_t3482415303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3625[1] = 
{
	TableCellCollection_t3482415303::get_offset_of_cc_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3626 = { sizeof (TableCellControlBuilder_t3488491678), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3627 = { sizeof (TableCell_t423125296), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3628 = { sizeof (Table_t574440801), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3628[2] = 
{
	Table_t574440801::get_offset_of_rows_36(),
	Table_t574440801::get_offset_of_generateTableSections_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3629 = { sizeof (RowControlCollection_t2421930286), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3630 = { sizeof (TableHeaderCell_t3148554236), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3631 = { sizeof (TableHeaderScope_t374879970)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3631[4] = 
{
	TableHeaderScope_t374879970::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3632 = { sizeof (TableItemStyle_t3907927463), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3633 = { sizeof (TableRowCollection_t3754565154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3633[2] = 
{
	TableRowCollection_t3754565154::get_offset_of_cc_0(),
	TableRowCollection_t3754565154::get_offset_of_owner_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3634 = { sizeof (TableRow_t1154691476), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3634[3] = 
{
	TableRow_t1154691476::get_offset_of_cells_36(),
	TableRow_t1154691476::get_offset_of_tableRowSectionSet_37(),
	TableRow_t1154691476::get_offset_of_U3CContainerU3Ek__BackingField_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3635 = { sizeof (CellControlCollection_t2950217935), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3636 = { sizeof (TableRowSection_t1448447860)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3636[4] = 
{
	TableRowSection_t1448447860::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3637 = { sizeof (TableStyle_t1026318625), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3638 = { sizeof (UnitConverter_t1266597561), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3639 = { sizeof (Unit_t3186727900)+ sizeof (Il2CppObject), sizeof(Unit_t3186727900_marshaled_pinvoke), sizeof(Unit_t3186727900_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3639[5] = 
{
	Unit_t3186727900::get_offset_of_type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Unit_t3186727900::get_offset_of_value_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Unit_t3186727900::get_offset_of_valueSet_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Unit_t3186727900_StaticFields::get_offset_of_Empty_3(),
	Unit_t3186727900_StaticFields::get_offset_of_U3CU3Ef__switchU24map1B_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3640 = { sizeof (ParsingStage_t3610471096)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3640[6] = 
{
	ParsingStage_t3610471096::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3641 = { sizeof (UnitType_t3617768156)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3641[10] = 
{
	UnitType_t3617768156::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3642 = { sizeof (ValidatedControlConverter_t1434589485), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3643 = { sizeof (ValidatorDisplay_t3259174783)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3643[4] = 
{
	ValidatorDisplay_t3259174783::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3644 = { sizeof (VerticalAlignConverter_t1776218746), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3645 = { sizeof (VerticalAlign_t2849038207)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3645[5] = 
{
	VerticalAlign_t2849038207::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3646 = { sizeof (WebControl_t2705811671), -1, sizeof(WebControl_t2705811671_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3646[8] = 
{
	WebControl_t2705811671::get_offset_of_style_28(),
	WebControl_t2705811671::get_offset_of_tag_29(),
	WebControl_t2705811671::get_offset_of_tag_name_30(),
	WebControl_t2705811671::get_offset_of_attributes_31(),
	WebControl_t2705811671::get_offset_of_attribute_state_32(),
	WebControl_t2705811671::get_offset_of_enabled_33(),
	WebControl_t2705811671::get_offset_of_track_enabled_state_34(),
	WebControl_t2705811671_StaticFields::get_offset_of__script_trim_chars_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3647 = { sizeof (WebResourceAttribute_t4132945045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3647[3] = 
{
	WebResourceAttribute_t4132945045::get_offset_of_performSubstitution_0(),
	WebResourceAttribute_t4132945045::get_offset_of_webResource_1(),
	WebResourceAttribute_t4132945045::get_offset_of_contentType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3648 = { sizeof (AltSerialization_t1561118416), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3649 = { sizeof (DataSourceResolver_t2150809228), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3650 = { sizeof (FileUtils_t3174566191), -1, sizeof(FileUtils_t3174566191_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3650[1] = 
{
	FileUtils_t3174566191_StaticFields::get_offset_of_rnd_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3651 = { sizeof (CreateTempFile_t844781177), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3652 = { sizeof (Helpers_t1105960530), -1, sizeof(Helpers_t1105960530_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3652[1] = 
{
	Helpers_t1105960530_StaticFields::get_offset_of_InvariantCulture_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3653 = { sizeof (ICalls_t4071149221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3654 = { sizeof (SearchPattern_t3593873567), -1, sizeof(SearchPattern_t3593873567_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3654[3] = 
{
	SearchPattern_t3593873567::get_offset_of_ops_0(),
	SearchPattern_t3593873567::get_offset_of_ignore_1(),
	SearchPattern_t3593873567_StaticFields::get_offset_of_WildcardChars_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3655 = { sizeof (Op_t1428817098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3655[3] = 
{
	Op_t1428817098::get_offset_of_Code_0(),
	Op_t1428817098::get_offset_of_Argument_1(),
	Op_t1428817098::get_offset_of_Next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3656 = { sizeof (OpCode_t642418941)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3656[6] = 
{
	OpCode_t642418941::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3657 = { sizeof (StrUtils_t435421409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3658 = { sizeof (TimeUtil_t4122492887), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3659 = { sizeof (UrlUtils_t1214298665), -1, sizeof(UrlUtils_t1214298665_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3659[1] = 
{
	UrlUtils_t1214298665_StaticFields::get_offset_of_path_sep_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3660 = { sizeof (WebEncoding_t373074341), -1, sizeof(WebEncoding_t373074341_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3660[2] = 
{
	WebEncoding_t373074341_StaticFields::get_offset_of_cached_0(),
	WebEncoding_t373074341_StaticFields::get_offset_of_sect_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3661 = { sizeof (VirtualPath_t4270372584), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3661[13] = 
{
	VirtualPath_t4270372584::get_offset_of__absolute_0(),
	VirtualPath_t4270372584::get_offset_of__appRelative_1(),
	VirtualPath_t4270372584::get_offset_of__appRelativeNotRooted_2(),
	VirtualPath_t4270372584::get_offset_of__extension_3(),
	VirtualPath_t4270372584::get_offset_of__directory_4(),
	VirtualPath_t4270372584::get_offset_of__directoryNoNormalize_5(),
	VirtualPath_t4270372584::get_offset_of__currentRequestDirectory_6(),
	VirtualPath_t4270372584::get_offset_of__physicalPath_7(),
	VirtualPath_t4270372584::get_offset_of_U3CIsAbsoluteU3Ek__BackingField_8(),
	VirtualPath_t4270372584::get_offset_of_U3CIsFakeU3Ek__BackingField_9(),
	VirtualPath_t4270372584::get_offset_of_U3CIsRootedU3Ek__BackingField_10(),
	VirtualPath_t4270372584::get_offset_of_U3CIsAppRelativeU3Ek__BackingField_11(),
	VirtualPath_t4270372584::get_offset_of_U3COriginalU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3662 = { sizeof (VirtualPathUtility_t3131300229), -1, sizeof(VirtualPathUtility_t3131300229_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3662[5] = 
{
	VirtualPathUtility_t3131300229_StaticFields::get_offset_of_monoSettingsVerifyCompatibility_0(),
	VirtualPathUtility_t3131300229_StaticFields::get_offset_of_runningOnWindows_1(),
	VirtualPathUtility_t3131300229_StaticFields::get_offset_of_path_sep_2(),
	VirtualPathUtility_t3131300229_StaticFields::get_offset_of_invalidVirtualPathChars_3(),
	VirtualPathUtility_t3131300229_StaticFields::get_offset_of_aspNetVerificationKey_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3663 = { sizeof (WebCategoryAttribute_t1186646113), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3664 = { sizeof (WebROCollection_t4065660147), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3664[2] = 
{
	WebROCollection_t4065660147::get_offset_of_got_id_12(),
	WebROCollection_t4065660147::get_offset_of_id_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3665 = { sizeof (WebSysDescriptionAttribute_t2907340736), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3666 = { sizeof (BeginEventHandler_t3912883583), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3667 = { sizeof (CacheItemRemovedCallback_t3206551617), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3668 = { sizeof (ParseErrorHandler_t967647811), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3669 = { sizeof (TextParsedHandler_t4012621828), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3670 = { sizeof (TagParsedHandler_t2123538485), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3671 = { sizeof (ParsingCompleteHandler_t1375501938), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3672 = { sizeof (BuildManagerRemoveEntryEventHandler_t2403258536), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3673 = { sizeof (EndEventHandler_t957584525), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3674 = { sizeof (NoParamsDelegate_t1618427640), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3675 = { sizeof (DefaultAuthenticationEventHandler_t3408095196), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3676 = { sizeof (SessionStateItemExpireCallback_t619504878), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3677 = { sizeof (BuildMethod_t1930575983), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3678 = { sizeof (BuildTemplateMethod_t1250634990), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3679 = { sizeof (ControlSkinDelegate_t1165826654), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3680 = { sizeof (DataSourceViewSelectCallback_t767443522), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3681 = { sizeof (ExtractTemplateValuesMethod_t1264402007), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3682 = { sizeof (ImageClickEventHandler_t72956396), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3683 = { sizeof (RenderMethod_t2959628882), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3684 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255370), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255370_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3684[3] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255370_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t3057255370_StaticFields::get_offset_of_U24U24fieldU2D2_1(),
	U3CPrivateImplementationDetailsU3E_t3057255370_StaticFields::get_offset_of_U24U24fieldU2D5_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3685 = { sizeof (U24ArrayTypeU2412_t2490092600)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t2490092600 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3686 = { sizeof (U24ArrayTypeU248_t3244137468)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU248_t3244137468 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3687 = { sizeof (U24ArrayTypeU2432_t3652892013)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2432_t3652892013 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3688 = { sizeof (U3CModuleU3E_t692745545), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3689 = { sizeof (U3CModuleU3E_t692745546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3690 = { sizeof (U3CModuleU3E_t692745547), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3691 = { sizeof (MonoTODOAttribute_t4131080592), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3691[1] = 
{
	MonoTODOAttribute_t4131080592::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3692 = { sizeof (Claim_t2327046348), -1, sizeof(Claim_t2327046348_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3692[5] = 
{
	Claim_t2327046348_StaticFields::get_offset_of_default_comparer_0(),
	Claim_t2327046348_StaticFields::get_offset_of_system_1(),
	Claim_t2327046348::get_offset_of_claim_type_2(),
	Claim_t2327046348::get_offset_of_resource_3(),
	Claim_t2327046348::get_offset_of_right_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3693 = { sizeof (ClaimComparer_t870884883), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3694 = { sizeof (ClaimSet_t3529661467), -1, sizeof(ClaimSet_t3529661467_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3694[1] = 
{
	ClaimSet_t3529661467_StaticFields::get_offset_of_system_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3695 = { sizeof (ClaimTypes_t988662189), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3696 = { sizeof (DefaultClaimSet_t3476487852), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3696[2] = 
{
	DefaultClaimSet_t3476487852::get_offset_of_list_1(),
	DefaultClaimSet_t3476487852::get_offset_of_issuer_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3697 = { sizeof (Rights_t3119251884), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3698 = { sizeof (AuthorizationContext_t2521326227), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3699 = { sizeof (DefaultAuthorizationContext_t3136051856), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3699[2] = 
{
	DefaultAuthorizationContext_t3136051856::get_offset_of_ctx_0(),
	DefaultAuthorizationContext_t3136051856::get_offset_of_id_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
