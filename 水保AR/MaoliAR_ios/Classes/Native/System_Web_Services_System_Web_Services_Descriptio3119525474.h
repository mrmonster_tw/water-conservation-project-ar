﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_Services_System_Web_Services_Descriptio2466402210.h"

// System.Web.Services.Description.OperationCollection
struct OperationCollection_t3429260361;
// System.Web.Services.Description.ServiceDescription
struct ServiceDescription_t3693704363;
// System.Web.Services.Description.ServiceDescriptionFormatExtensionCollection
struct ServiceDescriptionFormatExtensionCollection_t2630153888;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.PortType
struct  PortType_t3119525474  : public NamedItem_t2466402210
{
public:
	// System.Web.Services.Description.OperationCollection System.Web.Services.Description.PortType::operations
	OperationCollection_t3429260361 * ___operations_4;
	// System.Web.Services.Description.ServiceDescription System.Web.Services.Description.PortType::serviceDescription
	ServiceDescription_t3693704363 * ___serviceDescription_5;
	// System.Web.Services.Description.ServiceDescriptionFormatExtensionCollection System.Web.Services.Description.PortType::extensions
	ServiceDescriptionFormatExtensionCollection_t2630153888 * ___extensions_6;

public:
	inline static int32_t get_offset_of_operations_4() { return static_cast<int32_t>(offsetof(PortType_t3119525474, ___operations_4)); }
	inline OperationCollection_t3429260361 * get_operations_4() const { return ___operations_4; }
	inline OperationCollection_t3429260361 ** get_address_of_operations_4() { return &___operations_4; }
	inline void set_operations_4(OperationCollection_t3429260361 * value)
	{
		___operations_4 = value;
		Il2CppCodeGenWriteBarrier(&___operations_4, value);
	}

	inline static int32_t get_offset_of_serviceDescription_5() { return static_cast<int32_t>(offsetof(PortType_t3119525474, ___serviceDescription_5)); }
	inline ServiceDescription_t3693704363 * get_serviceDescription_5() const { return ___serviceDescription_5; }
	inline ServiceDescription_t3693704363 ** get_address_of_serviceDescription_5() { return &___serviceDescription_5; }
	inline void set_serviceDescription_5(ServiceDescription_t3693704363 * value)
	{
		___serviceDescription_5 = value;
		Il2CppCodeGenWriteBarrier(&___serviceDescription_5, value);
	}

	inline static int32_t get_offset_of_extensions_6() { return static_cast<int32_t>(offsetof(PortType_t3119525474, ___extensions_6)); }
	inline ServiceDescriptionFormatExtensionCollection_t2630153888 * get_extensions_6() const { return ___extensions_6; }
	inline ServiceDescriptionFormatExtensionCollection_t2630153888 ** get_address_of_extensions_6() { return &___extensions_6; }
	inline void set_extensions_6(ServiceDescriptionFormatExtensionCollection_t2630153888 * value)
	{
		___extensions_6 = value;
		Il2CppCodeGenWriteBarrier(&___extensions_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
