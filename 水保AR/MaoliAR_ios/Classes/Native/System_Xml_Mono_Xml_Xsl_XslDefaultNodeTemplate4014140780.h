﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_XslTemplate152263049.h"

// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2760654312;
// Mono.Xml.Xsl.XslDefaultNodeTemplate
struct XslDefaultNodeTemplate_t4014140780;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.XslDefaultNodeTemplate
struct  XslDefaultNodeTemplate_t4014140780  : public XslTemplate_t152263049
{
public:
	// System.Xml.XmlQualifiedName Mono.Xml.Xsl.XslDefaultNodeTemplate::mode
	XmlQualifiedName_t2760654312 * ___mode_10;

public:
	inline static int32_t get_offset_of_mode_10() { return static_cast<int32_t>(offsetof(XslDefaultNodeTemplate_t4014140780, ___mode_10)); }
	inline XmlQualifiedName_t2760654312 * get_mode_10() const { return ___mode_10; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_mode_10() { return &___mode_10; }
	inline void set_mode_10(XmlQualifiedName_t2760654312 * value)
	{
		___mode_10 = value;
		Il2CppCodeGenWriteBarrier(&___mode_10, value);
	}
};

struct XslDefaultNodeTemplate_t4014140780_StaticFields
{
public:
	// Mono.Xml.Xsl.XslDefaultNodeTemplate Mono.Xml.Xsl.XslDefaultNodeTemplate::instance
	XslDefaultNodeTemplate_t4014140780 * ___instance_11;

public:
	inline static int32_t get_offset_of_instance_11() { return static_cast<int32_t>(offsetof(XslDefaultNodeTemplate_t4014140780_StaticFields, ___instance_11)); }
	inline XslDefaultNodeTemplate_t4014140780 * get_instance_11() const { return ___instance_11; }
	inline XslDefaultNodeTemplate_t4014140780 ** get_address_of_instance_11() { return &___instance_11; }
	inline void set_instance_11(XslDefaultNodeTemplate_t4014140780 * value)
	{
		___instance_11 = value;
		Il2CppCodeGenWriteBarrier(&___instance_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
