﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.UI.Control
struct Control_t3006474639;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.Adapters.ControlAdapter
struct  ControlAdapter_t3811171780  : public Il2CppObject
{
public:
	// System.Web.UI.Control System.Web.UI.Adapters.ControlAdapter::control
	Control_t3006474639 * ___control_0;

public:
	inline static int32_t get_offset_of_control_0() { return static_cast<int32_t>(offsetof(ControlAdapter_t3811171780, ___control_0)); }
	inline Control_t3006474639 * get_control_0() const { return ___control_0; }
	inline Control_t3006474639 ** get_address_of_control_0() { return &___control_0; }
	inline void set_control_0(Control_t3006474639 * value)
	{
		___control_0 = value;
		Il2CppCodeGenWriteBarrier(&___control_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
