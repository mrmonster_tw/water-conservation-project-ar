﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Co518829156.h"
#include "mscorlib_System_TimeSpan881159249.h"

// System.ServiceModel.Description.ServiceCredentials
struct ServiceCredentials_t3431196922;
// System.ServiceModel.Description.ServiceDescription
struct ServiceDescription_t2946919347;
// System.ServiceModel.UriSchemeKeyedCollection
struct UriSchemeKeyedCollection_t4035456475;
// System.ServiceModel.Dispatcher.ServiceThrottle
struct ServiceThrottle_t3841213848;
// System.Collections.Generic.List`1<System.ServiceModel.InstanceContext>
struct List_1_t770313400;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.ServiceModel.InstanceContext>
struct ReadOnlyCollection_1_t510814945;
// System.ServiceModel.Dispatcher.ChannelDispatcherCollection
struct ChannelDispatcherCollection_t2904826880;
// System.Collections.Generic.IDictionary`2<System.String,System.ServiceModel.Description.ContractDescription>
struct IDictionary_2_t3955253500;
// System.ServiceModel.IExtensionCollection`1<System.ServiceModel.ServiceHostBase>
struct IExtensionCollection_1_t1540282693;
// System.ServiceModel.Description.ContractDescription
struct ContractDescription_t1411178514;
// System.EventHandler`1<System.ServiceModel.UnknownMessageReceivedEventArgs>
struct EventHandler_1_t3913994551;
// System.ServiceModel.Description.ServiceAuthorizationBehavior
struct ServiceAuthorizationBehavior_t248521010;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.ServiceHostBase
struct  ServiceHostBase_t3741910535  : public CommunicationObject_t518829156
{
public:
	// System.ServiceModel.Description.ServiceCredentials System.ServiceModel.ServiceHostBase::credentials
	ServiceCredentials_t3431196922 * ___credentials_9;
	// System.ServiceModel.Description.ServiceDescription System.ServiceModel.ServiceHostBase::description
	ServiceDescription_t2946919347 * ___description_10;
	// System.ServiceModel.UriSchemeKeyedCollection System.ServiceModel.ServiceHostBase::base_addresses
	UriSchemeKeyedCollection_t4035456475 * ___base_addresses_11;
	// System.TimeSpan System.ServiceModel.ServiceHostBase::open_timeout
	TimeSpan_t881159249  ___open_timeout_12;
	// System.TimeSpan System.ServiceModel.ServiceHostBase::close_timeout
	TimeSpan_t881159249  ___close_timeout_13;
	// System.ServiceModel.Dispatcher.ServiceThrottle System.ServiceModel.ServiceHostBase::throttle
	ServiceThrottle_t3841213848 * ___throttle_14;
	// System.Collections.Generic.List`1<System.ServiceModel.InstanceContext> System.ServiceModel.ServiceHostBase::contexts
	List_1_t770313400 * ___contexts_15;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.ServiceModel.InstanceContext> System.ServiceModel.ServiceHostBase::exposed_contexts
	ReadOnlyCollection_1_t510814945 * ___exposed_contexts_16;
	// System.ServiceModel.Dispatcher.ChannelDispatcherCollection System.ServiceModel.ServiceHostBase::channel_dispatchers
	ChannelDispatcherCollection_t2904826880 * ___channel_dispatchers_17;
	// System.Collections.Generic.IDictionary`2<System.String,System.ServiceModel.Description.ContractDescription> System.ServiceModel.ServiceHostBase::contracts
	Il2CppObject* ___contracts_18;
	// System.Int32 System.ServiceModel.ServiceHostBase::flow_limit
	int32_t ___flow_limit_19;
	// System.ServiceModel.IExtensionCollection`1<System.ServiceModel.ServiceHostBase> System.ServiceModel.ServiceHostBase::extensions
	Il2CppObject* ___extensions_20;
	// System.ServiceModel.Description.ContractDescription System.ServiceModel.ServiceHostBase::mex_contract
	ContractDescription_t1411178514 * ___mex_contract_21;
	// System.ServiceModel.Description.ContractDescription System.ServiceModel.ServiceHostBase::help_page_contract
	ContractDescription_t1411178514 * ___help_page_contract_22;
	// System.EventHandler`1<System.ServiceModel.UnknownMessageReceivedEventArgs> System.ServiceModel.ServiceHostBase::UnknownMessageReceived
	EventHandler_1_t3913994551 * ___UnknownMessageReceived_23;
	// System.ServiceModel.Description.ServiceAuthorizationBehavior System.ServiceModel.ServiceHostBase::<Authorization>k__BackingField
	ServiceAuthorizationBehavior_t248521010 * ___U3CAuthorizationU3Ek__BackingField_24;

public:
	inline static int32_t get_offset_of_credentials_9() { return static_cast<int32_t>(offsetof(ServiceHostBase_t3741910535, ___credentials_9)); }
	inline ServiceCredentials_t3431196922 * get_credentials_9() const { return ___credentials_9; }
	inline ServiceCredentials_t3431196922 ** get_address_of_credentials_9() { return &___credentials_9; }
	inline void set_credentials_9(ServiceCredentials_t3431196922 * value)
	{
		___credentials_9 = value;
		Il2CppCodeGenWriteBarrier(&___credentials_9, value);
	}

	inline static int32_t get_offset_of_description_10() { return static_cast<int32_t>(offsetof(ServiceHostBase_t3741910535, ___description_10)); }
	inline ServiceDescription_t2946919347 * get_description_10() const { return ___description_10; }
	inline ServiceDescription_t2946919347 ** get_address_of_description_10() { return &___description_10; }
	inline void set_description_10(ServiceDescription_t2946919347 * value)
	{
		___description_10 = value;
		Il2CppCodeGenWriteBarrier(&___description_10, value);
	}

	inline static int32_t get_offset_of_base_addresses_11() { return static_cast<int32_t>(offsetof(ServiceHostBase_t3741910535, ___base_addresses_11)); }
	inline UriSchemeKeyedCollection_t4035456475 * get_base_addresses_11() const { return ___base_addresses_11; }
	inline UriSchemeKeyedCollection_t4035456475 ** get_address_of_base_addresses_11() { return &___base_addresses_11; }
	inline void set_base_addresses_11(UriSchemeKeyedCollection_t4035456475 * value)
	{
		___base_addresses_11 = value;
		Il2CppCodeGenWriteBarrier(&___base_addresses_11, value);
	}

	inline static int32_t get_offset_of_open_timeout_12() { return static_cast<int32_t>(offsetof(ServiceHostBase_t3741910535, ___open_timeout_12)); }
	inline TimeSpan_t881159249  get_open_timeout_12() const { return ___open_timeout_12; }
	inline TimeSpan_t881159249 * get_address_of_open_timeout_12() { return &___open_timeout_12; }
	inline void set_open_timeout_12(TimeSpan_t881159249  value)
	{
		___open_timeout_12 = value;
	}

	inline static int32_t get_offset_of_close_timeout_13() { return static_cast<int32_t>(offsetof(ServiceHostBase_t3741910535, ___close_timeout_13)); }
	inline TimeSpan_t881159249  get_close_timeout_13() const { return ___close_timeout_13; }
	inline TimeSpan_t881159249 * get_address_of_close_timeout_13() { return &___close_timeout_13; }
	inline void set_close_timeout_13(TimeSpan_t881159249  value)
	{
		___close_timeout_13 = value;
	}

	inline static int32_t get_offset_of_throttle_14() { return static_cast<int32_t>(offsetof(ServiceHostBase_t3741910535, ___throttle_14)); }
	inline ServiceThrottle_t3841213848 * get_throttle_14() const { return ___throttle_14; }
	inline ServiceThrottle_t3841213848 ** get_address_of_throttle_14() { return &___throttle_14; }
	inline void set_throttle_14(ServiceThrottle_t3841213848 * value)
	{
		___throttle_14 = value;
		Il2CppCodeGenWriteBarrier(&___throttle_14, value);
	}

	inline static int32_t get_offset_of_contexts_15() { return static_cast<int32_t>(offsetof(ServiceHostBase_t3741910535, ___contexts_15)); }
	inline List_1_t770313400 * get_contexts_15() const { return ___contexts_15; }
	inline List_1_t770313400 ** get_address_of_contexts_15() { return &___contexts_15; }
	inline void set_contexts_15(List_1_t770313400 * value)
	{
		___contexts_15 = value;
		Il2CppCodeGenWriteBarrier(&___contexts_15, value);
	}

	inline static int32_t get_offset_of_exposed_contexts_16() { return static_cast<int32_t>(offsetof(ServiceHostBase_t3741910535, ___exposed_contexts_16)); }
	inline ReadOnlyCollection_1_t510814945 * get_exposed_contexts_16() const { return ___exposed_contexts_16; }
	inline ReadOnlyCollection_1_t510814945 ** get_address_of_exposed_contexts_16() { return &___exposed_contexts_16; }
	inline void set_exposed_contexts_16(ReadOnlyCollection_1_t510814945 * value)
	{
		___exposed_contexts_16 = value;
		Il2CppCodeGenWriteBarrier(&___exposed_contexts_16, value);
	}

	inline static int32_t get_offset_of_channel_dispatchers_17() { return static_cast<int32_t>(offsetof(ServiceHostBase_t3741910535, ___channel_dispatchers_17)); }
	inline ChannelDispatcherCollection_t2904826880 * get_channel_dispatchers_17() const { return ___channel_dispatchers_17; }
	inline ChannelDispatcherCollection_t2904826880 ** get_address_of_channel_dispatchers_17() { return &___channel_dispatchers_17; }
	inline void set_channel_dispatchers_17(ChannelDispatcherCollection_t2904826880 * value)
	{
		___channel_dispatchers_17 = value;
		Il2CppCodeGenWriteBarrier(&___channel_dispatchers_17, value);
	}

	inline static int32_t get_offset_of_contracts_18() { return static_cast<int32_t>(offsetof(ServiceHostBase_t3741910535, ___contracts_18)); }
	inline Il2CppObject* get_contracts_18() const { return ___contracts_18; }
	inline Il2CppObject** get_address_of_contracts_18() { return &___contracts_18; }
	inline void set_contracts_18(Il2CppObject* value)
	{
		___contracts_18 = value;
		Il2CppCodeGenWriteBarrier(&___contracts_18, value);
	}

	inline static int32_t get_offset_of_flow_limit_19() { return static_cast<int32_t>(offsetof(ServiceHostBase_t3741910535, ___flow_limit_19)); }
	inline int32_t get_flow_limit_19() const { return ___flow_limit_19; }
	inline int32_t* get_address_of_flow_limit_19() { return &___flow_limit_19; }
	inline void set_flow_limit_19(int32_t value)
	{
		___flow_limit_19 = value;
	}

	inline static int32_t get_offset_of_extensions_20() { return static_cast<int32_t>(offsetof(ServiceHostBase_t3741910535, ___extensions_20)); }
	inline Il2CppObject* get_extensions_20() const { return ___extensions_20; }
	inline Il2CppObject** get_address_of_extensions_20() { return &___extensions_20; }
	inline void set_extensions_20(Il2CppObject* value)
	{
		___extensions_20 = value;
		Il2CppCodeGenWriteBarrier(&___extensions_20, value);
	}

	inline static int32_t get_offset_of_mex_contract_21() { return static_cast<int32_t>(offsetof(ServiceHostBase_t3741910535, ___mex_contract_21)); }
	inline ContractDescription_t1411178514 * get_mex_contract_21() const { return ___mex_contract_21; }
	inline ContractDescription_t1411178514 ** get_address_of_mex_contract_21() { return &___mex_contract_21; }
	inline void set_mex_contract_21(ContractDescription_t1411178514 * value)
	{
		___mex_contract_21 = value;
		Il2CppCodeGenWriteBarrier(&___mex_contract_21, value);
	}

	inline static int32_t get_offset_of_help_page_contract_22() { return static_cast<int32_t>(offsetof(ServiceHostBase_t3741910535, ___help_page_contract_22)); }
	inline ContractDescription_t1411178514 * get_help_page_contract_22() const { return ___help_page_contract_22; }
	inline ContractDescription_t1411178514 ** get_address_of_help_page_contract_22() { return &___help_page_contract_22; }
	inline void set_help_page_contract_22(ContractDescription_t1411178514 * value)
	{
		___help_page_contract_22 = value;
		Il2CppCodeGenWriteBarrier(&___help_page_contract_22, value);
	}

	inline static int32_t get_offset_of_UnknownMessageReceived_23() { return static_cast<int32_t>(offsetof(ServiceHostBase_t3741910535, ___UnknownMessageReceived_23)); }
	inline EventHandler_1_t3913994551 * get_UnknownMessageReceived_23() const { return ___UnknownMessageReceived_23; }
	inline EventHandler_1_t3913994551 ** get_address_of_UnknownMessageReceived_23() { return &___UnknownMessageReceived_23; }
	inline void set_UnknownMessageReceived_23(EventHandler_1_t3913994551 * value)
	{
		___UnknownMessageReceived_23 = value;
		Il2CppCodeGenWriteBarrier(&___UnknownMessageReceived_23, value);
	}

	inline static int32_t get_offset_of_U3CAuthorizationU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(ServiceHostBase_t3741910535, ___U3CAuthorizationU3Ek__BackingField_24)); }
	inline ServiceAuthorizationBehavior_t248521010 * get_U3CAuthorizationU3Ek__BackingField_24() const { return ___U3CAuthorizationU3Ek__BackingField_24; }
	inline ServiceAuthorizationBehavior_t248521010 ** get_address_of_U3CAuthorizationU3Ek__BackingField_24() { return &___U3CAuthorizationU3Ek__BackingField_24; }
	inline void set_U3CAuthorizationU3Ek__BackingField_24(ServiceAuthorizationBehavior_t248521010 * value)
	{
		___U3CAuthorizationU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAuthorizationU3Ek__BackingField_24, value);
	}
};

struct ServiceHostBase_t3741910535_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.ServiceModel.ServiceHostBase::<>f__switch$map2
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map2_25;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2_25() { return static_cast<int32_t>(offsetof(ServiceHostBase_t3741910535_StaticFields, ___U3CU3Ef__switchU24map2_25)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map2_25() const { return ___U3CU3Ef__switchU24map2_25; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map2_25() { return &___U3CU3Ef__switchU24map2_25; }
	inline void set_U3CU3Ef__switchU24map2_25(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map2_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map2_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
