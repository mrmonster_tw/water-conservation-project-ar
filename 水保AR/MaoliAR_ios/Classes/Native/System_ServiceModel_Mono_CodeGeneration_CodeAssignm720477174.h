﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_Mono_CodeGeneration_CodeExpres2163794278.h"

// Mono.CodeGeneration.CodeValueReference
struct CodeValueReference_t975266093;
// Mono.CodeGeneration.CodeExpression
struct CodeExpression_t2163794278;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.CodeGeneration.CodeAssignment
struct  CodeAssignment_t720477174  : public CodeExpression_t2163794278
{
public:
	// Mono.CodeGeneration.CodeValueReference Mono.CodeGeneration.CodeAssignment::var
	CodeValueReference_t975266093 * ___var_0;
	// Mono.CodeGeneration.CodeExpression Mono.CodeGeneration.CodeAssignment::exp
	CodeExpression_t2163794278 * ___exp_1;

public:
	inline static int32_t get_offset_of_var_0() { return static_cast<int32_t>(offsetof(CodeAssignment_t720477174, ___var_0)); }
	inline CodeValueReference_t975266093 * get_var_0() const { return ___var_0; }
	inline CodeValueReference_t975266093 ** get_address_of_var_0() { return &___var_0; }
	inline void set_var_0(CodeValueReference_t975266093 * value)
	{
		___var_0 = value;
		Il2CppCodeGenWriteBarrier(&___var_0, value);
	}

	inline static int32_t get_offset_of_exp_1() { return static_cast<int32_t>(offsetof(CodeAssignment_t720477174, ___exp_1)); }
	inline CodeExpression_t2163794278 * get_exp_1() const { return ___exp_1; }
	inline CodeExpression_t2163794278 ** get_address_of_exp_1() { return &___exp_1; }
	inline void set_exp_1(CodeExpression_t2163794278 * value)
	{
		___exp_1 = value;
		Il2CppCodeGenWriteBarrier(&___exp_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
