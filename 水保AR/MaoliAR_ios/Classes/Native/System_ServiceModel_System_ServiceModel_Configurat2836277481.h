﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.XmlDictionaryReaderQuotasElement
struct  XmlDictionaryReaderQuotasElement_t2836277481  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct XmlDictionaryReaderQuotasElement_t2836277481_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.XmlDictionaryReaderQuotasElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.XmlDictionaryReaderQuotasElement::max_array_length
	ConfigurationProperty_t3590861854 * ___max_array_length_14;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.XmlDictionaryReaderQuotasElement::max_bytes_per_read
	ConfigurationProperty_t3590861854 * ___max_bytes_per_read_15;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.XmlDictionaryReaderQuotasElement::max_depth
	ConfigurationProperty_t3590861854 * ___max_depth_16;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.XmlDictionaryReaderQuotasElement::max_name_table_char_count
	ConfigurationProperty_t3590861854 * ___max_name_table_char_count_17;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.XmlDictionaryReaderQuotasElement::max_string_content_length
	ConfigurationProperty_t3590861854 * ___max_string_content_length_18;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(XmlDictionaryReaderQuotasElement_t2836277481_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier(&___properties_13, value);
	}

	inline static int32_t get_offset_of_max_array_length_14() { return static_cast<int32_t>(offsetof(XmlDictionaryReaderQuotasElement_t2836277481_StaticFields, ___max_array_length_14)); }
	inline ConfigurationProperty_t3590861854 * get_max_array_length_14() const { return ___max_array_length_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_max_array_length_14() { return &___max_array_length_14; }
	inline void set_max_array_length_14(ConfigurationProperty_t3590861854 * value)
	{
		___max_array_length_14 = value;
		Il2CppCodeGenWriteBarrier(&___max_array_length_14, value);
	}

	inline static int32_t get_offset_of_max_bytes_per_read_15() { return static_cast<int32_t>(offsetof(XmlDictionaryReaderQuotasElement_t2836277481_StaticFields, ___max_bytes_per_read_15)); }
	inline ConfigurationProperty_t3590861854 * get_max_bytes_per_read_15() const { return ___max_bytes_per_read_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_max_bytes_per_read_15() { return &___max_bytes_per_read_15; }
	inline void set_max_bytes_per_read_15(ConfigurationProperty_t3590861854 * value)
	{
		___max_bytes_per_read_15 = value;
		Il2CppCodeGenWriteBarrier(&___max_bytes_per_read_15, value);
	}

	inline static int32_t get_offset_of_max_depth_16() { return static_cast<int32_t>(offsetof(XmlDictionaryReaderQuotasElement_t2836277481_StaticFields, ___max_depth_16)); }
	inline ConfigurationProperty_t3590861854 * get_max_depth_16() const { return ___max_depth_16; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_max_depth_16() { return &___max_depth_16; }
	inline void set_max_depth_16(ConfigurationProperty_t3590861854 * value)
	{
		___max_depth_16 = value;
		Il2CppCodeGenWriteBarrier(&___max_depth_16, value);
	}

	inline static int32_t get_offset_of_max_name_table_char_count_17() { return static_cast<int32_t>(offsetof(XmlDictionaryReaderQuotasElement_t2836277481_StaticFields, ___max_name_table_char_count_17)); }
	inline ConfigurationProperty_t3590861854 * get_max_name_table_char_count_17() const { return ___max_name_table_char_count_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_max_name_table_char_count_17() { return &___max_name_table_char_count_17; }
	inline void set_max_name_table_char_count_17(ConfigurationProperty_t3590861854 * value)
	{
		___max_name_table_char_count_17 = value;
		Il2CppCodeGenWriteBarrier(&___max_name_table_char_count_17, value);
	}

	inline static int32_t get_offset_of_max_string_content_length_18() { return static_cast<int32_t>(offsetof(XmlDictionaryReaderQuotasElement_t2836277481_StaticFields, ___max_string_content_length_18)); }
	inline ConfigurationProperty_t3590861854 * get_max_string_content_length_18() const { return ___max_string_content_length_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_max_string_content_length_18() { return &___max_string_content_length_18; }
	inline void set_max_string_content_length_18(ConfigurationProperty_t3590861854 * value)
	{
		___max_string_content_length_18 = value;
		Il2CppCodeGenWriteBarrier(&___max_string_content_length_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
