﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3156163955.h"

// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.ProfileSection
struct  ProfileSection_t2357888679  : public ConfigurationSection_t3156163955
{
public:

public:
};

struct ProfileSection_t2357888679_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Web.Configuration.ProfileSection::automaticSaveEnabledProp
	ConfigurationProperty_t3590861854 * ___automaticSaveEnabledProp_17;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.ProfileSection::defaultProviderProp
	ConfigurationProperty_t3590861854 * ___defaultProviderProp_18;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.ProfileSection::enabledProp
	ConfigurationProperty_t3590861854 * ___enabledProp_19;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.ProfileSection::inheritsProp
	ConfigurationProperty_t3590861854 * ___inheritsProp_20;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.ProfileSection::propertySettingsProp
	ConfigurationProperty_t3590861854 * ___propertySettingsProp_21;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.ProfileSection::providersProp
	ConfigurationProperty_t3590861854 * ___providersProp_22;
	// System.Configuration.ConfigurationPropertyCollection System.Web.Configuration.ProfileSection::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_23;

public:
	inline static int32_t get_offset_of_automaticSaveEnabledProp_17() { return static_cast<int32_t>(offsetof(ProfileSection_t2357888679_StaticFields, ___automaticSaveEnabledProp_17)); }
	inline ConfigurationProperty_t3590861854 * get_automaticSaveEnabledProp_17() const { return ___automaticSaveEnabledProp_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_automaticSaveEnabledProp_17() { return &___automaticSaveEnabledProp_17; }
	inline void set_automaticSaveEnabledProp_17(ConfigurationProperty_t3590861854 * value)
	{
		___automaticSaveEnabledProp_17 = value;
		Il2CppCodeGenWriteBarrier(&___automaticSaveEnabledProp_17, value);
	}

	inline static int32_t get_offset_of_defaultProviderProp_18() { return static_cast<int32_t>(offsetof(ProfileSection_t2357888679_StaticFields, ___defaultProviderProp_18)); }
	inline ConfigurationProperty_t3590861854 * get_defaultProviderProp_18() const { return ___defaultProviderProp_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_defaultProviderProp_18() { return &___defaultProviderProp_18; }
	inline void set_defaultProviderProp_18(ConfigurationProperty_t3590861854 * value)
	{
		___defaultProviderProp_18 = value;
		Il2CppCodeGenWriteBarrier(&___defaultProviderProp_18, value);
	}

	inline static int32_t get_offset_of_enabledProp_19() { return static_cast<int32_t>(offsetof(ProfileSection_t2357888679_StaticFields, ___enabledProp_19)); }
	inline ConfigurationProperty_t3590861854 * get_enabledProp_19() const { return ___enabledProp_19; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_enabledProp_19() { return &___enabledProp_19; }
	inline void set_enabledProp_19(ConfigurationProperty_t3590861854 * value)
	{
		___enabledProp_19 = value;
		Il2CppCodeGenWriteBarrier(&___enabledProp_19, value);
	}

	inline static int32_t get_offset_of_inheritsProp_20() { return static_cast<int32_t>(offsetof(ProfileSection_t2357888679_StaticFields, ___inheritsProp_20)); }
	inline ConfigurationProperty_t3590861854 * get_inheritsProp_20() const { return ___inheritsProp_20; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_inheritsProp_20() { return &___inheritsProp_20; }
	inline void set_inheritsProp_20(ConfigurationProperty_t3590861854 * value)
	{
		___inheritsProp_20 = value;
		Il2CppCodeGenWriteBarrier(&___inheritsProp_20, value);
	}

	inline static int32_t get_offset_of_propertySettingsProp_21() { return static_cast<int32_t>(offsetof(ProfileSection_t2357888679_StaticFields, ___propertySettingsProp_21)); }
	inline ConfigurationProperty_t3590861854 * get_propertySettingsProp_21() const { return ___propertySettingsProp_21; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_propertySettingsProp_21() { return &___propertySettingsProp_21; }
	inline void set_propertySettingsProp_21(ConfigurationProperty_t3590861854 * value)
	{
		___propertySettingsProp_21 = value;
		Il2CppCodeGenWriteBarrier(&___propertySettingsProp_21, value);
	}

	inline static int32_t get_offset_of_providersProp_22() { return static_cast<int32_t>(offsetof(ProfileSection_t2357888679_StaticFields, ___providersProp_22)); }
	inline ConfigurationProperty_t3590861854 * get_providersProp_22() const { return ___providersProp_22; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_providersProp_22() { return &___providersProp_22; }
	inline void set_providersProp_22(ConfigurationProperty_t3590861854 * value)
	{
		___providersProp_22 = value;
		Il2CppCodeGenWriteBarrier(&___providersProp_22, value);
	}

	inline static int32_t get_offset_of_properties_23() { return static_cast<int32_t>(offsetof(ProfileSection_t2357888679_StaticFields, ___properties_23)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_23() const { return ___properties_23; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_23() { return &___properties_23; }
	inline void set_properties_23(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_23 = value;
		Il2CppCodeGenWriteBarrier(&___properties_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
