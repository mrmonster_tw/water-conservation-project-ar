﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Bi859993683.h"
#include "System_ServiceModel_System_ServiceModel_WSMessageE2322128963.h"

// System.ServiceModel.EnvelopeVersion
struct EnvelopeVersion_t2487833488;
// System.Text.Encoding
struct Encoding_t1523322056;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.WSHttpBindingBase
struct  WSHttpBindingBase_t2163194083  : public Binding_t859993683
{
public:
	// System.ServiceModel.WSMessageEncoding System.ServiceModel.WSHttpBindingBase::message_encoding
	int32_t ___message_encoding_6;
	// System.ServiceModel.EnvelopeVersion System.ServiceModel.WSHttpBindingBase::env_version
	EnvelopeVersion_t2487833488 * ___env_version_7;
	// System.Text.Encoding System.ServiceModel.WSHttpBindingBase::text_encoding
	Encoding_t1523322056 * ___text_encoding_8;

public:
	inline static int32_t get_offset_of_message_encoding_6() { return static_cast<int32_t>(offsetof(WSHttpBindingBase_t2163194083, ___message_encoding_6)); }
	inline int32_t get_message_encoding_6() const { return ___message_encoding_6; }
	inline int32_t* get_address_of_message_encoding_6() { return &___message_encoding_6; }
	inline void set_message_encoding_6(int32_t value)
	{
		___message_encoding_6 = value;
	}

	inline static int32_t get_offset_of_env_version_7() { return static_cast<int32_t>(offsetof(WSHttpBindingBase_t2163194083, ___env_version_7)); }
	inline EnvelopeVersion_t2487833488 * get_env_version_7() const { return ___env_version_7; }
	inline EnvelopeVersion_t2487833488 ** get_address_of_env_version_7() { return &___env_version_7; }
	inline void set_env_version_7(EnvelopeVersion_t2487833488 * value)
	{
		___env_version_7 = value;
		Il2CppCodeGenWriteBarrier(&___env_version_7, value);
	}

	inline static int32_t get_offset_of_text_encoding_8() { return static_cast<int32_t>(offsetof(WSHttpBindingBase_t2163194083, ___text_encoding_8)); }
	inline Encoding_t1523322056 * get_text_encoding_8() const { return ___text_encoding_8; }
	inline Encoding_t1523322056 ** get_address_of_text_encoding_8() { return &___text_encoding_8; }
	inline void set_text_encoding_8(Encoding_t1523322056 * value)
	{
		___text_encoding_8 = value;
		Il2CppCodeGenWriteBarrier(&___text_encoding_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
