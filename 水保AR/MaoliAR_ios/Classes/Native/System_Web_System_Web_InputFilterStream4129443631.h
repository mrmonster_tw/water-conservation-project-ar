﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_Stream1273022909.h"

// System.IO.Stream
struct Stream_t1273022909;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.InputFilterStream
struct  InputFilterStream_t4129443631  : public Stream_t1273022909
{
public:
	// System.IO.Stream System.Web.InputFilterStream::stream
	Stream_t1273022909 * ___stream_2;

public:
	inline static int32_t get_offset_of_stream_2() { return static_cast<int32_t>(offsetof(InputFilterStream_t4129443631, ___stream_2)); }
	inline Stream_t1273022909 * get_stream_2() const { return ___stream_2; }
	inline Stream_t1273022909 ** get_address_of_stream_2() { return &___stream_2; }
	inline void set_stream_2(Stream_t1273022909 * value)
	{
		___stream_2 = value;
		Il2CppCodeGenWriteBarrier(&___stream_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
