﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_Reflection_MethodAttributes2366443849.h"

// System.Reflection.MethodBase
struct MethodBase_t609368412;
// Mono.CodeGeneration.CodeBuilder
struct CodeBuilder_t3190018006;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t1073948154;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// Mono.CodeGeneration.CodeClass
struct CodeClass_t2288932064;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.CodeGeneration.CodeMethod
struct  CodeMethod_t1546731557  : public Il2CppObject
{
public:
	// System.Reflection.MethodBase Mono.CodeGeneration.CodeMethod::methodBase
	MethodBase_t609368412 * ___methodBase_0;
	// Mono.CodeGeneration.CodeBuilder Mono.CodeGeneration.CodeMethod::builder
	CodeBuilder_t3190018006 * ___builder_1;
	// System.String Mono.CodeGeneration.CodeMethod::name
	String_t* ___name_2;
	// System.Reflection.MethodAttributes Mono.CodeGeneration.CodeMethod::attributes
	int32_t ___attributes_3;
	// System.Type Mono.CodeGeneration.CodeMethod::returnType
	Type_t * ___returnType_4;
	// System.Reflection.Emit.TypeBuilder Mono.CodeGeneration.CodeMethod::typeBuilder
	TypeBuilder_t1073948154 * ___typeBuilder_5;
	// System.Type[] Mono.CodeGeneration.CodeMethod::parameterTypes
	TypeU5BU5D_t3940880105* ___parameterTypes_6;
	// System.Collections.ArrayList Mono.CodeGeneration.CodeMethod::customAttributes
	ArrayList_t2718874744 * ___customAttributes_7;
	// Mono.CodeGeneration.CodeClass Mono.CodeGeneration.CodeMethod::cls
	CodeClass_t2288932064 * ___cls_8;

public:
	inline static int32_t get_offset_of_methodBase_0() { return static_cast<int32_t>(offsetof(CodeMethod_t1546731557, ___methodBase_0)); }
	inline MethodBase_t609368412 * get_methodBase_0() const { return ___methodBase_0; }
	inline MethodBase_t609368412 ** get_address_of_methodBase_0() { return &___methodBase_0; }
	inline void set_methodBase_0(MethodBase_t609368412 * value)
	{
		___methodBase_0 = value;
		Il2CppCodeGenWriteBarrier(&___methodBase_0, value);
	}

	inline static int32_t get_offset_of_builder_1() { return static_cast<int32_t>(offsetof(CodeMethod_t1546731557, ___builder_1)); }
	inline CodeBuilder_t3190018006 * get_builder_1() const { return ___builder_1; }
	inline CodeBuilder_t3190018006 ** get_address_of_builder_1() { return &___builder_1; }
	inline void set_builder_1(CodeBuilder_t3190018006 * value)
	{
		___builder_1 = value;
		Il2CppCodeGenWriteBarrier(&___builder_1, value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(CodeMethod_t1546731557, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}

	inline static int32_t get_offset_of_attributes_3() { return static_cast<int32_t>(offsetof(CodeMethod_t1546731557, ___attributes_3)); }
	inline int32_t get_attributes_3() const { return ___attributes_3; }
	inline int32_t* get_address_of_attributes_3() { return &___attributes_3; }
	inline void set_attributes_3(int32_t value)
	{
		___attributes_3 = value;
	}

	inline static int32_t get_offset_of_returnType_4() { return static_cast<int32_t>(offsetof(CodeMethod_t1546731557, ___returnType_4)); }
	inline Type_t * get_returnType_4() const { return ___returnType_4; }
	inline Type_t ** get_address_of_returnType_4() { return &___returnType_4; }
	inline void set_returnType_4(Type_t * value)
	{
		___returnType_4 = value;
		Il2CppCodeGenWriteBarrier(&___returnType_4, value);
	}

	inline static int32_t get_offset_of_typeBuilder_5() { return static_cast<int32_t>(offsetof(CodeMethod_t1546731557, ___typeBuilder_5)); }
	inline TypeBuilder_t1073948154 * get_typeBuilder_5() const { return ___typeBuilder_5; }
	inline TypeBuilder_t1073948154 ** get_address_of_typeBuilder_5() { return &___typeBuilder_5; }
	inline void set_typeBuilder_5(TypeBuilder_t1073948154 * value)
	{
		___typeBuilder_5 = value;
		Il2CppCodeGenWriteBarrier(&___typeBuilder_5, value);
	}

	inline static int32_t get_offset_of_parameterTypes_6() { return static_cast<int32_t>(offsetof(CodeMethod_t1546731557, ___parameterTypes_6)); }
	inline TypeU5BU5D_t3940880105* get_parameterTypes_6() const { return ___parameterTypes_6; }
	inline TypeU5BU5D_t3940880105** get_address_of_parameterTypes_6() { return &___parameterTypes_6; }
	inline void set_parameterTypes_6(TypeU5BU5D_t3940880105* value)
	{
		___parameterTypes_6 = value;
		Il2CppCodeGenWriteBarrier(&___parameterTypes_6, value);
	}

	inline static int32_t get_offset_of_customAttributes_7() { return static_cast<int32_t>(offsetof(CodeMethod_t1546731557, ___customAttributes_7)); }
	inline ArrayList_t2718874744 * get_customAttributes_7() const { return ___customAttributes_7; }
	inline ArrayList_t2718874744 ** get_address_of_customAttributes_7() { return &___customAttributes_7; }
	inline void set_customAttributes_7(ArrayList_t2718874744 * value)
	{
		___customAttributes_7 = value;
		Il2CppCodeGenWriteBarrier(&___customAttributes_7, value);
	}

	inline static int32_t get_offset_of_cls_8() { return static_cast<int32_t>(offsetof(CodeMethod_t1546731557, ___cls_8)); }
	inline CodeClass_t2288932064 * get_cls_8() const { return ___cls_8; }
	inline CodeClass_t2288932064 ** get_address_of_cls_8() { return &___cls_8; }
	inline void set_cls_8(CodeClass_t2288932064 * value)
	{
		___cls_8 = value;
		Il2CppCodeGenWriteBarrier(&___cls_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
