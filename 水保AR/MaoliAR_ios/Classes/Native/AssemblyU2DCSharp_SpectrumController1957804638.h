﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HighlighterController3493614325.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpectrumController
struct  SpectrumController_t1957804638  : public HighlighterController_t3493614325
{
public:
	// System.Single SpectrumController::speed
	float ___speed_5;
	// System.Int32 SpectrumController::period
	int32_t ___period_6;
	// System.Single SpectrumController::counter
	float ___counter_7;

public:
	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(SpectrumController_t1957804638, ___speed_5)); }
	inline float get_speed_5() const { return ___speed_5; }
	inline float* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(float value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_period_6() { return static_cast<int32_t>(offsetof(SpectrumController_t1957804638, ___period_6)); }
	inline int32_t get_period_6() const { return ___period_6; }
	inline int32_t* get_address_of_period_6() { return &___period_6; }
	inline void set_period_6(int32_t value)
	{
		___period_6 = value;
	}

	inline static int32_t get_offset_of_counter_7() { return static_cast<int32_t>(offsetof(SpectrumController_t1957804638, ___counter_7)); }
	inline float get_counter_7() const { return ___counter_7; }
	inline float* get_address_of_counter_7() { return &___counter_7; }
	inline void set_counter_7(float value)
	{
		___counter_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
