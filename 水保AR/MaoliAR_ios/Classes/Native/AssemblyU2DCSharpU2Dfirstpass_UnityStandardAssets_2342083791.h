﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"
#include "UnityEngine_UnityEngine_Vector22156229523.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.LensAberrations/VignetteSettings
struct  VignetteSettings_t2342083791 
{
public:
	// System.Boolean UnityStandardAssets.CinematicEffects.LensAberrations/VignetteSettings::enabled
	bool ___enabled_0;
	// UnityEngine.Color UnityStandardAssets.CinematicEffects.LensAberrations/VignetteSettings::color
	Color_t2555686324  ___color_1;
	// UnityEngine.Vector2 UnityStandardAssets.CinematicEffects.LensAberrations/VignetteSettings::center
	Vector2_t2156229523  ___center_2;
	// System.Single UnityStandardAssets.CinematicEffects.LensAberrations/VignetteSettings::intensity
	float ___intensity_3;
	// System.Single UnityStandardAssets.CinematicEffects.LensAberrations/VignetteSettings::smoothness
	float ___smoothness_4;
	// System.Single UnityStandardAssets.CinematicEffects.LensAberrations/VignetteSettings::roundness
	float ___roundness_5;
	// System.Single UnityStandardAssets.CinematicEffects.LensAberrations/VignetteSettings::blur
	float ___blur_6;
	// System.Single UnityStandardAssets.CinematicEffects.LensAberrations/VignetteSettings::desaturate
	float ___desaturate_7;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(VignetteSettings_t2342083791, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}

	inline static int32_t get_offset_of_color_1() { return static_cast<int32_t>(offsetof(VignetteSettings_t2342083791, ___color_1)); }
	inline Color_t2555686324  get_color_1() const { return ___color_1; }
	inline Color_t2555686324 * get_address_of_color_1() { return &___color_1; }
	inline void set_color_1(Color_t2555686324  value)
	{
		___color_1 = value;
	}

	inline static int32_t get_offset_of_center_2() { return static_cast<int32_t>(offsetof(VignetteSettings_t2342083791, ___center_2)); }
	inline Vector2_t2156229523  get_center_2() const { return ___center_2; }
	inline Vector2_t2156229523 * get_address_of_center_2() { return &___center_2; }
	inline void set_center_2(Vector2_t2156229523  value)
	{
		___center_2 = value;
	}

	inline static int32_t get_offset_of_intensity_3() { return static_cast<int32_t>(offsetof(VignetteSettings_t2342083791, ___intensity_3)); }
	inline float get_intensity_3() const { return ___intensity_3; }
	inline float* get_address_of_intensity_3() { return &___intensity_3; }
	inline void set_intensity_3(float value)
	{
		___intensity_3 = value;
	}

	inline static int32_t get_offset_of_smoothness_4() { return static_cast<int32_t>(offsetof(VignetteSettings_t2342083791, ___smoothness_4)); }
	inline float get_smoothness_4() const { return ___smoothness_4; }
	inline float* get_address_of_smoothness_4() { return &___smoothness_4; }
	inline void set_smoothness_4(float value)
	{
		___smoothness_4 = value;
	}

	inline static int32_t get_offset_of_roundness_5() { return static_cast<int32_t>(offsetof(VignetteSettings_t2342083791, ___roundness_5)); }
	inline float get_roundness_5() const { return ___roundness_5; }
	inline float* get_address_of_roundness_5() { return &___roundness_5; }
	inline void set_roundness_5(float value)
	{
		___roundness_5 = value;
	}

	inline static int32_t get_offset_of_blur_6() { return static_cast<int32_t>(offsetof(VignetteSettings_t2342083791, ___blur_6)); }
	inline float get_blur_6() const { return ___blur_6; }
	inline float* get_address_of_blur_6() { return &___blur_6; }
	inline void set_blur_6(float value)
	{
		___blur_6 = value;
	}

	inline static int32_t get_offset_of_desaturate_7() { return static_cast<int32_t>(offsetof(VignetteSettings_t2342083791, ___desaturate_7)); }
	inline float get_desaturate_7() const { return ___desaturate_7; }
	inline float* get_address_of_desaturate_7() { return &___desaturate_7; }
	inline void set_desaturate_7(float value)
	{
		___desaturate_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityStandardAssets.CinematicEffects.LensAberrations/VignetteSettings
struct VignetteSettings_t2342083791_marshaled_pinvoke
{
	int32_t ___enabled_0;
	Color_t2555686324  ___color_1;
	Vector2_t2156229523  ___center_2;
	float ___intensity_3;
	float ___smoothness_4;
	float ___roundness_5;
	float ___blur_6;
	float ___desaturate_7;
};
// Native definition for COM marshalling of UnityStandardAssets.CinematicEffects.LensAberrations/VignetteSettings
struct VignetteSettings_t2342083791_marshaled_com
{
	int32_t ___enabled_0;
	Color_t2555686324  ___color_1;
	Vector2_t2156229523  ___center_2;
	float ___intensity_3;
	float ___smoothness_4;
	float ___roundness_5;
	float ___blur_6;
	float ___desaturate_7;
};
