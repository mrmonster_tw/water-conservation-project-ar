﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.UI.ControlBuilder
struct ControlBuilder_t2523018631;
// System.Web.Compilation.ILocation
struct ILocation_t3961726794;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.BuilderLocation
struct  BuilderLocation_t1517769213  : public Il2CppObject
{
public:
	// System.Web.UI.ControlBuilder System.Web.Compilation.BuilderLocation::Builder
	ControlBuilder_t2523018631 * ___Builder_0;
	// System.Web.Compilation.ILocation System.Web.Compilation.BuilderLocation::Location
	Il2CppObject * ___Location_1;

public:
	inline static int32_t get_offset_of_Builder_0() { return static_cast<int32_t>(offsetof(BuilderLocation_t1517769213, ___Builder_0)); }
	inline ControlBuilder_t2523018631 * get_Builder_0() const { return ___Builder_0; }
	inline ControlBuilder_t2523018631 ** get_address_of_Builder_0() { return &___Builder_0; }
	inline void set_Builder_0(ControlBuilder_t2523018631 * value)
	{
		___Builder_0 = value;
		Il2CppCodeGenWriteBarrier(&___Builder_0, value);
	}

	inline static int32_t get_offset_of_Location_1() { return static_cast<int32_t>(offsetof(BuilderLocation_t1517769213, ___Location_1)); }
	inline Il2CppObject * get_Location_1() const { return ___Location_1; }
	inline Il2CppObject ** get_address_of_Location_1() { return &___Location_1; }
	inline void set_Location_1(Il2CppObject * value)
	{
		___Location_1 = value;
		Il2CppCodeGenWriteBarrier(&___Location_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
