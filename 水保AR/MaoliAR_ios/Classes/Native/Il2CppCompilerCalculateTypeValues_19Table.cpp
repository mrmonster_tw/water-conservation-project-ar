﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_Schema_XmlAtomicValue3775050121.h"
#include "System_Xml_System_Xml_Schema_XmlSchema3742557897.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSerializer1041468262.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAll1118454309.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotated2603549639.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotation2553753397.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAny1119175207.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAnyAttribute963227996.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAppInfo426084712.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAttribute2797257020.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAttributeGrou246430545.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAttributeGrou846390688.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaChoice959520675.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaCollection3610399789.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaCollectionEn1663512188.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaCompilationS2218765537.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaComplexConte3528540772.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaComplexConte2396613513.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaComplexConte3155540863.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaComplexType3740801802.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaContent1040349258.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaContentModel602185179.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaContentProces826201100.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaContentType3022550233.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDatatype322714710.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDerivationMe1774354337.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDocumentatio1182960532.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaElement427880856.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaEnumerationF2156689038.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaException3511258692.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaExternal3074890143.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFacet1906017689.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFacet_Facet1501039206.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaForm4264307319.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFractionDigi2589598443.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaGroup1441741786.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaGroupBase3631079376.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaGroupRef1314446647.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaIdentityConst297318432.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaImport3509836441.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaInclude2307352039.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaInfo997462956.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaKey3030860318.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaKeyref3124006214.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaLengthFacet4286280832.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaMaxExclusiveF786951263.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaMaxInclusiveF719708644.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaMaxLengthFac2192171319.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaMinExclusiveFa85871952.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaMinInclusiveFa18629333.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaMinLengthFace686585762.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaNotation2664560277.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaNumericFacet3753040035.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObject1315720168.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectCollec1064819932.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectEnumera503074204.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectTable2546974348.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectTable_1321079153.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaParticle3828501457.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaParticle_Emp2028271315.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaPatternFacet3316004401.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaRedefine4020109446.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSet266093086.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSequence2018345177.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSerializationW52073008.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleConten4264369274.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleConten1269327470.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleConten2746076865.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleType2678868104.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeCon599285223.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeLis472803608.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeRe3925451115.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeUn4071426880.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaTotalDigitsFa297930215.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaType2033747345.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaUnique2867867737.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaUse647315988.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidator1317961423.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidator_Tra766760941.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidity3794542157.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidationExc816160496.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaWhiteSpaceFa4158372164.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaXPath3156455507.h"
#include "System_Xml_System_Xml_Schema_XmlSeverityType1894651412.h"
#include "System_Xml_System_Xml_Schema_ValidationHandler3551360050.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaUtil956145399.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaReader1164558392.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidationFla877176585.h"
#include "System_Xml_System_Xml_Schema_XmlTypeCode2623622950.h"
#include "System_Xml_System_Xml_Serialization_CodeExporter3497037670.h"
#include "System_Xml_System_Xml_Serialization_CodeGeneration2228734478.h"
#include "System_Xml_System_Xml_Serialization_CodeIdentifier2202687290.h"
#include "System_Xml_System_Xml_Serialization_CodeIdentifier4095039290.h"
#include "System_Xml_System_Xml_Serialization_ImportContext1801135953.h"
#include "System_Xml_System_Xml_Serialization_KeyHelper3772181821.h"
#include "System_Xml_System_Xml_Serialization_MapCodeGenerat1608106344.h"
#include "System_Xml_System_Xml_Serialization_ReflectionHelpe300476302.h"
#include "System_Xml_System_Xml_Serialization_SchemaImporter2185830201.h"
#include "System_Xml_System_Xml_Serialization_SchemaTypes1741406581.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (XmlAtomicValue_t3775050121), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1900[11] = 
{
	XmlAtomicValue_t3775050121::get_offset_of_booleanValue_0(),
	XmlAtomicValue_t3775050121::get_offset_of_dateTimeValue_1(),
	XmlAtomicValue_t3775050121::get_offset_of_decimalValue_2(),
	XmlAtomicValue_t3775050121::get_offset_of_doubleValue_3(),
	XmlAtomicValue_t3775050121::get_offset_of_intValue_4(),
	XmlAtomicValue_t3775050121::get_offset_of_longValue_5(),
	XmlAtomicValue_t3775050121::get_offset_of_objectValue_6(),
	XmlAtomicValue_t3775050121::get_offset_of_floatValue_7(),
	XmlAtomicValue_t3775050121::get_offset_of_stringValue_8(),
	XmlAtomicValue_t3775050121::get_offset_of_schemaType_9(),
	XmlAtomicValue_t3775050121::get_offset_of_xmlTypeCode_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (XmlSchema_t3742557897), -1, sizeof(XmlSchema_t3742557897_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1901[21] = 
{
	XmlSchema_t3742557897::get_offset_of_attributeFormDefault_13(),
	XmlSchema_t3742557897::get_offset_of_attributeGroups_14(),
	XmlSchema_t3742557897::get_offset_of_attributes_15(),
	XmlSchema_t3742557897::get_offset_of_blockDefault_16(),
	XmlSchema_t3742557897::get_offset_of_elementFormDefault_17(),
	XmlSchema_t3742557897::get_offset_of_elements_18(),
	XmlSchema_t3742557897::get_offset_of_finalDefault_19(),
	XmlSchema_t3742557897::get_offset_of_groups_20(),
	XmlSchema_t3742557897::get_offset_of_id_21(),
	XmlSchema_t3742557897::get_offset_of_includes_22(),
	XmlSchema_t3742557897::get_offset_of_items_23(),
	XmlSchema_t3742557897::get_offset_of_notations_24(),
	XmlSchema_t3742557897::get_offset_of_schemaTypes_25(),
	XmlSchema_t3742557897::get_offset_of_targetNamespace_26(),
	XmlSchema_t3742557897::get_offset_of_unhandledAttributes_27(),
	XmlSchema_t3742557897::get_offset_of_version_28(),
	XmlSchema_t3742557897::get_offset_of_schemas_29(),
	XmlSchema_t3742557897::get_offset_of_nameTable_30(),
	XmlSchema_t3742557897::get_offset_of_missedSubComponents_31(),
	XmlSchema_t3742557897::get_offset_of_compilationItems_32(),
	XmlSchema_t3742557897_StaticFields::get_offset_of_U3CU3Ef__switchU24map41_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (XmlSchemaSerializer_t1041468262), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { sizeof (XmlSchemaAll_t1118454309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1903[3] = 
{
	XmlSchemaAll_t1118454309::get_offset_of_schema_28(),
	XmlSchemaAll_t1118454309::get_offset_of_items_29(),
	XmlSchemaAll_t1118454309::get_offset_of_emptiable_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (XmlSchemaAnnotated_t2603549639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1904[3] = 
{
	XmlSchemaAnnotated_t2603549639::get_offset_of_annotation_13(),
	XmlSchemaAnnotated_t2603549639::get_offset_of_id_14(),
	XmlSchemaAnnotated_t2603549639::get_offset_of_unhandledAttributes_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (XmlSchemaAnnotation_t2553753397), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1905[3] = 
{
	XmlSchemaAnnotation_t2553753397::get_offset_of_id_13(),
	XmlSchemaAnnotation_t2553753397::get_offset_of_items_14(),
	XmlSchemaAnnotation_t2553753397::get_offset_of_unhandledAttributes_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (XmlSchemaAny_t1119175207), -1, sizeof(XmlSchemaAny_t1119175207_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1906[4] = 
{
	XmlSchemaAny_t1119175207_StaticFields::get_offset_of_anyTypeContent_27(),
	XmlSchemaAny_t1119175207::get_offset_of_nameSpace_28(),
	XmlSchemaAny_t1119175207::get_offset_of_processing_29(),
	XmlSchemaAny_t1119175207::get_offset_of_wildcard_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (XmlSchemaAnyAttribute_t963227996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1907[3] = 
{
	XmlSchemaAnyAttribute_t963227996::get_offset_of_nameSpace_16(),
	XmlSchemaAnyAttribute_t963227996::get_offset_of_processing_17(),
	XmlSchemaAnyAttribute_t963227996::get_offset_of_wildcard_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (XmlSchemaAppInfo_t426084712), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1908[2] = 
{
	XmlSchemaAppInfo_t426084712::get_offset_of_markup_13(),
	XmlSchemaAppInfo_t426084712::get_offset_of_source_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (XmlSchemaAttribute_t2797257020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1909[17] = 
{
	XmlSchemaAttribute_t2797257020::get_offset_of_attributeType_16(),
	XmlSchemaAttribute_t2797257020::get_offset_of_attributeSchemaType_17(),
	XmlSchemaAttribute_t2797257020::get_offset_of_defaultValue_18(),
	XmlSchemaAttribute_t2797257020::get_offset_of_fixedValue_19(),
	XmlSchemaAttribute_t2797257020::get_offset_of_validatedDefaultValue_20(),
	XmlSchemaAttribute_t2797257020::get_offset_of_validatedFixedValue_21(),
	XmlSchemaAttribute_t2797257020::get_offset_of_validatedFixedTypedValue_22(),
	XmlSchemaAttribute_t2797257020::get_offset_of_form_23(),
	XmlSchemaAttribute_t2797257020::get_offset_of_name_24(),
	XmlSchemaAttribute_t2797257020::get_offset_of_targetNamespace_25(),
	XmlSchemaAttribute_t2797257020::get_offset_of_qualifiedName_26(),
	XmlSchemaAttribute_t2797257020::get_offset_of_refName_27(),
	XmlSchemaAttribute_t2797257020::get_offset_of_schemaType_28(),
	XmlSchemaAttribute_t2797257020::get_offset_of_schemaTypeName_29(),
	XmlSchemaAttribute_t2797257020::get_offset_of_use_30(),
	XmlSchemaAttribute_t2797257020::get_offset_of_validatedUse_31(),
	XmlSchemaAttribute_t2797257020::get_offset_of_referencedAttribute_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (XmlSchemaAttributeGroup_t246430545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1910[8] = 
{
	XmlSchemaAttributeGroup_t246430545::get_offset_of_anyAttribute_16(),
	XmlSchemaAttributeGroup_t246430545::get_offset_of_attributes_17(),
	XmlSchemaAttributeGroup_t246430545::get_offset_of_name_18(),
	XmlSchemaAttributeGroup_t246430545::get_offset_of_redefined_19(),
	XmlSchemaAttributeGroup_t246430545::get_offset_of_qualifiedName_20(),
	XmlSchemaAttributeGroup_t246430545::get_offset_of_attributeUses_21(),
	XmlSchemaAttributeGroup_t246430545::get_offset_of_anyAttributeUse_22(),
	XmlSchemaAttributeGroup_t246430545::get_offset_of_AttributeGroupRecursionCheck_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { sizeof (XmlSchemaAttributeGroupRef_t846390688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1911[1] = 
{
	XmlSchemaAttributeGroupRef_t846390688::get_offset_of_refName_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (XmlSchemaChoice_t959520675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1912[2] = 
{
	XmlSchemaChoice_t959520675::get_offset_of_items_28(),
	XmlSchemaChoice_t959520675::get_offset_of_minEffectiveTotalRange_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (XmlSchemaCollection_t3610399789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1913[2] = 
{
	XmlSchemaCollection_t3610399789::get_offset_of_schemaSet_0(),
	XmlSchemaCollection_t3610399789::get_offset_of_ValidationEventHandler_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (XmlSchemaCollectionEnumerator_t1663512188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1914[1] = 
{
	XmlSchemaCollectionEnumerator_t1663512188::get_offset_of_xenum_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (XmlSchemaCompilationSettings_t2218765537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1915[1] = 
{
	XmlSchemaCompilationSettings_t2218765537::get_offset_of_enable_upa_check_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (XmlSchemaComplexContent_t3528540772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1916[2] = 
{
	XmlSchemaComplexContent_t3528540772::get_offset_of_content_16(),
	XmlSchemaComplexContent_t3528540772::get_offset_of_isMixed_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (XmlSchemaComplexContentExtension_t2396613513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1917[4] = 
{
	XmlSchemaComplexContentExtension_t2396613513::get_offset_of_any_17(),
	XmlSchemaComplexContentExtension_t2396613513::get_offset_of_attributes_18(),
	XmlSchemaComplexContentExtension_t2396613513::get_offset_of_baseTypeName_19(),
	XmlSchemaComplexContentExtension_t2396613513::get_offset_of_particle_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (XmlSchemaComplexContentRestriction_t3155540863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1918[4] = 
{
	XmlSchemaComplexContentRestriction_t3155540863::get_offset_of_any_17(),
	XmlSchemaComplexContentRestriction_t3155540863::get_offset_of_attributes_18(),
	XmlSchemaComplexContentRestriction_t3155540863::get_offset_of_baseTypeName_19(),
	XmlSchemaComplexContentRestriction_t3155540863::get_offset_of_particle_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { sizeof (XmlSchemaComplexType_t3740801802), -1, sizeof(XmlSchemaComplexType_t3740801802_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1919[17] = 
{
	XmlSchemaComplexType_t3740801802::get_offset_of_anyAttribute_28(),
	XmlSchemaComplexType_t3740801802::get_offset_of_attributes_29(),
	XmlSchemaComplexType_t3740801802::get_offset_of_attributeUses_30(),
	XmlSchemaComplexType_t3740801802::get_offset_of_attributeWildcard_31(),
	XmlSchemaComplexType_t3740801802::get_offset_of_block_32(),
	XmlSchemaComplexType_t3740801802::get_offset_of_blockResolved_33(),
	XmlSchemaComplexType_t3740801802::get_offset_of_contentModel_34(),
	XmlSchemaComplexType_t3740801802::get_offset_of_validatableParticle_35(),
	XmlSchemaComplexType_t3740801802::get_offset_of_contentTypeParticle_36(),
	XmlSchemaComplexType_t3740801802::get_offset_of_isAbstract_37(),
	XmlSchemaComplexType_t3740801802::get_offset_of_isMixed_38(),
	XmlSchemaComplexType_t3740801802::get_offset_of_particle_39(),
	XmlSchemaComplexType_t3740801802::get_offset_of_resolvedContentType_40(),
	XmlSchemaComplexType_t3740801802::get_offset_of_ValidatedIsAbstract_41(),
	XmlSchemaComplexType_t3740801802_StaticFields::get_offset_of_anyType_42(),
	XmlSchemaComplexType_t3740801802_StaticFields::get_offset_of_AnyTypeName_43(),
	XmlSchemaComplexType_t3740801802::get_offset_of_CollectProcessId_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (XmlSchemaContent_t1040349258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1920[1] = 
{
	XmlSchemaContent_t1040349258::get_offset_of_actualBaseSchemaType_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (XmlSchemaContentModel_t602185179), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (XmlSchemaContentProcessing_t826201100)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1922[5] = 
{
	XmlSchemaContentProcessing_t826201100::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (XmlSchemaContentType_t3022550233)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1923[5] = 
{
	XmlSchemaContentType_t3022550233::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (XmlSchemaDatatype_t322714710), -1, sizeof(XmlSchemaDatatype_t322714710_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1924[55] = 
{
	XmlSchemaDatatype_t322714710::get_offset_of_WhitespaceValue_0(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_wsChars_1(),
	XmlSchemaDatatype_t322714710::get_offset_of_sb_2(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeAnySimpleType_3(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeString_4(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeNormalizedString_5(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeToken_6(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeLanguage_7(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeNMToken_8(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeNMTokens_9(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeName_10(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeNCName_11(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeID_12(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeIDRef_13(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeIDRefs_14(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeEntity_15(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeEntities_16(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeNotation_17(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeDecimal_18(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeInteger_19(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeLong_20(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeInt_21(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeShort_22(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeByte_23(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeNonNegativeInteger_24(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypePositiveInteger_25(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeUnsignedLong_26(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeUnsignedInt_27(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeUnsignedShort_28(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeUnsignedByte_29(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeNonPositiveInteger_30(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeNegativeInteger_31(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeFloat_32(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeDouble_33(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeBase64Binary_34(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeBoolean_35(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeAnyURI_36(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeDuration_37(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeDateTime_38(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeDate_39(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeTime_40(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeHexBinary_41(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeQName_42(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeGYearMonth_43(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeGMonthDay_44(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeGYear_45(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeGMonth_46(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeGDay_47(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeAnyAtomicType_48(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeUntypedAtomic_49(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeDayTimeDuration_50(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_datatypeYearMonthDuration_51(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_U3CU3Ef__switchU24map3E_52(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_U3CU3Ef__switchU24map3F_53(),
	XmlSchemaDatatype_t322714710_StaticFields::get_offset_of_U3CU3Ef__switchU24map40_54(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (XmlSchemaDerivationMethod_t1774354337)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1925[9] = 
{
	XmlSchemaDerivationMethod_t1774354337::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (XmlSchemaDocumentation_t1182960532), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1926[3] = 
{
	XmlSchemaDocumentation_t1182960532::get_offset_of_language_13(),
	XmlSchemaDocumentation_t1182960532::get_offset_of_markup_14(),
	XmlSchemaDocumentation_t1182960532::get_offset_of_source_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (XmlSchemaElement_t427880856), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1927[27] = 
{
	XmlSchemaElement_t427880856::get_offset_of_block_27(),
	XmlSchemaElement_t427880856::get_offset_of_constraints_28(),
	XmlSchemaElement_t427880856::get_offset_of_defaultValue_29(),
	XmlSchemaElement_t427880856::get_offset_of_elementType_30(),
	XmlSchemaElement_t427880856::get_offset_of_elementSchemaType_31(),
	XmlSchemaElement_t427880856::get_offset_of_final_32(),
	XmlSchemaElement_t427880856::get_offset_of_fixedValue_33(),
	XmlSchemaElement_t427880856::get_offset_of_form_34(),
	XmlSchemaElement_t427880856::get_offset_of_isAbstract_35(),
	XmlSchemaElement_t427880856::get_offset_of_isNillable_36(),
	XmlSchemaElement_t427880856::get_offset_of_name_37(),
	XmlSchemaElement_t427880856::get_offset_of_refName_38(),
	XmlSchemaElement_t427880856::get_offset_of_schemaType_39(),
	XmlSchemaElement_t427880856::get_offset_of_schemaTypeName_40(),
	XmlSchemaElement_t427880856::get_offset_of_substitutionGroup_41(),
	XmlSchemaElement_t427880856::get_offset_of_schema_42(),
	XmlSchemaElement_t427880856::get_offset_of_parentIsSchema_43(),
	XmlSchemaElement_t427880856::get_offset_of_qName_44(),
	XmlSchemaElement_t427880856::get_offset_of_blockResolved_45(),
	XmlSchemaElement_t427880856::get_offset_of_finalResolved_46(),
	XmlSchemaElement_t427880856::get_offset_of_referencedElement_47(),
	XmlSchemaElement_t427880856::get_offset_of_substitutingElements_48(),
	XmlSchemaElement_t427880856::get_offset_of_substitutionGroupElement_49(),
	XmlSchemaElement_t427880856::get_offset_of_actualIsAbstract_50(),
	XmlSchemaElement_t427880856::get_offset_of_actualIsNillable_51(),
	XmlSchemaElement_t427880856::get_offset_of_validatedDefaultValue_52(),
	XmlSchemaElement_t427880856::get_offset_of_validatedFixedValue_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (XmlSchemaEnumerationFacet_t2156689038), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { sizeof (XmlSchemaException_t3511258692), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1929[5] = 
{
	XmlSchemaException_t3511258692::get_offset_of_hasLineInfo_11(),
	XmlSchemaException_t3511258692::get_offset_of_lineNumber_12(),
	XmlSchemaException_t3511258692::get_offset_of_linePosition_13(),
	XmlSchemaException_t3511258692::get_offset_of_sourceObj_14(),
	XmlSchemaException_t3511258692::get_offset_of_sourceUri_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (XmlSchemaExternal_t3074890143), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1930[4] = 
{
	XmlSchemaExternal_t3074890143::get_offset_of_id_13(),
	XmlSchemaExternal_t3074890143::get_offset_of_schema_14(),
	XmlSchemaExternal_t3074890143::get_offset_of_location_15(),
	XmlSchemaExternal_t3074890143::get_offset_of_unhandledAttributes_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (XmlSchemaFacet_t1906017689), -1, sizeof(XmlSchemaFacet_t1906017689_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1931[3] = 
{
	XmlSchemaFacet_t1906017689_StaticFields::get_offset_of_AllFacets_16(),
	XmlSchemaFacet_t1906017689::get_offset_of_isFixed_17(),
	XmlSchemaFacet_t1906017689::get_offset_of_val_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (Facet_t1501039206)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1932[14] = 
{
	Facet_t1501039206::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (XmlSchemaForm_t4264307319)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1933[4] = 
{
	XmlSchemaForm_t4264307319::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (XmlSchemaFractionDigitsFacet_t2589598443), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (XmlSchemaGroup_t1441741786), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1935[4] = 
{
	XmlSchemaGroup_t1441741786::get_offset_of_name_16(),
	XmlSchemaGroup_t1441741786::get_offset_of_particle_17(),
	XmlSchemaGroup_t1441741786::get_offset_of_qualifiedName_18(),
	XmlSchemaGroup_t1441741786::get_offset_of_isCircularDefinition_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { sizeof (XmlSchemaGroupBase_t3631079376), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1936[1] = 
{
	XmlSchemaGroupBase_t3631079376::get_offset_of_compiledItems_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { sizeof (XmlSchemaGroupRef_t1314446647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1937[4] = 
{
	XmlSchemaGroupRef_t1314446647::get_offset_of_schema_27(),
	XmlSchemaGroupRef_t1314446647::get_offset_of_refName_28(),
	XmlSchemaGroupRef_t1314446647::get_offset_of_referencedGroup_29(),
	XmlSchemaGroupRef_t1314446647::get_offset_of_busy_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (XmlSchemaIdentityConstraint_t297318432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1938[5] = 
{
	XmlSchemaIdentityConstraint_t297318432::get_offset_of_fields_16(),
	XmlSchemaIdentityConstraint_t297318432::get_offset_of_name_17(),
	XmlSchemaIdentityConstraint_t297318432::get_offset_of_qName_18(),
	XmlSchemaIdentityConstraint_t297318432::get_offset_of_selector_19(),
	XmlSchemaIdentityConstraint_t297318432::get_offset_of_compiledSelector_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (XmlSchemaImport_t3509836441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1939[2] = 
{
	XmlSchemaImport_t3509836441::get_offset_of_annotation_17(),
	XmlSchemaImport_t3509836441::get_offset_of_nameSpace_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { sizeof (XmlSchemaInclude_t2307352039), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1940[1] = 
{
	XmlSchemaInclude_t2307352039::get_offset_of_annotation_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { sizeof (XmlSchemaInfo_t997462956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1941[7] = 
{
	XmlSchemaInfo_t997462956::get_offset_of_isDefault_0(),
	XmlSchemaInfo_t997462956::get_offset_of_isNil_1(),
	XmlSchemaInfo_t997462956::get_offset_of_memberType_2(),
	XmlSchemaInfo_t997462956::get_offset_of_attr_3(),
	XmlSchemaInfo_t997462956::get_offset_of_elem_4(),
	XmlSchemaInfo_t997462956::get_offset_of_type_5(),
	XmlSchemaInfo_t997462956::get_offset_of_validity_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { sizeof (XmlSchemaKey_t3030860318), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { sizeof (XmlSchemaKeyref_t3124006214), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1943[2] = 
{
	XmlSchemaKeyref_t3124006214::get_offset_of_refer_21(),
	XmlSchemaKeyref_t3124006214::get_offset_of_target_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (XmlSchemaLengthFacet_t4286280832), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (XmlSchemaMaxExclusiveFacet_t786951263), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (XmlSchemaMaxInclusiveFacet_t719708644), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (XmlSchemaMaxLengthFacet_t2192171319), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (XmlSchemaMinExclusiveFacet_t85871952), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (XmlSchemaMinInclusiveFacet_t18629333), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { sizeof (XmlSchemaMinLengthFacet_t686585762), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { sizeof (XmlSchemaNotation_t2664560277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1951[4] = 
{
	XmlSchemaNotation_t2664560277::get_offset_of_name_16(),
	XmlSchemaNotation_t2664560277::get_offset_of_pub_17(),
	XmlSchemaNotation_t2664560277::get_offset_of_system_18(),
	XmlSchemaNotation_t2664560277::get_offset_of_qualifiedName_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { sizeof (XmlSchemaNumericFacet_t3753040035), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { sizeof (XmlSchemaObject_t1315720168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1953[13] = 
{
	XmlSchemaObject_t1315720168::get_offset_of_lineNumber_0(),
	XmlSchemaObject_t1315720168::get_offset_of_linePosition_1(),
	XmlSchemaObject_t1315720168::get_offset_of_sourceUri_2(),
	XmlSchemaObject_t1315720168::get_offset_of_namespaces_3(),
	XmlSchemaObject_t1315720168::get_offset_of_unhandledAttributeList_4(),
	XmlSchemaObject_t1315720168::get_offset_of_isCompiled_5(),
	XmlSchemaObject_t1315720168::get_offset_of_errorCount_6(),
	XmlSchemaObject_t1315720168::get_offset_of_CompilationId_7(),
	XmlSchemaObject_t1315720168::get_offset_of_ValidationId_8(),
	XmlSchemaObject_t1315720168::get_offset_of_isRedefineChild_9(),
	XmlSchemaObject_t1315720168::get_offset_of_isRedefinedComponent_10(),
	XmlSchemaObject_t1315720168::get_offset_of_redefinedObject_11(),
	XmlSchemaObject_t1315720168::get_offset_of_parent_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { sizeof (XmlSchemaObjectCollection_t1064819932), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (XmlSchemaObjectEnumerator_t503074204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1955[1] = 
{
	XmlSchemaObjectEnumerator_t503074204::get_offset_of_ienum_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (XmlSchemaObjectTable_t2546974348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1956[1] = 
{
	XmlSchemaObjectTable_t2546974348::get_offset_of_table_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { sizeof (XmlSchemaObjectTableEnumerator_t1321079153), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1957[2] = 
{
	XmlSchemaObjectTableEnumerator_t1321079153::get_offset_of_xenum_0(),
	XmlSchemaObjectTableEnumerator_t1321079153::get_offset_of_tmp_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (XmlSchemaParticle_t3828501457), -1, sizeof(XmlSchemaParticle_t3828501457_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1958[11] = 
{
	XmlSchemaParticle_t3828501457::get_offset_of_minOccurs_16(),
	XmlSchemaParticle_t3828501457::get_offset_of_maxOccurs_17(),
	XmlSchemaParticle_t3828501457::get_offset_of_minstr_18(),
	XmlSchemaParticle_t3828501457::get_offset_of_maxstr_19(),
	XmlSchemaParticle_t3828501457_StaticFields::get_offset_of_empty_20(),
	XmlSchemaParticle_t3828501457::get_offset_of_validatedMinOccurs_21(),
	XmlSchemaParticle_t3828501457::get_offset_of_validatedMaxOccurs_22(),
	XmlSchemaParticle_t3828501457::get_offset_of_recursionDepth_23(),
	XmlSchemaParticle_t3828501457::get_offset_of_minEffectiveTotalRange_24(),
	XmlSchemaParticle_t3828501457::get_offset_of_parentIsGroupDefinition_25(),
	XmlSchemaParticle_t3828501457::get_offset_of_OptimizedParticle_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (EmptyParticle_t2028271315), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (XmlSchemaPatternFacet_t3316004401), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (XmlSchemaRedefine_t4020109446), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1961[4] = 
{
	XmlSchemaRedefine_t4020109446::get_offset_of_attributeGroups_17(),
	XmlSchemaRedefine_t4020109446::get_offset_of_groups_18(),
	XmlSchemaRedefine_t4020109446::get_offset_of_items_19(),
	XmlSchemaRedefine_t4020109446::get_offset_of_schemaTypes_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { sizeof (XmlSchemaSet_t266093086), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1962[12] = 
{
	XmlSchemaSet_t266093086::get_offset_of_nameTable_0(),
	XmlSchemaSet_t266093086::get_offset_of_xmlResolver_1(),
	XmlSchemaSet_t266093086::get_offset_of_schemas_2(),
	XmlSchemaSet_t266093086::get_offset_of_attributes_3(),
	XmlSchemaSet_t266093086::get_offset_of_elements_4(),
	XmlSchemaSet_t266093086::get_offset_of_types_5(),
	XmlSchemaSet_t266093086::get_offset_of_idCollection_6(),
	XmlSchemaSet_t266093086::get_offset_of_namedIdentities_7(),
	XmlSchemaSet_t266093086::get_offset_of_settings_8(),
	XmlSchemaSet_t266093086::get_offset_of_isCompiled_9(),
	XmlSchemaSet_t266093086::get_offset_of_CompilationId_10(),
	XmlSchemaSet_t266093086::get_offset_of_ValidationEventHandler_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (XmlSchemaSequence_t2018345177), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1963[1] = 
{
	XmlSchemaSequence_t2018345177::get_offset_of_items_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (XmlSchemaSerializationWriter_t52073008), -1, sizeof(XmlSchemaSerializationWriter_t52073008_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1964[1] = 
{
	XmlSchemaSerializationWriter_t52073008_StaticFields::get_offset_of_U3CU3Ef__switchU24map49_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { sizeof (XmlSchemaSimpleContent_t4264369274), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1965[1] = 
{
	XmlSchemaSimpleContent_t4264369274::get_offset_of_content_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (XmlSchemaSimpleContentExtension_t1269327470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1966[3] = 
{
	XmlSchemaSimpleContentExtension_t1269327470::get_offset_of_any_17(),
	XmlSchemaSimpleContentExtension_t1269327470::get_offset_of_attributes_18(),
	XmlSchemaSimpleContentExtension_t1269327470::get_offset_of_baseTypeName_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { sizeof (XmlSchemaSimpleContentRestriction_t2746076865), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1967[5] = 
{
	XmlSchemaSimpleContentRestriction_t2746076865::get_offset_of_any_17(),
	XmlSchemaSimpleContentRestriction_t2746076865::get_offset_of_attributes_18(),
	XmlSchemaSimpleContentRestriction_t2746076865::get_offset_of_baseType_19(),
	XmlSchemaSimpleContentRestriction_t2746076865::get_offset_of_baseTypeName_20(),
	XmlSchemaSimpleContentRestriction_t2746076865::get_offset_of_facets_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { sizeof (XmlSchemaSimpleType_t2678868104), -1, sizeof(XmlSchemaSimpleType_t2678868104_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1968[54] = 
{
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_schemaLocationType_28(),
	XmlSchemaSimpleType_t2678868104::get_offset_of_content_29(),
	XmlSchemaSimpleType_t2678868104::get_offset_of_islocal_30(),
	XmlSchemaSimpleType_t2678868104::get_offset_of_recursed_31(),
	XmlSchemaSimpleType_t2678868104::get_offset_of_variety_32(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsAnySimpleType_33(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsString_34(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsBoolean_35(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsDecimal_36(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsFloat_37(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsDouble_38(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsDuration_39(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsDateTime_40(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsTime_41(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsDate_42(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsGYearMonth_43(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsGYear_44(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsGMonthDay_45(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsGDay_46(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsGMonth_47(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsHexBinary_48(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsBase64Binary_49(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsAnyUri_50(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsQName_51(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsNotation_52(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsNormalizedString_53(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsToken_54(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsLanguage_55(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsNMToken_56(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsNMTokens_57(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsName_58(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsNCName_59(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsID_60(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsIDRef_61(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsIDRefs_62(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsEntity_63(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsEntities_64(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsInteger_65(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsNonPositiveInteger_66(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsNegativeInteger_67(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsLong_68(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsInt_69(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsShort_70(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsByte_71(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsNonNegativeInteger_72(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsUnsignedLong_73(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsUnsignedInt_74(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsUnsignedShort_75(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsUnsignedByte_76(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XsPositiveInteger_77(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XdtUntypedAtomic_78(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XdtAnyAtomicType_79(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XdtYearMonthDuration_80(),
	XmlSchemaSimpleType_t2678868104_StaticFields::get_offset_of_XdtDayTimeDuration_81(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (XmlSchemaSimpleTypeContent_t599285223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1969[1] = 
{
	XmlSchemaSimpleTypeContent_t599285223::get_offset_of_OwnerType_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (XmlSchemaSimpleTypeList_t472803608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1970[4] = 
{
	XmlSchemaSimpleTypeList_t472803608::get_offset_of_itemType_17(),
	XmlSchemaSimpleTypeList_t472803608::get_offset_of_itemTypeName_18(),
	XmlSchemaSimpleTypeList_t472803608::get_offset_of_validatedListItemType_19(),
	XmlSchemaSimpleTypeList_t472803608::get_offset_of_validatedListItemSchemaType_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { sizeof (XmlSchemaSimpleTypeRestriction_t3925451115), -1, sizeof(XmlSchemaSimpleTypeRestriction_t3925451115_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1971[18] = 
{
	XmlSchemaSimpleTypeRestriction_t3925451115::get_offset_of_baseType_17(),
	XmlSchemaSimpleTypeRestriction_t3925451115::get_offset_of_baseTypeName_18(),
	XmlSchemaSimpleTypeRestriction_t3925451115::get_offset_of_facets_19(),
	XmlSchemaSimpleTypeRestriction_t3925451115::get_offset_of_enumarationFacetValues_20(),
	XmlSchemaSimpleTypeRestriction_t3925451115::get_offset_of_patternFacetValues_21(),
	XmlSchemaSimpleTypeRestriction_t3925451115::get_offset_of_rexPatterns_22(),
	XmlSchemaSimpleTypeRestriction_t3925451115::get_offset_of_lengthFacet_23(),
	XmlSchemaSimpleTypeRestriction_t3925451115::get_offset_of_maxLengthFacet_24(),
	XmlSchemaSimpleTypeRestriction_t3925451115::get_offset_of_minLengthFacet_25(),
	XmlSchemaSimpleTypeRestriction_t3925451115::get_offset_of_fractionDigitsFacet_26(),
	XmlSchemaSimpleTypeRestriction_t3925451115::get_offset_of_totalDigitsFacet_27(),
	XmlSchemaSimpleTypeRestriction_t3925451115::get_offset_of_maxInclusiveFacet_28(),
	XmlSchemaSimpleTypeRestriction_t3925451115::get_offset_of_maxExclusiveFacet_29(),
	XmlSchemaSimpleTypeRestriction_t3925451115::get_offset_of_minInclusiveFacet_30(),
	XmlSchemaSimpleTypeRestriction_t3925451115::get_offset_of_minExclusiveFacet_31(),
	XmlSchemaSimpleTypeRestriction_t3925451115::get_offset_of_fixedFacets_32(),
	XmlSchemaSimpleTypeRestriction_t3925451115_StaticFields::get_offset_of_lengthStyle_33(),
	XmlSchemaSimpleTypeRestriction_t3925451115_StaticFields::get_offset_of_listFacets_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { sizeof (XmlSchemaSimpleTypeUnion_t4071426880), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1972[4] = 
{
	XmlSchemaSimpleTypeUnion_t4071426880::get_offset_of_baseTypes_17(),
	XmlSchemaSimpleTypeUnion_t4071426880::get_offset_of_memberTypes_18(),
	XmlSchemaSimpleTypeUnion_t4071426880::get_offset_of_validatedTypes_19(),
	XmlSchemaSimpleTypeUnion_t4071426880::get_offset_of_validatedSchemaTypes_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (XmlSchemaTotalDigitsFacet_t297930215), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (XmlSchemaType_t2033747345), -1, sizeof(XmlSchemaType_t2033747345_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1974[12] = 
{
	XmlSchemaType_t2033747345::get_offset_of_final_16(),
	XmlSchemaType_t2033747345::get_offset_of_isMixed_17(),
	XmlSchemaType_t2033747345::get_offset_of_name_18(),
	XmlSchemaType_t2033747345::get_offset_of_recursed_19(),
	XmlSchemaType_t2033747345::get_offset_of_BaseSchemaTypeName_20(),
	XmlSchemaType_t2033747345::get_offset_of_BaseXmlSchemaTypeInternal_21(),
	XmlSchemaType_t2033747345::get_offset_of_DatatypeInternal_22(),
	XmlSchemaType_t2033747345::get_offset_of_resolvedDerivedBy_23(),
	XmlSchemaType_t2033747345::get_offset_of_finalResolved_24(),
	XmlSchemaType_t2033747345::get_offset_of_QNameInternal_25(),
	XmlSchemaType_t2033747345_StaticFields::get_offset_of_U3CU3Ef__switchU24map42_26(),
	XmlSchemaType_t2033747345_StaticFields::get_offset_of_U3CU3Ef__switchU24map43_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (XmlSchemaUnique_t2867867737), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { sizeof (XmlSchemaUse_t647315988)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1976[5] = 
{
	XmlSchemaUse_t647315988::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { sizeof (XmlSchemaValidator_t1317961423), -1, sizeof(XmlSchemaValidator_t1317961423_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1977[26] = 
{
	XmlSchemaValidator_t1317961423_StaticFields::get_offset_of_emptyAttributeArray_0(),
	XmlSchemaValidator_t1317961423::get_offset_of_nominalEventSender_1(),
	XmlSchemaValidator_t1317961423::get_offset_of_lineInfo_2(),
	XmlSchemaValidator_t1317961423::get_offset_of_nsResolver_3(),
	XmlSchemaValidator_t1317961423::get_offset_of_sourceUri_4(),
	XmlSchemaValidator_t1317961423::get_offset_of_nameTable_5(),
	XmlSchemaValidator_t1317961423::get_offset_of_schemas_6(),
	XmlSchemaValidator_t1317961423::get_offset_of_xmlResolver_7(),
	XmlSchemaValidator_t1317961423::get_offset_of_options_8(),
	XmlSchemaValidator_t1317961423::get_offset_of_transition_9(),
	XmlSchemaValidator_t1317961423::get_offset_of_state_10(),
	XmlSchemaValidator_t1317961423::get_offset_of_occuredAtts_11(),
	XmlSchemaValidator_t1317961423::get_offset_of_defaultAttributes_12(),
	XmlSchemaValidator_t1317961423::get_offset_of_defaultAttributesCache_13(),
	XmlSchemaValidator_t1317961423::get_offset_of_idManager_14(),
	XmlSchemaValidator_t1317961423::get_offset_of_keyTables_15(),
	XmlSchemaValidator_t1317961423::get_offset_of_currentKeyFieldConsumers_16(),
	XmlSchemaValidator_t1317961423::get_offset_of_tmpKeyrefPool_17(),
	XmlSchemaValidator_t1317961423::get_offset_of_elementQNameStack_18(),
	XmlSchemaValidator_t1317961423::get_offset_of_storedCharacters_19(),
	XmlSchemaValidator_t1317961423::get_offset_of_shouldValidateCharacters_20(),
	XmlSchemaValidator_t1317961423::get_offset_of_depth_21(),
	XmlSchemaValidator_t1317961423::get_offset_of_xsiNilDepth_22(),
	XmlSchemaValidator_t1317961423::get_offset_of_skipValidationDepth_23(),
	XmlSchemaValidator_t1317961423::get_offset_of_CurrentAttributeType_24(),
	XmlSchemaValidator_t1317961423::get_offset_of_ValidationEventHandler_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { sizeof (Transition_t766760941)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1978[5] = 
{
	Transition_t766760941::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { sizeof (XmlSchemaValidity_t3794542157)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1979[4] = 
{
	XmlSchemaValidity_t3794542157::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { sizeof (XmlSchemaValidationException_t816160496), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { sizeof (XmlSchemaWhiteSpaceFacet_t4158372164), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { sizeof (XmlSchemaXPath_t3156455507), -1, sizeof(XmlSchemaXPath_t3156455507_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1982[6] = 
{
	XmlSchemaXPath_t3156455507::get_offset_of_xpath_16(),
	XmlSchemaXPath_t3156455507::get_offset_of_nsmgr_17(),
	XmlSchemaXPath_t3156455507::get_offset_of_isSelector_18(),
	XmlSchemaXPath_t3156455507::get_offset_of_compiledExpression_19(),
	XmlSchemaXPath_t3156455507::get_offset_of_currentPath_20(),
	XmlSchemaXPath_t3156455507_StaticFields::get_offset_of_U3CU3Ef__switchU24map4A_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { sizeof (XmlSeverityType_t1894651412)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1983[3] = 
{
	XmlSeverityType_t1894651412::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (ValidationHandler_t3551360050), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { sizeof (XmlSchemaUtil_t956145399), -1, sizeof(XmlSchemaUtil_t956145399_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1985[10] = 
{
	XmlSchemaUtil_t956145399_StaticFields::get_offset_of_FinalAllowed_0(),
	XmlSchemaUtil_t956145399_StaticFields::get_offset_of_ElementBlockAllowed_1(),
	XmlSchemaUtil_t956145399_StaticFields::get_offset_of_ComplexTypeBlockAllowed_2(),
	XmlSchemaUtil_t956145399_StaticFields::get_offset_of_StrictMsCompliant_3(),
	XmlSchemaUtil_t956145399_StaticFields::get_offset_of_U3CU3Ef__switchU24map4B_4(),
	XmlSchemaUtil_t956145399_StaticFields::get_offset_of_U3CU3Ef__switchU24map4C_5(),
	XmlSchemaUtil_t956145399_StaticFields::get_offset_of_U3CU3Ef__switchU24map4D_6(),
	XmlSchemaUtil_t956145399_StaticFields::get_offset_of_U3CU3Ef__switchU24map4E_7(),
	XmlSchemaUtil_t956145399_StaticFields::get_offset_of_U3CU3Ef__switchU24map4F_8(),
	XmlSchemaUtil_t956145399_StaticFields::get_offset_of_U3CU3Ef__switchU24map50_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { sizeof (XmlSchemaReader_t1164558392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1986[3] = 
{
	XmlSchemaReader_t1164558392::get_offset_of_reader_3(),
	XmlSchemaReader_t1164558392::get_offset_of_handler_4(),
	XmlSchemaReader_t1164558392::get_offset_of_hasLineInfo_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { sizeof (XmlSchemaValidationFlags_t877176585)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1987[7] = 
{
	XmlSchemaValidationFlags_t877176585::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { sizeof (XmlTypeCode_t2623622950)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1988[56] = 
{
	XmlTypeCode_t2623622950::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { sizeof (CodeExporter_t3497037670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1989[1] = 
{
	CodeExporter_t3497037670::get_offset_of_codeGenerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { sizeof (CodeGenerationOptions_t2228734478)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1990[7] = 
{
	CodeGenerationOptions_t2228734478::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { sizeof (CodeIdentifier_t2202687290), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { sizeof (CodeIdentifiers_t4095039290), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1992[3] = 
{
	CodeIdentifiers_t4095039290::get_offset_of_useCamelCasing_0(),
	CodeIdentifiers_t4095039290::get_offset_of_table_1(),
	CodeIdentifiers_t4095039290::get_offset_of_reserved_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { sizeof (ImportContext_t1801135953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1994[6] = 
{
	ImportContext_t1801135953::get_offset_of__shareTypes_0(),
	ImportContext_t1801135953::get_offset_of__typeIdentifiers_1(),
	ImportContext_t1801135953::get_offset_of__warnings_2(),
	ImportContext_t1801135953::get_offset_of_MappedTypes_3(),
	ImportContext_t1801135953::get_offset_of_DataMappedTypes_4(),
	ImportContext_t1801135953::get_offset_of_SharedAnonymousTypes_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { sizeof (KeyHelper_t3772181821), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (MapCodeGenerator_t1608106344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1996[10] = 
{
	MapCodeGenerator_t1608106344::get_offset_of_codeNamespace_0(),
	MapCodeGenerator_t1608106344::get_offset_of_codeCompileUnit_1(),
	MapCodeGenerator_t1608106344::get_offset_of_includeMetadata_2(),
	MapCodeGenerator_t1608106344::get_offset_of_exportedAnyType_3(),
	MapCodeGenerator_t1608106344::get_offset_of_includeArrayTypes_4(),
	MapCodeGenerator_t1608106344::get_offset_of_codeProvider_5(),
	MapCodeGenerator_t1608106344::get_offset_of_options_6(),
	MapCodeGenerator_t1608106344::get_offset_of_identifiers_7(),
	MapCodeGenerator_t1608106344::get_offset_of_exportedMaps_8(),
	MapCodeGenerator_t1608106344::get_offset_of_includeMaps_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { sizeof (ReflectionHelper_t300476302), -1, sizeof(ReflectionHelper_t300476302_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1997[3] = 
{
	ReflectionHelper_t300476302::get_offset_of__clrTypes_0(),
	ReflectionHelper_t300476302::get_offset_of__schemaTypes_1(),
	ReflectionHelper_t300476302_StaticFields::get_offset_of_empty_modifiers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { sizeof (SchemaImporter_t2185830201), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { sizeof (SchemaTypes_t1741406581)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1999[9] = 
{
	SchemaTypes_t1741406581::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
