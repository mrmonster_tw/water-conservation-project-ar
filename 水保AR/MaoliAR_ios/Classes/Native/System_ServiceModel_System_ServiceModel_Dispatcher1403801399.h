﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Dispatcher.MessageProcessingContext
struct MessageProcessingContext_t1414617556;
// System.ServiceModel.Dispatcher.DispatchRuntime
struct DispatchRuntime_t796075230;
// System.ServiceModel.IClientChannel
struct IClientChannel_t714275133;
// System.Object[]
struct ObjectU5BU5D_t2843939325;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.UserEventsHandler
struct  UserEventsHandler_t1403801399  : public Il2CppObject
{
public:
	// System.ServiceModel.Dispatcher.MessageProcessingContext System.ServiceModel.Dispatcher.UserEventsHandler::request_context
	MessageProcessingContext_t1414617556 * ___request_context_0;
	// System.ServiceModel.Dispatcher.DispatchRuntime System.ServiceModel.Dispatcher.UserEventsHandler::dispatch_runtime
	DispatchRuntime_t796075230 * ___dispatch_runtime_1;
	// System.ServiceModel.IClientChannel System.ServiceModel.Dispatcher.UserEventsHandler::channel
	Il2CppObject * ___channel_2;
	// System.Object[] System.ServiceModel.Dispatcher.UserEventsHandler::msg_inspectors_states
	ObjectU5BU5D_t2843939325* ___msg_inspectors_states_3;
	// System.Object[] System.ServiceModel.Dispatcher.UserEventsHandler::callcontext_initializers_states
	ObjectU5BU5D_t2843939325* ___callcontext_initializers_states_4;

public:
	inline static int32_t get_offset_of_request_context_0() { return static_cast<int32_t>(offsetof(UserEventsHandler_t1403801399, ___request_context_0)); }
	inline MessageProcessingContext_t1414617556 * get_request_context_0() const { return ___request_context_0; }
	inline MessageProcessingContext_t1414617556 ** get_address_of_request_context_0() { return &___request_context_0; }
	inline void set_request_context_0(MessageProcessingContext_t1414617556 * value)
	{
		___request_context_0 = value;
		Il2CppCodeGenWriteBarrier(&___request_context_0, value);
	}

	inline static int32_t get_offset_of_dispatch_runtime_1() { return static_cast<int32_t>(offsetof(UserEventsHandler_t1403801399, ___dispatch_runtime_1)); }
	inline DispatchRuntime_t796075230 * get_dispatch_runtime_1() const { return ___dispatch_runtime_1; }
	inline DispatchRuntime_t796075230 ** get_address_of_dispatch_runtime_1() { return &___dispatch_runtime_1; }
	inline void set_dispatch_runtime_1(DispatchRuntime_t796075230 * value)
	{
		___dispatch_runtime_1 = value;
		Il2CppCodeGenWriteBarrier(&___dispatch_runtime_1, value);
	}

	inline static int32_t get_offset_of_channel_2() { return static_cast<int32_t>(offsetof(UserEventsHandler_t1403801399, ___channel_2)); }
	inline Il2CppObject * get_channel_2() const { return ___channel_2; }
	inline Il2CppObject ** get_address_of_channel_2() { return &___channel_2; }
	inline void set_channel_2(Il2CppObject * value)
	{
		___channel_2 = value;
		Il2CppCodeGenWriteBarrier(&___channel_2, value);
	}

	inline static int32_t get_offset_of_msg_inspectors_states_3() { return static_cast<int32_t>(offsetof(UserEventsHandler_t1403801399, ___msg_inspectors_states_3)); }
	inline ObjectU5BU5D_t2843939325* get_msg_inspectors_states_3() const { return ___msg_inspectors_states_3; }
	inline ObjectU5BU5D_t2843939325** get_address_of_msg_inspectors_states_3() { return &___msg_inspectors_states_3; }
	inline void set_msg_inspectors_states_3(ObjectU5BU5D_t2843939325* value)
	{
		___msg_inspectors_states_3 = value;
		Il2CppCodeGenWriteBarrier(&___msg_inspectors_states_3, value);
	}

	inline static int32_t get_offset_of_callcontext_initializers_states_4() { return static_cast<int32_t>(offsetof(UserEventsHandler_t1403801399, ___callcontext_initializers_states_4)); }
	inline ObjectU5BU5D_t2843939325* get_callcontext_initializers_states_4() const { return ___callcontext_initializers_states_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_callcontext_initializers_states_4() { return &___callcontext_initializers_states_4; }
	inline void set_callcontext_initializers_states_4(ObjectU5BU5D_t2843939325* value)
	{
		___callcontext_initializers_states_4 = value;
		Il2CppCodeGenWriteBarrier(&___callcontext_initializers_states_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
