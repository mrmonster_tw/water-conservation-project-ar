﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.Reflection.Assembly
struct Assembly_t4102432799;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.BuildManagerCacheItem
struct  BuildManagerCacheItem_t1643815903  : public Il2CppObject
{
public:
	// System.String System.Web.Compilation.BuildManagerCacheItem::CompiledCustomString
	String_t* ___CompiledCustomString_0;
	// System.Reflection.Assembly System.Web.Compilation.BuildManagerCacheItem::BuiltAssembly
	Assembly_t4102432799 * ___BuiltAssembly_1;
	// System.String System.Web.Compilation.BuildManagerCacheItem::VirtualPath
	String_t* ___VirtualPath_2;
	// System.Type System.Web.Compilation.BuildManagerCacheItem::Type
	Type_t * ___Type_3;

public:
	inline static int32_t get_offset_of_CompiledCustomString_0() { return static_cast<int32_t>(offsetof(BuildManagerCacheItem_t1643815903, ___CompiledCustomString_0)); }
	inline String_t* get_CompiledCustomString_0() const { return ___CompiledCustomString_0; }
	inline String_t** get_address_of_CompiledCustomString_0() { return &___CompiledCustomString_0; }
	inline void set_CompiledCustomString_0(String_t* value)
	{
		___CompiledCustomString_0 = value;
		Il2CppCodeGenWriteBarrier(&___CompiledCustomString_0, value);
	}

	inline static int32_t get_offset_of_BuiltAssembly_1() { return static_cast<int32_t>(offsetof(BuildManagerCacheItem_t1643815903, ___BuiltAssembly_1)); }
	inline Assembly_t4102432799 * get_BuiltAssembly_1() const { return ___BuiltAssembly_1; }
	inline Assembly_t4102432799 ** get_address_of_BuiltAssembly_1() { return &___BuiltAssembly_1; }
	inline void set_BuiltAssembly_1(Assembly_t4102432799 * value)
	{
		___BuiltAssembly_1 = value;
		Il2CppCodeGenWriteBarrier(&___BuiltAssembly_1, value);
	}

	inline static int32_t get_offset_of_VirtualPath_2() { return static_cast<int32_t>(offsetof(BuildManagerCacheItem_t1643815903, ___VirtualPath_2)); }
	inline String_t* get_VirtualPath_2() const { return ___VirtualPath_2; }
	inline String_t** get_address_of_VirtualPath_2() { return &___VirtualPath_2; }
	inline void set_VirtualPath_2(String_t* value)
	{
		___VirtualPath_2 = value;
		Il2CppCodeGenWriteBarrier(&___VirtualPath_2, value);
	}

	inline static int32_t get_offset_of_Type_3() { return static_cast<int32_t>(offsetof(BuildManagerCacheItem_t1643815903, ___Type_3)); }
	inline Type_t * get_Type_3() const { return ___Type_3; }
	inline Type_t ** get_address_of_Type_3() { return &___Type_3; }
	inline void set_Type_3(Type_t * value)
	{
		___Type_3 = value;
		Il2CppCodeGenWriteBarrier(&___Type_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
