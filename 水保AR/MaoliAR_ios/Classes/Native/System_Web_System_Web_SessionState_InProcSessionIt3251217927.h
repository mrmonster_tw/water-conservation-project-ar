﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_DateTime3738529785.h"

// System.Web.SessionState.ISessionStateItemCollection
struct ISessionStateItemCollection_t2279755297;
// System.Threading.ReaderWriterLock
struct ReaderWriterLock_t367846299;
// System.Web.HttpStaticObjectsCollection
struct HttpStaticObjectsCollection_t518175362;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.SessionState.InProcSessionItem
struct  InProcSessionItem_t3251217927  : public Il2CppObject
{
public:
	// System.Boolean System.Web.SessionState.InProcSessionItem::locked
	bool ___locked_0;
	// System.Boolean System.Web.SessionState.InProcSessionItem::cookieless
	bool ___cookieless_1;
	// System.Web.SessionState.ISessionStateItemCollection System.Web.SessionState.InProcSessionItem::items
	Il2CppObject * ___items_2;
	// System.DateTime System.Web.SessionState.InProcSessionItem::lockedTime
	DateTime_t3738529785  ___lockedTime_3;
	// System.DateTime System.Web.SessionState.InProcSessionItem::expiresAt
	DateTime_t3738529785  ___expiresAt_4;
	// System.Threading.ReaderWriterLock System.Web.SessionState.InProcSessionItem::rwlock
	ReaderWriterLock_t367846299 * ___rwlock_5;
	// System.Int32 System.Web.SessionState.InProcSessionItem::lockId
	int32_t ___lockId_6;
	// System.Int32 System.Web.SessionState.InProcSessionItem::timeout
	int32_t ___timeout_7;
	// System.Boolean System.Web.SessionState.InProcSessionItem::resettingTimeout
	bool ___resettingTimeout_8;
	// System.Web.HttpStaticObjectsCollection System.Web.SessionState.InProcSessionItem::staticItems
	HttpStaticObjectsCollection_t518175362 * ___staticItems_9;

public:
	inline static int32_t get_offset_of_locked_0() { return static_cast<int32_t>(offsetof(InProcSessionItem_t3251217927, ___locked_0)); }
	inline bool get_locked_0() const { return ___locked_0; }
	inline bool* get_address_of_locked_0() { return &___locked_0; }
	inline void set_locked_0(bool value)
	{
		___locked_0 = value;
	}

	inline static int32_t get_offset_of_cookieless_1() { return static_cast<int32_t>(offsetof(InProcSessionItem_t3251217927, ___cookieless_1)); }
	inline bool get_cookieless_1() const { return ___cookieless_1; }
	inline bool* get_address_of_cookieless_1() { return &___cookieless_1; }
	inline void set_cookieless_1(bool value)
	{
		___cookieless_1 = value;
	}

	inline static int32_t get_offset_of_items_2() { return static_cast<int32_t>(offsetof(InProcSessionItem_t3251217927, ___items_2)); }
	inline Il2CppObject * get_items_2() const { return ___items_2; }
	inline Il2CppObject ** get_address_of_items_2() { return &___items_2; }
	inline void set_items_2(Il2CppObject * value)
	{
		___items_2 = value;
		Il2CppCodeGenWriteBarrier(&___items_2, value);
	}

	inline static int32_t get_offset_of_lockedTime_3() { return static_cast<int32_t>(offsetof(InProcSessionItem_t3251217927, ___lockedTime_3)); }
	inline DateTime_t3738529785  get_lockedTime_3() const { return ___lockedTime_3; }
	inline DateTime_t3738529785 * get_address_of_lockedTime_3() { return &___lockedTime_3; }
	inline void set_lockedTime_3(DateTime_t3738529785  value)
	{
		___lockedTime_3 = value;
	}

	inline static int32_t get_offset_of_expiresAt_4() { return static_cast<int32_t>(offsetof(InProcSessionItem_t3251217927, ___expiresAt_4)); }
	inline DateTime_t3738529785  get_expiresAt_4() const { return ___expiresAt_4; }
	inline DateTime_t3738529785 * get_address_of_expiresAt_4() { return &___expiresAt_4; }
	inline void set_expiresAt_4(DateTime_t3738529785  value)
	{
		___expiresAt_4 = value;
	}

	inline static int32_t get_offset_of_rwlock_5() { return static_cast<int32_t>(offsetof(InProcSessionItem_t3251217927, ___rwlock_5)); }
	inline ReaderWriterLock_t367846299 * get_rwlock_5() const { return ___rwlock_5; }
	inline ReaderWriterLock_t367846299 ** get_address_of_rwlock_5() { return &___rwlock_5; }
	inline void set_rwlock_5(ReaderWriterLock_t367846299 * value)
	{
		___rwlock_5 = value;
		Il2CppCodeGenWriteBarrier(&___rwlock_5, value);
	}

	inline static int32_t get_offset_of_lockId_6() { return static_cast<int32_t>(offsetof(InProcSessionItem_t3251217927, ___lockId_6)); }
	inline int32_t get_lockId_6() const { return ___lockId_6; }
	inline int32_t* get_address_of_lockId_6() { return &___lockId_6; }
	inline void set_lockId_6(int32_t value)
	{
		___lockId_6 = value;
	}

	inline static int32_t get_offset_of_timeout_7() { return static_cast<int32_t>(offsetof(InProcSessionItem_t3251217927, ___timeout_7)); }
	inline int32_t get_timeout_7() const { return ___timeout_7; }
	inline int32_t* get_address_of_timeout_7() { return &___timeout_7; }
	inline void set_timeout_7(int32_t value)
	{
		___timeout_7 = value;
	}

	inline static int32_t get_offset_of_resettingTimeout_8() { return static_cast<int32_t>(offsetof(InProcSessionItem_t3251217927, ___resettingTimeout_8)); }
	inline bool get_resettingTimeout_8() const { return ___resettingTimeout_8; }
	inline bool* get_address_of_resettingTimeout_8() { return &___resettingTimeout_8; }
	inline void set_resettingTimeout_8(bool value)
	{
		___resettingTimeout_8 = value;
	}

	inline static int32_t get_offset_of_staticItems_9() { return static_cast<int32_t>(offsetof(InProcSessionItem_t3251217927, ___staticItems_9)); }
	inline HttpStaticObjectsCollection_t518175362 * get_staticItems_9() const { return ___staticItems_9; }
	inline HttpStaticObjectsCollection_t518175362 ** get_address_of_staticItems_9() { return &___staticItems_9; }
	inline void set_staticItems_9(HttpStaticObjectsCollection_t518175362 * value)
	{
		___staticItems_9 = value;
		Il2CppCodeGenWriteBarrier(&___staticItems_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
