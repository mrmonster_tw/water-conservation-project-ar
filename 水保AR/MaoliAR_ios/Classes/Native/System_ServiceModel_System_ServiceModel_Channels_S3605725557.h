﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Generic.Dictionary`2<System.String,System.ServiceModel.Channels.SvcHttpHandler>
struct Dictionary_2_t1503939810;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.SvcHttpHandlerFactory
struct  SvcHttpHandlerFactory_t3605725557  : public Il2CppObject
{
public:
	// System.Type System.ServiceModel.Channels.SvcHttpHandlerFactory::service_type
	Type_t * ___service_type_1;
	// System.Type System.ServiceModel.Channels.SvcHttpHandlerFactory::factory_type
	Type_t * ___factory_type_2;

public:
	inline static int32_t get_offset_of_service_type_1() { return static_cast<int32_t>(offsetof(SvcHttpHandlerFactory_t3605725557, ___service_type_1)); }
	inline Type_t * get_service_type_1() const { return ___service_type_1; }
	inline Type_t ** get_address_of_service_type_1() { return &___service_type_1; }
	inline void set_service_type_1(Type_t * value)
	{
		___service_type_1 = value;
		Il2CppCodeGenWriteBarrier(&___service_type_1, value);
	}

	inline static int32_t get_offset_of_factory_type_2() { return static_cast<int32_t>(offsetof(SvcHttpHandlerFactory_t3605725557, ___factory_type_2)); }
	inline Type_t * get_factory_type_2() const { return ___factory_type_2; }
	inline Type_t ** get_address_of_factory_type_2() { return &___factory_type_2; }
	inline void set_factory_type_2(Type_t * value)
	{
		___factory_type_2 = value;
		Il2CppCodeGenWriteBarrier(&___factory_type_2, value);
	}
};

struct SvcHttpHandlerFactory_t3605725557_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.ServiceModel.Channels.SvcHttpHandler> System.ServiceModel.Channels.SvcHttpHandlerFactory::handlers
	Dictionary_2_t1503939810 * ___handlers_0;

public:
	inline static int32_t get_offset_of_handlers_0() { return static_cast<int32_t>(offsetof(SvcHttpHandlerFactory_t3605725557_StaticFields, ___handlers_0)); }
	inline Dictionary_2_t1503939810 * get_handlers_0() const { return ___handlers_0; }
	inline Dictionary_2_t1503939810 ** get_address_of_handlers_0() { return &___handlers_0; }
	inline void set_handlers_0(Dictionary_2_t1503939810 * value)
	{
		___handlers_0 = value;
		Il2CppCodeGenWriteBarrier(&___handlers_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
