﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3156163955.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.ExtensionsSection
struct  ExtensionsSection_t2655181283  : public ConfigurationSection_t3156163955
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.ExtensionsSection::_properties
	ConfigurationPropertyCollection_t2852175726 * ____properties_17;

public:
	inline static int32_t get_offset_of__properties_17() { return static_cast<int32_t>(offsetof(ExtensionsSection_t2655181283, ____properties_17)); }
	inline ConfigurationPropertyCollection_t2852175726 * get__properties_17() const { return ____properties_17; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of__properties_17() { return &____properties_17; }
	inline void set__properties_17(ConfigurationPropertyCollection_t2852175726 * value)
	{
		____properties_17 = value;
		Il2CppCodeGenWriteBarrier(&____properties_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
