﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Tokens_S1943429813.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Tokens.X509IssuerSerialKeyIdentifierClause
struct  X509IssuerSerialKeyIdentifierClause_t3285382384  : public SecurityKeyIdentifierClause_t1943429813
{
public:
	// System.String System.IdentityModel.Tokens.X509IssuerSerialKeyIdentifierClause::name
	String_t* ___name_3;
	// System.String System.IdentityModel.Tokens.X509IssuerSerialKeyIdentifierClause::serial
	String_t* ___serial_4;

public:
	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(X509IssuerSerialKeyIdentifierClause_t3285382384, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier(&___name_3, value);
	}

	inline static int32_t get_offset_of_serial_4() { return static_cast<int32_t>(offsetof(X509IssuerSerialKeyIdentifierClause_t3285382384, ___serial_4)); }
	inline String_t* get_serial_4() const { return ___serial_4; }
	inline String_t** get_address_of_serial_4() { return &___serial_4; }
	inline void set_serial_4(String_t* value)
	{
		___serial_4 = value;
		Il2CppCodeGenWriteBarrier(&___serial_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
