﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"





extern const Il2CppType Il2CppObject_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0 = { 1, GenInst_Il2CppObject_0_0_0_Types };
extern const Il2CppType Int32_t2950945753_0_0_0;
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Types[] = { &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0 = { 1, GenInst_Int32_t2950945753_0_0_0_Types };
extern const Il2CppType Char_t3634460470_0_0_0;
static const Il2CppType* GenInst_Char_t3634460470_0_0_0_Types[] = { &Char_t3634460470_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3634460470_0_0_0 = { 1, GenInst_Char_t3634460470_0_0_0_Types };
extern const Il2CppType Int64_t3736567304_0_0_0;
static const Il2CppType* GenInst_Int64_t3736567304_0_0_0_Types[] = { &Int64_t3736567304_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t3736567304_0_0_0 = { 1, GenInst_Int64_t3736567304_0_0_0_Types };
extern const Il2CppType UInt32_t2560061978_0_0_0;
static const Il2CppType* GenInst_UInt32_t2560061978_0_0_0_Types[] = { &UInt32_t2560061978_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t2560061978_0_0_0 = { 1, GenInst_UInt32_t2560061978_0_0_0_Types };
extern const Il2CppType UInt64_t4134040092_0_0_0;
static const Il2CppType* GenInst_UInt64_t4134040092_0_0_0_Types[] = { &UInt64_t4134040092_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t4134040092_0_0_0 = { 1, GenInst_UInt64_t4134040092_0_0_0_Types };
extern const Il2CppType Byte_t1134296376_0_0_0;
static const Il2CppType* GenInst_Byte_t1134296376_0_0_0_Types[] = { &Byte_t1134296376_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t1134296376_0_0_0 = { 1, GenInst_Byte_t1134296376_0_0_0_Types };
extern const Il2CppType SByte_t1669577662_0_0_0;
static const Il2CppType* GenInst_SByte_t1669577662_0_0_0_Types[] = { &SByte_t1669577662_0_0_0 };
extern const Il2CppGenericInst GenInst_SByte_t1669577662_0_0_0 = { 1, GenInst_SByte_t1669577662_0_0_0_Types };
extern const Il2CppType Int16_t2552820387_0_0_0;
static const Il2CppType* GenInst_Int16_t2552820387_0_0_0_Types[] = { &Int16_t2552820387_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t2552820387_0_0_0 = { 1, GenInst_Int16_t2552820387_0_0_0_Types };
extern const Il2CppType UInt16_t2177724958_0_0_0;
static const Il2CppType* GenInst_UInt16_t2177724958_0_0_0_Types[] = { &UInt16_t2177724958_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t2177724958_0_0_0 = { 1, GenInst_UInt16_t2177724958_0_0_0_Types };
extern const Il2CppType String_t_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Types[] = { &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
extern const Il2CppType IConvertible_t2977365677_0_0_0;
static const Il2CppType* GenInst_IConvertible_t2977365677_0_0_0_Types[] = { &IConvertible_t2977365677_0_0_0 };
extern const Il2CppGenericInst GenInst_IConvertible_t2977365677_0_0_0 = { 1, GenInst_IConvertible_t2977365677_0_0_0_Types };
extern const Il2CppType IComparable_t36111218_0_0_0;
static const Il2CppType* GenInst_IComparable_t36111218_0_0_0_Types[] = { &IComparable_t36111218_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_t36111218_0_0_0 = { 1, GenInst_IComparable_t36111218_0_0_0_Types };
extern const Il2CppType IEnumerable_t1941168011_0_0_0;
static const Il2CppType* GenInst_IEnumerable_t1941168011_0_0_0_Types[] = { &IEnumerable_t1941168011_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_t1941168011_0_0_0 = { 1, GenInst_IEnumerable_t1941168011_0_0_0_Types };
extern const Il2CppType ICloneable_t724424198_0_0_0;
static const Il2CppType* GenInst_ICloneable_t724424198_0_0_0_Types[] = { &ICloneable_t724424198_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloneable_t724424198_0_0_0 = { 1, GenInst_ICloneable_t724424198_0_0_0_Types };
extern const Il2CppType IComparable_1_t1216115102_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1216115102_0_0_0_Types[] = { &IComparable_1_t1216115102_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1216115102_0_0_0 = { 1, GenInst_IComparable_1_t1216115102_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2738596416_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2738596416_0_0_0_Types[] = { &IEquatable_1_t2738596416_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2738596416_0_0_0 = { 1, GenInst_IEquatable_1_t2738596416_0_0_0_Types };
extern const Il2CppType Type_t_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_Types[] = { &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
extern const Il2CppType IReflect_t2554276939_0_0_0;
static const Il2CppType* GenInst_IReflect_t2554276939_0_0_0_Types[] = { &IReflect_t2554276939_0_0_0 };
extern const Il2CppGenericInst GenInst_IReflect_t2554276939_0_0_0 = { 1, GenInst_IReflect_t2554276939_0_0_0_Types };
extern const Il2CppType _Type_t3588564251_0_0_0;
static const Il2CppType* GenInst__Type_t3588564251_0_0_0_Types[] = { &_Type_t3588564251_0_0_0 };
extern const Il2CppGenericInst GenInst__Type_t3588564251_0_0_0 = { 1, GenInst__Type_t3588564251_0_0_0_Types };
extern const Il2CppType MemberInfo_t_0_0_0;
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_Types[] = { &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
extern const Il2CppType ICustomAttributeProvider_t1530824137_0_0_0;
static const Il2CppType* GenInst_ICustomAttributeProvider_t1530824137_0_0_0_Types[] = { &ICustomAttributeProvider_t1530824137_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t1530824137_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t1530824137_0_0_0_Types };
extern const Il2CppType _MemberInfo_t3922476713_0_0_0;
static const Il2CppType* GenInst__MemberInfo_t3922476713_0_0_0_Types[] = { &_MemberInfo_t3922476713_0_0_0 };
extern const Il2CppGenericInst GenInst__MemberInfo_t3922476713_0_0_0 = { 1, GenInst__MemberInfo_t3922476713_0_0_0_Types };
extern const Il2CppType Double_t594665363_0_0_0;
static const Il2CppType* GenInst_Double_t594665363_0_0_0_Types[] = { &Double_t594665363_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t594665363_0_0_0 = { 1, GenInst_Double_t594665363_0_0_0_Types };
extern const Il2CppType Single_t1397266774_0_0_0;
static const Il2CppType* GenInst_Single_t1397266774_0_0_0_Types[] = { &Single_t1397266774_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t1397266774_0_0_0 = { 1, GenInst_Single_t1397266774_0_0_0_Types };
extern const Il2CppType Decimal_t2948259380_0_0_0;
static const Il2CppType* GenInst_Decimal_t2948259380_0_0_0_Types[] = { &Decimal_t2948259380_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t2948259380_0_0_0 = { 1, GenInst_Decimal_t2948259380_0_0_0_Types };
extern const Il2CppType Boolean_t97287965_0_0_0;
static const Il2CppType* GenInst_Boolean_t97287965_0_0_0_Types[] = { &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t97287965_0_0_0 = { 1, GenInst_Boolean_t97287965_0_0_0_Types };
extern const Il2CppType Delegate_t1188392813_0_0_0;
static const Il2CppType* GenInst_Delegate_t1188392813_0_0_0_Types[] = { &Delegate_t1188392813_0_0_0 };
extern const Il2CppGenericInst GenInst_Delegate_t1188392813_0_0_0 = { 1, GenInst_Delegate_t1188392813_0_0_0_Types };
extern const Il2CppType ISerializable_t3375760802_0_0_0;
static const Il2CppType* GenInst_ISerializable_t3375760802_0_0_0_Types[] = { &ISerializable_t3375760802_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializable_t3375760802_0_0_0 = { 1, GenInst_ISerializable_t3375760802_0_0_0_Types };
extern const Il2CppType ParameterInfo_t1861056598_0_0_0;
static const Il2CppType* GenInst_ParameterInfo_t1861056598_0_0_0_Types[] = { &ParameterInfo_t1861056598_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t1861056598_0_0_0 = { 1, GenInst_ParameterInfo_t1861056598_0_0_0_Types };
extern const Il2CppType _ParameterInfo_t489405856_0_0_0;
static const Il2CppType* GenInst__ParameterInfo_t489405856_0_0_0_Types[] = { &_ParameterInfo_t489405856_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterInfo_t489405856_0_0_0 = { 1, GenInst__ParameterInfo_t489405856_0_0_0_Types };
extern const Il2CppType ParameterModifier_t1461694466_0_0_0;
static const Il2CppType* GenInst_ParameterModifier_t1461694466_0_0_0_Types[] = { &ParameterModifier_t1461694466_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterModifier_t1461694466_0_0_0 = { 1, GenInst_ParameterModifier_t1461694466_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType MethodInfo_t_0_0_0;
static const Il2CppType* GenInst_MethodInfo_t_0_0_0_Types[] = { &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
extern const Il2CppType _MethodInfo_t3550065504_0_0_0;
static const Il2CppType* GenInst__MethodInfo_t3550065504_0_0_0_Types[] = { &_MethodInfo_t3550065504_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodInfo_t3550065504_0_0_0 = { 1, GenInst__MethodInfo_t3550065504_0_0_0_Types };
extern const Il2CppType MethodBase_t609368412_0_0_0;
static const Il2CppType* GenInst_MethodBase_t609368412_0_0_0_Types[] = { &MethodBase_t609368412_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBase_t609368412_0_0_0 = { 1, GenInst_MethodBase_t609368412_0_0_0_Types };
extern const Il2CppType _MethodBase_t1657248248_0_0_0;
static const Il2CppType* GenInst__MethodBase_t1657248248_0_0_0_Types[] = { &_MethodBase_t1657248248_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBase_t1657248248_0_0_0 = { 1, GenInst__MethodBase_t1657248248_0_0_0_Types };
extern const Il2CppType EventInfo_t_0_0_0;
static const Il2CppType* GenInst_EventInfo_t_0_0_0_Types[] = { &EventInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_EventInfo_t_0_0_0 = { 1, GenInst_EventInfo_t_0_0_0_Types };
extern const Il2CppType _EventInfo_t3826131156_0_0_0;
static const Il2CppType* GenInst__EventInfo_t3826131156_0_0_0_Types[] = { &_EventInfo_t3826131156_0_0_0 };
extern const Il2CppGenericInst GenInst__EventInfo_t3826131156_0_0_0 = { 1, GenInst__EventInfo_t3826131156_0_0_0_Types };
extern const Il2CppType FieldInfo_t_0_0_0;
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Types[] = { &FieldInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
extern const Il2CppType _FieldInfo_t2781946373_0_0_0;
static const Il2CppType* GenInst__FieldInfo_t2781946373_0_0_0_Types[] = { &_FieldInfo_t2781946373_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldInfo_t2781946373_0_0_0 = { 1, GenInst__FieldInfo_t2781946373_0_0_0_Types };
extern const Il2CppType PropertyInfo_t_0_0_0;
static const Il2CppType* GenInst_PropertyInfo_t_0_0_0_Types[] = { &PropertyInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
extern const Il2CppType _PropertyInfo_t4070324388_0_0_0;
static const Il2CppType* GenInst__PropertyInfo_t4070324388_0_0_0_Types[] = { &_PropertyInfo_t4070324388_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyInfo_t4070324388_0_0_0 = { 1, GenInst__PropertyInfo_t4070324388_0_0_0_Types };
extern const Il2CppType ConstructorInfo_t5769829_0_0_0;
static const Il2CppType* GenInst_ConstructorInfo_t5769829_0_0_0_Types[] = { &ConstructorInfo_t5769829_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t5769829_0_0_0 = { 1, GenInst_ConstructorInfo_t5769829_0_0_0_Types };
extern const Il2CppType _ConstructorInfo_t3357543833_0_0_0;
static const Il2CppType* GenInst__ConstructorInfo_t3357543833_0_0_0_Types[] = { &_ConstructorInfo_t3357543833_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t3357543833_0_0_0 = { 1, GenInst__ConstructorInfo_t3357543833_0_0_0_Types };
extern const Il2CppType IntPtr_t_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2401056908_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2401056908_0_0_0_Types[] = { &KeyValuePair_2_t2401056908_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2401056908_0_0_0 = { 1, GenInst_KeyValuePair_2_t2401056908_0_0_0_Types };
extern const Il2CppType Link_t544317964_0_0_0;
static const Il2CppType* GenInst_Link_t544317964_0_0_0_Types[] = { &Link_t544317964_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t544317964_0_0_0 = { 1, GenInst_Link_t544317964_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2950945753_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2950945753_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types };
extern const Il2CppType DictionaryEntry_t3123975638_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2950945753_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t3123975638_0_0_0_Types[] = { &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3123975638_0_0_0 = { 1, GenInst_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t2401056908_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2950945753_0_0_0, &KeyValuePair_2_t2401056908_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t2401056908_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t2401056908_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2950945753_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t838906923_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t838906923_0_0_0_Types[] = { &KeyValuePair_2_t838906923_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t838906923_0_0_0 = { 1, GenInst_KeyValuePair_2_t838906923_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t838906923_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2950945753_0_0_0, &KeyValuePair_2_t838906923_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t838906923_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t838906923_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2950945753_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2950945753_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types };
extern const Il2CppType TableRange_t3332867892_0_0_0;
static const Il2CppType* GenInst_TableRange_t3332867892_0_0_0_Types[] = { &TableRange_t3332867892_0_0_0 };
extern const Il2CppGenericInst GenInst_TableRange_t3332867892_0_0_0 = { 1, GenInst_TableRange_t3332867892_0_0_0_Types };
extern const Il2CppType TailoringInfo_t866433654_0_0_0;
static const Il2CppType* GenInst_TailoringInfo_t866433654_0_0_0_Types[] = { &TailoringInfo_t866433654_0_0_0 };
extern const Il2CppGenericInst GenInst_TailoringInfo_t866433654_0_0_0 = { 1, GenInst_TailoringInfo_t866433654_0_0_0_Types };
extern const Il2CppType Contraction_t1589275354_0_0_0;
static const Il2CppType* GenInst_Contraction_t1589275354_0_0_0_Types[] = { &Contraction_t1589275354_0_0_0 };
extern const Il2CppGenericInst GenInst_Contraction_t1589275354_0_0_0 = { 1, GenInst_Contraction_t1589275354_0_0_0_Types };
extern const Il2CppType Level2Map_t3640798870_0_0_0;
static const Il2CppType* GenInst_Level2Map_t3640798870_0_0_0_Types[] = { &Level2Map_t3640798870_0_0_0 };
extern const Il2CppGenericInst GenInst_Level2Map_t3640798870_0_0_0 = { 1, GenInst_Level2Map_t3640798870_0_0_0_Types };
extern const Il2CppType BigInteger_t2902905089_0_0_0;
static const Il2CppType* GenInst_BigInteger_t2902905089_0_0_0_Types[] = { &BigInteger_t2902905089_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t2902905089_0_0_0 = { 1, GenInst_BigInteger_t2902905089_0_0_0_Types };
extern const Il2CppType UriScheme_t2867806342_0_0_0;
static const Il2CppType* GenInst_UriScheme_t2867806342_0_0_0_Types[] = { &UriScheme_t2867806342_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t2867806342_0_0_0 = { 1, GenInst_UriScheme_t2867806342_0_0_0_Types };
extern const Il2CppType KeySizes_t85027896_0_0_0;
static const Il2CppType* GenInst_KeySizes_t85027896_0_0_0_Types[] = { &KeySizes_t85027896_0_0_0 };
extern const Il2CppGenericInst GenInst_KeySizes_t85027896_0_0_0 = { 1, GenInst_KeySizes_t85027896_0_0_0_Types };
extern const Il2CppType Assembly_t4102432799_0_0_0;
static const Il2CppType* GenInst_Assembly_t4102432799_0_0_0_Types[] = { &Assembly_t4102432799_0_0_0 };
extern const Il2CppGenericInst GenInst_Assembly_t4102432799_0_0_0 = { 1, GenInst_Assembly_t4102432799_0_0_0_Types };
extern const Il2CppType _Assembly_t1988906988_0_0_0;
static const Il2CppType* GenInst__Assembly_t1988906988_0_0_0_Types[] = { &_Assembly_t1988906988_0_0_0 };
extern const Il2CppGenericInst GenInst__Assembly_t1988906988_0_0_0 = { 1, GenInst__Assembly_t1988906988_0_0_0_Types };
extern const Il2CppType IEvidenceFactory_t4119273121_0_0_0;
static const Il2CppType* GenInst_IEvidenceFactory_t4119273121_0_0_0_Types[] = { &IEvidenceFactory_t4119273121_0_0_0 };
extern const Il2CppGenericInst GenInst_IEvidenceFactory_t4119273121_0_0_0 = { 1, GenInst_IEvidenceFactory_t4119273121_0_0_0_Types };
extern const Il2CppType DateTime_t3738529785_0_0_0;
static const Il2CppType* GenInst_DateTime_t3738529785_0_0_0_Types[] = { &DateTime_t3738529785_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t3738529785_0_0_0 = { 1, GenInst_DateTime_t3738529785_0_0_0_Types };
extern const Il2CppType DateTimeOffset_t3229287507_0_0_0;
static const Il2CppType* GenInst_DateTimeOffset_t3229287507_0_0_0_Types[] = { &DateTimeOffset_t3229287507_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t3229287507_0_0_0 = { 1, GenInst_DateTimeOffset_t3229287507_0_0_0_Types };
extern const Il2CppType TimeSpan_t881159249_0_0_0;
static const Il2CppType* GenInst_TimeSpan_t881159249_0_0_0_Types[] = { &TimeSpan_t881159249_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t881159249_0_0_0 = { 1, GenInst_TimeSpan_t881159249_0_0_0_Types };
extern const Il2CppType Guid_t_0_0_0;
static const Il2CppType* GenInst_Guid_t_0_0_0_Types[] = { &Guid_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Guid_t_0_0_0 = { 1, GenInst_Guid_t_0_0_0_Types };
extern const Il2CppType CustomAttributeData_t1084486650_0_0_0;
static const Il2CppType* GenInst_CustomAttributeData_t1084486650_0_0_0_Types[] = { &CustomAttributeData_t1084486650_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t1084486650_0_0_0 = { 1, GenInst_CustomAttributeData_t1084486650_0_0_0_Types };
extern const Il2CppType TermInfoStrings_t290279955_0_0_0;
static const Il2CppType* GenInst_TermInfoStrings_t290279955_0_0_0_Types[] = { &TermInfoStrings_t290279955_0_0_0 };
extern const Il2CppGenericInst GenInst_TermInfoStrings_t290279955_0_0_0 = { 1, GenInst_TermInfoStrings_t290279955_0_0_0_Types };
extern const Il2CppType Version_t3456873960_0_0_0;
static const Il2CppType* GenInst_Version_t3456873960_0_0_0_Types[] = { &Version_t3456873960_0_0_0 };
extern const Il2CppGenericInst GenInst_Version_t3456873960_0_0_0 = { 1, GenInst_Version_t3456873960_0_0_0_Types };
extern const Il2CppType Slot_t3975888750_0_0_0;
static const Il2CppType* GenInst_Slot_t3975888750_0_0_0_Types[] = { &Slot_t3975888750_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t3975888750_0_0_0 = { 1, GenInst_Slot_t3975888750_0_0_0_Types };
extern const Il2CppType Slot_t384495010_0_0_0;
static const Il2CppType* GenInst_Slot_t384495010_0_0_0_Types[] = { &Slot_t384495010_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t384495010_0_0_0 = { 1, GenInst_Slot_t384495010_0_0_0_Types };
extern const Il2CppType StackFrame_t3217253059_0_0_0;
static const Il2CppType* GenInst_StackFrame_t3217253059_0_0_0_Types[] = { &StackFrame_t3217253059_0_0_0 };
extern const Il2CppGenericInst GenInst_StackFrame_t3217253059_0_0_0 = { 1, GenInst_StackFrame_t3217253059_0_0_0_Types };
extern const Il2CppType Calendar_t1661121569_0_0_0;
static const Il2CppType* GenInst_Calendar_t1661121569_0_0_0_Types[] = { &Calendar_t1661121569_0_0_0 };
extern const Il2CppGenericInst GenInst_Calendar_t1661121569_0_0_0 = { 1, GenInst_Calendar_t1661121569_0_0_0_Types };
extern const Il2CppType CultureInfo_t4157843068_0_0_0;
static const Il2CppType* GenInst_CultureInfo_t4157843068_0_0_0_Types[] = { &CultureInfo_t4157843068_0_0_0 };
extern const Il2CppGenericInst GenInst_CultureInfo_t4157843068_0_0_0 = { 1, GenInst_CultureInfo_t4157843068_0_0_0_Types };
extern const Il2CppType IFormatProvider_t2518567562_0_0_0;
static const Il2CppType* GenInst_IFormatProvider_t2518567562_0_0_0_Types[] = { &IFormatProvider_t2518567562_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormatProvider_t2518567562_0_0_0 = { 1, GenInst_IFormatProvider_t2518567562_0_0_0_Types };
extern const Il2CppType FileInfo_t1169991790_0_0_0;
static const Il2CppType* GenInst_FileInfo_t1169991790_0_0_0_Types[] = { &FileInfo_t1169991790_0_0_0 };
extern const Il2CppGenericInst GenInst_FileInfo_t1169991790_0_0_0 = { 1, GenInst_FileInfo_t1169991790_0_0_0_Types };
extern const Il2CppType FileSystemInfo_t3745885336_0_0_0;
static const Il2CppType* GenInst_FileSystemInfo_t3745885336_0_0_0_Types[] = { &FileSystemInfo_t3745885336_0_0_0 };
extern const Il2CppGenericInst GenInst_FileSystemInfo_t3745885336_0_0_0 = { 1, GenInst_FileSystemInfo_t3745885336_0_0_0_Types };
extern const Il2CppType MarshalByRefObject_t2760389100_0_0_0;
static const Il2CppType* GenInst_MarshalByRefObject_t2760389100_0_0_0_Types[] = { &MarshalByRefObject_t2760389100_0_0_0 };
extern const Il2CppGenericInst GenInst_MarshalByRefObject_t2760389100_0_0_0 = { 1, GenInst_MarshalByRefObject_t2760389100_0_0_0_Types };
extern const Il2CppType Module_t2987026101_0_0_0;
static const Il2CppType* GenInst_Module_t2987026101_0_0_0_Types[] = { &Module_t2987026101_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t2987026101_0_0_0 = { 1, GenInst_Module_t2987026101_0_0_0_Types };
extern const Il2CppType _Module_t135161706_0_0_0;
static const Il2CppType* GenInst__Module_t135161706_0_0_0_Types[] = { &_Module_t135161706_0_0_0 };
extern const Il2CppGenericInst GenInst__Module_t135161706_0_0_0 = { 1, GenInst__Module_t135161706_0_0_0_Types };
extern const Il2CppType CustomAttributeTypedArgument_t2723150157_0_0_0;
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t2723150157_0_0_0_Types[] = { &CustomAttributeTypedArgument_t2723150157_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t2723150157_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t2723150157_0_0_0_Types };
extern const Il2CppType CustomAttributeNamedArgument_t287865710_0_0_0;
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t287865710_0_0_0_Types[] = { &CustomAttributeNamedArgument_t287865710_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t287865710_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t287865710_0_0_0_Types };
extern const Il2CppType Exception_t1436737249_0_0_0;
static const Il2CppType* GenInst_Exception_t1436737249_0_0_0_Types[] = { &Exception_t1436737249_0_0_0 };
extern const Il2CppGenericInst GenInst_Exception_t1436737249_0_0_0 = { 1, GenInst_Exception_t1436737249_0_0_0_Types };
extern const Il2CppType _Exception_t2109637702_0_0_0;
static const Il2CppType* GenInst__Exception_t2109637702_0_0_0_Types[] = { &_Exception_t2109637702_0_0_0 };
extern const Il2CppGenericInst GenInst__Exception_t2109637702_0_0_0 = { 1, GenInst__Exception_t2109637702_0_0_0_Types };
extern const Il2CppType ModuleBuilder_t731887691_0_0_0;
static const Il2CppType* GenInst_ModuleBuilder_t731887691_0_0_0_Types[] = { &ModuleBuilder_t731887691_0_0_0 };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t731887691_0_0_0 = { 1, GenInst_ModuleBuilder_t731887691_0_0_0_Types };
extern const Il2CppType _ModuleBuilder_t3217089703_0_0_0;
static const Il2CppType* GenInst__ModuleBuilder_t3217089703_0_0_0_Types[] = { &_ModuleBuilder_t3217089703_0_0_0 };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t3217089703_0_0_0 = { 1, GenInst__ModuleBuilder_t3217089703_0_0_0_Types };
extern const Il2CppType MonoResource_t4103430009_0_0_0;
static const Il2CppType* GenInst_MonoResource_t4103430009_0_0_0_Types[] = { &MonoResource_t4103430009_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoResource_t4103430009_0_0_0 = { 1, GenInst_MonoResource_t4103430009_0_0_0_Types };
extern const Il2CppType RefEmitPermissionSet_t484390987_0_0_0;
static const Il2CppType* GenInst_RefEmitPermissionSet_t484390987_0_0_0_Types[] = { &RefEmitPermissionSet_t484390987_0_0_0 };
extern const Il2CppGenericInst GenInst_RefEmitPermissionSet_t484390987_0_0_0 = { 1, GenInst_RefEmitPermissionSet_t484390987_0_0_0_Types };
extern const Il2CppType ParameterBuilder_t1137139675_0_0_0;
static const Il2CppType* GenInst_ParameterBuilder_t1137139675_0_0_0_Types[] = { &ParameterBuilder_t1137139675_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t1137139675_0_0_0 = { 1, GenInst_ParameterBuilder_t1137139675_0_0_0_Types };
extern const Il2CppType _ParameterBuilder_t3901898075_0_0_0;
static const Il2CppType* GenInst__ParameterBuilder_t3901898075_0_0_0_Types[] = { &_ParameterBuilder_t3901898075_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t3901898075_0_0_0 = { 1, GenInst__ParameterBuilder_t3901898075_0_0_0_Types };
extern const Il2CppType TypeU5BU5D_t3940880105_0_0_0;
static const Il2CppType* GenInst_TypeU5BU5D_t3940880105_0_0_0_Types[] = { &TypeU5BU5D_t3940880105_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t3940880105_0_0_0 = { 1, GenInst_TypeU5BU5D_t3940880105_0_0_0_Types };
extern const Il2CppType Il2CppArray_0_0_0;
static const Il2CppType* GenInst_Il2CppArray_0_0_0_Types[] = { &Il2CppArray_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppArray_0_0_0 = { 1, GenInst_Il2CppArray_0_0_0_Types };
extern const Il2CppType ICollection_t3904884886_0_0_0;
static const Il2CppType* GenInst_ICollection_t3904884886_0_0_0_Types[] = { &ICollection_t3904884886_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_t3904884886_0_0_0 = { 1, GenInst_ICollection_t3904884886_0_0_0_Types };
extern const Il2CppType IList_t2094931216_0_0_0;
static const Il2CppType* GenInst_IList_t2094931216_0_0_0_Types[] = { &IList_t2094931216_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_t2094931216_0_0_0 = { 1, GenInst_IList_t2094931216_0_0_0_Types };
extern const Il2CppType IList_1_t4297247_0_0_0;
static const Il2CppType* GenInst_IList_1_t4297247_0_0_0_Types[] = { &IList_1_t4297247_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t4297247_0_0_0 = { 1, GenInst_IList_1_t4297247_0_0_0_Types };
extern const Il2CppType ICollection_1_t1017129698_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1017129698_0_0_0_Types[] = { &ICollection_1_t1017129698_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1017129698_0_0_0 = { 1, GenInst_ICollection_1_t1017129698_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t1463797649_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t1463797649_0_0_0_Types[] = { &IEnumerable_1_t1463797649_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1463797649_0_0_0 = { 1, GenInst_IEnumerable_1_t1463797649_0_0_0_Types };
extern const Il2CppType IList_1_t74629426_0_0_0;
static const Il2CppType* GenInst_IList_1_t74629426_0_0_0_Types[] = { &IList_1_t74629426_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t74629426_0_0_0 = { 1, GenInst_IList_1_t74629426_0_0_0_Types };
extern const Il2CppType ICollection_1_t1087461877_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1087461877_0_0_0_Types[] = { &ICollection_1_t1087461877_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1087461877_0_0_0 = { 1, GenInst_ICollection_1_t1087461877_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t1534129828_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t1534129828_0_0_0_Types[] = { &IEnumerable_1_t1534129828_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1534129828_0_0_0 = { 1, GenInst_IEnumerable_1_t1534129828_0_0_0_Types };
extern const Il2CppType IList_1_t1108916738_0_0_0;
static const Il2CppType* GenInst_IList_1_t1108916738_0_0_0_Types[] = { &IList_1_t1108916738_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t1108916738_0_0_0 = { 1, GenInst_IList_1_t1108916738_0_0_0_Types };
extern const Il2CppType ICollection_1_t2121749189_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t2121749189_0_0_0_Types[] = { &ICollection_1_t2121749189_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t2121749189_0_0_0 = { 1, GenInst_ICollection_1_t2121749189_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2568417140_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t2568417140_0_0_0_Types[] = { &IEnumerable_1_t2568417140_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2568417140_0_0_0 = { 1, GenInst_IEnumerable_1_t2568417140_0_0_0_Types };
extern const Il2CppType IList_1_t900354228_0_0_0;
static const Il2CppType* GenInst_IList_1_t900354228_0_0_0_Types[] = { &IList_1_t900354228_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t900354228_0_0_0 = { 1, GenInst_IList_1_t900354228_0_0_0_Types };
extern const Il2CppType ICollection_1_t1913186679_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1913186679_0_0_0_Types[] = { &ICollection_1_t1913186679_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1913186679_0_0_0 = { 1, GenInst_ICollection_1_t1913186679_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2359854630_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t2359854630_0_0_0_Types[] = { &IEnumerable_1_t2359854630_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2359854630_0_0_0 = { 1, GenInst_IEnumerable_1_t2359854630_0_0_0_Types };
extern const Il2CppType IList_1_t3346143920_0_0_0;
static const Il2CppType* GenInst_IList_1_t3346143920_0_0_0_Types[] = { &IList_1_t3346143920_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3346143920_0_0_0 = { 1, GenInst_IList_1_t3346143920_0_0_0_Types };
extern const Il2CppType ICollection_1_t64009075_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t64009075_0_0_0_Types[] = { &ICollection_1_t64009075_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t64009075_0_0_0 = { 1, GenInst_ICollection_1_t64009075_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t510677026_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t510677026_0_0_0_Types[] = { &IEnumerable_1_t510677026_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t510677026_0_0_0 = { 1, GenInst_IEnumerable_1_t510677026_0_0_0_Types };
extern const Il2CppType IList_1_t1442829200_0_0_0;
static const Il2CppType* GenInst_IList_1_t1442829200_0_0_0_Types[] = { &IList_1_t1442829200_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t1442829200_0_0_0 = { 1, GenInst_IList_1_t1442829200_0_0_0_Types };
extern const Il2CppType ICollection_1_t2455661651_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t2455661651_0_0_0_Types[] = { &ICollection_1_t2455661651_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t2455661651_0_0_0 = { 1, GenInst_ICollection_1_t2455661651_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2902329602_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t2902329602_0_0_0_Types[] = { &IEnumerable_1_t2902329602_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2902329602_0_0_0 = { 1, GenInst_IEnumerable_1_t2902329602_0_0_0_Types };
extern const Il2CppType IList_1_t600458651_0_0_0;
static const Il2CppType* GenInst_IList_1_t600458651_0_0_0_Types[] = { &IList_1_t600458651_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t600458651_0_0_0 = { 1, GenInst_IList_1_t600458651_0_0_0_Types };
extern const Il2CppType ICollection_1_t1613291102_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1613291102_0_0_0_Types[] = { &ICollection_1_t1613291102_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1613291102_0_0_0 = { 1, GenInst_ICollection_1_t1613291102_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2059959053_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t2059959053_0_0_0_Types[] = { &IEnumerable_1_t2059959053_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2059959053_0_0_0 = { 1, GenInst_IEnumerable_1_t2059959053_0_0_0_Types };
extern const Il2CppType LocalBuilder_t3562264111_0_0_0;
static const Il2CppType* GenInst_LocalBuilder_t3562264111_0_0_0_Types[] = { &LocalBuilder_t3562264111_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalBuilder_t3562264111_0_0_0 = { 1, GenInst_LocalBuilder_t3562264111_0_0_0_Types };
extern const Il2CppType _LocalBuilder_t484236194_0_0_0;
static const Il2CppType* GenInst__LocalBuilder_t484236194_0_0_0_Types[] = { &_LocalBuilder_t484236194_0_0_0 };
extern const Il2CppGenericInst GenInst__LocalBuilder_t484236194_0_0_0 = { 1, GenInst__LocalBuilder_t484236194_0_0_0_Types };
extern const Il2CppType LocalVariableInfo_t2426779395_0_0_0;
static const Il2CppType* GenInst_LocalVariableInfo_t2426779395_0_0_0_Types[] = { &LocalVariableInfo_t2426779395_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalVariableInfo_t2426779395_0_0_0 = { 1, GenInst_LocalVariableInfo_t2426779395_0_0_0_Types };
extern const Il2CppType ILTokenInfo_t2325775114_0_0_0;
static const Il2CppType* GenInst_ILTokenInfo_t2325775114_0_0_0_Types[] = { &ILTokenInfo_t2325775114_0_0_0 };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t2325775114_0_0_0 = { 1, GenInst_ILTokenInfo_t2325775114_0_0_0_Types };
extern const Il2CppType LabelData_t360167391_0_0_0;
static const Il2CppType* GenInst_LabelData_t360167391_0_0_0_Types[] = { &LabelData_t360167391_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelData_t360167391_0_0_0 = { 1, GenInst_LabelData_t360167391_0_0_0_Types };
extern const Il2CppType LabelFixup_t858502054_0_0_0;
static const Il2CppType* GenInst_LabelFixup_t858502054_0_0_0_Types[] = { &LabelFixup_t858502054_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelFixup_t858502054_0_0_0 = { 1, GenInst_LabelFixup_t858502054_0_0_0_Types };
extern const Il2CppType GenericTypeParameterBuilder_t1988827940_0_0_0;
static const Il2CppType* GenInst_GenericTypeParameterBuilder_t1988827940_0_0_0_Types[] = { &GenericTypeParameterBuilder_t1988827940_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t1988827940_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t1988827940_0_0_0_Types };
extern const Il2CppType TypeBuilder_t1073948154_0_0_0;
static const Il2CppType* GenInst_TypeBuilder_t1073948154_0_0_0_Types[] = { &TypeBuilder_t1073948154_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeBuilder_t1073948154_0_0_0 = { 1, GenInst_TypeBuilder_t1073948154_0_0_0_Types };
extern const Il2CppType _TypeBuilder_t2501637272_0_0_0;
static const Il2CppType* GenInst__TypeBuilder_t2501637272_0_0_0_Types[] = { &_TypeBuilder_t2501637272_0_0_0 };
extern const Il2CppGenericInst GenInst__TypeBuilder_t2501637272_0_0_0 = { 1, GenInst__TypeBuilder_t2501637272_0_0_0_Types };
extern const Il2CppType MethodBuilder_t2807316753_0_0_0;
static const Il2CppType* GenInst_MethodBuilder_t2807316753_0_0_0_Types[] = { &MethodBuilder_t2807316753_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBuilder_t2807316753_0_0_0 = { 1, GenInst_MethodBuilder_t2807316753_0_0_0_Types };
extern const Il2CppType _MethodBuilder_t600455149_0_0_0;
static const Il2CppType* GenInst__MethodBuilder_t600455149_0_0_0_Types[] = { &_MethodBuilder_t600455149_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBuilder_t600455149_0_0_0 = { 1, GenInst__MethodBuilder_t600455149_0_0_0_Types };
extern const Il2CppType ConstructorBuilder_t2813524108_0_0_0;
static const Il2CppType* GenInst_ConstructorBuilder_t2813524108_0_0_0_Types[] = { &ConstructorBuilder_t2813524108_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t2813524108_0_0_0 = { 1, GenInst_ConstructorBuilder_t2813524108_0_0_0_Types };
extern const Il2CppType _ConstructorBuilder_t2416550571_0_0_0;
static const Il2CppType* GenInst__ConstructorBuilder_t2416550571_0_0_0_Types[] = { &_ConstructorBuilder_t2416550571_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t2416550571_0_0_0 = { 1, GenInst__ConstructorBuilder_t2416550571_0_0_0_Types };
extern const Il2CppType PropertyBuilder_t314297007_0_0_0;
static const Il2CppType* GenInst_PropertyBuilder_t314297007_0_0_0_Types[] = { &PropertyBuilder_t314297007_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyBuilder_t314297007_0_0_0 = { 1, GenInst_PropertyBuilder_t314297007_0_0_0_Types };
extern const Il2CppType _PropertyBuilder_t1366136710_0_0_0;
static const Il2CppType* GenInst__PropertyBuilder_t1366136710_0_0_0_Types[] = { &_PropertyBuilder_t1366136710_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyBuilder_t1366136710_0_0_0 = { 1, GenInst__PropertyBuilder_t1366136710_0_0_0_Types };
extern const Il2CppType FieldBuilder_t2627049993_0_0_0;
static const Il2CppType* GenInst_FieldBuilder_t2627049993_0_0_0_Types[] = { &FieldBuilder_t2627049993_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldBuilder_t2627049993_0_0_0 = { 1, GenInst_FieldBuilder_t2627049993_0_0_0_Types };
extern const Il2CppType _FieldBuilder_t2615792726_0_0_0;
static const Il2CppType* GenInst__FieldBuilder_t2615792726_0_0_0_Types[] = { &_FieldBuilder_t2615792726_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldBuilder_t2615792726_0_0_0 = { 1, GenInst__FieldBuilder_t2615792726_0_0_0_Types };
extern const Il2CppType ResourceInfo_t2872965302_0_0_0;
static const Il2CppType* GenInst_ResourceInfo_t2872965302_0_0_0_Types[] = { &ResourceInfo_t2872965302_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceInfo_t2872965302_0_0_0 = { 1, GenInst_ResourceInfo_t2872965302_0_0_0_Types };
extern const Il2CppType ResourceCacheItem_t51292791_0_0_0;
static const Il2CppType* GenInst_ResourceCacheItem_t51292791_0_0_0_Types[] = { &ResourceCacheItem_t51292791_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceCacheItem_t51292791_0_0_0 = { 1, GenInst_ResourceCacheItem_t51292791_0_0_0_Types };
extern const Il2CppType IContextAttribute_t176678928_0_0_0;
static const Il2CppType* GenInst_IContextAttribute_t176678928_0_0_0_Types[] = { &IContextAttribute_t176678928_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextAttribute_t176678928_0_0_0 = { 1, GenInst_IContextAttribute_t176678928_0_0_0_Types };
extern const Il2CppType IContextProperty_t840037424_0_0_0;
static const Il2CppType* GenInst_IContextProperty_t840037424_0_0_0_Types[] = { &IContextProperty_t840037424_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextProperty_t840037424_0_0_0 = { 1, GenInst_IContextProperty_t840037424_0_0_0_Types };
extern const Il2CppType Header_t549724581_0_0_0;
static const Il2CppType* GenInst_Header_t549724581_0_0_0_Types[] = { &Header_t549724581_0_0_0 };
extern const Il2CppGenericInst GenInst_Header_t549724581_0_0_0 = { 1, GenInst_Header_t549724581_0_0_0_Types };
extern const Il2CppType ITrackingHandler_t1244553475_0_0_0;
static const Il2CppType* GenInst_ITrackingHandler_t1244553475_0_0_0_Types[] = { &ITrackingHandler_t1244553475_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t1244553475_0_0_0 = { 1, GenInst_ITrackingHandler_t1244553475_0_0_0_Types };
extern const Il2CppType TypeTag_t3541821701_0_0_0;
static const Il2CppType* GenInst_TypeTag_t3541821701_0_0_0_Types[] = { &TypeTag_t3541821701_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeTag_t3541821701_0_0_0 = { 1, GenInst_TypeTag_t3541821701_0_0_0_Types };
extern const Il2CppType MonoType_t_0_0_0;
static const Il2CppType* GenInst_MonoType_t_0_0_0_Types[] = { &MonoType_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
extern const Il2CppType KeyContainerPermissionAccessEntry_t3026022710_0_0_0;
static const Il2CppType* GenInst_KeyContainerPermissionAccessEntry_t3026022710_0_0_0_Types[] = { &KeyContainerPermissionAccessEntry_t3026022710_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyContainerPermissionAccessEntry_t3026022710_0_0_0 = { 1, GenInst_KeyContainerPermissionAccessEntry_t3026022710_0_0_0_Types };
extern const Il2CppType StrongName_t3675724614_0_0_0;
static const Il2CppType* GenInst_StrongName_t3675724614_0_0_0_Types[] = { &StrongName_t3675724614_0_0_0 };
extern const Il2CppGenericInst GenInst_StrongName_t3675724614_0_0_0 = { 1, GenInst_StrongName_t3675724614_0_0_0_Types };
extern const Il2CppType IBuiltInEvidence_t554693121_0_0_0;
static const Il2CppType* GenInst_IBuiltInEvidence_t554693121_0_0_0_Types[] = { &IBuiltInEvidence_t554693121_0_0_0 };
extern const Il2CppGenericInst GenInst_IBuiltInEvidence_t554693121_0_0_0 = { 1, GenInst_IBuiltInEvidence_t554693121_0_0_0_Types };
extern const Il2CppType IIdentityPermissionFactory_t3268650966_0_0_0;
static const Il2CppType* GenInst_IIdentityPermissionFactory_t3268650966_0_0_0_Types[] = { &IIdentityPermissionFactory_t3268650966_0_0_0 };
extern const Il2CppGenericInst GenInst_IIdentityPermissionFactory_t3268650966_0_0_0 = { 1, GenInst_IIdentityPermissionFactory_t3268650966_0_0_0_Types };
extern const Il2CppType CodeConnectAccess_t1103527196_0_0_0;
static const Il2CppType* GenInst_CodeConnectAccess_t1103527196_0_0_0_Types[] = { &CodeConnectAccess_t1103527196_0_0_0 };
extern const Il2CppGenericInst GenInst_CodeConnectAccess_t1103527196_0_0_0 = { 1, GenInst_CodeConnectAccess_t1103527196_0_0_0_Types };
extern const Il2CppType WaitHandle_t1743403487_0_0_0;
static const Il2CppType* GenInst_WaitHandle_t1743403487_0_0_0_Types[] = { &WaitHandle_t1743403487_0_0_0 };
extern const Il2CppGenericInst GenInst_WaitHandle_t1743403487_0_0_0 = { 1, GenInst_WaitHandle_t1743403487_0_0_0_Types };
extern const Il2CppType IDisposable_t3640265483_0_0_0;
static const Il2CppType* GenInst_IDisposable_t3640265483_0_0_0_Types[] = { &IDisposable_t3640265483_0_0_0 };
extern const Il2CppGenericInst GenInst_IDisposable_t3640265483_0_0_0 = { 1, GenInst_IDisposable_t3640265483_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2530217319_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2530217319_0_0_0_Types[] = { &KeyValuePair_2_t2530217319_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2530217319_0_0_0 = { 1, GenInst_KeyValuePair_2_t2530217319_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2530217319_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t2530217319_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2530217319_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2530217319_0_0_0_Types };
extern const Il2CppType BigInteger_t2902905090_0_0_0;
static const Il2CppType* GenInst_BigInteger_t2902905090_0_0_0_Types[] = { &BigInteger_t2902905090_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t2902905090_0_0_0 = { 1, GenInst_BigInteger_t2902905090_0_0_0_Types };
extern const Il2CppType ByteU5BU5D_t4116647657_0_0_0;
static const Il2CppType* GenInst_ByteU5BU5D_t4116647657_0_0_0_Types[] = { &ByteU5BU5D_t4116647657_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t4116647657_0_0_0 = { 1, GenInst_ByteU5BU5D_t4116647657_0_0_0_Types };
extern const Il2CppType IList_1_t2949616159_0_0_0;
static const Il2CppType* GenInst_IList_1_t2949616159_0_0_0_Types[] = { &IList_1_t2949616159_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t2949616159_0_0_0 = { 1, GenInst_IList_1_t2949616159_0_0_0_Types };
extern const Il2CppType ICollection_1_t3962448610_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t3962448610_0_0_0_Types[] = { &ICollection_1_t3962448610_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t3962448610_0_0_0 = { 1, GenInst_ICollection_1_t3962448610_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t114149265_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t114149265_0_0_0_Types[] = { &IEnumerable_1_t114149265_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t114149265_0_0_0 = { 1, GenInst_IEnumerable_1_t114149265_0_0_0_Types };
extern const Il2CppType ClientCertificateType_t1004704908_0_0_0;
static const Il2CppType* GenInst_ClientCertificateType_t1004704908_0_0_0_Types[] = { &ClientCertificateType_t1004704908_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t1004704908_0_0_0 = { 1, GenInst_ClientCertificateType_t1004704908_0_0_0_Types };
extern const Il2CppType X509Certificate_t713131622_0_0_0;
static const Il2CppType* GenInst_X509Certificate_t713131622_0_0_0_Types[] = { &X509Certificate_t713131622_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Certificate_t713131622_0_0_0 = { 1, GenInst_X509Certificate_t713131622_0_0_0_Types };
extern const Il2CppType IDeserializationCallback_t4220500054_0_0_0;
static const Il2CppType* GenInst_IDeserializationCallback_t4220500054_0_0_0_Types[] = { &IDeserializationCallback_t4220500054_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t4220500054_0_0_0 = { 1, GenInst_IDeserializationCallback_t4220500054_0_0_0_Types };
extern const Il2CppType ConfigurationProperty_t3590861854_0_0_0;
static const Il2CppType* GenInst_ConfigurationProperty_t3590861854_0_0_0_Types[] = { &ConfigurationProperty_t3590861854_0_0_0 };
extern const Il2CppGenericInst GenInst_ConfigurationProperty_t3590861854_0_0_0 = { 1, GenInst_ConfigurationProperty_t3590861854_0_0_0_Types };
extern const Il2CppType XmlElement_t561603118_0_0_0;
static const Il2CppType* GenInst_XmlElement_t561603118_0_0_0_Types[] = { &XmlElement_t561603118_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlElement_t561603118_0_0_0 = { 1, GenInst_XmlElement_t561603118_0_0_0_Types };
extern const Il2CppType IHasXmlChildNode_t2708887342_0_0_0;
static const Il2CppType* GenInst_IHasXmlChildNode_t2708887342_0_0_0_Types[] = { &IHasXmlChildNode_t2708887342_0_0_0 };
extern const Il2CppGenericInst GenInst_IHasXmlChildNode_t2708887342_0_0_0 = { 1, GenInst_IHasXmlChildNode_t2708887342_0_0_0_Types };
extern const Il2CppType XmlLinkedNode_t1437094927_0_0_0;
static const Il2CppType* GenInst_XmlLinkedNode_t1437094927_0_0_0_Types[] = { &XmlLinkedNode_t1437094927_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlLinkedNode_t1437094927_0_0_0 = { 1, GenInst_XmlLinkedNode_t1437094927_0_0_0_Types };
extern const Il2CppType XmlNode_t3767805227_0_0_0;
static const Il2CppType* GenInst_XmlNode_t3767805227_0_0_0_Types[] = { &XmlNode_t3767805227_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlNode_t3767805227_0_0_0 = { 1, GenInst_XmlNode_t3767805227_0_0_0_Types };
extern const Il2CppType IXPathNavigable_t2716156786_0_0_0;
static const Il2CppType* GenInst_IXPathNavigable_t2716156786_0_0_0_Types[] = { &IXPathNavigable_t2716156786_0_0_0 };
extern const Il2CppGenericInst GenInst_IXPathNavigable_t2716156786_0_0_0 = { 1, GenInst_IXPathNavigable_t2716156786_0_0_0_Types };
extern const Il2CppType XPathResultType_t2828988488_0_0_0;
static const Il2CppType* GenInst_XPathResultType_t2828988488_0_0_0_Types[] = { &XPathResultType_t2828988488_0_0_0 };
extern const Il2CppGenericInst GenInst_XPathResultType_t2828988488_0_0_0 = { 1, GenInst_XPathResultType_t2828988488_0_0_0_Types };
extern const Il2CppType XmlSchemaAttribute_t2797257020_0_0_0;
static const Il2CppType* GenInst_XmlSchemaAttribute_t2797257020_0_0_0_Types[] = { &XmlSchemaAttribute_t2797257020_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaAttribute_t2797257020_0_0_0 = { 1, GenInst_XmlSchemaAttribute_t2797257020_0_0_0_Types };
extern const Il2CppType XmlSchemaAnnotated_t2603549639_0_0_0;
static const Il2CppType* GenInst_XmlSchemaAnnotated_t2603549639_0_0_0_Types[] = { &XmlSchemaAnnotated_t2603549639_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaAnnotated_t2603549639_0_0_0 = { 1, GenInst_XmlSchemaAnnotated_t2603549639_0_0_0_Types };
extern const Il2CppType XmlSchemaObject_t1315720168_0_0_0;
static const Il2CppType* GenInst_XmlSchemaObject_t1315720168_0_0_0_Types[] = { &XmlSchemaObject_t1315720168_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaObject_t1315720168_0_0_0 = { 1, GenInst_XmlSchemaObject_t1315720168_0_0_0_Types };
extern const Il2CppType XsdIdentityPath_t991900844_0_0_0;
static const Il2CppType* GenInst_XsdIdentityPath_t991900844_0_0_0_Types[] = { &XsdIdentityPath_t991900844_0_0_0 };
extern const Il2CppGenericInst GenInst_XsdIdentityPath_t991900844_0_0_0 = { 1, GenInst_XsdIdentityPath_t991900844_0_0_0_Types };
extern const Il2CppType XsdIdentityField_t1964115728_0_0_0;
static const Il2CppType* GenInst_XsdIdentityField_t1964115728_0_0_0_Types[] = { &XsdIdentityField_t1964115728_0_0_0 };
extern const Il2CppGenericInst GenInst_XsdIdentityField_t1964115728_0_0_0 = { 1, GenInst_XsdIdentityField_t1964115728_0_0_0_Types };
extern const Il2CppType XsdIdentityStep_t1480907129_0_0_0;
static const Il2CppType* GenInst_XsdIdentityStep_t1480907129_0_0_0_Types[] = { &XsdIdentityStep_t1480907129_0_0_0 };
extern const Il2CppGenericInst GenInst_XsdIdentityStep_t1480907129_0_0_0 = { 1, GenInst_XsdIdentityStep_t1480907129_0_0_0_Types };
extern const Il2CppType DTMXPathLinkedNode2_t3353097823_0_0_0;
static const Il2CppType* GenInst_DTMXPathLinkedNode2_t3353097823_0_0_0_Types[] = { &DTMXPathLinkedNode2_t3353097823_0_0_0 };
extern const Il2CppGenericInst GenInst_DTMXPathLinkedNode2_t3353097823_0_0_0 = { 1, GenInst_DTMXPathLinkedNode2_t3353097823_0_0_0_Types };
extern const Il2CppType DTMXPathAttributeNode2_t3707096872_0_0_0;
static const Il2CppType* GenInst_DTMXPathAttributeNode2_t3707096872_0_0_0_Types[] = { &DTMXPathAttributeNode2_t3707096872_0_0_0 };
extern const Il2CppGenericInst GenInst_DTMXPathAttributeNode2_t3707096872_0_0_0 = { 1, GenInst_DTMXPathAttributeNode2_t3707096872_0_0_0_Types };
extern const Il2CppType DTMXPathNamespaceNode2_t1119120712_0_0_0;
static const Il2CppType* GenInst_DTMXPathNamespaceNode2_t1119120712_0_0_0_Types[] = { &DTMXPathNamespaceNode2_t1119120712_0_0_0 };
extern const Il2CppGenericInst GenInst_DTMXPathNamespaceNode2_t1119120712_0_0_0 = { 1, GenInst_DTMXPathNamespaceNode2_t1119120712_0_0_0_Types };
extern const Il2CppType Sort_t3076360436_0_0_0;
static const Il2CppType* GenInst_Sort_t3076360436_0_0_0_Types[] = { &Sort_t3076360436_0_0_0 };
extern const Il2CppGenericInst GenInst_Sort_t3076360436_0_0_0 = { 1, GenInst_Sort_t3076360436_0_0_0_Types };
extern const Il2CppType XmlQualifiedName_t2760654312_0_0_0;
static const Il2CppType* GenInst_XmlQualifiedName_t2760654312_0_0_0_Types[] = { &XmlQualifiedName_t2760654312_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlQualifiedName_t2760654312_0_0_0 = { 1, GenInst_XmlQualifiedName_t2760654312_0_0_0_Types };
extern const Il2CppType Attribute_t270895445_0_0_0;
static const Il2CppType* GenInst_Attribute_t270895445_0_0_0_Types[] = { &Attribute_t270895445_0_0_0 };
extern const Il2CppGenericInst GenInst_Attribute_t270895445_0_0_0 = { 1, GenInst_Attribute_t270895445_0_0_0_Types };
extern const Il2CppType XPathSorter_t36376808_0_0_0;
static const Il2CppType* GenInst_XPathSorter_t36376808_0_0_0_Types[] = { &XPathSorter_t36376808_0_0_0 };
extern const Il2CppGenericInst GenInst_XPathSorter_t36376808_0_0_0 = { 1, GenInst_XPathSorter_t36376808_0_0_0_Types };
extern const Il2CppType XsltContextInfo_t2905292101_0_0_0;
static const Il2CppType* GenInst_XsltContextInfo_t2905292101_0_0_0_Types[] = { &XsltContextInfo_t2905292101_0_0_0 };
extern const Il2CppGenericInst GenInst_XsltContextInfo_t2905292101_0_0_0 = { 1, GenInst_XsltContextInfo_t2905292101_0_0_0_Types };
extern const Il2CppType TypeCode_t2987224087_0_0_0;
static const Il2CppType* GenInst_TypeCode_t2987224087_0_0_0_Types[] = { &TypeCode_t2987224087_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeCode_t2987224087_0_0_0 = { 1, GenInst_TypeCode_t2987224087_0_0_0_Types };
extern const Il2CppType XmlSchemaException_t3511258692_0_0_0;
static const Il2CppType* GenInst_XmlSchemaException_t3511258692_0_0_0_Types[] = { &XmlSchemaException_t3511258692_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaException_t3511258692_0_0_0 = { 1, GenInst_XmlSchemaException_t3511258692_0_0_0_Types };
extern const Il2CppType SystemException_t176217640_0_0_0;
static const Il2CppType* GenInst_SystemException_t176217640_0_0_0_Types[] = { &SystemException_t176217640_0_0_0 };
extern const Il2CppGenericInst GenInst_SystemException_t176217640_0_0_0 = { 1, GenInst_SystemException_t176217640_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3041488559_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3041488559_0_0_0_Types[] = { &KeyValuePair_2_t3041488559_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3041488559_0_0_0 = { 1, GenInst_KeyValuePair_2_t3041488559_0_0_0_Types };
extern const Il2CppType DTDNode_t858560093_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_DTDNode_t858560093_0_0_0_Types[] = { &String_t_0_0_0, &DTDNode_t858560093_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_DTDNode_t858560093_0_0_0 = { 2, GenInst_String_t_0_0_0_DTDNode_t858560093_0_0_0_Types };
static const Il2CppType* GenInst_DTDNode_t858560093_0_0_0_Types[] = { &DTDNode_t858560093_0_0_0 };
extern const Il2CppGenericInst GenInst_DTDNode_t858560093_0_0_0 = { 1, GenInst_DTDNode_t858560093_0_0_0_Types };
extern const Il2CppType IXmlLineInfo_t2353988607_0_0_0;
static const Il2CppType* GenInst_IXmlLineInfo_t2353988607_0_0_0_Types[] = { &IXmlLineInfo_t2353988607_0_0_0 };
extern const Il2CppGenericInst GenInst_IXmlLineInfo_t2353988607_0_0_0 = { 1, GenInst_IXmlLineInfo_t2353988607_0_0_0_Types };
extern const Il2CppType AttributeSlot_t3985135163_0_0_0;
static const Il2CppType* GenInst_AttributeSlot_t3985135163_0_0_0_Types[] = { &AttributeSlot_t3985135163_0_0_0 };
extern const Il2CppGenericInst GenInst_AttributeSlot_t3985135163_0_0_0 = { 1, GenInst_AttributeSlot_t3985135163_0_0_0_Types };
extern const Il2CppType Entry_t3052280359_0_0_0;
static const Il2CppType* GenInst_Entry_t3052280359_0_0_0_Types[] = { &Entry_t3052280359_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t3052280359_0_0_0 = { 1, GenInst_Entry_t3052280359_0_0_0_Types };
extern const Il2CppType NsDecl_t3938094415_0_0_0;
static const Il2CppType* GenInst_NsDecl_t3938094415_0_0_0_Types[] = { &NsDecl_t3938094415_0_0_0 };
extern const Il2CppGenericInst GenInst_NsDecl_t3938094415_0_0_0 = { 1, GenInst_NsDecl_t3938094415_0_0_0_Types };
extern const Il2CppType NsScope_t3958624705_0_0_0;
static const Il2CppType* GenInst_NsScope_t3958624705_0_0_0_Types[] = { &NsScope_t3958624705_0_0_0 };
extern const Il2CppGenericInst GenInst_NsScope_t3958624705_0_0_0 = { 1, GenInst_NsScope_t3958624705_0_0_0_Types };
extern const Il2CppType XmlAttributeTokenInfo_t384315108_0_0_0;
static const Il2CppType* GenInst_XmlAttributeTokenInfo_t384315108_0_0_0_Types[] = { &XmlAttributeTokenInfo_t384315108_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlAttributeTokenInfo_t384315108_0_0_0 = { 1, GenInst_XmlAttributeTokenInfo_t384315108_0_0_0_Types };
extern const Il2CppType XmlTokenInfo_t2519673037_0_0_0;
static const Il2CppType* GenInst_XmlTokenInfo_t2519673037_0_0_0_Types[] = { &XmlTokenInfo_t2519673037_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlTokenInfo_t2519673037_0_0_0 = { 1, GenInst_XmlTokenInfo_t2519673037_0_0_0_Types };
extern const Il2CppType TagName_t2891256255_0_0_0;
static const Il2CppType* GenInst_TagName_t2891256255_0_0_0_Types[] = { &TagName_t2891256255_0_0_0 };
extern const Il2CppGenericInst GenInst_TagName_t2891256255_0_0_0 = { 1, GenInst_TagName_t2891256255_0_0_0_Types };
extern const Il2CppType XmlNodeInfo_t4030693883_0_0_0;
static const Il2CppType* GenInst_XmlNodeInfo_t4030693883_0_0_0_Types[] = { &XmlNodeInfo_t4030693883_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlNodeInfo_t4030693883_0_0_0 = { 1, GenInst_XmlNodeInfo_t4030693883_0_0_0_Types };
extern const Il2CppType XPathNavigator_t787956054_0_0_0;
static const Il2CppType* GenInst_XPathNavigator_t787956054_0_0_0_Types[] = { &XPathNavigator_t787956054_0_0_0 };
extern const Il2CppGenericInst GenInst_XPathNavigator_t787956054_0_0_0 = { 1, GenInst_XPathNavigator_t787956054_0_0_0_Types };
extern const Il2CppType IXmlNamespaceResolver_t535375154_0_0_0;
static const Il2CppType* GenInst_IXmlNamespaceResolver_t535375154_0_0_0_Types[] = { &IXmlNamespaceResolver_t535375154_0_0_0 };
extern const Il2CppGenericInst GenInst_IXmlNamespaceResolver_t535375154_0_0_0 = { 1, GenInst_IXmlNamespaceResolver_t535375154_0_0_0_Types };
extern const Il2CppType XPathItem_t4250588140_0_0_0;
static const Il2CppType* GenInst_XPathItem_t4250588140_0_0_0_Types[] = { &XPathItem_t4250588140_0_0_0 };
extern const Il2CppGenericInst GenInst_XPathItem_t4250588140_0_0_0 = { 1, GenInst_XPathItem_t4250588140_0_0_0_Types };
extern const Il2CppType XmlAttribute_t1173852259_0_0_0;
static const Il2CppType* GenInst_XmlAttribute_t1173852259_0_0_0_Types[] = { &XmlAttribute_t1173852259_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlAttribute_t1173852259_0_0_0 = { 1, GenInst_XmlAttribute_t1173852259_0_0_0_Types };
extern const Il2CppType XmlSchema_t3742557897_0_0_0;
static const Il2CppType* GenInst_XmlSchema_t3742557897_0_0_0_Types[] = { &XmlSchema_t3742557897_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchema_t3742557897_0_0_0 = { 1, GenInst_XmlSchema_t3742557897_0_0_0_Types };
extern const Il2CppType Regex_t3657309853_0_0_0;
static const Il2CppType* GenInst_Regex_t3657309853_0_0_0_Types[] = { &Regex_t3657309853_0_0_0 };
extern const Il2CppGenericInst GenInst_Regex_t3657309853_0_0_0 = { 1, GenInst_Regex_t3657309853_0_0_0_Types };
extern const Il2CppType XmlSchemaSimpleType_t2678868104_0_0_0;
static const Il2CppType* GenInst_XmlSchemaSimpleType_t2678868104_0_0_0_Types[] = { &XmlSchemaSimpleType_t2678868104_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaSimpleType_t2678868104_0_0_0 = { 1, GenInst_XmlSchemaSimpleType_t2678868104_0_0_0_Types };
extern const Il2CppType XmlSchemaType_t2033747345_0_0_0;
static const Il2CppType* GenInst_XmlSchemaType_t2033747345_0_0_0_Types[] = { &XmlSchemaType_t2033747345_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaType_t2033747345_0_0_0 = { 1, GenInst_XmlSchemaType_t2033747345_0_0_0_Types };
extern const Il2CppType EnumMapMember_t3516323045_0_0_0;
static const Il2CppType* GenInst_EnumMapMember_t3516323045_0_0_0_Types[] = { &EnumMapMember_t3516323045_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumMapMember_t3516323045_0_0_0 = { 1, GenInst_EnumMapMember_t3516323045_0_0_0_Types };
extern const Il2CppType CodeAttributeArgument_t2420111610_0_0_0;
static const Il2CppType* GenInst_CodeAttributeArgument_t2420111610_0_0_0_Types[] = { &CodeAttributeArgument_t2420111610_0_0_0 };
extern const Il2CppGenericInst GenInst_CodeAttributeArgument_t2420111610_0_0_0 = { 1, GenInst_CodeAttributeArgument_t2420111610_0_0_0_Types };
extern const Il2CppType CodeTypeReference_t3809997434_0_0_0;
static const Il2CppType* GenInst_CodeTypeReference_t3809997434_0_0_0_Types[] = { &CodeTypeReference_t3809997434_0_0_0 };
extern const Il2CppGenericInst GenInst_CodeTypeReference_t3809997434_0_0_0 = { 1, GenInst_CodeTypeReference_t3809997434_0_0_0_Types };
extern const Il2CppType CodeObject_t3927604602_0_0_0;
static const Il2CppType* GenInst_CodeObject_t3927604602_0_0_0_Types[] = { &CodeObject_t3927604602_0_0_0 };
extern const Il2CppGenericInst GenInst_CodeObject_t3927604602_0_0_0 = { 1, GenInst_CodeObject_t3927604602_0_0_0_Types };
extern const Il2CppType GenerationResult_t2469053731_0_0_0;
static const Il2CppType* GenInst_GenerationResult_t2469053731_0_0_0_Types[] = { &GenerationResult_t2469053731_0_0_0 };
extern const Il2CppGenericInst GenInst_GenerationResult_t2469053731_0_0_0 = { 1, GenInst_GenerationResult_t2469053731_0_0_0_Types };
extern const Il2CppType XmlMapping_t1653394_0_0_0;
static const Il2CppType* GenInst_XmlMapping_t1653394_0_0_0_Types[] = { &XmlMapping_t1653394_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlMapping_t1653394_0_0_0 = { 1, GenInst_XmlMapping_t1653394_0_0_0_Types };
extern const Il2CppType Hook_t924144422_0_0_0;
static const Il2CppType* GenInst_Hook_t924144422_0_0_0_Types[] = { &Hook_t924144422_0_0_0 };
extern const Il2CppGenericInst GenInst_Hook_t924144422_0_0_0 = { 1, GenInst_Hook_t924144422_0_0_0_Types };
extern const Il2CppType XmlReflectionMember_t1313238960_0_0_0;
static const Il2CppType* GenInst_XmlReflectionMember_t1313238960_0_0_0_Types[] = { &XmlReflectionMember_t1313238960_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlReflectionMember_t1313238960_0_0_0 = { 1, GenInst_XmlReflectionMember_t1313238960_0_0_0_Types };
extern const Il2CppType SoapSchemaMember_t859827560_0_0_0;
static const Il2CppType* GenInst_SoapSchemaMember_t859827560_0_0_0_Types[] = { &SoapSchemaMember_t859827560_0_0_0 };
extern const Il2CppGenericInst GenInst_SoapSchemaMember_t859827560_0_0_0 = { 1, GenInst_SoapSchemaMember_t859827560_0_0_0_Types };
extern const Il2CppType SoapIncludeAttribute_t666096636_0_0_0;
static const Il2CppType* GenInst_SoapIncludeAttribute_t666096636_0_0_0_Types[] = { &SoapIncludeAttribute_t666096636_0_0_0 };
extern const Il2CppGenericInst GenInst_SoapIncludeAttribute_t666096636_0_0_0 = { 1, GenInst_SoapIncludeAttribute_t666096636_0_0_0_Types };
extern const Il2CppType Attribute_t861562559_0_0_0;
static const Il2CppType* GenInst_Attribute_t861562559_0_0_0_Types[] = { &Attribute_t861562559_0_0_0 };
extern const Il2CppGenericInst GenInst_Attribute_t861562559_0_0_0 = { 1, GenInst_Attribute_t861562559_0_0_0_Types };
extern const Il2CppType _Attribute_t122494719_0_0_0;
static const Il2CppType* GenInst__Attribute_t122494719_0_0_0_Types[] = { &_Attribute_t122494719_0_0_0 };
extern const Il2CppGenericInst GenInst__Attribute_t122494719_0_0_0 = { 1, GenInst__Attribute_t122494719_0_0_0_Types };
extern const Il2CppType XmlMemberMapping_t113569223_0_0_0;
static const Il2CppType* GenInst_XmlMemberMapping_t113569223_0_0_0_Types[] = { &XmlMemberMapping_t113569223_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlMemberMapping_t113569223_0_0_0 = { 1, GenInst_XmlMemberMapping_t113569223_0_0_0_Types };
extern const Il2CppType XmlIncludeAttribute_t1446401819_0_0_0;
static const Il2CppType* GenInst_XmlIncludeAttribute_t1446401819_0_0_0_Types[] = { &XmlIncludeAttribute_t1446401819_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlIncludeAttribute_t1446401819_0_0_0 = { 1, GenInst_XmlIncludeAttribute_t1446401819_0_0_0_Types };
extern const Il2CppType XmlSerializer_t1117804635_0_0_0;
static const Il2CppType* GenInst_XmlSerializer_t1117804635_0_0_0_Types[] = { &XmlSerializer_t1117804635_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSerializer_t1117804635_0_0_0 = { 1, GenInst_XmlSerializer_t1117804635_0_0_0_Types };
extern const Il2CppType SerializerData_t3337767682_0_0_0;
static const Il2CppType* GenInst_SerializerData_t3337767682_0_0_0_Types[] = { &SerializerData_t3337767682_0_0_0 };
extern const Il2CppGenericInst GenInst_SerializerData_t3337767682_0_0_0 = { 1, GenInst_SerializerData_t3337767682_0_0_0_Types };
extern const Il2CppType XmlTypeMapMemberAttribute_t2452898273_0_0_0;
static const Il2CppType* GenInst_XmlTypeMapMemberAttribute_t2452898273_0_0_0_Types[] = { &XmlTypeMapMemberAttribute_t2452898273_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlTypeMapMemberAttribute_t2452898273_0_0_0 = { 1, GenInst_XmlTypeMapMemberAttribute_t2452898273_0_0_0_Types };
extern const Il2CppType XmlTypeMapMember_t3739392753_0_0_0;
static const Il2CppType* GenInst_XmlTypeMapMember_t3739392753_0_0_0_Types[] = { &XmlTypeMapMember_t3739392753_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlTypeMapMember_t3739392753_0_0_0 = { 1, GenInst_XmlTypeMapMember_t3739392753_0_0_0_Types };
extern const Il2CppType XmlTypeMapElementInfo_t2741990046_0_0_0;
static const Il2CppType* GenInst_XmlTypeMapElementInfo_t2741990046_0_0_0_Types[] = { &XmlTypeMapElementInfo_t2741990046_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlTypeMapElementInfo_t2741990046_0_0_0 = { 1, GenInst_XmlTypeMapElementInfo_t2741990046_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0 = { 2, GenInst_String_t_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4030379155_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4030379155_0_0_0_Types[] = { &KeyValuePair_2_t4030379155_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4030379155_0_0_0 = { 1, GenInst_KeyValuePair_2_t4030379155_0_0_0_Types };
extern const Il2CppType CodeExpression_t2166265795_0_0_0;
static const Il2CppType* GenInst_CodeExpression_t2166265795_0_0_0_Types[] = { &CodeExpression_t2166265795_0_0_0 };
extern const Il2CppGenericInst GenInst_CodeExpression_t2166265795_0_0_0 = { 1, GenInst_CodeExpression_t2166265795_0_0_0_Types };
extern const Il2CppType CodeStatement_t371410868_0_0_0;
static const Il2CppType* GenInst_CodeStatement_t371410868_0_0_0_Types[] = { &CodeStatement_t371410868_0_0_0 };
extern const Il2CppGenericInst GenInst_CodeStatement_t371410868_0_0_0 = { 1, GenInst_CodeStatement_t371410868_0_0_0_Types };
extern const Il2CppType CodeTypeMember_t1555525554_0_0_0;
static const Il2CppType* GenInst_CodeTypeMember_t1555525554_0_0_0_Types[] = { &CodeTypeMember_t1555525554_0_0_0 };
extern const Il2CppGenericInst GenInst_CodeTypeMember_t1555525554_0_0_0 = { 1, GenInst_CodeTypeMember_t1555525554_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_KeyValuePair_2_t4030379155_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &KeyValuePair_2_t4030379155_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_KeyValuePair_2_t4030379155_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_KeyValuePair_2_t4030379155_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType CompilerInfo_t947794800_0_0_0;
static const Il2CppType* GenInst_CompilerInfo_t947794800_0_0_0_Types[] = { &CompilerInfo_t947794800_0_0_0 };
extern const Il2CppGenericInst GenInst_CompilerInfo_t947794800_0_0_0 = { 1, GenInst_CompilerInfo_t947794800_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_CompilerInfo_t947794800_0_0_0_Types[] = { &String_t_0_0_0, &CompilerInfo_t947794800_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_CompilerInfo_t947794800_0_0_0 = { 2, GenInst_String_t_0_0_0_CompilerInfo_t947794800_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_CompilerInfo_t947794800_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &CompilerInfo_t947794800_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_CompilerInfo_t947794800_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_CompilerInfo_t947794800_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3130723266_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3130723266_0_0_0_Types[] = { &KeyValuePair_2_t3130723266_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3130723266_0_0_0 = { 1, GenInst_KeyValuePair_2_t3130723266_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_CompilerInfo_t947794800_0_0_0_KeyValuePair_2_t3130723266_0_0_0_Types[] = { &String_t_0_0_0, &CompilerInfo_t947794800_0_0_0, &KeyValuePair_2_t3130723266_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_CompilerInfo_t947794800_0_0_0_KeyValuePair_2_t3130723266_0_0_0 = { 3, GenInst_String_t_0_0_0_CompilerInfo_t947794800_0_0_0_KeyValuePair_2_t3130723266_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_CompilerInfo_t947794800_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &CompilerInfo_t947794800_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_CompilerInfo_t947794800_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_CompilerInfo_t947794800_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_CompilerInfo_t947794800_0_0_0_CompilerInfo_t947794800_0_0_0_Types[] = { &String_t_0_0_0, &CompilerInfo_t947794800_0_0_0, &CompilerInfo_t947794800_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_CompilerInfo_t947794800_0_0_0_CompilerInfo_t947794800_0_0_0 = { 3, GenInst_String_t_0_0_0_CompilerInfo_t947794800_0_0_0_CompilerInfo_t947794800_0_0_0_Types };
extern const Il2CppType Node_t2057664286_0_0_0;
static const Il2CppType* GenInst_Node_t2057664286_0_0_0_Types[] = { &Node_t2057664286_0_0_0 };
extern const Il2CppGenericInst GenInst_Node_t2057664286_0_0_0 = { 1, GenInst_Node_t2057664286_0_0_0_Types };
extern const Il2CppType PropertyDescriptor_t3244362832_0_0_0;
static const Il2CppType* GenInst_PropertyDescriptor_t3244362832_0_0_0_Types[] = { &PropertyDescriptor_t3244362832_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyDescriptor_t3244362832_0_0_0 = { 1, GenInst_PropertyDescriptor_t3244362832_0_0_0_Types };
extern const Il2CppType MemberDescriptor_t3815403747_0_0_0;
static const Il2CppType* GenInst_MemberDescriptor_t3815403747_0_0_0_Types[] = { &MemberDescriptor_t3815403747_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberDescriptor_t3815403747_0_0_0 = { 1, GenInst_MemberDescriptor_t3815403747_0_0_0_Types };
extern const Il2CppType Enum_t4135868527_0_0_0;
static const Il2CppType* GenInst_Enum_t4135868527_0_0_0_Types[] = { &Enum_t4135868527_0_0_0 };
extern const Il2CppGenericInst GenInst_Enum_t4135868527_0_0_0 = { 1, GenInst_Enum_t4135868527_0_0_0_Types };
extern const Il2CppType IFormattable_t1450744796_0_0_0;
static const Il2CppType* GenInst_IFormattable_t1450744796_0_0_0_Types[] = { &IFormattable_t1450744796_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormattable_t1450744796_0_0_0 = { 1, GenInst_IFormattable_t1450744796_0_0_0_Types };
extern const Il2CppType ValueType_t3640485471_0_0_0;
static const Il2CppType* GenInst_ValueType_t3640485471_0_0_0_Types[] = { &ValueType_t3640485471_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueType_t3640485471_0_0_0 = { 1, GenInst_ValueType_t3640485471_0_0_0_Types };
extern const Il2CppType AttributeU5BU5D_t1575011174_0_0_0;
static const Il2CppType* GenInst_AttributeU5BU5D_t1575011174_0_0_0_Types[] = { &AttributeU5BU5D_t1575011174_0_0_0 };
extern const Il2CppGenericInst GenInst_AttributeU5BU5D_t1575011174_0_0_0 = { 1, GenInst_AttributeU5BU5D_t1575011174_0_0_0_Types };
extern const Il2CppType IList_1_t2676882342_0_0_0;
static const Il2CppType* GenInst_IList_1_t2676882342_0_0_0_Types[] = { &IList_1_t2676882342_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t2676882342_0_0_0 = { 1, GenInst_IList_1_t2676882342_0_0_0_Types };
extern const Il2CppType ICollection_1_t3689714793_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t3689714793_0_0_0_Types[] = { &ICollection_1_t3689714793_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t3689714793_0_0_0 = { 1, GenInst_ICollection_1_t3689714793_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t4136382744_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t4136382744_0_0_0_Types[] = { &IEnumerable_1_t4136382744_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t4136382744_0_0_0 = { 1, GenInst_IEnumerable_1_t4136382744_0_0_0_Types };
extern const Il2CppType IList_1_t1937814502_0_0_0;
static const Il2CppType* GenInst_IList_1_t1937814502_0_0_0_Types[] = { &IList_1_t1937814502_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t1937814502_0_0_0 = { 1, GenInst_IList_1_t1937814502_0_0_0_Types };
extern const Il2CppType ICollection_1_t2950646953_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t2950646953_0_0_0_Types[] = { &ICollection_1_t2950646953_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t2950646953_0_0_0 = { 1, GenInst_ICollection_1_t2950646953_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t3397314904_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t3397314904_0_0_0_Types[] = { &IEnumerable_1_t3397314904_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3397314904_0_0_0 = { 1, GenInst_IEnumerable_1_t3397314904_0_0_0_Types };
extern const Il2CppType LinkedList_1_t2071723904_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_LinkedList_1_t2071723904_0_0_0_Types[] = { &Type_t_0_0_0, &LinkedList_1_t2071723904_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_LinkedList_1_t2071723904_0_0_0 = { 2, GenInst_Type_t_0_0_0_LinkedList_1_t2071723904_0_0_0_Types };
extern const Il2CppType TypeDescriptionProvider_t3232077895_0_0_0;
static const Il2CppType* GenInst_TypeDescriptionProvider_t3232077895_0_0_0_Types[] = { &TypeDescriptionProvider_t3232077895_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeDescriptionProvider_t3232077895_0_0_0 = { 1, GenInst_TypeDescriptionProvider_t3232077895_0_0_0_Types };
static const Il2CppType* GenInst_LinkedList_1_t2071723904_0_0_0_Types[] = { &LinkedList_1_t2071723904_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedList_1_t2071723904_0_0_0 = { 1, GenInst_LinkedList_1_t2071723904_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_LinkedList_1_t2071723904_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Type_t_0_0_0, &LinkedList_1_t2071723904_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_LinkedList_1_t2071723904_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Type_t_0_0_0_LinkedList_1_t2071723904_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2618775839_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2618775839_0_0_0_Types[] = { &KeyValuePair_2_t2618775839_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2618775839_0_0_0 = { 1, GenInst_KeyValuePair_2_t2618775839_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_LinkedList_1_t2071723904_0_0_0_KeyValuePair_2_t2618775839_0_0_0_Types[] = { &Type_t_0_0_0, &LinkedList_1_t2071723904_0_0_0, &KeyValuePair_2_t2618775839_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_LinkedList_1_t2071723904_0_0_0_KeyValuePair_2_t2618775839_0_0_0 = { 3, GenInst_Type_t_0_0_0_LinkedList_1_t2071723904_0_0_0_KeyValuePair_2_t2618775839_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_LinkedList_1_t2071723904_0_0_0_Type_t_0_0_0_Types[] = { &Type_t_0_0_0, &LinkedList_1_t2071723904_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_LinkedList_1_t2071723904_0_0_0_Type_t_0_0_0 = { 3, GenInst_Type_t_0_0_0_LinkedList_1_t2071723904_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_LinkedList_1_t2071723904_0_0_0_LinkedList_1_t2071723904_0_0_0_Types[] = { &Type_t_0_0_0, &LinkedList_1_t2071723904_0_0_0, &LinkedList_1_t2071723904_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_LinkedList_1_t2071723904_0_0_0_LinkedList_1_t2071723904_0_0_0 = { 3, GenInst_Type_t_0_0_0_LinkedList_1_t2071723904_0_0_0_LinkedList_1_t2071723904_0_0_0_Types };
extern const Il2CppType WeakObjectWrapper_t827463650_0_0_0;
static const Il2CppType* GenInst_WeakObjectWrapper_t827463650_0_0_0_LinkedList_1_t2071723904_0_0_0_Types[] = { &WeakObjectWrapper_t827463650_0_0_0, &LinkedList_1_t2071723904_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakObjectWrapper_t827463650_0_0_0_LinkedList_1_t2071723904_0_0_0 = { 2, GenInst_WeakObjectWrapper_t827463650_0_0_0_LinkedList_1_t2071723904_0_0_0_Types };
static const Il2CppType* GenInst_WeakObjectWrapper_t827463650_0_0_0_Types[] = { &WeakObjectWrapper_t827463650_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakObjectWrapper_t827463650_0_0_0 = { 1, GenInst_WeakObjectWrapper_t827463650_0_0_0_Types };
static const Il2CppType* GenInst_WeakObjectWrapper_t827463650_0_0_0_LinkedList_1_t2071723904_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &WeakObjectWrapper_t827463650_0_0_0, &LinkedList_1_t2071723904_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakObjectWrapper_t827463650_0_0_0_LinkedList_1_t2071723904_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_WeakObjectWrapper_t827463650_0_0_0_LinkedList_1_t2071723904_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3762142349_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3762142349_0_0_0_Types[] = { &KeyValuePair_2_t3762142349_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3762142349_0_0_0 = { 1, GenInst_KeyValuePair_2_t3762142349_0_0_0_Types };
static const Il2CppType* GenInst_WeakObjectWrapper_t827463650_0_0_0_LinkedList_1_t2071723904_0_0_0_KeyValuePair_2_t3762142349_0_0_0_Types[] = { &WeakObjectWrapper_t827463650_0_0_0, &LinkedList_1_t2071723904_0_0_0, &KeyValuePair_2_t3762142349_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakObjectWrapper_t827463650_0_0_0_LinkedList_1_t2071723904_0_0_0_KeyValuePair_2_t3762142349_0_0_0 = { 3, GenInst_WeakObjectWrapper_t827463650_0_0_0_LinkedList_1_t2071723904_0_0_0_KeyValuePair_2_t3762142349_0_0_0_Types };
static const Il2CppType* GenInst_WeakObjectWrapper_t827463650_0_0_0_LinkedList_1_t2071723904_0_0_0_WeakObjectWrapper_t827463650_0_0_0_Types[] = { &WeakObjectWrapper_t827463650_0_0_0, &LinkedList_1_t2071723904_0_0_0, &WeakObjectWrapper_t827463650_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakObjectWrapper_t827463650_0_0_0_LinkedList_1_t2071723904_0_0_0_WeakObjectWrapper_t827463650_0_0_0 = { 3, GenInst_WeakObjectWrapper_t827463650_0_0_0_LinkedList_1_t2071723904_0_0_0_WeakObjectWrapper_t827463650_0_0_0_Types };
static const Il2CppType* GenInst_WeakObjectWrapper_t827463650_0_0_0_LinkedList_1_t2071723904_0_0_0_LinkedList_1_t2071723904_0_0_0_Types[] = { &WeakObjectWrapper_t827463650_0_0_0, &LinkedList_1_t2071723904_0_0_0, &LinkedList_1_t2071723904_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakObjectWrapper_t827463650_0_0_0_LinkedList_1_t2071723904_0_0_0_LinkedList_1_t2071723904_0_0_0 = { 3, GenInst_WeakObjectWrapper_t827463650_0_0_0_LinkedList_1_t2071723904_0_0_0_LinkedList_1_t2071723904_0_0_0_Types };
extern const Il2CppType Cookie_t993873397_0_0_0;
static const Il2CppType* GenInst_Cookie_t993873397_0_0_0_Types[] = { &Cookie_t993873397_0_0_0 };
extern const Il2CppGenericInst GenInst_Cookie_t993873397_0_0_0 = { 1, GenInst_Cookie_t993873397_0_0_0_Types };
extern const Il2CppType IPAddress_t241777590_0_0_0;
static const Il2CppType* GenInst_IPAddress_t241777590_0_0_0_Types[] = { &IPAddress_t241777590_0_0_0 };
extern const Il2CppGenericInst GenInst_IPAddress_t241777590_0_0_0 = { 1, GenInst_IPAddress_t241777590_0_0_0_Types };
extern const Il2CppType X509ChainStatus_t133602714_0_0_0;
static const Il2CppType* GenInst_X509ChainStatus_t133602714_0_0_0_Types[] = { &X509ChainStatus_t133602714_0_0_0 };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t133602714_0_0_0 = { 1, GenInst_X509ChainStatus_t133602714_0_0_0_Types };
extern const Il2CppType ArraySegment_1_t283560987_0_0_0;
static const Il2CppType* GenInst_ArraySegment_1_t283560987_0_0_0_Types[] = { &ArraySegment_1_t283560987_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySegment_1_t283560987_0_0_0 = { 1, GenInst_ArraySegment_1_t283560987_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3842366416_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3842366416_0_0_0_Types[] = { &KeyValuePair_2_t3842366416_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3842366416_0_0_0 = { 1, GenInst_KeyValuePair_2_t3842366416_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t97287965_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t97287965_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t97287965_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_KeyValuePair_2_t3842366416_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t97287965_0_0_0, &KeyValuePair_2_t3842366416_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_KeyValuePair_2_t3842366416_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_KeyValuePair_2_t3842366416_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t97287965_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2280216431_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2280216431_0_0_0_Types[] = { &KeyValuePair_2_t2280216431_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2280216431_0_0_0 = { 1, GenInst_KeyValuePair_2_t2280216431_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_KeyValuePair_2_t2280216431_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t97287965_0_0_0, &KeyValuePair_2_t2280216431_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_KeyValuePair_2_t2280216431_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_KeyValuePair_2_t2280216431_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t97287965_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t97287965_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0_Types };
extern const Il2CppType Capture_t2232016050_0_0_0;
static const Il2CppType* GenInst_Capture_t2232016050_0_0_0_Types[] = { &Capture_t2232016050_0_0_0 };
extern const Il2CppGenericInst GenInst_Capture_t2232016050_0_0_0 = { 1, GenInst_Capture_t2232016050_0_0_0_Types };
extern const Il2CppType DynamicMethod_t2537779570_0_0_0;
static const Il2CppType* GenInst_DynamicMethod_t2537779570_0_0_0_Types[] = { &DynamicMethod_t2537779570_0_0_0 };
extern const Il2CppGenericInst GenInst_DynamicMethod_t2537779570_0_0_0 = { 1, GenInst_DynamicMethod_t2537779570_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4237331251_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4237331251_0_0_0_Types[] = { &KeyValuePair_2_t4237331251_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4237331251_0_0_0 = { 1, GenInst_KeyValuePair_2_t4237331251_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Int32_t2950945753_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Int32_t2950945753_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t4237331251_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Int32_t2950945753_0_0_0, &KeyValuePair_2_t4237331251_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t4237331251_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t4237331251_0_0_0_Types };
extern const Il2CppType Label_t2281661643_0_0_0;
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Label_t2281661643_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Label_t2281661643_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Label_t2281661643_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_Label_t2281661643_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3568047141_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3568047141_0_0_0_Types[] = { &KeyValuePair_2_t3568047141_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3568047141_0_0_0 = { 1, GenInst_KeyValuePair_2_t3568047141_0_0_0_Types };
static const Il2CppType* GenInst_Label_t2281661643_0_0_0_Types[] = { &Label_t2281661643_0_0_0 };
extern const Il2CppGenericInst GenInst_Label_t2281661643_0_0_0 = { 1, GenInst_Label_t2281661643_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Label_t2281661643_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Label_t2281661643_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Label_t2281661643_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Label_t2281661643_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Label_t2281661643_0_0_0_Label_t2281661643_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Label_t2281661643_0_0_0, &Label_t2281661643_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Label_t2281661643_0_0_0_Label_t2281661643_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Label_t2281661643_0_0_0_Label_t2281661643_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Label_t2281661643_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Label_t2281661643_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Label_t2281661643_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Label_t2281661643_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Label_t2281661643_0_0_0_KeyValuePair_2_t3568047141_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Label_t2281661643_0_0_0, &KeyValuePair_2_t3568047141_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Label_t2281661643_0_0_0_KeyValuePair_2_t3568047141_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Label_t2281661643_0_0_0_KeyValuePair_2_t3568047141_0_0_0_Types };
extern const Il2CppType Group_t2468205786_0_0_0;
static const Il2CppType* GenInst_Group_t2468205786_0_0_0_Types[] = { &Group_t2468205786_0_0_0 };
extern const Il2CppGenericInst GenInst_Group_t2468205786_0_0_0 = { 1, GenInst_Group_t2468205786_0_0_0_Types };
extern const Il2CppType Mark_t3471605523_0_0_0;
static const Il2CppType* GenInst_Mark_t3471605523_0_0_0_Types[] = { &Mark_t3471605523_0_0_0 };
extern const Il2CppGenericInst GenInst_Mark_t3471605523_0_0_0 = { 1, GenInst_Mark_t3471605523_0_0_0_Types };
extern const Il2CppType UriScheme_t722425697_0_0_0;
static const Il2CppType* GenInst_UriScheme_t722425697_0_0_0_Types[] = { &UriScheme_t722425697_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t722425697_0_0_0 = { 1, GenInst_UriScheme_t722425697_0_0_0_Types };
extern const Il2CppType IEnlistmentNotification_t276083705_0_0_0;
static const Il2CppType* GenInst_IEnlistmentNotification_t276083705_0_0_0_Types[] = { &IEnlistmentNotification_t276083705_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnlistmentNotification_t276083705_0_0_0 = { 1, GenInst_IEnlistmentNotification_t276083705_0_0_0_Types };
extern const Il2CppType ISinglePhaseNotification_t3269684407_0_0_0;
static const Il2CppType* GenInst_ISinglePhaseNotification_t3269684407_0_0_0_Types[] = { &ISinglePhaseNotification_t3269684407_0_0_0 };
extern const Il2CppGenericInst GenInst_ISinglePhaseNotification_t3269684407_0_0_0 = { 1, GenInst_ISinglePhaseNotification_t3269684407_0_0_0_Types };
extern const Il2CppType SignalHandler_t1590986791_0_0_0;
static const Il2CppType* GenInst_SignalHandler_t1590986791_0_0_0_Types[] = { &SignalHandler_t1590986791_0_0_0 };
extern const Il2CppGenericInst GenInst_SignalHandler_t1590986791_0_0_0 = { 1, GenInst_SignalHandler_t1590986791_0_0_0_Types };
extern const Il2CppType MulticastDelegate_t157516450_0_0_0;
static const Il2CppType* GenInst_MulticastDelegate_t157516450_0_0_0_Types[] = { &MulticastDelegate_t157516450_0_0_0 };
extern const Il2CppGenericInst GenInst_MulticastDelegate_t157516450_0_0_0 = { 1, GenInst_MulticastDelegate_t157516450_0_0_0_Types };
extern const Il2CppType Link_t3209266973_0_0_0;
static const Il2CppType* GenInst_Link_t3209266973_0_0_0_Types[] = { &Link_t3209266973_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t3209266973_0_0_0 = { 1, GenInst_Link_t3209266973_0_0_0_Types };
extern const Il2CppType LockDetails_t2068764019_0_0_0;
static const Il2CppType* GenInst_LockDetails_t2068764019_0_0_0_Types[] = { &LockDetails_t2068764019_0_0_0 };
extern const Il2CppGenericInst GenInst_LockDetails_t2068764019_0_0_0 = { 1, GenInst_LockDetails_t2068764019_0_0_0_Types };
static const Il2CppType* GenInst_ByteU5BU5D_t4116647657_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &ByteU5BU5D_t4116647657_0_0_0, &Int32_t2950945753_0_0_0, &Int32_t2950945753_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t4116647657_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0 = { 4, GenInst_ByteU5BU5D_t4116647657_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2950945753_0_0_0, &Int32_t2950945753_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0 = { 4, GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_ByteU5BU5D_t4116647657_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &ByteU5BU5D_t4116647657_0_0_0, &Int32_t2950945753_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t4116647657_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_ByteU5BU5D_t4116647657_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 4, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType FontFamily_t13778311_0_0_0;
static const Il2CppType* GenInst_FontFamily_t13778311_0_0_0_Types[] = { &FontFamily_t13778311_0_0_0 };
extern const Il2CppGenericInst GenInst_FontFamily_t13778311_0_0_0 = { 1, GenInst_FontFamily_t13778311_0_0_0_Types };
extern const Il2CppType IconImage_t1835934191_0_0_0;
static const Il2CppType* GenInst_IconImage_t1835934191_0_0_0_Types[] = { &IconImage_t1835934191_0_0_0 };
extern const Il2CppGenericInst GenInst_IconImage_t1835934191_0_0_0 = { 1, GenInst_IconImage_t1835934191_0_0_0_Types };
extern const Il2CppType IconDirEntry_t3003987536_0_0_0;
static const Il2CppType* GenInst_IconDirEntry_t3003987536_0_0_0_Types[] = { &IconDirEntry_t3003987536_0_0_0 };
extern const Il2CppGenericInst GenInst_IconDirEntry_t3003987536_0_0_0 = { 1, GenInst_IconDirEntry_t3003987536_0_0_0_Types };
extern const Il2CppType Color_t1869934208_0_0_0;
static const Il2CppType* GenInst_Color_t1869934208_0_0_0_Types[] = { &Color_t1869934208_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t1869934208_0_0_0 = { 1, GenInst_Color_t1869934208_0_0_0_Types };
extern const Il2CppType ImageCodecInfo_t2037253290_0_0_0;
static const Il2CppType* GenInst_ImageCodecInfo_t2037253290_0_0_0_Types[] = { &ImageCodecInfo_t2037253290_0_0_0 };
extern const Il2CppGenericInst GenInst_ImageCodecInfo_t2037253290_0_0_0 = { 1, GenInst_ImageCodecInfo_t2037253290_0_0_0_Types };
extern const Il2CppType ImageFormat_t730671025_0_0_0;
static const Il2CppType* GenInst_ImageFormat_t730671025_0_0_0_Types[] = { &ImageFormat_t730671025_0_0_0 };
extern const Il2CppGenericInst GenInst_ImageFormat_t730671025_0_0_0 = { 1, GenInst_ImageFormat_t730671025_0_0_0_Types };
extern const Il2CppType EncoderParameter_t138078747_0_0_0;
static const Il2CppType* GenInst_EncoderParameter_t138078747_0_0_0_Types[] = { &EncoderParameter_t138078747_0_0_0 };
extern const Il2CppGenericInst GenInst_EncoderParameter_t138078747_0_0_0 = { 1, GenInst_EncoderParameter_t138078747_0_0_0_Types };
extern const Il2CppType SettingsMappingWhat_t1462987973_0_0_0;
static const Il2CppType* GenInst_SettingsMappingWhat_t1462987973_0_0_0_Types[] = { &SettingsMappingWhat_t1462987973_0_0_0 };
extern const Il2CppGenericInst GenInst_SettingsMappingWhat_t1462987973_0_0_0 = { 1, GenInst_SettingsMappingWhat_t1462987973_0_0_0_Types };
extern const Il2CppType SettingsMapping_t162400643_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_SettingsMapping_t162400643_0_0_0_Types[] = { &Type_t_0_0_0, &SettingsMapping_t162400643_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_SettingsMapping_t162400643_0_0_0 = { 2, GenInst_Type_t_0_0_0_SettingsMapping_t162400643_0_0_0_Types };
static const Il2CppType* GenInst_SettingsMapping_t162400643_0_0_0_Types[] = { &SettingsMapping_t162400643_0_0_0 };
extern const Il2CppGenericInst GenInst_SettingsMapping_t162400643_0_0_0 = { 1, GenInst_SettingsMapping_t162400643_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_SettingsMapping_t162400643_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Type_t_0_0_0, &SettingsMapping_t162400643_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_SettingsMapping_t162400643_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Type_t_0_0_0_SettingsMapping_t162400643_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t709452578_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t709452578_0_0_0_Types[] = { &KeyValuePair_2_t709452578_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t709452578_0_0_0 = { 1, GenInst_KeyValuePair_2_t709452578_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_SettingsMapping_t162400643_0_0_0_KeyValuePair_2_t709452578_0_0_0_Types[] = { &Type_t_0_0_0, &SettingsMapping_t162400643_0_0_0, &KeyValuePair_2_t709452578_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_SettingsMapping_t162400643_0_0_0_KeyValuePair_2_t709452578_0_0_0 = { 3, GenInst_Type_t_0_0_0_SettingsMapping_t162400643_0_0_0_KeyValuePair_2_t709452578_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_SettingsMapping_t162400643_0_0_0_Type_t_0_0_0_Types[] = { &Type_t_0_0_0, &SettingsMapping_t162400643_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_SettingsMapping_t162400643_0_0_0_Type_t_0_0_0 = { 3, GenInst_Type_t_0_0_0_SettingsMapping_t162400643_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_SettingsMapping_t162400643_0_0_0_SettingsMapping_t162400643_0_0_0_Types[] = { &Type_t_0_0_0, &SettingsMapping_t162400643_0_0_0, &SettingsMapping_t162400643_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_SettingsMapping_t162400643_0_0_0_SettingsMapping_t162400643_0_0_0 = { 3, GenInst_Type_t_0_0_0_SettingsMapping_t162400643_0_0_0_SettingsMapping_t162400643_0_0_0_Types };
extern const Il2CppType SettingsMappingWhatContents_t3035976441_0_0_0;
static const Il2CppType* GenInst_SettingsMappingWhatContents_t3035976441_0_0_0_Types[] = { &SettingsMappingWhatContents_t3035976441_0_0_0 };
extern const Il2CppGenericInst GenInst_SettingsMappingWhatContents_t3035976441_0_0_0 = { 1, GenInst_SettingsMappingWhatContents_t3035976441_0_0_0_Types };
extern const Il2CppType CodeParameterDeclarationExpression_t649585606_0_0_0;
static const Il2CppType* GenInst_CodeParameterDeclarationExpression_t649585606_0_0_0_Types[] = { &CodeParameterDeclarationExpression_t649585606_0_0_0 };
extern const Il2CppGenericInst GenInst_CodeParameterDeclarationExpression_t649585606_0_0_0 = { 1, GenInst_CodeParameterDeclarationExpression_t649585606_0_0_0_Types };
extern const Il2CppType SerializationMap_t4088073813_0_0_0;
static const Il2CppType* GenInst_SerializationMap_t4088073813_0_0_0_Types[] = { &SerializationMap_t4088073813_0_0_0 };
extern const Il2CppGenericInst GenInst_SerializationMap_t4088073813_0_0_0 = { 1, GenInst_SerializationMap_t4088073813_0_0_0_Types };
extern const Il2CppType DataMemberInfo_t700542586_0_0_0;
static const Il2CppType* GenInst_DataMemberInfo_t700542586_0_0_0_Types[] = { &DataMemberInfo_t700542586_0_0_0 };
extern const Il2CppGenericInst GenInst_DataMemberInfo_t700542586_0_0_0 = { 1, GenInst_DataMemberInfo_t700542586_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_XmlQualifiedName_t2760654312_0_0_0_Types[] = { &Type_t_0_0_0, &XmlQualifiedName_t2760654312_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_XmlQualifiedName_t2760654312_0_0_0 = { 2, GenInst_Type_t_0_0_0_XmlQualifiedName_t2760654312_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_XmlQualifiedName_t2760654312_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Type_t_0_0_0, &XmlQualifiedName_t2760654312_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_XmlQualifiedName_t2760654312_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Type_t_0_0_0_XmlQualifiedName_t2760654312_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3307706247_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3307706247_0_0_0_Types[] = { &KeyValuePair_2_t3307706247_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3307706247_0_0_0 = { 1, GenInst_KeyValuePair_2_t3307706247_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_XmlQualifiedName_t2760654312_0_0_0_KeyValuePair_2_t3307706247_0_0_0_Types[] = { &Type_t_0_0_0, &XmlQualifiedName_t2760654312_0_0_0, &KeyValuePair_2_t3307706247_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_XmlQualifiedName_t2760654312_0_0_0_KeyValuePair_2_t3307706247_0_0_0 = { 3, GenInst_Type_t_0_0_0_XmlQualifiedName_t2760654312_0_0_0_KeyValuePair_2_t3307706247_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_XmlQualifiedName_t2760654312_0_0_0_Type_t_0_0_0_Types[] = { &Type_t_0_0_0, &XmlQualifiedName_t2760654312_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_XmlQualifiedName_t2760654312_0_0_0_Type_t_0_0_0 = { 3, GenInst_Type_t_0_0_0_XmlQualifiedName_t2760654312_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_XmlQualifiedName_t2760654312_0_0_0_XmlQualifiedName_t2760654312_0_0_0_Types[] = { &Type_t_0_0_0, &XmlQualifiedName_t2760654312_0_0_0, &XmlQualifiedName_t2760654312_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_XmlQualifiedName_t2760654312_0_0_0_XmlQualifiedName_t2760654312_0_0_0 = { 3, GenInst_Type_t_0_0_0_XmlQualifiedName_t2760654312_0_0_0_XmlQualifiedName_t2760654312_0_0_0_Types };
extern const Il2CppType EnumMemberInfo_t3072296154_0_0_0;
static const Il2CppType* GenInst_EnumMemberInfo_t3072296154_0_0_0_Types[] = { &EnumMemberInfo_t3072296154_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumMemberInfo_t3072296154_0_0_0 = { 1, GenInst_EnumMemberInfo_t3072296154_0_0_0_Types };
extern const Il2CppType AttrNodeInfo_t1586542217_0_0_0;
static const Il2CppType* GenInst_AttrNodeInfo_t1586542217_0_0_0_Types[] = { &AttrNodeInfo_t1586542217_0_0_0 };
extern const Il2CppGenericInst GenInst_AttrNodeInfo_t1586542217_0_0_0 = { 1, GenInst_AttrNodeInfo_t1586542217_0_0_0_Types };
extern const Il2CppType NodeInfo_t2956564772_0_0_0;
static const Il2CppType* GenInst_NodeInfo_t2956564772_0_0_0_Types[] = { &NodeInfo_t2956564772_0_0_0 };
extern const Il2CppGenericInst GenInst_NodeInfo_t2956564772_0_0_0 = { 1, GenInst_NodeInfo_t2956564772_0_0_0_Types };
extern const Il2CppType XmlDictionaryString_t3504120266_0_0_0;
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_XmlDictionaryString_t3504120266_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &XmlDictionaryString_t3504120266_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_XmlDictionaryString_t3504120266_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_XmlDictionaryString_t3504120266_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t71524366_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t71524366_0_0_0_Types[] = { &KeyValuePair_2_t71524366_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t71524366_0_0_0 = { 1, GenInst_KeyValuePair_2_t71524366_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Il2CppObject_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t71524366_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t71524366_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t71524366_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t71524366_0_0_0_Types };
static const Il2CppType* GenInst_XmlDictionaryString_t3504120266_0_0_0_Types[] = { &XmlDictionaryString_t3504120266_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlDictionaryString_t3504120266_0_0_0 = { 1, GenInst_XmlDictionaryString_t3504120266_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_XmlDictionaryString_t3504120266_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &XmlDictionaryString_t3504120266_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_XmlDictionaryString_t3504120266_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_XmlDictionaryString_t3504120266_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t495538468_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t495538468_0_0_0_Types[] = { &KeyValuePair_2_t495538468_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t495538468_0_0_0 = { 1, GenInst_KeyValuePair_2_t495538468_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_XmlDictionaryString_t3504120266_0_0_0_KeyValuePair_2_t495538468_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &XmlDictionaryString_t3504120266_0_0_0, &KeyValuePair_2_t495538468_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_XmlDictionaryString_t3504120266_0_0_0_KeyValuePair_2_t495538468_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_XmlDictionaryString_t3504120266_0_0_0_KeyValuePair_2_t495538468_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_XmlDictionaryString_t3504120266_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &XmlDictionaryString_t3504120266_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_XmlDictionaryString_t3504120266_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_XmlDictionaryString_t3504120266_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_XmlDictionaryString_t3504120266_0_0_0_XmlDictionaryString_t3504120266_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &XmlDictionaryString_t3504120266_0_0_0, &XmlDictionaryString_t3504120266_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_XmlDictionaryString_t3504120266_0_0_0_XmlDictionaryString_t3504120266_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_XmlDictionaryString_t3504120266_0_0_0_XmlDictionaryString_t3504120266_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t968067334_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t968067334_0_0_0_Types[] = { &KeyValuePair_2_t968067334_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t968067334_0_0_0 = { 1, GenInst_KeyValuePair_2_t968067334_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Il2CppObject_0_0_0_Types[] = { &String_t_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_String_t_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType XmlSpace_t3324193251_0_0_0;
static const Il2CppType* GenInst_XmlSpace_t3324193251_0_0_0_Types[] = { &XmlSpace_t3324193251_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSpace_t3324193251_0_0_0 = { 1, GenInst_XmlSpace_t3324193251_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t968067334_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &KeyValuePair_2_t968067334_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t968067334_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_KeyValuePair_2_t968067334_0_0_0_Boolean_t97287965_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2530217319_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &KeyValuePair_2_t2530217319_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2530217319_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_KeyValuePair_2_t2530217319_0_0_0_Boolean_t97287965_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_XmlDictionaryString_t3504120266_0_0_0_Types[] = { &String_t_0_0_0, &XmlDictionaryString_t3504120266_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XmlDictionaryString_t3504120266_0_0_0 = { 2, GenInst_String_t_0_0_0_XmlDictionaryString_t3504120266_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_XmlDictionaryString_t3504120266_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &XmlDictionaryString_t3504120266_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XmlDictionaryString_t3504120266_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_XmlDictionaryString_t3504120266_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1392081436_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1392081436_0_0_0_Types[] = { &KeyValuePair_2_t1392081436_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1392081436_0_0_0 = { 1, GenInst_KeyValuePair_2_t1392081436_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_XmlDictionaryString_t3504120266_0_0_0_KeyValuePair_2_t1392081436_0_0_0_Types[] = { &String_t_0_0_0, &XmlDictionaryString_t3504120266_0_0_0, &KeyValuePair_2_t1392081436_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XmlDictionaryString_t3504120266_0_0_0_KeyValuePair_2_t1392081436_0_0_0 = { 3, GenInst_String_t_0_0_0_XmlDictionaryString_t3504120266_0_0_0_KeyValuePair_2_t1392081436_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_XmlDictionaryString_t3504120266_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &XmlDictionaryString_t3504120266_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XmlDictionaryString_t3504120266_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_XmlDictionaryString_t3504120266_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_XmlDictionaryString_t3504120266_0_0_0_XmlDictionaryString_t3504120266_0_0_0_Types[] = { &String_t_0_0_0, &XmlDictionaryString_t3504120266_0_0_0, &XmlDictionaryString_t3504120266_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XmlDictionaryString_t3504120266_0_0_0_XmlDictionaryString_t3504120266_0_0_0 = { 3, GenInst_String_t_0_0_0_XmlDictionaryString_t3504120266_0_0_0_XmlDictionaryString_t3504120266_0_0_0_Types };
extern const Il2CppType Encoding_t1523322056_0_0_0;
static const Il2CppType* GenInst_Encoding_t1523322056_0_0_0_Types[] = { &Encoding_t1523322056_0_0_0 };
extern const Il2CppGenericInst GenInst_Encoding_t1523322056_0_0_0 = { 1, GenInst_Encoding_t1523322056_0_0_0_Types };
extern const Il2CppType MimeEncodedStream_t622371820_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_MimeEncodedStream_t622371820_0_0_0_Types[] = { &String_t_0_0_0, &MimeEncodedStream_t622371820_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MimeEncodedStream_t622371820_0_0_0 = { 2, GenInst_String_t_0_0_0_MimeEncodedStream_t622371820_0_0_0_Types };
static const Il2CppType* GenInst_MimeEncodedStream_t622371820_0_0_0_Types[] = { &MimeEncodedStream_t622371820_0_0_0 };
extern const Il2CppGenericInst GenInst_MimeEncodedStream_t622371820_0_0_0 = { 1, GenInst_MimeEncodedStream_t622371820_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_MimeEncodedStream_t622371820_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &MimeEncodedStream_t622371820_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MimeEncodedStream_t622371820_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_MimeEncodedStream_t622371820_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2805300286_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2805300286_0_0_0_Types[] = { &KeyValuePair_2_t2805300286_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2805300286_0_0_0 = { 1, GenInst_KeyValuePair_2_t2805300286_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_MimeEncodedStream_t622371820_0_0_0_KeyValuePair_2_t2805300286_0_0_0_Types[] = { &String_t_0_0_0, &MimeEncodedStream_t622371820_0_0_0, &KeyValuePair_2_t2805300286_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MimeEncodedStream_t622371820_0_0_0_KeyValuePair_2_t2805300286_0_0_0 = { 3, GenInst_String_t_0_0_0_MimeEncodedStream_t622371820_0_0_0_KeyValuePair_2_t2805300286_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_MimeEncodedStream_t622371820_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &MimeEncodedStream_t622371820_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MimeEncodedStream_t622371820_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_MimeEncodedStream_t622371820_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_MimeEncodedStream_t622371820_0_0_0_MimeEncodedStream_t622371820_0_0_0_Types[] = { &String_t_0_0_0, &MimeEncodedStream_t622371820_0_0_0, &MimeEncodedStream_t622371820_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MimeEncodedStream_t622371820_0_0_0_MimeEncodedStream_t622371820_0_0_0 = { 3, GenInst_String_t_0_0_0_MimeEncodedStream_t622371820_0_0_0_MimeEncodedStream_t622371820_0_0_0_Types };
extern const Il2CppType AssemblyName_t270931938_0_0_0;
static const Il2CppType* GenInst_AssemblyName_t270931938_0_0_0_Types[] = { &AssemblyName_t270931938_0_0_0 };
extern const Il2CppGenericInst GenInst_AssemblyName_t270931938_0_0_0 = { 1, GenInst_AssemblyName_t270931938_0_0_0_Types };
extern const Il2CppType _AssemblyName_t3550739211_0_0_0;
static const Il2CppType* GenInst__AssemblyName_t3550739211_0_0_0_Types[] = { &_AssemblyName_t3550739211_0_0_0 };
extern const Il2CppGenericInst GenInst__AssemblyName_t3550739211_0_0_0 = { 1, GenInst__AssemblyName_t3550739211_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Type_t_0_0_0_Types[] = { &Type_t_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Type_t_0_0_0 = { 2, GenInst_Type_t_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Type_t_0_0_0, &Type_t_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Type_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3030996695_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3030996695_0_0_0_Types[] = { &KeyValuePair_2_t3030996695_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3030996695_0_0_0 = { 1, GenInst_KeyValuePair_2_t3030996695_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Type_t_0_0_0_KeyValuePair_2_t3030996695_0_0_0_Types[] = { &Type_t_0_0_0, &Type_t_0_0_0, &KeyValuePair_2_t3030996695_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Type_t_0_0_0_KeyValuePair_2_t3030996695_0_0_0 = { 3, GenInst_Type_t_0_0_0_Type_t_0_0_0_KeyValuePair_2_t3030996695_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Type_t_0_0_0_Type_t_0_0_0_Types[] = { &Type_t_0_0_0, &Type_t_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Type_t_0_0_0_Type_t_0_0_0 = { 3, GenInst_Type_t_0_0_0_Type_t_0_0_0_Type_t_0_0_0_Types };
extern const Il2CppType Node_t2365075726_0_0_0;
static const Il2CppType* GenInst_Node_t2365075726_0_0_0_Types[] = { &Node_t2365075726_0_0_0 };
extern const Il2CppGenericInst GenInst_Node_t2365075726_0_0_0 = { 1, GenInst_Node_t2365075726_0_0_0_Types };
extern const Il2CppType File_t3103643102_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_File_t3103643102_0_0_0_Types[] = { &String_t_0_0_0, &File_t3103643102_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_File_t3103643102_0_0_0 = { 2, GenInst_String_t_0_0_0_File_t3103643102_0_0_0_Types };
static const Il2CppType* GenInst_File_t3103643102_0_0_0_Types[] = { &File_t3103643102_0_0_0 };
extern const Il2CppGenericInst GenInst_File_t3103643102_0_0_0 = { 1, GenInst_File_t3103643102_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_File_t3103643102_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &File_t3103643102_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_File_t3103643102_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_File_t3103643102_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t991604272_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t991604272_0_0_0_Types[] = { &KeyValuePair_2_t991604272_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t991604272_0_0_0 = { 1, GenInst_KeyValuePair_2_t991604272_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_File_t3103643102_0_0_0_KeyValuePair_2_t991604272_0_0_0_Types[] = { &String_t_0_0_0, &File_t3103643102_0_0_0, &KeyValuePair_2_t991604272_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_File_t3103643102_0_0_0_KeyValuePair_2_t991604272_0_0_0 = { 3, GenInst_String_t_0_0_0_File_t3103643102_0_0_0_KeyValuePair_2_t991604272_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_File_t3103643102_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &File_t3103643102_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_File_t3103643102_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_File_t3103643102_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_File_t3103643102_0_0_0_File_t3103643102_0_0_0_Types[] = { &String_t_0_0_0, &File_t3103643102_0_0_0, &File_t3103643102_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_File_t3103643102_0_0_0_File_t3103643102_0_0_0 = { 3, GenInst_String_t_0_0_0_File_t3103643102_0_0_0_File_t3103643102_0_0_0_Types };
extern const Il2CppType List_1_t280750548_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t280750548_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t280750548_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t280750548_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t280750548_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2463679014_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2463679014_0_0_0_Types[] = { &KeyValuePair_2_t2463679014_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2463679014_0_0_0 = { 1, GenInst_KeyValuePair_2_t2463679014_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t280750548_0_0_0_Types[] = { &List_1_t280750548_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t280750548_0_0_0 = { 1, GenInst_List_1_t280750548_0_0_0_Types };
extern const Il2CppType Match_t3408321083_0_0_0;
static const Il2CppType* GenInst_Match_t3408321083_0_0_0_Types[] = { &Match_t3408321083_0_0_0 };
extern const Il2CppGenericInst GenInst_Match_t3408321083_0_0_0 = { 1, GenInst_Match_t3408321083_0_0_0_Types };
extern const Il2CppType Identification_t455147138_0_0_0;
static const Il2CppType* GenInst_Identification_t455147138_0_0_0_Types[] = { &Identification_t455147138_0_0_0 };
extern const Il2CppGenericInst GenInst_Identification_t455147138_0_0_0 = { 1, GenInst_Identification_t455147138_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Node_t2365075726_0_0_0_Types[] = { &String_t_0_0_0, &Node_t2365075726_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Node_t2365075726_0_0_0 = { 2, GenInst_String_t_0_0_0_Node_t2365075726_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t253036896_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t253036896_0_0_0_Types[] = { &KeyValuePair_2_t253036896_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t253036896_0_0_0 = { 1, GenInst_KeyValuePair_2_t253036896_0_0_0_Types };
extern const Il2CppType CacheItem_t2069182237_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_CacheItem_t2069182237_0_0_0_Types[] = { &String_t_0_0_0, &CacheItem_t2069182237_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_CacheItem_t2069182237_0_0_0 = { 2, GenInst_String_t_0_0_0_CacheItem_t2069182237_0_0_0_Types };
static const Il2CppType* GenInst_CacheItem_t2069182237_0_0_0_Types[] = { &CacheItem_t2069182237_0_0_0 };
extern const Il2CppGenericInst GenInst_CacheItem_t2069182237_0_0_0 = { 1, GenInst_CacheItem_t2069182237_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_CacheItem_t2069182237_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &CacheItem_t2069182237_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_CacheItem_t2069182237_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_CacheItem_t2069182237_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4252110703_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4252110703_0_0_0_Types[] = { &KeyValuePair_2_t4252110703_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4252110703_0_0_0 = { 1, GenInst_KeyValuePair_2_t4252110703_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_CacheItem_t2069182237_0_0_0_KeyValuePair_2_t4252110703_0_0_0_Types[] = { &String_t_0_0_0, &CacheItem_t2069182237_0_0_0, &KeyValuePair_2_t4252110703_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_CacheItem_t2069182237_0_0_0_KeyValuePair_2_t4252110703_0_0_0 = { 3, GenInst_String_t_0_0_0_CacheItem_t2069182237_0_0_0_KeyValuePair_2_t4252110703_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_CacheItem_t2069182237_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &CacheItem_t2069182237_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_CacheItem_t2069182237_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_CacheItem_t2069182237_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_CacheItem_t2069182237_0_0_0_CacheItem_t2069182237_0_0_0_Types[] = { &String_t_0_0_0, &CacheItem_t2069182237_0_0_0, &CacheItem_t2069182237_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_CacheItem_t2069182237_0_0_0_CacheItem_t2069182237_0_0_0 = { 3, GenInst_String_t_0_0_0_CacheItem_t2069182237_0_0_0_CacheItem_t2069182237_0_0_0_Types };
extern const Il2CppType FileSystemWatcher_t416760199_0_0_0;
static const Il2CppType* GenInst_FileSystemWatcher_t416760199_0_0_0_Types[] = { &FileSystemWatcher_t416760199_0_0_0 };
extern const Il2CppGenericInst GenInst_FileSystemWatcher_t416760199_0_0_0 = { 1, GenInst_FileSystemWatcher_t416760199_0_0_0_Types };
extern const Il2CppType ISupportInitialize_t1184141141_0_0_0;
static const Il2CppType* GenInst_ISupportInitialize_t1184141141_0_0_0_Types[] = { &ISupportInitialize_t1184141141_0_0_0 };
extern const Il2CppGenericInst GenInst_ISupportInitialize_t1184141141_0_0_0 = { 1, GenInst_ISupportInitialize_t1184141141_0_0_0_Types };
extern const Il2CppType Component_t3620823400_0_0_0;
static const Il2CppType* GenInst_Component_t3620823400_0_0_0_Types[] = { &Component_t3620823400_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t3620823400_0_0_0 = { 1, GenInst_Component_t3620823400_0_0_0_Types };
extern const Il2CppType IComponent_t2320218252_0_0_0;
static const Il2CppType* GenInst_IComponent_t2320218252_0_0_0_Types[] = { &IComponent_t2320218252_0_0_0 };
extern const Il2CppGenericInst GenInst_IComponent_t2320218252_0_0_0 = { 1, GenInst_IComponent_t2320218252_0_0_0_Types };
extern const Il2CppType DataItem_t1175161993_0_0_0;
static const Il2CppType* GenInst_DataItem_t1175161993_0_0_0_Types[] = { &DataItem_t1175161993_0_0_0 };
extern const Il2CppGenericInst GenInst_DataItem_t1175161993_0_0_0 = { 1, GenInst_DataItem_t1175161993_0_0_0_Types };
extern const Il2CppType AspComponent_t590775874_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_AspComponent_t590775874_0_0_0_Types[] = { &String_t_0_0_0, &AspComponent_t590775874_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_AspComponent_t590775874_0_0_0 = { 2, GenInst_String_t_0_0_0_AspComponent_t590775874_0_0_0_Types };
static const Il2CppType* GenInst_AspComponent_t590775874_0_0_0_Types[] = { &AspComponent_t590775874_0_0_0 };
extern const Il2CppGenericInst GenInst_AspComponent_t590775874_0_0_0 = { 1, GenInst_AspComponent_t590775874_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_AspComponent_t590775874_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &AspComponent_t590775874_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_AspComponent_t590775874_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_AspComponent_t590775874_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2773704340_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2773704340_0_0_0_Types[] = { &KeyValuePair_2_t2773704340_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2773704340_0_0_0 = { 1, GenInst_KeyValuePair_2_t2773704340_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_AspComponent_t590775874_0_0_0_KeyValuePair_2_t2773704340_0_0_0_Types[] = { &String_t_0_0_0, &AspComponent_t590775874_0_0_0, &KeyValuePair_2_t2773704340_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_AspComponent_t590775874_0_0_0_KeyValuePair_2_t2773704340_0_0_0 = { 3, GenInst_String_t_0_0_0_AspComponent_t590775874_0_0_0_KeyValuePair_2_t2773704340_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_AspComponent_t590775874_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &AspComponent_t590775874_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_AspComponent_t590775874_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_AspComponent_t590775874_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_AspComponent_t590775874_0_0_0_AspComponent_t590775874_0_0_0_Types[] = { &String_t_0_0_0, &AspComponent_t590775874_0_0_0, &AspComponent_t590775874_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_AspComponent_t590775874_0_0_0_AspComponent_t590775874_0_0_0 = { 3, GenInst_String_t_0_0_0_AspComponent_t590775874_0_0_0_AspComponent_t590775874_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Assembly_t4102432799_0_0_0_Types[] = { &String_t_0_0_0, &Assembly_t4102432799_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Assembly_t4102432799_0_0_0 = { 2, GenInst_String_t_0_0_0_Assembly_t4102432799_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Assembly_t4102432799_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &Assembly_t4102432799_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Assembly_t4102432799_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_Assembly_t4102432799_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1990393969_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1990393969_0_0_0_Types[] = { &KeyValuePair_2_t1990393969_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1990393969_0_0_0 = { 1, GenInst_KeyValuePair_2_t1990393969_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Assembly_t4102432799_0_0_0_KeyValuePair_2_t1990393969_0_0_0_Types[] = { &String_t_0_0_0, &Assembly_t4102432799_0_0_0, &KeyValuePair_2_t1990393969_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Assembly_t4102432799_0_0_0_KeyValuePair_2_t1990393969_0_0_0 = { 3, GenInst_String_t_0_0_0_Assembly_t4102432799_0_0_0_KeyValuePair_2_t1990393969_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Assembly_t4102432799_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &Assembly_t4102432799_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Assembly_t4102432799_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_Assembly_t4102432799_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Assembly_t4102432799_0_0_0_Assembly_t4102432799_0_0_0_Types[] = { &String_t_0_0_0, &Assembly_t4102432799_0_0_0, &Assembly_t4102432799_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Assembly_t4102432799_0_0_0_Assembly_t4102432799_0_0_0 = { 3, GenInst_String_t_0_0_0_Assembly_t4102432799_0_0_0_Assembly_t4102432799_0_0_0_Types };
extern const Il2CppType CodeCompileUnit_t2527618915_0_0_0;
static const Il2CppType* GenInst_CodeCompileUnit_t2527618915_0_0_0_Types[] = { &CodeCompileUnit_t2527618915_0_0_0 };
extern const Il2CppGenericInst GenInst_CodeCompileUnit_t2527618915_0_0_0 = { 1, GenInst_CodeCompileUnit_t2527618915_0_0_0_Types };
extern const Il2CppType AppCodeAssembly_t1261384177_0_0_0;
static const Il2CppType* GenInst_AppCodeAssembly_t1261384177_0_0_0_Types[] = { &AppCodeAssembly_t1261384177_0_0_0 };
extern const Il2CppGenericInst GenInst_AppCodeAssembly_t1261384177_0_0_0 = { 1, GenInst_AppCodeAssembly_t1261384177_0_0_0_Types };
extern const Il2CppType AppResourceFileInfo_t3599744125_0_0_0;
static const Il2CppType* GenInst_AppResourceFileInfo_t3599744125_0_0_0_Types[] = { &AppResourceFileInfo_t3599744125_0_0_0 };
extern const Il2CppGenericInst GenInst_AppResourceFileInfo_t3599744125_0_0_0 = { 1, GenInst_AppResourceFileInfo_t3599744125_0_0_0_Types };
extern const Il2CppType List_1_t3319525431_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t3319525431_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t3319525431_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t3319525431_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t3319525431_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t3319525431_0_0_0_Types[] = { &List_1_t3319525431_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3319525431_0_0_0 = { 1, GenInst_List_1_t3319525431_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t3319525431_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t3319525431_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t3319525431_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t3319525431_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1207486601_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1207486601_0_0_0_Types[] = { &KeyValuePair_2_t1207486601_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1207486601_0_0_0 = { 1, GenInst_KeyValuePair_2_t1207486601_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t3319525431_0_0_0_KeyValuePair_2_t1207486601_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t3319525431_0_0_0, &KeyValuePair_2_t1207486601_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t3319525431_0_0_0_KeyValuePair_2_t1207486601_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t3319525431_0_0_0_KeyValuePair_2_t1207486601_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t3319525431_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t3319525431_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t3319525431_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t3319525431_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t3319525431_0_0_0_List_1_t3319525431_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t3319525431_0_0_0, &List_1_t3319525431_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t3319525431_0_0_0_List_1_t3319525431_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t3319525431_0_0_0_List_1_t3319525431_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Type_t_0_0_0_Types[] = { &String_t_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Type_t_0_0_0 = { 2, GenInst_String_t_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &Type_t_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t371905930_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t371905930_0_0_0_Types[] = { &KeyValuePair_2_t371905930_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t371905930_0_0_0 = { 1, GenInst_KeyValuePair_2_t371905930_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Type_t_0_0_0_KeyValuePair_2_t371905930_0_0_0_Types[] = { &String_t_0_0_0, &Type_t_0_0_0, &KeyValuePair_2_t371905930_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Type_t_0_0_0_KeyValuePair_2_t371905930_0_0_0 = { 3, GenInst_String_t_0_0_0_Type_t_0_0_0_KeyValuePair_2_t371905930_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Type_t_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &Type_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Type_t_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_Type_t_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Type_t_0_0_0_Type_t_0_0_0_Types[] = { &String_t_0_0_0, &Type_t_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Type_t_0_0_0_Type_t_0_0_0 = { 3, GenInst_String_t_0_0_0_Type_t_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t968067334_0_0_0_Types[] = { &String_t_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t968067334_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t968067334_0_0_0 = { 3, GenInst_String_t_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t968067334_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Il2CppObject_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &Il2CppObject_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Il2CppObject_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_Il2CppObject_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &String_t_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_String_t_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType TextBlock_t3735829980_0_0_0;
static const Il2CppType* GenInst_TextBlock_t3735829980_0_0_0_Types[] = { &TextBlock_t3735829980_0_0_0 };
extern const Il2CppGenericInst GenInst_TextBlock_t3735829980_0_0_0 = { 1, GenInst_TextBlock_t3735829980_0_0_0_Types };
extern const Il2CppType ServerSideScript_t2595428029_0_0_0;
static const Il2CppType* GenInst_ServerSideScript_t2595428029_0_0_0_Types[] = { &ServerSideScript_t2595428029_0_0_0 };
extern const Il2CppGenericInst GenInst_ServerSideScript_t2595428029_0_0_0 = { 1, GenInst_ServerSideScript_t2595428029_0_0_0_Types };
extern const Il2CppType List_1_t715253879_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t715253879_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t715253879_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t715253879_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t715253879_0_0_0_Types };
extern const Il2CppType CompileUnitPartialType_t3538146433_0_0_0;
static const Il2CppType* GenInst_CompileUnitPartialType_t3538146433_0_0_0_Types[] = { &CompileUnitPartialType_t3538146433_0_0_0 };
extern const Il2CppGenericInst GenInst_CompileUnitPartialType_t3538146433_0_0_0 = { 1, GenInst_CompileUnitPartialType_t3538146433_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t715253879_0_0_0_Types[] = { &List_1_t715253879_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t715253879_0_0_0 = { 1, GenInst_List_1_t715253879_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t715253879_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t715253879_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t715253879_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t715253879_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2898182345_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2898182345_0_0_0_Types[] = { &KeyValuePair_2_t2898182345_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2898182345_0_0_0 = { 1, GenInst_KeyValuePair_2_t2898182345_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t715253879_0_0_0_KeyValuePair_2_t2898182345_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t715253879_0_0_0, &KeyValuePair_2_t2898182345_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t715253879_0_0_0_KeyValuePair_2_t2898182345_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t715253879_0_0_0_KeyValuePair_2_t2898182345_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t715253879_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t715253879_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t715253879_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t715253879_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t715253879_0_0_0_List_1_t715253879_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t715253879_0_0_0, &List_1_t715253879_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t715253879_0_0_0_List_1_t715253879_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t715253879_0_0_0_List_1_t715253879_0_0_0_Types };
extern const Il2CppType BuildProvider_t3736381005_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_BuildProvider_t3736381005_0_0_0_Types[] = { &String_t_0_0_0, &BuildProvider_t3736381005_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_BuildProvider_t3736381005_0_0_0 = { 2, GenInst_String_t_0_0_0_BuildProvider_t3736381005_0_0_0_Types };
static const Il2CppType* GenInst_BuildProvider_t3736381005_0_0_0_Types[] = { &BuildProvider_t3736381005_0_0_0 };
extern const Il2CppGenericInst GenInst_BuildProvider_t3736381005_0_0_0 = { 1, GenInst_BuildProvider_t3736381005_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_BuildProvider_t3736381005_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &BuildProvider_t3736381005_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_BuildProvider_t3736381005_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_BuildProvider_t3736381005_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1624342175_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1624342175_0_0_0_Types[] = { &KeyValuePair_2_t1624342175_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1624342175_0_0_0 = { 1, GenInst_KeyValuePair_2_t1624342175_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_BuildProvider_t3736381005_0_0_0_KeyValuePair_2_t1624342175_0_0_0_Types[] = { &String_t_0_0_0, &BuildProvider_t3736381005_0_0_0, &KeyValuePair_2_t1624342175_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_BuildProvider_t3736381005_0_0_0_KeyValuePair_2_t1624342175_0_0_0 = { 3, GenInst_String_t_0_0_0_BuildProvider_t3736381005_0_0_0_KeyValuePair_2_t1624342175_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_BuildProvider_t3736381005_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &BuildProvider_t3736381005_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_BuildProvider_t3736381005_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_BuildProvider_t3736381005_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_BuildProvider_t3736381005_0_0_0_BuildProvider_t3736381005_0_0_0_Types[] = { &String_t_0_0_0, &BuildProvider_t3736381005_0_0_0, &BuildProvider_t3736381005_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_BuildProvider_t3736381005_0_0_0_BuildProvider_t3736381005_0_0_0 = { 3, GenInst_String_t_0_0_0_BuildProvider_t3736381005_0_0_0_BuildProvider_t3736381005_0_0_0_Types };
extern const Il2CppType CodeUnit_t1519973372_0_0_0;
static const Il2CppType* GenInst_CodeUnit_t1519973372_0_0_0_Types[] = { &CodeUnit_t1519973372_0_0_0 };
extern const Il2CppGenericInst GenInst_CodeUnit_t1519973372_0_0_0 = { 1, GenInst_CodeUnit_t1519973372_0_0_0_Types };
extern const Il2CppType CodeDomProvider_t110349836_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_CodeDomProvider_t110349836_0_0_0_Types[] = { &Type_t_0_0_0, &CodeDomProvider_t110349836_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_CodeDomProvider_t110349836_0_0_0 = { 2, GenInst_Type_t_0_0_0_CodeDomProvider_t110349836_0_0_0_Types };
static const Il2CppType* GenInst_CodeDomProvider_t110349836_0_0_0_Types[] = { &CodeDomProvider_t110349836_0_0_0 };
extern const Il2CppGenericInst GenInst_CodeDomProvider_t110349836_0_0_0 = { 1, GenInst_CodeDomProvider_t110349836_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_CodeDomProvider_t110349836_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Type_t_0_0_0, &CodeDomProvider_t110349836_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_CodeDomProvider_t110349836_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Type_t_0_0_0_CodeDomProvider_t110349836_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t657401771_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t657401771_0_0_0_Types[] = { &KeyValuePair_2_t657401771_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t657401771_0_0_0 = { 1, GenInst_KeyValuePair_2_t657401771_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_CodeDomProvider_t110349836_0_0_0_KeyValuePair_2_t657401771_0_0_0_Types[] = { &Type_t_0_0_0, &CodeDomProvider_t110349836_0_0_0, &KeyValuePair_2_t657401771_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_CodeDomProvider_t110349836_0_0_0_KeyValuePair_2_t657401771_0_0_0 = { 3, GenInst_Type_t_0_0_0_CodeDomProvider_t110349836_0_0_0_KeyValuePair_2_t657401771_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_CodeDomProvider_t110349836_0_0_0_Type_t_0_0_0_Types[] = { &Type_t_0_0_0, &CodeDomProvider_t110349836_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_CodeDomProvider_t110349836_0_0_0_Type_t_0_0_0 = { 3, GenInst_Type_t_0_0_0_CodeDomProvider_t110349836_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_CodeDomProvider_t110349836_0_0_0_CodeDomProvider_t110349836_0_0_0_Types[] = { &Type_t_0_0_0, &CodeDomProvider_t110349836_0_0_0, &CodeDomProvider_t110349836_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_CodeDomProvider_t110349836_0_0_0_CodeDomProvider_t110349836_0_0_0 = { 3, GenInst_Type_t_0_0_0_CodeDomProvider_t110349836_0_0_0_CodeDomProvider_t110349836_0_0_0_Types };
extern const Il2CppType BuildManagerCacheItem_t1643815903_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_BuildManagerCacheItem_t1643815903_0_0_0_Types[] = { &String_t_0_0_0, &BuildManagerCacheItem_t1643815903_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_BuildManagerCacheItem_t1643815903_0_0_0 = { 2, GenInst_String_t_0_0_0_BuildManagerCacheItem_t1643815903_0_0_0_Types };
static const Il2CppType* GenInst_BuildManagerCacheItem_t1643815903_0_0_0_Types[] = { &BuildManagerCacheItem_t1643815903_0_0_0 };
extern const Il2CppGenericInst GenInst_BuildManagerCacheItem_t1643815903_0_0_0 = { 1, GenInst_BuildManagerCacheItem_t1643815903_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_BuildManagerCacheItem_t1643815903_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &BuildManagerCacheItem_t1643815903_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_BuildManagerCacheItem_t1643815903_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_BuildManagerCacheItem_t1643815903_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3826744369_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3826744369_0_0_0_Types[] = { &KeyValuePair_2_t3826744369_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3826744369_0_0_0 = { 1, GenInst_KeyValuePair_2_t3826744369_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_BuildManagerCacheItem_t1643815903_0_0_0_KeyValuePair_2_t3826744369_0_0_0_Types[] = { &String_t_0_0_0, &BuildManagerCacheItem_t1643815903_0_0_0, &KeyValuePair_2_t3826744369_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_BuildManagerCacheItem_t1643815903_0_0_0_KeyValuePair_2_t3826744369_0_0_0 = { 3, GenInst_String_t_0_0_0_BuildManagerCacheItem_t1643815903_0_0_0_KeyValuePair_2_t3826744369_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_BuildManagerCacheItem_t1643815903_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &BuildManagerCacheItem_t1643815903_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_BuildManagerCacheItem_t1643815903_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_BuildManagerCacheItem_t1643815903_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_BuildManagerCacheItem_t1643815903_0_0_0_BuildManagerCacheItem_t1643815903_0_0_0_Types[] = { &String_t_0_0_0, &BuildManagerCacheItem_t1643815903_0_0_0, &BuildManagerCacheItem_t1643815903_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_BuildManagerCacheItem_t1643815903_0_0_0_BuildManagerCacheItem_t1643815903_0_0_0 = { 3, GenInst_String_t_0_0_0_BuildManagerCacheItem_t1643815903_0_0_0_BuildManagerCacheItem_t1643815903_0_0_0_Types };
extern const Il2CppType PreCompilationData_t3220258209_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_PreCompilationData_t3220258209_0_0_0_Types[] = { &String_t_0_0_0, &PreCompilationData_t3220258209_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_PreCompilationData_t3220258209_0_0_0 = { 2, GenInst_String_t_0_0_0_PreCompilationData_t3220258209_0_0_0_Types };
static const Il2CppType* GenInst_PreCompilationData_t3220258209_0_0_0_Types[] = { &PreCompilationData_t3220258209_0_0_0 };
extern const Il2CppGenericInst GenInst_PreCompilationData_t3220258209_0_0_0 = { 1, GenInst_PreCompilationData_t3220258209_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_PreCompilationData_t3220258209_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &PreCompilationData_t3220258209_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_PreCompilationData_t3220258209_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_PreCompilationData_t3220258209_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1108219379_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1108219379_0_0_0_Types[] = { &KeyValuePair_2_t1108219379_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1108219379_0_0_0 = { 1, GenInst_KeyValuePair_2_t1108219379_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_PreCompilationData_t3220258209_0_0_0_KeyValuePair_2_t1108219379_0_0_0_Types[] = { &String_t_0_0_0, &PreCompilationData_t3220258209_0_0_0, &KeyValuePair_2_t1108219379_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_PreCompilationData_t3220258209_0_0_0_KeyValuePair_2_t1108219379_0_0_0 = { 3, GenInst_String_t_0_0_0_PreCompilationData_t3220258209_0_0_0_KeyValuePair_2_t1108219379_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_PreCompilationData_t3220258209_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &PreCompilationData_t3220258209_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_PreCompilationData_t3220258209_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_PreCompilationData_t3220258209_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_PreCompilationData_t3220258209_0_0_0_PreCompilationData_t3220258209_0_0_0_Types[] = { &String_t_0_0_0, &PreCompilationData_t3220258209_0_0_0, &PreCompilationData_t3220258209_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_PreCompilationData_t3220258209_0_0_0_PreCompilationData_t3220258209_0_0_0 = { 3, GenInst_String_t_0_0_0_PreCompilationData_t3220258209_0_0_0_PreCompilationData_t3220258209_0_0_0_Types };
extern const Il2CppType BuildProviderGroup_t619564070_0_0_0;
static const Il2CppType* GenInst_BuildProviderGroup_t619564070_0_0_0_Types[] = { &BuildProviderGroup_t619564070_0_0_0 };
extern const Il2CppGenericInst GenInst_BuildProviderGroup_t619564070_0_0_0 = { 1, GenInst_BuildProviderGroup_t619564070_0_0_0_Types };
extern const Il2CppType List_1_t913488451_0_0_0;
static const Il2CppType* GenInst_List_1_t913488451_0_0_0_Types[] = { &List_1_t913488451_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t913488451_0_0_0 = { 1, GenInst_List_1_t913488451_0_0_0_Types };
static const Il2CppType* GenInst_BuildProvider_t3736381005_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &BuildProvider_t3736381005_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_BuildProvider_t3736381005_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_BuildProvider_t3736381005_0_0_0_Boolean_t97287965_0_0_0_Types };
static const Il2CppType* GenInst_BuildProvider_t3736381005_0_0_0_Boolean_t97287965_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &BuildProvider_t3736381005_0_0_0, &Boolean_t97287965_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_BuildProvider_t3736381005_0_0_0_Boolean_t97287965_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_BuildProvider_t3736381005_0_0_0_Boolean_t97287965_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3833317299_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3833317299_0_0_0_Types[] = { &KeyValuePair_2_t3833317299_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3833317299_0_0_0 = { 1, GenInst_KeyValuePair_2_t3833317299_0_0_0_Types };
static const Il2CppType* GenInst_BuildProvider_t3736381005_0_0_0_Boolean_t97287965_0_0_0_KeyValuePair_2_t3833317299_0_0_0_Types[] = { &BuildProvider_t3736381005_0_0_0, &Boolean_t97287965_0_0_0, &KeyValuePair_2_t3833317299_0_0_0 };
extern const Il2CppGenericInst GenInst_BuildProvider_t3736381005_0_0_0_Boolean_t97287965_0_0_0_KeyValuePair_2_t3833317299_0_0_0 = { 3, GenInst_BuildProvider_t3736381005_0_0_0_Boolean_t97287965_0_0_0_KeyValuePair_2_t3833317299_0_0_0_Types };
static const Il2CppType* GenInst_BuildProvider_t3736381005_0_0_0_Boolean_t97287965_0_0_0_BuildProvider_t3736381005_0_0_0_Types[] = { &BuildProvider_t3736381005_0_0_0, &Boolean_t97287965_0_0_0, &BuildProvider_t3736381005_0_0_0 };
extern const Il2CppGenericInst GenInst_BuildProvider_t3736381005_0_0_0_Boolean_t97287965_0_0_0_BuildProvider_t3736381005_0_0_0 = { 3, GenInst_BuildProvider_t3736381005_0_0_0_Boolean_t97287965_0_0_0_BuildProvider_t3736381005_0_0_0_Types };
static const Il2CppType* GenInst_BuildProvider_t3736381005_0_0_0_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &BuildProvider_t3736381005_0_0_0, &Boolean_t97287965_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_BuildProvider_t3736381005_0_0_0_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0 = { 3, GenInst_BuildProvider_t3736381005_0_0_0_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0_Types };
extern const Il2CppType ResourceManagerCacheKey_t3697749236_0_0_0;
extern const Il2CppType ResourceManager_t4037989559_0_0_0;
static const Il2CppType* GenInst_ResourceManagerCacheKey_t3697749236_0_0_0_ResourceManager_t4037989559_0_0_0_Types[] = { &ResourceManagerCacheKey_t3697749236_0_0_0, &ResourceManager_t4037989559_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceManagerCacheKey_t3697749236_0_0_0_ResourceManager_t4037989559_0_0_0 = { 2, GenInst_ResourceManagerCacheKey_t3697749236_0_0_0_ResourceManager_t4037989559_0_0_0_Types };
static const Il2CppType* GenInst_ResourceManagerCacheKey_t3697749236_0_0_0_Types[] = { &ResourceManagerCacheKey_t3697749236_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceManagerCacheKey_t3697749236_0_0_0 = { 1, GenInst_ResourceManagerCacheKey_t3697749236_0_0_0_Types };
static const Il2CppType* GenInst_ResourceManager_t4037989559_0_0_0_Types[] = { &ResourceManager_t4037989559_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceManager_t4037989559_0_0_0 = { 1, GenInst_ResourceManager_t4037989559_0_0_0_Types };
static const Il2CppType* GenInst_ResourceManagerCacheKey_t3697749236_0_0_0_ResourceManager_t4037989559_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &ResourceManagerCacheKey_t3697749236_0_0_0, &ResourceManager_t4037989559_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceManagerCacheKey_t3697749236_0_0_0_ResourceManager_t4037989559_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_ResourceManagerCacheKey_t3697749236_0_0_0_ResourceManager_t4037989559_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2472737834_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2472737834_0_0_0_Types[] = { &KeyValuePair_2_t2472737834_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2472737834_0_0_0 = { 1, GenInst_KeyValuePair_2_t2472737834_0_0_0_Types };
static const Il2CppType* GenInst_ResourceManagerCacheKey_t3697749236_0_0_0_ResourceManager_t4037989559_0_0_0_KeyValuePair_2_t2472737834_0_0_0_Types[] = { &ResourceManagerCacheKey_t3697749236_0_0_0, &ResourceManager_t4037989559_0_0_0, &KeyValuePair_2_t2472737834_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceManagerCacheKey_t3697749236_0_0_0_ResourceManager_t4037989559_0_0_0_KeyValuePair_2_t2472737834_0_0_0 = { 3, GenInst_ResourceManagerCacheKey_t3697749236_0_0_0_ResourceManager_t4037989559_0_0_0_KeyValuePair_2_t2472737834_0_0_0_Types };
static const Il2CppType* GenInst_ResourceManagerCacheKey_t3697749236_0_0_0_ResourceManager_t4037989559_0_0_0_ResourceManagerCacheKey_t3697749236_0_0_0_Types[] = { &ResourceManagerCacheKey_t3697749236_0_0_0, &ResourceManager_t4037989559_0_0_0, &ResourceManagerCacheKey_t3697749236_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceManagerCacheKey_t3697749236_0_0_0_ResourceManager_t4037989559_0_0_0_ResourceManagerCacheKey_t3697749236_0_0_0 = { 3, GenInst_ResourceManagerCacheKey_t3697749236_0_0_0_ResourceManager_t4037989559_0_0_0_ResourceManagerCacheKey_t3697749236_0_0_0_Types };
static const Il2CppType* GenInst_ResourceManagerCacheKey_t3697749236_0_0_0_ResourceManager_t4037989559_0_0_0_ResourceManager_t4037989559_0_0_0_Types[] = { &ResourceManagerCacheKey_t3697749236_0_0_0, &ResourceManager_t4037989559_0_0_0, &ResourceManager_t4037989559_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceManagerCacheKey_t3697749236_0_0_0_ResourceManager_t4037989559_0_0_0_ResourceManager_t4037989559_0_0_0 = { 3, GenInst_ResourceManagerCacheKey_t3697749236_0_0_0_ResourceManager_t4037989559_0_0_0_ResourceManager_t4037989559_0_0_0_Types };
extern const Il2CppType TemplateParser_t24149626_0_0_0;
static const Il2CppType* GenInst_TemplateParser_t24149626_0_0_0_Types[] = { &TemplateParser_t24149626_0_0_0 };
extern const Il2CppGenericInst GenInst_TemplateParser_t24149626_0_0_0 = { 1, GenInst_TemplateParser_t24149626_0_0_0_Types };
extern const Il2CppType ExtractDirectiveDependencies_t2024460703_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_ExtractDirectiveDependencies_t2024460703_0_0_0_Types[] = { &String_t_0_0_0, &ExtractDirectiveDependencies_t2024460703_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ExtractDirectiveDependencies_t2024460703_0_0_0 = { 2, GenInst_String_t_0_0_0_ExtractDirectiveDependencies_t2024460703_0_0_0_Types };
static const Il2CppType* GenInst_ExtractDirectiveDependencies_t2024460703_0_0_0_Types[] = { &ExtractDirectiveDependencies_t2024460703_0_0_0 };
extern const Il2CppGenericInst GenInst_ExtractDirectiveDependencies_t2024460703_0_0_0 = { 1, GenInst_ExtractDirectiveDependencies_t2024460703_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4207389169_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4207389169_0_0_0_Types[] = { &KeyValuePair_2_t4207389169_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4207389169_0_0_0 = { 1, GenInst_KeyValuePair_2_t4207389169_0_0_0_Types };
extern const Il2CppType UnknownAttributeDescriptor_t1586516698_0_0_0;
static const Il2CppType* GenInst_UnknownAttributeDescriptor_t1586516698_0_0_0_Types[] = { &UnknownAttributeDescriptor_t1586516698_0_0_0 };
extern const Il2CppGenericInst GenInst_UnknownAttributeDescriptor_t1586516698_0_0_0 = { 1, GenInst_UnknownAttributeDescriptor_t1586516698_0_0_0_Types };
extern const Il2CppType TemplateInstance_t2414853837_0_0_0;
static const Il2CppType* GenInst_TemplateInstance_t2414853837_0_0_0_Types[] = { &TemplateInstance_t2414853837_0_0_0 };
extern const Il2CppGenericInst GenInst_TemplateInstance_t2414853837_0_0_0 = { 1, GenInst_TemplateInstance_t2414853837_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_DateTime_t3738529785_0_0_0_Types[] = { &String_t_0_0_0, &DateTime_t3738529785_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_DateTime_t3738529785_0_0_0 = { 2, GenInst_String_t_0_0_0_DateTime_t3738529785_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_DateTime_t3738529785_0_0_0_Types[] = { &Il2CppObject_0_0_0, &DateTime_t3738529785_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_DateTime_t3738529785_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_DateTime_t3738529785_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3188640940_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3188640940_0_0_0_Types[] = { &KeyValuePair_2_t3188640940_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3188640940_0_0_0 = { 1, GenInst_KeyValuePair_2_t3188640940_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_DateTime_t3738529785_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &DateTime_t3738529785_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_DateTime_t3738529785_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_DateTime_t3738529785_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_DateTime_t3738529785_0_0_0_DateTime_t3738529785_0_0_0_Types[] = { &Il2CppObject_0_0_0, &DateTime_t3738529785_0_0_0, &DateTime_t3738529785_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_DateTime_t3738529785_0_0_0_DateTime_t3738529785_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_DateTime_t3738529785_0_0_0_DateTime_t3738529785_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_DateTime_t3738529785_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Il2CppObject_0_0_0, &DateTime_t3738529785_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_DateTime_t3738529785_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_DateTime_t3738529785_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_DateTime_t3738529785_0_0_0_KeyValuePair_2_t3188640940_0_0_0_Types[] = { &Il2CppObject_0_0_0, &DateTime_t3738529785_0_0_0, &KeyValuePair_2_t3188640940_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_DateTime_t3738529785_0_0_0_KeyValuePair_2_t3188640940_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_DateTime_t3738529785_0_0_0_KeyValuePair_2_t3188640940_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_DateTime_t3738529785_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &DateTime_t3738529785_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_DateTime_t3738529785_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_DateTime_t3738529785_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1626490955_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1626490955_0_0_0_Types[] = { &KeyValuePair_2_t1626490955_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1626490955_0_0_0 = { 1, GenInst_KeyValuePair_2_t1626490955_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_DateTime_t3738529785_0_0_0_KeyValuePair_2_t1626490955_0_0_0_Types[] = { &String_t_0_0_0, &DateTime_t3738529785_0_0_0, &KeyValuePair_2_t1626490955_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_DateTime_t3738529785_0_0_0_KeyValuePair_2_t1626490955_0_0_0 = { 3, GenInst_String_t_0_0_0_DateTime_t3738529785_0_0_0_KeyValuePair_2_t1626490955_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_DateTime_t3738529785_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &DateTime_t3738529785_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_DateTime_t3738529785_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_DateTime_t3738529785_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_DateTime_t3738529785_0_0_0_DateTime_t3738529785_0_0_0_Types[] = { &String_t_0_0_0, &DateTime_t3738529785_0_0_0, &DateTime_t3738529785_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_DateTime_t3738529785_0_0_0_DateTime_t3738529785_0_0_0 = { 3, GenInst_String_t_0_0_0_DateTime_t3738529785_0_0_0_DateTime_t3738529785_0_0_0_Types };
extern const Il2CppType WebResourceAttribute_t4132945045_0_0_0;
static const Il2CppType* GenInst_WebResourceAttribute_t4132945045_0_0_0_Types[] = { &WebResourceAttribute_t4132945045_0_0_0 };
extern const Il2CppGenericInst GenInst_WebResourceAttribute_t4132945045_0_0_0 = { 1, GenInst_WebResourceAttribute_t4132945045_0_0_0_Types };
extern const Il2CppType StringU5BU5D_t1281789340_0_0_0;
static const Il2CppType* GenInst_StringU5BU5D_t1281789340_0_0_0_Types[] = { &StringU5BU5D_t1281789340_0_0_0 };
extern const Il2CppGenericInst GenInst_StringU5BU5D_t1281789340_0_0_0 = { 1, GenInst_StringU5BU5D_t1281789340_0_0_0_Types };
extern const Il2CppType IList_1_t3662770472_0_0_0;
static const Il2CppType* GenInst_IList_1_t3662770472_0_0_0_Types[] = { &IList_1_t3662770472_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3662770472_0_0_0 = { 1, GenInst_IList_1_t3662770472_0_0_0_Types };
extern const Il2CppType ICollection_1_t380635627_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t380635627_0_0_0_Types[] = { &ICollection_1_t380635627_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t380635627_0_0_0 = { 1, GenInst_ICollection_1_t380635627_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t827303578_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t827303578_0_0_0_Types[] = { &IEnumerable_1_t827303578_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t827303578_0_0_0 = { 1, GenInst_IEnumerable_1_t827303578_0_0_0_Types };
extern const Il2CppType IList_1_t497718164_0_0_0;
static const Il2CppType* GenInst_IList_1_t497718164_0_0_0_Types[] = { &IList_1_t497718164_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t497718164_0_0_0 = { 1, GenInst_IList_1_t497718164_0_0_0_Types };
extern const Il2CppType ICollection_1_t1510550615_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1510550615_0_0_0_Types[] = { &ICollection_1_t1510550615_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1510550615_0_0_0 = { 1, GenInst_ICollection_1_t1510550615_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t1957218566_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t1957218566_0_0_0_Types[] = { &IEnumerable_1_t1957218566_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1957218566_0_0_0 = { 1, GenInst_IEnumerable_1_t1957218566_0_0_0_Types };
extern const Il2CppType IList_1_t1851431001_0_0_0;
static const Il2CppType* GenInst_IList_1_t1851431001_0_0_0_Types[] = { &IList_1_t1851431001_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t1851431001_0_0_0 = { 1, GenInst_IList_1_t1851431001_0_0_0_Types };
extern const Il2CppType ICollection_1_t2864263452_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t2864263452_0_0_0_Types[] = { &ICollection_1_t2864263452_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t2864263452_0_0_0 = { 1, GenInst_ICollection_1_t2864263452_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t3310931403_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t3310931403_0_0_0_Types[] = { &IEnumerable_1_t3310931403_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3310931403_0_0_0 = { 1, GenInst_IEnumerable_1_t3310931403_0_0_0_Types };
extern const Il2CppType IList_1_t3756487794_0_0_0;
static const Il2CppType* GenInst_IList_1_t3756487794_0_0_0_Types[] = { &IList_1_t3756487794_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3756487794_0_0_0 = { 1, GenInst_IList_1_t3756487794_0_0_0_Types };
extern const Il2CppType ICollection_1_t474352949_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t474352949_0_0_0_Types[] = { &ICollection_1_t474352949_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t474352949_0_0_0 = { 1, GenInst_ICollection_1_t474352949_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t921020900_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t921020900_0_0_0_Types[] = { &IEnumerable_1_t921020900_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t921020900_0_0_0 = { 1, GenInst_IEnumerable_1_t921020900_0_0_0_Types };
extern const Il2CppType IList_1_t2539743981_0_0_0;
static const Il2CppType* GenInst_IList_1_t2539743981_0_0_0_Types[] = { &IList_1_t2539743981_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t2539743981_0_0_0 = { 1, GenInst_IList_1_t2539743981_0_0_0_Types };
extern const Il2CppType ICollection_1_t3552576432_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t3552576432_0_0_0_Types[] = { &ICollection_1_t3552576432_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t3552576432_0_0_0 = { 1, GenInst_ICollection_1_t3552576432_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t3999244383_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t3999244383_0_0_0_Types[] = { &IEnumerable_1_t3999244383_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3999244383_0_0_0 = { 1, GenInst_IEnumerable_1_t3999244383_0_0_0_Types };
extern const Il2CppType IList_1_t3031434885_0_0_0;
static const Il2CppType* GenInst_IList_1_t3031434885_0_0_0_Types[] = { &IList_1_t3031434885_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3031434885_0_0_0 = { 1, GenInst_IList_1_t3031434885_0_0_0_Types };
extern const Il2CppType ICollection_1_t4044267336_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t4044267336_0_0_0_Types[] = { &ICollection_1_t4044267336_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t4044267336_0_0_0 = { 1, GenInst_ICollection_1_t4044267336_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t195967991_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t195967991_0_0_0_Types[] = { &IEnumerable_1_t195967991_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t195967991_0_0_0 = { 1, GenInst_IEnumerable_1_t195967991_0_0_0_Types };
extern const Il2CppType IList_1_t258948903_0_0_0;
static const Il2CppType* GenInst_IList_1_t258948903_0_0_0_Types[] = { &IList_1_t258948903_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t258948903_0_0_0 = { 1, GenInst_IList_1_t258948903_0_0_0_Types };
extern const Il2CppType ICollection_1_t1271781354_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1271781354_0_0_0_Types[] = { &ICollection_1_t1271781354_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1271781354_0_0_0 = { 1, GenInst_ICollection_1_t1271781354_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t1718449305_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t1718449305_0_0_0_Types[] = { &IEnumerable_1_t1718449305_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1718449305_0_0_0 = { 1, GenInst_IEnumerable_1_t1718449305_0_0_0_Types };
extern const Il2CppType IResourceProvider_t232608435_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_IResourceProvider_t232608435_0_0_0_Types[] = { &String_t_0_0_0, &IResourceProvider_t232608435_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_IResourceProvider_t232608435_0_0_0 = { 2, GenInst_String_t_0_0_0_IResourceProvider_t232608435_0_0_0_Types };
static const Il2CppType* GenInst_IResourceProvider_t232608435_0_0_0_Types[] = { &IResourceProvider_t232608435_0_0_0 };
extern const Il2CppGenericInst GenInst_IResourceProvider_t232608435_0_0_0 = { 1, GenInst_IResourceProvider_t232608435_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_IResourceProvider_t232608435_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &IResourceProvider_t232608435_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_IResourceProvider_t232608435_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_IResourceProvider_t232608435_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2415536901_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2415536901_0_0_0_Types[] = { &KeyValuePair_2_t2415536901_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2415536901_0_0_0 = { 1, GenInst_KeyValuePair_2_t2415536901_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_IResourceProvider_t232608435_0_0_0_KeyValuePair_2_t2415536901_0_0_0_Types[] = { &String_t_0_0_0, &IResourceProvider_t232608435_0_0_0, &KeyValuePair_2_t2415536901_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_IResourceProvider_t232608435_0_0_0_KeyValuePair_2_t2415536901_0_0_0 = { 3, GenInst_String_t_0_0_0_IResourceProvider_t232608435_0_0_0_KeyValuePair_2_t2415536901_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_IResourceProvider_t232608435_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &IResourceProvider_t232608435_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_IResourceProvider_t232608435_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_IResourceProvider_t232608435_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_IResourceProvider_t232608435_0_0_0_IResourceProvider_t232608435_0_0_0_Types[] = { &String_t_0_0_0, &IResourceProvider_t232608435_0_0_0, &IResourceProvider_t232608435_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_IResourceProvider_t232608435_0_0_0_IResourceProvider_t232608435_0_0_0 = { 3, GenInst_String_t_0_0_0_IResourceProvider_t232608435_0_0_0_IResourceProvider_t232608435_0_0_0_Types };
extern const Il2CppType IHttpHandler_t2624218893_0_0_0;
static const Il2CppType* GenInst_IHttpHandler_t2624218893_0_0_0_Types[] = { &IHttpHandler_t2624218893_0_0_0 };
extern const Il2CppGenericInst GenInst_IHttpHandler_t2624218893_0_0_0 = { 1, GenInst_IHttpHandler_t2624218893_0_0_0_Types };
extern const Il2CppType InfoTraceData_t1062706700_0_0_0;
static const Il2CppType* GenInst_InfoTraceData_t1062706700_0_0_0_Types[] = { &InfoTraceData_t1062706700_0_0_0 };
extern const Il2CppGenericInst GenInst_InfoTraceData_t1062706700_0_0_0 = { 1, GenInst_InfoTraceData_t1062706700_0_0_0_Types };
extern const Il2CppType ControlTraceData_t1335153449_0_0_0;
static const Il2CppType* GenInst_ControlTraceData_t1335153449_0_0_0_Types[] = { &ControlTraceData_t1335153449_0_0_0 };
extern const Il2CppGenericInst GenInst_ControlTraceData_t1335153449_0_0_0 = { 1, GenInst_ControlTraceData_t1335153449_0_0_0_Types };
extern const Il2CppType NameValueTraceData_t2568000481_0_0_0;
static const Il2CppType* GenInst_NameValueTraceData_t2568000481_0_0_0_Types[] = { &NameValueTraceData_t2568000481_0_0_0 };
extern const Il2CppGenericInst GenInst_NameValueTraceData_t2568000481_0_0_0 = { 1, GenInst_NameValueTraceData_t2568000481_0_0_0_Types };
extern const Il2CppType TraceData_t4177544667_0_0_0;
static const Il2CppType* GenInst_TraceData_t4177544667_0_0_0_Types[] = { &TraceData_t4177544667_0_0_0 };
extern const Il2CppGenericInst GenInst_TraceData_t4177544667_0_0_0 = { 1, GenInst_TraceData_t4177544667_0_0_0_Types };
extern const Il2CppType Control_t3006474639_0_0_0;
static const Il2CppType* GenInst_Control_t3006474639_0_0_0_Types[] = { &Control_t3006474639_0_0_0 };
extern const Il2CppGenericInst GenInst_Control_t3006474639_0_0_0 = { 1, GenInst_Control_t3006474639_0_0_0_Types };
extern const Il2CppType IControlBuilderAccessor_t1240310411_0_0_0;
static const Il2CppType* GenInst_IControlBuilderAccessor_t1240310411_0_0_0_Types[] = { &IControlBuilderAccessor_t1240310411_0_0_0 };
extern const Il2CppGenericInst GenInst_IControlBuilderAccessor_t1240310411_0_0_0 = { 1, GenInst_IControlBuilderAccessor_t1240310411_0_0_0_Types };
extern const Il2CppType IControlDesignerAccessor_t1554684719_0_0_0;
static const Il2CppType* GenInst_IControlDesignerAccessor_t1554684719_0_0_0_Types[] = { &IControlDesignerAccessor_t1554684719_0_0_0 };
extern const Il2CppGenericInst GenInst_IControlDesignerAccessor_t1554684719_0_0_0 = { 1, GenInst_IControlDesignerAccessor_t1554684719_0_0_0_Types };
extern const Il2CppType IDataBindingsAccessor_t834853709_0_0_0;
static const Il2CppType* GenInst_IDataBindingsAccessor_t834853709_0_0_0_Types[] = { &IDataBindingsAccessor_t834853709_0_0_0 };
extern const Il2CppGenericInst GenInst_IDataBindingsAccessor_t834853709_0_0_0 = { 1, GenInst_IDataBindingsAccessor_t834853709_0_0_0_Types };
extern const Il2CppType IExpressionsAccessor_t2801768539_0_0_0;
static const Il2CppType* GenInst_IExpressionsAccessor_t2801768539_0_0_0_Types[] = { &IExpressionsAccessor_t2801768539_0_0_0 };
extern const Il2CppGenericInst GenInst_IExpressionsAccessor_t2801768539_0_0_0 = { 1, GenInst_IExpressionsAccessor_t2801768539_0_0_0_Types };
extern const Il2CppType IParserAccessor_t2616046584_0_0_0;
static const Il2CppType* GenInst_IParserAccessor_t2616046584_0_0_0_Types[] = { &IParserAccessor_t2616046584_0_0_0 };
extern const Il2CppGenericInst GenInst_IParserAccessor_t2616046584_0_0_0 = { 1, GenInst_IParserAccessor_t2616046584_0_0_0_Types };
extern const Il2CppType IUrlResolutionService_t4016525173_0_0_0;
static const Il2CppType* GenInst_IUrlResolutionService_t4016525173_0_0_0_Types[] = { &IUrlResolutionService_t4016525173_0_0_0 };
extern const Il2CppGenericInst GenInst_IUrlResolutionService_t4016525173_0_0_0 = { 1, GenInst_IUrlResolutionService_t4016525173_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_PropertyInfo_t_0_0_0_Types[] = { &Type_t_0_0_0, &PropertyInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_PropertyInfo_t_0_0_0 = { 2, GenInst_Type_t_0_0_0_PropertyInfo_t_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_PropertyInfo_t_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Type_t_0_0_0, &PropertyInfo_t_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_PropertyInfo_t_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Type_t_0_0_0_PropertyInfo_t_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1234393886_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1234393886_0_0_0_Types[] = { &KeyValuePair_2_t1234393886_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1234393886_0_0_0 = { 1, GenInst_KeyValuePair_2_t1234393886_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_PropertyInfo_t_0_0_0_KeyValuePair_2_t1234393886_0_0_0_Types[] = { &Type_t_0_0_0, &PropertyInfo_t_0_0_0, &KeyValuePair_2_t1234393886_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_PropertyInfo_t_0_0_0_KeyValuePair_2_t1234393886_0_0_0 = { 3, GenInst_Type_t_0_0_0_PropertyInfo_t_0_0_0_KeyValuePair_2_t1234393886_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_PropertyInfo_t_0_0_0_Type_t_0_0_0_Types[] = { &Type_t_0_0_0, &PropertyInfo_t_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_PropertyInfo_t_0_0_0_Type_t_0_0_0 = { 3, GenInst_Type_t_0_0_0_PropertyInfo_t_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_PropertyInfo_t_0_0_0_PropertyInfo_t_0_0_0_Types[] = { &Type_t_0_0_0, &PropertyInfo_t_0_0_0, &PropertyInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_PropertyInfo_t_0_0_0_PropertyInfo_t_0_0_0 = { 3, GenInst_Type_t_0_0_0_PropertyInfo_t_0_0_0_PropertyInfo_t_0_0_0_Types };
extern const Il2CppType AddedTag_t1198678936_0_0_0;
static const Il2CppType* GenInst_AddedTag_t1198678936_0_0_0_Types[] = { &AddedTag_t1198678936_0_0_0 };
extern const Il2CppGenericInst GenInst_AddedTag_t1198678936_0_0_0 = { 1, GenInst_AddedTag_t1198678936_0_0_0_Types };
extern const Il2CppType AddedStyle_t611321135_0_0_0;
static const Il2CppType* GenInst_AddedStyle_t611321135_0_0_0_Types[] = { &AddedStyle_t611321135_0_0_0 };
extern const Il2CppGenericInst GenInst_AddedStyle_t611321135_0_0_0 = { 1, GenInst_AddedStyle_t611321135_0_0_0_Types };
extern const Il2CppType AddedAttr_t2359971688_0_0_0;
static const Il2CppType* GenInst_AddedAttr_t2359971688_0_0_0_Types[] = { &AddedAttr_t2359971688_0_0_0 };
extern const Il2CppGenericInst GenInst_AddedAttr_t2359971688_0_0_0 = { 1, GenInst_AddedAttr_t2359971688_0_0_0_Types };
extern const Il2CppType HtmlTag_t2430431696_0_0_0;
static const Il2CppType* GenInst_HtmlTag_t2430431696_0_0_0_Types[] = { &HtmlTag_t2430431696_0_0_0 };
extern const Il2CppGenericInst GenInst_HtmlTag_t2430431696_0_0_0 = { 1, GenInst_HtmlTag_t2430431696_0_0_0_Types };
extern const Il2CppType HtmlAttribute_t421148674_0_0_0;
static const Il2CppType* GenInst_HtmlAttribute_t421148674_0_0_0_Types[] = { &HtmlAttribute_t421148674_0_0_0 };
extern const Il2CppGenericInst GenInst_HtmlAttribute_t421148674_0_0_0 = { 1, GenInst_HtmlAttribute_t421148674_0_0_0_Types };
extern const Il2CppType HtmlStyle_t2610452647_0_0_0;
static const Il2CppType* GenInst_HtmlStyle_t2610452647_0_0_0_Types[] = { &HtmlStyle_t2610452647_0_0_0 };
extern const Il2CppGenericInst GenInst_HtmlStyle_t2610452647_0_0_0 = { 1, GenInst_HtmlStyle_t2610452647_0_0_0_Types };
extern const Il2CppType ObjectFormatter_t1981273209_0_0_0;
static const Il2CppType* GenInst_ObjectFormatter_t1981273209_0_0_0_Types[] = { &ObjectFormatter_t1981273209_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectFormatter_t1981273209_0_0_0 = { 1, GenInst_ObjectFormatter_t1981273209_0_0_0_Types };
extern const Il2CppType PageAsyncTask_t4073791294_0_0_0;
static const Il2CppType* GenInst_PageAsyncTask_t4073791294_0_0_0_Types[] = { &PageAsyncTask_t4073791294_0_0_0 };
extern const Il2CppGenericInst GenInst_PageAsyncTask_t4073791294_0_0_0 = { 1, GenInst_PageAsyncTask_t4073791294_0_0_0_Types };
extern const Il2CppType IAsyncResult_t767004451_0_0_0;
static const Il2CppType* GenInst_IAsyncResult_t767004451_0_0_0_Types[] = { &IAsyncResult_t767004451_0_0_0 };
extern const Il2CppGenericInst GenInst_IAsyncResult_t767004451_0_0_0 = { 1, GenInst_IAsyncResult_t767004451_0_0_0_Types };
extern const Il2CppType TemplateBinding_t3663525489_0_0_0;
static const Il2CppType* GenInst_TemplateBinding_t3663525489_0_0_0_Types[] = { &TemplateBinding_t3663525489_0_0_0 };
extern const Il2CppGenericInst GenInst_TemplateBinding_t3663525489_0_0_0 = { 1, GenInst_TemplateBinding_t3663525489_0_0_0_Types };
extern const Il2CppType Claim_t2327046348_0_0_0;
static const Il2CppType* GenInst_Claim_t2327046348_0_0_0_Types[] = { &Claim_t2327046348_0_0_0 };
extern const Il2CppGenericInst GenInst_Claim_t2327046348_0_0_0 = { 1, GenInst_Claim_t2327046348_0_0_0_Types };
extern const Il2CppType IAuthorizationPolicy_t3860878873_0_0_0;
static const Il2CppType* GenInst_IAuthorizationPolicy_t3860878873_0_0_0_Types[] = { &IAuthorizationPolicy_t3860878873_0_0_0 };
extern const Il2CppGenericInst GenInst_IAuthorizationPolicy_t3860878873_0_0_0 = { 1, GenInst_IAuthorizationPolicy_t3860878873_0_0_0_Types };
extern const Il2CppType IAuthorizationComponent_t1761415880_0_0_0;
static const Il2CppType* GenInst_IAuthorizationComponent_t1761415880_0_0_0_Types[] = { &IAuthorizationComponent_t1761415880_0_0_0 };
extern const Il2CppGenericInst GenInst_IAuthorizationComponent_t1761415880_0_0_0 = { 1, GenInst_IAuthorizationComponent_t1761415880_0_0_0_Types };
extern const Il2CppType ClaimSet_t3529661467_0_0_0;
static const Il2CppType* GenInst_ClaimSet_t3529661467_0_0_0_Types[] = { &ClaimSet_t3529661467_0_0_0 };
extern const Il2CppGenericInst GenInst_ClaimSet_t3529661467_0_0_0 = { 1, GenInst_ClaimSet_t3529661467_0_0_0_Types };
static const Il2CppType* GenInst_IAuthorizationPolicy_t3860878873_0_0_0_ClaimSet_t3529661467_0_0_0_Types[] = { &IAuthorizationPolicy_t3860878873_0_0_0, &ClaimSet_t3529661467_0_0_0 };
extern const Il2CppGenericInst GenInst_IAuthorizationPolicy_t3860878873_0_0_0_ClaimSet_t3529661467_0_0_0 = { 2, GenInst_IAuthorizationPolicy_t3860878873_0_0_0_ClaimSet_t3529661467_0_0_0_Types };
static const Il2CppType* GenInst_IAuthorizationPolicy_t3860878873_0_0_0_ClaimSet_t3529661467_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &IAuthorizationPolicy_t3860878873_0_0_0, &ClaimSet_t3529661467_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_IAuthorizationPolicy_t3860878873_0_0_0_ClaimSet_t3529661467_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_IAuthorizationPolicy_t3860878873_0_0_0_ClaimSet_t3529661467_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t749215797_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t749215797_0_0_0_Types[] = { &KeyValuePair_2_t749215797_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t749215797_0_0_0 = { 1, GenInst_KeyValuePair_2_t749215797_0_0_0_Types };
static const Il2CppType* GenInst_IAuthorizationPolicy_t3860878873_0_0_0_ClaimSet_t3529661467_0_0_0_KeyValuePair_2_t749215797_0_0_0_Types[] = { &IAuthorizationPolicy_t3860878873_0_0_0, &ClaimSet_t3529661467_0_0_0, &KeyValuePair_2_t749215797_0_0_0 };
extern const Il2CppGenericInst GenInst_IAuthorizationPolicy_t3860878873_0_0_0_ClaimSet_t3529661467_0_0_0_KeyValuePair_2_t749215797_0_0_0 = { 3, GenInst_IAuthorizationPolicy_t3860878873_0_0_0_ClaimSet_t3529661467_0_0_0_KeyValuePair_2_t749215797_0_0_0_Types };
static const Il2CppType* GenInst_IAuthorizationPolicy_t3860878873_0_0_0_ClaimSet_t3529661467_0_0_0_IAuthorizationPolicy_t3860878873_0_0_0_Types[] = { &IAuthorizationPolicy_t3860878873_0_0_0, &ClaimSet_t3529661467_0_0_0, &IAuthorizationPolicy_t3860878873_0_0_0 };
extern const Il2CppGenericInst GenInst_IAuthorizationPolicy_t3860878873_0_0_0_ClaimSet_t3529661467_0_0_0_IAuthorizationPolicy_t3860878873_0_0_0 = { 3, GenInst_IAuthorizationPolicy_t3860878873_0_0_0_ClaimSet_t3529661467_0_0_0_IAuthorizationPolicy_t3860878873_0_0_0_Types };
static const Il2CppType* GenInst_IAuthorizationPolicy_t3860878873_0_0_0_ClaimSet_t3529661467_0_0_0_ClaimSet_t3529661467_0_0_0_Types[] = { &IAuthorizationPolicy_t3860878873_0_0_0, &ClaimSet_t3529661467_0_0_0, &ClaimSet_t3529661467_0_0_0 };
extern const Il2CppGenericInst GenInst_IAuthorizationPolicy_t3860878873_0_0_0_ClaimSet_t3529661467_0_0_0_ClaimSet_t3529661467_0_0_0 = { 3, GenInst_IAuthorizationPolicy_t3860878873_0_0_0_ClaimSet_t3529661467_0_0_0_ClaimSet_t3529661467_0_0_0_Types };
extern const Il2CppType IIdentity_t2948385546_0_0_0;
static const Il2CppType* GenInst_IIdentity_t2948385546_0_0_0_Types[] = { &IIdentity_t2948385546_0_0_0 };
extern const Il2CppGenericInst GenInst_IIdentity_t2948385546_0_0_0 = { 1, GenInst_IIdentity_t2948385546_0_0_0_Types };
extern const Il2CppType SecurityToken_t1271873540_0_0_0;
static const Il2CppType* GenInst_SecurityToken_t1271873540_0_0_0_Types[] = { &SecurityToken_t1271873540_0_0_0 };
extern const Il2CppGenericInst GenInst_SecurityToken_t1271873540_0_0_0 = { 1, GenInst_SecurityToken_t1271873540_0_0_0_Types };
extern const Il2CppType SecurityKeyIdentifierClause_t1943429813_0_0_0;
static const Il2CppType* GenInst_SecurityKeyIdentifierClause_t1943429813_0_0_0_Types[] = { &SecurityKeyIdentifierClause_t1943429813_0_0_0 };
extern const Il2CppGenericInst GenInst_SecurityKeyIdentifierClause_t1943429813_0_0_0 = { 1, GenInst_SecurityKeyIdentifierClause_t1943429813_0_0_0_Types };
extern const Il2CppType SecurityKey_t4128675802_0_0_0;
static const Il2CppType* GenInst_SecurityKey_t4128675802_0_0_0_Types[] = { &SecurityKey_t4128675802_0_0_0 };
extern const Il2CppGenericInst GenInst_SecurityKey_t4128675802_0_0_0 = { 1, GenInst_SecurityKey_t4128675802_0_0_0_Types };
extern const Il2CppType jvalue_t1372148875_0_0_0;
static const Il2CppType* GenInst_jvalue_t1372148875_0_0_0_Types[] = { &jvalue_t1372148875_0_0_0 };
extern const Il2CppGenericInst GenInst_jvalue_t1372148875_0_0_0 = { 1, GenInst_jvalue_t1372148875_0_0_0_Types };
extern const Il2CppType AndroidJavaObject_t4131667876_0_0_0;
static const Il2CppType* GenInst_AndroidJavaObject_t4131667876_0_0_0_Types[] = { &AndroidJavaObject_t4131667876_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJavaObject_t4131667876_0_0_0 = { 1, GenInst_AndroidJavaObject_t4131667876_0_0_0_Types };
extern const Il2CppType Object_t631007953_0_0_0;
static const Il2CppType* GenInst_Object_t631007953_0_0_0_Types[] = { &Object_t631007953_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t631007953_0_0_0 = { 1, GenInst_Object_t631007953_0_0_0_Types };
extern const Il2CppType Camera_t4157153871_0_0_0;
static const Il2CppType* GenInst_Camera_t4157153871_0_0_0_Types[] = { &Camera_t4157153871_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t4157153871_0_0_0 = { 1, GenInst_Camera_t4157153871_0_0_0_Types };
extern const Il2CppType Behaviour_t1437897464_0_0_0;
static const Il2CppType* GenInst_Behaviour_t1437897464_0_0_0_Types[] = { &Behaviour_t1437897464_0_0_0 };
extern const Il2CppGenericInst GenInst_Behaviour_t1437897464_0_0_0 = { 1, GenInst_Behaviour_t1437897464_0_0_0_Types };
extern const Il2CppType Component_t1923634451_0_0_0;
static const Il2CppType* GenInst_Component_t1923634451_0_0_0_Types[] = { &Component_t1923634451_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t1923634451_0_0_0 = { 1, GenInst_Component_t1923634451_0_0_0_Types };
extern const Il2CppType Display_t1387065949_0_0_0;
static const Il2CppType* GenInst_Display_t1387065949_0_0_0_Types[] = { &Display_t1387065949_0_0_0 };
extern const Il2CppGenericInst GenInst_Display_t1387065949_0_0_0 = { 1, GenInst_Display_t1387065949_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t97287965_0_0_0_String_t_0_0_0_Types[] = { &Boolean_t97287965_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t97287965_0_0_0_String_t_0_0_0 = { 2, GenInst_Boolean_t97287965_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t97287965_0_0_0_Il2CppObject_0_0_0_Types[] = { &Boolean_t97287965_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t97287965_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Boolean_t97287965_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType AchievementDescription_t3217594527_0_0_0;
static const Il2CppType* GenInst_AchievementDescription_t3217594527_0_0_0_Types[] = { &AchievementDescription_t3217594527_0_0_0 };
extern const Il2CppGenericInst GenInst_AchievementDescription_t3217594527_0_0_0 = { 1, GenInst_AchievementDescription_t3217594527_0_0_0_Types };
extern const Il2CppType IAchievementDescription_t2514275728_0_0_0;
static const Il2CppType* GenInst_IAchievementDescription_t2514275728_0_0_0_Types[] = { &IAchievementDescription_t2514275728_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t2514275728_0_0_0 = { 1, GenInst_IAchievementDescription_t2514275728_0_0_0_Types };
extern const Il2CppType UserProfile_t3137328177_0_0_0;
static const Il2CppType* GenInst_UserProfile_t3137328177_0_0_0_Types[] = { &UserProfile_t3137328177_0_0_0 };
extern const Il2CppGenericInst GenInst_UserProfile_t3137328177_0_0_0 = { 1, GenInst_UserProfile_t3137328177_0_0_0_Types };
extern const Il2CppType IUserProfile_t360500636_0_0_0;
static const Il2CppType* GenInst_IUserProfile_t360500636_0_0_0_Types[] = { &IUserProfile_t360500636_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfile_t360500636_0_0_0 = { 1, GenInst_IUserProfile_t360500636_0_0_0_Types };
extern const Il2CppType GcLeaderboard_t4132273028_0_0_0;
static const Il2CppType* GenInst_GcLeaderboard_t4132273028_0_0_0_Types[] = { &GcLeaderboard_t4132273028_0_0_0 };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t4132273028_0_0_0 = { 1, GenInst_GcLeaderboard_t4132273028_0_0_0_Types };
extern const Il2CppType IAchievementDescriptionU5BU5D_t1821964849_0_0_0;
static const Il2CppType* GenInst_IAchievementDescriptionU5BU5D_t1821964849_0_0_0_Types[] = { &IAchievementDescriptionU5BU5D_t1821964849_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t1821964849_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t1821964849_0_0_0_Types };
extern const Il2CppType IAchievementU5BU5D_t1892338339_0_0_0;
static const Il2CppType* GenInst_IAchievementU5BU5D_t1892338339_0_0_0_Types[] = { &IAchievementU5BU5D_t1892338339_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t1892338339_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t1892338339_0_0_0_Types };
extern const Il2CppType IAchievement_t1421108358_0_0_0;
static const Il2CppType* GenInst_IAchievement_t1421108358_0_0_0_Types[] = { &IAchievement_t1421108358_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievement_t1421108358_0_0_0 = { 1, GenInst_IAchievement_t1421108358_0_0_0_Types };
extern const Il2CppType GcAchievementData_t675222246_0_0_0;
static const Il2CppType* GenInst_GcAchievementData_t675222246_0_0_0_Types[] = { &GcAchievementData_t675222246_0_0_0 };
extern const Il2CppGenericInst GenInst_GcAchievementData_t675222246_0_0_0 = { 1, GenInst_GcAchievementData_t675222246_0_0_0_Types };
extern const Il2CppType Achievement_t565359984_0_0_0;
static const Il2CppType* GenInst_Achievement_t565359984_0_0_0_Types[] = { &Achievement_t565359984_0_0_0 };
extern const Il2CppGenericInst GenInst_Achievement_t565359984_0_0_0 = { 1, GenInst_Achievement_t565359984_0_0_0_Types };
extern const Il2CppType IScoreU5BU5D_t527871248_0_0_0;
static const Il2CppType* GenInst_IScoreU5BU5D_t527871248_0_0_0_Types[] = { &IScoreU5BU5D_t527871248_0_0_0 };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t527871248_0_0_0 = { 1, GenInst_IScoreU5BU5D_t527871248_0_0_0_Types };
extern const Il2CppType IScore_t2559910621_0_0_0;
static const Il2CppType* GenInst_IScore_t2559910621_0_0_0_Types[] = { &IScore_t2559910621_0_0_0 };
extern const Il2CppGenericInst GenInst_IScore_t2559910621_0_0_0 = { 1, GenInst_IScore_t2559910621_0_0_0_Types };
extern const Il2CppType GcScoreData_t2125309831_0_0_0;
static const Il2CppType* GenInst_GcScoreData_t2125309831_0_0_0_Types[] = { &GcScoreData_t2125309831_0_0_0 };
extern const Il2CppGenericInst GenInst_GcScoreData_t2125309831_0_0_0 = { 1, GenInst_GcScoreData_t2125309831_0_0_0_Types };
extern const Il2CppType Score_t1968645328_0_0_0;
static const Il2CppType* GenInst_Score_t1968645328_0_0_0_Types[] = { &Score_t1968645328_0_0_0 };
extern const Il2CppGenericInst GenInst_Score_t1968645328_0_0_0 = { 1, GenInst_Score_t1968645328_0_0_0_Types };
extern const Il2CppType IUserProfileU5BU5D_t909679733_0_0_0;
static const Il2CppType* GenInst_IUserProfileU5BU5D_t909679733_0_0_0_Types[] = { &IUserProfileU5BU5D_t909679733_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t909679733_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t909679733_0_0_0_Types };
extern const Il2CppType Material_t340375123_0_0_0;
static const Il2CppType* GenInst_Material_t340375123_0_0_0_Types[] = { &Material_t340375123_0_0_0 };
extern const Il2CppGenericInst GenInst_Material_t340375123_0_0_0 = { 1, GenInst_Material_t340375123_0_0_0_Types };
extern const Il2CppType Vector2_t2156229523_0_0_0;
static const Il2CppType* GenInst_Vector2_t2156229523_0_0_0_Types[] = { &Vector2_t2156229523_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t2156229523_0_0_0 = { 1, GenInst_Vector2_t2156229523_0_0_0_Types };
extern const Il2CppType RenderBuffer_t586150500_0_0_0;
static const Il2CppType* GenInst_RenderBuffer_t586150500_0_0_0_Types[] = { &RenderBuffer_t586150500_0_0_0 };
extern const Il2CppGenericInst GenInst_RenderBuffer_t586150500_0_0_0 = { 1, GenInst_RenderBuffer_t586150500_0_0_0_Types };
extern const Il2CppType Plane_t1000493321_0_0_0;
static const Il2CppType* GenInst_Plane_t1000493321_0_0_0_Types[] = { &Plane_t1000493321_0_0_0 };
extern const Il2CppGenericInst GenInst_Plane_t1000493321_0_0_0 = { 1, GenInst_Plane_t1000493321_0_0_0_Types };
extern const Il2CppType Touch_t1921856868_0_0_0;
static const Il2CppType* GenInst_Touch_t1921856868_0_0_0_Types[] = { &Touch_t1921856868_0_0_0 };
extern const Il2CppGenericInst GenInst_Touch_t1921856868_0_0_0 = { 1, GenInst_Touch_t1921856868_0_0_0_Types };
extern const Il2CppType Keyframe_t4206410242_0_0_0;
static const Il2CppType* GenInst_Keyframe_t4206410242_0_0_0_Types[] = { &Keyframe_t4206410242_0_0_0 };
extern const Il2CppGenericInst GenInst_Keyframe_t4206410242_0_0_0 = { 1, GenInst_Keyframe_t4206410242_0_0_0_Types };
extern const Il2CppType Vector3_t3722313464_0_0_0;
static const Il2CppType* GenInst_Vector3_t3722313464_0_0_0_Types[] = { &Vector3_t3722313464_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t3722313464_0_0_0 = { 1, GenInst_Vector3_t3722313464_0_0_0_Types };
extern const Il2CppType Vector4_t3319028937_0_0_0;
static const Il2CppType* GenInst_Vector4_t3319028937_0_0_0_Types[] = { &Vector4_t3319028937_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t3319028937_0_0_0 = { 1, GenInst_Vector4_t3319028937_0_0_0_Types };
extern const Il2CppType Color_t2555686324_0_0_0;
static const Il2CppType* GenInst_Color_t2555686324_0_0_0_Types[] = { &Color_t2555686324_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t2555686324_0_0_0 = { 1, GenInst_Color_t2555686324_0_0_0_Types };
extern const Il2CppType Color32_t2600501292_0_0_0;
static const Il2CppType* GenInst_Color32_t2600501292_0_0_0_Types[] = { &Color32_t2600501292_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t2600501292_0_0_0 = { 1, GenInst_Color32_t2600501292_0_0_0_Types };
extern const Il2CppType RenderTargetIdentifier_t2079184500_0_0_0;
static const Il2CppType* GenInst_RenderTargetIdentifier_t2079184500_0_0_0_Types[] = { &RenderTargetIdentifier_t2079184500_0_0_0 };
extern const Il2CppGenericInst GenInst_RenderTargetIdentifier_t2079184500_0_0_0 = { 1, GenInst_RenderTargetIdentifier_t2079184500_0_0_0_Types };
extern const Il2CppType Scene_t2348375561_0_0_0;
extern const Il2CppType LoadSceneMode_t3251202195_0_0_0;
static const Il2CppType* GenInst_Scene_t2348375561_0_0_0_LoadSceneMode_t3251202195_0_0_0_Types[] = { &Scene_t2348375561_0_0_0, &LoadSceneMode_t3251202195_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t2348375561_0_0_0_LoadSceneMode_t3251202195_0_0_0 = { 2, GenInst_Scene_t2348375561_0_0_0_LoadSceneMode_t3251202195_0_0_0_Types };
static const Il2CppType* GenInst_Scene_t2348375561_0_0_0_Types[] = { &Scene_t2348375561_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t2348375561_0_0_0 = { 1, GenInst_Scene_t2348375561_0_0_0_Types };
static const Il2CppType* GenInst_Scene_t2348375561_0_0_0_Scene_t2348375561_0_0_0_Types[] = { &Scene_t2348375561_0_0_0, &Scene_t2348375561_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t2348375561_0_0_0_Scene_t2348375561_0_0_0 = { 2, GenInst_Scene_t2348375561_0_0_0_Scene_t2348375561_0_0_0_Types };
extern const Il2CppType ContactPoint_t3758755253_0_0_0;
static const Il2CppType* GenInst_ContactPoint_t3758755253_0_0_0_Types[] = { &ContactPoint_t3758755253_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint_t3758755253_0_0_0 = { 1, GenInst_ContactPoint_t3758755253_0_0_0_Types };
extern const Il2CppType RaycastHit_t1056001966_0_0_0;
static const Il2CppType* GenInst_RaycastHit_t1056001966_0_0_0_Types[] = { &RaycastHit_t1056001966_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit_t1056001966_0_0_0 = { 1, GenInst_RaycastHit_t1056001966_0_0_0_Types };
extern const Il2CppType Collider_t1773347010_0_0_0;
static const Il2CppType* GenInst_Collider_t1773347010_0_0_0_Types[] = { &Collider_t1773347010_0_0_0 };
extern const Il2CppGenericInst GenInst_Collider_t1773347010_0_0_0 = { 1, GenInst_Collider_t1773347010_0_0_0_Types };
extern const Il2CppType Rigidbody2D_t939494601_0_0_0;
static const Il2CppType* GenInst_Rigidbody2D_t939494601_0_0_0_Types[] = { &Rigidbody2D_t939494601_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody2D_t939494601_0_0_0 = { 1, GenInst_Rigidbody2D_t939494601_0_0_0_Types };
extern const Il2CppType RaycastHit2D_t2279581989_0_0_0;
static const Il2CppType* GenInst_RaycastHit2D_t2279581989_0_0_0_Types[] = { &RaycastHit2D_t2279581989_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t2279581989_0_0_0 = { 1, GenInst_RaycastHit2D_t2279581989_0_0_0_Types };
extern const Il2CppType Collider2D_t2806799626_0_0_0;
static const Il2CppType* GenInst_Collider2D_t2806799626_0_0_0_Types[] = { &Collider2D_t2806799626_0_0_0 };
extern const Il2CppGenericInst GenInst_Collider2D_t2806799626_0_0_0 = { 1, GenInst_Collider2D_t2806799626_0_0_0_Types };
extern const Il2CppType ContactPoint2D_t3390240644_0_0_0;
static const Il2CppType* GenInst_ContactPoint2D_t3390240644_0_0_0_Types[] = { &ContactPoint2D_t3390240644_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint2D_t3390240644_0_0_0 = { 1, GenInst_ContactPoint2D_t3390240644_0_0_0_Types };
extern const Il2CppType WebCamDevice_t1322781432_0_0_0;
static const Il2CppType* GenInst_WebCamDevice_t1322781432_0_0_0_Types[] = { &WebCamDevice_t1322781432_0_0_0 };
extern const Il2CppGenericInst GenInst_WebCamDevice_t1322781432_0_0_0 = { 1, GenInst_WebCamDevice_t1322781432_0_0_0_Types };
extern const Il2CppType AnimatorClipInfo_t3156717155_0_0_0;
static const Il2CppType* GenInst_AnimatorClipInfo_t3156717155_0_0_0_Types[] = { &AnimatorClipInfo_t3156717155_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimatorClipInfo_t3156717155_0_0_0 = { 1, GenInst_AnimatorClipInfo_t3156717155_0_0_0_Types };
extern const Il2CppType AnimatorControllerParameter_t1758260042_0_0_0;
static const Il2CppType* GenInst_AnimatorControllerParameter_t1758260042_0_0_0_Types[] = { &AnimatorControllerParameter_t1758260042_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimatorControllerParameter_t1758260042_0_0_0 = { 1, GenInst_AnimatorControllerParameter_t1758260042_0_0_0_Types };
extern const Il2CppType UIVertex_t4057497605_0_0_0;
static const Il2CppType* GenInst_UIVertex_t4057497605_0_0_0_Types[] = { &UIVertex_t4057497605_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t4057497605_0_0_0 = { 1, GenInst_UIVertex_t4057497605_0_0_0_Types };
extern const Il2CppType UICharInfo_t75501106_0_0_0;
static const Il2CppType* GenInst_UICharInfo_t75501106_0_0_0_Types[] = { &UICharInfo_t75501106_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t75501106_0_0_0 = { 1, GenInst_UICharInfo_t75501106_0_0_0_Types };
extern const Il2CppType UILineInfo_t4195266810_0_0_0;
static const Il2CppType* GenInst_UILineInfo_t4195266810_0_0_0_Types[] = { &UILineInfo_t4195266810_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t4195266810_0_0_0 = { 1, GenInst_UILineInfo_t4195266810_0_0_0_Types };
extern const Il2CppType Font_t1956802104_0_0_0;
static const Il2CppType* GenInst_Font_t1956802104_0_0_0_Types[] = { &Font_t1956802104_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t1956802104_0_0_0 = { 1, GenInst_Font_t1956802104_0_0_0_Types };
extern const Il2CppType GUILayoutOption_t811797299_0_0_0;
static const Il2CppType* GenInst_GUILayoutOption_t811797299_0_0_0_Types[] = { &GUILayoutOption_t811797299_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t811797299_0_0_0 = { 1, GenInst_GUILayoutOption_t811797299_0_0_0_Types };
extern const Il2CppType GUILayoutEntry_t3214611570_0_0_0;
static const Il2CppType* GenInst_GUILayoutEntry_t3214611570_0_0_0_Types[] = { &GUILayoutEntry_t3214611570_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t3214611570_0_0_0 = { 1, GenInst_GUILayoutEntry_t3214611570_0_0_0_Types };
extern const Il2CppType LayoutCache_t78309876_0_0_0;
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &LayoutCache_t78309876_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_Types };
static const Il2CppType* GenInst_LayoutCache_t78309876_0_0_0_Types[] = { &LayoutCache_t78309876_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutCache_t78309876_0_0_0 = { 1, GenInst_LayoutCache_t78309876_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &LayoutCache_t78309876_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1364695374_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1364695374_0_0_0_Types[] = { &KeyValuePair_2_t1364695374_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1364695374_0_0_0 = { 1, GenInst_KeyValuePair_2_t1364695374_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_KeyValuePair_2_t1364695374_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &LayoutCache_t78309876_0_0_0, &KeyValuePair_2_t1364695374_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_KeyValuePair_2_t1364695374_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_KeyValuePair_2_t1364695374_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &LayoutCache_t78309876_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_LayoutCache_t78309876_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &LayoutCache_t78309876_0_0_0, &LayoutCache_t78309876_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_LayoutCache_t78309876_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_LayoutCache_t78309876_0_0_0_Types };
extern const Il2CppType GUIStyle_t3956901511_0_0_0;
static const Il2CppType* GenInst_GUIStyle_t3956901511_0_0_0_Types[] = { &GUIStyle_t3956901511_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIStyle_t3956901511_0_0_0 = { 1, GenInst_GUIStyle_t3956901511_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t3956901511_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t3956901511_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1844862681_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1844862681_0_0_0_Types[] = { &KeyValuePair_2_t1844862681_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1844862681_0_0_0 = { 1, GenInst_KeyValuePair_2_t1844862681_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_KeyValuePair_2_t1844862681_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t3956901511_0_0_0, &KeyValuePair_2_t1844862681_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_KeyValuePair_2_t1844862681_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_KeyValuePair_2_t1844862681_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t3956901511_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_GUIStyle_t3956901511_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t3956901511_0_0_0, &GUIStyle_t3956901511_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_GUIStyle_t3956901511_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_GUIStyle_t3956901511_0_0_0_Types };
extern const Il2CppType Event_t2956885303_0_0_0;
extern const Il2CppType TextEditOp_t1927482598_0_0_0;
static const Il2CppType* GenInst_Event_t2956885303_0_0_0_TextEditOp_t1927482598_0_0_0_Types[] = { &Event_t2956885303_0_0_0, &TextEditOp_t1927482598_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t2956885303_0_0_0_TextEditOp_t1927482598_0_0_0 = { 2, GenInst_Event_t2956885303_0_0_0_TextEditOp_t1927482598_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t1927482598_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t1927482598_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t1927482598_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_TextEditOp_t1927482598_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1377593753_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1377593753_0_0_0_Types[] = { &KeyValuePair_2_t1377593753_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1377593753_0_0_0 = { 1, GenInst_KeyValuePair_2_t1377593753_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t1927482598_0_0_0_Types[] = { &TextEditOp_t1927482598_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t1927482598_0_0_0 = { 1, GenInst_TextEditOp_t1927482598_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t1927482598_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t1927482598_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t1927482598_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t1927482598_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t1927482598_0_0_0_TextEditOp_t1927482598_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t1927482598_0_0_0, &TextEditOp_t1927482598_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t1927482598_0_0_0_TextEditOp_t1927482598_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t1927482598_0_0_0_TextEditOp_t1927482598_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t1927482598_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t1927482598_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t1927482598_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t1927482598_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t1927482598_0_0_0_KeyValuePair_2_t1377593753_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t1927482598_0_0_0, &KeyValuePair_2_t1377593753_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t1927482598_0_0_0_KeyValuePair_2_t1377593753_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t1927482598_0_0_0_KeyValuePair_2_t1377593753_0_0_0_Types };
static const Il2CppType* GenInst_Event_t2956885303_0_0_0_Types[] = { &Event_t2956885303_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t2956885303_0_0_0 = { 1, GenInst_Event_t2956885303_0_0_0_Types };
static const Il2CppType* GenInst_Event_t2956885303_0_0_0_TextEditOp_t1927482598_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Event_t2956885303_0_0_0, &TextEditOp_t1927482598_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t2956885303_0_0_0_TextEditOp_t1927482598_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Event_t2956885303_0_0_0_TextEditOp_t1927482598_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1157710762_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1157710762_0_0_0_Types[] = { &KeyValuePair_2_t1157710762_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1157710762_0_0_0 = { 1, GenInst_KeyValuePair_2_t1157710762_0_0_0_Types };
static const Il2CppType* GenInst_Event_t2956885303_0_0_0_TextEditOp_t1927482598_0_0_0_KeyValuePair_2_t1157710762_0_0_0_Types[] = { &Event_t2956885303_0_0_0, &TextEditOp_t1927482598_0_0_0, &KeyValuePair_2_t1157710762_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t2956885303_0_0_0_TextEditOp_t1927482598_0_0_0_KeyValuePair_2_t1157710762_0_0_0 = { 3, GenInst_Event_t2956885303_0_0_0_TextEditOp_t1927482598_0_0_0_KeyValuePair_2_t1157710762_0_0_0_Types };
static const Il2CppType* GenInst_Event_t2956885303_0_0_0_TextEditOp_t1927482598_0_0_0_Event_t2956885303_0_0_0_Types[] = { &Event_t2956885303_0_0_0, &TextEditOp_t1927482598_0_0_0, &Event_t2956885303_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t2956885303_0_0_0_TextEditOp_t1927482598_0_0_0_Event_t2956885303_0_0_0 = { 3, GenInst_Event_t2956885303_0_0_0_TextEditOp_t1927482598_0_0_0_Event_t2956885303_0_0_0_Types };
static const Il2CppType* GenInst_Event_t2956885303_0_0_0_TextEditOp_t1927482598_0_0_0_TextEditOp_t1927482598_0_0_0_Types[] = { &Event_t2956885303_0_0_0, &TextEditOp_t1927482598_0_0_0, &TextEditOp_t1927482598_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t2956885303_0_0_0_TextEditOp_t1927482598_0_0_0_TextEditOp_t1927482598_0_0_0 = { 3, GenInst_Event_t2956885303_0_0_0_TextEditOp_t1927482598_0_0_0_TextEditOp_t1927482598_0_0_0_Types };
extern const Il2CppType DisallowMultipleComponent_t1422053217_0_0_0;
static const Il2CppType* GenInst_DisallowMultipleComponent_t1422053217_0_0_0_Types[] = { &DisallowMultipleComponent_t1422053217_0_0_0 };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t1422053217_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t1422053217_0_0_0_Types };
extern const Il2CppType ExecuteInEditMode_t3727731349_0_0_0;
static const Il2CppType* GenInst_ExecuteInEditMode_t3727731349_0_0_0_Types[] = { &ExecuteInEditMode_t3727731349_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t3727731349_0_0_0 = { 1, GenInst_ExecuteInEditMode_t3727731349_0_0_0_Types };
extern const Il2CppType RequireComponent_t3490506609_0_0_0;
static const Il2CppType* GenInst_RequireComponent_t3490506609_0_0_0_Types[] = { &RequireComponent_t3490506609_0_0_0 };
extern const Il2CppGenericInst GenInst_RequireComponent_t3490506609_0_0_0 = { 1, GenInst_RequireComponent_t3490506609_0_0_0_Types };
extern const Il2CppType HitInfo_t3229609740_0_0_0;
static const Il2CppType* GenInst_HitInfo_t3229609740_0_0_0_Types[] = { &HitInfo_t3229609740_0_0_0 };
extern const Il2CppGenericInst GenInst_HitInfo_t3229609740_0_0_0 = { 1, GenInst_HitInfo_t3229609740_0_0_0_Types };
extern const Il2CppType PersistentCall_t3407714124_0_0_0;
static const Il2CppType* GenInst_PersistentCall_t3407714124_0_0_0_Types[] = { &PersistentCall_t3407714124_0_0_0 };
extern const Il2CppGenericInst GenInst_PersistentCall_t3407714124_0_0_0 = { 1, GenInst_PersistentCall_t3407714124_0_0_0_Types };
extern const Il2CppType BaseInvokableCall_t2703961024_0_0_0;
static const Il2CppType* GenInst_BaseInvokableCall_t2703961024_0_0_0_Types[] = { &BaseInvokableCall_t2703961024_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t2703961024_0_0_0 = { 1, GenInst_BaseInvokableCall_t2703961024_0_0_0_Types };
extern const Il2CppType MessageTypeSubscribers_t1684935770_0_0_0;
static const Il2CppType* GenInst_MessageTypeSubscribers_t1684935770_0_0_0_Types[] = { &MessageTypeSubscribers_t1684935770_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageTypeSubscribers_t1684935770_0_0_0 = { 1, GenInst_MessageTypeSubscribers_t1684935770_0_0_0_Types };
static const Il2CppType* GenInst_MessageTypeSubscribers_t1684935770_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &MessageTypeSubscribers_t1684935770_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageTypeSubscribers_t1684935770_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_MessageTypeSubscribers_t1684935770_0_0_0_Boolean_t97287965_0_0_0_Types };
extern const Il2CppType MessageEventArgs_t1170575784_0_0_0;
static const Il2CppType* GenInst_MessageEventArgs_t1170575784_0_0_0_Types[] = { &MessageEventArgs_t1170575784_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageEventArgs_t1170575784_0_0_0 = { 1, GenInst_MessageEventArgs_t1170575784_0_0_0_Types };
extern const Il2CppType VuforiaAbstractBehaviour_t3510878193_0_0_0;
static const Il2CppType* GenInst_VuforiaAbstractBehaviour_t3510878193_0_0_0_Types[] = { &VuforiaAbstractBehaviour_t3510878193_0_0_0 };
extern const Il2CppGenericInst GenInst_VuforiaAbstractBehaviour_t3510878193_0_0_0 = { 1, GenInst_VuforiaAbstractBehaviour_t3510878193_0_0_0_Types };
extern const Il2CppType TrackableBehaviour_t1113559212_0_0_0;
static const Il2CppType* GenInst_TrackableBehaviour_t1113559212_0_0_0_Types[] = { &TrackableBehaviour_t1113559212_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableBehaviour_t1113559212_0_0_0 = { 1, GenInst_TrackableBehaviour_t1113559212_0_0_0_Types };
extern const Il2CppType MonoBehaviour_t3962482529_0_0_0;
static const Il2CppType* GenInst_MonoBehaviour_t3962482529_0_0_0_Types[] = { &MonoBehaviour_t3962482529_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t3962482529_0_0_0 = { 1, GenInst_MonoBehaviour_t3962482529_0_0_0_Types };
extern const Il2CppType TrackableIdPair_t4227350457_0_0_0;
static const Il2CppType* GenInst_TrackableIdPair_t4227350457_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &TrackableIdPair_t4227350457_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableIdPair_t4227350457_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_TrackableIdPair_t4227350457_0_0_0_Boolean_t97287965_0_0_0_Types };
extern const Il2CppType GameObject_t1113636619_0_0_0;
static const Il2CppType* GenInst_GameObject_t1113636619_0_0_0_Types[] = { &GameObject_t1113636619_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t1113636619_0_0_0 = { 1, GenInst_GameObject_t1113636619_0_0_0_Types };
extern const Il2CppType RenderTexture_t2108887433_0_0_0;
static const Il2CppType* GenInst_RenderTexture_t2108887433_0_0_0_Types[] = { &RenderTexture_t2108887433_0_0_0 };
extern const Il2CppGenericInst GenInst_RenderTexture_t2108887433_0_0_0 = { 1, GenInst_RenderTexture_t2108887433_0_0_0_Types };
extern const Il2CppType Texture_t3661962703_0_0_0;
static const Il2CppType* GenInst_Texture_t3661962703_0_0_0_Types[] = { &Texture_t3661962703_0_0_0 };
extern const Il2CppGenericInst GenInst_Texture_t3661962703_0_0_0 = { 1, GenInst_Texture_t3661962703_0_0_0_Types };
extern const Il2CppType EyewearCalibrationReading_t664929988_0_0_0;
static const Il2CppType* GenInst_EyewearCalibrationReading_t664929988_0_0_0_Types[] = { &EyewearCalibrationReading_t664929988_0_0_0 };
extern const Il2CppGenericInst GenInst_EyewearCalibrationReading_t664929988_0_0_0 = { 1, GenInst_EyewearCalibrationReading_t664929988_0_0_0_Types };
extern const Il2CppType VideoBackgroundAbstractBehaviour_t293578642_0_0_0;
static const Il2CppType* GenInst_Camera_t4157153871_0_0_0_VideoBackgroundAbstractBehaviour_t293578642_0_0_0_Types[] = { &Camera_t4157153871_0_0_0, &VideoBackgroundAbstractBehaviour_t293578642_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t4157153871_0_0_0_VideoBackgroundAbstractBehaviour_t293578642_0_0_0 = { 2, GenInst_Camera_t4157153871_0_0_0_VideoBackgroundAbstractBehaviour_t293578642_0_0_0_Types };
static const Il2CppType* GenInst_VideoBackgroundAbstractBehaviour_t293578642_0_0_0_Types[] = { &VideoBackgroundAbstractBehaviour_t293578642_0_0_0 };
extern const Il2CppGenericInst GenInst_VideoBackgroundAbstractBehaviour_t293578642_0_0_0 = { 1, GenInst_VideoBackgroundAbstractBehaviour_t293578642_0_0_0_Types };
static const Il2CppType* GenInst_Camera_t4157153871_0_0_0_VideoBackgroundAbstractBehaviour_t293578642_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Camera_t4157153871_0_0_0, &VideoBackgroundAbstractBehaviour_t293578642_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t4157153871_0_0_0_VideoBackgroundAbstractBehaviour_t293578642_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Camera_t4157153871_0_0_0_VideoBackgroundAbstractBehaviour_t293578642_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2904705502_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2904705502_0_0_0_Types[] = { &KeyValuePair_2_t2904705502_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2904705502_0_0_0 = { 1, GenInst_KeyValuePair_2_t2904705502_0_0_0_Types };
static const Il2CppType* GenInst_Camera_t4157153871_0_0_0_VideoBackgroundAbstractBehaviour_t293578642_0_0_0_KeyValuePair_2_t2904705502_0_0_0_Types[] = { &Camera_t4157153871_0_0_0, &VideoBackgroundAbstractBehaviour_t293578642_0_0_0, &KeyValuePair_2_t2904705502_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t4157153871_0_0_0_VideoBackgroundAbstractBehaviour_t293578642_0_0_0_KeyValuePair_2_t2904705502_0_0_0 = { 3, GenInst_Camera_t4157153871_0_0_0_VideoBackgroundAbstractBehaviour_t293578642_0_0_0_KeyValuePair_2_t2904705502_0_0_0_Types };
static const Il2CppType* GenInst_Camera_t4157153871_0_0_0_VideoBackgroundAbstractBehaviour_t293578642_0_0_0_Camera_t4157153871_0_0_0_Types[] = { &Camera_t4157153871_0_0_0, &VideoBackgroundAbstractBehaviour_t293578642_0_0_0, &Camera_t4157153871_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t4157153871_0_0_0_VideoBackgroundAbstractBehaviour_t293578642_0_0_0_Camera_t4157153871_0_0_0 = { 3, GenInst_Camera_t4157153871_0_0_0_VideoBackgroundAbstractBehaviour_t293578642_0_0_0_Camera_t4157153871_0_0_0_Types };
static const Il2CppType* GenInst_Camera_t4157153871_0_0_0_VideoBackgroundAbstractBehaviour_t293578642_0_0_0_VideoBackgroundAbstractBehaviour_t293578642_0_0_0_Types[] = { &Camera_t4157153871_0_0_0, &VideoBackgroundAbstractBehaviour_t293578642_0_0_0, &VideoBackgroundAbstractBehaviour_t293578642_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t4157153871_0_0_0_VideoBackgroundAbstractBehaviour_t293578642_0_0_0_VideoBackgroundAbstractBehaviour_t293578642_0_0_0 = { 3, GenInst_Camera_t4157153871_0_0_0_VideoBackgroundAbstractBehaviour_t293578642_0_0_0_VideoBackgroundAbstractBehaviour_t293578642_0_0_0_Types };
extern const Il2CppType Renderer_t2627027031_0_0_0;
static const Il2CppType* GenInst_Renderer_t2627027031_0_0_0_Types[] = { &Renderer_t2627027031_0_0_0 };
extern const Il2CppGenericInst GenInst_Renderer_t2627027031_0_0_0 = { 1, GenInst_Renderer_t2627027031_0_0_0_Types };
extern const Il2CppType PoseAgeEntry_t2181165958_0_0_0;
static const Il2CppType* GenInst_TrackableIdPair_t4227350457_0_0_0_PoseAgeEntry_t2181165958_0_0_0_Types[] = { &TrackableIdPair_t4227350457_0_0_0, &PoseAgeEntry_t2181165958_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableIdPair_t4227350457_0_0_0_PoseAgeEntry_t2181165958_0_0_0 = { 2, GenInst_TrackableIdPair_t4227350457_0_0_0_PoseAgeEntry_t2181165958_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3558069120_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3558069120_0_0_0_Types[] = { &KeyValuePair_2_t3558069120_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3558069120_0_0_0 = { 1, GenInst_KeyValuePair_2_t3558069120_0_0_0_Types };
static const Il2CppType* GenInst_PoseAgeEntry_t2181165958_0_0_0_Types[] = { &PoseAgeEntry_t2181165958_0_0_0 };
extern const Il2CppGenericInst GenInst_PoseAgeEntry_t2181165958_0_0_0 = { 1, GenInst_PoseAgeEntry_t2181165958_0_0_0_Types };
static const Il2CppType* GenInst_TrackableIdPair_t4227350457_0_0_0_Types[] = { &TrackableIdPair_t4227350457_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableIdPair_t4227350457_0_0_0 = { 1, GenInst_TrackableIdPair_t4227350457_0_0_0_Types };
static const Il2CppType* GenInst_TrackableIdPair_t4227350457_0_0_0_PoseAgeEntry_t2181165958_0_0_0_TrackableIdPair_t4227350457_0_0_0_Types[] = { &TrackableIdPair_t4227350457_0_0_0, &PoseAgeEntry_t2181165958_0_0_0, &TrackableIdPair_t4227350457_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableIdPair_t4227350457_0_0_0_PoseAgeEntry_t2181165958_0_0_0_TrackableIdPair_t4227350457_0_0_0 = { 3, GenInst_TrackableIdPair_t4227350457_0_0_0_PoseAgeEntry_t2181165958_0_0_0_TrackableIdPair_t4227350457_0_0_0_Types };
static const Il2CppType* GenInst_TrackableIdPair_t4227350457_0_0_0_PoseAgeEntry_t2181165958_0_0_0_PoseAgeEntry_t2181165958_0_0_0_Types[] = { &TrackableIdPair_t4227350457_0_0_0, &PoseAgeEntry_t2181165958_0_0_0, &PoseAgeEntry_t2181165958_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableIdPair_t4227350457_0_0_0_PoseAgeEntry_t2181165958_0_0_0_PoseAgeEntry_t2181165958_0_0_0 = { 3, GenInst_TrackableIdPair_t4227350457_0_0_0_PoseAgeEntry_t2181165958_0_0_0_PoseAgeEntry_t2181165958_0_0_0_Types };
static const Il2CppType* GenInst_TrackableIdPair_t4227350457_0_0_0_PoseAgeEntry_t2181165958_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &TrackableIdPair_t4227350457_0_0_0, &PoseAgeEntry_t2181165958_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableIdPair_t4227350457_0_0_0_PoseAgeEntry_t2181165958_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_TrackableIdPair_t4227350457_0_0_0_PoseAgeEntry_t2181165958_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_TrackableIdPair_t4227350457_0_0_0_PoseAgeEntry_t2181165958_0_0_0_KeyValuePair_2_t3558069120_0_0_0_Types[] = { &TrackableIdPair_t4227350457_0_0_0, &PoseAgeEntry_t2181165958_0_0_0, &KeyValuePair_2_t3558069120_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableIdPair_t4227350457_0_0_0_PoseAgeEntry_t2181165958_0_0_0_KeyValuePair_2_t3558069120_0_0_0 = { 3, GenInst_TrackableIdPair_t4227350457_0_0_0_PoseAgeEntry_t2181165958_0_0_0_KeyValuePair_2_t3558069120_0_0_0_Types };
extern const Il2CppType Link_t3080106562_0_0_0;
static const Il2CppType* GenInst_Link_t3080106562_0_0_0_Types[] = { &Link_t3080106562_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t3080106562_0_0_0 = { 1, GenInst_Link_t3080106562_0_0_0_Types };
extern const Il2CppType PoseInfo_t1612729179_0_0_0;
static const Il2CppType* GenInst_TrackableIdPair_t4227350457_0_0_0_PoseInfo_t1612729179_0_0_0_Types[] = { &TrackableIdPair_t4227350457_0_0_0, &PoseInfo_t1612729179_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableIdPair_t4227350457_0_0_0_PoseInfo_t1612729179_0_0_0 = { 2, GenInst_TrackableIdPair_t4227350457_0_0_0_PoseInfo_t1612729179_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2989632341_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2989632341_0_0_0_Types[] = { &KeyValuePair_2_t2989632341_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2989632341_0_0_0 = { 1, GenInst_KeyValuePair_2_t2989632341_0_0_0_Types };
static const Il2CppType* GenInst_PoseInfo_t1612729179_0_0_0_Types[] = { &PoseInfo_t1612729179_0_0_0 };
extern const Il2CppGenericInst GenInst_PoseInfo_t1612729179_0_0_0 = { 1, GenInst_PoseInfo_t1612729179_0_0_0_Types };
static const Il2CppType* GenInst_TrackableIdPair_t4227350457_0_0_0_PoseInfo_t1612729179_0_0_0_TrackableIdPair_t4227350457_0_0_0_Types[] = { &TrackableIdPair_t4227350457_0_0_0, &PoseInfo_t1612729179_0_0_0, &TrackableIdPair_t4227350457_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableIdPair_t4227350457_0_0_0_PoseInfo_t1612729179_0_0_0_TrackableIdPair_t4227350457_0_0_0 = { 3, GenInst_TrackableIdPair_t4227350457_0_0_0_PoseInfo_t1612729179_0_0_0_TrackableIdPair_t4227350457_0_0_0_Types };
static const Il2CppType* GenInst_TrackableIdPair_t4227350457_0_0_0_PoseInfo_t1612729179_0_0_0_PoseInfo_t1612729179_0_0_0_Types[] = { &TrackableIdPair_t4227350457_0_0_0, &PoseInfo_t1612729179_0_0_0, &PoseInfo_t1612729179_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableIdPair_t4227350457_0_0_0_PoseInfo_t1612729179_0_0_0_PoseInfo_t1612729179_0_0_0 = { 3, GenInst_TrackableIdPair_t4227350457_0_0_0_PoseInfo_t1612729179_0_0_0_PoseInfo_t1612729179_0_0_0_Types };
static const Il2CppType* GenInst_TrackableIdPair_t4227350457_0_0_0_PoseInfo_t1612729179_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &TrackableIdPair_t4227350457_0_0_0, &PoseInfo_t1612729179_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableIdPair_t4227350457_0_0_0_PoseInfo_t1612729179_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_TrackableIdPair_t4227350457_0_0_0_PoseInfo_t1612729179_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_TrackableIdPair_t4227350457_0_0_0_PoseInfo_t1612729179_0_0_0_KeyValuePair_2_t2989632341_0_0_0_Types[] = { &TrackableIdPair_t4227350457_0_0_0, &PoseInfo_t1612729179_0_0_0, &KeyValuePair_2_t2989632341_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableIdPair_t4227350457_0_0_0_PoseInfo_t1612729179_0_0_0_KeyValuePair_2_t2989632341_0_0_0 = { 3, GenInst_TrackableIdPair_t4227350457_0_0_0_PoseInfo_t1612729179_0_0_0_KeyValuePair_2_t2989632341_0_0_0_Types };
extern const Il2CppType Status_t1100905814_0_0_0;
static const Il2CppType* GenInst_TrackableIdPair_t4227350457_0_0_0_Status_t1100905814_0_0_0_Types[] = { &TrackableIdPair_t4227350457_0_0_0, &Status_t1100905814_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableIdPair_t4227350457_0_0_0_Status_t1100905814_0_0_0 = { 2, GenInst_TrackableIdPair_t4227350457_0_0_0_Status_t1100905814_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2477808976_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2477808976_0_0_0_Types[] = { &KeyValuePair_2_t2477808976_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2477808976_0_0_0 = { 1, GenInst_KeyValuePair_2_t2477808976_0_0_0_Types };
static const Il2CppType* GenInst_Status_t1100905814_0_0_0_Types[] = { &Status_t1100905814_0_0_0 };
extern const Il2CppGenericInst GenInst_Status_t1100905814_0_0_0 = { 1, GenInst_Status_t1100905814_0_0_0_Types };
static const Il2CppType* GenInst_TrackableIdPair_t4227350457_0_0_0_Status_t1100905814_0_0_0_TrackableIdPair_t4227350457_0_0_0_Types[] = { &TrackableIdPair_t4227350457_0_0_0, &Status_t1100905814_0_0_0, &TrackableIdPair_t4227350457_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableIdPair_t4227350457_0_0_0_Status_t1100905814_0_0_0_TrackableIdPair_t4227350457_0_0_0 = { 3, GenInst_TrackableIdPair_t4227350457_0_0_0_Status_t1100905814_0_0_0_TrackableIdPair_t4227350457_0_0_0_Types };
static const Il2CppType* GenInst_TrackableIdPair_t4227350457_0_0_0_Status_t1100905814_0_0_0_Status_t1100905814_0_0_0_Types[] = { &TrackableIdPair_t4227350457_0_0_0, &Status_t1100905814_0_0_0, &Status_t1100905814_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableIdPair_t4227350457_0_0_0_Status_t1100905814_0_0_0_Status_t1100905814_0_0_0 = { 3, GenInst_TrackableIdPair_t4227350457_0_0_0_Status_t1100905814_0_0_0_Status_t1100905814_0_0_0_Types };
static const Il2CppType* GenInst_TrackableIdPair_t4227350457_0_0_0_Status_t1100905814_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &TrackableIdPair_t4227350457_0_0_0, &Status_t1100905814_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableIdPair_t4227350457_0_0_0_Status_t1100905814_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_TrackableIdPair_t4227350457_0_0_0_Status_t1100905814_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_TrackableIdPair_t4227350457_0_0_0_Status_t1100905814_0_0_0_KeyValuePair_2_t2477808976_0_0_0_Types[] = { &TrackableIdPair_t4227350457_0_0_0, &Status_t1100905814_0_0_0, &KeyValuePair_2_t2477808976_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableIdPair_t4227350457_0_0_0_Status_t1100905814_0_0_0_KeyValuePair_2_t2477808976_0_0_0 = { 3, GenInst_TrackableIdPair_t4227350457_0_0_0_Status_t1100905814_0_0_0_KeyValuePair_2_t2477808976_0_0_0_Types };
extern const Il2CppType List_1_t2052726287_0_0_0;
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_List_1_t2052726287_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &List_1_t2052726287_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_List_1_t2052726287_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_List_1_t2052726287_0_0_0_Types };
extern const Il2CppType VuMarkAbstractBehaviour_t580651545_0_0_0;
static const Il2CppType* GenInst_VuMarkAbstractBehaviour_t580651545_0_0_0_Types[] = { &VuMarkAbstractBehaviour_t580651545_0_0_0 };
extern const Il2CppGenericInst GenInst_VuMarkAbstractBehaviour_t580651545_0_0_0 = { 1, GenInst_VuMarkAbstractBehaviour_t580651545_0_0_0_Types };
extern const Il2CppType DataSetTrackableBehaviour_t3430730379_0_0_0;
static const Il2CppType* GenInst_DataSetTrackableBehaviour_t3430730379_0_0_0_Types[] = { &DataSetTrackableBehaviour_t3430730379_0_0_0 };
extern const Il2CppGenericInst GenInst_DataSetTrackableBehaviour_t3430730379_0_0_0 = { 1, GenInst_DataSetTrackableBehaviour_t3430730379_0_0_0_Types };
extern const Il2CppType WorldCenterTrackableBehaviour_t632567575_0_0_0;
static const Il2CppType* GenInst_WorldCenterTrackableBehaviour_t632567575_0_0_0_Types[] = { &WorldCenterTrackableBehaviour_t632567575_0_0_0 };
extern const Il2CppGenericInst GenInst_WorldCenterTrackableBehaviour_t632567575_0_0_0 = { 1, GenInst_WorldCenterTrackableBehaviour_t632567575_0_0_0_Types };
extern const Il2CppType IEditDataSetBehaviour_t502520577_0_0_0;
static const Il2CppType* GenInst_IEditDataSetBehaviour_t502520577_0_0_0_Types[] = { &IEditDataSetBehaviour_t502520577_0_0_0 };
extern const Il2CppGenericInst GenInst_IEditDataSetBehaviour_t502520577_0_0_0 = { 1, GenInst_IEditDataSetBehaviour_t502520577_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t2052726287_0_0_0_Types[] = { &List_1_t2052726287_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2052726287_0_0_0 = { 1, GenInst_List_1_t2052726287_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_List_1_t2052726287_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &List_1_t2052726287_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_List_1_t2052726287_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_List_1_t2052726287_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3339111785_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3339111785_0_0_0_Types[] = { &KeyValuePair_2_t3339111785_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3339111785_0_0_0 = { 1, GenInst_KeyValuePair_2_t3339111785_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_List_1_t2052726287_0_0_0_KeyValuePair_2_t3339111785_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &List_1_t2052726287_0_0_0, &KeyValuePair_2_t3339111785_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_List_1_t2052726287_0_0_0_KeyValuePair_2_t3339111785_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_List_1_t2052726287_0_0_0_KeyValuePair_2_t3339111785_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_List_1_t2052726287_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &List_1_t2052726287_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_List_1_t2052726287_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_List_1_t2052726287_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_List_1_t2052726287_0_0_0_List_1_t2052726287_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &List_1_t2052726287_0_0_0, &List_1_t2052726287_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_List_1_t2052726287_0_0_0_List_1_t2052726287_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_List_1_t2052726287_0_0_0_List_1_t2052726287_0_0_0_Types };
extern const Il2CppType VuMarkTarget_t1129573803_0_0_0;
static const Il2CppType* GenInst_VuMarkTarget_t1129573803_0_0_0_Types[] = { &VuMarkTarget_t1129573803_0_0_0 };
extern const Il2CppGenericInst GenInst_VuMarkTarget_t1129573803_0_0_0 = { 1, GenInst_VuMarkTarget_t1129573803_0_0_0_Types };
extern const Il2CppType ObjectTarget_t3212252422_0_0_0;
static const Il2CppType* GenInst_ObjectTarget_t3212252422_0_0_0_Types[] = { &ObjectTarget_t3212252422_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectTarget_t3212252422_0_0_0 = { 1, GenInst_ObjectTarget_t3212252422_0_0_0_Types };
extern const Il2CppType ExtendedTrackable_t2690660116_0_0_0;
static const Il2CppType* GenInst_ExtendedTrackable_t2690660116_0_0_0_Types[] = { &ExtendedTrackable_t2690660116_0_0_0 };
extern const Il2CppGenericInst GenInst_ExtendedTrackable_t2690660116_0_0_0 = { 1, GenInst_ExtendedTrackable_t2690660116_0_0_0_Types };
extern const Il2CppType Trackable_t2451999991_0_0_0;
static const Il2CppType* GenInst_Trackable_t2451999991_0_0_0_Types[] = { &Trackable_t2451999991_0_0_0 };
extern const Il2CppGenericInst GenInst_Trackable_t2451999991_0_0_0 = { 1, GenInst_Trackable_t2451999991_0_0_0_Types };
extern const Il2CppType VuMarkTargetData_t3925829072_0_0_0;
static const Il2CppType* GenInst_VuMarkTargetData_t3925829072_0_0_0_Types[] = { &VuMarkTargetData_t3925829072_0_0_0 };
extern const Il2CppGenericInst GenInst_VuMarkTargetData_t3925829072_0_0_0 = { 1, GenInst_VuMarkTargetData_t3925829072_0_0_0_Types };
extern const Il2CppType VuMarkTargetResultData_t1414459591_0_0_0;
static const Il2CppType* GenInst_VuMarkTargetResultData_t1414459591_0_0_0_Types[] = { &VuMarkTargetResultData_t1414459591_0_0_0 };
extern const Il2CppGenericInst GenInst_VuMarkTargetResultData_t1414459591_0_0_0 = { 1, GenInst_VuMarkTargetResultData_t1414459591_0_0_0_Types };
extern const Il2CppType List_1_t128053199_0_0_0;
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_List_1_t128053199_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &List_1_t128053199_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_List_1_t128053199_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_List_1_t128053199_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t128053199_0_0_0_Types[] = { &List_1_t128053199_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t128053199_0_0_0 = { 1, GenInst_List_1_t128053199_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_List_1_t128053199_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &List_1_t128053199_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_List_1_t128053199_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_List_1_t128053199_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1414438697_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1414438697_0_0_0_Types[] = { &KeyValuePair_2_t1414438697_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1414438697_0_0_0 = { 1, GenInst_KeyValuePair_2_t1414438697_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_List_1_t128053199_0_0_0_KeyValuePair_2_t1414438697_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &List_1_t128053199_0_0_0, &KeyValuePair_2_t1414438697_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_List_1_t128053199_0_0_0_KeyValuePair_2_t1414438697_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_List_1_t128053199_0_0_0_KeyValuePair_2_t1414438697_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_List_1_t128053199_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &List_1_t128053199_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_List_1_t128053199_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_List_1_t128053199_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_List_1_t128053199_0_0_0_List_1_t128053199_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &List_1_t128053199_0_0_0, &List_1_t128053199_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_List_1_t128053199_0_0_0_List_1_t128053199_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_List_1_t128053199_0_0_0_List_1_t128053199_0_0_0_Types };
extern const Il2CppType TrackableResultData_t3800049328_0_0_0;
static const Il2CppType* GenInst_TrackableResultData_t3800049328_0_0_0_Types[] = { &TrackableResultData_t3800049328_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableResultData_t3800049328_0_0_0 = { 1, GenInst_TrackableResultData_t3800049328_0_0_0_Types };
extern const Il2CppType HideExcessAreaAbstractBehaviour_t3369390328_0_0_0;
static const Il2CppType* GenInst_HideExcessAreaAbstractBehaviour_t3369390328_0_0_0_Types[] = { &HideExcessAreaAbstractBehaviour_t3369390328_0_0_0 };
extern const Il2CppGenericInst GenInst_HideExcessAreaAbstractBehaviour_t3369390328_0_0_0 = { 1, GenInst_HideExcessAreaAbstractBehaviour_t3369390328_0_0_0_Types };
extern const Il2CppType IViewerParameters_t2017581997_0_0_0;
static const Il2CppType* GenInst_IViewerParameters_t2017581997_0_0_0_Types[] = { &IViewerParameters_t2017581997_0_0_0 };
extern const Il2CppGenericInst GenInst_IViewerParameters_t2017581997_0_0_0 = { 1, GenInst_IViewerParameters_t2017581997_0_0_0_Types };
extern const Il2CppType CameraField_t1483002240_0_0_0;
static const Il2CppType* GenInst_CameraField_t1483002240_0_0_0_Types[] = { &CameraField_t1483002240_0_0_0 };
extern const Il2CppGenericInst GenInst_CameraField_t1483002240_0_0_0 = { 1, GenInst_CameraField_t1483002240_0_0_0_Types };
extern const Il2CppType ICloudRecoEventHandler_t3872625445_0_0_0;
static const Il2CppType* GenInst_ICloudRecoEventHandler_t3872625445_0_0_0_Types[] = { &ICloudRecoEventHandler_t3872625445_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloudRecoEventHandler_t3872625445_0_0_0 = { 1, GenInst_ICloudRecoEventHandler_t3872625445_0_0_0_Types };
extern const Il2CppType TargetSearchResult_t3441982613_0_0_0;
static const Il2CppType* GenInst_TargetSearchResult_t3441982613_0_0_0_Types[] = { &TargetSearchResult_t3441982613_0_0_0 };
extern const Il2CppGenericInst GenInst_TargetSearchResult_t3441982613_0_0_0 = { 1, GenInst_TargetSearchResult_t3441982613_0_0_0_Types };
extern const Il2CppType ReconstructionAbstractBehaviour_t818896732_0_0_0;
static const Il2CppType* GenInst_ReconstructionAbstractBehaviour_t818896732_0_0_0_Types[] = { &ReconstructionAbstractBehaviour_t818896732_0_0_0 };
extern const Il2CppGenericInst GenInst_ReconstructionAbstractBehaviour_t818896732_0_0_0 = { 1, GenInst_ReconstructionAbstractBehaviour_t818896732_0_0_0_Types };
extern const Il2CppType InitError_t3420749710_0_0_0;
static const Il2CppType* GenInst_InitError_t3420749710_0_0_0_Types[] = { &InitError_t3420749710_0_0_0 };
extern const Il2CppGenericInst GenInst_InitError_t3420749710_0_0_0 = { 1, GenInst_InitError_t3420749710_0_0_0_Types };
extern const Il2CppType VirtualButton_t386166510_0_0_0;
static const Il2CppType* GenInst_VirtualButton_t386166510_0_0_0_Types[] = { &VirtualButton_t386166510_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButton_t386166510_0_0_0 = { 1, GenInst_VirtualButton_t386166510_0_0_0_Types };
extern const Il2CppType PIXEL_FORMAT_t3209881435_0_0_0;
extern const Il2CppType Image_t745056343_0_0_0;
static const Il2CppType* GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Image_t745056343_0_0_0_Types[] = { &PIXEL_FORMAT_t3209881435_0_0_0, &Image_t745056343_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Image_t745056343_0_0_0 = { 2, GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Image_t745056343_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Il2CppObject_0_0_0_Types[] = { &PIXEL_FORMAT_t3209881435_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4068375620_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4068375620_0_0_0_Types[] = { &KeyValuePair_2_t4068375620_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4068375620_0_0_0 = { 1, GenInst_KeyValuePair_2_t4068375620_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Types[] = { &PIXEL_FORMAT_t3209881435_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t3209881435_0_0_0 = { 1, GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Il2CppObject_0_0_0_PIXEL_FORMAT_t3209881435_0_0_0_Types[] = { &PIXEL_FORMAT_t3209881435_0_0_0, &Il2CppObject_0_0_0, &PIXEL_FORMAT_t3209881435_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Il2CppObject_0_0_0_PIXEL_FORMAT_t3209881435_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Il2CppObject_0_0_0_PIXEL_FORMAT_t3209881435_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &PIXEL_FORMAT_t3209881435_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &PIXEL_FORMAT_t3209881435_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4068375620_0_0_0_Types[] = { &PIXEL_FORMAT_t3209881435_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t4068375620_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4068375620_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4068375620_0_0_0_Types };
static const Il2CppType* GenInst_Image_t745056343_0_0_0_Types[] = { &Image_t745056343_0_0_0 };
extern const Il2CppGenericInst GenInst_Image_t745056343_0_0_0 = { 1, GenInst_Image_t745056343_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Image_t745056343_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &PIXEL_FORMAT_t3209881435_0_0_0, &Image_t745056343_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Image_t745056343_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Image_t745056343_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1733325799_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1733325799_0_0_0_Types[] = { &KeyValuePair_2_t1733325799_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1733325799_0_0_0 = { 1, GenInst_KeyValuePair_2_t1733325799_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Image_t745056343_0_0_0_KeyValuePair_2_t1733325799_0_0_0_Types[] = { &PIXEL_FORMAT_t3209881435_0_0_0, &Image_t745056343_0_0_0, &KeyValuePair_2_t1733325799_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Image_t745056343_0_0_0_KeyValuePair_2_t1733325799_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Image_t745056343_0_0_0_KeyValuePair_2_t1733325799_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Image_t745056343_0_0_0_PIXEL_FORMAT_t3209881435_0_0_0_Types[] = { &PIXEL_FORMAT_t3209881435_0_0_0, &Image_t745056343_0_0_0, &PIXEL_FORMAT_t3209881435_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Image_t745056343_0_0_0_PIXEL_FORMAT_t3209881435_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Image_t745056343_0_0_0_PIXEL_FORMAT_t3209881435_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Image_t745056343_0_0_0_Image_t745056343_0_0_0_Types[] = { &PIXEL_FORMAT_t3209881435_0_0_0, &Image_t745056343_0_0_0, &Image_t745056343_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Image_t745056343_0_0_0_Image_t745056343_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Image_t745056343_0_0_0_Image_t745056343_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Trackable_t2451999991_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Trackable_t2451999991_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Trackable_t2451999991_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_Trackable_t2451999991_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Trackable_t2451999991_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Trackable_t2451999991_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Trackable_t2451999991_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Trackable_t2451999991_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3738385489_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3738385489_0_0_0_Types[] = { &KeyValuePair_2_t3738385489_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3738385489_0_0_0 = { 1, GenInst_KeyValuePair_2_t3738385489_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Trackable_t2451999991_0_0_0_KeyValuePair_2_t3738385489_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Trackable_t2451999991_0_0_0, &KeyValuePair_2_t3738385489_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Trackable_t2451999991_0_0_0_KeyValuePair_2_t3738385489_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Trackable_t2451999991_0_0_0_KeyValuePair_2_t3738385489_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Trackable_t2451999991_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Trackable_t2451999991_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Trackable_t2451999991_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Trackable_t2451999991_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Trackable_t2451999991_0_0_0_Trackable_t2451999991_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Trackable_t2451999991_0_0_0, &Trackable_t2451999991_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Trackable_t2451999991_0_0_0_Trackable_t2451999991_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Trackable_t2451999991_0_0_0_Trackable_t2451999991_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_VirtualButton_t386166510_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &VirtualButton_t386166510_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_VirtualButton_t386166510_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_VirtualButton_t386166510_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_VirtualButton_t386166510_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &VirtualButton_t386166510_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_VirtualButton_t386166510_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_VirtualButton_t386166510_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1672552008_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1672552008_0_0_0_Types[] = { &KeyValuePair_2_t1672552008_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1672552008_0_0_0 = { 1, GenInst_KeyValuePair_2_t1672552008_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_VirtualButton_t386166510_0_0_0_KeyValuePair_2_t1672552008_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &VirtualButton_t386166510_0_0_0, &KeyValuePair_2_t1672552008_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_VirtualButton_t386166510_0_0_0_KeyValuePair_2_t1672552008_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_VirtualButton_t386166510_0_0_0_KeyValuePair_2_t1672552008_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_VirtualButton_t386166510_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &VirtualButton_t386166510_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_VirtualButton_t386166510_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_VirtualButton_t386166510_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_VirtualButton_t386166510_0_0_0_VirtualButton_t386166510_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &VirtualButton_t386166510_0_0_0, &VirtualButton_t386166510_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_VirtualButton_t386166510_0_0_0_VirtualButton_t386166510_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_VirtualButton_t386166510_0_0_0_VirtualButton_t386166510_0_0_0_Types };
extern const Il2CppType DataSet_t3286034874_0_0_0;
static const Il2CppType* GenInst_DataSet_t3286034874_0_0_0_Types[] = { &DataSet_t3286034874_0_0_0 };
extern const Il2CppGenericInst GenInst_DataSet_t3286034874_0_0_0 = { 1, GenInst_DataSet_t3286034874_0_0_0_Types };
extern const Il2CppType DataSetImpl_t2094717509_0_0_0;
static const Il2CppType* GenInst_DataSetImpl_t2094717509_0_0_0_Types[] = { &DataSetImpl_t2094717509_0_0_0 };
extern const Il2CppGenericInst GenInst_DataSetImpl_t2094717509_0_0_0 = { 1, GenInst_DataSetImpl_t2094717509_0_0_0_Types };
extern const Il2CppType WordData_t2883825721_0_0_0;
static const Il2CppType* GenInst_WordData_t2883825721_0_0_0_Types[] = { &WordData_t2883825721_0_0_0 };
extern const Il2CppGenericInst GenInst_WordData_t2883825721_0_0_0 = { 1, GenInst_WordData_t2883825721_0_0_0_Types };
extern const Il2CppType WordResultData_t1835118957_0_0_0;
static const Il2CppType* GenInst_WordResultData_t1835118957_0_0_0_Types[] = { &WordResultData_t1835118957_0_0_0 };
extern const Il2CppGenericInst GenInst_WordResultData_t1835118957_0_0_0 = { 1, GenInst_WordResultData_t1835118957_0_0_0_Types };
extern const Il2CppType SmartTerrainRevisionData_t2744445717_0_0_0;
static const Il2CppType* GenInst_SmartTerrainRevisionData_t2744445717_0_0_0_Types[] = { &SmartTerrainRevisionData_t2744445717_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainRevisionData_t2744445717_0_0_0 = { 1, GenInst_SmartTerrainRevisionData_t2744445717_0_0_0_Types };
extern const Il2CppType SurfaceData_t1300926610_0_0_0;
static const Il2CppType* GenInst_SurfaceData_t1300926610_0_0_0_Types[] = { &SurfaceData_t1300926610_0_0_0 };
extern const Il2CppGenericInst GenInst_SurfaceData_t1300926610_0_0_0 = { 1, GenInst_SurfaceData_t1300926610_0_0_0_Types };
extern const Il2CppType PropData_t489181770_0_0_0;
static const Il2CppType* GenInst_PropData_t489181770_0_0_0_Types[] = { &PropData_t489181770_0_0_0 };
extern const Il2CppGenericInst GenInst_PropData_t489181770_0_0_0 = { 1, GenInst_PropData_t489181770_0_0_0_Types };
extern const Il2CppType SmartTerrainTrackableBehaviour_t495369070_0_0_0;
static const Il2CppType* GenInst_SmartTerrainTrackableBehaviour_t495369070_0_0_0_Types[] = { &SmartTerrainTrackableBehaviour_t495369070_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainTrackableBehaviour_t495369070_0_0_0 = { 1, GenInst_SmartTerrainTrackableBehaviour_t495369070_0_0_0_Types };
extern const Il2CppType SmartTerrainTrackable_t3754799548_0_0_0;
static const Il2CppType* GenInst_SmartTerrainTrackable_t3754799548_0_0_0_Types[] = { &SmartTerrainTrackable_t3754799548_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainTrackable_t3754799548_0_0_0 = { 1, GenInst_SmartTerrainTrackable_t3754799548_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_UInt16_t2177724958_0_0_0_Types[] = { &Type_t_0_0_0, &UInt16_t2177724958_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_UInt16_t2177724958_0_0_0 = { 2, GenInst_Type_t_0_0_0_UInt16_t2177724958_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_UInt16_t2177724958_0_0_0_Types[] = { &Il2CppObject_0_0_0, &UInt16_t2177724958_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_UInt16_t2177724958_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_UInt16_t2177724958_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1627836113_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1627836113_0_0_0_Types[] = { &KeyValuePair_2_t1627836113_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1627836113_0_0_0 = { 1, GenInst_KeyValuePair_2_t1627836113_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_UInt16_t2177724958_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &UInt16_t2177724958_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_UInt16_t2177724958_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_UInt16_t2177724958_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_UInt16_t2177724958_0_0_0_UInt16_t2177724958_0_0_0_Types[] = { &Il2CppObject_0_0_0, &UInt16_t2177724958_0_0_0, &UInt16_t2177724958_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_UInt16_t2177724958_0_0_0_UInt16_t2177724958_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_UInt16_t2177724958_0_0_0_UInt16_t2177724958_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_UInt16_t2177724958_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Il2CppObject_0_0_0, &UInt16_t2177724958_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_UInt16_t2177724958_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_UInt16_t2177724958_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_UInt16_t2177724958_0_0_0_KeyValuePair_2_t1627836113_0_0_0_Types[] = { &Il2CppObject_0_0_0, &UInt16_t2177724958_0_0_0, &KeyValuePair_2_t1627836113_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_UInt16_t2177724958_0_0_0_KeyValuePair_2_t1627836113_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_UInt16_t2177724958_0_0_0_KeyValuePair_2_t1627836113_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_UInt16_t2177724958_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Type_t_0_0_0, &UInt16_t2177724958_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_UInt16_t2177724958_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Type_t_0_0_0_UInt16_t2177724958_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2724776893_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2724776893_0_0_0_Types[] = { &KeyValuePair_2_t2724776893_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2724776893_0_0_0 = { 1, GenInst_KeyValuePair_2_t2724776893_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_UInt16_t2177724958_0_0_0_KeyValuePair_2_t2724776893_0_0_0_Types[] = { &Type_t_0_0_0, &UInt16_t2177724958_0_0_0, &KeyValuePair_2_t2724776893_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_UInt16_t2177724958_0_0_0_KeyValuePair_2_t2724776893_0_0_0 = { 3, GenInst_Type_t_0_0_0_UInt16_t2177724958_0_0_0_KeyValuePair_2_t2724776893_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_UInt16_t2177724958_0_0_0_Type_t_0_0_0_Types[] = { &Type_t_0_0_0, &UInt16_t2177724958_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_UInt16_t2177724958_0_0_0_Type_t_0_0_0 = { 3, GenInst_Type_t_0_0_0_UInt16_t2177724958_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_UInt16_t2177724958_0_0_0_UInt16_t2177724958_0_0_0_Types[] = { &Type_t_0_0_0, &UInt16_t2177724958_0_0_0, &UInt16_t2177724958_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_UInt16_t2177724958_0_0_0_UInt16_t2177724958_0_0_0 = { 3, GenInst_Type_t_0_0_0_UInt16_t2177724958_0_0_0_UInt16_t2177724958_0_0_0_Types };
extern const Il2CppType RectangleData_t1039179782_0_0_0;
static const Il2CppType* GenInst_RectangleData_t1039179782_0_0_0_Types[] = { &RectangleData_t1039179782_0_0_0 };
extern const Il2CppGenericInst GenInst_RectangleData_t1039179782_0_0_0 = { 1, GenInst_RectangleData_t1039179782_0_0_0_Types };
extern const Il2CppType WordResult_t3640773802_0_0_0;
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_WordResult_t3640773802_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &WordResult_t3640773802_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_WordResult_t3640773802_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_WordResult_t3640773802_0_0_0_Types };
static const Il2CppType* GenInst_WordResult_t3640773802_0_0_0_Types[] = { &WordResult_t3640773802_0_0_0 };
extern const Il2CppGenericInst GenInst_WordResult_t3640773802_0_0_0 = { 1, GenInst_WordResult_t3640773802_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_WordResult_t3640773802_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &WordResult_t3640773802_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_WordResult_t3640773802_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_WordResult_t3640773802_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t632192004_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t632192004_0_0_0_Types[] = { &KeyValuePair_2_t632192004_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t632192004_0_0_0 = { 1, GenInst_KeyValuePair_2_t632192004_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_WordResult_t3640773802_0_0_0_KeyValuePair_2_t632192004_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &WordResult_t3640773802_0_0_0, &KeyValuePair_2_t632192004_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_WordResult_t3640773802_0_0_0_KeyValuePair_2_t632192004_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_WordResult_t3640773802_0_0_0_KeyValuePair_2_t632192004_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_WordResult_t3640773802_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &WordResult_t3640773802_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_WordResult_t3640773802_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_WordResult_t3640773802_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_WordResult_t3640773802_0_0_0_WordResult_t3640773802_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &WordResult_t3640773802_0_0_0, &WordResult_t3640773802_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_WordResult_t3640773802_0_0_0_WordResult_t3640773802_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_WordResult_t3640773802_0_0_0_WordResult_t3640773802_0_0_0_Types };
extern const Il2CppType Word_t1116038618_0_0_0;
static const Il2CppType* GenInst_Word_t1116038618_0_0_0_Types[] = { &Word_t1116038618_0_0_0 };
extern const Il2CppGenericInst GenInst_Word_t1116038618_0_0_0 = { 1, GenInst_Word_t1116038618_0_0_0_Types };
extern const Il2CppType WordAbstractBehaviour_t3502498754_0_0_0;
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_WordAbstractBehaviour_t3502498754_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &WordAbstractBehaviour_t3502498754_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_WordAbstractBehaviour_t3502498754_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_WordAbstractBehaviour_t3502498754_0_0_0_Types };
static const Il2CppType* GenInst_WordAbstractBehaviour_t3502498754_0_0_0_Types[] = { &WordAbstractBehaviour_t3502498754_0_0_0 };
extern const Il2CppGenericInst GenInst_WordAbstractBehaviour_t3502498754_0_0_0 = { 1, GenInst_WordAbstractBehaviour_t3502498754_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_WordAbstractBehaviour_t3502498754_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &WordAbstractBehaviour_t3502498754_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_WordAbstractBehaviour_t3502498754_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_WordAbstractBehaviour_t3502498754_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t493916956_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t493916956_0_0_0_Types[] = { &KeyValuePair_2_t493916956_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t493916956_0_0_0 = { 1, GenInst_KeyValuePair_2_t493916956_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_WordAbstractBehaviour_t3502498754_0_0_0_KeyValuePair_2_t493916956_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &WordAbstractBehaviour_t3502498754_0_0_0, &KeyValuePair_2_t493916956_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_WordAbstractBehaviour_t3502498754_0_0_0_KeyValuePair_2_t493916956_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_WordAbstractBehaviour_t3502498754_0_0_0_KeyValuePair_2_t493916956_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_WordAbstractBehaviour_t3502498754_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &WordAbstractBehaviour_t3502498754_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_WordAbstractBehaviour_t3502498754_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_WordAbstractBehaviour_t3502498754_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_WordAbstractBehaviour_t3502498754_0_0_0_WordAbstractBehaviour_t3502498754_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &WordAbstractBehaviour_t3502498754_0_0_0, &WordAbstractBehaviour_t3502498754_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_WordAbstractBehaviour_t3502498754_0_0_0_WordAbstractBehaviour_t3502498754_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_WordAbstractBehaviour_t3502498754_0_0_0_WordAbstractBehaviour_t3502498754_0_0_0_Types };
extern const Il2CppType List_1_t679606200_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t679606200_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t679606200_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t679606200_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t679606200_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t679606200_0_0_0_Types[] = { &List_1_t679606200_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t679606200_0_0_0 = { 1, GenInst_List_1_t679606200_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t679606200_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t679606200_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t679606200_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t679606200_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2862534666_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2862534666_0_0_0_Types[] = { &KeyValuePair_2_t2862534666_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2862534666_0_0_0 = { 1, GenInst_KeyValuePair_2_t2862534666_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t679606200_0_0_0_KeyValuePair_2_t2862534666_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t679606200_0_0_0, &KeyValuePair_2_t2862534666_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t679606200_0_0_0_KeyValuePair_2_t2862534666_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t679606200_0_0_0_KeyValuePair_2_t2862534666_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t679606200_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t679606200_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t679606200_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t679606200_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t679606200_0_0_0_List_1_t679606200_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t679606200_0_0_0, &List_1_t679606200_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t679606200_0_0_0_List_1_t679606200_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t679606200_0_0_0_List_1_t679606200_0_0_0_Types };
extern const Il2CppType ISmartTerrainEventHandler_t439660020_0_0_0;
static const Il2CppType* GenInst_ISmartTerrainEventHandler_t439660020_0_0_0_Types[] = { &ISmartTerrainEventHandler_t439660020_0_0_0 };
extern const Il2CppGenericInst GenInst_ISmartTerrainEventHandler_t439660020_0_0_0 = { 1, GenInst_ISmartTerrainEventHandler_t439660020_0_0_0_Types };
extern const Il2CppType SmartTerrainInitializationInfo_t1789741059_0_0_0;
static const Il2CppType* GenInst_SmartTerrainInitializationInfo_t1789741059_0_0_0_Types[] = { &SmartTerrainInitializationInfo_t1789741059_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainInitializationInfo_t1789741059_0_0_0 = { 1, GenInst_SmartTerrainInitializationInfo_t1789741059_0_0_0_Types };
extern const Il2CppType Prop_t3878399200_0_0_0;
static const Il2CppType* GenInst_Prop_t3878399200_0_0_0_Types[] = { &Prop_t3878399200_0_0_0 };
extern const Il2CppGenericInst GenInst_Prop_t3878399200_0_0_0 = { 1, GenInst_Prop_t3878399200_0_0_0_Types };
extern const Il2CppType Surface_t1158577034_0_0_0;
static const Il2CppType* GenInst_Surface_t1158577034_0_0_0_Types[] = { &Surface_t1158577034_0_0_0 };
extern const Il2CppGenericInst GenInst_Surface_t1158577034_0_0_0 = { 1, GenInst_Surface_t1158577034_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Surface_t1158577034_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Surface_t1158577034_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Surface_t1158577034_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_Surface_t1158577034_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Surface_t1158577034_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Surface_t1158577034_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Surface_t1158577034_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Surface_t1158577034_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2444962532_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2444962532_0_0_0_Types[] = { &KeyValuePair_2_t2444962532_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2444962532_0_0_0 = { 1, GenInst_KeyValuePair_2_t2444962532_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Surface_t1158577034_0_0_0_KeyValuePair_2_t2444962532_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Surface_t1158577034_0_0_0, &KeyValuePair_2_t2444962532_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Surface_t1158577034_0_0_0_KeyValuePair_2_t2444962532_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Surface_t1158577034_0_0_0_KeyValuePair_2_t2444962532_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Surface_t1158577034_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Surface_t1158577034_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Surface_t1158577034_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Surface_t1158577034_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Surface_t1158577034_0_0_0_Surface_t1158577034_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Surface_t1158577034_0_0_0, &Surface_t1158577034_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Surface_t1158577034_0_0_0_Surface_t1158577034_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Surface_t1158577034_0_0_0_Surface_t1158577034_0_0_0_Types };
extern const Il2CppType SurfaceAbstractBehaviour_t3611421852_0_0_0;
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_SurfaceAbstractBehaviour_t3611421852_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &SurfaceAbstractBehaviour_t3611421852_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_SurfaceAbstractBehaviour_t3611421852_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_SurfaceAbstractBehaviour_t3611421852_0_0_0_Types };
static const Il2CppType* GenInst_SurfaceAbstractBehaviour_t3611421852_0_0_0_Types[] = { &SurfaceAbstractBehaviour_t3611421852_0_0_0 };
extern const Il2CppGenericInst GenInst_SurfaceAbstractBehaviour_t3611421852_0_0_0 = { 1, GenInst_SurfaceAbstractBehaviour_t3611421852_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_SurfaceAbstractBehaviour_t3611421852_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &SurfaceAbstractBehaviour_t3611421852_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_SurfaceAbstractBehaviour_t3611421852_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_SurfaceAbstractBehaviour_t3611421852_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t602840054_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t602840054_0_0_0_Types[] = { &KeyValuePair_2_t602840054_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t602840054_0_0_0 = { 1, GenInst_KeyValuePair_2_t602840054_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_SurfaceAbstractBehaviour_t3611421852_0_0_0_KeyValuePair_2_t602840054_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &SurfaceAbstractBehaviour_t3611421852_0_0_0, &KeyValuePair_2_t602840054_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_SurfaceAbstractBehaviour_t3611421852_0_0_0_KeyValuePair_2_t602840054_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_SurfaceAbstractBehaviour_t3611421852_0_0_0_KeyValuePair_2_t602840054_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_SurfaceAbstractBehaviour_t3611421852_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &SurfaceAbstractBehaviour_t3611421852_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_SurfaceAbstractBehaviour_t3611421852_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_SurfaceAbstractBehaviour_t3611421852_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_SurfaceAbstractBehaviour_t3611421852_0_0_0_SurfaceAbstractBehaviour_t3611421852_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &SurfaceAbstractBehaviour_t3611421852_0_0_0, &SurfaceAbstractBehaviour_t3611421852_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_SurfaceAbstractBehaviour_t3611421852_0_0_0_SurfaceAbstractBehaviour_t3611421852_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_SurfaceAbstractBehaviour_t3611421852_0_0_0_SurfaceAbstractBehaviour_t3611421852_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Prop_t3878399200_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Prop_t3878399200_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Prop_t3878399200_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_Prop_t3878399200_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Prop_t3878399200_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Prop_t3878399200_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Prop_t3878399200_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Prop_t3878399200_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t869817402_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t869817402_0_0_0_Types[] = { &KeyValuePair_2_t869817402_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t869817402_0_0_0 = { 1, GenInst_KeyValuePair_2_t869817402_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Prop_t3878399200_0_0_0_KeyValuePair_2_t869817402_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Prop_t3878399200_0_0_0, &KeyValuePair_2_t869817402_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Prop_t3878399200_0_0_0_KeyValuePair_2_t869817402_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Prop_t3878399200_0_0_0_KeyValuePair_2_t869817402_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Prop_t3878399200_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Prop_t3878399200_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Prop_t3878399200_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Prop_t3878399200_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Prop_t3878399200_0_0_0_Prop_t3878399200_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Prop_t3878399200_0_0_0, &Prop_t3878399200_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Prop_t3878399200_0_0_0_Prop_t3878399200_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Prop_t3878399200_0_0_0_Prop_t3878399200_0_0_0_Types };
extern const Il2CppType PropAbstractBehaviour_t2080236229_0_0_0;
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_PropAbstractBehaviour_t2080236229_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &PropAbstractBehaviour_t2080236229_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_PropAbstractBehaviour_t2080236229_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_PropAbstractBehaviour_t2080236229_0_0_0_Types };
static const Il2CppType* GenInst_PropAbstractBehaviour_t2080236229_0_0_0_Types[] = { &PropAbstractBehaviour_t2080236229_0_0_0 };
extern const Il2CppGenericInst GenInst_PropAbstractBehaviour_t2080236229_0_0_0 = { 1, GenInst_PropAbstractBehaviour_t2080236229_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_PropAbstractBehaviour_t2080236229_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &PropAbstractBehaviour_t2080236229_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_PropAbstractBehaviour_t2080236229_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_PropAbstractBehaviour_t2080236229_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3366621727_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3366621727_0_0_0_Types[] = { &KeyValuePair_2_t3366621727_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3366621727_0_0_0 = { 1, GenInst_KeyValuePair_2_t3366621727_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_PropAbstractBehaviour_t2080236229_0_0_0_KeyValuePair_2_t3366621727_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &PropAbstractBehaviour_t2080236229_0_0_0, &KeyValuePair_2_t3366621727_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_PropAbstractBehaviour_t2080236229_0_0_0_KeyValuePair_2_t3366621727_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_PropAbstractBehaviour_t2080236229_0_0_0_KeyValuePair_2_t3366621727_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_PropAbstractBehaviour_t2080236229_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &PropAbstractBehaviour_t2080236229_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_PropAbstractBehaviour_t2080236229_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_PropAbstractBehaviour_t2080236229_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_PropAbstractBehaviour_t2080236229_0_0_0_PropAbstractBehaviour_t2080236229_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &PropAbstractBehaviour_t2080236229_0_0_0, &PropAbstractBehaviour_t2080236229_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_PropAbstractBehaviour_t2080236229_0_0_0_PropAbstractBehaviour_t2080236229_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_PropAbstractBehaviour_t2080236229_0_0_0_PropAbstractBehaviour_t2080236229_0_0_0_Types };
extern const Il2CppType MeshFilter_t3523625662_0_0_0;
static const Il2CppType* GenInst_MeshFilter_t3523625662_0_0_0_Types[] = { &MeshFilter_t3523625662_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshFilter_t3523625662_0_0_0 = { 1, GenInst_MeshFilter_t3523625662_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_TrackableBehaviour_t1113559212_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &TrackableBehaviour_t1113559212_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_TrackableBehaviour_t1113559212_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_TrackableBehaviour_t1113559212_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_TrackableBehaviour_t1113559212_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &TrackableBehaviour_t1113559212_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_TrackableBehaviour_t1113559212_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_TrackableBehaviour_t1113559212_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2399944710_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2399944710_0_0_0_Types[] = { &KeyValuePair_2_t2399944710_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2399944710_0_0_0 = { 1, GenInst_KeyValuePair_2_t2399944710_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_TrackableBehaviour_t1113559212_0_0_0_KeyValuePair_2_t2399944710_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &TrackableBehaviour_t1113559212_0_0_0, &KeyValuePair_2_t2399944710_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_TrackableBehaviour_t1113559212_0_0_0_KeyValuePair_2_t2399944710_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_TrackableBehaviour_t1113559212_0_0_0_KeyValuePair_2_t2399944710_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_TrackableBehaviour_t1113559212_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &TrackableBehaviour_t1113559212_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_TrackableBehaviour_t1113559212_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_TrackableBehaviour_t1113559212_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_TrackableBehaviour_t1113559212_0_0_0_TrackableBehaviour_t1113559212_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &TrackableBehaviour_t1113559212_0_0_0, &TrackableBehaviour_t1113559212_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_TrackableBehaviour_t1113559212_0_0_0_TrackableBehaviour_t1113559212_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_TrackableBehaviour_t1113559212_0_0_0_TrackableBehaviour_t1113559212_0_0_0_Types };
extern const Il2CppType VirtualButtonAbstractBehaviour_t2408702468_0_0_0;
static const Il2CppType* GenInst_VirtualButtonAbstractBehaviour_t2408702468_0_0_0_Types[] = { &VirtualButtonAbstractBehaviour_t2408702468_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonAbstractBehaviour_t2408702468_0_0_0 = { 1, GenInst_VirtualButtonAbstractBehaviour_t2408702468_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Status_t1100905814_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Status_t1100905814_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Status_t1100905814_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_Status_t1100905814_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2387291312_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2387291312_0_0_0_Types[] = { &KeyValuePair_2_t2387291312_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2387291312_0_0_0 = { 1, GenInst_KeyValuePair_2_t2387291312_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Status_t1100905814_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Status_t1100905814_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Status_t1100905814_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Status_t1100905814_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Status_t1100905814_0_0_0_Status_t1100905814_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Status_t1100905814_0_0_0, &Status_t1100905814_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Status_t1100905814_0_0_0_Status_t1100905814_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Status_t1100905814_0_0_0_Status_t1100905814_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Status_t1100905814_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Status_t1100905814_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Status_t1100905814_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Status_t1100905814_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Status_t1100905814_0_0_0_KeyValuePair_2_t2387291312_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Status_t1100905814_0_0_0, &KeyValuePair_2_t2387291312_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Status_t1100905814_0_0_0_KeyValuePair_2_t2387291312_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Status_t1100905814_0_0_0_KeyValuePair_2_t2387291312_0_0_0_Types };
extern const Il2CppType VirtualButtonData_t2901758114_0_0_0;
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_VirtualButtonData_t2901758114_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &VirtualButtonData_t2901758114_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_VirtualButtonData_t2901758114_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_VirtualButtonData_t2901758114_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4188143612_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4188143612_0_0_0_Types[] = { &KeyValuePair_2_t4188143612_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4188143612_0_0_0 = { 1, GenInst_KeyValuePair_2_t4188143612_0_0_0_Types };
static const Il2CppType* GenInst_VirtualButtonData_t2901758114_0_0_0_Types[] = { &VirtualButtonData_t2901758114_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonData_t2901758114_0_0_0 = { 1, GenInst_VirtualButtonData_t2901758114_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_VirtualButtonData_t2901758114_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &VirtualButtonData_t2901758114_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_VirtualButtonData_t2901758114_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_VirtualButtonData_t2901758114_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_VirtualButtonData_t2901758114_0_0_0_VirtualButtonData_t2901758114_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &VirtualButtonData_t2901758114_0_0_0, &VirtualButtonData_t2901758114_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_VirtualButtonData_t2901758114_0_0_0_VirtualButtonData_t2901758114_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_VirtualButtonData_t2901758114_0_0_0_VirtualButtonData_t2901758114_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_VirtualButtonData_t2901758114_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &VirtualButtonData_t2901758114_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_VirtualButtonData_t2901758114_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_VirtualButtonData_t2901758114_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_VirtualButtonData_t2901758114_0_0_0_KeyValuePair_2_t4188143612_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &VirtualButtonData_t2901758114_0_0_0, &KeyValuePair_2_t4188143612_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_VirtualButtonData_t2901758114_0_0_0_KeyValuePair_2_t4188143612_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_VirtualButtonData_t2901758114_0_0_0_KeyValuePair_2_t4188143612_0_0_0_Types };
extern const Il2CppType ImageTarget_t3707016494_0_0_0;
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_ImageTarget_t3707016494_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &ImageTarget_t3707016494_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_ImageTarget_t3707016494_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_ImageTarget_t3707016494_0_0_0_Types };
static const Il2CppType* GenInst_ImageTarget_t3707016494_0_0_0_Types[] = { &ImageTarget_t3707016494_0_0_0 };
extern const Il2CppGenericInst GenInst_ImageTarget_t3707016494_0_0_0 = { 1, GenInst_ImageTarget_t3707016494_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_ImageTarget_t3707016494_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &ImageTarget_t3707016494_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_ImageTarget_t3707016494_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_ImageTarget_t3707016494_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t698434696_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t698434696_0_0_0_Types[] = { &KeyValuePair_2_t698434696_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t698434696_0_0_0 = { 1, GenInst_KeyValuePair_2_t698434696_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_ImageTarget_t3707016494_0_0_0_KeyValuePair_2_t698434696_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &ImageTarget_t3707016494_0_0_0, &KeyValuePair_2_t698434696_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_ImageTarget_t3707016494_0_0_0_KeyValuePair_2_t698434696_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_ImageTarget_t3707016494_0_0_0_KeyValuePair_2_t698434696_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_ImageTarget_t3707016494_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &ImageTarget_t3707016494_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_ImageTarget_t3707016494_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_ImageTarget_t3707016494_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_ImageTarget_t3707016494_0_0_0_ImageTarget_t3707016494_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &ImageTarget_t3707016494_0_0_0, &ImageTarget_t3707016494_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_ImageTarget_t3707016494_0_0_0_ImageTarget_t3707016494_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_ImageTarget_t3707016494_0_0_0_ImageTarget_t3707016494_0_0_0_Types };
extern const Il2CppType ProfileData_t3519391925_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_ProfileData_t3519391925_0_0_0_Types[] = { &String_t_0_0_0, &ProfileData_t3519391925_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ProfileData_t3519391925_0_0_0 = { 2, GenInst_String_t_0_0_0_ProfileData_t3519391925_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ProfileData_t3519391925_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ProfileData_t3519391925_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ProfileData_t3519391925_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_ProfileData_t3519391925_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2969503080_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2969503080_0_0_0_Types[] = { &KeyValuePair_2_t2969503080_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2969503080_0_0_0 = { 1, GenInst_KeyValuePair_2_t2969503080_0_0_0_Types };
static const Il2CppType* GenInst_ProfileData_t3519391925_0_0_0_Types[] = { &ProfileData_t3519391925_0_0_0 };
extern const Il2CppGenericInst GenInst_ProfileData_t3519391925_0_0_0 = { 1, GenInst_ProfileData_t3519391925_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ProfileData_t3519391925_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ProfileData_t3519391925_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ProfileData_t3519391925_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ProfileData_t3519391925_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ProfileData_t3519391925_0_0_0_ProfileData_t3519391925_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ProfileData_t3519391925_0_0_0, &ProfileData_t3519391925_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ProfileData_t3519391925_0_0_0_ProfileData_t3519391925_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ProfileData_t3519391925_0_0_0_ProfileData_t3519391925_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ProfileData_t3519391925_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ProfileData_t3519391925_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ProfileData_t3519391925_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ProfileData_t3519391925_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ProfileData_t3519391925_0_0_0_KeyValuePair_2_t2969503080_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ProfileData_t3519391925_0_0_0, &KeyValuePair_2_t2969503080_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ProfileData_t3519391925_0_0_0_KeyValuePair_2_t2969503080_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ProfileData_t3519391925_0_0_0_KeyValuePair_2_t2969503080_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ProfileData_t3519391925_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &ProfileData_t3519391925_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ProfileData_t3519391925_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_ProfileData_t3519391925_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1407353095_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1407353095_0_0_0_Types[] = { &KeyValuePair_2_t1407353095_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1407353095_0_0_0 = { 1, GenInst_KeyValuePair_2_t1407353095_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ProfileData_t3519391925_0_0_0_KeyValuePair_2_t1407353095_0_0_0_Types[] = { &String_t_0_0_0, &ProfileData_t3519391925_0_0_0, &KeyValuePair_2_t1407353095_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ProfileData_t3519391925_0_0_0_KeyValuePair_2_t1407353095_0_0_0 = { 3, GenInst_String_t_0_0_0_ProfileData_t3519391925_0_0_0_KeyValuePair_2_t1407353095_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ProfileData_t3519391925_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &ProfileData_t3519391925_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ProfileData_t3519391925_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_ProfileData_t3519391925_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ProfileData_t3519391925_0_0_0_ProfileData_t3519391925_0_0_0_Types[] = { &String_t_0_0_0, &ProfileData_t3519391925_0_0_0, &ProfileData_t3519391925_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ProfileData_t3519391925_0_0_0_ProfileData_t3519391925_0_0_0 = { 3, GenInst_String_t_0_0_0_ProfileData_t3519391925_0_0_0_ProfileData_t3519391925_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_VirtualButtonAbstractBehaviour_t2408702468_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &VirtualButtonAbstractBehaviour_t2408702468_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_VirtualButtonAbstractBehaviour_t2408702468_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_VirtualButtonAbstractBehaviour_t2408702468_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_VirtualButtonAbstractBehaviour_t2408702468_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &VirtualButtonAbstractBehaviour_t2408702468_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_VirtualButtonAbstractBehaviour_t2408702468_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_VirtualButtonAbstractBehaviour_t2408702468_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3695087966_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3695087966_0_0_0_Types[] = { &KeyValuePair_2_t3695087966_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3695087966_0_0_0 = { 1, GenInst_KeyValuePair_2_t3695087966_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_VirtualButtonAbstractBehaviour_t2408702468_0_0_0_KeyValuePair_2_t3695087966_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &VirtualButtonAbstractBehaviour_t2408702468_0_0_0, &KeyValuePair_2_t3695087966_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_VirtualButtonAbstractBehaviour_t2408702468_0_0_0_KeyValuePair_2_t3695087966_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_VirtualButtonAbstractBehaviour_t2408702468_0_0_0_KeyValuePair_2_t3695087966_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_VirtualButtonAbstractBehaviour_t2408702468_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &VirtualButtonAbstractBehaviour_t2408702468_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_VirtualButtonAbstractBehaviour_t2408702468_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_VirtualButtonAbstractBehaviour_t2408702468_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_VirtualButtonAbstractBehaviour_t2408702468_0_0_0_VirtualButtonAbstractBehaviour_t2408702468_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &VirtualButtonAbstractBehaviour_t2408702468_0_0_0, &VirtualButtonAbstractBehaviour_t2408702468_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_VirtualButtonAbstractBehaviour_t2408702468_0_0_0_VirtualButtonAbstractBehaviour_t2408702468_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_VirtualButtonAbstractBehaviour_t2408702468_0_0_0_VirtualButtonAbstractBehaviour_t2408702468_0_0_0_Types };
extern const Il2CppType ITrackerEventHandler_t2342393219_0_0_0;
static const Il2CppType* GenInst_ITrackerEventHandler_t2342393219_0_0_0_Types[] = { &ITrackerEventHandler_t2342393219_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackerEventHandler_t2342393219_0_0_0 = { 1, GenInst_ITrackerEventHandler_t2342393219_0_0_0_Types };
extern const Il2CppType IVideoBackgroundEventHandler_t3728063431_0_0_0;
static const Il2CppType* GenInst_IVideoBackgroundEventHandler_t3728063431_0_0_0_Types[] = { &IVideoBackgroundEventHandler_t3728063431_0_0_0 };
extern const Il2CppGenericInst GenInst_IVideoBackgroundEventHandler_t3728063431_0_0_0 = { 1, GenInst_IVideoBackgroundEventHandler_t3728063431_0_0_0_Types };
extern const Il2CppType BackgroundPlaneAbstractBehaviour_t4147679365_0_0_0;
static const Il2CppType* GenInst_BackgroundPlaneAbstractBehaviour_t4147679365_0_0_0_Types[] = { &BackgroundPlaneAbstractBehaviour_t4147679365_0_0_0 };
extern const Il2CppGenericInst GenInst_BackgroundPlaneAbstractBehaviour_t4147679365_0_0_0 = { 1, GenInst_BackgroundPlaneAbstractBehaviour_t4147679365_0_0_0_Types };
extern const Il2CppType ITextRecoEventHandler_t196339532_0_0_0;
static const Il2CppType* GenInst_ITextRecoEventHandler_t196339532_0_0_0_Types[] = { &ITextRecoEventHandler_t196339532_0_0_0 };
extern const Il2CppGenericInst GenInst_ITextRecoEventHandler_t196339532_0_0_0 = { 1, GenInst_ITextRecoEventHandler_t196339532_0_0_0_Types };
extern const Il2CppType ITrackableEventHandler_t1495975588_0_0_0;
static const Il2CppType* GenInst_ITrackableEventHandler_t1495975588_0_0_0_Types[] = { &ITrackableEventHandler_t1495975588_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackableEventHandler_t1495975588_0_0_0 = { 1, GenInst_ITrackableEventHandler_t1495975588_0_0_0_Types };
extern const Il2CppType IUserDefinedTargetEventHandler_t1256813275_0_0_0;
static const Il2CppType* GenInst_IUserDefinedTargetEventHandler_t1256813275_0_0_0_Types[] = { &IUserDefinedTargetEventHandler_t1256813275_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserDefinedTargetEventHandler_t1256813275_0_0_0 = { 1, GenInst_IUserDefinedTargetEventHandler_t1256813275_0_0_0_Types };
extern const Il2CppType MeshRenderer_t587009260_0_0_0;
static const Il2CppType* GenInst_MeshRenderer_t587009260_0_0_0_Types[] = { &MeshRenderer_t587009260_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshRenderer_t587009260_0_0_0 = { 1, GenInst_MeshRenderer_t587009260_0_0_0_Types };
extern const Il2CppType Link_t716170069_0_0_0;
static const Il2CppType* GenInst_Link_t716170069_0_0_0_Types[] = { &Link_t716170069_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t716170069_0_0_0 = { 1, GenInst_Link_t716170069_0_0_0_Types };
extern const Il2CppType Link_t422739451_0_0_0;
static const Il2CppType* GenInst_Link_t422739451_0_0_0_Types[] = { &Link_t422739451_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t422739451_0_0_0 = { 1, GenInst_Link_t422739451_0_0_0_Types };
extern const Il2CppType Link_t4276840174_0_0_0;
static const Il2CppType* GenInst_Link_t4276840174_0_0_0_Types[] = { &Link_t4276840174_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t4276840174_0_0_0 = { 1, GenInst_Link_t4276840174_0_0_0_Types };
extern const Il2CppType Link_t3498551137_0_0_0;
static const Il2CppType* GenInst_Link_t3498551137_0_0_0_Types[] = { &Link_t3498551137_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t3498551137_0_0_0 = { 1, GenInst_Link_t3498551137_0_0_0_Types };
extern const Il2CppType IVirtualButtonEventHandler_t3188643434_0_0_0;
static const Il2CppType* GenInst_IVirtualButtonEventHandler_t3188643434_0_0_0_Types[] = { &IVirtualButtonEventHandler_t3188643434_0_0_0 };
extern const Il2CppGenericInst GenInst_IVirtualButtonEventHandler_t3188643434_0_0_0 = { 1, GenInst_IVirtualButtonEventHandler_t3188643434_0_0_0_Types };
extern const Il2CppType List_1_t942764925_0_0_0;
static const Il2CppType* GenInst_List_1_t942764925_0_0_0_Types[] = { &List_1_t942764925_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t942764925_0_0_0 = { 1, GenInst_List_1_t942764925_0_0_0_Types };
extern const Il2CppType DispatcherKey_t2110064572_0_0_0;
extern const Il2CppType Dispatcher_t684365006_0_0_0;
static const Il2CppType* GenInst_DispatcherKey_t2110064572_0_0_0_Dispatcher_t684365006_0_0_0_Types[] = { &DispatcherKey_t2110064572_0_0_0, &Dispatcher_t684365006_0_0_0 };
extern const Il2CppGenericInst GenInst_DispatcherKey_t2110064572_0_0_0_Dispatcher_t684365006_0_0_0 = { 2, GenInst_DispatcherKey_t2110064572_0_0_0_Dispatcher_t684365006_0_0_0_Types };
static const Il2CppType* GenInst_DispatcherKey_t2110064572_0_0_0_Types[] = { &DispatcherKey_t2110064572_0_0_0 };
extern const Il2CppGenericInst GenInst_DispatcherKey_t2110064572_0_0_0 = { 1, GenInst_DispatcherKey_t2110064572_0_0_0_Types };
static const Il2CppType* GenInst_Dispatcher_t684365006_0_0_0_Types[] = { &Dispatcher_t684365006_0_0_0 };
extern const Il2CppGenericInst GenInst_Dispatcher_t684365006_0_0_0 = { 1, GenInst_Dispatcher_t684365006_0_0_0_Types };
static const Il2CppType* GenInst_DispatcherKey_t2110064572_0_0_0_Dispatcher_t684365006_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &DispatcherKey_t2110064572_0_0_0, &Dispatcher_t684365006_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_DispatcherKey_t2110064572_0_0_0_Dispatcher_t684365006_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_DispatcherKey_t2110064572_0_0_0_Dispatcher_t684365006_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3263919449_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3263919449_0_0_0_Types[] = { &KeyValuePair_2_t3263919449_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3263919449_0_0_0 = { 1, GenInst_KeyValuePair_2_t3263919449_0_0_0_Types };
static const Il2CppType* GenInst_DispatcherKey_t2110064572_0_0_0_Dispatcher_t684365006_0_0_0_KeyValuePair_2_t3263919449_0_0_0_Types[] = { &DispatcherKey_t2110064572_0_0_0, &Dispatcher_t684365006_0_0_0, &KeyValuePair_2_t3263919449_0_0_0 };
extern const Il2CppGenericInst GenInst_DispatcherKey_t2110064572_0_0_0_Dispatcher_t684365006_0_0_0_KeyValuePair_2_t3263919449_0_0_0 = { 3, GenInst_DispatcherKey_t2110064572_0_0_0_Dispatcher_t684365006_0_0_0_KeyValuePair_2_t3263919449_0_0_0_Types };
static const Il2CppType* GenInst_DispatcherKey_t2110064572_0_0_0_Dispatcher_t684365006_0_0_0_DispatcherKey_t2110064572_0_0_0_Types[] = { &DispatcherKey_t2110064572_0_0_0, &Dispatcher_t684365006_0_0_0, &DispatcherKey_t2110064572_0_0_0 };
extern const Il2CppGenericInst GenInst_DispatcherKey_t2110064572_0_0_0_Dispatcher_t684365006_0_0_0_DispatcherKey_t2110064572_0_0_0 = { 3, GenInst_DispatcherKey_t2110064572_0_0_0_Dispatcher_t684365006_0_0_0_DispatcherKey_t2110064572_0_0_0_Types };
static const Il2CppType* GenInst_DispatcherKey_t2110064572_0_0_0_Dispatcher_t684365006_0_0_0_Dispatcher_t684365006_0_0_0_Types[] = { &DispatcherKey_t2110064572_0_0_0, &Dispatcher_t684365006_0_0_0, &Dispatcher_t684365006_0_0_0 };
extern const Il2CppGenericInst GenInst_DispatcherKey_t2110064572_0_0_0_Dispatcher_t684365006_0_0_0_Dispatcher_t684365006_0_0_0 = { 3, GenInst_DispatcherKey_t2110064572_0_0_0_Dispatcher_t684365006_0_0_0_Dispatcher_t684365006_0_0_0_Types };
extern const Il2CppType CodeExpression_t2163794278_0_0_0;
static const Il2CppType* GenInst_CodeExpression_t2163794278_0_0_0_Types[] = { &CodeExpression_t2163794278_0_0_0 };
extern const Il2CppGenericInst GenInst_CodeExpression_t2163794278_0_0_0 = { 1, GenInst_CodeExpression_t2163794278_0_0_0_Types };
extern const Il2CppType CodeItem_t493730090_0_0_0;
static const Il2CppType* GenInst_CodeItem_t493730090_0_0_0_Types[] = { &CodeItem_t493730090_0_0_0 };
extern const Il2CppGenericInst GenInst_CodeItem_t493730090_0_0_0 = { 1, GenInst_CodeItem_t493730090_0_0_0_Types };
extern const Il2CppType ClientCertificateType_t1004704909_0_0_0;
static const Il2CppType* GenInst_ClientCertificateType_t1004704909_0_0_0_Types[] = { &ClientCertificateType_t1004704909_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t1004704909_0_0_0 = { 1, GenInst_ClientCertificateType_t1004704909_0_0_0_Types };
extern const Il2CppType DTMXPathLinkedNode2_t3353097824_0_0_0;
static const Il2CppType* GenInst_DTMXPathLinkedNode2_t3353097824_0_0_0_Types[] = { &DTMXPathLinkedNode2_t3353097824_0_0_0 };
extern const Il2CppGenericInst GenInst_DTMXPathLinkedNode2_t3353097824_0_0_0 = { 1, GenInst_DTMXPathLinkedNode2_t3353097824_0_0_0_Types };
extern const Il2CppType DTMXPathAttributeNode2_t3707096873_0_0_0;
static const Il2CppType* GenInst_DTMXPathAttributeNode2_t3707096873_0_0_0_Types[] = { &DTMXPathAttributeNode2_t3707096873_0_0_0 };
extern const Il2CppGenericInst GenInst_DTMXPathAttributeNode2_t3707096873_0_0_0 = { 1, GenInst_DTMXPathAttributeNode2_t3707096873_0_0_0_Types };
extern const Il2CppType DTMXPathNamespaceNode2_t1119120713_0_0_0;
static const Il2CppType* GenInst_DTMXPathNamespaceNode2_t1119120713_0_0_0_Types[] = { &DTMXPathNamespaceNode2_t1119120713_0_0_0 };
extern const Il2CppGenericInst GenInst_DTMXPathNamespaceNode2_t1119120713_0_0_0 = { 1, GenInst_DTMXPathNamespaceNode2_t1119120713_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Il2CppObject_0_0_0_Types[] = { &Type_t_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Type_t_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Type_t_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Type_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3627158099_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3627158099_0_0_0_Types[] = { &KeyValuePair_2_t3627158099_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3627158099_0_0_0 = { 1, GenInst_KeyValuePair_2_t3627158099_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3627158099_0_0_0_Types[] = { &Type_t_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t3627158099_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3627158099_0_0_0 = { 3, GenInst_Type_t_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3627158099_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Il2CppObject_0_0_0_Type_t_0_0_0_Types[] = { &Type_t_0_0_0, &Il2CppObject_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Il2CppObject_0_0_0_Type_t_0_0_0 = { 3, GenInst_Type_t_0_0_0_Il2CppObject_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Type_t_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Type_t_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType Uri_t100236324_0_0_0;
static const Il2CppType* GenInst_Uri_t100236324_0_0_0_Types[] = { &Uri_t100236324_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t100236324_0_0_0 = { 1, GenInst_Uri_t100236324_0_0_0_Types };
extern const Il2CppType ServiceHostBase_t3741910535_0_0_0;
static const Il2CppType* GenInst_ServiceHostBase_t3741910535_0_0_0_Types[] = { &ServiceHostBase_t3741910535_0_0_0 };
extern const Il2CppGenericInst GenInst_ServiceHostBase_t3741910535_0_0_0 = { 1, GenInst_ServiceHostBase_t3741910535_0_0_0_Types };
extern const Il2CppType AddressHeader_t3096310965_0_0_0;
static const Il2CppType* GenInst_AddressHeader_t3096310965_0_0_0_Types[] = { &AddressHeader_t3096310965_0_0_0 };
extern const Il2CppGenericInst GenInst_AddressHeader_t3096310965_0_0_0 = { 1, GenInst_AddressHeader_t3096310965_0_0_0_Types };
extern const Il2CppType IReplyChannel_t3856619590_0_0_0;
static const Il2CppType* GenInst_IReplyChannel_t3856619590_0_0_0_Types[] = { &IReplyChannel_t3856619590_0_0_0 };
extern const Il2CppGenericInst GenInst_IReplyChannel_t3856619590_0_0_0 = { 1, GenInst_IReplyChannel_t3856619590_0_0_0_Types };
static const Il2CppType* GenInst_TimeSpan_t881159249_0_0_0_Il2CppObject_0_0_0_Types[] = { &TimeSpan_t881159249_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t881159249_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_TimeSpan_t881159249_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType BindingElement_t3643137745_0_0_0;
static const Il2CppType* GenInst_BindingElement_t3643137745_0_0_0_Types[] = { &BindingElement_t3643137745_0_0_0 };
extern const Il2CppGenericInst GenInst_BindingElement_t3643137745_0_0_0 = { 1, GenInst_BindingElement_t3643137745_0_0_0_Types };
static const Il2CppType* GenInst_TimeSpan_t881159249_0_0_0_IReplyChannel_t3856619590_0_0_0_Types[] = { &TimeSpan_t881159249_0_0_0, &IReplyChannel_t3856619590_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t881159249_0_0_0_IReplyChannel_t3856619590_0_0_0 = { 2, GenInst_TimeSpan_t881159249_0_0_0_IReplyChannel_t3856619590_0_0_0_Types };
extern const Il2CppType IChannel_t937207545_0_0_0;
static const Il2CppType* GenInst_IChannel_t937207545_0_0_0_Types[] = { &IChannel_t937207545_0_0_0 };
extern const Il2CppGenericInst GenInst_IChannel_t937207545_0_0_0 = { 1, GenInst_IChannel_t937207545_0_0_0_Types };
extern const Il2CppType ICommunicationObject_t3884464922_0_0_0;
static const Il2CppType* GenInst_ICommunicationObject_t3884464922_0_0_0_Types[] = { &ICommunicationObject_t3884464922_0_0_0 };
extern const Il2CppGenericInst GenInst_ICommunicationObject_t3884464922_0_0_0 = { 1, GenInst_ICommunicationObject_t3884464922_0_0_0_Types };
extern const Il2CppType HttpContext_t1969259010_0_0_0;
static const Il2CppType* GenInst_HttpContext_t1969259010_0_0_0_Types[] = { &HttpContext_t1969259010_0_0_0 };
extern const Il2CppGenericInst GenInst_HttpContext_t1969259010_0_0_0 = { 1, GenInst_HttpContext_t1969259010_0_0_0_Types };
extern const Il2CppType IServiceProvider_t1439597990_0_0_0;
static const Il2CppType* GenInst_IServiceProvider_t1439597990_0_0_0_Types[] = { &IServiceProvider_t1439597990_0_0_0 };
extern const Il2CppGenericInst GenInst_IServiceProvider_t1439597990_0_0_0 = { 1, GenInst_IServiceProvider_t1439597990_0_0_0_Types };
extern const Il2CppType HttpContextInfo_t3561535433_0_0_0;
static const Il2CppType* GenInst_HttpContextInfo_t3561535433_0_0_0_Types[] = { &HttpContextInfo_t3561535433_0_0_0 };
extern const Il2CppGenericInst GenInst_HttpContextInfo_t3561535433_0_0_0 = { 1, GenInst_HttpContextInfo_t3561535433_0_0_0_Types };
extern const Il2CppType HttpListener_t988452056_0_0_0;
static const Il2CppType* GenInst_Uri_t100236324_0_0_0_HttpListener_t988452056_0_0_0_Types[] = { &Uri_t100236324_0_0_0, &HttpListener_t988452056_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t100236324_0_0_0_HttpListener_t988452056_0_0_0 = { 2, GenInst_Uri_t100236324_0_0_0_HttpListener_t988452056_0_0_0_Types };
static const Il2CppType* GenInst_HttpListener_t988452056_0_0_0_Types[] = { &HttpListener_t988452056_0_0_0 };
extern const Il2CppGenericInst GenInst_HttpListener_t988452056_0_0_0 = { 1, GenInst_HttpListener_t988452056_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t100236324_0_0_0_HttpListener_t988452056_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Uri_t100236324_0_0_0, &HttpListener_t988452056_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t100236324_0_0_0_HttpListener_t988452056_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Uri_t100236324_0_0_0_HttpListener_t988452056_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2563037787_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2563037787_0_0_0_Types[] = { &KeyValuePair_2_t2563037787_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2563037787_0_0_0 = { 1, GenInst_KeyValuePair_2_t2563037787_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t100236324_0_0_0_HttpListener_t988452056_0_0_0_KeyValuePair_2_t2563037787_0_0_0_Types[] = { &Uri_t100236324_0_0_0, &HttpListener_t988452056_0_0_0, &KeyValuePair_2_t2563037787_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t100236324_0_0_0_HttpListener_t988452056_0_0_0_KeyValuePair_2_t2563037787_0_0_0 = { 3, GenInst_Uri_t100236324_0_0_0_HttpListener_t988452056_0_0_0_KeyValuePair_2_t2563037787_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t100236324_0_0_0_HttpListener_t988452056_0_0_0_Uri_t100236324_0_0_0_Types[] = { &Uri_t100236324_0_0_0, &HttpListener_t988452056_0_0_0, &Uri_t100236324_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t100236324_0_0_0_HttpListener_t988452056_0_0_0_Uri_t100236324_0_0_0 = { 3, GenInst_Uri_t100236324_0_0_0_HttpListener_t988452056_0_0_0_Uri_t100236324_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t100236324_0_0_0_HttpListener_t988452056_0_0_0_HttpListener_t988452056_0_0_0_Types[] = { &Uri_t100236324_0_0_0, &HttpListener_t988452056_0_0_0, &HttpListener_t988452056_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t100236324_0_0_0_HttpListener_t988452056_0_0_0_HttpListener_t988452056_0_0_0 = { 3, GenInst_Uri_t100236324_0_0_0_HttpListener_t988452056_0_0_0_HttpListener_t988452056_0_0_0_Types };
extern const Il2CppType IChannelListener_t114797722_0_0_0;
static const Il2CppType* GenInst_IChannelListener_t114797722_0_0_0_HttpContext_t1969259010_0_0_0_Types[] = { &IChannelListener_t114797722_0_0_0, &HttpContext_t1969259010_0_0_0 };
extern const Il2CppGenericInst GenInst_IChannelListener_t114797722_0_0_0_HttpContext_t1969259010_0_0_0 = { 2, GenInst_IChannelListener_t114797722_0_0_0_HttpContext_t1969259010_0_0_0_Types };
extern const Il2CppType List_1_t1586872464_0_0_0;
static const Il2CppType* GenInst_Uri_t100236324_0_0_0_List_1_t1586872464_0_0_0_Types[] = { &Uri_t100236324_0_0_0, &List_1_t1586872464_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t100236324_0_0_0_List_1_t1586872464_0_0_0 = { 2, GenInst_Uri_t100236324_0_0_0_List_1_t1586872464_0_0_0_Types };
static const Il2CppType* GenInst_IChannelListener_t114797722_0_0_0_Types[] = { &IChannelListener_t114797722_0_0_0 };
extern const Il2CppGenericInst GenInst_IChannelListener_t114797722_0_0_0 = { 1, GenInst_IChannelListener_t114797722_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t1586872464_0_0_0_Types[] = { &List_1_t1586872464_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1586872464_0_0_0 = { 1, GenInst_List_1_t1586872464_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t100236324_0_0_0_List_1_t1586872464_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Uri_t100236324_0_0_0, &List_1_t1586872464_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t100236324_0_0_0_List_1_t1586872464_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Uri_t100236324_0_0_0_List_1_t1586872464_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3161458195_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3161458195_0_0_0_Types[] = { &KeyValuePair_2_t3161458195_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3161458195_0_0_0 = { 1, GenInst_KeyValuePair_2_t3161458195_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t100236324_0_0_0_List_1_t1586872464_0_0_0_KeyValuePair_2_t3161458195_0_0_0_Types[] = { &Uri_t100236324_0_0_0, &List_1_t1586872464_0_0_0, &KeyValuePair_2_t3161458195_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t100236324_0_0_0_List_1_t1586872464_0_0_0_KeyValuePair_2_t3161458195_0_0_0 = { 3, GenInst_Uri_t100236324_0_0_0_List_1_t1586872464_0_0_0_KeyValuePair_2_t3161458195_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t100236324_0_0_0_List_1_t1586872464_0_0_0_Uri_t100236324_0_0_0_Types[] = { &Uri_t100236324_0_0_0, &List_1_t1586872464_0_0_0, &Uri_t100236324_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t100236324_0_0_0_List_1_t1586872464_0_0_0_Uri_t100236324_0_0_0 = { 3, GenInst_Uri_t100236324_0_0_0_List_1_t1586872464_0_0_0_Uri_t100236324_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t100236324_0_0_0_List_1_t1586872464_0_0_0_List_1_t1586872464_0_0_0_Types[] = { &Uri_t100236324_0_0_0, &List_1_t1586872464_0_0_0, &List_1_t1586872464_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t100236324_0_0_0_List_1_t1586872464_0_0_0_List_1_t1586872464_0_0_0 = { 3, GenInst_Uri_t100236324_0_0_0_List_1_t1586872464_0_0_0_List_1_t1586872464_0_0_0_Types };
extern const Il2CppType HttpListenerContext_t424880822_0_0_0;
static const Il2CppType* GenInst_HttpListenerContext_t424880822_0_0_0_Types[] = { &HttpListenerContext_t424880822_0_0_0 };
extern const Il2CppGenericInst GenInst_HttpListenerContext_t424880822_0_0_0 = { 1, GenInst_HttpListenerContext_t424880822_0_0_0_Types };
extern const Il2CppType IDuplexSession_t2479312727_0_0_0;
static const Il2CppType* GenInst_IDuplexSession_t2479312727_0_0_0_Types[] = { &IDuplexSession_t2479312727_0_0_0 };
extern const Il2CppGenericInst GenInst_IDuplexSession_t2479312727_0_0_0 = { 1, GenInst_IDuplexSession_t2479312727_0_0_0_Types };
extern const Il2CppType IInputSession_t208887958_0_0_0;
static const Il2CppType* GenInst_IInputSession_t208887958_0_0_0_Types[] = { &IInputSession_t208887958_0_0_0 };
extern const Il2CppGenericInst GenInst_IInputSession_t208887958_0_0_0 = { 1, GenInst_IInputSession_t208887958_0_0_0_Types };
extern const Il2CppType IOutputSession_t2452150453_0_0_0;
static const Il2CppType* GenInst_IOutputSession_t2452150453_0_0_0_Types[] = { &IOutputSession_t2452150453_0_0_0 };
extern const Il2CppGenericInst GenInst_IOutputSession_t2452150453_0_0_0 = { 1, GenInst_IOutputSession_t2452150453_0_0_0_Types };
extern const Il2CppType MessageHeaderInfo_t464847589_0_0_0;
static const Il2CppType* GenInst_MessageHeaderInfo_t464847589_0_0_0_Types[] = { &MessageHeaderInfo_t464847589_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageHeaderInfo_t464847589_0_0_0 = { 1, GenInst_MessageHeaderInfo_t464847589_0_0_0_Types };
extern const Il2CppType FaultReasonText_t3547351049_0_0_0;
static const Il2CppType* GenInst_FaultReasonText_t3547351049_0_0_0_Types[] = { &FaultReasonText_t3547351049_0_0_0 };
extern const Il2CppGenericInst GenInst_FaultReasonText_t3547351049_0_0_0 = { 1, GenInst_FaultReasonText_t3547351049_0_0_0_Types };
extern const Il2CppType XmlObjectSerializer_t3967301761_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_Types[] = { &Type_t_0_0_0, &XmlObjectSerializer_t3967301761_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_XmlObjectSerializer_t3967301761_0_0_0 = { 2, GenInst_Type_t_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_Types };
static const Il2CppType* GenInst_XmlObjectSerializer_t3967301761_0_0_0_Types[] = { &XmlObjectSerializer_t3967301761_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlObjectSerializer_t3967301761_0_0_0 = { 1, GenInst_XmlObjectSerializer_t3967301761_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Type_t_0_0_0, &XmlObjectSerializer_t3967301761_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Type_t_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t219386400_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t219386400_0_0_0_Types[] = { &KeyValuePair_2_t219386400_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t219386400_0_0_0 = { 1, GenInst_KeyValuePair_2_t219386400_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_KeyValuePair_2_t219386400_0_0_0_Types[] = { &Type_t_0_0_0, &XmlObjectSerializer_t3967301761_0_0_0, &KeyValuePair_2_t219386400_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_KeyValuePair_2_t219386400_0_0_0 = { 3, GenInst_Type_t_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_KeyValuePair_2_t219386400_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_Type_t_0_0_0_Types[] = { &Type_t_0_0_0, &XmlObjectSerializer_t3967301761_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_Type_t_0_0_0 = { 3, GenInst_Type_t_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_Types[] = { &Type_t_0_0_0, &XmlObjectSerializer_t3967301761_0_0_0, &XmlObjectSerializer_t3967301761_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_XmlObjectSerializer_t3967301761_0_0_0 = { 3, GenInst_Type_t_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_Types };
extern const Il2CppType SupportingTokenInfo_t3011094535_0_0_0;
static const Il2CppType* GenInst_SupportingTokenInfo_t3011094535_0_0_0_Types[] = { &SupportingTokenInfo_t3011094535_0_0_0 };
extern const Il2CppGenericInst GenInst_SupportingTokenInfo_t3011094535_0_0_0 = { 1, GenInst_SupportingTokenInfo_t3011094535_0_0_0_Types };
extern const Il2CppType SupportingTokenParameters_t907883796_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_SupportingTokenParameters_t907883796_0_0_0_Types[] = { &String_t_0_0_0, &SupportingTokenParameters_t907883796_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_SupportingTokenParameters_t907883796_0_0_0 = { 2, GenInst_String_t_0_0_0_SupportingTokenParameters_t907883796_0_0_0_Types };
static const Il2CppType* GenInst_SupportingTokenParameters_t907883796_0_0_0_Types[] = { &SupportingTokenParameters_t907883796_0_0_0 };
extern const Il2CppGenericInst GenInst_SupportingTokenParameters_t907883796_0_0_0 = { 1, GenInst_SupportingTokenParameters_t907883796_0_0_0_Types };
extern const Il2CppType SecurityTokenParameters_t2868958784_0_0_0;
static const Il2CppType* GenInst_SecurityTokenParameters_t2868958784_0_0_0_Types[] = { &SecurityTokenParameters_t2868958784_0_0_0 };
extern const Il2CppGenericInst GenInst_SecurityTokenParameters_t2868958784_0_0_0 = { 1, GenInst_SecurityTokenParameters_t2868958784_0_0_0_Types };
extern const Il2CppType IOutputChannel_t519638262_0_0_0;
static const Il2CppType* GenInst_IOutputChannel_t519638262_0_0_0_Types[] = { &IOutputChannel_t519638262_0_0_0 };
extern const Il2CppGenericInst GenInst_IOutputChannel_t519638262_0_0_0 = { 1, GenInst_IOutputChannel_t519638262_0_0_0_Types };
extern const Il2CppType IContextChannel_t910994827_0_0_0;
static const Il2CppType* GenInst_IContextChannel_t910994827_0_0_0_Types[] = { &IContextChannel_t910994827_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextChannel_t910994827_0_0_0 = { 1, GenInst_IContextChannel_t910994827_0_0_0_Types };
extern const Il2CppType ICustomPeerResolverClient_t543357795_0_0_0;
static const Il2CppType* GenInst_ICustomPeerResolverClient_t543357795_0_0_0_Types[] = { &ICustomPeerResolverClient_t543357795_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomPeerResolverClient_t543357795_0_0_0 = { 1, GenInst_ICustomPeerResolverClient_t543357795_0_0_0_Types };
extern const Il2CppType IEndpointBehavior_t1578562619_0_0_0;
static const Il2CppType* GenInst_IEndpointBehavior_t1578562619_0_0_0_Types[] = { &IEndpointBehavior_t1578562619_0_0_0 };
extern const Il2CppGenericInst GenInst_IEndpointBehavior_t1578562619_0_0_0 = { 1, GenInst_IEndpointBehavior_t1578562619_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IEndpointBehavior_t1578562619_0_0_0_Types[] = { &Type_t_0_0_0, &IEndpointBehavior_t1578562619_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IEndpointBehavior_t1578562619_0_0_0 = { 2, GenInst_Type_t_0_0_0_IEndpointBehavior_t1578562619_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IEndpointBehavior_t1578562619_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Type_t_0_0_0, &IEndpointBehavior_t1578562619_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IEndpointBehavior_t1578562619_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Type_t_0_0_0_IEndpointBehavior_t1578562619_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2125614554_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2125614554_0_0_0_Types[] = { &KeyValuePair_2_t2125614554_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2125614554_0_0_0 = { 1, GenInst_KeyValuePair_2_t2125614554_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IEndpointBehavior_t1578562619_0_0_0_KeyValuePair_2_t2125614554_0_0_0_Types[] = { &Type_t_0_0_0, &IEndpointBehavior_t1578562619_0_0_0, &KeyValuePair_2_t2125614554_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IEndpointBehavior_t1578562619_0_0_0_KeyValuePair_2_t2125614554_0_0_0 = { 3, GenInst_Type_t_0_0_0_IEndpointBehavior_t1578562619_0_0_0_KeyValuePair_2_t2125614554_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IEndpointBehavior_t1578562619_0_0_0_Type_t_0_0_0_Types[] = { &Type_t_0_0_0, &IEndpointBehavior_t1578562619_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IEndpointBehavior_t1578562619_0_0_0_Type_t_0_0_0 = { 3, GenInst_Type_t_0_0_0_IEndpointBehavior_t1578562619_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IEndpointBehavior_t1578562619_0_0_0_IEndpointBehavior_t1578562619_0_0_0_Types[] = { &Type_t_0_0_0, &IEndpointBehavior_t1578562619_0_0_0, &IEndpointBehavior_t1578562619_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IEndpointBehavior_t1578562619_0_0_0_IEndpointBehavior_t1578562619_0_0_0 = { 3, GenInst_Type_t_0_0_0_IEndpointBehavior_t1578562619_0_0_0_IEndpointBehavior_t1578562619_0_0_0_Types };
extern const Il2CppType PeerNodeAddress_t2098027372_0_0_0;
static const Il2CppType* GenInst_PeerNodeAddress_t2098027372_0_0_0_Types[] = { &PeerNodeAddress_t2098027372_0_0_0 };
extern const Il2CppGenericInst GenInst_PeerNodeAddress_t2098027372_0_0_0 = { 1, GenInst_PeerNodeAddress_t2098027372_0_0_0_Types };
extern const Il2CppType IDuplexSessionChannel_t292986819_0_0_0;
static const Il2CppType* GenInst_IDuplexSessionChannel_t292986819_0_0_0_Types[] = { &IDuplexSessionChannel_t292986819_0_0_0 };
extern const Il2CppGenericInst GenInst_IDuplexSessionChannel_t292986819_0_0_0 = { 1, GenInst_IDuplexSessionChannel_t292986819_0_0_0_Types };
extern const Il2CppType RemotePeerConnection_t1863582616_0_0_0;
static const Il2CppType* GenInst_RemotePeerConnection_t1863582616_0_0_0_Types[] = { &RemotePeerConnection_t1863582616_0_0_0 };
extern const Il2CppGenericInst GenInst_RemotePeerConnection_t1863582616_0_0_0 = { 1, GenInst_RemotePeerConnection_t1863582616_0_0_0_Types };
extern const Il2CppType Message_t869514973_0_0_0;
static const Il2CppType* GenInst_Message_t869514973_0_0_0_Types[] = { &Message_t869514973_0_0_0 };
extern const Il2CppGenericInst GenInst_Message_t869514973_0_0_0 = { 1, GenInst_Message_t869514973_0_0_0_Types };
extern const Il2CppType UnknownMessageReceivedEventArgs_t1694867822_0_0_0;
static const Il2CppType* GenInst_UnknownMessageReceivedEventArgs_t1694867822_0_0_0_Types[] = { &UnknownMessageReceivedEventArgs_t1694867822_0_0_0 };
extern const Il2CppGenericInst GenInst_UnknownMessageReceivedEventArgs_t1694867822_0_0_0 = { 1, GenInst_UnknownMessageReceivedEventArgs_t1694867822_0_0_0_Types };
extern const Il2CppType IPeerConnectorClient_t888178396_0_0_0;
static const Il2CppType* GenInst_IPeerConnectorClient_t888178396_0_0_0_Types[] = { &IPeerConnectorClient_t888178396_0_0_0 };
extern const Il2CppGenericInst GenInst_IPeerConnectorClient_t888178396_0_0_0 = { 1, GenInst_IPeerConnectorClient_t888178396_0_0_0_Types };
extern const Il2CppType WelcomeInfo_t1049730577_0_0_0;
static const Il2CppType* GenInst_WelcomeInfo_t1049730577_0_0_0_Types[] = { &WelcomeInfo_t1049730577_0_0_0 };
extern const Il2CppGenericInst GenInst_WelcomeInfo_t1049730577_0_0_0 = { 1, GenInst_WelcomeInfo_t1049730577_0_0_0_Types };
extern const Il2CppType IServiceBehavior_t43716000_0_0_0;
static const Il2CppType* GenInst_IServiceBehavior_t43716000_0_0_0_Types[] = { &IServiceBehavior_t43716000_0_0_0 };
extern const Il2CppGenericInst GenInst_IServiceBehavior_t43716000_0_0_0 = { 1, GenInst_IServiceBehavior_t43716000_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IServiceBehavior_t43716000_0_0_0_Types[] = { &Type_t_0_0_0, &IServiceBehavior_t43716000_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IServiceBehavior_t43716000_0_0_0 = { 2, GenInst_Type_t_0_0_0_IServiceBehavior_t43716000_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IServiceBehavior_t43716000_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Type_t_0_0_0, &IServiceBehavior_t43716000_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IServiceBehavior_t43716000_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Type_t_0_0_0_IServiceBehavior_t43716000_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t590767935_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t590767935_0_0_0_Types[] = { &KeyValuePair_2_t590767935_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t590767935_0_0_0 = { 1, GenInst_KeyValuePair_2_t590767935_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IServiceBehavior_t43716000_0_0_0_KeyValuePair_2_t590767935_0_0_0_Types[] = { &Type_t_0_0_0, &IServiceBehavior_t43716000_0_0_0, &KeyValuePair_2_t590767935_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IServiceBehavior_t43716000_0_0_0_KeyValuePair_2_t590767935_0_0_0 = { 3, GenInst_Type_t_0_0_0_IServiceBehavior_t43716000_0_0_0_KeyValuePair_2_t590767935_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IServiceBehavior_t43716000_0_0_0_Type_t_0_0_0_Types[] = { &Type_t_0_0_0, &IServiceBehavior_t43716000_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IServiceBehavior_t43716000_0_0_0_Type_t_0_0_0 = { 3, GenInst_Type_t_0_0_0_IServiceBehavior_t43716000_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IServiceBehavior_t43716000_0_0_0_IServiceBehavior_t43716000_0_0_0_Types[] = { &Type_t_0_0_0, &IServiceBehavior_t43716000_0_0_0, &IServiceBehavior_t43716000_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IServiceBehavior_t43716000_0_0_0_IServiceBehavior_t43716000_0_0_0 = { 3, GenInst_Type_t_0_0_0_IServiceBehavior_t43716000_0_0_0_IServiceBehavior_t43716000_0_0_0_Types };
static const Il2CppType* GenInst_RemotePeerConnection_t1863582616_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &RemotePeerConnection_t1863582616_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_RemotePeerConnection_t1863582616_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_RemotePeerConnection_t1863582616_0_0_0_Boolean_t97287965_0_0_0_Types };
extern const Il2CppType IDuplexChannel_t3233783714_0_0_0;
static const Il2CppType* GenInst_IDuplexChannel_t3233783714_0_0_0_Types[] = { &IDuplexChannel_t3233783714_0_0_0 };
extern const Il2CppGenericInst GenInst_IDuplexChannel_t3233783714_0_0_0 = { 1, GenInst_IDuplexChannel_t3233783714_0_0_0_Types };
extern const Il2CppType IInputChannel_t3471598504_0_0_0;
static const Il2CppType* GenInst_IInputChannel_t3471598504_0_0_0_Types[] = { &IInputChannel_t3471598504_0_0_0 };
extern const Il2CppGenericInst GenInst_IInputChannel_t3471598504_0_0_0 = { 1, GenInst_IInputChannel_t3471598504_0_0_0_Types };
static const Il2CppType* GenInst_TimeSpan_t881159249_0_0_0_IInputChannel_t3471598504_0_0_0_Types[] = { &TimeSpan_t881159249_0_0_0, &IInputChannel_t3471598504_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t881159249_0_0_0_IInputChannel_t3471598504_0_0_0 = { 2, GenInst_TimeSpan_t881159249_0_0_0_IInputChannel_t3471598504_0_0_0_Types };
static const Il2CppType* GenInst_TimeSpan_t881159249_0_0_0_IDuplexChannel_t3233783714_0_0_0_Types[] = { &TimeSpan_t881159249_0_0_0, &IDuplexChannel_t3233783714_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t881159249_0_0_0_IDuplexChannel_t3233783714_0_0_0 = { 2, GenInst_TimeSpan_t881159249_0_0_0_IDuplexChannel_t3233783714_0_0_0_Types };
extern const Il2CppType SecurityTokenResolver_t3905425829_0_0_0;
static const Il2CppType* GenInst_SecurityTokenResolver_t3905425829_0_0_0_Types[] = { &SecurityTokenResolver_t3905425829_0_0_0 };
extern const Il2CppGenericInst GenInst_SecurityTokenResolver_t3905425829_0_0_0 = { 1, GenInst_SecurityTokenResolver_t3905425829_0_0_0_Types };
extern const Il2CppType SupportingTokenSpecification_t3961793461_0_0_0;
static const Il2CppType* GenInst_SupportingTokenSpecification_t3961793461_0_0_0_Types[] = { &SupportingTokenSpecification_t3961793461_0_0_0 };
extern const Il2CppGenericInst GenInst_SupportingTokenSpecification_t3961793461_0_0_0 = { 1, GenInst_SupportingTokenSpecification_t3961793461_0_0_0_Types };
extern const Il2CppType SecurityTokenSpecification_t3911394864_0_0_0;
static const Il2CppType* GenInst_SecurityTokenSpecification_t3911394864_0_0_0_Types[] = { &SecurityTokenSpecification_t3911394864_0_0_0 };
extern const Il2CppGenericInst GenInst_SecurityTokenSpecification_t3911394864_0_0_0 = { 1, GenInst_SecurityTokenSpecification_t3911394864_0_0_0_Types };
extern const Il2CppType WSSignedXml_t2681103009_0_0_0;
static const Il2CppType* GenInst_WSSignedXml_t2681103009_0_0_0_Types[] = { &WSSignedXml_t2681103009_0_0_0 };
extern const Il2CppGenericInst GenInst_WSSignedXml_t2681103009_0_0_0 = { 1, GenInst_WSSignedXml_t2681103009_0_0_0_Types };
extern const Il2CppType SignedXml_t2168796367_0_0_0;
static const Il2CppType* GenInst_SignedXml_t2168796367_0_0_0_Types[] = { &SignedXml_t2168796367_0_0_0 };
extern const Il2CppGenericInst GenInst_SignedXml_t2168796367_0_0_0 = { 1, GenInst_SignedXml_t2168796367_0_0_0_Types };
extern const Il2CppType DerivedKeySecurityToken_t1977550489_0_0_0;
static const Il2CppType* GenInst_DerivedKeySecurityToken_t1977550489_0_0_0_Types[] = { &DerivedKeySecurityToken_t1977550489_0_0_0 };
extern const Il2CppGenericInst GenInst_DerivedKeySecurityToken_t1977550489_0_0_0 = { 1, GenInst_DerivedKeySecurityToken_t1977550489_0_0_0_Types };
extern const Il2CppType Wss11SignatureConfirmation_t1374635364_0_0_0;
static const Il2CppType* GenInst_Wss11SignatureConfirmation_t1374635364_0_0_0_Types[] = { &Wss11SignatureConfirmation_t1374635364_0_0_0 };
extern const Il2CppGenericInst GenInst_Wss11SignatureConfirmation_t1374635364_0_0_0 = { 1, GenInst_Wss11SignatureConfirmation_t1374635364_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_SupportingTokenParameters_t907883796_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &SupportingTokenParameters_t907883796_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_SupportingTokenParameters_t907883796_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_SupportingTokenParameters_t907883796_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3090812262_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3090812262_0_0_0_Types[] = { &KeyValuePair_2_t3090812262_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3090812262_0_0_0 = { 1, GenInst_KeyValuePair_2_t3090812262_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_SupportingTokenParameters_t907883796_0_0_0_KeyValuePair_2_t3090812262_0_0_0_Types[] = { &String_t_0_0_0, &SupportingTokenParameters_t907883796_0_0_0, &KeyValuePair_2_t3090812262_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_SupportingTokenParameters_t907883796_0_0_0_KeyValuePair_2_t3090812262_0_0_0 = { 3, GenInst_String_t_0_0_0_SupportingTokenParameters_t907883796_0_0_0_KeyValuePair_2_t3090812262_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_SupportingTokenParameters_t907883796_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &SupportingTokenParameters_t907883796_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_SupportingTokenParameters_t907883796_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_SupportingTokenParameters_t907883796_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_SupportingTokenParameters_t907883796_0_0_0_SupportingTokenParameters_t907883796_0_0_0_Types[] = { &String_t_0_0_0, &SupportingTokenParameters_t907883796_0_0_0, &SupportingTokenParameters_t907883796_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_SupportingTokenParameters_t907883796_0_0_0_SupportingTokenParameters_t907883796_0_0_0 = { 3, GenInst_String_t_0_0_0_SupportingTokenParameters_t907883796_0_0_0_SupportingTokenParameters_t907883796_0_0_0_Types };
extern const Il2CppType ManualResetEvent_t451242010_0_0_0;
static const Il2CppType* GenInst_ManualResetEvent_t451242010_0_0_0_Types[] = { &ManualResetEvent_t451242010_0_0_0 };
extern const Il2CppGenericInst GenInst_ManualResetEvent_t451242010_0_0_0 = { 1, GenInst_ManualResetEvent_t451242010_0_0_0_Types };
extern const Il2CppType EventWaitHandle_t777845177_0_0_0;
static const Il2CppType* GenInst_EventWaitHandle_t777845177_0_0_0_Types[] = { &EventWaitHandle_t777845177_0_0_0 };
extern const Il2CppGenericInst GenInst_EventWaitHandle_t777845177_0_0_0 = { 1, GenInst_EventWaitHandle_t777845177_0_0_0_Types };
extern const Il2CppType WcfListenerInfo_t1525779230_0_0_0;
static const Il2CppType* GenInst_IChannelListener_t114797722_0_0_0_WcfListenerInfo_t1525779230_0_0_0_Types[] = { &IChannelListener_t114797722_0_0_0, &WcfListenerInfo_t1525779230_0_0_0 };
extern const Il2CppGenericInst GenInst_IChannelListener_t114797722_0_0_0_WcfListenerInfo_t1525779230_0_0_0 = { 2, GenInst_IChannelListener_t114797722_0_0_0_WcfListenerInfo_t1525779230_0_0_0_Types };
static const Il2CppType* GenInst_WcfListenerInfo_t1525779230_0_0_0_Types[] = { &WcfListenerInfo_t1525779230_0_0_0 };
extern const Il2CppGenericInst GenInst_WcfListenerInfo_t1525779230_0_0_0 = { 1, GenInst_WcfListenerInfo_t1525779230_0_0_0_Types };
static const Il2CppType* GenInst_IChannelListener_t114797722_0_0_0_WcfListenerInfo_t1525779230_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &IChannelListener_t114797722_0_0_0, &WcfListenerInfo_t1525779230_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_IChannelListener_t114797722_0_0_0_WcfListenerInfo_t1525779230_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_IChannelListener_t114797722_0_0_0_WcfListenerInfo_t1525779230_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1288040851_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1288040851_0_0_0_Types[] = { &KeyValuePair_2_t1288040851_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1288040851_0_0_0 = { 1, GenInst_KeyValuePair_2_t1288040851_0_0_0_Types };
static const Il2CppType* GenInst_IChannelListener_t114797722_0_0_0_WcfListenerInfo_t1525779230_0_0_0_KeyValuePair_2_t1288040851_0_0_0_Types[] = { &IChannelListener_t114797722_0_0_0, &WcfListenerInfo_t1525779230_0_0_0, &KeyValuePair_2_t1288040851_0_0_0 };
extern const Il2CppGenericInst GenInst_IChannelListener_t114797722_0_0_0_WcfListenerInfo_t1525779230_0_0_0_KeyValuePair_2_t1288040851_0_0_0 = { 3, GenInst_IChannelListener_t114797722_0_0_0_WcfListenerInfo_t1525779230_0_0_0_KeyValuePair_2_t1288040851_0_0_0_Types };
static const Il2CppType* GenInst_IChannelListener_t114797722_0_0_0_WcfListenerInfo_t1525779230_0_0_0_IChannelListener_t114797722_0_0_0_Types[] = { &IChannelListener_t114797722_0_0_0, &WcfListenerInfo_t1525779230_0_0_0, &IChannelListener_t114797722_0_0_0 };
extern const Il2CppGenericInst GenInst_IChannelListener_t114797722_0_0_0_WcfListenerInfo_t1525779230_0_0_0_IChannelListener_t114797722_0_0_0 = { 3, GenInst_IChannelListener_t114797722_0_0_0_WcfListenerInfo_t1525779230_0_0_0_IChannelListener_t114797722_0_0_0_Types };
static const Il2CppType* GenInst_IChannelListener_t114797722_0_0_0_WcfListenerInfo_t1525779230_0_0_0_WcfListenerInfo_t1525779230_0_0_0_Types[] = { &IChannelListener_t114797722_0_0_0, &WcfListenerInfo_t1525779230_0_0_0, &WcfListenerInfo_t1525779230_0_0_0 };
extern const Il2CppGenericInst GenInst_IChannelListener_t114797722_0_0_0_WcfListenerInfo_t1525779230_0_0_0_WcfListenerInfo_t1525779230_0_0_0 = { 3, GenInst_IChannelListener_t114797722_0_0_0_WcfListenerInfo_t1525779230_0_0_0_WcfListenerInfo_t1525779230_0_0_0_Types };
extern const Il2CppType AutoResetEvent_t1333520283_0_0_0;
static const Il2CppType* GenInst_HttpContext_t1969259010_0_0_0_AutoResetEvent_t1333520283_0_0_0_Types[] = { &HttpContext_t1969259010_0_0_0, &AutoResetEvent_t1333520283_0_0_0 };
extern const Il2CppGenericInst GenInst_HttpContext_t1969259010_0_0_0_AutoResetEvent_t1333520283_0_0_0 = { 2, GenInst_HttpContext_t1969259010_0_0_0_AutoResetEvent_t1333520283_0_0_0_Types };
static const Il2CppType* GenInst_AutoResetEvent_t1333520283_0_0_0_Types[] = { &AutoResetEvent_t1333520283_0_0_0 };
extern const Il2CppGenericInst GenInst_AutoResetEvent_t1333520283_0_0_0 = { 1, GenInst_AutoResetEvent_t1333520283_0_0_0_Types };
static const Il2CppType* GenInst_HttpContext_t1969259010_0_0_0_AutoResetEvent_t1333520283_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &HttpContext_t1969259010_0_0_0, &AutoResetEvent_t1333520283_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_HttpContext_t1969259010_0_0_0_AutoResetEvent_t1333520283_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_HttpContext_t1969259010_0_0_0_AutoResetEvent_t1333520283_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t788180488_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t788180488_0_0_0_Types[] = { &KeyValuePair_2_t788180488_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t788180488_0_0_0 = { 1, GenInst_KeyValuePair_2_t788180488_0_0_0_Types };
static const Il2CppType* GenInst_HttpContext_t1969259010_0_0_0_AutoResetEvent_t1333520283_0_0_0_KeyValuePair_2_t788180488_0_0_0_Types[] = { &HttpContext_t1969259010_0_0_0, &AutoResetEvent_t1333520283_0_0_0, &KeyValuePair_2_t788180488_0_0_0 };
extern const Il2CppGenericInst GenInst_HttpContext_t1969259010_0_0_0_AutoResetEvent_t1333520283_0_0_0_KeyValuePair_2_t788180488_0_0_0 = { 3, GenInst_HttpContext_t1969259010_0_0_0_AutoResetEvent_t1333520283_0_0_0_KeyValuePair_2_t788180488_0_0_0_Types };
static const Il2CppType* GenInst_HttpContext_t1969259010_0_0_0_AutoResetEvent_t1333520283_0_0_0_HttpContext_t1969259010_0_0_0_Types[] = { &HttpContext_t1969259010_0_0_0, &AutoResetEvent_t1333520283_0_0_0, &HttpContext_t1969259010_0_0_0 };
extern const Il2CppGenericInst GenInst_HttpContext_t1969259010_0_0_0_AutoResetEvent_t1333520283_0_0_0_HttpContext_t1969259010_0_0_0 = { 3, GenInst_HttpContext_t1969259010_0_0_0_AutoResetEvent_t1333520283_0_0_0_HttpContext_t1969259010_0_0_0_Types };
static const Il2CppType* GenInst_HttpContext_t1969259010_0_0_0_AutoResetEvent_t1333520283_0_0_0_AutoResetEvent_t1333520283_0_0_0_Types[] = { &HttpContext_t1969259010_0_0_0, &AutoResetEvent_t1333520283_0_0_0, &AutoResetEvent_t1333520283_0_0_0 };
extern const Il2CppGenericInst GenInst_HttpContext_t1969259010_0_0_0_AutoResetEvent_t1333520283_0_0_0_AutoResetEvent_t1333520283_0_0_0 = { 3, GenInst_HttpContext_t1969259010_0_0_0_AutoResetEvent_t1333520283_0_0_0_AutoResetEvent_t1333520283_0_0_0_Types };
extern const Il2CppType IExtension_1_t2909257122_0_0_0;
static const Il2CppType* GenInst_IExtension_1_t2909257122_0_0_0_Types[] = { &IExtension_1_t2909257122_0_0_0 };
extern const Il2CppGenericInst GenInst_IExtension_1_t2909257122_0_0_0 = { 1, GenInst_IExtension_1_t2909257122_0_0_0_Types };
extern const Il2CppType IExtension_1_t3571061493_0_0_0;
static const Il2CppType* GenInst_IExtension_1_t3571061493_0_0_0_Types[] = { &IExtension_1_t3571061493_0_0_0 };
extern const Il2CppGenericInst GenInst_IExtension_1_t3571061493_0_0_0 = { 1, GenInst_IExtension_1_t3571061493_0_0_0_Types };
extern const Il2CppType SvcHttpHandler_t1718683511_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_SvcHttpHandler_t1718683511_0_0_0_Types[] = { &String_t_0_0_0, &SvcHttpHandler_t1718683511_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_SvcHttpHandler_t1718683511_0_0_0 = { 2, GenInst_String_t_0_0_0_SvcHttpHandler_t1718683511_0_0_0_Types };
static const Il2CppType* GenInst_SvcHttpHandler_t1718683511_0_0_0_Types[] = { &SvcHttpHandler_t1718683511_0_0_0 };
extern const Il2CppGenericInst GenInst_SvcHttpHandler_t1718683511_0_0_0 = { 1, GenInst_SvcHttpHandler_t1718683511_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_SvcHttpHandler_t1718683511_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &SvcHttpHandler_t1718683511_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_SvcHttpHandler_t1718683511_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_SvcHttpHandler_t1718683511_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3901611977_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3901611977_0_0_0_Types[] = { &KeyValuePair_2_t3901611977_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3901611977_0_0_0 = { 1, GenInst_KeyValuePair_2_t3901611977_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_SvcHttpHandler_t1718683511_0_0_0_KeyValuePair_2_t3901611977_0_0_0_Types[] = { &String_t_0_0_0, &SvcHttpHandler_t1718683511_0_0_0, &KeyValuePair_2_t3901611977_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_SvcHttpHandler_t1718683511_0_0_0_KeyValuePair_2_t3901611977_0_0_0 = { 3, GenInst_String_t_0_0_0_SvcHttpHandler_t1718683511_0_0_0_KeyValuePair_2_t3901611977_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_SvcHttpHandler_t1718683511_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &SvcHttpHandler_t1718683511_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_SvcHttpHandler_t1718683511_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_SvcHttpHandler_t1718683511_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_SvcHttpHandler_t1718683511_0_0_0_SvcHttpHandler_t1718683511_0_0_0_Types[] = { &String_t_0_0_0, &SvcHttpHandler_t1718683511_0_0_0, &SvcHttpHandler_t1718683511_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_SvcHttpHandler_t1718683511_0_0_0_SvcHttpHandler_t1718683511_0_0_0 = { 3, GenInst_String_t_0_0_0_SvcHttpHandler_t1718683511_0_0_0_SvcHttpHandler_t1718683511_0_0_0_Types };
static const Il2CppType* GenInst_SvcHttpHandler_t1718683511_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &SvcHttpHandler_t1718683511_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_SvcHttpHandler_t1718683511_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_SvcHttpHandler_t1718683511_0_0_0_Boolean_t97287965_0_0_0_Types };
extern const Il2CppType ChannelDispatcherBase_t2436929862_0_0_0;
static const Il2CppType* GenInst_ChannelDispatcherBase_t2436929862_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &ChannelDispatcherBase_t2436929862_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_ChannelDispatcherBase_t2436929862_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_ChannelDispatcherBase_t2436929862_0_0_0_Boolean_t97287965_0_0_0_Types };
static const Il2CppType* GenInst_ChannelDispatcherBase_t2436929862_0_0_0_Types[] = { &ChannelDispatcherBase_t2436929862_0_0_0 };
extern const Il2CppGenericInst GenInst_ChannelDispatcherBase_t2436929862_0_0_0 = { 1, GenInst_ChannelDispatcherBase_t2436929862_0_0_0_Types };
extern const Il2CppType TcpDuplexSessionChannel_t645469680_0_0_0;
static const Il2CppType* GenInst_TcpDuplexSessionChannel_t645469680_0_0_0_Types[] = { &TcpDuplexSessionChannel_t645469680_0_0_0 };
extern const Il2CppGenericInst GenInst_TcpDuplexSessionChannel_t645469680_0_0_0 = { 1, GenInst_TcpDuplexSessionChannel_t645469680_0_0_0_Types };
extern const Il2CppType ISessionChannel_1_t2045330825_0_0_0;
static const Il2CppType* GenInst_ISessionChannel_1_t2045330825_0_0_0_Types[] = { &ISessionChannel_1_t2045330825_0_0_0 };
extern const Il2CppGenericInst GenInst_ISessionChannel_1_t2045330825_0_0_0 = { 1, GenInst_ISessionChannel_1_t2045330825_0_0_0_Types };
extern const Il2CppType DuplexChannelBase_t2906515918_0_0_0;
static const Il2CppType* GenInst_DuplexChannelBase_t2906515918_0_0_0_Types[] = { &DuplexChannelBase_t2906515918_0_0_0 };
extern const Il2CppGenericInst GenInst_DuplexChannelBase_t2906515918_0_0_0 = { 1, GenInst_DuplexChannelBase_t2906515918_0_0_0_Types };
extern const Il2CppType ChannelBase_t1586116283_0_0_0;
static const Il2CppType* GenInst_ChannelBase_t1586116283_0_0_0_Types[] = { &ChannelBase_t1586116283_0_0_0 };
extern const Il2CppGenericInst GenInst_ChannelBase_t1586116283_0_0_0 = { 1, GenInst_ChannelBase_t1586116283_0_0_0_Types };
extern const Il2CppType IDefaultCommunicationTimeouts_t2848643016_0_0_0;
static const Il2CppType* GenInst_IDefaultCommunicationTimeouts_t2848643016_0_0_0_Types[] = { &IDefaultCommunicationTimeouts_t2848643016_0_0_0 };
extern const Il2CppGenericInst GenInst_IDefaultCommunicationTimeouts_t2848643016_0_0_0 = { 1, GenInst_IDefaultCommunicationTimeouts_t2848643016_0_0_0_Types };
extern const Il2CppType CommunicationObject_t518829156_0_0_0;
static const Il2CppType* GenInst_CommunicationObject_t518829156_0_0_0_Types[] = { &CommunicationObject_t518829156_0_0_0 };
extern const Il2CppGenericInst GenInst_CommunicationObject_t518829156_0_0_0 = { 1, GenInst_CommunicationObject_t518829156_0_0_0_Types };
extern const Il2CppType EncryptedData_t3129875747_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_EncryptedData_t3129875747_0_0_0_Types[] = { &String_t_0_0_0, &EncryptedData_t3129875747_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_EncryptedData_t3129875747_0_0_0 = { 2, GenInst_String_t_0_0_0_EncryptedData_t3129875747_0_0_0_Types };
static const Il2CppType* GenInst_EncryptedData_t3129875747_0_0_0_Types[] = { &EncryptedData_t3129875747_0_0_0 };
extern const Il2CppGenericInst GenInst_EncryptedData_t3129875747_0_0_0 = { 1, GenInst_EncryptedData_t3129875747_0_0_0_Types };
extern const Il2CppType EncryptedType_t2124600183_0_0_0;
static const Il2CppType* GenInst_EncryptedType_t2124600183_0_0_0_Types[] = { &EncryptedType_t2124600183_0_0_0 };
extern const Il2CppGenericInst GenInst_EncryptedType_t2124600183_0_0_0 = { 1, GenInst_EncryptedType_t2124600183_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_EncryptedData_t3129875747_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &EncryptedData_t3129875747_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_EncryptedData_t3129875747_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_EncryptedData_t3129875747_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1017836917_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1017836917_0_0_0_Types[] = { &KeyValuePair_2_t1017836917_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1017836917_0_0_0 = { 1, GenInst_KeyValuePair_2_t1017836917_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_EncryptedData_t3129875747_0_0_0_KeyValuePair_2_t1017836917_0_0_0_Types[] = { &String_t_0_0_0, &EncryptedData_t3129875747_0_0_0, &KeyValuePair_2_t1017836917_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_EncryptedData_t3129875747_0_0_0_KeyValuePair_2_t1017836917_0_0_0 = { 3, GenInst_String_t_0_0_0_EncryptedData_t3129875747_0_0_0_KeyValuePair_2_t1017836917_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_EncryptedData_t3129875747_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &EncryptedData_t3129875747_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_EncryptedData_t3129875747_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_EncryptedData_t3129875747_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_EncryptedData_t3129875747_0_0_0_EncryptedData_t3129875747_0_0_0_Types[] = { &String_t_0_0_0, &EncryptedData_t3129875747_0_0_0, &EncryptedData_t3129875747_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_EncryptedData_t3129875747_0_0_0_EncryptedData_t3129875747_0_0_0 = { 3, GenInst_String_t_0_0_0_EncryptedData_t3129875747_0_0_0_EncryptedData_t3129875747_0_0_0_Types };
extern const Il2CppType ReferenceList_t2222396100_0_0_0;
static const Il2CppType* GenInst_ReferenceList_t2222396100_0_0_0_Types[] = { &ReferenceList_t2222396100_0_0_0 };
extern const Il2CppGenericInst GenInst_ReferenceList_t2222396100_0_0_0 = { 1, GenInst_ReferenceList_t2222396100_0_0_0_Types };
extern const Il2CppType WrappedKeySecurityToken_t738218815_0_0_0;
static const Il2CppType* GenInst_WrappedKeySecurityToken_t738218815_0_0_0_Types[] = { &WrappedKeySecurityToken_t738218815_0_0_0 };
extern const Il2CppGenericInst GenInst_WrappedKeySecurityToken_t738218815_0_0_0 = { 1, GenInst_WrappedKeySecurityToken_t738218815_0_0_0_Types };
extern const Il2CppType BaseAddressElement_t4110471995_0_0_0;
static const Il2CppType* GenInst_BaseAddressElement_t4110471995_0_0_0_Types[] = { &BaseAddressElement_t4110471995_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseAddressElement_t4110471995_0_0_0 = { 1, GenInst_BaseAddressElement_t4110471995_0_0_0_Types };
extern const Il2CppType BasicHttpBinding_t4145755420_0_0_0;
extern const Il2CppType BasicHttpBindingElement_t4053390136_0_0_0;
static const Il2CppType* GenInst_BasicHttpBinding_t4145755420_0_0_0_BasicHttpBindingElement_t4053390136_0_0_0_Types[] = { &BasicHttpBinding_t4145755420_0_0_0, &BasicHttpBindingElement_t4053390136_0_0_0 };
extern const Il2CppGenericInst GenInst_BasicHttpBinding_t4145755420_0_0_0_BasicHttpBindingElement_t4053390136_0_0_0 = { 2, GenInst_BasicHttpBinding_t4145755420_0_0_0_BasicHttpBindingElement_t4053390136_0_0_0_Types };
extern const Il2CppType IBindingConfigurationElement_t3662973274_0_0_0;
static const Il2CppType* GenInst_IBindingConfigurationElement_t3662973274_0_0_0_Types[] = { &IBindingConfigurationElement_t3662973274_0_0_0 };
extern const Il2CppGenericInst GenInst_IBindingConfigurationElement_t3662973274_0_0_0 = { 1, GenInst_IBindingConfigurationElement_t3662973274_0_0_0_Types };
static const Il2CppType* GenInst_BasicHttpBindingElement_t4053390136_0_0_0_Types[] = { &BasicHttpBindingElement_t4053390136_0_0_0 };
extern const Il2CppGenericInst GenInst_BasicHttpBindingElement_t4053390136_0_0_0 = { 1, GenInst_BasicHttpBindingElement_t4053390136_0_0_0_Types };
extern const Il2CppType ExtensionElement_t875191958_0_0_0;
static const Il2CppType* GenInst_ExtensionElement_t875191958_0_0_0_Types[] = { &ExtensionElement_t875191958_0_0_0 };
extern const Il2CppGenericInst GenInst_ExtensionElement_t875191958_0_0_0 = { 1, GenInst_ExtensionElement_t875191958_0_0_0_Types };
extern const Il2CppType ChannelEndpointElement_t3229391106_0_0_0;
static const Il2CppType* GenInst_ChannelEndpointElement_t3229391106_0_0_0_Types[] = { &ChannelEndpointElement_t3229391106_0_0_0 };
extern const Il2CppGenericInst GenInst_ChannelEndpointElement_t3229391106_0_0_0 = { 1, GenInst_ChannelEndpointElement_t3229391106_0_0_0_Types };
extern const Il2CppType ClaimTypeElement_t2235917530_0_0_0;
static const Il2CppType* GenInst_ClaimTypeElement_t2235917530_0_0_0_Types[] = { &ClaimTypeElement_t2235917530_0_0_0 };
extern const Il2CppGenericInst GenInst_ClaimTypeElement_t2235917530_0_0_0 = { 1, GenInst_ClaimTypeElement_t2235917530_0_0_0_Types };
extern const Il2CppType PolicyImporterElement_t611423477_0_0_0;
static const Il2CppType* GenInst_PolicyImporterElement_t611423477_0_0_0_Types[] = { &PolicyImporterElement_t611423477_0_0_0 };
extern const Il2CppGenericInst GenInst_PolicyImporterElement_t611423477_0_0_0 = { 1, GenInst_PolicyImporterElement_t611423477_0_0_0_Types };
extern const Il2CppType WsdlImporterElement_t929425501_0_0_0;
static const Il2CppType* GenInst_WsdlImporterElement_t929425501_0_0_0_Types[] = { &WsdlImporterElement_t929425501_0_0_0 };
extern const Il2CppGenericInst GenInst_WsdlImporterElement_t929425501_0_0_0 = { 1, GenInst_WsdlImporterElement_t929425501_0_0_0_Types };
extern const Il2CppType CustomBindingElement_t3873939656_0_0_0;
static const Il2CppType* GenInst_CustomBindingElement_t3873939656_0_0_0_Types[] = { &CustomBindingElement_t3873939656_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomBindingElement_t3873939656_0_0_0 = { 1, GenInst_CustomBindingElement_t3873939656_0_0_0_Types };
extern const Il2CppType BindingElementExtensionElement_t2478436489_0_0_0;
static const Il2CppType* GenInst_BindingElementExtensionElement_t2478436489_0_0_0_Types[] = { &BindingElementExtensionElement_t2478436489_0_0_0 };
extern const Il2CppGenericInst GenInst_BindingElementExtensionElement_t2478436489_0_0_0 = { 1, GenInst_BindingElementExtensionElement_t2478436489_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_BindingElementExtensionElement_t2478436489_0_0_0_Types[] = { &Type_t_0_0_0, &BindingElementExtensionElement_t2478436489_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_BindingElementExtensionElement_t2478436489_0_0_0 = { 2, GenInst_Type_t_0_0_0_BindingElementExtensionElement_t2478436489_0_0_0_Types };
extern const Il2CppType ServiceModelExtensionElement_t2931985411_0_0_0;
static const Il2CppType* GenInst_ServiceModelExtensionElement_t2931985411_0_0_0_Types[] = { &ServiceModelExtensionElement_t2931985411_0_0_0 };
extern const Il2CppGenericInst GenInst_ServiceModelExtensionElement_t2931985411_0_0_0 = { 1, GenInst_ServiceModelExtensionElement_t2931985411_0_0_0_Types };
extern const Il2CppType ConfigurationElement_t3318566633_0_0_0;
static const Il2CppType* GenInst_ConfigurationElement_t3318566633_0_0_0_Types[] = { &ConfigurationElement_t3318566633_0_0_0 };
extern const Il2CppGenericInst GenInst_ConfigurationElement_t3318566633_0_0_0 = { 1, GenInst_ConfigurationElement_t3318566633_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_BindingElementExtensionElement_t2478436489_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Type_t_0_0_0, &BindingElementExtensionElement_t2478436489_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_BindingElementExtensionElement_t2478436489_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Type_t_0_0_0_BindingElementExtensionElement_t2478436489_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3025488424_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3025488424_0_0_0_Types[] = { &KeyValuePair_2_t3025488424_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3025488424_0_0_0 = { 1, GenInst_KeyValuePair_2_t3025488424_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_BindingElementExtensionElement_t2478436489_0_0_0_KeyValuePair_2_t3025488424_0_0_0_Types[] = { &Type_t_0_0_0, &BindingElementExtensionElement_t2478436489_0_0_0, &KeyValuePair_2_t3025488424_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_BindingElementExtensionElement_t2478436489_0_0_0_KeyValuePair_2_t3025488424_0_0_0 = { 3, GenInst_Type_t_0_0_0_BindingElementExtensionElement_t2478436489_0_0_0_KeyValuePair_2_t3025488424_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_BindingElementExtensionElement_t2478436489_0_0_0_Type_t_0_0_0_Types[] = { &Type_t_0_0_0, &BindingElementExtensionElement_t2478436489_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_BindingElementExtensionElement_t2478436489_0_0_0_Type_t_0_0_0 = { 3, GenInst_Type_t_0_0_0_BindingElementExtensionElement_t2478436489_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_BindingElementExtensionElement_t2478436489_0_0_0_BindingElementExtensionElement_t2478436489_0_0_0_Types[] = { &Type_t_0_0_0, &BindingElementExtensionElement_t2478436489_0_0_0, &BindingElementExtensionElement_t2478436489_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_BindingElementExtensionElement_t2478436489_0_0_0_BindingElementExtensionElement_t2478436489_0_0_0 = { 3, GenInst_Type_t_0_0_0_BindingElementExtensionElement_t2478436489_0_0_0_BindingElementExtensionElement_t2478436489_0_0_0_Types };
extern const Il2CppType BehaviorExtensionElement_t4188976535_0_0_0;
static const Il2CppType* GenInst_BehaviorExtensionElement_t4188976535_0_0_0_Types[] = { &BehaviorExtensionElement_t4188976535_0_0_0 };
extern const Il2CppGenericInst GenInst_BehaviorExtensionElement_t4188976535_0_0_0 = { 1, GenInst_BehaviorExtensionElement_t4188976535_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_BehaviorExtensionElement_t4188976535_0_0_0_Types[] = { &Type_t_0_0_0, &BehaviorExtensionElement_t4188976535_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_BehaviorExtensionElement_t4188976535_0_0_0 = { 2, GenInst_Type_t_0_0_0_BehaviorExtensionElement_t4188976535_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_BehaviorExtensionElement_t4188976535_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Type_t_0_0_0, &BehaviorExtensionElement_t4188976535_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_BehaviorExtensionElement_t4188976535_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Type_t_0_0_0_BehaviorExtensionElement_t4188976535_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t441061174_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t441061174_0_0_0_Types[] = { &KeyValuePair_2_t441061174_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t441061174_0_0_0 = { 1, GenInst_KeyValuePair_2_t441061174_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_BehaviorExtensionElement_t4188976535_0_0_0_KeyValuePair_2_t441061174_0_0_0_Types[] = { &Type_t_0_0_0, &BehaviorExtensionElement_t4188976535_0_0_0, &KeyValuePair_2_t441061174_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_BehaviorExtensionElement_t4188976535_0_0_0_KeyValuePair_2_t441061174_0_0_0 = { 3, GenInst_Type_t_0_0_0_BehaviorExtensionElement_t4188976535_0_0_0_KeyValuePair_2_t441061174_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_BehaviorExtensionElement_t4188976535_0_0_0_Type_t_0_0_0_Types[] = { &Type_t_0_0_0, &BehaviorExtensionElement_t4188976535_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_BehaviorExtensionElement_t4188976535_0_0_0_Type_t_0_0_0 = { 3, GenInst_Type_t_0_0_0_BehaviorExtensionElement_t4188976535_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_BehaviorExtensionElement_t4188976535_0_0_0_BehaviorExtensionElement_t4188976535_0_0_0_Types[] = { &Type_t_0_0_0, &BehaviorExtensionElement_t4188976535_0_0_0, &BehaviorExtensionElement_t4188976535_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_BehaviorExtensionElement_t4188976535_0_0_0_BehaviorExtensionElement_t4188976535_0_0_0 = { 3, GenInst_Type_t_0_0_0_BehaviorExtensionElement_t4188976535_0_0_0_BehaviorExtensionElement_t4188976535_0_0_0_Types };
extern const Il2CppType EndpointBehaviorElement_t2191068479_0_0_0;
static const Il2CppType* GenInst_EndpointBehaviorElement_t2191068479_0_0_0_Types[] = { &EndpointBehaviorElement_t2191068479_0_0_0 };
extern const Il2CppGenericInst GenInst_EndpointBehaviorElement_t2191068479_0_0_0 = { 1, GenInst_EndpointBehaviorElement_t2191068479_0_0_0_Types };
extern const Il2CppType IssuedTokenClientBehaviorsElement_t1890443082_0_0_0;
static const Il2CppType* GenInst_IssuedTokenClientBehaviorsElement_t1890443082_0_0_0_Types[] = { &IssuedTokenClientBehaviorsElement_t1890443082_0_0_0 };
extern const Il2CppGenericInst GenInst_IssuedTokenClientBehaviorsElement_t1890443082_0_0_0 = { 1, GenInst_IssuedTokenClientBehaviorsElement_t1890443082_0_0_0_Types };
extern const Il2CppType MessageSecurityVersion_t2079539966_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_MessageSecurityVersion_t2079539966_0_0_0_Types[] = { &String_t_0_0_0, &MessageSecurityVersion_t2079539966_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MessageSecurityVersion_t2079539966_0_0_0 = { 2, GenInst_String_t_0_0_0_MessageSecurityVersion_t2079539966_0_0_0_Types };
static const Il2CppType* GenInst_MessageSecurityVersion_t2079539966_0_0_0_Types[] = { &MessageSecurityVersion_t2079539966_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageSecurityVersion_t2079539966_0_0_0 = { 1, GenInst_MessageSecurityVersion_t2079539966_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_MessageSecurityVersion_t2079539966_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &MessageSecurityVersion_t2079539966_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MessageSecurityVersion_t2079539966_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_MessageSecurityVersion_t2079539966_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4262468432_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4262468432_0_0_0_Types[] = { &KeyValuePair_2_t4262468432_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4262468432_0_0_0 = { 1, GenInst_KeyValuePair_2_t4262468432_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_MessageSecurityVersion_t2079539966_0_0_0_KeyValuePair_2_t4262468432_0_0_0_Types[] = { &String_t_0_0_0, &MessageSecurityVersion_t2079539966_0_0_0, &KeyValuePair_2_t4262468432_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MessageSecurityVersion_t2079539966_0_0_0_KeyValuePair_2_t4262468432_0_0_0 = { 3, GenInst_String_t_0_0_0_MessageSecurityVersion_t2079539966_0_0_0_KeyValuePair_2_t4262468432_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_MessageSecurityVersion_t2079539966_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &MessageSecurityVersion_t2079539966_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MessageSecurityVersion_t2079539966_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_MessageSecurityVersion_t2079539966_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_MessageSecurityVersion_t2079539966_0_0_0_MessageSecurityVersion_t2079539966_0_0_0_Types[] = { &String_t_0_0_0, &MessageSecurityVersion_t2079539966_0_0_0, &MessageSecurityVersion_t2079539966_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MessageSecurityVersion_t2079539966_0_0_0_MessageSecurityVersion_t2079539966_0_0_0 = { 3, GenInst_String_t_0_0_0_MessageSecurityVersion_t2079539966_0_0_0_MessageSecurityVersion_t2079539966_0_0_0_Types };
extern const Il2CppType WSHttpBinding_t3299795862_0_0_0;
extern const Il2CppType MexHttpBindingElement_t3331489804_0_0_0;
static const Il2CppType* GenInst_WSHttpBinding_t3299795862_0_0_0_MexHttpBindingElement_t3331489804_0_0_0_Types[] = { &WSHttpBinding_t3299795862_0_0_0, &MexHttpBindingElement_t3331489804_0_0_0 };
extern const Il2CppGenericInst GenInst_WSHttpBinding_t3299795862_0_0_0_MexHttpBindingElement_t3331489804_0_0_0 = { 2, GenInst_WSHttpBinding_t3299795862_0_0_0_MexHttpBindingElement_t3331489804_0_0_0_Types };
static const Il2CppType* GenInst_MexHttpBindingElement_t3331489804_0_0_0_Types[] = { &MexHttpBindingElement_t3331489804_0_0_0 };
extern const Il2CppGenericInst GenInst_MexHttpBindingElement_t3331489804_0_0_0 = { 1, GenInst_MexHttpBindingElement_t3331489804_0_0_0_Types };
static const Il2CppType* GenInst_WSHttpBinding_t3299795862_0_0_0_Types[] = { &WSHttpBinding_t3299795862_0_0_0 };
extern const Il2CppGenericInst GenInst_WSHttpBinding_t3299795862_0_0_0 = { 1, GenInst_WSHttpBinding_t3299795862_0_0_0_Types };
extern const Il2CppType MexHttpsBindingElement_t1326184832_0_0_0;
static const Il2CppType* GenInst_WSHttpBinding_t3299795862_0_0_0_MexHttpsBindingElement_t1326184832_0_0_0_Types[] = { &WSHttpBinding_t3299795862_0_0_0, &MexHttpsBindingElement_t1326184832_0_0_0 };
extern const Il2CppGenericInst GenInst_WSHttpBinding_t3299795862_0_0_0_MexHttpsBindingElement_t1326184832_0_0_0 = { 2, GenInst_WSHttpBinding_t3299795862_0_0_0_MexHttpsBindingElement_t1326184832_0_0_0_Types };
static const Il2CppType* GenInst_MexHttpsBindingElement_t1326184832_0_0_0_Types[] = { &MexHttpsBindingElement_t1326184832_0_0_0 };
extern const Il2CppGenericInst GenInst_MexHttpsBindingElement_t1326184832_0_0_0 = { 1, GenInst_MexHttpsBindingElement_t1326184832_0_0_0_Types };
extern const Il2CppType CustomBinding_t4187704225_0_0_0;
extern const Il2CppType MexNamedPipeBindingElement_t936983379_0_0_0;
static const Il2CppType* GenInst_CustomBinding_t4187704225_0_0_0_MexNamedPipeBindingElement_t936983379_0_0_0_Types[] = { &CustomBinding_t4187704225_0_0_0, &MexNamedPipeBindingElement_t936983379_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomBinding_t4187704225_0_0_0_MexNamedPipeBindingElement_t936983379_0_0_0 = { 2, GenInst_CustomBinding_t4187704225_0_0_0_MexNamedPipeBindingElement_t936983379_0_0_0_Types };
static const Il2CppType* GenInst_MexNamedPipeBindingElement_t936983379_0_0_0_Types[] = { &MexNamedPipeBindingElement_t936983379_0_0_0 };
extern const Il2CppGenericInst GenInst_MexNamedPipeBindingElement_t936983379_0_0_0 = { 1, GenInst_MexNamedPipeBindingElement_t936983379_0_0_0_Types };
static const Il2CppType* GenInst_CustomBinding_t4187704225_0_0_0_Types[] = { &CustomBinding_t4187704225_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomBinding_t4187704225_0_0_0 = { 1, GenInst_CustomBinding_t4187704225_0_0_0_Types };
extern const Il2CppType MexTcpBindingElement_t1365091002_0_0_0;
static const Il2CppType* GenInst_CustomBinding_t4187704225_0_0_0_MexTcpBindingElement_t1365091002_0_0_0_Types[] = { &CustomBinding_t4187704225_0_0_0, &MexTcpBindingElement_t1365091002_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomBinding_t4187704225_0_0_0_MexTcpBindingElement_t1365091002_0_0_0 = { 2, GenInst_CustomBinding_t4187704225_0_0_0_MexTcpBindingElement_t1365091002_0_0_0_Types };
static const Il2CppType* GenInst_MexTcpBindingElement_t1365091002_0_0_0_Types[] = { &MexTcpBindingElement_t1365091002_0_0_0 };
extern const Il2CppGenericInst GenInst_MexTcpBindingElement_t1365091002_0_0_0 = { 1, GenInst_MexTcpBindingElement_t1365091002_0_0_0_Types };
extern const Il2CppType MsmqIntegrationBinding_t1515740572_0_0_0;
extern const Il2CppType MsmqIntegrationBindingElement_t4143405573_0_0_0;
static const Il2CppType* GenInst_MsmqIntegrationBinding_t1515740572_0_0_0_MsmqIntegrationBindingElement_t4143405573_0_0_0_Types[] = { &MsmqIntegrationBinding_t1515740572_0_0_0, &MsmqIntegrationBindingElement_t4143405573_0_0_0 };
extern const Il2CppGenericInst GenInst_MsmqIntegrationBinding_t1515740572_0_0_0_MsmqIntegrationBindingElement_t4143405573_0_0_0 = { 2, GenInst_MsmqIntegrationBinding_t1515740572_0_0_0_MsmqIntegrationBindingElement_t4143405573_0_0_0_Types };
static const Il2CppType* GenInst_MsmqIntegrationBindingElement_t4143405573_0_0_0_Types[] = { &MsmqIntegrationBindingElement_t4143405573_0_0_0 };
extern const Il2CppGenericInst GenInst_MsmqIntegrationBindingElement_t4143405573_0_0_0 = { 1, GenInst_MsmqIntegrationBindingElement_t4143405573_0_0_0_Types };
extern const Il2CppType NetMsmqBinding_t3496970231_0_0_0;
extern const Il2CppType NetMsmqBindingElement_t4085042952_0_0_0;
static const Il2CppType* GenInst_NetMsmqBinding_t3496970231_0_0_0_NetMsmqBindingElement_t4085042952_0_0_0_Types[] = { &NetMsmqBinding_t3496970231_0_0_0, &NetMsmqBindingElement_t4085042952_0_0_0 };
extern const Il2CppGenericInst GenInst_NetMsmqBinding_t3496970231_0_0_0_NetMsmqBindingElement_t4085042952_0_0_0 = { 2, GenInst_NetMsmqBinding_t3496970231_0_0_0_NetMsmqBindingElement_t4085042952_0_0_0_Types };
static const Il2CppType* GenInst_NetMsmqBindingElement_t4085042952_0_0_0_Types[] = { &NetMsmqBindingElement_t4085042952_0_0_0 };
extern const Il2CppGenericInst GenInst_NetMsmqBindingElement_t4085042952_0_0_0 = { 1, GenInst_NetMsmqBindingElement_t4085042952_0_0_0_Types };
extern const Il2CppType NetNamedPipeBinding_t983330350_0_0_0;
extern const Il2CppType NetNamedPipeBindingElement_t1517060569_0_0_0;
static const Il2CppType* GenInst_NetNamedPipeBinding_t983330350_0_0_0_NetNamedPipeBindingElement_t1517060569_0_0_0_Types[] = { &NetNamedPipeBinding_t983330350_0_0_0, &NetNamedPipeBindingElement_t1517060569_0_0_0 };
extern const Il2CppGenericInst GenInst_NetNamedPipeBinding_t983330350_0_0_0_NetNamedPipeBindingElement_t1517060569_0_0_0 = { 2, GenInst_NetNamedPipeBinding_t983330350_0_0_0_NetNamedPipeBindingElement_t1517060569_0_0_0_Types };
static const Il2CppType* GenInst_NetNamedPipeBindingElement_t1517060569_0_0_0_Types[] = { &NetNamedPipeBindingElement_t1517060569_0_0_0 };
extern const Il2CppGenericInst GenInst_NetNamedPipeBindingElement_t1517060569_0_0_0 = { 1, GenInst_NetNamedPipeBindingElement_t1517060569_0_0_0_Types };
extern const Il2CppType NetPeerTcpBinding_t3507636973_0_0_0;
extern const Il2CppType NetPeerTcpBindingElement_t764197943_0_0_0;
static const Il2CppType* GenInst_NetPeerTcpBinding_t3507636973_0_0_0_NetPeerTcpBindingElement_t764197943_0_0_0_Types[] = { &NetPeerTcpBinding_t3507636973_0_0_0, &NetPeerTcpBindingElement_t764197943_0_0_0 };
extern const Il2CppGenericInst GenInst_NetPeerTcpBinding_t3507636973_0_0_0_NetPeerTcpBindingElement_t764197943_0_0_0 = { 2, GenInst_NetPeerTcpBinding_t3507636973_0_0_0_NetPeerTcpBindingElement_t764197943_0_0_0_Types };
static const Il2CppType* GenInst_NetPeerTcpBindingElement_t764197943_0_0_0_Types[] = { &NetPeerTcpBindingElement_t764197943_0_0_0 };
extern const Il2CppGenericInst GenInst_NetPeerTcpBindingElement_t764197943_0_0_0 = { 1, GenInst_NetPeerTcpBindingElement_t764197943_0_0_0_Types };
static const Il2CppType* GenInst_IBindingConfigurationElement_t3662973274_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &IBindingConfigurationElement_t3662973274_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_IBindingConfigurationElement_t3662973274_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_IBindingConfigurationElement_t3662973274_0_0_0_Boolean_t97287965_0_0_0_Types };
extern const Il2CppType NetTcpBinding_t2266210195_0_0_0;
extern const Il2CppType NetTcpBindingElement_t1442378023_0_0_0;
static const Il2CppType* GenInst_NetTcpBinding_t2266210195_0_0_0_NetTcpBindingElement_t1442378023_0_0_0_Types[] = { &NetTcpBinding_t2266210195_0_0_0, &NetTcpBindingElement_t1442378023_0_0_0 };
extern const Il2CppGenericInst GenInst_NetTcpBinding_t2266210195_0_0_0_NetTcpBindingElement_t1442378023_0_0_0 = { 2, GenInst_NetTcpBinding_t2266210195_0_0_0_NetTcpBindingElement_t1442378023_0_0_0_Types };
static const Il2CppType* GenInst_NetTcpBindingElement_t1442378023_0_0_0_Types[] = { &NetTcpBindingElement_t1442378023_0_0_0 };
extern const Il2CppGenericInst GenInst_NetTcpBindingElement_t1442378023_0_0_0 = { 1, GenInst_NetTcpBindingElement_t1442378023_0_0_0_Types };
extern const Il2CppType ServiceBehaviorElement_t2597675026_0_0_0;
static const Il2CppType* GenInst_ServiceBehaviorElement_t2597675026_0_0_0_Types[] = { &ServiceBehaviorElement_t2597675026_0_0_0 };
extern const Il2CppGenericInst GenInst_ServiceBehaviorElement_t2597675026_0_0_0 = { 1, GenInst_ServiceBehaviorElement_t2597675026_0_0_0_Types };
extern const Il2CppType ServiceElement_t3010554678_0_0_0;
static const Il2CppType* GenInst_ServiceElement_t3010554678_0_0_0_Types[] = { &ServiceElement_t3010554678_0_0_0 };
extern const Il2CppGenericInst GenInst_ServiceElement_t3010554678_0_0_0 = { 1, GenInst_ServiceElement_t3010554678_0_0_0_Types };
extern const Il2CppType ServiceEndpointElement_t1873720233_0_0_0;
static const Il2CppType* GenInst_ServiceEndpointElement_t1873720233_0_0_0_Types[] = { &ServiceEndpointElement_t1873720233_0_0_0 };
extern const Il2CppGenericInst GenInst_ServiceEndpointElement_t1873720233_0_0_0 = { 1, GenInst_ServiceEndpointElement_t1873720233_0_0_0_Types };
extern const Il2CppType WS2007FederationHttpBinding_t502258771_0_0_0;
extern const Il2CppType WS2007FederationHttpBindingElement_t3751332056_0_0_0;
static const Il2CppType* GenInst_WS2007FederationHttpBinding_t502258771_0_0_0_WS2007FederationHttpBindingElement_t3751332056_0_0_0_Types[] = { &WS2007FederationHttpBinding_t502258771_0_0_0, &WS2007FederationHttpBindingElement_t3751332056_0_0_0 };
extern const Il2CppGenericInst GenInst_WS2007FederationHttpBinding_t502258771_0_0_0_WS2007FederationHttpBindingElement_t3751332056_0_0_0 = { 2, GenInst_WS2007FederationHttpBinding_t502258771_0_0_0_WS2007FederationHttpBindingElement_t3751332056_0_0_0_Types };
static const Il2CppType* GenInst_WS2007FederationHttpBindingElement_t3751332056_0_0_0_Types[] = { &WS2007FederationHttpBindingElement_t3751332056_0_0_0 };
extern const Il2CppGenericInst GenInst_WS2007FederationHttpBindingElement_t3751332056_0_0_0 = { 1, GenInst_WS2007FederationHttpBindingElement_t3751332056_0_0_0_Types };
extern const Il2CppType WS2007HttpBinding_t3179680017_0_0_0;
extern const Il2CppType WS2007HttpBindingElement_t1255517706_0_0_0;
static const Il2CppType* GenInst_WS2007HttpBinding_t3179680017_0_0_0_WS2007HttpBindingElement_t1255517706_0_0_0_Types[] = { &WS2007HttpBinding_t3179680017_0_0_0, &WS2007HttpBindingElement_t1255517706_0_0_0 };
extern const Il2CppGenericInst GenInst_WS2007HttpBinding_t3179680017_0_0_0_WS2007HttpBindingElement_t1255517706_0_0_0 = { 2, GenInst_WS2007HttpBinding_t3179680017_0_0_0_WS2007HttpBindingElement_t1255517706_0_0_0_Types };
static const Il2CppType* GenInst_WS2007HttpBindingElement_t1255517706_0_0_0_Types[] = { &WS2007HttpBindingElement_t1255517706_0_0_0 };
extern const Il2CppGenericInst GenInst_WS2007HttpBindingElement_t1255517706_0_0_0 = { 1, GenInst_WS2007HttpBindingElement_t1255517706_0_0_0_Types };
extern const Il2CppType WSDualHttpBinding_t3779174110_0_0_0;
extern const Il2CppType WSDualHttpBindingElement_t2001662549_0_0_0;
static const Il2CppType* GenInst_WSDualHttpBinding_t3779174110_0_0_0_WSDualHttpBindingElement_t2001662549_0_0_0_Types[] = { &WSDualHttpBinding_t3779174110_0_0_0, &WSDualHttpBindingElement_t2001662549_0_0_0 };
extern const Il2CppGenericInst GenInst_WSDualHttpBinding_t3779174110_0_0_0_WSDualHttpBindingElement_t2001662549_0_0_0 = { 2, GenInst_WSDualHttpBinding_t3779174110_0_0_0_WSDualHttpBindingElement_t2001662549_0_0_0_Types };
static const Il2CppType* GenInst_WSDualHttpBindingElement_t2001662549_0_0_0_Types[] = { &WSDualHttpBindingElement_t2001662549_0_0_0 };
extern const Il2CppGenericInst GenInst_WSDualHttpBindingElement_t2001662549_0_0_0 = { 1, GenInst_WSDualHttpBindingElement_t2001662549_0_0_0_Types };
extern const Il2CppType WSFederationHttpBinding_t1523044440_0_0_0;
extern const Il2CppType WSFederationHttpBindingElement_t154325968_0_0_0;
static const Il2CppType* GenInst_WSFederationHttpBinding_t1523044440_0_0_0_WSFederationHttpBindingElement_t154325968_0_0_0_Types[] = { &WSFederationHttpBinding_t1523044440_0_0_0, &WSFederationHttpBindingElement_t154325968_0_0_0 };
extern const Il2CppGenericInst GenInst_WSFederationHttpBinding_t1523044440_0_0_0_WSFederationHttpBindingElement_t154325968_0_0_0 = { 2, GenInst_WSFederationHttpBinding_t1523044440_0_0_0_WSFederationHttpBindingElement_t154325968_0_0_0_Types };
static const Il2CppType* GenInst_WSFederationHttpBindingElement_t154325968_0_0_0_Types[] = { &WSFederationHttpBindingElement_t154325968_0_0_0 };
extern const Il2CppGenericInst GenInst_WSFederationHttpBindingElement_t154325968_0_0_0 = { 1, GenInst_WSFederationHttpBindingElement_t154325968_0_0_0_Types };
extern const Il2CppType WSHttpBindingElement_t1467430044_0_0_0;
static const Il2CppType* GenInst_WSHttpBinding_t3299795862_0_0_0_WSHttpBindingElement_t1467430044_0_0_0_Types[] = { &WSHttpBinding_t3299795862_0_0_0, &WSHttpBindingElement_t1467430044_0_0_0 };
extern const Il2CppGenericInst GenInst_WSHttpBinding_t3299795862_0_0_0_WSHttpBindingElement_t1467430044_0_0_0 = { 2, GenInst_WSHttpBinding_t3299795862_0_0_0_WSHttpBindingElement_t1467430044_0_0_0_Types };
static const Il2CppType* GenInst_WSHttpBindingElement_t1467430044_0_0_0_Types[] = { &WSHttpBindingElement_t1467430044_0_0_0 };
extern const Il2CppGenericInst GenInst_WSHttpBindingElement_t1467430044_0_0_0 = { 1, GenInst_WSHttpBindingElement_t1467430044_0_0_0_Types };
extern const Il2CppType X509ScopedServiceCertificateElement_t614401471_0_0_0;
static const Il2CppType* GenInst_X509ScopedServiceCertificateElement_t614401471_0_0_0_Types[] = { &X509ScopedServiceCertificateElement_t614401471_0_0_0 };
extern const Il2CppGenericInst GenInst_X509ScopedServiceCertificateElement_t614401471_0_0_0 = { 1, GenInst_X509ScopedServiceCertificateElement_t614401471_0_0_0_Types };
extern const Il2CppType XmlElementElement_t4113496974_0_0_0;
static const Il2CppType* GenInst_XmlElementElement_t4113496974_0_0_0_Types[] = { &XmlElementElement_t4113496974_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlElementElement_t4113496974_0_0_0 = { 1, GenInst_XmlElementElement_t4113496974_0_0_0_Types };
extern const Il2CppType IContractBehavior_t389363567_0_0_0;
static const Il2CppType* GenInst_IContractBehavior_t389363567_0_0_0_Types[] = { &IContractBehavior_t389363567_0_0_0 };
extern const Il2CppGenericInst GenInst_IContractBehavior_t389363567_0_0_0 = { 1, GenInst_IContractBehavior_t389363567_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IContractBehavior_t389363567_0_0_0_Types[] = { &Type_t_0_0_0, &IContractBehavior_t389363567_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IContractBehavior_t389363567_0_0_0 = { 2, GenInst_Type_t_0_0_0_IContractBehavior_t389363567_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IContractBehavior_t389363567_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Type_t_0_0_0, &IContractBehavior_t389363567_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IContractBehavior_t389363567_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Type_t_0_0_0_IContractBehavior_t389363567_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t936415502_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t936415502_0_0_0_Types[] = { &KeyValuePair_2_t936415502_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t936415502_0_0_0 = { 1, GenInst_KeyValuePair_2_t936415502_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IContractBehavior_t389363567_0_0_0_KeyValuePair_2_t936415502_0_0_0_Types[] = { &Type_t_0_0_0, &IContractBehavior_t389363567_0_0_0, &KeyValuePair_2_t936415502_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IContractBehavior_t389363567_0_0_0_KeyValuePair_2_t936415502_0_0_0 = { 3, GenInst_Type_t_0_0_0_IContractBehavior_t389363567_0_0_0_KeyValuePair_2_t936415502_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IContractBehavior_t389363567_0_0_0_Type_t_0_0_0_Types[] = { &Type_t_0_0_0, &IContractBehavior_t389363567_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IContractBehavior_t389363567_0_0_0_Type_t_0_0_0 = { 3, GenInst_Type_t_0_0_0_IContractBehavior_t389363567_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IContractBehavior_t389363567_0_0_0_IContractBehavior_t389363567_0_0_0_Types[] = { &Type_t_0_0_0, &IContractBehavior_t389363567_0_0_0, &IContractBehavior_t389363567_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IContractBehavior_t389363567_0_0_0_IContractBehavior_t389363567_0_0_0 = { 3, GenInst_Type_t_0_0_0_IContractBehavior_t389363567_0_0_0_IContractBehavior_t389363567_0_0_0_Types };
extern const Il2CppType OperationDescription_t1389309725_0_0_0;
static const Il2CppType* GenInst_OperationDescription_t1389309725_0_0_0_Types[] = { &OperationDescription_t1389309725_0_0_0 };
extern const Il2CppGenericInst GenInst_OperationDescription_t1389309725_0_0_0 = { 1, GenInst_OperationDescription_t1389309725_0_0_0_Types };
extern const Il2CppType IOperationBehavior_t2503870007_0_0_0;
static const Il2CppType* GenInst_IOperationBehavior_t2503870007_0_0_0_Types[] = { &IOperationBehavior_t2503870007_0_0_0 };
extern const Il2CppGenericInst GenInst_IOperationBehavior_t2503870007_0_0_0 = { 1, GenInst_IOperationBehavior_t2503870007_0_0_0_Types };
extern const Il2CppType ClientOperation_t2430985984_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_ClientOperation_t2430985984_0_0_0_Types[] = { &String_t_0_0_0, &ClientOperation_t2430985984_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ClientOperation_t2430985984_0_0_0 = { 2, GenInst_String_t_0_0_0_ClientOperation_t2430985984_0_0_0_Types };
static const Il2CppType* GenInst_ClientOperation_t2430985984_0_0_0_Types[] = { &ClientOperation_t2430985984_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientOperation_t2430985984_0_0_0 = { 1, GenInst_ClientOperation_t2430985984_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ClientOperation_t2430985984_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &ClientOperation_t2430985984_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ClientOperation_t2430985984_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_ClientOperation_t2430985984_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t318947154_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t318947154_0_0_0_Types[] = { &KeyValuePair_2_t318947154_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t318947154_0_0_0 = { 1, GenInst_KeyValuePair_2_t318947154_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ClientOperation_t2430985984_0_0_0_KeyValuePair_2_t318947154_0_0_0_Types[] = { &String_t_0_0_0, &ClientOperation_t2430985984_0_0_0, &KeyValuePair_2_t318947154_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ClientOperation_t2430985984_0_0_0_KeyValuePair_2_t318947154_0_0_0 = { 3, GenInst_String_t_0_0_0_ClientOperation_t2430985984_0_0_0_KeyValuePair_2_t318947154_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ClientOperation_t2430985984_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &ClientOperation_t2430985984_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ClientOperation_t2430985984_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_ClientOperation_t2430985984_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ClientOperation_t2430985984_0_0_0_ClientOperation_t2430985984_0_0_0_Types[] = { &String_t_0_0_0, &ClientOperation_t2430985984_0_0_0, &ClientOperation_t2430985984_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ClientOperation_t2430985984_0_0_0_ClientOperation_t2430985984_0_0_0 = { 3, GenInst_String_t_0_0_0_ClientOperation_t2430985984_0_0_0_ClientOperation_t2430985984_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IOperationBehavior_t2503870007_0_0_0_Types[] = { &Type_t_0_0_0, &IOperationBehavior_t2503870007_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IOperationBehavior_t2503870007_0_0_0 = { 2, GenInst_Type_t_0_0_0_IOperationBehavior_t2503870007_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IOperationBehavior_t2503870007_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Type_t_0_0_0, &IOperationBehavior_t2503870007_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IOperationBehavior_t2503870007_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Type_t_0_0_0_IOperationBehavior_t2503870007_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3050921942_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3050921942_0_0_0_Types[] = { &KeyValuePair_2_t3050921942_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3050921942_0_0_0 = { 1, GenInst_KeyValuePair_2_t3050921942_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IOperationBehavior_t2503870007_0_0_0_KeyValuePair_2_t3050921942_0_0_0_Types[] = { &Type_t_0_0_0, &IOperationBehavior_t2503870007_0_0_0, &KeyValuePair_2_t3050921942_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IOperationBehavior_t2503870007_0_0_0_KeyValuePair_2_t3050921942_0_0_0 = { 3, GenInst_Type_t_0_0_0_IOperationBehavior_t2503870007_0_0_0_KeyValuePair_2_t3050921942_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IOperationBehavior_t2503870007_0_0_0_Type_t_0_0_0_Types[] = { &Type_t_0_0_0, &IOperationBehavior_t2503870007_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IOperationBehavior_t2503870007_0_0_0_Type_t_0_0_0 = { 3, GenInst_Type_t_0_0_0_IOperationBehavior_t2503870007_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IOperationBehavior_t2503870007_0_0_0_IOperationBehavior_t2503870007_0_0_0_Types[] = { &Type_t_0_0_0, &IOperationBehavior_t2503870007_0_0_0, &IOperationBehavior_t2503870007_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IOperationBehavior_t2503870007_0_0_0_IOperationBehavior_t2503870007_0_0_0 = { 3, GenInst_Type_t_0_0_0_IOperationBehavior_t2503870007_0_0_0_IOperationBehavior_t2503870007_0_0_0_Types };
extern const Il2CppType MessageDescription_t512795677_0_0_0;
static const Il2CppType* GenInst_MessageDescription_t512795677_0_0_0_Types[] = { &MessageDescription_t512795677_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageDescription_t512795677_0_0_0 = { 1, GenInst_MessageDescription_t512795677_0_0_0_Types };
extern const Il2CppType MessagePartDescription_t2026856693_0_0_0;
static const Il2CppType* GenInst_MessagePartDescription_t2026856693_0_0_0_Types[] = { &MessagePartDescription_t2026856693_0_0_0 };
extern const Il2CppGenericInst GenInst_MessagePartDescription_t2026856693_0_0_0 = { 1, GenInst_MessagePartDescription_t2026856693_0_0_0_Types };
extern const Il2CppType ServiceContractAttribute_t4131169907_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ServiceContractAttribute_t4131169907_0_0_0_Types[] = { &Type_t_0_0_0, &ServiceContractAttribute_t4131169907_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ServiceContractAttribute_t4131169907_0_0_0 = { 2, GenInst_Type_t_0_0_0_ServiceContractAttribute_t4131169907_0_0_0_Types };
static const Il2CppType* GenInst_ServiceContractAttribute_t4131169907_0_0_0_Types[] = { &ServiceContractAttribute_t4131169907_0_0_0 };
extern const Il2CppGenericInst GenInst_ServiceContractAttribute_t4131169907_0_0_0 = { 1, GenInst_ServiceContractAttribute_t4131169907_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ServiceContractAttribute_t4131169907_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Type_t_0_0_0, &ServiceContractAttribute_t4131169907_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ServiceContractAttribute_t4131169907_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Type_t_0_0_0_ServiceContractAttribute_t4131169907_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t383254546_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t383254546_0_0_0_Types[] = { &KeyValuePair_2_t383254546_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t383254546_0_0_0 = { 1, GenInst_KeyValuePair_2_t383254546_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ServiceContractAttribute_t4131169907_0_0_0_KeyValuePair_2_t383254546_0_0_0_Types[] = { &Type_t_0_0_0, &ServiceContractAttribute_t4131169907_0_0_0, &KeyValuePair_2_t383254546_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ServiceContractAttribute_t4131169907_0_0_0_KeyValuePair_2_t383254546_0_0_0 = { 3, GenInst_Type_t_0_0_0_ServiceContractAttribute_t4131169907_0_0_0_KeyValuePair_2_t383254546_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ServiceContractAttribute_t4131169907_0_0_0_Type_t_0_0_0_Types[] = { &Type_t_0_0_0, &ServiceContractAttribute_t4131169907_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ServiceContractAttribute_t4131169907_0_0_0_Type_t_0_0_0 = { 3, GenInst_Type_t_0_0_0_ServiceContractAttribute_t4131169907_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ServiceContractAttribute_t4131169907_0_0_0_ServiceContractAttribute_t4131169907_0_0_0_Types[] = { &Type_t_0_0_0, &ServiceContractAttribute_t4131169907_0_0_0, &ServiceContractAttribute_t4131169907_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ServiceContractAttribute_t4131169907_0_0_0_ServiceContractAttribute_t4131169907_0_0_0 = { 3, GenInst_Type_t_0_0_0_ServiceContractAttribute_t4131169907_0_0_0_ServiceContractAttribute_t4131169907_0_0_0_Types };
extern const Il2CppType FaultDescription_t2068025689_0_0_0;
static const Il2CppType* GenInst_FaultDescription_t2068025689_0_0_0_Types[] = { &FaultDescription_t2068025689_0_0_0 };
extern const Il2CppGenericInst GenInst_FaultDescription_t2068025689_0_0_0 = { 1, GenInst_FaultDescription_t2068025689_0_0_0_Types };
extern const Il2CppType ServiceEndpoint_t4038493094_0_0_0;
static const Il2CppType* GenInst_ServiceEndpoint_t4038493094_0_0_0_Types[] = { &ServiceEndpoint_t4038493094_0_0_0 };
extern const Il2CppGenericInst GenInst_ServiceEndpoint_t4038493094_0_0_0 = { 1, GenInst_ServiceEndpoint_t4038493094_0_0_0_Types };
extern const Il2CppType MessageHeaderDescription_t75735776_0_0_0;
static const Il2CppType* GenInst_XmlQualifiedName_t2760654312_0_0_0_MessageHeaderDescription_t75735776_0_0_0_Types[] = { &XmlQualifiedName_t2760654312_0_0_0, &MessageHeaderDescription_t75735776_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlQualifiedName_t2760654312_0_0_0_MessageHeaderDescription_t75735776_0_0_0 = { 2, GenInst_XmlQualifiedName_t2760654312_0_0_0_MessageHeaderDescription_t75735776_0_0_0_Types };
static const Il2CppType* GenInst_MessageHeaderDescription_t75735776_0_0_0_Types[] = { &MessageHeaderDescription_t75735776_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageHeaderDescription_t75735776_0_0_0 = { 1, GenInst_MessageHeaderDescription_t75735776_0_0_0_Types };
static const Il2CppType* GenInst_XmlQualifiedName_t2760654312_0_0_0_MessageHeaderDescription_t75735776_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &XmlQualifiedName_t2760654312_0_0_0, &MessageHeaderDescription_t75735776_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlQualifiedName_t2760654312_0_0_0_MessageHeaderDescription_t75735776_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_XmlQualifiedName_t2760654312_0_0_0_MessageHeaderDescription_t75735776_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2448405263_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2448405263_0_0_0_Types[] = { &KeyValuePair_2_t2448405263_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2448405263_0_0_0 = { 1, GenInst_KeyValuePair_2_t2448405263_0_0_0_Types };
static const Il2CppType* GenInst_XmlQualifiedName_t2760654312_0_0_0_MessageHeaderDescription_t75735776_0_0_0_KeyValuePair_2_t2448405263_0_0_0_Types[] = { &XmlQualifiedName_t2760654312_0_0_0, &MessageHeaderDescription_t75735776_0_0_0, &KeyValuePair_2_t2448405263_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlQualifiedName_t2760654312_0_0_0_MessageHeaderDescription_t75735776_0_0_0_KeyValuePair_2_t2448405263_0_0_0 = { 3, GenInst_XmlQualifiedName_t2760654312_0_0_0_MessageHeaderDescription_t75735776_0_0_0_KeyValuePair_2_t2448405263_0_0_0_Types };
static const Il2CppType* GenInst_XmlQualifiedName_t2760654312_0_0_0_MessageHeaderDescription_t75735776_0_0_0_XmlQualifiedName_t2760654312_0_0_0_Types[] = { &XmlQualifiedName_t2760654312_0_0_0, &MessageHeaderDescription_t75735776_0_0_0, &XmlQualifiedName_t2760654312_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlQualifiedName_t2760654312_0_0_0_MessageHeaderDescription_t75735776_0_0_0_XmlQualifiedName_t2760654312_0_0_0 = { 3, GenInst_XmlQualifiedName_t2760654312_0_0_0_MessageHeaderDescription_t75735776_0_0_0_XmlQualifiedName_t2760654312_0_0_0_Types };
static const Il2CppType* GenInst_XmlQualifiedName_t2760654312_0_0_0_MessageHeaderDescription_t75735776_0_0_0_MessageHeaderDescription_t75735776_0_0_0_Types[] = { &XmlQualifiedName_t2760654312_0_0_0, &MessageHeaderDescription_t75735776_0_0_0, &MessageHeaderDescription_t75735776_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlQualifiedName_t2760654312_0_0_0_MessageHeaderDescription_t75735776_0_0_0_MessageHeaderDescription_t75735776_0_0_0 = { 3, GenInst_XmlQualifiedName_t2760654312_0_0_0_MessageHeaderDescription_t75735776_0_0_0_MessageHeaderDescription_t75735776_0_0_0_Types };
static const Il2CppType* GenInst_XmlQualifiedName_t2760654312_0_0_0_MessagePartDescription_t2026856693_0_0_0_Types[] = { &XmlQualifiedName_t2760654312_0_0_0, &MessagePartDescription_t2026856693_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlQualifiedName_t2760654312_0_0_0_MessagePartDescription_t2026856693_0_0_0 = { 2, GenInst_XmlQualifiedName_t2760654312_0_0_0_MessagePartDescription_t2026856693_0_0_0_Types };
static const Il2CppType* GenInst_XmlQualifiedName_t2760654312_0_0_0_MessagePartDescription_t2026856693_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &XmlQualifiedName_t2760654312_0_0_0, &MessagePartDescription_t2026856693_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlQualifiedName_t2760654312_0_0_0_MessagePartDescription_t2026856693_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_XmlQualifiedName_t2760654312_0_0_0_MessagePartDescription_t2026856693_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t104558884_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t104558884_0_0_0_Types[] = { &KeyValuePair_2_t104558884_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t104558884_0_0_0 = { 1, GenInst_KeyValuePair_2_t104558884_0_0_0_Types };
static const Il2CppType* GenInst_XmlQualifiedName_t2760654312_0_0_0_MessagePartDescription_t2026856693_0_0_0_KeyValuePair_2_t104558884_0_0_0_Types[] = { &XmlQualifiedName_t2760654312_0_0_0, &MessagePartDescription_t2026856693_0_0_0, &KeyValuePair_2_t104558884_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlQualifiedName_t2760654312_0_0_0_MessagePartDescription_t2026856693_0_0_0_KeyValuePair_2_t104558884_0_0_0 = { 3, GenInst_XmlQualifiedName_t2760654312_0_0_0_MessagePartDescription_t2026856693_0_0_0_KeyValuePair_2_t104558884_0_0_0_Types };
static const Il2CppType* GenInst_XmlQualifiedName_t2760654312_0_0_0_MessagePartDescription_t2026856693_0_0_0_XmlQualifiedName_t2760654312_0_0_0_Types[] = { &XmlQualifiedName_t2760654312_0_0_0, &MessagePartDescription_t2026856693_0_0_0, &XmlQualifiedName_t2760654312_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlQualifiedName_t2760654312_0_0_0_MessagePartDescription_t2026856693_0_0_0_XmlQualifiedName_t2760654312_0_0_0 = { 3, GenInst_XmlQualifiedName_t2760654312_0_0_0_MessagePartDescription_t2026856693_0_0_0_XmlQualifiedName_t2760654312_0_0_0_Types };
static const Il2CppType* GenInst_XmlQualifiedName_t2760654312_0_0_0_MessagePartDescription_t2026856693_0_0_0_MessagePartDescription_t2026856693_0_0_0_Types[] = { &XmlQualifiedName_t2760654312_0_0_0, &MessagePartDescription_t2026856693_0_0_0, &MessagePartDescription_t2026856693_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlQualifiedName_t2760654312_0_0_0_MessagePartDescription_t2026856693_0_0_0_MessagePartDescription_t2026856693_0_0_0 = { 3, GenInst_XmlQualifiedName_t2760654312_0_0_0_MessagePartDescription_t2026856693_0_0_0_MessagePartDescription_t2026856693_0_0_0_Types };
extern const Il2CppType MessagePropertyDescription_t3921441415_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_MessagePropertyDescription_t3921441415_0_0_0_Types[] = { &String_t_0_0_0, &MessagePropertyDescription_t3921441415_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MessagePropertyDescription_t3921441415_0_0_0 = { 2, GenInst_String_t_0_0_0_MessagePropertyDescription_t3921441415_0_0_0_Types };
static const Il2CppType* GenInst_MessagePropertyDescription_t3921441415_0_0_0_Types[] = { &MessagePropertyDescription_t3921441415_0_0_0 };
extern const Il2CppGenericInst GenInst_MessagePropertyDescription_t3921441415_0_0_0 = { 1, GenInst_MessagePropertyDescription_t3921441415_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_MessagePropertyDescription_t3921441415_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &MessagePropertyDescription_t3921441415_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MessagePropertyDescription_t3921441415_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_MessagePropertyDescription_t3921441415_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1809402585_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1809402585_0_0_0_Types[] = { &KeyValuePair_2_t1809402585_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1809402585_0_0_0 = { 1, GenInst_KeyValuePair_2_t1809402585_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_MessagePropertyDescription_t3921441415_0_0_0_KeyValuePair_2_t1809402585_0_0_0_Types[] = { &String_t_0_0_0, &MessagePropertyDescription_t3921441415_0_0_0, &KeyValuePair_2_t1809402585_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MessagePropertyDescription_t3921441415_0_0_0_KeyValuePair_2_t1809402585_0_0_0 = { 3, GenInst_String_t_0_0_0_MessagePropertyDescription_t3921441415_0_0_0_KeyValuePair_2_t1809402585_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_MessagePropertyDescription_t3921441415_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &MessagePropertyDescription_t3921441415_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MessagePropertyDescription_t3921441415_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_MessagePropertyDescription_t3921441415_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_MessagePropertyDescription_t3921441415_0_0_0_MessagePropertyDescription_t3921441415_0_0_0_Types[] = { &String_t_0_0_0, &MessagePropertyDescription_t3921441415_0_0_0, &MessagePropertyDescription_t3921441415_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MessagePropertyDescription_t3921441415_0_0_0_MessagePropertyDescription_t3921441415_0_0_0 = { 3, GenInst_String_t_0_0_0_MessagePropertyDescription_t3921441415_0_0_0_MessagePropertyDescription_t3921441415_0_0_0_Types };
extern const Il2CppType MetadataSection_t4067397855_0_0_0;
static const Il2CppType* GenInst_MetadataSection_t4067397855_0_0_0_Types[] = { &MetadataSection_t4067397855_0_0_0 };
extern const Il2CppGenericInst GenInst_MetadataSection_t4067397855_0_0_0 = { 1, GenInst_MetadataSection_t4067397855_0_0_0_Types };
extern const Il2CppType EndpointDispatcher_t2708702820_0_0_0;
static const Il2CppType* GenInst_EndpointDispatcher_t2708702820_0_0_0_Types[] = { &EndpointDispatcher_t2708702820_0_0_0 };
extern const Il2CppGenericInst GenInst_EndpointDispatcher_t2708702820_0_0_0 = { 1, GenInst_EndpointDispatcher_t2708702820_0_0_0_Types };
extern const Il2CppType ChannelDispatcher_t2781106991_0_0_0;
static const Il2CppType* GenInst_Uri_t100236324_0_0_0_ChannelDispatcher_t2781106991_0_0_0_Types[] = { &Uri_t100236324_0_0_0, &ChannelDispatcher_t2781106991_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t100236324_0_0_0_ChannelDispatcher_t2781106991_0_0_0 = { 2, GenInst_Uri_t100236324_0_0_0_ChannelDispatcher_t2781106991_0_0_0_Types };
static const Il2CppType* GenInst_ChannelDispatcher_t2781106991_0_0_0_Types[] = { &ChannelDispatcher_t2781106991_0_0_0 };
extern const Il2CppGenericInst GenInst_ChannelDispatcher_t2781106991_0_0_0 = { 1, GenInst_ChannelDispatcher_t2781106991_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t100236324_0_0_0_ChannelDispatcher_t2781106991_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Uri_t100236324_0_0_0, &ChannelDispatcher_t2781106991_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t100236324_0_0_0_ChannelDispatcher_t2781106991_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Uri_t100236324_0_0_0_ChannelDispatcher_t2781106991_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t60725426_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t60725426_0_0_0_Types[] = { &KeyValuePair_2_t60725426_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t60725426_0_0_0 = { 1, GenInst_KeyValuePair_2_t60725426_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t100236324_0_0_0_ChannelDispatcher_t2781106991_0_0_0_KeyValuePair_2_t60725426_0_0_0_Types[] = { &Uri_t100236324_0_0_0, &ChannelDispatcher_t2781106991_0_0_0, &KeyValuePair_2_t60725426_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t100236324_0_0_0_ChannelDispatcher_t2781106991_0_0_0_KeyValuePair_2_t60725426_0_0_0 = { 3, GenInst_Uri_t100236324_0_0_0_ChannelDispatcher_t2781106991_0_0_0_KeyValuePair_2_t60725426_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t100236324_0_0_0_ChannelDispatcher_t2781106991_0_0_0_Uri_t100236324_0_0_0_Types[] = { &Uri_t100236324_0_0_0, &ChannelDispatcher_t2781106991_0_0_0, &Uri_t100236324_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t100236324_0_0_0_ChannelDispatcher_t2781106991_0_0_0_Uri_t100236324_0_0_0 = { 3, GenInst_Uri_t100236324_0_0_0_ChannelDispatcher_t2781106991_0_0_0_Uri_t100236324_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t100236324_0_0_0_ChannelDispatcher_t2781106991_0_0_0_ChannelDispatcher_t2781106991_0_0_0_Types[] = { &Uri_t100236324_0_0_0, &ChannelDispatcher_t2781106991_0_0_0, &ChannelDispatcher_t2781106991_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t100236324_0_0_0_ChannelDispatcher_t2781106991_0_0_0_ChannelDispatcher_t2781106991_0_0_0 = { 3, GenInst_Uri_t100236324_0_0_0_ChannelDispatcher_t2781106991_0_0_0_ChannelDispatcher_t2781106991_0_0_0_Types };
extern const Il2CppType ServiceDescription_t3693704363_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_ServiceDescription_t3693704363_0_0_0_Types[] = { &String_t_0_0_0, &ServiceDescription_t3693704363_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ServiceDescription_t3693704363_0_0_0 = { 2, GenInst_String_t_0_0_0_ServiceDescription_t3693704363_0_0_0_Types };
static const Il2CppType* GenInst_ServiceDescription_t3693704363_0_0_0_Types[] = { &ServiceDescription_t3693704363_0_0_0 };
extern const Il2CppGenericInst GenInst_ServiceDescription_t3693704363_0_0_0 = { 1, GenInst_ServiceDescription_t3693704363_0_0_0_Types };
extern const Il2CppType NamedItem_t2466402210_0_0_0;
static const Il2CppType* GenInst_NamedItem_t2466402210_0_0_0_Types[] = { &NamedItem_t2466402210_0_0_0 };
extern const Il2CppGenericInst GenInst_NamedItem_t2466402210_0_0_0 = { 1, GenInst_NamedItem_t2466402210_0_0_0_Types };
extern const Il2CppType DocumentableItem_t546804031_0_0_0;
static const Il2CppType* GenInst_DocumentableItem_t546804031_0_0_0_Types[] = { &DocumentableItem_t546804031_0_0_0 };
extern const Il2CppGenericInst GenInst_DocumentableItem_t546804031_0_0_0 = { 1, GenInst_DocumentableItem_t546804031_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ServiceDescription_t3693704363_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &ServiceDescription_t3693704363_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ServiceDescription_t3693704363_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_ServiceDescription_t3693704363_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1581665533_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1581665533_0_0_0_Types[] = { &KeyValuePair_2_t1581665533_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1581665533_0_0_0 = { 1, GenInst_KeyValuePair_2_t1581665533_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ServiceDescription_t3693704363_0_0_0_KeyValuePair_2_t1581665533_0_0_0_Types[] = { &String_t_0_0_0, &ServiceDescription_t3693704363_0_0_0, &KeyValuePair_2_t1581665533_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ServiceDescription_t3693704363_0_0_0_KeyValuePair_2_t1581665533_0_0_0 = { 3, GenInst_String_t_0_0_0_ServiceDescription_t3693704363_0_0_0_KeyValuePair_2_t1581665533_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ServiceDescription_t3693704363_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &ServiceDescription_t3693704363_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ServiceDescription_t3693704363_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_ServiceDescription_t3693704363_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ServiceDescription_t3693704363_0_0_0_ServiceDescription_t3693704363_0_0_0_Types[] = { &String_t_0_0_0, &ServiceDescription_t3693704363_0_0_0, &ServiceDescription_t3693704363_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ServiceDescription_t3693704363_0_0_0_ServiceDescription_t3693704363_0_0_0 = { 3, GenInst_String_t_0_0_0_ServiceDescription_t3693704363_0_0_0_ServiceDescription_t3693704363_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_XmlSchema_t3742557897_0_0_0_Types[] = { &String_t_0_0_0, &XmlSchema_t3742557897_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XmlSchema_t3742557897_0_0_0 = { 2, GenInst_String_t_0_0_0_XmlSchema_t3742557897_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_XmlSchema_t3742557897_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &XmlSchema_t3742557897_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XmlSchema_t3742557897_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_XmlSchema_t3742557897_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1630519067_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1630519067_0_0_0_Types[] = { &KeyValuePair_2_t1630519067_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1630519067_0_0_0 = { 1, GenInst_KeyValuePair_2_t1630519067_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_XmlSchema_t3742557897_0_0_0_KeyValuePair_2_t1630519067_0_0_0_Types[] = { &String_t_0_0_0, &XmlSchema_t3742557897_0_0_0, &KeyValuePair_2_t1630519067_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XmlSchema_t3742557897_0_0_0_KeyValuePair_2_t1630519067_0_0_0 = { 3, GenInst_String_t_0_0_0_XmlSchema_t3742557897_0_0_0_KeyValuePair_2_t1630519067_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_XmlSchema_t3742557897_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &XmlSchema_t3742557897_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XmlSchema_t3742557897_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_XmlSchema_t3742557897_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_XmlSchema_t3742557897_0_0_0_XmlSchema_t3742557897_0_0_0_Types[] = { &String_t_0_0_0, &XmlSchema_t3742557897_0_0_0, &XmlSchema_t3742557897_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XmlSchema_t3742557897_0_0_0_XmlSchema_t3742557897_0_0_0 = { 3, GenInst_String_t_0_0_0_XmlSchema_t3742557897_0_0_0_XmlSchema_t3742557897_0_0_0_Types };
extern const Il2CppType IWSTrustSecurityTokenService_t90517692_0_0_0;
static const Il2CppType* GenInst_IWSTrustSecurityTokenService_t90517692_0_0_0_Types[] = { &IWSTrustSecurityTokenService_t90517692_0_0_0 };
extern const Il2CppGenericInst GenInst_IWSTrustSecurityTokenService_t90517692_0_0_0 = { 1, GenInst_IWSTrustSecurityTokenService_t90517692_0_0_0_Types };
extern const Il2CppType WstRequestSecurityTokenResponse_t889264267_0_0_0;
static const Il2CppType* GenInst_WstRequestSecurityTokenResponse_t889264267_0_0_0_Types[] = { &WstRequestSecurityTokenResponse_t889264267_0_0_0 };
extern const Il2CppGenericInst GenInst_WstRequestSecurityTokenResponse_t889264267_0_0_0 = { 1, GenInst_WstRequestSecurityTokenResponse_t889264267_0_0_0_Types };
extern const Il2CppType WstRequestSecurityTokenBase_t1978373106_0_0_0;
static const Il2CppType* GenInst_WstRequestSecurityTokenBase_t1978373106_0_0_0_Types[] = { &WstRequestSecurityTokenBase_t1978373106_0_0_0 };
extern const Il2CppGenericInst GenInst_WstRequestSecurityTokenBase_t1978373106_0_0_0 = { 1, GenInst_WstRequestSecurityTokenBase_t1978373106_0_0_0_Types };
extern const Il2CppType BodyWriter_t3441673553_0_0_0;
static const Il2CppType* GenInst_BodyWriter_t3441673553_0_0_0_Types[] = { &BodyWriter_t3441673553_0_0_0 };
extern const Il2CppGenericInst GenInst_BodyWriter_t3441673553_0_0_0 = { 1, GenInst_BodyWriter_t3441673553_0_0_0_Types };
extern const Il2CppType MessageBodyDescription_t2541387169_0_0_0;
static const Il2CppType* GenInst_MessageBodyDescription_t2541387169_0_0_0_XmlSerializer_t1117804635_0_0_0_Types[] = { &MessageBodyDescription_t2541387169_0_0_0, &XmlSerializer_t1117804635_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageBodyDescription_t2541387169_0_0_0_XmlSerializer_t1117804635_0_0_0 = { 2, GenInst_MessageBodyDescription_t2541387169_0_0_0_XmlSerializer_t1117804635_0_0_0_Types };
static const Il2CppType* GenInst_MessageBodyDescription_t2541387169_0_0_0_Types[] = { &MessageBodyDescription_t2541387169_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageBodyDescription_t2541387169_0_0_0 = { 1, GenInst_MessageBodyDescription_t2541387169_0_0_0_Types };
static const Il2CppType* GenInst_MessageBodyDescription_t2541387169_0_0_0_XmlSerializer_t1117804635_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &MessageBodyDescription_t2541387169_0_0_0, &XmlSerializer_t1117804635_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageBodyDescription_t2541387169_0_0_0_XmlSerializer_t1117804635_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_MessageBodyDescription_t2541387169_0_0_0_XmlSerializer_t1117804635_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3705273805_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3705273805_0_0_0_Types[] = { &KeyValuePair_2_t3705273805_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3705273805_0_0_0 = { 1, GenInst_KeyValuePair_2_t3705273805_0_0_0_Types };
static const Il2CppType* GenInst_MessageBodyDescription_t2541387169_0_0_0_XmlSerializer_t1117804635_0_0_0_KeyValuePair_2_t3705273805_0_0_0_Types[] = { &MessageBodyDescription_t2541387169_0_0_0, &XmlSerializer_t1117804635_0_0_0, &KeyValuePair_2_t3705273805_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageBodyDescription_t2541387169_0_0_0_XmlSerializer_t1117804635_0_0_0_KeyValuePair_2_t3705273805_0_0_0 = { 3, GenInst_MessageBodyDescription_t2541387169_0_0_0_XmlSerializer_t1117804635_0_0_0_KeyValuePair_2_t3705273805_0_0_0_Types };
static const Il2CppType* GenInst_MessageBodyDescription_t2541387169_0_0_0_XmlSerializer_t1117804635_0_0_0_MessageBodyDescription_t2541387169_0_0_0_Types[] = { &MessageBodyDescription_t2541387169_0_0_0, &XmlSerializer_t1117804635_0_0_0, &MessageBodyDescription_t2541387169_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageBodyDescription_t2541387169_0_0_0_XmlSerializer_t1117804635_0_0_0_MessageBodyDescription_t2541387169_0_0_0 = { 3, GenInst_MessageBodyDescription_t2541387169_0_0_0_XmlSerializer_t1117804635_0_0_0_MessageBodyDescription_t2541387169_0_0_0_Types };
static const Il2CppType* GenInst_MessageBodyDescription_t2541387169_0_0_0_XmlSerializer_t1117804635_0_0_0_XmlSerializer_t1117804635_0_0_0_Types[] = { &MessageBodyDescription_t2541387169_0_0_0, &XmlSerializer_t1117804635_0_0_0, &XmlSerializer_t1117804635_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageBodyDescription_t2541387169_0_0_0_XmlSerializer_t1117804635_0_0_0_XmlSerializer_t1117804635_0_0_0 = { 3, GenInst_MessageBodyDescription_t2541387169_0_0_0_XmlSerializer_t1117804635_0_0_0_XmlSerializer_t1117804635_0_0_0_Types };
extern const Il2CppType XmlMembersMapping_t4154751912_0_0_0;
static const Il2CppType* GenInst_XmlMembersMapping_t4154751912_0_0_0_Types[] = { &XmlMembersMapping_t4154751912_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlMembersMapping_t4154751912_0_0_0 = { 1, GenInst_XmlMembersMapping_t4154751912_0_0_0_Types };
static const Il2CppType* GenInst_MessagePartDescription_t2026856693_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_Types[] = { &MessagePartDescription_t2026856693_0_0_0, &XmlObjectSerializer_t3967301761_0_0_0 };
extern const Il2CppGenericInst GenInst_MessagePartDescription_t2026856693_0_0_0_XmlObjectSerializer_t3967301761_0_0_0 = { 2, GenInst_MessagePartDescription_t2026856693_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_Types };
static const Il2CppType* GenInst_MessagePartDescription_t2026856693_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &MessagePartDescription_t2026856693_0_0_0, &XmlObjectSerializer_t3967301761_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_MessagePartDescription_t2026856693_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_MessagePartDescription_t2026856693_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t842235599_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t842235599_0_0_0_Types[] = { &KeyValuePair_2_t842235599_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t842235599_0_0_0 = { 1, GenInst_KeyValuePair_2_t842235599_0_0_0_Types };
static const Il2CppType* GenInst_MessagePartDescription_t2026856693_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_KeyValuePair_2_t842235599_0_0_0_Types[] = { &MessagePartDescription_t2026856693_0_0_0, &XmlObjectSerializer_t3967301761_0_0_0, &KeyValuePair_2_t842235599_0_0_0 };
extern const Il2CppGenericInst GenInst_MessagePartDescription_t2026856693_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_KeyValuePair_2_t842235599_0_0_0 = { 3, GenInst_MessagePartDescription_t2026856693_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_KeyValuePair_2_t842235599_0_0_0_Types };
static const Il2CppType* GenInst_MessagePartDescription_t2026856693_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_MessagePartDescription_t2026856693_0_0_0_Types[] = { &MessagePartDescription_t2026856693_0_0_0, &XmlObjectSerializer_t3967301761_0_0_0, &MessagePartDescription_t2026856693_0_0_0 };
extern const Il2CppGenericInst GenInst_MessagePartDescription_t2026856693_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_MessagePartDescription_t2026856693_0_0_0 = { 3, GenInst_MessagePartDescription_t2026856693_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_MessagePartDescription_t2026856693_0_0_0_Types };
static const Il2CppType* GenInst_MessagePartDescription_t2026856693_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_Types[] = { &MessagePartDescription_t2026856693_0_0_0, &XmlObjectSerializer_t3967301761_0_0_0, &XmlObjectSerializer_t3967301761_0_0_0 };
extern const Il2CppGenericInst GenInst_MessagePartDescription_t2026856693_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_XmlObjectSerializer_t3967301761_0_0_0 = { 3, GenInst_MessagePartDescription_t2026856693_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_Types };
extern const Il2CppType IErrorHandler_t3951425786_0_0_0;
static const Il2CppType* GenInst_IErrorHandler_t3951425786_0_0_0_Types[] = { &IErrorHandler_t3951425786_0_0_0 };
extern const Il2CppGenericInst GenInst_IErrorHandler_t3951425786_0_0_0 = { 1, GenInst_IErrorHandler_t3951425786_0_0_0_Types };
extern const Il2CppType IChannelInitializer_t2714610642_0_0_0;
static const Il2CppType* GenInst_IChannelInitializer_t2714610642_0_0_0_Types[] = { &IChannelInitializer_t2714610642_0_0_0 };
extern const Il2CppGenericInst GenInst_IChannelInitializer_t2714610642_0_0_0 = { 1, GenInst_IChannelInitializer_t2714610642_0_0_0_Types };
extern const Il2CppType IReplySessionChannel_t1412553907_0_0_0;
static const Il2CppType* GenInst_IReplySessionChannel_t1412553907_0_0_0_Types[] = { &IReplySessionChannel_t1412553907_0_0_0 };
extern const Il2CppGenericInst GenInst_IReplySessionChannel_t1412553907_0_0_0 = { 1, GenInst_IReplySessionChannel_t1412553907_0_0_0_Types };
extern const Il2CppType IInputSessionChannel_t1608238312_0_0_0;
static const Il2CppType* GenInst_IInputSessionChannel_t1608238312_0_0_0_Types[] = { &IInputSessionChannel_t1608238312_0_0_0 };
extern const Il2CppGenericInst GenInst_IInputSessionChannel_t1608238312_0_0_0 = { 1, GenInst_IInputSessionChannel_t1608238312_0_0_0_Types };
extern const Il2CppType IParameterInspector_t717813771_0_0_0;
static const Il2CppType* GenInst_IParameterInspector_t717813771_0_0_0_Types[] = { &IParameterInspector_t717813771_0_0_0 };
extern const Il2CppGenericInst GenInst_IParameterInspector_t717813771_0_0_0 = { 1, GenInst_IParameterInspector_t717813771_0_0_0_Types };
extern const Il2CppType IInteractiveChannelInitializer_t2558021170_0_0_0;
static const Il2CppType* GenInst_IInteractiveChannelInitializer_t2558021170_0_0_0_Types[] = { &IInteractiveChannelInitializer_t2558021170_0_0_0 };
extern const Il2CppGenericInst GenInst_IInteractiveChannelInitializer_t2558021170_0_0_0 = { 1, GenInst_IInteractiveChannelInitializer_t2558021170_0_0_0_Types };
extern const Il2CppType IClientMessageInspector_t2654722838_0_0_0;
static const Il2CppType* GenInst_IClientMessageInspector_t2654722838_0_0_0_Types[] = { &IClientMessageInspector_t2654722838_0_0_0 };
extern const Il2CppGenericInst GenInst_IClientMessageInspector_t2654722838_0_0_0 = { 1, GenInst_IClientMessageInspector_t2654722838_0_0_0_Types };
extern const Il2CppType ICallContextInitializer_t2439543260_0_0_0;
static const Il2CppType* GenInst_ICallContextInitializer_t2439543260_0_0_0_Types[] = { &ICallContextInitializer_t2439543260_0_0_0 };
extern const Il2CppGenericInst GenInst_ICallContextInitializer_t2439543260_0_0_0 = { 1, GenInst_ICallContextInitializer_t2439543260_0_0_0_Types };
extern const Il2CppType DispatchOperation_t1810306617_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_DispatchOperation_t1810306617_0_0_0_Types[] = { &String_t_0_0_0, &DispatchOperation_t1810306617_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_DispatchOperation_t1810306617_0_0_0 = { 2, GenInst_String_t_0_0_0_DispatchOperation_t1810306617_0_0_0_Types };
static const Il2CppType* GenInst_DispatchOperation_t1810306617_0_0_0_Types[] = { &DispatchOperation_t1810306617_0_0_0 };
extern const Il2CppGenericInst GenInst_DispatchOperation_t1810306617_0_0_0 = { 1, GenInst_DispatchOperation_t1810306617_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_DispatchOperation_t1810306617_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &DispatchOperation_t1810306617_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_DispatchOperation_t1810306617_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_DispatchOperation_t1810306617_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3993235083_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3993235083_0_0_0_Types[] = { &KeyValuePair_2_t3993235083_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3993235083_0_0_0 = { 1, GenInst_KeyValuePair_2_t3993235083_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_DispatchOperation_t1810306617_0_0_0_KeyValuePair_2_t3993235083_0_0_0_Types[] = { &String_t_0_0_0, &DispatchOperation_t1810306617_0_0_0, &KeyValuePair_2_t3993235083_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_DispatchOperation_t1810306617_0_0_0_KeyValuePair_2_t3993235083_0_0_0 = { 3, GenInst_String_t_0_0_0_DispatchOperation_t1810306617_0_0_0_KeyValuePair_2_t3993235083_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_DispatchOperation_t1810306617_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &DispatchOperation_t1810306617_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_DispatchOperation_t1810306617_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_DispatchOperation_t1810306617_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_DispatchOperation_t1810306617_0_0_0_DispatchOperation_t1810306617_0_0_0_Types[] = { &String_t_0_0_0, &DispatchOperation_t1810306617_0_0_0, &DispatchOperation_t1810306617_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_DispatchOperation_t1810306617_0_0_0_DispatchOperation_t1810306617_0_0_0 = { 3, GenInst_String_t_0_0_0_DispatchOperation_t1810306617_0_0_0_DispatchOperation_t1810306617_0_0_0_Types };
extern const Il2CppType IInputSessionShutdown_t2537225736_0_0_0;
static const Il2CppType* GenInst_IInputSessionShutdown_t2537225736_0_0_0_Types[] = { &IInputSessionShutdown_t2537225736_0_0_0 };
extern const Il2CppGenericInst GenInst_IInputSessionShutdown_t2537225736_0_0_0 = { 1, GenInst_IInputSessionShutdown_t2537225736_0_0_0_Types };
extern const Il2CppType IInstanceContextInitializer_t1888350186_0_0_0;
static const Il2CppType* GenInst_IInstanceContextInitializer_t1888350186_0_0_0_Types[] = { &IInstanceContextInitializer_t1888350186_0_0_0 };
extern const Il2CppGenericInst GenInst_IInstanceContextInitializer_t1888350186_0_0_0 = { 1, GenInst_IInstanceContextInitializer_t1888350186_0_0_0_Types };
extern const Il2CppType IDispatchMessageInspector_t255215605_0_0_0;
static const Il2CppType* GenInst_IDispatchMessageInspector_t255215605_0_0_0_Types[] = { &IDispatchMessageInspector_t255215605_0_0_0 };
extern const Il2CppGenericInst GenInst_IDispatchMessageInspector_t255215605_0_0_0 = { 1, GenInst_IDispatchMessageInspector_t255215605_0_0_0_Types };
extern const Il2CppType UniqueId_t1383576913_0_0_0;
extern const Il2CppType InstanceContext_t3593205954_0_0_0;
static const Il2CppType* GenInst_UniqueId_t1383576913_0_0_0_InstanceContext_t3593205954_0_0_0_Types[] = { &UniqueId_t1383576913_0_0_0, &InstanceContext_t3593205954_0_0_0 };
extern const Il2CppGenericInst GenInst_UniqueId_t1383576913_0_0_0_InstanceContext_t3593205954_0_0_0 = { 2, GenInst_UniqueId_t1383576913_0_0_0_InstanceContext_t3593205954_0_0_0_Types };
static const Il2CppType* GenInst_UniqueId_t1383576913_0_0_0_Types[] = { &UniqueId_t1383576913_0_0_0 };
extern const Il2CppGenericInst GenInst_UniqueId_t1383576913_0_0_0 = { 1, GenInst_UniqueId_t1383576913_0_0_0_Types };
static const Il2CppType* GenInst_InstanceContext_t3593205954_0_0_0_Types[] = { &InstanceContext_t3593205954_0_0_0 };
extern const Il2CppGenericInst GenInst_InstanceContext_t3593205954_0_0_0 = { 1, GenInst_InstanceContext_t3593205954_0_0_0_Types };
extern const Il2CppType IExtensibleObject_1_t646653426_0_0_0;
static const Il2CppType* GenInst_IExtensibleObject_1_t646653426_0_0_0_Types[] = { &IExtensibleObject_1_t646653426_0_0_0 };
extern const Il2CppGenericInst GenInst_IExtensibleObject_1_t646653426_0_0_0 = { 1, GenInst_IExtensibleObject_1_t646653426_0_0_0_Types };
static const Il2CppType* GenInst_UniqueId_t1383576913_0_0_0_InstanceContext_t3593205954_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &UniqueId_t1383576913_0_0_0, &InstanceContext_t3593205954_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_UniqueId_t1383576913_0_0_0_InstanceContext_t3593205954_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_UniqueId_t1383576913_0_0_0_InstanceContext_t3593205954_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1176609220_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1176609220_0_0_0_Types[] = { &KeyValuePair_2_t1176609220_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1176609220_0_0_0 = { 1, GenInst_KeyValuePair_2_t1176609220_0_0_0_Types };
static const Il2CppType* GenInst_UniqueId_t1383576913_0_0_0_InstanceContext_t3593205954_0_0_0_KeyValuePair_2_t1176609220_0_0_0_Types[] = { &UniqueId_t1383576913_0_0_0, &InstanceContext_t3593205954_0_0_0, &KeyValuePair_2_t1176609220_0_0_0 };
extern const Il2CppGenericInst GenInst_UniqueId_t1383576913_0_0_0_InstanceContext_t3593205954_0_0_0_KeyValuePair_2_t1176609220_0_0_0 = { 3, GenInst_UniqueId_t1383576913_0_0_0_InstanceContext_t3593205954_0_0_0_KeyValuePair_2_t1176609220_0_0_0_Types };
static const Il2CppType* GenInst_UniqueId_t1383576913_0_0_0_InstanceContext_t3593205954_0_0_0_UniqueId_t1383576913_0_0_0_Types[] = { &UniqueId_t1383576913_0_0_0, &InstanceContext_t3593205954_0_0_0, &UniqueId_t1383576913_0_0_0 };
extern const Il2CppGenericInst GenInst_UniqueId_t1383576913_0_0_0_InstanceContext_t3593205954_0_0_0_UniqueId_t1383576913_0_0_0 = { 3, GenInst_UniqueId_t1383576913_0_0_0_InstanceContext_t3593205954_0_0_0_UniqueId_t1383576913_0_0_0_Types };
static const Il2CppType* GenInst_UniqueId_t1383576913_0_0_0_InstanceContext_t3593205954_0_0_0_InstanceContext_t3593205954_0_0_0_Types[] = { &UniqueId_t1383576913_0_0_0, &InstanceContext_t3593205954_0_0_0, &InstanceContext_t3593205954_0_0_0 };
extern const Il2CppGenericInst GenInst_UniqueId_t1383576913_0_0_0_InstanceContext_t3593205954_0_0_0_InstanceContext_t3593205954_0_0_0 = { 3, GenInst_UniqueId_t1383576913_0_0_0_InstanceContext_t3593205954_0_0_0_InstanceContext_t3593205954_0_0_0_Types };
extern const Il2CppType ClaimTypeRequirement_t1730123968_0_0_0;
static const Il2CppType* GenInst_ClaimTypeRequirement_t1730123968_0_0_0_Types[] = { &ClaimTypeRequirement_t1730123968_0_0_0 };
extern const Il2CppGenericInst GenInst_ClaimTypeRequirement_t1730123968_0_0_0 = { 1, GenInst_ClaimTypeRequirement_t1730123968_0_0_0_Types };
extern const Il2CppType Dictionary_2_t3105478127_0_0_0;
static const Il2CppType* GenInst_UniqueId_t1383576913_0_0_0_Dictionary_2_t3105478127_0_0_0_Types[] = { &UniqueId_t1383576913_0_0_0, &Dictionary_2_t3105478127_0_0_0 };
extern const Il2CppGenericInst GenInst_UniqueId_t1383576913_0_0_0_Dictionary_2_t3105478127_0_0_0 = { 2, GenInst_UniqueId_t1383576913_0_0_0_Dictionary_2_t3105478127_0_0_0_Types };
extern const Il2CppType SecurityContextSecurityToken_t3624779732_0_0_0;
static const Il2CppType* GenInst_UniqueId_t1383576913_0_0_0_SecurityContextSecurityToken_t3624779732_0_0_0_Types[] = { &UniqueId_t1383576913_0_0_0, &SecurityContextSecurityToken_t3624779732_0_0_0 };
extern const Il2CppGenericInst GenInst_UniqueId_t1383576913_0_0_0_SecurityContextSecurityToken_t3624779732_0_0_0 = { 2, GenInst_UniqueId_t1383576913_0_0_0_SecurityContextSecurityToken_t3624779732_0_0_0_Types };
static const Il2CppType* GenInst_SecurityContextSecurityToken_t3624779732_0_0_0_Types[] = { &SecurityContextSecurityToken_t3624779732_0_0_0 };
extern const Il2CppGenericInst GenInst_SecurityContextSecurityToken_t3624779732_0_0_0 = { 1, GenInst_SecurityContextSecurityToken_t3624779732_0_0_0_Types };
static const Il2CppType* GenInst_UniqueId_t1383576913_0_0_0_SecurityContextSecurityToken_t3624779732_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &UniqueId_t1383576913_0_0_0, &SecurityContextSecurityToken_t3624779732_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_UniqueId_t1383576913_0_0_0_SecurityContextSecurityToken_t3624779732_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_UniqueId_t1383576913_0_0_0_SecurityContextSecurityToken_t3624779732_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1208182998_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1208182998_0_0_0_Types[] = { &KeyValuePair_2_t1208182998_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1208182998_0_0_0 = { 1, GenInst_KeyValuePair_2_t1208182998_0_0_0_Types };
static const Il2CppType* GenInst_UniqueId_t1383576913_0_0_0_SecurityContextSecurityToken_t3624779732_0_0_0_KeyValuePair_2_t1208182998_0_0_0_Types[] = { &UniqueId_t1383576913_0_0_0, &SecurityContextSecurityToken_t3624779732_0_0_0, &KeyValuePair_2_t1208182998_0_0_0 };
extern const Il2CppGenericInst GenInst_UniqueId_t1383576913_0_0_0_SecurityContextSecurityToken_t3624779732_0_0_0_KeyValuePair_2_t1208182998_0_0_0 = { 3, GenInst_UniqueId_t1383576913_0_0_0_SecurityContextSecurityToken_t3624779732_0_0_0_KeyValuePair_2_t1208182998_0_0_0_Types };
static const Il2CppType* GenInst_UniqueId_t1383576913_0_0_0_SecurityContextSecurityToken_t3624779732_0_0_0_UniqueId_t1383576913_0_0_0_Types[] = { &UniqueId_t1383576913_0_0_0, &SecurityContextSecurityToken_t3624779732_0_0_0, &UniqueId_t1383576913_0_0_0 };
extern const Il2CppGenericInst GenInst_UniqueId_t1383576913_0_0_0_SecurityContextSecurityToken_t3624779732_0_0_0_UniqueId_t1383576913_0_0_0 = { 3, GenInst_UniqueId_t1383576913_0_0_0_SecurityContextSecurityToken_t3624779732_0_0_0_UniqueId_t1383576913_0_0_0_Types };
static const Il2CppType* GenInst_UniqueId_t1383576913_0_0_0_SecurityContextSecurityToken_t3624779732_0_0_0_SecurityContextSecurityToken_t3624779732_0_0_0_Types[] = { &UniqueId_t1383576913_0_0_0, &SecurityContextSecurityToken_t3624779732_0_0_0, &SecurityContextSecurityToken_t3624779732_0_0_0 };
extern const Il2CppGenericInst GenInst_UniqueId_t1383576913_0_0_0_SecurityContextSecurityToken_t3624779732_0_0_0_SecurityContextSecurityToken_t3624779732_0_0_0 = { 3, GenInst_UniqueId_t1383576913_0_0_0_SecurityContextSecurityToken_t3624779732_0_0_0_SecurityContextSecurityToken_t3624779732_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t3105478127_0_0_0_Types[] = { &Dictionary_2_t3105478127_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3105478127_0_0_0 = { 1, GenInst_Dictionary_2_t3105478127_0_0_0_Types };
extern const Il2CppType IDictionary_t1363984059_0_0_0;
static const Il2CppType* GenInst_IDictionary_t1363984059_0_0_0_Types[] = { &IDictionary_t1363984059_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_t1363984059_0_0_0 = { 1, GenInst_IDictionary_t1363984059_0_0_0_Types };
static const Il2CppType* GenInst_UniqueId_t1383576913_0_0_0_Dictionary_2_t3105478127_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &UniqueId_t1383576913_0_0_0, &Dictionary_2_t3105478127_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_UniqueId_t1383576913_0_0_0_Dictionary_2_t3105478127_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_UniqueId_t1383576913_0_0_0_Dictionary_2_t3105478127_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t688881393_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t688881393_0_0_0_Types[] = { &KeyValuePair_2_t688881393_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t688881393_0_0_0 = { 1, GenInst_KeyValuePair_2_t688881393_0_0_0_Types };
static const Il2CppType* GenInst_UniqueId_t1383576913_0_0_0_Dictionary_2_t3105478127_0_0_0_KeyValuePair_2_t688881393_0_0_0_Types[] = { &UniqueId_t1383576913_0_0_0, &Dictionary_2_t3105478127_0_0_0, &KeyValuePair_2_t688881393_0_0_0 };
extern const Il2CppGenericInst GenInst_UniqueId_t1383576913_0_0_0_Dictionary_2_t3105478127_0_0_0_KeyValuePair_2_t688881393_0_0_0 = { 3, GenInst_UniqueId_t1383576913_0_0_0_Dictionary_2_t3105478127_0_0_0_KeyValuePair_2_t688881393_0_0_0_Types };
static const Il2CppType* GenInst_UniqueId_t1383576913_0_0_0_Dictionary_2_t3105478127_0_0_0_UniqueId_t1383576913_0_0_0_Types[] = { &UniqueId_t1383576913_0_0_0, &Dictionary_2_t3105478127_0_0_0, &UniqueId_t1383576913_0_0_0 };
extern const Il2CppGenericInst GenInst_UniqueId_t1383576913_0_0_0_Dictionary_2_t3105478127_0_0_0_UniqueId_t1383576913_0_0_0 = { 3, GenInst_UniqueId_t1383576913_0_0_0_Dictionary_2_t3105478127_0_0_0_UniqueId_t1383576913_0_0_0_Types };
static const Il2CppType* GenInst_UniqueId_t1383576913_0_0_0_Dictionary_2_t3105478127_0_0_0_Dictionary_2_t3105478127_0_0_0_Types[] = { &UniqueId_t1383576913_0_0_0, &Dictionary_2_t3105478127_0_0_0, &Dictionary_2_t3105478127_0_0_0 };
extern const Il2CppGenericInst GenInst_UniqueId_t1383576913_0_0_0_Dictionary_2_t3105478127_0_0_0_Dictionary_2_t3105478127_0_0_0 = { 3, GenInst_UniqueId_t1383576913_0_0_0_Dictionary_2_t3105478127_0_0_0_Dictionary_2_t3105478127_0_0_0_Types };
extern const Il2CppType SspiServerSession_t2076952883_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_SspiServerSession_t2076952883_0_0_0_Types[] = { &String_t_0_0_0, &SspiServerSession_t2076952883_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_SspiServerSession_t2076952883_0_0_0 = { 2, GenInst_String_t_0_0_0_SspiServerSession_t2076952883_0_0_0_Types };
static const Il2CppType* GenInst_SspiServerSession_t2076952883_0_0_0_Types[] = { &SspiServerSession_t2076952883_0_0_0 };
extern const Il2CppGenericInst GenInst_SspiServerSession_t2076952883_0_0_0 = { 1, GenInst_SspiServerSession_t2076952883_0_0_0_Types };
extern const Il2CppType SspiSession_t1506236276_0_0_0;
static const Il2CppType* GenInst_SspiSession_t1506236276_0_0_0_Types[] = { &SspiSession_t1506236276_0_0_0 };
extern const Il2CppGenericInst GenInst_SspiSession_t1506236276_0_0_0 = { 1, GenInst_SspiSession_t1506236276_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_SspiServerSession_t2076952883_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &SspiServerSession_t2076952883_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_SspiServerSession_t2076952883_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_SspiServerSession_t2076952883_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4259881349_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4259881349_0_0_0_Types[] = { &KeyValuePair_2_t4259881349_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4259881349_0_0_0 = { 1, GenInst_KeyValuePair_2_t4259881349_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_SspiServerSession_t2076952883_0_0_0_KeyValuePair_2_t4259881349_0_0_0_Types[] = { &String_t_0_0_0, &SspiServerSession_t2076952883_0_0_0, &KeyValuePair_2_t4259881349_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_SspiServerSession_t2076952883_0_0_0_KeyValuePair_2_t4259881349_0_0_0 = { 3, GenInst_String_t_0_0_0_SspiServerSession_t2076952883_0_0_0_KeyValuePair_2_t4259881349_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_SspiServerSession_t2076952883_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &SspiServerSession_t2076952883_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_SspiServerSession_t2076952883_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_SspiServerSession_t2076952883_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_SspiServerSession_t2076952883_0_0_0_SspiServerSession_t2076952883_0_0_0_Types[] = { &String_t_0_0_0, &SspiServerSession_t2076952883_0_0_0, &SspiServerSession_t2076952883_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_SspiServerSession_t2076952883_0_0_0_SspiServerSession_t2076952883_0_0_0 = { 3, GenInst_String_t_0_0_0_SspiServerSession_t2076952883_0_0_0_SspiServerSession_t2076952883_0_0_0_Types };
extern const Il2CppType TlsServerSessionInfo_t613821154_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_TlsServerSessionInfo_t613821154_0_0_0_Types[] = { &String_t_0_0_0, &TlsServerSessionInfo_t613821154_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_TlsServerSessionInfo_t613821154_0_0_0 = { 2, GenInst_String_t_0_0_0_TlsServerSessionInfo_t613821154_0_0_0_Types };
static const Il2CppType* GenInst_TlsServerSessionInfo_t613821154_0_0_0_Types[] = { &TlsServerSessionInfo_t613821154_0_0_0 };
extern const Il2CppGenericInst GenInst_TlsServerSessionInfo_t613821154_0_0_0 = { 1, GenInst_TlsServerSessionInfo_t613821154_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_TlsServerSessionInfo_t613821154_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &TlsServerSessionInfo_t613821154_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_TlsServerSessionInfo_t613821154_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_TlsServerSessionInfo_t613821154_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2796749620_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2796749620_0_0_0_Types[] = { &KeyValuePair_2_t2796749620_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2796749620_0_0_0 = { 1, GenInst_KeyValuePair_2_t2796749620_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_TlsServerSessionInfo_t613821154_0_0_0_KeyValuePair_2_t2796749620_0_0_0_Types[] = { &String_t_0_0_0, &TlsServerSessionInfo_t613821154_0_0_0, &KeyValuePair_2_t2796749620_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_TlsServerSessionInfo_t613821154_0_0_0_KeyValuePair_2_t2796749620_0_0_0 = { 3, GenInst_String_t_0_0_0_TlsServerSessionInfo_t613821154_0_0_0_KeyValuePair_2_t2796749620_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_TlsServerSessionInfo_t613821154_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &TlsServerSessionInfo_t613821154_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_TlsServerSessionInfo_t613821154_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_TlsServerSessionInfo_t613821154_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_TlsServerSessionInfo_t613821154_0_0_0_TlsServerSessionInfo_t613821154_0_0_0_Types[] = { &String_t_0_0_0, &TlsServerSessionInfo_t613821154_0_0_0, &TlsServerSessionInfo_t613821154_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_TlsServerSessionInfo_t613821154_0_0_0_TlsServerSessionInfo_t613821154_0_0_0 = { 3, GenInst_String_t_0_0_0_TlsServerSessionInfo_t613821154_0_0_0_TlsServerSessionInfo_t613821154_0_0_0_Types };
extern const Il2CppType HandshakeMessage_t3696583169_0_0_0;
static const Il2CppType* GenInst_HandshakeMessage_t3696583169_0_0_0_Types[] = { &HandshakeMessage_t3696583169_0_0_0 };
extern const Il2CppGenericInst GenInst_HandshakeMessage_t3696583169_0_0_0 = { 1, GenInst_HandshakeMessage_t3696583169_0_0_0_Types };
extern const Il2CppType TlsStream_t2365453966_0_0_0;
static const Il2CppType* GenInst_TlsStream_t2365453966_0_0_0_Types[] = { &TlsStream_t2365453966_0_0_0 };
extern const Il2CppGenericInst GenInst_TlsStream_t2365453966_0_0_0 = { 1, GenInst_TlsStream_t2365453966_0_0_0_Types };
extern const Il2CppType Stream_t1273022909_0_0_0;
static const Il2CppType* GenInst_Stream_t1273022909_0_0_0_Types[] = { &Stream_t1273022909_0_0_0 };
extern const Il2CppGenericInst GenInst_Stream_t1273022909_0_0_0 = { 1, GenInst_Stream_t1273022909_0_0_0_Types };
extern const Il2CppType KeyedByTypeCollection_1_t4222441378_0_0_0;
static const Il2CppType* GenInst_Uri_t100236324_0_0_0_KeyedByTypeCollection_1_t4222441378_0_0_0_Types[] = { &Uri_t100236324_0_0_0, &KeyedByTypeCollection_1_t4222441378_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t100236324_0_0_0_KeyedByTypeCollection_1_t4222441378_0_0_0 = { 2, GenInst_Uri_t100236324_0_0_0_KeyedByTypeCollection_1_t4222441378_0_0_0_Types };
static const Il2CppType* GenInst_KeyedByTypeCollection_1_t4222441378_0_0_0_Types[] = { &KeyedByTypeCollection_1_t4222441378_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyedByTypeCollection_1_t4222441378_0_0_0 = { 1, GenInst_KeyedByTypeCollection_1_t4222441378_0_0_0_Types };
extern const Il2CppType KeyedCollection_2_t3162336987_0_0_0;
static const Il2CppType* GenInst_KeyedCollection_2_t3162336987_0_0_0_Types[] = { &KeyedCollection_2_t3162336987_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyedCollection_2_t3162336987_0_0_0 = { 1, GenInst_KeyedCollection_2_t3162336987_0_0_0_Types };
extern const Il2CppType Collection_1_t522918537_0_0_0;
static const Il2CppType* GenInst_Collection_1_t522918537_0_0_0_Types[] = { &Collection_1_t522918537_0_0_0 };
extern const Il2CppGenericInst GenInst_Collection_1_t522918537_0_0_0 = { 1, GenInst_Collection_1_t522918537_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t100236324_0_0_0_KeyedByTypeCollection_1_t4222441378_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Uri_t100236324_0_0_0, &KeyedByTypeCollection_1_t4222441378_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t100236324_0_0_0_KeyedByTypeCollection_1_t4222441378_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Uri_t100236324_0_0_0_KeyedByTypeCollection_1_t4222441378_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1502059813_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1502059813_0_0_0_Types[] = { &KeyValuePair_2_t1502059813_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1502059813_0_0_0 = { 1, GenInst_KeyValuePair_2_t1502059813_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t100236324_0_0_0_KeyedByTypeCollection_1_t4222441378_0_0_0_KeyValuePair_2_t1502059813_0_0_0_Types[] = { &Uri_t100236324_0_0_0, &KeyedByTypeCollection_1_t4222441378_0_0_0, &KeyValuePair_2_t1502059813_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t100236324_0_0_0_KeyedByTypeCollection_1_t4222441378_0_0_0_KeyValuePair_2_t1502059813_0_0_0 = { 3, GenInst_Uri_t100236324_0_0_0_KeyedByTypeCollection_1_t4222441378_0_0_0_KeyValuePair_2_t1502059813_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t100236324_0_0_0_KeyedByTypeCollection_1_t4222441378_0_0_0_Uri_t100236324_0_0_0_Types[] = { &Uri_t100236324_0_0_0, &KeyedByTypeCollection_1_t4222441378_0_0_0, &Uri_t100236324_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t100236324_0_0_0_KeyedByTypeCollection_1_t4222441378_0_0_0_Uri_t100236324_0_0_0 = { 3, GenInst_Uri_t100236324_0_0_0_KeyedByTypeCollection_1_t4222441378_0_0_0_Uri_t100236324_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t100236324_0_0_0_KeyedByTypeCollection_1_t4222441378_0_0_0_KeyedByTypeCollection_1_t4222441378_0_0_0_Types[] = { &Uri_t100236324_0_0_0, &KeyedByTypeCollection_1_t4222441378_0_0_0, &KeyedByTypeCollection_1_t4222441378_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t100236324_0_0_0_KeyedByTypeCollection_1_t4222441378_0_0_0_KeyedByTypeCollection_1_t4222441378_0_0_0 = { 3, GenInst_Uri_t100236324_0_0_0_KeyedByTypeCollection_1_t4222441378_0_0_0_KeyedByTypeCollection_1_t4222441378_0_0_0_Types };
extern const Il2CppType X509Certificate2_t714049126_0_0_0;
static const Il2CppType* GenInst_X509Certificate2_t714049126_0_0_0_Types[] = { &X509Certificate2_t714049126_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Certificate2_t714049126_0_0_0 = { 1, GenInst_X509Certificate2_t714049126_0_0_0_Types };
extern const Il2CppType MessagePartSpecification_t2368875147_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_MessagePartSpecification_t2368875147_0_0_0_Types[] = { &String_t_0_0_0, &MessagePartSpecification_t2368875147_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MessagePartSpecification_t2368875147_0_0_0 = { 2, GenInst_String_t_0_0_0_MessagePartSpecification_t2368875147_0_0_0_Types };
static const Il2CppType* GenInst_MessagePartSpecification_t2368875147_0_0_0_Types[] = { &MessagePartSpecification_t2368875147_0_0_0 };
extern const Il2CppGenericInst GenInst_MessagePartSpecification_t2368875147_0_0_0 = { 1, GenInst_MessagePartSpecification_t2368875147_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_MessagePartSpecification_t2368875147_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &MessagePartSpecification_t2368875147_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MessagePartSpecification_t2368875147_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_MessagePartSpecification_t2368875147_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t256836317_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t256836317_0_0_0_Types[] = { &KeyValuePair_2_t256836317_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t256836317_0_0_0 = { 1, GenInst_KeyValuePair_2_t256836317_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_MessagePartSpecification_t2368875147_0_0_0_KeyValuePair_2_t256836317_0_0_0_Types[] = { &String_t_0_0_0, &MessagePartSpecification_t2368875147_0_0_0, &KeyValuePair_2_t256836317_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MessagePartSpecification_t2368875147_0_0_0_KeyValuePair_2_t256836317_0_0_0 = { 3, GenInst_String_t_0_0_0_MessagePartSpecification_t2368875147_0_0_0_KeyValuePair_2_t256836317_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_MessagePartSpecification_t2368875147_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &MessagePartSpecification_t2368875147_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MessagePartSpecification_t2368875147_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_MessagePartSpecification_t2368875147_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_MessagePartSpecification_t2368875147_0_0_0_MessagePartSpecification_t2368875147_0_0_0_Types[] = { &String_t_0_0_0, &MessagePartSpecification_t2368875147_0_0_0, &MessagePartSpecification_t2368875147_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MessagePartSpecification_t2368875147_0_0_0_MessagePartSpecification_t2368875147_0_0_0 = { 3, GenInst_String_t_0_0_0_MessagePartSpecification_t2368875147_0_0_0_MessagePartSpecification_t2368875147_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t100236324_0_0_0_X509Certificate2_t714049126_0_0_0_Types[] = { &Uri_t100236324_0_0_0, &X509Certificate2_t714049126_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t100236324_0_0_0_X509Certificate2_t714049126_0_0_0 = { 2, GenInst_Uri_t100236324_0_0_0_X509Certificate2_t714049126_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t100236324_0_0_0_X509Certificate2_t714049126_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Uri_t100236324_0_0_0, &X509Certificate2_t714049126_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t100236324_0_0_0_X509Certificate2_t714049126_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Uri_t100236324_0_0_0_X509Certificate2_t714049126_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2288634857_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2288634857_0_0_0_Types[] = { &KeyValuePair_2_t2288634857_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2288634857_0_0_0 = { 1, GenInst_KeyValuePair_2_t2288634857_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t100236324_0_0_0_X509Certificate2_t714049126_0_0_0_KeyValuePair_2_t2288634857_0_0_0_Types[] = { &Uri_t100236324_0_0_0, &X509Certificate2_t714049126_0_0_0, &KeyValuePair_2_t2288634857_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t100236324_0_0_0_X509Certificate2_t714049126_0_0_0_KeyValuePair_2_t2288634857_0_0_0 = { 3, GenInst_Uri_t100236324_0_0_0_X509Certificate2_t714049126_0_0_0_KeyValuePair_2_t2288634857_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t100236324_0_0_0_X509Certificate2_t714049126_0_0_0_Uri_t100236324_0_0_0_Types[] = { &Uri_t100236324_0_0_0, &X509Certificate2_t714049126_0_0_0, &Uri_t100236324_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t100236324_0_0_0_X509Certificate2_t714049126_0_0_0_Uri_t100236324_0_0_0 = { 3, GenInst_Uri_t100236324_0_0_0_X509Certificate2_t714049126_0_0_0_Uri_t100236324_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t100236324_0_0_0_X509Certificate2_t714049126_0_0_0_X509Certificate2_t714049126_0_0_0_Types[] = { &Uri_t100236324_0_0_0, &X509Certificate2_t714049126_0_0_0, &X509Certificate2_t714049126_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t100236324_0_0_0_X509Certificate2_t714049126_0_0_0_X509Certificate2_t714049126_0_0_0 = { 3, GenInst_Uri_t100236324_0_0_0_X509Certificate2_t714049126_0_0_0_X509Certificate2_t714049126_0_0_0_Types };
extern const Il2CppType IOutputSessionChannel_t2309827865_0_0_0;
static const Il2CppType* GenInst_IOutputSessionChannel_t2309827865_0_0_0_Types[] = { &IOutputSessionChannel_t2309827865_0_0_0 };
extern const Il2CppGenericInst GenInst_IOutputSessionChannel_t2309827865_0_0_0 = { 1, GenInst_IOutputSessionChannel_t2309827865_0_0_0_Types };
extern const Il2CppType IRequestSessionChannel_t3405854163_0_0_0;
static const Il2CppType* GenInst_IRequestSessionChannel_t3405854163_0_0_0_Types[] = { &IRequestSessionChannel_t3405854163_0_0_0 };
extern const Il2CppGenericInst GenInst_IRequestSessionChannel_t3405854163_0_0_0 = { 1, GenInst_IRequestSessionChannel_t3405854163_0_0_0_Types };
extern const Il2CppType IRequestChannel_t3653845626_0_0_0;
static const Il2CppType* GenInst_IRequestChannel_t3653845626_0_0_0_Types[] = { &IRequestChannel_t3653845626_0_0_0 };
extern const Il2CppGenericInst GenInst_IRequestChannel_t3653845626_0_0_0 = { 1, GenInst_IRequestChannel_t3653845626_0_0_0_Types };
extern const Il2CppType OperationContext_t2829644788_0_0_0;
static const Il2CppType* GenInst_OperationContext_t2829644788_0_0_0_Types[] = { &OperationContext_t2829644788_0_0_0 };
extern const Il2CppGenericInst GenInst_OperationContext_t2829644788_0_0_0 = { 1, GenInst_OperationContext_t2829644788_0_0_0_Types };
extern const Il2CppType ContractDescription_t1411178514_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_ContractDescription_t1411178514_0_0_0_Types[] = { &String_t_0_0_0, &ContractDescription_t1411178514_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ContractDescription_t1411178514_0_0_0 = { 2, GenInst_String_t_0_0_0_ContractDescription_t1411178514_0_0_0_Types };
static const Il2CppType* GenInst_ContractDescription_t1411178514_0_0_0_Types[] = { &ContractDescription_t1411178514_0_0_0 };
extern const Il2CppGenericInst GenInst_ContractDescription_t1411178514_0_0_0 = { 1, GenInst_ContractDescription_t1411178514_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ContractDescription_t1411178514_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &ContractDescription_t1411178514_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ContractDescription_t1411178514_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_ContractDescription_t1411178514_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3594106980_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3594106980_0_0_0_Types[] = { &KeyValuePair_2_t3594106980_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3594106980_0_0_0 = { 1, GenInst_KeyValuePair_2_t3594106980_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ContractDescription_t1411178514_0_0_0_KeyValuePair_2_t3594106980_0_0_0_Types[] = { &String_t_0_0_0, &ContractDescription_t1411178514_0_0_0, &KeyValuePair_2_t3594106980_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ContractDescription_t1411178514_0_0_0_KeyValuePair_2_t3594106980_0_0_0 = { 3, GenInst_String_t_0_0_0_ContractDescription_t1411178514_0_0_0_KeyValuePair_2_t3594106980_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ContractDescription_t1411178514_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &ContractDescription_t1411178514_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ContractDescription_t1411178514_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_ContractDescription_t1411178514_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ContractDescription_t1411178514_0_0_0_ContractDescription_t1411178514_0_0_0_Types[] = { &String_t_0_0_0, &ContractDescription_t1411178514_0_0_0, &ContractDescription_t1411178514_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ContractDescription_t1411178514_0_0_0_ContractDescription_t1411178514_0_0_0 = { 3, GenInst_String_t_0_0_0_ContractDescription_t1411178514_0_0_0_ContractDescription_t1411178514_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Uri_t100236324_0_0_0_Types[] = { &String_t_0_0_0, &Uri_t100236324_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Uri_t100236324_0_0_0 = { 2, GenInst_String_t_0_0_0_Uri_t100236324_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Uri_t100236324_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &Uri_t100236324_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Uri_t100236324_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_Uri_t100236324_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2283164790_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2283164790_0_0_0_Types[] = { &KeyValuePair_2_t2283164790_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2283164790_0_0_0 = { 1, GenInst_KeyValuePair_2_t2283164790_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Uri_t100236324_0_0_0_KeyValuePair_2_t2283164790_0_0_0_Types[] = { &String_t_0_0_0, &Uri_t100236324_0_0_0, &KeyValuePair_2_t2283164790_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Uri_t100236324_0_0_0_KeyValuePair_2_t2283164790_0_0_0 = { 3, GenInst_String_t_0_0_0_Uri_t100236324_0_0_0_KeyValuePair_2_t2283164790_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Uri_t100236324_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &Uri_t100236324_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Uri_t100236324_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_Uri_t100236324_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Uri_t100236324_0_0_0_Uri_t100236324_0_0_0_Types[] = { &String_t_0_0_0, &Uri_t100236324_0_0_0, &Uri_t100236324_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Uri_t100236324_0_0_0_Uri_t100236324_0_0_0 = { 3, GenInst_String_t_0_0_0_Uri_t100236324_0_0_0_Uri_t100236324_0_0_0_Types };
static const Il2CppType* GenInst_ServiceEndpoint_t4038493094_0_0_0_ChannelDispatcher_t2781106991_0_0_0_Types[] = { &ServiceEndpoint_t4038493094_0_0_0, &ChannelDispatcher_t2781106991_0_0_0 };
extern const Il2CppGenericInst GenInst_ServiceEndpoint_t4038493094_0_0_0_ChannelDispatcher_t2781106991_0_0_0 = { 2, GenInst_ServiceEndpoint_t4038493094_0_0_0_ChannelDispatcher_t2781106991_0_0_0_Types };
static const Il2CppType* GenInst_ServiceEndpoint_t4038493094_0_0_0_ChannelDispatcher_t2781106991_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &ServiceEndpoint_t4038493094_0_0_0, &ChannelDispatcher_t2781106991_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_ServiceEndpoint_t4038493094_0_0_0_ChannelDispatcher_t2781106991_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_ServiceEndpoint_t4038493094_0_0_0_ChannelDispatcher_t2781106991_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4278733032_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4278733032_0_0_0_Types[] = { &KeyValuePair_2_t4278733032_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4278733032_0_0_0 = { 1, GenInst_KeyValuePair_2_t4278733032_0_0_0_Types };
static const Il2CppType* GenInst_ServiceEndpoint_t4038493094_0_0_0_ChannelDispatcher_t2781106991_0_0_0_KeyValuePair_2_t4278733032_0_0_0_Types[] = { &ServiceEndpoint_t4038493094_0_0_0, &ChannelDispatcher_t2781106991_0_0_0, &KeyValuePair_2_t4278733032_0_0_0 };
extern const Il2CppGenericInst GenInst_ServiceEndpoint_t4038493094_0_0_0_ChannelDispatcher_t2781106991_0_0_0_KeyValuePair_2_t4278733032_0_0_0 = { 3, GenInst_ServiceEndpoint_t4038493094_0_0_0_ChannelDispatcher_t2781106991_0_0_0_KeyValuePair_2_t4278733032_0_0_0_Types };
static const Il2CppType* GenInst_ServiceEndpoint_t4038493094_0_0_0_ChannelDispatcher_t2781106991_0_0_0_ServiceEndpoint_t4038493094_0_0_0_Types[] = { &ServiceEndpoint_t4038493094_0_0_0, &ChannelDispatcher_t2781106991_0_0_0, &ServiceEndpoint_t4038493094_0_0_0 };
extern const Il2CppGenericInst GenInst_ServiceEndpoint_t4038493094_0_0_0_ChannelDispatcher_t2781106991_0_0_0_ServiceEndpoint_t4038493094_0_0_0 = { 3, GenInst_ServiceEndpoint_t4038493094_0_0_0_ChannelDispatcher_t2781106991_0_0_0_ServiceEndpoint_t4038493094_0_0_0_Types };
static const Il2CppType* GenInst_ServiceEndpoint_t4038493094_0_0_0_ChannelDispatcher_t2781106991_0_0_0_ChannelDispatcher_t2781106991_0_0_0_Types[] = { &ServiceEndpoint_t4038493094_0_0_0, &ChannelDispatcher_t2781106991_0_0_0, &ChannelDispatcher_t2781106991_0_0_0 };
extern const Il2CppGenericInst GenInst_ServiceEndpoint_t4038493094_0_0_0_ChannelDispatcher_t2781106991_0_0_0_ChannelDispatcher_t2781106991_0_0_0 = { 3, GenInst_ServiceEndpoint_t4038493094_0_0_0_ChannelDispatcher_t2781106991_0_0_0_ChannelDispatcher_t2781106991_0_0_0_Types };
static const Il2CppType* GenInst_ServiceEndpoint_t4038493094_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &ServiceEndpoint_t4038493094_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_ServiceEndpoint_t4038493094_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_ServiceEndpoint_t4038493094_0_0_0_Boolean_t97287965_0_0_0_Types };
extern const Il2CppType SecurityBindingElement_t1737011943_0_0_0;
static const Il2CppType* GenInst_SecurityBindingElement_t1737011943_0_0_0_Types[] = { &SecurityBindingElement_t1737011943_0_0_0 };
extern const Il2CppGenericInst GenInst_SecurityBindingElement_t1737011943_0_0_0 = { 1, GenInst_SecurityBindingElement_t1737011943_0_0_0_Types };
extern const Il2CppType BaseInputModule_t2019268878_0_0_0;
static const Il2CppType* GenInst_BaseInputModule_t2019268878_0_0_0_Types[] = { &BaseInputModule_t2019268878_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInputModule_t2019268878_0_0_0 = { 1, GenInst_BaseInputModule_t2019268878_0_0_0_Types };
extern const Il2CppType UIBehaviour_t3495933518_0_0_0;
static const Il2CppType* GenInst_UIBehaviour_t3495933518_0_0_0_Types[] = { &UIBehaviour_t3495933518_0_0_0 };
extern const Il2CppGenericInst GenInst_UIBehaviour_t3495933518_0_0_0 = { 1, GenInst_UIBehaviour_t3495933518_0_0_0_Types };
extern const Il2CppType RaycastResult_t3360306849_0_0_0;
static const Il2CppType* GenInst_RaycastResult_t3360306849_0_0_0_Types[] = { &RaycastResult_t3360306849_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t3360306849_0_0_0 = { 1, GenInst_RaycastResult_t3360306849_0_0_0_Types };
extern const Il2CppType IDeselectHandler_t393712923_0_0_0;
static const Il2CppType* GenInst_IDeselectHandler_t393712923_0_0_0_Types[] = { &IDeselectHandler_t393712923_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeselectHandler_t393712923_0_0_0 = { 1, GenInst_IDeselectHandler_t393712923_0_0_0_Types };
extern const Il2CppType IEventSystemHandler_t3354683850_0_0_0;
static const Il2CppType* GenInst_IEventSystemHandler_t3354683850_0_0_0_Types[] = { &IEventSystemHandler_t3354683850_0_0_0 };
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t3354683850_0_0_0 = { 1, GenInst_IEventSystemHandler_t3354683850_0_0_0_Types };
extern const Il2CppType List_1_t531791296_0_0_0;
static const Il2CppType* GenInst_List_1_t531791296_0_0_0_Types[] = { &List_1_t531791296_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t531791296_0_0_0 = { 1, GenInst_List_1_t531791296_0_0_0_Types };
extern const Il2CppType List_1_t257213610_0_0_0;
static const Il2CppType* GenInst_List_1_t257213610_0_0_0_Types[] = { &List_1_t257213610_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t257213610_0_0_0 = { 1, GenInst_List_1_t257213610_0_0_0_Types };
extern const Il2CppType List_1_t3395709193_0_0_0;
static const Il2CppType* GenInst_List_1_t3395709193_0_0_0_Types[] = { &List_1_t3395709193_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3395709193_0_0_0 = { 1, GenInst_List_1_t3395709193_0_0_0_Types };
extern const Il2CppType ISelectHandler_t2271418839_0_0_0;
static const Il2CppType* GenInst_ISelectHandler_t2271418839_0_0_0_Types[] = { &ISelectHandler_t2271418839_0_0_0 };
extern const Il2CppGenericInst GenInst_ISelectHandler_t2271418839_0_0_0 = { 1, GenInst_ISelectHandler_t2271418839_0_0_0_Types };
extern const Il2CppType BaseRaycaster_t4150874583_0_0_0;
static const Il2CppType* GenInst_BaseRaycaster_t4150874583_0_0_0_Types[] = { &BaseRaycaster_t4150874583_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseRaycaster_t4150874583_0_0_0 = { 1, GenInst_BaseRaycaster_t4150874583_0_0_0_Types };
extern const Il2CppType Entry_t3344766165_0_0_0;
static const Il2CppType* GenInst_Entry_t3344766165_0_0_0_Types[] = { &Entry_t3344766165_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t3344766165_0_0_0 = { 1, GenInst_Entry_t3344766165_0_0_0_Types };
extern const Il2CppType BaseEventData_t3903027533_0_0_0;
static const Il2CppType* GenInst_BaseEventData_t3903027533_0_0_0_Types[] = { &BaseEventData_t3903027533_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseEventData_t3903027533_0_0_0 = { 1, GenInst_BaseEventData_t3903027533_0_0_0_Types };
extern const Il2CppType IPointerEnterHandler_t1016128679_0_0_0;
static const Il2CppType* GenInst_IPointerEnterHandler_t1016128679_0_0_0_Types[] = { &IPointerEnterHandler_t1016128679_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t1016128679_0_0_0 = { 1, GenInst_IPointerEnterHandler_t1016128679_0_0_0_Types };
extern const Il2CppType IPointerExitHandler_t4182793654_0_0_0;
static const Il2CppType* GenInst_IPointerExitHandler_t4182793654_0_0_0_Types[] = { &IPointerExitHandler_t4182793654_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t4182793654_0_0_0 = { 1, GenInst_IPointerExitHandler_t4182793654_0_0_0_Types };
extern const Il2CppType IPointerDownHandler_t1380080529_0_0_0;
static const Il2CppType* GenInst_IPointerDownHandler_t1380080529_0_0_0_Types[] = { &IPointerDownHandler_t1380080529_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t1380080529_0_0_0 = { 1, GenInst_IPointerDownHandler_t1380080529_0_0_0_Types };
extern const Il2CppType IPointerUpHandler_t277099170_0_0_0;
static const Il2CppType* GenInst_IPointerUpHandler_t277099170_0_0_0_Types[] = { &IPointerUpHandler_t277099170_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t277099170_0_0_0 = { 1, GenInst_IPointerUpHandler_t277099170_0_0_0_Types };
extern const Il2CppType IPointerClickHandler_t132471142_0_0_0;
static const Il2CppType* GenInst_IPointerClickHandler_t132471142_0_0_0_Types[] = { &IPointerClickHandler_t132471142_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t132471142_0_0_0 = { 1, GenInst_IPointerClickHandler_t132471142_0_0_0_Types };
extern const Il2CppType IInitializePotentialDragHandler_t608041180_0_0_0;
static const Il2CppType* GenInst_IInitializePotentialDragHandler_t608041180_0_0_0_Types[] = { &IInitializePotentialDragHandler_t608041180_0_0_0 };
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t608041180_0_0_0 = { 1, GenInst_IInitializePotentialDragHandler_t608041180_0_0_0_Types };
extern const Il2CppType IBeginDragHandler_t3293314358_0_0_0;
static const Il2CppType* GenInst_IBeginDragHandler_t3293314358_0_0_0_Types[] = { &IBeginDragHandler_t3293314358_0_0_0 };
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t3293314358_0_0_0 = { 1, GenInst_IBeginDragHandler_t3293314358_0_0_0_Types };
extern const Il2CppType IDragHandler_t2288426503_0_0_0;
static const Il2CppType* GenInst_IDragHandler_t2288426503_0_0_0_Types[] = { &IDragHandler_t2288426503_0_0_0 };
extern const Il2CppGenericInst GenInst_IDragHandler_t2288426503_0_0_0 = { 1, GenInst_IDragHandler_t2288426503_0_0_0_Types };
extern const Il2CppType IEndDragHandler_t297508562_0_0_0;
static const Il2CppType* GenInst_IEndDragHandler_t297508562_0_0_0_Types[] = { &IEndDragHandler_t297508562_0_0_0 };
extern const Il2CppGenericInst GenInst_IEndDragHandler_t297508562_0_0_0 = { 1, GenInst_IEndDragHandler_t297508562_0_0_0_Types };
extern const Il2CppType IDropHandler_t3627139509_0_0_0;
static const Il2CppType* GenInst_IDropHandler_t3627139509_0_0_0_Types[] = { &IDropHandler_t3627139509_0_0_0 };
extern const Il2CppGenericInst GenInst_IDropHandler_t3627139509_0_0_0 = { 1, GenInst_IDropHandler_t3627139509_0_0_0_Types };
extern const Il2CppType IScrollHandler_t4201797704_0_0_0;
static const Il2CppType* GenInst_IScrollHandler_t4201797704_0_0_0_Types[] = { &IScrollHandler_t4201797704_0_0_0 };
extern const Il2CppGenericInst GenInst_IScrollHandler_t4201797704_0_0_0 = { 1, GenInst_IScrollHandler_t4201797704_0_0_0_Types };
extern const Il2CppType IUpdateSelectedHandler_t4266291469_0_0_0;
static const Il2CppType* GenInst_IUpdateSelectedHandler_t4266291469_0_0_0_Types[] = { &IUpdateSelectedHandler_t4266291469_0_0_0 };
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t4266291469_0_0_0 = { 1, GenInst_IUpdateSelectedHandler_t4266291469_0_0_0_Types };
extern const Il2CppType IMoveHandler_t933334182_0_0_0;
static const Il2CppType* GenInst_IMoveHandler_t933334182_0_0_0_Types[] = { &IMoveHandler_t933334182_0_0_0 };
extern const Il2CppGenericInst GenInst_IMoveHandler_t933334182_0_0_0 = { 1, GenInst_IMoveHandler_t933334182_0_0_0_Types };
extern const Il2CppType ISubmitHandler_t2790798304_0_0_0;
static const Il2CppType* GenInst_ISubmitHandler_t2790798304_0_0_0_Types[] = { &ISubmitHandler_t2790798304_0_0_0 };
extern const Il2CppGenericInst GenInst_ISubmitHandler_t2790798304_0_0_0 = { 1, GenInst_ISubmitHandler_t2790798304_0_0_0_Types };
extern const Il2CppType ICancelHandler_t3974364820_0_0_0;
static const Il2CppType* GenInst_ICancelHandler_t3974364820_0_0_0_Types[] = { &ICancelHandler_t3974364820_0_0_0 };
extern const Il2CppGenericInst GenInst_ICancelHandler_t3974364820_0_0_0 = { 1, GenInst_ICancelHandler_t3974364820_0_0_0_Types };
extern const Il2CppType Transform_t3600365921_0_0_0;
static const Il2CppType* GenInst_Transform_t3600365921_0_0_0_Types[] = { &Transform_t3600365921_0_0_0 };
extern const Il2CppGenericInst GenInst_Transform_t3600365921_0_0_0 = { 1, GenInst_Transform_t3600365921_0_0_0_Types };
extern const Il2CppType BaseInput_t3630163547_0_0_0;
static const Il2CppType* GenInst_BaseInput_t3630163547_0_0_0_Types[] = { &BaseInput_t3630163547_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInput_t3630163547_0_0_0 = { 1, GenInst_BaseInput_t3630163547_0_0_0_Types };
extern const Il2CppType PointerEventData_t3807901092_0_0_0;
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &PointerEventData_t3807901092_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_Types };
static const Il2CppType* GenInst_PointerEventData_t3807901092_0_0_0_Types[] = { &PointerEventData_t3807901092_0_0_0 };
extern const Il2CppGenericInst GenInst_PointerEventData_t3807901092_0_0_0 = { 1, GenInst_PointerEventData_t3807901092_0_0_0_Types };
extern const Il2CppType AbstractEventData_t4171500731_0_0_0;
static const Il2CppType* GenInst_AbstractEventData_t4171500731_0_0_0_Types[] = { &AbstractEventData_t4171500731_0_0_0 };
extern const Il2CppGenericInst GenInst_AbstractEventData_t4171500731_0_0_0 = { 1, GenInst_AbstractEventData_t4171500731_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &PointerEventData_t3807901092_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t799319294_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t799319294_0_0_0_Types[] = { &KeyValuePair_2_t799319294_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t799319294_0_0_0 = { 1, GenInst_KeyValuePair_2_t799319294_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_KeyValuePair_2_t799319294_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &PointerEventData_t3807901092_0_0_0, &KeyValuePair_2_t799319294_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_KeyValuePair_2_t799319294_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_KeyValuePair_2_t799319294_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &PointerEventData_t3807901092_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_PointerEventData_t3807901092_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &PointerEventData_t3807901092_0_0_0, &PointerEventData_t3807901092_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_PointerEventData_t3807901092_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_PointerEventData_t3807901092_0_0_0_Types };
extern const Il2CppType ButtonState_t857139936_0_0_0;
static const Il2CppType* GenInst_ButtonState_t857139936_0_0_0_Types[] = { &ButtonState_t857139936_0_0_0 };
extern const Il2CppGenericInst GenInst_ButtonState_t857139936_0_0_0 = { 1, GenInst_ButtonState_t857139936_0_0_0_Types };
extern const Il2CppType ICanvasElement_t2121898866_0_0_0;
static const Il2CppType* GenInst_ICanvasElement_t2121898866_0_0_0_Types[] = { &ICanvasElement_t2121898866_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t2121898866_0_0_0 = { 1, GenInst_ICanvasElement_t2121898866_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &ICanvasElement_t2121898866_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0 = { 2, GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &ICanvasElement_t2121898866_0_0_0, &Int32_t2950945753_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1364477206_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1364477206_0_0_0_Types[] = { &KeyValuePair_2_t1364477206_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1364477206_0_0_0 = { 1, GenInst_KeyValuePair_2_t1364477206_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t1364477206_0_0_0_Types[] = { &ICanvasElement_t2121898866_0_0_0, &Int32_t2950945753_0_0_0, &KeyValuePair_2_t1364477206_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t1364477206_0_0_0 = { 3, GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t1364477206_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_ICanvasElement_t2121898866_0_0_0_Types[] = { &ICanvasElement_t2121898866_0_0_0, &Int32_t2950945753_0_0_0, &ICanvasElement_t2121898866_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_ICanvasElement_t2121898866_0_0_0 = { 3, GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_ICanvasElement_t2121898866_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &ICanvasElement_t2121898866_0_0_0, &Int32_t2950945753_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types };
extern const Il2CppType ColorBlock_t2139031574_0_0_0;
static const Il2CppType* GenInst_ColorBlock_t2139031574_0_0_0_Types[] = { &ColorBlock_t2139031574_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorBlock_t2139031574_0_0_0 = { 1, GenInst_ColorBlock_t2139031574_0_0_0_Types };
extern const Il2CppType OptionData_t3270282352_0_0_0;
static const Il2CppType* GenInst_OptionData_t3270282352_0_0_0_Types[] = { &OptionData_t3270282352_0_0_0 };
extern const Il2CppGenericInst GenInst_OptionData_t3270282352_0_0_0 = { 1, GenInst_OptionData_t3270282352_0_0_0_Types };
extern const Il2CppType DropdownItem_t1451952895_0_0_0;
static const Il2CppType* GenInst_DropdownItem_t1451952895_0_0_0_Types[] = { &DropdownItem_t1451952895_0_0_0 };
extern const Il2CppGenericInst GenInst_DropdownItem_t1451952895_0_0_0 = { 1, GenInst_DropdownItem_t1451952895_0_0_0_Types };
extern const Il2CppType FloatTween_t1274330004_0_0_0;
static const Il2CppType* GenInst_FloatTween_t1274330004_0_0_0_Types[] = { &FloatTween_t1274330004_0_0_0 };
extern const Il2CppGenericInst GenInst_FloatTween_t1274330004_0_0_0 = { 1, GenInst_FloatTween_t1274330004_0_0_0_Types };
extern const Il2CppType Sprite_t280657092_0_0_0;
static const Il2CppType* GenInst_Sprite_t280657092_0_0_0_Types[] = { &Sprite_t280657092_0_0_0 };
extern const Il2CppGenericInst GenInst_Sprite_t280657092_0_0_0 = { 1, GenInst_Sprite_t280657092_0_0_0_Types };
extern const Il2CppType Canvas_t3310196443_0_0_0;
static const Il2CppType* GenInst_Canvas_t3310196443_0_0_0_Types[] = { &Canvas_t3310196443_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t3310196443_0_0_0 = { 1, GenInst_Canvas_t3310196443_0_0_0_Types };
extern const Il2CppType List_1_t487303889_0_0_0;
static const Il2CppType* GenInst_List_1_t487303889_0_0_0_Types[] = { &List_1_t487303889_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t487303889_0_0_0 = { 1, GenInst_List_1_t487303889_0_0_0_Types };
extern const Il2CppType HashSet_1_t466832188_0_0_0;
static const Il2CppType* GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_Types[] = { &Font_t1956802104_0_0_0, &HashSet_1_t466832188_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0 = { 2, GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_Types };
extern const Il2CppType Text_t1901882714_0_0_0;
static const Il2CppType* GenInst_Text_t1901882714_0_0_0_Types[] = { &Text_t1901882714_0_0_0 };
extern const Il2CppGenericInst GenInst_Text_t1901882714_0_0_0 = { 1, GenInst_Text_t1901882714_0_0_0_Types };
extern const Il2CppType Link_t2031043523_0_0_0;
static const Il2CppType* GenInst_Link_t2031043523_0_0_0_Types[] = { &Link_t2031043523_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t2031043523_0_0_0 = { 1, GenInst_Link_t2031043523_0_0_0_Types };
extern const Il2CppType ILayoutElement_t4082016710_0_0_0;
static const Il2CppType* GenInst_ILayoutElement_t4082016710_0_0_0_Types[] = { &ILayoutElement_t4082016710_0_0_0 };
extern const Il2CppGenericInst GenInst_ILayoutElement_t4082016710_0_0_0 = { 1, GenInst_ILayoutElement_t4082016710_0_0_0_Types };
extern const Il2CppType MaskableGraphic_t3839221559_0_0_0;
static const Il2CppType* GenInst_MaskableGraphic_t3839221559_0_0_0_Types[] = { &MaskableGraphic_t3839221559_0_0_0 };
extern const Il2CppGenericInst GenInst_MaskableGraphic_t3839221559_0_0_0 = { 1, GenInst_MaskableGraphic_t3839221559_0_0_0_Types };
extern const Il2CppType IClippable_t1239629351_0_0_0;
static const Il2CppType* GenInst_IClippable_t1239629351_0_0_0_Types[] = { &IClippable_t1239629351_0_0_0 };
extern const Il2CppGenericInst GenInst_IClippable_t1239629351_0_0_0 = { 1, GenInst_IClippable_t1239629351_0_0_0_Types };
extern const Il2CppType IMaskable_t433386433_0_0_0;
static const Il2CppType* GenInst_IMaskable_t433386433_0_0_0_Types[] = { &IMaskable_t433386433_0_0_0 };
extern const Il2CppGenericInst GenInst_IMaskable_t433386433_0_0_0 = { 1, GenInst_IMaskable_t433386433_0_0_0_Types };
extern const Il2CppType IMaterialModifier_t1975025690_0_0_0;
static const Il2CppType* GenInst_IMaterialModifier_t1975025690_0_0_0_Types[] = { &IMaterialModifier_t1975025690_0_0_0 };
extern const Il2CppGenericInst GenInst_IMaterialModifier_t1975025690_0_0_0 = { 1, GenInst_IMaterialModifier_t1975025690_0_0_0_Types };
extern const Il2CppType Graphic_t1660335611_0_0_0;
static const Il2CppType* GenInst_Graphic_t1660335611_0_0_0_Types[] = { &Graphic_t1660335611_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t1660335611_0_0_0 = { 1, GenInst_Graphic_t1660335611_0_0_0_Types };
static const Il2CppType* GenInst_HashSet_1_t466832188_0_0_0_Types[] = { &HashSet_1_t466832188_0_0_0 };
extern const Il2CppGenericInst GenInst_HashSet_1_t466832188_0_0_0 = { 1, GenInst_HashSet_1_t466832188_0_0_0_Types };
static const Il2CppType* GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Font_t1956802104_0_0_0, &HashSet_1_t466832188_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2767015899_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2767015899_0_0_0_Types[] = { &KeyValuePair_2_t2767015899_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2767015899_0_0_0 = { 1, GenInst_KeyValuePair_2_t2767015899_0_0_0_Types };
static const Il2CppType* GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_KeyValuePair_2_t2767015899_0_0_0_Types[] = { &Font_t1956802104_0_0_0, &HashSet_1_t466832188_0_0_0, &KeyValuePair_2_t2767015899_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_KeyValuePair_2_t2767015899_0_0_0 = { 3, GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_KeyValuePair_2_t2767015899_0_0_0_Types };
static const Il2CppType* GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_Font_t1956802104_0_0_0_Types[] = { &Font_t1956802104_0_0_0, &HashSet_1_t466832188_0_0_0, &Font_t1956802104_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_Font_t1956802104_0_0_0 = { 3, GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_Font_t1956802104_0_0_0_Types };
static const Il2CppType* GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_HashSet_1_t466832188_0_0_0_Types[] = { &Font_t1956802104_0_0_0, &HashSet_1_t466832188_0_0_0, &HashSet_1_t466832188_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_HashSet_1_t466832188_0_0_0 = { 3, GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_HashSet_1_t466832188_0_0_0_Types };
extern const Il2CppType ColorTween_t809614380_0_0_0;
static const Il2CppType* GenInst_ColorTween_t809614380_0_0_0_Types[] = { &ColorTween_t809614380_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorTween_t809614380_0_0_0 = { 1, GenInst_ColorTween_t809614380_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t3109723551_0_0_0;
static const Il2CppType* GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_Types[] = { &Canvas_t3310196443_0_0_0, &IndexedSet_1_t3109723551_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0 = { 2, GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Graphic_t1660335611_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0 = { 2, GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Graphic_t1660335611_0_0_0, &Int32_t2950945753_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3639519817_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3639519817_0_0_0_Types[] = { &KeyValuePair_2_t3639519817_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3639519817_0_0_0 = { 1, GenInst_KeyValuePair_2_t3639519817_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t3639519817_0_0_0_Types[] = { &Graphic_t1660335611_0_0_0, &Int32_t2950945753_0_0_0, &KeyValuePair_2_t3639519817_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t3639519817_0_0_0 = { 3, GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t3639519817_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_Graphic_t1660335611_0_0_0_Types[] = { &Graphic_t1660335611_0_0_0, &Int32_t2950945753_0_0_0, &Graphic_t1660335611_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_Graphic_t1660335611_0_0_0 = { 3, GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_Graphic_t1660335611_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Graphic_t1660335611_0_0_0, &Int32_t2950945753_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t3109723551_0_0_0_Types[] = { &IndexedSet_1_t3109723551_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t3109723551_0_0_0 = { 1, GenInst_IndexedSet_1_t3109723551_0_0_0_Types };
static const Il2CppType* GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Canvas_t3310196443_0_0_0, &IndexedSet_1_t3109723551_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t398822319_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t398822319_0_0_0_Types[] = { &KeyValuePair_2_t398822319_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t398822319_0_0_0 = { 1, GenInst_KeyValuePair_2_t398822319_0_0_0_Types };
static const Il2CppType* GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_KeyValuePair_2_t398822319_0_0_0_Types[] = { &Canvas_t3310196443_0_0_0, &IndexedSet_1_t3109723551_0_0_0, &KeyValuePair_2_t398822319_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_KeyValuePair_2_t398822319_0_0_0 = { 3, GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_KeyValuePair_2_t398822319_0_0_0_Types };
static const Il2CppType* GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_Canvas_t3310196443_0_0_0_Types[] = { &Canvas_t3310196443_0_0_0, &IndexedSet_1_t3109723551_0_0_0, &Canvas_t3310196443_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_Canvas_t3310196443_0_0_0 = { 3, GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_Canvas_t3310196443_0_0_0_Types };
static const Il2CppType* GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_IndexedSet_1_t3109723551_0_0_0_Types[] = { &Canvas_t3310196443_0_0_0, &IndexedSet_1_t3109723551_0_0_0, &IndexedSet_1_t3109723551_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_IndexedSet_1_t3109723551_0_0_0 = { 3, GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_IndexedSet_1_t3109723551_0_0_0_Types };
extern const Il2CppType Type_t1152881528_0_0_0;
static const Il2CppType* GenInst_Type_t1152881528_0_0_0_Types[] = { &Type_t1152881528_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t1152881528_0_0_0 = { 1, GenInst_Type_t1152881528_0_0_0_Types };
extern const Il2CppType FillMethod_t1167457570_0_0_0;
static const Il2CppType* GenInst_FillMethod_t1167457570_0_0_0_Types[] = { &FillMethod_t1167457570_0_0_0 };
extern const Il2CppGenericInst GenInst_FillMethod_t1167457570_0_0_0 = { 1, GenInst_FillMethod_t1167457570_0_0_0_Types };
extern const Il2CppType ContentType_t1787303396_0_0_0;
static const Il2CppType* GenInst_ContentType_t1787303396_0_0_0_Types[] = { &ContentType_t1787303396_0_0_0 };
extern const Il2CppGenericInst GenInst_ContentType_t1787303396_0_0_0 = { 1, GenInst_ContentType_t1787303396_0_0_0_Types };
extern const Il2CppType LineType_t4214648469_0_0_0;
static const Il2CppType* GenInst_LineType_t4214648469_0_0_0_Types[] = { &LineType_t4214648469_0_0_0 };
extern const Il2CppGenericInst GenInst_LineType_t4214648469_0_0_0 = { 1, GenInst_LineType_t4214648469_0_0_0_Types };
extern const Il2CppType InputType_t1770400679_0_0_0;
static const Il2CppType* GenInst_InputType_t1770400679_0_0_0_Types[] = { &InputType_t1770400679_0_0_0 };
extern const Il2CppGenericInst GenInst_InputType_t1770400679_0_0_0 = { 1, GenInst_InputType_t1770400679_0_0_0_Types };
extern const Il2CppType TouchScreenKeyboardType_t1530597702_0_0_0;
static const Il2CppType* GenInst_TouchScreenKeyboardType_t1530597702_0_0_0_Types[] = { &TouchScreenKeyboardType_t1530597702_0_0_0 };
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t1530597702_0_0_0 = { 1, GenInst_TouchScreenKeyboardType_t1530597702_0_0_0_Types };
extern const Il2CppType CharacterValidation_t4051914437_0_0_0;
static const Il2CppType* GenInst_CharacterValidation_t4051914437_0_0_0_Types[] = { &CharacterValidation_t4051914437_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterValidation_t4051914437_0_0_0 = { 1, GenInst_CharacterValidation_t4051914437_0_0_0_Types };
extern const Il2CppType Mask_t1803652131_0_0_0;
static const Il2CppType* GenInst_Mask_t1803652131_0_0_0_Types[] = { &Mask_t1803652131_0_0_0 };
extern const Il2CppGenericInst GenInst_Mask_t1803652131_0_0_0 = { 1, GenInst_Mask_t1803652131_0_0_0_Types };
extern const Il2CppType ICanvasRaycastFilter_t2454702837_0_0_0;
static const Il2CppType* GenInst_ICanvasRaycastFilter_t2454702837_0_0_0_Types[] = { &ICanvasRaycastFilter_t2454702837_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasRaycastFilter_t2454702837_0_0_0 = { 1, GenInst_ICanvasRaycastFilter_t2454702837_0_0_0_Types };
extern const Il2CppType List_1_t3275726873_0_0_0;
static const Il2CppType* GenInst_List_1_t3275726873_0_0_0_Types[] = { &List_1_t3275726873_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3275726873_0_0_0 = { 1, GenInst_List_1_t3275726873_0_0_0_Types };
extern const Il2CppType RectMask2D_t3474889437_0_0_0;
static const Il2CppType* GenInst_RectMask2D_t3474889437_0_0_0_Types[] = { &RectMask2D_t3474889437_0_0_0 };
extern const Il2CppGenericInst GenInst_RectMask2D_t3474889437_0_0_0 = { 1, GenInst_RectMask2D_t3474889437_0_0_0_Types };
extern const Il2CppType IClipper_t1224123152_0_0_0;
static const Il2CppType* GenInst_IClipper_t1224123152_0_0_0_Types[] = { &IClipper_t1224123152_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t1224123152_0_0_0 = { 1, GenInst_IClipper_t1224123152_0_0_0_Types };
extern const Il2CppType List_1_t651996883_0_0_0;
static const Il2CppType* GenInst_List_1_t651996883_0_0_0_Types[] = { &List_1_t651996883_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t651996883_0_0_0 = { 1, GenInst_List_1_t651996883_0_0_0_Types };
extern const Il2CppType Navigation_t3049316579_0_0_0;
static const Il2CppType* GenInst_Navigation_t3049316579_0_0_0_Types[] = { &Navigation_t3049316579_0_0_0 };
extern const Il2CppGenericInst GenInst_Navigation_t3049316579_0_0_0 = { 1, GenInst_Navigation_t3049316579_0_0_0_Types };
extern const Il2CppType Link_t1368790160_0_0_0;
static const Il2CppType* GenInst_Link_t1368790160_0_0_0_Types[] = { &Link_t1368790160_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t1368790160_0_0_0 = { 1, GenInst_Link_t1368790160_0_0_0_Types };
extern const Il2CppType Direction_t3470714353_0_0_0;
static const Il2CppType* GenInst_Direction_t3470714353_0_0_0_Types[] = { &Direction_t3470714353_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t3470714353_0_0_0 = { 1, GenInst_Direction_t3470714353_0_0_0_Types };
extern const Il2CppType Selectable_t3250028441_0_0_0;
static const Il2CppType* GenInst_Selectable_t3250028441_0_0_0_Types[] = { &Selectable_t3250028441_0_0_0 };
extern const Il2CppGenericInst GenInst_Selectable_t3250028441_0_0_0 = { 1, GenInst_Selectable_t3250028441_0_0_0_Types };
extern const Il2CppType Transition_t1769908631_0_0_0;
static const Il2CppType* GenInst_Transition_t1769908631_0_0_0_Types[] = { &Transition_t1769908631_0_0_0 };
extern const Il2CppGenericInst GenInst_Transition_t1769908631_0_0_0 = { 1, GenInst_Transition_t1769908631_0_0_0_Types };
extern const Il2CppType SpriteState_t1362986479_0_0_0;
static const Il2CppType* GenInst_SpriteState_t1362986479_0_0_0_Types[] = { &SpriteState_t1362986479_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteState_t1362986479_0_0_0 = { 1, GenInst_SpriteState_t1362986479_0_0_0_Types };
extern const Il2CppType CanvasGroup_t4083511760_0_0_0;
static const Il2CppType* GenInst_CanvasGroup_t4083511760_0_0_0_Types[] = { &CanvasGroup_t4083511760_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasGroup_t4083511760_0_0_0 = { 1, GenInst_CanvasGroup_t4083511760_0_0_0_Types };
extern const Il2CppType Direction_t337909235_0_0_0;
static const Il2CppType* GenInst_Direction_t337909235_0_0_0_Types[] = { &Direction_t337909235_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t337909235_0_0_0 = { 1, GenInst_Direction_t337909235_0_0_0_Types };
extern const Il2CppType MatEntry_t2957107092_0_0_0;
static const Il2CppType* GenInst_MatEntry_t2957107092_0_0_0_Types[] = { &MatEntry_t2957107092_0_0_0 };
extern const Il2CppGenericInst GenInst_MatEntry_t2957107092_0_0_0 = { 1, GenInst_MatEntry_t2957107092_0_0_0_Types };
extern const Il2CppType Toggle_t2735377061_0_0_0;
static const Il2CppType* GenInst_Toggle_t2735377061_0_0_0_Types[] = { &Toggle_t2735377061_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t2735377061_0_0_0 = { 1, GenInst_Toggle_t2735377061_0_0_0_Types };
static const Il2CppType* GenInst_Toggle_t2735377061_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &Toggle_t2735377061_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t2735377061_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_Toggle_t2735377061_0_0_0_Boolean_t97287965_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &IClipper_t1224123152_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0 = { 2, GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &IClipper_t1224123152_0_0_0, &Int32_t2950945753_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1634190656_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1634190656_0_0_0_Types[] = { &KeyValuePair_2_t1634190656_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1634190656_0_0_0 = { 1, GenInst_KeyValuePair_2_t1634190656_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t1634190656_0_0_0_Types[] = { &IClipper_t1224123152_0_0_0, &Int32_t2950945753_0_0_0, &KeyValuePair_2_t1634190656_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t1634190656_0_0_0 = { 3, GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t1634190656_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_IClipper_t1224123152_0_0_0_Types[] = { &IClipper_t1224123152_0_0_0, &Int32_t2950945753_0_0_0, &IClipper_t1224123152_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_IClipper_t1224123152_0_0_0 = { 3, GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_IClipper_t1224123152_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &IClipper_t1224123152_0_0_0, &Int32_t2950945753_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types };
extern const Il2CppType AspectMode_t3417192999_0_0_0;
static const Il2CppType* GenInst_AspectMode_t3417192999_0_0_0_Types[] = { &AspectMode_t3417192999_0_0_0 };
extern const Il2CppGenericInst GenInst_AspectMode_t3417192999_0_0_0 = { 1, GenInst_AspectMode_t3417192999_0_0_0_Types };
extern const Il2CppType FitMode_t3267881214_0_0_0;
static const Il2CppType* GenInst_FitMode_t3267881214_0_0_0_Types[] = { &FitMode_t3267881214_0_0_0 };
extern const Il2CppGenericInst GenInst_FitMode_t3267881214_0_0_0 = { 1, GenInst_FitMode_t3267881214_0_0_0_Types };
extern const Il2CppType RectTransform_t3704657025_0_0_0;
static const Il2CppType* GenInst_RectTransform_t3704657025_0_0_0_Types[] = { &RectTransform_t3704657025_0_0_0 };
extern const Il2CppGenericInst GenInst_RectTransform_t3704657025_0_0_0 = { 1, GenInst_RectTransform_t3704657025_0_0_0_Types };
extern const Il2CppType LayoutRebuilder_t541313304_0_0_0;
static const Il2CppType* GenInst_LayoutRebuilder_t541313304_0_0_0_Types[] = { &LayoutRebuilder_t541313304_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t541313304_0_0_0 = { 1, GenInst_LayoutRebuilder_t541313304_0_0_0_Types };
static const Il2CppType* GenInst_ILayoutElement_t4082016710_0_0_0_Single_t1397266774_0_0_0_Types[] = { &ILayoutElement_t4082016710_0_0_0, &Single_t1397266774_0_0_0 };
extern const Il2CppGenericInst GenInst_ILayoutElement_t4082016710_0_0_0_Single_t1397266774_0_0_0 = { 2, GenInst_ILayoutElement_t4082016710_0_0_0_Single_t1397266774_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Single_t1397266774_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Single_t1397266774_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Single_t1397266774_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Single_t1397266774_0_0_0_Types };
extern const Il2CppType List_1_t899420910_0_0_0;
static const Il2CppType* GenInst_List_1_t899420910_0_0_0_Types[] = { &List_1_t899420910_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t899420910_0_0_0 = { 1, GenInst_List_1_t899420910_0_0_0_Types };
extern const Il2CppType List_1_t4072576034_0_0_0;
static const Il2CppType* GenInst_List_1_t4072576034_0_0_0_Types[] = { &List_1_t4072576034_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t4072576034_0_0_0 = { 1, GenInst_List_1_t4072576034_0_0_0_Types };
extern const Il2CppType List_1_t3628304265_0_0_0;
static const Il2CppType* GenInst_List_1_t3628304265_0_0_0_Types[] = { &List_1_t3628304265_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3628304265_0_0_0 = { 1, GenInst_List_1_t3628304265_0_0_0_Types };
extern const Il2CppType List_1_t496136383_0_0_0;
static const Il2CppType* GenInst_List_1_t496136383_0_0_0_Types[] = { &List_1_t496136383_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t496136383_0_0_0 = { 1, GenInst_List_1_t496136383_0_0_0_Types };
extern const Il2CppType List_1_t1234605051_0_0_0;
static const Il2CppType* GenInst_List_1_t1234605051_0_0_0_Types[] = { &List_1_t1234605051_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1234605051_0_0_0 = { 1, GenInst_List_1_t1234605051_0_0_0_Types };
extern const Il2CppType RendererCache_t1904069300_0_0_0;
static const Il2CppType* GenInst_RendererCache_t1904069300_0_0_0_Types[] = { &RendererCache_t1904069300_0_0_0 };
extern const Il2CppGenericInst GenInst_RendererCache_t1904069300_0_0_0 = { 1, GenInst_RendererCache_t1904069300_0_0_0_Types };
extern const Il2CppType Highlighter_t672210414_0_0_0;
static const Il2CppType* GenInst_Highlighter_t672210414_0_0_0_Types[] = { &Highlighter_t672210414_0_0_0 };
extern const Il2CppGenericInst GenInst_Highlighter_t672210414_0_0_0 = { 1, GenInst_Highlighter_t672210414_0_0_0_Types };
extern const Il2CppType Data_t1588725102_0_0_0;
static const Il2CppType* GenInst_Data_t1588725102_0_0_0_Types[] = { &Data_t1588725102_0_0_0 };
extern const Il2CppGenericInst GenInst_Data_t1588725102_0_0_0 = { 1, GenInst_Data_t1588725102_0_0_0_Types };
extern const Il2CppType Link_t801371223_0_0_0;
static const Il2CppType* GenInst_Link_t801371223_0_0_0_Types[] = { &Link_t801371223_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t801371223_0_0_0 = { 1, GenInst_Link_t801371223_0_0_0_Types };
extern const Il2CppType Shader_t4151988712_0_0_0;
static const Il2CppType* GenInst_Shader_t4151988712_0_0_0_Types[] = { &Shader_t4151988712_0_0_0 };
extern const Il2CppGenericInst GenInst_Shader_t4151988712_0_0_0 = { 1, GenInst_Shader_t4151988712_0_0_0_Types };
extern const Il2CppType HighlightingBase_t582374880_0_0_0;
static const Il2CppType* GenInst_HighlightingBase_t582374880_0_0_0_Types[] = { &HighlightingBase_t582374880_0_0_0 };
extern const Il2CppGenericInst GenInst_HighlightingBase_t582374880_0_0_0 = { 1, GenInst_HighlightingBase_t582374880_0_0_0_Types };
extern const Il2CppType Hashtable_t1853889766_0_0_0;
static const Il2CppType* GenInst_Hashtable_t1853889766_0_0_0_Types[] = { &Hashtable_t1853889766_0_0_0 };
extern const Il2CppGenericInst GenInst_Hashtable_t1853889766_0_0_0 = { 1, GenInst_Hashtable_t1853889766_0_0_0_Types };
extern const Il2CppType Rect_t2360479859_0_0_0;
static const Il2CppType* GenInst_Rect_t2360479859_0_0_0_Types[] = { &Rect_t2360479859_0_0_0 };
extern const Il2CppGenericInst GenInst_Rect_t2360479859_0_0_0 = { 1, GenInst_Rect_t2360479859_0_0_0_Types };
extern const Il2CppType iTween_t770867771_0_0_0;
static const Il2CppType* GenInst_iTween_t770867771_0_0_0_Types[] = { &iTween_t770867771_0_0_0 };
extern const Il2CppGenericInst GenInst_iTween_t770867771_0_0_0 = { 1, GenInst_iTween_t770867771_0_0_0_Types };
extern const Il2CppType Preset_t736195574_0_0_0;
static const Il2CppType* GenInst_Preset_t736195574_0_0_0_Types[] = { &Preset_t736195574_0_0_0 };
extern const Il2CppGenericInst GenInst_Preset_t736195574_0_0_0 = { 1, GenInst_Preset_t736195574_0_0_0_Types };
extern const Il2CppType QualitySettings_t1869387270_0_0_0;
static const Il2CppType* GenInst_QualitySettings_t1869387270_0_0_0_Types[] = { &QualitySettings_t1869387270_0_0_0 };
extern const Il2CppGenericInst GenInst_QualitySettings_t1869387270_0_0_0 = { 1, GenInst_QualitySettings_t1869387270_0_0_0_Types };
extern const Il2CppType QualitySettings_t1379802392_0_0_0;
static const Il2CppType* GenInst_QualitySettings_t1379802392_0_0_0_Types[] = { &QualitySettings_t1379802392_0_0_0 };
extern const Il2CppGenericInst GenInst_QualitySettings_t1379802392_0_0_0 = { 1, GenInst_QualitySettings_t1379802392_0_0_0_Types };
extern const Il2CppType Mesh_t3648964284_0_0_0;
static const Il2CppType* GenInst_Mesh_t3648964284_0_0_0_Types[] = { &Mesh_t3648964284_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_t3648964284_0_0_0 = { 1, GenInst_Mesh_t3648964284_0_0_0_Types };
extern const Il2CppType Frame_t3511111948_0_0_0;
static const Il2CppType* GenInst_Frame_t3511111948_0_0_0_Types[] = { &Frame_t3511111948_0_0_0 };
extern const Il2CppGenericInst GenInst_Frame_t3511111948_0_0_0 = { 1, GenInst_Frame_t3511111948_0_0_0_Types };
extern const Il2CppType RenderTextureFormat_t962350765_0_0_0;
static const Il2CppType* GenInst_RenderTextureFormat_t962350765_0_0_0_Types[] = { &RenderTextureFormat_t962350765_0_0_0 };
extern const Il2CppGenericInst GenInst_RenderTextureFormat_t962350765_0_0_0 = { 1, GenInst_RenderTextureFormat_t962350765_0_0_0_Types };
extern const Il2CppType Preset_t2972107632_0_0_0;
static const Il2CppType* GenInst_Preset_t2972107632_0_0_0_Types[] = { &Preset_t2972107632_0_0_0 };
extern const Il2CppGenericInst GenInst_Preset_t2972107632_0_0_0 = { 1, GenInst_Preset_t2972107632_0_0_0_Types };
extern const Il2CppType InvBaseItem_t2503063521_0_0_0;
static const Il2CppType* GenInst_InvBaseItem_t2503063521_0_0_0_Types[] = { &InvBaseItem_t2503063521_0_0_0 };
extern const Il2CppGenericInst GenInst_InvBaseItem_t2503063521_0_0_0 = { 1, GenInst_InvBaseItem_t2503063521_0_0_0_Types };
extern const Il2CppType InvDatabase_t3341607346_0_0_0;
static const Il2CppType* GenInst_InvDatabase_t3341607346_0_0_0_Types[] = { &InvDatabase_t3341607346_0_0_0 };
extern const Il2CppGenericInst GenInst_InvDatabase_t3341607346_0_0_0 = { 1, GenInst_InvDatabase_t3341607346_0_0_0_Types };
extern const Il2CppType InvStat_t493203737_0_0_0;
static const Il2CppType* GenInst_InvStat_t493203737_0_0_0_Types[] = { &InvStat_t493203737_0_0_0 };
extern const Il2CppGenericInst GenInst_InvStat_t493203737_0_0_0 = { 1, GenInst_InvStat_t493203737_0_0_0_Types };
extern const Il2CppType InvGameItem_t1203288033_0_0_0;
static const Il2CppType* GenInst_InvGameItem_t1203288033_0_0_0_Types[] = { &InvGameItem_t1203288033_0_0_0 };
extern const Il2CppGenericInst GenInst_InvGameItem_t1203288033_0_0_0 = { 1, GenInst_InvGameItem_t1203288033_0_0_0_Types };
extern const Il2CppType InvAttachmentPoint_t2909008346_0_0_0;
static const Il2CppType* GenInst_InvAttachmentPoint_t2909008346_0_0_0_Types[] = { &InvAttachmentPoint_t2909008346_0_0_0 };
extern const Il2CppGenericInst GenInst_InvAttachmentPoint_t2909008346_0_0_0 = { 1, GenInst_InvAttachmentPoint_t2909008346_0_0_0_Types };
extern const Il2CppType AnimationClip_t2318505987_0_0_0;
static const Il2CppType* GenInst_AnimationClip_t2318505987_0_0_0_Types[] = { &AnimationClip_t2318505987_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationClip_t2318505987_0_0_0 = { 1, GenInst_AnimationClip_t2318505987_0_0_0_Types };
extern const Il2CppType Motion_t1110556653_0_0_0;
static const Il2CppType* GenInst_Motion_t1110556653_0_0_0_Types[] = { &Motion_t1110556653_0_0_0 };
extern const Il2CppGenericInst GenInst_Motion_t1110556653_0_0_0 = { 1, GenInst_Motion_t1110556653_0_0_0_Types };
extern const Il2CppType UITweener_t260334902_0_0_0;
static const Il2CppType* GenInst_UITweener_t260334902_0_0_0_Types[] = { &UITweener_t260334902_0_0_0 };
extern const Il2CppGenericInst GenInst_UITweener_t260334902_0_0_0 = { 1, GenInst_UITweener_t260334902_0_0_0_Types };
extern const Il2CppType EventDelegate_t2738326060_0_0_0;
static const Il2CppType* GenInst_EventDelegate_t2738326060_0_0_0_Types[] = { &EventDelegate_t2738326060_0_0_0 };
extern const Il2CppGenericInst GenInst_EventDelegate_t2738326060_0_0_0 = { 1, GenInst_EventDelegate_t2738326060_0_0_0_Types };
extern const Il2CppType FadeEntry_t639421133_0_0_0;
static const Il2CppType* GenInst_FadeEntry_t639421133_0_0_0_Types[] = { &FadeEntry_t639421133_0_0_0 };
extern const Il2CppGenericInst GenInst_FadeEntry_t639421133_0_0_0 = { 1, GenInst_FadeEntry_t639421133_0_0_0_Types };
extern const Il2CppType UIButton_t1100396938_0_0_0;
static const Il2CppType* GenInst_UIButton_t1100396938_0_0_0_Types[] = { &UIButton_t1100396938_0_0_0 };
extern const Il2CppGenericInst GenInst_UIButton_t1100396938_0_0_0 = { 1, GenInst_UIButton_t1100396938_0_0_0_Types };
extern const Il2CppType UIButtonColor_t2038074180_0_0_0;
static const Il2CppType* GenInst_UIButtonColor_t2038074180_0_0_0_Types[] = { &UIButtonColor_t2038074180_0_0_0 };
extern const Il2CppGenericInst GenInst_UIButtonColor_t2038074180_0_0_0 = { 1, GenInst_UIButtonColor_t2038074180_0_0_0_Types };
extern const Il2CppType UIWidgetContainer_t30162560_0_0_0;
static const Il2CppType* GenInst_UIWidgetContainer_t30162560_0_0_0_Types[] = { &UIWidgetContainer_t30162560_0_0_0 };
extern const Il2CppGenericInst GenInst_UIWidgetContainer_t30162560_0_0_0 = { 1, GenInst_UIWidgetContainer_t30162560_0_0_0_Types };
extern const Il2CppType UIDragDropItem_t3922849381_0_0_0;
static const Il2CppType* GenInst_UIDragDropItem_t3922849381_0_0_0_Types[] = { &UIDragDropItem_t3922849381_0_0_0 };
extern const Il2CppGenericInst GenInst_UIDragDropItem_t3922849381_0_0_0 = { 1, GenInst_UIDragDropItem_t3922849381_0_0_0_Types };
extern const Il2CppType UIKeyBinding_t2698445843_0_0_0;
static const Il2CppType* GenInst_UIKeyBinding_t2698445843_0_0_0_Types[] = { &UIKeyBinding_t2698445843_0_0_0 };
extern const Il2CppGenericInst GenInst_UIKeyBinding_t2698445843_0_0_0 = { 1, GenInst_UIKeyBinding_t2698445843_0_0_0_Types };
extern const Il2CppType UIKeyNavigation_t4244956403_0_0_0;
static const Il2CppType* GenInst_UIKeyNavigation_t4244956403_0_0_0_Types[] = { &UIKeyNavigation_t4244956403_0_0_0 };
extern const Il2CppGenericInst GenInst_UIKeyNavigation_t4244956403_0_0_0 = { 1, GenInst_UIKeyNavigation_t4244956403_0_0_0_Types };
extern const Il2CppType UILabel_t3248798549_0_0_0;
static const Il2CppType* GenInst_UILabel_t3248798549_0_0_0_Types[] = { &UILabel_t3248798549_0_0_0 };
extern const Il2CppGenericInst GenInst_UILabel_t3248798549_0_0_0 = { 1, GenInst_UILabel_t3248798549_0_0_0_Types };
extern const Il2CppType UIWidget_t3538521925_0_0_0;
static const Il2CppType* GenInst_UIWidget_t3538521925_0_0_0_Types[] = { &UIWidget_t3538521925_0_0_0 };
extern const Il2CppGenericInst GenInst_UIWidget_t3538521925_0_0_0 = { 1, GenInst_UIWidget_t3538521925_0_0_0_Types };
extern const Il2CppType UIRect_t2875960382_0_0_0;
static const Il2CppType* GenInst_UIRect_t2875960382_0_0_0_Types[] = { &UIRect_t2875960382_0_0_0 };
extern const Il2CppGenericInst GenInst_UIRect_t2875960382_0_0_0 = { 1, GenInst_UIRect_t2875960382_0_0_0_Types };
extern const Il2CppType UIPlaySound_t1382070071_0_0_0;
static const Il2CppType* GenInst_UIPlaySound_t1382070071_0_0_0_Types[] = { &UIPlaySound_t1382070071_0_0_0 };
extern const Il2CppGenericInst GenInst_UIPlaySound_t1382070071_0_0_0 = { 1, GenInst_UIPlaySound_t1382070071_0_0_0_Types };
extern const Il2CppType UIToggle_t4192126258_0_0_0;
static const Il2CppType* GenInst_UIToggle_t4192126258_0_0_0_Types[] = { &UIToggle_t4192126258_0_0_0 };
extern const Il2CppGenericInst GenInst_UIToggle_t4192126258_0_0_0 = { 1, GenInst_UIToggle_t4192126258_0_0_0_Types };
extern const Il2CppType UIScrollView_t1973404950_0_0_0;
static const Il2CppType* GenInst_UIScrollView_t1973404950_0_0_0_Types[] = { &UIScrollView_t1973404950_0_0_0 };
extern const Il2CppGenericInst GenInst_UIScrollView_t1973404950_0_0_0 = { 1, GenInst_UIScrollView_t1973404950_0_0_0_Types };
extern const Il2CppType Bounds_t2266837910_0_0_0;
static const Il2CppType* GenInst_Bounds_t2266837910_0_0_0_Types[] = { &Bounds_t2266837910_0_0_0 };
extern const Il2CppGenericInst GenInst_Bounds_t2266837910_0_0_0 = { 1, GenInst_Bounds_t2266837910_0_0_0_Types };
extern const Il2CppType UIPanel_t1716472341_0_0_0;
static const Il2CppType* GenInst_UIPanel_t1716472341_0_0_0_Types[] = { &UIPanel_t1716472341_0_0_0 };
extern const Il2CppGenericInst GenInst_UIPanel_t1716472341_0_0_0 = { 1, GenInst_UIPanel_t1716472341_0_0_0_Types };
extern const Il2CppType BMGlyph_t3344884546_0_0_0;
static const Il2CppType* GenInst_BMGlyph_t3344884546_0_0_0_Types[] = { &BMGlyph_t3344884546_0_0_0 };
extern const Il2CppGenericInst GenInst_BMGlyph_t3344884546_0_0_0 = { 1, GenInst_BMGlyph_t3344884546_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_BMGlyph_t3344884546_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &BMGlyph_t3344884546_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_BMGlyph_t3344884546_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_BMGlyph_t3344884546_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_BMGlyph_t3344884546_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &BMGlyph_t3344884546_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_BMGlyph_t3344884546_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_BMGlyph_t3344884546_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t336302748_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t336302748_0_0_0_Types[] = { &KeyValuePair_2_t336302748_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t336302748_0_0_0 = { 1, GenInst_KeyValuePair_2_t336302748_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_BMGlyph_t3344884546_0_0_0_KeyValuePair_2_t336302748_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &BMGlyph_t3344884546_0_0_0, &KeyValuePair_2_t336302748_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_BMGlyph_t3344884546_0_0_0_KeyValuePair_2_t336302748_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_BMGlyph_t3344884546_0_0_0_KeyValuePair_2_t336302748_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_BMGlyph_t3344884546_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &BMGlyph_t3344884546_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_BMGlyph_t3344884546_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_BMGlyph_t3344884546_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_BMGlyph_t3344884546_0_0_0_BMGlyph_t3344884546_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &BMGlyph_t3344884546_0_0_0, &BMGlyph_t3344884546_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_BMGlyph_t3344884546_0_0_0_BMGlyph_t3344884546_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_BMGlyph_t3344884546_0_0_0_BMGlyph_t3344884546_0_0_0_Types };
extern const Il2CppType Parameter_t2966927026_0_0_0;
static const Il2CppType* GenInst_Parameter_t2966927026_0_0_0_Types[] = { &Parameter_t2966927026_0_0_0 };
extern const Il2CppGenericInst GenInst_Parameter_t2966927026_0_0_0 = { 1, GenInst_Parameter_t2966927026_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_StringU5BU5D_t1281789340_0_0_0_Types[] = { &String_t_0_0_0, &StringU5BU5D_t1281789340_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_StringU5BU5D_t1281789340_0_0_0 = { 2, GenInst_String_t_0_0_0_StringU5BU5D_t1281789340_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_StringU5BU5D_t1281789340_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &StringU5BU5D_t1281789340_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_StringU5BU5D_t1281789340_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_StringU5BU5D_t1281789340_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3464717806_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3464717806_0_0_0_Types[] = { &KeyValuePair_2_t3464717806_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3464717806_0_0_0 = { 1, GenInst_KeyValuePair_2_t3464717806_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_StringU5BU5D_t1281789340_0_0_0_KeyValuePair_2_t3464717806_0_0_0_Types[] = { &String_t_0_0_0, &StringU5BU5D_t1281789340_0_0_0, &KeyValuePair_2_t3464717806_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_StringU5BU5D_t1281789340_0_0_0_KeyValuePair_2_t3464717806_0_0_0 = { 3, GenInst_String_t_0_0_0_StringU5BU5D_t1281789340_0_0_0_KeyValuePair_2_t3464717806_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_StringU5BU5D_t1281789340_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &StringU5BU5D_t1281789340_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_StringU5BU5D_t1281789340_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_StringU5BU5D_t1281789340_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_StringU5BU5D_t1281789340_0_0_0_StringU5BU5D_t1281789340_0_0_0_Types[] = { &String_t_0_0_0, &StringU5BU5D_t1281789340_0_0_0, &StringU5BU5D_t1281789340_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_StringU5BU5D_t1281789340_0_0_0_StringU5BU5D_t1281789340_0_0_0 = { 3, GenInst_String_t_0_0_0_StringU5BU5D_t1281789340_0_0_0_StringU5BU5D_t1281789340_0_0_0_Types };
extern const Il2CppType KeyCode_t2599294277_0_0_0;
static const Il2CppType* GenInst_KeyCode_t2599294277_0_0_0_Types[] = { &KeyCode_t2599294277_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCode_t2599294277_0_0_0 = { 1, GenInst_KeyCode_t2599294277_0_0_0_Types };
extern const Il2CppType AudioListener_t2734094699_0_0_0;
static const Il2CppType* GenInst_AudioListener_t2734094699_0_0_0_Types[] = { &AudioListener_t2734094699_0_0_0 };
extern const Il2CppGenericInst GenInst_AudioListener_t2734094699_0_0_0 = { 1, GenInst_AudioListener_t2734094699_0_0_0_Types };
extern const Il2CppType UICamera_t1356438871_0_0_0;
static const Il2CppType* GenInst_UICamera_t1356438871_0_0_0_Types[] = { &UICamera_t1356438871_0_0_0 };
extern const Il2CppGenericInst GenInst_UICamera_t1356438871_0_0_0 = { 1, GenInst_UICamera_t1356438871_0_0_0_Types };
extern const Il2CppType UIRoot_t4022971450_0_0_0;
static const Il2CppType* GenInst_UIRoot_t4022971450_0_0_0_Types[] = { &UIRoot_t4022971450_0_0_0 };
extern const Il2CppGenericInst GenInst_UIRoot_t4022971450_0_0_0 = { 1, GenInst_UIRoot_t4022971450_0_0_0_Types };
extern const Il2CppType UIDrawCall_t1293405319_0_0_0;
static const Il2CppType* GenInst_UIDrawCall_t1293405319_0_0_0_Types[] = { &UIDrawCall_t1293405319_0_0_0 };
extern const Il2CppGenericInst GenInst_UIDrawCall_t1293405319_0_0_0 = { 1, GenInst_UIDrawCall_t1293405319_0_0_0_Types };
extern const Il2CppType Int32U5BU5D_t385246372_0_0_0;
static const Il2CppType* GenInst_Int32U5BU5D_t385246372_0_0_0_Types[] = { &Int32U5BU5D_t385246372_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32U5BU5D_t385246372_0_0_0 = { 1, GenInst_Int32U5BU5D_t385246372_0_0_0_Types };
extern const Il2CppType IList_1_t471298240_0_0_0;
static const Il2CppType* GenInst_IList_1_t471298240_0_0_0_Types[] = { &IList_1_t471298240_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t471298240_0_0_0 = { 1, GenInst_IList_1_t471298240_0_0_0_Types };
extern const Il2CppType ICollection_1_t1484130691_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1484130691_0_0_0_Types[] = { &ICollection_1_t1484130691_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1484130691_0_0_0 = { 1, GenInst_ICollection_1_t1484130691_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t1930798642_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t1930798642_0_0_0_Types[] = { &IEnumerable_1_t1930798642_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1930798642_0_0_0 = { 1, GenInst_IEnumerable_1_t1930798642_0_0_0_Types };
extern const Il2CppType UISpriteData_t900308526_0_0_0;
static const Il2CppType* GenInst_UISpriteData_t900308526_0_0_0_Types[] = { &UISpriteData_t900308526_0_0_0 };
extern const Il2CppGenericInst GenInst_UISpriteData_t900308526_0_0_0 = { 1, GenInst_UISpriteData_t900308526_0_0_0_Types };
extern const Il2CppType Sprite_t2895597119_0_0_0;
static const Il2CppType* GenInst_Sprite_t2895597119_0_0_0_Types[] = { &Sprite_t2895597119_0_0_0 };
extern const Il2CppGenericInst GenInst_Sprite_t2895597119_0_0_0 = { 1, GenInst_Sprite_t2895597119_0_0_0_Types };
extern const Il2CppType UISprite_t194114938_0_0_0;
static const Il2CppType* GenInst_UISprite_t194114938_0_0_0_Types[] = { &UISprite_t194114938_0_0_0 };
extern const Il2CppGenericInst GenInst_UISprite_t194114938_0_0_0 = { 1, GenInst_UISprite_t194114938_0_0_0_Types };
extern const Il2CppType UIBasicSprite_t1521297657_0_0_0;
static const Il2CppType* GenInst_UIBasicSprite_t1521297657_0_0_0_Types[] = { &UIBasicSprite_t1521297657_0_0_0 };
extern const Il2CppGenericInst GenInst_UIBasicSprite_t1521297657_0_0_0 = { 1, GenInst_UIBasicSprite_t1521297657_0_0_0_Types };
extern const Il2CppType UIFont_t2766063701_0_0_0;
static const Il2CppType* GenInst_UIFont_t2766063701_0_0_0_Types[] = { &UIFont_t2766063701_0_0_0 };
extern const Il2CppGenericInst GenInst_UIFont_t2766063701_0_0_0 = { 1, GenInst_UIFont_t2766063701_0_0_0_Types };
extern const Il2CppType MouseOrTouch_t3052596533_0_0_0;
static const Il2CppType* GenInst_MouseOrTouch_t3052596533_0_0_0_Types[] = { &MouseOrTouch_t3052596533_0_0_0 };
extern const Il2CppGenericInst GenInst_MouseOrTouch_t3052596533_0_0_0 = { 1, GenInst_MouseOrTouch_t3052596533_0_0_0_Types };
extern const Il2CppType DepthEntry_t628749918_0_0_0;
static const Il2CppType* GenInst_DepthEntry_t628749918_0_0_0_Types[] = { &DepthEntry_t628749918_0_0_0 };
extern const Il2CppGenericInst GenInst_DepthEntry_t628749918_0_0_0 = { 1, GenInst_DepthEntry_t628749918_0_0_0_Types };
extern const Il2CppType BMSymbol_t1586058841_0_0_0;
static const Il2CppType* GenInst_BMSymbol_t1586058841_0_0_0_Types[] = { &BMSymbol_t1586058841_0_0_0 };
extern const Il2CppGenericInst GenInst_BMSymbol_t1586058841_0_0_0 = { 1, GenInst_BMSymbol_t1586058841_0_0_0_Types };
static const Il2CppType* GenInst_Font_t1956802104_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Font_t1956802104_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t1956802104_0_0_0_Int32_t2950945753_0_0_0 = { 2, GenInst_Font_t1956802104_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Font_t1956802104_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Font_t1956802104_0_0_0, &Int32_t2950945753_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t1956802104_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Font_t1956802104_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t956162168_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t956162168_0_0_0_Types[] = { &KeyValuePair_2_t956162168_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t956162168_0_0_0 = { 1, GenInst_KeyValuePair_2_t956162168_0_0_0_Types };
static const Il2CppType* GenInst_Font_t1956802104_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t956162168_0_0_0_Types[] = { &Font_t1956802104_0_0_0, &Int32_t2950945753_0_0_0, &KeyValuePair_2_t956162168_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t1956802104_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t956162168_0_0_0 = { 3, GenInst_Font_t1956802104_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t956162168_0_0_0_Types };
static const Il2CppType* GenInst_Font_t1956802104_0_0_0_Int32_t2950945753_0_0_0_Font_t1956802104_0_0_0_Types[] = { &Font_t1956802104_0_0_0, &Int32_t2950945753_0_0_0, &Font_t1956802104_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t1956802104_0_0_0_Int32_t2950945753_0_0_0_Font_t1956802104_0_0_0 = { 3, GenInst_Font_t1956802104_0_0_0_Int32_t2950945753_0_0_0_Font_t1956802104_0_0_0_Types };
static const Il2CppType* GenInst_Font_t1956802104_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Font_t1956802104_0_0_0, &Int32_t2950945753_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t1956802104_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Font_t1956802104_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types };
extern const Il2CppType Paragraph_t1111063719_0_0_0;
static const Il2CppType* GenInst_Paragraph_t1111063719_0_0_0_Types[] = { &Paragraph_t1111063719_0_0_0 };
extern const Il2CppGenericInst GenInst_Paragraph_t1111063719_0_0_0 = { 1, GenInst_Paragraph_t1111063719_0_0_0_Types };
extern const Il2CppType BetterList_1_t266084037_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_BetterList_1_t266084037_0_0_0_Types[] = { &String_t_0_0_0, &BetterList_1_t266084037_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_BetterList_1_t266084037_0_0_0 = { 2, GenInst_String_t_0_0_0_BetterList_1_t266084037_0_0_0_Types };
static const Il2CppType* GenInst_BetterList_1_t266084037_0_0_0_Types[] = { &BetterList_1_t266084037_0_0_0 };
extern const Il2CppGenericInst GenInst_BetterList_1_t266084037_0_0_0 = { 1, GenInst_BetterList_1_t266084037_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_BetterList_1_t266084037_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &BetterList_1_t266084037_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_BetterList_1_t266084037_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_BetterList_1_t266084037_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2449012503_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2449012503_0_0_0_Types[] = { &KeyValuePair_2_t2449012503_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2449012503_0_0_0 = { 1, GenInst_KeyValuePair_2_t2449012503_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_BetterList_1_t266084037_0_0_0_KeyValuePair_2_t2449012503_0_0_0_Types[] = { &String_t_0_0_0, &BetterList_1_t266084037_0_0_0, &KeyValuePair_2_t2449012503_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_BetterList_1_t266084037_0_0_0_KeyValuePair_2_t2449012503_0_0_0 = { 3, GenInst_String_t_0_0_0_BetterList_1_t266084037_0_0_0_KeyValuePair_2_t2449012503_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_BetterList_1_t266084037_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &BetterList_1_t266084037_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_BetterList_1_t266084037_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_BetterList_1_t266084037_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_BetterList_1_t266084037_0_0_0_BetterList_1_t266084037_0_0_0_Types[] = { &String_t_0_0_0, &BetterList_1_t266084037_0_0_0, &BetterList_1_t266084037_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_BetterList_1_t266084037_0_0_0_BetterList_1_t266084037_0_0_0 = { 3, GenInst_String_t_0_0_0_BetterList_1_t266084037_0_0_0_BetterList_1_t266084037_0_0_0_Types };
extern const Il2CppType Button_t4055032469_0_0_0;
static const Il2CppType* GenInst_Button_t4055032469_0_0_0_Types[] = { &Button_t4055032469_0_0_0 };
extern const Il2CppGenericInst GenInst_Button_t4055032469_0_0_0 = { 1, GenInst_Button_t4055032469_0_0_0_Types };
extern const Il2CppType WireframeBehaviour_t1831066704_0_0_0;
static const Il2CppType* GenInst_WireframeBehaviour_t1831066704_0_0_0_Types[] = { &WireframeBehaviour_t1831066704_0_0_0 };
extern const Il2CppGenericInst GenInst_WireframeBehaviour_t1831066704_0_0_0 = { 1, GenInst_WireframeBehaviour_t1831066704_0_0_0_Types };
extern const Il2CppType AxisTouchButton_t3522881333_0_0_0;
static const Il2CppType* GenInst_AxisTouchButton_t3522881333_0_0_0_Types[] = { &AxisTouchButton_t3522881333_0_0_0 };
extern const Il2CppGenericInst GenInst_AxisTouchButton_t3522881333_0_0_0 = { 1, GenInst_AxisTouchButton_t3522881333_0_0_0_Types };
extern const Il2CppType VirtualAxis_t4087348596_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_VirtualAxis_t4087348596_0_0_0_Types[] = { &String_t_0_0_0, &VirtualAxis_t4087348596_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_VirtualAxis_t4087348596_0_0_0 = { 2, GenInst_String_t_0_0_0_VirtualAxis_t4087348596_0_0_0_Types };
static const Il2CppType* GenInst_VirtualAxis_t4087348596_0_0_0_Types[] = { &VirtualAxis_t4087348596_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualAxis_t4087348596_0_0_0 = { 1, GenInst_VirtualAxis_t4087348596_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_VirtualAxis_t4087348596_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &VirtualAxis_t4087348596_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_VirtualAxis_t4087348596_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_VirtualAxis_t4087348596_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1975309766_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1975309766_0_0_0_Types[] = { &KeyValuePair_2_t1975309766_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1975309766_0_0_0 = { 1, GenInst_KeyValuePair_2_t1975309766_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_VirtualAxis_t4087348596_0_0_0_KeyValuePair_2_t1975309766_0_0_0_Types[] = { &String_t_0_0_0, &VirtualAxis_t4087348596_0_0_0, &KeyValuePair_2_t1975309766_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_VirtualAxis_t4087348596_0_0_0_KeyValuePair_2_t1975309766_0_0_0 = { 3, GenInst_String_t_0_0_0_VirtualAxis_t4087348596_0_0_0_KeyValuePair_2_t1975309766_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_VirtualAxis_t4087348596_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &VirtualAxis_t4087348596_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_VirtualAxis_t4087348596_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_VirtualAxis_t4087348596_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_VirtualAxis_t4087348596_0_0_0_VirtualAxis_t4087348596_0_0_0_Types[] = { &String_t_0_0_0, &VirtualAxis_t4087348596_0_0_0, &VirtualAxis_t4087348596_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_VirtualAxis_t4087348596_0_0_0_VirtualAxis_t4087348596_0_0_0 = { 3, GenInst_String_t_0_0_0_VirtualAxis_t4087348596_0_0_0_VirtualAxis_t4087348596_0_0_0_Types };
extern const Il2CppType VirtualButton_t2756566330_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_VirtualButton_t2756566330_0_0_0_Types[] = { &String_t_0_0_0, &VirtualButton_t2756566330_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_VirtualButton_t2756566330_0_0_0 = { 2, GenInst_String_t_0_0_0_VirtualButton_t2756566330_0_0_0_Types };
static const Il2CppType* GenInst_VirtualButton_t2756566330_0_0_0_Types[] = { &VirtualButton_t2756566330_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButton_t2756566330_0_0_0 = { 1, GenInst_VirtualButton_t2756566330_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_VirtualButton_t2756566330_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &VirtualButton_t2756566330_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_VirtualButton_t2756566330_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_VirtualButton_t2756566330_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t644527500_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t644527500_0_0_0_Types[] = { &KeyValuePair_2_t644527500_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t644527500_0_0_0 = { 1, GenInst_KeyValuePair_2_t644527500_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_VirtualButton_t2756566330_0_0_0_KeyValuePair_2_t644527500_0_0_0_Types[] = { &String_t_0_0_0, &VirtualButton_t2756566330_0_0_0, &KeyValuePair_2_t644527500_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_VirtualButton_t2756566330_0_0_0_KeyValuePair_2_t644527500_0_0_0 = { 3, GenInst_String_t_0_0_0_VirtualButton_t2756566330_0_0_0_KeyValuePair_2_t644527500_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_VirtualButton_t2756566330_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &VirtualButton_t2756566330_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_VirtualButton_t2756566330_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_VirtualButton_t2756566330_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_VirtualButton_t2756566330_0_0_0_VirtualButton_t2756566330_0_0_0_Types[] = { &String_t_0_0_0, &VirtualButton_t2756566330_0_0_0, &VirtualButton_t2756566330_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_VirtualButton_t2756566330_0_0_0_VirtualButton_t2756566330_0_0_0 = { 3, GenInst_String_t_0_0_0_VirtualButton_t2756566330_0_0_0_VirtualButton_t2756566330_0_0_0_Types };
extern const Il2CppType SetAnimInfo_t2127528194_0_0_0;
static const Il2CppType* GenInst_SetAnimInfo_t2127528194_0_0_0_Types[] = { &SetAnimInfo_t2127528194_0_0_0 };
extern const Il2CppGenericInst GenInst_SetAnimInfo_t2127528194_0_0_0 = { 1, GenInst_SetAnimInfo_t2127528194_0_0_0_Types };
extern const Il2CppType Animator_t434523843_0_0_0;
static const Il2CppType* GenInst_Animator_t434523843_0_0_0_Types[] = { &Animator_t434523843_0_0_0 };
extern const Il2CppGenericInst GenInst_Animator_t434523843_0_0_0 = { 1, GenInst_Animator_t434523843_0_0_0_Types };
extern const Il2CppType Level_Controller_t1433348427_0_0_0;
static const Il2CppType* GenInst_Level_Controller_t1433348427_0_0_0_Types[] = { &Level_Controller_t1433348427_0_0_0 };
extern const Il2CppGenericInst GenInst_Level_Controller_t1433348427_0_0_0 = { 1, GenInst_Level_Controller_t1433348427_0_0_0_Types };
extern const Il2CppType Image_t2670269651_0_0_0;
static const Il2CppType* GenInst_Image_t2670269651_0_0_0_Types[] = { &Image_t2670269651_0_0_0 };
extern const Il2CppGenericInst GenInst_Image_t2670269651_0_0_0 = { 1, GenInst_Image_t2670269651_0_0_0_Types };
extern const Il2CppType TweenScale_t2539309033_0_0_0;
static const Il2CppType* GenInst_TweenScale_t2539309033_0_0_0_Types[] = { &TweenScale_t2539309033_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenScale_t2539309033_0_0_0 = { 1, GenInst_TweenScale_t2539309033_0_0_0_Types };
extern const Il2CppType ISerializationCallbackReceiver_t2363941153_0_0_0;
static const Il2CppType* GenInst_ISerializationCallbackReceiver_t2363941153_0_0_0_Types[] = { &ISerializationCallbackReceiver_t2363941153_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializationCallbackReceiver_t2363941153_0_0_0 = { 1, GenInst_ISerializationCallbackReceiver_t2363941153_0_0_0_Types };
extern const Il2CppType ImageTargetBehaviour_t2200418350_0_0_0;
static const Il2CppType* GenInst_ImageTargetBehaviour_t2200418350_0_0_0_Types[] = { &ImageTargetBehaviour_t2200418350_0_0_0 };
extern const Il2CppGenericInst GenInst_ImageTargetBehaviour_t2200418350_0_0_0 = { 1, GenInst_ImageTargetBehaviour_t2200418350_0_0_0_Types };
extern const Il2CppType ImageTargetAbstractBehaviour_t3577513769_0_0_0;
static const Il2CppType* GenInst_ImageTargetAbstractBehaviour_t3577513769_0_0_0_Types[] = { &ImageTargetAbstractBehaviour_t3577513769_0_0_0 };
extern const Il2CppGenericInst GenInst_ImageTargetAbstractBehaviour_t3577513769_0_0_0 = { 1, GenInst_ImageTargetAbstractBehaviour_t3577513769_0_0_0_Types };
extern const Il2CppType ISAWCService_t99629249_0_0_0;
static const Il2CppType* GenInst_ISAWCService_t99629249_0_0_0_Types[] = { &ISAWCService_t99629249_0_0_0 };
extern const Il2CppGenericInst GenInst_ISAWCService_t99629249_0_0_0 = { 1, GenInst_ISAWCService_t99629249_0_0_0_Types };
extern const Il2CppType BT_Mic_t2341817396_0_0_0;
static const Il2CppType* GenInst_BT_Mic_t2341817396_0_0_0_Types[] = { &BT_Mic_t2341817396_0_0_0 };
extern const Il2CppGenericInst GenInst_BT_Mic_t2341817396_0_0_0 = { 1, GenInst_BT_Mic_t2341817396_0_0_0_Types };
extern const Il2CppType Drag_Controller_t1925480236_0_0_0;
static const Il2CppType* GenInst_Drag_Controller_t1925480236_0_0_0_Types[] = { &Drag_Controller_t1925480236_0_0_0 };
extern const Il2CppGenericInst GenInst_Drag_Controller_t1925480236_0_0_0 = { 1, GenInst_Drag_Controller_t1925480236_0_0_0_Types };
extern const Il2CppType Point_Controller_t869836145_0_0_0;
static const Il2CppType* GenInst_Point_Controller_t869836145_0_0_0_Types[] = { &Point_Controller_t869836145_0_0_0 };
extern const Il2CppGenericInst GenInst_Point_Controller_t869836145_0_0_0 = { 1, GenInst_Point_Controller_t869836145_0_0_0_Types };
extern const Il2CppType leve4To2D_t1062310264_0_0_0;
static const Il2CppType* GenInst_leve4To2D_t1062310264_0_0_0_Types[] = { &leve4To2D_t1062310264_0_0_0 };
extern const Il2CppGenericInst GenInst_leve4To2D_t1062310264_0_0_0 = { 1, GenInst_leve4To2D_t1062310264_0_0_0_Types };
extern const Il2CppType SpriteRenderer_t3235626157_0_0_0;
static const Il2CppType* GenInst_SpriteRenderer_t3235626157_0_0_0_Types[] = { &SpriteRenderer_t3235626157_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteRenderer_t3235626157_0_0_0 = { 1, GenInst_SpriteRenderer_t3235626157_0_0_0_Types };
extern const Il2CppType AudioClip_t3680889665_0_0_0;
static const Il2CppType* GenInst_AudioClip_t3680889665_0_0_0_Types[] = { &AudioClip_t3680889665_0_0_0 };
extern const Il2CppGenericInst GenInst_AudioClip_t3680889665_0_0_0 = { 1, GenInst_AudioClip_t3680889665_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t1615002100_gp_0_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t1615002100_gp_0_0_0_0_Types[] = { &IEnumerable_1_t1615002100_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1615002100_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t1615002100_gp_0_0_0_0_Types };
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m2877758246_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2877758246_gp_0_0_0_0_Types[] = { &Array_InternalArray__IEnumerable_GetEnumerator_m2877758246_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2877758246_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2877758246_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3544654705_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3544654705_gp_0_0_0_0_Array_Sort_m3544654705_gp_0_0_0_0_Types[] = { &Array_Sort_m3544654705_gp_0_0_0_0, &Array_Sort_m3544654705_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3544654705_gp_0_0_0_0_Array_Sort_m3544654705_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m3544654705_gp_0_0_0_0_Array_Sort_m3544654705_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m2813331573_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2813331573_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2813331573_gp_0_0_0_0_Array_Sort_m2813331573_gp_1_0_0_0_Types[] = { &Array_Sort_m2813331573_gp_0_0_0_0, &Array_Sort_m2813331573_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2813331573_gp_0_0_0_0_Array_Sort_m2813331573_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m2813331573_gp_0_0_0_0_Array_Sort_m2813331573_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m541372433_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m541372433_gp_0_0_0_0_Types[] = { &Array_Sort_m541372433_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m541372433_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m541372433_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m541372433_gp_0_0_0_0_Array_Sort_m541372433_gp_0_0_0_0_Types[] = { &Array_Sort_m541372433_gp_0_0_0_0, &Array_Sort_m541372433_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m541372433_gp_0_0_0_0_Array_Sort_m541372433_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m541372433_gp_0_0_0_0_Array_Sort_m541372433_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3128382015_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3128382015_gp_0_0_0_0_Types[] = { &Array_Sort_m3128382015_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3128382015_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3128382015_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3128382015_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3128382015_gp_0_0_0_0_Array_Sort_m3128382015_gp_1_0_0_0_Types[] = { &Array_Sort_m3128382015_gp_0_0_0_0, &Array_Sort_m3128382015_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3128382015_gp_0_0_0_0_Array_Sort_m3128382015_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m3128382015_gp_0_0_0_0_Array_Sort_m3128382015_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m2971965616_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2971965616_gp_0_0_0_0_Array_Sort_m2971965616_gp_0_0_0_0_Types[] = { &Array_Sort_m2971965616_gp_0_0_0_0, &Array_Sort_m2971965616_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2971965616_gp_0_0_0_0_Array_Sort_m2971965616_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2971965616_gp_0_0_0_0_Array_Sort_m2971965616_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m2294228957_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2294228957_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2294228957_gp_0_0_0_0_Array_Sort_m2294228957_gp_1_0_0_0_Types[] = { &Array_Sort_m2294228957_gp_0_0_0_0, &Array_Sort_m2294228957_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2294228957_gp_0_0_0_0_Array_Sort_m2294228957_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m2294228957_gp_0_0_0_0_Array_Sort_m2294228957_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m3111895514_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3111895514_gp_0_0_0_0_Types[] = { &Array_Sort_m3111895514_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3111895514_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3111895514_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m3111895514_gp_0_0_0_0_Array_Sort_m3111895514_gp_0_0_0_0_Types[] = { &Array_Sort_m3111895514_gp_0_0_0_0, &Array_Sort_m3111895514_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3111895514_gp_0_0_0_0_Array_Sort_m3111895514_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m3111895514_gp_0_0_0_0_Array_Sort_m3111895514_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m4162554849_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m4162554849_gp_0_0_0_0_Types[] = { &Array_Sort_m4162554849_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m4162554849_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m4162554849_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m4162554849_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m4162554849_gp_1_0_0_0_Types[] = { &Array_Sort_m4162554849_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m4162554849_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m4162554849_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m4162554849_gp_0_0_0_0_Array_Sort_m4162554849_gp_1_0_0_0_Types[] = { &Array_Sort_m4162554849_gp_0_0_0_0, &Array_Sort_m4162554849_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m4162554849_gp_0_0_0_0_Array_Sort_m4162554849_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m4162554849_gp_0_0_0_0_Array_Sort_m4162554849_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m2347552592_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2347552592_gp_0_0_0_0_Types[] = { &Array_Sort_m2347552592_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2347552592_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2347552592_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1584977544_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1584977544_gp_0_0_0_0_Types[] = { &Array_Sort_m1584977544_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1584977544_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1584977544_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m2730016035_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m2730016035_gp_0_0_0_0_Types[] = { &Array_qsort_m2730016035_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m2730016035_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m2730016035_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m2730016035_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m2730016035_gp_0_0_0_0_Array_qsort_m2730016035_gp_1_0_0_0_Types[] = { &Array_qsort_m2730016035_gp_0_0_0_0, &Array_qsort_m2730016035_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m2730016035_gp_0_0_0_0_Array_qsort_m2730016035_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m2730016035_gp_0_0_0_0_Array_qsort_m2730016035_gp_1_0_0_0_Types };
extern const Il2CppType Array_compare_m1571452883_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_compare_m1571452883_gp_0_0_0_0_Types[] = { &Array_compare_m1571452883_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_compare_m1571452883_gp_0_0_0_0 = { 1, GenInst_Array_compare_m1571452883_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m2056624725_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m2056624725_gp_0_0_0_0_Types[] = { &Array_qsort_m2056624725_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m2056624725_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m2056624725_gp_0_0_0_0_Types };
extern const Il2CppType Array_Resize_m148240358_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Resize_m148240358_gp_0_0_0_0_Types[] = { &Array_Resize_m148240358_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Resize_m148240358_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m148240358_gp_0_0_0_0_Types };
extern const Il2CppType Array_TrueForAll_m2898582490_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_TrueForAll_m2898582490_gp_0_0_0_0_Types[] = { &Array_TrueForAll_m2898582490_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m2898582490_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m2898582490_gp_0_0_0_0_Types };
extern const Il2CppType Array_ForEach_m1810936836_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_ForEach_m1810936836_gp_0_0_0_0_Types[] = { &Array_ForEach_m1810936836_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ForEach_m1810936836_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m1810936836_gp_0_0_0_0_Types };
extern const Il2CppType Array_ConvertAll_m3156307665_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m3156307665_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_ConvertAll_m3156307665_gp_0_0_0_0_Array_ConvertAll_m3156307665_gp_1_0_0_0_Types[] = { &Array_ConvertAll_m3156307665_gp_0_0_0_0, &Array_ConvertAll_m3156307665_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m3156307665_gp_0_0_0_0_Array_ConvertAll_m3156307665_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m3156307665_gp_0_0_0_0_Array_ConvertAll_m3156307665_gp_1_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m314143680_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m314143680_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m314143680_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m314143680_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m314143680_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m460178338_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m460178338_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m460178338_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m460178338_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m460178338_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m4020594960_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m4020594960_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m4020594960_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m4020594960_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m4020594960_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m2045802321_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m2045802321_gp_0_0_0_0_Types[] = { &Array_FindIndex_m2045802321_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m2045802321_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m2045802321_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m193711900_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m193711900_gp_0_0_0_0_Types[] = { &Array_FindIndex_m193711900_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m193711900_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m193711900_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m3007915586_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m3007915586_gp_0_0_0_0_Types[] = { &Array_FindIndex_m3007915586_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m3007915586_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m3007915586_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m588894935_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m588894935_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m588894935_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m588894935_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m588894935_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m1820506567_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m1820506567_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m1820506567_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1820506567_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1820506567_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m818786257_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m818786257_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m818786257_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m818786257_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m818786257_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m1137121163_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m1137121163_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m1137121163_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1137121163_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1137121163_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m3665873194_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m3665873194_gp_0_0_0_0_Types[] = { &Array_IndexOf_m3665873194_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m3665873194_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m3665873194_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m751114778_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m751114778_gp_0_0_0_0_Types[] = { &Array_IndexOf_m751114778_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m751114778_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m751114778_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m1878557700_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m1878557700_gp_0_0_0_0_Types[] = { &Array_IndexOf_m1878557700_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m1878557700_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m1878557700_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m1660745251_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m1660745251_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m1660745251_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m1660745251_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m1660745251_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m3843535007_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m3843535007_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m3843535007_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m3843535007_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m3843535007_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m3595565626_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m3595565626_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m3595565626_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m3595565626_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m3595565626_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindAll_m1182922648_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindAll_m1182922648_gp_0_0_0_0_Types[] = { &Array_FindAll_m1182922648_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindAll_m1182922648_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m1182922648_gp_0_0_0_0_Types };
extern const Il2CppType Array_Exists_m1131952887_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Exists_m1131952887_gp_0_0_0_0_Types[] = { &Array_Exists_m1131952887_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Exists_m1131952887_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m1131952887_gp_0_0_0_0_Types };
extern const Il2CppType Array_AsReadOnly_m1239764914_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_AsReadOnly_m1239764914_gp_0_0_0_0_Types[] = { &Array_AsReadOnly_m1239764914_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m1239764914_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m1239764914_gp_0_0_0_0_Types };
extern const Il2CppType Array_Find_m143990730_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Find_m143990730_gp_0_0_0_0_Types[] = { &Array_Find_m143990730_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Find_m143990730_gp_0_0_0_0 = { 1, GenInst_Array_Find_m143990730_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLast_m1741869030_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLast_m1741869030_gp_0_0_0_0_Types[] = { &Array_FindLast_m1741869030_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLast_m1741869030_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m1741869030_gp_0_0_0_0_Types };
extern const Il2CppType InternalEnumerator_1_t2600413744_gp_0_0_0_0;
static const Il2CppType* GenInst_InternalEnumerator_1_t2600413744_gp_0_0_0_0_Types[] = { &InternalEnumerator_1_t2600413744_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t2600413744_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t2600413744_gp_0_0_0_0_Types };
extern const Il2CppType ArrayReadOnlyList_1_t221793636_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayReadOnlyList_1_t221793636_gp_0_0_0_0_Types[] = { &ArrayReadOnlyList_1_t221793636_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t221793636_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t221793636_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t1202911786_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1202911786_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t1202911786_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1202911786_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1202911786_gp_0_0_0_0_Types };
extern const Il2CppType IList_1_t523203890_gp_0_0_0_0;
static const Il2CppType* GenInst_IList_1_t523203890_gp_0_0_0_0_Types[] = { &IList_1_t523203890_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t523203890_gp_0_0_0_0 = { 1, GenInst_IList_1_t523203890_gp_0_0_0_0_Types };
extern const Il2CppType ICollection_1_t1449021101_gp_0_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1449021101_gp_0_0_0_0_Types[] = { &ICollection_1_t1449021101_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1449021101_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t1449021101_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_1_t3772285925_gp_0_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t3772285925_gp_0_0_0_0_Types[] = { &Nullable_1_t3772285925_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t3772285925_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t3772285925_gp_0_0_0_0_Types };
extern const Il2CppType MonoProperty_GetterAdapterFrame_m1909347418_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m1909347418_gp_1_0_0_0;
static const Il2CppType* GenInst_MonoProperty_GetterAdapterFrame_m1909347418_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m1909347418_gp_1_0_0_0_Types[] = { &MonoProperty_GetterAdapterFrame_m1909347418_gp_0_0_0_0, &MonoProperty_GetterAdapterFrame_m1909347418_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m1909347418_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m1909347418_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m1909347418_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m1909347418_gp_1_0_0_0_Types };
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m1636979383_gp_0_0_0_0;
static const Il2CppType* GenInst_MonoProperty_StaticGetterAdapterFrame_m1636979383_gp_0_0_0_0_Types[] = { &MonoProperty_StaticGetterAdapterFrame_m1636979383_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m1636979383_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m1636979383_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t3621973219_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Types[] = { &Dictionary_2_t3621973219_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3621973219_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t3621973219_gp_1_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_Types[] = { &Dictionary_2_t3621973219_gp_0_0_0_0, &Dictionary_2_t3621973219_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t3621973219_gp_1_0_0_0_Types[] = { &Dictionary_2_t3621973219_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3621973219_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t3621973219_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1772072192_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1772072192_0_0_0_Types[] = { &KeyValuePair_2_t1772072192_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1772072192_0_0_0 = { 1, GenInst_KeyValuePair_2_t1772072192_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_CopyTo_m966111031_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m966111031_gp_0_0_0_0_Types[] = { &Dictionary_2_t3621973219_gp_0_0_0_0, &Dictionary_2_t3621973219_gp_1_0_0_0, &Dictionary_2_Do_CopyTo_m966111031_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m966111031_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m966111031_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m842166809_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m842166809_gp_0_0_0_0_Types[] = { &Dictionary_2_t3621973219_gp_0_0_0_0, &Dictionary_2_t3621973219_gp_1_0_0_0, &Dictionary_2_Do_ICollectionCopyTo_m842166809_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m842166809_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m842166809_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m842166809_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &Dictionary_2_Do_ICollectionCopyTo_m842166809_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m842166809_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m842166809_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Dictionary_2_t3621973219_gp_0_0_0_0, &Dictionary_2_t3621973219_gp_1_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType ShimEnumerator_t3154898978_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t3154898978_gp_1_0_0_0;
static const Il2CppType* GenInst_ShimEnumerator_t3154898978_gp_0_0_0_0_ShimEnumerator_t3154898978_gp_1_0_0_0_Types[] = { &ShimEnumerator_t3154898978_gp_0_0_0_0, &ShimEnumerator_t3154898978_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t3154898978_gp_0_0_0_0_ShimEnumerator_t3154898978_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t3154898978_gp_0_0_0_0_ShimEnumerator_t3154898978_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t135598976_gp_0_0_0_0;
extern const Il2CppType Enumerator_t135598976_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t135598976_gp_0_0_0_0_Enumerator_t135598976_gp_1_0_0_0_Types[] = { &Enumerator_t135598976_gp_0_0_0_0, &Enumerator_t135598976_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t135598976_gp_0_0_0_0_Enumerator_t135598976_gp_1_0_0_0 = { 2, GenInst_Enumerator_t135598976_gp_0_0_0_0_Enumerator_t135598976_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1920611820_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1920611820_0_0_0_Types[] = { &KeyValuePair_2_t1920611820_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1920611820_0_0_0 = { 1, GenInst_KeyValuePair_2_t1920611820_0_0_0_Types };
extern const Il2CppType KeyCollection_t4251528776_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t4251528776_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyCollection_t4251528776_gp_0_0_0_0_KeyCollection_t4251528776_gp_1_0_0_0_Types[] = { &KeyCollection_t4251528776_gp_0_0_0_0, &KeyCollection_t4251528776_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t4251528776_gp_0_0_0_0_KeyCollection_t4251528776_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t4251528776_gp_0_0_0_0_KeyCollection_t4251528776_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t4251528776_gp_0_0_0_0_Types[] = { &KeyCollection_t4251528776_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t4251528776_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t4251528776_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t3443476011_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3443476011_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3443476011_gp_0_0_0_0_Enumerator_t3443476011_gp_1_0_0_0_Types[] = { &Enumerator_t3443476011_gp_0_0_0_0, &Enumerator_t3443476011_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3443476011_gp_0_0_0_0_Enumerator_t3443476011_gp_1_0_0_0 = { 2, GenInst_Enumerator_t3443476011_gp_0_0_0_0_Enumerator_t3443476011_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t3443476011_gp_0_0_0_0_Types[] = { &Enumerator_t3443476011_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3443476011_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3443476011_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t4251528776_gp_0_0_0_0_KeyCollection_t4251528776_gp_1_0_0_0_KeyCollection_t4251528776_gp_0_0_0_0_Types[] = { &KeyCollection_t4251528776_gp_0_0_0_0, &KeyCollection_t4251528776_gp_1_0_0_0, &KeyCollection_t4251528776_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t4251528776_gp_0_0_0_0_KeyCollection_t4251528776_gp_1_0_0_0_KeyCollection_t4251528776_gp_0_0_0_0 = { 3, GenInst_KeyCollection_t4251528776_gp_0_0_0_0_KeyCollection_t4251528776_gp_1_0_0_0_KeyCollection_t4251528776_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t4251528776_gp_0_0_0_0_KeyCollection_t4251528776_gp_0_0_0_0_Types[] = { &KeyCollection_t4251528776_gp_0_0_0_0, &KeyCollection_t4251528776_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t4251528776_gp_0_0_0_0_KeyCollection_t4251528776_gp_0_0_0_0 = { 2, GenInst_KeyCollection_t4251528776_gp_0_0_0_0_KeyCollection_t4251528776_gp_0_0_0_0_Types };
extern const Il2CppType ValueCollection_t2327722797_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t2327722797_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueCollection_t2327722797_gp_0_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0_Types[] = { &ValueCollection_t2327722797_gp_0_0_0_0, &ValueCollection_t2327722797_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2327722797_gp_0_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2327722797_gp_0_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2327722797_gp_1_0_0_0_Types[] = { &ValueCollection_t2327722797_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2327722797_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t2327722797_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t1602367158_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1602367158_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1602367158_gp_0_0_0_0_Enumerator_t1602367158_gp_1_0_0_0_Types[] = { &Enumerator_t1602367158_gp_0_0_0_0, &Enumerator_t1602367158_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1602367158_gp_0_0_0_0_Enumerator_t1602367158_gp_1_0_0_0 = { 2, GenInst_Enumerator_t1602367158_gp_0_0_0_0_Enumerator_t1602367158_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t1602367158_gp_1_0_0_0_Types[] = { &Enumerator_t1602367158_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1602367158_gp_1_0_0_0 = { 1, GenInst_Enumerator_t1602367158_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2327722797_gp_0_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0_Types[] = { &ValueCollection_t2327722797_gp_0_0_0_0, &ValueCollection_t2327722797_gp_1_0_0_0, &ValueCollection_t2327722797_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2327722797_gp_0_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t2327722797_gp_0_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2327722797_gp_1_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0_Types[] = { &ValueCollection_t2327722797_gp_1_0_0_0, &ValueCollection_t2327722797_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2327722797_gp_1_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2327722797_gp_1_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t3123975638_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &DictionaryEntry_t3123975638_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3123975638_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 2, GenInst_DictionaryEntry_t3123975638_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_KeyValuePair_2_t1772072192_0_0_0_Types[] = { &Dictionary_2_t3621973219_gp_0_0_0_0, &Dictionary_2_t3621973219_gp_1_0_0_0, &KeyValuePair_2_t1772072192_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_KeyValuePair_2_t1772072192_0_0_0 = { 3, GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_KeyValuePair_2_t1772072192_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1772072192_0_0_0_KeyValuePair_2_t1772072192_0_0_0_Types[] = { &KeyValuePair_2_t1772072192_0_0_0, &KeyValuePair_2_t1772072192_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1772072192_0_0_0_KeyValuePair_2_t1772072192_0_0_0 = { 2, GenInst_KeyValuePair_2_t1772072192_0_0_0_KeyValuePair_2_t1772072192_0_0_0_Types };
extern const Il2CppType IDictionary_2_t3177279192_gp_1_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t3177279192_gp_1_0_0_0_Types[] = { &IDictionary_2_t3177279192_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3177279192_gp_1_0_0_0 = { 1, GenInst_IDictionary_2_t3177279192_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1708549516_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1708549516_0_0_0_Types[] = { &KeyValuePair_2_t1708549516_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1708549516_0_0_0 = { 1, GenInst_KeyValuePair_2_t1708549516_0_0_0_Types };
extern const Il2CppType IDictionary_2_t3177279192_gp_0_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t3177279192_gp_0_0_0_0_IDictionary_2_t3177279192_gp_1_0_0_0_Types[] = { &IDictionary_2_t3177279192_gp_0_0_0_0, &IDictionary_2_t3177279192_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3177279192_gp_0_0_0_0_IDictionary_2_t3177279192_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t3177279192_gp_0_0_0_0_IDictionary_2_t3177279192_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4175610960_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t4175610960_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4175610960_gp_0_0_0_0_KeyValuePair_2_t4175610960_gp_1_0_0_0_Types[] = { &KeyValuePair_2_t4175610960_gp_0_0_0_0, &KeyValuePair_2_t4175610960_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4175610960_gp_0_0_0_0_KeyValuePair_2_t4175610960_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t4175610960_gp_0_0_0_0_KeyValuePair_2_t4175610960_gp_1_0_0_0_Types };
extern const Il2CppType EqualityComparer_1_t1549919139_gp_0_0_0_0;
static const Il2CppType* GenInst_EqualityComparer_1_t1549919139_gp_0_0_0_0_Types[] = { &EqualityComparer_1_t1549919139_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t1549919139_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t1549919139_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t4042948011_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t4042948011_gp_0_0_0_0_Types[] = { &DefaultComparer_t4042948011_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t4042948011_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t4042948011_gp_0_0_0_0_Types };
extern const Il2CppType GenericEqualityComparer_1_t2270490560_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericEqualityComparer_1_t2270490560_gp_0_0_0_0_Types[] = { &GenericEqualityComparer_1_t2270490560_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t2270490560_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t2270490560_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t284568025_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t284568025_gp_0_0_0_0_Types[] = { &List_1_t284568025_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t284568025_gp_0_0_0_0 = { 1, GenInst_List_1_t284568025_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t271486022_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t271486022_gp_0_0_0_0_Types[] = { &Enumerator_t271486022_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t271486022_gp_0_0_0_0 = { 1, GenInst_Enumerator_t271486022_gp_0_0_0_0_Types };
extern const Il2CppType Collection_1_t968317937_gp_0_0_0_0;
static const Il2CppType* GenInst_Collection_1_t968317937_gp_0_0_0_0_Types[] = { &Collection_1_t968317937_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collection_1_t968317937_gp_0_0_0_0 = { 1, GenInst_Collection_1_t968317937_gp_0_0_0_0_Types };
extern const Il2CppType KeyedCollection_2_t2564127707_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyedCollection_2_t2564127707_gp_1_0_0_0_Types[] = { &KeyedCollection_2_t2564127707_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyedCollection_2_t2564127707_gp_1_0_0_0 = { 1, GenInst_KeyedCollection_2_t2564127707_gp_1_0_0_0_Types };
extern const Il2CppType KeyedCollection_2_t2564127707_gp_0_0_0_0;
static const Il2CppType* GenInst_KeyedCollection_2_t2564127707_gp_0_0_0_0_Types[] = { &KeyedCollection_2_t2564127707_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyedCollection_2_t2564127707_gp_0_0_0_0 = { 1, GenInst_KeyedCollection_2_t2564127707_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyedCollection_2_t2564127707_gp_0_0_0_0_KeyedCollection_2_t2564127707_gp_1_0_0_0_Types[] = { &KeyedCollection_2_t2564127707_gp_0_0_0_0, &KeyedCollection_2_t2564127707_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyedCollection_2_t2564127707_gp_0_0_0_0_KeyedCollection_2_t2564127707_gp_1_0_0_0 = { 2, GenInst_KeyedCollection_2_t2564127707_gp_0_0_0_0_KeyedCollection_2_t2564127707_gp_1_0_0_0_Types };
extern const Il2CppType ReadOnlyCollection_1_t2757184810_gp_0_0_0_0;
static const Il2CppType* GenInst_ReadOnlyCollection_1_t2757184810_gp_0_0_0_0_Types[] = { &ReadOnlyCollection_1_t2757184810_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t2757184810_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t2757184810_gp_0_0_0_0_Types };
extern const Il2CppType ArraySegment_1_t3537599374_gp_0_0_0_0;
static const Il2CppType* GenInst_ArraySegment_1_t3537599374_gp_0_0_0_0_Types[] = { &ArraySegment_1_t3537599374_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySegment_1_t3537599374_gp_0_0_0_0 = { 1, GenInst_ArraySegment_1_t3537599374_gp_0_0_0_0_Types };
extern const Il2CppType Comparer_1_t4245720645_gp_0_0_0_0;
static const Il2CppType* GenInst_Comparer_1_t4245720645_gp_0_0_0_0_Types[] = { &Comparer_1_t4245720645_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Comparer_1_t4245720645_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t4245720645_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t3277344064_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t3277344064_gp_0_0_0_0_Types[] = { &DefaultComparer_t3277344064_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t3277344064_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t3277344064_gp_0_0_0_0_Types };
extern const Il2CppType GenericComparer_1_t3581574675_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericComparer_1_t3581574675_gp_0_0_0_0_Types[] = { &GenericComparer_1_t3581574675_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t3581574675_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t3581574675_gp_0_0_0_0_Types };
extern const Il2CppType LinkedList_1_t2189935060_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedList_1_t2189935060_gp_0_0_0_0_Types[] = { &LinkedList_1_t2189935060_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedList_1_t2189935060_gp_0_0_0_0 = { 1, GenInst_LinkedList_1_t2189935060_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t3560274443_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3560274443_gp_0_0_0_0_Types[] = { &Enumerator_t3560274443_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3560274443_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3560274443_gp_0_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t3023898186_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedListNode_1_t3023898186_gp_0_0_0_0_Types[] = { &LinkedListNode_1_t3023898186_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t3023898186_gp_0_0_0_0 = { 1, GenInst_LinkedListNode_1_t3023898186_gp_0_0_0_0_Types };
extern const Il2CppType RBTree_Intern_m195891103_gp_0_0_0_0;
static const Il2CppType* GenInst_RBTree_Intern_m195891103_gp_0_0_0_0_Types[] = { &RBTree_Intern_m195891103_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_RBTree_Intern_m195891103_gp_0_0_0_0 = { 1, GenInst_RBTree_Intern_m195891103_gp_0_0_0_0_Types };
extern const Il2CppType RBTree_Remove_m2673281728_gp_0_0_0_0;
static const Il2CppType* GenInst_RBTree_Remove_m2673281728_gp_0_0_0_0_Types[] = { &RBTree_Remove_m2673281728_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_RBTree_Remove_m2673281728_gp_0_0_0_0 = { 1, GenInst_RBTree_Remove_m2673281728_gp_0_0_0_0_Types };
extern const Il2CppType RBTree_Lookup_m2157084951_gp_0_0_0_0;
static const Il2CppType* GenInst_RBTree_Lookup_m2157084951_gp_0_0_0_0_Types[] = { &RBTree_Lookup_m2157084951_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_RBTree_Lookup_m2157084951_gp_0_0_0_0 = { 1, GenInst_RBTree_Lookup_m2157084951_gp_0_0_0_0_Types };
extern const Il2CppType RBTree_find_key_m367850148_gp_0_0_0_0;
static const Il2CppType* GenInst_RBTree_find_key_m367850148_gp_0_0_0_0_Types[] = { &RBTree_find_key_m367850148_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_RBTree_find_key_m367850148_gp_0_0_0_0 = { 1, GenInst_RBTree_find_key_m367850148_gp_0_0_0_0_Types };
extern const Il2CppType Queue_1_t3112285321_gp_0_0_0_0;
static const Il2CppType* GenInst_Queue_1_t3112285321_gp_0_0_0_0_Types[] = { &Queue_1_t3112285321_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Queue_1_t3112285321_gp_0_0_0_0 = { 1, GenInst_Queue_1_t3112285321_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t3782344991_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3782344991_gp_0_0_0_0_Types[] = { &Enumerator_t3782344991_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3782344991_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3782344991_gp_0_0_0_0_Types };
extern const Il2CppType SortedDictionary_2_t1121010512_gp_0_0_0_0;
static const Il2CppType* GenInst_SortedDictionary_2_t1121010512_gp_0_0_0_0_Types[] = { &SortedDictionary_2_t1121010512_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SortedDictionary_2_t1121010512_gp_0_0_0_0 = { 1, GenInst_SortedDictionary_2_t1121010512_gp_0_0_0_0_Types };
extern const Il2CppType SortedDictionary_2_t1121010512_gp_1_0_0_0;
static const Il2CppType* GenInst_SortedDictionary_2_t1121010512_gp_1_0_0_0_Types[] = { &SortedDictionary_2_t1121010512_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortedDictionary_2_t1121010512_gp_1_0_0_0 = { 1, GenInst_SortedDictionary_2_t1121010512_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_SortedDictionary_2_t1121010512_gp_0_0_0_0_SortedDictionary_2_t1121010512_gp_1_0_0_0_Types[] = { &SortedDictionary_2_t1121010512_gp_0_0_0_0, &SortedDictionary_2_t1121010512_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortedDictionary_2_t1121010512_gp_0_0_0_0_SortedDictionary_2_t1121010512_gp_1_0_0_0 = { 2, GenInst_SortedDictionary_2_t1121010512_gp_0_0_0_0_SortedDictionary_2_t1121010512_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1939139756_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1939139756_0_0_0_Types[] = { &KeyValuePair_2_t1939139756_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1939139756_0_0_0 = { 1, GenInst_KeyValuePair_2_t1939139756_0_0_0_Types };
extern const Il2CppType Node_t3077268875_gp_0_0_0_0;
extern const Il2CppType Node_t3077268875_gp_1_0_0_0;
static const Il2CppType* GenInst_Node_t3077268875_gp_0_0_0_0_Node_t3077268875_gp_1_0_0_0_Types[] = { &Node_t3077268875_gp_0_0_0_0, &Node_t3077268875_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Node_t3077268875_gp_0_0_0_0_Node_t3077268875_gp_1_0_0_0 = { 2, GenInst_Node_t3077268875_gp_0_0_0_0_Node_t3077268875_gp_1_0_0_0_Types };
extern const Il2CppType NodeHelper_t2693378302_gp_0_0_0_0;
static const Il2CppType* GenInst_NodeHelper_t2693378302_gp_0_0_0_0_Types[] = { &NodeHelper_t2693378302_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NodeHelper_t2693378302_gp_0_0_0_0 = { 1, GenInst_NodeHelper_t2693378302_gp_0_0_0_0_Types };
extern const Il2CppType NodeHelper_t2693378302_gp_1_0_0_0;
static const Il2CppType* GenInst_NodeHelper_t2693378302_gp_0_0_0_0_NodeHelper_t2693378302_gp_1_0_0_0_Types[] = { &NodeHelper_t2693378302_gp_0_0_0_0, &NodeHelper_t2693378302_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_NodeHelper_t2693378302_gp_0_0_0_0_NodeHelper_t2693378302_gp_1_0_0_0 = { 2, GenInst_NodeHelper_t2693378302_gp_0_0_0_0_NodeHelper_t2693378302_gp_1_0_0_0_Types };
extern const Il2CppType ValueCollection_t1821823815_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t1821823815_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueCollection_t1821823815_gp_0_0_0_0_ValueCollection_t1821823815_gp_1_0_0_0_Types[] = { &ValueCollection_t1821823815_gp_0_0_0_0, &ValueCollection_t1821823815_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t1821823815_gp_0_0_0_0_ValueCollection_t1821823815_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t1821823815_gp_0_0_0_0_ValueCollection_t1821823815_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t1821823815_gp_1_0_0_0_Types[] = { &ValueCollection_t1821823815_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t1821823815_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t1821823815_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t2847158728_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2847158728_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2847158728_gp_0_0_0_0_Enumerator_t2847158728_gp_1_0_0_0_Types[] = { &Enumerator_t2847158728_gp_0_0_0_0, &Enumerator_t2847158728_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2847158728_gp_0_0_0_0_Enumerator_t2847158728_gp_1_0_0_0 = { 2, GenInst_Enumerator_t2847158728_gp_0_0_0_0_Enumerator_t2847158728_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t2847158728_gp_1_0_0_0_Types[] = { &Enumerator_t2847158728_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2847158728_gp_1_0_0_0 = { 1, GenInst_Enumerator_t2847158728_gp_1_0_0_0_Types };
extern const Il2CppType KeyCollection_t3072514359_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t3072514359_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyCollection_t3072514359_gp_0_0_0_0_KeyCollection_t3072514359_gp_1_0_0_0_Types[] = { &KeyCollection_t3072514359_gp_0_0_0_0, &KeyCollection_t3072514359_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t3072514359_gp_0_0_0_0_KeyCollection_t3072514359_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t3072514359_gp_0_0_0_0_KeyCollection_t3072514359_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t3072514359_gp_0_0_0_0_Types[] = { &KeyCollection_t3072514359_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t3072514359_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t3072514359_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t1065364123_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1065364123_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1065364123_gp_0_0_0_0_Enumerator_t1065364123_gp_1_0_0_0_Types[] = { &Enumerator_t1065364123_gp_0_0_0_0, &Enumerator_t1065364123_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1065364123_gp_0_0_0_0_Enumerator_t1065364123_gp_1_0_0_0 = { 2, GenInst_Enumerator_t1065364123_gp_0_0_0_0_Enumerator_t1065364123_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t1065364123_gp_0_0_0_0_Types[] = { &Enumerator_t1065364123_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1065364123_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1065364123_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t665726594_gp_0_0_0_0;
extern const Il2CppType Enumerator_t665726594_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t665726594_gp_0_0_0_0_Enumerator_t665726594_gp_1_0_0_0_Types[] = { &Enumerator_t665726594_gp_0_0_0_0, &Enumerator_t665726594_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t665726594_gp_0_0_0_0_Enumerator_t665726594_gp_1_0_0_0 = { 2, GenInst_Enumerator_t665726594_gp_0_0_0_0_Enumerator_t665726594_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t353939364_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t353939364_0_0_0_Types[] = { &KeyValuePair_2_t353939364_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t353939364_0_0_0 = { 1, GenInst_KeyValuePair_2_t353939364_0_0_0_Types };
extern const Il2CppType SortedList_2_t3414374367_gp_0_0_0_0;
static const Il2CppType* GenInst_SortedList_2_t3414374367_gp_0_0_0_0_Types[] = { &SortedList_2_t3414374367_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SortedList_2_t3414374367_gp_0_0_0_0 = { 1, GenInst_SortedList_2_t3414374367_gp_0_0_0_0_Types };
extern const Il2CppType SortedList_2_t3414374367_gp_1_0_0_0;
static const Il2CppType* GenInst_SortedList_2_t3414374367_gp_1_0_0_0_Types[] = { &SortedList_2_t3414374367_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortedList_2_t3414374367_gp_1_0_0_0 = { 1, GenInst_SortedList_2_t3414374367_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_SortedList_2_t3414374367_gp_0_0_0_0_SortedList_2_t3414374367_gp_1_0_0_0_Types[] = { &SortedList_2_t3414374367_gp_0_0_0_0, &SortedList_2_t3414374367_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortedList_2_t3414374367_gp_0_0_0_0_SortedList_2_t3414374367_gp_1_0_0_0 = { 2, GenInst_SortedList_2_t3414374367_gp_0_0_0_0_SortedList_2_t3414374367_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t15439248_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t15439248_0_0_0_Types[] = { &KeyValuePair_2_t15439248_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t15439248_0_0_0 = { 1, GenInst_KeyValuePair_2_t15439248_0_0_0_Types };
extern const Il2CppType EnumeratorMode_t4114579086_gp_0_0_0_0;
extern const Il2CppType EnumeratorMode_t4114579086_gp_1_0_0_0;
static const Il2CppType* GenInst_EnumeratorMode_t4114579086_gp_0_0_0_0_EnumeratorMode_t4114579086_gp_1_0_0_0_Types[] = { &EnumeratorMode_t4114579086_gp_0_0_0_0, &EnumeratorMode_t4114579086_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumeratorMode_t4114579086_gp_0_0_0_0_EnumeratorMode_t4114579086_gp_1_0_0_0 = { 2, GenInst_EnumeratorMode_t4114579086_gp_0_0_0_0_EnumeratorMode_t4114579086_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t435247811_gp_0_0_0_0;
extern const Il2CppType Enumerator_t435247811_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t435247811_gp_0_0_0_0_Enumerator_t435247811_gp_1_0_0_0_Types[] = { &Enumerator_t435247811_gp_0_0_0_0, &Enumerator_t435247811_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t435247811_gp_0_0_0_0_Enumerator_t435247811_gp_1_0_0_0 = { 2, GenInst_Enumerator_t435247811_gp_0_0_0_0_Enumerator_t435247811_gp_1_0_0_0_Types };
extern const Il2CppType KeyEnumerator_t158881392_gp_0_0_0_0;
extern const Il2CppType KeyEnumerator_t158881392_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyEnumerator_t158881392_gp_0_0_0_0_KeyEnumerator_t158881392_gp_1_0_0_0_Types[] = { &KeyEnumerator_t158881392_gp_0_0_0_0, &KeyEnumerator_t158881392_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyEnumerator_t158881392_gp_0_0_0_0_KeyEnumerator_t158881392_gp_1_0_0_0 = { 2, GenInst_KeyEnumerator_t158881392_gp_0_0_0_0_KeyEnumerator_t158881392_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyEnumerator_t158881392_gp_0_0_0_0_Types[] = { &KeyEnumerator_t158881392_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyEnumerator_t158881392_gp_0_0_0_0 = { 1, GenInst_KeyEnumerator_t158881392_gp_0_0_0_0_Types };
extern const Il2CppType ValueEnumerator_t3711725945_gp_0_0_0_0;
extern const Il2CppType ValueEnumerator_t3711725945_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueEnumerator_t3711725945_gp_0_0_0_0_ValueEnumerator_t3711725945_gp_1_0_0_0_Types[] = { &ValueEnumerator_t3711725945_gp_0_0_0_0, &ValueEnumerator_t3711725945_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueEnumerator_t3711725945_gp_0_0_0_0_ValueEnumerator_t3711725945_gp_1_0_0_0 = { 2, GenInst_ValueEnumerator_t3711725945_gp_0_0_0_0_ValueEnumerator_t3711725945_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueEnumerator_t3711725945_gp_1_0_0_0_Types[] = { &ValueEnumerator_t3711725945_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueEnumerator_t3711725945_gp_1_0_0_0 = { 1, GenInst_ValueEnumerator_t3711725945_gp_1_0_0_0_Types };
extern const Il2CppType ListKeys_t3727950806_gp_0_0_0_0;
extern const Il2CppType ListKeys_t3727950806_gp_1_0_0_0;
static const Il2CppType* GenInst_ListKeys_t3727950806_gp_0_0_0_0_ListKeys_t3727950806_gp_1_0_0_0_Types[] = { &ListKeys_t3727950806_gp_0_0_0_0, &ListKeys_t3727950806_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ListKeys_t3727950806_gp_0_0_0_0_ListKeys_t3727950806_gp_1_0_0_0 = { 2, GenInst_ListKeys_t3727950806_gp_0_0_0_0_ListKeys_t3727950806_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ListKeys_t3727950806_gp_0_0_0_0_Types[] = { &ListKeys_t3727950806_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ListKeys_t3727950806_gp_0_0_0_0 = { 1, GenInst_ListKeys_t3727950806_gp_0_0_0_0_Types };
extern const Il2CppType GetEnumeratorU3Ec__Iterator2_t3896502338_gp_0_0_0_0;
extern const Il2CppType GetEnumeratorU3Ec__Iterator2_t3896502338_gp_1_0_0_0;
static const Il2CppType* GenInst_GetEnumeratorU3Ec__Iterator2_t3896502338_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator2_t3896502338_gp_1_0_0_0_Types[] = { &GetEnumeratorU3Ec__Iterator2_t3896502338_gp_0_0_0_0, &GetEnumeratorU3Ec__Iterator2_t3896502338_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_GetEnumeratorU3Ec__Iterator2_t3896502338_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator2_t3896502338_gp_1_0_0_0 = { 2, GenInst_GetEnumeratorU3Ec__Iterator2_t3896502338_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator2_t3896502338_gp_1_0_0_0_Types };
extern const Il2CppType ListValues_t4284896590_gp_0_0_0_0;
extern const Il2CppType ListValues_t4284896590_gp_1_0_0_0;
static const Il2CppType* GenInst_ListValues_t4284896590_gp_0_0_0_0_ListValues_t4284896590_gp_1_0_0_0_Types[] = { &ListValues_t4284896590_gp_0_0_0_0, &ListValues_t4284896590_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ListValues_t4284896590_gp_0_0_0_0_ListValues_t4284896590_gp_1_0_0_0 = { 2, GenInst_ListValues_t4284896590_gp_0_0_0_0_ListValues_t4284896590_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ListValues_t4284896590_gp_1_0_0_0_Types[] = { &ListValues_t4284896590_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ListValues_t4284896590_gp_1_0_0_0 = { 1, GenInst_ListValues_t4284896590_gp_1_0_0_0_Types };
extern const Il2CppType GetEnumeratorU3Ec__Iterator3_t3518944829_gp_0_0_0_0;
extern const Il2CppType GetEnumeratorU3Ec__Iterator3_t3518944829_gp_1_0_0_0;
static const Il2CppType* GenInst_GetEnumeratorU3Ec__Iterator3_t3518944829_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator3_t3518944829_gp_1_0_0_0_Types[] = { &GetEnumeratorU3Ec__Iterator3_t3518944829_gp_0_0_0_0, &GetEnumeratorU3Ec__Iterator3_t3518944829_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_GetEnumeratorU3Ec__Iterator3_t3518944829_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator3_t3518944829_gp_1_0_0_0 = { 2, GenInst_GetEnumeratorU3Ec__Iterator3_t3518944829_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator3_t3518944829_gp_1_0_0_0_Types };
extern const Il2CppType GetEnumeratorU3Ec__Iterator0_t3634528505_gp_0_0_0_0;
extern const Il2CppType GetEnumeratorU3Ec__Iterator0_t3634528505_gp_1_0_0_0;
static const Il2CppType* GenInst_GetEnumeratorU3Ec__Iterator0_t3634528505_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator0_t3634528505_gp_1_0_0_0_Types[] = { &GetEnumeratorU3Ec__Iterator0_t3634528505_gp_0_0_0_0, &GetEnumeratorU3Ec__Iterator0_t3634528505_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_GetEnumeratorU3Ec__Iterator0_t3634528505_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator0_t3634528505_gp_1_0_0_0 = { 2, GenInst_GetEnumeratorU3Ec__Iterator0_t3634528505_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator0_t3634528505_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1333058536_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1333058536_0_0_0_Types[] = { &KeyValuePair_2_t1333058536_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1333058536_0_0_0 = { 1, GenInst_KeyValuePair_2_t1333058536_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator1_t3684414104_gp_0_0_0_0;
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator1_t3684414104_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator1_t3684414104_gp_0_0_0_0_U3CGetEnumeratorU3Ec__Iterator1_t3684414104_gp_1_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator1_t3684414104_gp_0_0_0_0, &U3CGetEnumeratorU3Ec__Iterator1_t3684414104_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator1_t3684414104_gp_0_0_0_0_U3CGetEnumeratorU3Ec__Iterator1_t3684414104_gp_1_0_0_0 = { 2, GenInst_U3CGetEnumeratorU3Ec__Iterator1_t3684414104_gp_0_0_0_0_U3CGetEnumeratorU3Ec__Iterator1_t3684414104_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1749556876_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1749556876_0_0_0_Types[] = { &KeyValuePair_2_t1749556876_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1749556876_0_0_0 = { 1, GenInst_KeyValuePair_2_t1749556876_0_0_0_Types };
extern const Il2CppType Stack_1_t1463756442_gp_0_0_0_0;
static const Il2CppType* GenInst_Stack_1_t1463756442_gp_0_0_0_0_Types[] = { &Stack_1_t1463756442_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Stack_1_t1463756442_gp_0_0_0_0 = { 1, GenInst_Stack_1_t1463756442_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2989469293_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2989469293_gp_0_0_0_0_Types[] = { &Enumerator_t2989469293_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2989469293_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2989469293_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Any_m2256625727_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Any_m2256625727_gp_0_0_0_0_Types[] = { &Enumerable_Any_m2256625727_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m2256625727_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m2256625727_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Any_m3644852105_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Any_m3644852105_gp_0_0_0_0_Types[] = { &Enumerable_Any_m3644852105_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m3644852105_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m3644852105_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Any_m3644852105_gp_0_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &Enumerable_Any_m3644852105_gp_0_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m3644852105_gp_0_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_Enumerable_Any_m3644852105_gp_0_0_0_0_Boolean_t97287965_0_0_0_Types };
extern const Il2CppType Enumerable_Cast_m3341121705_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Cast_m3341121705_gp_0_0_0_0_Types[] = { &Enumerable_Cast_m3341121705_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Cast_m3341121705_gp_0_0_0_0 = { 1, GenInst_Enumerable_Cast_m3341121705_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateCastIterator_m1371803983_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateCastIterator_m1371803983_gp_0_0_0_0_Types[] = { &Enumerable_CreateCastIterator_m1371803983_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateCastIterator_m1371803983_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateCastIterator_m1371803983_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Contains_m4222801258_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Contains_m4222801258_gp_0_0_0_0_Types[] = { &Enumerable_Contains_m4222801258_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m4222801258_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m4222801258_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Contains_m1710981724_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Contains_m1710981724_gp_0_0_0_0_Types[] = { &Enumerable_Contains_m1710981724_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m1710981724_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m1710981724_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Empty_m821348200_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Empty_m821348200_gp_0_0_0_0_Types[] = { &Enumerable_Empty_m821348200_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Empty_m821348200_gp_0_0_0_0 = { 1, GenInst_Enumerable_Empty_m821348200_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_First_m1478900696_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_First_m1478900696_gp_0_0_0_0_Types[] = { &Enumerable_First_m1478900696_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m1478900696_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m1478900696_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_First_m1478900696_gp_0_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &Enumerable_First_m1478900696_gp_0_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m1478900696_gp_0_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_Enumerable_First_m1478900696_gp_0_0_0_0_Boolean_t97287965_0_0_0_Types };
extern const Il2CppType Enumerable_First_m2847869424_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_First_m2847869424_gp_0_0_0_0_Types[] = { &Enumerable_First_m2847869424_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m2847869424_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m2847869424_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_First_m2138099720_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_First_m2138099720_gp_0_0_0_0_Types[] = { &Enumerable_First_m2138099720_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m2138099720_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m2138099720_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_First_m2138099720_gp_0_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &Enumerable_First_m2138099720_gp_0_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m2138099720_gp_0_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_Enumerable_First_m2138099720_gp_0_0_0_0_Boolean_t97287965_0_0_0_Types };
extern const Il2CppType Enumerable_FirstOrDefault_m946660525_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_FirstOrDefault_m946660525_gp_0_0_0_0_Types[] = { &Enumerable_FirstOrDefault_m946660525_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_FirstOrDefault_m946660525_gp_0_0_0_0 = { 1, GenInst_Enumerable_FirstOrDefault_m946660525_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_FirstOrDefault_m946660525_gp_0_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &Enumerable_FirstOrDefault_m946660525_gp_0_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_FirstOrDefault_m946660525_gp_0_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_Enumerable_FirstOrDefault_m946660525_gp_0_0_0_0_Boolean_t97287965_0_0_0_Types };
extern const Il2CppType Enumerable_Last_m3038740421_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Last_m3038740421_gp_0_0_0_0_Types[] = { &Enumerable_Last_m3038740421_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Last_m3038740421_gp_0_0_0_0 = { 1, GenInst_Enumerable_Last_m3038740421_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Last_m3038740421_gp_0_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &Enumerable_Last_m3038740421_gp_0_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Last_m3038740421_gp_0_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_Enumerable_Last_m3038740421_gp_0_0_0_0_Boolean_t97287965_0_0_0_Types };
extern const Il2CppType Enumerable_LastOrDefault_m707763884_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_LastOrDefault_m707763884_gp_0_0_0_0_Types[] = { &Enumerable_LastOrDefault_m707763884_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_LastOrDefault_m707763884_gp_0_0_0_0 = { 1, GenInst_Enumerable_LastOrDefault_m707763884_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_LastOrDefault_m707763884_gp_0_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &Enumerable_LastOrDefault_m707763884_gp_0_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_LastOrDefault_m707763884_gp_0_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_Enumerable_LastOrDefault_m707763884_gp_0_0_0_0_Boolean_t97287965_0_0_0_Types };
extern const Il2CppType Enumerable_ToArray_m1974389789_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToArray_m1974389789_gp_0_0_0_0_Types[] = { &Enumerable_ToArray_m1974389789_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToArray_m1974389789_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToArray_m1974389789_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToList_m2780466421_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToList_m2780466421_gp_0_0_0_0_Types[] = { &Enumerable_ToList_m2780466421_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToList_m2780466421_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToList_m2780466421_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Where_m88697338_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Where_m88697338_gp_0_0_0_0_Types[] = { &Enumerable_Where_m88697338_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m88697338_gp_0_0_0_0 = { 1, GenInst_Enumerable_Where_m88697338_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Where_m88697338_gp_0_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &Enumerable_Where_m88697338_gp_0_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m88697338_gp_0_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_Enumerable_Where_m88697338_gp_0_0_0_0_Boolean_t97287965_0_0_0_Types };
extern const Il2CppType Enumerable_CreateWhereIterator_m3238110963_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m3238110963_gp_0_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m3238110963_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m3238110963_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateWhereIterator_m3238110963_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m3238110963_gp_0_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m3238110963_gp_0_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m3238110963_gp_0_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_Enumerable_CreateWhereIterator_m3238110963_gp_0_0_0_0_Boolean_t97287965_0_0_0_Types };
extern const Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t373619397_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t373619397_gp_0_0_0_0_Types[] = { &U3CCreateCastIteratorU3Ec__Iterator0_1_t373619397_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t373619397_gp_0_0_0_0 = { 1, GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t373619397_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t945640688_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t945640688_gp_0_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t945640688_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t945640688_gp_0_0_0_0 = { 1, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t945640688_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t945640688_gp_0_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t945640688_gp_0_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t945640688_gp_0_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t945640688_gp_0_0_0_0_Boolean_t97287965_0_0_0_Types };
extern const Il2CppType HashSet_1_t743387557_gp_0_0_0_0;
static const Il2CppType* GenInst_HashSet_1_t743387557_gp_0_0_0_0_Types[] = { &HashSet_1_t743387557_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_HashSet_1_t743387557_gp_0_0_0_0 = { 1, GenInst_HashSet_1_t743387557_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t3836401716_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3836401716_gp_0_0_0_0_Types[] = { &Enumerator_t3836401716_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3836401716_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3836401716_gp_0_0_0_0_Types };
extern const Il2CppType PrimeHelper_t2385147435_gp_0_0_0_0;
static const Il2CppType* GenInst_PrimeHelper_t2385147435_gp_0_0_0_0_Types[] = { &PrimeHelper_t2385147435_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PrimeHelper_t2385147435_gp_0_0_0_0 = { 1, GenInst_PrimeHelper_t2385147435_gp_0_0_0_0_Types };
extern const Il2CppType AppResourcesLengthComparer_1_t4191904486_gp_0_0_0_0;
static const Il2CppType* GenInst_AppResourcesLengthComparer_1_t4191904486_gp_0_0_0_0_Types[] = { &AppResourcesLengthComparer_1_t4191904486_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AppResourcesLengthComparer_1_t4191904486_gp_0_0_0_0 = { 1, GenInst_AppResourcesLengthComparer_1_t4191904486_gp_0_0_0_0_Types };
extern const Il2CppType GenericBuildProvider_1_t2468417315_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericBuildProvider_1_t2468417315_gp_0_0_0_0_Types[] = { &GenericBuildProvider_1_t2468417315_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericBuildProvider_1_t2468417315_gp_0_0_0_0 = { 1, GenInst_GenericBuildProvider_1_t2468417315_gp_0_0_0_0_Types };
extern const Il2CppType SecurityTokenRequirement_GetProperty_m2382599284_gp_0_0_0_0;
static const Il2CppType* GenInst_SecurityTokenRequirement_GetProperty_m2382599284_gp_0_0_0_0_Types[] = { &SecurityTokenRequirement_GetProperty_m2382599284_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SecurityTokenRequirement_GetProperty_m2382599284_gp_0_0_0_0 = { 1, GenInst_SecurityTokenRequirement_GetProperty_m2382599284_gp_0_0_0_0_Types };
extern const Il2CppType AndroidJavaObject_Call_m2209455074_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidJavaObject_Call_m2209455074_gp_0_0_0_0_Types[] = { &AndroidJavaObject_Call_m2209455074_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJavaObject_Call_m2209455074_gp_0_0_0_0 = { 1, GenInst_AndroidJavaObject_Call_m2209455074_gp_0_0_0_0_Types };
extern const Il2CppType AndroidJavaObject_CallStatic_m3993809864_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidJavaObject_CallStatic_m3993809864_gp_0_0_0_0_Types[] = { &AndroidJavaObject_CallStatic_m3993809864_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJavaObject_CallStatic_m3993809864_gp_0_0_0_0 = { 1, GenInst_AndroidJavaObject_CallStatic_m3993809864_gp_0_0_0_0_Types };
extern const Il2CppType AndroidJavaObject__Call_m113691125_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidJavaObject__Call_m113691125_gp_0_0_0_0_Types[] = { &AndroidJavaObject__Call_m113691125_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJavaObject__Call_m113691125_gp_0_0_0_0 = { 1, GenInst_AndroidJavaObject__Call_m113691125_gp_0_0_0_0_Types };
extern const Il2CppType AndroidJavaObject__CallStatic_m109308273_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidJavaObject__CallStatic_m109308273_gp_0_0_0_0_Types[] = { &AndroidJavaObject__CallStatic_m109308273_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJavaObject__CallStatic_m109308273_gp_0_0_0_0 = { 1, GenInst_AndroidJavaObject__CallStatic_m109308273_gp_0_0_0_0_Types };
extern const Il2CppType AndroidJNIHelper_ConvertFromJNIArray_m3095982117_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidJNIHelper_ConvertFromJNIArray_m3095982117_gp_0_0_0_0_Types[] = { &AndroidJNIHelper_ConvertFromJNIArray_m3095982117_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJNIHelper_ConvertFromJNIArray_m3095982117_gp_0_0_0_0 = { 1, GenInst_AndroidJNIHelper_ConvertFromJNIArray_m3095982117_gp_0_0_0_0_Types };
extern const Il2CppType AndroidJNIHelper_GetMethodID_m1845802273_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidJNIHelper_GetMethodID_m1845802273_gp_0_0_0_0_Types[] = { &AndroidJNIHelper_GetMethodID_m1845802273_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJNIHelper_GetMethodID_m1845802273_gp_0_0_0_0 = { 1, GenInst_AndroidJNIHelper_GetMethodID_m1845802273_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentInChildren_m1404611044_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentInChildren_m1404611044_gp_0_0_0_0_Types[] = { &Component_GetComponentInChildren_m1404611044_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentInChildren_m1404611044_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentInChildren_m1404611044_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m3615474666_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m3615474666_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m3615474666_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m3615474666_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m3615474666_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m933673001_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m933673001_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m933673001_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m933673001_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m933673001_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m4005089511_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m4005089511_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m4005089511_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m4005089511_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m4005089511_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m1915630048_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m1915630048_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m1915630048_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m1915630048_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m1915630048_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m555240590_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m555240590_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m555240590_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m555240590_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m555240590_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m2569671904_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m2569671904_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m2569671904_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m2569671904_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m2569671904_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m149672228_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m149672228_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m149672228_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m149672228_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m149672228_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m3598713100_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m3598713100_gp_0_0_0_0_Types[] = { &Component_GetComponents_m3598713100_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m3598713100_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m3598713100_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m3914262141_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m3914262141_gp_0_0_0_0_Types[] = { &Component_GetComponents_m3914262141_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m3914262141_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m3914262141_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentInChildren_m916317014_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentInChildren_m916317014_gp_0_0_0_0_Types[] = { &GameObject_GetComponentInChildren_m916317014_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentInChildren_m916317014_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentInChildren_m916317014_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponents_m2386781050_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponents_m2386781050_gp_0_0_0_0_Types[] = { &GameObject_GetComponents_m2386781050_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m2386781050_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponents_m2386781050_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m906810621_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m906810621_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m906810621_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m906810621_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m906810621_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m1887210244_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m1887210244_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m1887210244_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m1887210244_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m1887210244_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInParent_m241541908_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInParent_m241541908_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInParent_m241541908_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m241541908_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m241541908_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_GetAllocArrayFromChannel_m2078183201_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_GetAllocArrayFromChannel_m2078183201_gp_0_0_0_0_Types[] = { &Mesh_GetAllocArrayFromChannel_m2078183201_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_GetAllocArrayFromChannel_m2078183201_gp_0_0_0_0 = { 1, GenInst_Mesh_GetAllocArrayFromChannel_m2078183201_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SafeLength_m1325240367_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SafeLength_m1325240367_gp_0_0_0_0_Types[] = { &Mesh_SafeLength_m1325240367_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SafeLength_m1325240367_gp_0_0_0_0 = { 1, GenInst_Mesh_SafeLength_m1325240367_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetListForChannel_m462664139_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetListForChannel_m462664139_gp_0_0_0_0_Types[] = { &Mesh_SetListForChannel_m462664139_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m462664139_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m462664139_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetListForChannel_m631926134_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetListForChannel_m631926134_gp_0_0_0_0_Types[] = { &Mesh_SetListForChannel_m631926134_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m631926134_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m631926134_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetUvsImpl_m1664867204_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetUvsImpl_m1664867204_gp_0_0_0_0_Types[] = { &Mesh_SetUvsImpl_m1664867204_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetUvsImpl_m1664867204_gp_0_0_0_0 = { 1, GenInst_Mesh_SetUvsImpl_m1664867204_gp_0_0_0_0_Types };
extern const Il2CppType Object_Instantiate_m1874094322_gp_0_0_0_0;
static const Il2CppType* GenInst_Object_Instantiate_m1874094322_gp_0_0_0_0_Types[] = { &Object_Instantiate_m1874094322_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_Instantiate_m1874094322_gp_0_0_0_0 = { 1, GenInst_Object_Instantiate_m1874094322_gp_0_0_0_0_Types };
extern const Il2CppType Object_FindObjectsOfType_m1824391917_gp_0_0_0_0;
static const Il2CppType* GenInst_Object_FindObjectsOfType_m1824391917_gp_0_0_0_0_Types[] = { &Object_FindObjectsOfType_m1824391917_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_FindObjectsOfType_m1824391917_gp_0_0_0_0 = { 1, GenInst_Object_FindObjectsOfType_m1824391917_gp_0_0_0_0_Types };
extern const Il2CppType _AndroidJNIHelper_GetMethodID_m1668244749_gp_0_0_0_0;
static const Il2CppType* GenInst__AndroidJNIHelper_GetMethodID_m1668244749_gp_0_0_0_0_Types[] = { &_AndroidJNIHelper_GetMethodID_m1668244749_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst__AndroidJNIHelper_GetMethodID_m1668244749_gp_0_0_0_0 = { 1, GenInst__AndroidJNIHelper_GetMethodID_m1668244749_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_1_t3865199217_gp_0_0_0_0;
static const Il2CppType* GenInst_InvokableCall_1_t3865199217_gp_0_0_0_0_Types[] = { &InvokableCall_1_t3865199217_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_1_t3865199217_gp_0_0_0_0 = { 1, GenInst_InvokableCall_1_t3865199217_gp_0_0_0_0_Types };
extern const Il2CppType UnityAction_1_t802700511_0_0_0;
static const Il2CppType* GenInst_UnityAction_1_t802700511_0_0_0_Types[] = { &UnityAction_1_t802700511_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityAction_1_t802700511_0_0_0 = { 1, GenInst_UnityAction_1_t802700511_0_0_0_Types };
extern const Il2CppType InvokableCall_2_t3865133681_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t3865133681_gp_1_0_0_0;
static const Il2CppType* GenInst_InvokableCall_2_t3865133681_gp_0_0_0_0_InvokableCall_2_t3865133681_gp_1_0_0_0_Types[] = { &InvokableCall_2_t3865133681_gp_0_0_0_0, &InvokableCall_2_t3865133681_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3865133681_gp_0_0_0_0_InvokableCall_2_t3865133681_gp_1_0_0_0 = { 2, GenInst_InvokableCall_2_t3865133681_gp_0_0_0_0_InvokableCall_2_t3865133681_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t3865133681_gp_0_0_0_0_Types[] = { &InvokableCall_2_t3865133681_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3865133681_gp_0_0_0_0 = { 1, GenInst_InvokableCall_2_t3865133681_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t3865133681_gp_1_0_0_0_Types[] = { &InvokableCall_2_t3865133681_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3865133681_gp_1_0_0_0 = { 1, GenInst_InvokableCall_2_t3865133681_gp_1_0_0_0_Types };
extern const Il2CppType InvokableCall_3_t3865068145_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t3865068145_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t3865068145_gp_2_0_0_0;
static const Il2CppType* GenInst_InvokableCall_3_t3865068145_gp_0_0_0_0_InvokableCall_3_t3865068145_gp_1_0_0_0_InvokableCall_3_t3865068145_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3865068145_gp_0_0_0_0, &InvokableCall_3_t3865068145_gp_1_0_0_0, &InvokableCall_3_t3865068145_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3865068145_gp_0_0_0_0_InvokableCall_3_t3865068145_gp_1_0_0_0_InvokableCall_3_t3865068145_gp_2_0_0_0 = { 3, GenInst_InvokableCall_3_t3865068145_gp_0_0_0_0_InvokableCall_3_t3865068145_gp_1_0_0_0_InvokableCall_3_t3865068145_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3865068145_gp_0_0_0_0_Types[] = { &InvokableCall_3_t3865068145_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3865068145_gp_0_0_0_0 = { 1, GenInst_InvokableCall_3_t3865068145_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3865068145_gp_1_0_0_0_Types[] = { &InvokableCall_3_t3865068145_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3865068145_gp_1_0_0_0 = { 1, GenInst_InvokableCall_3_t3865068145_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3865068145_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3865068145_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3865068145_gp_2_0_0_0 = { 1, GenInst_InvokableCall_3_t3865068145_gp_2_0_0_0_Types };
extern const Il2CppType InvokableCall_4_t3865002609_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t3865002609_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t3865002609_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t3865002609_gp_3_0_0_0;
static const Il2CppType* GenInst_InvokableCall_4_t3865002609_gp_0_0_0_0_InvokableCall_4_t3865002609_gp_1_0_0_0_InvokableCall_4_t3865002609_gp_2_0_0_0_InvokableCall_4_t3865002609_gp_3_0_0_0_Types[] = { &InvokableCall_4_t3865002609_gp_0_0_0_0, &InvokableCall_4_t3865002609_gp_1_0_0_0, &InvokableCall_4_t3865002609_gp_2_0_0_0, &InvokableCall_4_t3865002609_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3865002609_gp_0_0_0_0_InvokableCall_4_t3865002609_gp_1_0_0_0_InvokableCall_4_t3865002609_gp_2_0_0_0_InvokableCall_4_t3865002609_gp_3_0_0_0 = { 4, GenInst_InvokableCall_4_t3865002609_gp_0_0_0_0_InvokableCall_4_t3865002609_gp_1_0_0_0_InvokableCall_4_t3865002609_gp_2_0_0_0_InvokableCall_4_t3865002609_gp_3_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3865002609_gp_0_0_0_0_Types[] = { &InvokableCall_4_t3865002609_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3865002609_gp_0_0_0_0 = { 1, GenInst_InvokableCall_4_t3865002609_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3865002609_gp_1_0_0_0_Types[] = { &InvokableCall_4_t3865002609_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3865002609_gp_1_0_0_0 = { 1, GenInst_InvokableCall_4_t3865002609_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3865002609_gp_2_0_0_0_Types[] = { &InvokableCall_4_t3865002609_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3865002609_gp_2_0_0_0 = { 1, GenInst_InvokableCall_4_t3865002609_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3865002609_gp_3_0_0_0_Types[] = { &InvokableCall_4_t3865002609_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3865002609_gp_3_0_0_0 = { 1, GenInst_InvokableCall_4_t3865002609_gp_3_0_0_0_Types };
extern const Il2CppType CachedInvokableCall_1_t3153979999_gp_0_0_0_0;
static const Il2CppType* GenInst_CachedInvokableCall_1_t3153979999_gp_0_0_0_0_Types[] = { &CachedInvokableCall_1_t3153979999_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t3153979999_gp_0_0_0_0 = { 1, GenInst_CachedInvokableCall_1_t3153979999_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_1_t74220259_gp_0_0_0_0;
static const Il2CppType* GenInst_UnityEvent_1_t74220259_gp_0_0_0_0_Types[] = { &UnityEvent_1_t74220259_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_1_t74220259_gp_0_0_0_0 = { 1, GenInst_UnityEvent_1_t74220259_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_2_t477504786_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t477504786_gp_1_0_0_0;
static const Il2CppType* GenInst_UnityEvent_2_t477504786_gp_0_0_0_0_UnityEvent_2_t477504786_gp_1_0_0_0_Types[] = { &UnityEvent_2_t477504786_gp_0_0_0_0, &UnityEvent_2_t477504786_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_2_t477504786_gp_0_0_0_0_UnityEvent_2_t477504786_gp_1_0_0_0 = { 2, GenInst_UnityEvent_2_t477504786_gp_0_0_0_0_UnityEvent_2_t477504786_gp_1_0_0_0_Types };
extern const Il2CppType UnityEvent_3_t3206388141_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t3206388141_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t3206388141_gp_2_0_0_0;
static const Il2CppType* GenInst_UnityEvent_3_t3206388141_gp_0_0_0_0_UnityEvent_3_t3206388141_gp_1_0_0_0_UnityEvent_3_t3206388141_gp_2_0_0_0_Types[] = { &UnityEvent_3_t3206388141_gp_0_0_0_0, &UnityEvent_3_t3206388141_gp_1_0_0_0, &UnityEvent_3_t3206388141_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_3_t3206388141_gp_0_0_0_0_UnityEvent_3_t3206388141_gp_1_0_0_0_UnityEvent_3_t3206388141_gp_2_0_0_0 = { 3, GenInst_UnityEvent_3_t3206388141_gp_0_0_0_0_UnityEvent_3_t3206388141_gp_1_0_0_0_UnityEvent_3_t3206388141_gp_2_0_0_0_Types };
extern const Il2CppType UnityEvent_4_t3609672668_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t3609672668_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t3609672668_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t3609672668_gp_3_0_0_0;
static const Il2CppType* GenInst_UnityEvent_4_t3609672668_gp_0_0_0_0_UnityEvent_4_t3609672668_gp_1_0_0_0_UnityEvent_4_t3609672668_gp_2_0_0_0_UnityEvent_4_t3609672668_gp_3_0_0_0_Types[] = { &UnityEvent_4_t3609672668_gp_0_0_0_0, &UnityEvent_4_t3609672668_gp_1_0_0_0, &UnityEvent_4_t3609672668_gp_2_0_0_0, &UnityEvent_4_t3609672668_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_4_t3609672668_gp_0_0_0_0_UnityEvent_4_t3609672668_gp_1_0_0_0_UnityEvent_4_t3609672668_gp_2_0_0_0_UnityEvent_4_t3609672668_gp_3_0_0_0 = { 4, GenInst_UnityEvent_4_t3609672668_gp_0_0_0_0_UnityEvent_4_t3609672668_gp_1_0_0_0_UnityEvent_4_t3609672668_gp_2_0_0_0_UnityEvent_4_t3609672668_gp_3_0_0_0_Types };
extern const Il2CppType DelegateHelper_InvokeWithExceptionHandling_m3762946312_gp_0_0_0_0;
static const Il2CppType* GenInst_DelegateHelper_InvokeWithExceptionHandling_m3762946312_gp_0_0_0_0_Types[] = { &DelegateHelper_InvokeWithExceptionHandling_m3762946312_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DelegateHelper_InvokeWithExceptionHandling_m3762946312_gp_0_0_0_0 = { 1, GenInst_DelegateHelper_InvokeWithExceptionHandling_m3762946312_gp_0_0_0_0_Types };
extern const Il2CppType DelegateHelper_InvokeWithExceptionHandling_m2366197299_gp_0_0_0_0;
extern const Il2CppType DelegateHelper_InvokeWithExceptionHandling_m2366197299_gp_1_0_0_0;
static const Il2CppType* GenInst_DelegateHelper_InvokeWithExceptionHandling_m2366197299_gp_0_0_0_0_DelegateHelper_InvokeWithExceptionHandling_m2366197299_gp_1_0_0_0_Types[] = { &DelegateHelper_InvokeWithExceptionHandling_m2366197299_gp_0_0_0_0, &DelegateHelper_InvokeWithExceptionHandling_m2366197299_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_DelegateHelper_InvokeWithExceptionHandling_m2366197299_gp_0_0_0_0_DelegateHelper_InvokeWithExceptionHandling_m2366197299_gp_1_0_0_0 = { 2, GenInst_DelegateHelper_InvokeWithExceptionHandling_m2366197299_gp_0_0_0_0_DelegateHelper_InvokeWithExceptionHandling_m2366197299_gp_1_0_0_0_Types };
extern const Il2CppType SmartTerrainBuilderImpl_CreateReconstruction_m3375019273_gp_0_0_0_0;
static const Il2CppType* GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m3375019273_gp_0_0_0_0_Types[] = { &SmartTerrainBuilderImpl_CreateReconstruction_m3375019273_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m3375019273_gp_0_0_0_0 = { 1, GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m3375019273_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t350623716_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t350623716_gp_0_0_0_0_Types[] = { &List_1_t350623716_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t350623716_gp_0_0_0_0 = { 1, GenInst_List_1_t350623716_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator6_t3713055553_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator6_t3713055553_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator6_t3713055553_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator6_t3713055553_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator6_t3713055553_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t3155882222_0_0_0;
static const Il2CppType* GenInst_List_1_t3155882222_0_0_0_Types[] = { &List_1_t3155882222_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3155882222_0_0_0 = { 1, GenInst_List_1_t3155882222_0_0_0_Types };
extern const Il2CppType KeyedByTypeCollection_1_t3023398834_gp_0_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_KeyedByTypeCollection_1_t3023398834_gp_0_0_0_0_Types[] = { &Type_t_0_0_0, &KeyedByTypeCollection_1_t3023398834_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_KeyedByTypeCollection_1_t3023398834_gp_0_0_0_0 = { 2, GenInst_Type_t_0_0_0_KeyedByTypeCollection_1_t3023398834_gp_0_0_0_0_Types };
extern const Il2CppType KeyedByTypeCollection_1_FindAll_m3460713529_gp_0_0_0_0;
static const Il2CppType* GenInst_KeyedByTypeCollection_1_FindAll_m3460713529_gp_0_0_0_0_Types[] = { &KeyedByTypeCollection_1_FindAll_m3460713529_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyedByTypeCollection_1_FindAll_m3460713529_gp_0_0_0_0 = { 1, GenInst_KeyedByTypeCollection_1_FindAll_m3460713529_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyedByTypeCollection_1_t3023398834_gp_0_0_0_0_Types[] = { &KeyedByTypeCollection_1_t3023398834_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyedByTypeCollection_1_t3023398834_gp_0_0_0_0 = { 1, GenInst_KeyedByTypeCollection_1_t3023398834_gp_0_0_0_0_Types };
extern const Il2CppType SynchronizedCollection_1_t2430655651_gp_0_0_0_0;
static const Il2CppType* GenInst_SynchronizedCollection_1_t2430655651_gp_0_0_0_0_Types[] = { &SynchronizedCollection_1_t2430655651_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SynchronizedCollection_1_t2430655651_gp_0_0_0_0 = { 1, GenInst_SynchronizedCollection_1_t2430655651_gp_0_0_0_0_Types };
extern const Il2CppType SynchronizedKeyedCollection_2_t3513088456_gp_1_0_0_0;
static const Il2CppType* GenInst_SynchronizedKeyedCollection_2_t3513088456_gp_1_0_0_0_Types[] = { &SynchronizedKeyedCollection_2_t3513088456_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SynchronizedKeyedCollection_2_t3513088456_gp_1_0_0_0 = { 1, GenInst_SynchronizedKeyedCollection_2_t3513088456_gp_1_0_0_0_Types };
extern const Il2CppType SynchronizedKeyedCollection_2_t3513088456_gp_0_0_0_0;
static const Il2CppType* GenInst_SynchronizedKeyedCollection_2_t3513088456_gp_0_0_0_0_SynchronizedKeyedCollection_2_t3513088456_gp_1_0_0_0_Types[] = { &SynchronizedKeyedCollection_2_t3513088456_gp_0_0_0_0, &SynchronizedKeyedCollection_2_t3513088456_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SynchronizedKeyedCollection_2_t3513088456_gp_0_0_0_0_SynchronizedKeyedCollection_2_t3513088456_gp_1_0_0_0 = { 2, GenInst_SynchronizedKeyedCollection_2_t3513088456_gp_0_0_0_0_SynchronizedKeyedCollection_2_t3513088456_gp_1_0_0_0_Types };
extern const Il2CppType SynchronizedReadOnlyCollection_1_t1361004819_gp_0_0_0_0;
static const Il2CppType* GenInst_SynchronizedReadOnlyCollection_1_t1361004819_gp_0_0_0_0_Types[] = { &SynchronizedReadOnlyCollection_1_t1361004819_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SynchronizedReadOnlyCollection_1_t1361004819_gp_0_0_0_0 = { 1, GenInst_SynchronizedReadOnlyCollection_1_t1361004819_gp_0_0_0_0_Types };
extern const Il2CppType AsymmetricSecurityBindingElement_BuildChannelFactoryCore_m1751482239_gp_0_0_0_0;
static const Il2CppType* GenInst_AsymmetricSecurityBindingElement_BuildChannelFactoryCore_m1751482239_gp_0_0_0_0_Types[] = { &AsymmetricSecurityBindingElement_BuildChannelFactoryCore_m1751482239_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AsymmetricSecurityBindingElement_BuildChannelFactoryCore_m1751482239_gp_0_0_0_0 = { 1, GenInst_AsymmetricSecurityBindingElement_BuildChannelFactoryCore_m1751482239_gp_0_0_0_0_Types };
extern const Il2CppType AsymmetricSecurityBindingElement_BuildChannelListenerCore_m1845291049_gp_0_0_0_0;
static const Il2CppType* GenInst_AsymmetricSecurityBindingElement_BuildChannelListenerCore_m1845291049_gp_0_0_0_0_Types[] = { &AsymmetricSecurityBindingElement_BuildChannelListenerCore_m1845291049_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AsymmetricSecurityBindingElement_BuildChannelListenerCore_m1845291049_gp_0_0_0_0 = { 1, GenInst_AsymmetricSecurityBindingElement_BuildChannelListenerCore_m1845291049_gp_0_0_0_0_Types };
extern const Il2CppType AsymmetricSecurityBindingElement_GetProperty_m2812938255_gp_0_0_0_0;
static const Il2CppType* GenInst_AsymmetricSecurityBindingElement_GetProperty_m2812938255_gp_0_0_0_0_Types[] = { &AsymmetricSecurityBindingElement_GetProperty_m2812938255_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AsymmetricSecurityBindingElement_GetProperty_m2812938255_gp_0_0_0_0 = { 1, GenInst_AsymmetricSecurityBindingElement_GetProperty_m2812938255_gp_0_0_0_0_Types };
extern const Il2CppType BinaryMessageEncodingBindingElement_BuildChannelFactory_m2836120619_gp_0_0_0_0;
static const Il2CppType* GenInst_BinaryMessageEncodingBindingElement_BuildChannelFactory_m2836120619_gp_0_0_0_0_Types[] = { &BinaryMessageEncodingBindingElement_BuildChannelFactory_m2836120619_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BinaryMessageEncodingBindingElement_BuildChannelFactory_m2836120619_gp_0_0_0_0 = { 1, GenInst_BinaryMessageEncodingBindingElement_BuildChannelFactory_m2836120619_gp_0_0_0_0_Types };
extern const Il2CppType BinaryMessageEncodingBindingElement_BuildChannelListener_m2460778595_gp_0_0_0_0;
static const Il2CppType* GenInst_BinaryMessageEncodingBindingElement_BuildChannelListener_m2460778595_gp_0_0_0_0_Types[] = { &BinaryMessageEncodingBindingElement_BuildChannelListener_m2460778595_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BinaryMessageEncodingBindingElement_BuildChannelListener_m2460778595_gp_0_0_0_0 = { 1, GenInst_BinaryMessageEncodingBindingElement_BuildChannelListener_m2460778595_gp_0_0_0_0_Types };
extern const Il2CppType BinaryMessageEncodingBindingElement_CanBuildChannelListener_m3187724959_gp_0_0_0_0;
static const Il2CppType* GenInst_BinaryMessageEncodingBindingElement_CanBuildChannelListener_m3187724959_gp_0_0_0_0_Types[] = { &BinaryMessageEncodingBindingElement_CanBuildChannelListener_m3187724959_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BinaryMessageEncodingBindingElement_CanBuildChannelListener_m3187724959_gp_0_0_0_0 = { 1, GenInst_BinaryMessageEncodingBindingElement_CanBuildChannelListener_m3187724959_gp_0_0_0_0_Types };
extern const Il2CppType Binding_BuildChannelFactory_m2702276000_gp_0_0_0_0;
static const Il2CppType* GenInst_Binding_BuildChannelFactory_m2702276000_gp_0_0_0_0_Types[] = { &Binding_BuildChannelFactory_m2702276000_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Binding_BuildChannelFactory_m2702276000_gp_0_0_0_0 = { 1, GenInst_Binding_BuildChannelFactory_m2702276000_gp_0_0_0_0_Types };
extern const Il2CppType Binding_BuildChannelListener_m3485620627_gp_0_0_0_0;
static const Il2CppType* GenInst_Binding_BuildChannelListener_m3485620627_gp_0_0_0_0_Types[] = { &Binding_BuildChannelListener_m3485620627_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Binding_BuildChannelListener_m3485620627_gp_0_0_0_0 = { 1, GenInst_Binding_BuildChannelListener_m3485620627_gp_0_0_0_0_Types };
extern const Il2CppType Binding_CanBuildChannelFactory_m3892110411_gp_0_0_0_0;
static const Il2CppType* GenInst_Binding_CanBuildChannelFactory_m3892110411_gp_0_0_0_0_Types[] = { &Binding_CanBuildChannelFactory_m3892110411_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Binding_CanBuildChannelFactory_m3892110411_gp_0_0_0_0 = { 1, GenInst_Binding_CanBuildChannelFactory_m3892110411_gp_0_0_0_0_Types };
extern const Il2CppType Binding_CanBuildChannelListener_m2369400018_gp_0_0_0_0;
static const Il2CppType* GenInst_Binding_CanBuildChannelListener_m2369400018_gp_0_0_0_0_Types[] = { &Binding_CanBuildChannelListener_m2369400018_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Binding_CanBuildChannelListener_m2369400018_gp_0_0_0_0 = { 1, GenInst_Binding_CanBuildChannelListener_m2369400018_gp_0_0_0_0_Types };
extern const Il2CppType BindingContext_BuildInnerChannelFactory_m364505994_gp_0_0_0_0;
static const Il2CppType* GenInst_BindingContext_BuildInnerChannelFactory_m364505994_gp_0_0_0_0_Types[] = { &BindingContext_BuildInnerChannelFactory_m364505994_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BindingContext_BuildInnerChannelFactory_m364505994_gp_0_0_0_0 = { 1, GenInst_BindingContext_BuildInnerChannelFactory_m364505994_gp_0_0_0_0_Types };
extern const Il2CppType BindingContext_BuildInnerChannelListener_m2649823158_gp_0_0_0_0;
static const Il2CppType* GenInst_BindingContext_BuildInnerChannelListener_m2649823158_gp_0_0_0_0_Types[] = { &BindingContext_BuildInnerChannelListener_m2649823158_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BindingContext_BuildInnerChannelListener_m2649823158_gp_0_0_0_0 = { 1, GenInst_BindingContext_BuildInnerChannelListener_m2649823158_gp_0_0_0_0_Types };
extern const Il2CppType BindingContext_CanBuildInnerChannelFactory_m142850783_gp_0_0_0_0;
static const Il2CppType* GenInst_BindingContext_CanBuildInnerChannelFactory_m142850783_gp_0_0_0_0_Types[] = { &BindingContext_CanBuildInnerChannelFactory_m142850783_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BindingContext_CanBuildInnerChannelFactory_m142850783_gp_0_0_0_0 = { 1, GenInst_BindingContext_CanBuildInnerChannelFactory_m142850783_gp_0_0_0_0_Types };
extern const Il2CppType BindingContext_CanBuildInnerChannelListener_m235529581_gp_0_0_0_0;
static const Il2CppType* GenInst_BindingContext_CanBuildInnerChannelListener_m235529581_gp_0_0_0_0_Types[] = { &BindingContext_CanBuildInnerChannelListener_m235529581_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BindingContext_CanBuildInnerChannelListener_m235529581_gp_0_0_0_0 = { 1, GenInst_BindingContext_CanBuildInnerChannelListener_m235529581_gp_0_0_0_0_Types };
extern const Il2CppType BindingContext_GetInnerProperty_m1828077793_gp_0_0_0_0;
static const Il2CppType* GenInst_BindingContext_GetInnerProperty_m1828077793_gp_0_0_0_0_Types[] = { &BindingContext_GetInnerProperty_m1828077793_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BindingContext_GetInnerProperty_m1828077793_gp_0_0_0_0 = { 1, GenInst_BindingContext_GetInnerProperty_m1828077793_gp_0_0_0_0_Types };
extern const Il2CppType BindingElement_BuildChannelFactory_m384837745_gp_0_0_0_0;
static const Il2CppType* GenInst_BindingElement_BuildChannelFactory_m384837745_gp_0_0_0_0_Types[] = { &BindingElement_BuildChannelFactory_m384837745_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BindingElement_BuildChannelFactory_m384837745_gp_0_0_0_0 = { 1, GenInst_BindingElement_BuildChannelFactory_m384837745_gp_0_0_0_0_Types };
extern const Il2CppType BindingElement_BuildChannelListener_m517227291_gp_0_0_0_0;
static const Il2CppType* GenInst_BindingElement_BuildChannelListener_m517227291_gp_0_0_0_0_Types[] = { &BindingElement_BuildChannelListener_m517227291_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BindingElement_BuildChannelListener_m517227291_gp_0_0_0_0 = { 1, GenInst_BindingElement_BuildChannelListener_m517227291_gp_0_0_0_0_Types };
extern const Il2CppType BindingElement_CanBuildChannelFactory_m3948462205_gp_0_0_0_0;
static const Il2CppType* GenInst_BindingElement_CanBuildChannelFactory_m3948462205_gp_0_0_0_0_Types[] = { &BindingElement_CanBuildChannelFactory_m3948462205_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BindingElement_CanBuildChannelFactory_m3948462205_gp_0_0_0_0 = { 1, GenInst_BindingElement_CanBuildChannelFactory_m3948462205_gp_0_0_0_0_Types };
extern const Il2CppType BindingElement_CanBuildChannelListener_m1475658708_gp_0_0_0_0;
static const Il2CppType* GenInst_BindingElement_CanBuildChannelListener_m1475658708_gp_0_0_0_0_Types[] = { &BindingElement_CanBuildChannelListener_m1475658708_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BindingElement_CanBuildChannelListener_m1475658708_gp_0_0_0_0 = { 1, GenInst_BindingElement_CanBuildChannelListener_m1475658708_gp_0_0_0_0_Types };
extern const Il2CppType BindingElementCollection_RemoveAll_m127050395_gp_0_0_0_0;
static const Il2CppType* GenInst_BindingElementCollection_RemoveAll_m127050395_gp_0_0_0_0_Types[] = { &BindingElementCollection_RemoveAll_m127050395_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BindingElementCollection_RemoveAll_m127050395_gp_0_0_0_0 = { 1, GenInst_BindingElementCollection_RemoveAll_m127050395_gp_0_0_0_0_Types };
extern const Il2CppType TransportChannelFactoryBase_1_t4012543988_gp_0_0_0_0;
static const Il2CppType* GenInst_TransportChannelFactoryBase_1_t4012543988_gp_0_0_0_0_Types[] = { &TransportChannelFactoryBase_1_t4012543988_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TransportChannelFactoryBase_1_t4012543988_gp_0_0_0_0 = { 1, GenInst_TransportChannelFactoryBase_1_t4012543988_gp_0_0_0_0_Types };
extern const Il2CppType ChannelFactoryBase_1_t2067588991_gp_0_0_0_0;
static const Il2CppType* GenInst_ChannelFactoryBase_1_t2067588991_gp_0_0_0_0_Types[] = { &ChannelFactoryBase_1_t2067588991_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ChannelFactoryBase_1_t2067588991_gp_0_0_0_0 = { 1, GenInst_ChannelFactoryBase_1_t2067588991_gp_0_0_0_0_Types };
extern const Il2CppType ChannelListenerBase_GetProperty_m3547516831_gp_0_0_0_0;
static const Il2CppType* GenInst_ChannelListenerBase_GetProperty_m3547516831_gp_0_0_0_0_Types[] = { &ChannelListenerBase_GetProperty_m3547516831_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ChannelListenerBase_GetProperty_m3547516831_gp_0_0_0_0 = { 1, GenInst_ChannelListenerBase_GetProperty_m3547516831_gp_0_0_0_0_Types };
extern const Il2CppType InternalChannelListenerBase_1_t1255230044_gp_0_0_0_0;
static const Il2CppType* GenInst_InternalChannelListenerBase_1_t1255230044_gp_0_0_0_0_Types[] = { &InternalChannelListenerBase_1_t1255230044_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalChannelListenerBase_1_t1255230044_gp_0_0_0_0 = { 1, GenInst_InternalChannelListenerBase_1_t1255230044_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_TimeSpan_t881159249_0_0_0_InternalChannelListenerBase_1_t1255230044_gp_0_0_0_0_Types[] = { &TimeSpan_t881159249_0_0_0, &InternalChannelListenerBase_1_t1255230044_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t881159249_0_0_0_InternalChannelListenerBase_1_t1255230044_gp_0_0_0_0 = { 2, GenInst_TimeSpan_t881159249_0_0_0_InternalChannelListenerBase_1_t1255230044_gp_0_0_0_0_Types };
extern const Il2CppType U3COnBeginAcceptChannelU3Ec__AnonStorey8_t1859370651_gp_0_0_0_0;
static const Il2CppType* GenInst_U3COnBeginAcceptChannelU3Ec__AnonStorey8_t1859370651_gp_0_0_0_0_Types[] = { &U3COnBeginAcceptChannelU3Ec__AnonStorey8_t1859370651_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3COnBeginAcceptChannelU3Ec__AnonStorey8_t1859370651_gp_0_0_0_0 = { 1, GenInst_U3COnBeginAcceptChannelU3Ec__AnonStorey8_t1859370651_gp_0_0_0_0_Types };
extern const Il2CppType ChannelListenerBase_1_t3869036235_gp_0_0_0_0;
static const Il2CppType* GenInst_ChannelListenerBase_1_t3869036235_gp_0_0_0_0_Types[] = { &ChannelListenerBase_1_t3869036235_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ChannelListenerBase_1_t3869036235_gp_0_0_0_0 = { 1, GenInst_ChannelListenerBase_1_t3869036235_gp_0_0_0_0_Types };
extern const Il2CppType HttpChannelFactory_1_t1944017751_gp_0_0_0_0;
static const Il2CppType* GenInst_HttpChannelFactory_1_t1944017751_gp_0_0_0_0_Types[] = { &HttpChannelFactory_1_t1944017751_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_HttpChannelFactory_1_t1944017751_gp_0_0_0_0 = { 1, GenInst_HttpChannelFactory_1_t1944017751_gp_0_0_0_0_Types };
extern const Il2CppType HttpSimpleChannelListener_1_t176083481_gp_0_0_0_0;
static const Il2CppType* GenInst_HttpSimpleChannelListener_1_t176083481_gp_0_0_0_0_Types[] = { &HttpSimpleChannelListener_1_t176083481_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_HttpSimpleChannelListener_1_t176083481_gp_0_0_0_0 = { 1, GenInst_HttpSimpleChannelListener_1_t176083481_gp_0_0_0_0_Types };
extern const Il2CppType AspNetChannelListener_1_t2531383694_gp_0_0_0_0;
static const Il2CppType* GenInst_AspNetChannelListener_1_t2531383694_gp_0_0_0_0_Types[] = { &AspNetChannelListener_1_t2531383694_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AspNetChannelListener_1_t2531383694_gp_0_0_0_0 = { 1, GenInst_AspNetChannelListener_1_t2531383694_gp_0_0_0_0_Types };
extern const Il2CppType HttpChannelListenerBase_1_t1301399014_gp_0_0_0_0;
static const Il2CppType* GenInst_HttpChannelListenerBase_1_t1301399014_gp_0_0_0_0_Types[] = { &HttpChannelListenerBase_1_t1301399014_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_HttpChannelListenerBase_1_t1301399014_gp_0_0_0_0 = { 1, GenInst_HttpChannelListenerBase_1_t1301399014_gp_0_0_0_0_Types };
extern const Il2CppType HttpTransportBindingElement_BuildChannelFactory_m3380490396_gp_0_0_0_0;
static const Il2CppType* GenInst_HttpTransportBindingElement_BuildChannelFactory_m3380490396_gp_0_0_0_0_Types[] = { &HttpTransportBindingElement_BuildChannelFactory_m3380490396_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_HttpTransportBindingElement_BuildChannelFactory_m3380490396_gp_0_0_0_0 = { 1, GenInst_HttpTransportBindingElement_BuildChannelFactory_m3380490396_gp_0_0_0_0_Types };
extern const Il2CppType HttpTransportBindingElement_BuildChannelListener_m2096701392_gp_0_0_0_0;
static const Il2CppType* GenInst_HttpTransportBindingElement_BuildChannelListener_m2096701392_gp_0_0_0_0_Types[] = { &HttpTransportBindingElement_BuildChannelListener_m2096701392_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_HttpTransportBindingElement_BuildChannelListener_m2096701392_gp_0_0_0_0 = { 1, GenInst_HttpTransportBindingElement_BuildChannelListener_m2096701392_gp_0_0_0_0_Types };
extern const Il2CppType HttpTransportBindingElement_GetProperty_m3676928347_gp_0_0_0_0;
static const Il2CppType* GenInst_HttpTransportBindingElement_GetProperty_m3676928347_gp_0_0_0_0_Types[] = { &HttpTransportBindingElement_GetProperty_m3676928347_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_HttpTransportBindingElement_GetProperty_m3676928347_gp_0_0_0_0 = { 1, GenInst_HttpTransportBindingElement_GetProperty_m3676928347_gp_0_0_0_0_Types };
extern const Il2CppType HttpsTransportBindingElement_BuildChannelFactory_m462496859_gp_0_0_0_0;
static const Il2CppType* GenInst_HttpsTransportBindingElement_BuildChannelFactory_m462496859_gp_0_0_0_0_Types[] = { &HttpsTransportBindingElement_BuildChannelFactory_m462496859_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_HttpsTransportBindingElement_BuildChannelFactory_m462496859_gp_0_0_0_0 = { 1, GenInst_HttpsTransportBindingElement_BuildChannelFactory_m462496859_gp_0_0_0_0_Types };
extern const Il2CppType HttpsTransportBindingElement_BuildChannelListener_m1543235516_gp_0_0_0_0;
static const Il2CppType* GenInst_HttpsTransportBindingElement_BuildChannelListener_m1543235516_gp_0_0_0_0_Types[] = { &HttpsTransportBindingElement_BuildChannelListener_m1543235516_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_HttpsTransportBindingElement_BuildChannelListener_m1543235516_gp_0_0_0_0 = { 1, GenInst_HttpsTransportBindingElement_BuildChannelListener_m1543235516_gp_0_0_0_0_Types };
extern const Il2CppType MessageHeaders_GetHeader_m2299488900_gp_0_0_0_0;
static const Il2CppType* GenInst_MessageHeaders_GetHeader_m2299488900_gp_0_0_0_0_Types[] = { &MessageHeaders_GetHeader_m2299488900_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageHeaders_GetHeader_m2299488900_gp_0_0_0_0 = { 1, GenInst_MessageHeaders_GetHeader_m2299488900_gp_0_0_0_0_Types };
extern const Il2CppType MsmqBindingElementBase_GetProperty_m3952560298_gp_0_0_0_0;
static const Il2CppType* GenInst_MsmqBindingElementBase_GetProperty_m3952560298_gp_0_0_0_0_Types[] = { &MsmqBindingElementBase_GetProperty_m3952560298_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MsmqBindingElementBase_GetProperty_m3952560298_gp_0_0_0_0 = { 1, GenInst_MsmqBindingElementBase_GetProperty_m3952560298_gp_0_0_0_0_Types };
extern const Il2CppType MsmqChannelFactory_1_t2220480299_gp_0_0_0_0;
static const Il2CppType* GenInst_MsmqChannelFactory_1_t2220480299_gp_0_0_0_0_Types[] = { &MsmqChannelFactory_1_t2220480299_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MsmqChannelFactory_1_t2220480299_gp_0_0_0_0 = { 1, GenInst_MsmqChannelFactory_1_t2220480299_gp_0_0_0_0_Types };
extern const Il2CppType MsmqTransportBindingElement_BuildChannelFactory_m1998761050_gp_0_0_0_0;
static const Il2CppType* GenInst_MsmqTransportBindingElement_BuildChannelFactory_m1998761050_gp_0_0_0_0_Types[] = { &MsmqTransportBindingElement_BuildChannelFactory_m1998761050_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MsmqTransportBindingElement_BuildChannelFactory_m1998761050_gp_0_0_0_0 = { 1, GenInst_MsmqTransportBindingElement_BuildChannelFactory_m1998761050_gp_0_0_0_0_Types };
extern const Il2CppType MsmqTransportBindingElement_BuildChannelListener_m3603513958_gp_0_0_0_0;
static const Il2CppType* GenInst_MsmqTransportBindingElement_BuildChannelListener_m3603513958_gp_0_0_0_0_Types[] = { &MsmqTransportBindingElement_BuildChannelListener_m3603513958_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MsmqTransportBindingElement_BuildChannelListener_m3603513958_gp_0_0_0_0 = { 1, GenInst_MsmqTransportBindingElement_BuildChannelListener_m3603513958_gp_0_0_0_0_Types };
extern const Il2CppType MtomMessageEncodingBindingElement_BuildChannelFactory_m908181888_gp_0_0_0_0;
static const Il2CppType* GenInst_MtomMessageEncodingBindingElement_BuildChannelFactory_m908181888_gp_0_0_0_0_Types[] = { &MtomMessageEncodingBindingElement_BuildChannelFactory_m908181888_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MtomMessageEncodingBindingElement_BuildChannelFactory_m908181888_gp_0_0_0_0 = { 1, GenInst_MtomMessageEncodingBindingElement_BuildChannelFactory_m908181888_gp_0_0_0_0_Types };
extern const Il2CppType MtomMessageEncodingBindingElement_BuildChannelListener_m194984888_gp_0_0_0_0;
static const Il2CppType* GenInst_MtomMessageEncodingBindingElement_BuildChannelListener_m194984888_gp_0_0_0_0_Types[] = { &MtomMessageEncodingBindingElement_BuildChannelListener_m194984888_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MtomMessageEncodingBindingElement_BuildChannelListener_m194984888_gp_0_0_0_0 = { 1, GenInst_MtomMessageEncodingBindingElement_BuildChannelListener_m194984888_gp_0_0_0_0_Types };
extern const Il2CppType MtomMessageEncodingBindingElement_CanBuildChannelListener_m3195457011_gp_0_0_0_0;
static const Il2CppType* GenInst_MtomMessageEncodingBindingElement_CanBuildChannelListener_m3195457011_gp_0_0_0_0_Types[] = { &MtomMessageEncodingBindingElement_CanBuildChannelListener_m3195457011_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MtomMessageEncodingBindingElement_CanBuildChannelListener_m3195457011_gp_0_0_0_0 = { 1, GenInst_MtomMessageEncodingBindingElement_CanBuildChannelListener_m3195457011_gp_0_0_0_0_Types };
extern const Il2CppType NamedPipeChannelFactory_1_t474837288_gp_0_0_0_0;
static const Il2CppType* GenInst_NamedPipeChannelFactory_1_t474837288_gp_0_0_0_0_Types[] = { &NamedPipeChannelFactory_1_t474837288_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NamedPipeChannelFactory_1_t474837288_gp_0_0_0_0 = { 1, GenInst_NamedPipeChannelFactory_1_t474837288_gp_0_0_0_0_Types };
extern const Il2CppType NamedPipeChannelListener_1_t3969090434_gp_0_0_0_0;
static const Il2CppType* GenInst_NamedPipeChannelListener_1_t3969090434_gp_0_0_0_0_Types[] = { &NamedPipeChannelListener_1_t3969090434_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NamedPipeChannelListener_1_t3969090434_gp_0_0_0_0 = { 1, GenInst_NamedPipeChannelListener_1_t3969090434_gp_0_0_0_0_Types };
extern const Il2CppType U3COnAcceptChannelU3Ec__AnonStoreyE_t609268473_gp_0_0_0_0;
static const Il2CppType* GenInst_U3COnAcceptChannelU3Ec__AnonStoreyE_t609268473_gp_0_0_0_0_Types[] = { &U3COnAcceptChannelU3Ec__AnonStoreyE_t609268473_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3COnAcceptChannelU3Ec__AnonStoreyE_t609268473_gp_0_0_0_0 = { 1, GenInst_U3COnAcceptChannelU3Ec__AnonStoreyE_t609268473_gp_0_0_0_0_Types };
extern const Il2CppType NamedPipeTransportBindingElement_BuildChannelFactory_m1135370280_gp_0_0_0_0;
static const Il2CppType* GenInst_NamedPipeTransportBindingElement_BuildChannelFactory_m1135370280_gp_0_0_0_0_Types[] = { &NamedPipeTransportBindingElement_BuildChannelFactory_m1135370280_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NamedPipeTransportBindingElement_BuildChannelFactory_m1135370280_gp_0_0_0_0 = { 1, GenInst_NamedPipeTransportBindingElement_BuildChannelFactory_m1135370280_gp_0_0_0_0_Types };
extern const Il2CppType NamedPipeTransportBindingElement_BuildChannelListener_m2417158508_gp_0_0_0_0;
static const Il2CppType* GenInst_NamedPipeTransportBindingElement_BuildChannelListener_m2417158508_gp_0_0_0_0_Types[] = { &NamedPipeTransportBindingElement_BuildChannelListener_m2417158508_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NamedPipeTransportBindingElement_BuildChannelListener_m2417158508_gp_0_0_0_0 = { 1, GenInst_NamedPipeTransportBindingElement_BuildChannelListener_m2417158508_gp_0_0_0_0_Types };
extern const Il2CppType NamedPipeTransportBindingElement_GetProperty_m656345567_gp_0_0_0_0;
static const Il2CppType* GenInst_NamedPipeTransportBindingElement_GetProperty_m656345567_gp_0_0_0_0_Types[] = { &NamedPipeTransportBindingElement_GetProperty_m656345567_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NamedPipeTransportBindingElement_GetProperty_m656345567_gp_0_0_0_0 = { 1, GenInst_NamedPipeTransportBindingElement_GetProperty_m656345567_gp_0_0_0_0_Types };
extern const Il2CppType PeerChannelFactory_1_t3525877856_gp_0_0_0_0;
static const Il2CppType* GenInst_PeerChannelFactory_1_t3525877856_gp_0_0_0_0_Types[] = { &PeerChannelFactory_1_t3525877856_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PeerChannelFactory_1_t3525877856_gp_0_0_0_0 = { 1, GenInst_PeerChannelFactory_1_t3525877856_gp_0_0_0_0_Types };
extern const Il2CppType PeerChannelListener_1_t665875855_gp_0_0_0_0;
static const Il2CppType* GenInst_PeerChannelListener_1_t665875855_gp_0_0_0_0_Types[] = { &PeerChannelListener_1_t665875855_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PeerChannelListener_1_t665875855_gp_0_0_0_0 = { 1, GenInst_PeerChannelListener_1_t665875855_gp_0_0_0_0_Types };
extern const Il2CppType PeerCustomResolverBindingElement_BuildChannelFactory_m4000210886_gp_0_0_0_0;
static const Il2CppType* GenInst_PeerCustomResolverBindingElement_BuildChannelFactory_m4000210886_gp_0_0_0_0_Types[] = { &PeerCustomResolverBindingElement_BuildChannelFactory_m4000210886_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PeerCustomResolverBindingElement_BuildChannelFactory_m4000210886_gp_0_0_0_0 = { 1, GenInst_PeerCustomResolverBindingElement_BuildChannelFactory_m4000210886_gp_0_0_0_0_Types };
extern const Il2CppType PeerCustomResolverBindingElement_BuildChannelListener_m2913003754_gp_0_0_0_0;
static const Il2CppType* GenInst_PeerCustomResolverBindingElement_BuildChannelListener_m2913003754_gp_0_0_0_0_Types[] = { &PeerCustomResolverBindingElement_BuildChannelListener_m2913003754_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PeerCustomResolverBindingElement_BuildChannelListener_m2913003754_gp_0_0_0_0 = { 1, GenInst_PeerCustomResolverBindingElement_BuildChannelListener_m2913003754_gp_0_0_0_0_Types };
extern const Il2CppType PeerTransportBindingElement_BuildChannelFactory_m852669533_gp_0_0_0_0;
static const Il2CppType* GenInst_PeerTransportBindingElement_BuildChannelFactory_m852669533_gp_0_0_0_0_Types[] = { &PeerTransportBindingElement_BuildChannelFactory_m852669533_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PeerTransportBindingElement_BuildChannelFactory_m852669533_gp_0_0_0_0 = { 1, GenInst_PeerTransportBindingElement_BuildChannelFactory_m852669533_gp_0_0_0_0_Types };
extern const Il2CppType PeerTransportBindingElement_BuildChannelListener_m4218990573_gp_0_0_0_0;
static const Il2CppType* GenInst_PeerTransportBindingElement_BuildChannelListener_m4218990573_gp_0_0_0_0_Types[] = { &PeerTransportBindingElement_BuildChannelListener_m4218990573_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PeerTransportBindingElement_BuildChannelListener_m4218990573_gp_0_0_0_0 = { 1, GenInst_PeerTransportBindingElement_BuildChannelListener_m4218990573_gp_0_0_0_0_Types };
extern const Il2CppType PeerTransportBindingElement_GetProperty_m322264804_gp_0_0_0_0;
static const Il2CppType* GenInst_PeerTransportBindingElement_GetProperty_m322264804_gp_0_0_0_0_Types[] = { &PeerTransportBindingElement_GetProperty_m322264804_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PeerTransportBindingElement_GetProperty_m322264804_gp_0_0_0_0 = { 1, GenInst_PeerTransportBindingElement_GetProperty_m322264804_gp_0_0_0_0_Types };
extern const Il2CppType PnrpPeerResolverBindingElement_BuildChannelFactory_m762075276_gp_0_0_0_0;
static const Il2CppType* GenInst_PnrpPeerResolverBindingElement_BuildChannelFactory_m762075276_gp_0_0_0_0_Types[] = { &PnrpPeerResolverBindingElement_BuildChannelFactory_m762075276_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PnrpPeerResolverBindingElement_BuildChannelFactory_m762075276_gp_0_0_0_0 = { 1, GenInst_PnrpPeerResolverBindingElement_BuildChannelFactory_m762075276_gp_0_0_0_0_Types };
extern const Il2CppType PnrpPeerResolverBindingElement_BuildChannelListener_m3914545043_gp_0_0_0_0;
static const Il2CppType* GenInst_PnrpPeerResolverBindingElement_BuildChannelListener_m3914545043_gp_0_0_0_0_Types[] = { &PnrpPeerResolverBindingElement_BuildChannelListener_m3914545043_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PnrpPeerResolverBindingElement_BuildChannelListener_m3914545043_gp_0_0_0_0 = { 1, GenInst_PnrpPeerResolverBindingElement_BuildChannelListener_m3914545043_gp_0_0_0_0_Types };
extern const Il2CppType SecurityBindingElement_CanBuildChannelFactory_m2127198948_gp_0_0_0_0;
static const Il2CppType* GenInst_SecurityBindingElement_CanBuildChannelFactory_m2127198948_gp_0_0_0_0_Types[] = { &SecurityBindingElement_CanBuildChannelFactory_m2127198948_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SecurityBindingElement_CanBuildChannelFactory_m2127198948_gp_0_0_0_0 = { 1, GenInst_SecurityBindingElement_CanBuildChannelFactory_m2127198948_gp_0_0_0_0_Types };
extern const Il2CppType SecurityBindingElement_BuildChannelFactory_m893612399_gp_0_0_0_0;
static const Il2CppType* GenInst_SecurityBindingElement_BuildChannelFactory_m893612399_gp_0_0_0_0_Types[] = { &SecurityBindingElement_BuildChannelFactory_m893612399_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SecurityBindingElement_BuildChannelFactory_m893612399_gp_0_0_0_0 = { 1, GenInst_SecurityBindingElement_BuildChannelFactory_m893612399_gp_0_0_0_0_Types };
extern const Il2CppType SecurityBindingElement_BuildChannelFactoryCore_m4206137793_gp_0_0_0_0;
static const Il2CppType* GenInst_SecurityBindingElement_BuildChannelFactoryCore_m4206137793_gp_0_0_0_0_Types[] = { &SecurityBindingElement_BuildChannelFactoryCore_m4206137793_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SecurityBindingElement_BuildChannelFactoryCore_m4206137793_gp_0_0_0_0 = { 1, GenInst_SecurityBindingElement_BuildChannelFactoryCore_m4206137793_gp_0_0_0_0_Types };
extern const Il2CppType SecurityBindingElement_CanBuildChannelListener_m3725582969_gp_0_0_0_0;
static const Il2CppType* GenInst_SecurityBindingElement_CanBuildChannelListener_m3725582969_gp_0_0_0_0_Types[] = { &SecurityBindingElement_CanBuildChannelListener_m3725582969_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SecurityBindingElement_CanBuildChannelListener_m3725582969_gp_0_0_0_0 = { 1, GenInst_SecurityBindingElement_CanBuildChannelListener_m3725582969_gp_0_0_0_0_Types };
extern const Il2CppType SecurityBindingElement_BuildChannelListener_m3136879580_gp_0_0_0_0;
static const Il2CppType* GenInst_SecurityBindingElement_BuildChannelListener_m3136879580_gp_0_0_0_0_Types[] = { &SecurityBindingElement_BuildChannelListener_m3136879580_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SecurityBindingElement_BuildChannelListener_m3136879580_gp_0_0_0_0 = { 1, GenInst_SecurityBindingElement_BuildChannelListener_m3136879580_gp_0_0_0_0_Types };
extern const Il2CppType SecurityBindingElement_BuildChannelListenerCore_m1443472304_gp_0_0_0_0;
static const Il2CppType* GenInst_SecurityBindingElement_BuildChannelListenerCore_m1443472304_gp_0_0_0_0_Types[] = { &SecurityBindingElement_BuildChannelListenerCore_m1443472304_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SecurityBindingElement_BuildChannelListenerCore_m1443472304_gp_0_0_0_0 = { 1, GenInst_SecurityBindingElement_BuildChannelListenerCore_m1443472304_gp_0_0_0_0_Types };
extern const Il2CppType SecurityChannelFactory_1_t2661327900_gp_0_0_0_0;
static const Il2CppType* GenInst_SecurityChannelFactory_1_t2661327900_gp_0_0_0_0_Types[] = { &SecurityChannelFactory_1_t2661327900_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SecurityChannelFactory_1_t2661327900_gp_0_0_0_0 = { 1, GenInst_SecurityChannelFactory_1_t2661327900_gp_0_0_0_0_Types };
extern const Il2CppType SecurityChannelListener_1_t3584330813_gp_0_0_0_0;
static const Il2CppType* GenInst_SecurityChannelListener_1_t3584330813_gp_0_0_0_0_Types[] = { &SecurityChannelListener_1_t3584330813_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SecurityChannelListener_1_t3584330813_gp_0_0_0_0 = { 1, GenInst_SecurityChannelListener_1_t3584330813_gp_0_0_0_0_Types };
extern const Il2CppType SecurityChannelListener_1_GetProperty_m3548153566_gp_0_0_0_0;
static const Il2CppType* GenInst_SecurityChannelListener_1_GetProperty_m3548153566_gp_0_0_0_0_Types[] = { &SecurityChannelListener_1_GetProperty_m3548153566_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SecurityChannelListener_1_GetProperty_m3548153566_gp_0_0_0_0 = { 1, GenInst_SecurityChannelListener_1_GetProperty_m3548153566_gp_0_0_0_0_Types };
extern const Il2CppType SymmetricSecurityBindingElement_BuildChannelFactoryCore_m3664457509_gp_0_0_0_0;
static const Il2CppType* GenInst_SymmetricSecurityBindingElement_BuildChannelFactoryCore_m3664457509_gp_0_0_0_0_Types[] = { &SymmetricSecurityBindingElement_BuildChannelFactoryCore_m3664457509_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SymmetricSecurityBindingElement_BuildChannelFactoryCore_m3664457509_gp_0_0_0_0 = { 1, GenInst_SymmetricSecurityBindingElement_BuildChannelFactoryCore_m3664457509_gp_0_0_0_0_Types };
extern const Il2CppType SymmetricSecurityBindingElement_BuildChannelListenerCore_m3398717442_gp_0_0_0_0;
static const Il2CppType* GenInst_SymmetricSecurityBindingElement_BuildChannelListenerCore_m3398717442_gp_0_0_0_0_Types[] = { &SymmetricSecurityBindingElement_BuildChannelListenerCore_m3398717442_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SymmetricSecurityBindingElement_BuildChannelListenerCore_m3398717442_gp_0_0_0_0 = { 1, GenInst_SymmetricSecurityBindingElement_BuildChannelListenerCore_m3398717442_gp_0_0_0_0_Types };
extern const Il2CppType SymmetricSecurityBindingElement_GetProperty_m527245184_gp_0_0_0_0;
static const Il2CppType* GenInst_SymmetricSecurityBindingElement_GetProperty_m527245184_gp_0_0_0_0_Types[] = { &SymmetricSecurityBindingElement_GetProperty_m527245184_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SymmetricSecurityBindingElement_GetProperty_m527245184_gp_0_0_0_0 = { 1, GenInst_SymmetricSecurityBindingElement_GetProperty_m527245184_gp_0_0_0_0_Types };
extern const Il2CppType TcpChannelFactory_1_t1190800706_gp_0_0_0_0;
static const Il2CppType* GenInst_TcpChannelFactory_1_t1190800706_gp_0_0_0_0_Types[] = { &TcpChannelFactory_1_t1190800706_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TcpChannelFactory_1_t1190800706_gp_0_0_0_0 = { 1, GenInst_TcpChannelFactory_1_t1190800706_gp_0_0_0_0_Types };
extern const Il2CppType TcpChannelListener_1_t1314382410_gp_0_0_0_0;
static const Il2CppType* GenInst_TcpChannelListener_1_t1314382410_gp_0_0_0_0_Types[] = { &TcpChannelListener_1_t1314382410_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TcpChannelListener_1_t1314382410_gp_0_0_0_0 = { 1, GenInst_TcpChannelListener_1_t1314382410_gp_0_0_0_0_Types };
extern const Il2CppType U3COnAcceptChannelU3Ec__AnonStorey12_t603992219_gp_0_0_0_0;
static const Il2CppType* GenInst_U3COnAcceptChannelU3Ec__AnonStorey12_t603992219_gp_0_0_0_0_Types[] = { &U3COnAcceptChannelU3Ec__AnonStorey12_t603992219_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3COnAcceptChannelU3Ec__AnonStorey12_t603992219_gp_0_0_0_0 = { 1, GenInst_U3COnAcceptChannelU3Ec__AnonStorey12_t603992219_gp_0_0_0_0_Types };
extern const Il2CppType U3CAcceptTcpClientU3Ec__AnonStorey13_t3277984792_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CAcceptTcpClientU3Ec__AnonStorey13_t3277984792_gp_0_0_0_0_Types[] = { &U3CAcceptTcpClientU3Ec__AnonStorey13_t3277984792_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CAcceptTcpClientU3Ec__AnonStorey13_t3277984792_gp_0_0_0_0 = { 1, GenInst_U3CAcceptTcpClientU3Ec__AnonStorey13_t3277984792_gp_0_0_0_0_Types };
extern const Il2CppType U3CAcceptTcpClientU3Ec__AnonStorey14_t3277526040_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CAcceptTcpClientU3Ec__AnonStorey14_t3277526040_gp_0_0_0_0_Types[] = { &U3CAcceptTcpClientU3Ec__AnonStorey14_t3277526040_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CAcceptTcpClientU3Ec__AnonStorey14_t3277526040_gp_0_0_0_0 = { 1, GenInst_U3CAcceptTcpClientU3Ec__AnonStorey14_t3277526040_gp_0_0_0_0_Types };
extern const Il2CppType TcpTransportBindingElement_BuildChannelFactory_m3905769953_gp_0_0_0_0;
static const Il2CppType* GenInst_TcpTransportBindingElement_BuildChannelFactory_m3905769953_gp_0_0_0_0_Types[] = { &TcpTransportBindingElement_BuildChannelFactory_m3905769953_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TcpTransportBindingElement_BuildChannelFactory_m3905769953_gp_0_0_0_0 = { 1, GenInst_TcpTransportBindingElement_BuildChannelFactory_m3905769953_gp_0_0_0_0_Types };
extern const Il2CppType TcpTransportBindingElement_BuildChannelListener_m2569812045_gp_0_0_0_0;
static const Il2CppType* GenInst_TcpTransportBindingElement_BuildChannelListener_m2569812045_gp_0_0_0_0_Types[] = { &TcpTransportBindingElement_BuildChannelListener_m2569812045_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TcpTransportBindingElement_BuildChannelListener_m2569812045_gp_0_0_0_0 = { 1, GenInst_TcpTransportBindingElement_BuildChannelListener_m2569812045_gp_0_0_0_0_Types };
extern const Il2CppType TcpTransportBindingElement_GetProperty_m1350457265_gp_0_0_0_0;
static const Il2CppType* GenInst_TcpTransportBindingElement_GetProperty_m1350457265_gp_0_0_0_0_Types[] = { &TcpTransportBindingElement_GetProperty_m1350457265_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TcpTransportBindingElement_GetProperty_m1350457265_gp_0_0_0_0 = { 1, GenInst_TcpTransportBindingElement_GetProperty_m1350457265_gp_0_0_0_0_Types };
extern const Il2CppType TextMessageEncodingBindingElement_BuildChannelFactory_m1972125352_gp_0_0_0_0;
static const Il2CppType* GenInst_TextMessageEncodingBindingElement_BuildChannelFactory_m1972125352_gp_0_0_0_0_Types[] = { &TextMessageEncodingBindingElement_BuildChannelFactory_m1972125352_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TextMessageEncodingBindingElement_BuildChannelFactory_m1972125352_gp_0_0_0_0 = { 1, GenInst_TextMessageEncodingBindingElement_BuildChannelFactory_m1972125352_gp_0_0_0_0_Types };
extern const Il2CppType TextMessageEncodingBindingElement_BuildChannelListener_m45428996_gp_0_0_0_0;
static const Il2CppType* GenInst_TextMessageEncodingBindingElement_BuildChannelListener_m45428996_gp_0_0_0_0_Types[] = { &TextMessageEncodingBindingElement_BuildChannelListener_m45428996_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TextMessageEncodingBindingElement_BuildChannelListener_m45428996_gp_0_0_0_0 = { 1, GenInst_TextMessageEncodingBindingElement_BuildChannelListener_m45428996_gp_0_0_0_0_Types };
extern const Il2CppType TextMessageEncodingBindingElement_CanBuildChannelListener_m1697465190_gp_0_0_0_0;
static const Il2CppType* GenInst_TextMessageEncodingBindingElement_CanBuildChannelListener_m1697465190_gp_0_0_0_0_Types[] = { &TextMessageEncodingBindingElement_CanBuildChannelListener_m1697465190_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TextMessageEncodingBindingElement_CanBuildChannelListener_m1697465190_gp_0_0_0_0 = { 1, GenInst_TextMessageEncodingBindingElement_CanBuildChannelListener_m1697465190_gp_0_0_0_0_Types };
extern const Il2CppType TextMessageEncodingBindingElement_GetProperty_m3914452139_gp_0_0_0_0;
static const Il2CppType* GenInst_TextMessageEncodingBindingElement_GetProperty_m3914452139_gp_0_0_0_0_Types[] = { &TextMessageEncodingBindingElement_GetProperty_m3914452139_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TextMessageEncodingBindingElement_GetProperty_m3914452139_gp_0_0_0_0 = { 1, GenInst_TextMessageEncodingBindingElement_GetProperty_m3914452139_gp_0_0_0_0_Types };
extern const Il2CppType TransactionFlowBindingElement_CanBuildChannelFactory_m4093754355_gp_0_0_0_0;
static const Il2CppType* GenInst_TransactionFlowBindingElement_CanBuildChannelFactory_m4093754355_gp_0_0_0_0_Types[] = { &TransactionFlowBindingElement_CanBuildChannelFactory_m4093754355_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TransactionFlowBindingElement_CanBuildChannelFactory_m4093754355_gp_0_0_0_0 = { 1, GenInst_TransactionFlowBindingElement_CanBuildChannelFactory_m4093754355_gp_0_0_0_0_Types };
extern const Il2CppType TransactionFlowBindingElement_CanBuildChannelListener_m738127627_gp_0_0_0_0;
static const Il2CppType* GenInst_TransactionFlowBindingElement_CanBuildChannelListener_m738127627_gp_0_0_0_0_Types[] = { &TransactionFlowBindingElement_CanBuildChannelListener_m738127627_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TransactionFlowBindingElement_CanBuildChannelListener_m738127627_gp_0_0_0_0 = { 1, GenInst_TransactionFlowBindingElement_CanBuildChannelListener_m738127627_gp_0_0_0_0_Types };
extern const Il2CppType TransactionFlowBindingElement_BuildChannelFactory_m281478039_gp_0_0_0_0;
static const Il2CppType* GenInst_TransactionFlowBindingElement_BuildChannelFactory_m281478039_gp_0_0_0_0_Types[] = { &TransactionFlowBindingElement_BuildChannelFactory_m281478039_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TransactionFlowBindingElement_BuildChannelFactory_m281478039_gp_0_0_0_0 = { 1, GenInst_TransactionFlowBindingElement_BuildChannelFactory_m281478039_gp_0_0_0_0_Types };
extern const Il2CppType TransactionFlowBindingElement_BuildChannelListener_m2433100597_gp_0_0_0_0;
static const Il2CppType* GenInst_TransactionFlowBindingElement_BuildChannelListener_m2433100597_gp_0_0_0_0_Types[] = { &TransactionFlowBindingElement_BuildChannelListener_m2433100597_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TransactionFlowBindingElement_BuildChannelListener_m2433100597_gp_0_0_0_0 = { 1, GenInst_TransactionFlowBindingElement_BuildChannelListener_m2433100597_gp_0_0_0_0_Types };
extern const Il2CppType TransactionChannelFactory_1_t3098953095_gp_0_0_0_0;
static const Il2CppType* GenInst_TransactionChannelFactory_1_t3098953095_gp_0_0_0_0_Types[] = { &TransactionChannelFactory_1_t3098953095_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TransactionChannelFactory_1_t3098953095_gp_0_0_0_0 = { 1, GenInst_TransactionChannelFactory_1_t3098953095_gp_0_0_0_0_Types };
extern const Il2CppType TransactionChannelListener_1_t2500118520_gp_0_0_0_0;
static const Il2CppType* GenInst_TransactionChannelListener_1_t2500118520_gp_0_0_0_0_Types[] = { &TransactionChannelListener_1_t2500118520_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TransactionChannelListener_1_t2500118520_gp_0_0_0_0 = { 1, GenInst_TransactionChannelListener_1_t2500118520_gp_0_0_0_0_Types };
extern const Il2CppType TransportBindingElement_GetProperty_m2036251320_gp_0_0_0_0;
static const Il2CppType* GenInst_TransportBindingElement_GetProperty_m2036251320_gp_0_0_0_0_Types[] = { &TransportBindingElement_GetProperty_m2036251320_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TransportBindingElement_GetProperty_m2036251320_gp_0_0_0_0 = { 1, GenInst_TransportBindingElement_GetProperty_m2036251320_gp_0_0_0_0_Types };
extern const Il2CppType TransportSecurityBindingElement_BuildChannelFactoryCore_m2596990168_gp_0_0_0_0;
static const Il2CppType* GenInst_TransportSecurityBindingElement_BuildChannelFactoryCore_m2596990168_gp_0_0_0_0_Types[] = { &TransportSecurityBindingElement_BuildChannelFactoryCore_m2596990168_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TransportSecurityBindingElement_BuildChannelFactoryCore_m2596990168_gp_0_0_0_0 = { 1, GenInst_TransportSecurityBindingElement_BuildChannelFactoryCore_m2596990168_gp_0_0_0_0_Types };
extern const Il2CppType TransportSecurityBindingElement_BuildChannelListenerCore_m1393449313_gp_0_0_0_0;
static const Il2CppType* GenInst_TransportSecurityBindingElement_BuildChannelListenerCore_m1393449313_gp_0_0_0_0_Types[] = { &TransportSecurityBindingElement_BuildChannelListenerCore_m1393449313_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TransportSecurityBindingElement_BuildChannelListenerCore_m1393449313_gp_0_0_0_0 = { 1, GenInst_TransportSecurityBindingElement_BuildChannelListenerCore_m1393449313_gp_0_0_0_0_Types };
extern const Il2CppType WSSecurityMessageHeader_FindAll_m4266741189_gp_0_0_0_0;
static const Il2CppType* GenInst_WSSecurityMessageHeader_FindAll_m4266741189_gp_0_0_0_0_Types[] = { &WSSecurityMessageHeader_FindAll_m4266741189_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_WSSecurityMessageHeader_FindAll_m4266741189_gp_0_0_0_0 = { 1, GenInst_WSSecurityMessageHeader_FindAll_m4266741189_gp_0_0_0_0_Types };
extern const Il2CppType StandardBindingElementCollection_1_t2952518451_gp_0_0_0_0;
static const Il2CppType* GenInst_StandardBindingElementCollection_1_t2952518451_gp_0_0_0_0_Types[] = { &StandardBindingElementCollection_1_t2952518451_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_StandardBindingElementCollection_1_t2952518451_gp_0_0_0_0 = { 1, GenInst_StandardBindingElementCollection_1_t2952518451_gp_0_0_0_0_Types };
extern const Il2CppType MexBindingBindingCollectionElement_2_t2854287389_gp_0_0_0_0;
extern const Il2CppType MexBindingBindingCollectionElement_2_t2854287389_gp_1_0_0_0;
static const Il2CppType* GenInst_MexBindingBindingCollectionElement_2_t2854287389_gp_0_0_0_0_MexBindingBindingCollectionElement_2_t2854287389_gp_1_0_0_0_Types[] = { &MexBindingBindingCollectionElement_2_t2854287389_gp_0_0_0_0, &MexBindingBindingCollectionElement_2_t2854287389_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_MexBindingBindingCollectionElement_2_t2854287389_gp_0_0_0_0_MexBindingBindingCollectionElement_2_t2854287389_gp_1_0_0_0 = { 2, GenInst_MexBindingBindingCollectionElement_2_t2854287389_gp_0_0_0_0_MexBindingBindingCollectionElement_2_t2854287389_gp_1_0_0_0_Types };
extern const Il2CppType NamedServiceModelExtensionCollectionElement_1_t1517949335_gp_0_0_0_0;
static const Il2CppType* GenInst_NamedServiceModelExtensionCollectionElement_1_t1517949335_gp_0_0_0_0_Types[] = { &NamedServiceModelExtensionCollectionElement_1_t1517949335_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NamedServiceModelExtensionCollectionElement_1_t1517949335_gp_0_0_0_0 = { 1, GenInst_NamedServiceModelExtensionCollectionElement_1_t1517949335_gp_0_0_0_0_Types };
extern const Il2CppType ServiceModelConfigurationElementCollection_1_t399084843_gp_0_0_0_0;
static const Il2CppType* GenInst_ServiceModelConfigurationElementCollection_1_t399084843_gp_0_0_0_0_Types[] = { &ServiceModelConfigurationElementCollection_1_t399084843_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ServiceModelConfigurationElementCollection_1_t399084843_gp_0_0_0_0 = { 1, GenInst_ServiceModelConfigurationElementCollection_1_t399084843_gp_0_0_0_0_Types };
extern const Il2CppType ServiceModelEnhancedConfigurationElementCollection_1_t2668806557_gp_0_0_0_0;
static const Il2CppType* GenInst_ServiceModelEnhancedConfigurationElementCollection_1_t2668806557_gp_0_0_0_0_Types[] = { &ServiceModelEnhancedConfigurationElementCollection_1_t2668806557_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ServiceModelEnhancedConfigurationElementCollection_1_t2668806557_gp_0_0_0_0 = { 1, GenInst_ServiceModelEnhancedConfigurationElementCollection_1_t2668806557_gp_0_0_0_0_Types };
extern const Il2CppType ServiceModelExtensionCollectionElement_1_t1335197220_gp_0_0_0_0;
static const Il2CppType* GenInst_ServiceModelExtensionCollectionElement_1_t1335197220_gp_0_0_0_0_Types[] = { &ServiceModelExtensionCollectionElement_1_t1335197220_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ServiceModelExtensionCollectionElement_1_t1335197220_gp_0_0_0_0 = { 1, GenInst_ServiceModelExtensionCollectionElement_1_t1335197220_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator5_t997212165_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator5_t997212165_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator5_t997212165_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator5_t997212165_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator5_t997212165_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ServiceModelExtensionCollectionElement_1_t1335197220_gp_0_0_0_0_Types[] = { &Type_t_0_0_0, &ServiceModelExtensionCollectionElement_1_t1335197220_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ServiceModelExtensionCollectionElement_1_t1335197220_gp_0_0_0_0 = { 2, GenInst_Type_t_0_0_0_ServiceModelExtensionCollectionElement_1_t1335197220_gp_0_0_0_0_Types };
extern const Il2CppType StandardBindingCollectionElement_2_t2904503531_gp_1_0_0_0;
static const Il2CppType* GenInst_StandardBindingCollectionElement_2_t2904503531_gp_1_0_0_0_Types[] = { &StandardBindingCollectionElement_2_t2904503531_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_StandardBindingCollectionElement_2_t2904503531_gp_1_0_0_0 = { 1, GenInst_StandardBindingCollectionElement_2_t2904503531_gp_1_0_0_0_Types };
extern const Il2CppType StandardBindingCollectionElement_2_t2904503531_gp_0_0_0_0;
static const Il2CppType* GenInst_StandardBindingCollectionElement_2_t2904503531_gp_0_0_0_0_StandardBindingCollectionElement_2_t2904503531_gp_1_0_0_0_Types[] = { &StandardBindingCollectionElement_2_t2904503531_gp_0_0_0_0, &StandardBindingCollectionElement_2_t2904503531_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_StandardBindingCollectionElement_2_t2904503531_gp_0_0_0_0_StandardBindingCollectionElement_2_t2904503531_gp_1_0_0_0 = { 2, GenInst_StandardBindingCollectionElement_2_t2904503531_gp_0_0_0_0_StandardBindingCollectionElement_2_t2904503531_gp_1_0_0_0_Types };
extern const Il2CppType ListenerLoopManager_CreateAcceptor_m3081673401_gp_0_0_0_0;
static const Il2CppType* GenInst_ListenerLoopManager_CreateAcceptor_m3081673401_gp_0_0_0_0_Types[] = { &ListenerLoopManager_CreateAcceptor_m3081673401_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ListenerLoopManager_CreateAcceptor_m3081673401_gp_0_0_0_0 = { 1, GenInst_ListenerLoopManager_CreateAcceptor_m3081673401_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateAcceptorU3Ec__AnonStorey1C_1_t1865474583_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateAcceptorU3Ec__AnonStorey1C_1_t1865474583_gp_0_0_0_0_Types[] = { &U3CCreateAcceptorU3Ec__AnonStorey1C_1_t1865474583_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateAcceptorU3Ec__AnonStorey1C_1_t1865474583_gp_0_0_0_0 = { 1, GenInst_U3CCreateAcceptorU3Ec__AnonStorey1C_1_t1865474583_gp_0_0_0_0_Types };
extern const Il2CppType ChannelFactory_1_t1254119128_gp_0_0_0_0;
static const Il2CppType* GenInst_ChannelFactory_1_t1254119128_gp_0_0_0_0_Types[] = { &ChannelFactory_1_t1254119128_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ChannelFactory_1_t1254119128_gp_0_0_0_0 = { 1, GenInst_ChannelFactory_1_t1254119128_gp_0_0_0_0_Types };
extern const Il2CppType ClientBase_1_t3896424024_gp_0_0_0_0;
static const Il2CppType* GenInst_ClientBase_1_t3896424024_gp_0_0_0_0_Types[] = { &ClientBase_1_t3896424024_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientBase_1_t3896424024_gp_0_0_0_0 = { 1, GenInst_ClientBase_1_t3896424024_gp_0_0_0_0_Types };
extern const Il2CppType DuplexChannelFactory_1_t1825049079_gp_0_0_0_0;
static const Il2CppType* GenInst_DuplexChannelFactory_1_t1825049079_gp_0_0_0_0_Types[] = { &DuplexChannelFactory_1_t1825049079_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DuplexChannelFactory_1_t1825049079_gp_0_0_0_0 = { 1, GenInst_DuplexChannelFactory_1_t1825049079_gp_0_0_0_0_Types };
extern const Il2CppType IExtension_1_t293605571_0_0_0;
static const Il2CppType* GenInst_IExtension_1_t293605571_0_0_0_Types[] = { &IExtension_1_t293605571_0_0_0 };
extern const Il2CppGenericInst GenInst_IExtension_1_t293605571_0_0_0 = { 1, GenInst_IExtension_1_t293605571_0_0_0_Types };
extern const Il2CppType ExtensionCollection_1_t4111789460_gp_0_0_0_0;
static const Il2CppType* GenInst_ExtensionCollection_1_t4111789460_gp_0_0_0_0_Types[] = { &ExtensionCollection_1_t4111789460_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExtensionCollection_1_t4111789460_gp_0_0_0_0 = { 1, GenInst_ExtensionCollection_1_t4111789460_gp_0_0_0_0_Types };
extern const Il2CppType IExtensibleObject_1_t289308579_gp_0_0_0_0;
static const Il2CppType* GenInst_IExtensibleObject_1_t289308579_gp_0_0_0_0_Types[] = { &IExtensibleObject_1_t289308579_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IExtensibleObject_1_t289308579_gp_0_0_0_0 = { 1, GenInst_IExtensibleObject_1_t289308579_gp_0_0_0_0_Types };
extern const Il2CppType IExtension_1_t3774563217_gp_0_0_0_0;
static const Il2CppType* GenInst_IExtension_1_t3774563217_gp_0_0_0_0_Types[] = { &IExtension_1_t3774563217_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IExtension_1_t3774563217_gp_0_0_0_0 = { 1, GenInst_IExtension_1_t3774563217_gp_0_0_0_0_Types };
extern const Il2CppType IExtension_1_t1118430608_0_0_0;
static const Il2CppType* GenInst_IExtension_1_t1118430608_0_0_0_Types[] = { &IExtension_1_t1118430608_0_0_0 };
extern const Il2CppGenericInst GenInst_IExtension_1_t1118430608_0_0_0 = { 1, GenInst_IExtension_1_t1118430608_0_0_0_Types };
extern const Il2CppType IExtensionCollection_1_t641647201_gp_0_0_0_0;
static const Il2CppType* GenInst_IExtensionCollection_1_t641647201_gp_0_0_0_0_Types[] = { &IExtensionCollection_1_t641647201_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IExtensionCollection_1_t641647201_gp_0_0_0_0 = { 1, GenInst_IExtensionCollection_1_t641647201_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_Execute_m513682320_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_Execute_m513682320_gp_0_0_0_0_Types[] = { &ExecuteEvents_Execute_m513682320_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m513682320_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_Execute_m513682320_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_ExecuteHierarchy_m2988888012_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_ExecuteHierarchy_m2988888012_gp_0_0_0_0_Types[] = { &ExecuteEvents_ExecuteHierarchy_m2988888012_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m2988888012_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_ExecuteHierarchy_m2988888012_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventList_m400222725_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventList_m400222725_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventList_m400222725_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m400222725_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventList_m400222725_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_CanHandleEvent_m1773513752_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_CanHandleEvent_m1773513752_gp_0_0_0_0_Types[] = { &ExecuteEvents_CanHandleEvent_m1773513752_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m1773513752_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_CanHandleEvent_m1773513752_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventHandler_m3154756557_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventHandler_m3154756557_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventHandler_m3154756557_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m3154756557_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventHandler_m3154756557_gp_0_0_0_0_Types };
extern const Il2CppType TweenRunner_1_t3844461449_gp_0_0_0_0;
static const Il2CppType* GenInst_TweenRunner_1_t3844461449_gp_0_0_0_0_Types[] = { &TweenRunner_1_t3844461449_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenRunner_1_t3844461449_gp_0_0_0_0 = { 1, GenInst_TweenRunner_1_t3844461449_gp_0_0_0_0_Types };
extern const Il2CppType Dropdown_GetOrAddComponent_m4080506703_gp_0_0_0_0;
static const Il2CppType* GenInst_Dropdown_GetOrAddComponent_m4080506703_gp_0_0_0_0_Types[] = { &Dropdown_GetOrAddComponent_m4080506703_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_GetOrAddComponent_m4080506703_gp_0_0_0_0 = { 1, GenInst_Dropdown_GetOrAddComponent_m4080506703_gp_0_0_0_0_Types };
extern const Il2CppType SetPropertyUtility_SetStruct_m2798032208_gp_0_0_0_0;
static const Il2CppType* GenInst_SetPropertyUtility_SetStruct_m2798032208_gp_0_0_0_0_Types[] = { &SetPropertyUtility_SetStruct_m2798032208_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SetPropertyUtility_SetStruct_m2798032208_gp_0_0_0_0 = { 1, GenInst_SetPropertyUtility_SetStruct_m2798032208_gp_0_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t2120020791_gp_0_0_0_0;
static const Il2CppType* GenInst_IndexedSet_1_t2120020791_gp_0_0_0_0_Types[] = { &IndexedSet_1_t2120020791_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t2120020791_gp_0_0_0_0 = { 1, GenInst_IndexedSet_1_t2120020791_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t2120020791_gp_0_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &IndexedSet_1_t2120020791_gp_0_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t2120020791_gp_0_0_0_0_Int32_t2950945753_0_0_0 = { 2, GenInst_IndexedSet_1_t2120020791_gp_0_0_0_0_Int32_t2950945753_0_0_0_Types };
extern const Il2CppType ListPool_1_t890186770_gp_0_0_0_0;
static const Il2CppType* GenInst_ListPool_1_t890186770_gp_0_0_0_0_Types[] = { &ListPool_1_t890186770_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ListPool_1_t890186770_gp_0_0_0_0 = { 1, GenInst_ListPool_1_t890186770_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t3009893961_0_0_0;
static const Il2CppType* GenInst_List_1_t3009893961_0_0_0_Types[] = { &List_1_t3009893961_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3009893961_0_0_0 = { 1, GenInst_List_1_t3009893961_0_0_0_Types };
extern const Il2CppType ObjectPool_1_t892185599_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_1_t892185599_gp_0_0_0_0_Types[] = { &ObjectPool_1_t892185599_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t892185599_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t892185599_gp_0_0_0_0_Types };
extern const Il2CppType BetterList_1_t2240351297_gp_0_0_0_0;
static const Il2CppType* GenInst_BetterList_1_t2240351297_gp_0_0_0_0_Types[] = { &BetterList_1_t2240351297_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BetterList_1_t2240351297_gp_0_0_0_0 = { 1, GenInst_BetterList_1_t2240351297_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t4155939275_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t4155939275_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t4155939275_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t4155939275_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t4155939275_gp_0_0_0_0_Types };
extern const Il2CppType NGUITools_AddChild_m4126302437_gp_0_0_0_0;
static const Il2CppType* GenInst_NGUITools_AddChild_m4126302437_gp_0_0_0_0_Types[] = { &NGUITools_AddChild_m4126302437_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NGUITools_AddChild_m4126302437_gp_0_0_0_0 = { 1, GenInst_NGUITools_AddChild_m4126302437_gp_0_0_0_0_Types };
extern const Il2CppType NGUITools_AddChild_m2329166398_gp_0_0_0_0;
static const Il2CppType* GenInst_NGUITools_AddChild_m2329166398_gp_0_0_0_0_Types[] = { &NGUITools_AddChild_m2329166398_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NGUITools_AddChild_m2329166398_gp_0_0_0_0 = { 1, GenInst_NGUITools_AddChild_m2329166398_gp_0_0_0_0_Types };
extern const Il2CppType NGUITools_AddWidget_m2648584008_gp_0_0_0_0;
static const Il2CppType* GenInst_NGUITools_AddWidget_m2648584008_gp_0_0_0_0_Types[] = { &NGUITools_AddWidget_m2648584008_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NGUITools_AddWidget_m2648584008_gp_0_0_0_0 = { 1, GenInst_NGUITools_AddWidget_m2648584008_gp_0_0_0_0_Types };
extern const Il2CppType NGUITools_AddWidget_m327501609_gp_0_0_0_0;
static const Il2CppType* GenInst_NGUITools_AddWidget_m327501609_gp_0_0_0_0_Types[] = { &NGUITools_AddWidget_m327501609_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NGUITools_AddWidget_m327501609_gp_0_0_0_0 = { 1, GenInst_NGUITools_AddWidget_m327501609_gp_0_0_0_0_Types };
extern const Il2CppType NGUITools_FindInParents_m3877358550_gp_0_0_0_0;
static const Il2CppType* GenInst_NGUITools_FindInParents_m3877358550_gp_0_0_0_0_Types[] = { &NGUITools_FindInParents_m3877358550_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NGUITools_FindInParents_m3877358550_gp_0_0_0_0 = { 1, GenInst_NGUITools_FindInParents_m3877358550_gp_0_0_0_0_Types };
extern const Il2CppType NGUITools_FindInParents_m1163017570_gp_0_0_0_0;
static const Il2CppType* GenInst_NGUITools_FindInParents_m1163017570_gp_0_0_0_0_Types[] = { &NGUITools_FindInParents_m1163017570_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NGUITools_FindInParents_m1163017570_gp_0_0_0_0 = { 1, GenInst_NGUITools_FindInParents_m1163017570_gp_0_0_0_0_Types };
extern const Il2CppType NGUITools_AddMissingComponent_m4019644757_gp_0_0_0_0;
static const Il2CppType* GenInst_NGUITools_AddMissingComponent_m4019644757_gp_0_0_0_0_Types[] = { &NGUITools_AddMissingComponent_m4019644757_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NGUITools_AddMissingComponent_m4019644757_gp_0_0_0_0 = { 1, GenInst_NGUITools_AddMissingComponent_m4019644757_gp_0_0_0_0_Types };
extern const Il2CppType NGUITools_Execute_m517571378_gp_0_0_0_0;
static const Il2CppType* GenInst_NGUITools_Execute_m517571378_gp_0_0_0_0_Types[] = { &NGUITools_Execute_m517571378_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NGUITools_Execute_m517571378_gp_0_0_0_0 = { 1, GenInst_NGUITools_Execute_m517571378_gp_0_0_0_0_Types };
extern const Il2CppType NGUITools_ExecuteAll_m1520266422_gp_0_0_0_0;
static const Il2CppType* GenInst_NGUITools_ExecuteAll_m1520266422_gp_0_0_0_0_Types[] = { &NGUITools_ExecuteAll_m1520266422_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NGUITools_ExecuteAll_m1520266422_gp_0_0_0_0 = { 1, GenInst_NGUITools_ExecuteAll_m1520266422_gp_0_0_0_0_Types };
extern const Il2CppType UITweener_Begin_m3540790004_gp_0_0_0_0;
static const Il2CppType* GenInst_UITweener_Begin_m3540790004_gp_0_0_0_0_Types[] = { &UITweener_Begin_m3540790004_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UITweener_Begin_m3540790004_gp_0_0_0_0 = { 1, GenInst_UITweener_Begin_m3540790004_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_CodeTypeMember_t1555525554_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &CodeTypeMember_t1555525554_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_CodeTypeMember_t1555525554_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_CodeTypeMember_t1555525554_0_0_0_Types };
extern const Il2CppType SerializableAttribute_t1992588303_0_0_0;
static const Il2CppType* GenInst_SerializableAttribute_t1992588303_0_0_0_Types[] = { &SerializableAttribute_t1992588303_0_0_0 };
extern const Il2CppGenericInst GenInst_SerializableAttribute_t1992588303_0_0_0 = { 1, GenInst_SerializableAttribute_t1992588303_0_0_0_Types };
extern const Il2CppType DataContractAttribute_t412496005_0_0_0;
static const Il2CppType* GenInst_DataContractAttribute_t412496005_0_0_0_Types[] = { &DataContractAttribute_t412496005_0_0_0 };
extern const Il2CppGenericInst GenInst_DataContractAttribute_t412496005_0_0_0 = { 1, GenInst_DataContractAttribute_t412496005_0_0_0_Types };
extern const Il2CppType CollectionDataContractAttribute_t3671020647_0_0_0;
static const Il2CppType* GenInst_CollectionDataContractAttribute_t3671020647_0_0_0_Types[] = { &CollectionDataContractAttribute_t3671020647_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionDataContractAttribute_t3671020647_0_0_0 = { 1, GenInst_CollectionDataContractAttribute_t3671020647_0_0_0_Types };
extern const Il2CppType XmlRootAttribute_t2306097217_0_0_0;
static const Il2CppType* GenInst_XmlRootAttribute_t2306097217_0_0_0_Types[] = { &XmlRootAttribute_t2306097217_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlRootAttribute_t2306097217_0_0_0 = { 1, GenInst_XmlRootAttribute_t2306097217_0_0_0_Types };
extern const Il2CppType CompilationSection_t1832220892_0_0_0;
static const Il2CppType* GenInst_CompilationSection_t1832220892_0_0_0_Types[] = { &CompilationSection_t1832220892_0_0_0 };
extern const Il2CppGenericInst GenInst_CompilationSection_t1832220892_0_0_0 = { 1, GenInst_CompilationSection_t1832220892_0_0_0_Types };
extern const Il2CppType ClientTargetSection_t572227672_0_0_0;
static const Il2CppType* GenInst_ClientTargetSection_t572227672_0_0_0_Types[] = { &ClientTargetSection_t572227672_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientTargetSection_t572227672_0_0_0 = { 1, GenInst_ClientTargetSection_t572227672_0_0_0_Types };
extern const Il2CppType PagesSection_t1064701840_0_0_0;
static const Il2CppType* GenInst_PagesSection_t1064701840_0_0_0_Types[] = { &PagesSection_t1064701840_0_0_0 };
extern const Il2CppGenericInst GenInst_PagesSection_t1064701840_0_0_0 = { 1, GenInst_PagesSection_t1064701840_0_0_0_Types };
extern const Il2CppType SecurityKeyUsage_t3512984903_0_0_0;
static const Il2CppType* GenInst_SecurityKeyUsage_t3512984903_0_0_0_Types[] = { &SecurityKeyUsage_t3512984903_0_0_0 };
extern const Il2CppGenericInst GenInst_SecurityKeyUsage_t3512984903_0_0_0 = { 1, GenInst_SecurityKeyUsage_t3512984903_0_0_0_Types };
extern const Il2CppType DefaultExecutionOrder_t3059642329_0_0_0;
static const Il2CppType* GenInst_DefaultExecutionOrder_t3059642329_0_0_0_Types[] = { &DefaultExecutionOrder_t3059642329_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultExecutionOrder_t3059642329_0_0_0 = { 1, GenInst_DefaultExecutionOrder_t3059642329_0_0_0_Types };
extern const Il2CppType PlayerConnection_t3081694049_0_0_0;
static const Il2CppType* GenInst_PlayerConnection_t3081694049_0_0_0_Types[] = { &PlayerConnection_t3081694049_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayerConnection_t3081694049_0_0_0 = { 1, GenInst_PlayerConnection_t3081694049_0_0_0_Types };
extern const Il2CppType ParticleSystem_t1800779281_0_0_0;
static const Il2CppType* GenInst_ParticleSystem_t1800779281_0_0_0_Types[] = { &ParticleSystem_t1800779281_0_0_0 };
extern const Il2CppGenericInst GenInst_ParticleSystem_t1800779281_0_0_0 = { 1, GenInst_ParticleSystem_t1800779281_0_0_0_Types };
extern const Il2CppType GUILayer_t2783472903_0_0_0;
static const Il2CppType* GenInst_GUILayer_t2783472903_0_0_0_Types[] = { &GUILayer_t2783472903_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayer_t2783472903_0_0_0 = { 1, GenInst_GUILayer_t2783472903_0_0_0_Types };
extern const Il2CppType ObjectTracker_t4177997237_0_0_0;
static const Il2CppType* GenInst_ObjectTracker_t4177997237_0_0_0_Types[] = { &ObjectTracker_t4177997237_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectTracker_t4177997237_0_0_0 = { 1, GenInst_ObjectTracker_t4177997237_0_0_0_Types };
extern const Il2CppType SmartTerrainTracker_t1238706968_0_0_0;
static const Il2CppType* GenInst_SmartTerrainTracker_t1238706968_0_0_0_Types[] = { &SmartTerrainTracker_t1238706968_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainTracker_t1238706968_0_0_0 = { 1, GenInst_SmartTerrainTracker_t1238706968_0_0_0_Types };
extern const Il2CppType DeviceTracker_t2315692373_0_0_0;
static const Il2CppType* GenInst_DeviceTracker_t2315692373_0_0_0_Types[] = { &DeviceTracker_t2315692373_0_0_0 };
extern const Il2CppGenericInst GenInst_DeviceTracker_t2315692373_0_0_0 = { 1, GenInst_DeviceTracker_t2315692373_0_0_0_Types };
extern const Il2CppType RotationalDeviceTracker_t2847210804_0_0_0;
static const Il2CppType* GenInst_RotationalDeviceTracker_t2847210804_0_0_0_Types[] = { &RotationalDeviceTracker_t2847210804_0_0_0 };
extern const Il2CppGenericInst GenInst_RotationalDeviceTracker_t2847210804_0_0_0 = { 1, GenInst_RotationalDeviceTracker_t2847210804_0_0_0_Types };
extern const Il2CppType DistortionRenderingBehaviour_t1858983148_0_0_0;
static const Il2CppType* GenInst_DistortionRenderingBehaviour_t1858983148_0_0_0_Types[] = { &DistortionRenderingBehaviour_t1858983148_0_0_0 };
extern const Il2CppGenericInst GenInst_DistortionRenderingBehaviour_t1858983148_0_0_0 = { 1, GenInst_DistortionRenderingBehaviour_t1858983148_0_0_0_Types };
extern const Il2CppType ReconstructionFromTarget_t3809448279_0_0_0;
static const Il2CppType* GenInst_ReconstructionFromTarget_t3809448279_0_0_0_Types[] = { &ReconstructionFromTarget_t3809448279_0_0_0 };
extern const Il2CppGenericInst GenInst_ReconstructionFromTarget_t3809448279_0_0_0 = { 1, GenInst_ReconstructionFromTarget_t3809448279_0_0_0_Types };
extern const Il2CppType TextTracker_t3950053289_0_0_0;
static const Il2CppType* GenInst_TextTracker_t3950053289_0_0_0_Types[] = { &TextTracker_t3950053289_0_0_0 };
extern const Il2CppGenericInst GenInst_TextTracker_t3950053289_0_0_0 = { 1, GenInst_TextTracker_t3950053289_0_0_0_Types };
extern const Il2CppType VuforiaAbstractConfiguration_t2684447159_0_0_0;
static const Il2CppType* GenInst_VuforiaAbstractConfiguration_t2684447159_0_0_0_Types[] = { &VuforiaAbstractConfiguration_t2684447159_0_0_0 };
extern const Il2CppGenericInst GenInst_VuforiaAbstractConfiguration_t2684447159_0_0_0 = { 1, GenInst_VuforiaAbstractConfiguration_t2684447159_0_0_0_Types };
extern const Il2CppType VuforiaDeinitBehaviour_t1700985552_0_0_0;
static const Il2CppType* GenInst_VuforiaDeinitBehaviour_t1700985552_0_0_0_Types[] = { &VuforiaDeinitBehaviour_t1700985552_0_0_0 };
extern const Il2CppGenericInst GenInst_VuforiaDeinitBehaviour_t1700985552_0_0_0 = { 1, GenInst_VuforiaDeinitBehaviour_t1700985552_0_0_0_Types };
extern const Il2CppType MetadataPublishingInfo_t2085662404_0_0_0;
static const Il2CppType* GenInst_MetadataPublishingInfo_t2085662404_0_0_0_Types[] = { &MetadataPublishingInfo_t2085662404_0_0_0 };
extern const Il2CppGenericInst GenInst_MetadataPublishingInfo_t2085662404_0_0_0 = { 1, GenInst_MetadataPublishingInfo_t2085662404_0_0_0_Types };
extern const Il2CppType HttpListenerManager_t555124713_0_0_0;
static const Il2CppType* GenInst_HttpListenerManager_t555124713_0_0_0_Types[] = { &HttpListenerManager_t555124713_0_0_0 };
extern const Il2CppGenericInst GenInst_HttpListenerManager_t555124713_0_0_0 = { 1, GenInst_HttpListenerManager_t555124713_0_0_0_Types };
extern const Il2CppType EndpointAddress_t3119842923_0_0_0;
static const Il2CppType* GenInst_EndpointAddress_t3119842923_0_0_0_Types[] = { &EndpointAddress_t3119842923_0_0_0 };
extern const Il2CppGenericInst GenInst_EndpointAddress_t3119842923_0_0_0 = { 1, GenInst_EndpointAddress_t3119842923_0_0_0_Types };
extern const Il2CppType ServiceBehaviorAttribute_t1657799891_0_0_0;
static const Il2CppType* GenInst_ServiceBehaviorAttribute_t1657799891_0_0_0_Types[] = { &ServiceBehaviorAttribute_t1657799891_0_0_0 };
extern const Il2CppGenericInst GenInst_ServiceBehaviorAttribute_t1657799891_0_0_0 = { 1, GenInst_ServiceBehaviorAttribute_t1657799891_0_0_0_Types };
extern const Il2CppType IPeerConnectorContract_t3354905095_0_0_0;
static const Il2CppType* GenInst_IPeerConnectorContract_t3354905095_0_0_0_Types[] = { &IPeerConnectorContract_t3354905095_0_0_0 };
extern const Il2CppGenericInst GenInst_IPeerConnectorContract_t3354905095_0_0_0 = { 1, GenInst_IPeerConnectorContract_t3354905095_0_0_0_Types };
extern const Il2CppType Binding_t859993683_0_0_0;
static const Il2CppType* GenInst_Binding_t859993683_0_0_0_Types[] = { &Binding_t859993683_0_0_0 };
extern const Il2CppGenericInst GenInst_Binding_t859993683_0_0_0 = { 1, GenInst_Binding_t859993683_0_0_0_Types };
extern const Il2CppType BindingContext_t2842489830_0_0_0;
static const Il2CppType* GenInst_BindingContext_t2842489830_0_0_0_Types[] = { &BindingContext_t2842489830_0_0_0 };
extern const Il2CppGenericInst GenInst_BindingContext_t2842489830_0_0_0 = { 1, GenInst_BindingContext_t2842489830_0_0_0_Types };
extern const Il2CppType SecurityTokenVersion_t1981162148_0_0_0;
static const Il2CppType* GenInst_SecurityTokenVersion_t1981162148_0_0_0_Types[] = { &SecurityTokenVersion_t1981162148_0_0_0 };
extern const Il2CppGenericInst GenInst_SecurityTokenVersion_t1981162148_0_0_0 = { 1, GenInst_SecurityTokenVersion_t1981162148_0_0_0_Types };
extern const Il2CppType SecurityAlgorithmSuite_t2980594242_0_0_0;
static const Il2CppType* GenInst_SecurityAlgorithmSuite_t2980594242_0_0_0_Types[] = { &SecurityAlgorithmSuite_t2980594242_0_0_0 };
extern const Il2CppGenericInst GenInst_SecurityAlgorithmSuite_t2980594242_0_0_0 = { 1, GenInst_SecurityAlgorithmSuite_t2980594242_0_0_0_Types };
extern const Il2CppType OperationBehaviorAttribute_t1398166452_0_0_0;
static const Il2CppType* GenInst_OperationBehaviorAttribute_t1398166452_0_0_0_Types[] = { &OperationBehaviorAttribute_t1398166452_0_0_0 };
extern const Il2CppGenericInst GenInst_OperationBehaviorAttribute_t1398166452_0_0_0 = { 1, GenInst_OperationBehaviorAttribute_t1398166452_0_0_0_Types };
extern const Il2CppType ServiceMetadataExtension_t3543702878_0_0_0;
static const Il2CppType* GenInst_ServiceMetadataExtension_t3543702878_0_0_0_Types[] = { &ServiceMetadataExtension_t3543702878_0_0_0 };
extern const Il2CppGenericInst GenInst_ServiceMetadataExtension_t3543702878_0_0_0 = { 1, GenInst_ServiceMetadataExtension_t3543702878_0_0_0_Types };
extern const Il2CppType MessageEncodingBindingElement_t3531153504_0_0_0;
static const Il2CppType* GenInst_MessageEncodingBindingElement_t3531153504_0_0_0_Types[] = { &MessageEncodingBindingElement_t3531153504_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageEncodingBindingElement_t3531153504_0_0_0 = { 1, GenInst_MessageEncodingBindingElement_t3531153504_0_0_0_Types };
extern const Il2CppType MessageSecurityBindingSupport_t1904510157_0_0_0;
static const Il2CppType* GenInst_MessageSecurityBindingSupport_t1904510157_0_0_0_Types[] = { &MessageSecurityBindingSupport_t1904510157_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageSecurityBindingSupport_t1904510157_0_0_0 = { 1, GenInst_MessageSecurityBindingSupport_t1904510157_0_0_0_Types };
extern const Il2CppType ISecurityCapabilities_t4129699402_0_0_0;
static const Il2CppType* GenInst_ISecurityCapabilities_t4129699402_0_0_0_Types[] = { &ISecurityCapabilities_t4129699402_0_0_0 };
extern const Il2CppGenericInst GenInst_ISecurityCapabilities_t4129699402_0_0_0 = { 1, GenInst_ISecurityCapabilities_t4129699402_0_0_0_Types };
extern const Il2CppType X509IssuerSerialKeyIdentifierClause_t3285382384_0_0_0;
static const Il2CppType* GenInst_X509IssuerSerialKeyIdentifierClause_t3285382384_0_0_0_Types[] = { &X509IssuerSerialKeyIdentifierClause_t3285382384_0_0_0 };
extern const Il2CppGenericInst GenInst_X509IssuerSerialKeyIdentifierClause_t3285382384_0_0_0 = { 1, GenInst_X509IssuerSerialKeyIdentifierClause_t3285382384_0_0_0_Types };
extern const Il2CppType X509ThumbprintKeyIdentifierClause_t1082272133_0_0_0;
static const Il2CppType* GenInst_X509ThumbprintKeyIdentifierClause_t1082272133_0_0_0_Types[] = { &X509ThumbprintKeyIdentifierClause_t1082272133_0_0_0 };
extern const Il2CppGenericInst GenInst_X509ThumbprintKeyIdentifierClause_t1082272133_0_0_0 = { 1, GenInst_X509ThumbprintKeyIdentifierClause_t1082272133_0_0_0_Types };
extern const Il2CppType X509SubjectKeyIdentifierClause_t676849360_0_0_0;
static const Il2CppType* GenInst_X509SubjectKeyIdentifierClause_t676849360_0_0_0_Types[] = { &X509SubjectKeyIdentifierClause_t676849360_0_0_0 };
extern const Il2CppGenericInst GenInst_X509SubjectKeyIdentifierClause_t676849360_0_0_0 = { 1, GenInst_X509SubjectKeyIdentifierClause_t676849360_0_0_0_Types };
extern const Il2CppType X509RawDataKeyIdentifierClause_t329336726_0_0_0;
static const Il2CppType* GenInst_X509RawDataKeyIdentifierClause_t329336726_0_0_0_Types[] = { &X509RawDataKeyIdentifierClause_t329336726_0_0_0 };
extern const Il2CppGenericInst GenInst_X509RawDataKeyIdentifierClause_t329336726_0_0_0 = { 1, GenInst_X509RawDataKeyIdentifierClause_t329336726_0_0_0_Types };
extern const Il2CppType ServiceMetadataBehavior_t566713614_0_0_0;
static const Il2CppType* GenInst_ServiceMetadataBehavior_t566713614_0_0_0_Types[] = { &ServiceMetadataBehavior_t566713614_0_0_0 };
extern const Il2CppGenericInst GenInst_ServiceMetadataBehavior_t566713614_0_0_0 = { 1, GenInst_ServiceMetadataBehavior_t566713614_0_0_0_Types };
extern const Il2CppType ServiceAuthorizationBehavior_t248521010_0_0_0;
static const Il2CppType* GenInst_ServiceAuthorizationBehavior_t248521010_0_0_0_Types[] = { &ServiceAuthorizationBehavior_t248521010_0_0_0 };
extern const Il2CppGenericInst GenInst_ServiceAuthorizationBehavior_t248521010_0_0_0 = { 1, GenInst_ServiceAuthorizationBehavior_t248521010_0_0_0_Types };
extern const Il2CppType ServiceDebugBehavior_t879073333_0_0_0;
static const Il2CppType* GenInst_ServiceDebugBehavior_t879073333_0_0_0_Types[] = { &ServiceDebugBehavior_t879073333_0_0_0 };
extern const Il2CppGenericInst GenInst_ServiceDebugBehavior_t879073333_0_0_0 = { 1, GenInst_ServiceDebugBehavior_t879073333_0_0_0_Types };
extern const Il2CppType EventSystem_t1003666588_0_0_0;
static const Il2CppType* GenInst_EventSystem_t1003666588_0_0_0_Types[] = { &EventSystem_t1003666588_0_0_0 };
extern const Il2CppGenericInst GenInst_EventSystem_t1003666588_0_0_0 = { 1, GenInst_EventSystem_t1003666588_0_0_0_Types };
extern const Il2CppType AxisEventData_t2331243652_0_0_0;
static const Il2CppType* GenInst_AxisEventData_t2331243652_0_0_0_Types[] = { &AxisEventData_t2331243652_0_0_0 };
extern const Il2CppGenericInst GenInst_AxisEventData_t2331243652_0_0_0 = { 1, GenInst_AxisEventData_t2331243652_0_0_0_Types };
extern const Il2CppType RawImage_t3182918964_0_0_0;
static const Il2CppType* GenInst_RawImage_t3182918964_0_0_0_Types[] = { &RawImage_t3182918964_0_0_0 };
extern const Il2CppGenericInst GenInst_RawImage_t3182918964_0_0_0 = { 1, GenInst_RawImage_t3182918964_0_0_0_Types };
extern const Il2CppType Slider_t3903728902_0_0_0;
static const Il2CppType* GenInst_Slider_t3903728902_0_0_0_Types[] = { &Slider_t3903728902_0_0_0 };
extern const Il2CppGenericInst GenInst_Slider_t3903728902_0_0_0 = { 1, GenInst_Slider_t3903728902_0_0_0_Types };
extern const Il2CppType Scrollbar_t1494447233_0_0_0;
static const Il2CppType* GenInst_Scrollbar_t1494447233_0_0_0_Types[] = { &Scrollbar_t1494447233_0_0_0 };
extern const Il2CppGenericInst GenInst_Scrollbar_t1494447233_0_0_0 = { 1, GenInst_Scrollbar_t1494447233_0_0_0_Types };
extern const Il2CppType InputField_t3762917431_0_0_0;
static const Il2CppType* GenInst_InputField_t3762917431_0_0_0_Types[] = { &InputField_t3762917431_0_0_0 };
extern const Il2CppGenericInst GenInst_InputField_t3762917431_0_0_0 = { 1, GenInst_InputField_t3762917431_0_0_0_Types };
extern const Il2CppType ScrollRect_t4137855814_0_0_0;
static const Il2CppType* GenInst_ScrollRect_t4137855814_0_0_0_Types[] = { &ScrollRect_t4137855814_0_0_0 };
extern const Il2CppGenericInst GenInst_ScrollRect_t4137855814_0_0_0 = { 1, GenInst_ScrollRect_t4137855814_0_0_0_Types };
extern const Il2CppType Dropdown_t2274391225_0_0_0;
static const Il2CppType* GenInst_Dropdown_t2274391225_0_0_0_Types[] = { &Dropdown_t2274391225_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_t2274391225_0_0_0 = { 1, GenInst_Dropdown_t2274391225_0_0_0_Types };
extern const Il2CppType GraphicRaycaster_t2999697109_0_0_0;
static const Il2CppType* GenInst_GraphicRaycaster_t2999697109_0_0_0_Types[] = { &GraphicRaycaster_t2999697109_0_0_0 };
extern const Il2CppGenericInst GenInst_GraphicRaycaster_t2999697109_0_0_0 = { 1, GenInst_GraphicRaycaster_t2999697109_0_0_0_Types };
extern const Il2CppType CanvasRenderer_t2598313366_0_0_0;
static const Il2CppType* GenInst_CanvasRenderer_t2598313366_0_0_0_Types[] = { &CanvasRenderer_t2598313366_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasRenderer_t2598313366_0_0_0 = { 1, GenInst_CanvasRenderer_t2598313366_0_0_0_Types };
extern const Il2CppType Corner_t1493259673_0_0_0;
static const Il2CppType* GenInst_Corner_t1493259673_0_0_0_Types[] = { &Corner_t1493259673_0_0_0 };
extern const Il2CppGenericInst GenInst_Corner_t1493259673_0_0_0 = { 1, GenInst_Corner_t1493259673_0_0_0_Types };
extern const Il2CppType Axis_t3613393006_0_0_0;
static const Il2CppType* GenInst_Axis_t3613393006_0_0_0_Types[] = { &Axis_t3613393006_0_0_0 };
extern const Il2CppGenericInst GenInst_Axis_t3613393006_0_0_0 = { 1, GenInst_Axis_t3613393006_0_0_0_Types };
extern const Il2CppType Constraint_t814224393_0_0_0;
static const Il2CppType* GenInst_Constraint_t814224393_0_0_0_Types[] = { &Constraint_t814224393_0_0_0 };
extern const Il2CppGenericInst GenInst_Constraint_t814224393_0_0_0 = { 1, GenInst_Constraint_t814224393_0_0_0_Types };
extern const Il2CppType SubmitEvent_t648412432_0_0_0;
static const Il2CppType* GenInst_SubmitEvent_t648412432_0_0_0_Types[] = { &SubmitEvent_t648412432_0_0_0 };
extern const Il2CppGenericInst GenInst_SubmitEvent_t648412432_0_0_0 = { 1, GenInst_SubmitEvent_t648412432_0_0_0_Types };
extern const Il2CppType OnChangeEvent_t467195904_0_0_0;
static const Il2CppType* GenInst_OnChangeEvent_t467195904_0_0_0_Types[] = { &OnChangeEvent_t467195904_0_0_0 };
extern const Il2CppGenericInst GenInst_OnChangeEvent_t467195904_0_0_0 = { 1, GenInst_OnChangeEvent_t467195904_0_0_0_Types };
extern const Il2CppType OnValidateInput_t2355412304_0_0_0;
static const Il2CppType* GenInst_OnValidateInput_t2355412304_0_0_0_Types[] = { &OnValidateInput_t2355412304_0_0_0 };
extern const Il2CppGenericInst GenInst_OnValidateInput_t2355412304_0_0_0 = { 1, GenInst_OnValidateInput_t2355412304_0_0_0_Types };
extern const Il2CppType LayoutElement_t1785403678_0_0_0;
static const Il2CppType* GenInst_LayoutElement_t1785403678_0_0_0_Types[] = { &LayoutElement_t1785403678_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutElement_t1785403678_0_0_0 = { 1, GenInst_LayoutElement_t1785403678_0_0_0_Types };
extern const Il2CppType RectOffset_t1369453676_0_0_0;
static const Il2CppType* GenInst_RectOffset_t1369453676_0_0_0_Types[] = { &RectOffset_t1369453676_0_0_0 };
extern const Il2CppGenericInst GenInst_RectOffset_t1369453676_0_0_0 = { 1, GenInst_RectOffset_t1369453676_0_0_0_Types };
extern const Il2CppType TextAnchor_t2035777396_0_0_0;
static const Il2CppType* GenInst_TextAnchor_t2035777396_0_0_0_Types[] = { &TextAnchor_t2035777396_0_0_0 };
extern const Il2CppGenericInst GenInst_TextAnchor_t2035777396_0_0_0 = { 1, GenInst_TextAnchor_t2035777396_0_0_0_Types };
extern const Il2CppType AnimationTriggers_t2532145056_0_0_0;
static const Il2CppType* GenInst_AnimationTriggers_t2532145056_0_0_0_Types[] = { &AnimationTriggers_t2532145056_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationTriggers_t2532145056_0_0_0 = { 1, GenInst_AnimationTriggers_t2532145056_0_0_0_Types };
extern const Il2CppType GUITexture_t951903601_0_0_0;
static const Il2CppType* GenInst_GUITexture_t951903601_0_0_0_Types[] = { &GUITexture_t951903601_0_0_0 };
extern const Il2CppGenericInst GenInst_GUITexture_t951903601_0_0_0 = { 1, GenInst_GUITexture_t951903601_0_0_0_Types };
extern const Il2CppType GUIText_t402233326_0_0_0;
static const Il2CppType* GenInst_GUIText_t402233326_0_0_0_Types[] = { &GUIText_t402233326_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIText_t402233326_0_0_0 = { 1, GenInst_GUIText_t402233326_0_0_0_Types };
extern const Il2CppType Light_t3756812086_0_0_0;
static const Il2CppType* GenInst_Light_t3756812086_0_0_0_Types[] = { &Light_t3756812086_0_0_0 };
extern const Il2CppGenericInst GenInst_Light_t3756812086_0_0_0 = { 1, GenInst_Light_t3756812086_0_0_0_Types };
extern const Il2CppType AudioSource_t3935305588_0_0_0;
static const Il2CppType* GenInst_AudioSource_t3935305588_0_0_0_Types[] = { &AudioSource_t3935305588_0_0_0 };
extern const Il2CppGenericInst GenInst_AudioSource_t3935305588_0_0_0 = { 1, GenInst_AudioSource_t3935305588_0_0_0_Types };
extern const Il2CppType Rigidbody_t3916780224_0_0_0;
static const Il2CppType* GenInst_Rigidbody_t3916780224_0_0_0_Types[] = { &Rigidbody_t3916780224_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody_t3916780224_0_0_0 = { 1, GenInst_Rigidbody_t3916780224_0_0_0_Types };
extern const Il2CppType Texture2D_t3840446185_0_0_0;
static const Il2CppType* GenInst_Texture2D_t3840446185_0_0_0_Types[] = { &Texture2D_t3840446185_0_0_0 };
extern const Il2CppGenericInst GenInst_Texture2D_t3840446185_0_0_0 = { 1, GenInst_Texture2D_t3840446185_0_0_0_Types };
extern const Il2CppType ActiveAnimation_t3475256642_0_0_0;
static const Il2CppType* GenInst_ActiveAnimation_t3475256642_0_0_0_Types[] = { &ActiveAnimation_t3475256642_0_0_0 };
extern const Il2CppGenericInst GenInst_ActiveAnimation_t3475256642_0_0_0 = { 1, GenInst_ActiveAnimation_t3475256642_0_0_0_Types };
extern const Il2CppType HighlighterController_t3493614325_0_0_0;
static const Il2CppType* GenInst_HighlighterController_t3493614325_0_0_0_Types[] = { &HighlighterController_t3493614325_0_0_0 };
extern const Il2CppGenericInst GenInst_HighlighterController_t3493614325_0_0_0 = { 1, GenInst_HighlighterController_t3493614325_0_0_0_Types };
extern const Il2CppType UIInput_t421821618_0_0_0;
static const Il2CppType* GenInst_UIInput_t421821618_0_0_0_Types[] = { &UIInput_t421821618_0_0_0 };
extern const Il2CppGenericInst GenInst_UIInput_t421821618_0_0_0 = { 1, GenInst_UIInput_t421821618_0_0_0_Types };
extern const Il2CppType UITexture_t3471168817_0_0_0;
static const Il2CppType* GenInst_UITexture_t3471168817_0_0_0_Types[] = { &UITexture_t3471168817_0_0_0 };
extern const Il2CppGenericInst GenInst_UITexture_t3471168817_0_0_0 = { 1, GenInst_UITexture_t3471168817_0_0_0_Types };
extern const Il2CppType BoxCollider_t1640800422_0_0_0;
static const Il2CppType* GenInst_BoxCollider_t1640800422_0_0_0_Types[] = { &BoxCollider_t1640800422_0_0_0 };
extern const Il2CppGenericInst GenInst_BoxCollider_t1640800422_0_0_0 = { 1, GenInst_BoxCollider_t1640800422_0_0_0_Types };
extern const Il2CppType InvEquipment_t3413562611_0_0_0;
static const Il2CppType* GenInst_InvEquipment_t3413562611_0_0_0_Types[] = { &InvEquipment_t3413562611_0_0_0 };
extern const Il2CppGenericInst GenInst_InvEquipment_t3413562611_0_0_0 = { 1, GenInst_InvEquipment_t3413562611_0_0_0_Types };
extern const Il2CppType ExampleDragDropSurface_t2709993285_0_0_0;
static const Il2CppType* GenInst_ExampleDragDropSurface_t2709993285_0_0_0_Types[] = { &ExampleDragDropSurface_t2709993285_0_0_0 };
extern const Il2CppGenericInst GenInst_ExampleDragDropSurface_t2709993285_0_0_0 = { 1, GenInst_ExampleDragDropSurface_t2709993285_0_0_0_Types };
extern const Il2CppType UIPopupList_t4167399471_0_0_0;
static const Il2CppType* GenInst_UIPopupList_t4167399471_0_0_0_Types[] = { &UIPopupList_t4167399471_0_0_0 };
extern const Il2CppGenericInst GenInst_UIPopupList_t4167399471_0_0_0 = { 1, GenInst_UIPopupList_t4167399471_0_0_0_Types };
extern const Il2CppType Win_Panel_Controller_t2195695921_0_0_0;
static const Il2CppType* GenInst_Win_Panel_Controller_t2195695921_0_0_0_Types[] = { &Win_Panel_Controller_t2195695921_0_0_0 };
extern const Il2CppGenericInst GenInst_Win_Panel_Controller_t2195695921_0_0_0 = { 1, GenInst_Win_Panel_Controller_t2195695921_0_0_0_Types };
extern const Il2CppType Scrollbar_Controller_t2992837185_0_0_0;
static const Il2CppType* GenInst_Scrollbar_Controller_t2992837185_0_0_0_Types[] = { &Scrollbar_Controller_t2992837185_0_0_0 };
extern const Il2CppGenericInst GenInst_Scrollbar_Controller_t2992837185_0_0_0 = { 1, GenInst_Scrollbar_Controller_t2992837185_0_0_0_Types };
extern const Il2CppType Tips_Controller_t170528621_0_0_0;
static const Il2CppType* GenInst_Tips_Controller_t170528621_0_0_0_Types[] = { &Tips_Controller_t170528621_0_0_0 };
extern const Il2CppGenericInst GenInst_Tips_Controller_t170528621_0_0_0 = { 1, GenInst_Tips_Controller_t170528621_0_0_0_Types };
extern const Il2CppType TextAsset_t3022178571_0_0_0;
static const Il2CppType* GenInst_TextAsset_t3022178571_0_0_0_Types[] = { &TextAsset_t3022178571_0_0_0 };
extern const Il2CppGenericInst GenInst_TextAsset_t3022178571_0_0_0 = { 1, GenInst_TextAsset_t3022178571_0_0_0_Types };
extern const Il2CppType GameManager_t1536523654_0_0_0;
static const Il2CppType* GenInst_GameManager_t1536523654_0_0_0_Types[] = { &GameManager_t1536523654_0_0_0 };
extern const Il2CppGenericInst GenInst_GameManager_t1536523654_0_0_0 = { 1, GenInst_GameManager_t1536523654_0_0_0_Types };
extern const Il2CppType NGUIDebug_t787955914_0_0_0;
static const Il2CppType* GenInst_NGUIDebug_t787955914_0_0_0_Types[] = { &NGUIDebug_t787955914_0_0_0 };
extern const Il2CppGenericInst GenInst_NGUIDebug_t787955914_0_0_0 = { 1, GenInst_NGUIDebug_t787955914_0_0_0_Types };
extern const Il2CppType BoxCollider2D_t3581341831_0_0_0;
static const Il2CppType* GenInst_BoxCollider2D_t3581341831_0_0_0_Types[] = { &BoxCollider2D_t3581341831_0_0_0 };
extern const Il2CppGenericInst GenInst_BoxCollider2D_t3581341831_0_0_0 = { 1, GenInst_BoxCollider2D_t3581341831_0_0_0_Types };
extern const Il2CppType UIAnchor_t2527798900_0_0_0;
static const Il2CppType* GenInst_UIAnchor_t2527798900_0_0_0_Types[] = { &UIAnchor_t2527798900_0_0_0 };
extern const Il2CppGenericInst GenInst_UIAnchor_t2527798900_0_0_0 = { 1, GenInst_UIAnchor_t2527798900_0_0_0_Types };
extern const Il2CppType Animation_t3648466861_0_0_0;
static const Il2CppType* GenInst_Animation_t3648466861_0_0_0_Types[] = { &Animation_t3648466861_0_0_0 };
extern const Il2CppGenericInst GenInst_Animation_t3648466861_0_0_0 = { 1, GenInst_Animation_t3648466861_0_0_0_Types };
extern const Il2CppType Change_Sprite_t4001473290_0_0_0;
static const Il2CppType* GenInst_Change_Sprite_t4001473290_0_0_0_Types[] = { &Change_Sprite_t4001473290_0_0_0 };
extern const Il2CppGenericInst GenInst_Change_Sprite_t4001473290_0_0_0 = { 1, GenInst_Change_Sprite_t4001473290_0_0_0_Types };
extern const Il2CppType ObjectPuter_t3420787522_0_0_0;
static const Il2CppType* GenInst_ObjectPuter_t3420787522_0_0_0_Types[] = { &ObjectPuter_t3420787522_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPuter_t3420787522_0_0_0 = { 1, GenInst_ObjectPuter_t3420787522_0_0_0_Types };
extern const Il2CppType Coroutine_t3196085901_0_0_0;
static const Il2CppType* GenInst_Coroutine_t3196085901_0_0_0_Types[] = { &Coroutine_t3196085901_0_0_0 };
extern const Il2CppGenericInst GenInst_Coroutine_t3196085901_0_0_0 = { 1, GenInst_Coroutine_t3196085901_0_0_0_Types };
extern const Il2CppType TimeLevel4_t4195116982_0_0_0;
static const Il2CppType* GenInst_TimeLevel4_t4195116982_0_0_0_Types[] = { &TimeLevel4_t4195116982_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeLevel4_t4195116982_0_0_0 = { 1, GenInst_TimeLevel4_t4195116982_0_0_0_Types };
extern const Il2CppType TimeGame_t3115909209_0_0_0;
static const Il2CppType* GenInst_TimeGame_t3115909209_0_0_0_Types[] = { &TimeGame_t3115909209_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeGame_t3115909209_0_0_0 = { 1, GenInst_TimeGame_t3115909209_0_0_0_Types };
extern const Il2CppType SpringPanel_t277350554_0_0_0;
static const Il2CppType* GenInst_SpringPanel_t277350554_0_0_0_Types[] = { &SpringPanel_t277350554_0_0_0 };
extern const Il2CppGenericInst GenInst_SpringPanel_t277350554_0_0_0 = { 1, GenInst_SpringPanel_t277350554_0_0_0_Types };
extern const Il2CppType SpringPosition_t3478173108_0_0_0;
static const Il2CppType* GenInst_SpringPosition_t3478173108_0_0_0_Types[] = { &SpringPosition_t3478173108_0_0_0 };
extern const Il2CppGenericInst GenInst_SpringPosition_t3478173108_0_0_0 = { 1, GenInst_SpringPosition_t3478173108_0_0_0_Types };
extern const Il2CppType Level_05_Controller_t1454764318_0_0_0;
static const Il2CppType* GenInst_Level_05_Controller_t1454764318_0_0_0_Types[] = { &Level_05_Controller_t1454764318_0_0_0 };
extern const Il2CppGenericInst GenInst_Level_05_Controller_t1454764318_0_0_0 = { 1, GenInst_Level_05_Controller_t1454764318_0_0_0_Types };
extern const Il2CppType Level_04_Controller_t985329950_0_0_0;
static const Il2CppType* GenInst_Level_04_Controller_t985329950_0_0_0_Types[] = { &Level_04_Controller_t985329950_0_0_0 };
extern const Il2CppGenericInst GenInst_Level_04_Controller_t985329950_0_0_0 = { 1, GenInst_Level_04_Controller_t985329950_0_0_0_Types };
extern const Il2CppType TweenAlpha_t3706845226_0_0_0;
static const Il2CppType* GenInst_TweenAlpha_t3706845226_0_0_0_Types[] = { &TweenAlpha_t3706845226_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenAlpha_t3706845226_0_0_0 = { 1, GenInst_TweenAlpha_t3706845226_0_0_0_Types };
extern const Il2CppType TweenColor_t2112002648_0_0_0;
static const Il2CppType* GenInst_TweenColor_t2112002648_0_0_0_Types[] = { &TweenColor_t2112002648_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenColor_t2112002648_0_0_0 = { 1, GenInst_TweenColor_t2112002648_0_0_0_Types };
extern const Il2CppType TweenFOV_t2203484580_0_0_0;
static const Il2CppType* GenInst_TweenFOV_t2203484580_0_0_0_Types[] = { &TweenFOV_t2203484580_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenFOV_t2203484580_0_0_0 = { 1, GenInst_TweenFOV_t2203484580_0_0_0_Types };
extern const Il2CppType UITable_t3168834800_0_0_0;
static const Il2CppType* GenInst_UITable_t3168834800_0_0_0_Types[] = { &UITable_t3168834800_0_0_0 };
extern const Il2CppGenericInst GenInst_UITable_t3168834800_0_0_0 = { 1, GenInst_UITable_t3168834800_0_0_0_Types };
extern const Il2CppType TweenHeight_t4009371699_0_0_0;
static const Il2CppType* GenInst_TweenHeight_t4009371699_0_0_0_Types[] = { &TweenHeight_t4009371699_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenHeight_t4009371699_0_0_0 = { 1, GenInst_TweenHeight_t4009371699_0_0_0_Types };
extern const Il2CppType TweenOrthoSize_t2102937296_0_0_0;
static const Il2CppType* GenInst_TweenOrthoSize_t2102937296_0_0_0_Types[] = { &TweenOrthoSize_t2102937296_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenOrthoSize_t2102937296_0_0_0 = { 1, GenInst_TweenOrthoSize_t2102937296_0_0_0_Types };
extern const Il2CppType TweenPosition_t1378762002_0_0_0;
static const Il2CppType* GenInst_TweenPosition_t1378762002_0_0_0_Types[] = { &TweenPosition_t1378762002_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenPosition_t1378762002_0_0_0 = { 1, GenInst_TweenPosition_t1378762002_0_0_0_Types };
extern const Il2CppType TweenRotation_t3072670746_0_0_0;
static const Il2CppType* GenInst_TweenRotation_t3072670746_0_0_0_Types[] = { &TweenRotation_t3072670746_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenRotation_t3072670746_0_0_0 = { 1, GenInst_TweenRotation_t3072670746_0_0_0_Types };
extern const Il2CppType TweenTransform_t1195296467_0_0_0;
static const Il2CppType* GenInst_TweenTransform_t1195296467_0_0_0_Types[] = { &TweenTransform_t1195296467_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenTransform_t1195296467_0_0_0 = { 1, GenInst_TweenTransform_t1195296467_0_0_0_Types };
extern const Il2CppType TweenVolume_t3718612080_0_0_0;
static const Il2CppType* GenInst_TweenVolume_t3718612080_0_0_0_Types[] = { &TweenVolume_t3718612080_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenVolume_t3718612080_0_0_0 = { 1, GenInst_TweenVolume_t3718612080_0_0_0_Types };
extern const Il2CppType TweenWidth_t2861389279_0_0_0;
static const Il2CppType* GenInst_TweenWidth_t2861389279_0_0_0_Types[] = { &TweenWidth_t2861389279_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenWidth_t2861389279_0_0_0 = { 1, GenInst_TweenWidth_t2861389279_0_0_0_Types };
extern const Il2CppType UI2DSprite_t1366157572_0_0_0;
static const Il2CppType* GenInst_UI2DSprite_t1366157572_0_0_0_Types[] = { &UI2DSprite_t1366157572_0_0_0 };
extern const Il2CppGenericInst GenInst_UI2DSprite_t1366157572_0_0_0 = { 1, GenInst_UI2DSprite_t1366157572_0_0_0_Types };
extern const Il2CppType UIGrid_t1536638187_0_0_0;
static const Il2CppType* GenInst_UIGrid_t1536638187_0_0_0_Types[] = { &UIGrid_t1536638187_0_0_0 };
extern const Il2CppGenericInst GenInst_UIGrid_t1536638187_0_0_0 = { 1, GenInst_UIGrid_t1536638187_0_0_0_Types };
extern const Il2CppType UIWrapContent_t1188558554_0_0_0;
static const Il2CppType* GenInst_UIWrapContent_t1188558554_0_0_0_Types[] = { &UIWrapContent_t1188558554_0_0_0 };
extern const Il2CppGenericInst GenInst_UIWrapContent_t1188558554_0_0_0 = { 1, GenInst_UIWrapContent_t1188558554_0_0_0_Types };
extern const Il2CppType UICenterOnChild_t253063637_0_0_0;
static const Il2CppType* GenInst_UICenterOnChild_t253063637_0_0_0_Types[] = { &UICenterOnChild_t253063637_0_0_0 };
extern const Il2CppGenericInst GenInst_UICenterOnChild_t253063637_0_0_0 = { 1, GenInst_UICenterOnChild_t253063637_0_0_0_Types };
extern const Il2CppType UIDraggableCamera_t1644204495_0_0_0;
static const Il2CppType* GenInst_UIDraggableCamera_t1644204495_0_0_0_Types[] = { &UIDraggableCamera_t1644204495_0_0_0 };
extern const Il2CppGenericInst GenInst_UIDraggableCamera_t1644204495_0_0_0 = { 1, GenInst_UIDraggableCamera_t1644204495_0_0_0_Types };
extern const Il2CppType UIDragScrollView_t2492060641_0_0_0;
static const Il2CppType* GenInst_UIDragScrollView_t2492060641_0_0_0_Types[] = { &UIDragScrollView_t2492060641_0_0_0 };
extern const Il2CppGenericInst GenInst_UIDragScrollView_t2492060641_0_0_0 = { 1, GenInst_UIDragScrollView_t2492060641_0_0_0_Types };
extern const Il2CppType UIDragDropContainer_t2199185397_0_0_0;
static const Il2CppType* GenInst_UIDragDropContainer_t2199185397_0_0_0_Types[] = { &UIDragDropContainer_t2199185397_0_0_0 };
extern const Il2CppGenericInst GenInst_UIDragDropContainer_t2199185397_0_0_0 = { 1, GenInst_UIDragDropContainer_t2199185397_0_0_0_Types };
extern const Il2CppType UIEventListener_t1665237878_0_0_0;
static const Il2CppType* GenInst_UIEventListener_t1665237878_0_0_0_Types[] = { &UIEventListener_t1665237878_0_0_0 };
extern const Il2CppGenericInst GenInst_UIEventListener_t1665237878_0_0_0 = { 1, GenInst_UIEventListener_t1665237878_0_0_0_Types };
extern const Il2CppType UIStorageSlot_t392832716_0_0_0;
static const Il2CppType* GenInst_UIStorageSlot_t392832716_0_0_0_Types[] = { &UIStorageSlot_t392832716_0_0_0 };
extern const Il2CppGenericInst GenInst_UIStorageSlot_t392832716_0_0_0 = { 1, GenInst_UIStorageSlot_t392832716_0_0_0_Types };
extern const Il2CppType UIOrthoCamera_t1944225589_0_0_0;
static const Il2CppType* GenInst_UIOrthoCamera_t1944225589_0_0_0_Types[] = { &UIOrthoCamera_t1944225589_0_0_0 };
extern const Il2CppGenericInst GenInst_UIOrthoCamera_t1944225589_0_0_0 = { 1, GenInst_UIOrthoCamera_t1944225589_0_0_0_Types };
extern const Il2CppType UIProgressBar_t1222110469_0_0_0;
static const Il2CppType* GenInst_UIProgressBar_t1222110469_0_0_0_Types[] = { &UIProgressBar_t1222110469_0_0_0 };
extern const Il2CppGenericInst GenInst_UIProgressBar_t1222110469_0_0_0 = { 1, GenInst_UIProgressBar_t1222110469_0_0_0_Types };
extern const Il2CppType UISlider_t886033014_0_0_0;
static const Il2CppType* GenInst_UISlider_t886033014_0_0_0_Types[] = { &UISlider_t886033014_0_0_0 };
extern const Il2CppGenericInst GenInst_UISlider_t886033014_0_0_0 = { 1, GenInst_UISlider_t886033014_0_0_0_Types };
extern const Il2CppType StandaloneInputModule_t2760469101_0_0_0;
static const Il2CppType* GenInst_StandaloneInputModule_t2760469101_0_0_0_Types[] = { &StandaloneInputModule_t2760469101_0_0_0 };
extern const Il2CppGenericInst GenInst_StandaloneInputModule_t2760469101_0_0_0 = { 1, GenInst_StandaloneInputModule_t2760469101_0_0_0_Types };
extern const Il2CppType BackgroundPlaneBehaviour_t3333547397_0_0_0;
static const Il2CppType* GenInst_BackgroundPlaneBehaviour_t3333547397_0_0_0_Types[] = { &BackgroundPlaneBehaviour_t3333547397_0_0_0 };
extern const Il2CppGenericInst GenInst_BackgroundPlaneBehaviour_t3333547397_0_0_0 = { 1, GenInst_BackgroundPlaneBehaviour_t3333547397_0_0_0_Types };
extern const Il2CppType ReconstructionBehaviour_t3655135626_0_0_0;
static const Il2CppType* GenInst_ReconstructionBehaviour_t3655135626_0_0_0_Types[] = { &ReconstructionBehaviour_t3655135626_0_0_0 };
extern const Il2CppGenericInst GenInst_ReconstructionBehaviour_t3655135626_0_0_0 = { 1, GenInst_ReconstructionBehaviour_t3655135626_0_0_0_Types };
extern const Il2CppType ComponentFactoryStarterBehaviour_t3931359467_0_0_0;
static const Il2CppType* GenInst_ComponentFactoryStarterBehaviour_t3931359467_0_0_0_Types[] = { &ComponentFactoryStarterBehaviour_t3931359467_0_0_0 };
extern const Il2CppGenericInst GenInst_ComponentFactoryStarterBehaviour_t3931359467_0_0_0 = { 1, GenInst_ComponentFactoryStarterBehaviour_t3931359467_0_0_0_Types };
extern const Il2CppType VuforiaBehaviour_t2151848540_0_0_0;
static const Il2CppType* GenInst_VuforiaBehaviour_t2151848540_0_0_0_Types[] = { &VuforiaBehaviour_t2151848540_0_0_0 };
extern const Il2CppGenericInst GenInst_VuforiaBehaviour_t2151848540_0_0_0 = { 1, GenInst_VuforiaBehaviour_t2151848540_0_0_0_Types };
extern const Il2CppType MaskOutBehaviour_t2745617306_0_0_0;
static const Il2CppType* GenInst_MaskOutBehaviour_t2745617306_0_0_0_Types[] = { &MaskOutBehaviour_t2745617306_0_0_0 };
extern const Il2CppGenericInst GenInst_MaskOutBehaviour_t2745617306_0_0_0 = { 1, GenInst_MaskOutBehaviour_t2745617306_0_0_0_Types };
extern const Il2CppType VirtualButtonBehaviour_t1436326451_0_0_0;
static const Il2CppType* GenInst_VirtualButtonBehaviour_t1436326451_0_0_0_Types[] = { &VirtualButtonBehaviour_t1436326451_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonBehaviour_t1436326451_0_0_0 = { 1, GenInst_VirtualButtonBehaviour_t1436326451_0_0_0_Types };
extern const Il2CppType TurnOffBehaviour_t65964226_0_0_0;
static const Il2CppType* GenInst_TurnOffBehaviour_t65964226_0_0_0_Types[] = { &TurnOffBehaviour_t65964226_0_0_0 };
extern const Il2CppGenericInst GenInst_TurnOffBehaviour_t65964226_0_0_0 = { 1, GenInst_TurnOffBehaviour_t65964226_0_0_0_Types };
extern const Il2CppType MultiTargetBehaviour_t2061511750_0_0_0;
static const Il2CppType* GenInst_MultiTargetBehaviour_t2061511750_0_0_0_Types[] = { &MultiTargetBehaviour_t2061511750_0_0_0 };
extern const Il2CppGenericInst GenInst_MultiTargetBehaviour_t2061511750_0_0_0 = { 1, GenInst_MultiTargetBehaviour_t2061511750_0_0_0_Types };
extern const Il2CppType CylinderTargetBehaviour_t822809409_0_0_0;
static const Il2CppType* GenInst_CylinderTargetBehaviour_t822809409_0_0_0_Types[] = { &CylinderTargetBehaviour_t822809409_0_0_0 };
extern const Il2CppGenericInst GenInst_CylinderTargetBehaviour_t822809409_0_0_0 = { 1, GenInst_CylinderTargetBehaviour_t822809409_0_0_0_Types };
extern const Il2CppType WordBehaviour_t209462683_0_0_0;
static const Il2CppType* GenInst_WordBehaviour_t209462683_0_0_0_Types[] = { &WordBehaviour_t209462683_0_0_0 };
extern const Il2CppGenericInst GenInst_WordBehaviour_t209462683_0_0_0 = { 1, GenInst_WordBehaviour_t209462683_0_0_0_Types };
extern const Il2CppType TextRecoBehaviour_t87475147_0_0_0;
static const Il2CppType* GenInst_TextRecoBehaviour_t87475147_0_0_0_Types[] = { &TextRecoBehaviour_t87475147_0_0_0 };
extern const Il2CppGenericInst GenInst_TextRecoBehaviour_t87475147_0_0_0 = { 1, GenInst_TextRecoBehaviour_t87475147_0_0_0_Types };
extern const Il2CppType ObjectTargetBehaviour_t728125005_0_0_0;
static const Il2CppType* GenInst_ObjectTargetBehaviour_t728125005_0_0_0_Types[] = { &ObjectTargetBehaviour_t728125005_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectTargetBehaviour_t728125005_0_0_0 = { 1, GenInst_ObjectTargetBehaviour_t728125005_0_0_0_Types };
extern const Il2CppType VuMarkBehaviour_t1178230459_0_0_0;
static const Il2CppType* GenInst_VuMarkBehaviour_t1178230459_0_0_0_Types[] = { &VuMarkBehaviour_t1178230459_0_0_0 };
extern const Il2CppGenericInst GenInst_VuMarkBehaviour_t1178230459_0_0_0 = { 1, GenInst_VuMarkBehaviour_t1178230459_0_0_0_Types };
extern const Il2CppType VuforiaConfiguration_t1763229349_0_0_0;
static const Il2CppType* GenInst_VuforiaConfiguration_t1763229349_0_0_0_Types[] = { &VuforiaConfiguration_t1763229349_0_0_0 };
extern const Il2CppGenericInst GenInst_VuforiaConfiguration_t1763229349_0_0_0 = { 1, GenInst_VuforiaConfiguration_t1763229349_0_0_0_Types };
extern const Il2CppType ClientCredentials_t1418047401_0_0_0;
static const Il2CppType* GenInst_ClientCredentials_t1418047401_0_0_0_Types[] = { &ClientCredentials_t1418047401_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientCredentials_t1418047401_0_0_0 = { 1, GenInst_ClientCredentials_t1418047401_0_0_0_Types };
extern const Il2CppType ChannelProtectionRequirements_t2141883287_0_0_0;
static const Il2CppType* GenInst_ChannelProtectionRequirements_t2141883287_0_0_0_Types[] = { &ChannelProtectionRequirements_t2141883287_0_0_0 };
extern const Il2CppGenericInst GenInst_ChannelProtectionRequirements_t2141883287_0_0_0 = { 1, GenInst_ChannelProtectionRequirements_t2141883287_0_0_0_Types };
extern const Il2CppType ServiceCredentials_t3431196922_0_0_0;
static const Il2CppType* GenInst_ServiceCredentials_t3431196922_0_0_0_Types[] = { &ServiceCredentials_t3431196922_0_0_0 };
extern const Il2CppGenericInst GenInst_ServiceCredentials_t3431196922_0_0_0 = { 1, GenInst_ServiceCredentials_t3431196922_0_0_0_Types };
static const Il2CppType* GenInst_Data_t1588725102_0_0_0_Data_t1588725102_0_0_0_Types[] = { &Data_t1588725102_0_0_0, &Data_t1588725102_0_0_0 };
extern const Il2CppGenericInst GenInst_Data_t1588725102_0_0_0_Data_t1588725102_0_0_0 = { 2, GenInst_Data_t1588725102_0_0_0_Data_t1588725102_0_0_0_Types };
static const Il2CppType* GenInst_Preset_t2972107632_0_0_0_Preset_t2972107632_0_0_0_Types[] = { &Preset_t2972107632_0_0_0, &Preset_t2972107632_0_0_0 };
extern const Il2CppGenericInst GenInst_Preset_t2972107632_0_0_0_Preset_t2972107632_0_0_0 = { 2, GenInst_Preset_t2972107632_0_0_0_Preset_t2972107632_0_0_0_Types };
static const Il2CppType* GenInst_SetAnimInfo_t2127528194_0_0_0_SetAnimInfo_t2127528194_0_0_0_Types[] = { &SetAnimInfo_t2127528194_0_0_0, &SetAnimInfo_t2127528194_0_0_0 };
extern const Il2CppGenericInst GenInst_SetAnimInfo_t2127528194_0_0_0_SetAnimInfo_t2127528194_0_0_0 = { 2, GenInst_SetAnimInfo_t2127528194_0_0_0_SetAnimInfo_t2127528194_0_0_0_Types };
static const Il2CppType* GenInst_Byte_t1134296376_0_0_0_Byte_t1134296376_0_0_0_Types[] = { &Byte_t1134296376_0_0_0, &Byte_t1134296376_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t1134296376_0_0_0_Byte_t1134296376_0_0_0 = { 2, GenInst_Byte_t1134296376_0_0_0_Byte_t1134296376_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2530217319_0_0_0_KeyValuePair_2_t2530217319_0_0_0_Types[] = { &KeyValuePair_2_t2530217319_0_0_0, &KeyValuePair_2_t2530217319_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2530217319_0_0_0_KeyValuePair_2_t2530217319_0_0_0 = { 2, GenInst_KeyValuePair_2_t2530217319_0_0_0_KeyValuePair_2_t2530217319_0_0_0_Types };
static const Il2CppType* GenInst_DateTime_t3738529785_0_0_0_DateTime_t3738529785_0_0_0_Types[] = { &DateTime_t3738529785_0_0_0, &DateTime_t3738529785_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t3738529785_0_0_0_DateTime_t3738529785_0_0_0 = { 2, GenInst_DateTime_t3738529785_0_0_0_DateTime_t3738529785_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t287865710_0_0_0_CustomAttributeNamedArgument_t287865710_0_0_0_Types[] = { &CustomAttributeNamedArgument_t287865710_0_0_0, &CustomAttributeNamedArgument_t287865710_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t287865710_0_0_0_CustomAttributeNamedArgument_t287865710_0_0_0 = { 2, GenInst_CustomAttributeNamedArgument_t287865710_0_0_0_CustomAttributeNamedArgument_t287865710_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t2723150157_0_0_0_CustomAttributeTypedArgument_t2723150157_0_0_0_Types[] = { &CustomAttributeTypedArgument_t2723150157_0_0_0, &CustomAttributeTypedArgument_t2723150157_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t2723150157_0_0_0_CustomAttributeTypedArgument_t2723150157_0_0_0 = { 2, GenInst_CustomAttributeTypedArgument_t2723150157_0_0_0_CustomAttributeTypedArgument_t2723150157_0_0_0_Types };
static const Il2CppType* GenInst_EnumMemberInfo_t3072296154_0_0_0_EnumMemberInfo_t3072296154_0_0_0_Types[] = { &EnumMemberInfo_t3072296154_0_0_0, &EnumMemberInfo_t3072296154_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumMemberInfo_t3072296154_0_0_0_EnumMemberInfo_t3072296154_0_0_0 = { 2, GenInst_EnumMemberInfo_t3072296154_0_0_0_EnumMemberInfo_t3072296154_0_0_0_Types };
static const Il2CppType* GenInst_CodeUnit_t1519973372_0_0_0_CodeUnit_t1519973372_0_0_0_Types[] = { &CodeUnit_t1519973372_0_0_0, &CodeUnit_t1519973372_0_0_0 };
extern const Il2CppGenericInst GenInst_CodeUnit_t1519973372_0_0_0_CodeUnit_t1519973372_0_0_0 = { 2, GenInst_CodeUnit_t1519973372_0_0_0_CodeUnit_t1519973372_0_0_0_Types };
static const Il2CppType* GenInst_AnimatorClipInfo_t3156717155_0_0_0_AnimatorClipInfo_t3156717155_0_0_0_Types[] = { &AnimatorClipInfo_t3156717155_0_0_0, &AnimatorClipInfo_t3156717155_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimatorClipInfo_t3156717155_0_0_0_AnimatorClipInfo_t3156717155_0_0_0 = { 2, GenInst_AnimatorClipInfo_t3156717155_0_0_0_AnimatorClipInfo_t3156717155_0_0_0_Types };
static const Il2CppType* GenInst_Color32_t2600501292_0_0_0_Color32_t2600501292_0_0_0_Types[] = { &Color32_t2600501292_0_0_0, &Color32_t2600501292_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t2600501292_0_0_0_Color32_t2600501292_0_0_0 = { 2, GenInst_Color32_t2600501292_0_0_0_Color32_t2600501292_0_0_0_Types };
static const Il2CppType* GenInst_RaycastResult_t3360306849_0_0_0_RaycastResult_t3360306849_0_0_0_Types[] = { &RaycastResult_t3360306849_0_0_0, &RaycastResult_t3360306849_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t3360306849_0_0_0_RaycastResult_t3360306849_0_0_0 = { 2, GenInst_RaycastResult_t3360306849_0_0_0_RaycastResult_t3360306849_0_0_0_Types };
static const Il2CppType* GenInst_UICharInfo_t75501106_0_0_0_UICharInfo_t75501106_0_0_0_Types[] = { &UICharInfo_t75501106_0_0_0, &UICharInfo_t75501106_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t75501106_0_0_0_UICharInfo_t75501106_0_0_0 = { 2, GenInst_UICharInfo_t75501106_0_0_0_UICharInfo_t75501106_0_0_0_Types };
static const Il2CppType* GenInst_UILineInfo_t4195266810_0_0_0_UILineInfo_t4195266810_0_0_0_Types[] = { &UILineInfo_t4195266810_0_0_0, &UILineInfo_t4195266810_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t4195266810_0_0_0_UILineInfo_t4195266810_0_0_0 = { 2, GenInst_UILineInfo_t4195266810_0_0_0_UILineInfo_t4195266810_0_0_0_Types };
static const Il2CppType* GenInst_UIVertex_t4057497605_0_0_0_UIVertex_t4057497605_0_0_0_Types[] = { &UIVertex_t4057497605_0_0_0, &UIVertex_t4057497605_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t4057497605_0_0_0_UIVertex_t4057497605_0_0_0 = { 2, GenInst_UIVertex_t4057497605_0_0_0_UIVertex_t4057497605_0_0_0_Types };
static const Il2CppType* GenInst_Vector2_t2156229523_0_0_0_Vector2_t2156229523_0_0_0_Types[] = { &Vector2_t2156229523_0_0_0, &Vector2_t2156229523_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t2156229523_0_0_0_Vector2_t2156229523_0_0_0 = { 2, GenInst_Vector2_t2156229523_0_0_0_Vector2_t2156229523_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t3722313464_0_0_0_Vector3_t3722313464_0_0_0_Types[] = { &Vector3_t3722313464_0_0_0, &Vector3_t3722313464_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t3722313464_0_0_0_Vector3_t3722313464_0_0_0 = { 2, GenInst_Vector3_t3722313464_0_0_0_Vector3_t3722313464_0_0_0_Types };
static const Il2CppType* GenInst_Vector4_t3319028937_0_0_0_Vector4_t3319028937_0_0_0_Types[] = { &Vector4_t3319028937_0_0_0, &Vector4_t3319028937_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t3319028937_0_0_0_Vector4_t3319028937_0_0_0 = { 2, GenInst_Vector4_t3319028937_0_0_0_Vector4_t3319028937_0_0_0_Types };
static const Il2CppType* GenInst_CameraField_t1483002240_0_0_0_CameraField_t1483002240_0_0_0_Types[] = { &CameraField_t1483002240_0_0_0, &CameraField_t1483002240_0_0_0 };
extern const Il2CppGenericInst GenInst_CameraField_t1483002240_0_0_0_CameraField_t1483002240_0_0_0 = { 2, GenInst_CameraField_t1483002240_0_0_0_CameraField_t1483002240_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t3209881435_0_0_0_PIXEL_FORMAT_t3209881435_0_0_0_Types[] = { &PIXEL_FORMAT_t3209881435_0_0_0, &PIXEL_FORMAT_t3209881435_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t3209881435_0_0_0_PIXEL_FORMAT_t3209881435_0_0_0 = { 2, GenInst_PIXEL_FORMAT_t3209881435_0_0_0_PIXEL_FORMAT_t3209881435_0_0_0_Types };
static const Il2CppType* GenInst_TargetSearchResult_t3441982613_0_0_0_TargetSearchResult_t3441982613_0_0_0_Types[] = { &TargetSearchResult_t3441982613_0_0_0, &TargetSearchResult_t3441982613_0_0_0 };
extern const Il2CppGenericInst GenInst_TargetSearchResult_t3441982613_0_0_0_TargetSearchResult_t3441982613_0_0_0 = { 2, GenInst_TargetSearchResult_t3441982613_0_0_0_TargetSearchResult_t3441982613_0_0_0_Types };
static const Il2CppType* GenInst_TrackableIdPair_t4227350457_0_0_0_TrackableIdPair_t4227350457_0_0_0_Types[] = { &TrackableIdPair_t4227350457_0_0_0, &TrackableIdPair_t4227350457_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableIdPair_t4227350457_0_0_0_TrackableIdPair_t4227350457_0_0_0 = { 2, GenInst_TrackableIdPair_t4227350457_0_0_0_TrackableIdPair_t4227350457_0_0_0_Types };
static const Il2CppType* GenInst_VuMarkTargetData_t3925829072_0_0_0_VuMarkTargetData_t3925829072_0_0_0_Types[] = { &VuMarkTargetData_t3925829072_0_0_0, &VuMarkTargetData_t3925829072_0_0_0 };
extern const Il2CppGenericInst GenInst_VuMarkTargetData_t3925829072_0_0_0_VuMarkTargetData_t3925829072_0_0_0 = { 2, GenInst_VuMarkTargetData_t3925829072_0_0_0_VuMarkTargetData_t3925829072_0_0_0_Types };
static const Il2CppType* GenInst_VuMarkTargetResultData_t1414459591_0_0_0_VuMarkTargetResultData_t1414459591_0_0_0_Types[] = { &VuMarkTargetResultData_t1414459591_0_0_0, &VuMarkTargetResultData_t1414459591_0_0_0 };
extern const Il2CppGenericInst GenInst_VuMarkTargetResultData_t1414459591_0_0_0_VuMarkTargetResultData_t1414459591_0_0_0 = { 2, GenInst_VuMarkTargetResultData_t1414459591_0_0_0_VuMarkTargetResultData_t1414459591_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4237331251_0_0_0_KeyValuePair_2_t4237331251_0_0_0_Types[] = { &KeyValuePair_2_t4237331251_0_0_0, &KeyValuePair_2_t4237331251_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4237331251_0_0_0_KeyValuePair_2_t4237331251_0_0_0 = { 2, GenInst_KeyValuePair_2_t4237331251_0_0_0_KeyValuePair_2_t4237331251_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4237331251_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t4237331251_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4237331251_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t4237331251_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t71524366_0_0_0_KeyValuePair_2_t71524366_0_0_0_Types[] = { &KeyValuePair_2_t71524366_0_0_0, &KeyValuePair_2_t71524366_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t71524366_0_0_0_KeyValuePair_2_t71524366_0_0_0 = { 2, GenInst_KeyValuePair_2_t71524366_0_0_0_KeyValuePair_2_t71524366_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t71524366_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t71524366_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t71524366_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t71524366_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3568047141_0_0_0_KeyValuePair_2_t3568047141_0_0_0_Types[] = { &KeyValuePair_2_t3568047141_0_0_0, &KeyValuePair_2_t3568047141_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3568047141_0_0_0_KeyValuePair_2_t3568047141_0_0_0 = { 2, GenInst_KeyValuePair_2_t3568047141_0_0_0_KeyValuePair_2_t3568047141_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3568047141_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3568047141_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3568047141_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3568047141_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Label_t2281661643_0_0_0_Il2CppObject_0_0_0_Types[] = { &Label_t2281661643_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Label_t2281661643_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Label_t2281661643_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Label_t2281661643_0_0_0_Label_t2281661643_0_0_0_Types[] = { &Label_t2281661643_0_0_0, &Label_t2281661643_0_0_0 };
extern const Il2CppGenericInst GenInst_Label_t2281661643_0_0_0_Label_t2281661643_0_0_0 = { 2, GenInst_Label_t2281661643_0_0_0_Label_t2281661643_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2387291312_0_0_0_KeyValuePair_2_t2387291312_0_0_0_Types[] = { &KeyValuePair_2_t2387291312_0_0_0, &KeyValuePair_2_t2387291312_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2387291312_0_0_0_KeyValuePair_2_t2387291312_0_0_0 = { 2, GenInst_KeyValuePair_2_t2387291312_0_0_0_KeyValuePair_2_t2387291312_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2387291312_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2387291312_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2387291312_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2387291312_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Status_t1100905814_0_0_0_Il2CppObject_0_0_0_Types[] = { &Status_t1100905814_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Status_t1100905814_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Status_t1100905814_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Status_t1100905814_0_0_0_Status_t1100905814_0_0_0_Types[] = { &Status_t1100905814_0_0_0, &Status_t1100905814_0_0_0 };
extern const Il2CppGenericInst GenInst_Status_t1100905814_0_0_0_Status_t1100905814_0_0_0 = { 2, GenInst_Status_t1100905814_0_0_0_Status_t1100905814_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4188143612_0_0_0_KeyValuePair_2_t4188143612_0_0_0_Types[] = { &KeyValuePair_2_t4188143612_0_0_0, &KeyValuePair_2_t4188143612_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4188143612_0_0_0_KeyValuePair_2_t4188143612_0_0_0 = { 2, GenInst_KeyValuePair_2_t4188143612_0_0_0_KeyValuePair_2_t4188143612_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4188143612_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t4188143612_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4188143612_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t4188143612_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_VirtualButtonData_t2901758114_0_0_0_Il2CppObject_0_0_0_Types[] = { &VirtualButtonData_t2901758114_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonData_t2901758114_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_VirtualButtonData_t2901758114_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_VirtualButtonData_t2901758114_0_0_0_VirtualButtonData_t2901758114_0_0_0_Types[] = { &VirtualButtonData_t2901758114_0_0_0, &VirtualButtonData_t2901758114_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonData_t2901758114_0_0_0_VirtualButtonData_t2901758114_0_0_0 = { 2, GenInst_VirtualButtonData_t2901758114_0_0_0_VirtualButtonData_t2901758114_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &Boolean_t97287965_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3842366416_0_0_0_KeyValuePair_2_t3842366416_0_0_0_Types[] = { &KeyValuePair_2_t3842366416_0_0_0, &KeyValuePair_2_t3842366416_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3842366416_0_0_0_KeyValuePair_2_t3842366416_0_0_0 = { 2, GenInst_KeyValuePair_2_t3842366416_0_0_0_KeyValuePair_2_t3842366416_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3842366416_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3842366416_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3842366416_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3842366416_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3188640940_0_0_0_KeyValuePair_2_t3188640940_0_0_0_Types[] = { &KeyValuePair_2_t3188640940_0_0_0, &KeyValuePair_2_t3188640940_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3188640940_0_0_0_KeyValuePair_2_t3188640940_0_0_0 = { 2, GenInst_KeyValuePair_2_t3188640940_0_0_0_KeyValuePair_2_t3188640940_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3188640940_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3188640940_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3188640940_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3188640940_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_DateTime_t3738529785_0_0_0_Il2CppObject_0_0_0_Types[] = { &DateTime_t3738529785_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t3738529785_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_DateTime_t3738529785_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2401056908_0_0_0_KeyValuePair_2_t2401056908_0_0_0_Types[] = { &KeyValuePair_2_t2401056908_0_0_0, &KeyValuePair_2_t2401056908_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2401056908_0_0_0_KeyValuePair_2_t2401056908_0_0_0 = { 2, GenInst_KeyValuePair_2_t2401056908_0_0_0_KeyValuePair_2_t2401056908_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2401056908_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2401056908_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2401056908_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2401056908_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2530217319_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2530217319_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2530217319_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2530217319_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1627836113_0_0_0_KeyValuePair_2_t1627836113_0_0_0_Types[] = { &KeyValuePair_2_t1627836113_0_0_0, &KeyValuePair_2_t1627836113_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1627836113_0_0_0_KeyValuePair_2_t1627836113_0_0_0 = { 2, GenInst_KeyValuePair_2_t1627836113_0_0_0_KeyValuePair_2_t1627836113_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1627836113_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1627836113_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1627836113_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1627836113_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_UInt16_t2177724958_0_0_0_Il2CppObject_0_0_0_Types[] = { &UInt16_t2177724958_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t2177724958_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_UInt16_t2177724958_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_UInt16_t2177724958_0_0_0_UInt16_t2177724958_0_0_0_Types[] = { &UInt16_t2177724958_0_0_0, &UInt16_t2177724958_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t2177724958_0_0_0_UInt16_t2177724958_0_0_0 = { 2, GenInst_UInt16_t2177724958_0_0_0_UInt16_t2177724958_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1377593753_0_0_0_KeyValuePair_2_t1377593753_0_0_0_Types[] = { &KeyValuePair_2_t1377593753_0_0_0, &KeyValuePair_2_t1377593753_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1377593753_0_0_0_KeyValuePair_2_t1377593753_0_0_0 = { 2, GenInst_KeyValuePair_2_t1377593753_0_0_0_KeyValuePair_2_t1377593753_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1377593753_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1377593753_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1377593753_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1377593753_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t1927482598_0_0_0_Il2CppObject_0_0_0_Types[] = { &TextEditOp_t1927482598_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t1927482598_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_TextEditOp_t1927482598_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t1927482598_0_0_0_TextEditOp_t1927482598_0_0_0_Types[] = { &TextEditOp_t1927482598_0_0_0, &TextEditOp_t1927482598_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t1927482598_0_0_0_TextEditOp_t1927482598_0_0_0 = { 2, GenInst_TextEditOp_t1927482598_0_0_0_TextEditOp_t1927482598_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2969503080_0_0_0_KeyValuePair_2_t2969503080_0_0_0_Types[] = { &KeyValuePair_2_t2969503080_0_0_0, &KeyValuePair_2_t2969503080_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2969503080_0_0_0_KeyValuePair_2_t2969503080_0_0_0 = { 2, GenInst_KeyValuePair_2_t2969503080_0_0_0_KeyValuePair_2_t2969503080_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2969503080_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2969503080_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2969503080_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2969503080_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ProfileData_t3519391925_0_0_0_Il2CppObject_0_0_0_Types[] = { &ProfileData_t3519391925_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ProfileData_t3519391925_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ProfileData_t3519391925_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ProfileData_t3519391925_0_0_0_ProfileData_t3519391925_0_0_0_Types[] = { &ProfileData_t3519391925_0_0_0, &ProfileData_t3519391925_0_0_0 };
extern const Il2CppGenericInst GenInst_ProfileData_t3519391925_0_0_0_ProfileData_t3519391925_0_0_0 = { 2, GenInst_ProfileData_t3519391925_0_0_0_ProfileData_t3519391925_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4068375620_0_0_0_KeyValuePair_2_t4068375620_0_0_0_Types[] = { &KeyValuePair_2_t4068375620_0_0_0, &KeyValuePair_2_t4068375620_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4068375620_0_0_0_KeyValuePair_2_t4068375620_0_0_0 = { 2, GenInst_KeyValuePair_2_t4068375620_0_0_0_KeyValuePair_2_t4068375620_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4068375620_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t4068375620_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4068375620_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t4068375620_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3558069120_0_0_0_KeyValuePair_2_t3558069120_0_0_0_Types[] = { &KeyValuePair_2_t3558069120_0_0_0, &KeyValuePair_2_t3558069120_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3558069120_0_0_0_KeyValuePair_2_t3558069120_0_0_0 = { 2, GenInst_KeyValuePair_2_t3558069120_0_0_0_KeyValuePair_2_t3558069120_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3558069120_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3558069120_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3558069120_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3558069120_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_PoseAgeEntry_t2181165958_0_0_0_Il2CppObject_0_0_0_Types[] = { &PoseAgeEntry_t2181165958_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_PoseAgeEntry_t2181165958_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_PoseAgeEntry_t2181165958_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_PoseAgeEntry_t2181165958_0_0_0_PoseAgeEntry_t2181165958_0_0_0_Types[] = { &PoseAgeEntry_t2181165958_0_0_0, &PoseAgeEntry_t2181165958_0_0_0 };
extern const Il2CppGenericInst GenInst_PoseAgeEntry_t2181165958_0_0_0_PoseAgeEntry_t2181165958_0_0_0 = { 2, GenInst_PoseAgeEntry_t2181165958_0_0_0_PoseAgeEntry_t2181165958_0_0_0_Types };
static const Il2CppType* GenInst_TrackableIdPair_t4227350457_0_0_0_Il2CppObject_0_0_0_Types[] = { &TrackableIdPair_t4227350457_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableIdPair_t4227350457_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_TrackableIdPair_t4227350457_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2989632341_0_0_0_KeyValuePair_2_t2989632341_0_0_0_Types[] = { &KeyValuePair_2_t2989632341_0_0_0, &KeyValuePair_2_t2989632341_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2989632341_0_0_0_KeyValuePair_2_t2989632341_0_0_0 = { 2, GenInst_KeyValuePair_2_t2989632341_0_0_0_KeyValuePair_2_t2989632341_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2989632341_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2989632341_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2989632341_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2989632341_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_PoseInfo_t1612729179_0_0_0_Il2CppObject_0_0_0_Types[] = { &PoseInfo_t1612729179_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_PoseInfo_t1612729179_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_PoseInfo_t1612729179_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_PoseInfo_t1612729179_0_0_0_PoseInfo_t1612729179_0_0_0_Types[] = { &PoseInfo_t1612729179_0_0_0, &PoseInfo_t1612729179_0_0_0 };
extern const Il2CppGenericInst GenInst_PoseInfo_t1612729179_0_0_0_PoseInfo_t1612729179_0_0_0 = { 2, GenInst_PoseInfo_t1612729179_0_0_0_PoseInfo_t1612729179_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2477808976_0_0_0_KeyValuePair_2_t2477808976_0_0_0_Types[] = { &KeyValuePair_2_t2477808976_0_0_0, &KeyValuePair_2_t2477808976_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2477808976_0_0_0_KeyValuePair_2_t2477808976_0_0_0 = { 2, GenInst_KeyValuePair_2_t2477808976_0_0_0_KeyValuePair_2_t2477808976_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2477808976_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2477808976_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2477808976_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2477808976_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType XmlDictionaryReaderQuotas_t173030297_0_0_0;
static const Il2CppType* GenInst_XmlDictionaryReaderQuotas_t173030297_0_0_0_Types[] = { &XmlDictionaryReaderQuotas_t173030297_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlDictionaryReaderQuotas_t173030297_0_0_0 = { 1, GenInst_XmlDictionaryReaderQuotas_t173030297_0_0_0_Types };
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[2167] = 
{
	&GenInst_Il2CppObject_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0,
	&GenInst_Char_t3634460470_0_0_0,
	&GenInst_Int64_t3736567304_0_0_0,
	&GenInst_UInt32_t2560061978_0_0_0,
	&GenInst_UInt64_t4134040092_0_0_0,
	&GenInst_Byte_t1134296376_0_0_0,
	&GenInst_SByte_t1669577662_0_0_0,
	&GenInst_Int16_t2552820387_0_0_0,
	&GenInst_UInt16_t2177724958_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_IConvertible_t2977365677_0_0_0,
	&GenInst_IComparable_t36111218_0_0_0,
	&GenInst_IEnumerable_t1941168011_0_0_0,
	&GenInst_ICloneable_t724424198_0_0_0,
	&GenInst_IComparable_1_t1216115102_0_0_0,
	&GenInst_IEquatable_1_t2738596416_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_IReflect_t2554276939_0_0_0,
	&GenInst__Type_t3588564251_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t1530824137_0_0_0,
	&GenInst__MemberInfo_t3922476713_0_0_0,
	&GenInst_Double_t594665363_0_0_0,
	&GenInst_Single_t1397266774_0_0_0,
	&GenInst_Decimal_t2948259380_0_0_0,
	&GenInst_Boolean_t97287965_0_0_0,
	&GenInst_Delegate_t1188392813_0_0_0,
	&GenInst_ISerializable_t3375760802_0_0_0,
	&GenInst_ParameterInfo_t1861056598_0_0_0,
	&GenInst__ParameterInfo_t489405856_0_0_0,
	&GenInst_ParameterModifier_t1461694466_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst__MethodInfo_t3550065504_0_0_0,
	&GenInst_MethodBase_t609368412_0_0_0,
	&GenInst__MethodBase_t1657248248_0_0_0,
	&GenInst_EventInfo_t_0_0_0,
	&GenInst__EventInfo_t3826131156_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t2781946373_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t4070324388_0_0_0,
	&GenInst_ConstructorInfo_t5769829_0_0_0,
	&GenInst__ConstructorInfo_t3357543833_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_KeyValuePair_2_t2401056908_0_0_0,
	&GenInst_Link_t544317964_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t2401056908_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t838906923_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t838906923_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_TableRange_t3332867892_0_0_0,
	&GenInst_TailoringInfo_t866433654_0_0_0,
	&GenInst_Contraction_t1589275354_0_0_0,
	&GenInst_Level2Map_t3640798870_0_0_0,
	&GenInst_BigInteger_t2902905089_0_0_0,
	&GenInst_UriScheme_t2867806342_0_0_0,
	&GenInst_KeySizes_t85027896_0_0_0,
	&GenInst_Assembly_t4102432799_0_0_0,
	&GenInst__Assembly_t1988906988_0_0_0,
	&GenInst_IEvidenceFactory_t4119273121_0_0_0,
	&GenInst_DateTime_t3738529785_0_0_0,
	&GenInst_DateTimeOffset_t3229287507_0_0_0,
	&GenInst_TimeSpan_t881159249_0_0_0,
	&GenInst_Guid_t_0_0_0,
	&GenInst_CustomAttributeData_t1084486650_0_0_0,
	&GenInst_TermInfoStrings_t290279955_0_0_0,
	&GenInst_Version_t3456873960_0_0_0,
	&GenInst_Slot_t3975888750_0_0_0,
	&GenInst_Slot_t384495010_0_0_0,
	&GenInst_StackFrame_t3217253059_0_0_0,
	&GenInst_Calendar_t1661121569_0_0_0,
	&GenInst_CultureInfo_t4157843068_0_0_0,
	&GenInst_IFormatProvider_t2518567562_0_0_0,
	&GenInst_FileInfo_t1169991790_0_0_0,
	&GenInst_FileSystemInfo_t3745885336_0_0_0,
	&GenInst_MarshalByRefObject_t2760389100_0_0_0,
	&GenInst_Module_t2987026101_0_0_0,
	&GenInst__Module_t135161706_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t2723150157_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t287865710_0_0_0,
	&GenInst_Exception_t1436737249_0_0_0,
	&GenInst__Exception_t2109637702_0_0_0,
	&GenInst_ModuleBuilder_t731887691_0_0_0,
	&GenInst__ModuleBuilder_t3217089703_0_0_0,
	&GenInst_MonoResource_t4103430009_0_0_0,
	&GenInst_RefEmitPermissionSet_t484390987_0_0_0,
	&GenInst_ParameterBuilder_t1137139675_0_0_0,
	&GenInst__ParameterBuilder_t3901898075_0_0_0,
	&GenInst_TypeU5BU5D_t3940880105_0_0_0,
	&GenInst_Il2CppArray_0_0_0,
	&GenInst_ICollection_t3904884886_0_0_0,
	&GenInst_IList_t2094931216_0_0_0,
	&GenInst_IList_1_t4297247_0_0_0,
	&GenInst_ICollection_1_t1017129698_0_0_0,
	&GenInst_IEnumerable_1_t1463797649_0_0_0,
	&GenInst_IList_1_t74629426_0_0_0,
	&GenInst_ICollection_1_t1087461877_0_0_0,
	&GenInst_IEnumerable_1_t1534129828_0_0_0,
	&GenInst_IList_1_t1108916738_0_0_0,
	&GenInst_ICollection_1_t2121749189_0_0_0,
	&GenInst_IEnumerable_1_t2568417140_0_0_0,
	&GenInst_IList_1_t900354228_0_0_0,
	&GenInst_ICollection_1_t1913186679_0_0_0,
	&GenInst_IEnumerable_1_t2359854630_0_0_0,
	&GenInst_IList_1_t3346143920_0_0_0,
	&GenInst_ICollection_1_t64009075_0_0_0,
	&GenInst_IEnumerable_1_t510677026_0_0_0,
	&GenInst_IList_1_t1442829200_0_0_0,
	&GenInst_ICollection_1_t2455661651_0_0_0,
	&GenInst_IEnumerable_1_t2902329602_0_0_0,
	&GenInst_IList_1_t600458651_0_0_0,
	&GenInst_ICollection_1_t1613291102_0_0_0,
	&GenInst_IEnumerable_1_t2059959053_0_0_0,
	&GenInst_LocalBuilder_t3562264111_0_0_0,
	&GenInst__LocalBuilder_t484236194_0_0_0,
	&GenInst_LocalVariableInfo_t2426779395_0_0_0,
	&GenInst_ILTokenInfo_t2325775114_0_0_0,
	&GenInst_LabelData_t360167391_0_0_0,
	&GenInst_LabelFixup_t858502054_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t1988827940_0_0_0,
	&GenInst_TypeBuilder_t1073948154_0_0_0,
	&GenInst__TypeBuilder_t2501637272_0_0_0,
	&GenInst_MethodBuilder_t2807316753_0_0_0,
	&GenInst__MethodBuilder_t600455149_0_0_0,
	&GenInst_ConstructorBuilder_t2813524108_0_0_0,
	&GenInst__ConstructorBuilder_t2416550571_0_0_0,
	&GenInst_PropertyBuilder_t314297007_0_0_0,
	&GenInst__PropertyBuilder_t1366136710_0_0_0,
	&GenInst_FieldBuilder_t2627049993_0_0_0,
	&GenInst__FieldBuilder_t2615792726_0_0_0,
	&GenInst_ResourceInfo_t2872965302_0_0_0,
	&GenInst_ResourceCacheItem_t51292791_0_0_0,
	&GenInst_IContextAttribute_t176678928_0_0_0,
	&GenInst_IContextProperty_t840037424_0_0_0,
	&GenInst_Header_t549724581_0_0_0,
	&GenInst_ITrackingHandler_t1244553475_0_0_0,
	&GenInst_TypeTag_t3541821701_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_KeyContainerPermissionAccessEntry_t3026022710_0_0_0,
	&GenInst_StrongName_t3675724614_0_0_0,
	&GenInst_IBuiltInEvidence_t554693121_0_0_0,
	&GenInst_IIdentityPermissionFactory_t3268650966_0_0_0,
	&GenInst_CodeConnectAccess_t1103527196_0_0_0,
	&GenInst_WaitHandle_t1743403487_0_0_0,
	&GenInst_IDisposable_t3640265483_0_0_0,
	&GenInst_KeyValuePair_2_t2530217319_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2530217319_0_0_0,
	&GenInst_BigInteger_t2902905090_0_0_0,
	&GenInst_ByteU5BU5D_t4116647657_0_0_0,
	&GenInst_IList_1_t2949616159_0_0_0,
	&GenInst_ICollection_1_t3962448610_0_0_0,
	&GenInst_IEnumerable_1_t114149265_0_0_0,
	&GenInst_ClientCertificateType_t1004704908_0_0_0,
	&GenInst_X509Certificate_t713131622_0_0_0,
	&GenInst_IDeserializationCallback_t4220500054_0_0_0,
	&GenInst_ConfigurationProperty_t3590861854_0_0_0,
	&GenInst_XmlElement_t561603118_0_0_0,
	&GenInst_IHasXmlChildNode_t2708887342_0_0_0,
	&GenInst_XmlLinkedNode_t1437094927_0_0_0,
	&GenInst_XmlNode_t3767805227_0_0_0,
	&GenInst_IXPathNavigable_t2716156786_0_0_0,
	&GenInst_XPathResultType_t2828988488_0_0_0,
	&GenInst_XmlSchemaAttribute_t2797257020_0_0_0,
	&GenInst_XmlSchemaAnnotated_t2603549639_0_0_0,
	&GenInst_XmlSchemaObject_t1315720168_0_0_0,
	&GenInst_XsdIdentityPath_t991900844_0_0_0,
	&GenInst_XsdIdentityField_t1964115728_0_0_0,
	&GenInst_XsdIdentityStep_t1480907129_0_0_0,
	&GenInst_DTMXPathLinkedNode2_t3353097823_0_0_0,
	&GenInst_DTMXPathAttributeNode2_t3707096872_0_0_0,
	&GenInst_DTMXPathNamespaceNode2_t1119120712_0_0_0,
	&GenInst_Sort_t3076360436_0_0_0,
	&GenInst_XmlQualifiedName_t2760654312_0_0_0,
	&GenInst_Attribute_t270895445_0_0_0,
	&GenInst_XPathSorter_t36376808_0_0_0,
	&GenInst_XsltContextInfo_t2905292101_0_0_0,
	&GenInst_TypeCode_t2987224087_0_0_0,
	&GenInst_XmlSchemaException_t3511258692_0_0_0,
	&GenInst_SystemException_t176217640_0_0_0,
	&GenInst_KeyValuePair_2_t3041488559_0_0_0,
	&GenInst_String_t_0_0_0_DTDNode_t858560093_0_0_0,
	&GenInst_DTDNode_t858560093_0_0_0,
	&GenInst_IXmlLineInfo_t2353988607_0_0_0,
	&GenInst_AttributeSlot_t3985135163_0_0_0,
	&GenInst_Entry_t3052280359_0_0_0,
	&GenInst_NsDecl_t3938094415_0_0_0,
	&GenInst_NsScope_t3958624705_0_0_0,
	&GenInst_XmlAttributeTokenInfo_t384315108_0_0_0,
	&GenInst_XmlTokenInfo_t2519673037_0_0_0,
	&GenInst_TagName_t2891256255_0_0_0,
	&GenInst_XmlNodeInfo_t4030693883_0_0_0,
	&GenInst_XPathNavigator_t787956054_0_0_0,
	&GenInst_IXmlNamespaceResolver_t535375154_0_0_0,
	&GenInst_XPathItem_t4250588140_0_0_0,
	&GenInst_XmlAttribute_t1173852259_0_0_0,
	&GenInst_XmlSchema_t3742557897_0_0_0,
	&GenInst_Regex_t3657309853_0_0_0,
	&GenInst_XmlSchemaSimpleType_t2678868104_0_0_0,
	&GenInst_XmlSchemaType_t2033747345_0_0_0,
	&GenInst_EnumMapMember_t3516323045_0_0_0,
	&GenInst_CodeAttributeArgument_t2420111610_0_0_0,
	&GenInst_CodeTypeReference_t3809997434_0_0_0,
	&GenInst_CodeObject_t3927604602_0_0_0,
	&GenInst_GenerationResult_t2469053731_0_0_0,
	&GenInst_XmlMapping_t1653394_0_0_0,
	&GenInst_Hook_t924144422_0_0_0,
	&GenInst_XmlReflectionMember_t1313238960_0_0_0,
	&GenInst_SoapSchemaMember_t859827560_0_0_0,
	&GenInst_SoapIncludeAttribute_t666096636_0_0_0,
	&GenInst_Attribute_t861562559_0_0_0,
	&GenInst__Attribute_t122494719_0_0_0,
	&GenInst_XmlMemberMapping_t113569223_0_0_0,
	&GenInst_XmlIncludeAttribute_t1446401819_0_0_0,
	&GenInst_XmlSerializer_t1117804635_0_0_0,
	&GenInst_SerializerData_t3337767682_0_0_0,
	&GenInst_XmlTypeMapMemberAttribute_t2452898273_0_0_0,
	&GenInst_XmlTypeMapMember_t3739392753_0_0_0,
	&GenInst_XmlTypeMapElementInfo_t2741990046_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0,
	&GenInst_KeyValuePair_2_t4030379155_0_0_0,
	&GenInst_CodeExpression_t2166265795_0_0_0,
	&GenInst_CodeStatement_t371410868_0_0_0,
	&GenInst_CodeTypeMember_t1555525554_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_KeyValuePair_2_t4030379155_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0,
	&GenInst_CompilerInfo_t947794800_0_0_0,
	&GenInst_String_t_0_0_0_CompilerInfo_t947794800_0_0_0,
	&GenInst_String_t_0_0_0_CompilerInfo_t947794800_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3130723266_0_0_0,
	&GenInst_String_t_0_0_0_CompilerInfo_t947794800_0_0_0_KeyValuePair_2_t3130723266_0_0_0,
	&GenInst_String_t_0_0_0_CompilerInfo_t947794800_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_CompilerInfo_t947794800_0_0_0_CompilerInfo_t947794800_0_0_0,
	&GenInst_Node_t2057664286_0_0_0,
	&GenInst_PropertyDescriptor_t3244362832_0_0_0,
	&GenInst_MemberDescriptor_t3815403747_0_0_0,
	&GenInst_Enum_t4135868527_0_0_0,
	&GenInst_IFormattable_t1450744796_0_0_0,
	&GenInst_ValueType_t3640485471_0_0_0,
	&GenInst_AttributeU5BU5D_t1575011174_0_0_0,
	&GenInst_IList_1_t2676882342_0_0_0,
	&GenInst_ICollection_1_t3689714793_0_0_0,
	&GenInst_IEnumerable_1_t4136382744_0_0_0,
	&GenInst_IList_1_t1937814502_0_0_0,
	&GenInst_ICollection_1_t2950646953_0_0_0,
	&GenInst_IEnumerable_1_t3397314904_0_0_0,
	&GenInst_Type_t_0_0_0_LinkedList_1_t2071723904_0_0_0,
	&GenInst_TypeDescriptionProvider_t3232077895_0_0_0,
	&GenInst_LinkedList_1_t2071723904_0_0_0,
	&GenInst_Type_t_0_0_0_LinkedList_1_t2071723904_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2618775839_0_0_0,
	&GenInst_Type_t_0_0_0_LinkedList_1_t2071723904_0_0_0_KeyValuePair_2_t2618775839_0_0_0,
	&GenInst_Type_t_0_0_0_LinkedList_1_t2071723904_0_0_0_Type_t_0_0_0,
	&GenInst_Type_t_0_0_0_LinkedList_1_t2071723904_0_0_0_LinkedList_1_t2071723904_0_0_0,
	&GenInst_WeakObjectWrapper_t827463650_0_0_0_LinkedList_1_t2071723904_0_0_0,
	&GenInst_WeakObjectWrapper_t827463650_0_0_0,
	&GenInst_WeakObjectWrapper_t827463650_0_0_0_LinkedList_1_t2071723904_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3762142349_0_0_0,
	&GenInst_WeakObjectWrapper_t827463650_0_0_0_LinkedList_1_t2071723904_0_0_0_KeyValuePair_2_t3762142349_0_0_0,
	&GenInst_WeakObjectWrapper_t827463650_0_0_0_LinkedList_1_t2071723904_0_0_0_WeakObjectWrapper_t827463650_0_0_0,
	&GenInst_WeakObjectWrapper_t827463650_0_0_0_LinkedList_1_t2071723904_0_0_0_LinkedList_1_t2071723904_0_0_0,
	&GenInst_Cookie_t993873397_0_0_0,
	&GenInst_IPAddress_t241777590_0_0_0,
	&GenInst_X509ChainStatus_t133602714_0_0_0,
	&GenInst_ArraySegment_1_t283560987_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_KeyValuePair_2_t3842366416_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_KeyValuePair_2_t3842366416_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2280216431_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_KeyValuePair_2_t2280216431_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_Capture_t2232016050_0_0_0,
	&GenInst_DynamicMethod_t2537779570_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_KeyValuePair_2_t4237331251_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t4237331251_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Label_t2281661643_0_0_0,
	&GenInst_KeyValuePair_2_t3568047141_0_0_0,
	&GenInst_Label_t2281661643_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Label_t2281661643_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Label_t2281661643_0_0_0_Label_t2281661643_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Label_t2281661643_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Label_t2281661643_0_0_0_KeyValuePair_2_t3568047141_0_0_0,
	&GenInst_Group_t2468205786_0_0_0,
	&GenInst_Mark_t3471605523_0_0_0,
	&GenInst_UriScheme_t722425697_0_0_0,
	&GenInst_IEnlistmentNotification_t276083705_0_0_0,
	&GenInst_ISinglePhaseNotification_t3269684407_0_0_0,
	&GenInst_SignalHandler_t1590986791_0_0_0,
	&GenInst_MulticastDelegate_t157516450_0_0_0,
	&GenInst_Link_t3209266973_0_0_0,
	&GenInst_LockDetails_t2068764019_0_0_0,
	&GenInst_ByteU5BU5D_t4116647657_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_ByteU5BU5D_t4116647657_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_FontFamily_t13778311_0_0_0,
	&GenInst_IconImage_t1835934191_0_0_0,
	&GenInst_IconDirEntry_t3003987536_0_0_0,
	&GenInst_Color_t1869934208_0_0_0,
	&GenInst_ImageCodecInfo_t2037253290_0_0_0,
	&GenInst_ImageFormat_t730671025_0_0_0,
	&GenInst_EncoderParameter_t138078747_0_0_0,
	&GenInst_SettingsMappingWhat_t1462987973_0_0_0,
	&GenInst_Type_t_0_0_0_SettingsMapping_t162400643_0_0_0,
	&GenInst_SettingsMapping_t162400643_0_0_0,
	&GenInst_Type_t_0_0_0_SettingsMapping_t162400643_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t709452578_0_0_0,
	&GenInst_Type_t_0_0_0_SettingsMapping_t162400643_0_0_0_KeyValuePair_2_t709452578_0_0_0,
	&GenInst_Type_t_0_0_0_SettingsMapping_t162400643_0_0_0_Type_t_0_0_0,
	&GenInst_Type_t_0_0_0_SettingsMapping_t162400643_0_0_0_SettingsMapping_t162400643_0_0_0,
	&GenInst_SettingsMappingWhatContents_t3035976441_0_0_0,
	&GenInst_CodeParameterDeclarationExpression_t649585606_0_0_0,
	&GenInst_SerializationMap_t4088073813_0_0_0,
	&GenInst_DataMemberInfo_t700542586_0_0_0,
	&GenInst_Type_t_0_0_0_XmlQualifiedName_t2760654312_0_0_0,
	&GenInst_Type_t_0_0_0_XmlQualifiedName_t2760654312_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3307706247_0_0_0,
	&GenInst_Type_t_0_0_0_XmlQualifiedName_t2760654312_0_0_0_KeyValuePair_2_t3307706247_0_0_0,
	&GenInst_Type_t_0_0_0_XmlQualifiedName_t2760654312_0_0_0_Type_t_0_0_0,
	&GenInst_Type_t_0_0_0_XmlQualifiedName_t2760654312_0_0_0_XmlQualifiedName_t2760654312_0_0_0,
	&GenInst_EnumMemberInfo_t3072296154_0_0_0,
	&GenInst_AttrNodeInfo_t1586542217_0_0_0,
	&GenInst_NodeInfo_t2956564772_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_XmlDictionaryString_t3504120266_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t71524366_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t71524366_0_0_0,
	&GenInst_XmlDictionaryString_t3504120266_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_XmlDictionaryString_t3504120266_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t495538468_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_XmlDictionaryString_t3504120266_0_0_0_KeyValuePair_2_t495538468_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_XmlDictionaryString_t3504120266_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_XmlDictionaryString_t3504120266_0_0_0_XmlDictionaryString_t3504120266_0_0_0,
	&GenInst_KeyValuePair_2_t968067334_0_0_0,
	&GenInst_String_t_0_0_0_Il2CppObject_0_0_0,
	&GenInst_XmlSpace_t3324193251_0_0_0,
	&GenInst_KeyValuePair_2_t968067334_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_KeyValuePair_2_t2530217319_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_String_t_0_0_0_XmlDictionaryString_t3504120266_0_0_0,
	&GenInst_String_t_0_0_0_XmlDictionaryString_t3504120266_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1392081436_0_0_0,
	&GenInst_String_t_0_0_0_XmlDictionaryString_t3504120266_0_0_0_KeyValuePair_2_t1392081436_0_0_0,
	&GenInst_String_t_0_0_0_XmlDictionaryString_t3504120266_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_XmlDictionaryString_t3504120266_0_0_0_XmlDictionaryString_t3504120266_0_0_0,
	&GenInst_Encoding_t1523322056_0_0_0,
	&GenInst_String_t_0_0_0_MimeEncodedStream_t622371820_0_0_0,
	&GenInst_MimeEncodedStream_t622371820_0_0_0,
	&GenInst_String_t_0_0_0_MimeEncodedStream_t622371820_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2805300286_0_0_0,
	&GenInst_String_t_0_0_0_MimeEncodedStream_t622371820_0_0_0_KeyValuePair_2_t2805300286_0_0_0,
	&GenInst_String_t_0_0_0_MimeEncodedStream_t622371820_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_MimeEncodedStream_t622371820_0_0_0_MimeEncodedStream_t622371820_0_0_0,
	&GenInst_AssemblyName_t270931938_0_0_0,
	&GenInst__AssemblyName_t3550739211_0_0_0,
	&GenInst_Type_t_0_0_0_Type_t_0_0_0,
	&GenInst_Type_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3030996695_0_0_0,
	&GenInst_Type_t_0_0_0_Type_t_0_0_0_KeyValuePair_2_t3030996695_0_0_0,
	&GenInst_Type_t_0_0_0_Type_t_0_0_0_Type_t_0_0_0,
	&GenInst_Node_t2365075726_0_0_0,
	&GenInst_String_t_0_0_0_File_t3103643102_0_0_0,
	&GenInst_File_t3103643102_0_0_0,
	&GenInst_String_t_0_0_0_File_t3103643102_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t991604272_0_0_0,
	&GenInst_String_t_0_0_0_File_t3103643102_0_0_0_KeyValuePair_2_t991604272_0_0_0,
	&GenInst_String_t_0_0_0_File_t3103643102_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_File_t3103643102_0_0_0_File_t3103643102_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t280750548_0_0_0,
	&GenInst_KeyValuePair_2_t2463679014_0_0_0,
	&GenInst_List_1_t280750548_0_0_0,
	&GenInst_Match_t3408321083_0_0_0,
	&GenInst_Identification_t455147138_0_0_0,
	&GenInst_String_t_0_0_0_Node_t2365075726_0_0_0,
	&GenInst_KeyValuePair_2_t253036896_0_0_0,
	&GenInst_String_t_0_0_0_CacheItem_t2069182237_0_0_0,
	&GenInst_CacheItem_t2069182237_0_0_0,
	&GenInst_String_t_0_0_0_CacheItem_t2069182237_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t4252110703_0_0_0,
	&GenInst_String_t_0_0_0_CacheItem_t2069182237_0_0_0_KeyValuePair_2_t4252110703_0_0_0,
	&GenInst_String_t_0_0_0_CacheItem_t2069182237_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_CacheItem_t2069182237_0_0_0_CacheItem_t2069182237_0_0_0,
	&GenInst_FileSystemWatcher_t416760199_0_0_0,
	&GenInst_ISupportInitialize_t1184141141_0_0_0,
	&GenInst_Component_t3620823400_0_0_0,
	&GenInst_IComponent_t2320218252_0_0_0,
	&GenInst_DataItem_t1175161993_0_0_0,
	&GenInst_String_t_0_0_0_AspComponent_t590775874_0_0_0,
	&GenInst_AspComponent_t590775874_0_0_0,
	&GenInst_String_t_0_0_0_AspComponent_t590775874_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2773704340_0_0_0,
	&GenInst_String_t_0_0_0_AspComponent_t590775874_0_0_0_KeyValuePair_2_t2773704340_0_0_0,
	&GenInst_String_t_0_0_0_AspComponent_t590775874_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_AspComponent_t590775874_0_0_0_AspComponent_t590775874_0_0_0,
	&GenInst_String_t_0_0_0_Assembly_t4102432799_0_0_0,
	&GenInst_String_t_0_0_0_Assembly_t4102432799_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1990393969_0_0_0,
	&GenInst_String_t_0_0_0_Assembly_t4102432799_0_0_0_KeyValuePair_2_t1990393969_0_0_0,
	&GenInst_String_t_0_0_0_Assembly_t4102432799_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_Assembly_t4102432799_0_0_0_Assembly_t4102432799_0_0_0,
	&GenInst_CodeCompileUnit_t2527618915_0_0_0,
	&GenInst_AppCodeAssembly_t1261384177_0_0_0,
	&GenInst_AppResourceFileInfo_t3599744125_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t3319525431_0_0_0,
	&GenInst_List_1_t3319525431_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t3319525431_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1207486601_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t3319525431_0_0_0_KeyValuePair_2_t1207486601_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t3319525431_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t3319525431_0_0_0_List_1_t3319525431_0_0_0,
	&GenInst_String_t_0_0_0_Type_t_0_0_0,
	&GenInst_String_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t371905930_0_0_0,
	&GenInst_String_t_0_0_0_Type_t_0_0_0_KeyValuePair_2_t371905930_0_0_0,
	&GenInst_String_t_0_0_0_Type_t_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_Type_t_0_0_0_Type_t_0_0_0,
	&GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_String_t_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t968067334_0_0_0,
	&GenInst_String_t_0_0_0_Il2CppObject_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TextBlock_t3735829980_0_0_0,
	&GenInst_ServerSideScript_t2595428029_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t715253879_0_0_0,
	&GenInst_CompileUnitPartialType_t3538146433_0_0_0,
	&GenInst_List_1_t715253879_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t715253879_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2898182345_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t715253879_0_0_0_KeyValuePair_2_t2898182345_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t715253879_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t715253879_0_0_0_List_1_t715253879_0_0_0,
	&GenInst_String_t_0_0_0_BuildProvider_t3736381005_0_0_0,
	&GenInst_BuildProvider_t3736381005_0_0_0,
	&GenInst_String_t_0_0_0_BuildProvider_t3736381005_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1624342175_0_0_0,
	&GenInst_String_t_0_0_0_BuildProvider_t3736381005_0_0_0_KeyValuePair_2_t1624342175_0_0_0,
	&GenInst_String_t_0_0_0_BuildProvider_t3736381005_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_BuildProvider_t3736381005_0_0_0_BuildProvider_t3736381005_0_0_0,
	&GenInst_CodeUnit_t1519973372_0_0_0,
	&GenInst_Type_t_0_0_0_CodeDomProvider_t110349836_0_0_0,
	&GenInst_CodeDomProvider_t110349836_0_0_0,
	&GenInst_Type_t_0_0_0_CodeDomProvider_t110349836_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t657401771_0_0_0,
	&GenInst_Type_t_0_0_0_CodeDomProvider_t110349836_0_0_0_KeyValuePair_2_t657401771_0_0_0,
	&GenInst_Type_t_0_0_0_CodeDomProvider_t110349836_0_0_0_Type_t_0_0_0,
	&GenInst_Type_t_0_0_0_CodeDomProvider_t110349836_0_0_0_CodeDomProvider_t110349836_0_0_0,
	&GenInst_String_t_0_0_0_BuildManagerCacheItem_t1643815903_0_0_0,
	&GenInst_BuildManagerCacheItem_t1643815903_0_0_0,
	&GenInst_String_t_0_0_0_BuildManagerCacheItem_t1643815903_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3826744369_0_0_0,
	&GenInst_String_t_0_0_0_BuildManagerCacheItem_t1643815903_0_0_0_KeyValuePair_2_t3826744369_0_0_0,
	&GenInst_String_t_0_0_0_BuildManagerCacheItem_t1643815903_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_BuildManagerCacheItem_t1643815903_0_0_0_BuildManagerCacheItem_t1643815903_0_0_0,
	&GenInst_String_t_0_0_0_PreCompilationData_t3220258209_0_0_0,
	&GenInst_PreCompilationData_t3220258209_0_0_0,
	&GenInst_String_t_0_0_0_PreCompilationData_t3220258209_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1108219379_0_0_0,
	&GenInst_String_t_0_0_0_PreCompilationData_t3220258209_0_0_0_KeyValuePair_2_t1108219379_0_0_0,
	&GenInst_String_t_0_0_0_PreCompilationData_t3220258209_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_PreCompilationData_t3220258209_0_0_0_PreCompilationData_t3220258209_0_0_0,
	&GenInst_BuildProviderGroup_t619564070_0_0_0,
	&GenInst_List_1_t913488451_0_0_0,
	&GenInst_BuildProvider_t3736381005_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_BuildProvider_t3736381005_0_0_0_Boolean_t97287965_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3833317299_0_0_0,
	&GenInst_BuildProvider_t3736381005_0_0_0_Boolean_t97287965_0_0_0_KeyValuePair_2_t3833317299_0_0_0,
	&GenInst_BuildProvider_t3736381005_0_0_0_Boolean_t97287965_0_0_0_BuildProvider_t3736381005_0_0_0,
	&GenInst_BuildProvider_t3736381005_0_0_0_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_ResourceManagerCacheKey_t3697749236_0_0_0_ResourceManager_t4037989559_0_0_0,
	&GenInst_ResourceManagerCacheKey_t3697749236_0_0_0,
	&GenInst_ResourceManager_t4037989559_0_0_0,
	&GenInst_ResourceManagerCacheKey_t3697749236_0_0_0_ResourceManager_t4037989559_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2472737834_0_0_0,
	&GenInst_ResourceManagerCacheKey_t3697749236_0_0_0_ResourceManager_t4037989559_0_0_0_KeyValuePair_2_t2472737834_0_0_0,
	&GenInst_ResourceManagerCacheKey_t3697749236_0_0_0_ResourceManager_t4037989559_0_0_0_ResourceManagerCacheKey_t3697749236_0_0_0,
	&GenInst_ResourceManagerCacheKey_t3697749236_0_0_0_ResourceManager_t4037989559_0_0_0_ResourceManager_t4037989559_0_0_0,
	&GenInst_TemplateParser_t24149626_0_0_0,
	&GenInst_String_t_0_0_0_ExtractDirectiveDependencies_t2024460703_0_0_0,
	&GenInst_ExtractDirectiveDependencies_t2024460703_0_0_0,
	&GenInst_KeyValuePair_2_t4207389169_0_0_0,
	&GenInst_UnknownAttributeDescriptor_t1586516698_0_0_0,
	&GenInst_TemplateInstance_t2414853837_0_0_0,
	&GenInst_String_t_0_0_0_DateTime_t3738529785_0_0_0,
	&GenInst_Il2CppObject_0_0_0_DateTime_t3738529785_0_0_0,
	&GenInst_KeyValuePair_2_t3188640940_0_0_0,
	&GenInst_Il2CppObject_0_0_0_DateTime_t3738529785_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_DateTime_t3738529785_0_0_0_DateTime_t3738529785_0_0_0,
	&GenInst_Il2CppObject_0_0_0_DateTime_t3738529785_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_Il2CppObject_0_0_0_DateTime_t3738529785_0_0_0_KeyValuePair_2_t3188640940_0_0_0,
	&GenInst_String_t_0_0_0_DateTime_t3738529785_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1626490955_0_0_0,
	&GenInst_String_t_0_0_0_DateTime_t3738529785_0_0_0_KeyValuePair_2_t1626490955_0_0_0,
	&GenInst_String_t_0_0_0_DateTime_t3738529785_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_DateTime_t3738529785_0_0_0_DateTime_t3738529785_0_0_0,
	&GenInst_WebResourceAttribute_t4132945045_0_0_0,
	&GenInst_StringU5BU5D_t1281789340_0_0_0,
	&GenInst_IList_1_t3662770472_0_0_0,
	&GenInst_ICollection_1_t380635627_0_0_0,
	&GenInst_IEnumerable_1_t827303578_0_0_0,
	&GenInst_IList_1_t497718164_0_0_0,
	&GenInst_ICollection_1_t1510550615_0_0_0,
	&GenInst_IEnumerable_1_t1957218566_0_0_0,
	&GenInst_IList_1_t1851431001_0_0_0,
	&GenInst_ICollection_1_t2864263452_0_0_0,
	&GenInst_IEnumerable_1_t3310931403_0_0_0,
	&GenInst_IList_1_t3756487794_0_0_0,
	&GenInst_ICollection_1_t474352949_0_0_0,
	&GenInst_IEnumerable_1_t921020900_0_0_0,
	&GenInst_IList_1_t2539743981_0_0_0,
	&GenInst_ICollection_1_t3552576432_0_0_0,
	&GenInst_IEnumerable_1_t3999244383_0_0_0,
	&GenInst_IList_1_t3031434885_0_0_0,
	&GenInst_ICollection_1_t4044267336_0_0_0,
	&GenInst_IEnumerable_1_t195967991_0_0_0,
	&GenInst_IList_1_t258948903_0_0_0,
	&GenInst_ICollection_1_t1271781354_0_0_0,
	&GenInst_IEnumerable_1_t1718449305_0_0_0,
	&GenInst_String_t_0_0_0_IResourceProvider_t232608435_0_0_0,
	&GenInst_IResourceProvider_t232608435_0_0_0,
	&GenInst_String_t_0_0_0_IResourceProvider_t232608435_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2415536901_0_0_0,
	&GenInst_String_t_0_0_0_IResourceProvider_t232608435_0_0_0_KeyValuePair_2_t2415536901_0_0_0,
	&GenInst_String_t_0_0_0_IResourceProvider_t232608435_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_IResourceProvider_t232608435_0_0_0_IResourceProvider_t232608435_0_0_0,
	&GenInst_IHttpHandler_t2624218893_0_0_0,
	&GenInst_InfoTraceData_t1062706700_0_0_0,
	&GenInst_ControlTraceData_t1335153449_0_0_0,
	&GenInst_NameValueTraceData_t2568000481_0_0_0,
	&GenInst_TraceData_t4177544667_0_0_0,
	&GenInst_Control_t3006474639_0_0_0,
	&GenInst_IControlBuilderAccessor_t1240310411_0_0_0,
	&GenInst_IControlDesignerAccessor_t1554684719_0_0_0,
	&GenInst_IDataBindingsAccessor_t834853709_0_0_0,
	&GenInst_IExpressionsAccessor_t2801768539_0_0_0,
	&GenInst_IParserAccessor_t2616046584_0_0_0,
	&GenInst_IUrlResolutionService_t4016525173_0_0_0,
	&GenInst_Type_t_0_0_0_PropertyInfo_t_0_0_0,
	&GenInst_Type_t_0_0_0_PropertyInfo_t_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1234393886_0_0_0,
	&GenInst_Type_t_0_0_0_PropertyInfo_t_0_0_0_KeyValuePair_2_t1234393886_0_0_0,
	&GenInst_Type_t_0_0_0_PropertyInfo_t_0_0_0_Type_t_0_0_0,
	&GenInst_Type_t_0_0_0_PropertyInfo_t_0_0_0_PropertyInfo_t_0_0_0,
	&GenInst_AddedTag_t1198678936_0_0_0,
	&GenInst_AddedStyle_t611321135_0_0_0,
	&GenInst_AddedAttr_t2359971688_0_0_0,
	&GenInst_HtmlTag_t2430431696_0_0_0,
	&GenInst_HtmlAttribute_t421148674_0_0_0,
	&GenInst_HtmlStyle_t2610452647_0_0_0,
	&GenInst_ObjectFormatter_t1981273209_0_0_0,
	&GenInst_PageAsyncTask_t4073791294_0_0_0,
	&GenInst_IAsyncResult_t767004451_0_0_0,
	&GenInst_TemplateBinding_t3663525489_0_0_0,
	&GenInst_Claim_t2327046348_0_0_0,
	&GenInst_IAuthorizationPolicy_t3860878873_0_0_0,
	&GenInst_IAuthorizationComponent_t1761415880_0_0_0,
	&GenInst_ClaimSet_t3529661467_0_0_0,
	&GenInst_IAuthorizationPolicy_t3860878873_0_0_0_ClaimSet_t3529661467_0_0_0,
	&GenInst_IAuthorizationPolicy_t3860878873_0_0_0_ClaimSet_t3529661467_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t749215797_0_0_0,
	&GenInst_IAuthorizationPolicy_t3860878873_0_0_0_ClaimSet_t3529661467_0_0_0_KeyValuePair_2_t749215797_0_0_0,
	&GenInst_IAuthorizationPolicy_t3860878873_0_0_0_ClaimSet_t3529661467_0_0_0_IAuthorizationPolicy_t3860878873_0_0_0,
	&GenInst_IAuthorizationPolicy_t3860878873_0_0_0_ClaimSet_t3529661467_0_0_0_ClaimSet_t3529661467_0_0_0,
	&GenInst_IIdentity_t2948385546_0_0_0,
	&GenInst_SecurityToken_t1271873540_0_0_0,
	&GenInst_SecurityKeyIdentifierClause_t1943429813_0_0_0,
	&GenInst_SecurityKey_t4128675802_0_0_0,
	&GenInst_jvalue_t1372148875_0_0_0,
	&GenInst_AndroidJavaObject_t4131667876_0_0_0,
	&GenInst_Object_t631007953_0_0_0,
	&GenInst_Camera_t4157153871_0_0_0,
	&GenInst_Behaviour_t1437897464_0_0_0,
	&GenInst_Component_t1923634451_0_0_0,
	&GenInst_Display_t1387065949_0_0_0,
	&GenInst_Boolean_t97287965_0_0_0_String_t_0_0_0,
	&GenInst_Boolean_t97287965_0_0_0_Il2CppObject_0_0_0,
	&GenInst_AchievementDescription_t3217594527_0_0_0,
	&GenInst_IAchievementDescription_t2514275728_0_0_0,
	&GenInst_UserProfile_t3137328177_0_0_0,
	&GenInst_IUserProfile_t360500636_0_0_0,
	&GenInst_GcLeaderboard_t4132273028_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t1821964849_0_0_0,
	&GenInst_IAchievementU5BU5D_t1892338339_0_0_0,
	&GenInst_IAchievement_t1421108358_0_0_0,
	&GenInst_GcAchievementData_t675222246_0_0_0,
	&GenInst_Achievement_t565359984_0_0_0,
	&GenInst_IScoreU5BU5D_t527871248_0_0_0,
	&GenInst_IScore_t2559910621_0_0_0,
	&GenInst_GcScoreData_t2125309831_0_0_0,
	&GenInst_Score_t1968645328_0_0_0,
	&GenInst_IUserProfileU5BU5D_t909679733_0_0_0,
	&GenInst_Material_t340375123_0_0_0,
	&GenInst_Vector2_t2156229523_0_0_0,
	&GenInst_RenderBuffer_t586150500_0_0_0,
	&GenInst_Plane_t1000493321_0_0_0,
	&GenInst_Touch_t1921856868_0_0_0,
	&GenInst_Keyframe_t4206410242_0_0_0,
	&GenInst_Vector3_t3722313464_0_0_0,
	&GenInst_Vector4_t3319028937_0_0_0,
	&GenInst_Color_t2555686324_0_0_0,
	&GenInst_Color32_t2600501292_0_0_0,
	&GenInst_RenderTargetIdentifier_t2079184500_0_0_0,
	&GenInst_Scene_t2348375561_0_0_0_LoadSceneMode_t3251202195_0_0_0,
	&GenInst_Scene_t2348375561_0_0_0,
	&GenInst_Scene_t2348375561_0_0_0_Scene_t2348375561_0_0_0,
	&GenInst_ContactPoint_t3758755253_0_0_0,
	&GenInst_RaycastHit_t1056001966_0_0_0,
	&GenInst_Collider_t1773347010_0_0_0,
	&GenInst_Rigidbody2D_t939494601_0_0_0,
	&GenInst_RaycastHit2D_t2279581989_0_0_0,
	&GenInst_Collider2D_t2806799626_0_0_0,
	&GenInst_ContactPoint2D_t3390240644_0_0_0,
	&GenInst_WebCamDevice_t1322781432_0_0_0,
	&GenInst_AnimatorClipInfo_t3156717155_0_0_0,
	&GenInst_AnimatorControllerParameter_t1758260042_0_0_0,
	&GenInst_UIVertex_t4057497605_0_0_0,
	&GenInst_UICharInfo_t75501106_0_0_0,
	&GenInst_UILineInfo_t4195266810_0_0_0,
	&GenInst_Font_t1956802104_0_0_0,
	&GenInst_GUILayoutOption_t811797299_0_0_0,
	&GenInst_GUILayoutEntry_t3214611570_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0,
	&GenInst_LayoutCache_t78309876_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1364695374_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_KeyValuePair_2_t1364695374_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_LayoutCache_t78309876_0_0_0,
	&GenInst_GUIStyle_t3956901511_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1844862681_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_KeyValuePair_2_t1844862681_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_GUIStyle_t3956901511_0_0_0,
	&GenInst_Event_t2956885303_0_0_0_TextEditOp_t1927482598_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t1927482598_0_0_0,
	&GenInst_KeyValuePair_2_t1377593753_0_0_0,
	&GenInst_TextEditOp_t1927482598_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t1927482598_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t1927482598_0_0_0_TextEditOp_t1927482598_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t1927482598_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t1927482598_0_0_0_KeyValuePair_2_t1377593753_0_0_0,
	&GenInst_Event_t2956885303_0_0_0,
	&GenInst_Event_t2956885303_0_0_0_TextEditOp_t1927482598_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1157710762_0_0_0,
	&GenInst_Event_t2956885303_0_0_0_TextEditOp_t1927482598_0_0_0_KeyValuePair_2_t1157710762_0_0_0,
	&GenInst_Event_t2956885303_0_0_0_TextEditOp_t1927482598_0_0_0_Event_t2956885303_0_0_0,
	&GenInst_Event_t2956885303_0_0_0_TextEditOp_t1927482598_0_0_0_TextEditOp_t1927482598_0_0_0,
	&GenInst_DisallowMultipleComponent_t1422053217_0_0_0,
	&GenInst_ExecuteInEditMode_t3727731349_0_0_0,
	&GenInst_RequireComponent_t3490506609_0_0_0,
	&GenInst_HitInfo_t3229609740_0_0_0,
	&GenInst_PersistentCall_t3407714124_0_0_0,
	&GenInst_BaseInvokableCall_t2703961024_0_0_0,
	&GenInst_MessageTypeSubscribers_t1684935770_0_0_0,
	&GenInst_MessageTypeSubscribers_t1684935770_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_MessageEventArgs_t1170575784_0_0_0,
	&GenInst_VuforiaAbstractBehaviour_t3510878193_0_0_0,
	&GenInst_TrackableBehaviour_t1113559212_0_0_0,
	&GenInst_MonoBehaviour_t3962482529_0_0_0,
	&GenInst_TrackableIdPair_t4227350457_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_GameObject_t1113636619_0_0_0,
	&GenInst_RenderTexture_t2108887433_0_0_0,
	&GenInst_Texture_t3661962703_0_0_0,
	&GenInst_EyewearCalibrationReading_t664929988_0_0_0,
	&GenInst_Camera_t4157153871_0_0_0_VideoBackgroundAbstractBehaviour_t293578642_0_0_0,
	&GenInst_VideoBackgroundAbstractBehaviour_t293578642_0_0_0,
	&GenInst_Camera_t4157153871_0_0_0_VideoBackgroundAbstractBehaviour_t293578642_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2904705502_0_0_0,
	&GenInst_Camera_t4157153871_0_0_0_VideoBackgroundAbstractBehaviour_t293578642_0_0_0_KeyValuePair_2_t2904705502_0_0_0,
	&GenInst_Camera_t4157153871_0_0_0_VideoBackgroundAbstractBehaviour_t293578642_0_0_0_Camera_t4157153871_0_0_0,
	&GenInst_Camera_t4157153871_0_0_0_VideoBackgroundAbstractBehaviour_t293578642_0_0_0_VideoBackgroundAbstractBehaviour_t293578642_0_0_0,
	&GenInst_Renderer_t2627027031_0_0_0,
	&GenInst_TrackableIdPair_t4227350457_0_0_0_PoseAgeEntry_t2181165958_0_0_0,
	&GenInst_KeyValuePair_2_t3558069120_0_0_0,
	&GenInst_PoseAgeEntry_t2181165958_0_0_0,
	&GenInst_TrackableIdPair_t4227350457_0_0_0,
	&GenInst_TrackableIdPair_t4227350457_0_0_0_PoseAgeEntry_t2181165958_0_0_0_TrackableIdPair_t4227350457_0_0_0,
	&GenInst_TrackableIdPair_t4227350457_0_0_0_PoseAgeEntry_t2181165958_0_0_0_PoseAgeEntry_t2181165958_0_0_0,
	&GenInst_TrackableIdPair_t4227350457_0_0_0_PoseAgeEntry_t2181165958_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_TrackableIdPair_t4227350457_0_0_0_PoseAgeEntry_t2181165958_0_0_0_KeyValuePair_2_t3558069120_0_0_0,
	&GenInst_Link_t3080106562_0_0_0,
	&GenInst_TrackableIdPair_t4227350457_0_0_0_PoseInfo_t1612729179_0_0_0,
	&GenInst_KeyValuePair_2_t2989632341_0_0_0,
	&GenInst_PoseInfo_t1612729179_0_0_0,
	&GenInst_TrackableIdPair_t4227350457_0_0_0_PoseInfo_t1612729179_0_0_0_TrackableIdPair_t4227350457_0_0_0,
	&GenInst_TrackableIdPair_t4227350457_0_0_0_PoseInfo_t1612729179_0_0_0_PoseInfo_t1612729179_0_0_0,
	&GenInst_TrackableIdPair_t4227350457_0_0_0_PoseInfo_t1612729179_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_TrackableIdPair_t4227350457_0_0_0_PoseInfo_t1612729179_0_0_0_KeyValuePair_2_t2989632341_0_0_0,
	&GenInst_TrackableIdPair_t4227350457_0_0_0_Status_t1100905814_0_0_0,
	&GenInst_KeyValuePair_2_t2477808976_0_0_0,
	&GenInst_Status_t1100905814_0_0_0,
	&GenInst_TrackableIdPair_t4227350457_0_0_0_Status_t1100905814_0_0_0_TrackableIdPair_t4227350457_0_0_0,
	&GenInst_TrackableIdPair_t4227350457_0_0_0_Status_t1100905814_0_0_0_Status_t1100905814_0_0_0,
	&GenInst_TrackableIdPair_t4227350457_0_0_0_Status_t1100905814_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_TrackableIdPair_t4227350457_0_0_0_Status_t1100905814_0_0_0_KeyValuePair_2_t2477808976_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_List_1_t2052726287_0_0_0,
	&GenInst_VuMarkAbstractBehaviour_t580651545_0_0_0,
	&GenInst_DataSetTrackableBehaviour_t3430730379_0_0_0,
	&GenInst_WorldCenterTrackableBehaviour_t632567575_0_0_0,
	&GenInst_IEditDataSetBehaviour_t502520577_0_0_0,
	&GenInst_List_1_t2052726287_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_List_1_t2052726287_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3339111785_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_List_1_t2052726287_0_0_0_KeyValuePair_2_t3339111785_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_List_1_t2052726287_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_List_1_t2052726287_0_0_0_List_1_t2052726287_0_0_0,
	&GenInst_VuMarkTarget_t1129573803_0_0_0,
	&GenInst_ObjectTarget_t3212252422_0_0_0,
	&GenInst_ExtendedTrackable_t2690660116_0_0_0,
	&GenInst_Trackable_t2451999991_0_0_0,
	&GenInst_VuMarkTargetData_t3925829072_0_0_0,
	&GenInst_VuMarkTargetResultData_t1414459591_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_List_1_t128053199_0_0_0,
	&GenInst_List_1_t128053199_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_List_1_t128053199_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1414438697_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_List_1_t128053199_0_0_0_KeyValuePair_2_t1414438697_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_List_1_t128053199_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_List_1_t128053199_0_0_0_List_1_t128053199_0_0_0,
	&GenInst_TrackableResultData_t3800049328_0_0_0,
	&GenInst_HideExcessAreaAbstractBehaviour_t3369390328_0_0_0,
	&GenInst_IViewerParameters_t2017581997_0_0_0,
	&GenInst_CameraField_t1483002240_0_0_0,
	&GenInst_ICloudRecoEventHandler_t3872625445_0_0_0,
	&GenInst_TargetSearchResult_t3441982613_0_0_0,
	&GenInst_ReconstructionAbstractBehaviour_t818896732_0_0_0,
	&GenInst_InitError_t3420749710_0_0_0,
	&GenInst_VirtualButton_t386166510_0_0_0,
	&GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Image_t745056343_0_0_0,
	&GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t4068375620_0_0_0,
	&GenInst_PIXEL_FORMAT_t3209881435_0_0_0,
	&GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Il2CppObject_0_0_0_PIXEL_FORMAT_t3209881435_0_0_0,
	&GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4068375620_0_0_0,
	&GenInst_Image_t745056343_0_0_0,
	&GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Image_t745056343_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1733325799_0_0_0,
	&GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Image_t745056343_0_0_0_KeyValuePair_2_t1733325799_0_0_0,
	&GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Image_t745056343_0_0_0_PIXEL_FORMAT_t3209881435_0_0_0,
	&GenInst_PIXEL_FORMAT_t3209881435_0_0_0_Image_t745056343_0_0_0_Image_t745056343_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Trackable_t2451999991_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Trackable_t2451999991_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3738385489_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Trackable_t2451999991_0_0_0_KeyValuePair_2_t3738385489_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Trackable_t2451999991_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Trackable_t2451999991_0_0_0_Trackable_t2451999991_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_VirtualButton_t386166510_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_VirtualButton_t386166510_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1672552008_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_VirtualButton_t386166510_0_0_0_KeyValuePair_2_t1672552008_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_VirtualButton_t386166510_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_VirtualButton_t386166510_0_0_0_VirtualButton_t386166510_0_0_0,
	&GenInst_DataSet_t3286034874_0_0_0,
	&GenInst_DataSetImpl_t2094717509_0_0_0,
	&GenInst_WordData_t2883825721_0_0_0,
	&GenInst_WordResultData_t1835118957_0_0_0,
	&GenInst_SmartTerrainRevisionData_t2744445717_0_0_0,
	&GenInst_SurfaceData_t1300926610_0_0_0,
	&GenInst_PropData_t489181770_0_0_0,
	&GenInst_SmartTerrainTrackableBehaviour_t495369070_0_0_0,
	&GenInst_SmartTerrainTrackable_t3754799548_0_0_0,
	&GenInst_Type_t_0_0_0_UInt16_t2177724958_0_0_0,
	&GenInst_Il2CppObject_0_0_0_UInt16_t2177724958_0_0_0,
	&GenInst_KeyValuePair_2_t1627836113_0_0_0,
	&GenInst_Il2CppObject_0_0_0_UInt16_t2177724958_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_UInt16_t2177724958_0_0_0_UInt16_t2177724958_0_0_0,
	&GenInst_Il2CppObject_0_0_0_UInt16_t2177724958_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_Il2CppObject_0_0_0_UInt16_t2177724958_0_0_0_KeyValuePair_2_t1627836113_0_0_0,
	&GenInst_Type_t_0_0_0_UInt16_t2177724958_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2724776893_0_0_0,
	&GenInst_Type_t_0_0_0_UInt16_t2177724958_0_0_0_KeyValuePair_2_t2724776893_0_0_0,
	&GenInst_Type_t_0_0_0_UInt16_t2177724958_0_0_0_Type_t_0_0_0,
	&GenInst_Type_t_0_0_0_UInt16_t2177724958_0_0_0_UInt16_t2177724958_0_0_0,
	&GenInst_RectangleData_t1039179782_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_WordResult_t3640773802_0_0_0,
	&GenInst_WordResult_t3640773802_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_WordResult_t3640773802_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t632192004_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_WordResult_t3640773802_0_0_0_KeyValuePair_2_t632192004_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_WordResult_t3640773802_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_WordResult_t3640773802_0_0_0_WordResult_t3640773802_0_0_0,
	&GenInst_Word_t1116038618_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_WordAbstractBehaviour_t3502498754_0_0_0,
	&GenInst_WordAbstractBehaviour_t3502498754_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_WordAbstractBehaviour_t3502498754_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t493916956_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_WordAbstractBehaviour_t3502498754_0_0_0_KeyValuePair_2_t493916956_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_WordAbstractBehaviour_t3502498754_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_WordAbstractBehaviour_t3502498754_0_0_0_WordAbstractBehaviour_t3502498754_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t679606200_0_0_0,
	&GenInst_List_1_t679606200_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t679606200_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2862534666_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t679606200_0_0_0_KeyValuePair_2_t2862534666_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t679606200_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t679606200_0_0_0_List_1_t679606200_0_0_0,
	&GenInst_ISmartTerrainEventHandler_t439660020_0_0_0,
	&GenInst_SmartTerrainInitializationInfo_t1789741059_0_0_0,
	&GenInst_Prop_t3878399200_0_0_0,
	&GenInst_Surface_t1158577034_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Surface_t1158577034_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Surface_t1158577034_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2444962532_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Surface_t1158577034_0_0_0_KeyValuePair_2_t2444962532_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Surface_t1158577034_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Surface_t1158577034_0_0_0_Surface_t1158577034_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_SurfaceAbstractBehaviour_t3611421852_0_0_0,
	&GenInst_SurfaceAbstractBehaviour_t3611421852_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_SurfaceAbstractBehaviour_t3611421852_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t602840054_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_SurfaceAbstractBehaviour_t3611421852_0_0_0_KeyValuePair_2_t602840054_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_SurfaceAbstractBehaviour_t3611421852_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_SurfaceAbstractBehaviour_t3611421852_0_0_0_SurfaceAbstractBehaviour_t3611421852_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Prop_t3878399200_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Prop_t3878399200_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t869817402_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Prop_t3878399200_0_0_0_KeyValuePair_2_t869817402_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Prop_t3878399200_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Prop_t3878399200_0_0_0_Prop_t3878399200_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_PropAbstractBehaviour_t2080236229_0_0_0,
	&GenInst_PropAbstractBehaviour_t2080236229_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_PropAbstractBehaviour_t2080236229_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3366621727_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_PropAbstractBehaviour_t2080236229_0_0_0_KeyValuePair_2_t3366621727_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_PropAbstractBehaviour_t2080236229_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_PropAbstractBehaviour_t2080236229_0_0_0_PropAbstractBehaviour_t2080236229_0_0_0,
	&GenInst_MeshFilter_t3523625662_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_TrackableBehaviour_t1113559212_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_TrackableBehaviour_t1113559212_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2399944710_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_TrackableBehaviour_t1113559212_0_0_0_KeyValuePair_2_t2399944710_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_TrackableBehaviour_t1113559212_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_TrackableBehaviour_t1113559212_0_0_0_TrackableBehaviour_t1113559212_0_0_0,
	&GenInst_VirtualButtonAbstractBehaviour_t2408702468_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Status_t1100905814_0_0_0,
	&GenInst_KeyValuePair_2_t2387291312_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Status_t1100905814_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Status_t1100905814_0_0_0_Status_t1100905814_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Status_t1100905814_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Status_t1100905814_0_0_0_KeyValuePair_2_t2387291312_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_VirtualButtonData_t2901758114_0_0_0,
	&GenInst_KeyValuePair_2_t4188143612_0_0_0,
	&GenInst_VirtualButtonData_t2901758114_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_VirtualButtonData_t2901758114_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_VirtualButtonData_t2901758114_0_0_0_VirtualButtonData_t2901758114_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_VirtualButtonData_t2901758114_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_VirtualButtonData_t2901758114_0_0_0_KeyValuePair_2_t4188143612_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_ImageTarget_t3707016494_0_0_0,
	&GenInst_ImageTarget_t3707016494_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_ImageTarget_t3707016494_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t698434696_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_ImageTarget_t3707016494_0_0_0_KeyValuePair_2_t698434696_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_ImageTarget_t3707016494_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_ImageTarget_t3707016494_0_0_0_ImageTarget_t3707016494_0_0_0,
	&GenInst_String_t_0_0_0_ProfileData_t3519391925_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ProfileData_t3519391925_0_0_0,
	&GenInst_KeyValuePair_2_t2969503080_0_0_0,
	&GenInst_ProfileData_t3519391925_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ProfileData_t3519391925_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ProfileData_t3519391925_0_0_0_ProfileData_t3519391925_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ProfileData_t3519391925_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ProfileData_t3519391925_0_0_0_KeyValuePair_2_t2969503080_0_0_0,
	&GenInst_String_t_0_0_0_ProfileData_t3519391925_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1407353095_0_0_0,
	&GenInst_String_t_0_0_0_ProfileData_t3519391925_0_0_0_KeyValuePair_2_t1407353095_0_0_0,
	&GenInst_String_t_0_0_0_ProfileData_t3519391925_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_ProfileData_t3519391925_0_0_0_ProfileData_t3519391925_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_VirtualButtonAbstractBehaviour_t2408702468_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_VirtualButtonAbstractBehaviour_t2408702468_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3695087966_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_VirtualButtonAbstractBehaviour_t2408702468_0_0_0_KeyValuePair_2_t3695087966_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_VirtualButtonAbstractBehaviour_t2408702468_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_VirtualButtonAbstractBehaviour_t2408702468_0_0_0_VirtualButtonAbstractBehaviour_t2408702468_0_0_0,
	&GenInst_ITrackerEventHandler_t2342393219_0_0_0,
	&GenInst_IVideoBackgroundEventHandler_t3728063431_0_0_0,
	&GenInst_BackgroundPlaneAbstractBehaviour_t4147679365_0_0_0,
	&GenInst_ITextRecoEventHandler_t196339532_0_0_0,
	&GenInst_ITrackableEventHandler_t1495975588_0_0_0,
	&GenInst_IUserDefinedTargetEventHandler_t1256813275_0_0_0,
	&GenInst_MeshRenderer_t587009260_0_0_0,
	&GenInst_Link_t716170069_0_0_0,
	&GenInst_Link_t422739451_0_0_0,
	&GenInst_Link_t4276840174_0_0_0,
	&GenInst_Link_t3498551137_0_0_0,
	&GenInst_IVirtualButtonEventHandler_t3188643434_0_0_0,
	&GenInst_List_1_t942764925_0_0_0,
	&GenInst_DispatcherKey_t2110064572_0_0_0_Dispatcher_t684365006_0_0_0,
	&GenInst_DispatcherKey_t2110064572_0_0_0,
	&GenInst_Dispatcher_t684365006_0_0_0,
	&GenInst_DispatcherKey_t2110064572_0_0_0_Dispatcher_t684365006_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3263919449_0_0_0,
	&GenInst_DispatcherKey_t2110064572_0_0_0_Dispatcher_t684365006_0_0_0_KeyValuePair_2_t3263919449_0_0_0,
	&GenInst_DispatcherKey_t2110064572_0_0_0_Dispatcher_t684365006_0_0_0_DispatcherKey_t2110064572_0_0_0,
	&GenInst_DispatcherKey_t2110064572_0_0_0_Dispatcher_t684365006_0_0_0_Dispatcher_t684365006_0_0_0,
	&GenInst_CodeExpression_t2163794278_0_0_0,
	&GenInst_CodeItem_t493730090_0_0_0,
	&GenInst_ClientCertificateType_t1004704909_0_0_0,
	&GenInst_DTMXPathLinkedNode2_t3353097824_0_0_0,
	&GenInst_DTMXPathAttributeNode2_t3707096873_0_0_0,
	&GenInst_DTMXPathNamespaceNode2_t1119120713_0_0_0,
	&GenInst_Type_t_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Type_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3627158099_0_0_0,
	&GenInst_Type_t_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3627158099_0_0_0,
	&GenInst_Type_t_0_0_0_Il2CppObject_0_0_0_Type_t_0_0_0,
	&GenInst_Type_t_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Uri_t100236324_0_0_0,
	&GenInst_ServiceHostBase_t3741910535_0_0_0,
	&GenInst_AddressHeader_t3096310965_0_0_0,
	&GenInst_IReplyChannel_t3856619590_0_0_0,
	&GenInst_TimeSpan_t881159249_0_0_0_Il2CppObject_0_0_0,
	&GenInst_BindingElement_t3643137745_0_0_0,
	&GenInst_TimeSpan_t881159249_0_0_0_IReplyChannel_t3856619590_0_0_0,
	&GenInst_IChannel_t937207545_0_0_0,
	&GenInst_ICommunicationObject_t3884464922_0_0_0,
	&GenInst_HttpContext_t1969259010_0_0_0,
	&GenInst_IServiceProvider_t1439597990_0_0_0,
	&GenInst_HttpContextInfo_t3561535433_0_0_0,
	&GenInst_Uri_t100236324_0_0_0_HttpListener_t988452056_0_0_0,
	&GenInst_HttpListener_t988452056_0_0_0,
	&GenInst_Uri_t100236324_0_0_0_HttpListener_t988452056_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2563037787_0_0_0,
	&GenInst_Uri_t100236324_0_0_0_HttpListener_t988452056_0_0_0_KeyValuePair_2_t2563037787_0_0_0,
	&GenInst_Uri_t100236324_0_0_0_HttpListener_t988452056_0_0_0_Uri_t100236324_0_0_0,
	&GenInst_Uri_t100236324_0_0_0_HttpListener_t988452056_0_0_0_HttpListener_t988452056_0_0_0,
	&GenInst_IChannelListener_t114797722_0_0_0_HttpContext_t1969259010_0_0_0,
	&GenInst_Uri_t100236324_0_0_0_List_1_t1586872464_0_0_0,
	&GenInst_IChannelListener_t114797722_0_0_0,
	&GenInst_List_1_t1586872464_0_0_0,
	&GenInst_Uri_t100236324_0_0_0_List_1_t1586872464_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3161458195_0_0_0,
	&GenInst_Uri_t100236324_0_0_0_List_1_t1586872464_0_0_0_KeyValuePair_2_t3161458195_0_0_0,
	&GenInst_Uri_t100236324_0_0_0_List_1_t1586872464_0_0_0_Uri_t100236324_0_0_0,
	&GenInst_Uri_t100236324_0_0_0_List_1_t1586872464_0_0_0_List_1_t1586872464_0_0_0,
	&GenInst_HttpListenerContext_t424880822_0_0_0,
	&GenInst_IDuplexSession_t2479312727_0_0_0,
	&GenInst_IInputSession_t208887958_0_0_0,
	&GenInst_IOutputSession_t2452150453_0_0_0,
	&GenInst_MessageHeaderInfo_t464847589_0_0_0,
	&GenInst_FaultReasonText_t3547351049_0_0_0,
	&GenInst_Type_t_0_0_0_XmlObjectSerializer_t3967301761_0_0_0,
	&GenInst_XmlObjectSerializer_t3967301761_0_0_0,
	&GenInst_Type_t_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t219386400_0_0_0,
	&GenInst_Type_t_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_KeyValuePair_2_t219386400_0_0_0,
	&GenInst_Type_t_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_Type_t_0_0_0,
	&GenInst_Type_t_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_XmlObjectSerializer_t3967301761_0_0_0,
	&GenInst_SupportingTokenInfo_t3011094535_0_0_0,
	&GenInst_String_t_0_0_0_SupportingTokenParameters_t907883796_0_0_0,
	&GenInst_SupportingTokenParameters_t907883796_0_0_0,
	&GenInst_SecurityTokenParameters_t2868958784_0_0_0,
	&GenInst_IOutputChannel_t519638262_0_0_0,
	&GenInst_IContextChannel_t910994827_0_0_0,
	&GenInst_ICustomPeerResolverClient_t543357795_0_0_0,
	&GenInst_IEndpointBehavior_t1578562619_0_0_0,
	&GenInst_Type_t_0_0_0_IEndpointBehavior_t1578562619_0_0_0,
	&GenInst_Type_t_0_0_0_IEndpointBehavior_t1578562619_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2125614554_0_0_0,
	&GenInst_Type_t_0_0_0_IEndpointBehavior_t1578562619_0_0_0_KeyValuePair_2_t2125614554_0_0_0,
	&GenInst_Type_t_0_0_0_IEndpointBehavior_t1578562619_0_0_0_Type_t_0_0_0,
	&GenInst_Type_t_0_0_0_IEndpointBehavior_t1578562619_0_0_0_IEndpointBehavior_t1578562619_0_0_0,
	&GenInst_PeerNodeAddress_t2098027372_0_0_0,
	&GenInst_IDuplexSessionChannel_t292986819_0_0_0,
	&GenInst_RemotePeerConnection_t1863582616_0_0_0,
	&GenInst_Message_t869514973_0_0_0,
	&GenInst_UnknownMessageReceivedEventArgs_t1694867822_0_0_0,
	&GenInst_IPeerConnectorClient_t888178396_0_0_0,
	&GenInst_WelcomeInfo_t1049730577_0_0_0,
	&GenInst_IServiceBehavior_t43716000_0_0_0,
	&GenInst_Type_t_0_0_0_IServiceBehavior_t43716000_0_0_0,
	&GenInst_Type_t_0_0_0_IServiceBehavior_t43716000_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t590767935_0_0_0,
	&GenInst_Type_t_0_0_0_IServiceBehavior_t43716000_0_0_0_KeyValuePair_2_t590767935_0_0_0,
	&GenInst_Type_t_0_0_0_IServiceBehavior_t43716000_0_0_0_Type_t_0_0_0,
	&GenInst_Type_t_0_0_0_IServiceBehavior_t43716000_0_0_0_IServiceBehavior_t43716000_0_0_0,
	&GenInst_RemotePeerConnection_t1863582616_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_IDuplexChannel_t3233783714_0_0_0,
	&GenInst_IInputChannel_t3471598504_0_0_0,
	&GenInst_TimeSpan_t881159249_0_0_0_IInputChannel_t3471598504_0_0_0,
	&GenInst_TimeSpan_t881159249_0_0_0_IDuplexChannel_t3233783714_0_0_0,
	&GenInst_SecurityTokenResolver_t3905425829_0_0_0,
	&GenInst_SupportingTokenSpecification_t3961793461_0_0_0,
	&GenInst_SecurityTokenSpecification_t3911394864_0_0_0,
	&GenInst_WSSignedXml_t2681103009_0_0_0,
	&GenInst_SignedXml_t2168796367_0_0_0,
	&GenInst_DerivedKeySecurityToken_t1977550489_0_0_0,
	&GenInst_Wss11SignatureConfirmation_t1374635364_0_0_0,
	&GenInst_String_t_0_0_0_SupportingTokenParameters_t907883796_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3090812262_0_0_0,
	&GenInst_String_t_0_0_0_SupportingTokenParameters_t907883796_0_0_0_KeyValuePair_2_t3090812262_0_0_0,
	&GenInst_String_t_0_0_0_SupportingTokenParameters_t907883796_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_SupportingTokenParameters_t907883796_0_0_0_SupportingTokenParameters_t907883796_0_0_0,
	&GenInst_ManualResetEvent_t451242010_0_0_0,
	&GenInst_EventWaitHandle_t777845177_0_0_0,
	&GenInst_IChannelListener_t114797722_0_0_0_WcfListenerInfo_t1525779230_0_0_0,
	&GenInst_WcfListenerInfo_t1525779230_0_0_0,
	&GenInst_IChannelListener_t114797722_0_0_0_WcfListenerInfo_t1525779230_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1288040851_0_0_0,
	&GenInst_IChannelListener_t114797722_0_0_0_WcfListenerInfo_t1525779230_0_0_0_KeyValuePair_2_t1288040851_0_0_0,
	&GenInst_IChannelListener_t114797722_0_0_0_WcfListenerInfo_t1525779230_0_0_0_IChannelListener_t114797722_0_0_0,
	&GenInst_IChannelListener_t114797722_0_0_0_WcfListenerInfo_t1525779230_0_0_0_WcfListenerInfo_t1525779230_0_0_0,
	&GenInst_HttpContext_t1969259010_0_0_0_AutoResetEvent_t1333520283_0_0_0,
	&GenInst_AutoResetEvent_t1333520283_0_0_0,
	&GenInst_HttpContext_t1969259010_0_0_0_AutoResetEvent_t1333520283_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t788180488_0_0_0,
	&GenInst_HttpContext_t1969259010_0_0_0_AutoResetEvent_t1333520283_0_0_0_KeyValuePair_2_t788180488_0_0_0,
	&GenInst_HttpContext_t1969259010_0_0_0_AutoResetEvent_t1333520283_0_0_0_HttpContext_t1969259010_0_0_0,
	&GenInst_HttpContext_t1969259010_0_0_0_AutoResetEvent_t1333520283_0_0_0_AutoResetEvent_t1333520283_0_0_0,
	&GenInst_IExtension_1_t2909257122_0_0_0,
	&GenInst_IExtension_1_t3571061493_0_0_0,
	&GenInst_String_t_0_0_0_SvcHttpHandler_t1718683511_0_0_0,
	&GenInst_SvcHttpHandler_t1718683511_0_0_0,
	&GenInst_String_t_0_0_0_SvcHttpHandler_t1718683511_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3901611977_0_0_0,
	&GenInst_String_t_0_0_0_SvcHttpHandler_t1718683511_0_0_0_KeyValuePair_2_t3901611977_0_0_0,
	&GenInst_String_t_0_0_0_SvcHttpHandler_t1718683511_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_SvcHttpHandler_t1718683511_0_0_0_SvcHttpHandler_t1718683511_0_0_0,
	&GenInst_SvcHttpHandler_t1718683511_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_ChannelDispatcherBase_t2436929862_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_ChannelDispatcherBase_t2436929862_0_0_0,
	&GenInst_TcpDuplexSessionChannel_t645469680_0_0_0,
	&GenInst_ISessionChannel_1_t2045330825_0_0_0,
	&GenInst_DuplexChannelBase_t2906515918_0_0_0,
	&GenInst_ChannelBase_t1586116283_0_0_0,
	&GenInst_IDefaultCommunicationTimeouts_t2848643016_0_0_0,
	&GenInst_CommunicationObject_t518829156_0_0_0,
	&GenInst_String_t_0_0_0_EncryptedData_t3129875747_0_0_0,
	&GenInst_EncryptedData_t3129875747_0_0_0,
	&GenInst_EncryptedType_t2124600183_0_0_0,
	&GenInst_String_t_0_0_0_EncryptedData_t3129875747_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1017836917_0_0_0,
	&GenInst_String_t_0_0_0_EncryptedData_t3129875747_0_0_0_KeyValuePair_2_t1017836917_0_0_0,
	&GenInst_String_t_0_0_0_EncryptedData_t3129875747_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_EncryptedData_t3129875747_0_0_0_EncryptedData_t3129875747_0_0_0,
	&GenInst_ReferenceList_t2222396100_0_0_0,
	&GenInst_WrappedKeySecurityToken_t738218815_0_0_0,
	&GenInst_BaseAddressElement_t4110471995_0_0_0,
	&GenInst_BasicHttpBinding_t4145755420_0_0_0_BasicHttpBindingElement_t4053390136_0_0_0,
	&GenInst_IBindingConfigurationElement_t3662973274_0_0_0,
	&GenInst_BasicHttpBindingElement_t4053390136_0_0_0,
	&GenInst_ExtensionElement_t875191958_0_0_0,
	&GenInst_ChannelEndpointElement_t3229391106_0_0_0,
	&GenInst_ClaimTypeElement_t2235917530_0_0_0,
	&GenInst_PolicyImporterElement_t611423477_0_0_0,
	&GenInst_WsdlImporterElement_t929425501_0_0_0,
	&GenInst_CustomBindingElement_t3873939656_0_0_0,
	&GenInst_BindingElementExtensionElement_t2478436489_0_0_0,
	&GenInst_Type_t_0_0_0_BindingElementExtensionElement_t2478436489_0_0_0,
	&GenInst_ServiceModelExtensionElement_t2931985411_0_0_0,
	&GenInst_ConfigurationElement_t3318566633_0_0_0,
	&GenInst_Type_t_0_0_0_BindingElementExtensionElement_t2478436489_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3025488424_0_0_0,
	&GenInst_Type_t_0_0_0_BindingElementExtensionElement_t2478436489_0_0_0_KeyValuePair_2_t3025488424_0_0_0,
	&GenInst_Type_t_0_0_0_BindingElementExtensionElement_t2478436489_0_0_0_Type_t_0_0_0,
	&GenInst_Type_t_0_0_0_BindingElementExtensionElement_t2478436489_0_0_0_BindingElementExtensionElement_t2478436489_0_0_0,
	&GenInst_BehaviorExtensionElement_t4188976535_0_0_0,
	&GenInst_Type_t_0_0_0_BehaviorExtensionElement_t4188976535_0_0_0,
	&GenInst_Type_t_0_0_0_BehaviorExtensionElement_t4188976535_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t441061174_0_0_0,
	&GenInst_Type_t_0_0_0_BehaviorExtensionElement_t4188976535_0_0_0_KeyValuePair_2_t441061174_0_0_0,
	&GenInst_Type_t_0_0_0_BehaviorExtensionElement_t4188976535_0_0_0_Type_t_0_0_0,
	&GenInst_Type_t_0_0_0_BehaviorExtensionElement_t4188976535_0_0_0_BehaviorExtensionElement_t4188976535_0_0_0,
	&GenInst_EndpointBehaviorElement_t2191068479_0_0_0,
	&GenInst_IssuedTokenClientBehaviorsElement_t1890443082_0_0_0,
	&GenInst_String_t_0_0_0_MessageSecurityVersion_t2079539966_0_0_0,
	&GenInst_MessageSecurityVersion_t2079539966_0_0_0,
	&GenInst_String_t_0_0_0_MessageSecurityVersion_t2079539966_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t4262468432_0_0_0,
	&GenInst_String_t_0_0_0_MessageSecurityVersion_t2079539966_0_0_0_KeyValuePair_2_t4262468432_0_0_0,
	&GenInst_String_t_0_0_0_MessageSecurityVersion_t2079539966_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_MessageSecurityVersion_t2079539966_0_0_0_MessageSecurityVersion_t2079539966_0_0_0,
	&GenInst_WSHttpBinding_t3299795862_0_0_0_MexHttpBindingElement_t3331489804_0_0_0,
	&GenInst_MexHttpBindingElement_t3331489804_0_0_0,
	&GenInst_WSHttpBinding_t3299795862_0_0_0,
	&GenInst_WSHttpBinding_t3299795862_0_0_0_MexHttpsBindingElement_t1326184832_0_0_0,
	&GenInst_MexHttpsBindingElement_t1326184832_0_0_0,
	&GenInst_CustomBinding_t4187704225_0_0_0_MexNamedPipeBindingElement_t936983379_0_0_0,
	&GenInst_MexNamedPipeBindingElement_t936983379_0_0_0,
	&GenInst_CustomBinding_t4187704225_0_0_0,
	&GenInst_CustomBinding_t4187704225_0_0_0_MexTcpBindingElement_t1365091002_0_0_0,
	&GenInst_MexTcpBindingElement_t1365091002_0_0_0,
	&GenInst_MsmqIntegrationBinding_t1515740572_0_0_0_MsmqIntegrationBindingElement_t4143405573_0_0_0,
	&GenInst_MsmqIntegrationBindingElement_t4143405573_0_0_0,
	&GenInst_NetMsmqBinding_t3496970231_0_0_0_NetMsmqBindingElement_t4085042952_0_0_0,
	&GenInst_NetMsmqBindingElement_t4085042952_0_0_0,
	&GenInst_NetNamedPipeBinding_t983330350_0_0_0_NetNamedPipeBindingElement_t1517060569_0_0_0,
	&GenInst_NetNamedPipeBindingElement_t1517060569_0_0_0,
	&GenInst_NetPeerTcpBinding_t3507636973_0_0_0_NetPeerTcpBindingElement_t764197943_0_0_0,
	&GenInst_NetPeerTcpBindingElement_t764197943_0_0_0,
	&GenInst_IBindingConfigurationElement_t3662973274_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_NetTcpBinding_t2266210195_0_0_0_NetTcpBindingElement_t1442378023_0_0_0,
	&GenInst_NetTcpBindingElement_t1442378023_0_0_0,
	&GenInst_ServiceBehaviorElement_t2597675026_0_0_0,
	&GenInst_ServiceElement_t3010554678_0_0_0,
	&GenInst_ServiceEndpointElement_t1873720233_0_0_0,
	&GenInst_WS2007FederationHttpBinding_t502258771_0_0_0_WS2007FederationHttpBindingElement_t3751332056_0_0_0,
	&GenInst_WS2007FederationHttpBindingElement_t3751332056_0_0_0,
	&GenInst_WS2007HttpBinding_t3179680017_0_0_0_WS2007HttpBindingElement_t1255517706_0_0_0,
	&GenInst_WS2007HttpBindingElement_t1255517706_0_0_0,
	&GenInst_WSDualHttpBinding_t3779174110_0_0_0_WSDualHttpBindingElement_t2001662549_0_0_0,
	&GenInst_WSDualHttpBindingElement_t2001662549_0_0_0,
	&GenInst_WSFederationHttpBinding_t1523044440_0_0_0_WSFederationHttpBindingElement_t154325968_0_0_0,
	&GenInst_WSFederationHttpBindingElement_t154325968_0_0_0,
	&GenInst_WSHttpBinding_t3299795862_0_0_0_WSHttpBindingElement_t1467430044_0_0_0,
	&GenInst_WSHttpBindingElement_t1467430044_0_0_0,
	&GenInst_X509ScopedServiceCertificateElement_t614401471_0_0_0,
	&GenInst_XmlElementElement_t4113496974_0_0_0,
	&GenInst_IContractBehavior_t389363567_0_0_0,
	&GenInst_Type_t_0_0_0_IContractBehavior_t389363567_0_0_0,
	&GenInst_Type_t_0_0_0_IContractBehavior_t389363567_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t936415502_0_0_0,
	&GenInst_Type_t_0_0_0_IContractBehavior_t389363567_0_0_0_KeyValuePair_2_t936415502_0_0_0,
	&GenInst_Type_t_0_0_0_IContractBehavior_t389363567_0_0_0_Type_t_0_0_0,
	&GenInst_Type_t_0_0_0_IContractBehavior_t389363567_0_0_0_IContractBehavior_t389363567_0_0_0,
	&GenInst_OperationDescription_t1389309725_0_0_0,
	&GenInst_IOperationBehavior_t2503870007_0_0_0,
	&GenInst_String_t_0_0_0_ClientOperation_t2430985984_0_0_0,
	&GenInst_ClientOperation_t2430985984_0_0_0,
	&GenInst_String_t_0_0_0_ClientOperation_t2430985984_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t318947154_0_0_0,
	&GenInst_String_t_0_0_0_ClientOperation_t2430985984_0_0_0_KeyValuePair_2_t318947154_0_0_0,
	&GenInst_String_t_0_0_0_ClientOperation_t2430985984_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_ClientOperation_t2430985984_0_0_0_ClientOperation_t2430985984_0_0_0,
	&GenInst_Type_t_0_0_0_IOperationBehavior_t2503870007_0_0_0,
	&GenInst_Type_t_0_0_0_IOperationBehavior_t2503870007_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3050921942_0_0_0,
	&GenInst_Type_t_0_0_0_IOperationBehavior_t2503870007_0_0_0_KeyValuePair_2_t3050921942_0_0_0,
	&GenInst_Type_t_0_0_0_IOperationBehavior_t2503870007_0_0_0_Type_t_0_0_0,
	&GenInst_Type_t_0_0_0_IOperationBehavior_t2503870007_0_0_0_IOperationBehavior_t2503870007_0_0_0,
	&GenInst_MessageDescription_t512795677_0_0_0,
	&GenInst_MessagePartDescription_t2026856693_0_0_0,
	&GenInst_Type_t_0_0_0_ServiceContractAttribute_t4131169907_0_0_0,
	&GenInst_ServiceContractAttribute_t4131169907_0_0_0,
	&GenInst_Type_t_0_0_0_ServiceContractAttribute_t4131169907_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t383254546_0_0_0,
	&GenInst_Type_t_0_0_0_ServiceContractAttribute_t4131169907_0_0_0_KeyValuePair_2_t383254546_0_0_0,
	&GenInst_Type_t_0_0_0_ServiceContractAttribute_t4131169907_0_0_0_Type_t_0_0_0,
	&GenInst_Type_t_0_0_0_ServiceContractAttribute_t4131169907_0_0_0_ServiceContractAttribute_t4131169907_0_0_0,
	&GenInst_FaultDescription_t2068025689_0_0_0,
	&GenInst_ServiceEndpoint_t4038493094_0_0_0,
	&GenInst_XmlQualifiedName_t2760654312_0_0_0_MessageHeaderDescription_t75735776_0_0_0,
	&GenInst_MessageHeaderDescription_t75735776_0_0_0,
	&GenInst_XmlQualifiedName_t2760654312_0_0_0_MessageHeaderDescription_t75735776_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2448405263_0_0_0,
	&GenInst_XmlQualifiedName_t2760654312_0_0_0_MessageHeaderDescription_t75735776_0_0_0_KeyValuePair_2_t2448405263_0_0_0,
	&GenInst_XmlQualifiedName_t2760654312_0_0_0_MessageHeaderDescription_t75735776_0_0_0_XmlQualifiedName_t2760654312_0_0_0,
	&GenInst_XmlQualifiedName_t2760654312_0_0_0_MessageHeaderDescription_t75735776_0_0_0_MessageHeaderDescription_t75735776_0_0_0,
	&GenInst_XmlQualifiedName_t2760654312_0_0_0_MessagePartDescription_t2026856693_0_0_0,
	&GenInst_XmlQualifiedName_t2760654312_0_0_0_MessagePartDescription_t2026856693_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t104558884_0_0_0,
	&GenInst_XmlQualifiedName_t2760654312_0_0_0_MessagePartDescription_t2026856693_0_0_0_KeyValuePair_2_t104558884_0_0_0,
	&GenInst_XmlQualifiedName_t2760654312_0_0_0_MessagePartDescription_t2026856693_0_0_0_XmlQualifiedName_t2760654312_0_0_0,
	&GenInst_XmlQualifiedName_t2760654312_0_0_0_MessagePartDescription_t2026856693_0_0_0_MessagePartDescription_t2026856693_0_0_0,
	&GenInst_String_t_0_0_0_MessagePropertyDescription_t3921441415_0_0_0,
	&GenInst_MessagePropertyDescription_t3921441415_0_0_0,
	&GenInst_String_t_0_0_0_MessagePropertyDescription_t3921441415_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1809402585_0_0_0,
	&GenInst_String_t_0_0_0_MessagePropertyDescription_t3921441415_0_0_0_KeyValuePair_2_t1809402585_0_0_0,
	&GenInst_String_t_0_0_0_MessagePropertyDescription_t3921441415_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_MessagePropertyDescription_t3921441415_0_0_0_MessagePropertyDescription_t3921441415_0_0_0,
	&GenInst_MetadataSection_t4067397855_0_0_0,
	&GenInst_EndpointDispatcher_t2708702820_0_0_0,
	&GenInst_Uri_t100236324_0_0_0_ChannelDispatcher_t2781106991_0_0_0,
	&GenInst_ChannelDispatcher_t2781106991_0_0_0,
	&GenInst_Uri_t100236324_0_0_0_ChannelDispatcher_t2781106991_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t60725426_0_0_0,
	&GenInst_Uri_t100236324_0_0_0_ChannelDispatcher_t2781106991_0_0_0_KeyValuePair_2_t60725426_0_0_0,
	&GenInst_Uri_t100236324_0_0_0_ChannelDispatcher_t2781106991_0_0_0_Uri_t100236324_0_0_0,
	&GenInst_Uri_t100236324_0_0_0_ChannelDispatcher_t2781106991_0_0_0_ChannelDispatcher_t2781106991_0_0_0,
	&GenInst_String_t_0_0_0_ServiceDescription_t3693704363_0_0_0,
	&GenInst_ServiceDescription_t3693704363_0_0_0,
	&GenInst_NamedItem_t2466402210_0_0_0,
	&GenInst_DocumentableItem_t546804031_0_0_0,
	&GenInst_String_t_0_0_0_ServiceDescription_t3693704363_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1581665533_0_0_0,
	&GenInst_String_t_0_0_0_ServiceDescription_t3693704363_0_0_0_KeyValuePair_2_t1581665533_0_0_0,
	&GenInst_String_t_0_0_0_ServiceDescription_t3693704363_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_ServiceDescription_t3693704363_0_0_0_ServiceDescription_t3693704363_0_0_0,
	&GenInst_String_t_0_0_0_XmlSchema_t3742557897_0_0_0,
	&GenInst_String_t_0_0_0_XmlSchema_t3742557897_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1630519067_0_0_0,
	&GenInst_String_t_0_0_0_XmlSchema_t3742557897_0_0_0_KeyValuePair_2_t1630519067_0_0_0,
	&GenInst_String_t_0_0_0_XmlSchema_t3742557897_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_XmlSchema_t3742557897_0_0_0_XmlSchema_t3742557897_0_0_0,
	&GenInst_IWSTrustSecurityTokenService_t90517692_0_0_0,
	&GenInst_WstRequestSecurityTokenResponse_t889264267_0_0_0,
	&GenInst_WstRequestSecurityTokenBase_t1978373106_0_0_0,
	&GenInst_BodyWriter_t3441673553_0_0_0,
	&GenInst_MessageBodyDescription_t2541387169_0_0_0_XmlSerializer_t1117804635_0_0_0,
	&GenInst_MessageBodyDescription_t2541387169_0_0_0,
	&GenInst_MessageBodyDescription_t2541387169_0_0_0_XmlSerializer_t1117804635_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3705273805_0_0_0,
	&GenInst_MessageBodyDescription_t2541387169_0_0_0_XmlSerializer_t1117804635_0_0_0_KeyValuePair_2_t3705273805_0_0_0,
	&GenInst_MessageBodyDescription_t2541387169_0_0_0_XmlSerializer_t1117804635_0_0_0_MessageBodyDescription_t2541387169_0_0_0,
	&GenInst_MessageBodyDescription_t2541387169_0_0_0_XmlSerializer_t1117804635_0_0_0_XmlSerializer_t1117804635_0_0_0,
	&GenInst_XmlMembersMapping_t4154751912_0_0_0,
	&GenInst_MessagePartDescription_t2026856693_0_0_0_XmlObjectSerializer_t3967301761_0_0_0,
	&GenInst_MessagePartDescription_t2026856693_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t842235599_0_0_0,
	&GenInst_MessagePartDescription_t2026856693_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_KeyValuePair_2_t842235599_0_0_0,
	&GenInst_MessagePartDescription_t2026856693_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_MessagePartDescription_t2026856693_0_0_0,
	&GenInst_MessagePartDescription_t2026856693_0_0_0_XmlObjectSerializer_t3967301761_0_0_0_XmlObjectSerializer_t3967301761_0_0_0,
	&GenInst_IErrorHandler_t3951425786_0_0_0,
	&GenInst_IChannelInitializer_t2714610642_0_0_0,
	&GenInst_IReplySessionChannel_t1412553907_0_0_0,
	&GenInst_IInputSessionChannel_t1608238312_0_0_0,
	&GenInst_IParameterInspector_t717813771_0_0_0,
	&GenInst_IInteractiveChannelInitializer_t2558021170_0_0_0,
	&GenInst_IClientMessageInspector_t2654722838_0_0_0,
	&GenInst_ICallContextInitializer_t2439543260_0_0_0,
	&GenInst_String_t_0_0_0_DispatchOperation_t1810306617_0_0_0,
	&GenInst_DispatchOperation_t1810306617_0_0_0,
	&GenInst_String_t_0_0_0_DispatchOperation_t1810306617_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3993235083_0_0_0,
	&GenInst_String_t_0_0_0_DispatchOperation_t1810306617_0_0_0_KeyValuePair_2_t3993235083_0_0_0,
	&GenInst_String_t_0_0_0_DispatchOperation_t1810306617_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_DispatchOperation_t1810306617_0_0_0_DispatchOperation_t1810306617_0_0_0,
	&GenInst_IInputSessionShutdown_t2537225736_0_0_0,
	&GenInst_IInstanceContextInitializer_t1888350186_0_0_0,
	&GenInst_IDispatchMessageInspector_t255215605_0_0_0,
	&GenInst_UniqueId_t1383576913_0_0_0_InstanceContext_t3593205954_0_0_0,
	&GenInst_UniqueId_t1383576913_0_0_0,
	&GenInst_InstanceContext_t3593205954_0_0_0,
	&GenInst_IExtensibleObject_1_t646653426_0_0_0,
	&GenInst_UniqueId_t1383576913_0_0_0_InstanceContext_t3593205954_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1176609220_0_0_0,
	&GenInst_UniqueId_t1383576913_0_0_0_InstanceContext_t3593205954_0_0_0_KeyValuePair_2_t1176609220_0_0_0,
	&GenInst_UniqueId_t1383576913_0_0_0_InstanceContext_t3593205954_0_0_0_UniqueId_t1383576913_0_0_0,
	&GenInst_UniqueId_t1383576913_0_0_0_InstanceContext_t3593205954_0_0_0_InstanceContext_t3593205954_0_0_0,
	&GenInst_ClaimTypeRequirement_t1730123968_0_0_0,
	&GenInst_UniqueId_t1383576913_0_0_0_Dictionary_2_t3105478127_0_0_0,
	&GenInst_UniqueId_t1383576913_0_0_0_SecurityContextSecurityToken_t3624779732_0_0_0,
	&GenInst_SecurityContextSecurityToken_t3624779732_0_0_0,
	&GenInst_UniqueId_t1383576913_0_0_0_SecurityContextSecurityToken_t3624779732_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1208182998_0_0_0,
	&GenInst_UniqueId_t1383576913_0_0_0_SecurityContextSecurityToken_t3624779732_0_0_0_KeyValuePair_2_t1208182998_0_0_0,
	&GenInst_UniqueId_t1383576913_0_0_0_SecurityContextSecurityToken_t3624779732_0_0_0_UniqueId_t1383576913_0_0_0,
	&GenInst_UniqueId_t1383576913_0_0_0_SecurityContextSecurityToken_t3624779732_0_0_0_SecurityContextSecurityToken_t3624779732_0_0_0,
	&GenInst_Dictionary_2_t3105478127_0_0_0,
	&GenInst_IDictionary_t1363984059_0_0_0,
	&GenInst_UniqueId_t1383576913_0_0_0_Dictionary_2_t3105478127_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t688881393_0_0_0,
	&GenInst_UniqueId_t1383576913_0_0_0_Dictionary_2_t3105478127_0_0_0_KeyValuePair_2_t688881393_0_0_0,
	&GenInst_UniqueId_t1383576913_0_0_0_Dictionary_2_t3105478127_0_0_0_UniqueId_t1383576913_0_0_0,
	&GenInst_UniqueId_t1383576913_0_0_0_Dictionary_2_t3105478127_0_0_0_Dictionary_2_t3105478127_0_0_0,
	&GenInst_String_t_0_0_0_SspiServerSession_t2076952883_0_0_0,
	&GenInst_SspiServerSession_t2076952883_0_0_0,
	&GenInst_SspiSession_t1506236276_0_0_0,
	&GenInst_String_t_0_0_0_SspiServerSession_t2076952883_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t4259881349_0_0_0,
	&GenInst_String_t_0_0_0_SspiServerSession_t2076952883_0_0_0_KeyValuePair_2_t4259881349_0_0_0,
	&GenInst_String_t_0_0_0_SspiServerSession_t2076952883_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_SspiServerSession_t2076952883_0_0_0_SspiServerSession_t2076952883_0_0_0,
	&GenInst_String_t_0_0_0_TlsServerSessionInfo_t613821154_0_0_0,
	&GenInst_TlsServerSessionInfo_t613821154_0_0_0,
	&GenInst_String_t_0_0_0_TlsServerSessionInfo_t613821154_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2796749620_0_0_0,
	&GenInst_String_t_0_0_0_TlsServerSessionInfo_t613821154_0_0_0_KeyValuePair_2_t2796749620_0_0_0,
	&GenInst_String_t_0_0_0_TlsServerSessionInfo_t613821154_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_TlsServerSessionInfo_t613821154_0_0_0_TlsServerSessionInfo_t613821154_0_0_0,
	&GenInst_HandshakeMessage_t3696583169_0_0_0,
	&GenInst_TlsStream_t2365453966_0_0_0,
	&GenInst_Stream_t1273022909_0_0_0,
	&GenInst_Uri_t100236324_0_0_0_KeyedByTypeCollection_1_t4222441378_0_0_0,
	&GenInst_KeyedByTypeCollection_1_t4222441378_0_0_0,
	&GenInst_KeyedCollection_2_t3162336987_0_0_0,
	&GenInst_Collection_1_t522918537_0_0_0,
	&GenInst_Uri_t100236324_0_0_0_KeyedByTypeCollection_1_t4222441378_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1502059813_0_0_0,
	&GenInst_Uri_t100236324_0_0_0_KeyedByTypeCollection_1_t4222441378_0_0_0_KeyValuePair_2_t1502059813_0_0_0,
	&GenInst_Uri_t100236324_0_0_0_KeyedByTypeCollection_1_t4222441378_0_0_0_Uri_t100236324_0_0_0,
	&GenInst_Uri_t100236324_0_0_0_KeyedByTypeCollection_1_t4222441378_0_0_0_KeyedByTypeCollection_1_t4222441378_0_0_0,
	&GenInst_X509Certificate2_t714049126_0_0_0,
	&GenInst_String_t_0_0_0_MessagePartSpecification_t2368875147_0_0_0,
	&GenInst_MessagePartSpecification_t2368875147_0_0_0,
	&GenInst_String_t_0_0_0_MessagePartSpecification_t2368875147_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t256836317_0_0_0,
	&GenInst_String_t_0_0_0_MessagePartSpecification_t2368875147_0_0_0_KeyValuePair_2_t256836317_0_0_0,
	&GenInst_String_t_0_0_0_MessagePartSpecification_t2368875147_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_MessagePartSpecification_t2368875147_0_0_0_MessagePartSpecification_t2368875147_0_0_0,
	&GenInst_Uri_t100236324_0_0_0_X509Certificate2_t714049126_0_0_0,
	&GenInst_Uri_t100236324_0_0_0_X509Certificate2_t714049126_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2288634857_0_0_0,
	&GenInst_Uri_t100236324_0_0_0_X509Certificate2_t714049126_0_0_0_KeyValuePair_2_t2288634857_0_0_0,
	&GenInst_Uri_t100236324_0_0_0_X509Certificate2_t714049126_0_0_0_Uri_t100236324_0_0_0,
	&GenInst_Uri_t100236324_0_0_0_X509Certificate2_t714049126_0_0_0_X509Certificate2_t714049126_0_0_0,
	&GenInst_IOutputSessionChannel_t2309827865_0_0_0,
	&GenInst_IRequestSessionChannel_t3405854163_0_0_0,
	&GenInst_IRequestChannel_t3653845626_0_0_0,
	&GenInst_OperationContext_t2829644788_0_0_0,
	&GenInst_String_t_0_0_0_ContractDescription_t1411178514_0_0_0,
	&GenInst_ContractDescription_t1411178514_0_0_0,
	&GenInst_String_t_0_0_0_ContractDescription_t1411178514_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3594106980_0_0_0,
	&GenInst_String_t_0_0_0_ContractDescription_t1411178514_0_0_0_KeyValuePair_2_t3594106980_0_0_0,
	&GenInst_String_t_0_0_0_ContractDescription_t1411178514_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_ContractDescription_t1411178514_0_0_0_ContractDescription_t1411178514_0_0_0,
	&GenInst_String_t_0_0_0_Uri_t100236324_0_0_0,
	&GenInst_String_t_0_0_0_Uri_t100236324_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2283164790_0_0_0,
	&GenInst_String_t_0_0_0_Uri_t100236324_0_0_0_KeyValuePair_2_t2283164790_0_0_0,
	&GenInst_String_t_0_0_0_Uri_t100236324_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_Uri_t100236324_0_0_0_Uri_t100236324_0_0_0,
	&GenInst_ServiceEndpoint_t4038493094_0_0_0_ChannelDispatcher_t2781106991_0_0_0,
	&GenInst_ServiceEndpoint_t4038493094_0_0_0_ChannelDispatcher_t2781106991_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t4278733032_0_0_0,
	&GenInst_ServiceEndpoint_t4038493094_0_0_0_ChannelDispatcher_t2781106991_0_0_0_KeyValuePair_2_t4278733032_0_0_0,
	&GenInst_ServiceEndpoint_t4038493094_0_0_0_ChannelDispatcher_t2781106991_0_0_0_ServiceEndpoint_t4038493094_0_0_0,
	&GenInst_ServiceEndpoint_t4038493094_0_0_0_ChannelDispatcher_t2781106991_0_0_0_ChannelDispatcher_t2781106991_0_0_0,
	&GenInst_ServiceEndpoint_t4038493094_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_SecurityBindingElement_t1737011943_0_0_0,
	&GenInst_BaseInputModule_t2019268878_0_0_0,
	&GenInst_UIBehaviour_t3495933518_0_0_0,
	&GenInst_RaycastResult_t3360306849_0_0_0,
	&GenInst_IDeselectHandler_t393712923_0_0_0,
	&GenInst_IEventSystemHandler_t3354683850_0_0_0,
	&GenInst_List_1_t531791296_0_0_0,
	&GenInst_List_1_t257213610_0_0_0,
	&GenInst_List_1_t3395709193_0_0_0,
	&GenInst_ISelectHandler_t2271418839_0_0_0,
	&GenInst_BaseRaycaster_t4150874583_0_0_0,
	&GenInst_Entry_t3344766165_0_0_0,
	&GenInst_BaseEventData_t3903027533_0_0_0,
	&GenInst_IPointerEnterHandler_t1016128679_0_0_0,
	&GenInst_IPointerExitHandler_t4182793654_0_0_0,
	&GenInst_IPointerDownHandler_t1380080529_0_0_0,
	&GenInst_IPointerUpHandler_t277099170_0_0_0,
	&GenInst_IPointerClickHandler_t132471142_0_0_0,
	&GenInst_IInitializePotentialDragHandler_t608041180_0_0_0,
	&GenInst_IBeginDragHandler_t3293314358_0_0_0,
	&GenInst_IDragHandler_t2288426503_0_0_0,
	&GenInst_IEndDragHandler_t297508562_0_0_0,
	&GenInst_IDropHandler_t3627139509_0_0_0,
	&GenInst_IScrollHandler_t4201797704_0_0_0,
	&GenInst_IUpdateSelectedHandler_t4266291469_0_0_0,
	&GenInst_IMoveHandler_t933334182_0_0_0,
	&GenInst_ISubmitHandler_t2790798304_0_0_0,
	&GenInst_ICancelHandler_t3974364820_0_0_0,
	&GenInst_Transform_t3600365921_0_0_0,
	&GenInst_BaseInput_t3630163547_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0,
	&GenInst_PointerEventData_t3807901092_0_0_0,
	&GenInst_AbstractEventData_t4171500731_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t799319294_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_KeyValuePair_2_t799319294_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_PointerEventData_t3807901092_0_0_0,
	&GenInst_ButtonState_t857139936_0_0_0,
	&GenInst_ICanvasElement_t2121898866_0_0_0,
	&GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1364477206_0_0_0,
	&GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t1364477206_0_0_0,
	&GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_ICanvasElement_t2121898866_0_0_0,
	&GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_ColorBlock_t2139031574_0_0_0,
	&GenInst_OptionData_t3270282352_0_0_0,
	&GenInst_DropdownItem_t1451952895_0_0_0,
	&GenInst_FloatTween_t1274330004_0_0_0,
	&GenInst_Sprite_t280657092_0_0_0,
	&GenInst_Canvas_t3310196443_0_0_0,
	&GenInst_List_1_t487303889_0_0_0,
	&GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0,
	&GenInst_Text_t1901882714_0_0_0,
	&GenInst_Link_t2031043523_0_0_0,
	&GenInst_ILayoutElement_t4082016710_0_0_0,
	&GenInst_MaskableGraphic_t3839221559_0_0_0,
	&GenInst_IClippable_t1239629351_0_0_0,
	&GenInst_IMaskable_t433386433_0_0_0,
	&GenInst_IMaterialModifier_t1975025690_0_0_0,
	&GenInst_Graphic_t1660335611_0_0_0,
	&GenInst_HashSet_1_t466832188_0_0_0,
	&GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2767015899_0_0_0,
	&GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_KeyValuePair_2_t2767015899_0_0_0,
	&GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_Font_t1956802104_0_0_0,
	&GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_HashSet_1_t466832188_0_0_0,
	&GenInst_ColorTween_t809614380_0_0_0,
	&GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0,
	&GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3639519817_0_0_0,
	&GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t3639519817_0_0_0,
	&GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_Graphic_t1660335611_0_0_0,
	&GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_IndexedSet_1_t3109723551_0_0_0,
	&GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t398822319_0_0_0,
	&GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_KeyValuePair_2_t398822319_0_0_0,
	&GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_Canvas_t3310196443_0_0_0,
	&GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_IndexedSet_1_t3109723551_0_0_0,
	&GenInst_Type_t1152881528_0_0_0,
	&GenInst_FillMethod_t1167457570_0_0_0,
	&GenInst_ContentType_t1787303396_0_0_0,
	&GenInst_LineType_t4214648469_0_0_0,
	&GenInst_InputType_t1770400679_0_0_0,
	&GenInst_TouchScreenKeyboardType_t1530597702_0_0_0,
	&GenInst_CharacterValidation_t4051914437_0_0_0,
	&GenInst_Mask_t1803652131_0_0_0,
	&GenInst_ICanvasRaycastFilter_t2454702837_0_0_0,
	&GenInst_List_1_t3275726873_0_0_0,
	&GenInst_RectMask2D_t3474889437_0_0_0,
	&GenInst_IClipper_t1224123152_0_0_0,
	&GenInst_List_1_t651996883_0_0_0,
	&GenInst_Navigation_t3049316579_0_0_0,
	&GenInst_Link_t1368790160_0_0_0,
	&GenInst_Direction_t3470714353_0_0_0,
	&GenInst_Selectable_t3250028441_0_0_0,
	&GenInst_Transition_t1769908631_0_0_0,
	&GenInst_SpriteState_t1362986479_0_0_0,
	&GenInst_CanvasGroup_t4083511760_0_0_0,
	&GenInst_Direction_t337909235_0_0_0,
	&GenInst_MatEntry_t2957107092_0_0_0,
	&GenInst_Toggle_t2735377061_0_0_0,
	&GenInst_Toggle_t2735377061_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1634190656_0_0_0,
	&GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t1634190656_0_0_0,
	&GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_IClipper_t1224123152_0_0_0,
	&GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_AspectMode_t3417192999_0_0_0,
	&GenInst_FitMode_t3267881214_0_0_0,
	&GenInst_RectTransform_t3704657025_0_0_0,
	&GenInst_LayoutRebuilder_t541313304_0_0_0,
	&GenInst_ILayoutElement_t4082016710_0_0_0_Single_t1397266774_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Single_t1397266774_0_0_0,
	&GenInst_List_1_t899420910_0_0_0,
	&GenInst_List_1_t4072576034_0_0_0,
	&GenInst_List_1_t3628304265_0_0_0,
	&GenInst_List_1_t496136383_0_0_0,
	&GenInst_List_1_t1234605051_0_0_0,
	&GenInst_RendererCache_t1904069300_0_0_0,
	&GenInst_Highlighter_t672210414_0_0_0,
	&GenInst_Data_t1588725102_0_0_0,
	&GenInst_Link_t801371223_0_0_0,
	&GenInst_Shader_t4151988712_0_0_0,
	&GenInst_HighlightingBase_t582374880_0_0_0,
	&GenInst_Hashtable_t1853889766_0_0_0,
	&GenInst_Rect_t2360479859_0_0_0,
	&GenInst_iTween_t770867771_0_0_0,
	&GenInst_Preset_t736195574_0_0_0,
	&GenInst_QualitySettings_t1869387270_0_0_0,
	&GenInst_QualitySettings_t1379802392_0_0_0,
	&GenInst_Mesh_t3648964284_0_0_0,
	&GenInst_Frame_t3511111948_0_0_0,
	&GenInst_RenderTextureFormat_t962350765_0_0_0,
	&GenInst_Preset_t2972107632_0_0_0,
	&GenInst_InvBaseItem_t2503063521_0_0_0,
	&GenInst_InvDatabase_t3341607346_0_0_0,
	&GenInst_InvStat_t493203737_0_0_0,
	&GenInst_InvGameItem_t1203288033_0_0_0,
	&GenInst_InvAttachmentPoint_t2909008346_0_0_0,
	&GenInst_AnimationClip_t2318505987_0_0_0,
	&GenInst_Motion_t1110556653_0_0_0,
	&GenInst_UITweener_t260334902_0_0_0,
	&GenInst_EventDelegate_t2738326060_0_0_0,
	&GenInst_FadeEntry_t639421133_0_0_0,
	&GenInst_UIButton_t1100396938_0_0_0,
	&GenInst_UIButtonColor_t2038074180_0_0_0,
	&GenInst_UIWidgetContainer_t30162560_0_0_0,
	&GenInst_UIDragDropItem_t3922849381_0_0_0,
	&GenInst_UIKeyBinding_t2698445843_0_0_0,
	&GenInst_UIKeyNavigation_t4244956403_0_0_0,
	&GenInst_UILabel_t3248798549_0_0_0,
	&GenInst_UIWidget_t3538521925_0_0_0,
	&GenInst_UIRect_t2875960382_0_0_0,
	&GenInst_UIPlaySound_t1382070071_0_0_0,
	&GenInst_UIToggle_t4192126258_0_0_0,
	&GenInst_UIScrollView_t1973404950_0_0_0,
	&GenInst_Bounds_t2266837910_0_0_0,
	&GenInst_UIPanel_t1716472341_0_0_0,
	&GenInst_BMGlyph_t3344884546_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_BMGlyph_t3344884546_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_BMGlyph_t3344884546_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t336302748_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_BMGlyph_t3344884546_0_0_0_KeyValuePair_2_t336302748_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_BMGlyph_t3344884546_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_BMGlyph_t3344884546_0_0_0_BMGlyph_t3344884546_0_0_0,
	&GenInst_Parameter_t2966927026_0_0_0,
	&GenInst_String_t_0_0_0_StringU5BU5D_t1281789340_0_0_0,
	&GenInst_String_t_0_0_0_StringU5BU5D_t1281789340_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3464717806_0_0_0,
	&GenInst_String_t_0_0_0_StringU5BU5D_t1281789340_0_0_0_KeyValuePair_2_t3464717806_0_0_0,
	&GenInst_String_t_0_0_0_StringU5BU5D_t1281789340_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_StringU5BU5D_t1281789340_0_0_0_StringU5BU5D_t1281789340_0_0_0,
	&GenInst_KeyCode_t2599294277_0_0_0,
	&GenInst_AudioListener_t2734094699_0_0_0,
	&GenInst_UICamera_t1356438871_0_0_0,
	&GenInst_UIRoot_t4022971450_0_0_0,
	&GenInst_UIDrawCall_t1293405319_0_0_0,
	&GenInst_Int32U5BU5D_t385246372_0_0_0,
	&GenInst_IList_1_t471298240_0_0_0,
	&GenInst_ICollection_1_t1484130691_0_0_0,
	&GenInst_IEnumerable_1_t1930798642_0_0_0,
	&GenInst_UISpriteData_t900308526_0_0_0,
	&GenInst_Sprite_t2895597119_0_0_0,
	&GenInst_UISprite_t194114938_0_0_0,
	&GenInst_UIBasicSprite_t1521297657_0_0_0,
	&GenInst_UIFont_t2766063701_0_0_0,
	&GenInst_MouseOrTouch_t3052596533_0_0_0,
	&GenInst_DepthEntry_t628749918_0_0_0,
	&GenInst_BMSymbol_t1586058841_0_0_0,
	&GenInst_Font_t1956802104_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Font_t1956802104_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t956162168_0_0_0,
	&GenInst_Font_t1956802104_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t956162168_0_0_0,
	&GenInst_Font_t1956802104_0_0_0_Int32_t2950945753_0_0_0_Font_t1956802104_0_0_0,
	&GenInst_Font_t1956802104_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Paragraph_t1111063719_0_0_0,
	&GenInst_String_t_0_0_0_BetterList_1_t266084037_0_0_0,
	&GenInst_BetterList_1_t266084037_0_0_0,
	&GenInst_String_t_0_0_0_BetterList_1_t266084037_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2449012503_0_0_0,
	&GenInst_String_t_0_0_0_BetterList_1_t266084037_0_0_0_KeyValuePair_2_t2449012503_0_0_0,
	&GenInst_String_t_0_0_0_BetterList_1_t266084037_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_BetterList_1_t266084037_0_0_0_BetterList_1_t266084037_0_0_0,
	&GenInst_Button_t4055032469_0_0_0,
	&GenInst_WireframeBehaviour_t1831066704_0_0_0,
	&GenInst_AxisTouchButton_t3522881333_0_0_0,
	&GenInst_String_t_0_0_0_VirtualAxis_t4087348596_0_0_0,
	&GenInst_VirtualAxis_t4087348596_0_0_0,
	&GenInst_String_t_0_0_0_VirtualAxis_t4087348596_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1975309766_0_0_0,
	&GenInst_String_t_0_0_0_VirtualAxis_t4087348596_0_0_0_KeyValuePair_2_t1975309766_0_0_0,
	&GenInst_String_t_0_0_0_VirtualAxis_t4087348596_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_VirtualAxis_t4087348596_0_0_0_VirtualAxis_t4087348596_0_0_0,
	&GenInst_String_t_0_0_0_VirtualButton_t2756566330_0_0_0,
	&GenInst_VirtualButton_t2756566330_0_0_0,
	&GenInst_String_t_0_0_0_VirtualButton_t2756566330_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t644527500_0_0_0,
	&GenInst_String_t_0_0_0_VirtualButton_t2756566330_0_0_0_KeyValuePair_2_t644527500_0_0_0,
	&GenInst_String_t_0_0_0_VirtualButton_t2756566330_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_VirtualButton_t2756566330_0_0_0_VirtualButton_t2756566330_0_0_0,
	&GenInst_SetAnimInfo_t2127528194_0_0_0,
	&GenInst_Animator_t434523843_0_0_0,
	&GenInst_Level_Controller_t1433348427_0_0_0,
	&GenInst_Image_t2670269651_0_0_0,
	&GenInst_TweenScale_t2539309033_0_0_0,
	&GenInst_ISerializationCallbackReceiver_t2363941153_0_0_0,
	&GenInst_ImageTargetBehaviour_t2200418350_0_0_0,
	&GenInst_ImageTargetAbstractBehaviour_t3577513769_0_0_0,
	&GenInst_ISAWCService_t99629249_0_0_0,
	&GenInst_BT_Mic_t2341817396_0_0_0,
	&GenInst_Drag_Controller_t1925480236_0_0_0,
	&GenInst_Point_Controller_t869836145_0_0_0,
	&GenInst_leve4To2D_t1062310264_0_0_0,
	&GenInst_SpriteRenderer_t3235626157_0_0_0,
	&GenInst_AudioClip_t3680889665_0_0_0,
	&GenInst_IEnumerable_1_t1615002100_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2877758246_gp_0_0_0_0,
	&GenInst_Array_Sort_m3544654705_gp_0_0_0_0_Array_Sort_m3544654705_gp_0_0_0_0,
	&GenInst_Array_Sort_m2813331573_gp_0_0_0_0_Array_Sort_m2813331573_gp_1_0_0_0,
	&GenInst_Array_Sort_m541372433_gp_0_0_0_0,
	&GenInst_Array_Sort_m541372433_gp_0_0_0_0_Array_Sort_m541372433_gp_0_0_0_0,
	&GenInst_Array_Sort_m3128382015_gp_0_0_0_0,
	&GenInst_Array_Sort_m3128382015_gp_0_0_0_0_Array_Sort_m3128382015_gp_1_0_0_0,
	&GenInst_Array_Sort_m2971965616_gp_0_0_0_0_Array_Sort_m2971965616_gp_0_0_0_0,
	&GenInst_Array_Sort_m2294228957_gp_0_0_0_0_Array_Sort_m2294228957_gp_1_0_0_0,
	&GenInst_Array_Sort_m3111895514_gp_0_0_0_0,
	&GenInst_Array_Sort_m3111895514_gp_0_0_0_0_Array_Sort_m3111895514_gp_0_0_0_0,
	&GenInst_Array_Sort_m4162554849_gp_0_0_0_0,
	&GenInst_Array_Sort_m4162554849_gp_1_0_0_0,
	&GenInst_Array_Sort_m4162554849_gp_0_0_0_0_Array_Sort_m4162554849_gp_1_0_0_0,
	&GenInst_Array_Sort_m2347552592_gp_0_0_0_0,
	&GenInst_Array_Sort_m1584977544_gp_0_0_0_0,
	&GenInst_Array_qsort_m2730016035_gp_0_0_0_0,
	&GenInst_Array_qsort_m2730016035_gp_0_0_0_0_Array_qsort_m2730016035_gp_1_0_0_0,
	&GenInst_Array_compare_m1571452883_gp_0_0_0_0,
	&GenInst_Array_qsort_m2056624725_gp_0_0_0_0,
	&GenInst_Array_Resize_m148240358_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m2898582490_gp_0_0_0_0,
	&GenInst_Array_ForEach_m1810936836_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m3156307665_gp_0_0_0_0_Array_ConvertAll_m3156307665_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m314143680_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m460178338_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m4020594960_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m2045802321_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m193711900_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m3007915586_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m588894935_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1820506567_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m818786257_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1137121163_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m3665873194_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m751114778_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m1878557700_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m1660745251_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m3843535007_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m3595565626_gp_0_0_0_0,
	&GenInst_Array_FindAll_m1182922648_gp_0_0_0_0,
	&GenInst_Array_Exists_m1131952887_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m1239764914_gp_0_0_0_0,
	&GenInst_Array_Find_m143990730_gp_0_0_0_0,
	&GenInst_Array_FindLast_m1741869030_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t2600413744_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t221793636_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1202911786_gp_0_0_0_0,
	&GenInst_IList_1_t523203890_gp_0_0_0_0,
	&GenInst_ICollection_1_t1449021101_gp_0_0_0_0,
	&GenInst_Nullable_1_t3772285925_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m1909347418_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m1909347418_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m1636979383_gp_0_0_0_0,
	&GenInst_Dictionary_2_t3621973219_gp_0_0_0_0,
	&GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0,
	&GenInst_Dictionary_2_t3621973219_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1772072192_0_0_0,
	&GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m966111031_gp_0_0_0_0,
	&GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m842166809_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m842166809_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_ShimEnumerator_t3154898978_gp_0_0_0_0_ShimEnumerator_t3154898978_gp_1_0_0_0,
	&GenInst_Enumerator_t135598976_gp_0_0_0_0_Enumerator_t135598976_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1920611820_0_0_0,
	&GenInst_KeyCollection_t4251528776_gp_0_0_0_0_KeyCollection_t4251528776_gp_1_0_0_0,
	&GenInst_KeyCollection_t4251528776_gp_0_0_0_0,
	&GenInst_Enumerator_t3443476011_gp_0_0_0_0_Enumerator_t3443476011_gp_1_0_0_0,
	&GenInst_Enumerator_t3443476011_gp_0_0_0_0,
	&GenInst_KeyCollection_t4251528776_gp_0_0_0_0_KeyCollection_t4251528776_gp_1_0_0_0_KeyCollection_t4251528776_gp_0_0_0_0,
	&GenInst_KeyCollection_t4251528776_gp_0_0_0_0_KeyCollection_t4251528776_gp_0_0_0_0,
	&GenInst_ValueCollection_t2327722797_gp_0_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0,
	&GenInst_ValueCollection_t2327722797_gp_1_0_0_0,
	&GenInst_Enumerator_t1602367158_gp_0_0_0_0_Enumerator_t1602367158_gp_1_0_0_0,
	&GenInst_Enumerator_t1602367158_gp_1_0_0_0,
	&GenInst_ValueCollection_t2327722797_gp_0_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0,
	&GenInst_ValueCollection_t2327722797_gp_1_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0,
	&GenInst_DictionaryEntry_t3123975638_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_KeyValuePair_2_t1772072192_0_0_0,
	&GenInst_KeyValuePair_2_t1772072192_0_0_0_KeyValuePair_2_t1772072192_0_0_0,
	&GenInst_IDictionary_2_t3177279192_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1708549516_0_0_0,
	&GenInst_IDictionary_2_t3177279192_gp_0_0_0_0_IDictionary_2_t3177279192_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t4175610960_gp_0_0_0_0_KeyValuePair_2_t4175610960_gp_1_0_0_0,
	&GenInst_EqualityComparer_1_t1549919139_gp_0_0_0_0,
	&GenInst_DefaultComparer_t4042948011_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t2270490560_gp_0_0_0_0,
	&GenInst_List_1_t284568025_gp_0_0_0_0,
	&GenInst_Enumerator_t271486022_gp_0_0_0_0,
	&GenInst_Collection_1_t968317937_gp_0_0_0_0,
	&GenInst_KeyedCollection_2_t2564127707_gp_1_0_0_0,
	&GenInst_KeyedCollection_2_t2564127707_gp_0_0_0_0,
	&GenInst_KeyedCollection_2_t2564127707_gp_0_0_0_0_KeyedCollection_2_t2564127707_gp_1_0_0_0,
	&GenInst_ReadOnlyCollection_1_t2757184810_gp_0_0_0_0,
	&GenInst_ArraySegment_1_t3537599374_gp_0_0_0_0,
	&GenInst_Comparer_1_t4245720645_gp_0_0_0_0,
	&GenInst_DefaultComparer_t3277344064_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t3581574675_gp_0_0_0_0,
	&GenInst_LinkedList_1_t2189935060_gp_0_0_0_0,
	&GenInst_Enumerator_t3560274443_gp_0_0_0_0,
	&GenInst_LinkedListNode_1_t3023898186_gp_0_0_0_0,
	&GenInst_RBTree_Intern_m195891103_gp_0_0_0_0,
	&GenInst_RBTree_Remove_m2673281728_gp_0_0_0_0,
	&GenInst_RBTree_Lookup_m2157084951_gp_0_0_0_0,
	&GenInst_RBTree_find_key_m367850148_gp_0_0_0_0,
	&GenInst_Queue_1_t3112285321_gp_0_0_0_0,
	&GenInst_Enumerator_t3782344991_gp_0_0_0_0,
	&GenInst_SortedDictionary_2_t1121010512_gp_0_0_0_0,
	&GenInst_SortedDictionary_2_t1121010512_gp_1_0_0_0,
	&GenInst_SortedDictionary_2_t1121010512_gp_0_0_0_0_SortedDictionary_2_t1121010512_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1939139756_0_0_0,
	&GenInst_Node_t3077268875_gp_0_0_0_0_Node_t3077268875_gp_1_0_0_0,
	&GenInst_NodeHelper_t2693378302_gp_0_0_0_0,
	&GenInst_NodeHelper_t2693378302_gp_0_0_0_0_NodeHelper_t2693378302_gp_1_0_0_0,
	&GenInst_ValueCollection_t1821823815_gp_0_0_0_0_ValueCollection_t1821823815_gp_1_0_0_0,
	&GenInst_ValueCollection_t1821823815_gp_1_0_0_0,
	&GenInst_Enumerator_t2847158728_gp_0_0_0_0_Enumerator_t2847158728_gp_1_0_0_0,
	&GenInst_Enumerator_t2847158728_gp_1_0_0_0,
	&GenInst_KeyCollection_t3072514359_gp_0_0_0_0_KeyCollection_t3072514359_gp_1_0_0_0,
	&GenInst_KeyCollection_t3072514359_gp_0_0_0_0,
	&GenInst_Enumerator_t1065364123_gp_0_0_0_0_Enumerator_t1065364123_gp_1_0_0_0,
	&GenInst_Enumerator_t1065364123_gp_0_0_0_0,
	&GenInst_Enumerator_t665726594_gp_0_0_0_0_Enumerator_t665726594_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t353939364_0_0_0,
	&GenInst_SortedList_2_t3414374367_gp_0_0_0_0,
	&GenInst_SortedList_2_t3414374367_gp_1_0_0_0,
	&GenInst_SortedList_2_t3414374367_gp_0_0_0_0_SortedList_2_t3414374367_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t15439248_0_0_0,
	&GenInst_EnumeratorMode_t4114579086_gp_0_0_0_0_EnumeratorMode_t4114579086_gp_1_0_0_0,
	&GenInst_Enumerator_t435247811_gp_0_0_0_0_Enumerator_t435247811_gp_1_0_0_0,
	&GenInst_KeyEnumerator_t158881392_gp_0_0_0_0_KeyEnumerator_t158881392_gp_1_0_0_0,
	&GenInst_KeyEnumerator_t158881392_gp_0_0_0_0,
	&GenInst_ValueEnumerator_t3711725945_gp_0_0_0_0_ValueEnumerator_t3711725945_gp_1_0_0_0,
	&GenInst_ValueEnumerator_t3711725945_gp_1_0_0_0,
	&GenInst_ListKeys_t3727950806_gp_0_0_0_0_ListKeys_t3727950806_gp_1_0_0_0,
	&GenInst_ListKeys_t3727950806_gp_0_0_0_0,
	&GenInst_GetEnumeratorU3Ec__Iterator2_t3896502338_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator2_t3896502338_gp_1_0_0_0,
	&GenInst_ListValues_t4284896590_gp_0_0_0_0_ListValues_t4284896590_gp_1_0_0_0,
	&GenInst_ListValues_t4284896590_gp_1_0_0_0,
	&GenInst_GetEnumeratorU3Ec__Iterator3_t3518944829_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator3_t3518944829_gp_1_0_0_0,
	&GenInst_GetEnumeratorU3Ec__Iterator0_t3634528505_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator0_t3634528505_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1333058536_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator1_t3684414104_gp_0_0_0_0_U3CGetEnumeratorU3Ec__Iterator1_t3684414104_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1749556876_0_0_0,
	&GenInst_Stack_1_t1463756442_gp_0_0_0_0,
	&GenInst_Enumerator_t2989469293_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m2256625727_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m3644852105_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m3644852105_gp_0_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_Enumerable_Cast_m3341121705_gp_0_0_0_0,
	&GenInst_Enumerable_CreateCastIterator_m1371803983_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m4222801258_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m1710981724_gp_0_0_0_0,
	&GenInst_Enumerable_Empty_m821348200_gp_0_0_0_0,
	&GenInst_Enumerable_First_m1478900696_gp_0_0_0_0,
	&GenInst_Enumerable_First_m1478900696_gp_0_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_Enumerable_First_m2847869424_gp_0_0_0_0,
	&GenInst_Enumerable_First_m2138099720_gp_0_0_0_0,
	&GenInst_Enumerable_First_m2138099720_gp_0_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_Enumerable_FirstOrDefault_m946660525_gp_0_0_0_0,
	&GenInst_Enumerable_FirstOrDefault_m946660525_gp_0_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_Enumerable_Last_m3038740421_gp_0_0_0_0,
	&GenInst_Enumerable_Last_m3038740421_gp_0_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_Enumerable_LastOrDefault_m707763884_gp_0_0_0_0,
	&GenInst_Enumerable_LastOrDefault_m707763884_gp_0_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_Enumerable_ToArray_m1974389789_gp_0_0_0_0,
	&GenInst_Enumerable_ToList_m2780466421_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m88697338_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m88697338_gp_0_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m3238110963_gp_0_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m3238110963_gp_0_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t373619397_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t945640688_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t945640688_gp_0_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_HashSet_1_t743387557_gp_0_0_0_0,
	&GenInst_Enumerator_t3836401716_gp_0_0_0_0,
	&GenInst_PrimeHelper_t2385147435_gp_0_0_0_0,
	&GenInst_AppResourcesLengthComparer_1_t4191904486_gp_0_0_0_0,
	&GenInst_GenericBuildProvider_1_t2468417315_gp_0_0_0_0,
	&GenInst_SecurityTokenRequirement_GetProperty_m2382599284_gp_0_0_0_0,
	&GenInst_AndroidJavaObject_Call_m2209455074_gp_0_0_0_0,
	&GenInst_AndroidJavaObject_CallStatic_m3993809864_gp_0_0_0_0,
	&GenInst_AndroidJavaObject__Call_m113691125_gp_0_0_0_0,
	&GenInst_AndroidJavaObject__CallStatic_m109308273_gp_0_0_0_0,
	&GenInst_AndroidJNIHelper_ConvertFromJNIArray_m3095982117_gp_0_0_0_0,
	&GenInst_AndroidJNIHelper_GetMethodID_m1845802273_gp_0_0_0_0,
	&GenInst_Component_GetComponentInChildren_m1404611044_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m3615474666_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m933673001_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m4005089511_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m1915630048_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m555240590_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m2569671904_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m149672228_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m3598713100_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m3914262141_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentInChildren_m916317014_gp_0_0_0_0,
	&GenInst_GameObject_GetComponents_m2386781050_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m906810621_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m1887210244_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m241541908_gp_0_0_0_0,
	&GenInst_Mesh_GetAllocArrayFromChannel_m2078183201_gp_0_0_0_0,
	&GenInst_Mesh_SafeLength_m1325240367_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m462664139_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m631926134_gp_0_0_0_0,
	&GenInst_Mesh_SetUvsImpl_m1664867204_gp_0_0_0_0,
	&GenInst_Object_Instantiate_m1874094322_gp_0_0_0_0,
	&GenInst_Object_FindObjectsOfType_m1824391917_gp_0_0_0_0,
	&GenInst__AndroidJNIHelper_GetMethodID_m1668244749_gp_0_0_0_0,
	&GenInst_InvokableCall_1_t3865199217_gp_0_0_0_0,
	&GenInst_UnityAction_1_t802700511_0_0_0,
	&GenInst_InvokableCall_2_t3865133681_gp_0_0_0_0_InvokableCall_2_t3865133681_gp_1_0_0_0,
	&GenInst_InvokableCall_2_t3865133681_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t3865133681_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3865068145_gp_0_0_0_0_InvokableCall_3_t3865068145_gp_1_0_0_0_InvokableCall_3_t3865068145_gp_2_0_0_0,
	&GenInst_InvokableCall_3_t3865068145_gp_0_0_0_0,
	&GenInst_InvokableCall_3_t3865068145_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3865068145_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t3865002609_gp_0_0_0_0_InvokableCall_4_t3865002609_gp_1_0_0_0_InvokableCall_4_t3865002609_gp_2_0_0_0_InvokableCall_4_t3865002609_gp_3_0_0_0,
	&GenInst_InvokableCall_4_t3865002609_gp_0_0_0_0,
	&GenInst_InvokableCall_4_t3865002609_gp_1_0_0_0,
	&GenInst_InvokableCall_4_t3865002609_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t3865002609_gp_3_0_0_0,
	&GenInst_CachedInvokableCall_1_t3153979999_gp_0_0_0_0,
	&GenInst_UnityEvent_1_t74220259_gp_0_0_0_0,
	&GenInst_UnityEvent_2_t477504786_gp_0_0_0_0_UnityEvent_2_t477504786_gp_1_0_0_0,
	&GenInst_UnityEvent_3_t3206388141_gp_0_0_0_0_UnityEvent_3_t3206388141_gp_1_0_0_0_UnityEvent_3_t3206388141_gp_2_0_0_0,
	&GenInst_UnityEvent_4_t3609672668_gp_0_0_0_0_UnityEvent_4_t3609672668_gp_1_0_0_0_UnityEvent_4_t3609672668_gp_2_0_0_0_UnityEvent_4_t3609672668_gp_3_0_0_0,
	&GenInst_DelegateHelper_InvokeWithExceptionHandling_m3762946312_gp_0_0_0_0,
	&GenInst_DelegateHelper_InvokeWithExceptionHandling_m2366197299_gp_0_0_0_0_DelegateHelper_InvokeWithExceptionHandling_m2366197299_gp_1_0_0_0,
	&GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m3375019273_gp_0_0_0_0,
	&GenInst_List_1_t350623716_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator6_t3713055553_gp_0_0_0_0,
	&GenInst_List_1_t3155882222_0_0_0,
	&GenInst_Type_t_0_0_0_KeyedByTypeCollection_1_t3023398834_gp_0_0_0_0,
	&GenInst_KeyedByTypeCollection_1_FindAll_m3460713529_gp_0_0_0_0,
	&GenInst_KeyedByTypeCollection_1_t3023398834_gp_0_0_0_0,
	&GenInst_SynchronizedCollection_1_t2430655651_gp_0_0_0_0,
	&GenInst_SynchronizedKeyedCollection_2_t3513088456_gp_1_0_0_0,
	&GenInst_SynchronizedKeyedCollection_2_t3513088456_gp_0_0_0_0_SynchronizedKeyedCollection_2_t3513088456_gp_1_0_0_0,
	&GenInst_SynchronizedReadOnlyCollection_1_t1361004819_gp_0_0_0_0,
	&GenInst_AsymmetricSecurityBindingElement_BuildChannelFactoryCore_m1751482239_gp_0_0_0_0,
	&GenInst_AsymmetricSecurityBindingElement_BuildChannelListenerCore_m1845291049_gp_0_0_0_0,
	&GenInst_AsymmetricSecurityBindingElement_GetProperty_m2812938255_gp_0_0_0_0,
	&GenInst_BinaryMessageEncodingBindingElement_BuildChannelFactory_m2836120619_gp_0_0_0_0,
	&GenInst_BinaryMessageEncodingBindingElement_BuildChannelListener_m2460778595_gp_0_0_0_0,
	&GenInst_BinaryMessageEncodingBindingElement_CanBuildChannelListener_m3187724959_gp_0_0_0_0,
	&GenInst_Binding_BuildChannelFactory_m2702276000_gp_0_0_0_0,
	&GenInst_Binding_BuildChannelListener_m3485620627_gp_0_0_0_0,
	&GenInst_Binding_CanBuildChannelFactory_m3892110411_gp_0_0_0_0,
	&GenInst_Binding_CanBuildChannelListener_m2369400018_gp_0_0_0_0,
	&GenInst_BindingContext_BuildInnerChannelFactory_m364505994_gp_0_0_0_0,
	&GenInst_BindingContext_BuildInnerChannelListener_m2649823158_gp_0_0_0_0,
	&GenInst_BindingContext_CanBuildInnerChannelFactory_m142850783_gp_0_0_0_0,
	&GenInst_BindingContext_CanBuildInnerChannelListener_m235529581_gp_0_0_0_0,
	&GenInst_BindingContext_GetInnerProperty_m1828077793_gp_0_0_0_0,
	&GenInst_BindingElement_BuildChannelFactory_m384837745_gp_0_0_0_0,
	&GenInst_BindingElement_BuildChannelListener_m517227291_gp_0_0_0_0,
	&GenInst_BindingElement_CanBuildChannelFactory_m3948462205_gp_0_0_0_0,
	&GenInst_BindingElement_CanBuildChannelListener_m1475658708_gp_0_0_0_0,
	&GenInst_BindingElementCollection_RemoveAll_m127050395_gp_0_0_0_0,
	&GenInst_TransportChannelFactoryBase_1_t4012543988_gp_0_0_0_0,
	&GenInst_ChannelFactoryBase_1_t2067588991_gp_0_0_0_0,
	&GenInst_ChannelListenerBase_GetProperty_m3547516831_gp_0_0_0_0,
	&GenInst_InternalChannelListenerBase_1_t1255230044_gp_0_0_0_0,
	&GenInst_TimeSpan_t881159249_0_0_0_InternalChannelListenerBase_1_t1255230044_gp_0_0_0_0,
	&GenInst_U3COnBeginAcceptChannelU3Ec__AnonStorey8_t1859370651_gp_0_0_0_0,
	&GenInst_ChannelListenerBase_1_t3869036235_gp_0_0_0_0,
	&GenInst_HttpChannelFactory_1_t1944017751_gp_0_0_0_0,
	&GenInst_HttpSimpleChannelListener_1_t176083481_gp_0_0_0_0,
	&GenInst_AspNetChannelListener_1_t2531383694_gp_0_0_0_0,
	&GenInst_HttpChannelListenerBase_1_t1301399014_gp_0_0_0_0,
	&GenInst_HttpTransportBindingElement_BuildChannelFactory_m3380490396_gp_0_0_0_0,
	&GenInst_HttpTransportBindingElement_BuildChannelListener_m2096701392_gp_0_0_0_0,
	&GenInst_HttpTransportBindingElement_GetProperty_m3676928347_gp_0_0_0_0,
	&GenInst_HttpsTransportBindingElement_BuildChannelFactory_m462496859_gp_0_0_0_0,
	&GenInst_HttpsTransportBindingElement_BuildChannelListener_m1543235516_gp_0_0_0_0,
	&GenInst_MessageHeaders_GetHeader_m2299488900_gp_0_0_0_0,
	&GenInst_MsmqBindingElementBase_GetProperty_m3952560298_gp_0_0_0_0,
	&GenInst_MsmqChannelFactory_1_t2220480299_gp_0_0_0_0,
	&GenInst_MsmqTransportBindingElement_BuildChannelFactory_m1998761050_gp_0_0_0_0,
	&GenInst_MsmqTransportBindingElement_BuildChannelListener_m3603513958_gp_0_0_0_0,
	&GenInst_MtomMessageEncodingBindingElement_BuildChannelFactory_m908181888_gp_0_0_0_0,
	&GenInst_MtomMessageEncodingBindingElement_BuildChannelListener_m194984888_gp_0_0_0_0,
	&GenInst_MtomMessageEncodingBindingElement_CanBuildChannelListener_m3195457011_gp_0_0_0_0,
	&GenInst_NamedPipeChannelFactory_1_t474837288_gp_0_0_0_0,
	&GenInst_NamedPipeChannelListener_1_t3969090434_gp_0_0_0_0,
	&GenInst_U3COnAcceptChannelU3Ec__AnonStoreyE_t609268473_gp_0_0_0_0,
	&GenInst_NamedPipeTransportBindingElement_BuildChannelFactory_m1135370280_gp_0_0_0_0,
	&GenInst_NamedPipeTransportBindingElement_BuildChannelListener_m2417158508_gp_0_0_0_0,
	&GenInst_NamedPipeTransportBindingElement_GetProperty_m656345567_gp_0_0_0_0,
	&GenInst_PeerChannelFactory_1_t3525877856_gp_0_0_0_0,
	&GenInst_PeerChannelListener_1_t665875855_gp_0_0_0_0,
	&GenInst_PeerCustomResolverBindingElement_BuildChannelFactory_m4000210886_gp_0_0_0_0,
	&GenInst_PeerCustomResolverBindingElement_BuildChannelListener_m2913003754_gp_0_0_0_0,
	&GenInst_PeerTransportBindingElement_BuildChannelFactory_m852669533_gp_0_0_0_0,
	&GenInst_PeerTransportBindingElement_BuildChannelListener_m4218990573_gp_0_0_0_0,
	&GenInst_PeerTransportBindingElement_GetProperty_m322264804_gp_0_0_0_0,
	&GenInst_PnrpPeerResolverBindingElement_BuildChannelFactory_m762075276_gp_0_0_0_0,
	&GenInst_PnrpPeerResolverBindingElement_BuildChannelListener_m3914545043_gp_0_0_0_0,
	&GenInst_SecurityBindingElement_CanBuildChannelFactory_m2127198948_gp_0_0_0_0,
	&GenInst_SecurityBindingElement_BuildChannelFactory_m893612399_gp_0_0_0_0,
	&GenInst_SecurityBindingElement_BuildChannelFactoryCore_m4206137793_gp_0_0_0_0,
	&GenInst_SecurityBindingElement_CanBuildChannelListener_m3725582969_gp_0_0_0_0,
	&GenInst_SecurityBindingElement_BuildChannelListener_m3136879580_gp_0_0_0_0,
	&GenInst_SecurityBindingElement_BuildChannelListenerCore_m1443472304_gp_0_0_0_0,
	&GenInst_SecurityChannelFactory_1_t2661327900_gp_0_0_0_0,
	&GenInst_SecurityChannelListener_1_t3584330813_gp_0_0_0_0,
	&GenInst_SecurityChannelListener_1_GetProperty_m3548153566_gp_0_0_0_0,
	&GenInst_SymmetricSecurityBindingElement_BuildChannelFactoryCore_m3664457509_gp_0_0_0_0,
	&GenInst_SymmetricSecurityBindingElement_BuildChannelListenerCore_m3398717442_gp_0_0_0_0,
	&GenInst_SymmetricSecurityBindingElement_GetProperty_m527245184_gp_0_0_0_0,
	&GenInst_TcpChannelFactory_1_t1190800706_gp_0_0_0_0,
	&GenInst_TcpChannelListener_1_t1314382410_gp_0_0_0_0,
	&GenInst_U3COnAcceptChannelU3Ec__AnonStorey12_t603992219_gp_0_0_0_0,
	&GenInst_U3CAcceptTcpClientU3Ec__AnonStorey13_t3277984792_gp_0_0_0_0,
	&GenInst_U3CAcceptTcpClientU3Ec__AnonStorey14_t3277526040_gp_0_0_0_0,
	&GenInst_TcpTransportBindingElement_BuildChannelFactory_m3905769953_gp_0_0_0_0,
	&GenInst_TcpTransportBindingElement_BuildChannelListener_m2569812045_gp_0_0_0_0,
	&GenInst_TcpTransportBindingElement_GetProperty_m1350457265_gp_0_0_0_0,
	&GenInst_TextMessageEncodingBindingElement_BuildChannelFactory_m1972125352_gp_0_0_0_0,
	&GenInst_TextMessageEncodingBindingElement_BuildChannelListener_m45428996_gp_0_0_0_0,
	&GenInst_TextMessageEncodingBindingElement_CanBuildChannelListener_m1697465190_gp_0_0_0_0,
	&GenInst_TextMessageEncodingBindingElement_GetProperty_m3914452139_gp_0_0_0_0,
	&GenInst_TransactionFlowBindingElement_CanBuildChannelFactory_m4093754355_gp_0_0_0_0,
	&GenInst_TransactionFlowBindingElement_CanBuildChannelListener_m738127627_gp_0_0_0_0,
	&GenInst_TransactionFlowBindingElement_BuildChannelFactory_m281478039_gp_0_0_0_0,
	&GenInst_TransactionFlowBindingElement_BuildChannelListener_m2433100597_gp_0_0_0_0,
	&GenInst_TransactionChannelFactory_1_t3098953095_gp_0_0_0_0,
	&GenInst_TransactionChannelListener_1_t2500118520_gp_0_0_0_0,
	&GenInst_TransportBindingElement_GetProperty_m2036251320_gp_0_0_0_0,
	&GenInst_TransportSecurityBindingElement_BuildChannelFactoryCore_m2596990168_gp_0_0_0_0,
	&GenInst_TransportSecurityBindingElement_BuildChannelListenerCore_m1393449313_gp_0_0_0_0,
	&GenInst_WSSecurityMessageHeader_FindAll_m4266741189_gp_0_0_0_0,
	&GenInst_StandardBindingElementCollection_1_t2952518451_gp_0_0_0_0,
	&GenInst_MexBindingBindingCollectionElement_2_t2854287389_gp_0_0_0_0_MexBindingBindingCollectionElement_2_t2854287389_gp_1_0_0_0,
	&GenInst_NamedServiceModelExtensionCollectionElement_1_t1517949335_gp_0_0_0_0,
	&GenInst_ServiceModelConfigurationElementCollection_1_t399084843_gp_0_0_0_0,
	&GenInst_ServiceModelEnhancedConfigurationElementCollection_1_t2668806557_gp_0_0_0_0,
	&GenInst_ServiceModelExtensionCollectionElement_1_t1335197220_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator5_t997212165_gp_0_0_0_0,
	&GenInst_Type_t_0_0_0_ServiceModelExtensionCollectionElement_1_t1335197220_gp_0_0_0_0,
	&GenInst_StandardBindingCollectionElement_2_t2904503531_gp_1_0_0_0,
	&GenInst_StandardBindingCollectionElement_2_t2904503531_gp_0_0_0_0_StandardBindingCollectionElement_2_t2904503531_gp_1_0_0_0,
	&GenInst_ListenerLoopManager_CreateAcceptor_m3081673401_gp_0_0_0_0,
	&GenInst_U3CCreateAcceptorU3Ec__AnonStorey1C_1_t1865474583_gp_0_0_0_0,
	&GenInst_ChannelFactory_1_t1254119128_gp_0_0_0_0,
	&GenInst_ClientBase_1_t3896424024_gp_0_0_0_0,
	&GenInst_DuplexChannelFactory_1_t1825049079_gp_0_0_0_0,
	&GenInst_IExtension_1_t293605571_0_0_0,
	&GenInst_ExtensionCollection_1_t4111789460_gp_0_0_0_0,
	&GenInst_IExtensibleObject_1_t289308579_gp_0_0_0_0,
	&GenInst_IExtension_1_t3774563217_gp_0_0_0_0,
	&GenInst_IExtension_1_t1118430608_0_0_0,
	&GenInst_IExtensionCollection_1_t641647201_gp_0_0_0_0,
	&GenInst_ExecuteEvents_Execute_m513682320_gp_0_0_0_0,
	&GenInst_ExecuteEvents_ExecuteHierarchy_m2988888012_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventList_m400222725_gp_0_0_0_0,
	&GenInst_ExecuteEvents_CanHandleEvent_m1773513752_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventHandler_m3154756557_gp_0_0_0_0,
	&GenInst_TweenRunner_1_t3844461449_gp_0_0_0_0,
	&GenInst_Dropdown_GetOrAddComponent_m4080506703_gp_0_0_0_0,
	&GenInst_SetPropertyUtility_SetStruct_m2798032208_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t2120020791_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t2120020791_gp_0_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_ListPool_1_t890186770_gp_0_0_0_0,
	&GenInst_List_1_t3009893961_0_0_0,
	&GenInst_ObjectPool_1_t892185599_gp_0_0_0_0,
	&GenInst_BetterList_1_t2240351297_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t4155939275_gp_0_0_0_0,
	&GenInst_NGUITools_AddChild_m4126302437_gp_0_0_0_0,
	&GenInst_NGUITools_AddChild_m2329166398_gp_0_0_0_0,
	&GenInst_NGUITools_AddWidget_m2648584008_gp_0_0_0_0,
	&GenInst_NGUITools_AddWidget_m327501609_gp_0_0_0_0,
	&GenInst_NGUITools_FindInParents_m3877358550_gp_0_0_0_0,
	&GenInst_NGUITools_FindInParents_m1163017570_gp_0_0_0_0,
	&GenInst_NGUITools_AddMissingComponent_m4019644757_gp_0_0_0_0,
	&GenInst_NGUITools_Execute_m517571378_gp_0_0_0_0,
	&GenInst_NGUITools_ExecuteAll_m1520266422_gp_0_0_0_0,
	&GenInst_UITweener_Begin_m3540790004_gp_0_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_CodeTypeMember_t1555525554_0_0_0,
	&GenInst_SerializableAttribute_t1992588303_0_0_0,
	&GenInst_DataContractAttribute_t412496005_0_0_0,
	&GenInst_CollectionDataContractAttribute_t3671020647_0_0_0,
	&GenInst_XmlRootAttribute_t2306097217_0_0_0,
	&GenInst_CompilationSection_t1832220892_0_0_0,
	&GenInst_ClientTargetSection_t572227672_0_0_0,
	&GenInst_PagesSection_t1064701840_0_0_0,
	&GenInst_SecurityKeyUsage_t3512984903_0_0_0,
	&GenInst_DefaultExecutionOrder_t3059642329_0_0_0,
	&GenInst_PlayerConnection_t3081694049_0_0_0,
	&GenInst_ParticleSystem_t1800779281_0_0_0,
	&GenInst_GUILayer_t2783472903_0_0_0,
	&GenInst_ObjectTracker_t4177997237_0_0_0,
	&GenInst_SmartTerrainTracker_t1238706968_0_0_0,
	&GenInst_DeviceTracker_t2315692373_0_0_0,
	&GenInst_RotationalDeviceTracker_t2847210804_0_0_0,
	&GenInst_DistortionRenderingBehaviour_t1858983148_0_0_0,
	&GenInst_ReconstructionFromTarget_t3809448279_0_0_0,
	&GenInst_TextTracker_t3950053289_0_0_0,
	&GenInst_VuforiaAbstractConfiguration_t2684447159_0_0_0,
	&GenInst_VuforiaDeinitBehaviour_t1700985552_0_0_0,
	&GenInst_MetadataPublishingInfo_t2085662404_0_0_0,
	&GenInst_HttpListenerManager_t555124713_0_0_0,
	&GenInst_EndpointAddress_t3119842923_0_0_0,
	&GenInst_ServiceBehaviorAttribute_t1657799891_0_0_0,
	&GenInst_IPeerConnectorContract_t3354905095_0_0_0,
	&GenInst_Binding_t859993683_0_0_0,
	&GenInst_BindingContext_t2842489830_0_0_0,
	&GenInst_SecurityTokenVersion_t1981162148_0_0_0,
	&GenInst_SecurityAlgorithmSuite_t2980594242_0_0_0,
	&GenInst_OperationBehaviorAttribute_t1398166452_0_0_0,
	&GenInst_ServiceMetadataExtension_t3543702878_0_0_0,
	&GenInst_MessageEncodingBindingElement_t3531153504_0_0_0,
	&GenInst_MessageSecurityBindingSupport_t1904510157_0_0_0,
	&GenInst_ISecurityCapabilities_t4129699402_0_0_0,
	&GenInst_X509IssuerSerialKeyIdentifierClause_t3285382384_0_0_0,
	&GenInst_X509ThumbprintKeyIdentifierClause_t1082272133_0_0_0,
	&GenInst_X509SubjectKeyIdentifierClause_t676849360_0_0_0,
	&GenInst_X509RawDataKeyIdentifierClause_t329336726_0_0_0,
	&GenInst_ServiceMetadataBehavior_t566713614_0_0_0,
	&GenInst_ServiceAuthorizationBehavior_t248521010_0_0_0,
	&GenInst_ServiceDebugBehavior_t879073333_0_0_0,
	&GenInst_EventSystem_t1003666588_0_0_0,
	&GenInst_AxisEventData_t2331243652_0_0_0,
	&GenInst_RawImage_t3182918964_0_0_0,
	&GenInst_Slider_t3903728902_0_0_0,
	&GenInst_Scrollbar_t1494447233_0_0_0,
	&GenInst_InputField_t3762917431_0_0_0,
	&GenInst_ScrollRect_t4137855814_0_0_0,
	&GenInst_Dropdown_t2274391225_0_0_0,
	&GenInst_GraphicRaycaster_t2999697109_0_0_0,
	&GenInst_CanvasRenderer_t2598313366_0_0_0,
	&GenInst_Corner_t1493259673_0_0_0,
	&GenInst_Axis_t3613393006_0_0_0,
	&GenInst_Constraint_t814224393_0_0_0,
	&GenInst_SubmitEvent_t648412432_0_0_0,
	&GenInst_OnChangeEvent_t467195904_0_0_0,
	&GenInst_OnValidateInput_t2355412304_0_0_0,
	&GenInst_LayoutElement_t1785403678_0_0_0,
	&GenInst_RectOffset_t1369453676_0_0_0,
	&GenInst_TextAnchor_t2035777396_0_0_0,
	&GenInst_AnimationTriggers_t2532145056_0_0_0,
	&GenInst_GUITexture_t951903601_0_0_0,
	&GenInst_GUIText_t402233326_0_0_0,
	&GenInst_Light_t3756812086_0_0_0,
	&GenInst_AudioSource_t3935305588_0_0_0,
	&GenInst_Rigidbody_t3916780224_0_0_0,
	&GenInst_Texture2D_t3840446185_0_0_0,
	&GenInst_ActiveAnimation_t3475256642_0_0_0,
	&GenInst_HighlighterController_t3493614325_0_0_0,
	&GenInst_UIInput_t421821618_0_0_0,
	&GenInst_UITexture_t3471168817_0_0_0,
	&GenInst_BoxCollider_t1640800422_0_0_0,
	&GenInst_InvEquipment_t3413562611_0_0_0,
	&GenInst_ExampleDragDropSurface_t2709993285_0_0_0,
	&GenInst_UIPopupList_t4167399471_0_0_0,
	&GenInst_Win_Panel_Controller_t2195695921_0_0_0,
	&GenInst_Scrollbar_Controller_t2992837185_0_0_0,
	&GenInst_Tips_Controller_t170528621_0_0_0,
	&GenInst_TextAsset_t3022178571_0_0_0,
	&GenInst_GameManager_t1536523654_0_0_0,
	&GenInst_NGUIDebug_t787955914_0_0_0,
	&GenInst_BoxCollider2D_t3581341831_0_0_0,
	&GenInst_UIAnchor_t2527798900_0_0_0,
	&GenInst_Animation_t3648466861_0_0_0,
	&GenInst_Change_Sprite_t4001473290_0_0_0,
	&GenInst_ObjectPuter_t3420787522_0_0_0,
	&GenInst_Coroutine_t3196085901_0_0_0,
	&GenInst_TimeLevel4_t4195116982_0_0_0,
	&GenInst_TimeGame_t3115909209_0_0_0,
	&GenInst_SpringPanel_t277350554_0_0_0,
	&GenInst_SpringPosition_t3478173108_0_0_0,
	&GenInst_Level_05_Controller_t1454764318_0_0_0,
	&GenInst_Level_04_Controller_t985329950_0_0_0,
	&GenInst_TweenAlpha_t3706845226_0_0_0,
	&GenInst_TweenColor_t2112002648_0_0_0,
	&GenInst_TweenFOV_t2203484580_0_0_0,
	&GenInst_UITable_t3168834800_0_0_0,
	&GenInst_TweenHeight_t4009371699_0_0_0,
	&GenInst_TweenOrthoSize_t2102937296_0_0_0,
	&GenInst_TweenPosition_t1378762002_0_0_0,
	&GenInst_TweenRotation_t3072670746_0_0_0,
	&GenInst_TweenTransform_t1195296467_0_0_0,
	&GenInst_TweenVolume_t3718612080_0_0_0,
	&GenInst_TweenWidth_t2861389279_0_0_0,
	&GenInst_UI2DSprite_t1366157572_0_0_0,
	&GenInst_UIGrid_t1536638187_0_0_0,
	&GenInst_UIWrapContent_t1188558554_0_0_0,
	&GenInst_UICenterOnChild_t253063637_0_0_0,
	&GenInst_UIDraggableCamera_t1644204495_0_0_0,
	&GenInst_UIDragScrollView_t2492060641_0_0_0,
	&GenInst_UIDragDropContainer_t2199185397_0_0_0,
	&GenInst_UIEventListener_t1665237878_0_0_0,
	&GenInst_UIStorageSlot_t392832716_0_0_0,
	&GenInst_UIOrthoCamera_t1944225589_0_0_0,
	&GenInst_UIProgressBar_t1222110469_0_0_0,
	&GenInst_UISlider_t886033014_0_0_0,
	&GenInst_StandaloneInputModule_t2760469101_0_0_0,
	&GenInst_BackgroundPlaneBehaviour_t3333547397_0_0_0,
	&GenInst_ReconstructionBehaviour_t3655135626_0_0_0,
	&GenInst_ComponentFactoryStarterBehaviour_t3931359467_0_0_0,
	&GenInst_VuforiaBehaviour_t2151848540_0_0_0,
	&GenInst_MaskOutBehaviour_t2745617306_0_0_0,
	&GenInst_VirtualButtonBehaviour_t1436326451_0_0_0,
	&GenInst_TurnOffBehaviour_t65964226_0_0_0,
	&GenInst_MultiTargetBehaviour_t2061511750_0_0_0,
	&GenInst_CylinderTargetBehaviour_t822809409_0_0_0,
	&GenInst_WordBehaviour_t209462683_0_0_0,
	&GenInst_TextRecoBehaviour_t87475147_0_0_0,
	&GenInst_ObjectTargetBehaviour_t728125005_0_0_0,
	&GenInst_VuMarkBehaviour_t1178230459_0_0_0,
	&GenInst_VuforiaConfiguration_t1763229349_0_0_0,
	&GenInst_ClientCredentials_t1418047401_0_0_0,
	&GenInst_ChannelProtectionRequirements_t2141883287_0_0_0,
	&GenInst_ServiceCredentials_t3431196922_0_0_0,
	&GenInst_Data_t1588725102_0_0_0_Data_t1588725102_0_0_0,
	&GenInst_Preset_t2972107632_0_0_0_Preset_t2972107632_0_0_0,
	&GenInst_SetAnimInfo_t2127528194_0_0_0_SetAnimInfo_t2127528194_0_0_0,
	&GenInst_Byte_t1134296376_0_0_0_Byte_t1134296376_0_0_0,
	&GenInst_KeyValuePair_2_t2530217319_0_0_0_KeyValuePair_2_t2530217319_0_0_0,
	&GenInst_DateTime_t3738529785_0_0_0_DateTime_t3738529785_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t287865710_0_0_0_CustomAttributeNamedArgument_t287865710_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t2723150157_0_0_0_CustomAttributeTypedArgument_t2723150157_0_0_0,
	&GenInst_EnumMemberInfo_t3072296154_0_0_0_EnumMemberInfo_t3072296154_0_0_0,
	&GenInst_CodeUnit_t1519973372_0_0_0_CodeUnit_t1519973372_0_0_0,
	&GenInst_AnimatorClipInfo_t3156717155_0_0_0_AnimatorClipInfo_t3156717155_0_0_0,
	&GenInst_Color32_t2600501292_0_0_0_Color32_t2600501292_0_0_0,
	&GenInst_RaycastResult_t3360306849_0_0_0_RaycastResult_t3360306849_0_0_0,
	&GenInst_UICharInfo_t75501106_0_0_0_UICharInfo_t75501106_0_0_0,
	&GenInst_UILineInfo_t4195266810_0_0_0_UILineInfo_t4195266810_0_0_0,
	&GenInst_UIVertex_t4057497605_0_0_0_UIVertex_t4057497605_0_0_0,
	&GenInst_Vector2_t2156229523_0_0_0_Vector2_t2156229523_0_0_0,
	&GenInst_Vector3_t3722313464_0_0_0_Vector3_t3722313464_0_0_0,
	&GenInst_Vector4_t3319028937_0_0_0_Vector4_t3319028937_0_0_0,
	&GenInst_CameraField_t1483002240_0_0_0_CameraField_t1483002240_0_0_0,
	&GenInst_PIXEL_FORMAT_t3209881435_0_0_0_PIXEL_FORMAT_t3209881435_0_0_0,
	&GenInst_TargetSearchResult_t3441982613_0_0_0_TargetSearchResult_t3441982613_0_0_0,
	&GenInst_TrackableIdPair_t4227350457_0_0_0_TrackableIdPair_t4227350457_0_0_0,
	&GenInst_VuMarkTargetData_t3925829072_0_0_0_VuMarkTargetData_t3925829072_0_0_0,
	&GenInst_VuMarkTargetResultData_t1414459591_0_0_0_VuMarkTargetResultData_t1414459591_0_0_0,
	&GenInst_KeyValuePair_2_t4237331251_0_0_0_KeyValuePair_2_t4237331251_0_0_0,
	&GenInst_KeyValuePair_2_t4237331251_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t71524366_0_0_0_KeyValuePair_2_t71524366_0_0_0,
	&GenInst_KeyValuePair_2_t71524366_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3568047141_0_0_0_KeyValuePair_2_t3568047141_0_0_0,
	&GenInst_KeyValuePair_2_t3568047141_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Label_t2281661643_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Label_t2281661643_0_0_0_Label_t2281661643_0_0_0,
	&GenInst_KeyValuePair_2_t2387291312_0_0_0_KeyValuePair_2_t2387291312_0_0_0,
	&GenInst_KeyValuePair_2_t2387291312_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Status_t1100905814_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Status_t1100905814_0_0_0_Status_t1100905814_0_0_0,
	&GenInst_KeyValuePair_2_t4188143612_0_0_0_KeyValuePair_2_t4188143612_0_0_0,
	&GenInst_KeyValuePair_2_t4188143612_0_0_0_Il2CppObject_0_0_0,
	&GenInst_VirtualButtonData_t2901758114_0_0_0_Il2CppObject_0_0_0,
	&GenInst_VirtualButtonData_t2901758114_0_0_0_VirtualButtonData_t2901758114_0_0_0,
	&GenInst_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_KeyValuePair_2_t3842366416_0_0_0_KeyValuePair_2_t3842366416_0_0_0,
	&GenInst_KeyValuePair_2_t3842366416_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3188640940_0_0_0_KeyValuePair_2_t3188640940_0_0_0,
	&GenInst_KeyValuePair_2_t3188640940_0_0_0_Il2CppObject_0_0_0,
	&GenInst_DateTime_t3738529785_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2401056908_0_0_0_KeyValuePair_2_t2401056908_0_0_0,
	&GenInst_KeyValuePair_2_t2401056908_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2530217319_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1627836113_0_0_0_KeyValuePair_2_t1627836113_0_0_0,
	&GenInst_KeyValuePair_2_t1627836113_0_0_0_Il2CppObject_0_0_0,
	&GenInst_UInt16_t2177724958_0_0_0_Il2CppObject_0_0_0,
	&GenInst_UInt16_t2177724958_0_0_0_UInt16_t2177724958_0_0_0,
	&GenInst_KeyValuePair_2_t1377593753_0_0_0_KeyValuePair_2_t1377593753_0_0_0,
	&GenInst_KeyValuePair_2_t1377593753_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TextEditOp_t1927482598_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TextEditOp_t1927482598_0_0_0_TextEditOp_t1927482598_0_0_0,
	&GenInst_KeyValuePair_2_t2969503080_0_0_0_KeyValuePair_2_t2969503080_0_0_0,
	&GenInst_KeyValuePair_2_t2969503080_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ProfileData_t3519391925_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ProfileData_t3519391925_0_0_0_ProfileData_t3519391925_0_0_0,
	&GenInst_KeyValuePair_2_t4068375620_0_0_0_KeyValuePair_2_t4068375620_0_0_0,
	&GenInst_KeyValuePair_2_t4068375620_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3558069120_0_0_0_KeyValuePair_2_t3558069120_0_0_0,
	&GenInst_KeyValuePair_2_t3558069120_0_0_0_Il2CppObject_0_0_0,
	&GenInst_PoseAgeEntry_t2181165958_0_0_0_Il2CppObject_0_0_0,
	&GenInst_PoseAgeEntry_t2181165958_0_0_0_PoseAgeEntry_t2181165958_0_0_0,
	&GenInst_TrackableIdPair_t4227350457_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2989632341_0_0_0_KeyValuePair_2_t2989632341_0_0_0,
	&GenInst_KeyValuePair_2_t2989632341_0_0_0_Il2CppObject_0_0_0,
	&GenInst_PoseInfo_t1612729179_0_0_0_Il2CppObject_0_0_0,
	&GenInst_PoseInfo_t1612729179_0_0_0_PoseInfo_t1612729179_0_0_0,
	&GenInst_KeyValuePair_2_t2477808976_0_0_0_KeyValuePair_2_t2477808976_0_0_0,
	&GenInst_KeyValuePair_2_t2477808976_0_0_0_Il2CppObject_0_0_0,
	&GenInst_XmlDictionaryReaderQuotas_t173030297_0_0_0,
};
