﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Vuforia_UnityExtensions_Vuforia_EyewearCalibrationP947793426.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.EyewearCalibrationProfileManagerImpl
struct  EyewearCalibrationProfileManagerImpl_t3765540215  : public EyewearCalibrationProfileManager_t947793426
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
