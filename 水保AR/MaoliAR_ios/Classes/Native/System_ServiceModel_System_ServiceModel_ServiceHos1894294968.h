﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_ServiceHos3741910535.h"

// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<System.String,System.ServiceModel.Description.ContractDescription>
struct Dictionary_2_t1196434813;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.ServiceHost
struct  ServiceHost_t1894294968  : public ServiceHostBase_t3741910535
{
public:
	// System.Type System.ServiceModel.ServiceHost::service_type
	Type_t * ___service_type_26;
	// System.Object System.ServiceModel.ServiceHost::instance
	Il2CppObject * ___instance_27;
	// System.Collections.Generic.Dictionary`2<System.String,System.ServiceModel.Description.ContractDescription> System.ServiceModel.ServiceHost::contracts
	Dictionary_2_t1196434813 * ___contracts_28;

public:
	inline static int32_t get_offset_of_service_type_26() { return static_cast<int32_t>(offsetof(ServiceHost_t1894294968, ___service_type_26)); }
	inline Type_t * get_service_type_26() const { return ___service_type_26; }
	inline Type_t ** get_address_of_service_type_26() { return &___service_type_26; }
	inline void set_service_type_26(Type_t * value)
	{
		___service_type_26 = value;
		Il2CppCodeGenWriteBarrier(&___service_type_26, value);
	}

	inline static int32_t get_offset_of_instance_27() { return static_cast<int32_t>(offsetof(ServiceHost_t1894294968, ___instance_27)); }
	inline Il2CppObject * get_instance_27() const { return ___instance_27; }
	inline Il2CppObject ** get_address_of_instance_27() { return &___instance_27; }
	inline void set_instance_27(Il2CppObject * value)
	{
		___instance_27 = value;
		Il2CppCodeGenWriteBarrier(&___instance_27, value);
	}

	inline static int32_t get_offset_of_contracts_28() { return static_cast<int32_t>(offsetof(ServiceHost_t1894294968, ___contracts_28)); }
	inline Dictionary_2_t1196434813 * get_contracts_28() const { return ___contracts_28; }
	inline Dictionary_2_t1196434813 ** get_address_of_contracts_28() { return &___contracts_28; }
	inline void set_contracts_28(Dictionary_2_t1196434813 * value)
	{
		___contracts_28 = value;
		Il2CppCodeGenWriteBarrier(&___contracts_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
