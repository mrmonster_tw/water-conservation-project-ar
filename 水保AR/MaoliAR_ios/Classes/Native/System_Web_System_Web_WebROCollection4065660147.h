﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Collections_Specialized_NameValueColl407452768.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.WebROCollection
struct  WebROCollection_t4065660147  : public NameValueCollection_t407452768
{
public:
	// System.Boolean System.Web.WebROCollection::got_id
	bool ___got_id_12;
	// System.Int32 System.Web.WebROCollection::id
	int32_t ___id_13;

public:
	inline static int32_t get_offset_of_got_id_12() { return static_cast<int32_t>(offsetof(WebROCollection_t4065660147, ___got_id_12)); }
	inline bool get_got_id_12() const { return ___got_id_12; }
	inline bool* get_address_of_got_id_12() { return &___got_id_12; }
	inline void set_got_id_12(bool value)
	{
		___got_id_12 = value;
	}

	inline static int32_t get_offset_of_id_13() { return static_cast<int32_t>(offsetof(WebROCollection_t4065660147, ___id_13)); }
	inline int32_t get_id_13() const { return ___id_13; }
	inline int32_t* get_address_of_id_13() { return &___id_13; }
	inline void set_id_13(int32_t value)
	{
		___id_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
