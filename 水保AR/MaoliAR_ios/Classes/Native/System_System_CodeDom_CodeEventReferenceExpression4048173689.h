﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeExpression2166265795.h"

// System.String
struct String_t;
// System.CodeDom.CodeExpression
struct CodeExpression_t2166265795;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeEventReferenceExpression
struct  CodeEventReferenceExpression_t4048173689  : public CodeExpression_t2166265795
{
public:
	// System.String System.CodeDom.CodeEventReferenceExpression::eventName
	String_t* ___eventName_1;
	// System.CodeDom.CodeExpression System.CodeDom.CodeEventReferenceExpression::targetObject
	CodeExpression_t2166265795 * ___targetObject_2;

public:
	inline static int32_t get_offset_of_eventName_1() { return static_cast<int32_t>(offsetof(CodeEventReferenceExpression_t4048173689, ___eventName_1)); }
	inline String_t* get_eventName_1() const { return ___eventName_1; }
	inline String_t** get_address_of_eventName_1() { return &___eventName_1; }
	inline void set_eventName_1(String_t* value)
	{
		___eventName_1 = value;
		Il2CppCodeGenWriteBarrier(&___eventName_1, value);
	}

	inline static int32_t get_offset_of_targetObject_2() { return static_cast<int32_t>(offsetof(CodeEventReferenceExpression_t4048173689, ___targetObject_2)); }
	inline CodeExpression_t2166265795 * get_targetObject_2() const { return ___targetObject_2; }
	inline CodeExpression_t2166265795 ** get_address_of_targetObject_2() { return &___targetObject_2; }
	inline void set_targetObject_2(CodeExpression_t2166265795 * value)
	{
		___targetObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___targetObject_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
