﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.String
struct String_t;
// System.Xml.XmlElement
struct XmlElement_t561603118;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.Xml.Manifest
struct  Manifest_t3366168394  : public Il2CppObject
{
public:
	// System.Collections.ArrayList System.Security.Cryptography.Xml.Manifest::references
	ArrayList_t2718874744 * ___references_0;
	// System.String System.Security.Cryptography.Xml.Manifest::id
	String_t* ___id_1;
	// System.Xml.XmlElement System.Security.Cryptography.Xml.Manifest::element
	XmlElement_t561603118 * ___element_2;

public:
	inline static int32_t get_offset_of_references_0() { return static_cast<int32_t>(offsetof(Manifest_t3366168394, ___references_0)); }
	inline ArrayList_t2718874744 * get_references_0() const { return ___references_0; }
	inline ArrayList_t2718874744 ** get_address_of_references_0() { return &___references_0; }
	inline void set_references_0(ArrayList_t2718874744 * value)
	{
		___references_0 = value;
		Il2CppCodeGenWriteBarrier(&___references_0, value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(Manifest_t3366168394, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier(&___id_1, value);
	}

	inline static int32_t get_offset_of_element_2() { return static_cast<int32_t>(offsetof(Manifest_t3366168394, ___element_2)); }
	inline XmlElement_t561603118 * get_element_2() const { return ___element_2; }
	inline XmlElement_t561603118 ** get_address_of_element_2() { return &___element_2; }
	inline void set_element_2(XmlElement_t561603118 * value)
	{
		___element_2 = value;
		Il2CppCodeGenWriteBarrier(&___element_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
