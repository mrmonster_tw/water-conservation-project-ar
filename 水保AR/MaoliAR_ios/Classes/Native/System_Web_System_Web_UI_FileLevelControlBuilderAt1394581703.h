﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"

// System.Web.UI.FileLevelControlBuilderAttribute
struct FileLevelControlBuilderAttribute_t1394581703;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.FileLevelControlBuilderAttribute
struct  FileLevelControlBuilderAttribute_t1394581703  : public Attribute_t861562559
{
public:
	// System.Type System.Web.UI.FileLevelControlBuilderAttribute::<BuilderType>k__BackingField
	Type_t * ___U3CBuilderTypeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CBuilderTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FileLevelControlBuilderAttribute_t1394581703, ___U3CBuilderTypeU3Ek__BackingField_1)); }
	inline Type_t * get_U3CBuilderTypeU3Ek__BackingField_1() const { return ___U3CBuilderTypeU3Ek__BackingField_1; }
	inline Type_t ** get_address_of_U3CBuilderTypeU3Ek__BackingField_1() { return &___U3CBuilderTypeU3Ek__BackingField_1; }
	inline void set_U3CBuilderTypeU3Ek__BackingField_1(Type_t * value)
	{
		___U3CBuilderTypeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CBuilderTypeU3Ek__BackingField_1, value);
	}
};

struct FileLevelControlBuilderAttribute_t1394581703_StaticFields
{
public:
	// System.Web.UI.FileLevelControlBuilderAttribute System.Web.UI.FileLevelControlBuilderAttribute::Default
	FileLevelControlBuilderAttribute_t1394581703 * ___Default_0;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(FileLevelControlBuilderAttribute_t1394581703_StaticFields, ___Default_0)); }
	inline FileLevelControlBuilderAttribute_t1394581703 * get_Default_0() const { return ___Default_0; }
	inline FileLevelControlBuilderAttribute_t1394581703 ** get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(FileLevelControlBuilderAttribute_t1394581703 * value)
	{
		___Default_0 = value;
		Il2CppCodeGenWriteBarrier(&___Default_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
