﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeDirective2939730587.h"
#include "System_System_CodeDom_CodeRegionMode2445849902.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeRegionDirective
struct  CodeRegionDirective_t2863244947  : public CodeDirective_t2939730587
{
public:
	// System.CodeDom.CodeRegionMode System.CodeDom.CodeRegionDirective::regionMode
	int32_t ___regionMode_1;
	// System.String System.CodeDom.CodeRegionDirective::regionText
	String_t* ___regionText_2;

public:
	inline static int32_t get_offset_of_regionMode_1() { return static_cast<int32_t>(offsetof(CodeRegionDirective_t2863244947, ___regionMode_1)); }
	inline int32_t get_regionMode_1() const { return ___regionMode_1; }
	inline int32_t* get_address_of_regionMode_1() { return &___regionMode_1; }
	inline void set_regionMode_1(int32_t value)
	{
		___regionMode_1 = value;
	}

	inline static int32_t get_offset_of_regionText_2() { return static_cast<int32_t>(offsetof(CodeRegionDirective_t2863244947, ___regionText_2)); }
	inline String_t* get_regionText_2() const { return ___regionText_2; }
	inline String_t** get_address_of_regionText_2() { return &___regionText_2; }
	inline void set_regionText_2(String_t* value)
	{
		___regionText_2 = value;
		Il2CppCodeGenWriteBarrier(&___regionText_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
