﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Queue
struct Queue_t3637523393;
// System.Exception
struct Exception_t1436737249;
// System.Diagnostics.PerformanceCounter
struct PerformanceCounter_t1972251440;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.QueueManager
struct  QueueManager_t3850794979  : public Il2CppObject
{
public:
	// System.Int32 System.Web.QueueManager::minFree
	int32_t ___minFree_0;
	// System.Int32 System.Web.QueueManager::minLocalFree
	int32_t ___minLocalFree_1;
	// System.Int32 System.Web.QueueManager::queueLimit
	int32_t ___queueLimit_2;
	// System.Collections.Queue System.Web.QueueManager::queue
	Queue_t3637523393 * ___queue_3;
	// System.Boolean System.Web.QueueManager::disposing
	bool ___disposing_4;
	// System.Exception System.Web.QueueManager::initialException
	Exception_t1436737249 * ___initialException_5;
	// System.Diagnostics.PerformanceCounter System.Web.QueueManager::requestsQueuedCounter
	PerformanceCounter_t1972251440 * ___requestsQueuedCounter_6;

public:
	inline static int32_t get_offset_of_minFree_0() { return static_cast<int32_t>(offsetof(QueueManager_t3850794979, ___minFree_0)); }
	inline int32_t get_minFree_0() const { return ___minFree_0; }
	inline int32_t* get_address_of_minFree_0() { return &___minFree_0; }
	inline void set_minFree_0(int32_t value)
	{
		___minFree_0 = value;
	}

	inline static int32_t get_offset_of_minLocalFree_1() { return static_cast<int32_t>(offsetof(QueueManager_t3850794979, ___minLocalFree_1)); }
	inline int32_t get_minLocalFree_1() const { return ___minLocalFree_1; }
	inline int32_t* get_address_of_minLocalFree_1() { return &___minLocalFree_1; }
	inline void set_minLocalFree_1(int32_t value)
	{
		___minLocalFree_1 = value;
	}

	inline static int32_t get_offset_of_queueLimit_2() { return static_cast<int32_t>(offsetof(QueueManager_t3850794979, ___queueLimit_2)); }
	inline int32_t get_queueLimit_2() const { return ___queueLimit_2; }
	inline int32_t* get_address_of_queueLimit_2() { return &___queueLimit_2; }
	inline void set_queueLimit_2(int32_t value)
	{
		___queueLimit_2 = value;
	}

	inline static int32_t get_offset_of_queue_3() { return static_cast<int32_t>(offsetof(QueueManager_t3850794979, ___queue_3)); }
	inline Queue_t3637523393 * get_queue_3() const { return ___queue_3; }
	inline Queue_t3637523393 ** get_address_of_queue_3() { return &___queue_3; }
	inline void set_queue_3(Queue_t3637523393 * value)
	{
		___queue_3 = value;
		Il2CppCodeGenWriteBarrier(&___queue_3, value);
	}

	inline static int32_t get_offset_of_disposing_4() { return static_cast<int32_t>(offsetof(QueueManager_t3850794979, ___disposing_4)); }
	inline bool get_disposing_4() const { return ___disposing_4; }
	inline bool* get_address_of_disposing_4() { return &___disposing_4; }
	inline void set_disposing_4(bool value)
	{
		___disposing_4 = value;
	}

	inline static int32_t get_offset_of_initialException_5() { return static_cast<int32_t>(offsetof(QueueManager_t3850794979, ___initialException_5)); }
	inline Exception_t1436737249 * get_initialException_5() const { return ___initialException_5; }
	inline Exception_t1436737249 ** get_address_of_initialException_5() { return &___initialException_5; }
	inline void set_initialException_5(Exception_t1436737249 * value)
	{
		___initialException_5 = value;
		Il2CppCodeGenWriteBarrier(&___initialException_5, value);
	}

	inline static int32_t get_offset_of_requestsQueuedCounter_6() { return static_cast<int32_t>(offsetof(QueueManager_t3850794979, ___requestsQueuedCounter_6)); }
	inline PerformanceCounter_t1972251440 * get_requestsQueuedCounter_6() const { return ___requestsQueuedCounter_6; }
	inline PerformanceCounter_t1972251440 ** get_address_of_requestsQueuedCounter_6() { return &___requestsQueuedCounter_6; }
	inline void set_requestsQueuedCounter_6(PerformanceCounter_t1972251440 * value)
	{
		___requestsQueuedCounter_6 = value;
		Il2CppCodeGenWriteBarrier(&___requestsQueuedCounter_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
