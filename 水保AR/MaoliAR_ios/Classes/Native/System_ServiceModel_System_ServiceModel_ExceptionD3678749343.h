﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.ExceptionDetail
struct ExceptionDetail_t3678749343;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.ExceptionDetail
struct  ExceptionDetail_t3678749343  : public Il2CppObject
{
public:
	// System.ServiceModel.ExceptionDetail System.ServiceModel.ExceptionDetail::<InnerException>k__BackingField
	ExceptionDetail_t3678749343 * ___U3CInnerExceptionU3Ek__BackingField_0;
	// System.String System.ServiceModel.ExceptionDetail::<Message>k__BackingField
	String_t* ___U3CMessageU3Ek__BackingField_1;
	// System.String System.ServiceModel.ExceptionDetail::<StackTrace>k__BackingField
	String_t* ___U3CStackTraceU3Ek__BackingField_2;
	// System.String System.ServiceModel.ExceptionDetail::<Type>k__BackingField
	String_t* ___U3CTypeU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CInnerExceptionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ExceptionDetail_t3678749343, ___U3CInnerExceptionU3Ek__BackingField_0)); }
	inline ExceptionDetail_t3678749343 * get_U3CInnerExceptionU3Ek__BackingField_0() const { return ___U3CInnerExceptionU3Ek__BackingField_0; }
	inline ExceptionDetail_t3678749343 ** get_address_of_U3CInnerExceptionU3Ek__BackingField_0() { return &___U3CInnerExceptionU3Ek__BackingField_0; }
	inline void set_U3CInnerExceptionU3Ek__BackingField_0(ExceptionDetail_t3678749343 * value)
	{
		___U3CInnerExceptionU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CInnerExceptionU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CMessageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ExceptionDetail_t3678749343, ___U3CMessageU3Ek__BackingField_1)); }
	inline String_t* get_U3CMessageU3Ek__BackingField_1() const { return ___U3CMessageU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CMessageU3Ek__BackingField_1() { return &___U3CMessageU3Ek__BackingField_1; }
	inline void set_U3CMessageU3Ek__BackingField_1(String_t* value)
	{
		___U3CMessageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMessageU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CStackTraceU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ExceptionDetail_t3678749343, ___U3CStackTraceU3Ek__BackingField_2)); }
	inline String_t* get_U3CStackTraceU3Ek__BackingField_2() const { return ___U3CStackTraceU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CStackTraceU3Ek__BackingField_2() { return &___U3CStackTraceU3Ek__BackingField_2; }
	inline void set_U3CStackTraceU3Ek__BackingField_2(String_t* value)
	{
		___U3CStackTraceU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CStackTraceU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ExceptionDetail_t3678749343, ___U3CTypeU3Ek__BackingField_3)); }
	inline String_t* get_U3CTypeU3Ek__BackingField_3() const { return ___U3CTypeU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CTypeU3Ek__BackingField_3() { return &___U3CTypeU3Ek__BackingField_3; }
	inline void set_U3CTypeU3Ek__BackingField_3(String_t* value)
	{
		___U3CTypeU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTypeU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
