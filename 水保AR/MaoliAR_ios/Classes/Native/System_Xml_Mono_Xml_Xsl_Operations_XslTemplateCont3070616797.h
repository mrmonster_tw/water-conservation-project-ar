﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Operations_XslCompiledElem1729269235.h"
#include "System_Xml_System_Xml_XPath_XPathNodeType3031007223.h"

// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslTemplateContent
struct  XslTemplateContent_t3070616797  : public XslCompiledElementBase_t1729269235
{
public:
	// System.Collections.ArrayList Mono.Xml.Xsl.Operations.XslTemplateContent::content
	ArrayList_t2718874744 * ___content_3;
	// System.Boolean Mono.Xml.Xsl.Operations.XslTemplateContent::hasStack
	bool ___hasStack_4;
	// System.Int32 Mono.Xml.Xsl.Operations.XslTemplateContent::stackSize
	int32_t ___stackSize_5;
	// System.Xml.XPath.XPathNodeType Mono.Xml.Xsl.Operations.XslTemplateContent::parentType
	int32_t ___parentType_6;
	// System.Boolean Mono.Xml.Xsl.Operations.XslTemplateContent::xslForEach
	bool ___xslForEach_7;

public:
	inline static int32_t get_offset_of_content_3() { return static_cast<int32_t>(offsetof(XslTemplateContent_t3070616797, ___content_3)); }
	inline ArrayList_t2718874744 * get_content_3() const { return ___content_3; }
	inline ArrayList_t2718874744 ** get_address_of_content_3() { return &___content_3; }
	inline void set_content_3(ArrayList_t2718874744 * value)
	{
		___content_3 = value;
		Il2CppCodeGenWriteBarrier(&___content_3, value);
	}

	inline static int32_t get_offset_of_hasStack_4() { return static_cast<int32_t>(offsetof(XslTemplateContent_t3070616797, ___hasStack_4)); }
	inline bool get_hasStack_4() const { return ___hasStack_4; }
	inline bool* get_address_of_hasStack_4() { return &___hasStack_4; }
	inline void set_hasStack_4(bool value)
	{
		___hasStack_4 = value;
	}

	inline static int32_t get_offset_of_stackSize_5() { return static_cast<int32_t>(offsetof(XslTemplateContent_t3070616797, ___stackSize_5)); }
	inline int32_t get_stackSize_5() const { return ___stackSize_5; }
	inline int32_t* get_address_of_stackSize_5() { return &___stackSize_5; }
	inline void set_stackSize_5(int32_t value)
	{
		___stackSize_5 = value;
	}

	inline static int32_t get_offset_of_parentType_6() { return static_cast<int32_t>(offsetof(XslTemplateContent_t3070616797, ___parentType_6)); }
	inline int32_t get_parentType_6() const { return ___parentType_6; }
	inline int32_t* get_address_of_parentType_6() { return &___parentType_6; }
	inline void set_parentType_6(int32_t value)
	{
		___parentType_6 = value;
	}

	inline static int32_t get_offset_of_xslForEach_7() { return static_cast<int32_t>(offsetof(XslTemplateContent_t3070616797, ___xslForEach_7)); }
	inline bool get_xslForEach_7() const { return ___xslForEach_7; }
	inline bool* get_address_of_xslForEach_7() { return &___xslForEach_7; }
	inline void set_xslForEach_7(bool value)
	{
		___xslForEach_7 = value;
	}
};

struct XslTemplateContent_t3070616797_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.Operations.XslTemplateContent::<>f__switch$mapC
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24mapC_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.Operations.XslTemplateContent::<>f__switch$mapD
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24mapD_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapC_8() { return static_cast<int32_t>(offsetof(XslTemplateContent_t3070616797_StaticFields, ___U3CU3Ef__switchU24mapC_8)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24mapC_8() const { return ___U3CU3Ef__switchU24mapC_8; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24mapC_8() { return &___U3CU3Ef__switchU24mapC_8; }
	inline void set_U3CU3Ef__switchU24mapC_8(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24mapC_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapC_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapD_9() { return static_cast<int32_t>(offsetof(XslTemplateContent_t3070616797_StaticFields, ___U3CU3Ef__switchU24mapD_9)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24mapD_9() const { return ___U3CU3Ef__switchU24mapD_9; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24mapD_9() { return &___U3CU3Ef__switchU24mapD_9; }
	inline void set_U3CU3Ef__switchU24mapD_9(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24mapD_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapD_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
