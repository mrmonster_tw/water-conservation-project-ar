﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"
#include "System_ServiceModel_System_ServiceModel_SessionMode513474935.h"
#include "System_System_Net_Security_ProtectionLevel2559578148.h"

// System.Type
struct Type_t;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.ServiceContractAttribute
struct  ServiceContractAttribute_t4131169907  : public Attribute_t861562559
{
public:
	// System.Type System.ServiceModel.ServiceContractAttribute::callback_contract
	Type_t * ___callback_contract_0;
	// System.String System.ServiceModel.ServiceContractAttribute::name
	String_t* ___name_1;
	// System.String System.ServiceModel.ServiceContractAttribute::ns
	String_t* ___ns_2;
	// System.ServiceModel.SessionMode System.ServiceModel.ServiceContractAttribute::session
	int32_t ___session_3;
	// System.Net.Security.ProtectionLevel System.ServiceModel.ServiceContractAttribute::protection_level
	int32_t ___protection_level_4;
	// System.Boolean System.ServiceModel.ServiceContractAttribute::has_protection_level
	bool ___has_protection_level_5;
	// System.String System.ServiceModel.ServiceContractAttribute::_configurationName
	String_t* ____configurationName_6;

public:
	inline static int32_t get_offset_of_callback_contract_0() { return static_cast<int32_t>(offsetof(ServiceContractAttribute_t4131169907, ___callback_contract_0)); }
	inline Type_t * get_callback_contract_0() const { return ___callback_contract_0; }
	inline Type_t ** get_address_of_callback_contract_0() { return &___callback_contract_0; }
	inline void set_callback_contract_0(Type_t * value)
	{
		___callback_contract_0 = value;
		Il2CppCodeGenWriteBarrier(&___callback_contract_0, value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(ServiceContractAttribute_t4131169907, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_ns_2() { return static_cast<int32_t>(offsetof(ServiceContractAttribute_t4131169907, ___ns_2)); }
	inline String_t* get_ns_2() const { return ___ns_2; }
	inline String_t** get_address_of_ns_2() { return &___ns_2; }
	inline void set_ns_2(String_t* value)
	{
		___ns_2 = value;
		Il2CppCodeGenWriteBarrier(&___ns_2, value);
	}

	inline static int32_t get_offset_of_session_3() { return static_cast<int32_t>(offsetof(ServiceContractAttribute_t4131169907, ___session_3)); }
	inline int32_t get_session_3() const { return ___session_3; }
	inline int32_t* get_address_of_session_3() { return &___session_3; }
	inline void set_session_3(int32_t value)
	{
		___session_3 = value;
	}

	inline static int32_t get_offset_of_protection_level_4() { return static_cast<int32_t>(offsetof(ServiceContractAttribute_t4131169907, ___protection_level_4)); }
	inline int32_t get_protection_level_4() const { return ___protection_level_4; }
	inline int32_t* get_address_of_protection_level_4() { return &___protection_level_4; }
	inline void set_protection_level_4(int32_t value)
	{
		___protection_level_4 = value;
	}

	inline static int32_t get_offset_of_has_protection_level_5() { return static_cast<int32_t>(offsetof(ServiceContractAttribute_t4131169907, ___has_protection_level_5)); }
	inline bool get_has_protection_level_5() const { return ___has_protection_level_5; }
	inline bool* get_address_of_has_protection_level_5() { return &___has_protection_level_5; }
	inline void set_has_protection_level_5(bool value)
	{
		___has_protection_level_5 = value;
	}

	inline static int32_t get_offset_of__configurationName_6() { return static_cast<int32_t>(offsetof(ServiceContractAttribute_t4131169907, ____configurationName_6)); }
	inline String_t* get__configurationName_6() const { return ____configurationName_6; }
	inline String_t** get_address_of__configurationName_6() { return &____configurationName_6; }
	inline void set__configurationName_6(String_t* value)
	{
		____configurationName_6 = value;
		Il2CppCodeGenWriteBarrier(&____configurationName_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
