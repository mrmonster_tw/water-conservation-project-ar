﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2404315739.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat962350765.h"

// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Texture3D
struct Texture3D_t1884131049;
// System.String
struct String_t;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// System.Single[]
struct SingleU5BU5D_t1444911251;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TonemappingLog
struct  TonemappingLog_t4031249120  : public PostEffectsBase_t2404315739
{
public:
	// System.Boolean TonemappingLog::enableAdaptive
	bool ___enableAdaptive_5;
	// System.Boolean TonemappingLog::debugClamp
	bool ___debugClamp_6;
	// System.Single TonemappingLog::middleGrey
	float ___middleGrey_7;
	// System.Single TonemappingLog::adaptionSpeed
	float ___adaptionSpeed_8;
	// System.Single TonemappingLog::adaptiveMin
	float ___adaptiveMin_9;
	// System.Single TonemappingLog::adaptiveMax
	float ___adaptiveMax_10;
	// System.Single TonemappingLog::logMid
	float ___logMid_11;
	// System.Single TonemappingLog::linearMid
	float ___linearMid_12;
	// System.Single TonemappingLog::dynamicRange
	float ___dynamicRange_13;
	// UnityEngine.AnimationCurve TonemappingLog::remapCurve
	AnimationCurve_t3046754366 * ___remapCurve_14;
	// UnityEngine.Texture2D TonemappingLog::curveTex
	Texture2D_t3840446185 * ___curveTex_15;
	// UnityEngine.Texture2D TonemappingLog::lutTex
	Texture2D_t3840446185 * ___lutTex_16;
	// UnityEngine.Texture3D TonemappingLog::converted3DLut
	Texture3D_t1884131049 * ___converted3DLut_17;
	// System.String TonemappingLog::lutTexName
	String_t* ___lutTexName_18;
	// UnityEngine.Shader TonemappingLog::tonemapperLog
	Shader_t4151988712 * ___tonemapperLog_19;
	// System.Boolean TonemappingLog::validRenderTextureFormat
	bool ___validRenderTextureFormat_20;
	// UnityEngine.Material TonemappingLog::tonemapMaterial
	Material_t340375123 * ___tonemapMaterial_21;
	// UnityEngine.RenderTexture TonemappingLog::rt
	RenderTexture_t2108887433 * ___rt_22;
	// UnityEngine.RenderTextureFormat TonemappingLog::rtFormat
	int32_t ___rtFormat_23;
	// System.Int32 TonemappingLog::curveLen
	int32_t ___curveLen_24;
	// System.Single[] TonemappingLog::curveData
	SingleU5BU5D_t1444911251* ___curveData_25;

public:
	inline static int32_t get_offset_of_enableAdaptive_5() { return static_cast<int32_t>(offsetof(TonemappingLog_t4031249120, ___enableAdaptive_5)); }
	inline bool get_enableAdaptive_5() const { return ___enableAdaptive_5; }
	inline bool* get_address_of_enableAdaptive_5() { return &___enableAdaptive_5; }
	inline void set_enableAdaptive_5(bool value)
	{
		___enableAdaptive_5 = value;
	}

	inline static int32_t get_offset_of_debugClamp_6() { return static_cast<int32_t>(offsetof(TonemappingLog_t4031249120, ___debugClamp_6)); }
	inline bool get_debugClamp_6() const { return ___debugClamp_6; }
	inline bool* get_address_of_debugClamp_6() { return &___debugClamp_6; }
	inline void set_debugClamp_6(bool value)
	{
		___debugClamp_6 = value;
	}

	inline static int32_t get_offset_of_middleGrey_7() { return static_cast<int32_t>(offsetof(TonemappingLog_t4031249120, ___middleGrey_7)); }
	inline float get_middleGrey_7() const { return ___middleGrey_7; }
	inline float* get_address_of_middleGrey_7() { return &___middleGrey_7; }
	inline void set_middleGrey_7(float value)
	{
		___middleGrey_7 = value;
	}

	inline static int32_t get_offset_of_adaptionSpeed_8() { return static_cast<int32_t>(offsetof(TonemappingLog_t4031249120, ___adaptionSpeed_8)); }
	inline float get_adaptionSpeed_8() const { return ___adaptionSpeed_8; }
	inline float* get_address_of_adaptionSpeed_8() { return &___adaptionSpeed_8; }
	inline void set_adaptionSpeed_8(float value)
	{
		___adaptionSpeed_8 = value;
	}

	inline static int32_t get_offset_of_adaptiveMin_9() { return static_cast<int32_t>(offsetof(TonemappingLog_t4031249120, ___adaptiveMin_9)); }
	inline float get_adaptiveMin_9() const { return ___adaptiveMin_9; }
	inline float* get_address_of_adaptiveMin_9() { return &___adaptiveMin_9; }
	inline void set_adaptiveMin_9(float value)
	{
		___adaptiveMin_9 = value;
	}

	inline static int32_t get_offset_of_adaptiveMax_10() { return static_cast<int32_t>(offsetof(TonemappingLog_t4031249120, ___adaptiveMax_10)); }
	inline float get_adaptiveMax_10() const { return ___adaptiveMax_10; }
	inline float* get_address_of_adaptiveMax_10() { return &___adaptiveMax_10; }
	inline void set_adaptiveMax_10(float value)
	{
		___adaptiveMax_10 = value;
	}

	inline static int32_t get_offset_of_logMid_11() { return static_cast<int32_t>(offsetof(TonemappingLog_t4031249120, ___logMid_11)); }
	inline float get_logMid_11() const { return ___logMid_11; }
	inline float* get_address_of_logMid_11() { return &___logMid_11; }
	inline void set_logMid_11(float value)
	{
		___logMid_11 = value;
	}

	inline static int32_t get_offset_of_linearMid_12() { return static_cast<int32_t>(offsetof(TonemappingLog_t4031249120, ___linearMid_12)); }
	inline float get_linearMid_12() const { return ___linearMid_12; }
	inline float* get_address_of_linearMid_12() { return &___linearMid_12; }
	inline void set_linearMid_12(float value)
	{
		___linearMid_12 = value;
	}

	inline static int32_t get_offset_of_dynamicRange_13() { return static_cast<int32_t>(offsetof(TonemappingLog_t4031249120, ___dynamicRange_13)); }
	inline float get_dynamicRange_13() const { return ___dynamicRange_13; }
	inline float* get_address_of_dynamicRange_13() { return &___dynamicRange_13; }
	inline void set_dynamicRange_13(float value)
	{
		___dynamicRange_13 = value;
	}

	inline static int32_t get_offset_of_remapCurve_14() { return static_cast<int32_t>(offsetof(TonemappingLog_t4031249120, ___remapCurve_14)); }
	inline AnimationCurve_t3046754366 * get_remapCurve_14() const { return ___remapCurve_14; }
	inline AnimationCurve_t3046754366 ** get_address_of_remapCurve_14() { return &___remapCurve_14; }
	inline void set_remapCurve_14(AnimationCurve_t3046754366 * value)
	{
		___remapCurve_14 = value;
		Il2CppCodeGenWriteBarrier(&___remapCurve_14, value);
	}

	inline static int32_t get_offset_of_curveTex_15() { return static_cast<int32_t>(offsetof(TonemappingLog_t4031249120, ___curveTex_15)); }
	inline Texture2D_t3840446185 * get_curveTex_15() const { return ___curveTex_15; }
	inline Texture2D_t3840446185 ** get_address_of_curveTex_15() { return &___curveTex_15; }
	inline void set_curveTex_15(Texture2D_t3840446185 * value)
	{
		___curveTex_15 = value;
		Il2CppCodeGenWriteBarrier(&___curveTex_15, value);
	}

	inline static int32_t get_offset_of_lutTex_16() { return static_cast<int32_t>(offsetof(TonemappingLog_t4031249120, ___lutTex_16)); }
	inline Texture2D_t3840446185 * get_lutTex_16() const { return ___lutTex_16; }
	inline Texture2D_t3840446185 ** get_address_of_lutTex_16() { return &___lutTex_16; }
	inline void set_lutTex_16(Texture2D_t3840446185 * value)
	{
		___lutTex_16 = value;
		Il2CppCodeGenWriteBarrier(&___lutTex_16, value);
	}

	inline static int32_t get_offset_of_converted3DLut_17() { return static_cast<int32_t>(offsetof(TonemappingLog_t4031249120, ___converted3DLut_17)); }
	inline Texture3D_t1884131049 * get_converted3DLut_17() const { return ___converted3DLut_17; }
	inline Texture3D_t1884131049 ** get_address_of_converted3DLut_17() { return &___converted3DLut_17; }
	inline void set_converted3DLut_17(Texture3D_t1884131049 * value)
	{
		___converted3DLut_17 = value;
		Il2CppCodeGenWriteBarrier(&___converted3DLut_17, value);
	}

	inline static int32_t get_offset_of_lutTexName_18() { return static_cast<int32_t>(offsetof(TonemappingLog_t4031249120, ___lutTexName_18)); }
	inline String_t* get_lutTexName_18() const { return ___lutTexName_18; }
	inline String_t** get_address_of_lutTexName_18() { return &___lutTexName_18; }
	inline void set_lutTexName_18(String_t* value)
	{
		___lutTexName_18 = value;
		Il2CppCodeGenWriteBarrier(&___lutTexName_18, value);
	}

	inline static int32_t get_offset_of_tonemapperLog_19() { return static_cast<int32_t>(offsetof(TonemappingLog_t4031249120, ___tonemapperLog_19)); }
	inline Shader_t4151988712 * get_tonemapperLog_19() const { return ___tonemapperLog_19; }
	inline Shader_t4151988712 ** get_address_of_tonemapperLog_19() { return &___tonemapperLog_19; }
	inline void set_tonemapperLog_19(Shader_t4151988712 * value)
	{
		___tonemapperLog_19 = value;
		Il2CppCodeGenWriteBarrier(&___tonemapperLog_19, value);
	}

	inline static int32_t get_offset_of_validRenderTextureFormat_20() { return static_cast<int32_t>(offsetof(TonemappingLog_t4031249120, ___validRenderTextureFormat_20)); }
	inline bool get_validRenderTextureFormat_20() const { return ___validRenderTextureFormat_20; }
	inline bool* get_address_of_validRenderTextureFormat_20() { return &___validRenderTextureFormat_20; }
	inline void set_validRenderTextureFormat_20(bool value)
	{
		___validRenderTextureFormat_20 = value;
	}

	inline static int32_t get_offset_of_tonemapMaterial_21() { return static_cast<int32_t>(offsetof(TonemappingLog_t4031249120, ___tonemapMaterial_21)); }
	inline Material_t340375123 * get_tonemapMaterial_21() const { return ___tonemapMaterial_21; }
	inline Material_t340375123 ** get_address_of_tonemapMaterial_21() { return &___tonemapMaterial_21; }
	inline void set_tonemapMaterial_21(Material_t340375123 * value)
	{
		___tonemapMaterial_21 = value;
		Il2CppCodeGenWriteBarrier(&___tonemapMaterial_21, value);
	}

	inline static int32_t get_offset_of_rt_22() { return static_cast<int32_t>(offsetof(TonemappingLog_t4031249120, ___rt_22)); }
	inline RenderTexture_t2108887433 * get_rt_22() const { return ___rt_22; }
	inline RenderTexture_t2108887433 ** get_address_of_rt_22() { return &___rt_22; }
	inline void set_rt_22(RenderTexture_t2108887433 * value)
	{
		___rt_22 = value;
		Il2CppCodeGenWriteBarrier(&___rt_22, value);
	}

	inline static int32_t get_offset_of_rtFormat_23() { return static_cast<int32_t>(offsetof(TonemappingLog_t4031249120, ___rtFormat_23)); }
	inline int32_t get_rtFormat_23() const { return ___rtFormat_23; }
	inline int32_t* get_address_of_rtFormat_23() { return &___rtFormat_23; }
	inline void set_rtFormat_23(int32_t value)
	{
		___rtFormat_23 = value;
	}

	inline static int32_t get_offset_of_curveLen_24() { return static_cast<int32_t>(offsetof(TonemappingLog_t4031249120, ___curveLen_24)); }
	inline int32_t get_curveLen_24() const { return ___curveLen_24; }
	inline int32_t* get_address_of_curveLen_24() { return &___curveLen_24; }
	inline void set_curveLen_24(int32_t value)
	{
		___curveLen_24 = value;
	}

	inline static int32_t get_offset_of_curveData_25() { return static_cast<int32_t>(offsetof(TonemappingLog_t4031249120, ___curveData_25)); }
	inline SingleU5BU5D_t1444911251* get_curveData_25() const { return ___curveData_25; }
	inline SingleU5BU5D_t1444911251** get_address_of_curveData_25() { return &___curveData_25; }
	inline void set_curveData_25(SingleU5BU5D_t1444911251* value)
	{
		___curveData_25 = value;
		Il2CppCodeGenWriteBarrier(&___curveData_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
