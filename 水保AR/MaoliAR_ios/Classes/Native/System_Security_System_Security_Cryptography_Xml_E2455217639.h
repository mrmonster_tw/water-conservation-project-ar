﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_Security_Cryptography_CipherMode84635067.h"
#include "mscorlib_System_Security_Cryptography_PaddingMode2546806710.h"

// System.Text.Encoding
struct Encoding_t1523322056;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Xml.XmlDocument
struct XmlDocument_t2837193595;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.Xml.EncryptedXml
struct  EncryptedXml_t2455217639  : public Il2CppObject
{
public:
	// System.Text.Encoding System.Security.Cryptography.Xml.EncryptedXml::encoding
	Encoding_t1523322056 * ___encoding_0;
	// System.Collections.Hashtable System.Security.Cryptography.Xml.EncryptedXml::keyNameMapping
	Hashtable_t1853889766 * ___keyNameMapping_1;
	// System.Security.Cryptography.CipherMode System.Security.Cryptography.Xml.EncryptedXml::mode
	int32_t ___mode_2;
	// System.Security.Cryptography.PaddingMode System.Security.Cryptography.Xml.EncryptedXml::padding
	int32_t ___padding_3;
	// System.Xml.XmlDocument System.Security.Cryptography.Xml.EncryptedXml::document
	XmlDocument_t2837193595 * ___document_4;

public:
	inline static int32_t get_offset_of_encoding_0() { return static_cast<int32_t>(offsetof(EncryptedXml_t2455217639, ___encoding_0)); }
	inline Encoding_t1523322056 * get_encoding_0() const { return ___encoding_0; }
	inline Encoding_t1523322056 ** get_address_of_encoding_0() { return &___encoding_0; }
	inline void set_encoding_0(Encoding_t1523322056 * value)
	{
		___encoding_0 = value;
		Il2CppCodeGenWriteBarrier(&___encoding_0, value);
	}

	inline static int32_t get_offset_of_keyNameMapping_1() { return static_cast<int32_t>(offsetof(EncryptedXml_t2455217639, ___keyNameMapping_1)); }
	inline Hashtable_t1853889766 * get_keyNameMapping_1() const { return ___keyNameMapping_1; }
	inline Hashtable_t1853889766 ** get_address_of_keyNameMapping_1() { return &___keyNameMapping_1; }
	inline void set_keyNameMapping_1(Hashtable_t1853889766 * value)
	{
		___keyNameMapping_1 = value;
		Il2CppCodeGenWriteBarrier(&___keyNameMapping_1, value);
	}

	inline static int32_t get_offset_of_mode_2() { return static_cast<int32_t>(offsetof(EncryptedXml_t2455217639, ___mode_2)); }
	inline int32_t get_mode_2() const { return ___mode_2; }
	inline int32_t* get_address_of_mode_2() { return &___mode_2; }
	inline void set_mode_2(int32_t value)
	{
		___mode_2 = value;
	}

	inline static int32_t get_offset_of_padding_3() { return static_cast<int32_t>(offsetof(EncryptedXml_t2455217639, ___padding_3)); }
	inline int32_t get_padding_3() const { return ___padding_3; }
	inline int32_t* get_address_of_padding_3() { return &___padding_3; }
	inline void set_padding_3(int32_t value)
	{
		___padding_3 = value;
	}

	inline static int32_t get_offset_of_document_4() { return static_cast<int32_t>(offsetof(EncryptedXml_t2455217639, ___document_4)); }
	inline XmlDocument_t2837193595 * get_document_4() const { return ___document_4; }
	inline XmlDocument_t2837193595 ** get_address_of_document_4() { return &___document_4; }
	inline void set_document_4(XmlDocument_t2837193595 * value)
	{
		___document_4 = value;
		Il2CppCodeGenWriteBarrier(&___document_4, value);
	}
};

struct EncryptedXml_t2455217639_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.Xml.EncryptedXml::<>f__switch$map8
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map8_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.Xml.EncryptedXml::<>f__switch$map9
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map9_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map8_5() { return static_cast<int32_t>(offsetof(EncryptedXml_t2455217639_StaticFields, ___U3CU3Ef__switchU24map8_5)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map8_5() const { return ___U3CU3Ef__switchU24map8_5; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map8_5() { return &___U3CU3Ef__switchU24map8_5; }
	inline void set_U3CU3Ef__switchU24map8_5(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map8_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map8_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map9_6() { return static_cast<int32_t>(offsetof(EncryptedXml_t2455217639_StaticFields, ___U3CU3Ef__switchU24map9_6)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map9_6() const { return ___U3CU3Ef__switchU24map9_6; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map9_6() { return &___U3CU3Ef__switchU24map9_6; }
	inline void set_U3CU3Ef__switchU24map9_6(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map9_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map9_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
