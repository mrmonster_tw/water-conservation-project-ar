﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// System.Collections.Generic.List`1<PresetSelector/Preset>
struct List_1_t149215078;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PresetSelector
struct  PresetSelector_t2134829519  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<PresetSelector/Preset> PresetSelector::presets
	List_1_t149215078 * ___presets_2;
	// System.Int32 PresetSelector::h
	int32_t ___h_3;

public:
	inline static int32_t get_offset_of_presets_2() { return static_cast<int32_t>(offsetof(PresetSelector_t2134829519, ___presets_2)); }
	inline List_1_t149215078 * get_presets_2() const { return ___presets_2; }
	inline List_1_t149215078 ** get_address_of_presets_2() { return &___presets_2; }
	inline void set_presets_2(List_1_t149215078 * value)
	{
		___presets_2 = value;
		Il2CppCodeGenWriteBarrier(&___presets_2, value);
	}

	inline static int32_t get_offset_of_h_3() { return static_cast<int32_t>(offsetof(PresetSelector_t2134829519, ___h_3)); }
	inline int32_t get_h_3() const { return ___h_3; }
	inline int32_t* get_address_of_h_3() { return &___h_3; }
	inline void set_h_3(int32_t value)
	{
		___h_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
