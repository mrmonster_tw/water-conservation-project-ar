﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_Compiler_CodeGenerator1606279797.h"

// System.String[]
struct StringU5BU5D_t1281789340;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.VisualBasic.VBCodeGenerator
struct  VBCodeGenerator_t186779574  : public CodeGenerator_t1606279797
{
public:
	// System.String[] Microsoft.VisualBasic.VBCodeGenerator::Keywords
	StringU5BU5D_t1281789340* ___Keywords_6;

public:
	inline static int32_t get_offset_of_Keywords_6() { return static_cast<int32_t>(offsetof(VBCodeGenerator_t186779574, ___Keywords_6)); }
	inline StringU5BU5D_t1281789340* get_Keywords_6() const { return ___Keywords_6; }
	inline StringU5BU5D_t1281789340** get_address_of_Keywords_6() { return &___Keywords_6; }
	inline void set_Keywords_6(StringU5BU5D_t1281789340* value)
	{
		___Keywords_6 = value;
		Il2CppCodeGenWriteBarrier(&___Keywords_6, value);
	}
};

struct VBCodeGenerator_t186779574_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Microsoft.VisualBasic.VBCodeGenerator::<>f__switch$map2
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map2_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2_7() { return static_cast<int32_t>(offsetof(VBCodeGenerator_t186779574_StaticFields, ___U3CU3Ef__switchU24map2_7)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map2_7() const { return ___U3CU3Ef__switchU24map2_7; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map2_7() { return &___U3CU3Ef__switchU24map2_7; }
	inline void set_U3CU3Ef__switchU24map2_7(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map2_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map2_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
