﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.HttpContext
struct HttpContext_t1969259010;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t1333520283;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.SessionState.SessionStateModule/CallbackState
struct  CallbackState_t123990870  : public Il2CppObject
{
public:
	// System.Web.HttpContext System.Web.SessionState.SessionStateModule/CallbackState::Context
	HttpContext_t1969259010 * ___Context_0;
	// System.Threading.AutoResetEvent System.Web.SessionState.SessionStateModule/CallbackState::AutoEvent
	AutoResetEvent_t1333520283 * ___AutoEvent_1;
	// System.String System.Web.SessionState.SessionStateModule/CallbackState::SessionId
	String_t* ___SessionId_2;
	// System.Boolean System.Web.SessionState.SessionStateModule/CallbackState::IsReadOnly
	bool ___IsReadOnly_3;

public:
	inline static int32_t get_offset_of_Context_0() { return static_cast<int32_t>(offsetof(CallbackState_t123990870, ___Context_0)); }
	inline HttpContext_t1969259010 * get_Context_0() const { return ___Context_0; }
	inline HttpContext_t1969259010 ** get_address_of_Context_0() { return &___Context_0; }
	inline void set_Context_0(HttpContext_t1969259010 * value)
	{
		___Context_0 = value;
		Il2CppCodeGenWriteBarrier(&___Context_0, value);
	}

	inline static int32_t get_offset_of_AutoEvent_1() { return static_cast<int32_t>(offsetof(CallbackState_t123990870, ___AutoEvent_1)); }
	inline AutoResetEvent_t1333520283 * get_AutoEvent_1() const { return ___AutoEvent_1; }
	inline AutoResetEvent_t1333520283 ** get_address_of_AutoEvent_1() { return &___AutoEvent_1; }
	inline void set_AutoEvent_1(AutoResetEvent_t1333520283 * value)
	{
		___AutoEvent_1 = value;
		Il2CppCodeGenWriteBarrier(&___AutoEvent_1, value);
	}

	inline static int32_t get_offset_of_SessionId_2() { return static_cast<int32_t>(offsetof(CallbackState_t123990870, ___SessionId_2)); }
	inline String_t* get_SessionId_2() const { return ___SessionId_2; }
	inline String_t** get_address_of_SessionId_2() { return &___SessionId_2; }
	inline void set_SessionId_2(String_t* value)
	{
		___SessionId_2 = value;
		Il2CppCodeGenWriteBarrier(&___SessionId_2, value);
	}

	inline static int32_t get_offset_of_IsReadOnly_3() { return static_cast<int32_t>(offsetof(CallbackState_t123990870, ___IsReadOnly_3)); }
	inline bool get_IsReadOnly_3() const { return ___IsReadOnly_3; }
	inline bool* get_address_of_IsReadOnly_3() { return &___IsReadOnly_3; }
	inline void set_IsReadOnly_3(bool value)
	{
		___IsReadOnly_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
