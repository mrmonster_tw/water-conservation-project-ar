﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Hashtable
struct Hashtable_t1853889766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpCacheVaryByHeaders
struct  HttpCacheVaryByHeaders_t2217784971  : public Il2CppObject
{
public:
	// System.Collections.Hashtable System.Web.HttpCacheVaryByHeaders::fields
	Hashtable_t1853889766 * ___fields_0;

public:
	inline static int32_t get_offset_of_fields_0() { return static_cast<int32_t>(offsetof(HttpCacheVaryByHeaders_t2217784971, ___fields_0)); }
	inline Hashtable_t1853889766 * get_fields_0() const { return ___fields_0; }
	inline Hashtable_t1853889766 ** get_address_of_fields_0() { return &___fields_0; }
	inline void set_fields_0(Hashtable_t1853889766 * value)
	{
		___fields_0 = value;
		Il2CppCodeGenWriteBarrier(&___fields_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
