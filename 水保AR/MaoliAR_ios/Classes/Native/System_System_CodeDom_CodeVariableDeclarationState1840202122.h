﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeStatement371410868.h"

// System.CodeDom.CodeExpression
struct CodeExpression_t2166265795;
// System.CodeDom.CodeTypeReference
struct CodeTypeReference_t3809997434;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeVariableDeclarationStatement
struct  CodeVariableDeclarationStatement_t1840202122  : public CodeStatement_t371410868
{
public:
	// System.CodeDom.CodeExpression System.CodeDom.CodeVariableDeclarationStatement::initExpression
	CodeExpression_t2166265795 * ___initExpression_4;
	// System.CodeDom.CodeTypeReference System.CodeDom.CodeVariableDeclarationStatement::type
	CodeTypeReference_t3809997434 * ___type_5;
	// System.String System.CodeDom.CodeVariableDeclarationStatement::name
	String_t* ___name_6;

public:
	inline static int32_t get_offset_of_initExpression_4() { return static_cast<int32_t>(offsetof(CodeVariableDeclarationStatement_t1840202122, ___initExpression_4)); }
	inline CodeExpression_t2166265795 * get_initExpression_4() const { return ___initExpression_4; }
	inline CodeExpression_t2166265795 ** get_address_of_initExpression_4() { return &___initExpression_4; }
	inline void set_initExpression_4(CodeExpression_t2166265795 * value)
	{
		___initExpression_4 = value;
		Il2CppCodeGenWriteBarrier(&___initExpression_4, value);
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(CodeVariableDeclarationStatement_t1840202122, ___type_5)); }
	inline CodeTypeReference_t3809997434 * get_type_5() const { return ___type_5; }
	inline CodeTypeReference_t3809997434 ** get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(CodeTypeReference_t3809997434 * value)
	{
		___type_5 = value;
		Il2CppCodeGenWriteBarrier(&___type_5, value);
	}

	inline static int32_t get_offset_of_name_6() { return static_cast<int32_t>(offsetof(CodeVariableDeclarationStatement_t1840202122, ___name_6)); }
	inline String_t* get_name_6() const { return ___name_6; }
	inline String_t** get_address_of_name_6() { return &___name_6; }
	inline void set_name_6(String_t* value)
	{
		___name_6 = value;
		Il2CppCodeGenWriteBarrier(&___name_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
