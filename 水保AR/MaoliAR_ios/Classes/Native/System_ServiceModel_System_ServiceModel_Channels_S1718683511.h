﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.ServiceModel.ServiceHostBase
struct ServiceHostBase_t3741910535;
// System.ServiceModel.Channels.WcfListenerInfoCollection
struct WcfListenerInfoCollection_t4171796405;
// System.Collections.Generic.Dictionary`2<System.Web.HttpContext,System.Threading.AutoResetEvent>
struct Dictionary_2_t2685475617;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t1333520283;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.SvcHttpHandler
struct  SvcHttpHandler_t1718683511  : public Il2CppObject
{
public:
	// System.Type System.ServiceModel.Channels.SvcHttpHandler::type
	Type_t * ___type_1;
	// System.Type System.ServiceModel.Channels.SvcHttpHandler::factory_type
	Type_t * ___factory_type_2;
	// System.String System.ServiceModel.Channels.SvcHttpHandler::path
	String_t* ___path_3;
	// System.ServiceModel.ServiceHostBase System.ServiceModel.Channels.SvcHttpHandler::host
	ServiceHostBase_t3741910535 * ___host_4;
	// System.ServiceModel.Channels.WcfListenerInfoCollection System.ServiceModel.Channels.SvcHttpHandler::listeners
	WcfListenerInfoCollection_t4171796405 * ___listeners_5;
	// System.Collections.Generic.Dictionary`2<System.Web.HttpContext,System.Threading.AutoResetEvent> System.ServiceModel.Channels.SvcHttpHandler::wcf_wait_handles
	Dictionary_2_t2685475617 * ___wcf_wait_handles_6;
	// System.Threading.AutoResetEvent System.ServiceModel.Channels.SvcHttpHandler::wait_for_request_handle
	AutoResetEvent_t1333520283 * ___wait_for_request_handle_7;
	// System.Int32 System.ServiceModel.Channels.SvcHttpHandler::close_state
	int32_t ___close_state_8;

public:
	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(SvcHttpHandler_t1718683511, ___type_1)); }
	inline Type_t * get_type_1() const { return ___type_1; }
	inline Type_t ** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(Type_t * value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier(&___type_1, value);
	}

	inline static int32_t get_offset_of_factory_type_2() { return static_cast<int32_t>(offsetof(SvcHttpHandler_t1718683511, ___factory_type_2)); }
	inline Type_t * get_factory_type_2() const { return ___factory_type_2; }
	inline Type_t ** get_address_of_factory_type_2() { return &___factory_type_2; }
	inline void set_factory_type_2(Type_t * value)
	{
		___factory_type_2 = value;
		Il2CppCodeGenWriteBarrier(&___factory_type_2, value);
	}

	inline static int32_t get_offset_of_path_3() { return static_cast<int32_t>(offsetof(SvcHttpHandler_t1718683511, ___path_3)); }
	inline String_t* get_path_3() const { return ___path_3; }
	inline String_t** get_address_of_path_3() { return &___path_3; }
	inline void set_path_3(String_t* value)
	{
		___path_3 = value;
		Il2CppCodeGenWriteBarrier(&___path_3, value);
	}

	inline static int32_t get_offset_of_host_4() { return static_cast<int32_t>(offsetof(SvcHttpHandler_t1718683511, ___host_4)); }
	inline ServiceHostBase_t3741910535 * get_host_4() const { return ___host_4; }
	inline ServiceHostBase_t3741910535 ** get_address_of_host_4() { return &___host_4; }
	inline void set_host_4(ServiceHostBase_t3741910535 * value)
	{
		___host_4 = value;
		Il2CppCodeGenWriteBarrier(&___host_4, value);
	}

	inline static int32_t get_offset_of_listeners_5() { return static_cast<int32_t>(offsetof(SvcHttpHandler_t1718683511, ___listeners_5)); }
	inline WcfListenerInfoCollection_t4171796405 * get_listeners_5() const { return ___listeners_5; }
	inline WcfListenerInfoCollection_t4171796405 ** get_address_of_listeners_5() { return &___listeners_5; }
	inline void set_listeners_5(WcfListenerInfoCollection_t4171796405 * value)
	{
		___listeners_5 = value;
		Il2CppCodeGenWriteBarrier(&___listeners_5, value);
	}

	inline static int32_t get_offset_of_wcf_wait_handles_6() { return static_cast<int32_t>(offsetof(SvcHttpHandler_t1718683511, ___wcf_wait_handles_6)); }
	inline Dictionary_2_t2685475617 * get_wcf_wait_handles_6() const { return ___wcf_wait_handles_6; }
	inline Dictionary_2_t2685475617 ** get_address_of_wcf_wait_handles_6() { return &___wcf_wait_handles_6; }
	inline void set_wcf_wait_handles_6(Dictionary_2_t2685475617 * value)
	{
		___wcf_wait_handles_6 = value;
		Il2CppCodeGenWriteBarrier(&___wcf_wait_handles_6, value);
	}

	inline static int32_t get_offset_of_wait_for_request_handle_7() { return static_cast<int32_t>(offsetof(SvcHttpHandler_t1718683511, ___wait_for_request_handle_7)); }
	inline AutoResetEvent_t1333520283 * get_wait_for_request_handle_7() const { return ___wait_for_request_handle_7; }
	inline AutoResetEvent_t1333520283 ** get_address_of_wait_for_request_handle_7() { return &___wait_for_request_handle_7; }
	inline void set_wait_for_request_handle_7(AutoResetEvent_t1333520283 * value)
	{
		___wait_for_request_handle_7 = value;
		Il2CppCodeGenWriteBarrier(&___wait_for_request_handle_7, value);
	}

	inline static int32_t get_offset_of_close_state_8() { return static_cast<int32_t>(offsetof(SvcHttpHandler_t1718683511, ___close_state_8)); }
	inline int32_t get_close_state_8() const { return ___close_state_8; }
	inline int32_t* get_address_of_close_state_8() { return &___close_state_8; }
	inline void set_close_state_8(int32_t value)
	{
		___close_state_8 = value;
	}
};

struct SvcHttpHandler_t1718683511_StaticFields
{
public:
	// System.Object System.ServiceModel.Channels.SvcHttpHandler::type_lock
	Il2CppObject * ___type_lock_0;

public:
	inline static int32_t get_offset_of_type_lock_0() { return static_cast<int32_t>(offsetof(SvcHttpHandler_t1718683511_StaticFields, ___type_lock_0)); }
	inline Il2CppObject * get_type_lock_0() const { return ___type_lock_0; }
	inline Il2CppObject ** get_address_of_type_lock_0() { return &___type_lock_0; }
	inline void set_type_lock_0(Il2CppObject * value)
	{
		___type_lock_0 = value;
		Il2CppCodeGenWriteBarrier(&___type_lock_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
