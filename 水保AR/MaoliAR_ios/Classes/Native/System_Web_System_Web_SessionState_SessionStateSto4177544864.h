﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.SessionState.ISessionStateItemCollection
struct ISessionStateItemCollection_t2279755297;
// System.Web.HttpStaticObjectsCollection
struct HttpStaticObjectsCollection_t518175362;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.SessionState.SessionStateStoreData
struct  SessionStateStoreData_t4177544864  : public Il2CppObject
{
public:
	// System.Web.SessionState.ISessionStateItemCollection System.Web.SessionState.SessionStateStoreData::sessionItems
	Il2CppObject * ___sessionItems_0;
	// System.Web.HttpStaticObjectsCollection System.Web.SessionState.SessionStateStoreData::staticObjects
	HttpStaticObjectsCollection_t518175362 * ___staticObjects_1;
	// System.Int32 System.Web.SessionState.SessionStateStoreData::timeout
	int32_t ___timeout_2;

public:
	inline static int32_t get_offset_of_sessionItems_0() { return static_cast<int32_t>(offsetof(SessionStateStoreData_t4177544864, ___sessionItems_0)); }
	inline Il2CppObject * get_sessionItems_0() const { return ___sessionItems_0; }
	inline Il2CppObject ** get_address_of_sessionItems_0() { return &___sessionItems_0; }
	inline void set_sessionItems_0(Il2CppObject * value)
	{
		___sessionItems_0 = value;
		Il2CppCodeGenWriteBarrier(&___sessionItems_0, value);
	}

	inline static int32_t get_offset_of_staticObjects_1() { return static_cast<int32_t>(offsetof(SessionStateStoreData_t4177544864, ___staticObjects_1)); }
	inline HttpStaticObjectsCollection_t518175362 * get_staticObjects_1() const { return ___staticObjects_1; }
	inline HttpStaticObjectsCollection_t518175362 ** get_address_of_staticObjects_1() { return &___staticObjects_1; }
	inline void set_staticObjects_1(HttpStaticObjectsCollection_t518175362 * value)
	{
		___staticObjects_1 = value;
		Il2CppCodeGenWriteBarrier(&___staticObjects_1, value);
	}

	inline static int32_t get_offset_of_timeout_2() { return static_cast<int32_t>(offsetof(SessionStateStoreData_t4177544864, ___timeout_2)); }
	inline int32_t get_timeout_2() const { return ___timeout_2; }
	inline int32_t* get_address_of_timeout_2() { return &___timeout_2; }
	inline void set_timeout_2(int32_t value)
	{
		___timeout_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
