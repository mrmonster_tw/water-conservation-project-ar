﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_S2174777289.h"

// System.ServiceModel.Channels.RecipientMessageSecurityBindingSupport
struct RecipientMessageSecurityBindingSupport_t1155974079;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.RecipientSecureMessageDecryptor
struct  RecipientSecureMessageDecryptor_t3079654121  : public SecureMessageDecryptor_t2174777289
{
public:
	// System.ServiceModel.Channels.RecipientMessageSecurityBindingSupport System.ServiceModel.Channels.RecipientSecureMessageDecryptor::security
	RecipientMessageSecurityBindingSupport_t1155974079 * ___security_11;

public:
	inline static int32_t get_offset_of_security_11() { return static_cast<int32_t>(offsetof(RecipientSecureMessageDecryptor_t3079654121, ___security_11)); }
	inline RecipientMessageSecurityBindingSupport_t1155974079 * get_security_11() const { return ___security_11; }
	inline RecipientMessageSecurityBindingSupport_t1155974079 ** get_address_of_security_11() { return &___security_11; }
	inline void set_security_11(RecipientMessageSecurityBindingSupport_t1155974079 * value)
	{
		___security_11 = value;
		Il2CppCodeGenWriteBarrier(&___security_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
