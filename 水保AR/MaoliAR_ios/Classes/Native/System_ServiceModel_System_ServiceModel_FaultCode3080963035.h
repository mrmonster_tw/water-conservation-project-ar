﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.ServiceModel.FaultCode
struct FaultCode_t3080963035;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.FaultCode
struct  FaultCode_t3080963035  : public Il2CppObject
{
public:
	// System.String System.ServiceModel.FaultCode::name
	String_t* ___name_0;
	// System.String System.ServiceModel.FaultCode::ns
	String_t* ___ns_1;
	// System.ServiceModel.FaultCode System.ServiceModel.FaultCode::subcode
	FaultCode_t3080963035 * ___subcode_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(FaultCode_t3080963035, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_ns_1() { return static_cast<int32_t>(offsetof(FaultCode_t3080963035, ___ns_1)); }
	inline String_t* get_ns_1() const { return ___ns_1; }
	inline String_t** get_address_of_ns_1() { return &___ns_1; }
	inline void set_ns_1(String_t* value)
	{
		___ns_1 = value;
		Il2CppCodeGenWriteBarrier(&___ns_1, value);
	}

	inline static int32_t get_offset_of_subcode_2() { return static_cast<int32_t>(offsetof(FaultCode_t3080963035, ___subcode_2)); }
	inline FaultCode_t3080963035 * get_subcode_2() const { return ___subcode_2; }
	inline FaultCode_t3080963035 ** get_address_of_subcode_2() { return &___subcode_2; }
	inline void set_subcode_2(FaultCode_t3080963035 * value)
	{
		___subcode_2 = value;
		Il2CppCodeGenWriteBarrier(&___subcode_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
