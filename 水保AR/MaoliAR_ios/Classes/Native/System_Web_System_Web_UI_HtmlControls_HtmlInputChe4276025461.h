﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_HtmlControls_HtmlInputCon1839583126.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.HtmlControls.HtmlInputCheckBox
struct  HtmlInputCheckBox_t4276025461  : public HtmlInputControl_t1839583126
{
public:

public:
};

struct HtmlInputCheckBox_t4276025461_StaticFields
{
public:
	// System.Object System.Web.UI.HtmlControls.HtmlInputCheckBox::EventServerChange
	Il2CppObject * ___EventServerChange_30;

public:
	inline static int32_t get_offset_of_EventServerChange_30() { return static_cast<int32_t>(offsetof(HtmlInputCheckBox_t4276025461_StaticFields, ___EventServerChange_30)); }
	inline Il2CppObject * get_EventServerChange_30() const { return ___EventServerChange_30; }
	inline Il2CppObject ** get_address_of_EventServerChange_30() { return &___EventServerChange_30; }
	inline void set_EventServerChange_30(Il2CppObject * value)
	{
		___EventServerChange_30 = value;
		Il2CppCodeGenWriteBarrier(&___EventServerChange_30, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
