﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaForm4264307319.h"

// System.Xml.Serialization.XmlTypeMapMember
struct XmlTypeMapMember_t3739392753;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlMemberMapping
struct  XmlMemberMapping_t113569223  : public Il2CppObject
{
public:
	// System.Xml.Serialization.XmlTypeMapMember System.Xml.Serialization.XmlMemberMapping::_mapMember
	XmlTypeMapMember_t3739392753 * ____mapMember_0;
	// System.String System.Xml.Serialization.XmlMemberMapping::_elementName
	String_t* ____elementName_1;
	// System.String System.Xml.Serialization.XmlMemberMapping::_memberName
	String_t* ____memberName_2;
	// System.String System.Xml.Serialization.XmlMemberMapping::_namespace
	String_t* ____namespace_3;
	// System.String System.Xml.Serialization.XmlMemberMapping::_typeNamespace
	String_t* ____typeNamespace_4;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Serialization.XmlMemberMapping::_form
	int32_t ____form_5;

public:
	inline static int32_t get_offset_of__mapMember_0() { return static_cast<int32_t>(offsetof(XmlMemberMapping_t113569223, ____mapMember_0)); }
	inline XmlTypeMapMember_t3739392753 * get__mapMember_0() const { return ____mapMember_0; }
	inline XmlTypeMapMember_t3739392753 ** get_address_of__mapMember_0() { return &____mapMember_0; }
	inline void set__mapMember_0(XmlTypeMapMember_t3739392753 * value)
	{
		____mapMember_0 = value;
		Il2CppCodeGenWriteBarrier(&____mapMember_0, value);
	}

	inline static int32_t get_offset_of__elementName_1() { return static_cast<int32_t>(offsetof(XmlMemberMapping_t113569223, ____elementName_1)); }
	inline String_t* get__elementName_1() const { return ____elementName_1; }
	inline String_t** get_address_of__elementName_1() { return &____elementName_1; }
	inline void set__elementName_1(String_t* value)
	{
		____elementName_1 = value;
		Il2CppCodeGenWriteBarrier(&____elementName_1, value);
	}

	inline static int32_t get_offset_of__memberName_2() { return static_cast<int32_t>(offsetof(XmlMemberMapping_t113569223, ____memberName_2)); }
	inline String_t* get__memberName_2() const { return ____memberName_2; }
	inline String_t** get_address_of__memberName_2() { return &____memberName_2; }
	inline void set__memberName_2(String_t* value)
	{
		____memberName_2 = value;
		Il2CppCodeGenWriteBarrier(&____memberName_2, value);
	}

	inline static int32_t get_offset_of__namespace_3() { return static_cast<int32_t>(offsetof(XmlMemberMapping_t113569223, ____namespace_3)); }
	inline String_t* get__namespace_3() const { return ____namespace_3; }
	inline String_t** get_address_of__namespace_3() { return &____namespace_3; }
	inline void set__namespace_3(String_t* value)
	{
		____namespace_3 = value;
		Il2CppCodeGenWriteBarrier(&____namespace_3, value);
	}

	inline static int32_t get_offset_of__typeNamespace_4() { return static_cast<int32_t>(offsetof(XmlMemberMapping_t113569223, ____typeNamespace_4)); }
	inline String_t* get__typeNamespace_4() const { return ____typeNamespace_4; }
	inline String_t** get_address_of__typeNamespace_4() { return &____typeNamespace_4; }
	inline void set__typeNamespace_4(String_t* value)
	{
		____typeNamespace_4 = value;
		Il2CppCodeGenWriteBarrier(&____typeNamespace_4, value);
	}

	inline static int32_t get_offset_of__form_5() { return static_cast<int32_t>(offsetof(XmlMemberMapping_t113569223, ____form_5)); }
	inline int32_t get__form_5() const { return ____form_5; }
	inline int32_t* get_address_of__form_5() { return &____form_5; }
	inline void set__form_5(int32_t value)
	{
		____form_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
