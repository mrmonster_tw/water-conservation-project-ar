﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Security.Cryptography.RSA
struct RSA_t2385438082;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.ManagedProtection
struct  ManagedProtection_t1203008551  : public Il2CppObject
{
public:

public:
};

struct ManagedProtection_t1203008551_StaticFields
{
public:
	// System.Security.Cryptography.RSA Mono.Security.Cryptography.ManagedProtection::user
	RSA_t2385438082 * ___user_0;
	// System.Security.Cryptography.RSA Mono.Security.Cryptography.ManagedProtection::machine
	RSA_t2385438082 * ___machine_1;

public:
	inline static int32_t get_offset_of_user_0() { return static_cast<int32_t>(offsetof(ManagedProtection_t1203008551_StaticFields, ___user_0)); }
	inline RSA_t2385438082 * get_user_0() const { return ___user_0; }
	inline RSA_t2385438082 ** get_address_of_user_0() { return &___user_0; }
	inline void set_user_0(RSA_t2385438082 * value)
	{
		___user_0 = value;
		Il2CppCodeGenWriteBarrier(&___user_0, value);
	}

	inline static int32_t get_offset_of_machine_1() { return static_cast<int32_t>(offsetof(ManagedProtection_t1203008551_StaticFields, ___machine_1)); }
	inline RSA_t2385438082 * get_machine_1() const { return ___machine_1; }
	inline RSA_t2385438082 ** get_address_of_machine_1() { return &___machine_1; }
	inline void set_machine_1(RSA_t2385438082 * value)
	{
		___machine_1 = value;
		Il2CppCodeGenWriteBarrier(&___machine_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
