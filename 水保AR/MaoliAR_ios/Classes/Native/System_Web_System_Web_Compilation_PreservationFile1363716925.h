﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Web_System_Web_Compilation_BuildResultTypeC2541654725.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.PreservationFile
struct  PreservationFile_t1363716925  : public Il2CppObject
{
public:
	// System.String System.Web.Compilation.PreservationFile::_filePath
	String_t* ____filePath_0;
	// System.String System.Web.Compilation.PreservationFile::_assembly
	String_t* ____assembly_1;
	// System.Int32 System.Web.Compilation.PreservationFile::_fileHash
	int32_t ____fileHash_2;
	// System.Int32 System.Web.Compilation.PreservationFile::_flags
	int32_t ____flags_3;
	// System.Int32 System.Web.Compilation.PreservationFile::_hash
	int32_t ____hash_4;
	// System.Web.Compilation.BuildResultTypeCode System.Web.Compilation.PreservationFile::_resultType
	int32_t ____resultType_5;
	// System.String System.Web.Compilation.PreservationFile::_virtualPath
	String_t* ____virtualPath_6;
	// System.Collections.Generic.List`1<System.String> System.Web.Compilation.PreservationFile::_filedeps
	List_1_t3319525431 * ____filedeps_7;

public:
	inline static int32_t get_offset_of__filePath_0() { return static_cast<int32_t>(offsetof(PreservationFile_t1363716925, ____filePath_0)); }
	inline String_t* get__filePath_0() const { return ____filePath_0; }
	inline String_t** get_address_of__filePath_0() { return &____filePath_0; }
	inline void set__filePath_0(String_t* value)
	{
		____filePath_0 = value;
		Il2CppCodeGenWriteBarrier(&____filePath_0, value);
	}

	inline static int32_t get_offset_of__assembly_1() { return static_cast<int32_t>(offsetof(PreservationFile_t1363716925, ____assembly_1)); }
	inline String_t* get__assembly_1() const { return ____assembly_1; }
	inline String_t** get_address_of__assembly_1() { return &____assembly_1; }
	inline void set__assembly_1(String_t* value)
	{
		____assembly_1 = value;
		Il2CppCodeGenWriteBarrier(&____assembly_1, value);
	}

	inline static int32_t get_offset_of__fileHash_2() { return static_cast<int32_t>(offsetof(PreservationFile_t1363716925, ____fileHash_2)); }
	inline int32_t get__fileHash_2() const { return ____fileHash_2; }
	inline int32_t* get_address_of__fileHash_2() { return &____fileHash_2; }
	inline void set__fileHash_2(int32_t value)
	{
		____fileHash_2 = value;
	}

	inline static int32_t get_offset_of__flags_3() { return static_cast<int32_t>(offsetof(PreservationFile_t1363716925, ____flags_3)); }
	inline int32_t get__flags_3() const { return ____flags_3; }
	inline int32_t* get_address_of__flags_3() { return &____flags_3; }
	inline void set__flags_3(int32_t value)
	{
		____flags_3 = value;
	}

	inline static int32_t get_offset_of__hash_4() { return static_cast<int32_t>(offsetof(PreservationFile_t1363716925, ____hash_4)); }
	inline int32_t get__hash_4() const { return ____hash_4; }
	inline int32_t* get_address_of__hash_4() { return &____hash_4; }
	inline void set__hash_4(int32_t value)
	{
		____hash_4 = value;
	}

	inline static int32_t get_offset_of__resultType_5() { return static_cast<int32_t>(offsetof(PreservationFile_t1363716925, ____resultType_5)); }
	inline int32_t get__resultType_5() const { return ____resultType_5; }
	inline int32_t* get_address_of__resultType_5() { return &____resultType_5; }
	inline void set__resultType_5(int32_t value)
	{
		____resultType_5 = value;
	}

	inline static int32_t get_offset_of__virtualPath_6() { return static_cast<int32_t>(offsetof(PreservationFile_t1363716925, ____virtualPath_6)); }
	inline String_t* get__virtualPath_6() const { return ____virtualPath_6; }
	inline String_t** get_address_of__virtualPath_6() { return &____virtualPath_6; }
	inline void set__virtualPath_6(String_t* value)
	{
		____virtualPath_6 = value;
		Il2CppCodeGenWriteBarrier(&____virtualPath_6, value);
	}

	inline static int32_t get_offset_of__filedeps_7() { return static_cast<int32_t>(offsetof(PreservationFile_t1363716925, ____filedeps_7)); }
	inline List_1_t3319525431 * get__filedeps_7() const { return ____filedeps_7; }
	inline List_1_t3319525431 ** get_address_of__filedeps_7() { return &____filedeps_7; }
	inline void set__filedeps_7(List_1_t3319525431 * value)
	{
		____filedeps_7 = value;
		Il2CppCodeGenWriteBarrier(&____filedeps_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
