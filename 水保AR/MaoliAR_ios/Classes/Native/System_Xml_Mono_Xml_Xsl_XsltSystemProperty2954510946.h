﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XPath_XPathFunction857746608.h"

// System.Xml.XPath.Expression
struct Expression_t1452783009;
// System.Xml.Xsl.IStaticXsltContext
struct IStaticXsltContext_t1496336571;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.XsltSystemProperty
struct  XsltSystemProperty_t2954510946  : public XPathFunction_t857746608
{
public:
	// System.Xml.XPath.Expression Mono.Xml.Xsl.XsltSystemProperty::arg0
	Expression_t1452783009 * ___arg0_0;
	// System.Xml.Xsl.IStaticXsltContext Mono.Xml.Xsl.XsltSystemProperty::ctx
	Il2CppObject * ___ctx_1;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XsltSystemProperty_t2954510946, ___arg0_0)); }
	inline Expression_t1452783009 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1452783009 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1452783009 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier(&___arg0_0, value);
	}

	inline static int32_t get_offset_of_ctx_1() { return static_cast<int32_t>(offsetof(XsltSystemProperty_t2954510946, ___ctx_1)); }
	inline Il2CppObject * get_ctx_1() const { return ___ctx_1; }
	inline Il2CppObject ** get_address_of_ctx_1() { return &___ctx_1; }
	inline void set_ctx_1(Il2CppObject * value)
	{
		___ctx_1 = value;
		Il2CppCodeGenWriteBarrier(&___ctx_1, value);
	}
};

struct XsltSystemProperty_t2954510946_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.XsltSystemProperty::<>f__switch$map28
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map28_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map28_2() { return static_cast<int32_t>(offsetof(XsltSystemProperty_t2954510946_StaticFields, ___U3CU3Ef__switchU24map28_2)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map28_2() const { return ___U3CU3Ef__switchU24map28_2; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map28_2() { return &___U3CU3Ef__switchU24map28_2; }
	inline void set_U3CU3Ef__switchU24map28_2(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map28_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map28_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
