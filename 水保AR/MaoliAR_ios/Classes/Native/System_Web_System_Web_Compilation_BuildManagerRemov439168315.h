﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_EventArgs3591816995.h"

// System.String
struct String_t;
// System.Web.HttpContext
struct HttpContext_t1969259010;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.BuildManagerRemoveEntryEventArgs
struct  BuildManagerRemoveEntryEventArgs_t439168315  : public EventArgs_t3591816995
{
public:
	// System.String System.Web.Compilation.BuildManagerRemoveEntryEventArgs::<EntryName>k__BackingField
	String_t* ___U3CEntryNameU3Ek__BackingField_1;
	// System.Web.HttpContext System.Web.Compilation.BuildManagerRemoveEntryEventArgs::<Context>k__BackingField
	HttpContext_t1969259010 * ___U3CContextU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CEntryNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BuildManagerRemoveEntryEventArgs_t439168315, ___U3CEntryNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CEntryNameU3Ek__BackingField_1() const { return ___U3CEntryNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CEntryNameU3Ek__BackingField_1() { return &___U3CEntryNameU3Ek__BackingField_1; }
	inline void set_U3CEntryNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CEntryNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CEntryNameU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CContextU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BuildManagerRemoveEntryEventArgs_t439168315, ___U3CContextU3Ek__BackingField_2)); }
	inline HttpContext_t1969259010 * get_U3CContextU3Ek__BackingField_2() const { return ___U3CContextU3Ek__BackingField_2; }
	inline HttpContext_t1969259010 ** get_address_of_U3CContextU3Ek__BackingField_2() { return &___U3CContextU3Ek__BackingField_2; }
	inline void set_U3CContextU3Ek__BackingField_2(HttpContext_t1969259010 * value)
	{
		___U3CContextU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CContextU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
