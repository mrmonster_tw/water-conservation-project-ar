﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1032325577.h"

// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.RenderTexture[]
struct RenderTextureU5BU5D_t4111643188;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.Bloom
struct  Bloom_t1975354525  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.CinematicEffects.Bloom/Settings UnityStandardAssets.CinematicEffects.Bloom::settings
	Settings_t1032325577  ___settings_2;
	// UnityEngine.Shader UnityStandardAssets.CinematicEffects.Bloom::m_Shader
	Shader_t4151988712 * ___m_Shader_3;
	// UnityEngine.Material UnityStandardAssets.CinematicEffects.Bloom::m_Material
	Material_t340375123 * ___m_Material_4;
	// UnityEngine.RenderTexture[] UnityStandardAssets.CinematicEffects.Bloom::m_blurBuffer1
	RenderTextureU5BU5D_t4111643188* ___m_blurBuffer1_6;
	// UnityEngine.RenderTexture[] UnityStandardAssets.CinematicEffects.Bloom::m_blurBuffer2
	RenderTextureU5BU5D_t4111643188* ___m_blurBuffer2_7;

public:
	inline static int32_t get_offset_of_settings_2() { return static_cast<int32_t>(offsetof(Bloom_t1975354525, ___settings_2)); }
	inline Settings_t1032325577  get_settings_2() const { return ___settings_2; }
	inline Settings_t1032325577 * get_address_of_settings_2() { return &___settings_2; }
	inline void set_settings_2(Settings_t1032325577  value)
	{
		___settings_2 = value;
	}

	inline static int32_t get_offset_of_m_Shader_3() { return static_cast<int32_t>(offsetof(Bloom_t1975354525, ___m_Shader_3)); }
	inline Shader_t4151988712 * get_m_Shader_3() const { return ___m_Shader_3; }
	inline Shader_t4151988712 ** get_address_of_m_Shader_3() { return &___m_Shader_3; }
	inline void set_m_Shader_3(Shader_t4151988712 * value)
	{
		___m_Shader_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_Shader_3, value);
	}

	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Bloom_t1975354525, ___m_Material_4)); }
	inline Material_t340375123 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t340375123 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t340375123 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_Material_4, value);
	}

	inline static int32_t get_offset_of_m_blurBuffer1_6() { return static_cast<int32_t>(offsetof(Bloom_t1975354525, ___m_blurBuffer1_6)); }
	inline RenderTextureU5BU5D_t4111643188* get_m_blurBuffer1_6() const { return ___m_blurBuffer1_6; }
	inline RenderTextureU5BU5D_t4111643188** get_address_of_m_blurBuffer1_6() { return &___m_blurBuffer1_6; }
	inline void set_m_blurBuffer1_6(RenderTextureU5BU5D_t4111643188* value)
	{
		___m_blurBuffer1_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_blurBuffer1_6, value);
	}

	inline static int32_t get_offset_of_m_blurBuffer2_7() { return static_cast<int32_t>(offsetof(Bloom_t1975354525, ___m_blurBuffer2_7)); }
	inline RenderTextureU5BU5D_t4111643188* get_m_blurBuffer2_7() const { return ___m_blurBuffer2_7; }
	inline RenderTextureU5BU5D_t4111643188** get_address_of_m_blurBuffer2_7() { return &___m_blurBuffer2_7; }
	inline void set_m_blurBuffer2_7(RenderTextureU5BU5D_t4111643188* value)
	{
		___m_blurBuffer2_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_blurBuffer2_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
