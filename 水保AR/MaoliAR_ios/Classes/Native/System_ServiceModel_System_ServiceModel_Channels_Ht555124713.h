﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Generic.Dictionary`2<System.Uri,System.Collections.Generic.List`1<System.ServiceModel.Channels.IChannelListener>>
struct Dictionary_2_t763786028;
// System.ServiceModel.Channels.IChannelListener
struct IChannelListener_t114797722;
// System.ServiceModel.Description.MetadataPublishingInfo
struct MetadataPublishingInfo_t2085662404;
// System.ServiceModel.Description.HttpGetWsdl
struct HttpGetWsdl_t3793260870;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t1333520283;
// System.Collections.Generic.List`1<System.ServiceModel.Channels.HttpContextInfo>
struct List_1_t738642879;
// System.ServiceModel.Channels.HttpTransportBindingElement
struct HttpTransportBindingElement_t2392894562;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.HttpListenerManager
struct  HttpListenerManager_t555124713  : public Il2CppObject
{
public:
	// System.ServiceModel.Channels.IChannelListener System.ServiceModel.Channels.HttpListenerManager::channel_listener
	Il2CppObject * ___channel_listener_1;
	// System.ServiceModel.Description.MetadataPublishingInfo System.ServiceModel.Channels.HttpListenerManager::mex_info
	MetadataPublishingInfo_t2085662404 * ___mex_info_2;
	// System.ServiceModel.Description.HttpGetWsdl System.ServiceModel.Channels.HttpListenerManager::wsdl_instance
	HttpGetWsdl_t3793260870 * ___wsdl_instance_3;
	// System.Threading.AutoResetEvent System.ServiceModel.Channels.HttpListenerManager::wait_http_ctx
	AutoResetEvent_t1333520283 * ___wait_http_ctx_4;
	// System.Collections.Generic.List`1<System.ServiceModel.Channels.HttpContextInfo> System.ServiceModel.Channels.HttpListenerManager::pending
	List_1_t738642879 * ___pending_5;
	// System.ServiceModel.Channels.HttpTransportBindingElement System.ServiceModel.Channels.HttpListenerManager::<Source>k__BackingField
	HttpTransportBindingElement_t2392894562 * ___U3CSourceU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_channel_listener_1() { return static_cast<int32_t>(offsetof(HttpListenerManager_t555124713, ___channel_listener_1)); }
	inline Il2CppObject * get_channel_listener_1() const { return ___channel_listener_1; }
	inline Il2CppObject ** get_address_of_channel_listener_1() { return &___channel_listener_1; }
	inline void set_channel_listener_1(Il2CppObject * value)
	{
		___channel_listener_1 = value;
		Il2CppCodeGenWriteBarrier(&___channel_listener_1, value);
	}

	inline static int32_t get_offset_of_mex_info_2() { return static_cast<int32_t>(offsetof(HttpListenerManager_t555124713, ___mex_info_2)); }
	inline MetadataPublishingInfo_t2085662404 * get_mex_info_2() const { return ___mex_info_2; }
	inline MetadataPublishingInfo_t2085662404 ** get_address_of_mex_info_2() { return &___mex_info_2; }
	inline void set_mex_info_2(MetadataPublishingInfo_t2085662404 * value)
	{
		___mex_info_2 = value;
		Il2CppCodeGenWriteBarrier(&___mex_info_2, value);
	}

	inline static int32_t get_offset_of_wsdl_instance_3() { return static_cast<int32_t>(offsetof(HttpListenerManager_t555124713, ___wsdl_instance_3)); }
	inline HttpGetWsdl_t3793260870 * get_wsdl_instance_3() const { return ___wsdl_instance_3; }
	inline HttpGetWsdl_t3793260870 ** get_address_of_wsdl_instance_3() { return &___wsdl_instance_3; }
	inline void set_wsdl_instance_3(HttpGetWsdl_t3793260870 * value)
	{
		___wsdl_instance_3 = value;
		Il2CppCodeGenWriteBarrier(&___wsdl_instance_3, value);
	}

	inline static int32_t get_offset_of_wait_http_ctx_4() { return static_cast<int32_t>(offsetof(HttpListenerManager_t555124713, ___wait_http_ctx_4)); }
	inline AutoResetEvent_t1333520283 * get_wait_http_ctx_4() const { return ___wait_http_ctx_4; }
	inline AutoResetEvent_t1333520283 ** get_address_of_wait_http_ctx_4() { return &___wait_http_ctx_4; }
	inline void set_wait_http_ctx_4(AutoResetEvent_t1333520283 * value)
	{
		___wait_http_ctx_4 = value;
		Il2CppCodeGenWriteBarrier(&___wait_http_ctx_4, value);
	}

	inline static int32_t get_offset_of_pending_5() { return static_cast<int32_t>(offsetof(HttpListenerManager_t555124713, ___pending_5)); }
	inline List_1_t738642879 * get_pending_5() const { return ___pending_5; }
	inline List_1_t738642879 ** get_address_of_pending_5() { return &___pending_5; }
	inline void set_pending_5(List_1_t738642879 * value)
	{
		___pending_5 = value;
		Il2CppCodeGenWriteBarrier(&___pending_5, value);
	}

	inline static int32_t get_offset_of_U3CSourceU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(HttpListenerManager_t555124713, ___U3CSourceU3Ek__BackingField_6)); }
	inline HttpTransportBindingElement_t2392894562 * get_U3CSourceU3Ek__BackingField_6() const { return ___U3CSourceU3Ek__BackingField_6; }
	inline HttpTransportBindingElement_t2392894562 ** get_address_of_U3CSourceU3Ek__BackingField_6() { return &___U3CSourceU3Ek__BackingField_6; }
	inline void set_U3CSourceU3Ek__BackingField_6(HttpTransportBindingElement_t2392894562 * value)
	{
		___U3CSourceU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSourceU3Ek__BackingField_6, value);
	}
};

struct HttpListenerManager_t555124713_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Uri,System.Collections.Generic.List`1<System.ServiceModel.Channels.IChannelListener>> System.ServiceModel.Channels.HttpListenerManager::registered_channels
	Dictionary_2_t763786028 * ___registered_channels_0;

public:
	inline static int32_t get_offset_of_registered_channels_0() { return static_cast<int32_t>(offsetof(HttpListenerManager_t555124713_StaticFields, ___registered_channels_0)); }
	inline Dictionary_2_t763786028 * get_registered_channels_0() const { return ___registered_channels_0; }
	inline Dictionary_2_t763786028 ** get_address_of_registered_channels_0() { return &___registered_channels_0; }
	inline void set_registered_channels_0(Dictionary_2_t763786028 * value)
	{
		___registered_channels_0 = value;
		Il2CppCodeGenWriteBarrier(&___registered_channels_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
