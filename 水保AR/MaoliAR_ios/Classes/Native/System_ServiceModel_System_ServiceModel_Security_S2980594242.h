﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Security.SecurityAlgorithmSuite
struct SecurityAlgorithmSuite_t2980594242;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.SecurityAlgorithmSuite
struct  SecurityAlgorithmSuite_t2980594242  : public Il2CppObject
{
public:

public:
};

struct SecurityAlgorithmSuite_t2980594242_StaticFields
{
public:
	// System.ServiceModel.Security.SecurityAlgorithmSuite System.ServiceModel.Security.SecurityAlgorithmSuite::b128
	SecurityAlgorithmSuite_t2980594242 * ___b128_0;
	// System.ServiceModel.Security.SecurityAlgorithmSuite System.ServiceModel.Security.SecurityAlgorithmSuite::b128r
	SecurityAlgorithmSuite_t2980594242 * ___b128r_1;
	// System.ServiceModel.Security.SecurityAlgorithmSuite System.ServiceModel.Security.SecurityAlgorithmSuite::b128s
	SecurityAlgorithmSuite_t2980594242 * ___b128s_2;
	// System.ServiceModel.Security.SecurityAlgorithmSuite System.ServiceModel.Security.SecurityAlgorithmSuite::b128sr
	SecurityAlgorithmSuite_t2980594242 * ___b128sr_3;
	// System.ServiceModel.Security.SecurityAlgorithmSuite System.ServiceModel.Security.SecurityAlgorithmSuite::b192
	SecurityAlgorithmSuite_t2980594242 * ___b192_4;
	// System.ServiceModel.Security.SecurityAlgorithmSuite System.ServiceModel.Security.SecurityAlgorithmSuite::b192r
	SecurityAlgorithmSuite_t2980594242 * ___b192r_5;
	// System.ServiceModel.Security.SecurityAlgorithmSuite System.ServiceModel.Security.SecurityAlgorithmSuite::b192s
	SecurityAlgorithmSuite_t2980594242 * ___b192s_6;
	// System.ServiceModel.Security.SecurityAlgorithmSuite System.ServiceModel.Security.SecurityAlgorithmSuite::b192sr
	SecurityAlgorithmSuite_t2980594242 * ___b192sr_7;
	// System.ServiceModel.Security.SecurityAlgorithmSuite System.ServiceModel.Security.SecurityAlgorithmSuite::b256
	SecurityAlgorithmSuite_t2980594242 * ___b256_8;
	// System.ServiceModel.Security.SecurityAlgorithmSuite System.ServiceModel.Security.SecurityAlgorithmSuite::b256r
	SecurityAlgorithmSuite_t2980594242 * ___b256r_9;
	// System.ServiceModel.Security.SecurityAlgorithmSuite System.ServiceModel.Security.SecurityAlgorithmSuite::b256s
	SecurityAlgorithmSuite_t2980594242 * ___b256s_10;
	// System.ServiceModel.Security.SecurityAlgorithmSuite System.ServiceModel.Security.SecurityAlgorithmSuite::b256sr
	SecurityAlgorithmSuite_t2980594242 * ___b256sr_11;
	// System.ServiceModel.Security.SecurityAlgorithmSuite System.ServiceModel.Security.SecurityAlgorithmSuite::tdes
	SecurityAlgorithmSuite_t2980594242 * ___tdes_12;
	// System.ServiceModel.Security.SecurityAlgorithmSuite System.ServiceModel.Security.SecurityAlgorithmSuite::tdes_r
	SecurityAlgorithmSuite_t2980594242 * ___tdes_r_13;
	// System.ServiceModel.Security.SecurityAlgorithmSuite System.ServiceModel.Security.SecurityAlgorithmSuite::tdes_s
	SecurityAlgorithmSuite_t2980594242 * ___tdes_s_14;
	// System.ServiceModel.Security.SecurityAlgorithmSuite System.ServiceModel.Security.SecurityAlgorithmSuite::tdes_sr
	SecurityAlgorithmSuite_t2980594242 * ___tdes_sr_15;

public:
	inline static int32_t get_offset_of_b128_0() { return static_cast<int32_t>(offsetof(SecurityAlgorithmSuite_t2980594242_StaticFields, ___b128_0)); }
	inline SecurityAlgorithmSuite_t2980594242 * get_b128_0() const { return ___b128_0; }
	inline SecurityAlgorithmSuite_t2980594242 ** get_address_of_b128_0() { return &___b128_0; }
	inline void set_b128_0(SecurityAlgorithmSuite_t2980594242 * value)
	{
		___b128_0 = value;
		Il2CppCodeGenWriteBarrier(&___b128_0, value);
	}

	inline static int32_t get_offset_of_b128r_1() { return static_cast<int32_t>(offsetof(SecurityAlgorithmSuite_t2980594242_StaticFields, ___b128r_1)); }
	inline SecurityAlgorithmSuite_t2980594242 * get_b128r_1() const { return ___b128r_1; }
	inline SecurityAlgorithmSuite_t2980594242 ** get_address_of_b128r_1() { return &___b128r_1; }
	inline void set_b128r_1(SecurityAlgorithmSuite_t2980594242 * value)
	{
		___b128r_1 = value;
		Il2CppCodeGenWriteBarrier(&___b128r_1, value);
	}

	inline static int32_t get_offset_of_b128s_2() { return static_cast<int32_t>(offsetof(SecurityAlgorithmSuite_t2980594242_StaticFields, ___b128s_2)); }
	inline SecurityAlgorithmSuite_t2980594242 * get_b128s_2() const { return ___b128s_2; }
	inline SecurityAlgorithmSuite_t2980594242 ** get_address_of_b128s_2() { return &___b128s_2; }
	inline void set_b128s_2(SecurityAlgorithmSuite_t2980594242 * value)
	{
		___b128s_2 = value;
		Il2CppCodeGenWriteBarrier(&___b128s_2, value);
	}

	inline static int32_t get_offset_of_b128sr_3() { return static_cast<int32_t>(offsetof(SecurityAlgorithmSuite_t2980594242_StaticFields, ___b128sr_3)); }
	inline SecurityAlgorithmSuite_t2980594242 * get_b128sr_3() const { return ___b128sr_3; }
	inline SecurityAlgorithmSuite_t2980594242 ** get_address_of_b128sr_3() { return &___b128sr_3; }
	inline void set_b128sr_3(SecurityAlgorithmSuite_t2980594242 * value)
	{
		___b128sr_3 = value;
		Il2CppCodeGenWriteBarrier(&___b128sr_3, value);
	}

	inline static int32_t get_offset_of_b192_4() { return static_cast<int32_t>(offsetof(SecurityAlgorithmSuite_t2980594242_StaticFields, ___b192_4)); }
	inline SecurityAlgorithmSuite_t2980594242 * get_b192_4() const { return ___b192_4; }
	inline SecurityAlgorithmSuite_t2980594242 ** get_address_of_b192_4() { return &___b192_4; }
	inline void set_b192_4(SecurityAlgorithmSuite_t2980594242 * value)
	{
		___b192_4 = value;
		Il2CppCodeGenWriteBarrier(&___b192_4, value);
	}

	inline static int32_t get_offset_of_b192r_5() { return static_cast<int32_t>(offsetof(SecurityAlgorithmSuite_t2980594242_StaticFields, ___b192r_5)); }
	inline SecurityAlgorithmSuite_t2980594242 * get_b192r_5() const { return ___b192r_5; }
	inline SecurityAlgorithmSuite_t2980594242 ** get_address_of_b192r_5() { return &___b192r_5; }
	inline void set_b192r_5(SecurityAlgorithmSuite_t2980594242 * value)
	{
		___b192r_5 = value;
		Il2CppCodeGenWriteBarrier(&___b192r_5, value);
	}

	inline static int32_t get_offset_of_b192s_6() { return static_cast<int32_t>(offsetof(SecurityAlgorithmSuite_t2980594242_StaticFields, ___b192s_6)); }
	inline SecurityAlgorithmSuite_t2980594242 * get_b192s_6() const { return ___b192s_6; }
	inline SecurityAlgorithmSuite_t2980594242 ** get_address_of_b192s_6() { return &___b192s_6; }
	inline void set_b192s_6(SecurityAlgorithmSuite_t2980594242 * value)
	{
		___b192s_6 = value;
		Il2CppCodeGenWriteBarrier(&___b192s_6, value);
	}

	inline static int32_t get_offset_of_b192sr_7() { return static_cast<int32_t>(offsetof(SecurityAlgorithmSuite_t2980594242_StaticFields, ___b192sr_7)); }
	inline SecurityAlgorithmSuite_t2980594242 * get_b192sr_7() const { return ___b192sr_7; }
	inline SecurityAlgorithmSuite_t2980594242 ** get_address_of_b192sr_7() { return &___b192sr_7; }
	inline void set_b192sr_7(SecurityAlgorithmSuite_t2980594242 * value)
	{
		___b192sr_7 = value;
		Il2CppCodeGenWriteBarrier(&___b192sr_7, value);
	}

	inline static int32_t get_offset_of_b256_8() { return static_cast<int32_t>(offsetof(SecurityAlgorithmSuite_t2980594242_StaticFields, ___b256_8)); }
	inline SecurityAlgorithmSuite_t2980594242 * get_b256_8() const { return ___b256_8; }
	inline SecurityAlgorithmSuite_t2980594242 ** get_address_of_b256_8() { return &___b256_8; }
	inline void set_b256_8(SecurityAlgorithmSuite_t2980594242 * value)
	{
		___b256_8 = value;
		Il2CppCodeGenWriteBarrier(&___b256_8, value);
	}

	inline static int32_t get_offset_of_b256r_9() { return static_cast<int32_t>(offsetof(SecurityAlgorithmSuite_t2980594242_StaticFields, ___b256r_9)); }
	inline SecurityAlgorithmSuite_t2980594242 * get_b256r_9() const { return ___b256r_9; }
	inline SecurityAlgorithmSuite_t2980594242 ** get_address_of_b256r_9() { return &___b256r_9; }
	inline void set_b256r_9(SecurityAlgorithmSuite_t2980594242 * value)
	{
		___b256r_9 = value;
		Il2CppCodeGenWriteBarrier(&___b256r_9, value);
	}

	inline static int32_t get_offset_of_b256s_10() { return static_cast<int32_t>(offsetof(SecurityAlgorithmSuite_t2980594242_StaticFields, ___b256s_10)); }
	inline SecurityAlgorithmSuite_t2980594242 * get_b256s_10() const { return ___b256s_10; }
	inline SecurityAlgorithmSuite_t2980594242 ** get_address_of_b256s_10() { return &___b256s_10; }
	inline void set_b256s_10(SecurityAlgorithmSuite_t2980594242 * value)
	{
		___b256s_10 = value;
		Il2CppCodeGenWriteBarrier(&___b256s_10, value);
	}

	inline static int32_t get_offset_of_b256sr_11() { return static_cast<int32_t>(offsetof(SecurityAlgorithmSuite_t2980594242_StaticFields, ___b256sr_11)); }
	inline SecurityAlgorithmSuite_t2980594242 * get_b256sr_11() const { return ___b256sr_11; }
	inline SecurityAlgorithmSuite_t2980594242 ** get_address_of_b256sr_11() { return &___b256sr_11; }
	inline void set_b256sr_11(SecurityAlgorithmSuite_t2980594242 * value)
	{
		___b256sr_11 = value;
		Il2CppCodeGenWriteBarrier(&___b256sr_11, value);
	}

	inline static int32_t get_offset_of_tdes_12() { return static_cast<int32_t>(offsetof(SecurityAlgorithmSuite_t2980594242_StaticFields, ___tdes_12)); }
	inline SecurityAlgorithmSuite_t2980594242 * get_tdes_12() const { return ___tdes_12; }
	inline SecurityAlgorithmSuite_t2980594242 ** get_address_of_tdes_12() { return &___tdes_12; }
	inline void set_tdes_12(SecurityAlgorithmSuite_t2980594242 * value)
	{
		___tdes_12 = value;
		Il2CppCodeGenWriteBarrier(&___tdes_12, value);
	}

	inline static int32_t get_offset_of_tdes_r_13() { return static_cast<int32_t>(offsetof(SecurityAlgorithmSuite_t2980594242_StaticFields, ___tdes_r_13)); }
	inline SecurityAlgorithmSuite_t2980594242 * get_tdes_r_13() const { return ___tdes_r_13; }
	inline SecurityAlgorithmSuite_t2980594242 ** get_address_of_tdes_r_13() { return &___tdes_r_13; }
	inline void set_tdes_r_13(SecurityAlgorithmSuite_t2980594242 * value)
	{
		___tdes_r_13 = value;
		Il2CppCodeGenWriteBarrier(&___tdes_r_13, value);
	}

	inline static int32_t get_offset_of_tdes_s_14() { return static_cast<int32_t>(offsetof(SecurityAlgorithmSuite_t2980594242_StaticFields, ___tdes_s_14)); }
	inline SecurityAlgorithmSuite_t2980594242 * get_tdes_s_14() const { return ___tdes_s_14; }
	inline SecurityAlgorithmSuite_t2980594242 ** get_address_of_tdes_s_14() { return &___tdes_s_14; }
	inline void set_tdes_s_14(SecurityAlgorithmSuite_t2980594242 * value)
	{
		___tdes_s_14 = value;
		Il2CppCodeGenWriteBarrier(&___tdes_s_14, value);
	}

	inline static int32_t get_offset_of_tdes_sr_15() { return static_cast<int32_t>(offsetof(SecurityAlgorithmSuite_t2980594242_StaticFields, ___tdes_sr_15)); }
	inline SecurityAlgorithmSuite_t2980594242 * get_tdes_sr_15() const { return ___tdes_sr_15; }
	inline SecurityAlgorithmSuite_t2980594242 ** get_address_of_tdes_sr_15() { return &___tdes_sr_15; }
	inline void set_tdes_sr_15(SecurityAlgorithmSuite_t2980594242 * value)
	{
		___tdes_sr_15 = value;
		Il2CppCodeGenWriteBarrier(&___tdes_sr_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
