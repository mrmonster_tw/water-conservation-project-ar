﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Change_Sprite
struct  Change_Sprite_t4001473290  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Sprite[] Change_Sprite::_sprite
	SpriteU5BU5D_t2581906349* ____sprite_2;

public:
	inline static int32_t get_offset_of__sprite_2() { return static_cast<int32_t>(offsetof(Change_Sprite_t4001473290, ____sprite_2)); }
	inline SpriteU5BU5D_t2581906349* get__sprite_2() const { return ____sprite_2; }
	inline SpriteU5BU5D_t2581906349** get_address_of__sprite_2() { return &____sprite_2; }
	inline void set__sprite_2(SpriteU5BU5D_t2581906349* value)
	{
		____sprite_2 = value;
		Il2CppCodeGenWriteBarrier(&____sprite_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
