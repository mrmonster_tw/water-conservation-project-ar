﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.HttpWorkerRequest
struct HttpWorkerRequest_t998871775;
// System.Web.HttpContext
struct HttpContext_t1969259010;
// System.Web.WebROCollection
struct WebROCollection_t4065660147;
// System.String
struct String_t;
// System.UriBuilder
struct UriBuilder_t579353065;
// System.Web.HttpBrowserCapabilities
struct HttpBrowserCapabilities_t4010224613;
// System.Text.Encoding
struct Encoding_t1523322056;
// System.IO.Stream
struct Stream_t1273022909;
// System.Web.InputFilterStream
struct InputFilterStream_t4129443631;
// System.Web.HttpCookieCollection
struct HttpCookieCollection_t1178559666;
// System.Web.HttpFileCollection
struct HttpFileCollection_t355459549;
// System.Web.ServerVariablesCollection
struct ServerVariablesCollection_t3116558453;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Uri
struct Uri_t100236324;
// System.Web.TempFileStream
struct TempFileStream_t420614451;
// System.Net.IPAddress[]
struct IPAddressU5BU5D_t596328627;
// System.Web.Configuration.UrlMappingCollection
struct UrlMappingCollection_t3845908325;
// System.Char[]
struct CharU5BU5D_t3528271667;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpRequest
struct  HttpRequest_t809700260  : public Il2CppObject
{
public:
	// System.Web.HttpWorkerRequest System.Web.HttpRequest::worker_request
	HttpWorkerRequest_t998871775 * ___worker_request_0;
	// System.Web.HttpContext System.Web.HttpRequest::context
	HttpContext_t1969259010 * ___context_1;
	// System.Web.WebROCollection System.Web.HttpRequest::query_string_nvc
	WebROCollection_t4065660147 * ___query_string_nvc_2;
	// System.String System.Web.HttpRequest::orig_url
	String_t* ___orig_url_3;
	// System.UriBuilder System.Web.HttpRequest::url_components
	UriBuilder_t579353065 * ___url_components_4;
	// System.String System.Web.HttpRequest::client_target
	String_t* ___client_target_5;
	// System.Web.HttpBrowserCapabilities System.Web.HttpRequest::browser_capabilities
	HttpBrowserCapabilities_t4010224613 * ___browser_capabilities_6;
	// System.String System.Web.HttpRequest::file_path
	String_t* ___file_path_7;
	// System.String System.Web.HttpRequest::base_virtual_dir
	String_t* ___base_virtual_dir_8;
	// System.String System.Web.HttpRequest::root_virtual_dir
	String_t* ___root_virtual_dir_9;
	// System.String System.Web.HttpRequest::client_file_path
	String_t* ___client_file_path_10;
	// System.String System.Web.HttpRequest::content_type
	String_t* ___content_type_11;
	// System.Int32 System.Web.HttpRequest::content_length
	int32_t ___content_length_12;
	// System.Text.Encoding System.Web.HttpRequest::encoding
	Encoding_t1523322056 * ___encoding_13;
	// System.String System.Web.HttpRequest::current_exe_path
	String_t* ___current_exe_path_14;
	// System.String System.Web.HttpRequest::physical_path
	String_t* ___physical_path_15;
	// System.String System.Web.HttpRequest::unescaped_path
	String_t* ___unescaped_path_16;
	// System.String System.Web.HttpRequest::path_info
	String_t* ___path_info_17;
	// System.Web.WebROCollection System.Web.HttpRequest::headers
	WebROCollection_t4065660147 * ___headers_18;
	// System.IO.Stream System.Web.HttpRequest::input_stream
	Stream_t1273022909 * ___input_stream_19;
	// System.Web.InputFilterStream System.Web.HttpRequest::input_filter
	InputFilterStream_t4129443631 * ___input_filter_20;
	// System.IO.Stream System.Web.HttpRequest::filter
	Stream_t1273022909 * ___filter_21;
	// System.Web.HttpCookieCollection System.Web.HttpRequest::cookies
	HttpCookieCollection_t1178559666 * ___cookies_22;
	// System.String System.Web.HttpRequest::http_method
	String_t* ___http_method_23;
	// System.Web.WebROCollection System.Web.HttpRequest::form
	WebROCollection_t4065660147 * ___form_24;
	// System.Web.HttpFileCollection System.Web.HttpRequest::files
	HttpFileCollection_t355459549 * ___files_25;
	// System.Web.ServerVariablesCollection System.Web.HttpRequest::server_variables
	ServerVariablesCollection_t3116558453 * ___server_variables_26;
	// System.String System.Web.HttpRequest::request_type
	String_t* ___request_type_27;
	// System.String[] System.Web.HttpRequest::user_languages
	StringU5BU5D_t1281789340* ___user_languages_28;
	// System.Uri System.Web.HttpRequest::cached_url
	Uri_t100236324 * ___cached_url_29;
	// System.Web.TempFileStream System.Web.HttpRequest::request_file
	TempFileStream_t420614451 * ___request_file_30;
	// System.Boolean System.Web.HttpRequest::validate_cookies
	bool ___validate_cookies_32;
	// System.Boolean System.Web.HttpRequest::validate_query_string
	bool ___validate_query_string_33;
	// System.Boolean System.Web.HttpRequest::validate_form
	bool ___validate_form_34;
	// System.Boolean System.Web.HttpRequest::checked_cookies
	bool ___checked_cookies_35;
	// System.Boolean System.Web.HttpRequest::checked_query_string
	bool ___checked_query_string_36;
	// System.Boolean System.Web.HttpRequest::checked_form
	bool ___checked_form_37;

public:
	inline static int32_t get_offset_of_worker_request_0() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___worker_request_0)); }
	inline HttpWorkerRequest_t998871775 * get_worker_request_0() const { return ___worker_request_0; }
	inline HttpWorkerRequest_t998871775 ** get_address_of_worker_request_0() { return &___worker_request_0; }
	inline void set_worker_request_0(HttpWorkerRequest_t998871775 * value)
	{
		___worker_request_0 = value;
		Il2CppCodeGenWriteBarrier(&___worker_request_0, value);
	}

	inline static int32_t get_offset_of_context_1() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___context_1)); }
	inline HttpContext_t1969259010 * get_context_1() const { return ___context_1; }
	inline HttpContext_t1969259010 ** get_address_of_context_1() { return &___context_1; }
	inline void set_context_1(HttpContext_t1969259010 * value)
	{
		___context_1 = value;
		Il2CppCodeGenWriteBarrier(&___context_1, value);
	}

	inline static int32_t get_offset_of_query_string_nvc_2() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___query_string_nvc_2)); }
	inline WebROCollection_t4065660147 * get_query_string_nvc_2() const { return ___query_string_nvc_2; }
	inline WebROCollection_t4065660147 ** get_address_of_query_string_nvc_2() { return &___query_string_nvc_2; }
	inline void set_query_string_nvc_2(WebROCollection_t4065660147 * value)
	{
		___query_string_nvc_2 = value;
		Il2CppCodeGenWriteBarrier(&___query_string_nvc_2, value);
	}

	inline static int32_t get_offset_of_orig_url_3() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___orig_url_3)); }
	inline String_t* get_orig_url_3() const { return ___orig_url_3; }
	inline String_t** get_address_of_orig_url_3() { return &___orig_url_3; }
	inline void set_orig_url_3(String_t* value)
	{
		___orig_url_3 = value;
		Il2CppCodeGenWriteBarrier(&___orig_url_3, value);
	}

	inline static int32_t get_offset_of_url_components_4() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___url_components_4)); }
	inline UriBuilder_t579353065 * get_url_components_4() const { return ___url_components_4; }
	inline UriBuilder_t579353065 ** get_address_of_url_components_4() { return &___url_components_4; }
	inline void set_url_components_4(UriBuilder_t579353065 * value)
	{
		___url_components_4 = value;
		Il2CppCodeGenWriteBarrier(&___url_components_4, value);
	}

	inline static int32_t get_offset_of_client_target_5() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___client_target_5)); }
	inline String_t* get_client_target_5() const { return ___client_target_5; }
	inline String_t** get_address_of_client_target_5() { return &___client_target_5; }
	inline void set_client_target_5(String_t* value)
	{
		___client_target_5 = value;
		Il2CppCodeGenWriteBarrier(&___client_target_5, value);
	}

	inline static int32_t get_offset_of_browser_capabilities_6() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___browser_capabilities_6)); }
	inline HttpBrowserCapabilities_t4010224613 * get_browser_capabilities_6() const { return ___browser_capabilities_6; }
	inline HttpBrowserCapabilities_t4010224613 ** get_address_of_browser_capabilities_6() { return &___browser_capabilities_6; }
	inline void set_browser_capabilities_6(HttpBrowserCapabilities_t4010224613 * value)
	{
		___browser_capabilities_6 = value;
		Il2CppCodeGenWriteBarrier(&___browser_capabilities_6, value);
	}

	inline static int32_t get_offset_of_file_path_7() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___file_path_7)); }
	inline String_t* get_file_path_7() const { return ___file_path_7; }
	inline String_t** get_address_of_file_path_7() { return &___file_path_7; }
	inline void set_file_path_7(String_t* value)
	{
		___file_path_7 = value;
		Il2CppCodeGenWriteBarrier(&___file_path_7, value);
	}

	inline static int32_t get_offset_of_base_virtual_dir_8() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___base_virtual_dir_8)); }
	inline String_t* get_base_virtual_dir_8() const { return ___base_virtual_dir_8; }
	inline String_t** get_address_of_base_virtual_dir_8() { return &___base_virtual_dir_8; }
	inline void set_base_virtual_dir_8(String_t* value)
	{
		___base_virtual_dir_8 = value;
		Il2CppCodeGenWriteBarrier(&___base_virtual_dir_8, value);
	}

	inline static int32_t get_offset_of_root_virtual_dir_9() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___root_virtual_dir_9)); }
	inline String_t* get_root_virtual_dir_9() const { return ___root_virtual_dir_9; }
	inline String_t** get_address_of_root_virtual_dir_9() { return &___root_virtual_dir_9; }
	inline void set_root_virtual_dir_9(String_t* value)
	{
		___root_virtual_dir_9 = value;
		Il2CppCodeGenWriteBarrier(&___root_virtual_dir_9, value);
	}

	inline static int32_t get_offset_of_client_file_path_10() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___client_file_path_10)); }
	inline String_t* get_client_file_path_10() const { return ___client_file_path_10; }
	inline String_t** get_address_of_client_file_path_10() { return &___client_file_path_10; }
	inline void set_client_file_path_10(String_t* value)
	{
		___client_file_path_10 = value;
		Il2CppCodeGenWriteBarrier(&___client_file_path_10, value);
	}

	inline static int32_t get_offset_of_content_type_11() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___content_type_11)); }
	inline String_t* get_content_type_11() const { return ___content_type_11; }
	inline String_t** get_address_of_content_type_11() { return &___content_type_11; }
	inline void set_content_type_11(String_t* value)
	{
		___content_type_11 = value;
		Il2CppCodeGenWriteBarrier(&___content_type_11, value);
	}

	inline static int32_t get_offset_of_content_length_12() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___content_length_12)); }
	inline int32_t get_content_length_12() const { return ___content_length_12; }
	inline int32_t* get_address_of_content_length_12() { return &___content_length_12; }
	inline void set_content_length_12(int32_t value)
	{
		___content_length_12 = value;
	}

	inline static int32_t get_offset_of_encoding_13() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___encoding_13)); }
	inline Encoding_t1523322056 * get_encoding_13() const { return ___encoding_13; }
	inline Encoding_t1523322056 ** get_address_of_encoding_13() { return &___encoding_13; }
	inline void set_encoding_13(Encoding_t1523322056 * value)
	{
		___encoding_13 = value;
		Il2CppCodeGenWriteBarrier(&___encoding_13, value);
	}

	inline static int32_t get_offset_of_current_exe_path_14() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___current_exe_path_14)); }
	inline String_t* get_current_exe_path_14() const { return ___current_exe_path_14; }
	inline String_t** get_address_of_current_exe_path_14() { return &___current_exe_path_14; }
	inline void set_current_exe_path_14(String_t* value)
	{
		___current_exe_path_14 = value;
		Il2CppCodeGenWriteBarrier(&___current_exe_path_14, value);
	}

	inline static int32_t get_offset_of_physical_path_15() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___physical_path_15)); }
	inline String_t* get_physical_path_15() const { return ___physical_path_15; }
	inline String_t** get_address_of_physical_path_15() { return &___physical_path_15; }
	inline void set_physical_path_15(String_t* value)
	{
		___physical_path_15 = value;
		Il2CppCodeGenWriteBarrier(&___physical_path_15, value);
	}

	inline static int32_t get_offset_of_unescaped_path_16() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___unescaped_path_16)); }
	inline String_t* get_unescaped_path_16() const { return ___unescaped_path_16; }
	inline String_t** get_address_of_unescaped_path_16() { return &___unescaped_path_16; }
	inline void set_unescaped_path_16(String_t* value)
	{
		___unescaped_path_16 = value;
		Il2CppCodeGenWriteBarrier(&___unescaped_path_16, value);
	}

	inline static int32_t get_offset_of_path_info_17() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___path_info_17)); }
	inline String_t* get_path_info_17() const { return ___path_info_17; }
	inline String_t** get_address_of_path_info_17() { return &___path_info_17; }
	inline void set_path_info_17(String_t* value)
	{
		___path_info_17 = value;
		Il2CppCodeGenWriteBarrier(&___path_info_17, value);
	}

	inline static int32_t get_offset_of_headers_18() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___headers_18)); }
	inline WebROCollection_t4065660147 * get_headers_18() const { return ___headers_18; }
	inline WebROCollection_t4065660147 ** get_address_of_headers_18() { return &___headers_18; }
	inline void set_headers_18(WebROCollection_t4065660147 * value)
	{
		___headers_18 = value;
		Il2CppCodeGenWriteBarrier(&___headers_18, value);
	}

	inline static int32_t get_offset_of_input_stream_19() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___input_stream_19)); }
	inline Stream_t1273022909 * get_input_stream_19() const { return ___input_stream_19; }
	inline Stream_t1273022909 ** get_address_of_input_stream_19() { return &___input_stream_19; }
	inline void set_input_stream_19(Stream_t1273022909 * value)
	{
		___input_stream_19 = value;
		Il2CppCodeGenWriteBarrier(&___input_stream_19, value);
	}

	inline static int32_t get_offset_of_input_filter_20() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___input_filter_20)); }
	inline InputFilterStream_t4129443631 * get_input_filter_20() const { return ___input_filter_20; }
	inline InputFilterStream_t4129443631 ** get_address_of_input_filter_20() { return &___input_filter_20; }
	inline void set_input_filter_20(InputFilterStream_t4129443631 * value)
	{
		___input_filter_20 = value;
		Il2CppCodeGenWriteBarrier(&___input_filter_20, value);
	}

	inline static int32_t get_offset_of_filter_21() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___filter_21)); }
	inline Stream_t1273022909 * get_filter_21() const { return ___filter_21; }
	inline Stream_t1273022909 ** get_address_of_filter_21() { return &___filter_21; }
	inline void set_filter_21(Stream_t1273022909 * value)
	{
		___filter_21 = value;
		Il2CppCodeGenWriteBarrier(&___filter_21, value);
	}

	inline static int32_t get_offset_of_cookies_22() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___cookies_22)); }
	inline HttpCookieCollection_t1178559666 * get_cookies_22() const { return ___cookies_22; }
	inline HttpCookieCollection_t1178559666 ** get_address_of_cookies_22() { return &___cookies_22; }
	inline void set_cookies_22(HttpCookieCollection_t1178559666 * value)
	{
		___cookies_22 = value;
		Il2CppCodeGenWriteBarrier(&___cookies_22, value);
	}

	inline static int32_t get_offset_of_http_method_23() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___http_method_23)); }
	inline String_t* get_http_method_23() const { return ___http_method_23; }
	inline String_t** get_address_of_http_method_23() { return &___http_method_23; }
	inline void set_http_method_23(String_t* value)
	{
		___http_method_23 = value;
		Il2CppCodeGenWriteBarrier(&___http_method_23, value);
	}

	inline static int32_t get_offset_of_form_24() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___form_24)); }
	inline WebROCollection_t4065660147 * get_form_24() const { return ___form_24; }
	inline WebROCollection_t4065660147 ** get_address_of_form_24() { return &___form_24; }
	inline void set_form_24(WebROCollection_t4065660147 * value)
	{
		___form_24 = value;
		Il2CppCodeGenWriteBarrier(&___form_24, value);
	}

	inline static int32_t get_offset_of_files_25() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___files_25)); }
	inline HttpFileCollection_t355459549 * get_files_25() const { return ___files_25; }
	inline HttpFileCollection_t355459549 ** get_address_of_files_25() { return &___files_25; }
	inline void set_files_25(HttpFileCollection_t355459549 * value)
	{
		___files_25 = value;
		Il2CppCodeGenWriteBarrier(&___files_25, value);
	}

	inline static int32_t get_offset_of_server_variables_26() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___server_variables_26)); }
	inline ServerVariablesCollection_t3116558453 * get_server_variables_26() const { return ___server_variables_26; }
	inline ServerVariablesCollection_t3116558453 ** get_address_of_server_variables_26() { return &___server_variables_26; }
	inline void set_server_variables_26(ServerVariablesCollection_t3116558453 * value)
	{
		___server_variables_26 = value;
		Il2CppCodeGenWriteBarrier(&___server_variables_26, value);
	}

	inline static int32_t get_offset_of_request_type_27() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___request_type_27)); }
	inline String_t* get_request_type_27() const { return ___request_type_27; }
	inline String_t** get_address_of_request_type_27() { return &___request_type_27; }
	inline void set_request_type_27(String_t* value)
	{
		___request_type_27 = value;
		Il2CppCodeGenWriteBarrier(&___request_type_27, value);
	}

	inline static int32_t get_offset_of_user_languages_28() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___user_languages_28)); }
	inline StringU5BU5D_t1281789340* get_user_languages_28() const { return ___user_languages_28; }
	inline StringU5BU5D_t1281789340** get_address_of_user_languages_28() { return &___user_languages_28; }
	inline void set_user_languages_28(StringU5BU5D_t1281789340* value)
	{
		___user_languages_28 = value;
		Il2CppCodeGenWriteBarrier(&___user_languages_28, value);
	}

	inline static int32_t get_offset_of_cached_url_29() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___cached_url_29)); }
	inline Uri_t100236324 * get_cached_url_29() const { return ___cached_url_29; }
	inline Uri_t100236324 ** get_address_of_cached_url_29() { return &___cached_url_29; }
	inline void set_cached_url_29(Uri_t100236324 * value)
	{
		___cached_url_29 = value;
		Il2CppCodeGenWriteBarrier(&___cached_url_29, value);
	}

	inline static int32_t get_offset_of_request_file_30() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___request_file_30)); }
	inline TempFileStream_t420614451 * get_request_file_30() const { return ___request_file_30; }
	inline TempFileStream_t420614451 ** get_address_of_request_file_30() { return &___request_file_30; }
	inline void set_request_file_30(TempFileStream_t420614451 * value)
	{
		___request_file_30 = value;
		Il2CppCodeGenWriteBarrier(&___request_file_30, value);
	}

	inline static int32_t get_offset_of_validate_cookies_32() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___validate_cookies_32)); }
	inline bool get_validate_cookies_32() const { return ___validate_cookies_32; }
	inline bool* get_address_of_validate_cookies_32() { return &___validate_cookies_32; }
	inline void set_validate_cookies_32(bool value)
	{
		___validate_cookies_32 = value;
	}

	inline static int32_t get_offset_of_validate_query_string_33() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___validate_query_string_33)); }
	inline bool get_validate_query_string_33() const { return ___validate_query_string_33; }
	inline bool* get_address_of_validate_query_string_33() { return &___validate_query_string_33; }
	inline void set_validate_query_string_33(bool value)
	{
		___validate_query_string_33 = value;
	}

	inline static int32_t get_offset_of_validate_form_34() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___validate_form_34)); }
	inline bool get_validate_form_34() const { return ___validate_form_34; }
	inline bool* get_address_of_validate_form_34() { return &___validate_form_34; }
	inline void set_validate_form_34(bool value)
	{
		___validate_form_34 = value;
	}

	inline static int32_t get_offset_of_checked_cookies_35() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___checked_cookies_35)); }
	inline bool get_checked_cookies_35() const { return ___checked_cookies_35; }
	inline bool* get_address_of_checked_cookies_35() { return &___checked_cookies_35; }
	inline void set_checked_cookies_35(bool value)
	{
		___checked_cookies_35 = value;
	}

	inline static int32_t get_offset_of_checked_query_string_36() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___checked_query_string_36)); }
	inline bool get_checked_query_string_36() const { return ___checked_query_string_36; }
	inline bool* get_address_of_checked_query_string_36() { return &___checked_query_string_36; }
	inline void set_checked_query_string_36(bool value)
	{
		___checked_query_string_36 = value;
	}

	inline static int32_t get_offset_of_checked_form_37() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260, ___checked_form_37)); }
	inline bool get_checked_form_37() const { return ___checked_form_37; }
	inline bool* get_address_of_checked_form_37() { return &___checked_form_37; }
	inline void set_checked_form_37(bool value)
	{
		___checked_form_37 = value;
	}
};

struct HttpRequest_t809700260_StaticFields
{
public:
	// System.Net.IPAddress[] System.Web.HttpRequest::host_addresses
	IPAddressU5BU5D_t596328627* ___host_addresses_31;
	// System.Web.Configuration.UrlMappingCollection System.Web.HttpRequest::urlMappings
	UrlMappingCollection_t3845908325 * ___urlMappings_38;
	// System.Char[] System.Web.HttpRequest::queryTrimChars
	CharU5BU5D_t3528271667* ___queryTrimChars_39;

public:
	inline static int32_t get_offset_of_host_addresses_31() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260_StaticFields, ___host_addresses_31)); }
	inline IPAddressU5BU5D_t596328627* get_host_addresses_31() const { return ___host_addresses_31; }
	inline IPAddressU5BU5D_t596328627** get_address_of_host_addresses_31() { return &___host_addresses_31; }
	inline void set_host_addresses_31(IPAddressU5BU5D_t596328627* value)
	{
		___host_addresses_31 = value;
		Il2CppCodeGenWriteBarrier(&___host_addresses_31, value);
	}

	inline static int32_t get_offset_of_urlMappings_38() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260_StaticFields, ___urlMappings_38)); }
	inline UrlMappingCollection_t3845908325 * get_urlMappings_38() const { return ___urlMappings_38; }
	inline UrlMappingCollection_t3845908325 ** get_address_of_urlMappings_38() { return &___urlMappings_38; }
	inline void set_urlMappings_38(UrlMappingCollection_t3845908325 * value)
	{
		___urlMappings_38 = value;
		Il2CppCodeGenWriteBarrier(&___urlMappings_38, value);
	}

	inline static int32_t get_offset_of_queryTrimChars_39() { return static_cast<int32_t>(offsetof(HttpRequest_t809700260_StaticFields, ___queryTrimChars_39)); }
	inline CharU5BU5D_t3528271667* get_queryTrimChars_39() const { return ___queryTrimChars_39; }
	inline CharU5BU5D_t3528271667** get_address_of_queryTrimChars_39() { return &___queryTrimChars_39; }
	inline void set_queryTrimChars_39(CharU5BU5D_t3528271667* value)
	{
		___queryTrimChars_39 = value;
		Il2CppCodeGenWriteBarrier(&___queryTrimChars_39, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
