﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XmlReader3121518892.h"
#include "System_Xml_System_Xml_ReadState944984020.h"

// System.String
struct String_t;
// System.Xml.XmlNameTable
struct XmlNameTable_t71772148;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.DummyStateXmlReader
struct  DummyStateXmlReader_t3823067943  : public XmlReader_t3121518892
{
public:
	// System.String System.Xml.DummyStateXmlReader::base_uri
	String_t* ___base_uri_3;
	// System.Xml.XmlNameTable System.Xml.DummyStateXmlReader::name_table
	XmlNameTable_t71772148 * ___name_table_4;
	// System.Xml.ReadState System.Xml.DummyStateXmlReader::read_state
	int32_t ___read_state_5;

public:
	inline static int32_t get_offset_of_base_uri_3() { return static_cast<int32_t>(offsetof(DummyStateXmlReader_t3823067943, ___base_uri_3)); }
	inline String_t* get_base_uri_3() const { return ___base_uri_3; }
	inline String_t** get_address_of_base_uri_3() { return &___base_uri_3; }
	inline void set_base_uri_3(String_t* value)
	{
		___base_uri_3 = value;
		Il2CppCodeGenWriteBarrier(&___base_uri_3, value);
	}

	inline static int32_t get_offset_of_name_table_4() { return static_cast<int32_t>(offsetof(DummyStateXmlReader_t3823067943, ___name_table_4)); }
	inline XmlNameTable_t71772148 * get_name_table_4() const { return ___name_table_4; }
	inline XmlNameTable_t71772148 ** get_address_of_name_table_4() { return &___name_table_4; }
	inline void set_name_table_4(XmlNameTable_t71772148 * value)
	{
		___name_table_4 = value;
		Il2CppCodeGenWriteBarrier(&___name_table_4, value);
	}

	inline static int32_t get_offset_of_read_state_5() { return static_cast<int32_t>(offsetof(DummyStateXmlReader_t3823067943, ___read_state_5)); }
	inline int32_t get_read_state_5() const { return ___read_state_5; }
	inline int32_t* get_address_of_read_state_5() { return &___read_state_5; }
	inline void set_read_state_5(int32_t value)
	{
		___read_state_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
