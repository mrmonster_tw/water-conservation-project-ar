﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Mono_CSharp_CSharpCodeGenerator4201673143.h"

// System.String
struct String_t;
// System.Threading.Mutex
struct Mutex_t3066672582;
// System.Collections.Specialized.StringCollection
struct StringCollection_t167406615;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.CSharp.CSharpCodeCompiler
struct  CSharpCodeCompiler_t3305382017  : public CSharpCodeGenerator_t4201673143
{
public:
	// System.Threading.Mutex Mono.CSharp.CSharpCodeCompiler::mcsOutMutex
	Mutex_t3066672582 * ___mcsOutMutex_13;
	// System.Collections.Specialized.StringCollection Mono.CSharp.CSharpCodeCompiler::mcsOutput
	StringCollection_t167406615 * ___mcsOutput_14;

public:
	inline static int32_t get_offset_of_mcsOutMutex_13() { return static_cast<int32_t>(offsetof(CSharpCodeCompiler_t3305382017, ___mcsOutMutex_13)); }
	inline Mutex_t3066672582 * get_mcsOutMutex_13() const { return ___mcsOutMutex_13; }
	inline Mutex_t3066672582 ** get_address_of_mcsOutMutex_13() { return &___mcsOutMutex_13; }
	inline void set_mcsOutMutex_13(Mutex_t3066672582 * value)
	{
		___mcsOutMutex_13 = value;
		Il2CppCodeGenWriteBarrier(&___mcsOutMutex_13, value);
	}

	inline static int32_t get_offset_of_mcsOutput_14() { return static_cast<int32_t>(offsetof(CSharpCodeCompiler_t3305382017, ___mcsOutput_14)); }
	inline StringCollection_t167406615 * get_mcsOutput_14() const { return ___mcsOutput_14; }
	inline StringCollection_t167406615 ** get_address_of_mcsOutput_14() { return &___mcsOutput_14; }
	inline void set_mcsOutput_14(StringCollection_t167406615 * value)
	{
		___mcsOutput_14 = value;
		Il2CppCodeGenWriteBarrier(&___mcsOutput_14, value);
	}
};

struct CSharpCodeCompiler_t3305382017_StaticFields
{
public:
	// System.String Mono.CSharp.CSharpCodeCompiler::windowsMcsPath
	String_t* ___windowsMcsPath_11;
	// System.String Mono.CSharp.CSharpCodeCompiler::windowsMonoPath
	String_t* ___windowsMonoPath_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.CSharp.CSharpCodeCompiler::<>f__switch$map1
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map1_15;

public:
	inline static int32_t get_offset_of_windowsMcsPath_11() { return static_cast<int32_t>(offsetof(CSharpCodeCompiler_t3305382017_StaticFields, ___windowsMcsPath_11)); }
	inline String_t* get_windowsMcsPath_11() const { return ___windowsMcsPath_11; }
	inline String_t** get_address_of_windowsMcsPath_11() { return &___windowsMcsPath_11; }
	inline void set_windowsMcsPath_11(String_t* value)
	{
		___windowsMcsPath_11 = value;
		Il2CppCodeGenWriteBarrier(&___windowsMcsPath_11, value);
	}

	inline static int32_t get_offset_of_windowsMonoPath_12() { return static_cast<int32_t>(offsetof(CSharpCodeCompiler_t3305382017_StaticFields, ___windowsMonoPath_12)); }
	inline String_t* get_windowsMonoPath_12() const { return ___windowsMonoPath_12; }
	inline String_t** get_address_of_windowsMonoPath_12() { return &___windowsMonoPath_12; }
	inline void set_windowsMonoPath_12(String_t* value)
	{
		___windowsMonoPath_12 = value;
		Il2CppCodeGenWriteBarrier(&___windowsMonoPath_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_15() { return static_cast<int32_t>(offsetof(CSharpCodeCompiler_t3305382017_StaticFields, ___U3CU3Ef__switchU24map1_15)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map1_15() const { return ___U3CU3Ef__switchU24map1_15; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map1_15() { return &___U3CU3Ef__switchU24map1_15; }
	inline void set_U3CU3Ef__switchU24map1_15(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map1_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
