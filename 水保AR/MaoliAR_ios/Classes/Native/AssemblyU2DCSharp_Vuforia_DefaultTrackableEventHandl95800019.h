﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t1113559212;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DefaultTrackableEventHandler
struct  DefaultTrackableEventHandler_t95800019  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Events.UnityEvent Vuforia.DefaultTrackableEventHandler::TrackingFound
	UnityEvent_t2581268647 * ___TrackingFound_2;
	// UnityEngine.Events.UnityEvent Vuforia.DefaultTrackableEventHandler::TrackingLost
	UnityEvent_t2581268647 * ___TrackingLost_3;
	// Vuforia.TrackableBehaviour Vuforia.DefaultTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t1113559212 * ___mTrackableBehaviour_4;

public:
	inline static int32_t get_offset_of_TrackingFound_2() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t95800019, ___TrackingFound_2)); }
	inline UnityEvent_t2581268647 * get_TrackingFound_2() const { return ___TrackingFound_2; }
	inline UnityEvent_t2581268647 ** get_address_of_TrackingFound_2() { return &___TrackingFound_2; }
	inline void set_TrackingFound_2(UnityEvent_t2581268647 * value)
	{
		___TrackingFound_2 = value;
		Il2CppCodeGenWriteBarrier(&___TrackingFound_2, value);
	}

	inline static int32_t get_offset_of_TrackingLost_3() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t95800019, ___TrackingLost_3)); }
	inline UnityEvent_t2581268647 * get_TrackingLost_3() const { return ___TrackingLost_3; }
	inline UnityEvent_t2581268647 ** get_address_of_TrackingLost_3() { return &___TrackingLost_3; }
	inline void set_TrackingLost_3(UnityEvent_t2581268647 * value)
	{
		___TrackingLost_3 = value;
		Il2CppCodeGenWriteBarrier(&___TrackingLost_3, value);
	}

	inline static int32_t get_offset_of_mTrackableBehaviour_4() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t95800019, ___mTrackableBehaviour_4)); }
	inline TrackableBehaviour_t1113559212 * get_mTrackableBehaviour_4() const { return ___mTrackableBehaviour_4; }
	inline TrackableBehaviour_t1113559212 ** get_address_of_mTrackableBehaviour_4() { return &___mTrackableBehaviour_4; }
	inline void set_mTrackableBehaviour_4(TrackableBehaviour_t1113559212 * value)
	{
		___mTrackableBehaviour_4 = value;
		Il2CppCodeGenWriteBarrier(&___mTrackableBehaviour_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
