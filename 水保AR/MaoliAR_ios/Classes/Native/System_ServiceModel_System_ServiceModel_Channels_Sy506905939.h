﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_S1438243711.h"

// System.ServiceModel.Channels.SymmetricSecurityBindingElement
struct SymmetricSecurityBindingElement_t3733530694;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.SymmetricSecurityCapabilities
struct  SymmetricSecurityCapabilities_t506905939  : public SecurityCapabilities_t1438243711
{
public:
	// System.ServiceModel.Channels.SymmetricSecurityBindingElement System.ServiceModel.Channels.SymmetricSecurityCapabilities::element
	SymmetricSecurityBindingElement_t3733530694 * ___element_0;

public:
	inline static int32_t get_offset_of_element_0() { return static_cast<int32_t>(offsetof(SymmetricSecurityCapabilities_t506905939, ___element_0)); }
	inline SymmetricSecurityBindingElement_t3733530694 * get_element_0() const { return ___element_0; }
	inline SymmetricSecurityBindingElement_t3733530694 ** get_address_of_element_0() { return &___element_0; }
	inline void set_element_0(SymmetricSecurityBindingElement_t3733530694 * value)
	{
		___element_0 = value;
		Il2CppCodeGenWriteBarrier(&___element_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
