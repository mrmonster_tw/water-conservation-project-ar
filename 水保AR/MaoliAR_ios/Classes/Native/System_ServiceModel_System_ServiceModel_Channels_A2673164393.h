﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_S1737011943.h"
#include "System_ServiceModel_System_ServiceModel_Security_M4027609129.h"

// System.ServiceModel.Security.Tokens.SecurityTokenParameters
struct SecurityTokenParameters_t2868958784;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.AsymmetricSecurityBindingElement
struct  AsymmetricSecurityBindingElement_t2673164393  : public SecurityBindingElement_t1737011943
{
public:
	// System.ServiceModel.Security.MessageProtectionOrder System.ServiceModel.Channels.AsymmetricSecurityBindingElement::msg_protection_order
	int32_t ___msg_protection_order_11;
	// System.ServiceModel.Security.Tokens.SecurityTokenParameters System.ServiceModel.Channels.AsymmetricSecurityBindingElement::initiator_token_params
	SecurityTokenParameters_t2868958784 * ___initiator_token_params_12;
	// System.ServiceModel.Security.Tokens.SecurityTokenParameters System.ServiceModel.Channels.AsymmetricSecurityBindingElement::recipient_token_params
	SecurityTokenParameters_t2868958784 * ___recipient_token_params_13;
	// System.Boolean System.ServiceModel.Channels.AsymmetricSecurityBindingElement::allow_serialized_sign
	bool ___allow_serialized_sign_14;
	// System.Boolean System.ServiceModel.Channels.AsymmetricSecurityBindingElement::require_sig_confirm
	bool ___require_sig_confirm_15;

public:
	inline static int32_t get_offset_of_msg_protection_order_11() { return static_cast<int32_t>(offsetof(AsymmetricSecurityBindingElement_t2673164393, ___msg_protection_order_11)); }
	inline int32_t get_msg_protection_order_11() const { return ___msg_protection_order_11; }
	inline int32_t* get_address_of_msg_protection_order_11() { return &___msg_protection_order_11; }
	inline void set_msg_protection_order_11(int32_t value)
	{
		___msg_protection_order_11 = value;
	}

	inline static int32_t get_offset_of_initiator_token_params_12() { return static_cast<int32_t>(offsetof(AsymmetricSecurityBindingElement_t2673164393, ___initiator_token_params_12)); }
	inline SecurityTokenParameters_t2868958784 * get_initiator_token_params_12() const { return ___initiator_token_params_12; }
	inline SecurityTokenParameters_t2868958784 ** get_address_of_initiator_token_params_12() { return &___initiator_token_params_12; }
	inline void set_initiator_token_params_12(SecurityTokenParameters_t2868958784 * value)
	{
		___initiator_token_params_12 = value;
		Il2CppCodeGenWriteBarrier(&___initiator_token_params_12, value);
	}

	inline static int32_t get_offset_of_recipient_token_params_13() { return static_cast<int32_t>(offsetof(AsymmetricSecurityBindingElement_t2673164393, ___recipient_token_params_13)); }
	inline SecurityTokenParameters_t2868958784 * get_recipient_token_params_13() const { return ___recipient_token_params_13; }
	inline SecurityTokenParameters_t2868958784 ** get_address_of_recipient_token_params_13() { return &___recipient_token_params_13; }
	inline void set_recipient_token_params_13(SecurityTokenParameters_t2868958784 * value)
	{
		___recipient_token_params_13 = value;
		Il2CppCodeGenWriteBarrier(&___recipient_token_params_13, value);
	}

	inline static int32_t get_offset_of_allow_serialized_sign_14() { return static_cast<int32_t>(offsetof(AsymmetricSecurityBindingElement_t2673164393, ___allow_serialized_sign_14)); }
	inline bool get_allow_serialized_sign_14() const { return ___allow_serialized_sign_14; }
	inline bool* get_address_of_allow_serialized_sign_14() { return &___allow_serialized_sign_14; }
	inline void set_allow_serialized_sign_14(bool value)
	{
		___allow_serialized_sign_14 = value;
	}

	inline static int32_t get_offset_of_require_sig_confirm_15() { return static_cast<int32_t>(offsetof(AsymmetricSecurityBindingElement_t2673164393, ___require_sig_confirm_15)); }
	inline bool get_require_sig_confirm_15() const { return ___require_sig_confirm_15; }
	inline bool* get_address_of_require_sig_confirm_15() { return &___require_sig_confirm_15; }
	inline void set_require_sig_confirm_15(bool value)
	{
		___require_sig_confirm_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
