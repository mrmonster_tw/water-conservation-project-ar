﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NetworkCheck
struct  NetworkCheck_t4038460763  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Events.UnityEvent NetworkCheck::Offline__Event
	UnityEvent_t2581268647 * ___Offline__Event_2;
	// UnityEngine.Events.UnityEvent NetworkCheck::Online_Event
	UnityEvent_t2581268647 * ___Online_Event_3;

public:
	inline static int32_t get_offset_of_Offline__Event_2() { return static_cast<int32_t>(offsetof(NetworkCheck_t4038460763, ___Offline__Event_2)); }
	inline UnityEvent_t2581268647 * get_Offline__Event_2() const { return ___Offline__Event_2; }
	inline UnityEvent_t2581268647 ** get_address_of_Offline__Event_2() { return &___Offline__Event_2; }
	inline void set_Offline__Event_2(UnityEvent_t2581268647 * value)
	{
		___Offline__Event_2 = value;
		Il2CppCodeGenWriteBarrier(&___Offline__Event_2, value);
	}

	inline static int32_t get_offset_of_Online_Event_3() { return static_cast<int32_t>(offsetof(NetworkCheck_t4038460763, ___Online_Event_3)); }
	inline UnityEvent_t2581268647 * get_Online_Event_3() const { return ___Online_Event_3; }
	inline UnityEvent_t2581268647 ** get_address_of_Online_Event_3() { return &___Online_Event_3; }
	inline void set_Online_Event_3(UnityEvent_t2581268647 * value)
	{
		___Online_Event_3 = value;
		Il2CppCodeGenWriteBarrier(&___Online_Event_3, value);
	}
};

struct NetworkCheck_t4038460763_StaticFields
{
public:
	// System.Boolean NetworkCheck::is_connet
	bool ___is_connet_4;

public:
	inline static int32_t get_offset_of_is_connet_4() { return static_cast<int32_t>(offsetof(NetworkCheck_t4038460763_StaticFields, ___is_connet_4)); }
	inline bool get_is_connet_4() const { return ___is_connet_4; }
	inline bool* get_address_of_is_connet_4() { return &___is_connet_4; }
	inline void set_is_connet_4(bool value)
	{
		___is_connet_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
