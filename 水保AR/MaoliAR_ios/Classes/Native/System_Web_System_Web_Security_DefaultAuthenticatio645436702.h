﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_EventArgs3591816995.h"

// System.Web.HttpContext
struct HttpContext_t1969259010;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Security.DefaultAuthenticationEventArgs
struct  DefaultAuthenticationEventArgs_t645436702  : public EventArgs_t3591816995
{
public:
	// System.Web.HttpContext System.Web.Security.DefaultAuthenticationEventArgs::_context
	HttpContext_t1969259010 * ____context_1;

public:
	inline static int32_t get_offset_of__context_1() { return static_cast<int32_t>(offsetof(DefaultAuthenticationEventArgs_t645436702, ____context_1)); }
	inline HttpContext_t1969259010 * get__context_1() const { return ____context_1; }
	inline HttpContext_t1969259010 ** get_address_of__context_1() { return &____context_1; }
	inline void set__context_1(HttpContext_t1969259010 * value)
	{
		____context_1 = value;
		Il2CppCodeGenWriteBarrier(&____context_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
