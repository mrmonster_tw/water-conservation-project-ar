﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_ControlBuilder2523018631.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.CodeBuilder
struct  CodeBuilder_t1710888898  : public ControlBuilder_t2523018631
{
public:
	// System.String System.Web.UI.CodeBuilder::code
	String_t* ___code_28;
	// System.Boolean System.Web.UI.CodeBuilder::isAssign
	bool ___isAssign_29;

public:
	inline static int32_t get_offset_of_code_28() { return static_cast<int32_t>(offsetof(CodeBuilder_t1710888898, ___code_28)); }
	inline String_t* get_code_28() const { return ___code_28; }
	inline String_t** get_address_of_code_28() { return &___code_28; }
	inline void set_code_28(String_t* value)
	{
		___code_28 = value;
		Il2CppCodeGenWriteBarrier(&___code_28, value);
	}

	inline static int32_t get_offset_of_isAssign_29() { return static_cast<int32_t>(offsetof(CodeBuilder_t1710888898, ___isAssign_29)); }
	inline bool get_isAssign_29() const { return ___isAssign_29; }
	inline bool* get_address_of_isAssign_29() { return &___isAssign_29; }
	inline void set_isAssign_29(bool value)
	{
		___isAssign_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
