﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.CallbackDebugBehavior
struct  CallbackDebugBehavior_t2126994175  : public Il2CppObject
{
public:
	// System.Boolean System.ServiceModel.Description.CallbackDebugBehavior::include_exception
	bool ___include_exception_0;

public:
	inline static int32_t get_offset_of_include_exception_0() { return static_cast<int32_t>(offsetof(CallbackDebugBehavior_t2126994175, ___include_exception_0)); }
	inline bool get_include_exception_0() const { return ___include_exception_0; }
	inline bool* get_address_of_include_exception_0() { return &___include_exception_0; }
	inline void set_include_exception_0(bool value)
	{
		___include_exception_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
