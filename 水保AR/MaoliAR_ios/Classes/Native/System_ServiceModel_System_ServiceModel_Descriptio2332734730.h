﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Description.WsaEndpointReference
struct WsaEndpointReference_t4192482636;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.WspAppliesTo
struct  WspAppliesTo_t2332734730  : public Il2CppObject
{
public:
	// System.ServiceModel.Description.WsaEndpointReference System.ServiceModel.Description.WspAppliesTo::EndpointReference
	WsaEndpointReference_t4192482636 * ___EndpointReference_0;

public:
	inline static int32_t get_offset_of_EndpointReference_0() { return static_cast<int32_t>(offsetof(WspAppliesTo_t2332734730, ___EndpointReference_0)); }
	inline WsaEndpointReference_t4192482636 * get_EndpointReference_0() const { return ___EndpointReference_0; }
	inline WsaEndpointReference_t4192482636 ** get_address_of_EndpointReference_0() { return &___EndpointReference_0; }
	inline void set_EndpointReference_0(WsaEndpointReference_t4192482636 * value)
	{
		___EndpointReference_0 = value;
		Il2CppCodeGenWriteBarrier(&___EndpointReference_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
