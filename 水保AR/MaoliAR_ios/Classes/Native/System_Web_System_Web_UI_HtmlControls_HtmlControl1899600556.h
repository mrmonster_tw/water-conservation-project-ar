﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_Control3006474639.h"

// System.String
struct String_t;
// System.Web.UI.AttributeCollection
struct AttributeCollection_t3488369622;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.HtmlControls.HtmlControl
struct  HtmlControl_t1899600556  : public Control_t3006474639
{
public:
	// System.String System.Web.UI.HtmlControls.HtmlControl::_tagName
	String_t* ____tagName_28;
	// System.Web.UI.AttributeCollection System.Web.UI.HtmlControls.HtmlControl::_attributes
	AttributeCollection_t3488369622 * ____attributes_29;

public:
	inline static int32_t get_offset_of__tagName_28() { return static_cast<int32_t>(offsetof(HtmlControl_t1899600556, ____tagName_28)); }
	inline String_t* get__tagName_28() const { return ____tagName_28; }
	inline String_t** get_address_of__tagName_28() { return &____tagName_28; }
	inline void set__tagName_28(String_t* value)
	{
		____tagName_28 = value;
		Il2CppCodeGenWriteBarrier(&____tagName_28, value);
	}

	inline static int32_t get_offset_of__attributes_29() { return static_cast<int32_t>(offsetof(HtmlControl_t1899600556, ____attributes_29)); }
	inline AttributeCollection_t3488369622 * get__attributes_29() const { return ____attributes_29; }
	inline AttributeCollection_t3488369622 ** get_address_of__attributes_29() { return &____attributes_29; }
	inline void set__attributes_29(AttributeCollection_t3488369622 * value)
	{
		____attributes_29 = value;
		Il2CppCodeGenWriteBarrier(&____attributes_29, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
