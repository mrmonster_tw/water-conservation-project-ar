﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Xml.XmlQualifiedName[]
struct XmlQualifiedNameU5BU5D_t1471530361;
// System.ServiceModel.Security.MessagePartSpecification
struct MessagePartSpecification_t2368875147;
// System.Collections.Generic.IList`1<System.Xml.XmlQualifiedName>
struct IList_1_t281006799;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.MessagePartSpecification
struct  MessagePartSpecification_t2368875147  : public Il2CppObject
{
public:
	// System.Boolean System.ServiceModel.Security.MessagePartSpecification::body
	bool ___body_2;
	// System.Collections.Generic.IList`1<System.Xml.XmlQualifiedName> System.ServiceModel.Security.MessagePartSpecification::header_types
	Il2CppObject* ___header_types_3;

public:
	inline static int32_t get_offset_of_body_2() { return static_cast<int32_t>(offsetof(MessagePartSpecification_t2368875147, ___body_2)); }
	inline bool get_body_2() const { return ___body_2; }
	inline bool* get_address_of_body_2() { return &___body_2; }
	inline void set_body_2(bool value)
	{
		___body_2 = value;
	}

	inline static int32_t get_offset_of_header_types_3() { return static_cast<int32_t>(offsetof(MessagePartSpecification_t2368875147, ___header_types_3)); }
	inline Il2CppObject* get_header_types_3() const { return ___header_types_3; }
	inline Il2CppObject** get_address_of_header_types_3() { return &___header_types_3; }
	inline void set_header_types_3(Il2CppObject* value)
	{
		___header_types_3 = value;
		Il2CppCodeGenWriteBarrier(&___header_types_3, value);
	}
};

struct MessagePartSpecification_t2368875147_StaticFields
{
public:
	// System.Xml.XmlQualifiedName[] System.ServiceModel.Security.MessagePartSpecification::empty
	XmlQualifiedNameU5BU5D_t1471530361* ___empty_0;
	// System.ServiceModel.Security.MessagePartSpecification System.ServiceModel.Security.MessagePartSpecification::no_parts
	MessagePartSpecification_t2368875147 * ___no_parts_1;

public:
	inline static int32_t get_offset_of_empty_0() { return static_cast<int32_t>(offsetof(MessagePartSpecification_t2368875147_StaticFields, ___empty_0)); }
	inline XmlQualifiedNameU5BU5D_t1471530361* get_empty_0() const { return ___empty_0; }
	inline XmlQualifiedNameU5BU5D_t1471530361** get_address_of_empty_0() { return &___empty_0; }
	inline void set_empty_0(XmlQualifiedNameU5BU5D_t1471530361* value)
	{
		___empty_0 = value;
		Il2CppCodeGenWriteBarrier(&___empty_0, value);
	}

	inline static int32_t get_offset_of_no_parts_1() { return static_cast<int32_t>(offsetof(MessagePartSpecification_t2368875147_StaticFields, ___no_parts_1)); }
	inline MessagePartSpecification_t2368875147 * get_no_parts_1() const { return ___no_parts_1; }
	inline MessagePartSpecification_t2368875147 ** get_address_of_no_parts_1() { return &___no_parts_1; }
	inline void set_no_parts_1(MessagePartSpecification_t2368875147 * value)
	{
		___no_parts_1 = value;
		Il2CppCodeGenWriteBarrier(&___no_parts_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
