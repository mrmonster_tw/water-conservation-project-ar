﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "AssemblyU2DCSharp_RestFulsClass_RestFulClass_GetFun620056848.h"
#include "AssemblyU2DCSharp_RestFulsClass_RestFulClass_GetSc4269622460.h"

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestFulsClass.RestFulClass/RestFul/Datas/<Getdata>c__Iterator0
struct  U3CGetdataU3Ec__Iterator0_t3912795149  : public Il2CppObject
{
public:
	// RestFulsClass.RestFulClass/GetFuncTypes RestFulsClass.RestFulClass/RestFul/Datas/<Getdata>c__Iterator0::typemode
	int32_t ___typemode_0;
	// RestFulsClass.RestFulClass/GetSceneMode RestFulsClass.RestFulClass/RestFul/Datas/<Getdata>c__Iterator0::sceneMode
	int32_t ___sceneMode_1;
	// System.String RestFulsClass.RestFulClass/RestFul/Datas/<Getdata>c__Iterator0::<Func>__0
	String_t* ___U3CFuncU3E__0_2;
	// System.String[] RestFulsClass.RestFulClass/RestFul/Datas/<Getdata>c__Iterator0::elseStr
	StringU5BU5D_t1281789340* ___elseStr_3;
	// System.String RestFulsClass.RestFulClass/RestFul/Datas/<Getdata>c__Iterator0::<requstUrl>__0
	String_t* ___U3CrequstUrlU3E__0_4;
	// System.Object RestFulsClass.RestFulClass/RestFul/Datas/<Getdata>c__Iterator0::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean RestFulsClass.RestFulClass/RestFul/Datas/<Getdata>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 RestFulsClass.RestFulClass/RestFul/Datas/<Getdata>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_typemode_0() { return static_cast<int32_t>(offsetof(U3CGetdataU3Ec__Iterator0_t3912795149, ___typemode_0)); }
	inline int32_t get_typemode_0() const { return ___typemode_0; }
	inline int32_t* get_address_of_typemode_0() { return &___typemode_0; }
	inline void set_typemode_0(int32_t value)
	{
		___typemode_0 = value;
	}

	inline static int32_t get_offset_of_sceneMode_1() { return static_cast<int32_t>(offsetof(U3CGetdataU3Ec__Iterator0_t3912795149, ___sceneMode_1)); }
	inline int32_t get_sceneMode_1() const { return ___sceneMode_1; }
	inline int32_t* get_address_of_sceneMode_1() { return &___sceneMode_1; }
	inline void set_sceneMode_1(int32_t value)
	{
		___sceneMode_1 = value;
	}

	inline static int32_t get_offset_of_U3CFuncU3E__0_2() { return static_cast<int32_t>(offsetof(U3CGetdataU3Ec__Iterator0_t3912795149, ___U3CFuncU3E__0_2)); }
	inline String_t* get_U3CFuncU3E__0_2() const { return ___U3CFuncU3E__0_2; }
	inline String_t** get_address_of_U3CFuncU3E__0_2() { return &___U3CFuncU3E__0_2; }
	inline void set_U3CFuncU3E__0_2(String_t* value)
	{
		___U3CFuncU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CFuncU3E__0_2, value);
	}

	inline static int32_t get_offset_of_elseStr_3() { return static_cast<int32_t>(offsetof(U3CGetdataU3Ec__Iterator0_t3912795149, ___elseStr_3)); }
	inline StringU5BU5D_t1281789340* get_elseStr_3() const { return ___elseStr_3; }
	inline StringU5BU5D_t1281789340** get_address_of_elseStr_3() { return &___elseStr_3; }
	inline void set_elseStr_3(StringU5BU5D_t1281789340* value)
	{
		___elseStr_3 = value;
		Il2CppCodeGenWriteBarrier(&___elseStr_3, value);
	}

	inline static int32_t get_offset_of_U3CrequstUrlU3E__0_4() { return static_cast<int32_t>(offsetof(U3CGetdataU3Ec__Iterator0_t3912795149, ___U3CrequstUrlU3E__0_4)); }
	inline String_t* get_U3CrequstUrlU3E__0_4() const { return ___U3CrequstUrlU3E__0_4; }
	inline String_t** get_address_of_U3CrequstUrlU3E__0_4() { return &___U3CrequstUrlU3E__0_4; }
	inline void set_U3CrequstUrlU3E__0_4(String_t* value)
	{
		___U3CrequstUrlU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrequstUrlU3E__0_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CGetdataU3Ec__Iterator0_t3912795149, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CGetdataU3Ec__Iterator0_t3912795149, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CGetdataU3Ec__Iterator0_t3912795149, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
