﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.EnvelopeVersion
struct EnvelopeVersion_t2487833488;
// System.ServiceModel.Channels.AddressingVersion
struct AddressingVersion_t2257583243;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.MessageVersion
struct  MessageVersion_t1958933598  : public Il2CppObject
{
public:
	// System.ServiceModel.EnvelopeVersion System.ServiceModel.Channels.MessageVersion::envelope
	EnvelopeVersion_t2487833488 * ___envelope_0;
	// System.ServiceModel.Channels.AddressingVersion System.ServiceModel.Channels.MessageVersion::addressing
	AddressingVersion_t2257583243 * ___addressing_1;

public:
	inline static int32_t get_offset_of_envelope_0() { return static_cast<int32_t>(offsetof(MessageVersion_t1958933598, ___envelope_0)); }
	inline EnvelopeVersion_t2487833488 * get_envelope_0() const { return ___envelope_0; }
	inline EnvelopeVersion_t2487833488 ** get_address_of_envelope_0() { return &___envelope_0; }
	inline void set_envelope_0(EnvelopeVersion_t2487833488 * value)
	{
		___envelope_0 = value;
		Il2CppCodeGenWriteBarrier(&___envelope_0, value);
	}

	inline static int32_t get_offset_of_addressing_1() { return static_cast<int32_t>(offsetof(MessageVersion_t1958933598, ___addressing_1)); }
	inline AddressingVersion_t2257583243 * get_addressing_1() const { return ___addressing_1; }
	inline AddressingVersion_t2257583243 ** get_address_of_addressing_1() { return &___addressing_1; }
	inline void set_addressing_1(AddressingVersion_t2257583243 * value)
	{
		___addressing_1 = value;
		Il2CppCodeGenWriteBarrier(&___addressing_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
