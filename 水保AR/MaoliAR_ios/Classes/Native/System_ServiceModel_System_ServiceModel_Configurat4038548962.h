﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Configurat3588124660.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.IssuedTokenParametersEndpointAddressElement
struct  IssuedTokenParametersEndpointAddressElement_t4038548962  : public EndpointAddressElementBase_t3588124660
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.IssuedTokenParametersEndpointAddressElement::_properties
	ConfigurationPropertyCollection_t2852175726 * ____properties_14;

public:
	inline static int32_t get_offset_of__properties_14() { return static_cast<int32_t>(offsetof(IssuedTokenParametersEndpointAddressElement_t4038548962, ____properties_14)); }
	inline ConfigurationPropertyCollection_t2852175726 * get__properties_14() const { return ____properties_14; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of__properties_14() { return &____properties_14; }
	inline void set__properties_14(ConfigurationPropertyCollection_t2852175726 * value)
	{
		____properties_14 = value;
		Il2CppCodeGenWriteBarrier(&____properties_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
