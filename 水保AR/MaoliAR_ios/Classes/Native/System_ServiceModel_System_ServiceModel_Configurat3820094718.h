﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.X509RecipientCertificateClientElement
struct  X509RecipientCertificateClientElement_t3820094718  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct X509RecipientCertificateClientElement_t3820094718_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.X509RecipientCertificateClientElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.X509RecipientCertificateClientElement::authentication
	ConfigurationProperty_t3590861854 * ___authentication_14;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.X509RecipientCertificateClientElement::default_certificate
	ConfigurationProperty_t3590861854 * ___default_certificate_15;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.X509RecipientCertificateClientElement::scoped_certificates
	ConfigurationProperty_t3590861854 * ___scoped_certificates_16;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(X509RecipientCertificateClientElement_t3820094718_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier(&___properties_13, value);
	}

	inline static int32_t get_offset_of_authentication_14() { return static_cast<int32_t>(offsetof(X509RecipientCertificateClientElement_t3820094718_StaticFields, ___authentication_14)); }
	inline ConfigurationProperty_t3590861854 * get_authentication_14() const { return ___authentication_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_authentication_14() { return &___authentication_14; }
	inline void set_authentication_14(ConfigurationProperty_t3590861854 * value)
	{
		___authentication_14 = value;
		Il2CppCodeGenWriteBarrier(&___authentication_14, value);
	}

	inline static int32_t get_offset_of_default_certificate_15() { return static_cast<int32_t>(offsetof(X509RecipientCertificateClientElement_t3820094718_StaticFields, ___default_certificate_15)); }
	inline ConfigurationProperty_t3590861854 * get_default_certificate_15() const { return ___default_certificate_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_default_certificate_15() { return &___default_certificate_15; }
	inline void set_default_certificate_15(ConfigurationProperty_t3590861854 * value)
	{
		___default_certificate_15 = value;
		Il2CppCodeGenWriteBarrier(&___default_certificate_15, value);
	}

	inline static int32_t get_offset_of_scoped_certificates_16() { return static_cast<int32_t>(offsetof(X509RecipientCertificateClientElement_t3820094718_StaticFields, ___scoped_certificates_16)); }
	inline ConfigurationProperty_t3590861854 * get_scoped_certificates_16() const { return ___scoped_certificates_16; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_scoped_certificates_16() { return &___scoped_certificates_16; }
	inline void set_scoped_certificates_16(ConfigurationProperty_t3590861854 * value)
	{
		___scoped_certificates_16 = value;
		Il2CppCodeGenWriteBarrier(&___scoped_certificates_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
