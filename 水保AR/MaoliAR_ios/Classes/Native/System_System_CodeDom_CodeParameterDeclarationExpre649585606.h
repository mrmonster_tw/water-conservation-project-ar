﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeExpression2166265795.h"
#include "System_System_CodeDom_FieldDirection1550335646.h"

// System.CodeDom.CodeAttributeDeclarationCollection
struct CodeAttributeDeclarationCollection_t3890917538;
// System.String
struct String_t;
// System.CodeDom.CodeTypeReference
struct CodeTypeReference_t3809997434;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeParameterDeclarationExpression
struct  CodeParameterDeclarationExpression_t649585606  : public CodeExpression_t2166265795
{
public:
	// System.CodeDom.CodeAttributeDeclarationCollection System.CodeDom.CodeParameterDeclarationExpression::customAttributes
	CodeAttributeDeclarationCollection_t3890917538 * ___customAttributes_1;
	// System.CodeDom.FieldDirection System.CodeDom.CodeParameterDeclarationExpression::direction
	int32_t ___direction_2;
	// System.String System.CodeDom.CodeParameterDeclarationExpression::name
	String_t* ___name_3;
	// System.CodeDom.CodeTypeReference System.CodeDom.CodeParameterDeclarationExpression::type
	CodeTypeReference_t3809997434 * ___type_4;

public:
	inline static int32_t get_offset_of_customAttributes_1() { return static_cast<int32_t>(offsetof(CodeParameterDeclarationExpression_t649585606, ___customAttributes_1)); }
	inline CodeAttributeDeclarationCollection_t3890917538 * get_customAttributes_1() const { return ___customAttributes_1; }
	inline CodeAttributeDeclarationCollection_t3890917538 ** get_address_of_customAttributes_1() { return &___customAttributes_1; }
	inline void set_customAttributes_1(CodeAttributeDeclarationCollection_t3890917538 * value)
	{
		___customAttributes_1 = value;
		Il2CppCodeGenWriteBarrier(&___customAttributes_1, value);
	}

	inline static int32_t get_offset_of_direction_2() { return static_cast<int32_t>(offsetof(CodeParameterDeclarationExpression_t649585606, ___direction_2)); }
	inline int32_t get_direction_2() const { return ___direction_2; }
	inline int32_t* get_address_of_direction_2() { return &___direction_2; }
	inline void set_direction_2(int32_t value)
	{
		___direction_2 = value;
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(CodeParameterDeclarationExpression_t649585606, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier(&___name_3, value);
	}

	inline static int32_t get_offset_of_type_4() { return static_cast<int32_t>(offsetof(CodeParameterDeclarationExpression_t649585606, ___type_4)); }
	inline CodeTypeReference_t3809997434 * get_type_4() const { return ___type_4; }
	inline CodeTypeReference_t3809997434 ** get_address_of_type_4() { return &___type_4; }
	inline void set_type_4(CodeTypeReference_t3809997434 * value)
	{
		___type_4 = value;
		Il2CppCodeGenWriteBarrier(&___type_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
