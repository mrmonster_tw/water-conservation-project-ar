﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XPath_Expression1452783009.h"

// Mono.Xml.Xsl.Operations.XslGeneralVariable
struct XslGeneralVariable_t1456221871;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XPathVariableBinding
struct  XPathVariableBinding_t3684345436  : public Expression_t1452783009
{
public:
	// Mono.Xml.Xsl.Operations.XslGeneralVariable Mono.Xml.Xsl.Operations.XPathVariableBinding::v
	XslGeneralVariable_t1456221871 * ___v_0;

public:
	inline static int32_t get_offset_of_v_0() { return static_cast<int32_t>(offsetof(XPathVariableBinding_t3684345436, ___v_0)); }
	inline XslGeneralVariable_t1456221871 * get_v_0() const { return ___v_0; }
	inline XslGeneralVariable_t1456221871 ** get_address_of_v_0() { return &___v_0; }
	inline void set_v_0(XslGeneralVariable_t1456221871 * value)
	{
		___v_0 = value;
		Il2CppCodeGenWriteBarrier(&___v_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
