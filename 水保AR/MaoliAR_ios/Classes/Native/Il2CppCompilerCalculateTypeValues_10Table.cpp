﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Text_EncoderFallback1188251036.h"
#include "mscorlib_System_Text_EncoderFallbackBuffer3523102303.h"
#include "mscorlib_System_Text_EncoderFallbackException2773771926.h"
#include "mscorlib_System_Text_EncoderReplacementFallback1623206753.h"
#include "mscorlib_System_Text_EncoderReplacementFallbackBuf1863187162.h"
#include "mscorlib_System_Text_Encoding1523322056.h"
#include "mscorlib_System_Text_Encoding_ForwardingDecoder3503728819.h"
#include "mscorlib_System_Text_Latin1Encoding950549005.h"
#include "mscorlib_System_Text_StringBuilder1712802186.h"
#include "mscorlib_System_Text_UnicodeEncoding1959134050.h"
#include "mscorlib_System_Text_UnicodeEncoding_UnicodeDecoder872550992.h"
#include "mscorlib_System_Text_UTF7Encoding2644108479.h"
#include "mscorlib_System_Text_UTF7Encoding_UTF7Decoder2247208115.h"
#include "mscorlib_System_Text_UTF8Encoding3956466879.h"
#include "mscorlib_System_Text_UTF8Encoding_UTF8Decoder2159214862.h"
#include "mscorlib_System_Text_UTF32Encoding312252005.h"
#include "mscorlib_System_Text_UTF32Encoding_UTF32Decoder635925672.h"
#include "mscorlib_System_Threading_AutoResetEvent1333520283.h"
#include "mscorlib_System_Threading_CompressedStack1202932761.h"
#include "mscorlib_System_Threading_EventResetMode3817241503.h"
#include "mscorlib_System_Threading_EventWaitHandle777845177.h"
#include "mscorlib_System_Threading_ExecutionContext1748372627.h"
#include "mscorlib_System_Threading_Interlocked2273387594.h"
#include "mscorlib_System_Threading_LockQueue2679652224.h"
#include "mscorlib_System_Threading_ManualResetEvent451242010.h"
#include "mscorlib_System_Threading_Monitor2197244473.h"
#include "mscorlib_System_Threading_Mutex3066672582.h"
#include "mscorlib_System_Threading_NativeEventCalls570794723.h"
#include "mscorlib_System_Threading_ReaderWriterLock367846299.h"
#include "mscorlib_System_Threading_RegisteredWaitHandle1529538454.h"
#include "mscorlib_System_Threading_SynchronizationLockExcept841761767.h"
#include "mscorlib_System_Threading_Thread2300836069.h"
#include "mscorlib_System_Threading_ThreadAbortException4074510458.h"
#include "mscorlib_System_Threading_ThreadInterruptedExcepti3240955163.h"
#include "mscorlib_System_Threading_ThreadPool2177989550.h"
#include "mscorlib_System_Threading_ThreadState2533302383.h"
#include "mscorlib_System_Threading_ThreadStateException3003788475.h"
#include "mscorlib_System_Threading_Timer716671026.h"
#include "mscorlib_System_Threading_Timer_TimerComparer2774265395.h"
#include "mscorlib_System_Threading_Timer_Scheduler3215764947.h"
#include "mscorlib_System_Threading_WaitHandle1743403487.h"
#include "mscorlib_System_Collections_Generic_Link544317964.h"
#include "mscorlib_System_Collections_Generic_KeyNotFoundExc2292407383.h"
#include "mscorlib_Mono_Math_Prime_PrimalityTest1539325943.h"
#include "mscorlib_System_AppDomainInitializer682969308.h"
#include "mscorlib_System_AssemblyLoadEventHandler107971893.h"
#include "mscorlib_System_ConsoleCancelEventHandler3919940372.h"
#include "mscorlib_System_EventHandler1348719766.h"
#include "mscorlib_System_ResolveEventHandler2775508208.h"
#include "mscorlib_System_UnhandledExceptionEventHandler3101989324.h"
#include "mscorlib_System_Reflection_MemberFilter426314064.h"
#include "mscorlib_System_Reflection_TypeFilter2356120900.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_CrossCont387175271.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_HeaderH2878040134.h"
#include "mscorlib_System_Threading_SendOrPostCallback2750080073.h"
#include "mscorlib_System_Threading_ThreadStart1006689297.h"
#include "mscorlib_System_Threading_TimerCallback1438585625.h"
#include "mscorlib_System_Threading_WaitCallback2448485498.h"
#include "mscorlib_System_Threading_WaitOrTimerCallback1973723282.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E3057255361.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra520724128.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr2469145093.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3254766644.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr4289081651.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr2732071528.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1704471045.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3652892010.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1337922363.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra499776625.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1000 = { sizeof (EncoderFallback_t1188251036), -1, sizeof(EncoderFallback_t1188251036_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1000[3] = 
{
	EncoderFallback_t1188251036_StaticFields::get_offset_of_exception_fallback_0(),
	EncoderFallback_t1188251036_StaticFields::get_offset_of_replacement_fallback_1(),
	EncoderFallback_t1188251036_StaticFields::get_offset_of_standard_safe_fallback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1001 = { sizeof (EncoderFallbackBuffer_t3523102303), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1002 = { sizeof (EncoderFallbackException_t2773771926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1002[4] = 
{
	EncoderFallbackException_t2773771926::get_offset_of_char_unknown_13(),
	EncoderFallbackException_t2773771926::get_offset_of_char_unknown_high_14(),
	EncoderFallbackException_t2773771926::get_offset_of_char_unknown_low_15(),
	EncoderFallbackException_t2773771926::get_offset_of_index_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1003 = { sizeof (EncoderReplacementFallback_t1623206753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1003[1] = 
{
	EncoderReplacementFallback_t1623206753::get_offset_of_replacement_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1004 = { sizeof (EncoderReplacementFallbackBuffer_t1863187162), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1004[3] = 
{
	EncoderReplacementFallbackBuffer_t1863187162::get_offset_of_replacement_0(),
	EncoderReplacementFallbackBuffer_t1863187162::get_offset_of_current_1(),
	EncoderReplacementFallbackBuffer_t1863187162::get_offset_of_fallback_assigned_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1005 = { sizeof (Encoding_t1523322056), -1, sizeof(Encoding_t1523322056_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1005[28] = 
{
	Encoding_t1523322056::get_offset_of_codePage_0(),
	Encoding_t1523322056::get_offset_of_windows_code_page_1(),
	Encoding_t1523322056::get_offset_of_is_readonly_2(),
	Encoding_t1523322056::get_offset_of_decoder_fallback_3(),
	Encoding_t1523322056::get_offset_of_encoder_fallback_4(),
	Encoding_t1523322056_StaticFields::get_offset_of_i18nAssembly_5(),
	Encoding_t1523322056_StaticFields::get_offset_of_i18nDisabled_6(),
	Encoding_t1523322056_StaticFields::get_offset_of_encodings_7(),
	Encoding_t1523322056::get_offset_of_body_name_8(),
	Encoding_t1523322056::get_offset_of_encoding_name_9(),
	Encoding_t1523322056::get_offset_of_header_name_10(),
	Encoding_t1523322056::get_offset_of_is_mail_news_display_11(),
	Encoding_t1523322056::get_offset_of_is_mail_news_save_12(),
	Encoding_t1523322056::get_offset_of_is_browser_save_13(),
	Encoding_t1523322056::get_offset_of_is_browser_display_14(),
	Encoding_t1523322056::get_offset_of_web_name_15(),
	Encoding_t1523322056_StaticFields::get_offset_of_asciiEncoding_16(),
	Encoding_t1523322056_StaticFields::get_offset_of_bigEndianEncoding_17(),
	Encoding_t1523322056_StaticFields::get_offset_of_defaultEncoding_18(),
	Encoding_t1523322056_StaticFields::get_offset_of_utf7Encoding_19(),
	Encoding_t1523322056_StaticFields::get_offset_of_utf8EncodingWithMarkers_20(),
	Encoding_t1523322056_StaticFields::get_offset_of_utf8EncodingWithoutMarkers_21(),
	Encoding_t1523322056_StaticFields::get_offset_of_unicodeEncoding_22(),
	Encoding_t1523322056_StaticFields::get_offset_of_isoLatin1Encoding_23(),
	Encoding_t1523322056_StaticFields::get_offset_of_utf8EncodingUnsafe_24(),
	Encoding_t1523322056_StaticFields::get_offset_of_utf32Encoding_25(),
	Encoding_t1523322056_StaticFields::get_offset_of_bigEndianUTF32Encoding_26(),
	Encoding_t1523322056_StaticFields::get_offset_of_lockobj_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1006 = { sizeof (ForwardingDecoder_t3503728819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1006[1] = 
{
	ForwardingDecoder_t3503728819::get_offset_of_encoding_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1007 = { sizeof (Latin1Encoding_t950549005), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1008 = { sizeof (StringBuilder_t1712802186), sizeof(char*), 0, 0 };
extern const int32_t g_FieldOffsetTable1008[5] = 
{
	0,
	StringBuilder_t1712802186::get_offset_of__length_1(),
	StringBuilder_t1712802186::get_offset_of__str_2(),
	StringBuilder_t1712802186::get_offset_of__cached_str_3(),
	StringBuilder_t1712802186::get_offset_of__maxCapacity_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1009 = { sizeof (UnicodeEncoding_t1959134050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1009[2] = 
{
	UnicodeEncoding_t1959134050::get_offset_of_bigEndian_28(),
	UnicodeEncoding_t1959134050::get_offset_of_byteOrderMark_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1010 = { sizeof (UnicodeDecoder_t872550992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1010[2] = 
{
	UnicodeDecoder_t872550992::get_offset_of_bigEndian_2(),
	UnicodeDecoder_t872550992::get_offset_of_leftOverByte_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1011 = { sizeof (UTF7Encoding_t2644108479), -1, sizeof(UTF7Encoding_t2644108479_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1011[3] = 
{
	UTF7Encoding_t2644108479::get_offset_of_allowOptionals_28(),
	UTF7Encoding_t2644108479_StaticFields::get_offset_of_encodingRules_29(),
	UTF7Encoding_t2644108479_StaticFields::get_offset_of_base64Values_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1012 = { sizeof (UTF7Decoder_t2247208115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1012[1] = 
{
	UTF7Decoder_t2247208115::get_offset_of_leftOver_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1013 = { sizeof (UTF8Encoding_t3956466879), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1013[1] = 
{
	UTF8Encoding_t3956466879::get_offset_of_emitIdentifier_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1014 = { sizeof (UTF8Decoder_t2159214862), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1014[2] = 
{
	UTF8Decoder_t2159214862::get_offset_of_leftOverBits_2(),
	UTF8Decoder_t2159214862::get_offset_of_leftOverCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1015 = { sizeof (UTF32Encoding_t312252005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1015[2] = 
{
	UTF32Encoding_t312252005::get_offset_of_bigEndian_28(),
	UTF32Encoding_t312252005::get_offset_of_byteOrderMark_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1016 = { sizeof (UTF32Decoder_t635925672), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1016[3] = 
{
	UTF32Decoder_t635925672::get_offset_of_bigEndian_2(),
	UTF32Decoder_t635925672::get_offset_of_leftOverByte_3(),
	UTF32Decoder_t635925672::get_offset_of_leftOverLength_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1017 = { sizeof (AutoResetEvent_t1333520283), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1018 = { sizeof (CompressedStack_t1202932761), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1018[1] = 
{
	CompressedStack_t1202932761::get_offset_of__list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1019 = { sizeof (EventResetMode_t3817241503)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1019[3] = 
{
	EventResetMode_t3817241503::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1020 = { sizeof (EventWaitHandle_t777845177), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1021 = { sizeof (ExecutionContext_t1748372627), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1021[3] = 
{
	ExecutionContext_t1748372627::get_offset_of__sc_0(),
	ExecutionContext_t1748372627::get_offset_of__suppressFlow_1(),
	ExecutionContext_t1748372627::get_offset_of__capture_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1022 = { sizeof (Interlocked_t2273387594), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1023 = { sizeof (LockQueue_t2679652224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1023[2] = 
{
	LockQueue_t2679652224::get_offset_of_rwlock_0(),
	LockQueue_t2679652224::get_offset_of_lockCount_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1024 = { sizeof (ManualResetEvent_t451242010), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1025 = { sizeof (Monitor_t2197244473), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1026 = { sizeof (Mutex_t3066672582), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1027 = { sizeof (NativeEventCalls_t570794723), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1028 = { sizeof (ReaderWriterLock_t367846299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1028[6] = 
{
	ReaderWriterLock_t367846299::get_offset_of_seq_num_0(),
	ReaderWriterLock_t367846299::get_offset_of_state_1(),
	ReaderWriterLock_t367846299::get_offset_of_readers_2(),
	ReaderWriterLock_t367846299::get_offset_of_writer_queue_3(),
	ReaderWriterLock_t367846299::get_offset_of_reader_locks_4(),
	ReaderWriterLock_t367846299::get_offset_of_writer_lock_owner_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1029 = { sizeof (RegisteredWaitHandle_t1529538454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1029[9] = 
{
	RegisteredWaitHandle_t1529538454::get_offset_of__waitObject_1(),
	RegisteredWaitHandle_t1529538454::get_offset_of__callback_2(),
	RegisteredWaitHandle_t1529538454::get_offset_of__timeout_3(),
	RegisteredWaitHandle_t1529538454::get_offset_of__state_4(),
	RegisteredWaitHandle_t1529538454::get_offset_of__executeOnlyOnce_5(),
	RegisteredWaitHandle_t1529538454::get_offset_of__finalEvent_6(),
	RegisteredWaitHandle_t1529538454::get_offset_of__cancelEvent_7(),
	RegisteredWaitHandle_t1529538454::get_offset_of__callsInProcess_8(),
	RegisteredWaitHandle_t1529538454::get_offset_of__unregistered_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1030 = { sizeof (SynchronizationLockException_t841761767), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1031 = { sizeof (Thread_t2300836069), -1, sizeof(Thread_t2300836069_StaticFields), sizeof(Thread_t2300836069_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable1031[52] = 
{
	Thread_t2300836069::get_offset_of_lock_thread_id_0(),
	Thread_t2300836069::get_offset_of_system_thread_handle_1(),
	Thread_t2300836069::get_offset_of_cached_culture_info_2(),
	Thread_t2300836069::get_offset_of_unused0_3(),
	Thread_t2300836069::get_offset_of_threadpool_thread_4(),
	Thread_t2300836069::get_offset_of_name_5(),
	Thread_t2300836069::get_offset_of_name_len_6(),
	Thread_t2300836069::get_offset_of_state_7(),
	Thread_t2300836069::get_offset_of_abort_exc_8(),
	Thread_t2300836069::get_offset_of_abort_state_handle_9(),
	Thread_t2300836069::get_offset_of_thread_id_10(),
	Thread_t2300836069::get_offset_of_start_notify_11(),
	Thread_t2300836069::get_offset_of_stack_ptr_12(),
	Thread_t2300836069::get_offset_of_static_data_13(),
	Thread_t2300836069::get_offset_of_jit_data_14(),
	Thread_t2300836069::get_offset_of_lock_data_15(),
	Thread_t2300836069::get_offset_of_current_appcontext_16(),
	Thread_t2300836069::get_offset_of_stack_size_17(),
	Thread_t2300836069::get_offset_of_start_obj_18(),
	Thread_t2300836069::get_offset_of_appdomain_refs_19(),
	Thread_t2300836069::get_offset_of_interruption_requested_20(),
	Thread_t2300836069::get_offset_of_suspend_event_21(),
	Thread_t2300836069::get_offset_of_suspended_event_22(),
	Thread_t2300836069::get_offset_of_resume_event_23(),
	Thread_t2300836069::get_offset_of_synch_cs_24(),
	Thread_t2300836069::get_offset_of_serialized_culture_info_25(),
	Thread_t2300836069::get_offset_of_serialized_culture_info_len_26(),
	Thread_t2300836069::get_offset_of_serialized_ui_culture_info_27(),
	Thread_t2300836069::get_offset_of_serialized_ui_culture_info_len_28(),
	Thread_t2300836069::get_offset_of_thread_dump_requested_29(),
	Thread_t2300836069::get_offset_of_end_stack_30(),
	Thread_t2300836069::get_offset_of_thread_interrupt_requested_31(),
	Thread_t2300836069::get_offset_of_apartment_state_32(),
	Thread_t2300836069::get_offset_of_critical_region_level_33(),
	Thread_t2300836069::get_offset_of_small_id_34(),
	Thread_t2300836069::get_offset_of_manage_callback_35(),
	Thread_t2300836069::get_offset_of_pending_exception_36(),
	Thread_t2300836069::get_offset_of_ec_to_set_37(),
	Thread_t2300836069::get_offset_of_interrupt_on_stop_38(),
	Thread_t2300836069::get_offset_of_unused3_39(),
	Thread_t2300836069::get_offset_of_unused4_40(),
	Thread_t2300836069::get_offset_of_unused5_41(),
	Thread_t2300836069::get_offset_of_unused6_42(),
	THREAD_STATIC_FIELD_OFFSET,
	THREAD_STATIC_FIELD_OFFSET,
	Thread_t2300836069::get_offset_of_threadstart_45(),
	Thread_t2300836069::get_offset_of_managed_id_46(),
	Thread_t2300836069::get_offset_of__principal_47(),
	Thread_t2300836069_StaticFields::get_offset_of_datastorehash_48(),
	Thread_t2300836069_StaticFields::get_offset_of_datastore_lock_49(),
	Thread_t2300836069::get_offset_of_in_currentculture_50(),
	Thread_t2300836069_StaticFields::get_offset_of_culture_lock_51(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1032 = { sizeof (ThreadAbortException_t4074510458), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1033 = { sizeof (ThreadInterruptedException_t3240955163), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1034 = { sizeof (ThreadPool_t2177989550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1035 = { sizeof (ThreadState_t2533302383)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1035[11] = 
{
	ThreadState_t2533302383::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1036 = { sizeof (ThreadStateException_t3003788475), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1037 = { sizeof (Timer_t716671026), -1, sizeof(Timer_t716671026_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1037[7] = 
{
	Timer_t716671026_StaticFields::get_offset_of_scheduler_1(),
	Timer_t716671026::get_offset_of_callback_2(),
	Timer_t716671026::get_offset_of_state_3(),
	Timer_t716671026::get_offset_of_due_time_ms_4(),
	Timer_t716671026::get_offset_of_period_ms_5(),
	Timer_t716671026::get_offset_of_next_run_6(),
	Timer_t716671026::get_offset_of_disposed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1038 = { sizeof (TimerComparer_t2774265395), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1039 = { sizeof (Scheduler_t3215764947), -1, sizeof(Scheduler_t3215764947_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1039[2] = 
{
	Scheduler_t3215764947_StaticFields::get_offset_of_instance_0(),
	Scheduler_t3215764947::get_offset_of_list_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1040 = { sizeof (WaitHandle_t1743403487), -1, sizeof(WaitHandle_t1743403487_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1040[4] = 
{
	0,
	WaitHandle_t1743403487::get_offset_of_safe_wait_handle_2(),
	WaitHandle_t1743403487_StaticFields::get_offset_of_InvalidHandle_3(),
	WaitHandle_t1743403487::get_offset_of_disposed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1041 = { sizeof (Link_t544317964)+ sizeof (Il2CppObject), sizeof(Link_t544317964 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1041[2] = 
{
	Link_t544317964::get_offset_of_HashCode_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Link_t544317964::get_offset_of_Next_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1042 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1042[16] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1043 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1043[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1044 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1044[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1045 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1045[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1046 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1046[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1047 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1047[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1048 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1048[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1049 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1050 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1051 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1052 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1053 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1053[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1054 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1054[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1055 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1056 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1057 = { sizeof (KeyNotFoundException_t2292407383), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1058 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1058[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1059 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1059[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1060 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1061 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1062 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1062[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1063 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1063[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1064 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1064[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1065 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1065[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1066 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1066[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1067 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1068 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1069 = { sizeof (PrimalityTest_t1539325943), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1070 = { sizeof (AppDomainInitializer_t682969308), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1071 = { sizeof (AssemblyLoadEventHandler_t107971893), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1072 = { sizeof (ConsoleCancelEventHandler_t3919940372), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1073 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1074 = { sizeof (EventHandler_t1348719766), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1075 = { sizeof (ResolveEventHandler_t2775508208), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1076 = { sizeof (UnhandledExceptionEventHandler_t3101989324), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1077 = { sizeof (MemberFilter_t426314064), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1078 = { sizeof (TypeFilter_t2356120900), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1079 = { sizeof (CrossContextDelegate_t387175271), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1080 = { sizeof (HeaderHandler_t2878040134), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1081 = { sizeof (SendOrPostCallback_t2750080073), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1082 = { sizeof (ThreadStart_t1006689297), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1083 = { sizeof (TimerCallback_t1438585625), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1084 = { sizeof (WaitCallback_t2448485498), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1085 = { sizeof (WaitOrTimerCallback_t1973723282), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1086 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1087 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1088 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1089 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1090 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255361), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1090[59] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D1_1(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D2_2(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D3_3(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D4_4(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D5_5(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D6_6(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D15_7(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D16_8(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D17_9(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D18_10(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D19_11(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D20_12(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D21_13(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D22_14(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D23_15(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D24_16(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D25_17(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D26_18(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D27_19(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D28_20(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D29_21(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D30_22(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D31_23(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D32_24(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D34_25(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D35_26(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D36_27(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D37_28(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D38_29(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D39_30(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D40_31(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D41_32(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D42_33(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D43_34(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D44_35(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D45_36(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D46_37(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D47_38(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D48_39(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D49_40(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D50_41(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D51_42(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D52_43(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D53_44(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D54_45(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D55_46(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D56_47(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D57_48(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D58_49(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D59_50(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D60_51(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D61_52(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D62_53(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D63_54(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D64_55(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D65_56(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D67_57(),
	U3CPrivateImplementationDetailsU3E_t3057255361_StaticFields::get_offset_of_U24U24fieldU2D68_58(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1091 = { sizeof (U24ArrayTypeU2452_t520724128)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2452_t520724128 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1092 = { sizeof (U24ArrayTypeU2424_t2469145093)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2424_t2469145093 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1093 = { sizeof (U24ArrayTypeU2416_t3254766644)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2416_t3254766644 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1094 = { sizeof (U24ArrayTypeU24120_t4289081651)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24120_t4289081651 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1095 = { sizeof (U24ArrayTypeU243132_t2732071528)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU243132_t2732071528 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1096 = { sizeof (U24ArrayTypeU2420_t1704471045)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2420_t1704471045 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1097 = { sizeof (U24ArrayTypeU2432_t3652892010)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2432_t3652892010 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1098 = { sizeof (U24ArrayTypeU2448_t1337922363)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2448_t1337922363 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1099 = { sizeof (U24ArrayTypeU2464_t499776625)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2464_t499776625 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
