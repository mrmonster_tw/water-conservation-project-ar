﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_StringComparison3657712135.h"

// System.Web.VirtualPath
struct VirtualPath_t4270372584;
// System.String
struct String_t;
// System.Web.Configuration.CompilationSection
struct CompilationSection_t1832220892;
// System.Collections.Generic.Dictionary`2<System.String,System.Web.Compilation.BuildProvider>
struct Dictionary_2_t3521637304;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3954782707;
// System.Web.Hosting.VirtualPathProvider
struct VirtualPathProvider_t2835917181;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.BuildManagerDirectoryBuilder
struct  BuildManagerDirectoryBuilder_t1749203966  : public Il2CppObject
{
public:
	// System.Web.VirtualPath System.Web.Compilation.BuildManagerDirectoryBuilder::virtualPath
	VirtualPath_t4270372584 * ___virtualPath_0;
	// System.String System.Web.Compilation.BuildManagerDirectoryBuilder::virtualPathDirectory
	String_t* ___virtualPathDirectory_1;
	// System.Web.Configuration.CompilationSection System.Web.Compilation.BuildManagerDirectoryBuilder::compilationSection
	CompilationSection_t1832220892 * ___compilationSection_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Web.Compilation.BuildProvider> System.Web.Compilation.BuildManagerDirectoryBuilder::buildProviders
	Dictionary_2_t3521637304 * ___buildProviders_3;
	// System.Collections.Generic.IEqualityComparer`1<System.String> System.Web.Compilation.BuildManagerDirectoryBuilder::dictionaryComparer
	Il2CppObject* ___dictionaryComparer_4;
	// System.StringComparison System.Web.Compilation.BuildManagerDirectoryBuilder::stringComparer
	int32_t ___stringComparer_5;
	// System.Web.Hosting.VirtualPathProvider System.Web.Compilation.BuildManagerDirectoryBuilder::vpp
	VirtualPathProvider_t2835917181 * ___vpp_6;

public:
	inline static int32_t get_offset_of_virtualPath_0() { return static_cast<int32_t>(offsetof(BuildManagerDirectoryBuilder_t1749203966, ___virtualPath_0)); }
	inline VirtualPath_t4270372584 * get_virtualPath_0() const { return ___virtualPath_0; }
	inline VirtualPath_t4270372584 ** get_address_of_virtualPath_0() { return &___virtualPath_0; }
	inline void set_virtualPath_0(VirtualPath_t4270372584 * value)
	{
		___virtualPath_0 = value;
		Il2CppCodeGenWriteBarrier(&___virtualPath_0, value);
	}

	inline static int32_t get_offset_of_virtualPathDirectory_1() { return static_cast<int32_t>(offsetof(BuildManagerDirectoryBuilder_t1749203966, ___virtualPathDirectory_1)); }
	inline String_t* get_virtualPathDirectory_1() const { return ___virtualPathDirectory_1; }
	inline String_t** get_address_of_virtualPathDirectory_1() { return &___virtualPathDirectory_1; }
	inline void set_virtualPathDirectory_1(String_t* value)
	{
		___virtualPathDirectory_1 = value;
		Il2CppCodeGenWriteBarrier(&___virtualPathDirectory_1, value);
	}

	inline static int32_t get_offset_of_compilationSection_2() { return static_cast<int32_t>(offsetof(BuildManagerDirectoryBuilder_t1749203966, ___compilationSection_2)); }
	inline CompilationSection_t1832220892 * get_compilationSection_2() const { return ___compilationSection_2; }
	inline CompilationSection_t1832220892 ** get_address_of_compilationSection_2() { return &___compilationSection_2; }
	inline void set_compilationSection_2(CompilationSection_t1832220892 * value)
	{
		___compilationSection_2 = value;
		Il2CppCodeGenWriteBarrier(&___compilationSection_2, value);
	}

	inline static int32_t get_offset_of_buildProviders_3() { return static_cast<int32_t>(offsetof(BuildManagerDirectoryBuilder_t1749203966, ___buildProviders_3)); }
	inline Dictionary_2_t3521637304 * get_buildProviders_3() const { return ___buildProviders_3; }
	inline Dictionary_2_t3521637304 ** get_address_of_buildProviders_3() { return &___buildProviders_3; }
	inline void set_buildProviders_3(Dictionary_2_t3521637304 * value)
	{
		___buildProviders_3 = value;
		Il2CppCodeGenWriteBarrier(&___buildProviders_3, value);
	}

	inline static int32_t get_offset_of_dictionaryComparer_4() { return static_cast<int32_t>(offsetof(BuildManagerDirectoryBuilder_t1749203966, ___dictionaryComparer_4)); }
	inline Il2CppObject* get_dictionaryComparer_4() const { return ___dictionaryComparer_4; }
	inline Il2CppObject** get_address_of_dictionaryComparer_4() { return &___dictionaryComparer_4; }
	inline void set_dictionaryComparer_4(Il2CppObject* value)
	{
		___dictionaryComparer_4 = value;
		Il2CppCodeGenWriteBarrier(&___dictionaryComparer_4, value);
	}

	inline static int32_t get_offset_of_stringComparer_5() { return static_cast<int32_t>(offsetof(BuildManagerDirectoryBuilder_t1749203966, ___stringComparer_5)); }
	inline int32_t get_stringComparer_5() const { return ___stringComparer_5; }
	inline int32_t* get_address_of_stringComparer_5() { return &___stringComparer_5; }
	inline void set_stringComparer_5(int32_t value)
	{
		___stringComparer_5 = value;
	}

	inline static int32_t get_offset_of_vpp_6() { return static_cast<int32_t>(offsetof(BuildManagerDirectoryBuilder_t1749203966, ___vpp_6)); }
	inline VirtualPathProvider_t2835917181 * get_vpp_6() const { return ___vpp_6; }
	inline VirtualPathProvider_t2835917181 ** get_address_of_vpp_6() { return &___vpp_6; }
	inline void set_vpp_6(VirtualPathProvider_t2835917181 * value)
	{
		___vpp_6 = value;
		Il2CppCodeGenWriteBarrier(&___vpp_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
