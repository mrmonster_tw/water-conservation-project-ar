﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_DateTime3738529785.h"
#include "mscorlib_System_TimeSpan881159249.h"

// System.Collections.Generic.Dictionary`2<System.String,System.Web.Caching.CacheItem>
struct Dictionary_2_t1854438536;
// System.Web.Caching.Cache
struct Cache_t4020976765;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Caching.Cache
struct  Cache_t4020976765  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Web.Caching.CacheItem> System.Web.Caching.Cache::cache
	Dictionary_2_t1854438536 * ___cache_0;
	// System.Web.Caching.Cache System.Web.Caching.Cache::dependencyCache
	Cache_t4020976765 * ___dependencyCache_1;

public:
	inline static int32_t get_offset_of_cache_0() { return static_cast<int32_t>(offsetof(Cache_t4020976765, ___cache_0)); }
	inline Dictionary_2_t1854438536 * get_cache_0() const { return ___cache_0; }
	inline Dictionary_2_t1854438536 ** get_address_of_cache_0() { return &___cache_0; }
	inline void set_cache_0(Dictionary_2_t1854438536 * value)
	{
		___cache_0 = value;
		Il2CppCodeGenWriteBarrier(&___cache_0, value);
	}

	inline static int32_t get_offset_of_dependencyCache_1() { return static_cast<int32_t>(offsetof(Cache_t4020976765, ___dependencyCache_1)); }
	inline Cache_t4020976765 * get_dependencyCache_1() const { return ___dependencyCache_1; }
	inline Cache_t4020976765 ** get_address_of_dependencyCache_1() { return &___dependencyCache_1; }
	inline void set_dependencyCache_1(Cache_t4020976765 * value)
	{
		___dependencyCache_1 = value;
		Il2CppCodeGenWriteBarrier(&___dependencyCache_1, value);
	}
};

struct Cache_t4020976765_StaticFields
{
public:
	// System.DateTime System.Web.Caching.Cache::NoAbsoluteExpiration
	DateTime_t3738529785  ___NoAbsoluteExpiration_2;
	// System.TimeSpan System.Web.Caching.Cache::NoSlidingExpiration
	TimeSpan_t881159249  ___NoSlidingExpiration_3;

public:
	inline static int32_t get_offset_of_NoAbsoluteExpiration_2() { return static_cast<int32_t>(offsetof(Cache_t4020976765_StaticFields, ___NoAbsoluteExpiration_2)); }
	inline DateTime_t3738529785  get_NoAbsoluteExpiration_2() const { return ___NoAbsoluteExpiration_2; }
	inline DateTime_t3738529785 * get_address_of_NoAbsoluteExpiration_2() { return &___NoAbsoluteExpiration_2; }
	inline void set_NoAbsoluteExpiration_2(DateTime_t3738529785  value)
	{
		___NoAbsoluteExpiration_2 = value;
	}

	inline static int32_t get_offset_of_NoSlidingExpiration_3() { return static_cast<int32_t>(offsetof(Cache_t4020976765_StaticFields, ___NoSlidingExpiration_3)); }
	inline TimeSpan_t881159249  get_NoSlidingExpiration_3() const { return ___NoSlidingExpiration_3; }
	inline TimeSpan_t881159249 * get_address_of_NoSlidingExpiration_3() { return &___NoSlidingExpiration_3; }
	inline void set_NoSlidingExpiration_3(TimeSpan_t881159249  value)
	{
		___NoSlidingExpiration_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
