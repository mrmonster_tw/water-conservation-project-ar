﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Collections.ArrayList
struct ArrayList_t2718874744;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.ExtensionManager
struct  ExtensionManager_t3142439337  : public Il2CppObject
{
public:

public:
};

struct ExtensionManager_t3142439337_StaticFields
{
public:
	// System.Collections.Hashtable System.Web.Services.Description.ExtensionManager::extensionsByName
	Hashtable_t1853889766 * ___extensionsByName_0;
	// System.Collections.Hashtable System.Web.Services.Description.ExtensionManager::extensionsByType
	Hashtable_t1853889766 * ___extensionsByType_1;
	// System.Collections.ArrayList System.Web.Services.Description.ExtensionManager::maps
	ArrayList_t2718874744 * ___maps_2;
	// System.Collections.ArrayList System.Web.Services.Description.ExtensionManager::extensions
	ArrayList_t2718874744 * ___extensions_3;

public:
	inline static int32_t get_offset_of_extensionsByName_0() { return static_cast<int32_t>(offsetof(ExtensionManager_t3142439337_StaticFields, ___extensionsByName_0)); }
	inline Hashtable_t1853889766 * get_extensionsByName_0() const { return ___extensionsByName_0; }
	inline Hashtable_t1853889766 ** get_address_of_extensionsByName_0() { return &___extensionsByName_0; }
	inline void set_extensionsByName_0(Hashtable_t1853889766 * value)
	{
		___extensionsByName_0 = value;
		Il2CppCodeGenWriteBarrier(&___extensionsByName_0, value);
	}

	inline static int32_t get_offset_of_extensionsByType_1() { return static_cast<int32_t>(offsetof(ExtensionManager_t3142439337_StaticFields, ___extensionsByType_1)); }
	inline Hashtable_t1853889766 * get_extensionsByType_1() const { return ___extensionsByType_1; }
	inline Hashtable_t1853889766 ** get_address_of_extensionsByType_1() { return &___extensionsByType_1; }
	inline void set_extensionsByType_1(Hashtable_t1853889766 * value)
	{
		___extensionsByType_1 = value;
		Il2CppCodeGenWriteBarrier(&___extensionsByType_1, value);
	}

	inline static int32_t get_offset_of_maps_2() { return static_cast<int32_t>(offsetof(ExtensionManager_t3142439337_StaticFields, ___maps_2)); }
	inline ArrayList_t2718874744 * get_maps_2() const { return ___maps_2; }
	inline ArrayList_t2718874744 ** get_address_of_maps_2() { return &___maps_2; }
	inline void set_maps_2(ArrayList_t2718874744 * value)
	{
		___maps_2 = value;
		Il2CppCodeGenWriteBarrier(&___maps_2, value);
	}

	inline static int32_t get_offset_of_extensions_3() { return static_cast<int32_t>(offsetof(ExtensionManager_t3142439337_StaticFields, ___extensions_3)); }
	inline ArrayList_t2718874744 * get_extensions_3() const { return ___extensions_3; }
	inline ArrayList_t2718874744 ** get_address_of_extensions_3() { return &___extensions_3; }
	inline void set_extensions_3(ArrayList_t2718874744 * value)
	{
		___extensions_3 = value;
		Il2CppCodeGenWriteBarrier(&___extensions_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
