﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Xsl_XsltContext2039362735.h"

// System.Security.Cryptography.Xml.XmlDsigXPathTransform/XmlDsigXPathFunctionHere
struct XmlDsigXPathFunctionHere_t59088751;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.Xml.XmlDsigXPathTransform/XmlDsigXPathContext
struct  XmlDsigXPathContext_t2680727779  : public XsltContext_t2039362735
{
public:
	// System.Security.Cryptography.Xml.XmlDsigXPathTransform/XmlDsigXPathFunctionHere System.Security.Cryptography.Xml.XmlDsigXPathTransform/XmlDsigXPathContext::here
	XmlDsigXPathFunctionHere_t59088751 * ___here_9;

public:
	inline static int32_t get_offset_of_here_9() { return static_cast<int32_t>(offsetof(XmlDsigXPathContext_t2680727779, ___here_9)); }
	inline XmlDsigXPathFunctionHere_t59088751 * get_here_9() const { return ___here_9; }
	inline XmlDsigXPathFunctionHere_t59088751 ** get_address_of_here_9() { return &___here_9; }
	inline void set_here_9(XmlDsigXPathFunctionHere_t59088751 * value)
	{
		___here_9 = value;
		Il2CppCodeGenWriteBarrier(&___here_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
