﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Selector3905425829.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IdentityModel.Tokens.SecurityToken>
struct ReadOnlyCollection_1_t2484449827;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Selectors.SecurityTokenResolver/DefaultSecurityTokenResolver
struct  DefaultSecurityTokenResolver_t965239324  : public SecurityTokenResolver_t3905425829
{
public:
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IdentityModel.Tokens.SecurityToken> System.IdentityModel.Selectors.SecurityTokenResolver/DefaultSecurityTokenResolver::tokens
	ReadOnlyCollection_1_t2484449827 * ___tokens_0;
	// System.Boolean System.IdentityModel.Selectors.SecurityTokenResolver/DefaultSecurityTokenResolver::match_local
	bool ___match_local_1;

public:
	inline static int32_t get_offset_of_tokens_0() { return static_cast<int32_t>(offsetof(DefaultSecurityTokenResolver_t965239324, ___tokens_0)); }
	inline ReadOnlyCollection_1_t2484449827 * get_tokens_0() const { return ___tokens_0; }
	inline ReadOnlyCollection_1_t2484449827 ** get_address_of_tokens_0() { return &___tokens_0; }
	inline void set_tokens_0(ReadOnlyCollection_1_t2484449827 * value)
	{
		___tokens_0 = value;
		Il2CppCodeGenWriteBarrier(&___tokens_0, value);
	}

	inline static int32_t get_offset_of_match_local_1() { return static_cast<int32_t>(offsetof(DefaultSecurityTokenResolver_t965239324, ___match_local_1)); }
	inline bool get_match_local_1() const { return ___match_local_1; }
	inline bool* get_address_of_match_local_1() { return &___match_local_1; }
	inline void set_match_local_1(bool value)
	{
		___match_local_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
