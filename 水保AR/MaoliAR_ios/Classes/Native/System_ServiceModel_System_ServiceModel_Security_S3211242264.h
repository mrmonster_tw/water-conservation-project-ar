﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Security.SecurityVersion
struct SecurityVersion_t3211242264;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.SecurityVersion
struct  SecurityVersion_t3211242264  : public Il2CppObject
{
public:

public:
};

struct SecurityVersion_t3211242264_StaticFields
{
public:
	// System.ServiceModel.Security.SecurityVersion System.ServiceModel.Security.SecurityVersion::wss10
	SecurityVersion_t3211242264 * ___wss10_0;
	// System.ServiceModel.Security.SecurityVersion System.ServiceModel.Security.SecurityVersion::wss11
	SecurityVersion_t3211242264 * ___wss11_1;

public:
	inline static int32_t get_offset_of_wss10_0() { return static_cast<int32_t>(offsetof(SecurityVersion_t3211242264_StaticFields, ___wss10_0)); }
	inline SecurityVersion_t3211242264 * get_wss10_0() const { return ___wss10_0; }
	inline SecurityVersion_t3211242264 ** get_address_of_wss10_0() { return &___wss10_0; }
	inline void set_wss10_0(SecurityVersion_t3211242264 * value)
	{
		___wss10_0 = value;
		Il2CppCodeGenWriteBarrier(&___wss10_0, value);
	}

	inline static int32_t get_offset_of_wss11_1() { return static_cast<int32_t>(offsetof(SecurityVersion_t3211242264_StaticFields, ___wss11_1)); }
	inline SecurityVersion_t3211242264 * get_wss11_1() const { return ___wss11_1; }
	inline SecurityVersion_t3211242264 ** get_address_of_wss11_1() { return &___wss11_1; }
	inline void set_wss11_1(SecurityVersion_t3211242264 * value)
	{
		___wss11_1 = value;
		Il2CppCodeGenWriteBarrier(&___wss11_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
