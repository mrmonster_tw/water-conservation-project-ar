﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Globalization.CultureInfo
struct CultureInfo_t4157843068;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Util.Helpers
struct  Helpers_t1105960530  : public Il2CppObject
{
public:

public:
};

struct Helpers_t1105960530_StaticFields
{
public:
	// System.Globalization.CultureInfo System.Web.Util.Helpers::InvariantCulture
	CultureInfo_t4157843068 * ___InvariantCulture_0;

public:
	inline static int32_t get_offset_of_InvariantCulture_0() { return static_cast<int32_t>(offsetof(Helpers_t1105960530_StaticFields, ___InvariantCulture_0)); }
	inline CultureInfo_t4157843068 * get_InvariantCulture_0() const { return ___InvariantCulture_0; }
	inline CultureInfo_t4157843068 ** get_address_of_InvariantCulture_0() { return &___InvariantCulture_0; }
	inline void set_InvariantCulture_0(CultureInfo_t4157843068 * value)
	{
		___InvariantCulture_0 = value;
		Il2CppCodeGenWriteBarrier(&___InvariantCulture_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
