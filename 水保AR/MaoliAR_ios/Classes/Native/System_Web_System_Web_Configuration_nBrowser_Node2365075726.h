﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Web_System_Web_Configuration_nBrowser_NodeTy169286799.h"

// System.String
struct String_t;
// System.Xml.XmlNode
struct XmlNode_t3767805227;
// System.Web.Configuration.nBrowser.Identification[]
struct IdentificationU5BU5D_t3644587319;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t407452768;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.SortedList`2<System.String,System.Web.Configuration.nBrowser.Node>
struct SortedList_2_t3492399853;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.nBrowser.Node
struct  Node_t2365075726  : public Il2CppObject
{
public:
	// System.Web.Configuration.nBrowser.NodeType System.Web.Configuration.nBrowser.Node::pName
	int32_t ___pName_0;
	// System.String System.Web.Configuration.nBrowser.Node::pId
	String_t* ___pId_1;
	// System.String System.Web.Configuration.nBrowser.Node::pParentID
	String_t* ___pParentID_2;
	// System.String System.Web.Configuration.nBrowser.Node::pRefID
	String_t* ___pRefID_3;
	// System.String System.Web.Configuration.nBrowser.Node::pMarkupTextWriterType
	String_t* ___pMarkupTextWriterType_4;
	// System.String System.Web.Configuration.nBrowser.Node::pFileName
	String_t* ___pFileName_5;
	// System.Xml.XmlNode System.Web.Configuration.nBrowser.Node::xmlNode
	XmlNode_t3767805227 * ___xmlNode_6;
	// System.Web.Configuration.nBrowser.Identification[] System.Web.Configuration.nBrowser.Node::Identification
	IdentificationU5BU5D_t3644587319* ___Identification_7;
	// System.Web.Configuration.nBrowser.Identification[] System.Web.Configuration.nBrowser.Node::Capture
	IdentificationU5BU5D_t3644587319* ___Capture_8;
	// System.Collections.Specialized.NameValueCollection System.Web.Configuration.nBrowser.Node::Capabilities
	NameValueCollection_t407452768 * ___Capabilities_9;
	// System.Collections.Specialized.NameValueCollection System.Web.Configuration.nBrowser.Node::Adapter
	NameValueCollection_t407452768 * ___Adapter_10;
	// System.Type[] System.Web.Configuration.nBrowser.Node::AdapterControlTypes
	TypeU5BU5D_t3940880105* ___AdapterControlTypes_11;
	// System.Type[] System.Web.Configuration.nBrowser.Node::AdapterTypes
	TypeU5BU5D_t3940880105* ___AdapterTypes_12;
	// System.Collections.Generic.List`1<System.String> System.Web.Configuration.nBrowser.Node::ChildrenKeys
	List_1_t3319525431 * ___ChildrenKeys_13;
	// System.Collections.Generic.List`1<System.String> System.Web.Configuration.nBrowser.Node::DefaultChildrenKeys
	List_1_t3319525431 * ___DefaultChildrenKeys_14;
	// System.Collections.Generic.SortedList`2<System.String,System.Web.Configuration.nBrowser.Node> System.Web.Configuration.nBrowser.Node::Children
	SortedList_2_t3492399853 * ___Children_15;
	// System.Collections.Generic.SortedList`2<System.String,System.Web.Configuration.nBrowser.Node> System.Web.Configuration.nBrowser.Node::DefaultChildren
	SortedList_2_t3492399853 * ___DefaultChildren_16;
	// System.Collections.Specialized.NameValueCollection System.Web.Configuration.nBrowser.Node::sampleHeaders
	NameValueCollection_t407452768 * ___sampleHeaders_17;
	// System.Boolean System.Web.Configuration.nBrowser.Node::HaveAdapterTypes
	bool ___HaveAdapterTypes_18;
	// System.Object System.Web.Configuration.nBrowser.Node::LookupAdapterTypesLock
	Il2CppObject * ___LookupAdapterTypesLock_19;

public:
	inline static int32_t get_offset_of_pName_0() { return static_cast<int32_t>(offsetof(Node_t2365075726, ___pName_0)); }
	inline int32_t get_pName_0() const { return ___pName_0; }
	inline int32_t* get_address_of_pName_0() { return &___pName_0; }
	inline void set_pName_0(int32_t value)
	{
		___pName_0 = value;
	}

	inline static int32_t get_offset_of_pId_1() { return static_cast<int32_t>(offsetof(Node_t2365075726, ___pId_1)); }
	inline String_t* get_pId_1() const { return ___pId_1; }
	inline String_t** get_address_of_pId_1() { return &___pId_1; }
	inline void set_pId_1(String_t* value)
	{
		___pId_1 = value;
		Il2CppCodeGenWriteBarrier(&___pId_1, value);
	}

	inline static int32_t get_offset_of_pParentID_2() { return static_cast<int32_t>(offsetof(Node_t2365075726, ___pParentID_2)); }
	inline String_t* get_pParentID_2() const { return ___pParentID_2; }
	inline String_t** get_address_of_pParentID_2() { return &___pParentID_2; }
	inline void set_pParentID_2(String_t* value)
	{
		___pParentID_2 = value;
		Il2CppCodeGenWriteBarrier(&___pParentID_2, value);
	}

	inline static int32_t get_offset_of_pRefID_3() { return static_cast<int32_t>(offsetof(Node_t2365075726, ___pRefID_3)); }
	inline String_t* get_pRefID_3() const { return ___pRefID_3; }
	inline String_t** get_address_of_pRefID_3() { return &___pRefID_3; }
	inline void set_pRefID_3(String_t* value)
	{
		___pRefID_3 = value;
		Il2CppCodeGenWriteBarrier(&___pRefID_3, value);
	}

	inline static int32_t get_offset_of_pMarkupTextWriterType_4() { return static_cast<int32_t>(offsetof(Node_t2365075726, ___pMarkupTextWriterType_4)); }
	inline String_t* get_pMarkupTextWriterType_4() const { return ___pMarkupTextWriterType_4; }
	inline String_t** get_address_of_pMarkupTextWriterType_4() { return &___pMarkupTextWriterType_4; }
	inline void set_pMarkupTextWriterType_4(String_t* value)
	{
		___pMarkupTextWriterType_4 = value;
		Il2CppCodeGenWriteBarrier(&___pMarkupTextWriterType_4, value);
	}

	inline static int32_t get_offset_of_pFileName_5() { return static_cast<int32_t>(offsetof(Node_t2365075726, ___pFileName_5)); }
	inline String_t* get_pFileName_5() const { return ___pFileName_5; }
	inline String_t** get_address_of_pFileName_5() { return &___pFileName_5; }
	inline void set_pFileName_5(String_t* value)
	{
		___pFileName_5 = value;
		Il2CppCodeGenWriteBarrier(&___pFileName_5, value);
	}

	inline static int32_t get_offset_of_xmlNode_6() { return static_cast<int32_t>(offsetof(Node_t2365075726, ___xmlNode_6)); }
	inline XmlNode_t3767805227 * get_xmlNode_6() const { return ___xmlNode_6; }
	inline XmlNode_t3767805227 ** get_address_of_xmlNode_6() { return &___xmlNode_6; }
	inline void set_xmlNode_6(XmlNode_t3767805227 * value)
	{
		___xmlNode_6 = value;
		Il2CppCodeGenWriteBarrier(&___xmlNode_6, value);
	}

	inline static int32_t get_offset_of_Identification_7() { return static_cast<int32_t>(offsetof(Node_t2365075726, ___Identification_7)); }
	inline IdentificationU5BU5D_t3644587319* get_Identification_7() const { return ___Identification_7; }
	inline IdentificationU5BU5D_t3644587319** get_address_of_Identification_7() { return &___Identification_7; }
	inline void set_Identification_7(IdentificationU5BU5D_t3644587319* value)
	{
		___Identification_7 = value;
		Il2CppCodeGenWriteBarrier(&___Identification_7, value);
	}

	inline static int32_t get_offset_of_Capture_8() { return static_cast<int32_t>(offsetof(Node_t2365075726, ___Capture_8)); }
	inline IdentificationU5BU5D_t3644587319* get_Capture_8() const { return ___Capture_8; }
	inline IdentificationU5BU5D_t3644587319** get_address_of_Capture_8() { return &___Capture_8; }
	inline void set_Capture_8(IdentificationU5BU5D_t3644587319* value)
	{
		___Capture_8 = value;
		Il2CppCodeGenWriteBarrier(&___Capture_8, value);
	}

	inline static int32_t get_offset_of_Capabilities_9() { return static_cast<int32_t>(offsetof(Node_t2365075726, ___Capabilities_9)); }
	inline NameValueCollection_t407452768 * get_Capabilities_9() const { return ___Capabilities_9; }
	inline NameValueCollection_t407452768 ** get_address_of_Capabilities_9() { return &___Capabilities_9; }
	inline void set_Capabilities_9(NameValueCollection_t407452768 * value)
	{
		___Capabilities_9 = value;
		Il2CppCodeGenWriteBarrier(&___Capabilities_9, value);
	}

	inline static int32_t get_offset_of_Adapter_10() { return static_cast<int32_t>(offsetof(Node_t2365075726, ___Adapter_10)); }
	inline NameValueCollection_t407452768 * get_Adapter_10() const { return ___Adapter_10; }
	inline NameValueCollection_t407452768 ** get_address_of_Adapter_10() { return &___Adapter_10; }
	inline void set_Adapter_10(NameValueCollection_t407452768 * value)
	{
		___Adapter_10 = value;
		Il2CppCodeGenWriteBarrier(&___Adapter_10, value);
	}

	inline static int32_t get_offset_of_AdapterControlTypes_11() { return static_cast<int32_t>(offsetof(Node_t2365075726, ___AdapterControlTypes_11)); }
	inline TypeU5BU5D_t3940880105* get_AdapterControlTypes_11() const { return ___AdapterControlTypes_11; }
	inline TypeU5BU5D_t3940880105** get_address_of_AdapterControlTypes_11() { return &___AdapterControlTypes_11; }
	inline void set_AdapterControlTypes_11(TypeU5BU5D_t3940880105* value)
	{
		___AdapterControlTypes_11 = value;
		Il2CppCodeGenWriteBarrier(&___AdapterControlTypes_11, value);
	}

	inline static int32_t get_offset_of_AdapterTypes_12() { return static_cast<int32_t>(offsetof(Node_t2365075726, ___AdapterTypes_12)); }
	inline TypeU5BU5D_t3940880105* get_AdapterTypes_12() const { return ___AdapterTypes_12; }
	inline TypeU5BU5D_t3940880105** get_address_of_AdapterTypes_12() { return &___AdapterTypes_12; }
	inline void set_AdapterTypes_12(TypeU5BU5D_t3940880105* value)
	{
		___AdapterTypes_12 = value;
		Il2CppCodeGenWriteBarrier(&___AdapterTypes_12, value);
	}

	inline static int32_t get_offset_of_ChildrenKeys_13() { return static_cast<int32_t>(offsetof(Node_t2365075726, ___ChildrenKeys_13)); }
	inline List_1_t3319525431 * get_ChildrenKeys_13() const { return ___ChildrenKeys_13; }
	inline List_1_t3319525431 ** get_address_of_ChildrenKeys_13() { return &___ChildrenKeys_13; }
	inline void set_ChildrenKeys_13(List_1_t3319525431 * value)
	{
		___ChildrenKeys_13 = value;
		Il2CppCodeGenWriteBarrier(&___ChildrenKeys_13, value);
	}

	inline static int32_t get_offset_of_DefaultChildrenKeys_14() { return static_cast<int32_t>(offsetof(Node_t2365075726, ___DefaultChildrenKeys_14)); }
	inline List_1_t3319525431 * get_DefaultChildrenKeys_14() const { return ___DefaultChildrenKeys_14; }
	inline List_1_t3319525431 ** get_address_of_DefaultChildrenKeys_14() { return &___DefaultChildrenKeys_14; }
	inline void set_DefaultChildrenKeys_14(List_1_t3319525431 * value)
	{
		___DefaultChildrenKeys_14 = value;
		Il2CppCodeGenWriteBarrier(&___DefaultChildrenKeys_14, value);
	}

	inline static int32_t get_offset_of_Children_15() { return static_cast<int32_t>(offsetof(Node_t2365075726, ___Children_15)); }
	inline SortedList_2_t3492399853 * get_Children_15() const { return ___Children_15; }
	inline SortedList_2_t3492399853 ** get_address_of_Children_15() { return &___Children_15; }
	inline void set_Children_15(SortedList_2_t3492399853 * value)
	{
		___Children_15 = value;
		Il2CppCodeGenWriteBarrier(&___Children_15, value);
	}

	inline static int32_t get_offset_of_DefaultChildren_16() { return static_cast<int32_t>(offsetof(Node_t2365075726, ___DefaultChildren_16)); }
	inline SortedList_2_t3492399853 * get_DefaultChildren_16() const { return ___DefaultChildren_16; }
	inline SortedList_2_t3492399853 ** get_address_of_DefaultChildren_16() { return &___DefaultChildren_16; }
	inline void set_DefaultChildren_16(SortedList_2_t3492399853 * value)
	{
		___DefaultChildren_16 = value;
		Il2CppCodeGenWriteBarrier(&___DefaultChildren_16, value);
	}

	inline static int32_t get_offset_of_sampleHeaders_17() { return static_cast<int32_t>(offsetof(Node_t2365075726, ___sampleHeaders_17)); }
	inline NameValueCollection_t407452768 * get_sampleHeaders_17() const { return ___sampleHeaders_17; }
	inline NameValueCollection_t407452768 ** get_address_of_sampleHeaders_17() { return &___sampleHeaders_17; }
	inline void set_sampleHeaders_17(NameValueCollection_t407452768 * value)
	{
		___sampleHeaders_17 = value;
		Il2CppCodeGenWriteBarrier(&___sampleHeaders_17, value);
	}

	inline static int32_t get_offset_of_HaveAdapterTypes_18() { return static_cast<int32_t>(offsetof(Node_t2365075726, ___HaveAdapterTypes_18)); }
	inline bool get_HaveAdapterTypes_18() const { return ___HaveAdapterTypes_18; }
	inline bool* get_address_of_HaveAdapterTypes_18() { return &___HaveAdapterTypes_18; }
	inline void set_HaveAdapterTypes_18(bool value)
	{
		___HaveAdapterTypes_18 = value;
	}

	inline static int32_t get_offset_of_LookupAdapterTypesLock_19() { return static_cast<int32_t>(offsetof(Node_t2365075726, ___LookupAdapterTypesLock_19)); }
	inline Il2CppObject * get_LookupAdapterTypesLock_19() const { return ___LookupAdapterTypesLock_19; }
	inline Il2CppObject ** get_address_of_LookupAdapterTypesLock_19() { return &___LookupAdapterTypesLock_19; }
	inline void set_LookupAdapterTypesLock_19(Il2CppObject * value)
	{
		___LookupAdapterTypesLock_19 = value;
		Il2CppCodeGenWriteBarrier(&___LookupAdapterTypesLock_19, value);
	}
};

struct Node_t2365075726_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Web.Configuration.nBrowser.Node::<>f__switch$map1
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map1_20;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Web.Configuration.nBrowser.Node::<>f__switch$map2
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map2_21;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Web.Configuration.nBrowser.Node::<>f__switch$map3
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map3_22;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_20() { return static_cast<int32_t>(offsetof(Node_t2365075726_StaticFields, ___U3CU3Ef__switchU24map1_20)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map1_20() const { return ___U3CU3Ef__switchU24map1_20; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map1_20() { return &___U3CU3Ef__switchU24map1_20; }
	inline void set_U3CU3Ef__switchU24map1_20(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map1_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1_20, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2_21() { return static_cast<int32_t>(offsetof(Node_t2365075726_StaticFields, ___U3CU3Ef__switchU24map2_21)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map2_21() const { return ___U3CU3Ef__switchU24map2_21; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map2_21() { return &___U3CU3Ef__switchU24map2_21; }
	inline void set_U3CU3Ef__switchU24map2_21(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map2_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map2_21, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map3_22() { return static_cast<int32_t>(offsetof(Node_t2365075726_StaticFields, ___U3CU3Ef__switchU24map3_22)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map3_22() const { return ___U3CU3Ef__switchU24map3_22; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map3_22() { return &___U3CU3Ef__switchU24map3_22; }
	inline void set_U3CU3Ef__switchU24map3_22(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map3_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map3_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
