﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_BaseParser4057001241.h"
#include "System_Web_System_Web_UI_TemplateParser_OutputCach4174059373.h"
#include "System_Web_System_Web_UI_OutputCacheLocation1771607847.h"

// System.String
struct String_t;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t4177511560;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<System.Web.UI.ServerSideScript>
struct List_1_t4067502771;
// System.Type
struct Type_t;
// System.Web.UI.RootBuilder
struct RootBuilder_t1594119744;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Web.UI.PageParserFilter
struct PageParserFilter_t169228355;
// System.Collections.Generic.List`1<System.Web.UI.UnknownAttributeDescriptor>
struct List_1_t3058591440;
// System.Collections.Generic.Stack`1<System.String>
struct Stack_1_t2690840144;
// System.Web.Compilation.ILocation
struct ILocation_t3961726794;
// System.Web.Compilation.AspGenerator
struct AspGenerator_t2810561191;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.TemplateParser
struct  TemplateParser_t24149626  : public BaseParser_t4057001241
{
public:
	// System.String System.Web.UI.TemplateParser::inputFile
	String_t* ___inputFile_5;
	// System.Collections.IDictionary System.Web.UI.TemplateParser::mainAttributes
	Il2CppObject * ___mainAttributes_6;
	// System.Collections.ArrayList System.Web.UI.TemplateParser::dependencies
	ArrayList_t2718874744 * ___dependencies_7;
	// System.Collections.ArrayList System.Web.UI.TemplateParser::assemblies
	ArrayList_t2718874744 * ___assemblies_8;
	// System.Collections.IDictionary System.Web.UI.TemplateParser::anames
	Il2CppObject * ___anames_9;
	// System.String[] System.Web.UI.TemplateParser::binDirAssemblies
	StringU5BU5D_t1281789340* ___binDirAssemblies_10;
	// System.Collections.Generic.Dictionary`2<System.String,System.Boolean> System.Web.UI.TemplateParser::namespacesCache
	Dictionary_2_t4177511560 * ___namespacesCache_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.Boolean> System.Web.UI.TemplateParser::imports
	Dictionary_2_t4177511560 * ___imports_12;
	// System.Collections.Generic.List`1<System.String> System.Web.UI.TemplateParser::interfaces
	List_1_t3319525431 * ___interfaces_13;
	// System.Collections.Generic.List`1<System.Web.UI.ServerSideScript> System.Web.UI.TemplateParser::scripts
	List_1_t4067502771 * ___scripts_14;
	// System.Type System.Web.UI.TemplateParser::baseType
	Type_t * ___baseType_15;
	// System.Boolean System.Web.UI.TemplateParser::baseTypeIsGlobal
	bool ___baseTypeIsGlobal_16;
	// System.String System.Web.UI.TemplateParser::className
	String_t* ___className_17;
	// System.Web.UI.RootBuilder System.Web.UI.TemplateParser::rootBuilder
	RootBuilder_t1594119744 * ___rootBuilder_18;
	// System.Boolean System.Web.UI.TemplateParser::debug
	bool ___debug_19;
	// System.String System.Web.UI.TemplateParser::compilerOptions
	String_t* ___compilerOptions_20;
	// System.String System.Web.UI.TemplateParser::language
	String_t* ___language_21;
	// System.Boolean System.Web.UI.TemplateParser::implicitLanguage
	bool ___implicitLanguage_22;
	// System.Boolean System.Web.UI.TemplateParser::strictOn
	bool ___strictOn_23;
	// System.Boolean System.Web.UI.TemplateParser::explicitOn
	bool ___explicitOn_24;
	// System.Boolean System.Web.UI.TemplateParser::linePragmasOn
	bool ___linePragmasOn_25;
	// System.Boolean System.Web.UI.TemplateParser::output_cache
	bool ___output_cache_26;
	// System.Int32 System.Web.UI.TemplateParser::oc_duration
	int32_t ___oc_duration_27;
	// System.String System.Web.UI.TemplateParser::oc_header
	String_t* ___oc_header_28;
	// System.String System.Web.UI.TemplateParser::oc_custom
	String_t* ___oc_custom_29;
	// System.String System.Web.UI.TemplateParser::oc_param
	String_t* ___oc_param_30;
	// System.String System.Web.UI.TemplateParser::oc_controls
	String_t* ___oc_controls_31;
	// System.String System.Web.UI.TemplateParser::oc_content_encodings
	String_t* ___oc_content_encodings_32;
	// System.String System.Web.UI.TemplateParser::oc_cacheprofile
	String_t* ___oc_cacheprofile_33;
	// System.String System.Web.UI.TemplateParser::oc_sqldependency
	String_t* ___oc_sqldependency_34;
	// System.Boolean System.Web.UI.TemplateParser::oc_nostore
	bool ___oc_nostore_35;
	// System.Web.UI.TemplateParser/OutputCacheParsedParams System.Web.UI.TemplateParser::oc_parsed_params
	int32_t ___oc_parsed_params_36;
	// System.Boolean System.Web.UI.TemplateParser::oc_shared
	bool ___oc_shared_37;
	// System.Web.UI.OutputCacheLocation System.Web.UI.TemplateParser::oc_location
	int32_t ___oc_location_38;
	// System.Int32 System.Web.UI.TemplateParser::allowedMainDirectives
	int32_t ___allowedMainDirectives_39;
	// System.Byte[] System.Web.UI.TemplateParser::md5checksum
	ByteU5BU5D_t4116647657* ___md5checksum_40;
	// System.String System.Web.UI.TemplateParser::src
	String_t* ___src_41;
	// System.Boolean System.Web.UI.TemplateParser::srcIsLegacy
	bool ___srcIsLegacy_42;
	// System.String System.Web.UI.TemplateParser::partialClassName
	String_t* ___partialClassName_43;
	// System.String System.Web.UI.TemplateParser::codeFileBaseClass
	String_t* ___codeFileBaseClass_44;
	// System.String System.Web.UI.TemplateParser::metaResourceKey
	String_t* ___metaResourceKey_45;
	// System.Type System.Web.UI.TemplateParser::codeFileBaseClassType
	Type_t * ___codeFileBaseClassType_46;
	// System.Type System.Web.UI.TemplateParser::pageParserFilterType
	Type_t * ___pageParserFilterType_47;
	// System.Web.UI.PageParserFilter System.Web.UI.TemplateParser::pageParserFilter
	PageParserFilter_t169228355 * ___pageParserFilter_48;
	// System.Collections.Generic.List`1<System.Web.UI.UnknownAttributeDescriptor> System.Web.UI.TemplateParser::unknownMainAttributes
	List_1_t3058591440 * ___unknownMainAttributes_49;
	// System.Collections.Generic.Stack`1<System.String> System.Web.UI.TemplateParser::includeDirs
	Stack_1_t2690840144 * ___includeDirs_50;
	// System.Collections.Generic.List`1<System.String> System.Web.UI.TemplateParser::registeredTagNames
	List_1_t3319525431 * ___registeredTagNames_51;
	// System.Web.Compilation.ILocation System.Web.UI.TemplateParser::directiveLocation
	Il2CppObject * ___directiveLocation_52;
	// System.Int32 System.Web.UI.TemplateParser::appAssemblyIndex
	int32_t ___appAssemblyIndex_53;
	// System.Web.Compilation.AspGenerator System.Web.UI.TemplateParser::<AspGenerator>k__BackingField
	AspGenerator_t2810561191 * ___U3CAspGeneratorU3Ek__BackingField_55;

public:
	inline static int32_t get_offset_of_inputFile_5() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___inputFile_5)); }
	inline String_t* get_inputFile_5() const { return ___inputFile_5; }
	inline String_t** get_address_of_inputFile_5() { return &___inputFile_5; }
	inline void set_inputFile_5(String_t* value)
	{
		___inputFile_5 = value;
		Il2CppCodeGenWriteBarrier(&___inputFile_5, value);
	}

	inline static int32_t get_offset_of_mainAttributes_6() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___mainAttributes_6)); }
	inline Il2CppObject * get_mainAttributes_6() const { return ___mainAttributes_6; }
	inline Il2CppObject ** get_address_of_mainAttributes_6() { return &___mainAttributes_6; }
	inline void set_mainAttributes_6(Il2CppObject * value)
	{
		___mainAttributes_6 = value;
		Il2CppCodeGenWriteBarrier(&___mainAttributes_6, value);
	}

	inline static int32_t get_offset_of_dependencies_7() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___dependencies_7)); }
	inline ArrayList_t2718874744 * get_dependencies_7() const { return ___dependencies_7; }
	inline ArrayList_t2718874744 ** get_address_of_dependencies_7() { return &___dependencies_7; }
	inline void set_dependencies_7(ArrayList_t2718874744 * value)
	{
		___dependencies_7 = value;
		Il2CppCodeGenWriteBarrier(&___dependencies_7, value);
	}

	inline static int32_t get_offset_of_assemblies_8() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___assemblies_8)); }
	inline ArrayList_t2718874744 * get_assemblies_8() const { return ___assemblies_8; }
	inline ArrayList_t2718874744 ** get_address_of_assemblies_8() { return &___assemblies_8; }
	inline void set_assemblies_8(ArrayList_t2718874744 * value)
	{
		___assemblies_8 = value;
		Il2CppCodeGenWriteBarrier(&___assemblies_8, value);
	}

	inline static int32_t get_offset_of_anames_9() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___anames_9)); }
	inline Il2CppObject * get_anames_9() const { return ___anames_9; }
	inline Il2CppObject ** get_address_of_anames_9() { return &___anames_9; }
	inline void set_anames_9(Il2CppObject * value)
	{
		___anames_9 = value;
		Il2CppCodeGenWriteBarrier(&___anames_9, value);
	}

	inline static int32_t get_offset_of_binDirAssemblies_10() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___binDirAssemblies_10)); }
	inline StringU5BU5D_t1281789340* get_binDirAssemblies_10() const { return ___binDirAssemblies_10; }
	inline StringU5BU5D_t1281789340** get_address_of_binDirAssemblies_10() { return &___binDirAssemblies_10; }
	inline void set_binDirAssemblies_10(StringU5BU5D_t1281789340* value)
	{
		___binDirAssemblies_10 = value;
		Il2CppCodeGenWriteBarrier(&___binDirAssemblies_10, value);
	}

	inline static int32_t get_offset_of_namespacesCache_11() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___namespacesCache_11)); }
	inline Dictionary_2_t4177511560 * get_namespacesCache_11() const { return ___namespacesCache_11; }
	inline Dictionary_2_t4177511560 ** get_address_of_namespacesCache_11() { return &___namespacesCache_11; }
	inline void set_namespacesCache_11(Dictionary_2_t4177511560 * value)
	{
		___namespacesCache_11 = value;
		Il2CppCodeGenWriteBarrier(&___namespacesCache_11, value);
	}

	inline static int32_t get_offset_of_imports_12() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___imports_12)); }
	inline Dictionary_2_t4177511560 * get_imports_12() const { return ___imports_12; }
	inline Dictionary_2_t4177511560 ** get_address_of_imports_12() { return &___imports_12; }
	inline void set_imports_12(Dictionary_2_t4177511560 * value)
	{
		___imports_12 = value;
		Il2CppCodeGenWriteBarrier(&___imports_12, value);
	}

	inline static int32_t get_offset_of_interfaces_13() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___interfaces_13)); }
	inline List_1_t3319525431 * get_interfaces_13() const { return ___interfaces_13; }
	inline List_1_t3319525431 ** get_address_of_interfaces_13() { return &___interfaces_13; }
	inline void set_interfaces_13(List_1_t3319525431 * value)
	{
		___interfaces_13 = value;
		Il2CppCodeGenWriteBarrier(&___interfaces_13, value);
	}

	inline static int32_t get_offset_of_scripts_14() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___scripts_14)); }
	inline List_1_t4067502771 * get_scripts_14() const { return ___scripts_14; }
	inline List_1_t4067502771 ** get_address_of_scripts_14() { return &___scripts_14; }
	inline void set_scripts_14(List_1_t4067502771 * value)
	{
		___scripts_14 = value;
		Il2CppCodeGenWriteBarrier(&___scripts_14, value);
	}

	inline static int32_t get_offset_of_baseType_15() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___baseType_15)); }
	inline Type_t * get_baseType_15() const { return ___baseType_15; }
	inline Type_t ** get_address_of_baseType_15() { return &___baseType_15; }
	inline void set_baseType_15(Type_t * value)
	{
		___baseType_15 = value;
		Il2CppCodeGenWriteBarrier(&___baseType_15, value);
	}

	inline static int32_t get_offset_of_baseTypeIsGlobal_16() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___baseTypeIsGlobal_16)); }
	inline bool get_baseTypeIsGlobal_16() const { return ___baseTypeIsGlobal_16; }
	inline bool* get_address_of_baseTypeIsGlobal_16() { return &___baseTypeIsGlobal_16; }
	inline void set_baseTypeIsGlobal_16(bool value)
	{
		___baseTypeIsGlobal_16 = value;
	}

	inline static int32_t get_offset_of_className_17() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___className_17)); }
	inline String_t* get_className_17() const { return ___className_17; }
	inline String_t** get_address_of_className_17() { return &___className_17; }
	inline void set_className_17(String_t* value)
	{
		___className_17 = value;
		Il2CppCodeGenWriteBarrier(&___className_17, value);
	}

	inline static int32_t get_offset_of_rootBuilder_18() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___rootBuilder_18)); }
	inline RootBuilder_t1594119744 * get_rootBuilder_18() const { return ___rootBuilder_18; }
	inline RootBuilder_t1594119744 ** get_address_of_rootBuilder_18() { return &___rootBuilder_18; }
	inline void set_rootBuilder_18(RootBuilder_t1594119744 * value)
	{
		___rootBuilder_18 = value;
		Il2CppCodeGenWriteBarrier(&___rootBuilder_18, value);
	}

	inline static int32_t get_offset_of_debug_19() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___debug_19)); }
	inline bool get_debug_19() const { return ___debug_19; }
	inline bool* get_address_of_debug_19() { return &___debug_19; }
	inline void set_debug_19(bool value)
	{
		___debug_19 = value;
	}

	inline static int32_t get_offset_of_compilerOptions_20() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___compilerOptions_20)); }
	inline String_t* get_compilerOptions_20() const { return ___compilerOptions_20; }
	inline String_t** get_address_of_compilerOptions_20() { return &___compilerOptions_20; }
	inline void set_compilerOptions_20(String_t* value)
	{
		___compilerOptions_20 = value;
		Il2CppCodeGenWriteBarrier(&___compilerOptions_20, value);
	}

	inline static int32_t get_offset_of_language_21() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___language_21)); }
	inline String_t* get_language_21() const { return ___language_21; }
	inline String_t** get_address_of_language_21() { return &___language_21; }
	inline void set_language_21(String_t* value)
	{
		___language_21 = value;
		Il2CppCodeGenWriteBarrier(&___language_21, value);
	}

	inline static int32_t get_offset_of_implicitLanguage_22() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___implicitLanguage_22)); }
	inline bool get_implicitLanguage_22() const { return ___implicitLanguage_22; }
	inline bool* get_address_of_implicitLanguage_22() { return &___implicitLanguage_22; }
	inline void set_implicitLanguage_22(bool value)
	{
		___implicitLanguage_22 = value;
	}

	inline static int32_t get_offset_of_strictOn_23() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___strictOn_23)); }
	inline bool get_strictOn_23() const { return ___strictOn_23; }
	inline bool* get_address_of_strictOn_23() { return &___strictOn_23; }
	inline void set_strictOn_23(bool value)
	{
		___strictOn_23 = value;
	}

	inline static int32_t get_offset_of_explicitOn_24() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___explicitOn_24)); }
	inline bool get_explicitOn_24() const { return ___explicitOn_24; }
	inline bool* get_address_of_explicitOn_24() { return &___explicitOn_24; }
	inline void set_explicitOn_24(bool value)
	{
		___explicitOn_24 = value;
	}

	inline static int32_t get_offset_of_linePragmasOn_25() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___linePragmasOn_25)); }
	inline bool get_linePragmasOn_25() const { return ___linePragmasOn_25; }
	inline bool* get_address_of_linePragmasOn_25() { return &___linePragmasOn_25; }
	inline void set_linePragmasOn_25(bool value)
	{
		___linePragmasOn_25 = value;
	}

	inline static int32_t get_offset_of_output_cache_26() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___output_cache_26)); }
	inline bool get_output_cache_26() const { return ___output_cache_26; }
	inline bool* get_address_of_output_cache_26() { return &___output_cache_26; }
	inline void set_output_cache_26(bool value)
	{
		___output_cache_26 = value;
	}

	inline static int32_t get_offset_of_oc_duration_27() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___oc_duration_27)); }
	inline int32_t get_oc_duration_27() const { return ___oc_duration_27; }
	inline int32_t* get_address_of_oc_duration_27() { return &___oc_duration_27; }
	inline void set_oc_duration_27(int32_t value)
	{
		___oc_duration_27 = value;
	}

	inline static int32_t get_offset_of_oc_header_28() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___oc_header_28)); }
	inline String_t* get_oc_header_28() const { return ___oc_header_28; }
	inline String_t** get_address_of_oc_header_28() { return &___oc_header_28; }
	inline void set_oc_header_28(String_t* value)
	{
		___oc_header_28 = value;
		Il2CppCodeGenWriteBarrier(&___oc_header_28, value);
	}

	inline static int32_t get_offset_of_oc_custom_29() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___oc_custom_29)); }
	inline String_t* get_oc_custom_29() const { return ___oc_custom_29; }
	inline String_t** get_address_of_oc_custom_29() { return &___oc_custom_29; }
	inline void set_oc_custom_29(String_t* value)
	{
		___oc_custom_29 = value;
		Il2CppCodeGenWriteBarrier(&___oc_custom_29, value);
	}

	inline static int32_t get_offset_of_oc_param_30() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___oc_param_30)); }
	inline String_t* get_oc_param_30() const { return ___oc_param_30; }
	inline String_t** get_address_of_oc_param_30() { return &___oc_param_30; }
	inline void set_oc_param_30(String_t* value)
	{
		___oc_param_30 = value;
		Il2CppCodeGenWriteBarrier(&___oc_param_30, value);
	}

	inline static int32_t get_offset_of_oc_controls_31() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___oc_controls_31)); }
	inline String_t* get_oc_controls_31() const { return ___oc_controls_31; }
	inline String_t** get_address_of_oc_controls_31() { return &___oc_controls_31; }
	inline void set_oc_controls_31(String_t* value)
	{
		___oc_controls_31 = value;
		Il2CppCodeGenWriteBarrier(&___oc_controls_31, value);
	}

	inline static int32_t get_offset_of_oc_content_encodings_32() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___oc_content_encodings_32)); }
	inline String_t* get_oc_content_encodings_32() const { return ___oc_content_encodings_32; }
	inline String_t** get_address_of_oc_content_encodings_32() { return &___oc_content_encodings_32; }
	inline void set_oc_content_encodings_32(String_t* value)
	{
		___oc_content_encodings_32 = value;
		Il2CppCodeGenWriteBarrier(&___oc_content_encodings_32, value);
	}

	inline static int32_t get_offset_of_oc_cacheprofile_33() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___oc_cacheprofile_33)); }
	inline String_t* get_oc_cacheprofile_33() const { return ___oc_cacheprofile_33; }
	inline String_t** get_address_of_oc_cacheprofile_33() { return &___oc_cacheprofile_33; }
	inline void set_oc_cacheprofile_33(String_t* value)
	{
		___oc_cacheprofile_33 = value;
		Il2CppCodeGenWriteBarrier(&___oc_cacheprofile_33, value);
	}

	inline static int32_t get_offset_of_oc_sqldependency_34() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___oc_sqldependency_34)); }
	inline String_t* get_oc_sqldependency_34() const { return ___oc_sqldependency_34; }
	inline String_t** get_address_of_oc_sqldependency_34() { return &___oc_sqldependency_34; }
	inline void set_oc_sqldependency_34(String_t* value)
	{
		___oc_sqldependency_34 = value;
		Il2CppCodeGenWriteBarrier(&___oc_sqldependency_34, value);
	}

	inline static int32_t get_offset_of_oc_nostore_35() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___oc_nostore_35)); }
	inline bool get_oc_nostore_35() const { return ___oc_nostore_35; }
	inline bool* get_address_of_oc_nostore_35() { return &___oc_nostore_35; }
	inline void set_oc_nostore_35(bool value)
	{
		___oc_nostore_35 = value;
	}

	inline static int32_t get_offset_of_oc_parsed_params_36() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___oc_parsed_params_36)); }
	inline int32_t get_oc_parsed_params_36() const { return ___oc_parsed_params_36; }
	inline int32_t* get_address_of_oc_parsed_params_36() { return &___oc_parsed_params_36; }
	inline void set_oc_parsed_params_36(int32_t value)
	{
		___oc_parsed_params_36 = value;
	}

	inline static int32_t get_offset_of_oc_shared_37() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___oc_shared_37)); }
	inline bool get_oc_shared_37() const { return ___oc_shared_37; }
	inline bool* get_address_of_oc_shared_37() { return &___oc_shared_37; }
	inline void set_oc_shared_37(bool value)
	{
		___oc_shared_37 = value;
	}

	inline static int32_t get_offset_of_oc_location_38() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___oc_location_38)); }
	inline int32_t get_oc_location_38() const { return ___oc_location_38; }
	inline int32_t* get_address_of_oc_location_38() { return &___oc_location_38; }
	inline void set_oc_location_38(int32_t value)
	{
		___oc_location_38 = value;
	}

	inline static int32_t get_offset_of_allowedMainDirectives_39() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___allowedMainDirectives_39)); }
	inline int32_t get_allowedMainDirectives_39() const { return ___allowedMainDirectives_39; }
	inline int32_t* get_address_of_allowedMainDirectives_39() { return &___allowedMainDirectives_39; }
	inline void set_allowedMainDirectives_39(int32_t value)
	{
		___allowedMainDirectives_39 = value;
	}

	inline static int32_t get_offset_of_md5checksum_40() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___md5checksum_40)); }
	inline ByteU5BU5D_t4116647657* get_md5checksum_40() const { return ___md5checksum_40; }
	inline ByteU5BU5D_t4116647657** get_address_of_md5checksum_40() { return &___md5checksum_40; }
	inline void set_md5checksum_40(ByteU5BU5D_t4116647657* value)
	{
		___md5checksum_40 = value;
		Il2CppCodeGenWriteBarrier(&___md5checksum_40, value);
	}

	inline static int32_t get_offset_of_src_41() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___src_41)); }
	inline String_t* get_src_41() const { return ___src_41; }
	inline String_t** get_address_of_src_41() { return &___src_41; }
	inline void set_src_41(String_t* value)
	{
		___src_41 = value;
		Il2CppCodeGenWriteBarrier(&___src_41, value);
	}

	inline static int32_t get_offset_of_srcIsLegacy_42() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___srcIsLegacy_42)); }
	inline bool get_srcIsLegacy_42() const { return ___srcIsLegacy_42; }
	inline bool* get_address_of_srcIsLegacy_42() { return &___srcIsLegacy_42; }
	inline void set_srcIsLegacy_42(bool value)
	{
		___srcIsLegacy_42 = value;
	}

	inline static int32_t get_offset_of_partialClassName_43() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___partialClassName_43)); }
	inline String_t* get_partialClassName_43() const { return ___partialClassName_43; }
	inline String_t** get_address_of_partialClassName_43() { return &___partialClassName_43; }
	inline void set_partialClassName_43(String_t* value)
	{
		___partialClassName_43 = value;
		Il2CppCodeGenWriteBarrier(&___partialClassName_43, value);
	}

	inline static int32_t get_offset_of_codeFileBaseClass_44() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___codeFileBaseClass_44)); }
	inline String_t* get_codeFileBaseClass_44() const { return ___codeFileBaseClass_44; }
	inline String_t** get_address_of_codeFileBaseClass_44() { return &___codeFileBaseClass_44; }
	inline void set_codeFileBaseClass_44(String_t* value)
	{
		___codeFileBaseClass_44 = value;
		Il2CppCodeGenWriteBarrier(&___codeFileBaseClass_44, value);
	}

	inline static int32_t get_offset_of_metaResourceKey_45() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___metaResourceKey_45)); }
	inline String_t* get_metaResourceKey_45() const { return ___metaResourceKey_45; }
	inline String_t** get_address_of_metaResourceKey_45() { return &___metaResourceKey_45; }
	inline void set_metaResourceKey_45(String_t* value)
	{
		___metaResourceKey_45 = value;
		Il2CppCodeGenWriteBarrier(&___metaResourceKey_45, value);
	}

	inline static int32_t get_offset_of_codeFileBaseClassType_46() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___codeFileBaseClassType_46)); }
	inline Type_t * get_codeFileBaseClassType_46() const { return ___codeFileBaseClassType_46; }
	inline Type_t ** get_address_of_codeFileBaseClassType_46() { return &___codeFileBaseClassType_46; }
	inline void set_codeFileBaseClassType_46(Type_t * value)
	{
		___codeFileBaseClassType_46 = value;
		Il2CppCodeGenWriteBarrier(&___codeFileBaseClassType_46, value);
	}

	inline static int32_t get_offset_of_pageParserFilterType_47() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___pageParserFilterType_47)); }
	inline Type_t * get_pageParserFilterType_47() const { return ___pageParserFilterType_47; }
	inline Type_t ** get_address_of_pageParserFilterType_47() { return &___pageParserFilterType_47; }
	inline void set_pageParserFilterType_47(Type_t * value)
	{
		___pageParserFilterType_47 = value;
		Il2CppCodeGenWriteBarrier(&___pageParserFilterType_47, value);
	}

	inline static int32_t get_offset_of_pageParserFilter_48() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___pageParserFilter_48)); }
	inline PageParserFilter_t169228355 * get_pageParserFilter_48() const { return ___pageParserFilter_48; }
	inline PageParserFilter_t169228355 ** get_address_of_pageParserFilter_48() { return &___pageParserFilter_48; }
	inline void set_pageParserFilter_48(PageParserFilter_t169228355 * value)
	{
		___pageParserFilter_48 = value;
		Il2CppCodeGenWriteBarrier(&___pageParserFilter_48, value);
	}

	inline static int32_t get_offset_of_unknownMainAttributes_49() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___unknownMainAttributes_49)); }
	inline List_1_t3058591440 * get_unknownMainAttributes_49() const { return ___unknownMainAttributes_49; }
	inline List_1_t3058591440 ** get_address_of_unknownMainAttributes_49() { return &___unknownMainAttributes_49; }
	inline void set_unknownMainAttributes_49(List_1_t3058591440 * value)
	{
		___unknownMainAttributes_49 = value;
		Il2CppCodeGenWriteBarrier(&___unknownMainAttributes_49, value);
	}

	inline static int32_t get_offset_of_includeDirs_50() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___includeDirs_50)); }
	inline Stack_1_t2690840144 * get_includeDirs_50() const { return ___includeDirs_50; }
	inline Stack_1_t2690840144 ** get_address_of_includeDirs_50() { return &___includeDirs_50; }
	inline void set_includeDirs_50(Stack_1_t2690840144 * value)
	{
		___includeDirs_50 = value;
		Il2CppCodeGenWriteBarrier(&___includeDirs_50, value);
	}

	inline static int32_t get_offset_of_registeredTagNames_51() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___registeredTagNames_51)); }
	inline List_1_t3319525431 * get_registeredTagNames_51() const { return ___registeredTagNames_51; }
	inline List_1_t3319525431 ** get_address_of_registeredTagNames_51() { return &___registeredTagNames_51; }
	inline void set_registeredTagNames_51(List_1_t3319525431 * value)
	{
		___registeredTagNames_51 = value;
		Il2CppCodeGenWriteBarrier(&___registeredTagNames_51, value);
	}

	inline static int32_t get_offset_of_directiveLocation_52() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___directiveLocation_52)); }
	inline Il2CppObject * get_directiveLocation_52() const { return ___directiveLocation_52; }
	inline Il2CppObject ** get_address_of_directiveLocation_52() { return &___directiveLocation_52; }
	inline void set_directiveLocation_52(Il2CppObject * value)
	{
		___directiveLocation_52 = value;
		Il2CppCodeGenWriteBarrier(&___directiveLocation_52, value);
	}

	inline static int32_t get_offset_of_appAssemblyIndex_53() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___appAssemblyIndex_53)); }
	inline int32_t get_appAssemblyIndex_53() const { return ___appAssemblyIndex_53; }
	inline int32_t* get_address_of_appAssemblyIndex_53() { return &___appAssemblyIndex_53; }
	inline void set_appAssemblyIndex_53(int32_t value)
	{
		___appAssemblyIndex_53 = value;
	}

	inline static int32_t get_offset_of_U3CAspGeneratorU3Ek__BackingField_55() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626, ___U3CAspGeneratorU3Ek__BackingField_55)); }
	inline AspGenerator_t2810561191 * get_U3CAspGeneratorU3Ek__BackingField_55() const { return ___U3CAspGeneratorU3Ek__BackingField_55; }
	inline AspGenerator_t2810561191 ** get_address_of_U3CAspGeneratorU3Ek__BackingField_55() { return &___U3CAspGeneratorU3Ek__BackingField_55; }
	inline void set_U3CAspGeneratorU3Ek__BackingField_55(AspGenerator_t2810561191 * value)
	{
		___U3CAspGeneratorU3Ek__BackingField_55 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAspGeneratorU3Ek__BackingField_55, value);
	}
};

struct TemplateParser_t24149626_StaticFields
{
public:
	// System.Int64 System.Web.UI.TemplateParser::autoClassCounter
	int64_t ___autoClassCounter_54;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Web.UI.TemplateParser::<>f__switch$map4
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map4_56;

public:
	inline static int32_t get_offset_of_autoClassCounter_54() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626_StaticFields, ___autoClassCounter_54)); }
	inline int64_t get_autoClassCounter_54() const { return ___autoClassCounter_54; }
	inline int64_t* get_address_of_autoClassCounter_54() { return &___autoClassCounter_54; }
	inline void set_autoClassCounter_54(int64_t value)
	{
		___autoClassCounter_54 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4_56() { return static_cast<int32_t>(offsetof(TemplateParser_t24149626_StaticFields, ___U3CU3Ef__switchU24map4_56)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map4_56() const { return ___U3CU3Ef__switchU24map4_56; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map4_56() { return &___U3CU3Ef__switchU24map4_56; }
	inline void set_U3CU3Ef__switchU24map4_56(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map4_56 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map4_56, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
