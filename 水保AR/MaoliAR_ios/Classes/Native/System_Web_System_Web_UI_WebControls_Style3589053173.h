﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_ComponentModel_Component3620823400.h"

// System.Web.UI.StateBag
struct StateBag_t282928164;
// System.Web.UI.WebControls.FontInfo
struct FontInfo_t1360706239;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.WebControls.Style
struct  Style_t3589053173  : public Component_t3620823400
{
public:
	// System.Int32 System.Web.UI.WebControls.Style::styles
	int32_t ___styles_4;
	// System.Int32 System.Web.UI.WebControls.Style::stylesTraked
	int32_t ___stylesTraked_5;
	// System.Web.UI.StateBag System.Web.UI.WebControls.Style::viewstate
	StateBag_t282928164 * ___viewstate_6;
	// System.Web.UI.WebControls.FontInfo System.Web.UI.WebControls.Style::fontinfo
	FontInfo_t1360706239 * ___fontinfo_7;
	// System.Boolean System.Web.UI.WebControls.Style::tracking
	bool ___tracking_8;
	// System.Boolean System.Web.UI.WebControls.Style::_isSharedViewState
	bool ____isSharedViewState_9;
	// System.String System.Web.UI.WebControls.Style::registered_class
	String_t* ___registered_class_10;

public:
	inline static int32_t get_offset_of_styles_4() { return static_cast<int32_t>(offsetof(Style_t3589053173, ___styles_4)); }
	inline int32_t get_styles_4() const { return ___styles_4; }
	inline int32_t* get_address_of_styles_4() { return &___styles_4; }
	inline void set_styles_4(int32_t value)
	{
		___styles_4 = value;
	}

	inline static int32_t get_offset_of_stylesTraked_5() { return static_cast<int32_t>(offsetof(Style_t3589053173, ___stylesTraked_5)); }
	inline int32_t get_stylesTraked_5() const { return ___stylesTraked_5; }
	inline int32_t* get_address_of_stylesTraked_5() { return &___stylesTraked_5; }
	inline void set_stylesTraked_5(int32_t value)
	{
		___stylesTraked_5 = value;
	}

	inline static int32_t get_offset_of_viewstate_6() { return static_cast<int32_t>(offsetof(Style_t3589053173, ___viewstate_6)); }
	inline StateBag_t282928164 * get_viewstate_6() const { return ___viewstate_6; }
	inline StateBag_t282928164 ** get_address_of_viewstate_6() { return &___viewstate_6; }
	inline void set_viewstate_6(StateBag_t282928164 * value)
	{
		___viewstate_6 = value;
		Il2CppCodeGenWriteBarrier(&___viewstate_6, value);
	}

	inline static int32_t get_offset_of_fontinfo_7() { return static_cast<int32_t>(offsetof(Style_t3589053173, ___fontinfo_7)); }
	inline FontInfo_t1360706239 * get_fontinfo_7() const { return ___fontinfo_7; }
	inline FontInfo_t1360706239 ** get_address_of_fontinfo_7() { return &___fontinfo_7; }
	inline void set_fontinfo_7(FontInfo_t1360706239 * value)
	{
		___fontinfo_7 = value;
		Il2CppCodeGenWriteBarrier(&___fontinfo_7, value);
	}

	inline static int32_t get_offset_of_tracking_8() { return static_cast<int32_t>(offsetof(Style_t3589053173, ___tracking_8)); }
	inline bool get_tracking_8() const { return ___tracking_8; }
	inline bool* get_address_of_tracking_8() { return &___tracking_8; }
	inline void set_tracking_8(bool value)
	{
		___tracking_8 = value;
	}

	inline static int32_t get_offset_of__isSharedViewState_9() { return static_cast<int32_t>(offsetof(Style_t3589053173, ____isSharedViewState_9)); }
	inline bool get__isSharedViewState_9() const { return ____isSharedViewState_9; }
	inline bool* get_address_of__isSharedViewState_9() { return &____isSharedViewState_9; }
	inline void set__isSharedViewState_9(bool value)
	{
		____isSharedViewState_9 = value;
	}

	inline static int32_t get_offset_of_registered_class_10() { return static_cast<int32_t>(offsetof(Style_t3589053173, ___registered_class_10)); }
	inline String_t* get_registered_class_10() const { return ___registered_class_10; }
	inline String_t** get_address_of_registered_class_10() { return &___registered_class_10; }
	inline void set_registered_class_10(String_t* value)
	{
		___registered_class_10 = value;
		Il2CppCodeGenWriteBarrier(&___registered_class_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
