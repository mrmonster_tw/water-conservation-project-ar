﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// System.Collections.Generic.List`1<UnityEngine.Object>
struct List_1_t2103082695;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectPuter
struct  ObjectPuter_t3420787522  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Object> ObjectPuter::putObjects
	List_1_t2103082695 * ___putObjects_2;

public:
	inline static int32_t get_offset_of_putObjects_2() { return static_cast<int32_t>(offsetof(ObjectPuter_t3420787522, ___putObjects_2)); }
	inline List_1_t2103082695 * get_putObjects_2() const { return ___putObjects_2; }
	inline List_1_t2103082695 ** get_address_of_putObjects_2() { return &___putObjects_2; }
	inline void set_putObjects_2(List_1_t2103082695 * value)
	{
		___putObjects_2 = value;
		Il2CppCodeGenWriteBarrier(&___putObjects_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
