﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_System_ServiceModel_PeerTransp3378031475.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.PeerTransportSecuritySettings
struct  PeerTransportSecuritySettings_t3643429466  : public Il2CppObject
{
public:
	// System.ServiceModel.PeerTransportCredentialType System.ServiceModel.PeerTransportSecuritySettings::<CredentialType>k__BackingField
	int32_t ___U3CCredentialTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CCredentialTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PeerTransportSecuritySettings_t3643429466, ___U3CCredentialTypeU3Ek__BackingField_0)); }
	inline int32_t get_U3CCredentialTypeU3Ek__BackingField_0() const { return ___U3CCredentialTypeU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CCredentialTypeU3Ek__BackingField_0() { return &___U3CCredentialTypeU3Ek__BackingField_0; }
	inline void set_U3CCredentialTypeU3Ek__BackingField_0(int32_t value)
	{
		___U3CCredentialTypeU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
