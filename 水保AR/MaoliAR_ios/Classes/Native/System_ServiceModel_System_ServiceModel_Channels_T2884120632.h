﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Channels.TcpBinaryFrameManager/MyBinaryReader
struct MyBinaryReader_t337765585;
// System.ServiceModel.Channels.TcpBinaryFrameManager/MyBinaryWriter
struct MyBinaryWriter_t3129094156;
// System.IO.Stream
struct Stream_t1273022909;
// System.IO.MemoryStream
struct MemoryStream_t94973147;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Xml.XmlBinaryReaderSession
struct XmlBinaryReaderSession_t3909229952;
// System.ServiceModel.Channels.TcpBinaryFrameManager/MyXmlBinaryWriterSession
struct MyXmlBinaryWriterSession_t2644113389;
// System.Uri
struct Uri_t100236324;
// System.ServiceModel.Channels.MessageEncoder
struct MessageEncoder_t3063398011;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.TcpBinaryFrameManager
struct  TcpBinaryFrameManager_t2884120632  : public Il2CppObject
{
public:
	// System.ServiceModel.Channels.TcpBinaryFrameManager/MyBinaryReader System.ServiceModel.Channels.TcpBinaryFrameManager::reader
	MyBinaryReader_t337765585 * ___reader_0;
	// System.ServiceModel.Channels.TcpBinaryFrameManager/MyBinaryWriter System.ServiceModel.Channels.TcpBinaryFrameManager::writer
	MyBinaryWriter_t3129094156 * ___writer_1;
	// System.IO.Stream System.ServiceModel.Channels.TcpBinaryFrameManager::s
	Stream_t1273022909 * ___s_2;
	// System.IO.MemoryStream System.ServiceModel.Channels.TcpBinaryFrameManager::buffer
	MemoryStream_t94973147 * ___buffer_3;
	// System.Boolean System.ServiceModel.Channels.TcpBinaryFrameManager::is_service_side
	bool ___is_service_side_4;
	// System.Int32 System.ServiceModel.Channels.TcpBinaryFrameManager::mode
	int32_t ___mode_5;
	// System.Xml.XmlBinaryReaderSession System.ServiceModel.Channels.TcpBinaryFrameManager::reader_session
	XmlBinaryReaderSession_t3909229952 * ___reader_session_7;
	// System.Int32 System.ServiceModel.Channels.TcpBinaryFrameManager::reader_session_items
	int32_t ___reader_session_items_8;
	// System.Byte[] System.ServiceModel.Channels.TcpBinaryFrameManager::eof_buffer
	ByteU5BU5D_t4116647657* ___eof_buffer_9;
	// System.ServiceModel.Channels.TcpBinaryFrameManager/MyXmlBinaryWriterSession System.ServiceModel.Channels.TcpBinaryFrameManager::writer_session
	MyXmlBinaryWriterSession_t2644113389 * ___writer_session_10;
	// System.Byte System.ServiceModel.Channels.TcpBinaryFrameManager::<EncodingRecord>k__BackingField
	uint8_t ___U3CEncodingRecordU3Ek__BackingField_11;
	// System.Uri System.ServiceModel.Channels.TcpBinaryFrameManager::<Via>k__BackingField
	Uri_t100236324 * ___U3CViaU3Ek__BackingField_12;
	// System.ServiceModel.Channels.MessageEncoder System.ServiceModel.Channels.TcpBinaryFrameManager::<Encoder>k__BackingField
	MessageEncoder_t3063398011 * ___U3CEncoderU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_reader_0() { return static_cast<int32_t>(offsetof(TcpBinaryFrameManager_t2884120632, ___reader_0)); }
	inline MyBinaryReader_t337765585 * get_reader_0() const { return ___reader_0; }
	inline MyBinaryReader_t337765585 ** get_address_of_reader_0() { return &___reader_0; }
	inline void set_reader_0(MyBinaryReader_t337765585 * value)
	{
		___reader_0 = value;
		Il2CppCodeGenWriteBarrier(&___reader_0, value);
	}

	inline static int32_t get_offset_of_writer_1() { return static_cast<int32_t>(offsetof(TcpBinaryFrameManager_t2884120632, ___writer_1)); }
	inline MyBinaryWriter_t3129094156 * get_writer_1() const { return ___writer_1; }
	inline MyBinaryWriter_t3129094156 ** get_address_of_writer_1() { return &___writer_1; }
	inline void set_writer_1(MyBinaryWriter_t3129094156 * value)
	{
		___writer_1 = value;
		Il2CppCodeGenWriteBarrier(&___writer_1, value);
	}

	inline static int32_t get_offset_of_s_2() { return static_cast<int32_t>(offsetof(TcpBinaryFrameManager_t2884120632, ___s_2)); }
	inline Stream_t1273022909 * get_s_2() const { return ___s_2; }
	inline Stream_t1273022909 ** get_address_of_s_2() { return &___s_2; }
	inline void set_s_2(Stream_t1273022909 * value)
	{
		___s_2 = value;
		Il2CppCodeGenWriteBarrier(&___s_2, value);
	}

	inline static int32_t get_offset_of_buffer_3() { return static_cast<int32_t>(offsetof(TcpBinaryFrameManager_t2884120632, ___buffer_3)); }
	inline MemoryStream_t94973147 * get_buffer_3() const { return ___buffer_3; }
	inline MemoryStream_t94973147 ** get_address_of_buffer_3() { return &___buffer_3; }
	inline void set_buffer_3(MemoryStream_t94973147 * value)
	{
		___buffer_3 = value;
		Il2CppCodeGenWriteBarrier(&___buffer_3, value);
	}

	inline static int32_t get_offset_of_is_service_side_4() { return static_cast<int32_t>(offsetof(TcpBinaryFrameManager_t2884120632, ___is_service_side_4)); }
	inline bool get_is_service_side_4() const { return ___is_service_side_4; }
	inline bool* get_address_of_is_service_side_4() { return &___is_service_side_4; }
	inline void set_is_service_side_4(bool value)
	{
		___is_service_side_4 = value;
	}

	inline static int32_t get_offset_of_mode_5() { return static_cast<int32_t>(offsetof(TcpBinaryFrameManager_t2884120632, ___mode_5)); }
	inline int32_t get_mode_5() const { return ___mode_5; }
	inline int32_t* get_address_of_mode_5() { return &___mode_5; }
	inline void set_mode_5(int32_t value)
	{
		___mode_5 = value;
	}

	inline static int32_t get_offset_of_reader_session_7() { return static_cast<int32_t>(offsetof(TcpBinaryFrameManager_t2884120632, ___reader_session_7)); }
	inline XmlBinaryReaderSession_t3909229952 * get_reader_session_7() const { return ___reader_session_7; }
	inline XmlBinaryReaderSession_t3909229952 ** get_address_of_reader_session_7() { return &___reader_session_7; }
	inline void set_reader_session_7(XmlBinaryReaderSession_t3909229952 * value)
	{
		___reader_session_7 = value;
		Il2CppCodeGenWriteBarrier(&___reader_session_7, value);
	}

	inline static int32_t get_offset_of_reader_session_items_8() { return static_cast<int32_t>(offsetof(TcpBinaryFrameManager_t2884120632, ___reader_session_items_8)); }
	inline int32_t get_reader_session_items_8() const { return ___reader_session_items_8; }
	inline int32_t* get_address_of_reader_session_items_8() { return &___reader_session_items_8; }
	inline void set_reader_session_items_8(int32_t value)
	{
		___reader_session_items_8 = value;
	}

	inline static int32_t get_offset_of_eof_buffer_9() { return static_cast<int32_t>(offsetof(TcpBinaryFrameManager_t2884120632, ___eof_buffer_9)); }
	inline ByteU5BU5D_t4116647657* get_eof_buffer_9() const { return ___eof_buffer_9; }
	inline ByteU5BU5D_t4116647657** get_address_of_eof_buffer_9() { return &___eof_buffer_9; }
	inline void set_eof_buffer_9(ByteU5BU5D_t4116647657* value)
	{
		___eof_buffer_9 = value;
		Il2CppCodeGenWriteBarrier(&___eof_buffer_9, value);
	}

	inline static int32_t get_offset_of_writer_session_10() { return static_cast<int32_t>(offsetof(TcpBinaryFrameManager_t2884120632, ___writer_session_10)); }
	inline MyXmlBinaryWriterSession_t2644113389 * get_writer_session_10() const { return ___writer_session_10; }
	inline MyXmlBinaryWriterSession_t2644113389 ** get_address_of_writer_session_10() { return &___writer_session_10; }
	inline void set_writer_session_10(MyXmlBinaryWriterSession_t2644113389 * value)
	{
		___writer_session_10 = value;
		Il2CppCodeGenWriteBarrier(&___writer_session_10, value);
	}

	inline static int32_t get_offset_of_U3CEncodingRecordU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(TcpBinaryFrameManager_t2884120632, ___U3CEncodingRecordU3Ek__BackingField_11)); }
	inline uint8_t get_U3CEncodingRecordU3Ek__BackingField_11() const { return ___U3CEncodingRecordU3Ek__BackingField_11; }
	inline uint8_t* get_address_of_U3CEncodingRecordU3Ek__BackingField_11() { return &___U3CEncodingRecordU3Ek__BackingField_11; }
	inline void set_U3CEncodingRecordU3Ek__BackingField_11(uint8_t value)
	{
		___U3CEncodingRecordU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CViaU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(TcpBinaryFrameManager_t2884120632, ___U3CViaU3Ek__BackingField_12)); }
	inline Uri_t100236324 * get_U3CViaU3Ek__BackingField_12() const { return ___U3CViaU3Ek__BackingField_12; }
	inline Uri_t100236324 ** get_address_of_U3CViaU3Ek__BackingField_12() { return &___U3CViaU3Ek__BackingField_12; }
	inline void set_U3CViaU3Ek__BackingField_12(Uri_t100236324 * value)
	{
		___U3CViaU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CViaU3Ek__BackingField_12, value);
	}

	inline static int32_t get_offset_of_U3CEncoderU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(TcpBinaryFrameManager_t2884120632, ___U3CEncoderU3Ek__BackingField_13)); }
	inline MessageEncoder_t3063398011 * get_U3CEncoderU3Ek__BackingField_13() const { return ___U3CEncoderU3Ek__BackingField_13; }
	inline MessageEncoder_t3063398011 ** get_address_of_U3CEncoderU3Ek__BackingField_13() { return &___U3CEncoderU3Ek__BackingField_13; }
	inline void set_U3CEncoderU3Ek__BackingField_13(MessageEncoder_t3063398011 * value)
	{
		___U3CEncoderU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CEncoderU3Ek__BackingField_13, value);
	}
};

struct TcpBinaryFrameManager_t2884120632_StaticFields
{
public:
	// System.Byte[] System.ServiceModel.Channels.TcpBinaryFrameManager::empty_bytes
	ByteU5BU5D_t4116647657* ___empty_bytes_6;

public:
	inline static int32_t get_offset_of_empty_bytes_6() { return static_cast<int32_t>(offsetof(TcpBinaryFrameManager_t2884120632_StaticFields, ___empty_bytes_6)); }
	inline ByteU5BU5D_t4116647657* get_empty_bytes_6() const { return ___empty_bytes_6; }
	inline ByteU5BU5D_t4116647657** get_address_of_empty_bytes_6() { return &___empty_bytes_6; }
	inline void set_empty_bytes_6(ByteU5BU5D_t4116647657* value)
	{
		___empty_bytes_6 = value;
		Il2CppCodeGenWriteBarrier(&___empty_bytes_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
