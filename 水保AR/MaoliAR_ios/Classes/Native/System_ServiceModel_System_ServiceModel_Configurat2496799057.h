﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.X509ServiceCertificateAuthenticationElement
struct  X509ServiceCertificateAuthenticationElement_t2496799057  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct X509ServiceCertificateAuthenticationElement_t2496799057_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.X509ServiceCertificateAuthenticationElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.X509ServiceCertificateAuthenticationElement::certificate_validation_mode
	ConfigurationProperty_t3590861854 * ___certificate_validation_mode_14;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.X509ServiceCertificateAuthenticationElement::custom_certificate_validator_type
	ConfigurationProperty_t3590861854 * ___custom_certificate_validator_type_15;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.X509ServiceCertificateAuthenticationElement::revocation_mode
	ConfigurationProperty_t3590861854 * ___revocation_mode_16;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.X509ServiceCertificateAuthenticationElement::trusted_store_location
	ConfigurationProperty_t3590861854 * ___trusted_store_location_17;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(X509ServiceCertificateAuthenticationElement_t2496799057_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier(&___properties_13, value);
	}

	inline static int32_t get_offset_of_certificate_validation_mode_14() { return static_cast<int32_t>(offsetof(X509ServiceCertificateAuthenticationElement_t2496799057_StaticFields, ___certificate_validation_mode_14)); }
	inline ConfigurationProperty_t3590861854 * get_certificate_validation_mode_14() const { return ___certificate_validation_mode_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_certificate_validation_mode_14() { return &___certificate_validation_mode_14; }
	inline void set_certificate_validation_mode_14(ConfigurationProperty_t3590861854 * value)
	{
		___certificate_validation_mode_14 = value;
		Il2CppCodeGenWriteBarrier(&___certificate_validation_mode_14, value);
	}

	inline static int32_t get_offset_of_custom_certificate_validator_type_15() { return static_cast<int32_t>(offsetof(X509ServiceCertificateAuthenticationElement_t2496799057_StaticFields, ___custom_certificate_validator_type_15)); }
	inline ConfigurationProperty_t3590861854 * get_custom_certificate_validator_type_15() const { return ___custom_certificate_validator_type_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_custom_certificate_validator_type_15() { return &___custom_certificate_validator_type_15; }
	inline void set_custom_certificate_validator_type_15(ConfigurationProperty_t3590861854 * value)
	{
		___custom_certificate_validator_type_15 = value;
		Il2CppCodeGenWriteBarrier(&___custom_certificate_validator_type_15, value);
	}

	inline static int32_t get_offset_of_revocation_mode_16() { return static_cast<int32_t>(offsetof(X509ServiceCertificateAuthenticationElement_t2496799057_StaticFields, ___revocation_mode_16)); }
	inline ConfigurationProperty_t3590861854 * get_revocation_mode_16() const { return ___revocation_mode_16; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_revocation_mode_16() { return &___revocation_mode_16; }
	inline void set_revocation_mode_16(ConfigurationProperty_t3590861854 * value)
	{
		___revocation_mode_16 = value;
		Il2CppCodeGenWriteBarrier(&___revocation_mode_16, value);
	}

	inline static int32_t get_offset_of_trusted_store_location_17() { return static_cast<int32_t>(offsetof(X509ServiceCertificateAuthenticationElement_t2496799057_StaticFields, ___trusted_store_location_17)); }
	inline ConfigurationProperty_t3590861854 * get_trusted_store_location_17() const { return ___trusted_store_location_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_trusted_store_location_17() { return &___trusted_store_location_17; }
	inline void set_trusted_store_location_17(ConfigurationProperty_t3590861854 * value)
	{
		___trusted_store_location_17 = value;
		Il2CppCodeGenWriteBarrier(&___trusted_store_location_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
