﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Net.WebHeaderCollection
struct WebHeaderCollection_t1942268960;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.HttpRequestMessageProperty
struct  HttpRequestMessageProperty_t1615723811  : public Il2CppObject
{
public:
	// System.Net.WebHeaderCollection System.ServiceModel.Channels.HttpRequestMessageProperty::headers
	WebHeaderCollection_t1942268960 * ___headers_0;
	// System.String System.ServiceModel.Channels.HttpRequestMessageProperty::method
	String_t* ___method_1;
	// System.String System.ServiceModel.Channels.HttpRequestMessageProperty::query_string
	String_t* ___query_string_2;

public:
	inline static int32_t get_offset_of_headers_0() { return static_cast<int32_t>(offsetof(HttpRequestMessageProperty_t1615723811, ___headers_0)); }
	inline WebHeaderCollection_t1942268960 * get_headers_0() const { return ___headers_0; }
	inline WebHeaderCollection_t1942268960 ** get_address_of_headers_0() { return &___headers_0; }
	inline void set_headers_0(WebHeaderCollection_t1942268960 * value)
	{
		___headers_0 = value;
		Il2CppCodeGenWriteBarrier(&___headers_0, value);
	}

	inline static int32_t get_offset_of_method_1() { return static_cast<int32_t>(offsetof(HttpRequestMessageProperty_t1615723811, ___method_1)); }
	inline String_t* get_method_1() const { return ___method_1; }
	inline String_t** get_address_of_method_1() { return &___method_1; }
	inline void set_method_1(String_t* value)
	{
		___method_1 = value;
		Il2CppCodeGenWriteBarrier(&___method_1, value);
	}

	inline static int32_t get_offset_of_query_string_2() { return static_cast<int32_t>(offsetof(HttpRequestMessageProperty_t1615723811, ___query_string_2)); }
	inline String_t* get_query_string_2() const { return ___query_string_2; }
	inline String_t** get_address_of_query_string_2() { return &___query_string_2; }
	inline void set_query_string_2(String_t* value)
	{
		___query_string_2 = value;
		Il2CppCodeGenWriteBarrier(&___query_string_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
