﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<Mono.Web.Util.SettingsMappingWhatContents>
struct List_1_t213083887;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Web.Util.SettingsMappingWhat
struct  SettingsMappingWhat_t1462987973  : public Il2CppObject
{
public:
	// System.String Mono.Web.Util.SettingsMappingWhat::_value
	String_t* ____value_0;
	// System.Collections.Generic.List`1<Mono.Web.Util.SettingsMappingWhatContents> Mono.Web.Util.SettingsMappingWhat::_contents
	List_1_t213083887 * ____contents_1;

public:
	inline static int32_t get_offset_of__value_0() { return static_cast<int32_t>(offsetof(SettingsMappingWhat_t1462987973, ____value_0)); }
	inline String_t* get__value_0() const { return ____value_0; }
	inline String_t** get_address_of__value_0() { return &____value_0; }
	inline void set__value_0(String_t* value)
	{
		____value_0 = value;
		Il2CppCodeGenWriteBarrier(&____value_0, value);
	}

	inline static int32_t get_offset_of__contents_1() { return static_cast<int32_t>(offsetof(SettingsMappingWhat_t1462987973, ____contents_1)); }
	inline List_1_t213083887 * get__contents_1() const { return ____contents_1; }
	inline List_1_t213083887 ** get_address_of__contents_1() { return &____contents_1; }
	inline void set__contents_1(List_1_t213083887 * value)
	{
		____contents_1 = value;
		Il2CppCodeGenWriteBarrier(&____contents_1, value);
	}
};

struct SettingsMappingWhat_t1462987973_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Web.Util.SettingsMappingWhat::<>f__switch$map0
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map0_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_2() { return static_cast<int32_t>(offsetof(SettingsMappingWhat_t1462987973_StaticFields, ___U3CU3Ef__switchU24map0_2)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map0_2() const { return ___U3CU3Ef__switchU24map0_2; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map0_2() { return &___U3CU3Ef__switchU24map0_2; }
	inline void set_U3CU3Ef__switchU24map0_2(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map0_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
