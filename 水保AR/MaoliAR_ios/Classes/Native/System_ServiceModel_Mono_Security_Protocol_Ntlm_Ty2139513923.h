﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_Mono_Security_Protocol_Ntlm_Mes422981883.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Ntlm.Type1Message
struct  Type1Message_t2139513923  : public MessageBase_t422981883
{
public:
	// System.String Mono.Security.Protocol.Ntlm.Type1Message::_host
	String_t* ____host_6;
	// System.String Mono.Security.Protocol.Ntlm.Type1Message::_domain
	String_t* ____domain_7;

public:
	inline static int32_t get_offset_of__host_6() { return static_cast<int32_t>(offsetof(Type1Message_t2139513923, ____host_6)); }
	inline String_t* get__host_6() const { return ____host_6; }
	inline String_t** get_address_of__host_6() { return &____host_6; }
	inline void set__host_6(String_t* value)
	{
		____host_6 = value;
		Il2CppCodeGenWriteBarrier(&____host_6, value);
	}

	inline static int32_t get_offset_of__domain_7() { return static_cast<int32_t>(offsetof(Type1Message_t2139513923, ____domain_7)); }
	inline String_t* get__domain_7() const { return ____domain_7; }
	inline String_t** get_address_of__domain_7() { return &____domain_7; }
	inline void set__domain_7(String_t* value)
	{
		____domain_7 = value;
		Il2CppCodeGenWriteBarrier(&____domain_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
