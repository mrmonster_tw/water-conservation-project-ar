﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"
#include "System_System_ComponentModel_BindingDirection2741897897.h"

// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.TemplateContainerAttribute
struct  TemplateContainerAttribute_t1232723725  : public Attribute_t861562559
{
public:
	// System.Type System.Web.UI.TemplateContainerAttribute::containerType
	Type_t * ___containerType_0;
	// System.ComponentModel.BindingDirection System.Web.UI.TemplateContainerAttribute::direction
	int32_t ___direction_1;

public:
	inline static int32_t get_offset_of_containerType_0() { return static_cast<int32_t>(offsetof(TemplateContainerAttribute_t1232723725, ___containerType_0)); }
	inline Type_t * get_containerType_0() const { return ___containerType_0; }
	inline Type_t ** get_address_of_containerType_0() { return &___containerType_0; }
	inline void set_containerType_0(Type_t * value)
	{
		___containerType_0 = value;
		Il2CppCodeGenWriteBarrier(&___containerType_0, value);
	}

	inline static int32_t get_offset_of_direction_1() { return static_cast<int32_t>(offsetof(TemplateContainerAttribute_t1232723725, ___direction_1)); }
	inline int32_t get_direction_1() const { return ___direction_1; }
	inline int32_t* get_address_of_direction_1() { return &___direction_1; }
	inline void set_direction_1(int32_t value)
	{
		___direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
