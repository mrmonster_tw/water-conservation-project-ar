﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene2348375561.h"

// UnityEngine.AsyncOperation
struct AsyncOperation_t1445031843;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// MainMenu_Controller
struct MainMenu_Controller_t4276260900;
// System.Object
struct Il2CppObject;
// System.Predicate`1<UnityEngine.UI.Image>
struct Predicate_1_t3495563775;
// System.Predicate`1<UnityEngine.UI.Text>
struct Predicate_1_t2727176838;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainMenu_Controller/<Loading>c__Iterator1
struct  U3CLoadingU3Ec__Iterator1_t404879543  : public Il2CppObject
{
public:
	// UnityEngine.SceneManagement.Scene MainMenu_Controller/<Loading>c__Iterator1::<unLoadScene>__0
	Scene_t2348375561  ___U3CunLoadSceneU3E__0_0;
	// UnityEngine.AsyncOperation MainMenu_Controller/<Loading>c__Iterator1::<LoadTarget>__0
	AsyncOperation_t1445031843 * ___U3CLoadTargetU3E__0_1;
	// UnityEngine.UI.Image MainMenu_Controller/<Loading>c__Iterator1::<請稍後bg>__0
	Image_t2670269651 * ___U3CU8ACBU7A0DU5F8CbgU3E__0_2;
	// System.Single MainMenu_Controller/<Loading>c__Iterator1::<time>__0
	float ___U3CtimeU3E__0_3;
	// UnityEngine.GameObject[] MainMenu_Controller/<Loading>c__Iterator1::$locvar0
	GameObjectU5BU5D_t3328599146* ___U24locvar0_4;
	// System.Int32 MainMenu_Controller/<Loading>c__Iterator1::$locvar1
	int32_t ___U24locvar1_5;
	// MainMenu_Controller MainMenu_Controller/<Loading>c__Iterator1::$this
	MainMenu_Controller_t4276260900 * ___U24this_6;
	// System.Object MainMenu_Controller/<Loading>c__Iterator1::$current
	Il2CppObject * ___U24current_7;
	// System.Boolean MainMenu_Controller/<Loading>c__Iterator1::$disposing
	bool ___U24disposing_8;
	// System.Int32 MainMenu_Controller/<Loading>c__Iterator1::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CunLoadSceneU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator1_t404879543, ___U3CunLoadSceneU3E__0_0)); }
	inline Scene_t2348375561  get_U3CunLoadSceneU3E__0_0() const { return ___U3CunLoadSceneU3E__0_0; }
	inline Scene_t2348375561 * get_address_of_U3CunLoadSceneU3E__0_0() { return &___U3CunLoadSceneU3E__0_0; }
	inline void set_U3CunLoadSceneU3E__0_0(Scene_t2348375561  value)
	{
		___U3CunLoadSceneU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CLoadTargetU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator1_t404879543, ___U3CLoadTargetU3E__0_1)); }
	inline AsyncOperation_t1445031843 * get_U3CLoadTargetU3E__0_1() const { return ___U3CLoadTargetU3E__0_1; }
	inline AsyncOperation_t1445031843 ** get_address_of_U3CLoadTargetU3E__0_1() { return &___U3CLoadTargetU3E__0_1; }
	inline void set_U3CLoadTargetU3E__0_1(AsyncOperation_t1445031843 * value)
	{
		___U3CLoadTargetU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLoadTargetU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CU8ACBU7A0DU5F8CbgU3E__0_2() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator1_t404879543, ___U3CU8ACBU7A0DU5F8CbgU3E__0_2)); }
	inline Image_t2670269651 * get_U3CU8ACBU7A0DU5F8CbgU3E__0_2() const { return ___U3CU8ACBU7A0DU5F8CbgU3E__0_2; }
	inline Image_t2670269651 ** get_address_of_U3CU8ACBU7A0DU5F8CbgU3E__0_2() { return &___U3CU8ACBU7A0DU5F8CbgU3E__0_2; }
	inline void set_U3CU8ACBU7A0DU5F8CbgU3E__0_2(Image_t2670269651 * value)
	{
		___U3CU8ACBU7A0DU5F8CbgU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU8ACBU7A0DU5F8CbgU3E__0_2, value);
	}

	inline static int32_t get_offset_of_U3CtimeU3E__0_3() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator1_t404879543, ___U3CtimeU3E__0_3)); }
	inline float get_U3CtimeU3E__0_3() const { return ___U3CtimeU3E__0_3; }
	inline float* get_address_of_U3CtimeU3E__0_3() { return &___U3CtimeU3E__0_3; }
	inline void set_U3CtimeU3E__0_3(float value)
	{
		___U3CtimeU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_4() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator1_t404879543, ___U24locvar0_4)); }
	inline GameObjectU5BU5D_t3328599146* get_U24locvar0_4() const { return ___U24locvar0_4; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_U24locvar0_4() { return &___U24locvar0_4; }
	inline void set_U24locvar0_4(GameObjectU5BU5D_t3328599146* value)
	{
		___U24locvar0_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_4, value);
	}

	inline static int32_t get_offset_of_U24locvar1_5() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator1_t404879543, ___U24locvar1_5)); }
	inline int32_t get_U24locvar1_5() const { return ___U24locvar1_5; }
	inline int32_t* get_address_of_U24locvar1_5() { return &___U24locvar1_5; }
	inline void set_U24locvar1_5(int32_t value)
	{
		___U24locvar1_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator1_t404879543, ___U24this_6)); }
	inline MainMenu_Controller_t4276260900 * get_U24this_6() const { return ___U24this_6; }
	inline MainMenu_Controller_t4276260900 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(MainMenu_Controller_t4276260900 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_6, value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator1_t404879543, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator1_t404879543, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator1_t404879543, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

struct U3CLoadingU3Ec__Iterator1_t404879543_StaticFields
{
public:
	// System.Predicate`1<UnityEngine.UI.Image> MainMenu_Controller/<Loading>c__Iterator1::<>f__am$cache0
	Predicate_1_t3495563775 * ___U3CU3Ef__amU24cache0_10;
	// System.Predicate`1<UnityEngine.UI.Text> MainMenu_Controller/<Loading>c__Iterator1::<>f__am$cache1
	Predicate_1_t2727176838 * ___U3CU3Ef__amU24cache1_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_10() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator1_t404879543_StaticFields, ___U3CU3Ef__amU24cache0_10)); }
	inline Predicate_1_t3495563775 * get_U3CU3Ef__amU24cache0_10() const { return ___U3CU3Ef__amU24cache0_10; }
	inline Predicate_1_t3495563775 ** get_address_of_U3CU3Ef__amU24cache0_10() { return &___U3CU3Ef__amU24cache0_10; }
	inline void set_U3CU3Ef__amU24cache0_10(Predicate_1_t3495563775 * value)
	{
		___U3CU3Ef__amU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_11() { return static_cast<int32_t>(offsetof(U3CLoadingU3Ec__Iterator1_t404879543_StaticFields, ___U3CU3Ef__amU24cache1_11)); }
	inline Predicate_1_t2727176838 * get_U3CU3Ef__amU24cache1_11() const { return ___U3CU3Ef__amU24cache1_11; }
	inline Predicate_1_t2727176838 ** get_address_of_U3CU3Ef__amU24cache1_11() { return &___U3CU3Ef__amU24cache1_11; }
	inline void set_U3CU3Ef__amU24cache1_11(Predicate_1_t2727176838 * value)
	{
		___U3CU3Ef__amU24cache1_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
