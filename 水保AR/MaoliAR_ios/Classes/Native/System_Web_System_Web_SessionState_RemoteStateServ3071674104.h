﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_MarshalByRefObject2760389100.h"

// System.Web.Caching.Cache
struct Cache_t4020976765;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.SessionState.RemoteStateServer
struct  RemoteStateServer_t3071674104  : public MarshalByRefObject_t2760389100
{
public:
	// System.Web.Caching.Cache System.Web.SessionState.RemoteStateServer::cache
	Cache_t4020976765 * ___cache_1;

public:
	inline static int32_t get_offset_of_cache_1() { return static_cast<int32_t>(offsetof(RemoteStateServer_t3071674104, ___cache_1)); }
	inline Cache_t4020976765 * get_cache_1() const { return ___cache_1; }
	inline Cache_t4020976765 ** get_address_of_cache_1() { return &___cache_1; }
	inline void set_cache_1(Cache_t4020976765 * value)
	{
		___cache_1 = value;
		Il2CppCodeGenWriteBarrier(&___cache_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
