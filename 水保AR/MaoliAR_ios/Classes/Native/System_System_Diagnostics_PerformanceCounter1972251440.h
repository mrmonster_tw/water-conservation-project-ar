﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_ComponentModel_Component3620823400.h"
#include "mscorlib_System_IntPtr840150181.h"
#include "System_System_Diagnostics_PerformanceCounterType1913661149.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.PerformanceCounter
struct  PerformanceCounter_t1972251440  : public Component_t3620823400
{
public:
	// System.String System.Diagnostics.PerformanceCounter::categoryName
	String_t* ___categoryName_4;
	// System.String System.Diagnostics.PerformanceCounter::counterName
	String_t* ___counterName_5;
	// System.String System.Diagnostics.PerformanceCounter::instanceName
	String_t* ___instanceName_6;
	// System.String System.Diagnostics.PerformanceCounter::machineName
	String_t* ___machineName_7;
	// System.IntPtr System.Diagnostics.PerformanceCounter::impl
	IntPtr_t ___impl_8;
	// System.Diagnostics.PerformanceCounterType System.Diagnostics.PerformanceCounter::type
	int32_t ___type_9;
	// System.Boolean System.Diagnostics.PerformanceCounter::readOnly
	bool ___readOnly_10;
	// System.Boolean System.Diagnostics.PerformanceCounter::changed
	bool ___changed_11;
	// System.Boolean System.Diagnostics.PerformanceCounter::is_custom
	bool ___is_custom_12;

public:
	inline static int32_t get_offset_of_categoryName_4() { return static_cast<int32_t>(offsetof(PerformanceCounter_t1972251440, ___categoryName_4)); }
	inline String_t* get_categoryName_4() const { return ___categoryName_4; }
	inline String_t** get_address_of_categoryName_4() { return &___categoryName_4; }
	inline void set_categoryName_4(String_t* value)
	{
		___categoryName_4 = value;
		Il2CppCodeGenWriteBarrier(&___categoryName_4, value);
	}

	inline static int32_t get_offset_of_counterName_5() { return static_cast<int32_t>(offsetof(PerformanceCounter_t1972251440, ___counterName_5)); }
	inline String_t* get_counterName_5() const { return ___counterName_5; }
	inline String_t** get_address_of_counterName_5() { return &___counterName_5; }
	inline void set_counterName_5(String_t* value)
	{
		___counterName_5 = value;
		Il2CppCodeGenWriteBarrier(&___counterName_5, value);
	}

	inline static int32_t get_offset_of_instanceName_6() { return static_cast<int32_t>(offsetof(PerformanceCounter_t1972251440, ___instanceName_6)); }
	inline String_t* get_instanceName_6() const { return ___instanceName_6; }
	inline String_t** get_address_of_instanceName_6() { return &___instanceName_6; }
	inline void set_instanceName_6(String_t* value)
	{
		___instanceName_6 = value;
		Il2CppCodeGenWriteBarrier(&___instanceName_6, value);
	}

	inline static int32_t get_offset_of_machineName_7() { return static_cast<int32_t>(offsetof(PerformanceCounter_t1972251440, ___machineName_7)); }
	inline String_t* get_machineName_7() const { return ___machineName_7; }
	inline String_t** get_address_of_machineName_7() { return &___machineName_7; }
	inline void set_machineName_7(String_t* value)
	{
		___machineName_7 = value;
		Il2CppCodeGenWriteBarrier(&___machineName_7, value);
	}

	inline static int32_t get_offset_of_impl_8() { return static_cast<int32_t>(offsetof(PerformanceCounter_t1972251440, ___impl_8)); }
	inline IntPtr_t get_impl_8() const { return ___impl_8; }
	inline IntPtr_t* get_address_of_impl_8() { return &___impl_8; }
	inline void set_impl_8(IntPtr_t value)
	{
		___impl_8 = value;
	}

	inline static int32_t get_offset_of_type_9() { return static_cast<int32_t>(offsetof(PerformanceCounter_t1972251440, ___type_9)); }
	inline int32_t get_type_9() const { return ___type_9; }
	inline int32_t* get_address_of_type_9() { return &___type_9; }
	inline void set_type_9(int32_t value)
	{
		___type_9 = value;
	}

	inline static int32_t get_offset_of_readOnly_10() { return static_cast<int32_t>(offsetof(PerformanceCounter_t1972251440, ___readOnly_10)); }
	inline bool get_readOnly_10() const { return ___readOnly_10; }
	inline bool* get_address_of_readOnly_10() { return &___readOnly_10; }
	inline void set_readOnly_10(bool value)
	{
		___readOnly_10 = value;
	}

	inline static int32_t get_offset_of_changed_11() { return static_cast<int32_t>(offsetof(PerformanceCounter_t1972251440, ___changed_11)); }
	inline bool get_changed_11() const { return ___changed_11; }
	inline bool* get_address_of_changed_11() { return &___changed_11; }
	inline void set_changed_11(bool value)
	{
		___changed_11 = value;
	}

	inline static int32_t get_offset_of_is_custom_12() { return static_cast<int32_t>(offsetof(PerformanceCounter_t1972251440, ___is_custom_12)); }
	inline bool get_is_custom_12() const { return ___is_custom_12; }
	inline bool* get_address_of_is_custom_12() { return &___is_custom_12; }
	inline void set_is_custom_12(bool value)
	{
		___is_custom_12 = value;
	}
};

struct PerformanceCounter_t1972251440_StaticFields
{
public:
	// System.Int32 System.Diagnostics.PerformanceCounter::DefaultFileMappingSize
	int32_t ___DefaultFileMappingSize_13;

public:
	inline static int32_t get_offset_of_DefaultFileMappingSize_13() { return static_cast<int32_t>(offsetof(PerformanceCounter_t1972251440_StaticFields, ___DefaultFileMappingSize_13)); }
	inline int32_t get_DefaultFileMappingSize_13() const { return ___DefaultFileMappingSize_13; }
	inline int32_t* get_address_of_DefaultFileMappingSize_13() { return &___DefaultFileMappingSize_13; }
	inline void set_DefaultFileMappingSize_13(int32_t value)
	{
		___DefaultFileMappingSize_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
