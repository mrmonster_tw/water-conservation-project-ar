﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Dispatcher_456598713.h"

// System.ServiceModel.Dispatcher.DispatchRuntime
struct DispatchRuntime_t796075230;
// System.ServiceModel.Channels.IChannel
struct IChannel_t937207545;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.InputOrReplyRequestProcessor
struct  InputOrReplyRequestProcessor_t3584320896  : public BaseRequestProcessor_t456598713
{
public:
	// System.ServiceModel.Dispatcher.DispatchRuntime System.ServiceModel.Dispatcher.InputOrReplyRequestProcessor::dispatch_runtime
	DispatchRuntime_t796075230 * ___dispatch_runtime_4;
	// System.ServiceModel.Channels.IChannel System.ServiceModel.Dispatcher.InputOrReplyRequestProcessor::reply_or_input
	Il2CppObject * ___reply_or_input_5;

public:
	inline static int32_t get_offset_of_dispatch_runtime_4() { return static_cast<int32_t>(offsetof(InputOrReplyRequestProcessor_t3584320896, ___dispatch_runtime_4)); }
	inline DispatchRuntime_t796075230 * get_dispatch_runtime_4() const { return ___dispatch_runtime_4; }
	inline DispatchRuntime_t796075230 ** get_address_of_dispatch_runtime_4() { return &___dispatch_runtime_4; }
	inline void set_dispatch_runtime_4(DispatchRuntime_t796075230 * value)
	{
		___dispatch_runtime_4 = value;
		Il2CppCodeGenWriteBarrier(&___dispatch_runtime_4, value);
	}

	inline static int32_t get_offset_of_reply_or_input_5() { return static_cast<int32_t>(offsetof(InputOrReplyRequestProcessor_t3584320896, ___reply_or_input_5)); }
	inline Il2CppObject * get_reply_or_input_5() const { return ___reply_or_input_5; }
	inline Il2CppObject ** get_address_of_reply_or_input_5() { return &___reply_or_input_5; }
	inline void set_reply_or_input_5(Il2CppObject * value)
	{
		___reply_or_input_5 = value;
		Il2CppCodeGenWriteBarrier(&___reply_or_input_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
