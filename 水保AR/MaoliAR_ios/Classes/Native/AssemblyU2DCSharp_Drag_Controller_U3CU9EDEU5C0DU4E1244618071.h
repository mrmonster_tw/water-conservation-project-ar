﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// Drag_Controller
struct Drag_Controller_t1925480236;
// System.Object
struct Il2CppObject;
// System.Predicate`1<UnityEngine.UI.Image>
struct Predicate_1_t3495563775;
// System.Predicate`1<Drag_Controller>
struct Predicate_1_t2750774360;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Drag_Controller/<點對了>c__Iterator0
struct  U3CU9EDEU5C0DU4E86U3Ec__Iterator0_t1244618071  : public Il2CppObject
{
public:
	// Drag_Controller Drag_Controller/<點對了>c__Iterator0::$this
	Drag_Controller_t1925480236 * ___U24this_0;
	// System.Object Drag_Controller/<點對了>c__Iterator0::$current
	Il2CppObject * ___U24current_1;
	// System.Boolean Drag_Controller/<點對了>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Drag_Controller/<點對了>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CU9EDEU5C0DU4E86U3Ec__Iterator0_t1244618071, ___U24this_0)); }
	inline Drag_Controller_t1925480236 * get_U24this_0() const { return ___U24this_0; }
	inline Drag_Controller_t1925480236 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Drag_Controller_t1925480236 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_0, value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CU9EDEU5C0DU4E86U3Ec__Iterator0_t1244618071, ___U24current_1)); }
	inline Il2CppObject * get_U24current_1() const { return ___U24current_1; }
	inline Il2CppObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(Il2CppObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_1, value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CU9EDEU5C0DU4E86U3Ec__Iterator0_t1244618071, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CU9EDEU5C0DU4E86U3Ec__Iterator0_t1244618071, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

struct U3CU9EDEU5C0DU4E86U3Ec__Iterator0_t1244618071_StaticFields
{
public:
	// System.Predicate`1<UnityEngine.UI.Image> Drag_Controller/<點對了>c__Iterator0::<>f__am$cache0
	Predicate_1_t3495563775 * ___U3CU3Ef__amU24cache0_4;
	// System.Predicate`1<UnityEngine.UI.Image> Drag_Controller/<點對了>c__Iterator0::<>f__am$cache1
	Predicate_1_t3495563775 * ___U3CU3Ef__amU24cache1_5;
	// System.Predicate`1<Drag_Controller> Drag_Controller/<點對了>c__Iterator0::<>f__am$cache2
	Predicate_1_t2750774360 * ___U3CU3Ef__amU24cache2_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(U3CU9EDEU5C0DU4E86U3Ec__Iterator0_t1244618071_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Predicate_1_t3495563775 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Predicate_1_t3495563775 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Predicate_1_t3495563775 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(U3CU9EDEU5C0DU4E86U3Ec__Iterator0_t1244618071_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline Predicate_1_t3495563775 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline Predicate_1_t3495563775 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(Predicate_1_t3495563775 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_6() { return static_cast<int32_t>(offsetof(U3CU9EDEU5C0DU4E86U3Ec__Iterator0_t1244618071_StaticFields, ___U3CU3Ef__amU24cache2_6)); }
	inline Predicate_1_t2750774360 * get_U3CU3Ef__amU24cache2_6() const { return ___U3CU3Ef__amU24cache2_6; }
	inline Predicate_1_t2750774360 ** get_address_of_U3CU3Ef__amU24cache2_6() { return &___U3CU3Ef__amU24cache2_6; }
	inline void set_U3CU3Ef__amU24cache2_6(Predicate_1_t2750774360 * value)
	{
		___U3CU3Ef__amU24cache2_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
