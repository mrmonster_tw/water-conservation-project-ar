﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_System_ServiceModel_MessageCre1579346758.h"

// System.ServiceModel.Security.SecurityAlgorithmSuite
struct SecurityAlgorithmSuite_t2980594242;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.MessageSecurityOverTcp
struct  MessageSecurityOverTcp_t2089208958  : public Il2CppObject
{
public:
	// System.ServiceModel.Security.SecurityAlgorithmSuite System.ServiceModel.MessageSecurityOverTcp::alg_suite
	SecurityAlgorithmSuite_t2980594242 * ___alg_suite_0;
	// System.ServiceModel.MessageCredentialType System.ServiceModel.MessageSecurityOverTcp::client_credential_type
	int32_t ___client_credential_type_1;

public:
	inline static int32_t get_offset_of_alg_suite_0() { return static_cast<int32_t>(offsetof(MessageSecurityOverTcp_t2089208958, ___alg_suite_0)); }
	inline SecurityAlgorithmSuite_t2980594242 * get_alg_suite_0() const { return ___alg_suite_0; }
	inline SecurityAlgorithmSuite_t2980594242 ** get_address_of_alg_suite_0() { return &___alg_suite_0; }
	inline void set_alg_suite_0(SecurityAlgorithmSuite_t2980594242 * value)
	{
		___alg_suite_0 = value;
		Il2CppCodeGenWriteBarrier(&___alg_suite_0, value);
	}

	inline static int32_t get_offset_of_client_credential_type_1() { return static_cast<int32_t>(offsetof(MessageSecurityOverTcp_t2089208958, ___client_credential_type_1)); }
	inline int32_t get_client_credential_type_1() const { return ___client_credential_type_1; }
	inline int32_t* get_address_of_client_credential_type_1() { return &___client_credential_type_1; }
	inline void set_client_credential_type_1(int32_t value)
	{
		___client_credential_type_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
