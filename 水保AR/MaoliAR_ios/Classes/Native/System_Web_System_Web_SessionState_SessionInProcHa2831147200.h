﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_SessionState_SessionStateStor182923317.h"

// System.Web.Caching.CacheItemRemovedCallback
struct CacheItemRemovedCallback_t3206551617;
// System.Web.SessionState.SessionStateItemExpireCallback
struct SessionStateItemExpireCallback_t619504878;
// System.Web.HttpStaticObjectsCollection
struct HttpStaticObjectsCollection_t518175362;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.SessionState.SessionInProcHandler
struct  SessionInProcHandler_t2831147200  : public SessionStateStoreProviderBase_t182923317
{
public:
	// System.Web.Caching.CacheItemRemovedCallback System.Web.SessionState.SessionInProcHandler::removedCB
	CacheItemRemovedCallback_t3206551617 * ___removedCB_3;
	// System.Web.SessionState.SessionStateItemExpireCallback System.Web.SessionState.SessionInProcHandler::expireCallback
	SessionStateItemExpireCallback_t619504878 * ___expireCallback_4;
	// System.Web.HttpStaticObjectsCollection System.Web.SessionState.SessionInProcHandler::staticObjects
	HttpStaticObjectsCollection_t518175362 * ___staticObjects_5;

public:
	inline static int32_t get_offset_of_removedCB_3() { return static_cast<int32_t>(offsetof(SessionInProcHandler_t2831147200, ___removedCB_3)); }
	inline CacheItemRemovedCallback_t3206551617 * get_removedCB_3() const { return ___removedCB_3; }
	inline CacheItemRemovedCallback_t3206551617 ** get_address_of_removedCB_3() { return &___removedCB_3; }
	inline void set_removedCB_3(CacheItemRemovedCallback_t3206551617 * value)
	{
		___removedCB_3 = value;
		Il2CppCodeGenWriteBarrier(&___removedCB_3, value);
	}

	inline static int32_t get_offset_of_expireCallback_4() { return static_cast<int32_t>(offsetof(SessionInProcHandler_t2831147200, ___expireCallback_4)); }
	inline SessionStateItemExpireCallback_t619504878 * get_expireCallback_4() const { return ___expireCallback_4; }
	inline SessionStateItemExpireCallback_t619504878 ** get_address_of_expireCallback_4() { return &___expireCallback_4; }
	inline void set_expireCallback_4(SessionStateItemExpireCallback_t619504878 * value)
	{
		___expireCallback_4 = value;
		Il2CppCodeGenWriteBarrier(&___expireCallback_4, value);
	}

	inline static int32_t get_offset_of_staticObjects_5() { return static_cast<int32_t>(offsetof(SessionInProcHandler_t2831147200, ___staticObjects_5)); }
	inline HttpStaticObjectsCollection_t518175362 * get_staticObjects_5() const { return ___staticObjects_5; }
	inline HttpStaticObjectsCollection_t518175362 ** get_address_of_staticObjects_5() { return &___staticObjects_5; }
	inline void set_staticObjects_5(HttpStaticObjectsCollection_t518175362 * value)
	{
		___staticObjects_5 = value;
		Il2CppCodeGenWriteBarrier(&___staticObjects_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
