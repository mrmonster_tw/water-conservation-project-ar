﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Xml_System_Xml_Serialization_CodeGeneration2228734478.h"
#include "System_Web_Services_System_Web_Services_Description992977419.h"

// System.Xml.Serialization.XmlSerializerImplementation
struct XmlSerializerImplementation_t3900572101;
// System.Collections.Specialized.StringCollection
struct StringCollection_t167406615;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.WebReferenceOptions
struct  WebReferenceOptions_t1872617518  : public Il2CppObject
{
public:
	// System.Xml.Serialization.CodeGenerationOptions System.Web.Services.Description.WebReferenceOptions::code_options
	int32_t ___code_options_1;
	// System.Collections.Specialized.StringCollection System.Web.Services.Description.WebReferenceOptions::importer_extensions
	StringCollection_t167406615 * ___importer_extensions_2;
	// System.Web.Services.Description.ServiceDescriptionImportStyle System.Web.Services.Description.WebReferenceOptions::style
	int32_t ___style_3;

public:
	inline static int32_t get_offset_of_code_options_1() { return static_cast<int32_t>(offsetof(WebReferenceOptions_t1872617518, ___code_options_1)); }
	inline int32_t get_code_options_1() const { return ___code_options_1; }
	inline int32_t* get_address_of_code_options_1() { return &___code_options_1; }
	inline void set_code_options_1(int32_t value)
	{
		___code_options_1 = value;
	}

	inline static int32_t get_offset_of_importer_extensions_2() { return static_cast<int32_t>(offsetof(WebReferenceOptions_t1872617518, ___importer_extensions_2)); }
	inline StringCollection_t167406615 * get_importer_extensions_2() const { return ___importer_extensions_2; }
	inline StringCollection_t167406615 ** get_address_of_importer_extensions_2() { return &___importer_extensions_2; }
	inline void set_importer_extensions_2(StringCollection_t167406615 * value)
	{
		___importer_extensions_2 = value;
		Il2CppCodeGenWriteBarrier(&___importer_extensions_2, value);
	}

	inline static int32_t get_offset_of_style_3() { return static_cast<int32_t>(offsetof(WebReferenceOptions_t1872617518, ___style_3)); }
	inline int32_t get_style_3() const { return ___style_3; }
	inline int32_t* get_address_of_style_3() { return &___style_3; }
	inline void set_style_3(int32_t value)
	{
		___style_3 = value;
	}
};

struct WebReferenceOptions_t1872617518_StaticFields
{
public:
	// System.Xml.Serialization.XmlSerializerImplementation System.Web.Services.Description.WebReferenceOptions::implementation
	XmlSerializerImplementation_t3900572101 * ___implementation_0;

public:
	inline static int32_t get_offset_of_implementation_0() { return static_cast<int32_t>(offsetof(WebReferenceOptions_t1872617518_StaticFields, ___implementation_0)); }
	inline XmlSerializerImplementation_t3900572101 * get_implementation_0() const { return ___implementation_0; }
	inline XmlSerializerImplementation_t3900572101 ** get_address_of_implementation_0() { return &___implementation_0; }
	inline void set_implementation_0(XmlSerializerImplementation_t3900572101 * value)
	{
		___implementation_0 = value;
		Il2CppCodeGenWriteBarrier(&___implementation_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
