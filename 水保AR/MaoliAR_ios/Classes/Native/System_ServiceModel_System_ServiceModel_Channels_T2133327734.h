﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_M3531153504.h"

// System.ServiceModel.Channels.MessageVersion
struct MessageVersion_t1958933598;
// System.Xml.XmlDictionaryReaderQuotas
struct XmlDictionaryReaderQuotas_t173030297;
// System.Text.Encoding
struct Encoding_t1523322056;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.TextMessageEncodingBindingElement
struct  TextMessageEncodingBindingElement_t2133327734  : public MessageEncodingBindingElement_t3531153504
{
public:
	// System.ServiceModel.Channels.MessageVersion System.ServiceModel.Channels.TextMessageEncodingBindingElement::version
	MessageVersion_t1958933598 * ___version_0;
	// System.Int32 System.ServiceModel.Channels.TextMessageEncodingBindingElement::max_read_pool_size
	int32_t ___max_read_pool_size_1;
	// System.Int32 System.ServiceModel.Channels.TextMessageEncodingBindingElement::max_write_pool_size
	int32_t ___max_write_pool_size_2;
	// System.Xml.XmlDictionaryReaderQuotas System.ServiceModel.Channels.TextMessageEncodingBindingElement::quotas
	XmlDictionaryReaderQuotas_t173030297 * ___quotas_3;
	// System.Text.Encoding System.ServiceModel.Channels.TextMessageEncodingBindingElement::encoding
	Encoding_t1523322056 * ___encoding_4;

public:
	inline static int32_t get_offset_of_version_0() { return static_cast<int32_t>(offsetof(TextMessageEncodingBindingElement_t2133327734, ___version_0)); }
	inline MessageVersion_t1958933598 * get_version_0() const { return ___version_0; }
	inline MessageVersion_t1958933598 ** get_address_of_version_0() { return &___version_0; }
	inline void set_version_0(MessageVersion_t1958933598 * value)
	{
		___version_0 = value;
		Il2CppCodeGenWriteBarrier(&___version_0, value);
	}

	inline static int32_t get_offset_of_max_read_pool_size_1() { return static_cast<int32_t>(offsetof(TextMessageEncodingBindingElement_t2133327734, ___max_read_pool_size_1)); }
	inline int32_t get_max_read_pool_size_1() const { return ___max_read_pool_size_1; }
	inline int32_t* get_address_of_max_read_pool_size_1() { return &___max_read_pool_size_1; }
	inline void set_max_read_pool_size_1(int32_t value)
	{
		___max_read_pool_size_1 = value;
	}

	inline static int32_t get_offset_of_max_write_pool_size_2() { return static_cast<int32_t>(offsetof(TextMessageEncodingBindingElement_t2133327734, ___max_write_pool_size_2)); }
	inline int32_t get_max_write_pool_size_2() const { return ___max_write_pool_size_2; }
	inline int32_t* get_address_of_max_write_pool_size_2() { return &___max_write_pool_size_2; }
	inline void set_max_write_pool_size_2(int32_t value)
	{
		___max_write_pool_size_2 = value;
	}

	inline static int32_t get_offset_of_quotas_3() { return static_cast<int32_t>(offsetof(TextMessageEncodingBindingElement_t2133327734, ___quotas_3)); }
	inline XmlDictionaryReaderQuotas_t173030297 * get_quotas_3() const { return ___quotas_3; }
	inline XmlDictionaryReaderQuotas_t173030297 ** get_address_of_quotas_3() { return &___quotas_3; }
	inline void set_quotas_3(XmlDictionaryReaderQuotas_t173030297 * value)
	{
		___quotas_3 = value;
		Il2CppCodeGenWriteBarrier(&___quotas_3, value);
	}

	inline static int32_t get_offset_of_encoding_4() { return static_cast<int32_t>(offsetof(TextMessageEncodingBindingElement_t2133327734, ___encoding_4)); }
	inline Encoding_t1523322056 * get_encoding_4() const { return ___encoding_4; }
	inline Encoding_t1523322056 ** get_address_of_encoding_4() { return &___encoding_4; }
	inline void set_encoding_4(Encoding_t1523322056 * value)
	{
		___encoding_4 = value;
		Il2CppCodeGenWriteBarrier(&___encoding_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
