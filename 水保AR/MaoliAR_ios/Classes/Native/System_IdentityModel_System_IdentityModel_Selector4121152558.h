﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Selector1397312864.h"

// System.IdentityModel.Selectors.X509CertificateValidator
struct X509CertificateValidator_t1375267704;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Selectors.X509SecurityTokenAuthenticator
struct  X509SecurityTokenAuthenticator_t4121152558  : public SecurityTokenAuthenticator_t1397312864
{
public:
	// System.Boolean System.IdentityModel.Selectors.X509SecurityTokenAuthenticator::map_to_windows
	bool ___map_to_windows_0;
	// System.Boolean System.IdentityModel.Selectors.X509SecurityTokenAuthenticator::include_win_groups
	bool ___include_win_groups_1;
	// System.IdentityModel.Selectors.X509CertificateValidator System.IdentityModel.Selectors.X509SecurityTokenAuthenticator::validator
	X509CertificateValidator_t1375267704 * ___validator_2;

public:
	inline static int32_t get_offset_of_map_to_windows_0() { return static_cast<int32_t>(offsetof(X509SecurityTokenAuthenticator_t4121152558, ___map_to_windows_0)); }
	inline bool get_map_to_windows_0() const { return ___map_to_windows_0; }
	inline bool* get_address_of_map_to_windows_0() { return &___map_to_windows_0; }
	inline void set_map_to_windows_0(bool value)
	{
		___map_to_windows_0 = value;
	}

	inline static int32_t get_offset_of_include_win_groups_1() { return static_cast<int32_t>(offsetof(X509SecurityTokenAuthenticator_t4121152558, ___include_win_groups_1)); }
	inline bool get_include_win_groups_1() const { return ___include_win_groups_1; }
	inline bool* get_address_of_include_win_groups_1() { return &___include_win_groups_1; }
	inline void set_include_win_groups_1(bool value)
	{
		___include_win_groups_1 = value;
	}

	inline static int32_t get_offset_of_validator_2() { return static_cast<int32_t>(offsetof(X509SecurityTokenAuthenticator_t4121152558, ___validator_2)); }
	inline X509CertificateValidator_t1375267704 * get_validator_2() const { return ___validator_2; }
	inline X509CertificateValidator_t1375267704 ** get_address_of_validator_2() { return &___validator_2; }
	inline void set_validator_2(X509CertificateValidator_t1375267704 * value)
	{
		___validator_2 = value;
		Il2CppCodeGenWriteBarrier(&___validator_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
