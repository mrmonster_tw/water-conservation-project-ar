﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Ch259291852.h"

// System.ServiceModel.Channels.IChannelListener`1<System.Object>
struct IChannelListener_1_t2538428116;
// System.Transactions.TransactionScope
struct TransactionScope_t3249669472;
// System.ServiceModel.TransactionProtocol
struct TransactionProtocol_t3972232485;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.TransactionChannelListener`1<System.Object>
struct  TransactionChannelListener_1_t2791487089  : public ChannelListenerBase_1_t259291852
{
public:
	// System.ServiceModel.Channels.IChannelListener`1<TChannel> System.ServiceModel.Channels.TransactionChannelListener`1::inner_listener
	Il2CppObject* ___inner_listener_12;
	// System.Transactions.TransactionScope System.ServiceModel.Channels.TransactionChannelListener`1::txscope
	TransactionScope_t3249669472 * ___txscope_13;
	// System.ServiceModel.TransactionProtocol System.ServiceModel.Channels.TransactionChannelListener`1::protocol
	TransactionProtocol_t3972232485 * ___protocol_14;

public:
	inline static int32_t get_offset_of_inner_listener_12() { return static_cast<int32_t>(offsetof(TransactionChannelListener_1_t2791487089, ___inner_listener_12)); }
	inline Il2CppObject* get_inner_listener_12() const { return ___inner_listener_12; }
	inline Il2CppObject** get_address_of_inner_listener_12() { return &___inner_listener_12; }
	inline void set_inner_listener_12(Il2CppObject* value)
	{
		___inner_listener_12 = value;
		Il2CppCodeGenWriteBarrier(&___inner_listener_12, value);
	}

	inline static int32_t get_offset_of_txscope_13() { return static_cast<int32_t>(offsetof(TransactionChannelListener_1_t2791487089, ___txscope_13)); }
	inline TransactionScope_t3249669472 * get_txscope_13() const { return ___txscope_13; }
	inline TransactionScope_t3249669472 ** get_address_of_txscope_13() { return &___txscope_13; }
	inline void set_txscope_13(TransactionScope_t3249669472 * value)
	{
		___txscope_13 = value;
		Il2CppCodeGenWriteBarrier(&___txscope_13, value);
	}

	inline static int32_t get_offset_of_protocol_14() { return static_cast<int32_t>(offsetof(TransactionChannelListener_1_t2791487089, ___protocol_14)); }
	inline TransactionProtocol_t3972232485 * get_protocol_14() const { return ___protocol_14; }
	inline TransactionProtocol_t3972232485 ** get_address_of_protocol_14() { return &___protocol_14; }
	inline void set_protocol_14(TransactionProtocol_t3972232485 * value)
	{
		___protocol_14 = value;
		Il2CppCodeGenWriteBarrier(&___protocol_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
