﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3999355687.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1334875082.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2030707387.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_C491321292.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2932745847.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingSettings
struct  ColorGradingSettings_t1923563914 
{
public:
	// System.Boolean UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingSettings::enabled
	bool ___enabled_0;
	// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingPrecision UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingSettings::precision
	int32_t ___precision_1;
	// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorWheelsSettings UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingSettings::colorWheels
	ColorWheelsSettings_t1334875082  ___colorWheels_2;
	// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/BasicsSettings UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingSettings::basics
	BasicsSettings_t2030707387  ___basics_3;
	// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ChannelMixerSettings UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingSettings::channelMixer
	ChannelMixerSettings_t491321292  ___channelMixer_4;
	// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/CurvesSettings UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingSettings::curves
	CurvesSettings_t2932745847  ___curves_5;
	// System.Boolean UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingSettings::useDithering
	bool ___useDithering_6;
	// System.Boolean UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingSettings::showDebug
	bool ___showDebug_7;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(ColorGradingSettings_t1923563914, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}

	inline static int32_t get_offset_of_precision_1() { return static_cast<int32_t>(offsetof(ColorGradingSettings_t1923563914, ___precision_1)); }
	inline int32_t get_precision_1() const { return ___precision_1; }
	inline int32_t* get_address_of_precision_1() { return &___precision_1; }
	inline void set_precision_1(int32_t value)
	{
		___precision_1 = value;
	}

	inline static int32_t get_offset_of_colorWheels_2() { return static_cast<int32_t>(offsetof(ColorGradingSettings_t1923563914, ___colorWheels_2)); }
	inline ColorWheelsSettings_t1334875082  get_colorWheels_2() const { return ___colorWheels_2; }
	inline ColorWheelsSettings_t1334875082 * get_address_of_colorWheels_2() { return &___colorWheels_2; }
	inline void set_colorWheels_2(ColorWheelsSettings_t1334875082  value)
	{
		___colorWheels_2 = value;
	}

	inline static int32_t get_offset_of_basics_3() { return static_cast<int32_t>(offsetof(ColorGradingSettings_t1923563914, ___basics_3)); }
	inline BasicsSettings_t2030707387  get_basics_3() const { return ___basics_3; }
	inline BasicsSettings_t2030707387 * get_address_of_basics_3() { return &___basics_3; }
	inline void set_basics_3(BasicsSettings_t2030707387  value)
	{
		___basics_3 = value;
	}

	inline static int32_t get_offset_of_channelMixer_4() { return static_cast<int32_t>(offsetof(ColorGradingSettings_t1923563914, ___channelMixer_4)); }
	inline ChannelMixerSettings_t491321292  get_channelMixer_4() const { return ___channelMixer_4; }
	inline ChannelMixerSettings_t491321292 * get_address_of_channelMixer_4() { return &___channelMixer_4; }
	inline void set_channelMixer_4(ChannelMixerSettings_t491321292  value)
	{
		___channelMixer_4 = value;
	}

	inline static int32_t get_offset_of_curves_5() { return static_cast<int32_t>(offsetof(ColorGradingSettings_t1923563914, ___curves_5)); }
	inline CurvesSettings_t2932745847  get_curves_5() const { return ___curves_5; }
	inline CurvesSettings_t2932745847 * get_address_of_curves_5() { return &___curves_5; }
	inline void set_curves_5(CurvesSettings_t2932745847  value)
	{
		___curves_5 = value;
	}

	inline static int32_t get_offset_of_useDithering_6() { return static_cast<int32_t>(offsetof(ColorGradingSettings_t1923563914, ___useDithering_6)); }
	inline bool get_useDithering_6() const { return ___useDithering_6; }
	inline bool* get_address_of_useDithering_6() { return &___useDithering_6; }
	inline void set_useDithering_6(bool value)
	{
		___useDithering_6 = value;
	}

	inline static int32_t get_offset_of_showDebug_7() { return static_cast<int32_t>(offsetof(ColorGradingSettings_t1923563914, ___showDebug_7)); }
	inline bool get_showDebug_7() const { return ___showDebug_7; }
	inline bool* get_address_of_showDebug_7() { return &___showDebug_7; }
	inline void set_showDebug_7(bool value)
	{
		___showDebug_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingSettings
struct ColorGradingSettings_t1923563914_marshaled_pinvoke
{
	int32_t ___enabled_0;
	int32_t ___precision_1;
	ColorWheelsSettings_t1334875082  ___colorWheels_2;
	BasicsSettings_t2030707387  ___basics_3;
	ChannelMixerSettings_t491321292_marshaled_pinvoke ___channelMixer_4;
	CurvesSettings_t2932745847_marshaled_pinvoke ___curves_5;
	int32_t ___useDithering_6;
	int32_t ___showDebug_7;
};
// Native definition for COM marshalling of UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingSettings
struct ColorGradingSettings_t1923563914_marshaled_com
{
	int32_t ___enabled_0;
	int32_t ___precision_1;
	ColorWheelsSettings_t1334875082  ___colorWheels_2;
	BasicsSettings_t2030707387  ___basics_3;
	ChannelMixerSettings_t491321292_marshaled_com ___channelMixer_4;
	CurvesSettings_t2932745847_marshaled_com ___curves_5;
	int32_t ___useDithering_6;
	int32_t ___showDebug_7;
};
