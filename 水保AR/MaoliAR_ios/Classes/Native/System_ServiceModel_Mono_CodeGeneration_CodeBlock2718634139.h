﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_Mono_CodeGeneration_CodeItem493730090.h"

// System.Collections.ArrayList
struct ArrayList_t2718874744;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.CodeGeneration.CodeBlock
struct  CodeBlock_t2718634139  : public CodeItem_t493730090
{
public:
	// System.Collections.ArrayList Mono.CodeGeneration.CodeBlock::statements
	ArrayList_t2718874744 * ___statements_0;

public:
	inline static int32_t get_offset_of_statements_0() { return static_cast<int32_t>(offsetof(CodeBlock_t2718634139, ___statements_0)); }
	inline ArrayList_t2718874744 * get_statements_0() const { return ___statements_0; }
	inline ArrayList_t2718874744 ** get_address_of_statements_0() { return &___statements_0; }
	inline void set_statements_0(ArrayList_t2718874744 * value)
	{
		___statements_0 = value;
		Il2CppCodeGenWriteBarrier(&___statements_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
