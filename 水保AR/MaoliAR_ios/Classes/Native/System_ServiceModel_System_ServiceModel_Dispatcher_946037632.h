﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.DataContractSerializerServiceBehavior
struct  DataContractSerializerServiceBehavior_t946037632  : public Il2CppObject
{
public:
	// System.Boolean System.ServiceModel.Dispatcher.DataContractSerializerServiceBehavior::<IgnoreExtensionDataObject>k__BackingField
	bool ___U3CIgnoreExtensionDataObjectU3Ek__BackingField_0;
	// System.Int32 System.ServiceModel.Dispatcher.DataContractSerializerServiceBehavior::<MaxItemsInObjectGraph>k__BackingField
	int32_t ___U3CMaxItemsInObjectGraphU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CIgnoreExtensionDataObjectU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DataContractSerializerServiceBehavior_t946037632, ___U3CIgnoreExtensionDataObjectU3Ek__BackingField_0)); }
	inline bool get_U3CIgnoreExtensionDataObjectU3Ek__BackingField_0() const { return ___U3CIgnoreExtensionDataObjectU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CIgnoreExtensionDataObjectU3Ek__BackingField_0() { return &___U3CIgnoreExtensionDataObjectU3Ek__BackingField_0; }
	inline void set_U3CIgnoreExtensionDataObjectU3Ek__BackingField_0(bool value)
	{
		___U3CIgnoreExtensionDataObjectU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CMaxItemsInObjectGraphU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DataContractSerializerServiceBehavior_t946037632, ___U3CMaxItemsInObjectGraphU3Ek__BackingField_1)); }
	inline int32_t get_U3CMaxItemsInObjectGraphU3Ek__BackingField_1() const { return ___U3CMaxItemsInObjectGraphU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CMaxItemsInObjectGraphU3Ek__BackingField_1() { return &___U3CMaxItemsInObjectGraphU3Ek__BackingField_1; }
	inline void set_U3CMaxItemsInObjectGraphU3Ek__BackingField_1(int32_t value)
	{
		___U3CMaxItemsInObjectGraphU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
