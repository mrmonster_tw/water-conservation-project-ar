﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_MessageCont680184223.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.MessageBodyMemberAttribute
struct  MessageBodyMemberAttribute_t2473986057  : public MessageContractMemberAttribute_t680184223
{
public:
	// System.Int32 System.ServiceModel.MessageBodyMemberAttribute::order
	int32_t ___order_2;

public:
	inline static int32_t get_offset_of_order_2() { return static_cast<int32_t>(offsetof(MessageBodyMemberAttribute_t2473986057, ___order_2)); }
	inline int32_t get_order_2() const { return ___order_2; }
	inline int32_t* get_address_of_order_2() { return &___order_2; }
	inline void set_order_2(int32_t value)
	{
		___order_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
