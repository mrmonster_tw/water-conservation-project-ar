﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_MessageSecu359378485.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.NonDualMessageSecurityOverHttp
struct  NonDualMessageSecurityOverHttp_t883223478  : public MessageSecurityOverHttp_t359378485
{
public:
	// System.Boolean System.ServiceModel.NonDualMessageSecurityOverHttp::establish_sec_ctx
	bool ___establish_sec_ctx_2;

public:
	inline static int32_t get_offset_of_establish_sec_ctx_2() { return static_cast<int32_t>(offsetof(NonDualMessageSecurityOverHttp_t883223478, ___establish_sec_ctx_2)); }
	inline bool get_establish_sec_ctx_2() const { return ___establish_sec_ctx_2; }
	inline bool* get_address_of_establish_sec_ctx_2() { return &___establish_sec_ctx_2; }
	inline void set_establish_sec_ctx_2(bool value)
	{
		___establish_sec_ctx_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
