﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_T4020609293.h"

// System.ServiceModel.Channels.NamedPipeTransportBindingElement
struct NamedPipeTransportBindingElement_t654821915;
// System.ServiceModel.Channels.MessageEncoder
struct MessageEncoder_t3063398011;
// System.Xml.XmlDictionaryReaderQuotas
struct XmlDictionaryReaderQuotas_t173030297;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.NamedPipeChannelFactory`1<System.Object>
struct  NamedPipeChannelFactory_1_t4232042529  : public TransportChannelFactoryBase_1_t4020609293
{
public:
	// System.ServiceModel.Channels.NamedPipeTransportBindingElement System.ServiceModel.Channels.NamedPipeChannelFactory`1::source
	NamedPipeTransportBindingElement_t654821915 * ___source_15;
	// System.ServiceModel.Channels.MessageEncoder System.ServiceModel.Channels.NamedPipeChannelFactory`1::encoder
	MessageEncoder_t3063398011 * ___encoder_16;
	// System.Xml.XmlDictionaryReaderQuotas System.ServiceModel.Channels.NamedPipeChannelFactory`1::quotas
	XmlDictionaryReaderQuotas_t173030297 * ___quotas_17;

public:
	inline static int32_t get_offset_of_source_15() { return static_cast<int32_t>(offsetof(NamedPipeChannelFactory_1_t4232042529, ___source_15)); }
	inline NamedPipeTransportBindingElement_t654821915 * get_source_15() const { return ___source_15; }
	inline NamedPipeTransportBindingElement_t654821915 ** get_address_of_source_15() { return &___source_15; }
	inline void set_source_15(NamedPipeTransportBindingElement_t654821915 * value)
	{
		___source_15 = value;
		Il2CppCodeGenWriteBarrier(&___source_15, value);
	}

	inline static int32_t get_offset_of_encoder_16() { return static_cast<int32_t>(offsetof(NamedPipeChannelFactory_1_t4232042529, ___encoder_16)); }
	inline MessageEncoder_t3063398011 * get_encoder_16() const { return ___encoder_16; }
	inline MessageEncoder_t3063398011 ** get_address_of_encoder_16() { return &___encoder_16; }
	inline void set_encoder_16(MessageEncoder_t3063398011 * value)
	{
		___encoder_16 = value;
		Il2CppCodeGenWriteBarrier(&___encoder_16, value);
	}

	inline static int32_t get_offset_of_quotas_17() { return static_cast<int32_t>(offsetof(NamedPipeChannelFactory_1_t4232042529, ___quotas_17)); }
	inline XmlDictionaryReaderQuotas_t173030297 * get_quotas_17() const { return ___quotas_17; }
	inline XmlDictionaryReaderQuotas_t173030297 ** get_address_of_quotas_17() { return &___quotas_17; }
	inline void set_quotas_17(XmlDictionaryReaderQuotas_t173030297 * value)
	{
		___quotas_17 = value;
		Il2CppCodeGenWriteBarrier(&___quotas_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
