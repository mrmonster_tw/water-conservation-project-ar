﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Char[]
struct CharU5BU5D_t3528271667;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Util.UrlUtils
struct  UrlUtils_t1214298665  : public Il2CppObject
{
public:

public:
};

struct UrlUtils_t1214298665_StaticFields
{
public:
	// System.Char[] System.Web.Util.UrlUtils::path_sep
	CharU5BU5D_t3528271667* ___path_sep_0;

public:
	inline static int32_t get_offset_of_path_sep_0() { return static_cast<int32_t>(offsetof(UrlUtils_t1214298665_StaticFields, ___path_sep_0)); }
	inline CharU5BU5D_t3528271667* get_path_sep_0() const { return ___path_sep_0; }
	inline CharU5BU5D_t3528271667** get_address_of_path_sep_0() { return &___path_sep_0; }
	inline void set_path_sep_0(CharU5BU5D_t3528271667* value)
	{
		___path_sep_0 = value;
		Il2CppCodeGenWriteBarrier(&___path_sep_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
