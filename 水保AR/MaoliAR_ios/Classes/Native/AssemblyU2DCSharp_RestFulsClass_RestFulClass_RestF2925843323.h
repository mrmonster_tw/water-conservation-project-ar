﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestFulsClass.RestFulClass/RestFul/Datas/<waitGetdataSend>c__Iterator1
struct  U3CwaitGetdataSendU3Ec__Iterator1_t2925843323  : public Il2CppObject
{
public:
	// UnityEngine.Networking.UnityWebRequest RestFulsClass.RestFulClass/RestFul/Datas/<waitGetdataSend>c__Iterator1::<www>__1
	UnityWebRequest_t463507806 * ___U3CwwwU3E__1_0;
	// System.String RestFulsClass.RestFulClass/RestFul/Datas/<waitGetdataSend>c__Iterator1::requstUrl
	String_t* ___requstUrl_1;
	// System.Object RestFulsClass.RestFulClass/RestFul/Datas/<waitGetdataSend>c__Iterator1::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean RestFulsClass.RestFulClass/RestFul/Datas/<waitGetdataSend>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 RestFulsClass.RestFulClass/RestFul/Datas/<waitGetdataSend>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CwwwU3E__1_0() { return static_cast<int32_t>(offsetof(U3CwaitGetdataSendU3Ec__Iterator1_t2925843323, ___U3CwwwU3E__1_0)); }
	inline UnityWebRequest_t463507806 * get_U3CwwwU3E__1_0() const { return ___U3CwwwU3E__1_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CwwwU3E__1_0() { return &___U3CwwwU3E__1_0; }
	inline void set_U3CwwwU3E__1_0(UnityWebRequest_t463507806 * value)
	{
		___U3CwwwU3E__1_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__1_0, value);
	}

	inline static int32_t get_offset_of_requstUrl_1() { return static_cast<int32_t>(offsetof(U3CwaitGetdataSendU3Ec__Iterator1_t2925843323, ___requstUrl_1)); }
	inline String_t* get_requstUrl_1() const { return ___requstUrl_1; }
	inline String_t** get_address_of_requstUrl_1() { return &___requstUrl_1; }
	inline void set_requstUrl_1(String_t* value)
	{
		___requstUrl_1 = value;
		Il2CppCodeGenWriteBarrier(&___requstUrl_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CwaitGetdataSendU3Ec__Iterator1_t2925843323, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CwaitGetdataSendU3Ec__Iterator1_t2925843323, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CwaitGetdataSendU3Ec__Iterator1_t2925843323, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
