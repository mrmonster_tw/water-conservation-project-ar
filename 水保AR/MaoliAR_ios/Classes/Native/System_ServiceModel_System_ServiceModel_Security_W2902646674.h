﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_Security_Principal_TokenImpersonat3773270939.h"

// System.Net.NetworkCredential
struct NetworkCredential_t3282608323;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.WindowsClientCredential
struct  WindowsClientCredential_t2902646674  : public Il2CppObject
{
public:
	// System.Boolean System.ServiceModel.Security.WindowsClientCredential::allow_ntlm
	bool ___allow_ntlm_0;
	// System.Security.Principal.TokenImpersonationLevel System.ServiceModel.Security.WindowsClientCredential::impersonation_level
	int32_t ___impersonation_level_1;
	// System.Net.NetworkCredential System.ServiceModel.Security.WindowsClientCredential::client_credential
	NetworkCredential_t3282608323 * ___client_credential_2;

public:
	inline static int32_t get_offset_of_allow_ntlm_0() { return static_cast<int32_t>(offsetof(WindowsClientCredential_t2902646674, ___allow_ntlm_0)); }
	inline bool get_allow_ntlm_0() const { return ___allow_ntlm_0; }
	inline bool* get_address_of_allow_ntlm_0() { return &___allow_ntlm_0; }
	inline void set_allow_ntlm_0(bool value)
	{
		___allow_ntlm_0 = value;
	}

	inline static int32_t get_offset_of_impersonation_level_1() { return static_cast<int32_t>(offsetof(WindowsClientCredential_t2902646674, ___impersonation_level_1)); }
	inline int32_t get_impersonation_level_1() const { return ___impersonation_level_1; }
	inline int32_t* get_address_of_impersonation_level_1() { return &___impersonation_level_1; }
	inline void set_impersonation_level_1(int32_t value)
	{
		___impersonation_level_1 = value;
	}

	inline static int32_t get_offset_of_client_credential_2() { return static_cast<int32_t>(offsetof(WindowsClientCredential_t2902646674, ___client_credential_2)); }
	inline NetworkCredential_t3282608323 * get_client_credential_2() const { return ___client_credential_2; }
	inline NetworkCredential_t3282608323 ** get_address_of_client_credential_2() { return &___client_credential_2; }
	inline void set_client_credential_2(NetworkCredential_t3282608323 * value)
	{
		___client_credential_2 = value;
		Il2CppCodeGenWriteBarrier(&___client_credential_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
