﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_Mono_CodeGeneration_CodeValueRe975266093.h"

// System.Type
struct Type_t;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.CodeGeneration.CodeArgumentReference
struct  CodeArgumentReference_t1456663465  : public CodeValueReference_t975266093
{
public:
	// System.Type Mono.CodeGeneration.CodeArgumentReference::type
	Type_t * ___type_0;
	// System.Int32 Mono.CodeGeneration.CodeArgumentReference::argNum
	int32_t ___argNum_1;
	// System.String Mono.CodeGeneration.CodeArgumentReference::name
	String_t* ___name_2;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(CodeArgumentReference_t1456663465, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier(&___type_0, value);
	}

	inline static int32_t get_offset_of_argNum_1() { return static_cast<int32_t>(offsetof(CodeArgumentReference_t1456663465, ___argNum_1)); }
	inline int32_t get_argNum_1() const { return ___argNum_1; }
	inline int32_t* get_address_of_argNum_1() { return &___argNum_1; }
	inline void set_argNum_1(int32_t value)
	{
		___argNum_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(CodeArgumentReference_t1456663465, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
