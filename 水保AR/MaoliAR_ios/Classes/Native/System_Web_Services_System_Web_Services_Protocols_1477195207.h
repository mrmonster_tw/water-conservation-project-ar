﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_ComponentModel_AsyncCompletedEventAr1863481821.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Protocols.InvokeCompletedEventArgs
struct  InvokeCompletedEventArgs_t1477195207  : public AsyncCompletedEventArgs_t1863481821
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
