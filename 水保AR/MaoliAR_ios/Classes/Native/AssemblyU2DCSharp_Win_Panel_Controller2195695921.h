﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.Text
struct Text_t1901882714;
// System.Predicate`1<UnityEngine.UI.Button>
struct Predicate_1_t585359297;
// System.Predicate`1<UnityEngine.UI.Text>
struct Predicate_1_t2727176838;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Win_Panel_Controller
struct  Win_Panel_Controller_t2195695921  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 Win_Panel_Controller::Index
	int32_t ___Index_2;
	// UnityEngine.GameObject[] Win_Panel_Controller::CharacterImage
	GameObjectU5BU5D_t3328599146* ___CharacterImage_3;
	// UnityEngine.GameObject[] Win_Panel_Controller::star_Images
	GameObjectU5BU5D_t3328599146* ___star_Images_4;
	// UnityEngine.GameObject[] Win_Panel_Controller::thanks
	GameObjectU5BU5D_t3328599146* ___thanks_5;
	// UnityEngine.GameObject Win_Panel_Controller::Parent_obj
	GameObject_t1113636619 * ___Parent_obj_6;
	// UnityEngine.GameObject Win_Panel_Controller::Shinning
	GameObject_t1113636619 * ___Shinning_7;
	// System.Single Win_Panel_Controller::Shinning_speed
	float ___Shinning_speed_8;
	// UnityEngine.Transform Win_Panel_Controller::didntfind
	Transform_t3600365921 * ___didntfind_9;
	// UnityEngine.UI.Button Win_Panel_Controller::bt_schedule
	Button_t4055032469 * ___bt_schedule_10;
	// UnityEngine.UI.Text Win_Panel_Controller::請點擊任意處以繼續遊戲
	Text_t1901882714 * ___U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_11;

public:
	inline static int32_t get_offset_of_Index_2() { return static_cast<int32_t>(offsetof(Win_Panel_Controller_t2195695921, ___Index_2)); }
	inline int32_t get_Index_2() const { return ___Index_2; }
	inline int32_t* get_address_of_Index_2() { return &___Index_2; }
	inline void set_Index_2(int32_t value)
	{
		___Index_2 = value;
	}

	inline static int32_t get_offset_of_CharacterImage_3() { return static_cast<int32_t>(offsetof(Win_Panel_Controller_t2195695921, ___CharacterImage_3)); }
	inline GameObjectU5BU5D_t3328599146* get_CharacterImage_3() const { return ___CharacterImage_3; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_CharacterImage_3() { return &___CharacterImage_3; }
	inline void set_CharacterImage_3(GameObjectU5BU5D_t3328599146* value)
	{
		___CharacterImage_3 = value;
		Il2CppCodeGenWriteBarrier(&___CharacterImage_3, value);
	}

	inline static int32_t get_offset_of_star_Images_4() { return static_cast<int32_t>(offsetof(Win_Panel_Controller_t2195695921, ___star_Images_4)); }
	inline GameObjectU5BU5D_t3328599146* get_star_Images_4() const { return ___star_Images_4; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_star_Images_4() { return &___star_Images_4; }
	inline void set_star_Images_4(GameObjectU5BU5D_t3328599146* value)
	{
		___star_Images_4 = value;
		Il2CppCodeGenWriteBarrier(&___star_Images_4, value);
	}

	inline static int32_t get_offset_of_thanks_5() { return static_cast<int32_t>(offsetof(Win_Panel_Controller_t2195695921, ___thanks_5)); }
	inline GameObjectU5BU5D_t3328599146* get_thanks_5() const { return ___thanks_5; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_thanks_5() { return &___thanks_5; }
	inline void set_thanks_5(GameObjectU5BU5D_t3328599146* value)
	{
		___thanks_5 = value;
		Il2CppCodeGenWriteBarrier(&___thanks_5, value);
	}

	inline static int32_t get_offset_of_Parent_obj_6() { return static_cast<int32_t>(offsetof(Win_Panel_Controller_t2195695921, ___Parent_obj_6)); }
	inline GameObject_t1113636619 * get_Parent_obj_6() const { return ___Parent_obj_6; }
	inline GameObject_t1113636619 ** get_address_of_Parent_obj_6() { return &___Parent_obj_6; }
	inline void set_Parent_obj_6(GameObject_t1113636619 * value)
	{
		___Parent_obj_6 = value;
		Il2CppCodeGenWriteBarrier(&___Parent_obj_6, value);
	}

	inline static int32_t get_offset_of_Shinning_7() { return static_cast<int32_t>(offsetof(Win_Panel_Controller_t2195695921, ___Shinning_7)); }
	inline GameObject_t1113636619 * get_Shinning_7() const { return ___Shinning_7; }
	inline GameObject_t1113636619 ** get_address_of_Shinning_7() { return &___Shinning_7; }
	inline void set_Shinning_7(GameObject_t1113636619 * value)
	{
		___Shinning_7 = value;
		Il2CppCodeGenWriteBarrier(&___Shinning_7, value);
	}

	inline static int32_t get_offset_of_Shinning_speed_8() { return static_cast<int32_t>(offsetof(Win_Panel_Controller_t2195695921, ___Shinning_speed_8)); }
	inline float get_Shinning_speed_8() const { return ___Shinning_speed_8; }
	inline float* get_address_of_Shinning_speed_8() { return &___Shinning_speed_8; }
	inline void set_Shinning_speed_8(float value)
	{
		___Shinning_speed_8 = value;
	}

	inline static int32_t get_offset_of_didntfind_9() { return static_cast<int32_t>(offsetof(Win_Panel_Controller_t2195695921, ___didntfind_9)); }
	inline Transform_t3600365921 * get_didntfind_9() const { return ___didntfind_9; }
	inline Transform_t3600365921 ** get_address_of_didntfind_9() { return &___didntfind_9; }
	inline void set_didntfind_9(Transform_t3600365921 * value)
	{
		___didntfind_9 = value;
		Il2CppCodeGenWriteBarrier(&___didntfind_9, value);
	}

	inline static int32_t get_offset_of_bt_schedule_10() { return static_cast<int32_t>(offsetof(Win_Panel_Controller_t2195695921, ___bt_schedule_10)); }
	inline Button_t4055032469 * get_bt_schedule_10() const { return ___bt_schedule_10; }
	inline Button_t4055032469 ** get_address_of_bt_schedule_10() { return &___bt_schedule_10; }
	inline void set_bt_schedule_10(Button_t4055032469 * value)
	{
		___bt_schedule_10 = value;
		Il2CppCodeGenWriteBarrier(&___bt_schedule_10, value);
	}

	inline static int32_t get_offset_of_U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_11() { return static_cast<int32_t>(offsetof(Win_Panel_Controller_t2195695921, ___U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_11)); }
	inline Text_t1901882714 * get_U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_11() const { return ___U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_11; }
	inline Text_t1901882714 ** get_address_of_U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_11() { return &___U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_11; }
	inline void set_U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_11(Text_t1901882714 * value)
	{
		___U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_11 = value;
		Il2CppCodeGenWriteBarrier(&___U8ACBU9EDEU64CAU4EFBU610FU8655U4EE5U7E7CU7E8CU904AU6232_11, value);
	}
};

struct Win_Panel_Controller_t2195695921_StaticFields
{
public:
	// System.Predicate`1<UnityEngine.UI.Button> Win_Panel_Controller::<>f__am$cache0
	Predicate_1_t585359297 * ___U3CU3Ef__amU24cache0_12;
	// System.Predicate`1<UnityEngine.UI.Text> Win_Panel_Controller::<>f__am$cache1
	Predicate_1_t2727176838 * ___U3CU3Ef__amU24cache1_13;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_12() { return static_cast<int32_t>(offsetof(Win_Panel_Controller_t2195695921_StaticFields, ___U3CU3Ef__amU24cache0_12)); }
	inline Predicate_1_t585359297 * get_U3CU3Ef__amU24cache0_12() const { return ___U3CU3Ef__amU24cache0_12; }
	inline Predicate_1_t585359297 ** get_address_of_U3CU3Ef__amU24cache0_12() { return &___U3CU3Ef__amU24cache0_12; }
	inline void set_U3CU3Ef__amU24cache0_12(Predicate_1_t585359297 * value)
	{
		___U3CU3Ef__amU24cache0_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_13() { return static_cast<int32_t>(offsetof(Win_Panel_Controller_t2195695921_StaticFields, ___U3CU3Ef__amU24cache1_13)); }
	inline Predicate_1_t2727176838 * get_U3CU3Ef__amU24cache1_13() const { return ___U3CU3Ef__amU24cache1_13; }
	inline Predicate_1_t2727176838 ** get_address_of_U3CU3Ef__amU24cache1_13() { return &___U3CU3Ef__amU24cache1_13; }
	inline void set_U3CU3Ef__amU24cache1_13(Predicate_1_t2727176838 * value)
	{
		___U3CU3Ef__amU24cache1_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
