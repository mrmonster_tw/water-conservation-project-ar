﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Web_System_Web_HttpCacheability811143856.h"
#include "mscorlib_System_DateTime3738529785.h"
#include "mscorlib_System_TimeSpan881159249.h"

// System.Web.HttpCacheVaryByContentEncodings
struct HttpCacheVaryByContentEncodings_t1137512671;
// System.Web.HttpCacheVaryByHeaders
struct HttpCacheVaryByHeaders_t2217784971;
// System.Web.HttpCacheVaryByParams
struct HttpCacheVaryByParams_t2818058713;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpCachePolicy
struct  HttpCachePolicy_t379126981  : public Il2CppObject
{
public:
	// System.Web.HttpCacheVaryByContentEncodings System.Web.HttpCachePolicy::vary_by_content_encodings
	HttpCacheVaryByContentEncodings_t1137512671 * ___vary_by_content_encodings_0;
	// System.Web.HttpCacheVaryByHeaders System.Web.HttpCachePolicy::vary_by_headers
	HttpCacheVaryByHeaders_t2217784971 * ___vary_by_headers_1;
	// System.Web.HttpCacheVaryByParams System.Web.HttpCachePolicy::vary_by_params
	HttpCacheVaryByParams_t2818058713 * ___vary_by_params_2;
	// System.Web.HttpCacheability System.Web.HttpCachePolicy::Cacheability
	int32_t ___Cacheability_3;
	// System.String System.Web.HttpCachePolicy::etag
	String_t* ___etag_4;
	// System.Boolean System.Web.HttpCachePolicy::etag_from_file_dependencies
	bool ___etag_from_file_dependencies_5;
	// System.Boolean System.Web.HttpCachePolicy::last_modified_from_file_dependencies
	bool ___last_modified_from_file_dependencies_6;
	// System.Boolean System.Web.HttpCachePolicy::have_expire_date
	bool ___have_expire_date_7;
	// System.DateTime System.Web.HttpCachePolicy::expire_date
	DateTime_t3738529785  ___expire_date_8;
	// System.Boolean System.Web.HttpCachePolicy::have_last_modified
	bool ___have_last_modified_9;
	// System.DateTime System.Web.HttpCachePolicy::last_modified
	DateTime_t3738529785  ___last_modified_10;
	// System.Boolean System.Web.HttpCachePolicy::HaveMaxAge
	bool ___HaveMaxAge_11;
	// System.TimeSpan System.Web.HttpCachePolicy::MaxAge
	TimeSpan_t881159249  ___MaxAge_12;
	// System.Boolean System.Web.HttpCachePolicy::allow_response_in_browser_history
	bool ___allow_response_in_browser_history_13;
	// System.Boolean System.Web.HttpCachePolicy::allow_server_caching
	bool ___allow_server_caching_14;
	// System.Boolean System.Web.HttpCachePolicy::set_no_store
	bool ___set_no_store_15;
	// System.Boolean System.Web.HttpCachePolicy::set_no_transform
	bool ___set_no_transform_16;
	// System.Boolean System.Web.HttpCachePolicy::valid_until_expires
	bool ___valid_until_expires_17;

public:
	inline static int32_t get_offset_of_vary_by_content_encodings_0() { return static_cast<int32_t>(offsetof(HttpCachePolicy_t379126981, ___vary_by_content_encodings_0)); }
	inline HttpCacheVaryByContentEncodings_t1137512671 * get_vary_by_content_encodings_0() const { return ___vary_by_content_encodings_0; }
	inline HttpCacheVaryByContentEncodings_t1137512671 ** get_address_of_vary_by_content_encodings_0() { return &___vary_by_content_encodings_0; }
	inline void set_vary_by_content_encodings_0(HttpCacheVaryByContentEncodings_t1137512671 * value)
	{
		___vary_by_content_encodings_0 = value;
		Il2CppCodeGenWriteBarrier(&___vary_by_content_encodings_0, value);
	}

	inline static int32_t get_offset_of_vary_by_headers_1() { return static_cast<int32_t>(offsetof(HttpCachePolicy_t379126981, ___vary_by_headers_1)); }
	inline HttpCacheVaryByHeaders_t2217784971 * get_vary_by_headers_1() const { return ___vary_by_headers_1; }
	inline HttpCacheVaryByHeaders_t2217784971 ** get_address_of_vary_by_headers_1() { return &___vary_by_headers_1; }
	inline void set_vary_by_headers_1(HttpCacheVaryByHeaders_t2217784971 * value)
	{
		___vary_by_headers_1 = value;
		Il2CppCodeGenWriteBarrier(&___vary_by_headers_1, value);
	}

	inline static int32_t get_offset_of_vary_by_params_2() { return static_cast<int32_t>(offsetof(HttpCachePolicy_t379126981, ___vary_by_params_2)); }
	inline HttpCacheVaryByParams_t2818058713 * get_vary_by_params_2() const { return ___vary_by_params_2; }
	inline HttpCacheVaryByParams_t2818058713 ** get_address_of_vary_by_params_2() { return &___vary_by_params_2; }
	inline void set_vary_by_params_2(HttpCacheVaryByParams_t2818058713 * value)
	{
		___vary_by_params_2 = value;
		Il2CppCodeGenWriteBarrier(&___vary_by_params_2, value);
	}

	inline static int32_t get_offset_of_Cacheability_3() { return static_cast<int32_t>(offsetof(HttpCachePolicy_t379126981, ___Cacheability_3)); }
	inline int32_t get_Cacheability_3() const { return ___Cacheability_3; }
	inline int32_t* get_address_of_Cacheability_3() { return &___Cacheability_3; }
	inline void set_Cacheability_3(int32_t value)
	{
		___Cacheability_3 = value;
	}

	inline static int32_t get_offset_of_etag_4() { return static_cast<int32_t>(offsetof(HttpCachePolicy_t379126981, ___etag_4)); }
	inline String_t* get_etag_4() const { return ___etag_4; }
	inline String_t** get_address_of_etag_4() { return &___etag_4; }
	inline void set_etag_4(String_t* value)
	{
		___etag_4 = value;
		Il2CppCodeGenWriteBarrier(&___etag_4, value);
	}

	inline static int32_t get_offset_of_etag_from_file_dependencies_5() { return static_cast<int32_t>(offsetof(HttpCachePolicy_t379126981, ___etag_from_file_dependencies_5)); }
	inline bool get_etag_from_file_dependencies_5() const { return ___etag_from_file_dependencies_5; }
	inline bool* get_address_of_etag_from_file_dependencies_5() { return &___etag_from_file_dependencies_5; }
	inline void set_etag_from_file_dependencies_5(bool value)
	{
		___etag_from_file_dependencies_5 = value;
	}

	inline static int32_t get_offset_of_last_modified_from_file_dependencies_6() { return static_cast<int32_t>(offsetof(HttpCachePolicy_t379126981, ___last_modified_from_file_dependencies_6)); }
	inline bool get_last_modified_from_file_dependencies_6() const { return ___last_modified_from_file_dependencies_6; }
	inline bool* get_address_of_last_modified_from_file_dependencies_6() { return &___last_modified_from_file_dependencies_6; }
	inline void set_last_modified_from_file_dependencies_6(bool value)
	{
		___last_modified_from_file_dependencies_6 = value;
	}

	inline static int32_t get_offset_of_have_expire_date_7() { return static_cast<int32_t>(offsetof(HttpCachePolicy_t379126981, ___have_expire_date_7)); }
	inline bool get_have_expire_date_7() const { return ___have_expire_date_7; }
	inline bool* get_address_of_have_expire_date_7() { return &___have_expire_date_7; }
	inline void set_have_expire_date_7(bool value)
	{
		___have_expire_date_7 = value;
	}

	inline static int32_t get_offset_of_expire_date_8() { return static_cast<int32_t>(offsetof(HttpCachePolicy_t379126981, ___expire_date_8)); }
	inline DateTime_t3738529785  get_expire_date_8() const { return ___expire_date_8; }
	inline DateTime_t3738529785 * get_address_of_expire_date_8() { return &___expire_date_8; }
	inline void set_expire_date_8(DateTime_t3738529785  value)
	{
		___expire_date_8 = value;
	}

	inline static int32_t get_offset_of_have_last_modified_9() { return static_cast<int32_t>(offsetof(HttpCachePolicy_t379126981, ___have_last_modified_9)); }
	inline bool get_have_last_modified_9() const { return ___have_last_modified_9; }
	inline bool* get_address_of_have_last_modified_9() { return &___have_last_modified_9; }
	inline void set_have_last_modified_9(bool value)
	{
		___have_last_modified_9 = value;
	}

	inline static int32_t get_offset_of_last_modified_10() { return static_cast<int32_t>(offsetof(HttpCachePolicy_t379126981, ___last_modified_10)); }
	inline DateTime_t3738529785  get_last_modified_10() const { return ___last_modified_10; }
	inline DateTime_t3738529785 * get_address_of_last_modified_10() { return &___last_modified_10; }
	inline void set_last_modified_10(DateTime_t3738529785  value)
	{
		___last_modified_10 = value;
	}

	inline static int32_t get_offset_of_HaveMaxAge_11() { return static_cast<int32_t>(offsetof(HttpCachePolicy_t379126981, ___HaveMaxAge_11)); }
	inline bool get_HaveMaxAge_11() const { return ___HaveMaxAge_11; }
	inline bool* get_address_of_HaveMaxAge_11() { return &___HaveMaxAge_11; }
	inline void set_HaveMaxAge_11(bool value)
	{
		___HaveMaxAge_11 = value;
	}

	inline static int32_t get_offset_of_MaxAge_12() { return static_cast<int32_t>(offsetof(HttpCachePolicy_t379126981, ___MaxAge_12)); }
	inline TimeSpan_t881159249  get_MaxAge_12() const { return ___MaxAge_12; }
	inline TimeSpan_t881159249 * get_address_of_MaxAge_12() { return &___MaxAge_12; }
	inline void set_MaxAge_12(TimeSpan_t881159249  value)
	{
		___MaxAge_12 = value;
	}

	inline static int32_t get_offset_of_allow_response_in_browser_history_13() { return static_cast<int32_t>(offsetof(HttpCachePolicy_t379126981, ___allow_response_in_browser_history_13)); }
	inline bool get_allow_response_in_browser_history_13() const { return ___allow_response_in_browser_history_13; }
	inline bool* get_address_of_allow_response_in_browser_history_13() { return &___allow_response_in_browser_history_13; }
	inline void set_allow_response_in_browser_history_13(bool value)
	{
		___allow_response_in_browser_history_13 = value;
	}

	inline static int32_t get_offset_of_allow_server_caching_14() { return static_cast<int32_t>(offsetof(HttpCachePolicy_t379126981, ___allow_server_caching_14)); }
	inline bool get_allow_server_caching_14() const { return ___allow_server_caching_14; }
	inline bool* get_address_of_allow_server_caching_14() { return &___allow_server_caching_14; }
	inline void set_allow_server_caching_14(bool value)
	{
		___allow_server_caching_14 = value;
	}

	inline static int32_t get_offset_of_set_no_store_15() { return static_cast<int32_t>(offsetof(HttpCachePolicy_t379126981, ___set_no_store_15)); }
	inline bool get_set_no_store_15() const { return ___set_no_store_15; }
	inline bool* get_address_of_set_no_store_15() { return &___set_no_store_15; }
	inline void set_set_no_store_15(bool value)
	{
		___set_no_store_15 = value;
	}

	inline static int32_t get_offset_of_set_no_transform_16() { return static_cast<int32_t>(offsetof(HttpCachePolicy_t379126981, ___set_no_transform_16)); }
	inline bool get_set_no_transform_16() const { return ___set_no_transform_16; }
	inline bool* get_address_of_set_no_transform_16() { return &___set_no_transform_16; }
	inline void set_set_no_transform_16(bool value)
	{
		___set_no_transform_16 = value;
	}

	inline static int32_t get_offset_of_valid_until_expires_17() { return static_cast<int32_t>(offsetof(HttpCachePolicy_t379126981, ___valid_until_expires_17)); }
	inline bool get_valid_until_expires_17() const { return ___valid_until_expires_17; }
	inline bool* get_address_of_valid_until_expires_17() { return &___valid_until_expires_17; }
	inline void set_valid_until_expires_17(bool value)
	{
		___valid_until_expires_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
