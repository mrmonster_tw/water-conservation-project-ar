﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_ResourceAttributes3997964906.h"
#include "mscorlib_System_Reflection_ResourceLocation1891396988.h"
#include "mscorlib_System_Reflection_StrongNameKeyPair3411219591.h"
#include "mscorlib_System_Reflection_TargetException3386045725.h"
#include "mscorlib_System_Reflection_TargetInvocationExcepti4266643902.h"
#include "mscorlib_System_Reflection_TargetParameterCountExc1216617239.h"
#include "mscorlib_System_Reflection_TypeAttributes113483779.h"
#include "mscorlib_System_Reflection_TypeDelegator3617470028.h"
#include "mscorlib_System_Reflection_Emit_RefEmitPermissionSe484390987.h"
#include "mscorlib_System_Reflection_Emit_MonoResource4103430009.h"
#include "mscorlib_System_Reflection_Emit_AssemblyBuilder359885250.h"
#include "mscorlib_System_Reflection_Emit_AssemblyBuilderAcc2806254258.h"
#include "mscorlib_System_Reflection_Emit_ConstructorBuilder2813524108.h"
#include "mscorlib_System_Reflection_Emit_DerivedType4286302013.h"
#include "mscorlib_System_Reflection_Emit_ArrayType700911890.h"
#include "mscorlib_System_Reflection_Emit_ByRefType2066805327.h"
#include "mscorlib_System_Reflection_Emit_DynamicMethod2537779570.h"
#include "mscorlib_System_Reflection_Emit_DynamicMethod_Anon3274688547.h"
#include "mscorlib_System_Reflection_Emit_DynamicMethodToken1364612456.h"
#include "mscorlib_System_Reflection_Emit_EnumBuilder2400448213.h"
#include "mscorlib_System_Reflection_Emit_FieldBuilder2627049993.h"
#include "mscorlib_System_Reflection_Emit_GenericTypeParamet1988827940.h"
#include "mscorlib_System_Reflection_Emit_ILTokenInfo2325775114.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator1388622344.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFi858502054.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelDa360167391.h"
#include "mscorlib_System_Reflection_Emit_Label2281661643.h"
#include "mscorlib_System_Reflection_Emit_LocalBuilder3562264111.h"
#include "mscorlib_System_Reflection_Emit_MethodBuilder2807316753.h"
#include "mscorlib_System_Reflection_Emit_MethodToken4055728386.h"
#include "mscorlib_System_Reflection_Emit_ModuleBuilder731887691.h"
#include "mscorlib_System_Reflection_Emit_ModuleBuilderTokenG944435078.h"
#include "mscorlib_System_Reflection_Emit_OpCodeNames3363784580.h"
#include "mscorlib_System_Reflection_Emit_OpCode123070264.h"
#include "mscorlib_System_Reflection_Emit_OpCodes126150456.h"
#include "mscorlib_System_Reflection_Emit_OperandType2622847887.h"
#include "mscorlib_System_Reflection_Emit_PackingSize2976435189.h"
#include "mscorlib_System_Reflection_Emit_ParameterBuilder1137139675.h"
#include "mscorlib_System_Reflection_Emit_PEFileKinds3631470751.h"
#include "mscorlib_System_Reflection_Emit_PropertyBuilder314297007.h"
#include "mscorlib_System_Reflection_Emit_StackBehaviour3009528134.h"
#include "mscorlib_System_Reflection_Emit_TypeBuilder1073948154.h"
#include "mscorlib_System_Reflection_Emit_UnmanagedMarshal984015687.h"
#include "mscorlib_System_Resources_MissingManifestResourceExc85971840.h"
#include "mscorlib_System_Resources_NeutralResourcesLanguage2027633532.h"
#include "mscorlib_System_Resources_ResourceManager4037989559.h"
#include "mscorlib_System_Resources_PredefinedResourceType4229135164.h"
#include "mscorlib_System_Resources_ResourceReader3300492639.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceI2872965302.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceCac51292791.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceE2106512554.h"
#include "mscorlib_System_Resources_ResourceSet2827911187.h"
#include "mscorlib_System_Resources_ResourceWriter1892498018.h"
#include "mscorlib_System_Resources_ResourceWriter_TypeByName584266494.h"
#include "mscorlib_System_Resources_RuntimeResourceSet3081501575.h"
#include "mscorlib_System_Resources_SatelliteContractVersion3549813284.h"
#include "mscorlib_System_Runtime_CompilerServices_Compilati3292409279.h"
#include "mscorlib_System_Runtime_CompilerServices_Compilati1721442111.h"
#include "mscorlib_System_Runtime_CompilerServices_DefaultDep548613883.h"
#include "mscorlib_System_Runtime_CompilerServices_Dependenc1606102833.h"
#include "mscorlib_System_Runtime_CompilerServices_IsVolatil2097058663.h"
#include "mscorlib_System_Runtime_CompilerServices_LoadHint3743314490.h"
#include "mscorlib_System_Runtime_CompilerServices_StringFre3031362240.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_Critic701527852.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_Cer573022029.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_Consi4071665526.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_Reliab502331440.h"
#include "mscorlib_System_Runtime_Hosting_ActivationArgument4219999170.h"
#include "mscorlib_System_Runtime_InteropServices_BestFitMap4073720592.h"
#include "mscorlib_System_Runtime_InteropServices_CallingCon1027624783.h"
#include "mscorlib_System_Runtime_InteropServices_CharSet3391187264.h"
#include "mscorlib_System_Runtime_InteropServices_ClassInter2274790349.h"
#include "mscorlib_System_Runtime_InteropServices_ClassInter4029497327.h"
#include "mscorlib_System_Runtime_InteropServices_ComCompati1233303509.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize500 = { sizeof (ResourceAttributes_t3997964906)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable500[3] = 
{
	ResourceAttributes_t3997964906::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize501 = { sizeof (ResourceLocation_t1891396988)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable501[4] = 
{
	ResourceLocation_t1891396988::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize502 = { sizeof (StrongNameKeyPair_t3411219591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable502[5] = 
{
	StrongNameKeyPair_t3411219591::get_offset_of__publicKey_0(),
	StrongNameKeyPair_t3411219591::get_offset_of__keyPairContainer_1(),
	StrongNameKeyPair_t3411219591::get_offset_of__keyPairExported_2(),
	StrongNameKeyPair_t3411219591::get_offset_of__keyPairArray_3(),
	StrongNameKeyPair_t3411219591::get_offset_of__rsa_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize503 = { sizeof (TargetException_t3386045725), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize504 = { sizeof (TargetInvocationException_t4266643902), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize505 = { sizeof (TargetParameterCountException_t1216617239), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize506 = { sizeof (TypeAttributes_t113483779)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable506[32] = 
{
	TypeAttributes_t113483779::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize507 = { sizeof (TypeDelegator_t3617470028), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable507[1] = 
{
	TypeDelegator_t3617470028::get_offset_of_typeImpl_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize508 = { sizeof (RefEmitPermissionSet_t484390987)+ sizeof (Il2CppObject), sizeof(RefEmitPermissionSet_t484390987_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable508[2] = 
{
	RefEmitPermissionSet_t484390987::get_offset_of_action_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RefEmitPermissionSet_t484390987::get_offset_of_pset_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize509 = { sizeof (MonoResource_t4103430009)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable509[6] = 
{
	MonoResource_t4103430009::get_offset_of_data_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoResource_t4103430009::get_offset_of_name_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoResource_t4103430009::get_offset_of_filename_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoResource_t4103430009::get_offset_of_attrs_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoResource_t4103430009::get_offset_of_offset_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoResource_t4103430009::get_offset_of_stream_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize510 = { sizeof (AssemblyBuilder_t359885250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable510[25] = 
{
	AssemblyBuilder_t359885250::get_offset_of_modules_10(),
	AssemblyBuilder_t359885250::get_offset_of_name_11(),
	AssemblyBuilder_t359885250::get_offset_of_dir_12(),
	AssemblyBuilder_t359885250::get_offset_of_resources_13(),
	AssemblyBuilder_t359885250::get_offset_of_version_14(),
	AssemblyBuilder_t359885250::get_offset_of_culture_15(),
	AssemblyBuilder_t359885250::get_offset_of_flags_16(),
	AssemblyBuilder_t359885250::get_offset_of_pekind_17(),
	AssemblyBuilder_t359885250::get_offset_of_access_18(),
	AssemblyBuilder_t359885250::get_offset_of_loaded_modules_19(),
	AssemblyBuilder_t359885250::get_offset_of_permissions_minimum_20(),
	AssemblyBuilder_t359885250::get_offset_of_permissions_optional_21(),
	AssemblyBuilder_t359885250::get_offset_of_permissions_refused_22(),
	AssemblyBuilder_t359885250::get_offset_of_corlib_internal_23(),
	AssemblyBuilder_t359885250::get_offset_of_pktoken_24(),
	AssemblyBuilder_t359885250::get_offset_of_corlib_object_type_25(),
	AssemblyBuilder_t359885250::get_offset_of_corlib_value_type_26(),
	AssemblyBuilder_t359885250::get_offset_of_corlib_enum_type_27(),
	AssemblyBuilder_t359885250::get_offset_of_corlib_void_type_28(),
	AssemblyBuilder_t359885250::get_offset_of_created_29(),
	AssemblyBuilder_t359885250::get_offset_of_is_module_only_30(),
	AssemblyBuilder_t359885250::get_offset_of_sn_31(),
	AssemblyBuilder_t359885250::get_offset_of_is_compiler_context_32(),
	AssemblyBuilder_t359885250::get_offset_of_versioninfo_culture_33(),
	AssemblyBuilder_t359885250::get_offset_of_manifest_module_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize511 = { sizeof (AssemblyBuilderAccess_t2806254258)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable511[5] = 
{
	AssemblyBuilderAccess_t2806254258::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize512 = { sizeof (ConstructorBuilder_t2813524108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable512[11] = 
{
	ConstructorBuilder_t2813524108::get_offset_of_ilgen_2(),
	ConstructorBuilder_t2813524108::get_offset_of_parameters_3(),
	ConstructorBuilder_t2813524108::get_offset_of_attrs_4(),
	ConstructorBuilder_t2813524108::get_offset_of_iattrs_5(),
	ConstructorBuilder_t2813524108::get_offset_of_table_idx_6(),
	ConstructorBuilder_t2813524108::get_offset_of_call_conv_7(),
	ConstructorBuilder_t2813524108::get_offset_of_type_8(),
	ConstructorBuilder_t2813524108::get_offset_of_pinfo_9(),
	ConstructorBuilder_t2813524108::get_offset_of_init_locals_10(),
	ConstructorBuilder_t2813524108::get_offset_of_paramModReq_11(),
	ConstructorBuilder_t2813524108::get_offset_of_paramModOpt_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize513 = { sizeof (DerivedType_t4286302013), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable513[1] = 
{
	DerivedType_t4286302013::get_offset_of_elementType_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize514 = { sizeof (ArrayType_t700911890), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable514[1] = 
{
	ArrayType_t700911890::get_offset_of_rank_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize515 = { sizeof (ByRefType_t2066805327), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize516 = { sizeof (DynamicMethod_t2537779570), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable516[17] = 
{
	DynamicMethod_t2537779570::get_offset_of_mhandle_0(),
	DynamicMethod_t2537779570::get_offset_of_name_1(),
	DynamicMethod_t2537779570::get_offset_of_returnType_2(),
	DynamicMethod_t2537779570::get_offset_of_parameters_3(),
	DynamicMethod_t2537779570::get_offset_of_attributes_4(),
	DynamicMethod_t2537779570::get_offset_of_callingConvention_5(),
	DynamicMethod_t2537779570::get_offset_of_module_6(),
	DynamicMethod_t2537779570::get_offset_of_skipVisibility_7(),
	DynamicMethod_t2537779570::get_offset_of_init_locals_8(),
	DynamicMethod_t2537779570::get_offset_of_ilgen_9(),
	DynamicMethod_t2537779570::get_offset_of_nrefs_10(),
	DynamicMethod_t2537779570::get_offset_of_refs_11(),
	DynamicMethod_t2537779570::get_offset_of_owner_12(),
	DynamicMethod_t2537779570::get_offset_of_deleg_13(),
	DynamicMethod_t2537779570::get_offset_of_method_14(),
	DynamicMethod_t2537779570::get_offset_of_pinfo_15(),
	DynamicMethod_t2537779570::get_offset_of_creating_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize517 = { sizeof (AnonHostModuleHolder_t3274688547), -1, sizeof(AnonHostModuleHolder_t3274688547_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable517[1] = 
{
	AnonHostModuleHolder_t3274688547_StaticFields::get_offset_of_anon_host_module_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize518 = { sizeof (DynamicMethodTokenGenerator_t1364612456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable518[1] = 
{
	DynamicMethodTokenGenerator_t1364612456::get_offset_of_m_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize519 = { sizeof (EnumBuilder_t2400448213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable519[2] = 
{
	EnumBuilder_t2400448213::get_offset_of__tb_8(),
	EnumBuilder_t2400448213::get_offset_of__underlyingType_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize520 = { sizeof (FieldBuilder_t2627049993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable520[5] = 
{
	FieldBuilder_t2627049993::get_offset_of_attrs_0(),
	FieldBuilder_t2627049993::get_offset_of_type_1(),
	FieldBuilder_t2627049993::get_offset_of_name_2(),
	FieldBuilder_t2627049993::get_offset_of_typeb_3(),
	FieldBuilder_t2627049993::get_offset_of_marshal_info_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize521 = { sizeof (GenericTypeParameterBuilder_t1988827940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable521[4] = 
{
	GenericTypeParameterBuilder_t1988827940::get_offset_of_tbuilder_8(),
	GenericTypeParameterBuilder_t1988827940::get_offset_of_mbuilder_9(),
	GenericTypeParameterBuilder_t1988827940::get_offset_of_name_10(),
	GenericTypeParameterBuilder_t1988827940::get_offset_of_base_type_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize522 = { sizeof (ILTokenInfo_t2325775114)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable522[2] = 
{
	ILTokenInfo_t2325775114::get_offset_of_member_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ILTokenInfo_t2325775114::get_offset_of_code_pos_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize523 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize524 = { sizeof (ILGenerator_t1388622344), -1, sizeof(ILGenerator_t1388622344_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable524[14] = 
{
	ILGenerator_t1388622344_StaticFields::get_offset_of_void_type_0(),
	ILGenerator_t1388622344::get_offset_of_code_1(),
	ILGenerator_t1388622344::get_offset_of_code_len_2(),
	ILGenerator_t1388622344::get_offset_of_max_stack_3(),
	ILGenerator_t1388622344::get_offset_of_cur_stack_4(),
	ILGenerator_t1388622344::get_offset_of_locals_5(),
	ILGenerator_t1388622344::get_offset_of_num_token_fixups_6(),
	ILGenerator_t1388622344::get_offset_of_token_fixups_7(),
	ILGenerator_t1388622344::get_offset_of_labels_8(),
	ILGenerator_t1388622344::get_offset_of_num_labels_9(),
	ILGenerator_t1388622344::get_offset_of_fixups_10(),
	ILGenerator_t1388622344::get_offset_of_num_fixups_11(),
	ILGenerator_t1388622344::get_offset_of_module_12(),
	ILGenerator_t1388622344::get_offset_of_token_gen_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize525 = { sizeof (LabelFixup_t858502054)+ sizeof (Il2CppObject), sizeof(LabelFixup_t858502054 ), 0, 0 };
extern const int32_t g_FieldOffsetTable525[3] = 
{
	LabelFixup_t858502054::get_offset_of_offset_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	LabelFixup_t858502054::get_offset_of_pos_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	LabelFixup_t858502054::get_offset_of_label_idx_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize526 = { sizeof (LabelData_t360167391)+ sizeof (Il2CppObject), sizeof(LabelData_t360167391 ), 0, 0 };
extern const int32_t g_FieldOffsetTable526[2] = 
{
	LabelData_t360167391::get_offset_of_addr_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	LabelData_t360167391::get_offset_of_maxStack_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize527 = { sizeof (Label_t2281661643)+ sizeof (Il2CppObject), sizeof(Label_t2281661643 ), 0, 0 };
extern const int32_t g_FieldOffsetTable527[1] = 
{
	Label_t2281661643::get_offset_of_label_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize528 = { sizeof (LocalBuilder_t3562264111), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable528[1] = 
{
	LocalBuilder_t3562264111::get_offset_of_ilgen_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize529 = { sizeof (MethodBuilder_t2807316753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable529[18] = 
{
	MethodBuilder_t2807316753::get_offset_of_rtype_0(),
	MethodBuilder_t2807316753::get_offset_of_parameters_1(),
	MethodBuilder_t2807316753::get_offset_of_attrs_2(),
	MethodBuilder_t2807316753::get_offset_of_iattrs_3(),
	MethodBuilder_t2807316753::get_offset_of_name_4(),
	MethodBuilder_t2807316753::get_offset_of_table_idx_5(),
	MethodBuilder_t2807316753::get_offset_of_code_6(),
	MethodBuilder_t2807316753::get_offset_of_ilgen_7(),
	MethodBuilder_t2807316753::get_offset_of_type_8(),
	MethodBuilder_t2807316753::get_offset_of_pinfo_9(),
	MethodBuilder_t2807316753::get_offset_of_override_method_10(),
	MethodBuilder_t2807316753::get_offset_of_call_conv_11(),
	MethodBuilder_t2807316753::get_offset_of_init_locals_12(),
	MethodBuilder_t2807316753::get_offset_of_generic_params_13(),
	MethodBuilder_t2807316753::get_offset_of_returnModReq_14(),
	MethodBuilder_t2807316753::get_offset_of_returnModOpt_15(),
	MethodBuilder_t2807316753::get_offset_of_paramModReq_16(),
	MethodBuilder_t2807316753::get_offset_of_paramModOpt_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize530 = { sizeof (MethodToken_t4055728386)+ sizeof (Il2CppObject), sizeof(MethodToken_t4055728386 ), sizeof(MethodToken_t4055728386_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable530[2] = 
{
	MethodToken_t4055728386::get_offset_of_tokValue_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MethodToken_t4055728386_StaticFields::get_offset_of_Empty_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize531 = { sizeof (ModuleBuilder_t731887691), -1, sizeof(ModuleBuilder_t731887691_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable531[13] = 
{
	ModuleBuilder_t731887691::get_offset_of_num_types_10(),
	ModuleBuilder_t731887691::get_offset_of_types_11(),
	ModuleBuilder_t731887691::get_offset_of_guid_12(),
	ModuleBuilder_t731887691::get_offset_of_table_idx_13(),
	ModuleBuilder_t731887691::get_offset_of_assemblyb_14(),
	ModuleBuilder_t731887691::get_offset_of_global_type_15(),
	ModuleBuilder_t731887691::get_offset_of_name_cache_16(),
	ModuleBuilder_t731887691::get_offset_of_us_string_cache_17(),
	ModuleBuilder_t731887691::get_offset_of_table_indexes_18(),
	ModuleBuilder_t731887691::get_offset_of_transient_19(),
	ModuleBuilder_t731887691::get_offset_of_token_gen_20(),
	ModuleBuilder_t731887691::get_offset_of_symbolWriter_21(),
	ModuleBuilder_t731887691_StaticFields::get_offset_of_type_modifiers_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize532 = { sizeof (ModuleBuilderTokenGenerator_t944435078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable532[1] = 
{
	ModuleBuilderTokenGenerator_t944435078::get_offset_of_mb_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize533 = { sizeof (OpCodeNames_t3363784580), -1, sizeof(OpCodeNames_t3363784580_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable533[1] = 
{
	OpCodeNames_t3363784580_StaticFields::get_offset_of_names_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize534 = { sizeof (OpCode_t123070264)+ sizeof (Il2CppObject), sizeof(OpCode_t123070264 ), 0, 0 };
extern const int32_t g_FieldOffsetTable534[8] = 
{
	OpCode_t123070264::get_offset_of_op1_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OpCode_t123070264::get_offset_of_op2_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OpCode_t123070264::get_offset_of_push_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OpCode_t123070264::get_offset_of_pop_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OpCode_t123070264::get_offset_of_size_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OpCode_t123070264::get_offset_of_type_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OpCode_t123070264::get_offset_of_args_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OpCode_t123070264::get_offset_of_flow_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize535 = { sizeof (OpCodes_t126150456), -1, sizeof(OpCodes_t126150456_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable535[226] = 
{
	OpCodes_t126150456_StaticFields::get_offset_of_Nop_0(),
	OpCodes_t126150456_StaticFields::get_offset_of_Break_1(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldarg_0_2(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldarg_1_3(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldarg_2_4(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldarg_3_5(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldloc_0_6(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldloc_1_7(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldloc_2_8(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldloc_3_9(),
	OpCodes_t126150456_StaticFields::get_offset_of_Stloc_0_10(),
	OpCodes_t126150456_StaticFields::get_offset_of_Stloc_1_11(),
	OpCodes_t126150456_StaticFields::get_offset_of_Stloc_2_12(),
	OpCodes_t126150456_StaticFields::get_offset_of_Stloc_3_13(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldarg_S_14(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldarga_S_15(),
	OpCodes_t126150456_StaticFields::get_offset_of_Starg_S_16(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldloc_S_17(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldloca_S_18(),
	OpCodes_t126150456_StaticFields::get_offset_of_Stloc_S_19(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldnull_20(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldc_I4_M1_21(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldc_I4_0_22(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldc_I4_1_23(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldc_I4_2_24(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldc_I4_3_25(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldc_I4_4_26(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldc_I4_5_27(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldc_I4_6_28(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldc_I4_7_29(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldc_I4_8_30(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldc_I4_S_31(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldc_I4_32(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldc_I8_33(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldc_R4_34(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldc_R8_35(),
	OpCodes_t126150456_StaticFields::get_offset_of_Dup_36(),
	OpCodes_t126150456_StaticFields::get_offset_of_Pop_37(),
	OpCodes_t126150456_StaticFields::get_offset_of_Jmp_38(),
	OpCodes_t126150456_StaticFields::get_offset_of_Call_39(),
	OpCodes_t126150456_StaticFields::get_offset_of_Calli_40(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ret_41(),
	OpCodes_t126150456_StaticFields::get_offset_of_Br_S_42(),
	OpCodes_t126150456_StaticFields::get_offset_of_Brfalse_S_43(),
	OpCodes_t126150456_StaticFields::get_offset_of_Brtrue_S_44(),
	OpCodes_t126150456_StaticFields::get_offset_of_Beq_S_45(),
	OpCodes_t126150456_StaticFields::get_offset_of_Bge_S_46(),
	OpCodes_t126150456_StaticFields::get_offset_of_Bgt_S_47(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ble_S_48(),
	OpCodes_t126150456_StaticFields::get_offset_of_Blt_S_49(),
	OpCodes_t126150456_StaticFields::get_offset_of_Bne_Un_S_50(),
	OpCodes_t126150456_StaticFields::get_offset_of_Bge_Un_S_51(),
	OpCodes_t126150456_StaticFields::get_offset_of_Bgt_Un_S_52(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ble_Un_S_53(),
	OpCodes_t126150456_StaticFields::get_offset_of_Blt_Un_S_54(),
	OpCodes_t126150456_StaticFields::get_offset_of_Br_55(),
	OpCodes_t126150456_StaticFields::get_offset_of_Brfalse_56(),
	OpCodes_t126150456_StaticFields::get_offset_of_Brtrue_57(),
	OpCodes_t126150456_StaticFields::get_offset_of_Beq_58(),
	OpCodes_t126150456_StaticFields::get_offset_of_Bge_59(),
	OpCodes_t126150456_StaticFields::get_offset_of_Bgt_60(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ble_61(),
	OpCodes_t126150456_StaticFields::get_offset_of_Blt_62(),
	OpCodes_t126150456_StaticFields::get_offset_of_Bne_Un_63(),
	OpCodes_t126150456_StaticFields::get_offset_of_Bge_Un_64(),
	OpCodes_t126150456_StaticFields::get_offset_of_Bgt_Un_65(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ble_Un_66(),
	OpCodes_t126150456_StaticFields::get_offset_of_Blt_Un_67(),
	OpCodes_t126150456_StaticFields::get_offset_of_Switch_68(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldind_I1_69(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldind_U1_70(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldind_I2_71(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldind_U2_72(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldind_I4_73(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldind_U4_74(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldind_I8_75(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldind_I_76(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldind_R4_77(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldind_R8_78(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldind_Ref_79(),
	OpCodes_t126150456_StaticFields::get_offset_of_Stind_Ref_80(),
	OpCodes_t126150456_StaticFields::get_offset_of_Stind_I1_81(),
	OpCodes_t126150456_StaticFields::get_offset_of_Stind_I2_82(),
	OpCodes_t126150456_StaticFields::get_offset_of_Stind_I4_83(),
	OpCodes_t126150456_StaticFields::get_offset_of_Stind_I8_84(),
	OpCodes_t126150456_StaticFields::get_offset_of_Stind_R4_85(),
	OpCodes_t126150456_StaticFields::get_offset_of_Stind_R8_86(),
	OpCodes_t126150456_StaticFields::get_offset_of_Add_87(),
	OpCodes_t126150456_StaticFields::get_offset_of_Sub_88(),
	OpCodes_t126150456_StaticFields::get_offset_of_Mul_89(),
	OpCodes_t126150456_StaticFields::get_offset_of_Div_90(),
	OpCodes_t126150456_StaticFields::get_offset_of_Div_Un_91(),
	OpCodes_t126150456_StaticFields::get_offset_of_Rem_92(),
	OpCodes_t126150456_StaticFields::get_offset_of_Rem_Un_93(),
	OpCodes_t126150456_StaticFields::get_offset_of_And_94(),
	OpCodes_t126150456_StaticFields::get_offset_of_Or_95(),
	OpCodes_t126150456_StaticFields::get_offset_of_Xor_96(),
	OpCodes_t126150456_StaticFields::get_offset_of_Shl_97(),
	OpCodes_t126150456_StaticFields::get_offset_of_Shr_98(),
	OpCodes_t126150456_StaticFields::get_offset_of_Shr_Un_99(),
	OpCodes_t126150456_StaticFields::get_offset_of_Neg_100(),
	OpCodes_t126150456_StaticFields::get_offset_of_Not_101(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_I1_102(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_I2_103(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_I4_104(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_I8_105(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_R4_106(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_R8_107(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_U4_108(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_U8_109(),
	OpCodes_t126150456_StaticFields::get_offset_of_Callvirt_110(),
	OpCodes_t126150456_StaticFields::get_offset_of_Cpobj_111(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldobj_112(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldstr_113(),
	OpCodes_t126150456_StaticFields::get_offset_of_Newobj_114(),
	OpCodes_t126150456_StaticFields::get_offset_of_Castclass_115(),
	OpCodes_t126150456_StaticFields::get_offset_of_Isinst_116(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_R_Un_117(),
	OpCodes_t126150456_StaticFields::get_offset_of_Unbox_118(),
	OpCodes_t126150456_StaticFields::get_offset_of_Throw_119(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldfld_120(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldflda_121(),
	OpCodes_t126150456_StaticFields::get_offset_of_Stfld_122(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldsfld_123(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldsflda_124(),
	OpCodes_t126150456_StaticFields::get_offset_of_Stsfld_125(),
	OpCodes_t126150456_StaticFields::get_offset_of_Stobj_126(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_Ovf_I1_Un_127(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_Ovf_I2_Un_128(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_Ovf_I4_Un_129(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_Ovf_I8_Un_130(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_Ovf_U1_Un_131(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_Ovf_U2_Un_132(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_Ovf_U4_Un_133(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_Ovf_U8_Un_134(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_Ovf_I_Un_135(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_Ovf_U_Un_136(),
	OpCodes_t126150456_StaticFields::get_offset_of_Box_137(),
	OpCodes_t126150456_StaticFields::get_offset_of_Newarr_138(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldlen_139(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldelema_140(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldelem_I1_141(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldelem_U1_142(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldelem_I2_143(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldelem_U2_144(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldelem_I4_145(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldelem_U4_146(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldelem_I8_147(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldelem_I_148(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldelem_R4_149(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldelem_R8_150(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldelem_Ref_151(),
	OpCodes_t126150456_StaticFields::get_offset_of_Stelem_I_152(),
	OpCodes_t126150456_StaticFields::get_offset_of_Stelem_I1_153(),
	OpCodes_t126150456_StaticFields::get_offset_of_Stelem_I2_154(),
	OpCodes_t126150456_StaticFields::get_offset_of_Stelem_I4_155(),
	OpCodes_t126150456_StaticFields::get_offset_of_Stelem_I8_156(),
	OpCodes_t126150456_StaticFields::get_offset_of_Stelem_R4_157(),
	OpCodes_t126150456_StaticFields::get_offset_of_Stelem_R8_158(),
	OpCodes_t126150456_StaticFields::get_offset_of_Stelem_Ref_159(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldelem_160(),
	OpCodes_t126150456_StaticFields::get_offset_of_Stelem_161(),
	OpCodes_t126150456_StaticFields::get_offset_of_Unbox_Any_162(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_Ovf_I1_163(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_Ovf_U1_164(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_Ovf_I2_165(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_Ovf_U2_166(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_Ovf_I4_167(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_Ovf_U4_168(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_Ovf_I8_169(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_Ovf_U8_170(),
	OpCodes_t126150456_StaticFields::get_offset_of_Refanyval_171(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ckfinite_172(),
	OpCodes_t126150456_StaticFields::get_offset_of_Mkrefany_173(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldtoken_174(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_U2_175(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_U1_176(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_I_177(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_Ovf_I_178(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_Ovf_U_179(),
	OpCodes_t126150456_StaticFields::get_offset_of_Add_Ovf_180(),
	OpCodes_t126150456_StaticFields::get_offset_of_Add_Ovf_Un_181(),
	OpCodes_t126150456_StaticFields::get_offset_of_Mul_Ovf_182(),
	OpCodes_t126150456_StaticFields::get_offset_of_Mul_Ovf_Un_183(),
	OpCodes_t126150456_StaticFields::get_offset_of_Sub_Ovf_184(),
	OpCodes_t126150456_StaticFields::get_offset_of_Sub_Ovf_Un_185(),
	OpCodes_t126150456_StaticFields::get_offset_of_Endfinally_186(),
	OpCodes_t126150456_StaticFields::get_offset_of_Leave_187(),
	OpCodes_t126150456_StaticFields::get_offset_of_Leave_S_188(),
	OpCodes_t126150456_StaticFields::get_offset_of_Stind_I_189(),
	OpCodes_t126150456_StaticFields::get_offset_of_Conv_U_190(),
	OpCodes_t126150456_StaticFields::get_offset_of_Prefix7_191(),
	OpCodes_t126150456_StaticFields::get_offset_of_Prefix6_192(),
	OpCodes_t126150456_StaticFields::get_offset_of_Prefix5_193(),
	OpCodes_t126150456_StaticFields::get_offset_of_Prefix4_194(),
	OpCodes_t126150456_StaticFields::get_offset_of_Prefix3_195(),
	OpCodes_t126150456_StaticFields::get_offset_of_Prefix2_196(),
	OpCodes_t126150456_StaticFields::get_offset_of_Prefix1_197(),
	OpCodes_t126150456_StaticFields::get_offset_of_Prefixref_198(),
	OpCodes_t126150456_StaticFields::get_offset_of_Arglist_199(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ceq_200(),
	OpCodes_t126150456_StaticFields::get_offset_of_Cgt_201(),
	OpCodes_t126150456_StaticFields::get_offset_of_Cgt_Un_202(),
	OpCodes_t126150456_StaticFields::get_offset_of_Clt_203(),
	OpCodes_t126150456_StaticFields::get_offset_of_Clt_Un_204(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldftn_205(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldvirtftn_206(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldarg_207(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldarga_208(),
	OpCodes_t126150456_StaticFields::get_offset_of_Starg_209(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldloc_210(),
	OpCodes_t126150456_StaticFields::get_offset_of_Ldloca_211(),
	OpCodes_t126150456_StaticFields::get_offset_of_Stloc_212(),
	OpCodes_t126150456_StaticFields::get_offset_of_Localloc_213(),
	OpCodes_t126150456_StaticFields::get_offset_of_Endfilter_214(),
	OpCodes_t126150456_StaticFields::get_offset_of_Unaligned_215(),
	OpCodes_t126150456_StaticFields::get_offset_of_Volatile_216(),
	OpCodes_t126150456_StaticFields::get_offset_of_Tailcall_217(),
	OpCodes_t126150456_StaticFields::get_offset_of_Initobj_218(),
	OpCodes_t126150456_StaticFields::get_offset_of_Constrained_219(),
	OpCodes_t126150456_StaticFields::get_offset_of_Cpblk_220(),
	OpCodes_t126150456_StaticFields::get_offset_of_Initblk_221(),
	OpCodes_t126150456_StaticFields::get_offset_of_Rethrow_222(),
	OpCodes_t126150456_StaticFields::get_offset_of_Sizeof_223(),
	OpCodes_t126150456_StaticFields::get_offset_of_Refanytype_224(),
	OpCodes_t126150456_StaticFields::get_offset_of_Readonly_225(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize536 = { sizeof (OperandType_t2622847887)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable536[19] = 
{
	OperandType_t2622847887::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize537 = { sizeof (PackingSize_t2976435189)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable537[10] = 
{
	PackingSize_t2976435189::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize538 = { sizeof (ParameterBuilder_t1137139675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable538[3] = 
{
	ParameterBuilder_t1137139675::get_offset_of_name_0(),
	ParameterBuilder_t1137139675::get_offset_of_attrs_1(),
	ParameterBuilder_t1137139675::get_offset_of_position_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize539 = { sizeof (PEFileKinds_t3631470751)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable539[4] = 
{
	PEFileKinds_t3631470751::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize540 = { sizeof (PropertyBuilder_t314297007), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable540[6] = 
{
	PropertyBuilder_t314297007::get_offset_of_attrs_0(),
	PropertyBuilder_t314297007::get_offset_of_name_1(),
	PropertyBuilder_t314297007::get_offset_of_type_2(),
	PropertyBuilder_t314297007::get_offset_of_set_method_3(),
	PropertyBuilder_t314297007::get_offset_of_get_method_4(),
	PropertyBuilder_t314297007::get_offset_of_typeb_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize541 = { sizeof (StackBehaviour_t3009528134)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable541[30] = 
{
	StackBehaviour_t3009528134::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize542 = { sizeof (TypeBuilder_t1073948154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable542[21] = 
{
	TypeBuilder_t1073948154::get_offset_of_tname_8(),
	TypeBuilder_t1073948154::get_offset_of_nspace_9(),
	TypeBuilder_t1073948154::get_offset_of_parent_10(),
	TypeBuilder_t1073948154::get_offset_of_nesting_type_11(),
	TypeBuilder_t1073948154::get_offset_of_interfaces_12(),
	TypeBuilder_t1073948154::get_offset_of_num_methods_13(),
	TypeBuilder_t1073948154::get_offset_of_methods_14(),
	TypeBuilder_t1073948154::get_offset_of_ctors_15(),
	TypeBuilder_t1073948154::get_offset_of_properties_16(),
	TypeBuilder_t1073948154::get_offset_of_fields_17(),
	TypeBuilder_t1073948154::get_offset_of_subtypes_18(),
	TypeBuilder_t1073948154::get_offset_of_attrs_19(),
	TypeBuilder_t1073948154::get_offset_of_table_idx_20(),
	TypeBuilder_t1073948154::get_offset_of_pmodule_21(),
	TypeBuilder_t1073948154::get_offset_of_class_size_22(),
	TypeBuilder_t1073948154::get_offset_of_packing_size_23(),
	TypeBuilder_t1073948154::get_offset_of_generic_params_24(),
	TypeBuilder_t1073948154::get_offset_of_created_25(),
	TypeBuilder_t1073948154::get_offset_of_fullname_26(),
	TypeBuilder_t1073948154::get_offset_of_createTypeCalled_27(),
	TypeBuilder_t1073948154::get_offset_of_underlying_type_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize543 = { sizeof (UnmanagedMarshal_t984015687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable543[9] = 
{
	UnmanagedMarshal_t984015687::get_offset_of_count_0(),
	UnmanagedMarshal_t984015687::get_offset_of_t_1(),
	UnmanagedMarshal_t984015687::get_offset_of_tbase_2(),
	UnmanagedMarshal_t984015687::get_offset_of_guid_3(),
	UnmanagedMarshal_t984015687::get_offset_of_mcookie_4(),
	UnmanagedMarshal_t984015687::get_offset_of_marshaltype_5(),
	UnmanagedMarshal_t984015687::get_offset_of_marshaltyperef_6(),
	UnmanagedMarshal_t984015687::get_offset_of_param_num_7(),
	UnmanagedMarshal_t984015687::get_offset_of_has_size_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize544 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize545 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize546 = { sizeof (MissingManifestResourceException_t85971840), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize547 = { sizeof (NeutralResourcesLanguageAttribute_t2027633532), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable547[1] = 
{
	NeutralResourcesLanguageAttribute_t2027633532::get_offset_of_culture_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize548 = { sizeof (ResourceManager_t4037989559), -1, sizeof(ResourceManager_t4037989559_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable548[12] = 
{
	ResourceManager_t4037989559_StaticFields::get_offset_of_ResourceCache_0(),
	ResourceManager_t4037989559_StaticFields::get_offset_of_NonExistent_1(),
	ResourceManager_t4037989559_StaticFields::get_offset_of_HeaderVersionNumber_2(),
	ResourceManager_t4037989559_StaticFields::get_offset_of_MagicNumber_3(),
	ResourceManager_t4037989559::get_offset_of_BaseNameField_4(),
	ResourceManager_t4037989559::get_offset_of_MainAssembly_5(),
	ResourceManager_t4037989559::get_offset_of_ResourceSets_6(),
	ResourceManager_t4037989559::get_offset_of_ignoreCase_7(),
	ResourceManager_t4037989559::get_offset_of_resourceSource_8(),
	ResourceManager_t4037989559::get_offset_of_resourceSetType_9(),
	ResourceManager_t4037989559::get_offset_of_resourceDir_10(),
	ResourceManager_t4037989559::get_offset_of_neutral_culture_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize549 = { sizeof (PredefinedResourceType_t4229135164)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable549[21] = 
{
	PredefinedResourceType_t4229135164::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize550 = { sizeof (ResourceReader_t3300492639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable550[13] = 
{
	ResourceReader_t3300492639::get_offset_of_reader_0(),
	ResourceReader_t3300492639::get_offset_of_readerLock_1(),
	ResourceReader_t3300492639::get_offset_of_formatter_2(),
	ResourceReader_t3300492639::get_offset_of_resourceCount_3(),
	ResourceReader_t3300492639::get_offset_of_typeCount_4(),
	ResourceReader_t3300492639::get_offset_of_typeNames_5(),
	ResourceReader_t3300492639::get_offset_of_hashes_6(),
	ResourceReader_t3300492639::get_offset_of_infos_7(),
	ResourceReader_t3300492639::get_offset_of_dataSectionOffset_8(),
	ResourceReader_t3300492639::get_offset_of_nameSectionOffset_9(),
	ResourceReader_t3300492639::get_offset_of_resource_ver_10(),
	ResourceReader_t3300492639::get_offset_of_cache_11(),
	ResourceReader_t3300492639::get_offset_of_cache_lock_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize551 = { sizeof (ResourceInfo_t2872965302)+ sizeof (Il2CppObject), sizeof(ResourceInfo_t2872965302_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable551[3] = 
{
	ResourceInfo_t2872965302::get_offset_of_ValuePosition_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ResourceInfo_t2872965302::get_offset_of_ResourceName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ResourceInfo_t2872965302::get_offset_of_TypeIndex_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize552 = { sizeof (ResourceCacheItem_t51292791)+ sizeof (Il2CppObject), sizeof(ResourceCacheItem_t51292791_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable552[2] = 
{
	ResourceCacheItem_t51292791::get_offset_of_ResourceName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ResourceCacheItem_t51292791::get_offset_of_ResourceValue_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize553 = { sizeof (ResourceEnumerator_t2106512554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable553[3] = 
{
	ResourceEnumerator_t2106512554::get_offset_of_reader_0(),
	ResourceEnumerator_t2106512554::get_offset_of_index_1(),
	ResourceEnumerator_t2106512554::get_offset_of_finished_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize554 = { sizeof (ResourceSet_t2827911187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable554[4] = 
{
	ResourceSet_t2827911187::get_offset_of_Reader_0(),
	ResourceSet_t2827911187::get_offset_of_Table_1(),
	ResourceSet_t2827911187::get_offset_of_resources_read_2(),
	ResourceSet_t2827911187::get_offset_of_disposed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize555 = { sizeof (ResourceWriter_t1892498018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable555[2] = 
{
	ResourceWriter_t1892498018::get_offset_of_resources_0(),
	ResourceWriter_t1892498018::get_offset_of_stream_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize556 = { sizeof (TypeByNameObject_t584266494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable556[2] = 
{
	TypeByNameObject_t584266494::get_offset_of_TypeName_0(),
	TypeByNameObject_t584266494::get_offset_of_Value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize557 = { sizeof (RuntimeResourceSet_t3081501575), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize558 = { sizeof (SatelliteContractVersionAttribute_t3549813284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable558[1] = 
{
	SatelliteContractVersionAttribute_t3549813284::get_offset_of_ver_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize559 = { sizeof (CompilationRelaxations_t3292409279)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable559[2] = 
{
	CompilationRelaxations_t3292409279::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize560 = { sizeof (CompilationRelaxationsAttribute_t1721442111), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable560[1] = 
{
	CompilationRelaxationsAttribute_t1721442111::get_offset_of_relax_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize561 = { sizeof (DefaultDependencyAttribute_t548613883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable561[1] = 
{
	DefaultDependencyAttribute_t548613883::get_offset_of_hint_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize562 = { sizeof (DependencyAttribute_t1606102833), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable562[2] = 
{
	DependencyAttribute_t1606102833::get_offset_of_dependentAssembly_0(),
	DependencyAttribute_t1606102833::get_offset_of_hint_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize563 = { sizeof (IsVolatile_t2097058663), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize564 = { sizeof (LoadHint_t3743314490)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable564[4] = 
{
	LoadHint_t3743314490::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize565 = { sizeof (StringFreezingAttribute_t3031362240), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize566 = { sizeof (CriticalFinalizerObject_t701527852), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize567 = { sizeof (Cer_t573022029)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable567[4] = 
{
	Cer_t573022029::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize568 = { sizeof (Consistency_t4071665526)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable568[5] = 
{
	Consistency_t4071665526::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize569 = { sizeof (ReliabilityContractAttribute_t502331440), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable569[2] = 
{
	ReliabilityContractAttribute_t502331440::get_offset_of_consistency_0(),
	ReliabilityContractAttribute_t502331440::get_offset_of_cer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize570 = { sizeof (ActivationArguments_t4219999170), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize571 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize572 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize573 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize574 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize575 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize576 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize577 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize578 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize579 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize580 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize581 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize582 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize583 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize584 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize585 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize586 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize587 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize588 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize589 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize590 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize591 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize592 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize593 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize594 = { sizeof (BestFitMappingAttribute_t4073720592), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable594[1] = 
{
	BestFitMappingAttribute_t4073720592::get_offset_of_bfm_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize595 = { sizeof (CallingConvention_t1027624783)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable595[6] = 
{
	CallingConvention_t1027624783::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize596 = { sizeof (CharSet_t3391187264)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable596[5] = 
{
	CharSet_t3391187264::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize597 = { sizeof (ClassInterfaceAttribute_t2274790349), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable597[1] = 
{
	ClassInterfaceAttribute_t2274790349::get_offset_of_ciType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize598 = { sizeof (ClassInterfaceType_t4029497327)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable598[4] = 
{
	ClassInterfaceType_t4029497327::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize599 = { sizeof (ComCompatibleVersionAttribute_t1233303509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable599[4] = 
{
	ComCompatibleVersionAttribute_t1233303509::get_offset_of_major_0(),
	ComCompatibleVersionAttribute_t1233303509::get_offset_of_minor_1(),
	ComCompatibleVersionAttribute_t1233303509::get_offset_of_build_2(),
	ComCompatibleVersionAttribute_t1233303509::get_offset_of_revision_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
