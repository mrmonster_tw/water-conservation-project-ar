﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3156163955.h"

// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Configuration.WebServicesSection
struct  WebServicesSection_t2191173613  : public ConfigurationSection_t3156163955
{
public:

public:
};

struct WebServicesSection_t2191173613_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Web.Services.Configuration.WebServicesSection::conformanceWarningsProp
	ConfigurationProperty_t3590861854 * ___conformanceWarningsProp_17;
	// System.Configuration.ConfigurationProperty System.Web.Services.Configuration.WebServicesSection::diagnosticsProp
	ConfigurationProperty_t3590861854 * ___diagnosticsProp_18;
	// System.Configuration.ConfigurationProperty System.Web.Services.Configuration.WebServicesSection::protocolsProp
	ConfigurationProperty_t3590861854 * ___protocolsProp_19;
	// System.Configuration.ConfigurationProperty System.Web.Services.Configuration.WebServicesSection::serviceDescriptionFormatExtensionTypesProp
	ConfigurationProperty_t3590861854 * ___serviceDescriptionFormatExtensionTypesProp_20;
	// System.Configuration.ConfigurationProperty System.Web.Services.Configuration.WebServicesSection::soapEnvelopeProcessingProp
	ConfigurationProperty_t3590861854 * ___soapEnvelopeProcessingProp_21;
	// System.Configuration.ConfigurationProperty System.Web.Services.Configuration.WebServicesSection::soapExtensionImporterTypesProp
	ConfigurationProperty_t3590861854 * ___soapExtensionImporterTypesProp_22;
	// System.Configuration.ConfigurationProperty System.Web.Services.Configuration.WebServicesSection::soapExtensionReflectorTypesProp
	ConfigurationProperty_t3590861854 * ___soapExtensionReflectorTypesProp_23;
	// System.Configuration.ConfigurationProperty System.Web.Services.Configuration.WebServicesSection::soapExtensionTypesProp
	ConfigurationProperty_t3590861854 * ___soapExtensionTypesProp_24;
	// System.Configuration.ConfigurationProperty System.Web.Services.Configuration.WebServicesSection::soapServerProtocolFactoryProp
	ConfigurationProperty_t3590861854 * ___soapServerProtocolFactoryProp_25;
	// System.Configuration.ConfigurationProperty System.Web.Services.Configuration.WebServicesSection::soapTransportImporterTypesProp
	ConfigurationProperty_t3590861854 * ___soapTransportImporterTypesProp_26;
	// System.Configuration.ConfigurationProperty System.Web.Services.Configuration.WebServicesSection::wsdlHelpGeneratorProp
	ConfigurationProperty_t3590861854 * ___wsdlHelpGeneratorProp_27;
	// System.Configuration.ConfigurationPropertyCollection System.Web.Services.Configuration.WebServicesSection::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_28;

public:
	inline static int32_t get_offset_of_conformanceWarningsProp_17() { return static_cast<int32_t>(offsetof(WebServicesSection_t2191173613_StaticFields, ___conformanceWarningsProp_17)); }
	inline ConfigurationProperty_t3590861854 * get_conformanceWarningsProp_17() const { return ___conformanceWarningsProp_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_conformanceWarningsProp_17() { return &___conformanceWarningsProp_17; }
	inline void set_conformanceWarningsProp_17(ConfigurationProperty_t3590861854 * value)
	{
		___conformanceWarningsProp_17 = value;
		Il2CppCodeGenWriteBarrier(&___conformanceWarningsProp_17, value);
	}

	inline static int32_t get_offset_of_diagnosticsProp_18() { return static_cast<int32_t>(offsetof(WebServicesSection_t2191173613_StaticFields, ___diagnosticsProp_18)); }
	inline ConfigurationProperty_t3590861854 * get_diagnosticsProp_18() const { return ___diagnosticsProp_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_diagnosticsProp_18() { return &___diagnosticsProp_18; }
	inline void set_diagnosticsProp_18(ConfigurationProperty_t3590861854 * value)
	{
		___diagnosticsProp_18 = value;
		Il2CppCodeGenWriteBarrier(&___diagnosticsProp_18, value);
	}

	inline static int32_t get_offset_of_protocolsProp_19() { return static_cast<int32_t>(offsetof(WebServicesSection_t2191173613_StaticFields, ___protocolsProp_19)); }
	inline ConfigurationProperty_t3590861854 * get_protocolsProp_19() const { return ___protocolsProp_19; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_protocolsProp_19() { return &___protocolsProp_19; }
	inline void set_protocolsProp_19(ConfigurationProperty_t3590861854 * value)
	{
		___protocolsProp_19 = value;
		Il2CppCodeGenWriteBarrier(&___protocolsProp_19, value);
	}

	inline static int32_t get_offset_of_serviceDescriptionFormatExtensionTypesProp_20() { return static_cast<int32_t>(offsetof(WebServicesSection_t2191173613_StaticFields, ___serviceDescriptionFormatExtensionTypesProp_20)); }
	inline ConfigurationProperty_t3590861854 * get_serviceDescriptionFormatExtensionTypesProp_20() const { return ___serviceDescriptionFormatExtensionTypesProp_20; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_serviceDescriptionFormatExtensionTypesProp_20() { return &___serviceDescriptionFormatExtensionTypesProp_20; }
	inline void set_serviceDescriptionFormatExtensionTypesProp_20(ConfigurationProperty_t3590861854 * value)
	{
		___serviceDescriptionFormatExtensionTypesProp_20 = value;
		Il2CppCodeGenWriteBarrier(&___serviceDescriptionFormatExtensionTypesProp_20, value);
	}

	inline static int32_t get_offset_of_soapEnvelopeProcessingProp_21() { return static_cast<int32_t>(offsetof(WebServicesSection_t2191173613_StaticFields, ___soapEnvelopeProcessingProp_21)); }
	inline ConfigurationProperty_t3590861854 * get_soapEnvelopeProcessingProp_21() const { return ___soapEnvelopeProcessingProp_21; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_soapEnvelopeProcessingProp_21() { return &___soapEnvelopeProcessingProp_21; }
	inline void set_soapEnvelopeProcessingProp_21(ConfigurationProperty_t3590861854 * value)
	{
		___soapEnvelopeProcessingProp_21 = value;
		Il2CppCodeGenWriteBarrier(&___soapEnvelopeProcessingProp_21, value);
	}

	inline static int32_t get_offset_of_soapExtensionImporterTypesProp_22() { return static_cast<int32_t>(offsetof(WebServicesSection_t2191173613_StaticFields, ___soapExtensionImporterTypesProp_22)); }
	inline ConfigurationProperty_t3590861854 * get_soapExtensionImporterTypesProp_22() const { return ___soapExtensionImporterTypesProp_22; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_soapExtensionImporterTypesProp_22() { return &___soapExtensionImporterTypesProp_22; }
	inline void set_soapExtensionImporterTypesProp_22(ConfigurationProperty_t3590861854 * value)
	{
		___soapExtensionImporterTypesProp_22 = value;
		Il2CppCodeGenWriteBarrier(&___soapExtensionImporterTypesProp_22, value);
	}

	inline static int32_t get_offset_of_soapExtensionReflectorTypesProp_23() { return static_cast<int32_t>(offsetof(WebServicesSection_t2191173613_StaticFields, ___soapExtensionReflectorTypesProp_23)); }
	inline ConfigurationProperty_t3590861854 * get_soapExtensionReflectorTypesProp_23() const { return ___soapExtensionReflectorTypesProp_23; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_soapExtensionReflectorTypesProp_23() { return &___soapExtensionReflectorTypesProp_23; }
	inline void set_soapExtensionReflectorTypesProp_23(ConfigurationProperty_t3590861854 * value)
	{
		___soapExtensionReflectorTypesProp_23 = value;
		Il2CppCodeGenWriteBarrier(&___soapExtensionReflectorTypesProp_23, value);
	}

	inline static int32_t get_offset_of_soapExtensionTypesProp_24() { return static_cast<int32_t>(offsetof(WebServicesSection_t2191173613_StaticFields, ___soapExtensionTypesProp_24)); }
	inline ConfigurationProperty_t3590861854 * get_soapExtensionTypesProp_24() const { return ___soapExtensionTypesProp_24; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_soapExtensionTypesProp_24() { return &___soapExtensionTypesProp_24; }
	inline void set_soapExtensionTypesProp_24(ConfigurationProperty_t3590861854 * value)
	{
		___soapExtensionTypesProp_24 = value;
		Il2CppCodeGenWriteBarrier(&___soapExtensionTypesProp_24, value);
	}

	inline static int32_t get_offset_of_soapServerProtocolFactoryProp_25() { return static_cast<int32_t>(offsetof(WebServicesSection_t2191173613_StaticFields, ___soapServerProtocolFactoryProp_25)); }
	inline ConfigurationProperty_t3590861854 * get_soapServerProtocolFactoryProp_25() const { return ___soapServerProtocolFactoryProp_25; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_soapServerProtocolFactoryProp_25() { return &___soapServerProtocolFactoryProp_25; }
	inline void set_soapServerProtocolFactoryProp_25(ConfigurationProperty_t3590861854 * value)
	{
		___soapServerProtocolFactoryProp_25 = value;
		Il2CppCodeGenWriteBarrier(&___soapServerProtocolFactoryProp_25, value);
	}

	inline static int32_t get_offset_of_soapTransportImporterTypesProp_26() { return static_cast<int32_t>(offsetof(WebServicesSection_t2191173613_StaticFields, ___soapTransportImporterTypesProp_26)); }
	inline ConfigurationProperty_t3590861854 * get_soapTransportImporterTypesProp_26() const { return ___soapTransportImporterTypesProp_26; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_soapTransportImporterTypesProp_26() { return &___soapTransportImporterTypesProp_26; }
	inline void set_soapTransportImporterTypesProp_26(ConfigurationProperty_t3590861854 * value)
	{
		___soapTransportImporterTypesProp_26 = value;
		Il2CppCodeGenWriteBarrier(&___soapTransportImporterTypesProp_26, value);
	}

	inline static int32_t get_offset_of_wsdlHelpGeneratorProp_27() { return static_cast<int32_t>(offsetof(WebServicesSection_t2191173613_StaticFields, ___wsdlHelpGeneratorProp_27)); }
	inline ConfigurationProperty_t3590861854 * get_wsdlHelpGeneratorProp_27() const { return ___wsdlHelpGeneratorProp_27; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_wsdlHelpGeneratorProp_27() { return &___wsdlHelpGeneratorProp_27; }
	inline void set_wsdlHelpGeneratorProp_27(ConfigurationProperty_t3590861854 * value)
	{
		___wsdlHelpGeneratorProp_27 = value;
		Il2CppCodeGenWriteBarrier(&___wsdlHelpGeneratorProp_27, value);
	}

	inline static int32_t get_offset_of_properties_28() { return static_cast<int32_t>(offsetof(WebServicesSection_t2191173613_StaticFields, ___properties_28)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_28() const { return ___properties_28; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_28() { return &___properties_28; }
	inline void set_properties_28(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_28 = value;
		Il2CppCodeGenWriteBarrier(&___properties_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
