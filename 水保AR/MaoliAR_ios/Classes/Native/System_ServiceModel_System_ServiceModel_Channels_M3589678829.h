﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_T2144904149.h"
#include "System_ServiceModel_System_ServiceModel_DeadLetter1799023886.h"
#include "System_ServiceModel_System_ServiceModel_ReceiveErr2226770027.h"
#include "mscorlib_System_TimeSpan881159249.h"

// System.Uri
struct Uri_t100236324;
// System.ServiceModel.MsmqTransportSecurity
struct MsmqTransportSecurity_t2605141420;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.MsmqBindingElementBase
struct  MsmqBindingElementBase_t3589678829  : public TransportBindingElement_t2144904149
{
public:
	// System.Uri System.ServiceModel.Channels.MsmqBindingElementBase::custom_dead_letter_queue
	Uri_t100236324 * ___custom_dead_letter_queue_3;
	// System.ServiceModel.DeadLetterQueue System.ServiceModel.Channels.MsmqBindingElementBase::dead_letter_queue
	int32_t ___dead_letter_queue_4;
	// System.Boolean System.ServiceModel.Channels.MsmqBindingElementBase::durable
	bool ___durable_5;
	// System.Boolean System.ServiceModel.Channels.MsmqBindingElementBase::exactly_once
	bool ___exactly_once_6;
	// System.Boolean System.ServiceModel.Channels.MsmqBindingElementBase::use_msmq_trace
	bool ___use_msmq_trace_7;
	// System.Boolean System.ServiceModel.Channels.MsmqBindingElementBase::use_source_journal
	bool ___use_source_journal_8;
	// System.Int32 System.ServiceModel.Channels.MsmqBindingElementBase::max_retry_cycles
	int32_t ___max_retry_cycles_9;
	// System.Int32 System.ServiceModel.Channels.MsmqBindingElementBase::receive_retry_count
	int32_t ___receive_retry_count_10;
	// System.ServiceModel.ReceiveErrorHandling System.ServiceModel.Channels.MsmqBindingElementBase::receive_error_handling
	int32_t ___receive_error_handling_11;
	// System.TimeSpan System.ServiceModel.Channels.MsmqBindingElementBase::retry_cycle_delay
	TimeSpan_t881159249  ___retry_cycle_delay_12;
	// System.TimeSpan System.ServiceModel.Channels.MsmqBindingElementBase::ttl
	TimeSpan_t881159249  ___ttl_13;
	// System.ServiceModel.MsmqTransportSecurity System.ServiceModel.Channels.MsmqBindingElementBase::transport_security
	MsmqTransportSecurity_t2605141420 * ___transport_security_14;

public:
	inline static int32_t get_offset_of_custom_dead_letter_queue_3() { return static_cast<int32_t>(offsetof(MsmqBindingElementBase_t3589678829, ___custom_dead_letter_queue_3)); }
	inline Uri_t100236324 * get_custom_dead_letter_queue_3() const { return ___custom_dead_letter_queue_3; }
	inline Uri_t100236324 ** get_address_of_custom_dead_letter_queue_3() { return &___custom_dead_letter_queue_3; }
	inline void set_custom_dead_letter_queue_3(Uri_t100236324 * value)
	{
		___custom_dead_letter_queue_3 = value;
		Il2CppCodeGenWriteBarrier(&___custom_dead_letter_queue_3, value);
	}

	inline static int32_t get_offset_of_dead_letter_queue_4() { return static_cast<int32_t>(offsetof(MsmqBindingElementBase_t3589678829, ___dead_letter_queue_4)); }
	inline int32_t get_dead_letter_queue_4() const { return ___dead_letter_queue_4; }
	inline int32_t* get_address_of_dead_letter_queue_4() { return &___dead_letter_queue_4; }
	inline void set_dead_letter_queue_4(int32_t value)
	{
		___dead_letter_queue_4 = value;
	}

	inline static int32_t get_offset_of_durable_5() { return static_cast<int32_t>(offsetof(MsmqBindingElementBase_t3589678829, ___durable_5)); }
	inline bool get_durable_5() const { return ___durable_5; }
	inline bool* get_address_of_durable_5() { return &___durable_5; }
	inline void set_durable_5(bool value)
	{
		___durable_5 = value;
	}

	inline static int32_t get_offset_of_exactly_once_6() { return static_cast<int32_t>(offsetof(MsmqBindingElementBase_t3589678829, ___exactly_once_6)); }
	inline bool get_exactly_once_6() const { return ___exactly_once_6; }
	inline bool* get_address_of_exactly_once_6() { return &___exactly_once_6; }
	inline void set_exactly_once_6(bool value)
	{
		___exactly_once_6 = value;
	}

	inline static int32_t get_offset_of_use_msmq_trace_7() { return static_cast<int32_t>(offsetof(MsmqBindingElementBase_t3589678829, ___use_msmq_trace_7)); }
	inline bool get_use_msmq_trace_7() const { return ___use_msmq_trace_7; }
	inline bool* get_address_of_use_msmq_trace_7() { return &___use_msmq_trace_7; }
	inline void set_use_msmq_trace_7(bool value)
	{
		___use_msmq_trace_7 = value;
	}

	inline static int32_t get_offset_of_use_source_journal_8() { return static_cast<int32_t>(offsetof(MsmqBindingElementBase_t3589678829, ___use_source_journal_8)); }
	inline bool get_use_source_journal_8() const { return ___use_source_journal_8; }
	inline bool* get_address_of_use_source_journal_8() { return &___use_source_journal_8; }
	inline void set_use_source_journal_8(bool value)
	{
		___use_source_journal_8 = value;
	}

	inline static int32_t get_offset_of_max_retry_cycles_9() { return static_cast<int32_t>(offsetof(MsmqBindingElementBase_t3589678829, ___max_retry_cycles_9)); }
	inline int32_t get_max_retry_cycles_9() const { return ___max_retry_cycles_9; }
	inline int32_t* get_address_of_max_retry_cycles_9() { return &___max_retry_cycles_9; }
	inline void set_max_retry_cycles_9(int32_t value)
	{
		___max_retry_cycles_9 = value;
	}

	inline static int32_t get_offset_of_receive_retry_count_10() { return static_cast<int32_t>(offsetof(MsmqBindingElementBase_t3589678829, ___receive_retry_count_10)); }
	inline int32_t get_receive_retry_count_10() const { return ___receive_retry_count_10; }
	inline int32_t* get_address_of_receive_retry_count_10() { return &___receive_retry_count_10; }
	inline void set_receive_retry_count_10(int32_t value)
	{
		___receive_retry_count_10 = value;
	}

	inline static int32_t get_offset_of_receive_error_handling_11() { return static_cast<int32_t>(offsetof(MsmqBindingElementBase_t3589678829, ___receive_error_handling_11)); }
	inline int32_t get_receive_error_handling_11() const { return ___receive_error_handling_11; }
	inline int32_t* get_address_of_receive_error_handling_11() { return &___receive_error_handling_11; }
	inline void set_receive_error_handling_11(int32_t value)
	{
		___receive_error_handling_11 = value;
	}

	inline static int32_t get_offset_of_retry_cycle_delay_12() { return static_cast<int32_t>(offsetof(MsmqBindingElementBase_t3589678829, ___retry_cycle_delay_12)); }
	inline TimeSpan_t881159249  get_retry_cycle_delay_12() const { return ___retry_cycle_delay_12; }
	inline TimeSpan_t881159249 * get_address_of_retry_cycle_delay_12() { return &___retry_cycle_delay_12; }
	inline void set_retry_cycle_delay_12(TimeSpan_t881159249  value)
	{
		___retry_cycle_delay_12 = value;
	}

	inline static int32_t get_offset_of_ttl_13() { return static_cast<int32_t>(offsetof(MsmqBindingElementBase_t3589678829, ___ttl_13)); }
	inline TimeSpan_t881159249  get_ttl_13() const { return ___ttl_13; }
	inline TimeSpan_t881159249 * get_address_of_ttl_13() { return &___ttl_13; }
	inline void set_ttl_13(TimeSpan_t881159249  value)
	{
		___ttl_13 = value;
	}

	inline static int32_t get_offset_of_transport_security_14() { return static_cast<int32_t>(offsetof(MsmqBindingElementBase_t3589678829, ___transport_security_14)); }
	inline MsmqTransportSecurity_t2605141420 * get_transport_security_14() const { return ___transport_security_14; }
	inline MsmqTransportSecurity_t2605141420 ** get_address_of_transport_security_14() { return &___transport_security_14; }
	inline void set_transport_security_14(MsmqTransportSecurity_t2605141420 * value)
	{
		___transport_security_14 = value;
		Il2CppCodeGenWriteBarrier(&___transport_security_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
