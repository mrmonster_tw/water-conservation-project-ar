﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_B3643137745.h"
#include "System_ServiceModel_System_ServiceModel_Security_S1263698963.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Se713688608.h"

// System.ServiceModel.Security.SecurityAlgorithmSuite
struct SecurityAlgorithmSuite_t2980594242;
// System.ServiceModel.MessageSecurityVersion
struct MessageSecurityVersion_t2079539966;
// System.ServiceModel.Security.Tokens.SupportingTokenParameters
struct SupportingTokenParameters_t907883796;
// System.Collections.Generic.IDictionary`2<System.String,System.ServiceModel.Security.Tokens.SupportingTokenParameters>
struct IDictionary_2_t3451958782;
// System.ServiceModel.Channels.LocalServiceSecuritySettings
struct LocalServiceSecuritySettings_t2374387342;
// System.ServiceModel.Channels.LocalClientSecuritySettings
struct LocalClientSecuritySettings_t2865192915;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.SecurityBindingElement
struct  SecurityBindingElement_t1737011943  : public BindingElement_t3643137745
{
public:
	// System.ServiceModel.Security.SecurityAlgorithmSuite System.ServiceModel.Channels.SecurityBindingElement::alg_suite
	SecurityAlgorithmSuite_t2980594242 * ___alg_suite_0;
	// System.ServiceModel.Security.SecurityKeyEntropyMode System.ServiceModel.Channels.SecurityBindingElement::key_entropy_mode
	int32_t ___key_entropy_mode_1;
	// System.ServiceModel.Channels.SecurityHeaderLayout System.ServiceModel.Channels.SecurityBindingElement::security_header_layout
	int32_t ___security_header_layout_2;
	// System.ServiceModel.MessageSecurityVersion System.ServiceModel.Channels.SecurityBindingElement::msg_security_version
	MessageSecurityVersion_t2079539966 * ___msg_security_version_3;
	// System.ServiceModel.Security.Tokens.SupportingTokenParameters System.ServiceModel.Channels.SecurityBindingElement::endpoint
	SupportingTokenParameters_t907883796 * ___endpoint_4;
	// System.ServiceModel.Security.Tokens.SupportingTokenParameters System.ServiceModel.Channels.SecurityBindingElement::opt_endpoint
	SupportingTokenParameters_t907883796 * ___opt_endpoint_5;
	// System.Collections.Generic.IDictionary`2<System.String,System.ServiceModel.Security.Tokens.SupportingTokenParameters> System.ServiceModel.Channels.SecurityBindingElement::operation
	Il2CppObject* ___operation_6;
	// System.Collections.Generic.IDictionary`2<System.String,System.ServiceModel.Security.Tokens.SupportingTokenParameters> System.ServiceModel.Channels.SecurityBindingElement::opt_operation
	Il2CppObject* ___opt_operation_7;
	// System.ServiceModel.Channels.LocalServiceSecuritySettings System.ServiceModel.Channels.SecurityBindingElement::service_settings
	LocalServiceSecuritySettings_t2374387342 * ___service_settings_8;
	// System.Boolean System.ServiceModel.Channels.SecurityBindingElement::<IncludeTimestamp>k__BackingField
	bool ___U3CIncludeTimestampU3Ek__BackingField_9;
	// System.ServiceModel.Channels.LocalClientSecuritySettings System.ServiceModel.Channels.SecurityBindingElement::<LocalClientSettings>k__BackingField
	LocalClientSecuritySettings_t2865192915 * ___U3CLocalClientSettingsU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_alg_suite_0() { return static_cast<int32_t>(offsetof(SecurityBindingElement_t1737011943, ___alg_suite_0)); }
	inline SecurityAlgorithmSuite_t2980594242 * get_alg_suite_0() const { return ___alg_suite_0; }
	inline SecurityAlgorithmSuite_t2980594242 ** get_address_of_alg_suite_0() { return &___alg_suite_0; }
	inline void set_alg_suite_0(SecurityAlgorithmSuite_t2980594242 * value)
	{
		___alg_suite_0 = value;
		Il2CppCodeGenWriteBarrier(&___alg_suite_0, value);
	}

	inline static int32_t get_offset_of_key_entropy_mode_1() { return static_cast<int32_t>(offsetof(SecurityBindingElement_t1737011943, ___key_entropy_mode_1)); }
	inline int32_t get_key_entropy_mode_1() const { return ___key_entropy_mode_1; }
	inline int32_t* get_address_of_key_entropy_mode_1() { return &___key_entropy_mode_1; }
	inline void set_key_entropy_mode_1(int32_t value)
	{
		___key_entropy_mode_1 = value;
	}

	inline static int32_t get_offset_of_security_header_layout_2() { return static_cast<int32_t>(offsetof(SecurityBindingElement_t1737011943, ___security_header_layout_2)); }
	inline int32_t get_security_header_layout_2() const { return ___security_header_layout_2; }
	inline int32_t* get_address_of_security_header_layout_2() { return &___security_header_layout_2; }
	inline void set_security_header_layout_2(int32_t value)
	{
		___security_header_layout_2 = value;
	}

	inline static int32_t get_offset_of_msg_security_version_3() { return static_cast<int32_t>(offsetof(SecurityBindingElement_t1737011943, ___msg_security_version_3)); }
	inline MessageSecurityVersion_t2079539966 * get_msg_security_version_3() const { return ___msg_security_version_3; }
	inline MessageSecurityVersion_t2079539966 ** get_address_of_msg_security_version_3() { return &___msg_security_version_3; }
	inline void set_msg_security_version_3(MessageSecurityVersion_t2079539966 * value)
	{
		___msg_security_version_3 = value;
		Il2CppCodeGenWriteBarrier(&___msg_security_version_3, value);
	}

	inline static int32_t get_offset_of_endpoint_4() { return static_cast<int32_t>(offsetof(SecurityBindingElement_t1737011943, ___endpoint_4)); }
	inline SupportingTokenParameters_t907883796 * get_endpoint_4() const { return ___endpoint_4; }
	inline SupportingTokenParameters_t907883796 ** get_address_of_endpoint_4() { return &___endpoint_4; }
	inline void set_endpoint_4(SupportingTokenParameters_t907883796 * value)
	{
		___endpoint_4 = value;
		Il2CppCodeGenWriteBarrier(&___endpoint_4, value);
	}

	inline static int32_t get_offset_of_opt_endpoint_5() { return static_cast<int32_t>(offsetof(SecurityBindingElement_t1737011943, ___opt_endpoint_5)); }
	inline SupportingTokenParameters_t907883796 * get_opt_endpoint_5() const { return ___opt_endpoint_5; }
	inline SupportingTokenParameters_t907883796 ** get_address_of_opt_endpoint_5() { return &___opt_endpoint_5; }
	inline void set_opt_endpoint_5(SupportingTokenParameters_t907883796 * value)
	{
		___opt_endpoint_5 = value;
		Il2CppCodeGenWriteBarrier(&___opt_endpoint_5, value);
	}

	inline static int32_t get_offset_of_operation_6() { return static_cast<int32_t>(offsetof(SecurityBindingElement_t1737011943, ___operation_6)); }
	inline Il2CppObject* get_operation_6() const { return ___operation_6; }
	inline Il2CppObject** get_address_of_operation_6() { return &___operation_6; }
	inline void set_operation_6(Il2CppObject* value)
	{
		___operation_6 = value;
		Il2CppCodeGenWriteBarrier(&___operation_6, value);
	}

	inline static int32_t get_offset_of_opt_operation_7() { return static_cast<int32_t>(offsetof(SecurityBindingElement_t1737011943, ___opt_operation_7)); }
	inline Il2CppObject* get_opt_operation_7() const { return ___opt_operation_7; }
	inline Il2CppObject** get_address_of_opt_operation_7() { return &___opt_operation_7; }
	inline void set_opt_operation_7(Il2CppObject* value)
	{
		___opt_operation_7 = value;
		Il2CppCodeGenWriteBarrier(&___opt_operation_7, value);
	}

	inline static int32_t get_offset_of_service_settings_8() { return static_cast<int32_t>(offsetof(SecurityBindingElement_t1737011943, ___service_settings_8)); }
	inline LocalServiceSecuritySettings_t2374387342 * get_service_settings_8() const { return ___service_settings_8; }
	inline LocalServiceSecuritySettings_t2374387342 ** get_address_of_service_settings_8() { return &___service_settings_8; }
	inline void set_service_settings_8(LocalServiceSecuritySettings_t2374387342 * value)
	{
		___service_settings_8 = value;
		Il2CppCodeGenWriteBarrier(&___service_settings_8, value);
	}

	inline static int32_t get_offset_of_U3CIncludeTimestampU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(SecurityBindingElement_t1737011943, ___U3CIncludeTimestampU3Ek__BackingField_9)); }
	inline bool get_U3CIncludeTimestampU3Ek__BackingField_9() const { return ___U3CIncludeTimestampU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CIncludeTimestampU3Ek__BackingField_9() { return &___U3CIncludeTimestampU3Ek__BackingField_9; }
	inline void set_U3CIncludeTimestampU3Ek__BackingField_9(bool value)
	{
		___U3CIncludeTimestampU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CLocalClientSettingsU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(SecurityBindingElement_t1737011943, ___U3CLocalClientSettingsU3Ek__BackingField_10)); }
	inline LocalClientSecuritySettings_t2865192915 * get_U3CLocalClientSettingsU3Ek__BackingField_10() const { return ___U3CLocalClientSettingsU3Ek__BackingField_10; }
	inline LocalClientSecuritySettings_t2865192915 ** get_address_of_U3CLocalClientSettingsU3Ek__BackingField_10() { return &___U3CLocalClientSettingsU3Ek__BackingField_10; }
	inline void set_U3CLocalClientSettingsU3Ek__BackingField_10(LocalClientSecuritySettings_t2865192915 * value)
	{
		___U3CLocalClientSettingsU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLocalClientSettingsU3Ek__BackingField_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
