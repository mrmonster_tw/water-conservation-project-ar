﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_FlashingController1810688774.h"

// UnityEngine.Transform
struct Transform_t3600365921;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// System.String[]
struct StringU5BU5D_t1281789340;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CompoundObjectController
struct  CompoundObjectController_t273631460  : public FlashingController_t1810688774
{
public:
	// UnityEngine.Transform CompoundObjectController::tr
	Transform_t3600365921 * ___tr_9;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> CompoundObjectController::objects
	List_1_t2585711361 * ___objects_10;
	// System.Int32 CompoundObjectController::currentShaderID
	int32_t ___currentShaderID_11;
	// System.String[] CompoundObjectController::shaderNames
	StringU5BU5D_t1281789340* ___shaderNames_12;
	// System.Int32 CompoundObjectController::ox
	int32_t ___ox_13;

public:
	inline static int32_t get_offset_of_tr_9() { return static_cast<int32_t>(offsetof(CompoundObjectController_t273631460, ___tr_9)); }
	inline Transform_t3600365921 * get_tr_9() const { return ___tr_9; }
	inline Transform_t3600365921 ** get_address_of_tr_9() { return &___tr_9; }
	inline void set_tr_9(Transform_t3600365921 * value)
	{
		___tr_9 = value;
		Il2CppCodeGenWriteBarrier(&___tr_9, value);
	}

	inline static int32_t get_offset_of_objects_10() { return static_cast<int32_t>(offsetof(CompoundObjectController_t273631460, ___objects_10)); }
	inline List_1_t2585711361 * get_objects_10() const { return ___objects_10; }
	inline List_1_t2585711361 ** get_address_of_objects_10() { return &___objects_10; }
	inline void set_objects_10(List_1_t2585711361 * value)
	{
		___objects_10 = value;
		Il2CppCodeGenWriteBarrier(&___objects_10, value);
	}

	inline static int32_t get_offset_of_currentShaderID_11() { return static_cast<int32_t>(offsetof(CompoundObjectController_t273631460, ___currentShaderID_11)); }
	inline int32_t get_currentShaderID_11() const { return ___currentShaderID_11; }
	inline int32_t* get_address_of_currentShaderID_11() { return &___currentShaderID_11; }
	inline void set_currentShaderID_11(int32_t value)
	{
		___currentShaderID_11 = value;
	}

	inline static int32_t get_offset_of_shaderNames_12() { return static_cast<int32_t>(offsetof(CompoundObjectController_t273631460, ___shaderNames_12)); }
	inline StringU5BU5D_t1281789340* get_shaderNames_12() const { return ___shaderNames_12; }
	inline StringU5BU5D_t1281789340** get_address_of_shaderNames_12() { return &___shaderNames_12; }
	inline void set_shaderNames_12(StringU5BU5D_t1281789340* value)
	{
		___shaderNames_12 = value;
		Il2CppCodeGenWriteBarrier(&___shaderNames_12, value);
	}

	inline static int32_t get_offset_of_ox_13() { return static_cast<int32_t>(offsetof(CompoundObjectController_t273631460, ___ox_13)); }
	inline int32_t get_ox_13() const { return ___ox_13; }
	inline int32_t* get_address_of_ox_13() { return &___ox_13; }
	inline void set_ox_13(int32_t value)
	{
		___ox_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
