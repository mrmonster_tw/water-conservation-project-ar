﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_Stream1273022909.h"

// System.Byte
struct Byte_t1134296376;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.IntPtrStream
struct  IntPtrStream_t1988557108  : public Stream_t1273022909
{
public:
	// System.Byte* System.Web.IntPtrStream::base_address
	uint8_t* ___base_address_2;
	// System.Int32 System.Web.IntPtrStream::size
	int32_t ___size_3;
	// System.Int32 System.Web.IntPtrStream::position
	int32_t ___position_4;
	// System.Boolean System.Web.IntPtrStream::owns
	bool ___owns_5;

public:
	inline static int32_t get_offset_of_base_address_2() { return static_cast<int32_t>(offsetof(IntPtrStream_t1988557108, ___base_address_2)); }
	inline uint8_t* get_base_address_2() const { return ___base_address_2; }
	inline uint8_t** get_address_of_base_address_2() { return &___base_address_2; }
	inline void set_base_address_2(uint8_t* value)
	{
		___base_address_2 = value;
	}

	inline static int32_t get_offset_of_size_3() { return static_cast<int32_t>(offsetof(IntPtrStream_t1988557108, ___size_3)); }
	inline int32_t get_size_3() const { return ___size_3; }
	inline int32_t* get_address_of_size_3() { return &___size_3; }
	inline void set_size_3(int32_t value)
	{
		___size_3 = value;
	}

	inline static int32_t get_offset_of_position_4() { return static_cast<int32_t>(offsetof(IntPtrStream_t1988557108, ___position_4)); }
	inline int32_t get_position_4() const { return ___position_4; }
	inline int32_t* get_address_of_position_4() { return &___position_4; }
	inline void set_position_4(int32_t value)
	{
		___position_4 = value;
	}

	inline static int32_t get_offset_of_owns_5() { return static_cast<int32_t>(offsetof(IntPtrStream_t1988557108, ___owns_5)); }
	inline bool get_owns_5() const { return ___owns_5; }
	inline bool* get_address_of_owns_5() { return &___owns_5; }
	inline void set_owns_5(bool value)
	{
		___owns_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
