﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"

// System.String
struct String_t;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.FaultContractAttribute
struct  FaultContractAttribute_t3255918006  : public Attribute_t861562559
{
public:
	// System.String System.ServiceModel.FaultContractAttribute::action
	String_t* ___action_0;
	// System.String System.ServiceModel.FaultContractAttribute::name
	String_t* ___name_1;
	// System.String System.ServiceModel.FaultContractAttribute::ns
	String_t* ___ns_2;
	// System.Type System.ServiceModel.FaultContractAttribute::detail_type
	Type_t * ___detail_type_3;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(FaultContractAttribute_t3255918006, ___action_0)); }
	inline String_t* get_action_0() const { return ___action_0; }
	inline String_t** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(String_t* value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier(&___action_0, value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(FaultContractAttribute_t3255918006, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_ns_2() { return static_cast<int32_t>(offsetof(FaultContractAttribute_t3255918006, ___ns_2)); }
	inline String_t* get_ns_2() const { return ___ns_2; }
	inline String_t** get_address_of_ns_2() { return &___ns_2; }
	inline void set_ns_2(String_t* value)
	{
		___ns_2 = value;
		Il2CppCodeGenWriteBarrier(&___ns_2, value);
	}

	inline static int32_t get_offset_of_detail_type_3() { return static_cast<int32_t>(offsetof(FaultContractAttribute_t3255918006, ___detail_type_3)); }
	inline Type_t * get_detail_type_3() const { return ___detail_type_3; }
	inline Type_t ** get_address_of_detail_type_3() { return &___detail_type_3; }
	inline void set_detail_type_3(Type_t * value)
	{
		___detail_type_3 = value;
		Il2CppCodeGenWriteBarrier(&___detail_type_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
