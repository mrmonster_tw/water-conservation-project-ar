﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.CodeDom.CodeCompileUnit
struct CodeCompileUnit_t2527618915;
// System.CodeDom.CodeNamespace
struct CodeNamespace_t2165007136;
// System.CodeDom.CodeTypeDeclaration
struct CodeTypeDeclaration_t2359234283;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.CompileUnitPartialType
struct  CompileUnitPartialType_t3538146433  : public Il2CppObject
{
public:
	// System.CodeDom.CodeCompileUnit System.Web.Compilation.CompileUnitPartialType::Unit
	CodeCompileUnit_t2527618915 * ___Unit_0;
	// System.CodeDom.CodeNamespace System.Web.Compilation.CompileUnitPartialType::ParentNamespace
	CodeNamespace_t2165007136 * ___ParentNamespace_1;
	// System.CodeDom.CodeTypeDeclaration System.Web.Compilation.CompileUnitPartialType::PartialType
	CodeTypeDeclaration_t2359234283 * ___PartialType_2;
	// System.String System.Web.Compilation.CompileUnitPartialType::typeName
	String_t* ___typeName_3;

public:
	inline static int32_t get_offset_of_Unit_0() { return static_cast<int32_t>(offsetof(CompileUnitPartialType_t3538146433, ___Unit_0)); }
	inline CodeCompileUnit_t2527618915 * get_Unit_0() const { return ___Unit_0; }
	inline CodeCompileUnit_t2527618915 ** get_address_of_Unit_0() { return &___Unit_0; }
	inline void set_Unit_0(CodeCompileUnit_t2527618915 * value)
	{
		___Unit_0 = value;
		Il2CppCodeGenWriteBarrier(&___Unit_0, value);
	}

	inline static int32_t get_offset_of_ParentNamespace_1() { return static_cast<int32_t>(offsetof(CompileUnitPartialType_t3538146433, ___ParentNamespace_1)); }
	inline CodeNamespace_t2165007136 * get_ParentNamespace_1() const { return ___ParentNamespace_1; }
	inline CodeNamespace_t2165007136 ** get_address_of_ParentNamespace_1() { return &___ParentNamespace_1; }
	inline void set_ParentNamespace_1(CodeNamespace_t2165007136 * value)
	{
		___ParentNamespace_1 = value;
		Il2CppCodeGenWriteBarrier(&___ParentNamespace_1, value);
	}

	inline static int32_t get_offset_of_PartialType_2() { return static_cast<int32_t>(offsetof(CompileUnitPartialType_t3538146433, ___PartialType_2)); }
	inline CodeTypeDeclaration_t2359234283 * get_PartialType_2() const { return ___PartialType_2; }
	inline CodeTypeDeclaration_t2359234283 ** get_address_of_PartialType_2() { return &___PartialType_2; }
	inline void set_PartialType_2(CodeTypeDeclaration_t2359234283 * value)
	{
		___PartialType_2 = value;
		Il2CppCodeGenWriteBarrier(&___PartialType_2, value);
	}

	inline static int32_t get_offset_of_typeName_3() { return static_cast<int32_t>(offsetof(CompileUnitPartialType_t3538146433, ___typeName_3)); }
	inline String_t* get_typeName_3() const { return ___typeName_3; }
	inline String_t** get_address_of_typeName_3() { return &___typeName_3; }
	inline void set_typeName_3(String_t* value)
	{
		___typeName_3 = value;
		Il2CppCodeGenWriteBarrier(&___typeName_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
