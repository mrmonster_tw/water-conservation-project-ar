﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Web_System_Web_Compilation_DefaultResourceP3181543722.h"
#include "System_Web_System_Web_Compilation_DefaultResourcePr400076996.h"
#include "System_Web_System_Web_Compilation_DefaultResourceP3697749236.h"
#include "System_Web_System_Web_Compilation_Directive1556494119.h"
#include "System_Web_System_Web_Compilation_ExpressionBuilde2932709422.h"
#include "System_Web_System_Web_Compilation_ExpressionBuilder750510412.h"
#include "System_Web_System_Web_Compilation_ExpressionEditor2899909910.h"
#include "System_Web_System_Web_Compilation_ExpressionPrefix2655546262.h"
#include "System_Web_System_Web_Compilation_GlobalAsaxCompil2475863205.h"
#include "System_Web_System_Web_Compilation_Location2966105053.h"
#include "System_Web_System_Web_Compilation_PageThemeCompile4116047890.h"
#include "System_Web_System_Web_Compilation_ParseException2344426112.h"
#include "System_Web_System_Web_Compilation_BuildResultTypeC2541654725.h"
#include "System_Web_System_Web_Compilation_PreservationFile1363716925.h"
#include "System_Web_System_Web_Compilation_ResourceExpressi1959015653.h"
#include "System_Web_System_Web_Compilation_ResourceExpressi4288169951.h"
#include "System_Web_System_Web_Compilation_ResourceProvider2982880633.h"
#include "System_Web_System_Web_Compilation_TagAttributes1165003511.h"
#include "System_Web_System_Web_Compilation_TagType2319653245.h"
#include "System_Web_System_Web_Compilation_TemplateBuildPro1963414078.h"
#include "System_Web_System_Web_Compilation_TemplateBuildPro2024460703.h"
#include "System_Web_System_Web_Compilation_TemplateControlCo645890099.h"
#include "System_Web_System_Web_Compilation_ThemeDirectoryBu2363030985.h"
#include "System_Web_System_Web_Compilation_WsdlBuildProvider589850398.h"
#include "System_Web_System_Web_Configuration_HttpConfigurat4152116388.h"
#include "System_Web_System_Web_Configuration_HandlersUtil2954279244.h"
#include "System_Web_System_Web_Configuration_MachineKeyRegi1766477781.h"
#include "System_Web_System_Web_Configuration_MachineKeyRegi1943640232.h"
#include "System_Web_System_Web_Configuration_MachineKeyValid131925354.h"
#include "System_Web_System_Web_Configuration_ApplicationSett959978309.h"
#include "System_Web_System_Web_Configuration_AssemblyCollec3387909168.h"
#include "System_Web_System_Web_Configuration_AssemblyInfo564158028.h"
#include "System_Web_System_Web_Configuration_BuildProviderCo626561738.h"
#include "System_Web_System_Web_Configuration_BuildProvider4236228096.h"
#include "System_Web_System_Web_Configuration_ClientTarget3650571946.h"
#include "System_Web_System_Web_Configuration_ClientTargetCo2257554110.h"
#include "System_Web_System_Web_Configuration_ClientTargetSec572227672.h"
#include "System_Web_System_Web_Configuration_CodeSubDirecto2687815105.h"
#include "System_Web_System_Web_Configuration_CodeSubDirecto1239547586.h"
#include "System_Web_System_Web_Configuration_CompilationSec1832220892.h"
#include "System_Web_System_Web_Configuration_Compiler682656647.h"
#include "System_Web_System_Web_Configuration_CompilerCollec4292244193.h"
#include "System_Web_System_Web_Configuration_CustomError2661424739.h"
#include "System_Web_System_Web_Configuration_CustomErrorCol2745282543.h"
#include "System_Web_System_Web_Configuration_CustomErrorsMod210069450.h"
#include "System_Web_System_Web_Configuration_CustomErrorsSec402377375.h"
#include "System_Web_System_Web_Configuration_ExpressionBuil1692175560.h"
#include "System_Web_System_Web_Configuration_ExpressionBuil2707979487.h"
#include "System_Web_System_Web_Configuration_GlobalizationSe244366663.h"
#include "System_Web_System_Web_Configuration_HttpConfigurat2615828244.h"
#include "System_Web_System_Web_Configuration_HttpHandlerAct1087594307.h"
#include "System_Web_System_Web_Configuration_HttpHandlerAct3464161669.h"
#include "System_Web_System_Web_Configuration_HttpHandlersSe1168259750.h"
#include "System_Web_System_Web_Configuration_HttpModuleActi1837251843.h"
#include "System_Web_System_Web_Configuration_HttpModuleActio537205479.h"
#include "System_Web_System_Web_Configuration_HttpModulesSec1140007476.h"
#include "System_Web_System_Web_Configuration_HttpRuntimeSect165321267.h"
#include "System_Web_System_Web_Configuration_MachineKeySect3377737715.h"
#include "System_Web_System_Web_Configuration_MachineKeySecti333983424.h"
#include "System_Web_System_Web_Configuration_MachineKeyVali3615775327.h"
#include "System_Web_System_Web_Configuration_MonoSettingsSe2949572421.h"
#include "System_Web_System_Web_Configuration_NamespaceColle3868179418.h"
#include "System_Web_System_Web_Configuration_NamespaceInfo3232851166.h"
#include "System_Web_System_Configuration_NullableStringValid388666549.h"
#include "System_Web_System_Web_Configuration_PagesEnableSes3709987186.h"
#include "System_Web_System_Web_Configuration_PagesSection1064701840.h"
#include "System_Web_System_Web_Configuration_ProfileGroupSe4115401530.h"
#include "System_Web_System_Web_Configuration_ProfileGroupSe1931628525.h"
#include "System_Web_System_Web_Configuration_ProfilePropert2069143144.h"
#include "System_Web_System_Web_Configuration_ProfilePropert3170835156.h"
#include "System_Web_System_Web_Configuration_ProfilePropert2271557816.h"
#include "System_Web_System_Web_Configuration_ProfileSection2357888679.h"
#include "System_Web_System_Web_Configuration_PropertyHelper2869223297.h"
#include "System_Web_System_Web_Configuration_ProvidersHelper721810479.h"
#include "System_Web_System_Web_Configuration_RootProfilePro3703489347.h"
#include "System_Web_System_Web_Configuration_SerializationM1194471064.h"
#include "System_Web_System_Web_Configuration_SessionStateSec500006340.h"
#include "System_Web_System_Web_Configuration_TagMapCollecti1554951808.h"
#include "System_Web_System_Web_Configuration_TagMapInfo470982543.h"
#include "System_Web_System_Web_Configuration_TagPrefixColle2604999621.h"
#include "System_Web_System_Web_Configuration_TagPrefixInfo1180606465.h"
#include "System_Web_System_Web_Configuration_TraceDisplayMo2507778562.h"
#include "System_Web_System_Web_Configuration_TraceSection3447155379.h"
#include "System_Web_System_Web_Configuration_UrlMapping3576390073.h"
#include "System_Web_System_Web_Configuration_UrlMappingColl3845908325.h"
#include "System_Web_System_Web_Configuration_UrlMappingsSect960595399.h"
#include "System_Web_System_Web_Configuration_VirtualDirecto3325528393.h"
#include "System_Web_System_Web_Configuration_VirtualDirecto3264766938.h"
#include "System_Web_System_Web_Configuration_WebApplication1989904319.h"
#include "System_Web_System_Web_Configuration_WebConfiguratio225230154.h"
#include "System_Web_System_Web_Configuration_WebConfiguratio108733871.h"
#include "System_Web_System_Web_Configuration_WebConfigurati2451561378.h"
#include "System_Web_System_Web_Configuration_Web20DefaultCon163121754.h"
#include "System_Web_System_Web_Configuration_WebContext875593705.h"
#include "System_Web_System_Web_Configuration_XhtmlConforman3960522365.h"
#include "System_Web_System_Web_Configuration_XhtmlConforman3023718644.h"
#include "System_Web_System_Web_Handlers_AssemblyResourceLoad761489670.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3200 = { sizeof (DefaultResourceProviderFactory_t3181543722), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3201 = { sizeof (DefaultResourceProvider_t400076996), -1, 0, sizeof(DefaultResourceProvider_t400076996_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable3201[3] = 
{
	THREAD_STATIC_FIELD_OFFSET,
	DefaultResourceProvider_t400076996::get_offset_of_resource_1(),
	DefaultResourceProvider_t400076996::get_offset_of_isGlobal_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3202 = { sizeof (ResourceManagerCacheKey_t3697749236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3202[2] = 
{
	ResourceManagerCacheKey_t3697749236::get_offset_of__name_0(),
	ResourceManagerCacheKey_t3697749236::get_offset_of__asm_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3203 = { sizeof (Directive_t1556494119), -1, sizeof(Directive_t1556494119_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3203[13] = 
{
	Directive_t1556494119_StaticFields::get_offset_of_directivesHash_0(),
	Directive_t1556494119_StaticFields::get_offset_of_page_atts_1(),
	Directive_t1556494119_StaticFields::get_offset_of_control_atts_2(),
	Directive_t1556494119_StaticFields::get_offset_of_import_atts_3(),
	Directive_t1556494119_StaticFields::get_offset_of_implements_atts_4(),
	Directive_t1556494119_StaticFields::get_offset_of_assembly_atts_5(),
	Directive_t1556494119_StaticFields::get_offset_of_register_atts_6(),
	Directive_t1556494119_StaticFields::get_offset_of_outputcache_atts_7(),
	Directive_t1556494119_StaticFields::get_offset_of_reference_atts_8(),
	Directive_t1556494119_StaticFields::get_offset_of_webservice_atts_9(),
	Directive_t1556494119_StaticFields::get_offset_of_application_atts_10(),
	Directive_t1556494119_StaticFields::get_offset_of_mastertype_atts_11(),
	Directive_t1556494119_StaticFields::get_offset_of_previouspagetype_atts_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3204 = { sizeof (ExpressionBuilderContext_t2932709422), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3204[1] = 
{
	ExpressionBuilderContext_t2932709422::get_offset_of_vpath_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3205 = { sizeof (ExpressionBuilder_t750510412), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3206 = { sizeof (ExpressionEditorAttribute_t2899909910), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3206[1] = 
{
	ExpressionEditorAttribute_t2899909910::get_offset_of_typeName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3207 = { sizeof (ExpressionPrefixAttribute_t2655546262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3207[1] = 
{
	ExpressionPrefixAttribute_t2655546262::get_offset_of_expressionPrefix_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3208 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3208[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3209 = { sizeof (GlobalAsaxCompiler_t2475863205), -1, sizeof(GlobalAsaxCompiler_t2475863205_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3209[3] = 
{
	GlobalAsaxCompiler_t2475863205::get_offset_of_parser_12(),
	GlobalAsaxCompiler_t2475863205_StaticFields::get_offset_of_applicationObjectTags_13(),
	GlobalAsaxCompiler_t2475863205_StaticFields::get_offset_of_sessionObjectTags_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3210 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3211 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3212 = { sizeof (Location_t2966105053), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3212[7] = 
{
	Location_t2966105053::get_offset_of_beginLine_0(),
	Location_t2966105053::get_offset_of_endLine_1(),
	Location_t2966105053::get_offset_of_beginColumn_2(),
	Location_t2966105053::get_offset_of_endColumn_3(),
	Location_t2966105053::get_offset_of_fileName_4(),
	Location_t2966105053::get_offset_of_plainText_5(),
	Location_t2966105053::get_offset_of_location_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3213 = { sizeof (PageThemeCompiler_t4116047890), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3213[1] = 
{
	PageThemeCompiler_t4116047890::get_offset_of_parser_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3214 = { sizeof (ParseException_t2344426112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3214[2] = 
{
	ParseException_t2344426112::get_offset_of_location_14(),
	ParseException_t2344426112::get_offset_of_fileText_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3215 = { sizeof (BuildResultTypeCode_t2541654725)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3215[8] = 
{
	BuildResultTypeCode_t2541654725::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3216 = { sizeof (PreservationFile_t1363716925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3216[8] = 
{
	PreservationFile_t1363716925::get_offset_of__filePath_0(),
	PreservationFile_t1363716925::get_offset_of__assembly_1(),
	PreservationFile_t1363716925::get_offset_of__fileHash_2(),
	PreservationFile_t1363716925::get_offset_of__flags_3(),
	PreservationFile_t1363716925::get_offset_of__hash_4(),
	PreservationFile_t1363716925::get_offset_of__resultType_5(),
	PreservationFile_t1363716925::get_offset_of__virtualPath_6(),
	PreservationFile_t1363716925::get_offset_of__filedeps_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3217 = { sizeof (ResourceExpressionBuilder_t1959015653), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3218 = { sizeof (ResourceExpressionFields_t4288169951), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3218[2] = 
{
	ResourceExpressionFields_t4288169951::get_offset_of_classKey_0(),
	ResourceExpressionFields_t4288169951::get_offset_of_resourceKey_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3219 = { sizeof (ResourceProviderFactory_t2982880633), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3220 = { sizeof (TagAttributes_t1165003511), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3220[5] = 
{
	TagAttributes_t1165003511::get_offset_of_atts_hash_0(),
	TagAttributes_t1165003511::get_offset_of_tmp_hash_1(),
	TagAttributes_t1165003511::get_offset_of_keys_2(),
	TagAttributes_t1165003511::get_offset_of_values_3(),
	TagAttributes_t1165003511::get_offset_of_got_hashed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3221 = { sizeof (TagType_t2319653245)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3221[11] = 
{
	TagType_t2319653245::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3222 = { sizeof (TemplateBuildProvider_t1963414078), -1, sizeof(TemplateBuildProvider_t1963414078_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3222[4] = 
{
	TemplateBuildProvider_t1963414078_StaticFields::get_offset_of_directiveAttributes_9(),
	TemplateBuildProvider_t1963414078_StaticFields::get_offset_of_directiveValueTrimChars_10(),
	TemplateBuildProvider_t1963414078::get_offset_of_dependencies_11(),
	TemplateBuildProvider_t1963414078::get_offset_of_compilationLanguage_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3223 = { sizeof (ExtractDirectiveDependencies_t2024460703), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3224 = { sizeof (TemplateControlCompiler_t645890099), -1, sizeof(TemplateControlCompiler_t645890099_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3224[12] = 
{
	TemplateControlCompiler_t645890099_StaticFields::get_offset_of_noCaseFlags_12(),
	TemplateControlCompiler_t645890099_StaticFields::get_offset_of_monoTypeType_13(),
	TemplateControlCompiler_t645890099::get_offset_of_parser_14(),
	TemplateControlCompiler_t645890099::get_offset_of_dataBoundAtts_15(),
	TemplateControlCompiler_t645890099::get_offset_of_currentLocation_16(),
	TemplateControlCompiler_t645890099_StaticFields::get_offset_of_colorConverter_17(),
	TemplateControlCompiler_t645890099_StaticFields::get_offset_of_ctrlVar_18(),
	TemplateControlCompiler_t645890099::get_offset_of_masterPageContentPlaceHolders_19(),
	TemplateControlCompiler_t645890099_StaticFields::get_offset_of_startsWithBindRegex_20(),
	TemplateControlCompiler_t645890099_StaticFields::get_offset_of_bindRegex_21(),
	TemplateControlCompiler_t645890099_StaticFields::get_offset_of_bindRegexInValue_22(),
	TemplateControlCompiler_t645890099_StaticFields::get_offset_of_evalRegexInValue_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3225 = { sizeof (ThemeDirectoryBuildProvider_t2363030985), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3226 = { sizeof (WsdlBuildProvider_t589850398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3226[1] = 
{
	WsdlBuildProvider_t589850398::get_offset_of__compilerType_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3227 = { sizeof (HttpConfigurationContext_t4152116388), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3227[1] = 
{
	HttpConfigurationContext_t4152116388::get_offset_of_virtualPath_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3228 = { sizeof (HandlersUtil_t2954279244), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3229 = { sizeof (MachineKeyRegistryStorage_t1766477781), -1, sizeof(MachineKeyRegistryStorage_t1766477781_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3229[2] = 
{
	MachineKeyRegistryStorage_t1766477781_StaticFields::get_offset_of_keyEncryption_0(),
	MachineKeyRegistryStorage_t1766477781_StaticFields::get_offset_of_keyValidation_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3230 = { sizeof (KeyType_t1943640232)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3230[3] = 
{
	KeyType_t1943640232::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3231 = { sizeof (MachineKeyValidation_t131925354)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3231[5] = 
{
	MachineKeyValidation_t131925354::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3232 = { sizeof (ApplicationSettingsConfigurationFileMap_t959978309), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3233 = { sizeof (AssemblyCollection_t3387909168), -1, sizeof(AssemblyCollection_t3387909168_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3233[1] = 
{
	AssemblyCollection_t3387909168_StaticFields::get_offset_of_properties_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3234 = { sizeof (AssemblyInfo_t564158028), -1, sizeof(AssemblyInfo_t564158028_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3234[2] = 
{
	AssemblyInfo_t564158028_StaticFields::get_offset_of_properties_13(),
	AssemblyInfo_t564158028_StaticFields::get_offset_of_assemblyProp_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3235 = { sizeof (BuildProviderCollection_t626561738), -1, sizeof(BuildProviderCollection_t626561738_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3235[1] = 
{
	BuildProviderCollection_t626561738_StaticFields::get_offset_of_props_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3236 = { sizeof (BuildProvider_t4236228096), -1, sizeof(BuildProvider_t4236228096_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3236[3] = 
{
	BuildProvider_t4236228096_StaticFields::get_offset_of_extensionProp_13(),
	BuildProvider_t4236228096_StaticFields::get_offset_of_typeProp_14(),
	BuildProvider_t4236228096_StaticFields::get_offset_of_properties_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3237 = { sizeof (ClientTarget_t3650571946), -1, sizeof(ClientTarget_t3650571946_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3237[3] = 
{
	ClientTarget_t3650571946_StaticFields::get_offset_of_aliasProp_13(),
	ClientTarget_t3650571946_StaticFields::get_offset_of_userAgentProp_14(),
	ClientTarget_t3650571946_StaticFields::get_offset_of_properties_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3238 = { sizeof (ClientTargetCollection_t2257554110), -1, sizeof(ClientTargetCollection_t2257554110_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3238[1] = 
{
	ClientTargetCollection_t2257554110_StaticFields::get_offset_of_properties_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3239 = { sizeof (ClientTargetSection_t572227672), -1, sizeof(ClientTargetSection_t572227672_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3239[2] = 
{
	ClientTargetSection_t572227672_StaticFields::get_offset_of_clientTargetsProp_17(),
	ClientTargetSection_t572227672_StaticFields::get_offset_of_properties_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3240 = { sizeof (CodeSubDirectoriesCollection_t2687815105), -1, sizeof(CodeSubDirectoriesCollection_t2687815105_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3240[1] = 
{
	CodeSubDirectoriesCollection_t2687815105_StaticFields::get_offset_of_props_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3241 = { sizeof (CodeSubDirectory_t1239547586), -1, sizeof(CodeSubDirectory_t1239547586_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3241[2] = 
{
	CodeSubDirectory_t1239547586_StaticFields::get_offset_of_directoryNameProp_13(),
	CodeSubDirectory_t1239547586_StaticFields::get_offset_of_properties_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3242 = { sizeof (CompilationSection_t1832220892), -1, sizeof(CompilationSection_t1832220892_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3242[19] = 
{
	CompilationSection_t1832220892_StaticFields::get_offset_of_properties_17(),
	CompilationSection_t1832220892_StaticFields::get_offset_of_compilersProp_18(),
	CompilationSection_t1832220892_StaticFields::get_offset_of_tempDirectoryProp_19(),
	CompilationSection_t1832220892_StaticFields::get_offset_of_debugProp_20(),
	CompilationSection_t1832220892_StaticFields::get_offset_of_strictProp_21(),
	CompilationSection_t1832220892_StaticFields::get_offset_of_explicitProp_22(),
	CompilationSection_t1832220892_StaticFields::get_offset_of_batchProp_23(),
	CompilationSection_t1832220892_StaticFields::get_offset_of_batchTimeoutProp_24(),
	CompilationSection_t1832220892_StaticFields::get_offset_of_maxBatchSizeProp_25(),
	CompilationSection_t1832220892_StaticFields::get_offset_of_maxBatchGeneratedFileSizeProp_26(),
	CompilationSection_t1832220892_StaticFields::get_offset_of_numRecompilesBeforeAppRestartProp_27(),
	CompilationSection_t1832220892_StaticFields::get_offset_of_defaultLanguageProp_28(),
	CompilationSection_t1832220892_StaticFields::get_offset_of_assembliesProp_29(),
	CompilationSection_t1832220892_StaticFields::get_offset_of_assemblyPostProcessorTypeProp_30(),
	CompilationSection_t1832220892_StaticFields::get_offset_of_buildProvidersProp_31(),
	CompilationSection_t1832220892_StaticFields::get_offset_of_expressionBuildersProp_32(),
	CompilationSection_t1832220892_StaticFields::get_offset_of_urlLinePragmasProp_33(),
	CompilationSection_t1832220892_StaticFields::get_offset_of_codeSubDirectoriesProp_34(),
	CompilationSection_t1832220892_StaticFields::get_offset_of_optimizeCompilationsProp_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3243 = { sizeof (Compiler_t682656647), -1, sizeof(Compiler_t682656647_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3243[6] = 
{
	Compiler_t682656647_StaticFields::get_offset_of_compilerOptionsProp_13(),
	Compiler_t682656647_StaticFields::get_offset_of_extensionProp_14(),
	Compiler_t682656647_StaticFields::get_offset_of_languageProp_15(),
	Compiler_t682656647_StaticFields::get_offset_of_typeProp_16(),
	Compiler_t682656647_StaticFields::get_offset_of_warningLevelProp_17(),
	Compiler_t682656647_StaticFields::get_offset_of_properties_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3244 = { sizeof (CompilerCollection_t4292244193), -1, sizeof(CompilerCollection_t4292244193_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3244[1] = 
{
	CompilerCollection_t4292244193_StaticFields::get_offset_of_properties_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3245 = { sizeof (CustomError_t2661424739), -1, sizeof(CustomError_t2661424739_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3245[3] = 
{
	CustomError_t2661424739_StaticFields::get_offset_of_redirectProp_13(),
	CustomError_t2661424739_StaticFields::get_offset_of_statusCodeProp_14(),
	CustomError_t2661424739_StaticFields::get_offset_of_properties_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3246 = { sizeof (CustomErrorCollection_t2745282543), -1, sizeof(CustomErrorCollection_t2745282543_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3246[1] = 
{
	CustomErrorCollection_t2745282543_StaticFields::get_offset_of_properties_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3247 = { sizeof (CustomErrorsMode_t210069450)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3247[4] = 
{
	CustomErrorsMode_t210069450::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3248 = { sizeof (CustomErrorsSection_t402377375), -1, sizeof(CustomErrorsSection_t402377375_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3248[4] = 
{
	CustomErrorsSection_t402377375_StaticFields::get_offset_of_defaultRedirectProp_17(),
	CustomErrorsSection_t402377375_StaticFields::get_offset_of_errorsProp_18(),
	CustomErrorsSection_t402377375_StaticFields::get_offset_of_modeProp_19(),
	CustomErrorsSection_t402377375_StaticFields::get_offset_of_properties_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3249 = { sizeof (ExpressionBuilderCollection_t1692175560), -1, sizeof(ExpressionBuilderCollection_t1692175560_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3249[1] = 
{
	ExpressionBuilderCollection_t1692175560_StaticFields::get_offset_of_props_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3250 = { sizeof (ExpressionBuilder_t2707979487), -1, sizeof(ExpressionBuilder_t2707979487_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3250[3] = 
{
	ExpressionBuilder_t2707979487_StaticFields::get_offset_of_expressionPrefixProp_13(),
	ExpressionBuilder_t2707979487_StaticFields::get_offset_of_typeProp_14(),
	ExpressionBuilder_t2707979487_StaticFields::get_offset_of_properties_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3251 = { sizeof (GlobalizationSection_t244366663), -1, sizeof(GlobalizationSection_t244366663_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3251[23] = 
{
	GlobalizationSection_t244366663_StaticFields::get_offset_of_cultureProp_17(),
	GlobalizationSection_t244366663_StaticFields::get_offset_of_enableBestFitResponseEncodingProp_18(),
	GlobalizationSection_t244366663_StaticFields::get_offset_of_enableClientBasedCultureProp_19(),
	GlobalizationSection_t244366663_StaticFields::get_offset_of_fileEncodingProp_20(),
	GlobalizationSection_t244366663_StaticFields::get_offset_of_requestEncodingProp_21(),
	GlobalizationSection_t244366663_StaticFields::get_offset_of_resourceProviderFactoryTypeProp_22(),
	GlobalizationSection_t244366663_StaticFields::get_offset_of_responseEncodingProp_23(),
	GlobalizationSection_t244366663_StaticFields::get_offset_of_responseHeaderEncodingProp_24(),
	GlobalizationSection_t244366663_StaticFields::get_offset_of_uiCultureProp_25(),
	GlobalizationSection_t244366663_StaticFields::get_offset_of_properties_26(),
	GlobalizationSection_t244366663::get_offset_of_cached_fileencoding_27(),
	GlobalizationSection_t244366663::get_offset_of_cached_requestencoding_28(),
	GlobalizationSection_t244366663::get_offset_of_cached_responseencoding_29(),
	GlobalizationSection_t244366663::get_offset_of_encodingHash_30(),
	GlobalizationSection_t244366663::get_offset_of_cached_culture_31(),
	GlobalizationSection_t244366663::get_offset_of_cached_cultureinfo_32(),
	GlobalizationSection_t244366663::get_offset_of_cached_uiculture_33(),
	GlobalizationSection_t244366663::get_offset_of_cached_uicultureinfo_34(),
	GlobalizationSection_t244366663_StaticFields::get_offset_of_encoding_warning_35(),
	GlobalizationSection_t244366663_StaticFields::get_offset_of_culture_warning_36(),
	GlobalizationSection_t244366663::get_offset_of_autoCulture_37(),
	GlobalizationSection_t244366663::get_offset_of_autoUICulture_38(),
	GlobalizationSection_t244366663_StaticFields::get_offset_of_U3CU3Ef__switchU24mapA_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3252 = { sizeof (HttpConfigurationSystem_t2615828244), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3253 = { sizeof (HttpHandlerAction_t1087594307), -1, sizeof(HttpHandlerAction_t1087594307_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3253[9] = 
{
	HttpHandlerAction_t1087594307_StaticFields::get_offset_of__properties_13(),
	HttpHandlerAction_t1087594307_StaticFields::get_offset_of_pathProp_14(),
	HttpHandlerAction_t1087594307_StaticFields::get_offset_of_typeProp_15(),
	HttpHandlerAction_t1087594307_StaticFields::get_offset_of_validateProp_16(),
	HttpHandlerAction_t1087594307_StaticFields::get_offset_of_verbProp_17(),
	HttpHandlerAction_t1087594307::get_offset_of_instance_18(),
	HttpHandlerAction_t1087594307::get_offset_of_type_19(),
	HttpHandlerAction_t1087594307::get_offset_of_cached_verb_20(),
	HttpHandlerAction_t1087594307::get_offset_of_cached_verbs_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3254 = { sizeof (HttpHandlerActionCollection_t3464161669), -1, sizeof(HttpHandlerActionCollection_t3464161669_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3254[1] = 
{
	HttpHandlerActionCollection_t3464161669_StaticFields::get_offset_of_properties_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3255 = { sizeof (HttpHandlersSection_t1168259750), -1, sizeof(HttpHandlersSection_t1168259750_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3255[2] = 
{
	HttpHandlersSection_t1168259750_StaticFields::get_offset_of_properties_17(),
	HttpHandlersSection_t1168259750_StaticFields::get_offset_of_handlersProp_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3256 = { sizeof (HttpModuleAction_t1837251843), -1, sizeof(HttpModuleAction_t1837251843_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3256[4] = 
{
	HttpModuleAction_t1837251843_StaticFields::get_offset_of_properties_13(),
	HttpModuleAction_t1837251843_StaticFields::get_offset_of_nameProp_14(),
	HttpModuleAction_t1837251843_StaticFields::get_offset_of_typeProp_15(),
	HttpModuleAction_t1837251843_StaticFields::get_offset_of_elementProperty_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3257 = { sizeof (HttpModuleActionCollection_t537205479), -1, sizeof(HttpModuleActionCollection_t537205479_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3257[1] = 
{
	HttpModuleActionCollection_t537205479_StaticFields::get_offset_of_properties_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3258 = { sizeof (HttpModulesSection_t1140007476), -1, sizeof(HttpModulesSection_t1140007476_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3258[2] = 
{
	HttpModulesSection_t1140007476_StaticFields::get_offset_of_properties_17(),
	HttpModulesSection_t1140007476_StaticFields::get_offset_of_modulesProp_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3259 = { sizeof (HttpRuntimeSection_t165321267), -1, sizeof(HttpRuntimeSection_t165321267_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3259[19] = 
{
	HttpRuntimeSection_t165321267_StaticFields::get_offset_of_apartmentThreadingProp_17(),
	HttpRuntimeSection_t165321267_StaticFields::get_offset_of_appRequestQueueLimitProp_18(),
	HttpRuntimeSection_t165321267_StaticFields::get_offset_of_delayNotificationTimeoutProp_19(),
	HttpRuntimeSection_t165321267_StaticFields::get_offset_of_enableProp_20(),
	HttpRuntimeSection_t165321267_StaticFields::get_offset_of_enableHeaderCheckingProp_21(),
	HttpRuntimeSection_t165321267_StaticFields::get_offset_of_enableKernelOutputCacheProp_22(),
	HttpRuntimeSection_t165321267_StaticFields::get_offset_of_enableVersionHeaderProp_23(),
	HttpRuntimeSection_t165321267_StaticFields::get_offset_of_executionTimeoutProp_24(),
	HttpRuntimeSection_t165321267_StaticFields::get_offset_of_maxRequestLengthProp_25(),
	HttpRuntimeSection_t165321267_StaticFields::get_offset_of_maxWaitChangeNotificationProp_26(),
	HttpRuntimeSection_t165321267_StaticFields::get_offset_of_minFreeThreadsProp_27(),
	HttpRuntimeSection_t165321267_StaticFields::get_offset_of_minLocalRequestFreeThreadsProp_28(),
	HttpRuntimeSection_t165321267_StaticFields::get_offset_of_requestLengthDiskThresholdProp_29(),
	HttpRuntimeSection_t165321267_StaticFields::get_offset_of_requireRootedSaveAsPathProp_30(),
	HttpRuntimeSection_t165321267_StaticFields::get_offset_of_sendCacheControlHeaderProp_31(),
	HttpRuntimeSection_t165321267_StaticFields::get_offset_of_shutdownTimeoutProp_32(),
	HttpRuntimeSection_t165321267_StaticFields::get_offset_of_useFullyQualifiedRedirectUrlProp_33(),
	HttpRuntimeSection_t165321267_StaticFields::get_offset_of_waitChangeNotificationProp_34(),
	HttpRuntimeSection_t165321267_StaticFields::get_offset_of_properties_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3260 = { sizeof (MachineKeySection_t3377737715), -1, sizeof(MachineKeySection_t3377737715_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3260[5] = 
{
	MachineKeySection_t3377737715_StaticFields::get_offset_of_decryptionProp_17(),
	MachineKeySection_t3377737715_StaticFields::get_offset_of_decryptionKeyProp_18(),
	MachineKeySection_t3377737715_StaticFields::get_offset_of_validationProp_19(),
	MachineKeySection_t3377737715_StaticFields::get_offset_of_validationKeyProp_20(),
	MachineKeySection_t3377737715_StaticFields::get_offset_of_properties_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3261 = { sizeof (MachineKeySectionUtils_t333983424), -1, sizeof(MachineKeySectionUtils_t333983424_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3261[5] = 
{
	MachineKeySectionUtils_t333983424_StaticFields::get_offset_of_autogenerated_0(),
	MachineKeySectionUtils_t333983424_StaticFields::get_offset_of_autogenerated_decrypt_1(),
	MachineKeySectionUtils_t333983424_StaticFields::get_offset_of_decryption_key_2(),
	MachineKeySectionUtils_t333983424_StaticFields::get_offset_of_decryption_key_192bits_3(),
	MachineKeySectionUtils_t333983424_StaticFields::get_offset_of_validation_key_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3262 = { sizeof (MachineKeyValidationConverter_t3615775327), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3263 = { sizeof (MonoSettingsSection_t2949572421), -1, sizeof(MonoSettingsSection_t2949572421_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3263[4] = 
{
	MonoSettingsSection_t2949572421_StaticFields::get_offset_of_properties_17(),
	MonoSettingsSection_t2949572421_StaticFields::get_offset_of_compilersCompatibilityProp_18(),
	MonoSettingsSection_t2949572421_StaticFields::get_offset_of_useCompilersCompatibilityProp_19(),
	MonoSettingsSection_t2949572421_StaticFields::get_offset_of_verificationCompatibilityProp_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3264 = { sizeof (NamespaceCollection_t3868179418), -1, sizeof(NamespaceCollection_t3868179418_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3264[2] = 
{
	NamespaceCollection_t3868179418_StaticFields::get_offset_of_properties_23(),
	NamespaceCollection_t3868179418_StaticFields::get_offset_of_autoImportVBNamespaceProp_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3265 = { sizeof (NamespaceInfo_t3232851166), -1, sizeof(NamespaceInfo_t3232851166_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3265[2] = 
{
	NamespaceInfo_t3232851166_StaticFields::get_offset_of_properties_13(),
	NamespaceInfo_t3232851166_StaticFields::get_offset_of_namespaceProp_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3266 = { sizeof (NullableStringValidator_t388666549), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3266[3] = 
{
	NullableStringValidator_t388666549::get_offset_of_invalidCharacters_0(),
	NullableStringValidator_t388666549::get_offset_of_maxLength_1(),
	NullableStringValidator_t388666549::get_offset_of_minLength_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3267 = { sizeof (PagesEnableSessionState_t3709987186)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3267[4] = 
{
	PagesEnableSessionState_t3709987186::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3268 = { sizeof (PagesSection_t1064701840), -1, sizeof(PagesSection_t1064701840_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3268[24] = 
{
	PagesSection_t1064701840_StaticFields::get_offset_of_properties_17(),
	PagesSection_t1064701840_StaticFields::get_offset_of_asyncTimeoutProp_18(),
	PagesSection_t1064701840_StaticFields::get_offset_of_autoEventWireupProp_19(),
	PagesSection_t1064701840_StaticFields::get_offset_of_bufferProp_20(),
	PagesSection_t1064701840_StaticFields::get_offset_of_controlsProp_21(),
	PagesSection_t1064701840_StaticFields::get_offset_of_enableEventValidationProp_22(),
	PagesSection_t1064701840_StaticFields::get_offset_of_enableSessionStateProp_23(),
	PagesSection_t1064701840_StaticFields::get_offset_of_enableViewStateProp_24(),
	PagesSection_t1064701840_StaticFields::get_offset_of_enableViewStateMacProp_25(),
	PagesSection_t1064701840_StaticFields::get_offset_of_maintainScrollPositionOnPostBackProp_26(),
	PagesSection_t1064701840_StaticFields::get_offset_of_masterPageFileProp_27(),
	PagesSection_t1064701840_StaticFields::get_offset_of_maxPageStateFieldLengthProp_28(),
	PagesSection_t1064701840_StaticFields::get_offset_of_modeProp_29(),
	PagesSection_t1064701840_StaticFields::get_offset_of_namespacesProp_30(),
	PagesSection_t1064701840_StaticFields::get_offset_of_pageBaseTypeProp_31(),
	PagesSection_t1064701840_StaticFields::get_offset_of_pageParserFilterTypeProp_32(),
	PagesSection_t1064701840_StaticFields::get_offset_of_smartNavigationProp_33(),
	PagesSection_t1064701840_StaticFields::get_offset_of_styleSheetThemeProp_34(),
	PagesSection_t1064701840_StaticFields::get_offset_of_tagMappingProp_35(),
	PagesSection_t1064701840_StaticFields::get_offset_of_themeProp_36(),
	PagesSection_t1064701840_StaticFields::get_offset_of_userControlBaseTypeProp_37(),
	PagesSection_t1064701840_StaticFields::get_offset_of_validateRequestProp_38(),
	PagesSection_t1064701840_StaticFields::get_offset_of_viewStateEncryptionModeProp_39(),
	PagesSection_t1064701840_StaticFields::get_offset_of_U3CU3Ef__switchU24mapB_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3269 = { sizeof (ProfileGroupSettings_t4115401530), -1, sizeof(ProfileGroupSettings_t4115401530_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3269[3] = 
{
	ProfileGroupSettings_t4115401530_StaticFields::get_offset_of_propertySettingsProp_13(),
	ProfileGroupSettings_t4115401530_StaticFields::get_offset_of_nameProp_14(),
	ProfileGroupSettings_t4115401530_StaticFields::get_offset_of_properties_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3270 = { sizeof (ProfileGroupSettingsCollection_t1931628525), -1, sizeof(ProfileGroupSettingsCollection_t1931628525_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3270[1] = 
{
	ProfileGroupSettingsCollection_t1931628525_StaticFields::get_offset_of_properties_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3271 = { sizeof (ProfilePropertyNameValidator_t2069143144), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3272 = { sizeof (ProfilePropertySettings_t3170835156), -1, sizeof(ProfilePropertySettings_t3170835156_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3272[9] = 
{
	ProfilePropertySettings_t3170835156_StaticFields::get_offset_of_allowAnonymousProp_13(),
	ProfilePropertySettings_t3170835156_StaticFields::get_offset_of_customProviderDataProp_14(),
	ProfilePropertySettings_t3170835156_StaticFields::get_offset_of_defaultValueProp_15(),
	ProfilePropertySettings_t3170835156_StaticFields::get_offset_of_nameProp_16(),
	ProfilePropertySettings_t3170835156_StaticFields::get_offset_of_providerProp_17(),
	ProfilePropertySettings_t3170835156_StaticFields::get_offset_of_readOnlyProp_18(),
	ProfilePropertySettings_t3170835156_StaticFields::get_offset_of_serializeAsProp_19(),
	ProfilePropertySettings_t3170835156_StaticFields::get_offset_of_typeProp_20(),
	ProfilePropertySettings_t3170835156_StaticFields::get_offset_of_properties_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3273 = { sizeof (ProfilePropertySettingsCollection_t2271557816), -1, sizeof(ProfilePropertySettingsCollection_t2271557816_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3273[1] = 
{
	ProfilePropertySettingsCollection_t2271557816_StaticFields::get_offset_of_properties_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3274 = { sizeof (ProfileSection_t2357888679), -1, sizeof(ProfileSection_t2357888679_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3274[7] = 
{
	ProfileSection_t2357888679_StaticFields::get_offset_of_automaticSaveEnabledProp_17(),
	ProfileSection_t2357888679_StaticFields::get_offset_of_defaultProviderProp_18(),
	ProfileSection_t2357888679_StaticFields::get_offset_of_enabledProp_19(),
	ProfileSection_t2357888679_StaticFields::get_offset_of_inheritsProp_20(),
	ProfileSection_t2357888679_StaticFields::get_offset_of_propertySettingsProp_21(),
	ProfileSection_t2357888679_StaticFields::get_offset_of_providersProp_22(),
	ProfileSection_t2357888679_StaticFields::get_offset_of_properties_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3275 = { sizeof (PropertyHelper_t2869223297), -1, sizeof(PropertyHelper_t2869223297_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3275[13] = 
{
	PropertyHelper_t2869223297_StaticFields::get_offset_of_WhiteSpaceTrimStringConverter_0(),
	PropertyHelper_t2869223297_StaticFields::get_offset_of_InfiniteTimeSpanConverter_1(),
	PropertyHelper_t2869223297_StaticFields::get_offset_of_InfiniteIntConverter_2(),
	PropertyHelper_t2869223297_StaticFields::get_offset_of_TimeSpanMinutesConverter_3(),
	PropertyHelper_t2869223297_StaticFields::get_offset_of_TimeSpanSecondsOrInfiniteConverter_4(),
	PropertyHelper_t2869223297_StaticFields::get_offset_of_TimeSpanSecondsConverter_5(),
	PropertyHelper_t2869223297_StaticFields::get_offset_of_CommaDelimitedStringCollectionConverter_6(),
	PropertyHelper_t2869223297_StaticFields::get_offset_of_DefaultValidator_7(),
	PropertyHelper_t2869223297_StaticFields::get_offset_of_NonEmptyStringValidator_8(),
	PropertyHelper_t2869223297_StaticFields::get_offset_of_PositiveTimeSpanValidator_9(),
	PropertyHelper_t2869223297_StaticFields::get_offset_of_TimeSpanMinutesOrInfiniteConverter_10(),
	PropertyHelper_t2869223297_StaticFields::get_offset_of_IntFromZeroToMaxValidator_11(),
	PropertyHelper_t2869223297_StaticFields::get_offset_of_IntFromOneToMax_1Validator_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3276 = { sizeof (ProvidersHelper_t721810479), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3277 = { sizeof (RootProfilePropertySettingsCollection_t3703489347), -1, sizeof(RootProfilePropertySettingsCollection_t3703489347_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3277[2] = 
{
	RootProfilePropertySettingsCollection_t3703489347_StaticFields::get_offset_of_properties_24(),
	RootProfilePropertySettingsCollection_t3703489347::get_offset_of_groupSettings_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3278 = { sizeof (SerializationMode_t1194471064)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3278[5] = 
{
	SerializationMode_t1194471064::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3279 = { sizeof (SessionStateSection_t500006340), -1, sizeof(SessionStateSection_t500006340_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3279[17] = 
{
	SessionStateSection_t500006340_StaticFields::get_offset_of_allowCustomSqlDatabaseProp_17(),
	SessionStateSection_t500006340_StaticFields::get_offset_of_cookielessProp_18(),
	SessionStateSection_t500006340_StaticFields::get_offset_of_cookieNameProp_19(),
	SessionStateSection_t500006340_StaticFields::get_offset_of_customProviderProp_20(),
	SessionStateSection_t500006340_StaticFields::get_offset_of_modeProp_21(),
	SessionStateSection_t500006340_StaticFields::get_offset_of_partitionResolverTypeProp_22(),
	SessionStateSection_t500006340_StaticFields::get_offset_of_providersProp_23(),
	SessionStateSection_t500006340_StaticFields::get_offset_of_regenerateExpiredSessionIdProp_24(),
	SessionStateSection_t500006340_StaticFields::get_offset_of_sessionIDManagerTypeProp_25(),
	SessionStateSection_t500006340_StaticFields::get_offset_of_sqlCommandTimeoutProp_26(),
	SessionStateSection_t500006340_StaticFields::get_offset_of_sqlConnectionStringProp_27(),
	SessionStateSection_t500006340_StaticFields::get_offset_of_stateConnectionStringProp_28(),
	SessionStateSection_t500006340_StaticFields::get_offset_of_stateNetworkTimeoutProp_29(),
	SessionStateSection_t500006340_StaticFields::get_offset_of_timeoutProp_30(),
	SessionStateSection_t500006340_StaticFields::get_offset_of_useHostingIdentityProp_31(),
	SessionStateSection_t500006340_StaticFields::get_offset_of_properties_32(),
	SessionStateSection_t500006340_StaticFields::get_offset_of_elementProperty_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3280 = { sizeof (TagMapCollection_t1554951808), -1, sizeof(TagMapCollection_t1554951808_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3280[1] = 
{
	TagMapCollection_t1554951808_StaticFields::get_offset_of_properties_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3281 = { sizeof (TagMapInfo_t470982543), -1, sizeof(TagMapInfo_t470982543_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3281[3] = 
{
	TagMapInfo_t470982543_StaticFields::get_offset_of_properties_13(),
	TagMapInfo_t470982543_StaticFields::get_offset_of_mappedTagTypeProp_14(),
	TagMapInfo_t470982543_StaticFields::get_offset_of_tagTypeProp_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3282 = { sizeof (TagPrefixCollection_t2604999621), -1, sizeof(TagPrefixCollection_t2604999621_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3282[1] = 
{
	TagPrefixCollection_t2604999621_StaticFields::get_offset_of_properties_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3283 = { sizeof (TagPrefixInfo_t1180606465), -1, sizeof(TagPrefixInfo_t1180606465_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3283[7] = 
{
	TagPrefixInfo_t1180606465_StaticFields::get_offset_of_properties_13(),
	TagPrefixInfo_t1180606465_StaticFields::get_offset_of_tagPrefixProp_14(),
	TagPrefixInfo_t1180606465_StaticFields::get_offset_of_namespaceProp_15(),
	TagPrefixInfo_t1180606465_StaticFields::get_offset_of_assemblyProp_16(),
	TagPrefixInfo_t1180606465_StaticFields::get_offset_of_tagNameProp_17(),
	TagPrefixInfo_t1180606465_StaticFields::get_offset_of_sourceProp_18(),
	TagPrefixInfo_t1180606465_StaticFields::get_offset_of_elementProperty_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3284 = { sizeof (TraceDisplayMode_t2507778562)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3284[3] = 
{
	TraceDisplayMode_t2507778562::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3285 = { sizeof (TraceSection_t3447155379), -1, sizeof(TraceSection_t3447155379_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3285[8] = 
{
	TraceSection_t3447155379_StaticFields::get_offset_of_enabledProp_17(),
	TraceSection_t3447155379_StaticFields::get_offset_of_localOnlyProp_18(),
	TraceSection_t3447155379_StaticFields::get_offset_of_mostRecentProp_19(),
	TraceSection_t3447155379_StaticFields::get_offset_of_pageOutputProp_20(),
	TraceSection_t3447155379_StaticFields::get_offset_of_requestLimitProp_21(),
	TraceSection_t3447155379_StaticFields::get_offset_of_traceModeProp_22(),
	TraceSection_t3447155379_StaticFields::get_offset_of_writeToDiagnosticsTraceProp_23(),
	TraceSection_t3447155379_StaticFields::get_offset_of_properties_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3286 = { sizeof (UrlMapping_t3576390073), -1, sizeof(UrlMapping_t3576390073_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3286[3] = 
{
	UrlMapping_t3576390073_StaticFields::get_offset_of_mappedUrlProp_13(),
	UrlMapping_t3576390073_StaticFields::get_offset_of_urlProp_14(),
	UrlMapping_t3576390073_StaticFields::get_offset_of_properties_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3287 = { sizeof (UrlMappingCollection_t3845908325), -1, sizeof(UrlMappingCollection_t3845908325_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3287[1] = 
{
	UrlMappingCollection_t3845908325_StaticFields::get_offset_of_properties_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3288 = { sizeof (UrlMappingsSection_t960595399), -1, sizeof(UrlMappingsSection_t960595399_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3288[3] = 
{
	UrlMappingsSection_t960595399_StaticFields::get_offset_of_enabledProp_17(),
	UrlMappingsSection_t960595399_StaticFields::get_offset_of_urlMappingsProp_18(),
	UrlMappingsSection_t960595399_StaticFields::get_offset_of_properties_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3289 = { sizeof (VirtualDirectoryMappingCollection_t3325528393), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3290 = { sizeof (VirtualDirectoryMapping_t3264766938), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3290[4] = 
{
	VirtualDirectoryMapping_t3264766938::get_offset_of_physicalDirectory_0(),
	VirtualDirectoryMapping_t3264766938::get_offset_of_isAppRoot_1(),
	VirtualDirectoryMapping_t3264766938::get_offset_of_configFileBaseName_2(),
	VirtualDirectoryMapping_t3264766938::get_offset_of_virtualDirectory_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3291 = { sizeof (WebApplicationLevel_t1989904319)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3291[4] = 
{
	WebApplicationLevel_t1989904319::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3292 = { sizeof (WebConfigurationFileMap_t225230154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3292[1] = 
{
	WebConfigurationFileMap_t225230154::get_offset_of_virtualDirectories_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3293 = { sizeof (WebConfigurationHost_t108733871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3293[1] = 
{
	WebConfigurationHost_t108733871::get_offset_of_map_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3294 = { sizeof (WebConfigurationManager_t2451561378), -1, sizeof(WebConfigurationManager_t2451561378_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3294[18] = 
{
	WebConfigurationManager_t2451561378_StaticFields::get_offset_of_pathTrimChars_0(),
	WebConfigurationManager_t2451561378_StaticFields::get_offset_of_suppressAppReloadLock_1(),
	WebConfigurationManager_t2451561378_StaticFields::get_offset_of_saveLocationsCacheLock_2(),
	WebConfigurationManager_t2451561378_StaticFields::get_offset_of_sectionCacheLock_3(),
	WebConfigurationManager_t2451561378_StaticFields::get_offset_of_configFactory_4(),
	WebConfigurationManager_t2451561378_StaticFields::get_offset_of_configurations_5(),
	WebConfigurationManager_t2451561378_StaticFields::get_offset_of_sectionCache_6(),
	WebConfigurationManager_t2451561378_StaticFields::get_offset_of_configPaths_7(),
	WebConfigurationManager_t2451561378_StaticFields::get_offset_of_suppressAppReload_8(),
	WebConfigurationManager_t2451561378_StaticFields::get_offset_of_saveLocationsCache_9(),
	WebConfigurationManager_t2451561378_StaticFields::get_offset_of_saveLocationsTimer_10(),
	WebConfigurationManager_t2451561378_StaticFields::get_offset_of_extra_assemblies_11(),
	WebConfigurationManager_t2451561378_StaticFields::get_offset_of_hasConfigErrors_12(),
	WebConfigurationManager_t2451561378_StaticFields::get_offset_of_hasConfigErrorsLock_13(),
	WebConfigurationManager_t2451561378_StaticFields::get_offset_of_get_runtime_object_14(),
	WebConfigurationManager_t2451561378_StaticFields::get_offset_of_oldConfig_15(),
	WebConfigurationManager_t2451561378_StaticFields::get_offset_of_config_16(),
	WebConfigurationManager_t2451561378_StaticFields::get_offset_of_lockobj_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3295 = { sizeof (Web20DefaultConfig_t163121754), -1, sizeof(Web20DefaultConfig_t163121754_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3295[1] = 
{
	Web20DefaultConfig_t163121754_StaticFields::get_offset_of_instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3296 = { sizeof (WebContext_t875593705), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3296[5] = 
{
	WebContext_t875593705::get_offset_of_pathLevel_0(),
	WebContext_t875593705::get_offset_of_site_1(),
	WebContext_t875593705::get_offset_of_applicationPath_2(),
	WebContext_t875593705::get_offset_of_path_3(),
	WebContext_t875593705::get_offset_of_locationSubPath_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3297 = { sizeof (XhtmlConformanceMode_t3960522365)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3297[4] = 
{
	XhtmlConformanceMode_t3960522365::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3298 = { sizeof (XhtmlConformanceSection_t3023718644), -1, sizeof(XhtmlConformanceSection_t3023718644_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3298[2] = 
{
	XhtmlConformanceSection_t3023718644_StaticFields::get_offset_of_modeProp_17(),
	XhtmlConformanceSection_t3023718644_StaticFields::get_offset_of_properties_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3299 = { sizeof (AssemblyResourceLoader_t761489670), -1, sizeof(AssemblyResourceLoader_t761489670_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3299[3] = 
{
	AssemblyResourceLoader_t761489670_StaticFields::get_offset_of_currAsm_0(),
	AssemblyResourceLoader_t761489670_StaticFields::get_offset_of__embeddedResources_1(),
	AssemblyResourceLoader_t761489670_StaticFields::get_offset_of_init_vector_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
