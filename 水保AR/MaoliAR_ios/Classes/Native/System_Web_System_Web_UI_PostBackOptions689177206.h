﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.UI.Control
struct Control_t3006474639;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.PostBackOptions
struct  PostBackOptions_t689177206  : public Il2CppObject
{
public:
	// System.Web.UI.Control System.Web.UI.PostBackOptions::control
	Control_t3006474639 * ___control_0;
	// System.String System.Web.UI.PostBackOptions::argument
	String_t* ___argument_1;
	// System.String System.Web.UI.PostBackOptions::actionUrl
	String_t* ___actionUrl_2;
	// System.Boolean System.Web.UI.PostBackOptions::autoPostBack
	bool ___autoPostBack_3;
	// System.Boolean System.Web.UI.PostBackOptions::requiresJavaScriptProtocol
	bool ___requiresJavaScriptProtocol_4;
	// System.Boolean System.Web.UI.PostBackOptions::trackFocus
	bool ___trackFocus_5;
	// System.Boolean System.Web.UI.PostBackOptions::clientSubmit
	bool ___clientSubmit_6;
	// System.Boolean System.Web.UI.PostBackOptions::performValidation
	bool ___performValidation_7;
	// System.String System.Web.UI.PostBackOptions::validationGroup
	String_t* ___validationGroup_8;

public:
	inline static int32_t get_offset_of_control_0() { return static_cast<int32_t>(offsetof(PostBackOptions_t689177206, ___control_0)); }
	inline Control_t3006474639 * get_control_0() const { return ___control_0; }
	inline Control_t3006474639 ** get_address_of_control_0() { return &___control_0; }
	inline void set_control_0(Control_t3006474639 * value)
	{
		___control_0 = value;
		Il2CppCodeGenWriteBarrier(&___control_0, value);
	}

	inline static int32_t get_offset_of_argument_1() { return static_cast<int32_t>(offsetof(PostBackOptions_t689177206, ___argument_1)); }
	inline String_t* get_argument_1() const { return ___argument_1; }
	inline String_t** get_address_of_argument_1() { return &___argument_1; }
	inline void set_argument_1(String_t* value)
	{
		___argument_1 = value;
		Il2CppCodeGenWriteBarrier(&___argument_1, value);
	}

	inline static int32_t get_offset_of_actionUrl_2() { return static_cast<int32_t>(offsetof(PostBackOptions_t689177206, ___actionUrl_2)); }
	inline String_t* get_actionUrl_2() const { return ___actionUrl_2; }
	inline String_t** get_address_of_actionUrl_2() { return &___actionUrl_2; }
	inline void set_actionUrl_2(String_t* value)
	{
		___actionUrl_2 = value;
		Il2CppCodeGenWriteBarrier(&___actionUrl_2, value);
	}

	inline static int32_t get_offset_of_autoPostBack_3() { return static_cast<int32_t>(offsetof(PostBackOptions_t689177206, ___autoPostBack_3)); }
	inline bool get_autoPostBack_3() const { return ___autoPostBack_3; }
	inline bool* get_address_of_autoPostBack_3() { return &___autoPostBack_3; }
	inline void set_autoPostBack_3(bool value)
	{
		___autoPostBack_3 = value;
	}

	inline static int32_t get_offset_of_requiresJavaScriptProtocol_4() { return static_cast<int32_t>(offsetof(PostBackOptions_t689177206, ___requiresJavaScriptProtocol_4)); }
	inline bool get_requiresJavaScriptProtocol_4() const { return ___requiresJavaScriptProtocol_4; }
	inline bool* get_address_of_requiresJavaScriptProtocol_4() { return &___requiresJavaScriptProtocol_4; }
	inline void set_requiresJavaScriptProtocol_4(bool value)
	{
		___requiresJavaScriptProtocol_4 = value;
	}

	inline static int32_t get_offset_of_trackFocus_5() { return static_cast<int32_t>(offsetof(PostBackOptions_t689177206, ___trackFocus_5)); }
	inline bool get_trackFocus_5() const { return ___trackFocus_5; }
	inline bool* get_address_of_trackFocus_5() { return &___trackFocus_5; }
	inline void set_trackFocus_5(bool value)
	{
		___trackFocus_5 = value;
	}

	inline static int32_t get_offset_of_clientSubmit_6() { return static_cast<int32_t>(offsetof(PostBackOptions_t689177206, ___clientSubmit_6)); }
	inline bool get_clientSubmit_6() const { return ___clientSubmit_6; }
	inline bool* get_address_of_clientSubmit_6() { return &___clientSubmit_6; }
	inline void set_clientSubmit_6(bool value)
	{
		___clientSubmit_6 = value;
	}

	inline static int32_t get_offset_of_performValidation_7() { return static_cast<int32_t>(offsetof(PostBackOptions_t689177206, ___performValidation_7)); }
	inline bool get_performValidation_7() const { return ___performValidation_7; }
	inline bool* get_address_of_performValidation_7() { return &___performValidation_7; }
	inline void set_performValidation_7(bool value)
	{
		___performValidation_7 = value;
	}

	inline static int32_t get_offset_of_validationGroup_8() { return static_cast<int32_t>(offsetof(PostBackOptions_t689177206, ___validationGroup_8)); }
	inline String_t* get_validationGroup_8() const { return ___validationGroup_8; }
	inline String_t** get_address_of_validationGroup_8() { return &___validationGroup_8; }
	inline void set_validationGroup_8(String_t* value)
	{
		___validationGroup_8 = value;
		Il2CppCodeGenWriteBarrier(&___validationGroup_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
