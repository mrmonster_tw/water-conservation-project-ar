﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeDirective2939730587.h"
#include "mscorlib_System_Guid3193532887.h"

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4116647657;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeChecksumPragma
struct  CodeChecksumPragma_t2409807200  : public CodeDirective_t2939730587
{
public:
	// System.String System.CodeDom.CodeChecksumPragma::fileName
	String_t* ___fileName_1;
	// System.Guid System.CodeDom.CodeChecksumPragma::checksumAlgorithmId
	Guid_t  ___checksumAlgorithmId_2;
	// System.Byte[] System.CodeDom.CodeChecksumPragma::checksumData
	ByteU5BU5D_t4116647657* ___checksumData_3;

public:
	inline static int32_t get_offset_of_fileName_1() { return static_cast<int32_t>(offsetof(CodeChecksumPragma_t2409807200, ___fileName_1)); }
	inline String_t* get_fileName_1() const { return ___fileName_1; }
	inline String_t** get_address_of_fileName_1() { return &___fileName_1; }
	inline void set_fileName_1(String_t* value)
	{
		___fileName_1 = value;
		Il2CppCodeGenWriteBarrier(&___fileName_1, value);
	}

	inline static int32_t get_offset_of_checksumAlgorithmId_2() { return static_cast<int32_t>(offsetof(CodeChecksumPragma_t2409807200, ___checksumAlgorithmId_2)); }
	inline Guid_t  get_checksumAlgorithmId_2() const { return ___checksumAlgorithmId_2; }
	inline Guid_t * get_address_of_checksumAlgorithmId_2() { return &___checksumAlgorithmId_2; }
	inline void set_checksumAlgorithmId_2(Guid_t  value)
	{
		___checksumAlgorithmId_2 = value;
	}

	inline static int32_t get_offset_of_checksumData_3() { return static_cast<int32_t>(offsetof(CodeChecksumPragma_t2409807200, ___checksumData_3)); }
	inline ByteU5BU5D_t4116647657* get_checksumData_3() const { return ___checksumData_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_checksumData_3() { return &___checksumData_3; }
	inline void set_checksumData_3(ByteU5BU5D_t4116647657* value)
	{
		___checksumData_3 = value;
		Il2CppCodeGenWriteBarrier(&___checksumData_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
