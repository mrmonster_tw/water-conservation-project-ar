﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GUIUtility1868551600.h"
#include "UnityEngine_UnityEngine_GUIClip1511352915.h"
#include "UnityEngine_UnityEngine_ScrollViewState3797911395.h"
#include "UnityEngine_UnityEngine_SliderState2207048770.h"
#include "UnityEngine_UnityEngine_SliderHandler1154919399.h"
#include "UnityEngine_UnityEngine_TextEditor2759855366.h"
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnappin2629979741.h"
#include "UnityEngine_UnityEngine_TextEditor_CharacterType1303021140.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp1927482598.h"
#include "UnityEngine_UnityEngine_Internal_DrawArguments407707935.h"
#include "UnityEngine_UnityEngine_Internal_DrawWithTextSelect978153917.h"
#include "UnityEngine_UnityEngine_Networking_UnityWebRequest463507806.h"
#include "UnityEngine_UnityEngine_Networking_UnityWebRequest1119759680.h"
#include "UnityEngine_UnityEngineInternal_WebRequestUtils3541624225.h"
#include "UnityEngine_UnityEngine_Networking_UploadHandler2993558019.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler2937767557.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler2928496527.h"
#include "UnityEngine_UnityEngine_RemoteSettings1718627291.h"
#include "UnityEngine_UnityEngine_RemoteSettings_UpdatedEven1027848393.h"
#include "UnityEngine_UnityEngine_ParticleRenderer1704730503.h"
#include "UnityEngine_UnityEngine_AndroidJavaRunnable377890659.h"
#include "UnityEngine_UnityEngine_AndroidJavaException386123666.h"
#include "UnityEngine_UnityEngine_AndroidJavaRunnableProxy3712271097.h"
#include "UnityEngine_UnityEngine_AndroidJavaProxy2835824643.h"
#include "UnityEngine_UnityEngine_AndroidReflection1263802604.h"
#include "UnityEngine_UnityEngine__AndroidJNIHelper3569676081.h"
#include "UnityEngine_UnityEngine_AndroidJNISafe3135565470.h"
#include "UnityEngine_UnityEngine_AttributeHelperEngine2735742303.h"
#include "UnityEngine_UnityEngine_DisallowMultipleComponent1422053217.h"
#include "UnityEngine_UnityEngine_RequireComponent3490506609.h"
#include "UnityEngine_UnityEngine_AddComponentMenu415040132.h"
#include "UnityEngine_UnityEngine_ContextMenu1295656858.h"
#include "UnityEngine_UnityEngine_ExecuteInEditMode3727731349.h"
#include "UnityEngine_UnityEngine_HideInInspector1216868993.h"
#include "UnityEngine_UnityEngine_DefaultExecutionOrder3059642329.h"
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttri1946008997.h"
#include "UnityEngine_UnityEngine_NativeClassAttribute2601352714.h"
#include "UnityEngine_UnityEngine_Scripting_GeneratedByOldBin433318409.h"
#include "UnityEngine_UnityEngine_SendMessageOptions3580193095.h"
#include "UnityEngine_UnityEngine_PrimitiveType3468579401.h"
#include "UnityEngine_UnityEngine_Space654135784.h"
#include "UnityEngine_UnityEngine_RuntimePlatform4159857903.h"
#include "UnityEngine_UnityEngine_OperatingSystemFamily1868066375.h"
#include "UnityEngine_UnityEngine_LogType73765434.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"
#include "UnityEngine_UnityEngine_Color322600501292.h"
#include "UnityEngine_UnityEngine_SetupCoroutine2062820429.h"
#include "UnityEngine_UnityEngine_WritableAttribute812406054.h"
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssembly3442416807.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2719720026.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_643925653.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_675222246.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2125309831.h"
#include "UnityEngine_UnityEngine_RenderBuffer586150500.h"
#include "UnityEngine_UnityEngine_Internal_DrawTextureArgume1705718261.h"
#include "UnityEngine_UnityEngine_RenderingPath883966888.h"
#include "UnityEngine_UnityEngine_TransparencySortMode3644896537.h"
#include "UnityEngine_UnityEngine_ComputeBufferType3421096473.h"
#include "UnityEngine_UnityEngine_FogMode1277989386.h"
#include "UnityEngine_UnityEngine_CameraClearFlags2362496923.h"
#include "UnityEngine_UnityEngine_DepthTextureMode4161834719.h"
#include "UnityEngine_UnityEngine_MeshTopology838400051.h"
#include "UnityEngine_UnityEngine_ColorSpace3453996949.h"
#include "UnityEngine_UnityEngine_ScreenOrientation1705519499.h"
#include "UnityEngine_UnityEngine_FilterMode3761284007.h"
#include "UnityEngine_UnityEngine_TextureWrapMode584250749.h"
#include "UnityEngine_UnityEngine_TextureFormat2701165832.h"
#include "UnityEngine_UnityEngine_CubemapFace1358225318.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat962350765.h"
#include "UnityEngine_UnityEngine_VRTextureUsage3142149582.h"
#include "UnityEngine_UnityEngine_RenderTextureReadWrite1793271918.h"
#include "UnityEngine_UnityEngine_Rendering_CompareFunction2171731108.h"
#include "UnityEngine_UnityEngine_Rendering_ColorWriteMask4282245599.h"
#include "UnityEngine_UnityEngine_Rendering_StencilOp3446174106.h"
#include "UnityEngine_UnityEngine_Rendering_CameraEvent2033959522.h"
#include "UnityEngine_UnityEngine_Rendering_BuiltinRenderTex2399837169.h"
#include "UnityEngine_UnityEngine_Rendering_GraphicsDeviceTy1797077436.h"
#include "UnityEngine_UnityEngine_Rendering_RenderTargetIden2079184500.h"
#include "UnityEngine_UnityEngine_GUIStateObjects327145277.h"
#include "UnityEngine_UnityEngine_KeyCode2599294277.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalU365094499.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserP3137328177.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achiev565359984.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achie3217594527.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score1968645328.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leade1065076763.h"
#include "UnityEngine_UnityEngineInternal_MathfInternal624072491.h"
#include "UnityEngine_UnityEngineInternal_ScriptingUtils2624832893.h"
#include "UnityEngine_UnityEngine_SendMouseEvents3273302915.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo3229609740.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState4177058321.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope604006431.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope539351503.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4000 = { sizeof (GUIUtility_t1868551600), -1, sizeof(GUIUtility_t1868551600_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4000[4] = 
{
	GUIUtility_t1868551600_StaticFields::get_offset_of_s_SkinMode_0(),
	GUIUtility_t1868551600_StaticFields::get_offset_of_s_OriginalID_1(),
	GUIUtility_t1868551600_StaticFields::get_offset_of_U3CguiIsExitingU3Ek__BackingField_2(),
	GUIUtility_t1868551600_StaticFields::get_offset_of_s_EditorScreenPointOffset_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4001 = { sizeof (GUIClip_t1511352915), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4002 = { sizeof (ScrollViewState_t3797911395), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4002[5] = 
{
	ScrollViewState_t3797911395::get_offset_of_position_0(),
	ScrollViewState_t3797911395::get_offset_of_visibleRect_1(),
	ScrollViewState_t3797911395::get_offset_of_viewRect_2(),
	ScrollViewState_t3797911395::get_offset_of_scrollPosition_3(),
	ScrollViewState_t3797911395::get_offset_of_apply_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4003 = { sizeof (SliderState_t2207048770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4003[3] = 
{
	SliderState_t2207048770::get_offset_of_dragStartPos_0(),
	SliderState_t2207048770::get_offset_of_dragStartValue_1(),
	SliderState_t2207048770::get_offset_of_isDragging_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4004 = { sizeof (SliderHandler_t1154919399)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4004[9] = 
{
	SliderHandler_t1154919399::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t1154919399::get_offset_of_currentValue_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t1154919399::get_offset_of_size_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t1154919399::get_offset_of_start_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t1154919399::get_offset_of_end_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t1154919399::get_offset_of_slider_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t1154919399::get_offset_of_thumb_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t1154919399::get_offset_of_horiz_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t1154919399::get_offset_of_id_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4005 = { sizeof (TextEditor_t2759855366), -1, sizeof(TextEditor_t2759855366_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4005[24] = 
{
	TextEditor_t2759855366::get_offset_of_keyboardOnScreen_0(),
	TextEditor_t2759855366::get_offset_of_controlID_1(),
	TextEditor_t2759855366::get_offset_of_style_2(),
	TextEditor_t2759855366::get_offset_of_multiline_3(),
	TextEditor_t2759855366::get_offset_of_hasHorizontalCursorPos_4(),
	TextEditor_t2759855366::get_offset_of_isPasswordField_5(),
	TextEditor_t2759855366::get_offset_of_m_HasFocus_6(),
	TextEditor_t2759855366::get_offset_of_scrollOffset_7(),
	TextEditor_t2759855366::get_offset_of_m_Content_8(),
	TextEditor_t2759855366::get_offset_of_m_Position_9(),
	TextEditor_t2759855366::get_offset_of_m_CursorIndex_10(),
	TextEditor_t2759855366::get_offset_of_m_SelectIndex_11(),
	TextEditor_t2759855366::get_offset_of_m_RevealCursor_12(),
	TextEditor_t2759855366::get_offset_of_graphicalCursorPos_13(),
	TextEditor_t2759855366::get_offset_of_graphicalSelectCursorPos_14(),
	TextEditor_t2759855366::get_offset_of_m_MouseDragSelectsWholeWords_15(),
	TextEditor_t2759855366::get_offset_of_m_DblClickInitPos_16(),
	TextEditor_t2759855366::get_offset_of_m_DblClickSnap_17(),
	TextEditor_t2759855366::get_offset_of_m_bJustSelected_18(),
	TextEditor_t2759855366::get_offset_of_m_iAltCursorPos_19(),
	TextEditor_t2759855366::get_offset_of_oldText_20(),
	TextEditor_t2759855366::get_offset_of_oldPos_21(),
	TextEditor_t2759855366::get_offset_of_oldSelectPos_22(),
	TextEditor_t2759855366_StaticFields::get_offset_of_s_Keyactions_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4006 = { sizeof (DblClickSnapping_t2629979741)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4006[3] = 
{
	DblClickSnapping_t2629979741::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4007 = { sizeof (CharacterType_t1303021140)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4007[5] = 
{
	CharacterType_t1303021140::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4008 = { sizeof (TextEditOp_t1927482598)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4008[51] = 
{
	TextEditOp_t1927482598::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4009 = { sizeof (Internal_DrawArguments_t407707935)+ sizeof (Il2CppObject), sizeof(Internal_DrawArguments_t407707935 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4009[6] = 
{
	Internal_DrawArguments_t407707935::get_offset_of_target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t407707935::get_offset_of_position_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t407707935::get_offset_of_isHover_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t407707935::get_offset_of_isActive_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t407707935::get_offset_of_on_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t407707935::get_offset_of_hasKeyboardFocus_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4010 = { sizeof (Internal_DrawWithTextSelectionArguments_t978153917)+ sizeof (Il2CppObject), sizeof(Internal_DrawWithTextSelectionArguments_t978153917 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4010[11] = 
{
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_position_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_firstPos_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_lastPos_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_cursorColor_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_selectionColor_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_isHover_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_isActive_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_on_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_hasKeyboardFocus_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_drawSelectionAsComposition_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4011 = { sizeof (UnityWebRequest_t463507806), sizeof(UnityWebRequest_t463507806_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4011[3] = 
{
	UnityWebRequest_t463507806::get_offset_of_m_Ptr_0(),
	UnityWebRequest_t463507806::get_offset_of_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_1(),
	UnityWebRequest_t463507806::get_offset_of_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4012 = { sizeof (UnityWebRequestMethod_t1119759680)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4012[6] = 
{
	UnityWebRequestMethod_t1119759680::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4013 = { sizeof (WebRequestUtils_t3541624225), -1, sizeof(WebRequestUtils_t3541624225_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4013[1] = 
{
	WebRequestUtils_t3541624225_StaticFields::get_offset_of_domainRegex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4014 = { sizeof (UploadHandler_t2993558019), sizeof(UploadHandler_t2993558019_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4014[1] = 
{
	UploadHandler_t2993558019::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4015 = { sizeof (DownloadHandler_t2937767557), sizeof(DownloadHandler_t2937767557_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4015[1] = 
{
	DownloadHandler_t2937767557::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4016 = { sizeof (DownloadHandlerBuffer_t2928496527), sizeof(DownloadHandlerBuffer_t2928496527_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4017 = { sizeof (RemoteSettings_t1718627291), -1, sizeof(RemoteSettings_t1718627291_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4017[1] = 
{
	RemoteSettings_t1718627291_StaticFields::get_offset_of_Updated_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4018 = { sizeof (UpdatedEventHandler_t1027848393), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4019 = { sizeof (ParticleRenderer_t1704730503), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4020 = { sizeof (AndroidJavaRunnable_t377890659), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4021 = { sizeof (AndroidJavaException_t386123666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4021[1] = 
{
	AndroidJavaException_t386123666::get_offset_of_mJavaStackTrace_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4022 = { sizeof (AndroidJavaRunnableProxy_t3712271097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4022[1] = 
{
	AndroidJavaRunnableProxy_t3712271097::get_offset_of_mRunnable_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4023 = { sizeof (AndroidJavaProxy_t2835824643), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4023[1] = 
{
	AndroidJavaProxy_t2835824643::get_offset_of_javaInterface_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4024 = { sizeof (AndroidReflection_t1263802604), -1, sizeof(AndroidReflection_t1263802604_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4024[5] = 
{
	AndroidReflection_t1263802604_StaticFields::get_offset_of_s_ReflectionHelperClass_0(),
	AndroidReflection_t1263802604_StaticFields::get_offset_of_s_ReflectionHelperGetConstructorID_1(),
	AndroidReflection_t1263802604_StaticFields::get_offset_of_s_ReflectionHelperGetMethodID_2(),
	AndroidReflection_t1263802604_StaticFields::get_offset_of_s_ReflectionHelperGetFieldID_3(),
	AndroidReflection_t1263802604_StaticFields::get_offset_of_s_ReflectionHelperNewProxyInstance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4025 = { sizeof (_AndroidJNIHelper_t3569676081), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4026 = { sizeof (AndroidJNISafe_t3135565470), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4027 = { sizeof (AttributeHelperEngine_t2735742303), -1, sizeof(AttributeHelperEngine_t2735742303_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4027[3] = 
{
	AttributeHelperEngine_t2735742303_StaticFields::get_offset_of__disallowMultipleComponentArray_0(),
	AttributeHelperEngine_t2735742303_StaticFields::get_offset_of__executeInEditModeArray_1(),
	AttributeHelperEngine_t2735742303_StaticFields::get_offset_of__requireComponentArray_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4028 = { sizeof (DisallowMultipleComponent_t1422053217), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4029 = { sizeof (RequireComponent_t3490506609), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4029[3] = 
{
	RequireComponent_t3490506609::get_offset_of_m_Type0_0(),
	RequireComponent_t3490506609::get_offset_of_m_Type1_1(),
	RequireComponent_t3490506609::get_offset_of_m_Type2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4030 = { sizeof (AddComponentMenu_t415040132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4030[2] = 
{
	AddComponentMenu_t415040132::get_offset_of_m_AddComponentMenu_0(),
	AddComponentMenu_t415040132::get_offset_of_m_Ordering_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4031 = { sizeof (ContextMenu_t1295656858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4031[3] = 
{
	ContextMenu_t1295656858::get_offset_of_menuItem_0(),
	ContextMenu_t1295656858::get_offset_of_validate_1(),
	ContextMenu_t1295656858::get_offset_of_priority_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4032 = { sizeof (ExecuteInEditMode_t3727731349), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4033 = { sizeof (HideInInspector_t1216868993), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4034 = { sizeof (DefaultExecutionOrder_t3059642329), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4034[1] = 
{
	DefaultExecutionOrder_t3059642329::get_offset_of_U3CorderU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4035 = { sizeof (IL2CPPStructAlignmentAttribute_t1946008997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4035[1] = 
{
	IL2CPPStructAlignmentAttribute_t1946008997::get_offset_of_Align_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4036 = { sizeof (NativeClassAttribute_t2601352714), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4036[1] = 
{
	NativeClassAttribute_t2601352714::get_offset_of_U3CQualifiedNativeNameU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4037 = { sizeof (GeneratedByOldBindingsGeneratorAttribute_t433318409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4038 = { sizeof (SendMessageOptions_t3580193095)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4038[3] = 
{
	SendMessageOptions_t3580193095::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4039 = { sizeof (PrimitiveType_t3468579401)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4039[7] = 
{
	PrimitiveType_t3468579401::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4040 = { sizeof (Space_t654135784)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4040[3] = 
{
	Space_t654135784::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4041 = { sizeof (RuntimePlatform_t4159857903)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4041[34] = 
{
	RuntimePlatform_t4159857903::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4042 = { sizeof (OperatingSystemFamily_t1868066375)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4042[5] = 
{
	OperatingSystemFamily_t1868066375::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4043 = { sizeof (LogType_t73765434)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4043[6] = 
{
	LogType_t73765434::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4044 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4044[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4045 = { sizeof (Color_t2555686324)+ sizeof (Il2CppObject), sizeof(Color_t2555686324 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4045[4] = 
{
	Color_t2555686324::get_offset_of_r_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color_t2555686324::get_offset_of_g_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color_t2555686324::get_offset_of_b_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color_t2555686324::get_offset_of_a_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4046 = { sizeof (Color32_t2600501292)+ sizeof (Il2CppObject), sizeof(Color32_t2600501292 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4046[4] = 
{
	Color32_t2600501292::get_offset_of_r_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color32_t2600501292::get_offset_of_g_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color32_t2600501292::get_offset_of_b_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color32_t2600501292::get_offset_of_a_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4047 = { sizeof (SetupCoroutine_t2062820429), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4048 = { sizeof (WritableAttribute_t812406054), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4049 = { sizeof (AssemblyIsEditorAssembly_t3442416807), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4050 = { sizeof (GcUserProfileData_t2719720026)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4050[4] = 
{
	GcUserProfileData_t2719720026::get_offset_of_userName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcUserProfileData_t2719720026::get_offset_of_userID_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcUserProfileData_t2719720026::get_offset_of_isFriend_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcUserProfileData_t2719720026::get_offset_of_image_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4051 = { sizeof (GcAchievementDescriptionData_t643925653)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4051[7] = 
{
	GcAchievementDescriptionData_t643925653::get_offset_of_m_Identifier_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t643925653::get_offset_of_m_Title_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t643925653::get_offset_of_m_Image_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t643925653::get_offset_of_m_AchievedDescription_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t643925653::get_offset_of_m_UnachievedDescription_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t643925653::get_offset_of_m_Hidden_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t643925653::get_offset_of_m_Points_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4052 = { sizeof (GcAchievementData_t675222246)+ sizeof (Il2CppObject), sizeof(GcAchievementData_t675222246_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4052[5] = 
{
	GcAchievementData_t675222246::get_offset_of_m_Identifier_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementData_t675222246::get_offset_of_m_PercentCompleted_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementData_t675222246::get_offset_of_m_Completed_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementData_t675222246::get_offset_of_m_Hidden_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementData_t675222246::get_offset_of_m_LastReportedDate_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4053 = { sizeof (GcScoreData_t2125309831)+ sizeof (Il2CppObject), sizeof(GcScoreData_t2125309831_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4053[7] = 
{
	GcScoreData_t2125309831::get_offset_of_m_Category_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t2125309831::get_offset_of_m_ValueLow_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t2125309831::get_offset_of_m_ValueHigh_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t2125309831::get_offset_of_m_Date_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t2125309831::get_offset_of_m_FormattedValue_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t2125309831::get_offset_of_m_PlayerID_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t2125309831::get_offset_of_m_Rank_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4054 = { sizeof (RenderBuffer_t586150500)+ sizeof (Il2CppObject), sizeof(RenderBuffer_t586150500 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4054[2] = 
{
	RenderBuffer_t586150500::get_offset_of_m_RenderTextureInstanceID_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RenderBuffer_t586150500::get_offset_of_m_BufferPtr_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4055 = { sizeof (Internal_DrawTextureArguments_t1705718261)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4055[10] = 
{
	Internal_DrawTextureArguments_t1705718261::get_offset_of_screenRect_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawTextureArguments_t1705718261::get_offset_of_sourceRect_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawTextureArguments_t1705718261::get_offset_of_leftBorder_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawTextureArguments_t1705718261::get_offset_of_rightBorder_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawTextureArguments_t1705718261::get_offset_of_topBorder_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawTextureArguments_t1705718261::get_offset_of_bottomBorder_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawTextureArguments_t1705718261::get_offset_of_color_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawTextureArguments_t1705718261::get_offset_of_pass_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawTextureArguments_t1705718261::get_offset_of_texture_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawTextureArguments_t1705718261::get_offset_of_mat_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4056 = { sizeof (RenderingPath_t883966888)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4056[6] = 
{
	RenderingPath_t883966888::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4057 = { sizeof (TransparencySortMode_t3644896537)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4057[5] = 
{
	TransparencySortMode_t3644896537::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4058 = { sizeof (ComputeBufferType_t3421096473)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4058[8] = 
{
	ComputeBufferType_t3421096473::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4059 = { sizeof (FogMode_t1277989386)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4059[4] = 
{
	FogMode_t1277989386::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4060 = { sizeof (CameraClearFlags_t2362496923)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4060[6] = 
{
	CameraClearFlags_t2362496923::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4061 = { sizeof (DepthTextureMode_t4161834719)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4061[5] = 
{
	DepthTextureMode_t4161834719::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4062 = { sizeof (MeshTopology_t838400051)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4062[6] = 
{
	MeshTopology_t838400051::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4063 = { sizeof (ColorSpace_t3453996949)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4063[4] = 
{
	ColorSpace_t3453996949::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4064 = { sizeof (ScreenOrientation_t1705519499)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4064[8] = 
{
	ScreenOrientation_t1705519499::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4065 = { sizeof (FilterMode_t3761284007)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4065[4] = 
{
	FilterMode_t3761284007::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4066 = { sizeof (TextureWrapMode_t584250749)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4066[3] = 
{
	TextureWrapMode_t584250749::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4067 = { sizeof (TextureFormat_t2701165832)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4067[54] = 
{
	TextureFormat_t2701165832::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4068 = { sizeof (CubemapFace_t1358225318)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4068[8] = 
{
	CubemapFace_t1358225318::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4069 = { sizeof (RenderTextureFormat_t962350765)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4069[26] = 
{
	RenderTextureFormat_t962350765::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4070 = { sizeof (VRTextureUsage_t3142149582)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4070[4] = 
{
	VRTextureUsage_t3142149582::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4071 = { sizeof (RenderTextureReadWrite_t1793271918)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4071[4] = 
{
	RenderTextureReadWrite_t1793271918::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4072 = { sizeof (CompareFunction_t2171731108)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4072[10] = 
{
	CompareFunction_t2171731108::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4073 = { sizeof (ColorWriteMask_t4282245599)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4073[6] = 
{
	ColorWriteMask_t4282245599::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4074 = { sizeof (StencilOp_t3446174106)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4074[9] = 
{
	StencilOp_t3446174106::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4075 = { sizeof (CameraEvent_t2033959522)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4075[24] = 
{
	CameraEvent_t2033959522::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4076 = { sizeof (BuiltinRenderTextureType_t2399837169)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4076[21] = 
{
	BuiltinRenderTextureType_t2399837169::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4077 = { sizeof (GraphicsDeviceType_t1797077436)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4077[18] = 
{
	GraphicsDeviceType_t1797077436::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4078 = { sizeof (RenderTargetIdentifier_t2079184500)+ sizeof (Il2CppObject), sizeof(RenderTargetIdentifier_t2079184500 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4078[3] = 
{
	RenderTargetIdentifier_t2079184500::get_offset_of_m_Type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RenderTargetIdentifier_t2079184500::get_offset_of_m_NameID_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RenderTargetIdentifier_t2079184500::get_offset_of_m_InstanceID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4079 = { sizeof (GUIStateObjects_t327145277), -1, sizeof(GUIStateObjects_t327145277_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4079[1] = 
{
	GUIStateObjects_t327145277_StaticFields::get_offset_of_s_StateCache_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4080 = { sizeof (KeyCode_t2599294277)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4080[322] = 
{
	KeyCode_t2599294277::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4081 = { sizeof (LocalUser_t365094499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4081[3] = 
{
	LocalUser_t365094499::get_offset_of_m_Friends_5(),
	LocalUser_t365094499::get_offset_of_m_Authenticated_6(),
	LocalUser_t365094499::get_offset_of_m_Underage_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4082 = { sizeof (UserProfile_t3137328177), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4082[5] = 
{
	UserProfile_t3137328177::get_offset_of_m_UserName_0(),
	UserProfile_t3137328177::get_offset_of_m_ID_1(),
	UserProfile_t3137328177::get_offset_of_m_IsFriend_2(),
	UserProfile_t3137328177::get_offset_of_m_State_3(),
	UserProfile_t3137328177::get_offset_of_m_Image_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4083 = { sizeof (Achievement_t565359984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4083[5] = 
{
	Achievement_t565359984::get_offset_of_m_Completed_0(),
	Achievement_t565359984::get_offset_of_m_Hidden_1(),
	Achievement_t565359984::get_offset_of_m_LastReportedDate_2(),
	Achievement_t565359984::get_offset_of_U3CidU3Ek__BackingField_3(),
	Achievement_t565359984::get_offset_of_U3CpercentCompletedU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4084 = { sizeof (AchievementDescription_t3217594527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4084[7] = 
{
	AchievementDescription_t3217594527::get_offset_of_m_Title_0(),
	AchievementDescription_t3217594527::get_offset_of_m_Image_1(),
	AchievementDescription_t3217594527::get_offset_of_m_AchievedDescription_2(),
	AchievementDescription_t3217594527::get_offset_of_m_UnachievedDescription_3(),
	AchievementDescription_t3217594527::get_offset_of_m_Hidden_4(),
	AchievementDescription_t3217594527::get_offset_of_m_Points_5(),
	AchievementDescription_t3217594527::get_offset_of_U3CidU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4085 = { sizeof (Score_t1968645328), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4085[6] = 
{
	Score_t1968645328::get_offset_of_m_Date_0(),
	Score_t1968645328::get_offset_of_m_FormattedValue_1(),
	Score_t1968645328::get_offset_of_m_UserID_2(),
	Score_t1968645328::get_offset_of_m_Rank_3(),
	Score_t1968645328::get_offset_of_U3CleaderboardIDU3Ek__BackingField_4(),
	Score_t1968645328::get_offset_of_U3CvalueU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4086 = { sizeof (Leaderboard_t1065076763), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4086[10] = 
{
	Leaderboard_t1065076763::get_offset_of_m_Loading_0(),
	Leaderboard_t1065076763::get_offset_of_m_LocalUserScore_1(),
	Leaderboard_t1065076763::get_offset_of_m_MaxRange_2(),
	Leaderboard_t1065076763::get_offset_of_m_Scores_3(),
	Leaderboard_t1065076763::get_offset_of_m_Title_4(),
	Leaderboard_t1065076763::get_offset_of_m_UserIDs_5(),
	Leaderboard_t1065076763::get_offset_of_U3CidU3Ek__BackingField_6(),
	Leaderboard_t1065076763::get_offset_of_U3CuserScopeU3Ek__BackingField_7(),
	Leaderboard_t1065076763::get_offset_of_U3CrangeU3Ek__BackingField_8(),
	Leaderboard_t1065076763::get_offset_of_U3CtimeScopeU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4087 = { sizeof (MathfInternal_t624072491)+ sizeof (Il2CppObject), sizeof(MathfInternal_t624072491 ), sizeof(MathfInternal_t624072491_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4087[3] = 
{
	MathfInternal_t624072491_StaticFields::get_offset_of_FloatMinNormal_0(),
	MathfInternal_t624072491_StaticFields::get_offset_of_FloatMinDenormal_1(),
	MathfInternal_t624072491_StaticFields::get_offset_of_IsFlushToZeroEnabled_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4088 = { sizeof (ScriptingUtils_t2624832893), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4089 = { sizeof (SendMouseEvents_t3273302915), -1, sizeof(SendMouseEvents_t3273302915_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4089[5] = 
{
	SendMouseEvents_t3273302915_StaticFields::get_offset_of_s_MouseUsed_0(),
	SendMouseEvents_t3273302915_StaticFields::get_offset_of_m_LastHit_1(),
	SendMouseEvents_t3273302915_StaticFields::get_offset_of_m_MouseDownHit_2(),
	SendMouseEvents_t3273302915_StaticFields::get_offset_of_m_CurrentHit_3(),
	SendMouseEvents_t3273302915_StaticFields::get_offset_of_m_Cameras_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4090 = { sizeof (HitInfo_t3229609740)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4090[2] = 
{
	HitInfo_t3229609740::get_offset_of_target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HitInfo_t3229609740::get_offset_of_camera_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4091 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4092 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4093 = { sizeof (UserState_t4177058321)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4093[6] = 
{
	UserState_t4177058321::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4094 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4095 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4096 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4097 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4098 = { sizeof (UserScope_t604006431)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4098[3] = 
{
	UserScope_t604006431::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4099 = { sizeof (TimeScope_t539351503)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4099[4] = 
{
	TimeScope_t539351503::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
