﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Runtime_InteropServices_ExternalEx3544951457.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpException
struct  HttpException_t2907797370  : public ExternalException_t3544951457
{
public:
	// System.Int32 System.Web.HttpException::http_code
	int32_t ___http_code_11;
	// System.String System.Web.HttpException::resource_name
	String_t* ___resource_name_12;
	// System.String System.Web.HttpException::description
	String_t* ___description_13;

public:
	inline static int32_t get_offset_of_http_code_11() { return static_cast<int32_t>(offsetof(HttpException_t2907797370, ___http_code_11)); }
	inline int32_t get_http_code_11() const { return ___http_code_11; }
	inline int32_t* get_address_of_http_code_11() { return &___http_code_11; }
	inline void set_http_code_11(int32_t value)
	{
		___http_code_11 = value;
	}

	inline static int32_t get_offset_of_resource_name_12() { return static_cast<int32_t>(offsetof(HttpException_t2907797370, ___resource_name_12)); }
	inline String_t* get_resource_name_12() const { return ___resource_name_12; }
	inline String_t** get_address_of_resource_name_12() { return &___resource_name_12; }
	inline void set_resource_name_12(String_t* value)
	{
		___resource_name_12 = value;
		Il2CppCodeGenWriteBarrier(&___resource_name_12, value);
	}

	inline static int32_t get_offset_of_description_13() { return static_cast<int32_t>(offsetof(HttpException_t2907797370, ___description_13)); }
	inline String_t* get_description_13() const { return ___description_13; }
	inline String_t** get_address_of_description_13() { return &___description_13; }
	inline void set_description_13(String_t* value)
	{
		___description_13 = value;
		Il2CppCodeGenWriteBarrier(&___description_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
