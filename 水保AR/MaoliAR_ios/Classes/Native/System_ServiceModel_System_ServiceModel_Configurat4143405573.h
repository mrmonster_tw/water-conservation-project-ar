﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Configurati551052268.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.MsmqIntegrationBindingElement
struct  MsmqIntegrationBindingElement_t4143405573  : public MsmqBindingElementBase_t551052268
{
public:

public:
};

struct MsmqIntegrationBindingElement_t4143405573_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.MsmqIntegrationBindingElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_27;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.MsmqIntegrationBindingElement::binding_element_type
	ConfigurationProperty_t3590861854 * ___binding_element_type_28;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.MsmqIntegrationBindingElement::security
	ConfigurationProperty_t3590861854 * ___security_29;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.MsmqIntegrationBindingElement::serialization_format
	ConfigurationProperty_t3590861854 * ___serialization_format_30;

public:
	inline static int32_t get_offset_of_properties_27() { return static_cast<int32_t>(offsetof(MsmqIntegrationBindingElement_t4143405573_StaticFields, ___properties_27)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_27() const { return ___properties_27; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_27() { return &___properties_27; }
	inline void set_properties_27(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_27 = value;
		Il2CppCodeGenWriteBarrier(&___properties_27, value);
	}

	inline static int32_t get_offset_of_binding_element_type_28() { return static_cast<int32_t>(offsetof(MsmqIntegrationBindingElement_t4143405573_StaticFields, ___binding_element_type_28)); }
	inline ConfigurationProperty_t3590861854 * get_binding_element_type_28() const { return ___binding_element_type_28; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_binding_element_type_28() { return &___binding_element_type_28; }
	inline void set_binding_element_type_28(ConfigurationProperty_t3590861854 * value)
	{
		___binding_element_type_28 = value;
		Il2CppCodeGenWriteBarrier(&___binding_element_type_28, value);
	}

	inline static int32_t get_offset_of_security_29() { return static_cast<int32_t>(offsetof(MsmqIntegrationBindingElement_t4143405573_StaticFields, ___security_29)); }
	inline ConfigurationProperty_t3590861854 * get_security_29() const { return ___security_29; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_security_29() { return &___security_29; }
	inline void set_security_29(ConfigurationProperty_t3590861854 * value)
	{
		___security_29 = value;
		Il2CppCodeGenWriteBarrier(&___security_29, value);
	}

	inline static int32_t get_offset_of_serialization_format_30() { return static_cast<int32_t>(offsetof(MsmqIntegrationBindingElement_t4143405573_StaticFields, ___serialization_format_30)); }
	inline ConfigurationProperty_t3590861854 * get_serialization_format_30() const { return ___serialization_format_30; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_serialization_format_30() { return &___serialization_format_30; }
	inline void set_serialization_format_30(ConfigurationProperty_t3590861854 * value)
	{
		___serialization_format_30 = value;
		Il2CppCodeGenWriteBarrier(&___serialization_format_30, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
