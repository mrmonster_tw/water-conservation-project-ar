﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_ServiceModel_System_ServiceModel_Channels_U2903644797.h"
#include "System_ServiceModel_System_ServiceModel_DuplexClien737641411.h"
#include "System_ServiceModel_System_ServiceModel_EndpointAd3119842923.h"
#include "System_ServiceModel_System_ServiceModel_EndpointAd2657313230.h"
#include "System_ServiceModel_System_ServiceModel_EndpointAd2643301447.h"
#include "System_ServiceModel_System_ServiceModel_EndpointId3899230012.h"
#include "System_ServiceModel_System_ServiceModel_EndpointNo1807428957.h"
#include "System_ServiceModel_System_ServiceModel_EnvelopeVe2487833488.h"
#include "System_ServiceModel_System_ServiceModel_ExceptionD3678749343.h"
#include "System_ServiceModel_System_ServiceModel_FaultCode3080963035.h"
#include "System_ServiceModel_System_ServiceModel_FaultContr3255918006.h"
#include "System_ServiceModel_System_ServiceModel_FaultExcep2858892349.h"
#include "System_ServiceModel_System_ServiceModel_FaultReaso2245999938.h"
#include "System_ServiceModel_System_ServiceModel_FaultReaso3547351049.h"
#include "System_ServiceModel_System_ServiceModel_FederatedM2993240700.h"
#include "System_ServiceModel_System_ServiceModel_HttpTransp2136062604.h"
#include "System_ServiceModel_System_ServiceModel_InstanceCo3593205954.h"
#include "System_ServiceModel_System_ServiceModel_MessageBod2473986057.h"
#include "System_ServiceModel_System_ServiceModel_MessageCont292133743.h"
#include "System_ServiceModel_System_ServiceModel_MessageCont680184223.h"
#include "System_ServiceModel_System_ServiceModel_MessageHea2022966637.h"
#include "System_ServiceModel_System_ServiceModel_MessagePar3244018206.h"
#include "System_ServiceModel_System_ServiceModel_MessageSecu359378485.h"
#include "System_ServiceModel_System_ServiceModel_MessageSec2089208958.h"
#include "System_ServiceModel_System_ServiceModel_MessageSec2079539966.h"
#include "System_ServiceModel_System_ServiceModel_MessageSec2151590395.h"
#include "System_ServiceModel_System_ServiceModel_MessageSecur66298087.h"
#include "System_ServiceModel_System_ServiceModel_MsmqBindin2036657955.h"
#include "System_ServiceModel_System_ServiceModel_MsmqTransp2605141420.h"
#include "System_ServiceModel_System_ServiceModel_NetMsmqBin3496970231.h"
#include "System_ServiceModel_System_ServiceModel_NetMsmqSec1571703333.h"
#include "System_ServiceModel_System_ServiceModel_NetNamedPip983330350.h"
#include "System_ServiceModel_System_ServiceModel_NetPeerTcp3507636973.h"
#include "System_ServiceModel_System_ServiceModel_NetTcpBind2266210195.h"
#include "System_ServiceModel_System_ServiceModel_NetTcpSecur856315759.h"
#include "System_ServiceModel_System_ServiceModel_NonDualMess883223478.h"
#include "System_ServiceModel_System_ServiceModel_OperationB1398166452.h"
#include "System_ServiceModel_System_ServiceModel_OperationC2829644788.h"
#include "System_ServiceModel_System_ServiceModel_OperationC3090301225.h"
#include "System_ServiceModel_System_ServiceModel_OperationC3102514551.h"
#include "System_ServiceModel_System_ServiceModel_PeerNode3248961724.h"
#include "System_ServiceModel_System_ServiceModel_PeerNodeImp609519394.h"
#include "System_ServiceModel_System_ServiceModel_PeerNodeAd2098027372.h"
#include "System_ServiceModel_System_ServiceModel_PeerResolv1567980581.h"
#include "System_ServiceModel_System_ServiceModel_PeerSecuri2125602112.h"
#include "System_ServiceModel_System_ServiceModel_PeerTransp3643429466.h"
#include "System_ServiceModel_System_ServiceModel_ProtocolExc898429968.h"
#include "System_ServiceModel_System_ServiceModel_RsaEndpoin2589302870.h"
#include "System_ServiceModel_System_ServiceModel_ServiceAutho56182799.h"
#include "System_ServiceModel_System_ServiceModel_ServiceBeh1657799891.h"
#include "System_ServiceModel_System_ServiceModel_ServiceCon4131169907.h"
#include "System_ServiceModel_System_ServiceModel_ServicePro4233789185.h"
#include "System_ServiceModel_System_ServiceModel_ServiceHos1894294968.h"
#include "System_ServiceModel_System_ServiceModel_ServiceHos3741910535.h"
#include "System_ServiceModel_System_ServiceModel_Dispatcher3637350213.h"
#include "System_ServiceModel_System_ServiceModel_ServiceHost465623925.h"
#include "System_ServiceModel_System_ServiceModel_ServiceKno1229054133.h"
#include "System_ServiceModel_System_ServiceModel_DuplexServ4048757758.h"
#include "System_ServiceModel_System_ServiceModel_ServiceRun1028057095.h"
#include "System_ServiceModel_System_ServiceModel_ServiceSec3919255606.h"
#include "System_ServiceModel_System_ServiceModel_SpnEndpoin3481830764.h"
#include "System_ServiceModel_System_ServiceModel_TcpTranspo3663278697.h"
#include "System_ServiceModel_System_ServiceModel_Transactio3972232485.h"
#include "System_ServiceModel_System_ServiceModel_Transaction759410032.h"
#include "System_ServiceModel_System_ServiceModel_Transaction854629616.h"
#include "System_ServiceModel_System_ServiceModel_TransferMod436880017.h"
#include "System_ServiceModel_System_ServiceModel_UnknownMes1694867822.h"
#include "System_ServiceModel_System_ServiceModel_UpnEndpoin3463235294.h"
#include "System_ServiceModel_System_ServiceModel_UriSchemeK4035456475.h"
#include "System_ServiceModel_System_ServiceModel_WS2007Feder502258771.h"
#include "System_ServiceModel_System_ServiceModel_WS2007Http3179680017.h"
#include "System_ServiceModel_System_ServiceModel_WSDualHttp3779174110.h"
#include "System_ServiceModel_System_ServiceModel_WSFederati1523044440.h"
#include "System_ServiceModel_System_ServiceModel_WSFederati3296238232.h"
#include "System_ServiceModel_System_ServiceModel_WSHttpBind3299795862.h"
#include "System_ServiceModel_System_ServiceModel_WSHttpBind2163194083.h"
#include "System_ServiceModel_System_ServiceModel_WSHttpSecu1511783750.h"
#include "System_ServiceModel_System_ServiceModel_X509Certif3008261117.h"
#include "System_ServiceModel_System_ServiceModel_XmlSeriali2960276763.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Cer4091668218.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Cer3743405224.h"
#include "System_ServiceModel_Mono_Security_Protocol_Tls_Pri3240194217.h"
#include "System_ServiceModel_System_ServiceModel_Dispatcher4033177082.h"
#include "System_ServiceModel_System_ServiceModel_Security_T3476183706.h"
#include "System_ServiceModel_U3CPrivateImplementationDetail3057255361.h"
#include "System_ServiceModel_U3CPrivateImplementationDetail3244137463.h"
#include "System_ServiceModel_U3CPrivateImplementationDetail1630999355.h"
#include "UnityEngine_UI_U3CModuleU3E692745525.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5300 = { sizeof (UseManagedPresentationBindingElementImporter_t2903644797), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5301 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5301[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5302 = { sizeof (DuplexClientRuntimeChannel_t737641411), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5302[7] = 
{
	DuplexClientRuntimeChannel_t737641411::get_offset_of_callback_instance_16(),
	DuplexClientRuntimeChannel_t737641411::get_offset_of_loop_17(),
	DuplexClientRuntimeChannel_t737641411::get_offset_of_receive_timeout_18(),
	DuplexClientRuntimeChannel_t737641411::get_offset_of_receive_synchronously_19(),
	DuplexClientRuntimeChannel_t737641411::get_offset_of_loop_result_20(),
	DuplexClientRuntimeChannel_t737641411::get_offset_of_loop_handle_21(),
	DuplexClientRuntimeChannel_t737641411::get_offset_of_finish_handle_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5303 = { sizeof (EndpointAddress_t3119842923), -1, sizeof(EndpointAddress_t3119842923_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5303[8] = 
{
	EndpointAddress_t3119842923_StaticFields::get_offset_of_w3c_anonymous_0(),
	EndpointAddress_t3119842923_StaticFields::get_offset_of_anonymous_role_1(),
	EndpointAddress_t3119842923_StaticFields::get_offset_of_none_role_2(),
	EndpointAddress_t3119842923::get_offset_of_address_3(),
	EndpointAddress_t3119842923::get_offset_of_headers_4(),
	EndpointAddress_t3119842923::get_offset_of_identity_5(),
	EndpointAddress_t3119842923::get_offset_of_metadata_reader_6(),
	EndpointAddress_t3119842923::get_offset_of_extension_reader_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5304 = { sizeof (EndpointAddress10_t2657313230), -1, sizeof(EndpointAddress10_t2657313230_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5304[2] = 
{
	EndpointAddress10_t2657313230_StaticFields::get_offset_of_w3c_anonymous_0(),
	EndpointAddress10_t2657313230::get_offset_of_address_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5305 = { sizeof (EndpointAddressAugust2004_t2643301447), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5305[1] = 
{
	EndpointAddressAugust2004_t2643301447::get_offset_of_address_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5306 = { sizeof (EndpointIdentity_t3899230012), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5306[2] = 
{
	EndpointIdentity_t3899230012::get_offset_of_claim_0(),
	EndpointIdentity_t3899230012::get_offset_of_comparer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5307 = { sizeof (EndpointNotFoundException_t1807428957), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5308 = { sizeof (EnvelopeVersion_t2487833488), -1, sizeof(EnvelopeVersion_t2487833488_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5308[7] = 
{
	EnvelopeVersion_t2487833488::get_offset_of_name_0(),
	EnvelopeVersion_t2487833488::get_offset_of_uri_1(),
	EnvelopeVersion_t2487833488::get_offset_of_next_destination_2(),
	EnvelopeVersion_t2487833488::get_offset_of_ultimate_destination_3(),
	EnvelopeVersion_t2487833488_StaticFields::get_offset_of_soap11_4(),
	EnvelopeVersion_t2487833488_StaticFields::get_offset_of_soap12_5(),
	EnvelopeVersion_t2487833488_StaticFields::get_offset_of_none_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5309 = { sizeof (ExceptionDetail_t3678749343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5309[4] = 
{
	ExceptionDetail_t3678749343::get_offset_of_U3CInnerExceptionU3Ek__BackingField_0(),
	ExceptionDetail_t3678749343::get_offset_of_U3CMessageU3Ek__BackingField_1(),
	ExceptionDetail_t3678749343::get_offset_of_U3CStackTraceU3Ek__BackingField_2(),
	ExceptionDetail_t3678749343::get_offset_of_U3CTypeU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5310 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5310[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5311 = { sizeof (FaultCode_t3080963035), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5311[3] = 
{
	FaultCode_t3080963035::get_offset_of_name_0(),
	FaultCode_t3080963035::get_offset_of_ns_1(),
	FaultCode_t3080963035::get_offset_of_subcode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5312 = { sizeof (FaultContractAttribute_t3255918006), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5312[4] = 
{
	FaultContractAttribute_t3255918006::get_offset_of_action_0(),
	FaultContractAttribute_t3255918006::get_offset_of_name_1(),
	FaultContractAttribute_t3255918006::get_offset_of_ns_2(),
	FaultContractAttribute_t3255918006::get_offset_of_detail_type_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5313 = { sizeof (FaultException_t2858892349), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5313[2] = 
{
	FaultException_t2858892349::get_offset_of_fault_11(),
	FaultException_t2858892349::get_offset_of_action_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5314 = { sizeof (FaultReason_t2245999938), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5314[2] = 
{
	FaultReason_t2245999938::get_offset_of_trans_0(),
	FaultReason_t2245999938::get_offset_of_public_trans_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5315 = { sizeof (FaultReasonText_t3547351049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5315[2] = 
{
	FaultReasonText_t3547351049::get_offset_of_text_0(),
	FaultReasonText_t3547351049::get_offset_of_xmllang_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5316 = { sizeof (FederatedMessageSecurityOverHttp_t2993240700), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5316[1] = 
{
	FederatedMessageSecurityOverHttp_t2993240700::get_offset_of_negotiate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5317 = { sizeof (HttpTransportSecurity_t2136062604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5317[1] = 
{
	HttpTransportSecurity_t2136062604::get_offset_of_client_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5318 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5319 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5320 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5321 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5322 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5323 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5324 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5325 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5326 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5327 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5328 = { sizeof (InstanceContext_t3593205954), -1, sizeof(InstanceContext_t3593205954_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5328[6] = 
{
	InstanceContext_t3593205954::get_offset_of_host_9(),
	InstanceContext_t3593205954::get_offset_of_implementation_10(),
	InstanceContext_t3593205954::get_offset_of__behavior_11(),
	InstanceContext_t3593205954::get_offset_of_is_user_instance_provider_12(),
	InstanceContext_t3593205954::get_offset_of_is_user_context_provider_13(),
	InstanceContext_t3593205954_StaticFields::get_offset_of_idle_callback_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5329 = { sizeof (MessageBodyMemberAttribute_t2473986057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5329[1] = 
{
	MessageBodyMemberAttribute_t2473986057::get_offset_of_order_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5330 = { sizeof (MessageContractAttribute_t292133743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5330[5] = 
{
	MessageContractAttribute_t292133743::get_offset_of_has_protection_level_0(),
	MessageContractAttribute_t292133743::get_offset_of_is_wrapped_1(),
	MessageContractAttribute_t292133743::get_offset_of_name_2(),
	MessageContractAttribute_t292133743::get_offset_of_ns_3(),
	MessageContractAttribute_t292133743::get_offset_of_protection_level_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5331 = { sizeof (MessageContractMemberAttribute_t680184223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5331[2] = 
{
	MessageContractMemberAttribute_t680184223::get_offset_of_name_0(),
	MessageContractMemberAttribute_t680184223::get_offset_of_ns_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5332 = { sizeof (MessageHeaderException_t2022966637), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5333 = { sizeof (MessageParameterAttribute_t3244018206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5333[1] = 
{
	MessageParameterAttribute_t3244018206::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5334 = { sizeof (MessageSecurityOverHttp_t359378485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5334[2] = 
{
	MessageSecurityOverHttp_t359378485::get_offset_of_client_credential_type_0(),
	MessageSecurityOverHttp_t359378485::get_offset_of_negotiate_service_credential_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5335 = { sizeof (MessageSecurityOverTcp_t2089208958), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5335[2] = 
{
	MessageSecurityOverTcp_t2089208958::get_offset_of_alg_suite_0(),
	MessageSecurityOverTcp_t2089208958::get_offset_of_client_credential_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5336 = { sizeof (MessageSecurityVersion_t2079539966), -1, sizeof(MessageSecurityVersion_t2079539966_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5336[8] = 
{
	MessageSecurityVersion_t2079539966_StaticFields::get_offset_of_wss10_basic_0(),
	MessageSecurityVersion_t2079539966_StaticFields::get_offset_of_wss11_1(),
	MessageSecurityVersion_t2079539966_StaticFields::get_offset_of_wss11_basic_2(),
	MessageSecurityVersion_t2079539966_StaticFields::get_offset_of_wss10_2007_basic_3(),
	MessageSecurityVersion_t2079539966_StaticFields::get_offset_of_wss11_2007_basic_4(),
	MessageSecurityVersion_t2079539966_StaticFields::get_offset_of_wss11_2007_5(),
	MessageSecurityVersion_t2079539966::get_offset_of_U3CSecureConversationVersionU3Ek__BackingField_6(),
	MessageSecurityVersion_t2079539966::get_offset_of_U3CTrustVersionU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5337 = { sizeof (MessageSecurityTokenVersion_t2151590395), -1, sizeof(MessageSecurityTokenVersion_t2151590395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5337[7] = 
{
	MessageSecurityTokenVersion_t2151590395_StaticFields::get_offset_of_specs10_profile_source_0(),
	MessageSecurityTokenVersion_t2151590395_StaticFields::get_offset_of_specs11_source_1(),
	MessageSecurityTokenVersion_t2151590395_StaticFields::get_offset_of_specs11_profile_source_2(),
	MessageSecurityTokenVersion_t2151590395_StaticFields::get_offset_of_wss10basic_3(),
	MessageSecurityTokenVersion_t2151590395_StaticFields::get_offset_of_wss11_4(),
	MessageSecurityTokenVersion_t2151590395_StaticFields::get_offset_of_wss11basic_5(),
	MessageSecurityTokenVersion_t2151590395::get_offset_of_specs_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5338 = { sizeof (MessageSecurityVersionImpl_t66298087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5338[3] = 
{
	MessageSecurityVersionImpl_t66298087::get_offset_of_wss11_8(),
	MessageSecurityVersionImpl_t66298087::get_offset_of_basic_profile_9(),
	MessageSecurityVersionImpl_t66298087::get_offset_of_use2007_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5339 = { sizeof (MsmqBindingBase_t2036657955), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5339[12] = 
{
	MsmqBindingBase_t2036657955::get_offset_of_custom_dead_letter_queue_6(),
	MsmqBindingBase_t2036657955::get_offset_of_dead_letter_queue_7(),
	MsmqBindingBase_t2036657955::get_offset_of_durable_8(),
	MsmqBindingBase_t2036657955::get_offset_of_exactly_once_9(),
	MsmqBindingBase_t2036657955::get_offset_of_use_msmq_trace_10(),
	MsmqBindingBase_t2036657955::get_offset_of_use_source_journal_11(),
	MsmqBindingBase_t2036657955::get_offset_of_max_retry_cycles_12(),
	MsmqBindingBase_t2036657955::get_offset_of_receive_retry_count_13(),
	MsmqBindingBase_t2036657955::get_offset_of_max_recv_msg_size_14(),
	MsmqBindingBase_t2036657955::get_offset_of_receive_error_handling_15(),
	MsmqBindingBase_t2036657955::get_offset_of_retry_cycle_delay_16(),
	MsmqBindingBase_t2036657955::get_offset_of_ttl_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5340 = { sizeof (MsmqTransportSecurity_t2605141420), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5340[4] = 
{
	MsmqTransportSecurity_t2605141420::get_offset_of_auth_0(),
	MsmqTransportSecurity_t2605141420::get_offset_of_enc_1(),
	MsmqTransportSecurity_t2605141420::get_offset_of_hash_2(),
	MsmqTransportSecurity_t2605141420::get_offset_of_protection_level_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5341 = { sizeof (NetMsmqBinding_t3496970231), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5341[5] = 
{
	NetMsmqBinding_t3496970231::get_offset_of_security_18(),
	NetMsmqBinding_t3496970231::get_offset_of_use_ad_19(),
	NetMsmqBinding_t3496970231::get_offset_of_max_buffer_pool_size_20(),
	NetMsmqBinding_t3496970231::get_offset_of_queue_tr_protocol_21(),
	NetMsmqBinding_t3496970231::get_offset_of_quotas_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5342 = { sizeof (NetMsmqSecurity_t1571703333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5342[1] = 
{
	NetMsmqSecurity_t1571703333::get_offset_of_transport_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5343 = { sizeof (NetNamedPipeBinding_t983330350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5343[2] = 
{
	NetNamedPipeBinding_t983330350::get_offset_of_reader_quotas_6(),
	NetNamedPipeBinding_t983330350::get_offset_of_transport_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5344 = { sizeof (NetPeerTcpBinding_t3507636973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5344[4] = 
{
	NetPeerTcpBinding_t3507636973::get_offset_of_reader_quotas_6(),
	NetPeerTcpBinding_t3507636973::get_offset_of_resolver_7(),
	NetPeerTcpBinding_t3507636973::get_offset_of_security_8(),
	NetPeerTcpBinding_t3507636973::get_offset_of_transport_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5345 = { sizeof (NetTcpBinding_t2266210195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5345[6] = 
{
	NetTcpBinding_t2266210195::get_offset_of_max_conn_6(),
	NetTcpBinding_t2266210195::get_offset_of_security_7(),
	NetTcpBinding_t2266210195::get_offset_of_reader_quotas_8(),
	NetTcpBinding_t2266210195::get_offset_of_transaction_flow_9(),
	NetTcpBinding_t2266210195::get_offset_of_transaction_protocol_10(),
	NetTcpBinding_t2266210195::get_offset_of_transport_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5346 = { sizeof (NetTcpSecurity_t856315759), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5346[3] = 
{
	NetTcpSecurity_t856315759::get_offset_of_message_0(),
	NetTcpSecurity_t856315759::get_offset_of_mode_1(),
	NetTcpSecurity_t856315759::get_offset_of_transport_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5347 = { sizeof (NonDualMessageSecurityOverHttp_t883223478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5347[1] = 
{
	NonDualMessageSecurityOverHttp_t883223478::get_offset_of_establish_sec_ctx_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5348 = { sizeof (OperationBehaviorAttribute_t1398166452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5348[5] = 
{
	OperationBehaviorAttribute_t1398166452::get_offset_of_impersonation_0(),
	OperationBehaviorAttribute_t1398166452::get_offset_of_tx_auto_complete_1(),
	OperationBehaviorAttribute_t1398166452::get_offset_of_tx_scope_required_2(),
	OperationBehaviorAttribute_t1398166452::get_offset_of_auto_dispose_params_3(),
	OperationBehaviorAttribute_t1398166452::get_offset_of_mode_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5349 = { sizeof (OperationContext_t2829644788), -1, 0, sizeof(OperationContext_t2829644788_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable5349[9] = 
{
	THREAD_STATIC_FIELD_OFFSET,
	OperationContext_t2829644788::get_offset_of_incoming_message_1(),
	OperationContext_t2829644788::get_offset_of_dispatcher_2(),
	OperationContext_t2829644788::get_offset_of_channel_3(),
	OperationContext_t2829644788::get_offset_of_request_ctx_4(),
	OperationContext_t2829644788::get_offset_of_outgoing_headers_5(),
	OperationContext_t2829644788::get_offset_of_outgoing_properties_6(),
	OperationContext_t2829644788::get_offset_of_instance_context_7(),
	OperationContext_t2829644788::get_offset_of_U3CIsUserContextU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5350 = { sizeof (OperationContextScope_t3090301225), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5350[1] = 
{
	OperationContextScope_t3090301225::get_offset_of_previous_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5351 = { sizeof (OperationContractAttribute_t3102514551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5351[9] = 
{
	OperationContractAttribute_t3102514551::get_offset_of_action_0(),
	OperationContractAttribute_t3102514551::get_offset_of_reply_action_1(),
	OperationContractAttribute_t3102514551::get_offset_of_name_2(),
	OperationContractAttribute_t3102514551::get_offset_of_is_initiating_3(),
	OperationContractAttribute_t3102514551::get_offset_of_is_terminating_4(),
	OperationContractAttribute_t3102514551::get_offset_of_is_oneway_5(),
	OperationContractAttribute_t3102514551::get_offset_of_is_async_6(),
	OperationContractAttribute_t3102514551::get_offset_of_protection_level_7(),
	OperationContractAttribute_t3102514551::get_offset_of_has_protection_level_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5352 = { sizeof (PeerNode_t3248961724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5352[7] = 
{
	PeerNode_t3248961724::get_offset_of_Offline_0(),
	PeerNode_t3248961724::get_offset_of_Online_1(),
	PeerNode_t3248961724::get_offset_of_U3CIsOnlineU3Ek__BackingField_2(),
	PeerNode_t3248961724::get_offset_of_U3CMeshIdU3Ek__BackingField_3(),
	PeerNode_t3248961724::get_offset_of_U3CNodeIdU3Ek__BackingField_4(),
	PeerNode_t3248961724::get_offset_of_U3CRegisteredIdU3Ek__BackingField_5(),
	PeerNode_t3248961724::get_offset_of_U3CPortU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5353 = { sizeof (PeerNodeImpl_t609519394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5353[1] = 
{
	PeerNodeImpl_t609519394::get_offset_of_listen_address_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5354 = { sizeof (PeerNodeAddress_t2098027372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5354[2] = 
{
	PeerNodeAddress_t2098027372::get_offset_of_endpoint_0(),
	PeerNodeAddress_t2098027372::get_offset_of_peer_addresses_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5355 = { sizeof (PeerResolver_t1567980581), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5356 = { sizeof (PeerSecuritySettings_t2125602112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5356[2] = 
{
	PeerSecuritySettings_t2125602112::get_offset_of_mode_0(),
	PeerSecuritySettings_t2125602112::get_offset_of_U3CTransportU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5357 = { sizeof (PeerTransportSecuritySettings_t3643429466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5357[1] = 
{
	PeerTransportSecuritySettings_t3643429466::get_offset_of_U3CCredentialTypeU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5358 = { sizeof (ProtocolException_t898429968), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5359 = { sizeof (RsaEndpointIdentity_t2589302870), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5360 = { sizeof (ServiceAuthorizationManager_t56182799), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5361 = { sizeof (ServiceBehaviorAttribute_t1657799891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5361[4] = 
{
	ServiceBehaviorAttribute_t1657799891::get_offset_of_singleton_0(),
	ServiceBehaviorAttribute_t1657799891::get_offset_of_U3CAddressFilterModeU3Ek__BackingField_1(),
	ServiceBehaviorAttribute_t1657799891::get_offset_of_U3CInstanceContextModeU3Ek__BackingField_2(),
	ServiceBehaviorAttribute_t1657799891::get_offset_of_U3CIncludeExceptionDetailInFaultsU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5362 = { sizeof (ServiceContractAttribute_t4131169907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5362[7] = 
{
	ServiceContractAttribute_t4131169907::get_offset_of_callback_contract_0(),
	ServiceContractAttribute_t4131169907::get_offset_of_name_1(),
	ServiceContractAttribute_t4131169907::get_offset_of_ns_2(),
	ServiceContractAttribute_t4131169907::get_offset_of_session_3(),
	ServiceContractAttribute_t4131169907::get_offset_of_protection_level_4(),
	ServiceContractAttribute_t4131169907::get_offset_of_has_protection_level_5(),
	ServiceContractAttribute_t4131169907::get_offset_of__configurationName_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5363 = { sizeof (ServiceProxyGenerator_t4233789185), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5364 = { sizeof (ServiceHost_t1894294968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5364[3] = 
{
	ServiceHost_t1894294968::get_offset_of_service_type_26(),
	ServiceHost_t1894294968::get_offset_of_instance_27(),
	ServiceHost_t1894294968::get_offset_of_contracts_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5365 = { sizeof (ServiceHostBase_t3741910535), -1, sizeof(ServiceHostBase_t3741910535_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5365[17] = 
{
	ServiceHostBase_t3741910535::get_offset_of_credentials_9(),
	ServiceHostBase_t3741910535::get_offset_of_description_10(),
	ServiceHostBase_t3741910535::get_offset_of_base_addresses_11(),
	ServiceHostBase_t3741910535::get_offset_of_open_timeout_12(),
	ServiceHostBase_t3741910535::get_offset_of_close_timeout_13(),
	ServiceHostBase_t3741910535::get_offset_of_throttle_14(),
	ServiceHostBase_t3741910535::get_offset_of_contexts_15(),
	ServiceHostBase_t3741910535::get_offset_of_exposed_contexts_16(),
	ServiceHostBase_t3741910535::get_offset_of_channel_dispatchers_17(),
	ServiceHostBase_t3741910535::get_offset_of_contracts_18(),
	ServiceHostBase_t3741910535::get_offset_of_flow_limit_19(),
	ServiceHostBase_t3741910535::get_offset_of_extensions_20(),
	ServiceHostBase_t3741910535::get_offset_of_mex_contract_21(),
	ServiceHostBase_t3741910535::get_offset_of_help_page_contract_22(),
	ServiceHostBase_t3741910535::get_offset_of_UnknownMessageReceived_23(),
	ServiceHostBase_t3741910535::get_offset_of_U3CAuthorizationU3Ek__BackingField_24(),
	ServiceHostBase_t3741910535_StaticFields::get_offset_of_U3CU3Ef__switchU24map2_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5366 = { sizeof (DispatcherBuilder_t3637350213), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5367 = { sizeof (ServiceHostingEnvironment_t465623925), -1, sizeof(ServiceHostingEnvironment_t465623925_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5367[1] = 
{
	ServiceHostingEnvironment_t465623925_StaticFields::get_offset_of_U3CInAspNetU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5368 = { sizeof (ServiceKnownTypeAttribute_t1229054133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5368[3] = 
{
	ServiceKnownTypeAttribute_t1229054133::get_offset_of_method_0(),
	ServiceKnownTypeAttribute_t1229054133::get_offset_of_declaring_type_1(),
	ServiceKnownTypeAttribute_t1229054133::get_offset_of_type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5369 = { sizeof (DuplexServiceRuntimeChannel_t4048757758), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5369[1] = 
{
	DuplexServiceRuntimeChannel_t4048757758::get_offset_of_client_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5370 = { sizeof (ServiceRuntimeChannel_t1028057095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5370[3] = 
{
	ServiceRuntimeChannel_t1028057095::get_offset_of_channel_9(),
	ServiceRuntimeChannel_t1028057095::get_offset_of_runtime_10(),
	ServiceRuntimeChannel_t1028057095::get_offset_of_U3COperationTimeoutU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5371 = { sizeof (ServiceSecurityContext_t3919255606), -1, sizeof(ServiceSecurityContext_t3919255606_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5371[4] = 
{
	ServiceSecurityContext_t3919255606_StaticFields::get_offset_of_anonymous_0(),
	ServiceSecurityContext_t3919255606::get_offset_of_context_1(),
	ServiceSecurityContext_t3919255606::get_offset_of_policies_2(),
	ServiceSecurityContext_t3919255606::get_offset_of_primary_identity_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5372 = { sizeof (SpnEndpointIdentity_t3481830764), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5373 = { sizeof (TcpTransportSecurity_t3663278697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5373[2] = 
{
	TcpTransportSecurity_t3663278697::get_offset_of_client_0(),
	TcpTransportSecurity_t3663278697::get_offset_of_protection_level_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5374 = { sizeof (TransactionProtocol_t3972232485), -1, sizeof(TransactionProtocol_t3972232485_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5374[2] = 
{
	TransactionProtocol_t3972232485_StaticFields::get_offset_of_wsat_0(),
	TransactionProtocol_t3972232485_StaticFields::get_offset_of_oletx_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5375 = { sizeof (WSAtomicTransactionProtocol_t759410032), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5376 = { sizeof (OleTransactionProtocol_t854629616), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5377 = { sizeof (TransferMode_t436880017)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5377[5] = 
{
	TransferMode_t436880017::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5378 = { sizeof (UnknownMessageReceivedEventArgs_t1694867822), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5378[1] = 
{
	UnknownMessageReceivedEventArgs_t1694867822::get_offset_of_message_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5379 = { sizeof (UpnEndpointIdentity_t3463235294), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5380 = { sizeof (UriSchemeKeyedCollection_t4035456475), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5381 = { sizeof (WS2007FederationHttpBinding_t502258771), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5382 = { sizeof (WS2007HttpBinding_t3179680017), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5383 = { sizeof (WSDualHttpBinding_t3779174110), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5384 = { sizeof (WSFederationHttpBinding_t1523044440), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5384[1] = 
{
	WSFederationHttpBinding_t1523044440::get_offset_of_security_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5385 = { sizeof (WSFederationHttpSecurity_t3296238232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5385[2] = 
{
	WSFederationHttpSecurity_t3296238232::get_offset_of_mode_0(),
	WSFederationHttpSecurity_t3296238232::get_offset_of_message_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5386 = { sizeof (WSHttpBinding_t3299795862), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5386[1] = 
{
	WSHttpBinding_t3299795862::get_offset_of_security_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5387 = { sizeof (WSHttpBindingBase_t2163194083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5387[3] = 
{
	WSHttpBindingBase_t2163194083::get_offset_of_message_encoding_6(),
	WSHttpBindingBase_t2163194083::get_offset_of_env_version_7(),
	WSHttpBindingBase_t2163194083::get_offset_of_text_encoding_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5388 = { sizeof (WSHttpSecurity_t1511783750), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5388[2] = 
{
	WSHttpSecurity_t1511783750::get_offset_of_mode_0(),
	WSHttpSecurity_t1511783750::get_offset_of_message_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5389 = { sizeof (X509CertificateEndpointIdentity_t3008261117), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5389[3] = 
{
	X509CertificateEndpointIdentity_t3008261117::get_offset_of_primary_2(),
	X509CertificateEndpointIdentity_t3008261117::get_offset_of_supporting_3(),
	X509CertificateEndpointIdentity_t3008261117::get_offset_of_all_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5390 = { sizeof (XmlSerializerFormatAttribute_t2960276763), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5391 = { sizeof (CertificateValidationCallback_t4091668219), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5392 = { sizeof (CertificateSelectionCallback_t3743405225), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5393 = { sizeof (PrivateKeySelectionCallback_t3240194218), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5394 = { sizeof (InstanceContextIdleCallback_t4033177082), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5395 = { sizeof (IssuedSecurityTokenHandler_t3476183706), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5396 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255372), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5396[8] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields::get_offset_of_U24U24fieldU2D1_1(),
	U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields::get_offset_of_U24U24fieldU2D2_2(),
	U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields::get_offset_of_U24U24fieldU2D3_3(),
	U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields::get_offset_of_U24U24fieldU2D4_4(),
	U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields::get_offset_of_U24U24fieldU2D5_5(),
	U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields::get_offset_of_U24U24fieldU2D6_6(),
	U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields::get_offset_of_U24U24fieldU2D7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5397 = { sizeof (U24ArrayTypeU248_t3244137469)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU248_t3244137469 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5398 = { sizeof (U24ArrayTypeU244_t1630999356)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU244_t1630999356 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5399 = { sizeof (U3CModuleU3E_t692745555), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
