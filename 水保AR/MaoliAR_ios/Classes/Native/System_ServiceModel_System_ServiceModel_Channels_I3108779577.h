﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_R1473296135.h"

// System.ServiceModel.IDefaultCommunicationTimeouts
struct IDefaultCommunicationTimeouts_t2848643016;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.InternalRequestContext
struct  InternalRequestContext_t3108779577  : public RequestContext_t1473296135
{
public:
	// System.ServiceModel.IDefaultCommunicationTimeouts System.ServiceModel.Channels.InternalRequestContext::timeouts
	Il2CppObject * ___timeouts_0;

public:
	inline static int32_t get_offset_of_timeouts_0() { return static_cast<int32_t>(offsetof(InternalRequestContext_t3108779577, ___timeouts_0)); }
	inline Il2CppObject * get_timeouts_0() const { return ___timeouts_0; }
	inline Il2CppObject ** get_address_of_timeouts_0() { return &___timeouts_0; }
	inline void set_timeouts_0(Il2CppObject * value)
	{
		___timeouts_0 = value;
		Il2CppCodeGenWriteBarrier(&___timeouts_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
