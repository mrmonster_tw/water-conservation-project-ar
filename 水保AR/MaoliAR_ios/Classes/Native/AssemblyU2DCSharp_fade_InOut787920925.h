﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"

// UnityEngine.UI.Image
struct Image_t2670269651;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// fade_InOut
struct  fade_InOut_t787920925  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image fade_InOut::Black
	Image_t2670269651 * ___Black_2;
	// System.Boolean fade_InOut::Run
	bool ___Run_3;
	// System.Single fade_InOut::Speed
	float ___Speed_4;
	// System.Single fade_InOut::Max
	float ___Max_5;
	// UnityEngine.Color fade_InOut::temp
	Color_t2555686324  ___temp_6;

public:
	inline static int32_t get_offset_of_Black_2() { return static_cast<int32_t>(offsetof(fade_InOut_t787920925, ___Black_2)); }
	inline Image_t2670269651 * get_Black_2() const { return ___Black_2; }
	inline Image_t2670269651 ** get_address_of_Black_2() { return &___Black_2; }
	inline void set_Black_2(Image_t2670269651 * value)
	{
		___Black_2 = value;
		Il2CppCodeGenWriteBarrier(&___Black_2, value);
	}

	inline static int32_t get_offset_of_Run_3() { return static_cast<int32_t>(offsetof(fade_InOut_t787920925, ___Run_3)); }
	inline bool get_Run_3() const { return ___Run_3; }
	inline bool* get_address_of_Run_3() { return &___Run_3; }
	inline void set_Run_3(bool value)
	{
		___Run_3 = value;
	}

	inline static int32_t get_offset_of_Speed_4() { return static_cast<int32_t>(offsetof(fade_InOut_t787920925, ___Speed_4)); }
	inline float get_Speed_4() const { return ___Speed_4; }
	inline float* get_address_of_Speed_4() { return &___Speed_4; }
	inline void set_Speed_4(float value)
	{
		___Speed_4 = value;
	}

	inline static int32_t get_offset_of_Max_5() { return static_cast<int32_t>(offsetof(fade_InOut_t787920925, ___Max_5)); }
	inline float get_Max_5() const { return ___Max_5; }
	inline float* get_address_of_Max_5() { return &___Max_5; }
	inline void set_Max_5(float value)
	{
		___Max_5 = value;
	}

	inline static int32_t get_offset_of_temp_6() { return static_cast<int32_t>(offsetof(fade_InOut_t787920925, ___temp_6)); }
	inline Color_t2555686324  get_temp_6() const { return ___temp_6; }
	inline Color_t2555686324 * get_address_of_temp_6() { return &___temp_6; }
	inline void set_temp_6(Color_t2555686324  value)
	{
		___temp_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
