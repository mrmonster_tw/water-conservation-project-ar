﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String[]
struct StringU5BU5D_t1281789340;
// System.Web.UI.StateBag
struct StateBag_t282928164;
// System.Web.UI.WebControls.Style
struct Style_t3589053173;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.WebControls.FontInfo
struct  FontInfo_t1360706239  : public Il2CppObject
{
public:
	// System.Web.UI.StateBag System.Web.UI.WebControls.FontInfo::bag
	StateBag_t282928164 * ___bag_1;
	// System.Web.UI.WebControls.Style System.Web.UI.WebControls.FontInfo::_owner
	Style_t3589053173 * ____owner_2;

public:
	inline static int32_t get_offset_of_bag_1() { return static_cast<int32_t>(offsetof(FontInfo_t1360706239, ___bag_1)); }
	inline StateBag_t282928164 * get_bag_1() const { return ___bag_1; }
	inline StateBag_t282928164 ** get_address_of_bag_1() { return &___bag_1; }
	inline void set_bag_1(StateBag_t282928164 * value)
	{
		___bag_1 = value;
		Il2CppCodeGenWriteBarrier(&___bag_1, value);
	}

	inline static int32_t get_offset_of__owner_2() { return static_cast<int32_t>(offsetof(FontInfo_t1360706239, ____owner_2)); }
	inline Style_t3589053173 * get__owner_2() const { return ____owner_2; }
	inline Style_t3589053173 ** get_address_of__owner_2() { return &____owner_2; }
	inline void set__owner_2(Style_t3589053173 * value)
	{
		____owner_2 = value;
		Il2CppCodeGenWriteBarrier(&____owner_2, value);
	}
};

struct FontInfo_t1360706239_StaticFields
{
public:
	// System.String[] System.Web.UI.WebControls.FontInfo::empty_names
	StringU5BU5D_t1281789340* ___empty_names_0;

public:
	inline static int32_t get_offset_of_empty_names_0() { return static_cast<int32_t>(offsetof(FontInfo_t1360706239_StaticFields, ___empty_names_0)); }
	inline StringU5BU5D_t1281789340* get_empty_names_0() const { return ___empty_names_0; }
	inline StringU5BU5D_t1281789340** get_address_of_empty_names_0() { return &___empty_names_0; }
	inline void set_empty_names_0(StringU5BU5D_t1281789340* value)
	{
		___empty_names_0 = value;
		Il2CppCodeGenWriteBarrier(&___empty_names_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
