﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"

// System.String
struct String_t;
// System.Type[]
struct TypeU5BU5D_t3940880105;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Configuration.XmlFormatExtensionAttribute
struct  XmlFormatExtensionAttribute_t3725372622  : public Attribute_t861562559
{
public:
	// System.String System.Web.Services.Configuration.XmlFormatExtensionAttribute::elementName
	String_t* ___elementName_0;
	// System.String System.Web.Services.Configuration.XmlFormatExtensionAttribute::ns
	String_t* ___ns_1;
	// System.Type[] System.Web.Services.Configuration.XmlFormatExtensionAttribute::extensionPoints
	TypeU5BU5D_t3940880105* ___extensionPoints_2;

public:
	inline static int32_t get_offset_of_elementName_0() { return static_cast<int32_t>(offsetof(XmlFormatExtensionAttribute_t3725372622, ___elementName_0)); }
	inline String_t* get_elementName_0() const { return ___elementName_0; }
	inline String_t** get_address_of_elementName_0() { return &___elementName_0; }
	inline void set_elementName_0(String_t* value)
	{
		___elementName_0 = value;
		Il2CppCodeGenWriteBarrier(&___elementName_0, value);
	}

	inline static int32_t get_offset_of_ns_1() { return static_cast<int32_t>(offsetof(XmlFormatExtensionAttribute_t3725372622, ___ns_1)); }
	inline String_t* get_ns_1() const { return ___ns_1; }
	inline String_t** get_address_of_ns_1() { return &___ns_1; }
	inline void set_ns_1(String_t* value)
	{
		___ns_1 = value;
		Il2CppCodeGenWriteBarrier(&___ns_1, value);
	}

	inline static int32_t get_offset_of_extensionPoints_2() { return static_cast<int32_t>(offsetof(XmlFormatExtensionAttribute_t3725372622, ___extensionPoints_2)); }
	inline TypeU5BU5D_t3940880105* get_extensionPoints_2() const { return ___extensionPoints_2; }
	inline TypeU5BU5D_t3940880105** get_address_of_extensionPoints_2() { return &___extensionPoints_2; }
	inline void set_extensionPoints_2(TypeU5BU5D_t3940880105* value)
	{
		___extensionPoints_2 = value;
		Il2CppCodeGenWriteBarrier(&___extensionPoints_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
