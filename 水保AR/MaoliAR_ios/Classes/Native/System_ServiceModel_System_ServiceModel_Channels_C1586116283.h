﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Co518829156.h"

// System.ServiceModel.Channels.ChannelManagerBase
struct ChannelManagerBase_t740725699;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.ChannelBase
struct  ChannelBase_t1586116283  : public CommunicationObject_t518829156
{
public:
	// System.ServiceModel.Channels.ChannelManagerBase System.ServiceModel.Channels.ChannelBase::manager
	ChannelManagerBase_t740725699 * ___manager_9;

public:
	inline static int32_t get_offset_of_manager_9() { return static_cast<int32_t>(offsetof(ChannelBase_t1586116283, ___manager_9)); }
	inline ChannelManagerBase_t740725699 * get_manager_9() const { return ___manager_9; }
	inline ChannelManagerBase_t740725699 ** get_address_of_manager_9() { return &___manager_9; }
	inline void set_manager_9(ChannelManagerBase_t740725699 * value)
	{
		___manager_9 = value;
		Il2CppCodeGenWriteBarrier(&___manager_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
