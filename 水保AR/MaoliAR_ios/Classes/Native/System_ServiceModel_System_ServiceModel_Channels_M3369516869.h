﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>
struct List_1_t2440142076;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.MessageProperties/ParameterValueCollection
struct  ParameterValueCollection_t3369516869  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>> System.ServiceModel.Channels.MessageProperties/ParameterValueCollection::source
	List_1_t2440142076 * ___source_0;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(ParameterValueCollection_t3369516869, ___source_0)); }
	inline List_1_t2440142076 * get_source_0() const { return ___source_0; }
	inline List_1_t2440142076 ** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(List_1_t2440142076 * value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier(&___source_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
