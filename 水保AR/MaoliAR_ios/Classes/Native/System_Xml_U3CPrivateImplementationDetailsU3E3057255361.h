﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A3503460108.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A1547144958.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A1683523542.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A2333946162.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar774576741.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A1951609148.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A2490092596.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar520724128.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A3244137463.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A1929481982.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A4290130235.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255364  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType$208 <PrivateImplementationDetails>::$$field-0
	U24ArrayTypeU24208_t3503460108  ___U24U24fieldU2D0_0;
	// <PrivateImplementationDetails>/$ArrayType$208 <PrivateImplementationDetails>::$$field-1
	U24ArrayTypeU24208_t3503460108  ___U24U24fieldU2D1_1;
	// <PrivateImplementationDetails>/$ArrayType$236 <PrivateImplementationDetails>::$$field-2
	U24ArrayTypeU24236_t1547144958  ___U24U24fieldU2D2_2;
	// <PrivateImplementationDetails>/$ArrayType$72 <PrivateImplementationDetails>::$$field-3
	U24ArrayTypeU2472_t1683523543  ___U24U24fieldU2D3_3;
	// <PrivateImplementationDetails>/$ArrayType$236 <PrivateImplementationDetails>::$$field-4
	U24ArrayTypeU24236_t1547144958  ___U24U24fieldU2D4_4;
	// <PrivateImplementationDetails>/$ArrayType$236 <PrivateImplementationDetails>::$$field-5
	U24ArrayTypeU24236_t1547144958  ___U24U24fieldU2D5_5;
	// <PrivateImplementationDetails>/$ArrayType$72 <PrivateImplementationDetails>::$$field-6
	U24ArrayTypeU2472_t1683523543  ___U24U24fieldU2D6_6;
	// <PrivateImplementationDetails>/$ArrayType$1532 <PrivateImplementationDetails>::$$field-7
	U24ArrayTypeU241532_t2333946162  ___U24U24fieldU2D7_7;
	// <PrivateImplementationDetails>/$ArrayType$1532 <PrivateImplementationDetails>::$$field-8
	U24ArrayTypeU241532_t2333946162  ___U24U24fieldU2D8_8;
	// <PrivateImplementationDetails>/$ArrayType$208 <PrivateImplementationDetails>::$$field-9
	U24ArrayTypeU24208_t3503460108  ___U24U24fieldU2D9_9;
	// <PrivateImplementationDetails>/$ArrayType$208 <PrivateImplementationDetails>::$$field-10
	U24ArrayTypeU24208_t3503460108  ___U24U24fieldU2D10_10;
	// <PrivateImplementationDetails>/$ArrayType$304 <PrivateImplementationDetails>::$$field-11
	U24ArrayTypeU24304_t774576741  ___U24U24fieldU2D11_11;
	// <PrivateImplementationDetails>/$ArrayType$72 <PrivateImplementationDetails>::$$field-12
	U24ArrayTypeU2472_t1683523543  ___U24U24fieldU2D12_12;
	// <PrivateImplementationDetails>/$ArrayType$304 <PrivateImplementationDetails>::$$field-13
	U24ArrayTypeU24304_t774576741  ___U24U24fieldU2D13_13;
	// <PrivateImplementationDetails>/$ArrayType$304 <PrivateImplementationDetails>::$$field-14
	U24ArrayTypeU24304_t774576741  ___U24U24fieldU2D14_14;
	// <PrivateImplementationDetails>/$ArrayType$72 <PrivateImplementationDetails>::$$field-15
	U24ArrayTypeU2472_t1683523543  ___U24U24fieldU2D15_15;
	// <PrivateImplementationDetails>/$ArrayType$1392 <PrivateImplementationDetails>::$$field-16
	U24ArrayTypeU241392_t1951609148  ___U24U24fieldU2D16_16;
	// <PrivateImplementationDetails>/$ArrayType$1392 <PrivateImplementationDetails>::$$field-17
	U24ArrayTypeU241392_t1951609148  ___U24U24fieldU2D17_17;
	// <PrivateImplementationDetails>/$ArrayType$12 <PrivateImplementationDetails>::$$field-18
	U24ArrayTypeU2412_t2490092598  ___U24U24fieldU2D18_18;
	// <PrivateImplementationDetails>/$ArrayType$52 <PrivateImplementationDetails>::$$field-19
	U24ArrayTypeU2452_t520724129  ___U24U24fieldU2D19_19;
	// <PrivateImplementationDetails>/$ArrayType$52 <PrivateImplementationDetails>::$$field-20
	U24ArrayTypeU2452_t520724129  ___U24U24fieldU2D20_20;
	// <PrivateImplementationDetails>/$ArrayType$52 <PrivateImplementationDetails>::$$field-21
	U24ArrayTypeU2452_t520724129  ___U24U24fieldU2D21_21;
	// <PrivateImplementationDetails>/$ArrayType$8 <PrivateImplementationDetails>::$$field-22
	U24ArrayTypeU248_t3244137465  ___U24U24fieldU2D22_22;
	// <PrivateImplementationDetails>/$ArrayType$8 <PrivateImplementationDetails>::$$field-36
	U24ArrayTypeU248_t3244137465  ___U24U24fieldU2D36_23;
	// <PrivateImplementationDetails>/$ArrayType$256 <PrivateImplementationDetails>::$$field-37
	U24ArrayTypeU24256_t1929481984  ___U24U24fieldU2D37_24;
	// <PrivateImplementationDetails>/$ArrayType$256 <PrivateImplementationDetails>::$$field-38
	U24ArrayTypeU24256_t1929481984  ___U24U24fieldU2D38_25;
	// <PrivateImplementationDetails>/$ArrayType$1280 <PrivateImplementationDetails>::$$field-39
	U24ArrayTypeU241280_t4290130235  ___U24U24fieldU2D39_26;
	// <PrivateImplementationDetails>/$ArrayType$12 <PrivateImplementationDetails>::$$field-40
	U24ArrayTypeU2412_t2490092598  ___U24U24fieldU2D40_27;
	// <PrivateImplementationDetails>/$ArrayType$12 <PrivateImplementationDetails>::$$field-41
	U24ArrayTypeU2412_t2490092598  ___U24U24fieldU2D41_28;
	// <PrivateImplementationDetails>/$ArrayType$8 <PrivateImplementationDetails>::$$field-42
	U24ArrayTypeU248_t3244137465  ___U24U24fieldU2D42_29;
	// <PrivateImplementationDetails>/$ArrayType$8 <PrivateImplementationDetails>::$$field-43
	U24ArrayTypeU248_t3244137465  ___U24U24fieldU2D43_30;
	// <PrivateImplementationDetails>/$ArrayType$8 <PrivateImplementationDetails>::$$field-44
	U24ArrayTypeU248_t3244137465  ___U24U24fieldU2D44_31;

public:
	inline static int32_t get_offset_of_U24U24fieldU2D0_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D0_0)); }
	inline U24ArrayTypeU24208_t3503460108  get_U24U24fieldU2D0_0() const { return ___U24U24fieldU2D0_0; }
	inline U24ArrayTypeU24208_t3503460108 * get_address_of_U24U24fieldU2D0_0() { return &___U24U24fieldU2D0_0; }
	inline void set_U24U24fieldU2D0_0(U24ArrayTypeU24208_t3503460108  value)
	{
		___U24U24fieldU2D0_0 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D1_1)); }
	inline U24ArrayTypeU24208_t3503460108  get_U24U24fieldU2D1_1() const { return ___U24U24fieldU2D1_1; }
	inline U24ArrayTypeU24208_t3503460108 * get_address_of_U24U24fieldU2D1_1() { return &___U24U24fieldU2D1_1; }
	inline void set_U24U24fieldU2D1_1(U24ArrayTypeU24208_t3503460108  value)
	{
		___U24U24fieldU2D1_1 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D2_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D2_2)); }
	inline U24ArrayTypeU24236_t1547144958  get_U24U24fieldU2D2_2() const { return ___U24U24fieldU2D2_2; }
	inline U24ArrayTypeU24236_t1547144958 * get_address_of_U24U24fieldU2D2_2() { return &___U24U24fieldU2D2_2; }
	inline void set_U24U24fieldU2D2_2(U24ArrayTypeU24236_t1547144958  value)
	{
		___U24U24fieldU2D2_2 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D3_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D3_3)); }
	inline U24ArrayTypeU2472_t1683523543  get_U24U24fieldU2D3_3() const { return ___U24U24fieldU2D3_3; }
	inline U24ArrayTypeU2472_t1683523543 * get_address_of_U24U24fieldU2D3_3() { return &___U24U24fieldU2D3_3; }
	inline void set_U24U24fieldU2D3_3(U24ArrayTypeU2472_t1683523543  value)
	{
		___U24U24fieldU2D3_3 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D4_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D4_4)); }
	inline U24ArrayTypeU24236_t1547144958  get_U24U24fieldU2D4_4() const { return ___U24U24fieldU2D4_4; }
	inline U24ArrayTypeU24236_t1547144958 * get_address_of_U24U24fieldU2D4_4() { return &___U24U24fieldU2D4_4; }
	inline void set_U24U24fieldU2D4_4(U24ArrayTypeU24236_t1547144958  value)
	{
		___U24U24fieldU2D4_4 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D5_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D5_5)); }
	inline U24ArrayTypeU24236_t1547144958  get_U24U24fieldU2D5_5() const { return ___U24U24fieldU2D5_5; }
	inline U24ArrayTypeU24236_t1547144958 * get_address_of_U24U24fieldU2D5_5() { return &___U24U24fieldU2D5_5; }
	inline void set_U24U24fieldU2D5_5(U24ArrayTypeU24236_t1547144958  value)
	{
		___U24U24fieldU2D5_5 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D6_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D6_6)); }
	inline U24ArrayTypeU2472_t1683523543  get_U24U24fieldU2D6_6() const { return ___U24U24fieldU2D6_6; }
	inline U24ArrayTypeU2472_t1683523543 * get_address_of_U24U24fieldU2D6_6() { return &___U24U24fieldU2D6_6; }
	inline void set_U24U24fieldU2D6_6(U24ArrayTypeU2472_t1683523543  value)
	{
		___U24U24fieldU2D6_6 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D7_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D7_7)); }
	inline U24ArrayTypeU241532_t2333946162  get_U24U24fieldU2D7_7() const { return ___U24U24fieldU2D7_7; }
	inline U24ArrayTypeU241532_t2333946162 * get_address_of_U24U24fieldU2D7_7() { return &___U24U24fieldU2D7_7; }
	inline void set_U24U24fieldU2D7_7(U24ArrayTypeU241532_t2333946162  value)
	{
		___U24U24fieldU2D7_7 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D8_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D8_8)); }
	inline U24ArrayTypeU241532_t2333946162  get_U24U24fieldU2D8_8() const { return ___U24U24fieldU2D8_8; }
	inline U24ArrayTypeU241532_t2333946162 * get_address_of_U24U24fieldU2D8_8() { return &___U24U24fieldU2D8_8; }
	inline void set_U24U24fieldU2D8_8(U24ArrayTypeU241532_t2333946162  value)
	{
		___U24U24fieldU2D8_8 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D9_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D9_9)); }
	inline U24ArrayTypeU24208_t3503460108  get_U24U24fieldU2D9_9() const { return ___U24U24fieldU2D9_9; }
	inline U24ArrayTypeU24208_t3503460108 * get_address_of_U24U24fieldU2D9_9() { return &___U24U24fieldU2D9_9; }
	inline void set_U24U24fieldU2D9_9(U24ArrayTypeU24208_t3503460108  value)
	{
		___U24U24fieldU2D9_9 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D10_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D10_10)); }
	inline U24ArrayTypeU24208_t3503460108  get_U24U24fieldU2D10_10() const { return ___U24U24fieldU2D10_10; }
	inline U24ArrayTypeU24208_t3503460108 * get_address_of_U24U24fieldU2D10_10() { return &___U24U24fieldU2D10_10; }
	inline void set_U24U24fieldU2D10_10(U24ArrayTypeU24208_t3503460108  value)
	{
		___U24U24fieldU2D10_10 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D11_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D11_11)); }
	inline U24ArrayTypeU24304_t774576741  get_U24U24fieldU2D11_11() const { return ___U24U24fieldU2D11_11; }
	inline U24ArrayTypeU24304_t774576741 * get_address_of_U24U24fieldU2D11_11() { return &___U24U24fieldU2D11_11; }
	inline void set_U24U24fieldU2D11_11(U24ArrayTypeU24304_t774576741  value)
	{
		___U24U24fieldU2D11_11 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D12_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D12_12)); }
	inline U24ArrayTypeU2472_t1683523543  get_U24U24fieldU2D12_12() const { return ___U24U24fieldU2D12_12; }
	inline U24ArrayTypeU2472_t1683523543 * get_address_of_U24U24fieldU2D12_12() { return &___U24U24fieldU2D12_12; }
	inline void set_U24U24fieldU2D12_12(U24ArrayTypeU2472_t1683523543  value)
	{
		___U24U24fieldU2D12_12 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D13_13() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D13_13)); }
	inline U24ArrayTypeU24304_t774576741  get_U24U24fieldU2D13_13() const { return ___U24U24fieldU2D13_13; }
	inline U24ArrayTypeU24304_t774576741 * get_address_of_U24U24fieldU2D13_13() { return &___U24U24fieldU2D13_13; }
	inline void set_U24U24fieldU2D13_13(U24ArrayTypeU24304_t774576741  value)
	{
		___U24U24fieldU2D13_13 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D14_14() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D14_14)); }
	inline U24ArrayTypeU24304_t774576741  get_U24U24fieldU2D14_14() const { return ___U24U24fieldU2D14_14; }
	inline U24ArrayTypeU24304_t774576741 * get_address_of_U24U24fieldU2D14_14() { return &___U24U24fieldU2D14_14; }
	inline void set_U24U24fieldU2D14_14(U24ArrayTypeU24304_t774576741  value)
	{
		___U24U24fieldU2D14_14 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D15_15() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D15_15)); }
	inline U24ArrayTypeU2472_t1683523543  get_U24U24fieldU2D15_15() const { return ___U24U24fieldU2D15_15; }
	inline U24ArrayTypeU2472_t1683523543 * get_address_of_U24U24fieldU2D15_15() { return &___U24U24fieldU2D15_15; }
	inline void set_U24U24fieldU2D15_15(U24ArrayTypeU2472_t1683523543  value)
	{
		___U24U24fieldU2D15_15 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D16_16() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D16_16)); }
	inline U24ArrayTypeU241392_t1951609148  get_U24U24fieldU2D16_16() const { return ___U24U24fieldU2D16_16; }
	inline U24ArrayTypeU241392_t1951609148 * get_address_of_U24U24fieldU2D16_16() { return &___U24U24fieldU2D16_16; }
	inline void set_U24U24fieldU2D16_16(U24ArrayTypeU241392_t1951609148  value)
	{
		___U24U24fieldU2D16_16 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D17_17() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D17_17)); }
	inline U24ArrayTypeU241392_t1951609148  get_U24U24fieldU2D17_17() const { return ___U24U24fieldU2D17_17; }
	inline U24ArrayTypeU241392_t1951609148 * get_address_of_U24U24fieldU2D17_17() { return &___U24U24fieldU2D17_17; }
	inline void set_U24U24fieldU2D17_17(U24ArrayTypeU241392_t1951609148  value)
	{
		___U24U24fieldU2D17_17 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D18_18() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D18_18)); }
	inline U24ArrayTypeU2412_t2490092598  get_U24U24fieldU2D18_18() const { return ___U24U24fieldU2D18_18; }
	inline U24ArrayTypeU2412_t2490092598 * get_address_of_U24U24fieldU2D18_18() { return &___U24U24fieldU2D18_18; }
	inline void set_U24U24fieldU2D18_18(U24ArrayTypeU2412_t2490092598  value)
	{
		___U24U24fieldU2D18_18 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D19_19() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D19_19)); }
	inline U24ArrayTypeU2452_t520724129  get_U24U24fieldU2D19_19() const { return ___U24U24fieldU2D19_19; }
	inline U24ArrayTypeU2452_t520724129 * get_address_of_U24U24fieldU2D19_19() { return &___U24U24fieldU2D19_19; }
	inline void set_U24U24fieldU2D19_19(U24ArrayTypeU2452_t520724129  value)
	{
		___U24U24fieldU2D19_19 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D20_20() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D20_20)); }
	inline U24ArrayTypeU2452_t520724129  get_U24U24fieldU2D20_20() const { return ___U24U24fieldU2D20_20; }
	inline U24ArrayTypeU2452_t520724129 * get_address_of_U24U24fieldU2D20_20() { return &___U24U24fieldU2D20_20; }
	inline void set_U24U24fieldU2D20_20(U24ArrayTypeU2452_t520724129  value)
	{
		___U24U24fieldU2D20_20 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D21_21() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D21_21)); }
	inline U24ArrayTypeU2452_t520724129  get_U24U24fieldU2D21_21() const { return ___U24U24fieldU2D21_21; }
	inline U24ArrayTypeU2452_t520724129 * get_address_of_U24U24fieldU2D21_21() { return &___U24U24fieldU2D21_21; }
	inline void set_U24U24fieldU2D21_21(U24ArrayTypeU2452_t520724129  value)
	{
		___U24U24fieldU2D21_21 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D22_22() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D22_22)); }
	inline U24ArrayTypeU248_t3244137465  get_U24U24fieldU2D22_22() const { return ___U24U24fieldU2D22_22; }
	inline U24ArrayTypeU248_t3244137465 * get_address_of_U24U24fieldU2D22_22() { return &___U24U24fieldU2D22_22; }
	inline void set_U24U24fieldU2D22_22(U24ArrayTypeU248_t3244137465  value)
	{
		___U24U24fieldU2D22_22 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D36_23() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D36_23)); }
	inline U24ArrayTypeU248_t3244137465  get_U24U24fieldU2D36_23() const { return ___U24U24fieldU2D36_23; }
	inline U24ArrayTypeU248_t3244137465 * get_address_of_U24U24fieldU2D36_23() { return &___U24U24fieldU2D36_23; }
	inline void set_U24U24fieldU2D36_23(U24ArrayTypeU248_t3244137465  value)
	{
		___U24U24fieldU2D36_23 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D37_24() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D37_24)); }
	inline U24ArrayTypeU24256_t1929481984  get_U24U24fieldU2D37_24() const { return ___U24U24fieldU2D37_24; }
	inline U24ArrayTypeU24256_t1929481984 * get_address_of_U24U24fieldU2D37_24() { return &___U24U24fieldU2D37_24; }
	inline void set_U24U24fieldU2D37_24(U24ArrayTypeU24256_t1929481984  value)
	{
		___U24U24fieldU2D37_24 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D38_25() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D38_25)); }
	inline U24ArrayTypeU24256_t1929481984  get_U24U24fieldU2D38_25() const { return ___U24U24fieldU2D38_25; }
	inline U24ArrayTypeU24256_t1929481984 * get_address_of_U24U24fieldU2D38_25() { return &___U24U24fieldU2D38_25; }
	inline void set_U24U24fieldU2D38_25(U24ArrayTypeU24256_t1929481984  value)
	{
		___U24U24fieldU2D38_25 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D39_26() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D39_26)); }
	inline U24ArrayTypeU241280_t4290130235  get_U24U24fieldU2D39_26() const { return ___U24U24fieldU2D39_26; }
	inline U24ArrayTypeU241280_t4290130235 * get_address_of_U24U24fieldU2D39_26() { return &___U24U24fieldU2D39_26; }
	inline void set_U24U24fieldU2D39_26(U24ArrayTypeU241280_t4290130235  value)
	{
		___U24U24fieldU2D39_26 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D40_27() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D40_27)); }
	inline U24ArrayTypeU2412_t2490092598  get_U24U24fieldU2D40_27() const { return ___U24U24fieldU2D40_27; }
	inline U24ArrayTypeU2412_t2490092598 * get_address_of_U24U24fieldU2D40_27() { return &___U24U24fieldU2D40_27; }
	inline void set_U24U24fieldU2D40_27(U24ArrayTypeU2412_t2490092598  value)
	{
		___U24U24fieldU2D40_27 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D41_28() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D41_28)); }
	inline U24ArrayTypeU2412_t2490092598  get_U24U24fieldU2D41_28() const { return ___U24U24fieldU2D41_28; }
	inline U24ArrayTypeU2412_t2490092598 * get_address_of_U24U24fieldU2D41_28() { return &___U24U24fieldU2D41_28; }
	inline void set_U24U24fieldU2D41_28(U24ArrayTypeU2412_t2490092598  value)
	{
		___U24U24fieldU2D41_28 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D42_29() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D42_29)); }
	inline U24ArrayTypeU248_t3244137465  get_U24U24fieldU2D42_29() const { return ___U24U24fieldU2D42_29; }
	inline U24ArrayTypeU248_t3244137465 * get_address_of_U24U24fieldU2D42_29() { return &___U24U24fieldU2D42_29; }
	inline void set_U24U24fieldU2D42_29(U24ArrayTypeU248_t3244137465  value)
	{
		___U24U24fieldU2D42_29 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D43_30() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D43_30)); }
	inline U24ArrayTypeU248_t3244137465  get_U24U24fieldU2D43_30() const { return ___U24U24fieldU2D43_30; }
	inline U24ArrayTypeU248_t3244137465 * get_address_of_U24U24fieldU2D43_30() { return &___U24U24fieldU2D43_30; }
	inline void set_U24U24fieldU2D43_30(U24ArrayTypeU248_t3244137465  value)
	{
		___U24U24fieldU2D43_30 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D44_31() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields, ___U24U24fieldU2D44_31)); }
	inline U24ArrayTypeU248_t3244137465  get_U24U24fieldU2D44_31() const { return ___U24U24fieldU2D44_31; }
	inline U24ArrayTypeU248_t3244137465 * get_address_of_U24U24fieldU2D44_31() { return &___U24U24fieldU2D44_31; }
	inline void set_U24U24fieldU2D44_31(U24ArrayTypeU248_t3244137465  value)
	{
		___U24U24fieldU2D44_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
