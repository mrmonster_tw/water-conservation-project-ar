﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.UI.ControlCollection
struct ControlCollection_t4212191938;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.HtmlControls.HtmlTableCellCollection
struct  HtmlTableCellCollection_t1920277368  : public Il2CppObject
{
public:
	// System.Web.UI.ControlCollection System.Web.UI.HtmlControls.HtmlTableCellCollection::cc
	ControlCollection_t4212191938 * ___cc_0;

public:
	inline static int32_t get_offset_of_cc_0() { return static_cast<int32_t>(offsetof(HtmlTableCellCollection_t1920277368, ___cc_0)); }
	inline ControlCollection_t4212191938 * get_cc_0() const { return ___cc_0; }
	inline ControlCollection_t4212191938 ** get_address_of_cc_0() { return &___cc_0; }
	inline void set_cc_0(ControlCollection_t4212191938 * value)
	{
		___cc_0 = value;
		Il2CppCodeGenWriteBarrier(&___cc_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
