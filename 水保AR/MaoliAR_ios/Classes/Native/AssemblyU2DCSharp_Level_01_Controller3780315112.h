﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// Level_Controller
struct Level_Controller_t1433348427;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.GameObject
struct GameObject_t1113636619;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Level_01_Controller
struct  Level_01_Controller_t3780315112  : public MonoBehaviour_t3962482529
{
public:
	// Level_Controller Level_01_Controller::lvManager
	Level_Controller_t1433348427 * ___lvManager_2;
	// UnityEngine.GameObject[] Level_01_Controller::all_unShow_Objs
	GameObjectU5BU5D_t3328599146* ___all_unShow_Objs_3;
	// UnityEngine.GameObject[] Level_01_Controller::all_Objs
	GameObjectU5BU5D_t3328599146* ___all_Objs_4;
	// UnityEngine.GameObject[] Level_01_Controller::chapter_01
	GameObjectU5BU5D_t3328599146* ___chapter_01_5;
	// UnityEngine.GameObject[] Level_01_Controller::chapter_02
	GameObjectU5BU5D_t3328599146* ___chapter_02_6;
	// UnityEngine.GameObject[] Level_01_Controller::LV1tips
	GameObjectU5BU5D_t3328599146* ___LV1tips_7;
	// UnityEngine.GameObject Level_01_Controller::win_panel
	GameObject_t1113636619 * ___win_panel_8;
	// UnityEngine.GameObject Level_01_Controller::good_road
	GameObject_t1113636619 * ___good_road_9;
	// UnityEngine.GameObject Level_01_Controller::good_water
	GameObject_t1113636619 * ___good_water_10;
	// UnityEngine.GameObject Level_01_Controller::Evaporation
	GameObject_t1113636619 * ___Evaporation_11;
	// UnityEngine.GameObject Level_01_Controller::Evaportranspiration
	GameObject_t1113636619 * ___Evaportranspiration_12;
	// UnityEngine.GameObject Level_01_Controller::Infiltration
	GameObject_t1113636619 * ___Infiltration_13;
	// UnityEngine.GameObject Level_01_Controller::tip_1
	GameObject_t1113636619 * ___tip_1_14;
	// UnityEngine.GameObject Level_01_Controller::tip_2
	GameObject_t1113636619 * ___tip_2_15;
	// System.Boolean Level_01_Controller::do_once
	bool ___do_once_16;
	// System.Boolean Level_01_Controller::do_once2
	bool ___do_once2_17;

public:
	inline static int32_t get_offset_of_lvManager_2() { return static_cast<int32_t>(offsetof(Level_01_Controller_t3780315112, ___lvManager_2)); }
	inline Level_Controller_t1433348427 * get_lvManager_2() const { return ___lvManager_2; }
	inline Level_Controller_t1433348427 ** get_address_of_lvManager_2() { return &___lvManager_2; }
	inline void set_lvManager_2(Level_Controller_t1433348427 * value)
	{
		___lvManager_2 = value;
		Il2CppCodeGenWriteBarrier(&___lvManager_2, value);
	}

	inline static int32_t get_offset_of_all_unShow_Objs_3() { return static_cast<int32_t>(offsetof(Level_01_Controller_t3780315112, ___all_unShow_Objs_3)); }
	inline GameObjectU5BU5D_t3328599146* get_all_unShow_Objs_3() const { return ___all_unShow_Objs_3; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_all_unShow_Objs_3() { return &___all_unShow_Objs_3; }
	inline void set_all_unShow_Objs_3(GameObjectU5BU5D_t3328599146* value)
	{
		___all_unShow_Objs_3 = value;
		Il2CppCodeGenWriteBarrier(&___all_unShow_Objs_3, value);
	}

	inline static int32_t get_offset_of_all_Objs_4() { return static_cast<int32_t>(offsetof(Level_01_Controller_t3780315112, ___all_Objs_4)); }
	inline GameObjectU5BU5D_t3328599146* get_all_Objs_4() const { return ___all_Objs_4; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_all_Objs_4() { return &___all_Objs_4; }
	inline void set_all_Objs_4(GameObjectU5BU5D_t3328599146* value)
	{
		___all_Objs_4 = value;
		Il2CppCodeGenWriteBarrier(&___all_Objs_4, value);
	}

	inline static int32_t get_offset_of_chapter_01_5() { return static_cast<int32_t>(offsetof(Level_01_Controller_t3780315112, ___chapter_01_5)); }
	inline GameObjectU5BU5D_t3328599146* get_chapter_01_5() const { return ___chapter_01_5; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_chapter_01_5() { return &___chapter_01_5; }
	inline void set_chapter_01_5(GameObjectU5BU5D_t3328599146* value)
	{
		___chapter_01_5 = value;
		Il2CppCodeGenWriteBarrier(&___chapter_01_5, value);
	}

	inline static int32_t get_offset_of_chapter_02_6() { return static_cast<int32_t>(offsetof(Level_01_Controller_t3780315112, ___chapter_02_6)); }
	inline GameObjectU5BU5D_t3328599146* get_chapter_02_6() const { return ___chapter_02_6; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_chapter_02_6() { return &___chapter_02_6; }
	inline void set_chapter_02_6(GameObjectU5BU5D_t3328599146* value)
	{
		___chapter_02_6 = value;
		Il2CppCodeGenWriteBarrier(&___chapter_02_6, value);
	}

	inline static int32_t get_offset_of_LV1tips_7() { return static_cast<int32_t>(offsetof(Level_01_Controller_t3780315112, ___LV1tips_7)); }
	inline GameObjectU5BU5D_t3328599146* get_LV1tips_7() const { return ___LV1tips_7; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_LV1tips_7() { return &___LV1tips_7; }
	inline void set_LV1tips_7(GameObjectU5BU5D_t3328599146* value)
	{
		___LV1tips_7 = value;
		Il2CppCodeGenWriteBarrier(&___LV1tips_7, value);
	}

	inline static int32_t get_offset_of_win_panel_8() { return static_cast<int32_t>(offsetof(Level_01_Controller_t3780315112, ___win_panel_8)); }
	inline GameObject_t1113636619 * get_win_panel_8() const { return ___win_panel_8; }
	inline GameObject_t1113636619 ** get_address_of_win_panel_8() { return &___win_panel_8; }
	inline void set_win_panel_8(GameObject_t1113636619 * value)
	{
		___win_panel_8 = value;
		Il2CppCodeGenWriteBarrier(&___win_panel_8, value);
	}

	inline static int32_t get_offset_of_good_road_9() { return static_cast<int32_t>(offsetof(Level_01_Controller_t3780315112, ___good_road_9)); }
	inline GameObject_t1113636619 * get_good_road_9() const { return ___good_road_9; }
	inline GameObject_t1113636619 ** get_address_of_good_road_9() { return &___good_road_9; }
	inline void set_good_road_9(GameObject_t1113636619 * value)
	{
		___good_road_9 = value;
		Il2CppCodeGenWriteBarrier(&___good_road_9, value);
	}

	inline static int32_t get_offset_of_good_water_10() { return static_cast<int32_t>(offsetof(Level_01_Controller_t3780315112, ___good_water_10)); }
	inline GameObject_t1113636619 * get_good_water_10() const { return ___good_water_10; }
	inline GameObject_t1113636619 ** get_address_of_good_water_10() { return &___good_water_10; }
	inline void set_good_water_10(GameObject_t1113636619 * value)
	{
		___good_water_10 = value;
		Il2CppCodeGenWriteBarrier(&___good_water_10, value);
	}

	inline static int32_t get_offset_of_Evaporation_11() { return static_cast<int32_t>(offsetof(Level_01_Controller_t3780315112, ___Evaporation_11)); }
	inline GameObject_t1113636619 * get_Evaporation_11() const { return ___Evaporation_11; }
	inline GameObject_t1113636619 ** get_address_of_Evaporation_11() { return &___Evaporation_11; }
	inline void set_Evaporation_11(GameObject_t1113636619 * value)
	{
		___Evaporation_11 = value;
		Il2CppCodeGenWriteBarrier(&___Evaporation_11, value);
	}

	inline static int32_t get_offset_of_Evaportranspiration_12() { return static_cast<int32_t>(offsetof(Level_01_Controller_t3780315112, ___Evaportranspiration_12)); }
	inline GameObject_t1113636619 * get_Evaportranspiration_12() const { return ___Evaportranspiration_12; }
	inline GameObject_t1113636619 ** get_address_of_Evaportranspiration_12() { return &___Evaportranspiration_12; }
	inline void set_Evaportranspiration_12(GameObject_t1113636619 * value)
	{
		___Evaportranspiration_12 = value;
		Il2CppCodeGenWriteBarrier(&___Evaportranspiration_12, value);
	}

	inline static int32_t get_offset_of_Infiltration_13() { return static_cast<int32_t>(offsetof(Level_01_Controller_t3780315112, ___Infiltration_13)); }
	inline GameObject_t1113636619 * get_Infiltration_13() const { return ___Infiltration_13; }
	inline GameObject_t1113636619 ** get_address_of_Infiltration_13() { return &___Infiltration_13; }
	inline void set_Infiltration_13(GameObject_t1113636619 * value)
	{
		___Infiltration_13 = value;
		Il2CppCodeGenWriteBarrier(&___Infiltration_13, value);
	}

	inline static int32_t get_offset_of_tip_1_14() { return static_cast<int32_t>(offsetof(Level_01_Controller_t3780315112, ___tip_1_14)); }
	inline GameObject_t1113636619 * get_tip_1_14() const { return ___tip_1_14; }
	inline GameObject_t1113636619 ** get_address_of_tip_1_14() { return &___tip_1_14; }
	inline void set_tip_1_14(GameObject_t1113636619 * value)
	{
		___tip_1_14 = value;
		Il2CppCodeGenWriteBarrier(&___tip_1_14, value);
	}

	inline static int32_t get_offset_of_tip_2_15() { return static_cast<int32_t>(offsetof(Level_01_Controller_t3780315112, ___tip_2_15)); }
	inline GameObject_t1113636619 * get_tip_2_15() const { return ___tip_2_15; }
	inline GameObject_t1113636619 ** get_address_of_tip_2_15() { return &___tip_2_15; }
	inline void set_tip_2_15(GameObject_t1113636619 * value)
	{
		___tip_2_15 = value;
		Il2CppCodeGenWriteBarrier(&___tip_2_15, value);
	}

	inline static int32_t get_offset_of_do_once_16() { return static_cast<int32_t>(offsetof(Level_01_Controller_t3780315112, ___do_once_16)); }
	inline bool get_do_once_16() const { return ___do_once_16; }
	inline bool* get_address_of_do_once_16() { return &___do_once_16; }
	inline void set_do_once_16(bool value)
	{
		___do_once_16 = value;
	}

	inline static int32_t get_offset_of_do_once2_17() { return static_cast<int32_t>(offsetof(Level_01_Controller_t3780315112, ___do_once2_17)); }
	inline bool get_do_once2_17() const { return ___do_once2_17; }
	inline bool* get_address_of_do_once2_17() { return &___do_once2_17; }
	inline void set_do_once2_17(bool value)
	{
		___do_once2_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
