﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_System_ServiceModel_Security_X3346525935.h"

// System.IdentityModel.Selectors.X509CertificateValidator
struct X509CertificateValidator_t1375267704;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.X509ServiceCertificateAuthentication
struct  X509ServiceCertificateAuthentication_t3406728790  : public Il2CppObject
{
public:
	// System.ServiceModel.Security.X509CertificateValidationMode System.ServiceModel.Security.X509ServiceCertificateAuthentication::validation_mode
	int32_t ___validation_mode_0;
	// System.IdentityModel.Selectors.X509CertificateValidator System.ServiceModel.Security.X509ServiceCertificateAuthentication::custom_validator
	X509CertificateValidator_t1375267704 * ___custom_validator_1;

public:
	inline static int32_t get_offset_of_validation_mode_0() { return static_cast<int32_t>(offsetof(X509ServiceCertificateAuthentication_t3406728790, ___validation_mode_0)); }
	inline int32_t get_validation_mode_0() const { return ___validation_mode_0; }
	inline int32_t* get_address_of_validation_mode_0() { return &___validation_mode_0; }
	inline void set_validation_mode_0(int32_t value)
	{
		___validation_mode_0 = value;
	}

	inline static int32_t get_offset_of_custom_validator_1() { return static_cast<int32_t>(offsetof(X509ServiceCertificateAuthentication_t3406728790, ___custom_validator_1)); }
	inline X509CertificateValidator_t1375267704 * get_custom_validator_1() const { return ___custom_validator_1; }
	inline X509CertificateValidator_t1375267704 ** get_address_of_custom_validator_1() { return &___custom_validator_1; }
	inline void set_custom_validator_1(X509CertificateValidator_t1375267704 * value)
	{
		___custom_validator_1 = value;
		Il2CppCodeGenWriteBarrier(&___custom_validator_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
