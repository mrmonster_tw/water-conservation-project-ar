﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Security_System_Security_Cryptography_Xml_T1105379765.h"

// System.Xml.XmlNodeList
struct XmlNodeList_t2551693786;
// System.Xml.XmlDocument
struct XmlDocument_t2837193595;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.Xml.XmlDsigXsltTransform
struct  XmlDsigXsltTransform_t2973394665  : public Transform_t1105379765
{
public:
	// System.Boolean System.Security.Cryptography.Xml.XmlDsigXsltTransform::comments
	bool ___comments_3;
	// System.Xml.XmlNodeList System.Security.Cryptography.Xml.XmlDsigXsltTransform::xnl
	XmlNodeList_t2551693786 * ___xnl_4;
	// System.Xml.XmlDocument System.Security.Cryptography.Xml.XmlDsigXsltTransform::inputDoc
	XmlDocument_t2837193595 * ___inputDoc_5;

public:
	inline static int32_t get_offset_of_comments_3() { return static_cast<int32_t>(offsetof(XmlDsigXsltTransform_t2973394665, ___comments_3)); }
	inline bool get_comments_3() const { return ___comments_3; }
	inline bool* get_address_of_comments_3() { return &___comments_3; }
	inline void set_comments_3(bool value)
	{
		___comments_3 = value;
	}

	inline static int32_t get_offset_of_xnl_4() { return static_cast<int32_t>(offsetof(XmlDsigXsltTransform_t2973394665, ___xnl_4)); }
	inline XmlNodeList_t2551693786 * get_xnl_4() const { return ___xnl_4; }
	inline XmlNodeList_t2551693786 ** get_address_of_xnl_4() { return &___xnl_4; }
	inline void set_xnl_4(XmlNodeList_t2551693786 * value)
	{
		___xnl_4 = value;
		Il2CppCodeGenWriteBarrier(&___xnl_4, value);
	}

	inline static int32_t get_offset_of_inputDoc_5() { return static_cast<int32_t>(offsetof(XmlDsigXsltTransform_t2973394665, ___inputDoc_5)); }
	inline XmlDocument_t2837193595 * get_inputDoc_5() const { return ___inputDoc_5; }
	inline XmlDocument_t2837193595 ** get_address_of_inputDoc_5() { return &___inputDoc_5; }
	inline void set_inputDoc_5(XmlDocument_t2837193595 * value)
	{
		___inputDoc_5 = value;
		Il2CppCodeGenWriteBarrier(&___inputDoc_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
