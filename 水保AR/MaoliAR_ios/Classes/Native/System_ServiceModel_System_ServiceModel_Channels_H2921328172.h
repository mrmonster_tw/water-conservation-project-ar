﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_H3141234096.h"

// System.Net.HttpListenerContext
struct HttpListenerContext_t424880822;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.HttpRequestContext
struct  HttpRequestContext_t2921328172  : public HttpRequestContextBase_t3141234096
{
public:
	// System.Net.HttpListenerContext System.ServiceModel.Channels.HttpRequestContext::ctx
	HttpListenerContext_t424880822 * ___ctx_2;

public:
	inline static int32_t get_offset_of_ctx_2() { return static_cast<int32_t>(offsetof(HttpRequestContext_t2921328172, ___ctx_2)); }
	inline HttpListenerContext_t424880822 * get_ctx_2() const { return ___ctx_2; }
	inline HttpListenerContext_t424880822 ** get_address_of_ctx_2() { return &___ctx_2; }
	inline void set_ctx_2(HttpListenerContext_t424880822 * value)
	{
		___ctx_2 = value;
		Il2CppCodeGenWriteBarrier(&___ctx_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
