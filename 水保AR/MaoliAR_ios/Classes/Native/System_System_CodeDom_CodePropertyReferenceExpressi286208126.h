﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeExpression2166265795.h"

// System.CodeDom.CodeExpression
struct CodeExpression_t2166265795;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodePropertyReferenceExpression
struct  CodePropertyReferenceExpression_t286208126  : public CodeExpression_t2166265795
{
public:
	// System.CodeDom.CodeExpression System.CodeDom.CodePropertyReferenceExpression::targetObject
	CodeExpression_t2166265795 * ___targetObject_1;
	// System.String System.CodeDom.CodePropertyReferenceExpression::propertyName
	String_t* ___propertyName_2;

public:
	inline static int32_t get_offset_of_targetObject_1() { return static_cast<int32_t>(offsetof(CodePropertyReferenceExpression_t286208126, ___targetObject_1)); }
	inline CodeExpression_t2166265795 * get_targetObject_1() const { return ___targetObject_1; }
	inline CodeExpression_t2166265795 ** get_address_of_targetObject_1() { return &___targetObject_1; }
	inline void set_targetObject_1(CodeExpression_t2166265795 * value)
	{
		___targetObject_1 = value;
		Il2CppCodeGenWriteBarrier(&___targetObject_1, value);
	}

	inline static int32_t get_offset_of_propertyName_2() { return static_cast<int32_t>(offsetof(CodePropertyReferenceExpression_t286208126, ___propertyName_2)); }
	inline String_t* get_propertyName_2() const { return ___propertyName_2; }
	inline String_t** get_address_of_propertyName_2() { return &___propertyName_2; }
	inline void set_propertyName_2(String_t* value)
	{
		___propertyName_2 = value;
		Il2CppCodeGenWriteBarrier(&___propertyName_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
