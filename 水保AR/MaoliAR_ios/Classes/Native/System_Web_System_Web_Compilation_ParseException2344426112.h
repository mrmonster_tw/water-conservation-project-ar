﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_HtmlizedException2999156009.h"

// System.Web.Compilation.ILocation
struct ILocation_t3961726794;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.ParseException
struct  ParseException_t2344426112  : public HtmlizedException_t2999156009
{
public:
	// System.Web.Compilation.ILocation System.Web.Compilation.ParseException::location
	Il2CppObject * ___location_14;
	// System.String System.Web.Compilation.ParseException::fileText
	String_t* ___fileText_15;

public:
	inline static int32_t get_offset_of_location_14() { return static_cast<int32_t>(offsetof(ParseException_t2344426112, ___location_14)); }
	inline Il2CppObject * get_location_14() const { return ___location_14; }
	inline Il2CppObject ** get_address_of_location_14() { return &___location_14; }
	inline void set_location_14(Il2CppObject * value)
	{
		___location_14 = value;
		Il2CppCodeGenWriteBarrier(&___location_14, value);
	}

	inline static int32_t get_offset_of_fileText_15() { return static_cast<int32_t>(offsetof(ParseException_t2344426112, ___fileText_15)); }
	inline String_t* get_fileText_15() const { return ___fileText_15; }
	inline String_t** get_address_of_fileText_15() { return &___fileText_15; }
	inline void set_fileText_15(String_t* value)
	{
		___fileText_15 = value;
		Il2CppCodeGenWriteBarrier(&___fileText_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
