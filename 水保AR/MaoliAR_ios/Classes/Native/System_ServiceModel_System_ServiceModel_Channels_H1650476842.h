﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Action`1<System.ServiceModel.Channels.HttpContextInfo>
struct Action_1_t3734003028;
// System.ServiceModel.Channels.HttpSimpleListenerManager
struct HttpSimpleListenerManager_t2559451545;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.HttpSimpleListenerManager/<KickContextReceiver>c__AnonStorey9
struct  U3CKickContextReceiverU3Ec__AnonStorey9_t1650476842  : public Il2CppObject
{
public:
	// System.Action`1<System.ServiceModel.Channels.HttpContextInfo> System.ServiceModel.Channels.HttpSimpleListenerManager/<KickContextReceiver>c__AnonStorey9::contextReceivedCallback
	Action_1_t3734003028 * ___contextReceivedCallback_0;
	// System.ServiceModel.Channels.HttpSimpleListenerManager System.ServiceModel.Channels.HttpSimpleListenerManager/<KickContextReceiver>c__AnonStorey9::<>f__this
	HttpSimpleListenerManager_t2559451545 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_contextReceivedCallback_0() { return static_cast<int32_t>(offsetof(U3CKickContextReceiverU3Ec__AnonStorey9_t1650476842, ___contextReceivedCallback_0)); }
	inline Action_1_t3734003028 * get_contextReceivedCallback_0() const { return ___contextReceivedCallback_0; }
	inline Action_1_t3734003028 ** get_address_of_contextReceivedCallback_0() { return &___contextReceivedCallback_0; }
	inline void set_contextReceivedCallback_0(Action_1_t3734003028 * value)
	{
		___contextReceivedCallback_0 = value;
		Il2CppCodeGenWriteBarrier(&___contextReceivedCallback_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CKickContextReceiverU3Ec__AnonStorey9_t1650476842, ___U3CU3Ef__this_1)); }
	inline HttpSimpleListenerManager_t2559451545 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline HttpSimpleListenerManager_t2559451545 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(HttpSimpleListenerManager_t2559451545 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
