﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_ComponentModel_TypeConverter2249118273.h"

// System.Collections.Generic.Dictionary`2<System.String,System.ServiceModel.MessageSecurityVersion>
struct Dictionary_2_t1864796265;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.MessageSecurityVersionConverter
struct  MessageSecurityVersionConverter_t867433891  : public TypeConverter_t2249118273
{
public:

public:
};

struct MessageSecurityVersionConverter_t867433891_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.ServiceModel.MessageSecurityVersion> System.ServiceModel.Configuration.MessageSecurityVersionConverter::_lookup
	Dictionary_2_t1864796265 * ____lookup_0;

public:
	inline static int32_t get_offset_of__lookup_0() { return static_cast<int32_t>(offsetof(MessageSecurityVersionConverter_t867433891_StaticFields, ____lookup_0)); }
	inline Dictionary_2_t1864796265 * get__lookup_0() const { return ____lookup_0; }
	inline Dictionary_2_t1864796265 ** get_address_of__lookup_0() { return &____lookup_0; }
	inline void set__lookup_0(Dictionary_2_t1864796265 * value)
	{
		____lookup_0 = value;
		Il2CppCodeGenWriteBarrier(&____lookup_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
