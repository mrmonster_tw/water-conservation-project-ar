﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeObject3927604602.h"
#include "System_System_CodeDom_CodeTypeReferenceOptions661023944.h"

// System.String
struct String_t;
// System.CodeDom.CodeTypeReference
struct CodeTypeReference_t3809997434;
// System.CodeDom.CodeTypeReferenceCollection
struct CodeTypeReferenceCollection_t3857551471;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeTypeReference
struct  CodeTypeReference_t3809997434  : public CodeObject_t3927604602
{
public:
	// System.String System.CodeDom.CodeTypeReference::baseType
	String_t* ___baseType_1;
	// System.CodeDom.CodeTypeReference System.CodeDom.CodeTypeReference::arrayElementType
	CodeTypeReference_t3809997434 * ___arrayElementType_2;
	// System.Int32 System.CodeDom.CodeTypeReference::arrayRank
	int32_t ___arrayRank_3;
	// System.Boolean System.CodeDom.CodeTypeReference::isInterface
	bool ___isInterface_4;
	// System.CodeDom.CodeTypeReferenceCollection System.CodeDom.CodeTypeReference::typeArguments
	CodeTypeReferenceCollection_t3857551471 * ___typeArguments_5;
	// System.CodeDom.CodeTypeReferenceOptions System.CodeDom.CodeTypeReference::referenceOptions
	int32_t ___referenceOptions_6;

public:
	inline static int32_t get_offset_of_baseType_1() { return static_cast<int32_t>(offsetof(CodeTypeReference_t3809997434, ___baseType_1)); }
	inline String_t* get_baseType_1() const { return ___baseType_1; }
	inline String_t** get_address_of_baseType_1() { return &___baseType_1; }
	inline void set_baseType_1(String_t* value)
	{
		___baseType_1 = value;
		Il2CppCodeGenWriteBarrier(&___baseType_1, value);
	}

	inline static int32_t get_offset_of_arrayElementType_2() { return static_cast<int32_t>(offsetof(CodeTypeReference_t3809997434, ___arrayElementType_2)); }
	inline CodeTypeReference_t3809997434 * get_arrayElementType_2() const { return ___arrayElementType_2; }
	inline CodeTypeReference_t3809997434 ** get_address_of_arrayElementType_2() { return &___arrayElementType_2; }
	inline void set_arrayElementType_2(CodeTypeReference_t3809997434 * value)
	{
		___arrayElementType_2 = value;
		Il2CppCodeGenWriteBarrier(&___arrayElementType_2, value);
	}

	inline static int32_t get_offset_of_arrayRank_3() { return static_cast<int32_t>(offsetof(CodeTypeReference_t3809997434, ___arrayRank_3)); }
	inline int32_t get_arrayRank_3() const { return ___arrayRank_3; }
	inline int32_t* get_address_of_arrayRank_3() { return &___arrayRank_3; }
	inline void set_arrayRank_3(int32_t value)
	{
		___arrayRank_3 = value;
	}

	inline static int32_t get_offset_of_isInterface_4() { return static_cast<int32_t>(offsetof(CodeTypeReference_t3809997434, ___isInterface_4)); }
	inline bool get_isInterface_4() const { return ___isInterface_4; }
	inline bool* get_address_of_isInterface_4() { return &___isInterface_4; }
	inline void set_isInterface_4(bool value)
	{
		___isInterface_4 = value;
	}

	inline static int32_t get_offset_of_typeArguments_5() { return static_cast<int32_t>(offsetof(CodeTypeReference_t3809997434, ___typeArguments_5)); }
	inline CodeTypeReferenceCollection_t3857551471 * get_typeArguments_5() const { return ___typeArguments_5; }
	inline CodeTypeReferenceCollection_t3857551471 ** get_address_of_typeArguments_5() { return &___typeArguments_5; }
	inline void set_typeArguments_5(CodeTypeReferenceCollection_t3857551471 * value)
	{
		___typeArguments_5 = value;
		Il2CppCodeGenWriteBarrier(&___typeArguments_5, value);
	}

	inline static int32_t get_offset_of_referenceOptions_6() { return static_cast<int32_t>(offsetof(CodeTypeReference_t3809997434, ___referenceOptions_6)); }
	inline int32_t get_referenceOptions_6() const { return ___referenceOptions_6; }
	inline int32_t* get_address_of_referenceOptions_6() { return &___referenceOptions_6; }
	inline void set_referenceOptions_6(int32_t value)
	{
		___referenceOptions_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
