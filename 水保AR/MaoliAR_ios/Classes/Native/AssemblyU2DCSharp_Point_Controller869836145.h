﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;
// TimeGame
struct TimeGame_t3115909209;
// UnityEngine.GameObject
struct GameObject_t1113636619;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Point_Controller
struct  Point_Controller_t869836145  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Point_Controller::IS_level03
	bool ___IS_level03_2;
	// System.Boolean Point_Controller::already_did
	bool ___already_did_3;
	// UnityEngine.Events.UnityEvent Point_Controller::Events
	UnityEvent_t2581268647 * ___Events_4;
	// TimeGame Point_Controller::tg
	TimeGame_t3115909209 * ___tg_5;
	// UnityEngine.GameObject Point_Controller::level5Finger
	GameObject_t1113636619 * ___level5Finger_6;

public:
	inline static int32_t get_offset_of_IS_level03_2() { return static_cast<int32_t>(offsetof(Point_Controller_t869836145, ___IS_level03_2)); }
	inline bool get_IS_level03_2() const { return ___IS_level03_2; }
	inline bool* get_address_of_IS_level03_2() { return &___IS_level03_2; }
	inline void set_IS_level03_2(bool value)
	{
		___IS_level03_2 = value;
	}

	inline static int32_t get_offset_of_already_did_3() { return static_cast<int32_t>(offsetof(Point_Controller_t869836145, ___already_did_3)); }
	inline bool get_already_did_3() const { return ___already_did_3; }
	inline bool* get_address_of_already_did_3() { return &___already_did_3; }
	inline void set_already_did_3(bool value)
	{
		___already_did_3 = value;
	}

	inline static int32_t get_offset_of_Events_4() { return static_cast<int32_t>(offsetof(Point_Controller_t869836145, ___Events_4)); }
	inline UnityEvent_t2581268647 * get_Events_4() const { return ___Events_4; }
	inline UnityEvent_t2581268647 ** get_address_of_Events_4() { return &___Events_4; }
	inline void set_Events_4(UnityEvent_t2581268647 * value)
	{
		___Events_4 = value;
		Il2CppCodeGenWriteBarrier(&___Events_4, value);
	}

	inline static int32_t get_offset_of_tg_5() { return static_cast<int32_t>(offsetof(Point_Controller_t869836145, ___tg_5)); }
	inline TimeGame_t3115909209 * get_tg_5() const { return ___tg_5; }
	inline TimeGame_t3115909209 ** get_address_of_tg_5() { return &___tg_5; }
	inline void set_tg_5(TimeGame_t3115909209 * value)
	{
		___tg_5 = value;
		Il2CppCodeGenWriteBarrier(&___tg_5, value);
	}

	inline static int32_t get_offset_of_level5Finger_6() { return static_cast<int32_t>(offsetof(Point_Controller_t869836145, ___level5Finger_6)); }
	inline GameObject_t1113636619 * get_level5Finger_6() const { return ___level5Finger_6; }
	inline GameObject_t1113636619 ** get_address_of_level5Finger_6() { return &___level5Finger_6; }
	inline void set_level5Finger_6(GameObject_t1113636619 * value)
	{
		___level5Finger_6 = value;
		Il2CppCodeGenWriteBarrier(&___level5Finger_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
