﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"

// System.ServiceModel.Description.ServiceEndpoint
struct ServiceEndpoint_t4038493094;
// System.ServiceModel.Dispatcher.ChannelDispatcher
struct ChannelDispatcher_t2781106991;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.ServiceModel.Description.ServiceEndpoint,System.ServiceModel.Dispatcher.ChannelDispatcher>
struct  KeyValuePair_2_t4278733032 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	ServiceEndpoint_t4038493094 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	ChannelDispatcher_t2781106991 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t4278733032, ___key_0)); }
	inline ServiceEndpoint_t4038493094 * get_key_0() const { return ___key_0; }
	inline ServiceEndpoint_t4038493094 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(ServiceEndpoint_t4038493094 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier(&___key_0, value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t4278733032, ___value_1)); }
	inline ChannelDispatcher_t2781106991 * get_value_1() const { return ___value_1; }
	inline ChannelDispatcher_t2781106991 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(ChannelDispatcher_t2781106991 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
