﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_System_ServiceModel_SecurityMo1299321141.h"

// System.ServiceModel.NonDualMessageSecurityOverHttp
struct NonDualMessageSecurityOverHttp_t883223478;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.WSHttpSecurity
struct  WSHttpSecurity_t1511783750  : public Il2CppObject
{
public:
	// System.ServiceModel.SecurityMode System.ServiceModel.WSHttpSecurity::mode
	int32_t ___mode_0;
	// System.ServiceModel.NonDualMessageSecurityOverHttp System.ServiceModel.WSHttpSecurity::message
	NonDualMessageSecurityOverHttp_t883223478 * ___message_1;

public:
	inline static int32_t get_offset_of_mode_0() { return static_cast<int32_t>(offsetof(WSHttpSecurity_t1511783750, ___mode_0)); }
	inline int32_t get_mode_0() const { return ___mode_0; }
	inline int32_t* get_address_of_mode_0() { return &___mode_0; }
	inline void set_mode_0(int32_t value)
	{
		___mode_0 = value;
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(WSHttpSecurity_t1511783750, ___message_1)); }
	inline NonDualMessageSecurityOverHttp_t883223478 * get_message_1() const { return ___message_1; }
	inline NonDualMessageSecurityOverHttp_t883223478 ** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(NonDualMessageSecurityOverHttp_t883223478 * value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier(&___message_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
