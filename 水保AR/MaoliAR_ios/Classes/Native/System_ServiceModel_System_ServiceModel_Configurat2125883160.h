﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.WindowsClientElement
struct  WindowsClientElement_t2125883160  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct WindowsClientElement_t2125883160_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.WindowsClientElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.WindowsClientElement::allowed_impersonation_level
	ConfigurationProperty_t3590861854 * ___allowed_impersonation_level_14;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.WindowsClientElement::allow_ntlm
	ConfigurationProperty_t3590861854 * ___allow_ntlm_15;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(WindowsClientElement_t2125883160_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier(&___properties_13, value);
	}

	inline static int32_t get_offset_of_allowed_impersonation_level_14() { return static_cast<int32_t>(offsetof(WindowsClientElement_t2125883160_StaticFields, ___allowed_impersonation_level_14)); }
	inline ConfigurationProperty_t3590861854 * get_allowed_impersonation_level_14() const { return ___allowed_impersonation_level_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_allowed_impersonation_level_14() { return &___allowed_impersonation_level_14; }
	inline void set_allowed_impersonation_level_14(ConfigurationProperty_t3590861854 * value)
	{
		___allowed_impersonation_level_14 = value;
		Il2CppCodeGenWriteBarrier(&___allowed_impersonation_level_14, value);
	}

	inline static int32_t get_offset_of_allow_ntlm_15() { return static_cast<int32_t>(offsetof(WindowsClientElement_t2125883160_StaticFields, ___allow_ntlm_15)); }
	inline ConfigurationProperty_t3590861854 * get_allow_ntlm_15() const { return ___allow_ntlm_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_allow_ntlm_15() { return &___allow_ntlm_15; }
	inline void set_allow_ntlm_15(ConfigurationProperty_t3590861854 * value)
	{
		___allow_ntlm_15 = value;
		Il2CppCodeGenWriteBarrier(&___allow_ntlm_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
