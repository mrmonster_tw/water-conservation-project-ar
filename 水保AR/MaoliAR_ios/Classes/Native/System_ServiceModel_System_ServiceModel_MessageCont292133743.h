﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"
#include "System_System_Net_Security_ProtectionLevel2559578148.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.MessageContractAttribute
struct  MessageContractAttribute_t292133743  : public Attribute_t861562559
{
public:
	// System.Boolean System.ServiceModel.MessageContractAttribute::has_protection_level
	bool ___has_protection_level_0;
	// System.Boolean System.ServiceModel.MessageContractAttribute::is_wrapped
	bool ___is_wrapped_1;
	// System.String System.ServiceModel.MessageContractAttribute::name
	String_t* ___name_2;
	// System.String System.ServiceModel.MessageContractAttribute::ns
	String_t* ___ns_3;
	// System.Net.Security.ProtectionLevel System.ServiceModel.MessageContractAttribute::protection_level
	int32_t ___protection_level_4;

public:
	inline static int32_t get_offset_of_has_protection_level_0() { return static_cast<int32_t>(offsetof(MessageContractAttribute_t292133743, ___has_protection_level_0)); }
	inline bool get_has_protection_level_0() const { return ___has_protection_level_0; }
	inline bool* get_address_of_has_protection_level_0() { return &___has_protection_level_0; }
	inline void set_has_protection_level_0(bool value)
	{
		___has_protection_level_0 = value;
	}

	inline static int32_t get_offset_of_is_wrapped_1() { return static_cast<int32_t>(offsetof(MessageContractAttribute_t292133743, ___is_wrapped_1)); }
	inline bool get_is_wrapped_1() const { return ___is_wrapped_1; }
	inline bool* get_address_of_is_wrapped_1() { return &___is_wrapped_1; }
	inline void set_is_wrapped_1(bool value)
	{
		___is_wrapped_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(MessageContractAttribute_t292133743, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}

	inline static int32_t get_offset_of_ns_3() { return static_cast<int32_t>(offsetof(MessageContractAttribute_t292133743, ___ns_3)); }
	inline String_t* get_ns_3() const { return ___ns_3; }
	inline String_t** get_address_of_ns_3() { return &___ns_3; }
	inline void set_ns_3(String_t* value)
	{
		___ns_3 = value;
		Il2CppCodeGenWriteBarrier(&___ns_3, value);
	}

	inline static int32_t get_offset_of_protection_level_4() { return static_cast<int32_t>(offsetof(MessageContractAttribute_t292133743, ___protection_level_4)); }
	inline int32_t get_protection_level_4() const { return ___protection_level_4; }
	inline int32_t* get_address_of_protection_level_4() { return &___protection_level_4; }
	inline void set_protection_level_4(int32_t value)
	{
		___protection_level_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
