﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_HtmlControls_HtmlInputCon1839583126.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.HtmlControls.HtmlInputButton
struct  HtmlInputButton_t695971026  : public HtmlInputControl_t1839583126
{
public:

public:
};

struct HtmlInputButton_t695971026_StaticFields
{
public:
	// System.Object System.Web.UI.HtmlControls.HtmlInputButton::ServerClickEvent
	Il2CppObject * ___ServerClickEvent_30;

public:
	inline static int32_t get_offset_of_ServerClickEvent_30() { return static_cast<int32_t>(offsetof(HtmlInputButton_t695971026_StaticFields, ___ServerClickEvent_30)); }
	inline Il2CppObject * get_ServerClickEvent_30() const { return ___ServerClickEvent_30; }
	inline Il2CppObject ** get_address_of_ServerClickEvent_30() { return &___ServerClickEvent_30; }
	inline void set_ServerClickEvent_30(Il2CppObject * value)
	{
		___ServerClickEvent_30 = value;
		Il2CppCodeGenWriteBarrier(&___ServerClickEvent_30, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
