﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Object
struct Il2CppObject;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.String
struct String_t;
// System.Web.UI.ControlCollection
struct ControlCollection_t4212191938;
// System.Web.UI.Control
struct Control_t3006474639;
// System.Web.UI.Page
struct Page_t770747966;
// System.ComponentModel.ISite
struct ISite_t4006303512;
// System.Web.UI.StateBag
struct StateBag_t282928164;
// System.ComponentModel.EventHandlerList
struct EventHandlerList_t1108123056;
// System.Web.UI.RenderMethod
struct RenderMethod_t2959628882;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Web.UI.TemplateControl
struct TemplateControl_t1922940480;
// System.Web.UI.Adapters.ControlAdapter
struct ControlAdapter_t3811171780;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.Control
struct  Control_t3006474639  : public Il2CppObject
{
public:
	// System.Int32 System.Web.UI.Control::event_mask
	int32_t ___event_mask_7;
	// System.String System.Web.UI.Control::uniqueID
	String_t* ___uniqueID_8;
	// System.String System.Web.UI.Control::_userId
	String_t* ____userId_9;
	// System.Web.UI.ControlCollection System.Web.UI.Control::_controls
	ControlCollection_t4212191938 * ____controls_10;
	// System.Web.UI.Control System.Web.UI.Control::_namingContainer
	Control_t3006474639 * ____namingContainer_11;
	// System.Web.UI.Page System.Web.UI.Control::_page
	Page_t770747966 * ____page_12;
	// System.Web.UI.Control System.Web.UI.Control::_parent
	Control_t3006474639 * ____parent_13;
	// System.ComponentModel.ISite System.Web.UI.Control::_site
	Il2CppObject * ____site_14;
	// System.Web.UI.StateBag System.Web.UI.Control::_viewState
	StateBag_t282928164 * ____viewState_15;
	// System.ComponentModel.EventHandlerList System.Web.UI.Control::_events
	EventHandlerList_t1108123056 * ____events_16;
	// System.Web.UI.RenderMethod System.Web.UI.Control::_renderMethodDelegate
	RenderMethod_t2959628882 * ____renderMethodDelegate_17;
	// System.Collections.Hashtable System.Web.UI.Control::_controlsCache
	Hashtable_t1853889766 * ____controlsCache_18;
	// System.Int32 System.Web.UI.Control::defaultNumberID
	int32_t ___defaultNumberID_19;
	// System.Collections.Hashtable System.Web.UI.Control::pendingVS
	Hashtable_t1853889766 * ___pendingVS_20;
	// System.Web.UI.TemplateControl System.Web.UI.Control::_templateControl
	TemplateControl_t1922940480 * ____templateControl_21;
	// System.String System.Web.UI.Control::_templateSourceDirectory
	String_t* ____templateSourceDirectory_22;
	// System.Int32 System.Web.UI.Control::stateMask
	int32_t ___stateMask_23;
	// System.Web.UI.Adapters.ControlAdapter System.Web.UI.Control::adapter
	ControlAdapter_t3811171780 * ___adapter_24;
	// System.Boolean System.Web.UI.Control::did_adapter_lookup
	bool ___did_adapter_lookup_25;
	// System.String System.Web.UI.Control::skinId
	String_t* ___skinId_26;
	// System.Boolean System.Web.UI.Control::_enableTheming
	bool ____enableTheming_27;

public:
	inline static int32_t get_offset_of_event_mask_7() { return static_cast<int32_t>(offsetof(Control_t3006474639, ___event_mask_7)); }
	inline int32_t get_event_mask_7() const { return ___event_mask_7; }
	inline int32_t* get_address_of_event_mask_7() { return &___event_mask_7; }
	inline void set_event_mask_7(int32_t value)
	{
		___event_mask_7 = value;
	}

	inline static int32_t get_offset_of_uniqueID_8() { return static_cast<int32_t>(offsetof(Control_t3006474639, ___uniqueID_8)); }
	inline String_t* get_uniqueID_8() const { return ___uniqueID_8; }
	inline String_t** get_address_of_uniqueID_8() { return &___uniqueID_8; }
	inline void set_uniqueID_8(String_t* value)
	{
		___uniqueID_8 = value;
		Il2CppCodeGenWriteBarrier(&___uniqueID_8, value);
	}

	inline static int32_t get_offset_of__userId_9() { return static_cast<int32_t>(offsetof(Control_t3006474639, ____userId_9)); }
	inline String_t* get__userId_9() const { return ____userId_9; }
	inline String_t** get_address_of__userId_9() { return &____userId_9; }
	inline void set__userId_9(String_t* value)
	{
		____userId_9 = value;
		Il2CppCodeGenWriteBarrier(&____userId_9, value);
	}

	inline static int32_t get_offset_of__controls_10() { return static_cast<int32_t>(offsetof(Control_t3006474639, ____controls_10)); }
	inline ControlCollection_t4212191938 * get__controls_10() const { return ____controls_10; }
	inline ControlCollection_t4212191938 ** get_address_of__controls_10() { return &____controls_10; }
	inline void set__controls_10(ControlCollection_t4212191938 * value)
	{
		____controls_10 = value;
		Il2CppCodeGenWriteBarrier(&____controls_10, value);
	}

	inline static int32_t get_offset_of__namingContainer_11() { return static_cast<int32_t>(offsetof(Control_t3006474639, ____namingContainer_11)); }
	inline Control_t3006474639 * get__namingContainer_11() const { return ____namingContainer_11; }
	inline Control_t3006474639 ** get_address_of__namingContainer_11() { return &____namingContainer_11; }
	inline void set__namingContainer_11(Control_t3006474639 * value)
	{
		____namingContainer_11 = value;
		Il2CppCodeGenWriteBarrier(&____namingContainer_11, value);
	}

	inline static int32_t get_offset_of__page_12() { return static_cast<int32_t>(offsetof(Control_t3006474639, ____page_12)); }
	inline Page_t770747966 * get__page_12() const { return ____page_12; }
	inline Page_t770747966 ** get_address_of__page_12() { return &____page_12; }
	inline void set__page_12(Page_t770747966 * value)
	{
		____page_12 = value;
		Il2CppCodeGenWriteBarrier(&____page_12, value);
	}

	inline static int32_t get_offset_of__parent_13() { return static_cast<int32_t>(offsetof(Control_t3006474639, ____parent_13)); }
	inline Control_t3006474639 * get__parent_13() const { return ____parent_13; }
	inline Control_t3006474639 ** get_address_of__parent_13() { return &____parent_13; }
	inline void set__parent_13(Control_t3006474639 * value)
	{
		____parent_13 = value;
		Il2CppCodeGenWriteBarrier(&____parent_13, value);
	}

	inline static int32_t get_offset_of__site_14() { return static_cast<int32_t>(offsetof(Control_t3006474639, ____site_14)); }
	inline Il2CppObject * get__site_14() const { return ____site_14; }
	inline Il2CppObject ** get_address_of__site_14() { return &____site_14; }
	inline void set__site_14(Il2CppObject * value)
	{
		____site_14 = value;
		Il2CppCodeGenWriteBarrier(&____site_14, value);
	}

	inline static int32_t get_offset_of__viewState_15() { return static_cast<int32_t>(offsetof(Control_t3006474639, ____viewState_15)); }
	inline StateBag_t282928164 * get__viewState_15() const { return ____viewState_15; }
	inline StateBag_t282928164 ** get_address_of__viewState_15() { return &____viewState_15; }
	inline void set__viewState_15(StateBag_t282928164 * value)
	{
		____viewState_15 = value;
		Il2CppCodeGenWriteBarrier(&____viewState_15, value);
	}

	inline static int32_t get_offset_of__events_16() { return static_cast<int32_t>(offsetof(Control_t3006474639, ____events_16)); }
	inline EventHandlerList_t1108123056 * get__events_16() const { return ____events_16; }
	inline EventHandlerList_t1108123056 ** get_address_of__events_16() { return &____events_16; }
	inline void set__events_16(EventHandlerList_t1108123056 * value)
	{
		____events_16 = value;
		Il2CppCodeGenWriteBarrier(&____events_16, value);
	}

	inline static int32_t get_offset_of__renderMethodDelegate_17() { return static_cast<int32_t>(offsetof(Control_t3006474639, ____renderMethodDelegate_17)); }
	inline RenderMethod_t2959628882 * get__renderMethodDelegate_17() const { return ____renderMethodDelegate_17; }
	inline RenderMethod_t2959628882 ** get_address_of__renderMethodDelegate_17() { return &____renderMethodDelegate_17; }
	inline void set__renderMethodDelegate_17(RenderMethod_t2959628882 * value)
	{
		____renderMethodDelegate_17 = value;
		Il2CppCodeGenWriteBarrier(&____renderMethodDelegate_17, value);
	}

	inline static int32_t get_offset_of__controlsCache_18() { return static_cast<int32_t>(offsetof(Control_t3006474639, ____controlsCache_18)); }
	inline Hashtable_t1853889766 * get__controlsCache_18() const { return ____controlsCache_18; }
	inline Hashtable_t1853889766 ** get_address_of__controlsCache_18() { return &____controlsCache_18; }
	inline void set__controlsCache_18(Hashtable_t1853889766 * value)
	{
		____controlsCache_18 = value;
		Il2CppCodeGenWriteBarrier(&____controlsCache_18, value);
	}

	inline static int32_t get_offset_of_defaultNumberID_19() { return static_cast<int32_t>(offsetof(Control_t3006474639, ___defaultNumberID_19)); }
	inline int32_t get_defaultNumberID_19() const { return ___defaultNumberID_19; }
	inline int32_t* get_address_of_defaultNumberID_19() { return &___defaultNumberID_19; }
	inline void set_defaultNumberID_19(int32_t value)
	{
		___defaultNumberID_19 = value;
	}

	inline static int32_t get_offset_of_pendingVS_20() { return static_cast<int32_t>(offsetof(Control_t3006474639, ___pendingVS_20)); }
	inline Hashtable_t1853889766 * get_pendingVS_20() const { return ___pendingVS_20; }
	inline Hashtable_t1853889766 ** get_address_of_pendingVS_20() { return &___pendingVS_20; }
	inline void set_pendingVS_20(Hashtable_t1853889766 * value)
	{
		___pendingVS_20 = value;
		Il2CppCodeGenWriteBarrier(&___pendingVS_20, value);
	}

	inline static int32_t get_offset_of__templateControl_21() { return static_cast<int32_t>(offsetof(Control_t3006474639, ____templateControl_21)); }
	inline TemplateControl_t1922940480 * get__templateControl_21() const { return ____templateControl_21; }
	inline TemplateControl_t1922940480 ** get_address_of__templateControl_21() { return &____templateControl_21; }
	inline void set__templateControl_21(TemplateControl_t1922940480 * value)
	{
		____templateControl_21 = value;
		Il2CppCodeGenWriteBarrier(&____templateControl_21, value);
	}

	inline static int32_t get_offset_of__templateSourceDirectory_22() { return static_cast<int32_t>(offsetof(Control_t3006474639, ____templateSourceDirectory_22)); }
	inline String_t* get__templateSourceDirectory_22() const { return ____templateSourceDirectory_22; }
	inline String_t** get_address_of__templateSourceDirectory_22() { return &____templateSourceDirectory_22; }
	inline void set__templateSourceDirectory_22(String_t* value)
	{
		____templateSourceDirectory_22 = value;
		Il2CppCodeGenWriteBarrier(&____templateSourceDirectory_22, value);
	}

	inline static int32_t get_offset_of_stateMask_23() { return static_cast<int32_t>(offsetof(Control_t3006474639, ___stateMask_23)); }
	inline int32_t get_stateMask_23() const { return ___stateMask_23; }
	inline int32_t* get_address_of_stateMask_23() { return &___stateMask_23; }
	inline void set_stateMask_23(int32_t value)
	{
		___stateMask_23 = value;
	}

	inline static int32_t get_offset_of_adapter_24() { return static_cast<int32_t>(offsetof(Control_t3006474639, ___adapter_24)); }
	inline ControlAdapter_t3811171780 * get_adapter_24() const { return ___adapter_24; }
	inline ControlAdapter_t3811171780 ** get_address_of_adapter_24() { return &___adapter_24; }
	inline void set_adapter_24(ControlAdapter_t3811171780 * value)
	{
		___adapter_24 = value;
		Il2CppCodeGenWriteBarrier(&___adapter_24, value);
	}

	inline static int32_t get_offset_of_did_adapter_lookup_25() { return static_cast<int32_t>(offsetof(Control_t3006474639, ___did_adapter_lookup_25)); }
	inline bool get_did_adapter_lookup_25() const { return ___did_adapter_lookup_25; }
	inline bool* get_address_of_did_adapter_lookup_25() { return &___did_adapter_lookup_25; }
	inline void set_did_adapter_lookup_25(bool value)
	{
		___did_adapter_lookup_25 = value;
	}

	inline static int32_t get_offset_of_skinId_26() { return static_cast<int32_t>(offsetof(Control_t3006474639, ___skinId_26)); }
	inline String_t* get_skinId_26() const { return ___skinId_26; }
	inline String_t** get_address_of_skinId_26() { return &___skinId_26; }
	inline void set_skinId_26(String_t* value)
	{
		___skinId_26 = value;
		Il2CppCodeGenWriteBarrier(&___skinId_26, value);
	}

	inline static int32_t get_offset_of__enableTheming_27() { return static_cast<int32_t>(offsetof(Control_t3006474639, ____enableTheming_27)); }
	inline bool get__enableTheming_27() const { return ____enableTheming_27; }
	inline bool* get_address_of__enableTheming_27() { return &____enableTheming_27; }
	inline void set__enableTheming_27(bool value)
	{
		____enableTheming_27 = value;
	}
};

struct Control_t3006474639_StaticFields
{
public:
	// System.Object System.Web.UI.Control::DataBindingEvent
	Il2CppObject * ___DataBindingEvent_0;
	// System.Object System.Web.UI.Control::DisposedEvent
	Il2CppObject * ___DisposedEvent_1;
	// System.Object System.Web.UI.Control::InitEvent
	Il2CppObject * ___InitEvent_2;
	// System.Object System.Web.UI.Control::LoadEvent
	Il2CppObject * ___LoadEvent_3;
	// System.Object System.Web.UI.Control::PreRenderEvent
	Il2CppObject * ___PreRenderEvent_4;
	// System.Object System.Web.UI.Control::UnloadEvent
	Il2CppObject * ___UnloadEvent_5;
	// System.String[] System.Web.UI.Control::defaultNameArray
	StringU5BU5D_t1281789340* ___defaultNameArray_6;

public:
	inline static int32_t get_offset_of_DataBindingEvent_0() { return static_cast<int32_t>(offsetof(Control_t3006474639_StaticFields, ___DataBindingEvent_0)); }
	inline Il2CppObject * get_DataBindingEvent_0() const { return ___DataBindingEvent_0; }
	inline Il2CppObject ** get_address_of_DataBindingEvent_0() { return &___DataBindingEvent_0; }
	inline void set_DataBindingEvent_0(Il2CppObject * value)
	{
		___DataBindingEvent_0 = value;
		Il2CppCodeGenWriteBarrier(&___DataBindingEvent_0, value);
	}

	inline static int32_t get_offset_of_DisposedEvent_1() { return static_cast<int32_t>(offsetof(Control_t3006474639_StaticFields, ___DisposedEvent_1)); }
	inline Il2CppObject * get_DisposedEvent_1() const { return ___DisposedEvent_1; }
	inline Il2CppObject ** get_address_of_DisposedEvent_1() { return &___DisposedEvent_1; }
	inline void set_DisposedEvent_1(Il2CppObject * value)
	{
		___DisposedEvent_1 = value;
		Il2CppCodeGenWriteBarrier(&___DisposedEvent_1, value);
	}

	inline static int32_t get_offset_of_InitEvent_2() { return static_cast<int32_t>(offsetof(Control_t3006474639_StaticFields, ___InitEvent_2)); }
	inline Il2CppObject * get_InitEvent_2() const { return ___InitEvent_2; }
	inline Il2CppObject ** get_address_of_InitEvent_2() { return &___InitEvent_2; }
	inline void set_InitEvent_2(Il2CppObject * value)
	{
		___InitEvent_2 = value;
		Il2CppCodeGenWriteBarrier(&___InitEvent_2, value);
	}

	inline static int32_t get_offset_of_LoadEvent_3() { return static_cast<int32_t>(offsetof(Control_t3006474639_StaticFields, ___LoadEvent_3)); }
	inline Il2CppObject * get_LoadEvent_3() const { return ___LoadEvent_3; }
	inline Il2CppObject ** get_address_of_LoadEvent_3() { return &___LoadEvent_3; }
	inline void set_LoadEvent_3(Il2CppObject * value)
	{
		___LoadEvent_3 = value;
		Il2CppCodeGenWriteBarrier(&___LoadEvent_3, value);
	}

	inline static int32_t get_offset_of_PreRenderEvent_4() { return static_cast<int32_t>(offsetof(Control_t3006474639_StaticFields, ___PreRenderEvent_4)); }
	inline Il2CppObject * get_PreRenderEvent_4() const { return ___PreRenderEvent_4; }
	inline Il2CppObject ** get_address_of_PreRenderEvent_4() { return &___PreRenderEvent_4; }
	inline void set_PreRenderEvent_4(Il2CppObject * value)
	{
		___PreRenderEvent_4 = value;
		Il2CppCodeGenWriteBarrier(&___PreRenderEvent_4, value);
	}

	inline static int32_t get_offset_of_UnloadEvent_5() { return static_cast<int32_t>(offsetof(Control_t3006474639_StaticFields, ___UnloadEvent_5)); }
	inline Il2CppObject * get_UnloadEvent_5() const { return ___UnloadEvent_5; }
	inline Il2CppObject ** get_address_of_UnloadEvent_5() { return &___UnloadEvent_5; }
	inline void set_UnloadEvent_5(Il2CppObject * value)
	{
		___UnloadEvent_5 = value;
		Il2CppCodeGenWriteBarrier(&___UnloadEvent_5, value);
	}

	inline static int32_t get_offset_of_defaultNameArray_6() { return static_cast<int32_t>(offsetof(Control_t3006474639_StaticFields, ___defaultNameArray_6)); }
	inline StringU5BU5D_t1281789340* get_defaultNameArray_6() const { return ___defaultNameArray_6; }
	inline StringU5BU5D_t1281789340** get_address_of_defaultNameArray_6() { return &___defaultNameArray_6; }
	inline void set_defaultNameArray_6(StringU5BU5D_t1281789340* value)
	{
		___defaultNameArray_6 = value;
		Il2CppCodeGenWriteBarrier(&___defaultNameArray_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
