﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Me859888167.h"

// System.ServiceModel.Channels.RecipientMessageSecurityBindingSupport
struct RecipientMessageSecurityBindingSupport_t1155974079;
// System.ServiceModel.Channels.SecurityRequestContext
struct SecurityRequestContext_t3267761371;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.RecipientMessageSecurityGenerator
struct  RecipientMessageSecurityGenerator_t2220786290  : public MessageSecurityGenerator_t859888167
{
public:
	// System.ServiceModel.Channels.RecipientMessageSecurityBindingSupport System.ServiceModel.Channels.RecipientMessageSecurityGenerator::security
	RecipientMessageSecurityBindingSupport_t1155974079 * ___security_5;
	// System.ServiceModel.Channels.SecurityRequestContext System.ServiceModel.Channels.RecipientMessageSecurityGenerator::req_ctx
	SecurityRequestContext_t3267761371 * ___req_ctx_6;

public:
	inline static int32_t get_offset_of_security_5() { return static_cast<int32_t>(offsetof(RecipientMessageSecurityGenerator_t2220786290, ___security_5)); }
	inline RecipientMessageSecurityBindingSupport_t1155974079 * get_security_5() const { return ___security_5; }
	inline RecipientMessageSecurityBindingSupport_t1155974079 ** get_address_of_security_5() { return &___security_5; }
	inline void set_security_5(RecipientMessageSecurityBindingSupport_t1155974079 * value)
	{
		___security_5 = value;
		Il2CppCodeGenWriteBarrier(&___security_5, value);
	}

	inline static int32_t get_offset_of_req_ctx_6() { return static_cast<int32_t>(offsetof(RecipientMessageSecurityGenerator_t2220786290, ___req_ctx_6)); }
	inline SecurityRequestContext_t3267761371 * get_req_ctx_6() const { return ___req_ctx_6; }
	inline SecurityRequestContext_t3267761371 ** get_address_of_req_ctx_6() { return &___req_ctx_6; }
	inline void set_req_ctx_6(SecurityRequestContext_t3267761371 * value)
	{
		___req_ctx_6 = value;
		Il2CppCodeGenWriteBarrier(&___req_ctx_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
