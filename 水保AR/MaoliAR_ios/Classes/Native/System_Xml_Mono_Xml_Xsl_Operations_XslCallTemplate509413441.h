﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Operations_XslCompiledElemen50593777.h"

// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2760654312;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslCallTemplate
struct  XslCallTemplate_t509413441  : public XslCompiledElement_t50593777
{
public:
	// System.Xml.XmlQualifiedName Mono.Xml.Xsl.Operations.XslCallTemplate::name
	XmlQualifiedName_t2760654312 * ___name_3;
	// System.Collections.ArrayList Mono.Xml.Xsl.Operations.XslCallTemplate::withParams
	ArrayList_t2718874744 * ___withParams_4;

public:
	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(XslCallTemplate_t509413441, ___name_3)); }
	inline XmlQualifiedName_t2760654312 * get_name_3() const { return ___name_3; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(XmlQualifiedName_t2760654312 * value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier(&___name_3, value);
	}

	inline static int32_t get_offset_of_withParams_4() { return static_cast<int32_t>(offsetof(XslCallTemplate_t509413441, ___withParams_4)); }
	inline ArrayList_t2718874744 * get_withParams_4() const { return ___withParams_4; }
	inline ArrayList_t2718874744 ** get_address_of_withParams_4() { return &___withParams_4; }
	inline void set_withParams_4(ArrayList_t2718874744 * value)
	{
		___withParams_4 = value;
		Il2CppCodeGenWriteBarrier(&___withParams_4, value);
	}
};

struct XslCallTemplate_t509413441_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.Operations.XslCallTemplate::<>f__switch$map8
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map8_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map8_5() { return static_cast<int32_t>(offsetof(XslCallTemplate_t509413441_StaticFields, ___U3CU3Ef__switchU24map8_5)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map8_5() const { return ___U3CU3Ef__switchU24map8_5; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map8_5() { return &___U3CU3Ef__switchU24map8_5; }
	inline void set_U3CU3Ef__switchU24map8_5(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map8_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map8_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
