﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// notMobile_MapController
struct  notMobile_MapController_t3835353441  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image notMobile_MapController::map_m
	Image_t2670269651 * ___map_m_2;
	// UnityEngine.Sprite[] notMobile_MapController::spriteIndex
	SpriteU5BU5D_t2581906349* ___spriteIndex_3;

public:
	inline static int32_t get_offset_of_map_m_2() { return static_cast<int32_t>(offsetof(notMobile_MapController_t3835353441, ___map_m_2)); }
	inline Image_t2670269651 * get_map_m_2() const { return ___map_m_2; }
	inline Image_t2670269651 ** get_address_of_map_m_2() { return &___map_m_2; }
	inline void set_map_m_2(Image_t2670269651 * value)
	{
		___map_m_2 = value;
		Il2CppCodeGenWriteBarrier(&___map_m_2, value);
	}

	inline static int32_t get_offset_of_spriteIndex_3() { return static_cast<int32_t>(offsetof(notMobile_MapController_t3835353441, ___spriteIndex_3)); }
	inline SpriteU5BU5D_t2581906349* get_spriteIndex_3() const { return ___spriteIndex_3; }
	inline SpriteU5BU5D_t2581906349** get_address_of_spriteIndex_3() { return &___spriteIndex_3; }
	inline void set_spriteIndex_3(SpriteU5BU5D_t2581906349* value)
	{
		___spriteIndex_3 = value;
		Il2CppCodeGenWriteBarrier(&___spriteIndex_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
