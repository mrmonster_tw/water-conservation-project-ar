﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Tokens_S1943429813.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Tokens.SamlAssertionKeyIdentifierClause
struct  SamlAssertionKeyIdentifierClause_t4042822033  : public SecurityKeyIdentifierClause_t1943429813
{
public:
	// System.String System.IdentityModel.Tokens.SamlAssertionKeyIdentifierClause::id
	String_t* ___id_3;

public:
	inline static int32_t get_offset_of_id_3() { return static_cast<int32_t>(offsetof(SamlAssertionKeyIdentifierClause_t4042822033, ___id_3)); }
	inline String_t* get_id_3() const { return ___id_3; }
	inline String_t** get_address_of_id_3() { return &___id_3; }
	inline void set_id_3(String_t* value)
	{
		___id_3 = value;
		Il2CppCodeGenWriteBarrier(&___id_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
