﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_L2_Soultion034219689772.h"
#include "AssemblyU2DCSharp_Rightobj523783405.h"
#include "AssemblyU2DCSharp_Soultion01_ch3285493964.h"
#include "AssemblyU2DCSharp_Soultion02_ch3287656652.h"
#include "AssemblyU2DCSharp_EquipItems327539537.h"
#include "AssemblyU2DCSharp_EquipRandomItem236311155.h"
#include "AssemblyU2DCSharp_UICursor770290061.h"
#include "AssemblyU2DCSharp_UIEquipmentSlot103405972.h"
#include "AssemblyU2DCSharp_UIItemSlot3702669905.h"
#include "AssemblyU2DCSharp_UIItemStorage2918019538.h"
#include "AssemblyU2DCSharp_UIStorageSlot392832716.h"
#include "AssemblyU2DCSharp_InvAttachmentPoint2909008346.h"
#include "AssemblyU2DCSharp_InvBaseItem2503063521.h"
#include "AssemblyU2DCSharp_InvBaseItem_Slot994390089.h"
#include "AssemblyU2DCSharp_InvDatabase3341607346.h"
#include "AssemblyU2DCSharp_InvEquipment3413562611.h"
#include "AssemblyU2DCSharp_InvGameItem1203288033.h"
#include "AssemblyU2DCSharp_InvGameItem_Quality1425208229.h"
#include "AssemblyU2DCSharp_InvStat493203737.h"
#include "AssemblyU2DCSharp_InvStat_Identifier3929935772.h"
#include "AssemblyU2DCSharp_InvStat_Modifier3329030742.h"
#include "AssemblyU2DCSharp_ChatInput2323778298.h"
#include "AssemblyU2DCSharp_DownloadTexture185951951.h"
#include "AssemblyU2DCSharp_DownloadTexture_U3CStartU3Ec__It1871398101.h"
#include "AssemblyU2DCSharp_EnvelopContent3305418577.h"
#include "AssemblyU2DCSharp_ExampleDragDropItem398806628.h"
#include "AssemblyU2DCSharp_ExampleDragDropSurface2709993285.h"
#include "AssemblyU2DCSharp_LagPosition1022635563.h"
#include "AssemblyU2DCSharp_LagRotation3758629971.h"
#include "AssemblyU2DCSharp_LoadLevelOnClick2255431531.h"
#include "AssemblyU2DCSharp_LookAtTarget1968561443.h"
#include "AssemblyU2DCSharp_OpenURLOnClick66867727.h"
#include "AssemblyU2DCSharp_PanWithMouse28913686.h"
#include "AssemblyU2DCSharp_PlayIdleAnimations3347250912.h"
#include "AssemblyU2DCSharp_SetColorPickerColor3377107005.h"
#include "AssemblyU2DCSharp_Spin2228728196.h"
#include "AssemblyU2DCSharp_SpinWithMouse3499877925.h"
#include "AssemblyU2DCSharp_Tutorial52411054926.h"
#include "AssemblyU2DCSharp_UISliderColors4207082228.h"
#include "AssemblyU2DCSharp_WindowAutoYaw800598140.h"
#include "AssemblyU2DCSharp_WindowDragTilt1337067838.h"
#include "AssemblyU2DCSharp_LanguageSelection1927862481.h"
#include "AssemblyU2DCSharp_TypewriterEffect1340895546.h"
#include "AssemblyU2DCSharp_TypewriterEffect_FadeEntry639421133.h"
#include "AssemblyU2DCSharp_UIButton1100396938.h"
#include "AssemblyU2DCSharp_UIButtonActivate1184781629.h"
#include "AssemblyU2DCSharp_UIButtonColor2038074180.h"
#include "AssemblyU2DCSharp_UIButtonColor_State3991372483.h"
#include "AssemblyU2DCSharp_UIButtonKeys486517330.h"
#include "AssemblyU2DCSharp_UIButtonMessage952534536.h"
#include "AssemblyU2DCSharp_UIButtonMessage_Trigger148524997.h"
#include "AssemblyU2DCSharp_UIButtonOffset4041087173.h"
#include "AssemblyU2DCSharp_UIButtonRotation284788413.h"
#include "AssemblyU2DCSharp_UIButtonScale3973287916.h"
#include "AssemblyU2DCSharp_UICenterOnChild253063637.h"
#include "AssemblyU2DCSharp_UICenterOnChild_OnCenterCallback2077531662.h"
#include "AssemblyU2DCSharp_UICenterOnClick4058804012.h"
#include "AssemblyU2DCSharp_UIDragCamera3716731078.h"
#include "AssemblyU2DCSharp_UIDragDropContainer2199185397.h"
#include "AssemblyU2DCSharp_UIDragDropItem3922849381.h"
#include "AssemblyU2DCSharp_UIDragDropItem_Restriction4221874387.h"
#include "AssemblyU2DCSharp_UIDragDropItem_U3CEnableDragScrol656083354.h"
#include "AssemblyU2DCSharp_UIDragDropRoot3716445314.h"
#include "AssemblyU2DCSharp_UIDragObject167167434.h"
#include "AssemblyU2DCSharp_UIDragObject_DragEffect2686339116.h"
#include "AssemblyU2DCSharp_UIDragResize3151593629.h"
#include "AssemblyU2DCSharp_UIDragScrollView2492060641.h"
#include "AssemblyU2DCSharp_UIDraggableCamera1644204495.h"
#include "AssemblyU2DCSharp_UIEventTrigger4038454782.h"
#include "AssemblyU2DCSharp_UIForwardEvents2244381111.h"
#include "AssemblyU2DCSharp_UIGrid1536638187.h"
#include "AssemblyU2DCSharp_UIGrid_OnReposition1372889220.h"
#include "AssemblyU2DCSharp_UIGrid_Arrangement1850956547.h"
#include "AssemblyU2DCSharp_UIGrid_Sorting533260699.h"
#include "AssemblyU2DCSharp_UIImageButton3745027676.h"
#include "AssemblyU2DCSharp_UIKeyBinding2698445843.h"
#include "AssemblyU2DCSharp_UIKeyBinding_Action4142137194.h"
#include "AssemblyU2DCSharp_UIKeyBinding_Modifier1697911081.h"
#include "AssemblyU2DCSharp_UIKeyNavigation4244956403.h"
#include "AssemblyU2DCSharp_UIKeyNavigation_Constraint2778583555.h"
#include "AssemblyU2DCSharp_UIPlayAnimation1976799768.h"
#include "AssemblyU2DCSharp_UIPlaySound1382070071.h"
#include "AssemblyU2DCSharp_UIPlaySound_Trigger2172727401.h"
#include "AssemblyU2DCSharp_UIPlayTween2213781924.h"
#include "AssemblyU2DCSharp_UIPopupList4167399471.h"
#include "AssemblyU2DCSharp_UIPopupList_Position1583461796.h"
#include "AssemblyU2DCSharp_UIPopupList_OpenOn1997085761.h"
#include "AssemblyU2DCSharp_UIPopupList_LegacyEvent2749056879.h"
#include "AssemblyU2DCSharp_UIPopupList_U3CUpdateTweenPositi2265653740.h"
#include "AssemblyU2DCSharp_UIPopupList_U3CCloseIfUnselected2833751522.h"
#include "AssemblyU2DCSharp_UIProgressBar1222110469.h"
#include "AssemblyU2DCSharp_UIProgressBar_FillDirection551265397.h"
#include "AssemblyU2DCSharp_UIProgressBar_OnDragFinished3715779777.h"
#include "AssemblyU2DCSharp_UISavedOption2504198202.h"
#include "AssemblyU2DCSharp_UIScrollBar1285772729.h"
#include "AssemblyU2DCSharp_UIScrollBar_Direction340832489.h"
#include "AssemblyU2DCSharp_UIScrollView1973404950.h"
#include "AssemblyU2DCSharp_UIScrollView_Movement247277292.h"
#include "AssemblyU2DCSharp_UIScrollView_DragEffect491601944.h"
#include "AssemblyU2DCSharp_UIScrollView_ShowCondition1535012424.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5800 = { sizeof (L2_Soultion03_t4219689772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5800[5] = 
{
	L2_Soultion03_t4219689772::get_offset_of_L2_Soultion03_ball_2(),
	L2_Soultion03_t4219689772::get_offset_of_finger_3(),
	L2_Soultion03_t4219689772::get_offset_of_touchfinger_4(),
	L2_Soultion03_t4219689772::get_offset_of_s_1_5(),
	L2_Soultion03_t4219689772::get_offset_of_s_2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5801 = { sizeof (Rightobj_t523783405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5801[1] = 
{
	Rightobj_t523783405::get_offset_of_cs_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5802 = { sizeof (Soultion01_ch_t3285493964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5802[4] = 
{
	Soultion01_ch_t3285493964::get_offset_of_Soultion01_ball_2(),
	Soultion01_ch_t3285493964::get_offset_of_finger_3(),
	Soultion01_ch_t3285493964::get_offset_of_touchfinger_4(),
	Soultion01_ch_t3285493964::get_offset_of_s_2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5803 = { sizeof (Soultion02_ch_t3287656652), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5803[4] = 
{
	Soultion02_ch_t3287656652::get_offset_of_Soultion02_ball_2(),
	Soultion02_ch_t3287656652::get_offset_of_finger_3(),
	Soultion02_ch_t3287656652::get_offset_of_touchfinger_4(),
	Soultion02_ch_t3287656652::get_offset_of_s_1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5804 = { sizeof (EquipItems_t327539537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5804[1] = 
{
	EquipItems_t327539537::get_offset_of_itemIDs_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5805 = { sizeof (EquipRandomItem_t236311155), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5805[1] = 
{
	EquipRandomItem_t236311155::get_offset_of_equipment_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5806 = { sizeof (UICursor_t770290061), -1, sizeof(UICursor_t770290061_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5806[6] = 
{
	UICursor_t770290061_StaticFields::get_offset_of_instance_2(),
	UICursor_t770290061::get_offset_of_uiCamera_3(),
	UICursor_t770290061::get_offset_of_mTrans_4(),
	UICursor_t770290061::get_offset_of_mSprite_5(),
	UICursor_t770290061::get_offset_of_mAtlas_6(),
	UICursor_t770290061::get_offset_of_mSpriteName_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5807 = { sizeof (UIEquipmentSlot_t103405972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5807[2] = 
{
	UIEquipmentSlot_t103405972::get_offset_of_equipment_11(),
	UIEquipmentSlot_t103405972::get_offset_of_slot_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5808 = { sizeof (UIItemSlot_t3702669905), -1, sizeof(UIItemSlot_t3702669905_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5808[9] = 
{
	UIItemSlot_t3702669905::get_offset_of_icon_2(),
	UIItemSlot_t3702669905::get_offset_of_background_3(),
	UIItemSlot_t3702669905::get_offset_of_label_4(),
	UIItemSlot_t3702669905::get_offset_of_grabSound_5(),
	UIItemSlot_t3702669905::get_offset_of_placeSound_6(),
	UIItemSlot_t3702669905::get_offset_of_errorSound_7(),
	UIItemSlot_t3702669905::get_offset_of_mItem_8(),
	UIItemSlot_t3702669905::get_offset_of_mText_9(),
	UIItemSlot_t3702669905_StaticFields::get_offset_of_mDraggedItem_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5809 = { sizeof (UIItemStorage_t2918019538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5809[8] = 
{
	UIItemStorage_t2918019538::get_offset_of_maxItemCount_2(),
	UIItemStorage_t2918019538::get_offset_of_maxRows_3(),
	UIItemStorage_t2918019538::get_offset_of_maxColumns_4(),
	UIItemStorage_t2918019538::get_offset_of_template_5(),
	UIItemStorage_t2918019538::get_offset_of_background_6(),
	UIItemStorage_t2918019538::get_offset_of_spacing_7(),
	UIItemStorage_t2918019538::get_offset_of_padding_8(),
	UIItemStorage_t2918019538::get_offset_of_mItems_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5810 = { sizeof (UIStorageSlot_t392832716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5810[2] = 
{
	UIStorageSlot_t392832716::get_offset_of_storage_11(),
	UIStorageSlot_t392832716::get_offset_of_slot_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5811 = { sizeof (InvAttachmentPoint_t2909008346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5811[3] = 
{
	InvAttachmentPoint_t2909008346::get_offset_of_slot_2(),
	InvAttachmentPoint_t2909008346::get_offset_of_mPrefab_3(),
	InvAttachmentPoint_t2909008346::get_offset_of_mChild_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5812 = { sizeof (InvBaseItem_t2503063521), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5812[11] = 
{
	InvBaseItem_t2503063521::get_offset_of_id16_0(),
	InvBaseItem_t2503063521::get_offset_of_name_1(),
	InvBaseItem_t2503063521::get_offset_of_description_2(),
	InvBaseItem_t2503063521::get_offset_of_slot_3(),
	InvBaseItem_t2503063521::get_offset_of_minItemLevel_4(),
	InvBaseItem_t2503063521::get_offset_of_maxItemLevel_5(),
	InvBaseItem_t2503063521::get_offset_of_stats_6(),
	InvBaseItem_t2503063521::get_offset_of_attachment_7(),
	InvBaseItem_t2503063521::get_offset_of_color_8(),
	InvBaseItem_t2503063521::get_offset_of_iconAtlas_9(),
	InvBaseItem_t2503063521::get_offset_of_iconName_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5813 = { sizeof (Slot_t994390089)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5813[10] = 
{
	Slot_t994390089::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5814 = { sizeof (InvDatabase_t3341607346), -1, sizeof(InvDatabase_t3341607346_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5814[5] = 
{
	InvDatabase_t3341607346_StaticFields::get_offset_of_mList_2(),
	InvDatabase_t3341607346_StaticFields::get_offset_of_mIsDirty_3(),
	InvDatabase_t3341607346::get_offset_of_databaseID_4(),
	InvDatabase_t3341607346::get_offset_of_items_5(),
	InvDatabase_t3341607346::get_offset_of_iconAtlas_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5815 = { sizeof (InvEquipment_t3413562611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5815[2] = 
{
	InvEquipment_t3413562611::get_offset_of_mItems_2(),
	InvEquipment_t3413562611::get_offset_of_mAttachments_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5816 = { sizeof (InvGameItem_t1203288033), -1, sizeof(InvGameItem_t1203288033_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5816[5] = 
{
	InvGameItem_t1203288033::get_offset_of_mBaseItemID_0(),
	InvGameItem_t1203288033::get_offset_of_quality_1(),
	InvGameItem_t1203288033::get_offset_of_itemLevel_2(),
	InvGameItem_t1203288033::get_offset_of_mBaseItem_3(),
	InvGameItem_t1203288033_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5817 = { sizeof (Quality_t1425208229)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5817[14] = 
{
	Quality_t1425208229::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5818 = { sizeof (InvStat_t493203737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5818[3] = 
{
	InvStat_t493203737::get_offset_of_id_0(),
	InvStat_t493203737::get_offset_of_modifier_1(),
	InvStat_t493203737::get_offset_of_amount_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5819 = { sizeof (Identifier_t3929935772)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5819[11] = 
{
	Identifier_t3929935772::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5820 = { sizeof (Modifier_t3329030742)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5820[3] = 
{
	Modifier_t3329030742::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5821 = { sizeof (ChatInput_t2323778298), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5821[3] = 
{
	ChatInput_t2323778298::get_offset_of_textList_2(),
	ChatInput_t2323778298::get_offset_of_fillWithDummyData_3(),
	ChatInput_t2323778298::get_offset_of_mInput_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5822 = { sizeof (DownloadTexture_t185951951), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5822[3] = 
{
	DownloadTexture_t185951951::get_offset_of_url_2(),
	DownloadTexture_t185951951::get_offset_of_pixelPerfect_3(),
	DownloadTexture_t185951951::get_offset_of_mTex_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5823 = { sizeof (U3CStartU3Ec__Iterator0_t1871398101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5823[5] = 
{
	U3CStartU3Ec__Iterator0_t1871398101::get_offset_of_U3CwwwU3E__0_0(),
	U3CStartU3Ec__Iterator0_t1871398101::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t1871398101::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t1871398101::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t1871398101::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5824 = { sizeof (EnvelopContent_t3305418577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5824[6] = 
{
	EnvelopContent_t3305418577::get_offset_of_targetRoot_2(),
	EnvelopContent_t3305418577::get_offset_of_padLeft_3(),
	EnvelopContent_t3305418577::get_offset_of_padRight_4(),
	EnvelopContent_t3305418577::get_offset_of_padBottom_5(),
	EnvelopContent_t3305418577::get_offset_of_padTop_6(),
	EnvelopContent_t3305418577::get_offset_of_mStarted_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5825 = { sizeof (ExampleDragDropItem_t398806628), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5825[1] = 
{
	ExampleDragDropItem_t398806628::get_offset_of_prefab_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5826 = { sizeof (ExampleDragDropSurface_t2709993285), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5826[1] = 
{
	ExampleDragDropSurface_t2709993285::get_offset_of_rotatePlacedObject_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5827 = { sizeof (LagPosition_t1022635563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5827[6] = 
{
	LagPosition_t1022635563::get_offset_of_speed_2(),
	LagPosition_t1022635563::get_offset_of_ignoreTimeScale_3(),
	LagPosition_t1022635563::get_offset_of_mTrans_4(),
	LagPosition_t1022635563::get_offset_of_mRelative_5(),
	LagPosition_t1022635563::get_offset_of_mAbsolute_6(),
	LagPosition_t1022635563::get_offset_of_mStarted_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5828 = { sizeof (LagRotation_t3758629971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5828[5] = 
{
	LagRotation_t3758629971::get_offset_of_speed_2(),
	LagRotation_t3758629971::get_offset_of_ignoreTimeScale_3(),
	LagRotation_t3758629971::get_offset_of_mTrans_4(),
	LagRotation_t3758629971::get_offset_of_mRelative_5(),
	LagRotation_t3758629971::get_offset_of_mAbsolute_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5829 = { sizeof (LoadLevelOnClick_t2255431531), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5829[1] = 
{
	LoadLevelOnClick_t2255431531::get_offset_of_levelName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5830 = { sizeof (LookAtTarget_t1968561443), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5830[4] = 
{
	LookAtTarget_t1968561443::get_offset_of_level_2(),
	LookAtTarget_t1968561443::get_offset_of_target_3(),
	LookAtTarget_t1968561443::get_offset_of_speed_4(),
	LookAtTarget_t1968561443::get_offset_of_mTrans_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5831 = { sizeof (OpenURLOnClick_t66867727), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5832 = { sizeof (PanWithMouse_t28913686), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5832[5] = 
{
	PanWithMouse_t28913686::get_offset_of_degrees_2(),
	PanWithMouse_t28913686::get_offset_of_range_3(),
	PanWithMouse_t28913686::get_offset_of_mTrans_4(),
	PanWithMouse_t28913686::get_offset_of_mStart_5(),
	PanWithMouse_t28913686::get_offset_of_mRot_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5833 = { sizeof (PlayIdleAnimations_t3347250912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5833[5] = 
{
	PlayIdleAnimations_t3347250912::get_offset_of_mAnim_2(),
	PlayIdleAnimations_t3347250912::get_offset_of_mIdle_3(),
	PlayIdleAnimations_t3347250912::get_offset_of_mBreaks_4(),
	PlayIdleAnimations_t3347250912::get_offset_of_mNextBreak_5(),
	PlayIdleAnimations_t3347250912::get_offset_of_mLastIndex_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5834 = { sizeof (SetColorPickerColor_t3377107005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5834[1] = 
{
	SetColorPickerColor_t3377107005::get_offset_of_mWidget_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5835 = { sizeof (Spin_t2228728196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5835[4] = 
{
	Spin_t2228728196::get_offset_of_rotationsPerSecond_2(),
	Spin_t2228728196::get_offset_of_ignoreTimeScale_3(),
	Spin_t2228728196::get_offset_of_mRb_4(),
	Spin_t2228728196::get_offset_of_mTrans_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5836 = { sizeof (SpinWithMouse_t3499877925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5836[3] = 
{
	SpinWithMouse_t3499877925::get_offset_of_target_2(),
	SpinWithMouse_t3499877925::get_offset_of_speed_3(),
	SpinWithMouse_t3499877925::get_offset_of_mTrans_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5837 = { sizeof (Tutorial5_t2411054926), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5838 = { sizeof (UISliderColors_t4207082228), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5838[4] = 
{
	UISliderColors_t4207082228::get_offset_of_sprite_2(),
	UISliderColors_t4207082228::get_offset_of_colors_3(),
	UISliderColors_t4207082228::get_offset_of_mBar_4(),
	UISliderColors_t4207082228::get_offset_of_mSprite_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5839 = { sizeof (WindowAutoYaw_t800598140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5839[4] = 
{
	WindowAutoYaw_t800598140::get_offset_of_updateOrder_2(),
	WindowAutoYaw_t800598140::get_offset_of_uiCamera_3(),
	WindowAutoYaw_t800598140::get_offset_of_yawAmount_4(),
	WindowAutoYaw_t800598140::get_offset_of_mTrans_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5840 = { sizeof (WindowDragTilt_t1337067838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5840[5] = 
{
	WindowDragTilt_t1337067838::get_offset_of_updateOrder_2(),
	WindowDragTilt_t1337067838::get_offset_of_degrees_3(),
	WindowDragTilt_t1337067838::get_offset_of_mLastPos_4(),
	WindowDragTilt_t1337067838::get_offset_of_mTrans_5(),
	WindowDragTilt_t1337067838::get_offset_of_mAngle_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5841 = { sizeof (LanguageSelection_t1927862481), -1, sizeof(LanguageSelection_t1927862481_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5841[2] = 
{
	LanguageSelection_t1927862481::get_offset_of_mList_2(),
	LanguageSelection_t1927862481_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5842 = { sizeof (TypewriterEffect_t1340895546), -1, sizeof(TypewriterEffect_t1340895546_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5842[15] = 
{
	TypewriterEffect_t1340895546_StaticFields::get_offset_of_current_2(),
	TypewriterEffect_t1340895546::get_offset_of_charsPerSecond_3(),
	TypewriterEffect_t1340895546::get_offset_of_fadeInTime_4(),
	TypewriterEffect_t1340895546::get_offset_of_delayOnPeriod_5(),
	TypewriterEffect_t1340895546::get_offset_of_delayOnNewLine_6(),
	TypewriterEffect_t1340895546::get_offset_of_scrollView_7(),
	TypewriterEffect_t1340895546::get_offset_of_keepFullDimensions_8(),
	TypewriterEffect_t1340895546::get_offset_of_onFinished_9(),
	TypewriterEffect_t1340895546::get_offset_of_mLabel_10(),
	TypewriterEffect_t1340895546::get_offset_of_mFullText_11(),
	TypewriterEffect_t1340895546::get_offset_of_mCurrentOffset_12(),
	TypewriterEffect_t1340895546::get_offset_of_mNextChar_13(),
	TypewriterEffect_t1340895546::get_offset_of_mReset_14(),
	TypewriterEffect_t1340895546::get_offset_of_mActive_15(),
	TypewriterEffect_t1340895546::get_offset_of_mFade_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5843 = { sizeof (FadeEntry_t639421133)+ sizeof (Il2CppObject), sizeof(FadeEntry_t639421133_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5843[3] = 
{
	FadeEntry_t639421133::get_offset_of_index_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FadeEntry_t639421133::get_offset_of_text_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FadeEntry_t639421133::get_offset_of_alpha_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5844 = { sizeof (UIButton_t1100396938), -1, sizeof(UIButton_t1100396938_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5844[14] = 
{
	UIButton_t1100396938_StaticFields::get_offset_of_current_12(),
	UIButton_t1100396938::get_offset_of_dragHighlight_13(),
	UIButton_t1100396938::get_offset_of_hoverSprite_14(),
	UIButton_t1100396938::get_offset_of_pressedSprite_15(),
	UIButton_t1100396938::get_offset_of_disabledSprite_16(),
	UIButton_t1100396938::get_offset_of_hoverSprite2D_17(),
	UIButton_t1100396938::get_offset_of_pressedSprite2D_18(),
	UIButton_t1100396938::get_offset_of_disabledSprite2D_19(),
	UIButton_t1100396938::get_offset_of_pixelSnap_20(),
	UIButton_t1100396938::get_offset_of_onClick_21(),
	UIButton_t1100396938::get_offset_of_mSprite_22(),
	UIButton_t1100396938::get_offset_of_mSprite2D_23(),
	UIButton_t1100396938::get_offset_of_mNormalSprite_24(),
	UIButton_t1100396938::get_offset_of_mNormalSprite2D_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5845 = { sizeof (UIButtonActivate_t1184781629), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5845[2] = 
{
	UIButtonActivate_t1184781629::get_offset_of_target_2(),
	UIButtonActivate_t1184781629::get_offset_of_state_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5846 = { sizeof (UIButtonColor_t2038074180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5846[10] = 
{
	UIButtonColor_t2038074180::get_offset_of_tweenTarget_2(),
	UIButtonColor_t2038074180::get_offset_of_hover_3(),
	UIButtonColor_t2038074180::get_offset_of_pressed_4(),
	UIButtonColor_t2038074180::get_offset_of_disabledColor_5(),
	UIButtonColor_t2038074180::get_offset_of_duration_6(),
	UIButtonColor_t2038074180::get_offset_of_mStartingColor_7(),
	UIButtonColor_t2038074180::get_offset_of_mDefaultColor_8(),
	UIButtonColor_t2038074180::get_offset_of_mInitDone_9(),
	UIButtonColor_t2038074180::get_offset_of_mWidget_10(),
	UIButtonColor_t2038074180::get_offset_of_mState_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5847 = { sizeof (State_t3991372483)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5847[5] = 
{
	State_t3991372483::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5848 = { sizeof (UIButtonKeys_t486517330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5848[5] = 
{
	UIButtonKeys_t486517330::get_offset_of_selectOnClick_12(),
	UIButtonKeys_t486517330::get_offset_of_selectOnUp_13(),
	UIButtonKeys_t486517330::get_offset_of_selectOnDown_14(),
	UIButtonKeys_t486517330::get_offset_of_selectOnLeft_15(),
	UIButtonKeys_t486517330::get_offset_of_selectOnRight_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5849 = { sizeof (UIButtonMessage_t952534536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5849[5] = 
{
	UIButtonMessage_t952534536::get_offset_of_target_2(),
	UIButtonMessage_t952534536::get_offset_of_functionName_3(),
	UIButtonMessage_t952534536::get_offset_of_trigger_4(),
	UIButtonMessage_t952534536::get_offset_of_includeChildren_5(),
	UIButtonMessage_t952534536::get_offset_of_mStarted_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5850 = { sizeof (Trigger_t148524997)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5850[7] = 
{
	Trigger_t148524997::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5851 = { sizeof (UIButtonOffset_t4041087173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5851[7] = 
{
	UIButtonOffset_t4041087173::get_offset_of_tweenTarget_2(),
	UIButtonOffset_t4041087173::get_offset_of_hover_3(),
	UIButtonOffset_t4041087173::get_offset_of_pressed_4(),
	UIButtonOffset_t4041087173::get_offset_of_duration_5(),
	UIButtonOffset_t4041087173::get_offset_of_mPos_6(),
	UIButtonOffset_t4041087173::get_offset_of_mStarted_7(),
	UIButtonOffset_t4041087173::get_offset_of_mPressed_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5852 = { sizeof (UIButtonRotation_t284788413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5852[6] = 
{
	UIButtonRotation_t284788413::get_offset_of_tweenTarget_2(),
	UIButtonRotation_t284788413::get_offset_of_hover_3(),
	UIButtonRotation_t284788413::get_offset_of_pressed_4(),
	UIButtonRotation_t284788413::get_offset_of_duration_5(),
	UIButtonRotation_t284788413::get_offset_of_mRot_6(),
	UIButtonRotation_t284788413::get_offset_of_mStarted_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5853 = { sizeof (UIButtonScale_t3973287916), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5853[6] = 
{
	UIButtonScale_t3973287916::get_offset_of_tweenTarget_2(),
	UIButtonScale_t3973287916::get_offset_of_hover_3(),
	UIButtonScale_t3973287916::get_offset_of_pressed_4(),
	UIButtonScale_t3973287916::get_offset_of_duration_5(),
	UIButtonScale_t3973287916::get_offset_of_mScale_6(),
	UIButtonScale_t3973287916::get_offset_of_mStarted_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5854 = { sizeof (UICenterOnChild_t253063637), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5854[6] = 
{
	UICenterOnChild_t253063637::get_offset_of_springStrength_2(),
	UICenterOnChild_t253063637::get_offset_of_nextPageThreshold_3(),
	UICenterOnChild_t253063637::get_offset_of_onFinished_4(),
	UICenterOnChild_t253063637::get_offset_of_onCenter_5(),
	UICenterOnChild_t253063637::get_offset_of_mScrollView_6(),
	UICenterOnChild_t253063637::get_offset_of_mCenteredObject_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5855 = { sizeof (OnCenterCallback_t2077531662), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5856 = { sizeof (UICenterOnClick_t4058804012), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5857 = { sizeof (UIDragCamera_t3716731078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5857[1] = 
{
	UIDragCamera_t3716731078::get_offset_of_draggableCamera_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5858 = { sizeof (UIDragDropContainer_t2199185397), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5858[1] = 
{
	UIDragDropContainer_t2199185397::get_offset_of_reparentTarget_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5859 = { sizeof (UIDragDropItem_t3922849381), -1, sizeof(UIDragDropItem_t3922849381_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5859[18] = 
{
	UIDragDropItem_t3922849381::get_offset_of_restriction_2(),
	UIDragDropItem_t3922849381::get_offset_of_cloneOnDrag_3(),
	UIDragDropItem_t3922849381::get_offset_of_pressAndHoldDelay_4(),
	UIDragDropItem_t3922849381::get_offset_of_interactable_5(),
	UIDragDropItem_t3922849381::get_offset_of_mTrans_6(),
	UIDragDropItem_t3922849381::get_offset_of_mParent_7(),
	UIDragDropItem_t3922849381::get_offset_of_mCollider_8(),
	UIDragDropItem_t3922849381::get_offset_of_mCollider2D_9(),
	UIDragDropItem_t3922849381::get_offset_of_mButton_10(),
	UIDragDropItem_t3922849381::get_offset_of_mRoot_11(),
	UIDragDropItem_t3922849381::get_offset_of_mGrid_12(),
	UIDragDropItem_t3922849381::get_offset_of_mTable_13(),
	UIDragDropItem_t3922849381::get_offset_of_mDragStartTime_14(),
	UIDragDropItem_t3922849381::get_offset_of_mDragScrollView_15(),
	UIDragDropItem_t3922849381::get_offset_of_mPressed_16(),
	UIDragDropItem_t3922849381::get_offset_of_mDragging_17(),
	UIDragDropItem_t3922849381::get_offset_of_mTouch_18(),
	UIDragDropItem_t3922849381_StaticFields::get_offset_of_draggedItems_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5860 = { sizeof (Restriction_t4221874387)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5860[5] = 
{
	Restriction_t4221874387::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5861 = { sizeof (U3CEnableDragScrollViewU3Ec__Iterator0_t656083354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5861[4] = 
{
	U3CEnableDragScrollViewU3Ec__Iterator0_t656083354::get_offset_of_U24this_0(),
	U3CEnableDragScrollViewU3Ec__Iterator0_t656083354::get_offset_of_U24current_1(),
	U3CEnableDragScrollViewU3Ec__Iterator0_t656083354::get_offset_of_U24disposing_2(),
	U3CEnableDragScrollViewU3Ec__Iterator0_t656083354::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5862 = { sizeof (UIDragDropRoot_t3716445314), -1, sizeof(UIDragDropRoot_t3716445314_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5862[1] = 
{
	UIDragDropRoot_t3716445314_StaticFields::get_offset_of_root_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5863 = { sizeof (UIDragObject_t167167434), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5863[18] = 
{
	UIDragObject_t167167434::get_offset_of_target_2(),
	UIDragObject_t167167434::get_offset_of_panelRegion_3(),
	UIDragObject_t167167434::get_offset_of_scrollMomentum_4(),
	UIDragObject_t167167434::get_offset_of_restrictWithinPanel_5(),
	UIDragObject_t167167434::get_offset_of_contentRect_6(),
	UIDragObject_t167167434::get_offset_of_dragEffect_7(),
	UIDragObject_t167167434::get_offset_of_momentumAmount_8(),
	UIDragObject_t167167434::get_offset_of_scale_9(),
	UIDragObject_t167167434::get_offset_of_scrollWheelFactor_10(),
	UIDragObject_t167167434::get_offset_of_mPlane_11(),
	UIDragObject_t167167434::get_offset_of_mTargetPos_12(),
	UIDragObject_t167167434::get_offset_of_mLastPos_13(),
	UIDragObject_t167167434::get_offset_of_mMomentum_14(),
	UIDragObject_t167167434::get_offset_of_mScroll_15(),
	UIDragObject_t167167434::get_offset_of_mBounds_16(),
	UIDragObject_t167167434::get_offset_of_mTouchID_17(),
	UIDragObject_t167167434::get_offset_of_mStarted_18(),
	UIDragObject_t167167434::get_offset_of_mPressed_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5864 = { sizeof (DragEffect_t2686339116)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5864[4] = 
{
	DragEffect_t2686339116::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5865 = { sizeof (UIDragResize_t3151593629), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5865[13] = 
{
	UIDragResize_t3151593629::get_offset_of_target_2(),
	UIDragResize_t3151593629::get_offset_of_pivot_3(),
	UIDragResize_t3151593629::get_offset_of_minWidth_4(),
	UIDragResize_t3151593629::get_offset_of_minHeight_5(),
	UIDragResize_t3151593629::get_offset_of_maxWidth_6(),
	UIDragResize_t3151593629::get_offset_of_maxHeight_7(),
	UIDragResize_t3151593629::get_offset_of_updateAnchors_8(),
	UIDragResize_t3151593629::get_offset_of_mPlane_9(),
	UIDragResize_t3151593629::get_offset_of_mRayPos_10(),
	UIDragResize_t3151593629::get_offset_of_mLocalPos_11(),
	UIDragResize_t3151593629::get_offset_of_mWidth_12(),
	UIDragResize_t3151593629::get_offset_of_mHeight_13(),
	UIDragResize_t3151593629::get_offset_of_mDragging_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5866 = { sizeof (UIDragScrollView_t2492060641), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5866[6] = 
{
	UIDragScrollView_t2492060641::get_offset_of_scrollView_2(),
	UIDragScrollView_t2492060641::get_offset_of_draggablePanel_3(),
	UIDragScrollView_t2492060641::get_offset_of_mTrans_4(),
	UIDragScrollView_t2492060641::get_offset_of_mScroll_5(),
	UIDragScrollView_t2492060641::get_offset_of_mAutoFind_6(),
	UIDragScrollView_t2492060641::get_offset_of_mStarted_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5867 = { sizeof (UIDraggableCamera_t1644204495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5867[14] = 
{
	UIDraggableCamera_t1644204495::get_offset_of_rootForBounds_2(),
	UIDraggableCamera_t1644204495::get_offset_of_scale_3(),
	UIDraggableCamera_t1644204495::get_offset_of_scrollWheelFactor_4(),
	UIDraggableCamera_t1644204495::get_offset_of_dragEffect_5(),
	UIDraggableCamera_t1644204495::get_offset_of_smoothDragStart_6(),
	UIDraggableCamera_t1644204495::get_offset_of_momentumAmount_7(),
	UIDraggableCamera_t1644204495::get_offset_of_mCam_8(),
	UIDraggableCamera_t1644204495::get_offset_of_mTrans_9(),
	UIDraggableCamera_t1644204495::get_offset_of_mPressed_10(),
	UIDraggableCamera_t1644204495::get_offset_of_mMomentum_11(),
	UIDraggableCamera_t1644204495::get_offset_of_mBounds_12(),
	UIDraggableCamera_t1644204495::get_offset_of_mScroll_13(),
	UIDraggableCamera_t1644204495::get_offset_of_mRoot_14(),
	UIDraggableCamera_t1644204495::get_offset_of_mDragStarted_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5868 = { sizeof (UIEventTrigger_t4038454782), -1, sizeof(UIEventTrigger_t4038454782_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5868[14] = 
{
	UIEventTrigger_t4038454782_StaticFields::get_offset_of_current_2(),
	UIEventTrigger_t4038454782::get_offset_of_onHoverOver_3(),
	UIEventTrigger_t4038454782::get_offset_of_onHoverOut_4(),
	UIEventTrigger_t4038454782::get_offset_of_onPress_5(),
	UIEventTrigger_t4038454782::get_offset_of_onRelease_6(),
	UIEventTrigger_t4038454782::get_offset_of_onSelect_7(),
	UIEventTrigger_t4038454782::get_offset_of_onDeselect_8(),
	UIEventTrigger_t4038454782::get_offset_of_onClick_9(),
	UIEventTrigger_t4038454782::get_offset_of_onDoubleClick_10(),
	UIEventTrigger_t4038454782::get_offset_of_onDragStart_11(),
	UIEventTrigger_t4038454782::get_offset_of_onDragEnd_12(),
	UIEventTrigger_t4038454782::get_offset_of_onDragOver_13(),
	UIEventTrigger_t4038454782::get_offset_of_onDragOut_14(),
	UIEventTrigger_t4038454782::get_offset_of_onDrag_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5869 = { sizeof (UIForwardEvents_t2244381111), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5869[10] = 
{
	UIForwardEvents_t2244381111::get_offset_of_target_2(),
	UIForwardEvents_t2244381111::get_offset_of_onHover_3(),
	UIForwardEvents_t2244381111::get_offset_of_onPress_4(),
	UIForwardEvents_t2244381111::get_offset_of_onClick_5(),
	UIForwardEvents_t2244381111::get_offset_of_onDoubleClick_6(),
	UIForwardEvents_t2244381111::get_offset_of_onSelect_7(),
	UIForwardEvents_t2244381111::get_offset_of_onDrag_8(),
	UIForwardEvents_t2244381111::get_offset_of_onDrop_9(),
	UIForwardEvents_t2244381111::get_offset_of_onSubmit_10(),
	UIForwardEvents_t2244381111::get_offset_of_onScroll_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5870 = { sizeof (UIGrid_t1536638187), -1, sizeof(UIGrid_t1536638187_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5870[18] = 
{
	UIGrid_t1536638187::get_offset_of_arrangement_2(),
	UIGrid_t1536638187::get_offset_of_sorting_3(),
	UIGrid_t1536638187::get_offset_of_pivot_4(),
	UIGrid_t1536638187::get_offset_of_maxPerLine_5(),
	UIGrid_t1536638187::get_offset_of_cellWidth_6(),
	UIGrid_t1536638187::get_offset_of_cellHeight_7(),
	UIGrid_t1536638187::get_offset_of_animateSmoothly_8(),
	UIGrid_t1536638187::get_offset_of_hideInactive_9(),
	UIGrid_t1536638187::get_offset_of_keepWithinPanel_10(),
	UIGrid_t1536638187::get_offset_of_onReposition_11(),
	UIGrid_t1536638187::get_offset_of_onCustomSort_12(),
	UIGrid_t1536638187::get_offset_of_sorted_13(),
	UIGrid_t1536638187::get_offset_of_mReposition_14(),
	UIGrid_t1536638187::get_offset_of_mPanel_15(),
	UIGrid_t1536638187::get_offset_of_mInitDone_16(),
	UIGrid_t1536638187_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_17(),
	UIGrid_t1536638187_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_18(),
	UIGrid_t1536638187_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5871 = { sizeof (OnReposition_t1372889220), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5872 = { sizeof (Arrangement_t1850956547)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5872[4] = 
{
	Arrangement_t1850956547::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5873 = { sizeof (Sorting_t533260699)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5873[6] = 
{
	Sorting_t533260699::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5874 = { sizeof (UIImageButton_t3745027676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5874[6] = 
{
	UIImageButton_t3745027676::get_offset_of_target_2(),
	UIImageButton_t3745027676::get_offset_of_normalSprite_3(),
	UIImageButton_t3745027676::get_offset_of_hoverSprite_4(),
	UIImageButton_t3745027676::get_offset_of_pressedSprite_5(),
	UIImageButton_t3745027676::get_offset_of_disabledSprite_6(),
	UIImageButton_t3745027676::get_offset_of_pixelSnap_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5875 = { sizeof (UIKeyBinding_t2698445843), -1, sizeof(UIKeyBinding_t2698445843_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5875[7] = 
{
	UIKeyBinding_t2698445843_StaticFields::get_offset_of_mList_2(),
	UIKeyBinding_t2698445843::get_offset_of_keyCode_3(),
	UIKeyBinding_t2698445843::get_offset_of_modifier_4(),
	UIKeyBinding_t2698445843::get_offset_of_action_5(),
	UIKeyBinding_t2698445843::get_offset_of_mIgnoreUp_6(),
	UIKeyBinding_t2698445843::get_offset_of_mIsInput_7(),
	UIKeyBinding_t2698445843::get_offset_of_mPress_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5876 = { sizeof (Action_t4142137194)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5876[4] = 
{
	Action_t4142137194::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5877 = { sizeof (Modifier_t1697911081)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5877[6] = 
{
	Modifier_t1697911081::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5878 = { sizeof (UIKeyNavigation_t4244956403), -1, sizeof(UIKeyNavigation_t4244956403_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5878[10] = 
{
	UIKeyNavigation_t4244956403_StaticFields::get_offset_of_list_2(),
	UIKeyNavigation_t4244956403::get_offset_of_constraint_3(),
	UIKeyNavigation_t4244956403::get_offset_of_onUp_4(),
	UIKeyNavigation_t4244956403::get_offset_of_onDown_5(),
	UIKeyNavigation_t4244956403::get_offset_of_onLeft_6(),
	UIKeyNavigation_t4244956403::get_offset_of_onRight_7(),
	UIKeyNavigation_t4244956403::get_offset_of_onClick_8(),
	UIKeyNavigation_t4244956403::get_offset_of_onTab_9(),
	UIKeyNavigation_t4244956403::get_offset_of_startsSelected_10(),
	UIKeyNavigation_t4244956403::get_offset_of_mStarted_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5879 = { sizeof (Constraint_t2778583555)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5879[5] = 
{
	Constraint_t2778583555::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5880 = { sizeof (UIPlayAnimation_t1976799768), -1, sizeof(UIPlayAnimation_t1976799768_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5880[16] = 
{
	UIPlayAnimation_t1976799768_StaticFields::get_offset_of_current_2(),
	UIPlayAnimation_t1976799768::get_offset_of_target_3(),
	UIPlayAnimation_t1976799768::get_offset_of_animator_4(),
	UIPlayAnimation_t1976799768::get_offset_of_clipName_5(),
	UIPlayAnimation_t1976799768::get_offset_of_trigger_6(),
	UIPlayAnimation_t1976799768::get_offset_of_playDirection_7(),
	UIPlayAnimation_t1976799768::get_offset_of_resetOnPlay_8(),
	UIPlayAnimation_t1976799768::get_offset_of_clearSelection_9(),
	UIPlayAnimation_t1976799768::get_offset_of_ifDisabledOnPlay_10(),
	UIPlayAnimation_t1976799768::get_offset_of_disableWhenFinished_11(),
	UIPlayAnimation_t1976799768::get_offset_of_onFinished_12(),
	UIPlayAnimation_t1976799768::get_offset_of_eventReceiver_13(),
	UIPlayAnimation_t1976799768::get_offset_of_callWhenFinished_14(),
	UIPlayAnimation_t1976799768::get_offset_of_mStarted_15(),
	UIPlayAnimation_t1976799768::get_offset_of_mActivated_16(),
	UIPlayAnimation_t1976799768::get_offset_of_dragHighlight_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5881 = { sizeof (UIPlaySound_t1382070071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5881[5] = 
{
	UIPlaySound_t1382070071::get_offset_of_audioClip_2(),
	UIPlaySound_t1382070071::get_offset_of_trigger_3(),
	UIPlaySound_t1382070071::get_offset_of_volume_4(),
	UIPlaySound_t1382070071::get_offset_of_pitch_5(),
	UIPlaySound_t1382070071::get_offset_of_mIsOver_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5882 = { sizeof (Trigger_t2172727401)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5882[9] = 
{
	Trigger_t2172727401::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5883 = { sizeof (UIPlayTween_t2213781924), -1, sizeof(UIPlayTween_t2213781924_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5883[17] = 
{
	UIPlayTween_t2213781924_StaticFields::get_offset_of_current_2(),
	UIPlayTween_t2213781924::get_offset_of_tweenTarget_3(),
	UIPlayTween_t2213781924::get_offset_of_tweenGroup_4(),
	UIPlayTween_t2213781924::get_offset_of_trigger_5(),
	UIPlayTween_t2213781924::get_offset_of_playDirection_6(),
	UIPlayTween_t2213781924::get_offset_of_resetOnPlay_7(),
	UIPlayTween_t2213781924::get_offset_of_resetIfDisabled_8(),
	UIPlayTween_t2213781924::get_offset_of_ifDisabledOnPlay_9(),
	UIPlayTween_t2213781924::get_offset_of_disableWhenFinished_10(),
	UIPlayTween_t2213781924::get_offset_of_includeChildren_11(),
	UIPlayTween_t2213781924::get_offset_of_onFinished_12(),
	UIPlayTween_t2213781924::get_offset_of_eventReceiver_13(),
	UIPlayTween_t2213781924::get_offset_of_callWhenFinished_14(),
	UIPlayTween_t2213781924::get_offset_of_mTweens_15(),
	UIPlayTween_t2213781924::get_offset_of_mStarted_16(),
	UIPlayTween_t2213781924::get_offset_of_mActive_17(),
	UIPlayTween_t2213781924::get_offset_of_mActivated_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5884 = { sizeof (UIPopupList_t4167399471), -1, sizeof(UIPopupList_t4167399471_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5884[42] = 
{
	UIPopupList_t4167399471_StaticFields::get_offset_of_current_2(),
	UIPopupList_t4167399471_StaticFields::get_offset_of_mChild_3(),
	UIPopupList_t4167399471_StaticFields::get_offset_of_mFadeOutComplete_4(),
	0,
	UIPopupList_t4167399471::get_offset_of_atlas_6(),
	UIPopupList_t4167399471::get_offset_of_bitmapFont_7(),
	UIPopupList_t4167399471::get_offset_of_trueTypeFont_8(),
	UIPopupList_t4167399471::get_offset_of_fontSize_9(),
	UIPopupList_t4167399471::get_offset_of_fontStyle_10(),
	UIPopupList_t4167399471::get_offset_of_backgroundSprite_11(),
	UIPopupList_t4167399471::get_offset_of_highlightSprite_12(),
	UIPopupList_t4167399471::get_offset_of_position_13(),
	UIPopupList_t4167399471::get_offset_of_alignment_14(),
	UIPopupList_t4167399471::get_offset_of_items_15(),
	UIPopupList_t4167399471::get_offset_of_itemData_16(),
	UIPopupList_t4167399471::get_offset_of_padding_17(),
	UIPopupList_t4167399471::get_offset_of_textColor_18(),
	UIPopupList_t4167399471::get_offset_of_backgroundColor_19(),
	UIPopupList_t4167399471::get_offset_of_highlightColor_20(),
	UIPopupList_t4167399471::get_offset_of_isAnimated_21(),
	UIPopupList_t4167399471::get_offset_of_isLocalized_22(),
	UIPopupList_t4167399471::get_offset_of_openOn_23(),
	UIPopupList_t4167399471::get_offset_of_onChange_24(),
	UIPopupList_t4167399471::get_offset_of_mSelectedItem_25(),
	UIPopupList_t4167399471::get_offset_of_mPanel_26(),
	UIPopupList_t4167399471::get_offset_of_mBackground_27(),
	UIPopupList_t4167399471::get_offset_of_mHighlight_28(),
	UIPopupList_t4167399471::get_offset_of_mHighlightedLabel_29(),
	UIPopupList_t4167399471::get_offset_of_mLabelList_30(),
	UIPopupList_t4167399471::get_offset_of_mBgBorder_31(),
	UIPopupList_t4167399471::get_offset_of_mSelection_32(),
	UIPopupList_t4167399471::get_offset_of_mOpenFrame_33(),
	UIPopupList_t4167399471::get_offset_of_eventReceiver_34(),
	UIPopupList_t4167399471::get_offset_of_functionName_35(),
	UIPopupList_t4167399471::get_offset_of_textScale_36(),
	UIPopupList_t4167399471::get_offset_of_font_37(),
	UIPopupList_t4167399471::get_offset_of_textLabel_38(),
	UIPopupList_t4167399471::get_offset_of_mLegacyEvent_39(),
	UIPopupList_t4167399471::get_offset_of_mExecuting_40(),
	UIPopupList_t4167399471::get_offset_of_mUseDynamicFont_41(),
	UIPopupList_t4167399471::get_offset_of_mTweening_42(),
	UIPopupList_t4167399471::get_offset_of_source_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5885 = { sizeof (Position_t1583461796)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5885[4] = 
{
	Position_t1583461796::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5886 = { sizeof (OpenOn_t1997085761)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5886[5] = 
{
	OpenOn_t1997085761::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5887 = { sizeof (LegacyEvent_t2749056879), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5888 = { sizeof (U3CUpdateTweenPositionU3Ec__Iterator0_t2265653740), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5888[5] = 
{
	U3CUpdateTweenPositionU3Ec__Iterator0_t2265653740::get_offset_of_U3CtpU3E__1_0(),
	U3CUpdateTweenPositionU3Ec__Iterator0_t2265653740::get_offset_of_U24this_1(),
	U3CUpdateTweenPositionU3Ec__Iterator0_t2265653740::get_offset_of_U24current_2(),
	U3CUpdateTweenPositionU3Ec__Iterator0_t2265653740::get_offset_of_U24disposing_3(),
	U3CUpdateTweenPositionU3Ec__Iterator0_t2265653740::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5889 = { sizeof (U3CCloseIfUnselectedU3Ec__Iterator1_t2833751522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5889[4] = 
{
	U3CCloseIfUnselectedU3Ec__Iterator1_t2833751522::get_offset_of_U24this_0(),
	U3CCloseIfUnselectedU3Ec__Iterator1_t2833751522::get_offset_of_U24current_1(),
	U3CCloseIfUnselectedU3Ec__Iterator1_t2833751522::get_offset_of_U24disposing_2(),
	U3CCloseIfUnselectedU3Ec__Iterator1_t2833751522::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5890 = { sizeof (UIProgressBar_t1222110469), -1, sizeof(UIProgressBar_t1222110469_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5890[13] = 
{
	UIProgressBar_t1222110469_StaticFields::get_offset_of_current_2(),
	UIProgressBar_t1222110469::get_offset_of_onDragFinished_3(),
	UIProgressBar_t1222110469::get_offset_of_thumb_4(),
	UIProgressBar_t1222110469::get_offset_of_mBG_5(),
	UIProgressBar_t1222110469::get_offset_of_mFG_6(),
	UIProgressBar_t1222110469::get_offset_of_mValue_7(),
	UIProgressBar_t1222110469::get_offset_of_mFill_8(),
	UIProgressBar_t1222110469::get_offset_of_mTrans_9(),
	UIProgressBar_t1222110469::get_offset_of_mIsDirty_10(),
	UIProgressBar_t1222110469::get_offset_of_mCam_11(),
	UIProgressBar_t1222110469::get_offset_of_mOffset_12(),
	UIProgressBar_t1222110469::get_offset_of_numberOfSteps_13(),
	UIProgressBar_t1222110469::get_offset_of_onChange_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5891 = { sizeof (FillDirection_t551265397)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5891[5] = 
{
	FillDirection_t551265397::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5892 = { sizeof (OnDragFinished_t3715779777), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5893 = { sizeof (UISavedOption_t2504198202), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5893[4] = 
{
	UISavedOption_t2504198202::get_offset_of_keyName_2(),
	UISavedOption_t2504198202::get_offset_of_mList_3(),
	UISavedOption_t2504198202::get_offset_of_mCheck_4(),
	UISavedOption_t2504198202::get_offset_of_mSlider_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5894 = { sizeof (UIScrollBar_t1285772729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5894[3] = 
{
	UIScrollBar_t1285772729::get_offset_of_mSize_19(),
	UIScrollBar_t1285772729::get_offset_of_mScroll_20(),
	UIScrollBar_t1285772729::get_offset_of_mDir_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5895 = { sizeof (Direction_t340832489)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5895[4] = 
{
	Direction_t340832489::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5896 = { sizeof (UIScrollView_t1973404950), -1, sizeof(UIScrollView_t1973404950_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5896[37] = 
{
	UIScrollView_t1973404950_StaticFields::get_offset_of_list_2(),
	UIScrollView_t1973404950::get_offset_of_movement_3(),
	UIScrollView_t1973404950::get_offset_of_dragEffect_4(),
	UIScrollView_t1973404950::get_offset_of_restrictWithinPanel_5(),
	UIScrollView_t1973404950::get_offset_of_disableDragIfFits_6(),
	UIScrollView_t1973404950::get_offset_of_smoothDragStart_7(),
	UIScrollView_t1973404950::get_offset_of_iOSDragEmulation_8(),
	UIScrollView_t1973404950::get_offset_of_scrollWheelFactor_9(),
	UIScrollView_t1973404950::get_offset_of_momentumAmount_10(),
	UIScrollView_t1973404950::get_offset_of_dampenStrength_11(),
	UIScrollView_t1973404950::get_offset_of_horizontalScrollBar_12(),
	UIScrollView_t1973404950::get_offset_of_verticalScrollBar_13(),
	UIScrollView_t1973404950::get_offset_of_showScrollBars_14(),
	UIScrollView_t1973404950::get_offset_of_customMovement_15(),
	UIScrollView_t1973404950::get_offset_of_contentPivot_16(),
	UIScrollView_t1973404950::get_offset_of_onDragStarted_17(),
	UIScrollView_t1973404950::get_offset_of_onDragFinished_18(),
	UIScrollView_t1973404950::get_offset_of_onMomentumMove_19(),
	UIScrollView_t1973404950::get_offset_of_onStoppedMoving_20(),
	UIScrollView_t1973404950::get_offset_of_scale_21(),
	UIScrollView_t1973404950::get_offset_of_relativePositionOnReset_22(),
	UIScrollView_t1973404950::get_offset_of_mTrans_23(),
	UIScrollView_t1973404950::get_offset_of_mPanel_24(),
	UIScrollView_t1973404950::get_offset_of_mPlane_25(),
	UIScrollView_t1973404950::get_offset_of_mLastPos_26(),
	UIScrollView_t1973404950::get_offset_of_mPressed_27(),
	UIScrollView_t1973404950::get_offset_of_mMomentum_28(),
	UIScrollView_t1973404950::get_offset_of_mScroll_29(),
	UIScrollView_t1973404950::get_offset_of_mBounds_30(),
	UIScrollView_t1973404950::get_offset_of_mCalculatedBounds_31(),
	UIScrollView_t1973404950::get_offset_of_mShouldMove_32(),
	UIScrollView_t1973404950::get_offset_of_mIgnoreCallbacks_33(),
	UIScrollView_t1973404950::get_offset_of_mDragID_34(),
	UIScrollView_t1973404950::get_offset_of_mDragStartOffset_35(),
	UIScrollView_t1973404950::get_offset_of_mDragStarted_36(),
	UIScrollView_t1973404950::get_offset_of_mStarted_37(),
	UIScrollView_t1973404950::get_offset_of_centerOnChild_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5897 = { sizeof (Movement_t247277292)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5897[5] = 
{
	Movement_t247277292::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5898 = { sizeof (DragEffect_t491601944)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5898[4] = 
{
	DragEffect_t491601944::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5899 = { sizeof (ShowCondition_t1535012424)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5899[4] = 
{
	ShowCondition_t1535012424::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
