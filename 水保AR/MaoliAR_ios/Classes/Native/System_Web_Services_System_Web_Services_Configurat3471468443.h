﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Configuration.XmlFormatExtensionPointAttribute
struct  XmlFormatExtensionPointAttribute_t3471468443  : public Attribute_t861562559
{
public:
	// System.Boolean System.Web.Services.Configuration.XmlFormatExtensionPointAttribute::allowElements
	bool ___allowElements_0;
	// System.String System.Web.Services.Configuration.XmlFormatExtensionPointAttribute::memberName
	String_t* ___memberName_1;

public:
	inline static int32_t get_offset_of_allowElements_0() { return static_cast<int32_t>(offsetof(XmlFormatExtensionPointAttribute_t3471468443, ___allowElements_0)); }
	inline bool get_allowElements_0() const { return ___allowElements_0; }
	inline bool* get_address_of_allowElements_0() { return &___allowElements_0; }
	inline void set_allowElements_0(bool value)
	{
		___allowElements_0 = value;
	}

	inline static int32_t get_offset_of_memberName_1() { return static_cast<int32_t>(offsetof(XmlFormatExtensionPointAttribute_t3471468443, ___memberName_1)); }
	inline String_t* get_memberName_1() const { return ___memberName_1; }
	inline String_t** get_address_of_memberName_1() { return &___memberName_1; }
	inline void set_memberName_1(String_t* value)
	{
		___memberName_1 = value;
		Il2CppCodeGenWriteBarrier(&___memberName_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
