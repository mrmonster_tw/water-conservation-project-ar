﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_Mono_Security_Protocol_Ntlm_Ntl822829952.h"
#include "System_ServiceModel_Mono_Security_Protocol_Ntlm_Ntl663077424.h"

// System.Byte[]
struct ByteU5BU5D_t4116647657;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Ntlm.MessageBase
struct  MessageBase_t422981883  : public Il2CppObject
{
public:
	// System.Int32 Mono.Security.Protocol.Ntlm.MessageBase::_type
	int32_t ____type_2;
	// Mono.Security.Protocol.Ntlm.NtlmFlags Mono.Security.Protocol.Ntlm.MessageBase::_flags
	int32_t ____flags_3;
	// Mono.Security.Protocol.Ntlm.NtlmVersion Mono.Security.Protocol.Ntlm.MessageBase::_version
	int32_t ____version_4;
	// System.Byte[] Mono.Security.Protocol.Ntlm.MessageBase::_osversion
	ByteU5BU5D_t4116647657* ____osversion_5;

public:
	inline static int32_t get_offset_of__type_2() { return static_cast<int32_t>(offsetof(MessageBase_t422981883, ____type_2)); }
	inline int32_t get__type_2() const { return ____type_2; }
	inline int32_t* get_address_of__type_2() { return &____type_2; }
	inline void set__type_2(int32_t value)
	{
		____type_2 = value;
	}

	inline static int32_t get_offset_of__flags_3() { return static_cast<int32_t>(offsetof(MessageBase_t422981883, ____flags_3)); }
	inline int32_t get__flags_3() const { return ____flags_3; }
	inline int32_t* get_address_of__flags_3() { return &____flags_3; }
	inline void set__flags_3(int32_t value)
	{
		____flags_3 = value;
	}

	inline static int32_t get_offset_of__version_4() { return static_cast<int32_t>(offsetof(MessageBase_t422981883, ____version_4)); }
	inline int32_t get__version_4() const { return ____version_4; }
	inline int32_t* get_address_of__version_4() { return &____version_4; }
	inline void set__version_4(int32_t value)
	{
		____version_4 = value;
	}

	inline static int32_t get_offset_of__osversion_5() { return static_cast<int32_t>(offsetof(MessageBase_t422981883, ____osversion_5)); }
	inline ByteU5BU5D_t4116647657* get__osversion_5() const { return ____osversion_5; }
	inline ByteU5BU5D_t4116647657** get_address_of__osversion_5() { return &____osversion_5; }
	inline void set__osversion_5(ByteU5BU5D_t4116647657* value)
	{
		____osversion_5 = value;
		Il2CppCodeGenWriteBarrier(&____osversion_5, value);
	}
};

struct MessageBase_t422981883_StaticFields
{
public:
	// System.Byte[] Mono.Security.Protocol.Ntlm.MessageBase::_current_os_version
	ByteU5BU5D_t4116647657* ____current_os_version_0;
	// System.Byte[] Mono.Security.Protocol.Ntlm.MessageBase::header
	ByteU5BU5D_t4116647657* ___header_1;

public:
	inline static int32_t get_offset_of__current_os_version_0() { return static_cast<int32_t>(offsetof(MessageBase_t422981883_StaticFields, ____current_os_version_0)); }
	inline ByteU5BU5D_t4116647657* get__current_os_version_0() const { return ____current_os_version_0; }
	inline ByteU5BU5D_t4116647657** get_address_of__current_os_version_0() { return &____current_os_version_0; }
	inline void set__current_os_version_0(ByteU5BU5D_t4116647657* value)
	{
		____current_os_version_0 = value;
		Il2CppCodeGenWriteBarrier(&____current_os_version_0, value);
	}

	inline static int32_t get_offset_of_header_1() { return static_cast<int32_t>(offsetof(MessageBase_t422981883_StaticFields, ___header_1)); }
	inline ByteU5BU5D_t4116647657* get_header_1() const { return ___header_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_header_1() { return &___header_1; }
	inline void set_header_1(ByteU5BU5D_t4116647657* value)
	{
		___header_1 = value;
		Il2CppCodeGenWriteBarrier(&___header_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
