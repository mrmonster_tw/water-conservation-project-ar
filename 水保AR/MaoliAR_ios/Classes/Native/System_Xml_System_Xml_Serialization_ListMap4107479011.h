﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Serialization_ObjectMap4068517277.h"

// System.Xml.Serialization.XmlTypeMapElementInfoList
struct XmlTypeMapElementInfoList_t2115305165;
// System.Xml.Serialization.XmlTypeMapping
struct XmlTypeMapping_t3915382669;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.ListMap
struct  ListMap_t4107479011  : public ObjectMap_t4068517277
{
public:
	// System.Xml.Serialization.XmlTypeMapElementInfoList System.Xml.Serialization.ListMap::_itemInfo
	XmlTypeMapElementInfoList_t2115305165 * ____itemInfo_0;
	// System.Boolean System.Xml.Serialization.ListMap::_gotNestedMapping
	bool ____gotNestedMapping_1;
	// System.Xml.Serialization.XmlTypeMapping System.Xml.Serialization.ListMap::_nestedArrayMapping
	XmlTypeMapping_t3915382669 * ____nestedArrayMapping_2;
	// System.String System.Xml.Serialization.ListMap::_choiceMember
	String_t* ____choiceMember_3;

public:
	inline static int32_t get_offset_of__itemInfo_0() { return static_cast<int32_t>(offsetof(ListMap_t4107479011, ____itemInfo_0)); }
	inline XmlTypeMapElementInfoList_t2115305165 * get__itemInfo_0() const { return ____itemInfo_0; }
	inline XmlTypeMapElementInfoList_t2115305165 ** get_address_of__itemInfo_0() { return &____itemInfo_0; }
	inline void set__itemInfo_0(XmlTypeMapElementInfoList_t2115305165 * value)
	{
		____itemInfo_0 = value;
		Il2CppCodeGenWriteBarrier(&____itemInfo_0, value);
	}

	inline static int32_t get_offset_of__gotNestedMapping_1() { return static_cast<int32_t>(offsetof(ListMap_t4107479011, ____gotNestedMapping_1)); }
	inline bool get__gotNestedMapping_1() const { return ____gotNestedMapping_1; }
	inline bool* get_address_of__gotNestedMapping_1() { return &____gotNestedMapping_1; }
	inline void set__gotNestedMapping_1(bool value)
	{
		____gotNestedMapping_1 = value;
	}

	inline static int32_t get_offset_of__nestedArrayMapping_2() { return static_cast<int32_t>(offsetof(ListMap_t4107479011, ____nestedArrayMapping_2)); }
	inline XmlTypeMapping_t3915382669 * get__nestedArrayMapping_2() const { return ____nestedArrayMapping_2; }
	inline XmlTypeMapping_t3915382669 ** get_address_of__nestedArrayMapping_2() { return &____nestedArrayMapping_2; }
	inline void set__nestedArrayMapping_2(XmlTypeMapping_t3915382669 * value)
	{
		____nestedArrayMapping_2 = value;
		Il2CppCodeGenWriteBarrier(&____nestedArrayMapping_2, value);
	}

	inline static int32_t get_offset_of__choiceMember_3() { return static_cast<int32_t>(offsetof(ListMap_t4107479011, ____choiceMember_3)); }
	inline String_t* get__choiceMember_3() const { return ____choiceMember_3; }
	inline String_t** get_address_of__choiceMember_3() { return &____choiceMember_3; }
	inline void set__choiceMember_3(String_t* value)
	{
		____choiceMember_3 = value;
		Il2CppCodeGenWriteBarrier(&____choiceMember_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
