﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_ComponentModel_TypeConverter2249118273.h"

// System.Drawing.FontFamily[]
struct FontFamilyU5BU5D_t2560626814;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Drawing.FontConverter/FontNameConverter
struct  FontNameConverter_t3052059787  : public TypeConverter_t2249118273
{
public:
	// System.Drawing.FontFamily[] System.Drawing.FontConverter/FontNameConverter::fonts
	FontFamilyU5BU5D_t2560626814* ___fonts_0;

public:
	inline static int32_t get_offset_of_fonts_0() { return static_cast<int32_t>(offsetof(FontNameConverter_t3052059787, ___fonts_0)); }
	inline FontFamilyU5BU5D_t2560626814* get_fonts_0() const { return ___fonts_0; }
	inline FontFamilyU5BU5D_t2560626814** get_address_of_fonts_0() { return &___fonts_0; }
	inline void set_fonts_0(FontFamilyU5BU5D_t2560626814* value)
	{
		___fonts_0 = value;
		Il2CppCodeGenWriteBarrier(&___fonts_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
