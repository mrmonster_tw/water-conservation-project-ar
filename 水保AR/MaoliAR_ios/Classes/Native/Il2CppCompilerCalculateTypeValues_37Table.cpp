﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_IdentityModel_System_IdentityModel_Policy_E4159608136.h"
#include "System_IdentityModel_System_IdentityModel_Policy_De426032594.h"
#include "System_IdentityModel_System_IdentityModel_Selector1007898913.h"
#include "System_IdentityModel_System_IdentityModel_Selector2987988020.h"
#include "System_IdentityModel_System_IdentityModel_Selector3925062288.h"
#include "System_IdentityModel_System_IdentityModel_Selectors627200303.h"
#include "System_IdentityModel_System_IdentityModel_Selector3338573298.h"
#include "System_IdentityModel_System_IdentityModel_Selector3401768365.h"
#include "System_IdentityModel_System_IdentityModel_Selector1397312864.h"
#include "System_IdentityModel_System_IdentityModel_Selectors202133775.h"
#include "System_IdentityModel_System_IdentityModel_Selector2079378378.h"
#include "System_IdentityModel_System_IdentityModel_Selector1975057878.h"
#include "System_IdentityModel_System_IdentityModel_Selector3905425829.h"
#include "System_IdentityModel_System_IdentityModel_Selectors965239324.h"
#include "System_IdentityModel_System_IdentityModel_Selectors972969391.h"
#include "System_IdentityModel_System_IdentityModel_Selector1981162148.h"
#include "System_IdentityModel_System_IdentityModel_Selectors284109284.h"
#include "System_IdentityModel_System_IdentityModel_Selector4240724900.h"
#include "System_IdentityModel_System_IdentityModel_Selectors101608826.h"
#include "System_IdentityModel_System_IdentityModel_Selector2535273929.h"
#include "System_IdentityModel_System_IdentityModel_Selector1453713750.h"
#include "System_IdentityModel_System_IdentityModel_Selectors122184792.h"
#include "System_IdentityModel_System_IdentityModel_Selector1308009660.h"
#include "System_IdentityModel_System_IdentityModel_Selector1375267704.h"
#include "System_IdentityModel_System_IdentityModel_Selectors_77653204.h"
#include "System_IdentityModel_System_IdentityModel_Selector3718446341.h"
#include "System_IdentityModel_System_IdentityModel_Selector4121152558.h"
#include "System_IdentityModel_System_IdentityModel_Selector1590037481.h"
#include "System_IdentityModel_System_IdentityModel_Selector2527431997.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_A3970863348.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_B3487952943.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_E2037270612.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_Ge395812252.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_I3171133310.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_K1189367464.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_L3641697518.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_R2003951325.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_S4056824942.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_S4042822033.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_S1750670118.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_S1478133644.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_Sa699963112.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_S2804555615.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_S1930669850.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_Sa985931480.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_S4128675802.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_Se806165896.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_S1943429813.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_Se114151047.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_S3512984903.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_S1271873540.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_S1526649921.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_S3586841219.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_S1678653605.h"
#include "System_IdentityModel_Mono_Security_Cryptography_HM3689525210.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_U2268299345.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_W1335964444.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_X5306644821.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_X3285382384.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_X5329336726.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_X1311826287.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_X5676849360.h"
#include "System_IdentityModel_System_IdentityModel_Tokens_X1082272133.h"
#include "System_Windows_Forms_U3CModuleU3E692745525.h"
#include "UnityEngine_U3CModuleU3E692745525.h"
#include "UnityEngine_UnityEngine_AndroidJavaObject4131667876.h"
#include "UnityEngine_UnityEngine_AndroidJavaClass32045322.h"
#include "UnityEngine_UnityEngine_jvalue1372148875.h"
#include "UnityEngine_UnityEngine_AndroidJNIHelper1961468794.h"
#include "UnityEngine_UnityEngine_AndroidJNI738955263.h"
#include "UnityEngine_UnityEngine_NetworkReachability3450623372.h"
#include "UnityEngine_UnityEngine_Application1852185770.h"
#include "UnityEngine_UnityEngine_Application_LowMemoryCallb4104246196.h"
#include "UnityEngine_UnityEngine_Application_LogCallback3588208630.h"
#include "UnityEngine_UnityEngine_UserAuthorization2950000085.h"
#include "UnityEngine_UnityEngine_AssetBundleCreateRequest3119663542.h"
#include "UnityEngine_UnityEngine_AssetBundleRequest699759206.h"
#include "UnityEngine_UnityEngine_AssetBundle1153907252.h"
#include "UnityEngine_UnityEngine_AsyncOperation1445031843.h"
#include "UnityEngine_UnityEngine_SystemInfo3561985952.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1699091251.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdate1634918743.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame1314943911.h"
#include "UnityEngine_UnityEngine_CustomYieldInstruction1895667560.h"
#include "UnityEngine_UnityEngine_Coroutine3829159415.h"
#include "UnityEngine_UnityEngine_ScriptableObject2528358522.h"
#include "UnityEngine_UnityEngine_Behaviour1437897464.h"
#include "UnityEngine_UnityEngine_Camera4157153871.h"
#include "UnityEngine_UnityEngine_Camera_CameraCallback190067161.h"
#include "UnityEngine_UnityEngine_Component1923634451.h"
#include "UnityEngine_UnityEngine_ComputeBuffer1033194329.h"
#include "UnityEngine_UnityEngine_UnhandledExceptionHandler1162846485.h"
#include "UnityEngine_UnityEngine_CullingGroupEvent1722745023.h"
#include "UnityEngine_UnityEngine_CullingGroup2096318768.h"
#include "UnityEngine_UnityEngine_CullingGroup_StateChanged2136737110.h"
#include "UnityEngine_UnityEngine_CursorLockMode2840764040.h"
#include "UnityEngine_UnityEngine_Cursor1422555833.h"
#include "UnityEngine_UnityEngine_DebugLogHandler826086171.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3700 = { sizeof (EvaluationContext_t4159608136), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3701 = { sizeof (DefaultEvaluationContext_t426032594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3701[6] = 
{
	DefaultEvaluationContext_t426032594::get_offset_of_expiration_time_0(),
	DefaultEvaluationContext_t426032594::get_offset_of_generation_1(),
	DefaultEvaluationContext_t426032594::get_offset_of_claim_sets_2(),
	DefaultEvaluationContext_t426032594::get_offset_of_exposed_claim_sets_3(),
	DefaultEvaluationContext_t426032594::get_offset_of_properties_4(),
	DefaultEvaluationContext_t426032594::get_offset_of_claim_set_map_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3702 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3703 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3704 = { sizeof (CustomUserNameSecurityTokenAuthenticator_t1007898913), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3704[1] = 
{
	CustomUserNameSecurityTokenAuthenticator_t1007898913::get_offset_of_validator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3705 = { sizeof (SystemIdentityAuthorizationPolicy_t2987988020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3705[1] = 
{
	SystemIdentityAuthorizationPolicy_t2987988020::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3706 = { sizeof (AuthorizedCustomUserPolicy_t3925062288), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3706[1] = 
{
	AuthorizedCustomUserPolicy_t3925062288::get_offset_of_user_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3707 = { sizeof (KerberosSecurityTokenProvider_t627200303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3707[3] = 
{
	KerberosSecurityTokenProvider_t627200303::get_offset_of_name_0(),
	KerberosSecurityTokenProvider_t627200303::get_offset_of_impersonation_level_1(),
	KerberosSecurityTokenProvider_t627200303::get_offset_of_credential_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3708 = { sizeof (RsaSecurityTokenAuthenticator_t3338573298), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3709 = { sizeof (RsaAuthorizationPolicy_t3401768365), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3709[2] = 
{
	RsaAuthorizationPolicy_t3401768365::get_offset_of_id_0(),
	RsaAuthorizationPolicy_t3401768365::get_offset_of_rsa_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3710 = { sizeof (SecurityTokenAuthenticator_t1397312864), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3711 = { sizeof (SecurityTokenManager_t202133775), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3712 = { sizeof (SecurityTokenProvider_t2079378378), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3713 = { sizeof (SecurityTokenRequirement_t1975057878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3713[1] = 
{
	SecurityTokenRequirement_t1975057878::get_offset_of_properties_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3714 = { sizeof (SecurityTokenResolver_t3905425829), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3715 = { sizeof (DefaultSecurityTokenResolver_t965239324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3715[2] = 
{
	DefaultSecurityTokenResolver_t965239324::get_offset_of_tokens_0(),
	DefaultSecurityTokenResolver_t965239324::get_offset_of_match_local_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3716 = { sizeof (SecurityTokenSerializer_t972969391), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3717 = { sizeof (SecurityTokenVersion_t1981162148), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3718 = { sizeof (SystemIdentityAuthorizationPolicy_t284109284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3718[1] = 
{
	SystemIdentityAuthorizationPolicy_t284109284::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3719 = { sizeof (UserNamePasswordValidator_t4240724900), -1, sizeof(UserNamePasswordValidator_t4240724900_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3719[1] = 
{
	UserNamePasswordValidator_t4240724900_StaticFields::get_offset_of_none_validator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3720 = { sizeof (NoneValidator_t101608826), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3721 = { sizeof (MembershipUserNameValidator_t2535273929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3721[1] = 
{
	MembershipUserNameValidator_t2535273929::get_offset_of_provider_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3722 = { sizeof (UserNameSecurityTokenAuthenticator_t1453713750), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3723 = { sizeof (UserNameSecurityTokenProvider_t122184792), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3723[2] = 
{
	UserNameSecurityTokenProvider_t122184792::get_offset_of_user_0(),
	UserNameSecurityTokenProvider_t122184792::get_offset_of_pass_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3724 = { sizeof (WindowsUserNameSecurityTokenAuthenticator_t1308009660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3724[1] = 
{
	WindowsUserNameSecurityTokenAuthenticator_t1308009660::get_offset_of_include_win_groups_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3725 = { sizeof (X509CertificateValidator_t1375267704), -1, sizeof(X509CertificateValidator_t1375267704_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3725[4] = 
{
	X509CertificateValidator_t1375267704_StaticFields::get_offset_of_none_0(),
	X509CertificateValidator_t1375267704_StaticFields::get_offset_of_chain_1(),
	X509CertificateValidator_t1375267704_StaticFields::get_offset_of_peer_or_chain_2(),
	X509CertificateValidator_t1375267704_StaticFields::get_offset_of_peer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3726 = { sizeof (X509NoValidator_t77653204), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3727 = { sizeof (X509CertificateValidatorImpl_t3718446341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3727[5] = 
{
	X509CertificateValidatorImpl_t3718446341::get_offset_of_check_peer_4(),
	X509CertificateValidatorImpl_t3718446341::get_offset_of_check_chain_5(),
	X509CertificateValidatorImpl_t3718446341::get_offset_of_use_machine_ctx_6(),
	X509CertificateValidatorImpl_t3718446341::get_offset_of_policy_7(),
	X509CertificateValidatorImpl_t3718446341::get_offset_of_chain_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3728 = { sizeof (X509SecurityTokenAuthenticator_t4121152558), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3728[3] = 
{
	X509SecurityTokenAuthenticator_t4121152558::get_offset_of_map_to_windows_0(),
	X509SecurityTokenAuthenticator_t4121152558::get_offset_of_include_win_groups_1(),
	X509SecurityTokenAuthenticator_t4121152558::get_offset_of_validator_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3729 = { sizeof (X509AuthorizationPolicy_t1590037481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3729[1] = 
{
	X509AuthorizationPolicy_t1590037481::get_offset_of_cert_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3730 = { sizeof (X509SecurityTokenProvider_t2527431997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3730[2] = 
{
	X509SecurityTokenProvider_t2527431997::get_offset_of_cert_0(),
	X509SecurityTokenProvider_t2527431997::get_offset_of_store_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3731 = { sizeof (AsymmetricSecurityKey_t3970863348), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3732 = { sizeof (BinaryKeyIdentifierClause_t3487952943), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3732[1] = 
{
	BinaryKeyIdentifierClause_t3487952943::get_offset_of_data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3733 = { sizeof (EncryptedKeyIdentifierClause_t2037270612), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3733[3] = 
{
	EncryptedKeyIdentifierClause_t2037270612::get_offset_of_carried_key_name_4(),
	EncryptedKeyIdentifierClause_t2037270612::get_offset_of_enc_method_5(),
	EncryptedKeyIdentifierClause_t2037270612::get_offset_of_identifier_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3734 = { sizeof (GenericXmlSecurityToken_t395812252), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3734[2] = 
{
	GenericXmlSecurityToken_t395812252::get_offset_of_xml_0(),
	GenericXmlSecurityToken_t395812252::get_offset_of_proof_token_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3735 = { sizeof (InMemorySymmetricSecurityKey_t3171133310), -1, sizeof(InMemorySymmetricSecurityKey_t3171133310_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3735[3] = 
{
	InMemorySymmetricSecurityKey_t3171133310::get_offset_of_key_0(),
	InMemorySymmetricSecurityKey_t3171133310_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_1(),
	InMemorySymmetricSecurityKey_t3171133310_StaticFields::get_offset_of_U3CU3Ef__switchU24map2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3736 = { sizeof (KerberosRequestorSecurityToken_t1189367464), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3736[1] = 
{
	KerberosRequestorSecurityToken_t1189367464::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3737 = { sizeof (LocalIdKeyIdentifierClause_t3641697518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3737[2] = 
{
	LocalIdKeyIdentifierClause_t3641697518::get_offset_of_local_id_3(),
	LocalIdKeyIdentifierClause_t3641697518::get_offset_of_owner_type_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3738 = { sizeof (RsaSecurityToken_t2003951325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3738[2] = 
{
	RsaSecurityToken_t2003951325::get_offset_of_rsa_0(),
	RsaSecurityToken_t2003951325::get_offset_of_id_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3739 = { sizeof (SamlAccessDecision_t4056824942)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3739[4] = 
{
	SamlAccessDecision_t4056824942::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3740 = { sizeof (SamlAssertionKeyIdentifierClause_t4042822033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3740[1] = 
{
	SamlAssertionKeyIdentifierClause_t4042822033::get_offset_of_id_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3741 = { sizeof (SamlAuthenticationClaimResource_t1750670118), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3742 = { sizeof (SamlAuthorityBinding_t1478133644), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3743 = { sizeof (SamlAuthorizationDecisionClaimResource_t699963112), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3744 = { sizeof (SamlNameIdentifierClaimResource_t2804555615), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3745 = { sizeof (SamlSecurityToken_t1930669850), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3746 = { sizeof (SamlSerializer_t985931480), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3747 = { sizeof (SecurityKey_t4128675802), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3748 = { sizeof (SecurityKeyIdentifier_t806165896), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3748[2] = 
{
	SecurityKeyIdentifier_t806165896::get_offset_of_list_0(),
	SecurityKeyIdentifier_t806165896::get_offset_of_is_readonly_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3749 = { sizeof (SecurityKeyIdentifierClause_t1943429813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3749[3] = 
{
	SecurityKeyIdentifierClause_t1943429813::get_offset_of_clause_type_0(),
	SecurityKeyIdentifierClause_t1943429813::get_offset_of_nonce_1(),
	SecurityKeyIdentifierClause_t1943429813::get_offset_of_deriv_length_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3750 = { sizeof (SecurityKeyType_t114151047)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3750[3] = 
{
	SecurityKeyType_t114151047::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3751 = { sizeof (SecurityKeyUsage_t3512984903)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3751[3] = 
{
	SecurityKeyUsage_t3512984903::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3752 = { sizeof (SecurityToken_t1271873540), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3753 = { sizeof (SecurityTokenException_t1526649921), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3754 = { sizeof (SecurityTokenTypes_t3586841219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3755 = { sizeof (SymmetricSecurityKey_t1678653605), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3756 = { sizeof (HMAC_t3689525211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3756[4] = 
{
	HMAC_t3689525211::get_offset_of_hash_5(),
	HMAC_t3689525211::get_offset_of_hashing_6(),
	HMAC_t3689525211::get_offset_of_innerPad_7(),
	HMAC_t3689525211::get_offset_of_outerPad_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3757 = { sizeof (UserNameSecurityToken_t2268299345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3757[4] = 
{
	UserNameSecurityToken_t2268299345::get_offset_of_from_0(),
	UserNameSecurityToken_t2268299345::get_offset_of_username_1(),
	UserNameSecurityToken_t2268299345::get_offset_of_password_2(),
	UserNameSecurityToken_t2268299345::get_offset_of_id_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3758 = { sizeof (WindowsSecurityToken_t1335964444), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3758[1] = 
{
	WindowsSecurityToken_t1335964444::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3759 = { sizeof (X509AsymmetricSecurityKey_t306644821), -1, sizeof(X509AsymmetricSecurityKey_t306644821_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3759[5] = 
{
	X509AsymmetricSecurityKey_t306644821::get_offset_of_cert_0(),
	X509AsymmetricSecurityKey_t306644821_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_1(),
	X509AsymmetricSecurityKey_t306644821_StaticFields::get_offset_of_U3CU3Ef__switchU24map8_2(),
	X509AsymmetricSecurityKey_t306644821_StaticFields::get_offset_of_U3CU3Ef__switchU24map9_3(),
	X509AsymmetricSecurityKey_t306644821_StaticFields::get_offset_of_U3CU3Ef__switchU24mapA_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3760 = { sizeof (X509IssuerSerialKeyIdentifierClause_t3285382384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3760[2] = 
{
	X509IssuerSerialKeyIdentifierClause_t3285382384::get_offset_of_name_3(),
	X509IssuerSerialKeyIdentifierClause_t3285382384::get_offset_of_serial_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3761 = { sizeof (X509RawDataKeyIdentifierClause_t329336726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3761[1] = 
{
	X509RawDataKeyIdentifierClause_t329336726::get_offset_of_cert_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3762 = { sizeof (X509SecurityToken_t1311826287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3762[3] = 
{
	X509SecurityToken_t1311826287::get_offset_of_cert_0(),
	X509SecurityToken_t1311826287::get_offset_of_id_1(),
	X509SecurityToken_t1311826287::get_offset_of_keys_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3763 = { sizeof (X509SubjectKeyIdentifierClause_t676849360), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3764 = { sizeof (X509ThumbprintKeyIdentifierClause_t1082272133), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3765 = { sizeof (U3CModuleU3E_t692745548), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3766 = { sizeof (U3CModuleU3E_t692745549), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3767 = { sizeof (AndroidJavaObject_t4131667876), -1, sizeof(AndroidJavaObject_t4131667876_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3767[5] = 
{
	AndroidJavaObject_t4131667876_StaticFields::get_offset_of_enableDebugPrints_0(),
	AndroidJavaObject_t4131667876::get_offset_of_m_disposed_1(),
	AndroidJavaObject_t4131667876::get_offset_of_m_jobject_2(),
	AndroidJavaObject_t4131667876::get_offset_of_m_jclass_3(),
	AndroidJavaObject_t4131667876_StaticFields::get_offset_of_s_JavaLangClass_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3768 = { sizeof (AndroidJavaClass_t32045322), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3769 = { sizeof (jvalue_t1372148875)+ sizeof (Il2CppObject), sizeof(jvalue_t1372148875_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3769[9] = 
{
	jvalue_t1372148875::get_offset_of_z_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	jvalue_t1372148875::get_offset_of_b_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	jvalue_t1372148875::get_offset_of_c_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	jvalue_t1372148875::get_offset_of_s_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	jvalue_t1372148875::get_offset_of_i_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	jvalue_t1372148875::get_offset_of_j_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	jvalue_t1372148875::get_offset_of_f_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	jvalue_t1372148875::get_offset_of_d_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	jvalue_t1372148875::get_offset_of_l_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3770 = { sizeof (AndroidJNIHelper_t1961468794), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3771 = { sizeof (AndroidJNI_t738955263), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3772 = { sizeof (NetworkReachability_t3450623372)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3772[4] = 
{
	NetworkReachability_t3450623372::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3773 = { sizeof (Application_t1852185770), -1, sizeof(Application_t1852185770_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3773[3] = 
{
	Application_t1852185770_StaticFields::get_offset_of_lowMemory_0(),
	Application_t1852185770_StaticFields::get_offset_of_s_LogCallbackHandler_1(),
	Application_t1852185770_StaticFields::get_offset_of_s_LogCallbackHandlerThreaded_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3774 = { sizeof (LowMemoryCallback_t4104246196), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3775 = { sizeof (LogCallback_t3588208630), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3776 = { sizeof (UserAuthorization_t2950000085)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3776[3] = 
{
	UserAuthorization_t2950000085::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3777 = { sizeof (AssetBundleCreateRequest_t3119663542), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3778 = { sizeof (AssetBundleRequest_t699759206), sizeof(AssetBundleRequest_t699759206_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3779 = { sizeof (AssetBundle_t1153907252), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3780 = { sizeof (AsyncOperation_t1445031843), sizeof(AsyncOperation_t1445031843_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3780[1] = 
{
	AsyncOperation_t1445031843::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3781 = { sizeof (SystemInfo_t3561985952), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3782 = { sizeof (WaitForSeconds_t1699091251), sizeof(WaitForSeconds_t1699091251_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3782[1] = 
{
	WaitForSeconds_t1699091251::get_offset_of_m_Seconds_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3783 = { sizeof (WaitForFixedUpdate_t1634918743), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3784 = { sizeof (WaitForEndOfFrame_t1314943911), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3785 = { sizeof (CustomYieldInstruction_t1895667560), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3786 = { sizeof (Coroutine_t3829159415), sizeof(Coroutine_t3829159415_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3786[1] = 
{
	Coroutine_t3829159415::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3787 = { sizeof (ScriptableObject_t2528358522), sizeof(ScriptableObject_t2528358522_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3788 = { sizeof (Behaviour_t1437897464), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3789 = { sizeof (Camera_t4157153871), -1, sizeof(Camera_t4157153871_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3789[3] = 
{
	Camera_t4157153871_StaticFields::get_offset_of_onPreCull_2(),
	Camera_t4157153871_StaticFields::get_offset_of_onPreRender_3(),
	Camera_t4157153871_StaticFields::get_offset_of_onPostRender_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3790 = { sizeof (CameraCallback_t190067161), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3791 = { sizeof (Component_t1923634451), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3792 = { sizeof (ComputeBuffer_t1033194329), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3792[1] = 
{
	ComputeBuffer_t1033194329::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3793 = { sizeof (UnhandledExceptionHandler_t1162846485), -1, sizeof(UnhandledExceptionHandler_t1162846485_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3793[1] = 
{
	UnhandledExceptionHandler_t1162846485_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3794 = { sizeof (CullingGroupEvent_t1722745023)+ sizeof (Il2CppObject), sizeof(CullingGroupEvent_t1722745023 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3794[3] = 
{
	CullingGroupEvent_t1722745023::get_offset_of_m_Index_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CullingGroupEvent_t1722745023::get_offset_of_m_PrevState_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CullingGroupEvent_t1722745023::get_offset_of_m_ThisState_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3795 = { sizeof (CullingGroup_t2096318768), sizeof(CullingGroup_t2096318768_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3795[2] = 
{
	CullingGroup_t2096318768::get_offset_of_m_Ptr_0(),
	CullingGroup_t2096318768::get_offset_of_m_OnStateChanged_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3796 = { sizeof (StateChanged_t2136737110), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3797 = { sizeof (CursorLockMode_t2840764040)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3797[4] = 
{
	CursorLockMode_t2840764040::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3798 = { sizeof (Cursor_t1422555833), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3799 = { sizeof (DebugLogHandler_t826086171), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
