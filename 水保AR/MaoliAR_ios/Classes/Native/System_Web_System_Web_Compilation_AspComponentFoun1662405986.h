﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_Compilation_AspComponentFoun1085528248.h"

// System.String
struct String_t;
// System.Reflection.Assembly
struct Assembly_t4102432799;
// System.Collections.Generic.Dictionary`2<System.String,System.Reflection.Assembly>
struct Dictionary_2_t3887689098;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.AspComponentFoundry/AssemblyFoundry
struct  AssemblyFoundry_t1662405986  : public Foundry_t1085528248
{
public:
	// System.String System.Web.Compilation.AspComponentFoundry/AssemblyFoundry::nameSpace
	String_t* ___nameSpace_1;
	// System.Reflection.Assembly System.Web.Compilation.AspComponentFoundry/AssemblyFoundry::assembly
	Assembly_t4102432799 * ___assembly_2;
	// System.String System.Web.Compilation.AspComponentFoundry/AssemblyFoundry::assemblyName
	String_t* ___assemblyName_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Reflection.Assembly> System.Web.Compilation.AspComponentFoundry/AssemblyFoundry::assemblyCache
	Dictionary_2_t3887689098 * ___assemblyCache_4;

public:
	inline static int32_t get_offset_of_nameSpace_1() { return static_cast<int32_t>(offsetof(AssemblyFoundry_t1662405986, ___nameSpace_1)); }
	inline String_t* get_nameSpace_1() const { return ___nameSpace_1; }
	inline String_t** get_address_of_nameSpace_1() { return &___nameSpace_1; }
	inline void set_nameSpace_1(String_t* value)
	{
		___nameSpace_1 = value;
		Il2CppCodeGenWriteBarrier(&___nameSpace_1, value);
	}

	inline static int32_t get_offset_of_assembly_2() { return static_cast<int32_t>(offsetof(AssemblyFoundry_t1662405986, ___assembly_2)); }
	inline Assembly_t4102432799 * get_assembly_2() const { return ___assembly_2; }
	inline Assembly_t4102432799 ** get_address_of_assembly_2() { return &___assembly_2; }
	inline void set_assembly_2(Assembly_t4102432799 * value)
	{
		___assembly_2 = value;
		Il2CppCodeGenWriteBarrier(&___assembly_2, value);
	}

	inline static int32_t get_offset_of_assemblyName_3() { return static_cast<int32_t>(offsetof(AssemblyFoundry_t1662405986, ___assemblyName_3)); }
	inline String_t* get_assemblyName_3() const { return ___assemblyName_3; }
	inline String_t** get_address_of_assemblyName_3() { return &___assemblyName_3; }
	inline void set_assemblyName_3(String_t* value)
	{
		___assemblyName_3 = value;
		Il2CppCodeGenWriteBarrier(&___assemblyName_3, value);
	}

	inline static int32_t get_offset_of_assemblyCache_4() { return static_cast<int32_t>(offsetof(AssemblyFoundry_t1662405986, ___assemblyCache_4)); }
	inline Dictionary_2_t3887689098 * get_assemblyCache_4() const { return ___assemblyCache_4; }
	inline Dictionary_2_t3887689098 ** get_address_of_assemblyCache_4() { return &___assemblyCache_4; }
	inline void set_assemblyCache_4(Dictionary_2_t3887689098 * value)
	{
		___assemblyCache_4 = value;
		Il2CppCodeGenWriteBarrier(&___assemblyCache_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
