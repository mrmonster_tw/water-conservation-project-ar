﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3549286319.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E692745525.h"
#include "AssemblyU2DUnityScript_JSHighlighterController967495899.h"
#include "mscorlib_System_Void1185182177.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "mscorlib_System_RuntimeTypeHandle3027515415.h"
#include "mscorlib_System_Type2483944760.h"
#include "UnityEngine_UnityEngine_Object631007953.h"
#include "UnityEngine_UnityEngine_Component1923634451.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_Hi672210414.h"
#include "mscorlib_System_Boolean97287965.h"
#include "UnityEngine_UnityEngine_GameObject1113636619.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"
#include "mscorlib_System_Single1397266774.h"

// JSHighlighterController
struct JSHighlighterController_t967495899;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// System.Type
struct Type_t;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// HighlightingSystem.Highlighter
struct Highlighter_t672210414;
extern const Il2CppType* Highlighter_t672210414_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Highlighter_t672210414_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t631007953_il2cpp_TypeInfo_var;
extern const uint32_t JSHighlighterController_Awake_m313278770_MetadataUsageId;




// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1339182015 (MonoBehaviour_t3962482529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m1620074514 (Il2CppObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.Component::GetComponent(System.Type)
extern "C"  Component_t1923634451 * Component_GetComponent_m2813676312 (Component_t1923634451 * __this, Type_t * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1454075600 (Il2CppObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1113636619 * Component_get_gameObject_m2648350745 (Component_t1923634451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::AddComponent(System.Type)
extern "C"  Component_t1923634451 * GameObject_AddComponent_m2485158059 (GameObject_t1113636619 * __this, Type_t * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m3057542999 (Color_t2555686324 * __this, float p0, float p1, float p2, float p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightingSystem.Highlighter::FlashingOn(UnityEngine.Color,UnityEngine.Color,System.Single)
extern "C"  void Highlighter_FlashingOn_m66103418 (Highlighter_t672210414 * __this, Color_t2555686324  p0, Color_t2555686324  p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JSHighlighterController::.ctor()
extern "C"  void JSHighlighterController__ctor_m2286307089 (JSHighlighterController_t967495899 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1339182015(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSHighlighterController::Awake()
extern "C"  void JSHighlighterController_Awake_m313278770 (JSHighlighterController_t967495899 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSHighlighterController_Awake_m313278770_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Highlighter_t672210414_0_0_0_var), /*hidden argument*/NULL);
		Component_t1923634451 * L_1 = Component_GetComponent_m2813676312(__this, L_0, /*hidden argument*/NULL);
		__this->set_h_2(((Highlighter_t672210414 *)CastclassClass(L_1, Highlighter_t672210414_il2cpp_TypeInfo_var)));
		Highlighter_t672210414 * L_2 = __this->get_h_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_004c;
		}
	}
	{
		GameObject_t1113636619 * L_4 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Highlighter_t672210414_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_4);
		Component_t1923634451 * L_6 = GameObject_AddComponent_m2485158059(L_4, L_5, /*hidden argument*/NULL);
		__this->set_h_2(((Highlighter_t672210414 *)CastclassClass(L_6, Highlighter_t672210414_il2cpp_TypeInfo_var)));
	}

IL_004c:
	{
		return;
	}
}
// System.Void JSHighlighterController::Start()
extern "C"  void JSHighlighterController_Start_m3902786733 (JSHighlighterController_t967495899 * __this, const MethodInfo* method)
{
	{
		Highlighter_t672210414 * L_0 = __this->get_h_2();
		Color_t2555686324  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Color__ctor_m3057542999(&L_1, (1.0f), (1.0f), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		Color_t2555686324  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Color__ctor_m3057542999(&L_2, (1.0f), (1.0f), (((float)((float)0))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_0);
		Highlighter_FlashingOn_m66103418(L_0, L_1, L_2, (2.0f), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
