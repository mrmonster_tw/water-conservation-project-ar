﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Xml.XPath.XPathNavigatorComparer
struct XPathNavigatorComparer_t2421573191;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNavigatorComparer
struct  XPathNavigatorComparer_t2421573191  : public Il2CppObject
{
public:

public:
};

struct XPathNavigatorComparer_t2421573191_StaticFields
{
public:
	// System.Xml.XPath.XPathNavigatorComparer System.Xml.XPath.XPathNavigatorComparer::Instance
	XPathNavigatorComparer_t2421573191 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(XPathNavigatorComparer_t2421573191_StaticFields, ___Instance_0)); }
	inline XPathNavigatorComparer_t2421573191 * get_Instance_0() const { return ___Instance_0; }
	inline XPathNavigatorComparer_t2421573191 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(XPathNavigatorComparer_t2421573191 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
