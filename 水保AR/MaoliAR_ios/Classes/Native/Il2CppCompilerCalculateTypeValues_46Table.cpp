﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_ServiceModel_System_ServiceModel_Channels_A3096310965.h"
#include "System_ServiceModel_System_ServiceModel_Channels_A2516210953.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Ad331929339.h"
#include "System_ServiceModel_System_ServiceModel_Channels_A2257583243.h"
#include "System_ServiceModel_System_ServiceModel_Channels_A2159115947.h"
#include "System_ServiceModel_System_ServiceModel_Channels_A1824225625.h"
#include "System_ServiceModel_System_ServiceModel_Channels_A2673164393.h"
#include "System_ServiceModel_System_ServiceModel_Channels_B3396373419.h"
#include "System_ServiceModel_System_ServiceModel_Channels_B4125415926.h"
#include "System_ServiceModel_System_ServiceModel_Channels_B4154157131.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Bi859993683.h"
#include "System_ServiceModel_System_ServiceModel_Channels_B2842489830.h"
#include "System_ServiceModel_System_ServiceModel_Channels_B3643137745.h"
#include "System_ServiceModel_System_ServiceModel_Channels_B2456870521.h"
#include "System_ServiceModel_System_ServiceModel_Channels_B3413021685.h"
#include "System_ServiceModel_System_ServiceModel_Channels_B3441673553.h"
#include "System_ServiceModel_System_ServiceModel_Channels_C1253282329.h"
#include "System_ServiceModel_System_ServiceModel_Channels_C1586116283.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Ch715356738.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Ch990592377.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Ch740725699.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Co518829156.h"
#include "System_ServiceModel_System_ServiceModel_Channels_C1818663349.h"
#include "System_ServiceModel_System_ServiceModel_Channels_C1159308668.h"
#include "System_ServiceModel_System_ServiceModel_Channels_C3754390252.h"
#include "System_ServiceModel_System_ServiceModel_Channels_C4187704225.h"
#include "System_ServiceModel_System_ServiceModel_Channels_D2906515918.h"
#include "System_ServiceModel_System_ServiceModel_Channels_D4202970142.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Du405913978.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Dup28742579.h"
#include "System_ServiceModel_System_ServiceModel_Channels_D1048830322.h"
#include "System_ServiceModel_System_ServiceModel_Channels_D3887558294.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Du217889813.h"
#include "System_ServiceModel_System_ServiceModel_Channels_F1517933559.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Fa768171908.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Ht684206763.h"
#include "System_ServiceModel_System_ServiceModel_Channels_H3561535433.h"
#include "System_ServiceModel_System_ServiceModel_Channels_H3912101009.h"
#include "System_ServiceModel_System_ServiceModel_Channels_As957273228.h"
#include "System_ServiceModel_System_ServiceModel_Channels_H2559451545.h"
#include "System_ServiceModel_System_ServiceModel_Channels_H1650476842.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Asp71798493.h"
#include "System_ServiceModel_System_ServiceModel_Channels_A3055456028.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Ht555124713.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Ht942440974.h"
#include "System_ServiceModel_System_ServiceModel_Channels_H3854184778.h"
#include "System_ServiceModel_System_ServiceModel_Channels_H2921328172.h"
#include "System_ServiceModel_System_ServiceModel_Channels_H3141234096.h"
#include "System_ServiceModel_System_ServiceModel_Channels_H1615723811.h"
#include "System_ServiceModel_System_ServiceModel_Channels_H2019380601.h"
#include "System_ServiceModel_System_ServiceModel_Channels_H2392894562.h"
#include "System_ServiceModel_System_ServiceModel_Channels_H3982995109.h"
#include "System_ServiceModel_System_ServiceModel_Channels_H1161995113.h"
#include "System_ServiceModel_System_ServiceModel_Channels_L2865192915.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Me869514973.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Me778472114.h"
#include "System_ServiceModel_System_ServiceModel_Channels_D1331859589.h"
#include "System_ServiceModel_System_ServiceModel_Channels_M3063398011.h"
#include "System_ServiceModel_System_ServiceModel_Channels_M3561136435.h"
#include "System_ServiceModel_System_ServiceModel_Channels_M3531153504.h"
#include "System_ServiceModel_System_ServiceModel_Channels_M3849330512.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Me929740066.h"
#include "System_ServiceModel_System_ServiceModel_Channels_M3639017112.h"
#include "System_ServiceModel_System_ServiceModel_Channels_M1251849029.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4600 = { sizeof (AddressHeader_t3096310965), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4601 = { sizeof (DefaultAddressHeader_t2516210953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4601[4] = 
{
	DefaultAddressHeader_t2516210953::get_offset_of_name_0(),
	DefaultAddressHeader_t2516210953::get_offset_of_ns_1(),
	DefaultAddressHeader_t2516210953::get_offset_of_formatter_2(),
	DefaultAddressHeader_t2516210953::get_offset_of_value_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4602 = { sizeof (AddressHeaderCollection_t331929339), -1, sizeof(AddressHeaderCollection_t331929339_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4602[1] = 
{
	AddressHeaderCollection_t331929339_StaticFields::get_offset_of_empty_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4603 = { sizeof (AddressingVersion_t2257583243), -1, sizeof(AddressingVersion_t2257583243_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4603[5] = 
{
	AddressingVersion_t2257583243::get_offset_of_name_0(),
	AddressingVersion_t2257583243::get_offset_of_address_1(),
	AddressingVersion_t2257583243_StaticFields::get_offset_of_addressing200408_2(),
	AddressingVersion_t2257583243_StaticFields::get_offset_of_addressing1_0_3(),
	AddressingVersion_t2257583243_StaticFields::get_offset_of_none_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4604 = { sizeof (AspNetReplyChannel_t2159115947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4604[4] = 
{
	AspNetReplyChannel_t2159115947::get_offset_of_listener_16(),
	AspNetReplyChannel_t2159115947::get_offset_of_waiting_17(),
	AspNetReplyChannel_t2159115947::get_offset_of_http_context_18(),
	AspNetReplyChannel_t2159115947::get_offset_of_wait_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4605 = { sizeof (AspNetRequestContext_t1824225625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4605[2] = 
{
	AspNetRequestContext_t1824225625::get_offset_of_channel_2(),
	AspNetRequestContext_t1824225625::get_offset_of_ctx_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4606 = { sizeof (AsymmetricSecurityBindingElement_t2673164393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4606[5] = 
{
	AsymmetricSecurityBindingElement_t2673164393::get_offset_of_msg_protection_order_11(),
	AsymmetricSecurityBindingElement_t2673164393::get_offset_of_initiator_token_params_12(),
	AsymmetricSecurityBindingElement_t2673164393::get_offset_of_recipient_token_params_13(),
	AsymmetricSecurityBindingElement_t2673164393::get_offset_of_allow_serialized_sign_14(),
	AsymmetricSecurityBindingElement_t2673164393::get_offset_of_require_sig_confirm_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4607 = { sizeof (BinaryMessageEncoder_t3396373419), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4607[4] = 
{
	BinaryMessageEncoder_t3396373419::get_offset_of_owner_0(),
	BinaryMessageEncoder_t3396373419::get_offset_of_session_1(),
	BinaryMessageEncoder_t3396373419::get_offset_of_U3CCurrentReaderSessionU3Ek__BackingField_2(),
	BinaryMessageEncoder_t3396373419::get_offset_of_U3CCurrentWriterSessionU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4608 = { sizeof (BinaryMessageEncoderFactory_t4125415926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4608[2] = 
{
	BinaryMessageEncoderFactory_t4125415926::get_offset_of_owner_0(),
	BinaryMessageEncoderFactory_t4125415926::get_offset_of_encoder_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4609 = { sizeof (BinaryMessageEncodingBindingElement_t4154157131), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4609[4] = 
{
	BinaryMessageEncodingBindingElement_t4154157131::get_offset_of_max_read_pool_size_0(),
	BinaryMessageEncodingBindingElement_t4154157131::get_offset_of_max_write_pool_size_1(),
	BinaryMessageEncodingBindingElement_t4154157131::get_offset_of_quotas_2(),
	BinaryMessageEncodingBindingElement_t4154157131::get_offset_of_version_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4610 = { sizeof (Binding_t859993683), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4610[6] = 
{
	Binding_t859993683::get_offset_of_name_0(),
	Binding_t859993683::get_offset_of_ns_1(),
	Binding_t859993683::get_offset_of_open_timeout_2(),
	Binding_t859993683::get_offset_of_close_timeout_3(),
	Binding_t859993683::get_offset_of_receive_timeout_4(),
	Binding_t859993683::get_offset_of_send_timeout_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4611 = { sizeof (BindingContext_t2842489830), -1, sizeof(BindingContext_t2842489830_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4611[8] = 
{
	BindingContext_t2842489830_StaticFields::get_offset_of_empty_collection_0(),
	BindingContext_t2842489830::get_offset_of_binding_1(),
	BindingContext_t2842489830::get_offset_of_parameters_2(),
	BindingContext_t2842489830::get_offset_of_elements_3(),
	BindingContext_t2842489830::get_offset_of_remaining_elements_4(),
	BindingContext_t2842489830::get_offset_of_listen_uri_base_5(),
	BindingContext_t2842489830::get_offset_of_listen_uri_relative_6(),
	BindingContext_t2842489830::get_offset_of_listen_uri_mode_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4612 = { sizeof (BindingElement_t3643137745), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4613 = { sizeof (BindingElementCollection_t2456870521), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4614 = { sizeof (BindingParameterCollection_t3413021685), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4615 = { sizeof (BodyWriter_t3441673553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4615[1] = 
{
	BodyWriter_t3441673553::get_offset_of_is_buffered_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4616 = { sizeof (CachingCompiler_t1253282329), -1, sizeof(CachingCompiler_t1253282329_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4616[2] = 
{
	CachingCompiler_t1253282329_StaticFields::get_offset_of_dynamicBase_0(),
	CachingCompiler_t1253282329_StaticFields::get_offset_of_compilationTickets_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4617 = { sizeof (ChannelBase_t1586116283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4617[1] = 
{
	ChannelBase_t1586116283::get_offset_of_manager_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4618 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4618[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4619 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4619[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4620 = { sizeof (ChannelFactoryBase_t715356738), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4620[4] = 
{
	ChannelFactoryBase_t715356738::get_offset_of_open_timeout_9(),
	ChannelFactoryBase_t715356738::get_offset_of_close_timeout_10(),
	ChannelFactoryBase_t715356738::get_offset_of_receive_timeout_11(),
	ChannelFactoryBase_t715356738::get_offset_of_send_timeout_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4621 = { sizeof (ChannelListenerBase_t990592377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4621[2] = 
{
	ChannelListenerBase_t990592377::get_offset_of_timeouts_9(),
	ChannelListenerBase_t990592377::get_offset_of_properties_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4622 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4622[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4623 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4623[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4624 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4624[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4625 = { sizeof (ChannelManagerBase_t740725699), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4626 = { sizeof (CommunicationObject_t518829156), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4626[9] = 
{
	CommunicationObject_t518829156::get_offset_of_mutex_0(),
	CommunicationObject_t518829156::get_offset_of_state_1(),
	CommunicationObject_t518829156::get_offset_of_default_open_timeout_2(),
	CommunicationObject_t518829156::get_offset_of_default_close_timeout_3(),
	CommunicationObject_t518829156::get_offset_of_Closed_4(),
	CommunicationObject_t518829156::get_offset_of_Closing_5(),
	CommunicationObject_t518829156::get_offset_of_Faulted_6(),
	CommunicationObject_t518829156::get_offset_of_Opened_7(),
	CommunicationObject_t518829156::get_offset_of_Opening_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4627 = { sizeof (CompilationException_t1818663349), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4627[3] = 
{
	CompilationException_t1818663349::get_offset_of_filename_11(),
	CompilationException_t1818663349::get_offset_of_errors_12(),
	CompilationException_t1818663349::get_offset_of_fileText_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4628 = { sizeof (CompositeDuplexBindingElementImporter_t1159308668), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4629 = { sizeof (ConnectionOrientedTransportBindingElement_t3754390252), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4629[8] = 
{
	ConnectionOrientedTransportBindingElement_t3754390252::get_offset_of_connection_buf_size_3(),
	ConnectionOrientedTransportBindingElement_t3754390252::get_offset_of_max_buf_size_4(),
	ConnectionOrientedTransportBindingElement_t3754390252::get_offset_of_max_pending_conn_5(),
	ConnectionOrientedTransportBindingElement_t3754390252::get_offset_of_max_pending_accepts_6(),
	ConnectionOrientedTransportBindingElement_t3754390252::get_offset_of_host_cmp_mode_7(),
	ConnectionOrientedTransportBindingElement_t3754390252::get_offset_of_max_output_delay_8(),
	ConnectionOrientedTransportBindingElement_t3754390252::get_offset_of_ch_init_timeout_9(),
	ConnectionOrientedTransportBindingElement_t3754390252::get_offset_of_transfer_mode_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4630 = { sizeof (CustomBinding_t4187704225), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4630[4] = 
{
	CustomBinding_t4187704225::get_offset_of_elements_6(),
	CustomBinding_t4187704225::get_offset_of_binding_7(),
	CustomBinding_t4187704225::get_offset_of_security_8(),
	CustomBinding_t4187704225::get_offset_of_scheme_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4631 = { sizeof (DuplexChannelBase_t2906515918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4631[10] = 
{
	DuplexChannelBase_t2906515918::get_offset_of_channel_listener_base_10(),
	DuplexChannelBase_t2906515918::get_offset_of_local_address_11(),
	DuplexChannelBase_t2906515918::get_offset_of_remote_address_12(),
	DuplexChannelBase_t2906515918::get_offset_of_via_13(),
	DuplexChannelBase_t2906515918::get_offset_of_open_handler_14(),
	DuplexChannelBase_t2906515918::get_offset_of_close_handler_15(),
	DuplexChannelBase_t2906515918::get_offset_of_send_handler_16(),
	DuplexChannelBase_t2906515918::get_offset_of_receive_handler_17(),
	DuplexChannelBase_t2906515918::get_offset_of_try_receive_handler_18(),
	DuplexChannelBase_t2906515918::get_offset_of_wait_handler_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4632 = { sizeof (AsyncSendHandler_t4202970142), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4633 = { sizeof (AsyncReceiveHandler_t405913978), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4634 = { sizeof (TryReceiveHandler_t28742579), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4635 = { sizeof (AsyncWaitForMessageHandler_t1048830322), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4636 = { sizeof (DuplexSessionBase_t3887558294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4636[2] = 
{
	DuplexSessionBase_t3887558294::get_offset_of_async_method_0(),
	DuplexSessionBase_t3887558294::get_offset_of_U3CIdU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4637 = { sizeof (AsyncHandler_t217889813), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4638 = { sizeof (FaultConverter_t1517933559), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4639 = { sizeof (SimpleFaultConverter_t768171908), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4639[1] = 
{
	SimpleFaultConverter_t768171908::get_offset_of_version_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4640 = { sizeof (HtmlizedException_t684206763), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4641 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4641[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4642 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4642[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4643 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4644 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4644[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4645 = { sizeof (HttpContextInfo_t3561535433), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4646 = { sizeof (HttpListenerContextInfo_t3912101009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4646[1] = 
{
	HttpListenerContextInfo_t3912101009::get_offset_of_ctx_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4647 = { sizeof (AspNetHttpContextInfo_t957273228), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4647[1] = 
{
	AspNetHttpContextInfo_t957273228::get_offset_of_ctx_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4648 = { sizeof (HttpSimpleListenerManager_t2559451545), -1, sizeof(HttpSimpleListenerManager_t2559451545_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4648[2] = 
{
	HttpSimpleListenerManager_t2559451545_StaticFields::get_offset_of_opened_listeners_7(),
	HttpSimpleListenerManager_t2559451545::get_offset_of_http_listener_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4649 = { sizeof (U3CKickContextReceiverU3Ec__AnonStorey9_t1650476842), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4649[2] = 
{
	U3CKickContextReceiverU3Ec__AnonStorey9_t1650476842::get_offset_of_contextReceivedCallback_0(),
	U3CKickContextReceiverU3Ec__AnonStorey9_t1650476842::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4650 = { sizeof (AspNetListenerManager_t71798493), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4650[2] = 
{
	AspNetListenerManager_t71798493::get_offset_of_http_handler_7(),
	AspNetListenerManager_t71798493::get_offset_of_wait_delegate_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4651 = { sizeof (U3CKickContextReceiverU3Ec__AnonStoreyA_t3055456028), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4651[2] = 
{
	U3CKickContextReceiverU3Ec__AnonStoreyA_t3055456028::get_offset_of_contextReceivedCallback_0(),
	U3CKickContextReceiverU3Ec__AnonStoreyA_t3055456028::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4652 = { sizeof (HttpListenerManager_t555124713), -1, sizeof(HttpListenerManager_t555124713_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4652[7] = 
{
	HttpListenerManager_t555124713_StaticFields::get_offset_of_registered_channels_0(),
	HttpListenerManager_t555124713::get_offset_of_channel_listener_1(),
	HttpListenerManager_t555124713::get_offset_of_mex_info_2(),
	HttpListenerManager_t555124713::get_offset_of_wsdl_instance_3(),
	HttpListenerManager_t555124713::get_offset_of_wait_http_ctx_4(),
	HttpListenerManager_t555124713::get_offset_of_pending_5(),
	HttpListenerManager_t555124713::get_offset_of_U3CSourceU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4653 = { sizeof (HttpSimpleReplyChannel_t942440974), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4653[4] = 
{
	HttpSimpleReplyChannel_t942440974::get_offset_of_source_16(),
	HttpSimpleReplyChannel_t942440974::get_offset_of_waiting_17(),
	HttpSimpleReplyChannel_t942440974::get_offset_of_reqctx_18(),
	HttpSimpleReplyChannel_t942440974::get_offset_of_wait_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4654 = { sizeof (HttpReplyChannel_t3854184778), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4654[1] = 
{
	HttpReplyChannel_t3854184778::get_offset_of_source_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4655 = { sizeof (HttpRequestContext_t2921328172), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4655[1] = 
{
	HttpRequestContext_t2921328172::get_offset_of_ctx_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4656 = { sizeof (HttpRequestContextBase_t3141234096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4656[2] = 
{
	HttpRequestContextBase_t3141234096::get_offset_of_request_0(),
	HttpRequestContextBase_t3141234096::get_offset_of_channel_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4657 = { sizeof (HttpRequestMessageProperty_t1615723811), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4657[3] = 
{
	HttpRequestMessageProperty_t1615723811::get_offset_of_headers_0(),
	HttpRequestMessageProperty_t1615723811::get_offset_of_method_1(),
	HttpRequestMessageProperty_t1615723811::get_offset_of_query_string_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4658 = { sizeof (HttpResponseMessageProperty_t2019380601), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4658[4] = 
{
	HttpResponseMessageProperty_t2019380601::get_offset_of_headers_0(),
	HttpResponseMessageProperty_t2019380601::get_offset_of_status_desc_1(),
	HttpResponseMessageProperty_t2019380601::get_offset_of_status_code_2(),
	HttpResponseMessageProperty_t2019380601::get_offset_of_suppress_entity_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4659 = { sizeof (HttpTransportBindingElement_t2392894562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4659[13] = 
{
	HttpTransportBindingElement_t2392894562::get_offset_of_allow_cookies_3(),
	HttpTransportBindingElement_t2392894562::get_offset_of_bypass_proxy_on_local_4(),
	HttpTransportBindingElement_t2392894562::get_offset_of_unsafe_ntlm_auth_5(),
	HttpTransportBindingElement_t2392894562::get_offset_of_use_default_proxy_6(),
	HttpTransportBindingElement_t2392894562::get_offset_of_keep_alive_enabled_7(),
	HttpTransportBindingElement_t2392894562::get_offset_of_max_buffer_size_8(),
	HttpTransportBindingElement_t2392894562::get_offset_of_host_cmp_mode_9(),
	HttpTransportBindingElement_t2392894562::get_offset_of_proxy_address_10(),
	HttpTransportBindingElement_t2392894562::get_offset_of_realm_11(),
	HttpTransportBindingElement_t2392894562::get_offset_of_transfer_mode_12(),
	HttpTransportBindingElement_t2392894562::get_offset_of_timeouts_13(),
	HttpTransportBindingElement_t2392894562::get_offset_of_auth_scheme_14(),
	HttpTransportBindingElement_t2392894562::get_offset_of_proxy_auth_scheme_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4660 = { sizeof (HttpBindingProperties_t3982995109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4660[1] = 
{
	HttpBindingProperties_t3982995109::get_offset_of_source_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4661 = { sizeof (HttpsTransportBindingElement_t1161995113), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4661[1] = 
{
	HttpsTransportBindingElement_t1161995113::get_offset_of_req_cli_cert_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4662 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4663 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4664 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4665 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4666 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4667 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4668 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4669 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4670 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4671 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4672 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4673 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4674 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4675 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4676 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4677 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4678 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4679 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4680 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4681 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4682 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4683 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4684 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4685 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4686 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4687 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4688 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4689 = { sizeof (LocalClientSecuritySettings_t2865192915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4689[12] = 
{
	LocalClientSecuritySettings_t2865192915::get_offset_of_cache_cookies_0(),
	LocalClientSecuritySettings_t2865192915::get_offset_of_cookie_renewal_1(),
	LocalClientSecuritySettings_t2865192915::get_offset_of_detect_replays_2(),
	LocalClientSecuritySettings_t2865192915::get_offset_of_verifier_3(),
	LocalClientSecuritySettings_t2865192915::get_offset_of_max_cookie_cache_time_4(),
	LocalClientSecuritySettings_t2865192915::get_offset_of_reconnect_5(),
	LocalClientSecuritySettings_t2865192915::get_offset_of_replay_cache_size_6(),
	LocalClientSecuritySettings_t2865192915::get_offset_of_renewal_interval_7(),
	LocalClientSecuritySettings_t2865192915::get_offset_of_rollover_interval_8(),
	LocalClientSecuritySettings_t2865192915::get_offset_of_U3CMaxClockSkewU3Ek__BackingField_9(),
	LocalClientSecuritySettings_t2865192915::get_offset_of_U3CReplayWindowU3Ek__BackingField_10(),
	LocalClientSecuritySettings_t2865192915::get_offset_of_U3CTimestampValidityDurationU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4690 = { sizeof (Message_t869514973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4690[3] = 
{
	Message_t869514973::get_offset_of_disposed_0(),
	Message_t869514973::get_offset_of_body_id_1(),
	Message_t869514973::get_offset_of____state_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4691 = { sizeof (MessageBuffer_t778472114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4691[1] = 
{
	MessageBuffer_t778472114::get_offset_of_nav_cache_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4692 = { sizeof (DefaultMessageBuffer_t1331859589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4692[6] = 
{
	DefaultMessageBuffer_t1331859589::get_offset_of_headers_1(),
	DefaultMessageBuffer_t1331859589::get_offset_of_properties_2(),
	DefaultMessageBuffer_t1331859589::get_offset_of_body_3(),
	DefaultMessageBuffer_t1331859589::get_offset_of_closed_4(),
	DefaultMessageBuffer_t1331859589::get_offset_of_is_fault_5(),
	DefaultMessageBuffer_t1331859589::get_offset_of_max_buffer_size_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4693 = { sizeof (MessageEncoder_t3063398011), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4694 = { sizeof (MessageEncoderFactory_t3561136435), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4695 = { sizeof (MessageEncodingBindingElement_t3531153504), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4696 = { sizeof (MessageEncodingBindingElementImporter_t3849330512), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4697 = { sizeof (MessageFault_t929740066), -1, sizeof(MessageFault_t929740066_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4697[3] = 
{
	MessageFault_t929740066_StaticFields::get_offset_of_U3CU3Ef__switchU24map3_0(),
	MessageFault_t929740066_StaticFields::get_offset_of_U3CU3Ef__switchU24map4_1(),
	MessageFault_t929740066_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4698 = { sizeof (SimpleMessageFault_t3639017112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4698[7] = 
{
	SimpleMessageFault_t3639017112::get_offset_of_has_detail_3(),
	SimpleMessageFault_t3639017112::get_offset_of_actor_4(),
	SimpleMessageFault_t3639017112::get_offset_of_node_5(),
	SimpleMessageFault_t3639017112::get_offset_of_code_6(),
	SimpleMessageFault_t3639017112::get_offset_of_reason_7(),
	SimpleMessageFault_t3639017112::get_offset_of_detail_8(),
	SimpleMessageFault_t3639017112::get_offset_of_formatter_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4699 = { sizeof (MessageFaultBodyWriter_t1251849029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4699[2] = 
{
	MessageFaultBodyWriter_t1251849029::get_offset_of_fault_1(),
	MessageFaultBodyWriter_t1251849029::get_offset_of_version_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
