﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_System_ServiceModel_BasicHttpS2873933683.h"

// System.ServiceModel.BasicHttpMessageSecurity
struct BasicHttpMessageSecurity_t1555777380;
// System.ServiceModel.HttpTransportSecurity
struct HttpTransportSecurity_t2136062604;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.BasicHttpSecurity
struct  BasicHttpSecurity_t548302097  : public Il2CppObject
{
public:
	// System.ServiceModel.BasicHttpMessageSecurity System.ServiceModel.BasicHttpSecurity::message
	BasicHttpMessageSecurity_t1555777380 * ___message_0;
	// System.ServiceModel.BasicHttpSecurityMode System.ServiceModel.BasicHttpSecurity::mode
	int32_t ___mode_1;
	// System.ServiceModel.HttpTransportSecurity System.ServiceModel.BasicHttpSecurity::transport
	HttpTransportSecurity_t2136062604 * ___transport_2;

public:
	inline static int32_t get_offset_of_message_0() { return static_cast<int32_t>(offsetof(BasicHttpSecurity_t548302097, ___message_0)); }
	inline BasicHttpMessageSecurity_t1555777380 * get_message_0() const { return ___message_0; }
	inline BasicHttpMessageSecurity_t1555777380 ** get_address_of_message_0() { return &___message_0; }
	inline void set_message_0(BasicHttpMessageSecurity_t1555777380 * value)
	{
		___message_0 = value;
		Il2CppCodeGenWriteBarrier(&___message_0, value);
	}

	inline static int32_t get_offset_of_mode_1() { return static_cast<int32_t>(offsetof(BasicHttpSecurity_t548302097, ___mode_1)); }
	inline int32_t get_mode_1() const { return ___mode_1; }
	inline int32_t* get_address_of_mode_1() { return &___mode_1; }
	inline void set_mode_1(int32_t value)
	{
		___mode_1 = value;
	}

	inline static int32_t get_offset_of_transport_2() { return static_cast<int32_t>(offsetof(BasicHttpSecurity_t548302097, ___transport_2)); }
	inline HttpTransportSecurity_t2136062604 * get_transport_2() const { return ___transport_2; }
	inline HttpTransportSecurity_t2136062604 ** get_address_of_transport_2() { return &___transport_2; }
	inline void set_transport_2(HttpTransportSecurity_t2136062604 * value)
	{
		___transport_2 = value;
		Il2CppCodeGenWriteBarrier(&___transport_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
