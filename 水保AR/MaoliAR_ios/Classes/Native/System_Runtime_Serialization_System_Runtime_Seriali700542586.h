﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.DataMemberInfo
struct  DataMemberInfo_t700542586  : public Il2CppObject
{
public:
	// System.Int32 System.Runtime.Serialization.DataMemberInfo::Order
	int32_t ___Order_0;
	// System.Boolean System.Runtime.Serialization.DataMemberInfo::IsRequired
	bool ___IsRequired_1;
	// System.String System.Runtime.Serialization.DataMemberInfo::XmlName
	String_t* ___XmlName_2;
	// System.Reflection.MemberInfo System.Runtime.Serialization.DataMemberInfo::Member
	MemberInfo_t * ___Member_3;
	// System.String System.Runtime.Serialization.DataMemberInfo::XmlNamespace
	String_t* ___XmlNamespace_4;
	// System.String System.Runtime.Serialization.DataMemberInfo::XmlRootNamespace
	String_t* ___XmlRootNamespace_5;
	// System.Type System.Runtime.Serialization.DataMemberInfo::MemberType
	Type_t * ___MemberType_6;

public:
	inline static int32_t get_offset_of_Order_0() { return static_cast<int32_t>(offsetof(DataMemberInfo_t700542586, ___Order_0)); }
	inline int32_t get_Order_0() const { return ___Order_0; }
	inline int32_t* get_address_of_Order_0() { return &___Order_0; }
	inline void set_Order_0(int32_t value)
	{
		___Order_0 = value;
	}

	inline static int32_t get_offset_of_IsRequired_1() { return static_cast<int32_t>(offsetof(DataMemberInfo_t700542586, ___IsRequired_1)); }
	inline bool get_IsRequired_1() const { return ___IsRequired_1; }
	inline bool* get_address_of_IsRequired_1() { return &___IsRequired_1; }
	inline void set_IsRequired_1(bool value)
	{
		___IsRequired_1 = value;
	}

	inline static int32_t get_offset_of_XmlName_2() { return static_cast<int32_t>(offsetof(DataMemberInfo_t700542586, ___XmlName_2)); }
	inline String_t* get_XmlName_2() const { return ___XmlName_2; }
	inline String_t** get_address_of_XmlName_2() { return &___XmlName_2; }
	inline void set_XmlName_2(String_t* value)
	{
		___XmlName_2 = value;
		Il2CppCodeGenWriteBarrier(&___XmlName_2, value);
	}

	inline static int32_t get_offset_of_Member_3() { return static_cast<int32_t>(offsetof(DataMemberInfo_t700542586, ___Member_3)); }
	inline MemberInfo_t * get_Member_3() const { return ___Member_3; }
	inline MemberInfo_t ** get_address_of_Member_3() { return &___Member_3; }
	inline void set_Member_3(MemberInfo_t * value)
	{
		___Member_3 = value;
		Il2CppCodeGenWriteBarrier(&___Member_3, value);
	}

	inline static int32_t get_offset_of_XmlNamespace_4() { return static_cast<int32_t>(offsetof(DataMemberInfo_t700542586, ___XmlNamespace_4)); }
	inline String_t* get_XmlNamespace_4() const { return ___XmlNamespace_4; }
	inline String_t** get_address_of_XmlNamespace_4() { return &___XmlNamespace_4; }
	inline void set_XmlNamespace_4(String_t* value)
	{
		___XmlNamespace_4 = value;
		Il2CppCodeGenWriteBarrier(&___XmlNamespace_4, value);
	}

	inline static int32_t get_offset_of_XmlRootNamespace_5() { return static_cast<int32_t>(offsetof(DataMemberInfo_t700542586, ___XmlRootNamespace_5)); }
	inline String_t* get_XmlRootNamespace_5() const { return ___XmlRootNamespace_5; }
	inline String_t** get_address_of_XmlRootNamespace_5() { return &___XmlRootNamespace_5; }
	inline void set_XmlRootNamespace_5(String_t* value)
	{
		___XmlRootNamespace_5 = value;
		Il2CppCodeGenWriteBarrier(&___XmlRootNamespace_5, value);
	}

	inline static int32_t get_offset_of_MemberType_6() { return static_cast<int32_t>(offsetof(DataMemberInfo_t700542586, ___MemberType_6)); }
	inline Type_t * get_MemberType_6() const { return ___MemberType_6; }
	inline Type_t ** get_address_of_MemberType_6() { return &___MemberType_6; }
	inline void set_MemberType_6(Type_t * value)
	{
		___MemberType_6 = value;
		Il2CppCodeGenWriteBarrier(&___MemberType_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
