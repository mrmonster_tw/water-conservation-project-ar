﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t435877138;
// System.Text.StringBuilder
struct StringBuilder_t1712802186;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.DecimalFormatPattern
struct  DecimalFormatPattern_t3266596013  : public Il2CppObject
{
public:
	// System.String Mono.Xml.Xsl.DecimalFormatPattern::Prefix
	String_t* ___Prefix_0;
	// System.String Mono.Xml.Xsl.DecimalFormatPattern::Suffix
	String_t* ___Suffix_1;
	// System.String Mono.Xml.Xsl.DecimalFormatPattern::NumberPart
	String_t* ___NumberPart_2;
	// System.Globalization.NumberFormatInfo Mono.Xml.Xsl.DecimalFormatPattern::info
	NumberFormatInfo_t435877138 * ___info_3;
	// System.Text.StringBuilder Mono.Xml.Xsl.DecimalFormatPattern::builder
	StringBuilder_t1712802186 * ___builder_4;

public:
	inline static int32_t get_offset_of_Prefix_0() { return static_cast<int32_t>(offsetof(DecimalFormatPattern_t3266596013, ___Prefix_0)); }
	inline String_t* get_Prefix_0() const { return ___Prefix_0; }
	inline String_t** get_address_of_Prefix_0() { return &___Prefix_0; }
	inline void set_Prefix_0(String_t* value)
	{
		___Prefix_0 = value;
		Il2CppCodeGenWriteBarrier(&___Prefix_0, value);
	}

	inline static int32_t get_offset_of_Suffix_1() { return static_cast<int32_t>(offsetof(DecimalFormatPattern_t3266596013, ___Suffix_1)); }
	inline String_t* get_Suffix_1() const { return ___Suffix_1; }
	inline String_t** get_address_of_Suffix_1() { return &___Suffix_1; }
	inline void set_Suffix_1(String_t* value)
	{
		___Suffix_1 = value;
		Il2CppCodeGenWriteBarrier(&___Suffix_1, value);
	}

	inline static int32_t get_offset_of_NumberPart_2() { return static_cast<int32_t>(offsetof(DecimalFormatPattern_t3266596013, ___NumberPart_2)); }
	inline String_t* get_NumberPart_2() const { return ___NumberPart_2; }
	inline String_t** get_address_of_NumberPart_2() { return &___NumberPart_2; }
	inline void set_NumberPart_2(String_t* value)
	{
		___NumberPart_2 = value;
		Il2CppCodeGenWriteBarrier(&___NumberPart_2, value);
	}

	inline static int32_t get_offset_of_info_3() { return static_cast<int32_t>(offsetof(DecimalFormatPattern_t3266596013, ___info_3)); }
	inline NumberFormatInfo_t435877138 * get_info_3() const { return ___info_3; }
	inline NumberFormatInfo_t435877138 ** get_address_of_info_3() { return &___info_3; }
	inline void set_info_3(NumberFormatInfo_t435877138 * value)
	{
		___info_3 = value;
		Il2CppCodeGenWriteBarrier(&___info_3, value);
	}

	inline static int32_t get_offset_of_builder_4() { return static_cast<int32_t>(offsetof(DecimalFormatPattern_t3266596013, ___builder_4)); }
	inline StringBuilder_t1712802186 * get_builder_4() const { return ___builder_4; }
	inline StringBuilder_t1712802186 ** get_address_of_builder_4() { return &___builder_4; }
	inline void set_builder_4(StringBuilder_t1712802186 * value)
	{
		___builder_4 = value;
		Il2CppCodeGenWriteBarrier(&___builder_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
