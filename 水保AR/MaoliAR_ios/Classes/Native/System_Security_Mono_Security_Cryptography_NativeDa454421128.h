﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"
#include "mscorlib_System_IntPtr840150181.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.NativeDapiProtection/DATA_BLOB
struct  DATA_BLOB_t454421128 
{
public:
	// System.Int32 Mono.Security.Cryptography.NativeDapiProtection/DATA_BLOB::cbData
	int32_t ___cbData_0;
	// System.IntPtr Mono.Security.Cryptography.NativeDapiProtection/DATA_BLOB::pbData
	IntPtr_t ___pbData_1;

public:
	inline static int32_t get_offset_of_cbData_0() { return static_cast<int32_t>(offsetof(DATA_BLOB_t454421128, ___cbData_0)); }
	inline int32_t get_cbData_0() const { return ___cbData_0; }
	inline int32_t* get_address_of_cbData_0() { return &___cbData_0; }
	inline void set_cbData_0(int32_t value)
	{
		___cbData_0 = value;
	}

	inline static int32_t get_offset_of_pbData_1() { return static_cast<int32_t>(offsetof(DATA_BLOB_t454421128, ___pbData_1)); }
	inline IntPtr_t get_pbData_1() const { return ___pbData_1; }
	inline IntPtr_t* get_address_of_pbData_1() { return &___pbData_1; }
	inline void set_pbData_1(IntPtr_t value)
	{
		___pbData_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
