﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// UnityEngine.AudioListener
struct AudioListener_t2734094699;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.KeyCode[]
struct KeyCodeU5BU5D_t2223234056;
// System.Comparison`1<UIWidget>
struct Comparison_1_t3313453104;
// System.Comparison`1<UIPanel>
struct Comparison_1_t1491403520;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NGUITools
struct  NGUITools_t1206951095  : public Il2CppObject
{
public:

public:
};

struct NGUITools_t1206951095_StaticFields
{
public:
	// UnityEngine.AudioListener NGUITools::mListener
	AudioListener_t2734094699 * ___mListener_0;
	// System.Boolean NGUITools::mLoaded
	bool ___mLoaded_1;
	// System.Single NGUITools::mGlobalVolume
	float ___mGlobalVolume_2;
	// System.Single NGUITools::mLastTimestamp
	float ___mLastTimestamp_3;
	// UnityEngine.AudioClip NGUITools::mLastClip
	AudioClip_t3680889665 * ___mLastClip_4;
	// UnityEngine.Vector3[] NGUITools::mSides
	Vector3U5BU5D_t1718750761* ___mSides_5;
	// UnityEngine.KeyCode[] NGUITools::keys
	KeyCodeU5BU5D_t2223234056* ___keys_6;
	// System.Comparison`1<UIWidget> NGUITools::<>f__mg$cache0
	Comparison_1_t3313453104 * ___U3CU3Ef__mgU24cache0_7;
	// System.Comparison`1<UIPanel> NGUITools::<>f__mg$cache1
	Comparison_1_t1491403520 * ___U3CU3Ef__mgU24cache1_8;

public:
	inline static int32_t get_offset_of_mListener_0() { return static_cast<int32_t>(offsetof(NGUITools_t1206951095_StaticFields, ___mListener_0)); }
	inline AudioListener_t2734094699 * get_mListener_0() const { return ___mListener_0; }
	inline AudioListener_t2734094699 ** get_address_of_mListener_0() { return &___mListener_0; }
	inline void set_mListener_0(AudioListener_t2734094699 * value)
	{
		___mListener_0 = value;
		Il2CppCodeGenWriteBarrier(&___mListener_0, value);
	}

	inline static int32_t get_offset_of_mLoaded_1() { return static_cast<int32_t>(offsetof(NGUITools_t1206951095_StaticFields, ___mLoaded_1)); }
	inline bool get_mLoaded_1() const { return ___mLoaded_1; }
	inline bool* get_address_of_mLoaded_1() { return &___mLoaded_1; }
	inline void set_mLoaded_1(bool value)
	{
		___mLoaded_1 = value;
	}

	inline static int32_t get_offset_of_mGlobalVolume_2() { return static_cast<int32_t>(offsetof(NGUITools_t1206951095_StaticFields, ___mGlobalVolume_2)); }
	inline float get_mGlobalVolume_2() const { return ___mGlobalVolume_2; }
	inline float* get_address_of_mGlobalVolume_2() { return &___mGlobalVolume_2; }
	inline void set_mGlobalVolume_2(float value)
	{
		___mGlobalVolume_2 = value;
	}

	inline static int32_t get_offset_of_mLastTimestamp_3() { return static_cast<int32_t>(offsetof(NGUITools_t1206951095_StaticFields, ___mLastTimestamp_3)); }
	inline float get_mLastTimestamp_3() const { return ___mLastTimestamp_3; }
	inline float* get_address_of_mLastTimestamp_3() { return &___mLastTimestamp_3; }
	inline void set_mLastTimestamp_3(float value)
	{
		___mLastTimestamp_3 = value;
	}

	inline static int32_t get_offset_of_mLastClip_4() { return static_cast<int32_t>(offsetof(NGUITools_t1206951095_StaticFields, ___mLastClip_4)); }
	inline AudioClip_t3680889665 * get_mLastClip_4() const { return ___mLastClip_4; }
	inline AudioClip_t3680889665 ** get_address_of_mLastClip_4() { return &___mLastClip_4; }
	inline void set_mLastClip_4(AudioClip_t3680889665 * value)
	{
		___mLastClip_4 = value;
		Il2CppCodeGenWriteBarrier(&___mLastClip_4, value);
	}

	inline static int32_t get_offset_of_mSides_5() { return static_cast<int32_t>(offsetof(NGUITools_t1206951095_StaticFields, ___mSides_5)); }
	inline Vector3U5BU5D_t1718750761* get_mSides_5() const { return ___mSides_5; }
	inline Vector3U5BU5D_t1718750761** get_address_of_mSides_5() { return &___mSides_5; }
	inline void set_mSides_5(Vector3U5BU5D_t1718750761* value)
	{
		___mSides_5 = value;
		Il2CppCodeGenWriteBarrier(&___mSides_5, value);
	}

	inline static int32_t get_offset_of_keys_6() { return static_cast<int32_t>(offsetof(NGUITools_t1206951095_StaticFields, ___keys_6)); }
	inline KeyCodeU5BU5D_t2223234056* get_keys_6() const { return ___keys_6; }
	inline KeyCodeU5BU5D_t2223234056** get_address_of_keys_6() { return &___keys_6; }
	inline void set_keys_6(KeyCodeU5BU5D_t2223234056* value)
	{
		___keys_6 = value;
		Il2CppCodeGenWriteBarrier(&___keys_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_7() { return static_cast<int32_t>(offsetof(NGUITools_t1206951095_StaticFields, ___U3CU3Ef__mgU24cache0_7)); }
	inline Comparison_1_t3313453104 * get_U3CU3Ef__mgU24cache0_7() const { return ___U3CU3Ef__mgU24cache0_7; }
	inline Comparison_1_t3313453104 ** get_address_of_U3CU3Ef__mgU24cache0_7() { return &___U3CU3Ef__mgU24cache0_7; }
	inline void set_U3CU3Ef__mgU24cache0_7(Comparison_1_t3313453104 * value)
	{
		___U3CU3Ef__mgU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_8() { return static_cast<int32_t>(offsetof(NGUITools_t1206951095_StaticFields, ___U3CU3Ef__mgU24cache1_8)); }
	inline Comparison_1_t1491403520 * get_U3CU3Ef__mgU24cache1_8() const { return ___U3CU3Ef__mgU24cache1_8; }
	inline Comparison_1_t1491403520 ** get_address_of_U3CU3Ef__mgU24cache1_8() { return &___U3CU3Ef__mgU24cache1_8; }
	inline void set_U3CU3Ef__mgU24cache1_8(Comparison_1_t1491403520 * value)
	{
		___U3CU3Ef__mgU24cache1_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache1_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
