﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_TimeSpan881159249.h"

// System.Collections.ObjectModel.Collection`1<System.Type>
struct Collection_1_t1428300678;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.LocalServiceSecuritySettings
struct  LocalServiceSecuritySettings_t2374387342  : public Il2CppObject
{
public:
	// System.TimeSpan System.ServiceModel.Channels.LocalServiceSecuritySettings::cookie_lifetime
	TimeSpan_t881159249  ___cookie_lifetime_0;
	// System.Collections.ObjectModel.Collection`1<System.Type> System.ServiceModel.Channels.LocalServiceSecuritySettings::claim_types
	Collection_1_t1428300678 * ___claim_types_1;

public:
	inline static int32_t get_offset_of_cookie_lifetime_0() { return static_cast<int32_t>(offsetof(LocalServiceSecuritySettings_t2374387342, ___cookie_lifetime_0)); }
	inline TimeSpan_t881159249  get_cookie_lifetime_0() const { return ___cookie_lifetime_0; }
	inline TimeSpan_t881159249 * get_address_of_cookie_lifetime_0() { return &___cookie_lifetime_0; }
	inline void set_cookie_lifetime_0(TimeSpan_t881159249  value)
	{
		___cookie_lifetime_0 = value;
	}

	inline static int32_t get_offset_of_claim_types_1() { return static_cast<int32_t>(offsetof(LocalServiceSecuritySettings_t2374387342, ___claim_types_1)); }
	inline Collection_1_t1428300678 * get_claim_types_1() const { return ___claim_types_1; }
	inline Collection_1_t1428300678 ** get_address_of_claim_types_1() { return &___claim_types_1; }
	inline void set_claim_types_1(Collection_1_t1428300678 * value)
	{
		___claim_types_1 = value;
		Il2CppCodeGenWriteBarrier(&___claim_types_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
