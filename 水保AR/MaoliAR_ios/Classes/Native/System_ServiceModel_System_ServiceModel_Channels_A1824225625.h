﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_H3141234096.h"

// System.ServiceModel.Channels.AspNetReplyChannel
struct AspNetReplyChannel_t2159115947;
// System.Web.HttpContext
struct HttpContext_t1969259010;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.AspNetRequestContext
struct  AspNetRequestContext_t1824225625  : public HttpRequestContextBase_t3141234096
{
public:
	// System.ServiceModel.Channels.AspNetReplyChannel System.ServiceModel.Channels.AspNetRequestContext::channel
	AspNetReplyChannel_t2159115947 * ___channel_2;
	// System.Web.HttpContext System.ServiceModel.Channels.AspNetRequestContext::ctx
	HttpContext_t1969259010 * ___ctx_3;

public:
	inline static int32_t get_offset_of_channel_2() { return static_cast<int32_t>(offsetof(AspNetRequestContext_t1824225625, ___channel_2)); }
	inline AspNetReplyChannel_t2159115947 * get_channel_2() const { return ___channel_2; }
	inline AspNetReplyChannel_t2159115947 ** get_address_of_channel_2() { return &___channel_2; }
	inline void set_channel_2(AspNetReplyChannel_t2159115947 * value)
	{
		___channel_2 = value;
		Il2CppCodeGenWriteBarrier(&___channel_2, value);
	}

	inline static int32_t get_offset_of_ctx_3() { return static_cast<int32_t>(offsetof(AspNetRequestContext_t1824225625, ___ctx_3)); }
	inline HttpContext_t1969259010 * get_ctx_3() const { return ___ctx_3; }
	inline HttpContext_t1969259010 ** get_address_of_ctx_3() { return &___ctx_3; }
	inline void set_ctx_3(HttpContext_t1969259010 * value)
	{
		___ctx_3 = value;
		Il2CppCodeGenWriteBarrier(&___ctx_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
