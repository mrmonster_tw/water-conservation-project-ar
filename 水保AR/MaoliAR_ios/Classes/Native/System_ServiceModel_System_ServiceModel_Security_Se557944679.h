﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Selectors202133775.h"

// System.ServiceModel.Description.ServiceCredentials
struct ServiceCredentials_t3431196922;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.ServiceCredentialsSecurityTokenManager
struct  ServiceCredentialsSecurityTokenManager_t557944679  : public SecurityTokenManager_t202133775
{
public:
	// System.ServiceModel.Description.ServiceCredentials System.ServiceModel.Security.ServiceCredentialsSecurityTokenManager::credentials
	ServiceCredentials_t3431196922 * ___credentials_0;

public:
	inline static int32_t get_offset_of_credentials_0() { return static_cast<int32_t>(offsetof(ServiceCredentialsSecurityTokenManager_t557944679, ___credentials_0)); }
	inline ServiceCredentials_t3431196922 * get_credentials_0() const { return ___credentials_0; }
	inline ServiceCredentials_t3431196922 ** get_address_of_credentials_0() { return &___credentials_0; }
	inline void set_credentials_0(ServiceCredentials_t3431196922 * value)
	{
		___credentials_0 = value;
		Il2CppCodeGenWriteBarrier(&___credentials_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
