﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Operations_XslAvt_AvtPart3412049665.h"

// System.Xml.XPath.XPathExpression
struct XPathExpression_t1723793351;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslAvt/XPathAvtPart
struct  XPathAvtPart_t3066905968  : public AvtPart_t3412049665
{
public:
	// System.Xml.XPath.XPathExpression Mono.Xml.Xsl.Operations.XslAvt/XPathAvtPart::expr
	XPathExpression_t1723793351 * ___expr_0;

public:
	inline static int32_t get_offset_of_expr_0() { return static_cast<int32_t>(offsetof(XPathAvtPart_t3066905968, ___expr_0)); }
	inline XPathExpression_t1723793351 * get_expr_0() const { return ___expr_0; }
	inline XPathExpression_t1723793351 ** get_address_of_expr_0() { return &___expr_0; }
	inline void set_expr_0(XPathExpression_t1723793351 * value)
	{
		___expr_0 = value;
		Il2CppCodeGenWriteBarrier(&___expr_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
