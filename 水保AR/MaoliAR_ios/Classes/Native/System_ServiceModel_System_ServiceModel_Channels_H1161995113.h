﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_H2392894562.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.HttpsTransportBindingElement
struct  HttpsTransportBindingElement_t1161995113  : public HttpTransportBindingElement_t2392894562
{
public:
	// System.Boolean System.ServiceModel.Channels.HttpsTransportBindingElement::req_cli_cert
	bool ___req_cli_cert_16;

public:
	inline static int32_t get_offset_of_req_cli_cert_16() { return static_cast<int32_t>(offsetof(HttpsTransportBindingElement_t1161995113, ___req_cli_cert_16)); }
	inline bool get_req_cli_cert_16() const { return ___req_cli_cert_16; }
	inline bool* get_address_of_req_cli_cert_16() { return &___req_cli_cert_16; }
	inline void set_req_cli_cert_16(bool value)
	{
		___req_cli_cert_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
