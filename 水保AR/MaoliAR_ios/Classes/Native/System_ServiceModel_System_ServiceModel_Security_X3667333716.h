﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_System_ServiceModel_Security_X3346525935.h"
#include "System_System_Security_Cryptography_X509Certificat2571829933.h"
#include "System_System_Security_Cryptography_X509Certificat2864310644.h"

// System.IdentityModel.Selectors.X509CertificateValidator
struct X509CertificateValidator_t1375267704;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.X509ClientCertificateAuthentication
struct  X509ClientCertificateAuthentication_t3667333716  : public Il2CppObject
{
public:
	// System.ServiceModel.Security.X509CertificateValidationMode System.ServiceModel.Security.X509ClientCertificateAuthentication::validation_mode
	int32_t ___validation_mode_0;
	// System.IdentityModel.Selectors.X509CertificateValidator System.ServiceModel.Security.X509ClientCertificateAuthentication::custom_validator
	X509CertificateValidator_t1375267704 * ___custom_validator_1;
	// System.Boolean System.ServiceModel.Security.X509ClientCertificateAuthentication::include_windows_groups
	bool ___include_windows_groups_2;
	// System.Security.Cryptography.X509Certificates.X509RevocationMode System.ServiceModel.Security.X509ClientCertificateAuthentication::revocation_mode
	int32_t ___revocation_mode_3;
	// System.Security.Cryptography.X509Certificates.StoreLocation System.ServiceModel.Security.X509ClientCertificateAuthentication::trusted_store_loc
	int32_t ___trusted_store_loc_4;

public:
	inline static int32_t get_offset_of_validation_mode_0() { return static_cast<int32_t>(offsetof(X509ClientCertificateAuthentication_t3667333716, ___validation_mode_0)); }
	inline int32_t get_validation_mode_0() const { return ___validation_mode_0; }
	inline int32_t* get_address_of_validation_mode_0() { return &___validation_mode_0; }
	inline void set_validation_mode_0(int32_t value)
	{
		___validation_mode_0 = value;
	}

	inline static int32_t get_offset_of_custom_validator_1() { return static_cast<int32_t>(offsetof(X509ClientCertificateAuthentication_t3667333716, ___custom_validator_1)); }
	inline X509CertificateValidator_t1375267704 * get_custom_validator_1() const { return ___custom_validator_1; }
	inline X509CertificateValidator_t1375267704 ** get_address_of_custom_validator_1() { return &___custom_validator_1; }
	inline void set_custom_validator_1(X509CertificateValidator_t1375267704 * value)
	{
		___custom_validator_1 = value;
		Il2CppCodeGenWriteBarrier(&___custom_validator_1, value);
	}

	inline static int32_t get_offset_of_include_windows_groups_2() { return static_cast<int32_t>(offsetof(X509ClientCertificateAuthentication_t3667333716, ___include_windows_groups_2)); }
	inline bool get_include_windows_groups_2() const { return ___include_windows_groups_2; }
	inline bool* get_address_of_include_windows_groups_2() { return &___include_windows_groups_2; }
	inline void set_include_windows_groups_2(bool value)
	{
		___include_windows_groups_2 = value;
	}

	inline static int32_t get_offset_of_revocation_mode_3() { return static_cast<int32_t>(offsetof(X509ClientCertificateAuthentication_t3667333716, ___revocation_mode_3)); }
	inline int32_t get_revocation_mode_3() const { return ___revocation_mode_3; }
	inline int32_t* get_address_of_revocation_mode_3() { return &___revocation_mode_3; }
	inline void set_revocation_mode_3(int32_t value)
	{
		___revocation_mode_3 = value;
	}

	inline static int32_t get_offset_of_trusted_store_loc_4() { return static_cast<int32_t>(offsetof(X509ClientCertificateAuthentication_t3667333716, ___trusted_store_loc_4)); }
	inline int32_t get_trusted_store_loc_4() const { return ___trusted_store_loc_4; }
	inline int32_t* get_address_of_trusted_store_loc_4() { return &___trusted_store_loc_4; }
	inline void set_trusted_store_loc_4(int32_t value)
	{
		___trusted_store_loc_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
