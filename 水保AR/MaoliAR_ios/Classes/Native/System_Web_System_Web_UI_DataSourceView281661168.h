﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ComponentModel.EventHandlerList
struct EventHandlerList_t1108123056;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.DataSourceView
struct  DataSourceView_t281661168  : public Il2CppObject
{
public:
	// System.ComponentModel.EventHandlerList System.Web.UI.DataSourceView::eventsList
	EventHandlerList_t1108123056 * ___eventsList_0;

public:
	inline static int32_t get_offset_of_eventsList_0() { return static_cast<int32_t>(offsetof(DataSourceView_t281661168, ___eventsList_0)); }
	inline EventHandlerList_t1108123056 * get_eventsList_0() const { return ___eventsList_0; }
	inline EventHandlerList_t1108123056 ** get_address_of_eventsList_0() { return &___eventsList_0; }
	inline void set_eventsList_0(EventHandlerList_t1108123056 * value)
	{
		___eventsList_0 = value;
		Il2CppCodeGenWriteBarrier(&___eventsList_0, value);
	}
};

struct DataSourceView_t281661168_StaticFields
{
public:
	// System.Object System.Web.UI.DataSourceView::EventDataSourceViewChanged
	Il2CppObject * ___EventDataSourceViewChanged_1;

public:
	inline static int32_t get_offset_of_EventDataSourceViewChanged_1() { return static_cast<int32_t>(offsetof(DataSourceView_t281661168_StaticFields, ___EventDataSourceViewChanged_1)); }
	inline Il2CppObject * get_EventDataSourceViewChanged_1() const { return ___EventDataSourceViewChanged_1; }
	inline Il2CppObject ** get_address_of_EventDataSourceViewChanged_1() { return &___EventDataSourceViewChanged_1; }
	inline void set_EventDataSourceViewChanged_1(Il2CppObject * value)
	{
		___EventDataSourceViewChanged_1 = value;
		Il2CppCodeGenWriteBarrier(&___EventDataSourceViewChanged_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
