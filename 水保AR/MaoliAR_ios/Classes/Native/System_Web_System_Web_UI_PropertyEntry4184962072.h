﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.PropertyEntry
struct  PropertyEntry_t4184962072  : public Il2CppObject
{
public:
	// System.Type System.Web.UI.PropertyEntry::type
	Type_t * ___type_0;
	// System.String System.Web.UI.PropertyEntry::name
	String_t* ___name_1;
	// System.Reflection.PropertyInfo System.Web.UI.PropertyEntry::pinfo
	PropertyInfo_t * ___pinfo_2;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(PropertyEntry_t4184962072, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier(&___type_0, value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(PropertyEntry_t4184962072, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_pinfo_2() { return static_cast<int32_t>(offsetof(PropertyEntry_t4184962072, ___pinfo_2)); }
	inline PropertyInfo_t * get_pinfo_2() const { return ___pinfo_2; }
	inline PropertyInfo_t ** get_address_of_pinfo_2() { return &___pinfo_2; }
	inline void set_pinfo_2(PropertyInfo_t * value)
	{
		___pinfo_2 = value;
		Il2CppCodeGenWriteBarrier(&___pinfo_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
