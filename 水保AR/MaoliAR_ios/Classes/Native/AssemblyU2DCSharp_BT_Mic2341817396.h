﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BT_Mic
struct  BT_Mic_t2341817396  : public Il2CppObject
{
public:
	// System.Boolean BT_Mic::SW
	bool ___SW_0;
	// System.String BT_Mic::MIC
	String_t* ___MIC_1;

public:
	inline static int32_t get_offset_of_SW_0() { return static_cast<int32_t>(offsetof(BT_Mic_t2341817396, ___SW_0)); }
	inline bool get_SW_0() const { return ___SW_0; }
	inline bool* get_address_of_SW_0() { return &___SW_0; }
	inline void set_SW_0(bool value)
	{
		___SW_0 = value;
	}

	inline static int32_t get_offset_of_MIC_1() { return static_cast<int32_t>(offsetof(BT_Mic_t2341817396, ___MIC_1)); }
	inline String_t* get_MIC_1() const { return ___MIC_1; }
	inline String_t** get_address_of_MIC_1() { return &___MIC_1; }
	inline void set_MIC_1(String_t* value)
	{
		___MIC_1 = value;
		Il2CppCodeGenWriteBarrier(&___MIC_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
