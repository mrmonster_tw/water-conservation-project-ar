﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Xml.Serialization.XmlTypeMapping
struct XmlTypeMapping_t3915382669;
// System.Xml.Schema.XmlSchemaComplexType
struct XmlSchemaComplexType_t3740801802;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2760654312;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSchemaImporter/MapFixup
struct  MapFixup_t630237321  : public Il2CppObject
{
public:
	// System.Xml.Serialization.XmlTypeMapping System.Xml.Serialization.XmlSchemaImporter/MapFixup::Map
	XmlTypeMapping_t3915382669 * ___Map_0;
	// System.Xml.Schema.XmlSchemaComplexType System.Xml.Serialization.XmlSchemaImporter/MapFixup::SchemaType
	XmlSchemaComplexType_t3740801802 * ___SchemaType_1;
	// System.Xml.XmlQualifiedName System.Xml.Serialization.XmlSchemaImporter/MapFixup::TypeName
	XmlQualifiedName_t2760654312 * ___TypeName_2;

public:
	inline static int32_t get_offset_of_Map_0() { return static_cast<int32_t>(offsetof(MapFixup_t630237321, ___Map_0)); }
	inline XmlTypeMapping_t3915382669 * get_Map_0() const { return ___Map_0; }
	inline XmlTypeMapping_t3915382669 ** get_address_of_Map_0() { return &___Map_0; }
	inline void set_Map_0(XmlTypeMapping_t3915382669 * value)
	{
		___Map_0 = value;
		Il2CppCodeGenWriteBarrier(&___Map_0, value);
	}

	inline static int32_t get_offset_of_SchemaType_1() { return static_cast<int32_t>(offsetof(MapFixup_t630237321, ___SchemaType_1)); }
	inline XmlSchemaComplexType_t3740801802 * get_SchemaType_1() const { return ___SchemaType_1; }
	inline XmlSchemaComplexType_t3740801802 ** get_address_of_SchemaType_1() { return &___SchemaType_1; }
	inline void set_SchemaType_1(XmlSchemaComplexType_t3740801802 * value)
	{
		___SchemaType_1 = value;
		Il2CppCodeGenWriteBarrier(&___SchemaType_1, value);
	}

	inline static int32_t get_offset_of_TypeName_2() { return static_cast<int32_t>(offsetof(MapFixup_t630237321, ___TypeName_2)); }
	inline XmlQualifiedName_t2760654312 * get_TypeName_2() const { return ___TypeName_2; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_TypeName_2() { return &___TypeName_2; }
	inline void set_TypeName_2(XmlQualifiedName_t2760654312 * value)
	{
		___TypeName_2 = value;
		Il2CppCodeGenWriteBarrier(&___TypeName_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
