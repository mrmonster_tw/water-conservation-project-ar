﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Channels.PeerDuplexChannel/RemotePeerConnection
struct RemotePeerConnection_t1863582616;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.PeerDuplexChannel/<CreateInnerClient>c__AnonStoreyF
struct  U3CCreateInnerClientU3Ec__AnonStoreyF_t507262574  : public Il2CppObject
{
public:
	// System.ServiceModel.Channels.PeerDuplexChannel/RemotePeerConnection System.ServiceModel.Channels.PeerDuplexChannel/<CreateInnerClient>c__AnonStoreyF::conn
	RemotePeerConnection_t1863582616 * ___conn_0;

public:
	inline static int32_t get_offset_of_conn_0() { return static_cast<int32_t>(offsetof(U3CCreateInnerClientU3Ec__AnonStoreyF_t507262574, ___conn_0)); }
	inline RemotePeerConnection_t1863582616 * get_conn_0() const { return ___conn_0; }
	inline RemotePeerConnection_t1863582616 ** get_address_of_conn_0() { return &___conn_0; }
	inline void set_conn_0(RemotePeerConnection_t1863582616 * value)
	{
		___conn_0 = value;
		Il2CppCodeGenWriteBarrier(&___conn_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
