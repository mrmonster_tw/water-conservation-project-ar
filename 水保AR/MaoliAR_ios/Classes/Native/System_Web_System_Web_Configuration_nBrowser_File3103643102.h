﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Xml.XmlDocument
struct XmlDocument_t2837193595;
// System.Web.Configuration.nBrowser.Node[]
struct NodeU5BU5D_t386454267;
// System.Collections.Specialized.ListDictionary
struct ListDictionary_t1624492310;
// System.Collections.Generic.List`1<System.Web.Configuration.nBrowser.Node>
struct List_1_t3837150468;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.nBrowser.File
struct  File_t3103643102  : public Il2CppObject
{
public:
	// System.Xml.XmlDocument System.Web.Configuration.nBrowser.File::BrowserFile
	XmlDocument_t2837193595 * ___BrowserFile_0;
	// System.Web.Configuration.nBrowser.Node[] System.Web.Configuration.nBrowser.File::Nodes
	NodeU5BU5D_t386454267* ___Nodes_1;
	// System.Collections.Specialized.ListDictionary System.Web.Configuration.nBrowser.File::Lookup
	ListDictionary_t1624492310 * ___Lookup_2;
	// System.Collections.Specialized.ListDictionary System.Web.Configuration.nBrowser.File::DefaultLookup
	ListDictionary_t1624492310 * ___DefaultLookup_3;
	// System.Collections.Generic.List`1<System.Web.Configuration.nBrowser.Node> System.Web.Configuration.nBrowser.File::RefNodes
	List_1_t3837150468 * ___RefNodes_4;
	// System.String System.Web.Configuration.nBrowser.File::pFileName
	String_t* ___pFileName_5;

public:
	inline static int32_t get_offset_of_BrowserFile_0() { return static_cast<int32_t>(offsetof(File_t3103643102, ___BrowserFile_0)); }
	inline XmlDocument_t2837193595 * get_BrowserFile_0() const { return ___BrowserFile_0; }
	inline XmlDocument_t2837193595 ** get_address_of_BrowserFile_0() { return &___BrowserFile_0; }
	inline void set_BrowserFile_0(XmlDocument_t2837193595 * value)
	{
		___BrowserFile_0 = value;
		Il2CppCodeGenWriteBarrier(&___BrowserFile_0, value);
	}

	inline static int32_t get_offset_of_Nodes_1() { return static_cast<int32_t>(offsetof(File_t3103643102, ___Nodes_1)); }
	inline NodeU5BU5D_t386454267* get_Nodes_1() const { return ___Nodes_1; }
	inline NodeU5BU5D_t386454267** get_address_of_Nodes_1() { return &___Nodes_1; }
	inline void set_Nodes_1(NodeU5BU5D_t386454267* value)
	{
		___Nodes_1 = value;
		Il2CppCodeGenWriteBarrier(&___Nodes_1, value);
	}

	inline static int32_t get_offset_of_Lookup_2() { return static_cast<int32_t>(offsetof(File_t3103643102, ___Lookup_2)); }
	inline ListDictionary_t1624492310 * get_Lookup_2() const { return ___Lookup_2; }
	inline ListDictionary_t1624492310 ** get_address_of_Lookup_2() { return &___Lookup_2; }
	inline void set_Lookup_2(ListDictionary_t1624492310 * value)
	{
		___Lookup_2 = value;
		Il2CppCodeGenWriteBarrier(&___Lookup_2, value);
	}

	inline static int32_t get_offset_of_DefaultLookup_3() { return static_cast<int32_t>(offsetof(File_t3103643102, ___DefaultLookup_3)); }
	inline ListDictionary_t1624492310 * get_DefaultLookup_3() const { return ___DefaultLookup_3; }
	inline ListDictionary_t1624492310 ** get_address_of_DefaultLookup_3() { return &___DefaultLookup_3; }
	inline void set_DefaultLookup_3(ListDictionary_t1624492310 * value)
	{
		___DefaultLookup_3 = value;
		Il2CppCodeGenWriteBarrier(&___DefaultLookup_3, value);
	}

	inline static int32_t get_offset_of_RefNodes_4() { return static_cast<int32_t>(offsetof(File_t3103643102, ___RefNodes_4)); }
	inline List_1_t3837150468 * get_RefNodes_4() const { return ___RefNodes_4; }
	inline List_1_t3837150468 ** get_address_of_RefNodes_4() { return &___RefNodes_4; }
	inline void set_RefNodes_4(List_1_t3837150468 * value)
	{
		___RefNodes_4 = value;
		Il2CppCodeGenWriteBarrier(&___RefNodes_4, value);
	}

	inline static int32_t get_offset_of_pFileName_5() { return static_cast<int32_t>(offsetof(File_t3103643102, ___pFileName_5)); }
	inline String_t* get_pFileName_5() const { return ___pFileName_5; }
	inline String_t** get_address_of_pFileName_5() { return &___pFileName_5; }
	inline void set_pFileName_5(String_t* value)
	{
		___pFileName_5 = value;
		Il2CppCodeGenWriteBarrier(&___pFileName_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
