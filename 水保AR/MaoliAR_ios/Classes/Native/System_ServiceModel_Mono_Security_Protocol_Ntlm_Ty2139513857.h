﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_Mono_Security_Protocol_Ntlm_Mes422981883.h"

// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Ntlm.Type3Message
struct  Type3Message_t2139513857  : public MessageBase_t422981883
{
public:
	// System.Byte[] Mono.Security.Protocol.Ntlm.Type3Message::_challenge
	ByteU5BU5D_t4116647657* ____challenge_6;
	// System.String Mono.Security.Protocol.Ntlm.Type3Message::_host
	String_t* ____host_7;
	// System.String Mono.Security.Protocol.Ntlm.Type3Message::_domain
	String_t* ____domain_8;
	// System.String Mono.Security.Protocol.Ntlm.Type3Message::_username
	String_t* ____username_9;
	// System.String Mono.Security.Protocol.Ntlm.Type3Message::_password
	String_t* ____password_10;
	// System.Byte[] Mono.Security.Protocol.Ntlm.Type3Message::_lm
	ByteU5BU5D_t4116647657* ____lm_11;
	// System.Byte[] Mono.Security.Protocol.Ntlm.Type3Message::_nt
	ByteU5BU5D_t4116647657* ____nt_12;
	// System.Byte[] Mono.Security.Protocol.Ntlm.Type3Message::_nonce
	ByteU5BU5D_t4116647657* ____nonce_13;

public:
	inline static int32_t get_offset_of__challenge_6() { return static_cast<int32_t>(offsetof(Type3Message_t2139513857, ____challenge_6)); }
	inline ByteU5BU5D_t4116647657* get__challenge_6() const { return ____challenge_6; }
	inline ByteU5BU5D_t4116647657** get_address_of__challenge_6() { return &____challenge_6; }
	inline void set__challenge_6(ByteU5BU5D_t4116647657* value)
	{
		____challenge_6 = value;
		Il2CppCodeGenWriteBarrier(&____challenge_6, value);
	}

	inline static int32_t get_offset_of__host_7() { return static_cast<int32_t>(offsetof(Type3Message_t2139513857, ____host_7)); }
	inline String_t* get__host_7() const { return ____host_7; }
	inline String_t** get_address_of__host_7() { return &____host_7; }
	inline void set__host_7(String_t* value)
	{
		____host_7 = value;
		Il2CppCodeGenWriteBarrier(&____host_7, value);
	}

	inline static int32_t get_offset_of__domain_8() { return static_cast<int32_t>(offsetof(Type3Message_t2139513857, ____domain_8)); }
	inline String_t* get__domain_8() const { return ____domain_8; }
	inline String_t** get_address_of__domain_8() { return &____domain_8; }
	inline void set__domain_8(String_t* value)
	{
		____domain_8 = value;
		Il2CppCodeGenWriteBarrier(&____domain_8, value);
	}

	inline static int32_t get_offset_of__username_9() { return static_cast<int32_t>(offsetof(Type3Message_t2139513857, ____username_9)); }
	inline String_t* get__username_9() const { return ____username_9; }
	inline String_t** get_address_of__username_9() { return &____username_9; }
	inline void set__username_9(String_t* value)
	{
		____username_9 = value;
		Il2CppCodeGenWriteBarrier(&____username_9, value);
	}

	inline static int32_t get_offset_of__password_10() { return static_cast<int32_t>(offsetof(Type3Message_t2139513857, ____password_10)); }
	inline String_t* get__password_10() const { return ____password_10; }
	inline String_t** get_address_of__password_10() { return &____password_10; }
	inline void set__password_10(String_t* value)
	{
		____password_10 = value;
		Il2CppCodeGenWriteBarrier(&____password_10, value);
	}

	inline static int32_t get_offset_of__lm_11() { return static_cast<int32_t>(offsetof(Type3Message_t2139513857, ____lm_11)); }
	inline ByteU5BU5D_t4116647657* get__lm_11() const { return ____lm_11; }
	inline ByteU5BU5D_t4116647657** get_address_of__lm_11() { return &____lm_11; }
	inline void set__lm_11(ByteU5BU5D_t4116647657* value)
	{
		____lm_11 = value;
		Il2CppCodeGenWriteBarrier(&____lm_11, value);
	}

	inline static int32_t get_offset_of__nt_12() { return static_cast<int32_t>(offsetof(Type3Message_t2139513857, ____nt_12)); }
	inline ByteU5BU5D_t4116647657* get__nt_12() const { return ____nt_12; }
	inline ByteU5BU5D_t4116647657** get_address_of__nt_12() { return &____nt_12; }
	inline void set__nt_12(ByteU5BU5D_t4116647657* value)
	{
		____nt_12 = value;
		Il2CppCodeGenWriteBarrier(&____nt_12, value);
	}

	inline static int32_t get_offset_of__nonce_13() { return static_cast<int32_t>(offsetof(Type3Message_t2139513857, ____nonce_13)); }
	inline ByteU5BU5D_t4116647657* get__nonce_13() const { return ____nonce_13; }
	inline ByteU5BU5D_t4116647657** get_address_of__nonce_13() { return &____nonce_13; }
	inline void set__nonce_13(ByteU5BU5D_t4116647657* value)
	{
		____nonce_13 = value;
		Il2CppCodeGenWriteBarrier(&____nonce_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
