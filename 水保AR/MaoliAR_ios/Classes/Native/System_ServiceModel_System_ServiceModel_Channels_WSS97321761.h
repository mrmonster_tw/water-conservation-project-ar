﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Me831925271.h"

// System.IdentityModel.Selectors.SecurityTokenSerializer
struct SecurityTokenSerializer_t972969391;
// System.Collections.ObjectModel.Collection`1<System.Object>
struct Collection_1_t2024462082;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.WSSecurityMessageHeader
struct  WSSecurityMessageHeader_t97321761  : public MessageHeader_t831925271
{
public:
	// System.IdentityModel.Selectors.SecurityTokenSerializer System.ServiceModel.Channels.WSSecurityMessageHeader::serializer
	SecurityTokenSerializer_t972969391 * ___serializer_6;
	// System.Collections.ObjectModel.Collection`1<System.Object> System.ServiceModel.Channels.WSSecurityMessageHeader::contents
	Collection_1_t2024462082 * ___contents_7;

public:
	inline static int32_t get_offset_of_serializer_6() { return static_cast<int32_t>(offsetof(WSSecurityMessageHeader_t97321761, ___serializer_6)); }
	inline SecurityTokenSerializer_t972969391 * get_serializer_6() const { return ___serializer_6; }
	inline SecurityTokenSerializer_t972969391 ** get_address_of_serializer_6() { return &___serializer_6; }
	inline void set_serializer_6(SecurityTokenSerializer_t972969391 * value)
	{
		___serializer_6 = value;
		Il2CppCodeGenWriteBarrier(&___serializer_6, value);
	}

	inline static int32_t get_offset_of_contents_7() { return static_cast<int32_t>(offsetof(WSSecurityMessageHeader_t97321761, ___contents_7)); }
	inline Collection_1_t2024462082 * get_contents_7() const { return ___contents_7; }
	inline Collection_1_t2024462082 ** get_address_of_contents_7() { return &___contents_7; }
	inline void set_contents_7(Collection_1_t2024462082 * value)
	{
		___contents_7 = value;
		Il2CppCodeGenWriteBarrier(&___contents_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
