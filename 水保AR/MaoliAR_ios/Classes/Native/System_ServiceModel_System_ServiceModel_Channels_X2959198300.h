﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Me869514973.h"

// System.ServiceModel.Channels.MessageVersion
struct MessageVersion_t1958933598;
// System.Xml.XmlDictionaryReader
struct XmlDictionaryReader_t1044334689;
// System.ServiceModel.Channels.MessageHeaders
struct MessageHeaders_t4050072634;
// System.ServiceModel.Channels.MessageProperties
struct MessageProperties_t4101341573;
// System.Char[]
struct CharU5BU5D_t3528271667;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.XmlReaderMessage
struct  XmlReaderMessage_t2959198300  : public Message_t869514973
{
public:
	// System.ServiceModel.Channels.MessageVersion System.ServiceModel.Channels.XmlReaderMessage::version
	MessageVersion_t1958933598 * ___version_3;
	// System.Xml.XmlDictionaryReader System.ServiceModel.Channels.XmlReaderMessage::reader
	XmlDictionaryReader_t1044334689 * ___reader_4;
	// System.ServiceModel.Channels.MessageHeaders System.ServiceModel.Channels.XmlReaderMessage::headers
	MessageHeaders_t4050072634 * ___headers_5;
	// System.ServiceModel.Channels.MessageProperties System.ServiceModel.Channels.XmlReaderMessage::properties
	MessageProperties_t4101341573 * ___properties_6;
	// System.Boolean System.ServiceModel.Channels.XmlReaderMessage::is_empty
	bool ___is_empty_7;
	// System.Boolean System.ServiceModel.Channels.XmlReaderMessage::is_fault
	bool ___is_fault_8;
	// System.Boolean System.ServiceModel.Channels.XmlReaderMessage::body_started
	bool ___body_started_9;
	// System.Boolean System.ServiceModel.Channels.XmlReaderMessage::body_consumed
	bool ___body_consumed_10;
	// System.Int32 System.ServiceModel.Channels.XmlReaderMessage::max_headers
	int32_t ___max_headers_11;

public:
	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(XmlReaderMessage_t2959198300, ___version_3)); }
	inline MessageVersion_t1958933598 * get_version_3() const { return ___version_3; }
	inline MessageVersion_t1958933598 ** get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(MessageVersion_t1958933598 * value)
	{
		___version_3 = value;
		Il2CppCodeGenWriteBarrier(&___version_3, value);
	}

	inline static int32_t get_offset_of_reader_4() { return static_cast<int32_t>(offsetof(XmlReaderMessage_t2959198300, ___reader_4)); }
	inline XmlDictionaryReader_t1044334689 * get_reader_4() const { return ___reader_4; }
	inline XmlDictionaryReader_t1044334689 ** get_address_of_reader_4() { return &___reader_4; }
	inline void set_reader_4(XmlDictionaryReader_t1044334689 * value)
	{
		___reader_4 = value;
		Il2CppCodeGenWriteBarrier(&___reader_4, value);
	}

	inline static int32_t get_offset_of_headers_5() { return static_cast<int32_t>(offsetof(XmlReaderMessage_t2959198300, ___headers_5)); }
	inline MessageHeaders_t4050072634 * get_headers_5() const { return ___headers_5; }
	inline MessageHeaders_t4050072634 ** get_address_of_headers_5() { return &___headers_5; }
	inline void set_headers_5(MessageHeaders_t4050072634 * value)
	{
		___headers_5 = value;
		Il2CppCodeGenWriteBarrier(&___headers_5, value);
	}

	inline static int32_t get_offset_of_properties_6() { return static_cast<int32_t>(offsetof(XmlReaderMessage_t2959198300, ___properties_6)); }
	inline MessageProperties_t4101341573 * get_properties_6() const { return ___properties_6; }
	inline MessageProperties_t4101341573 ** get_address_of_properties_6() { return &___properties_6; }
	inline void set_properties_6(MessageProperties_t4101341573 * value)
	{
		___properties_6 = value;
		Il2CppCodeGenWriteBarrier(&___properties_6, value);
	}

	inline static int32_t get_offset_of_is_empty_7() { return static_cast<int32_t>(offsetof(XmlReaderMessage_t2959198300, ___is_empty_7)); }
	inline bool get_is_empty_7() const { return ___is_empty_7; }
	inline bool* get_address_of_is_empty_7() { return &___is_empty_7; }
	inline void set_is_empty_7(bool value)
	{
		___is_empty_7 = value;
	}

	inline static int32_t get_offset_of_is_fault_8() { return static_cast<int32_t>(offsetof(XmlReaderMessage_t2959198300, ___is_fault_8)); }
	inline bool get_is_fault_8() const { return ___is_fault_8; }
	inline bool* get_address_of_is_fault_8() { return &___is_fault_8; }
	inline void set_is_fault_8(bool value)
	{
		___is_fault_8 = value;
	}

	inline static int32_t get_offset_of_body_started_9() { return static_cast<int32_t>(offsetof(XmlReaderMessage_t2959198300, ___body_started_9)); }
	inline bool get_body_started_9() const { return ___body_started_9; }
	inline bool* get_address_of_body_started_9() { return &___body_started_9; }
	inline void set_body_started_9(bool value)
	{
		___body_started_9 = value;
	}

	inline static int32_t get_offset_of_body_consumed_10() { return static_cast<int32_t>(offsetof(XmlReaderMessage_t2959198300, ___body_consumed_10)); }
	inline bool get_body_consumed_10() const { return ___body_consumed_10; }
	inline bool* get_address_of_body_consumed_10() { return &___body_consumed_10; }
	inline void set_body_consumed_10(bool value)
	{
		___body_consumed_10 = value;
	}

	inline static int32_t get_offset_of_max_headers_11() { return static_cast<int32_t>(offsetof(XmlReaderMessage_t2959198300, ___max_headers_11)); }
	inline int32_t get_max_headers_11() const { return ___max_headers_11; }
	inline int32_t* get_address_of_max_headers_11() { return &___max_headers_11; }
	inline void set_max_headers_11(int32_t value)
	{
		___max_headers_11 = value;
	}
};

struct XmlReaderMessage_t2959198300_StaticFields
{
public:
	// System.Char[] System.ServiceModel.Channels.XmlReaderMessage::whitespaceChars
	CharU5BU5D_t3528271667* ___whitespaceChars_12;

public:
	inline static int32_t get_offset_of_whitespaceChars_12() { return static_cast<int32_t>(offsetof(XmlReaderMessage_t2959198300_StaticFields, ___whitespaceChars_12)); }
	inline CharU5BU5D_t3528271667* get_whitespaceChars_12() const { return ___whitespaceChars_12; }
	inline CharU5BU5D_t3528271667** get_address_of_whitespaceChars_12() { return &___whitespaceChars_12; }
	inline void set_whitespaceChars_12(CharU5BU5D_t3528271667* value)
	{
		___whitespaceChars_12 = value;
		Il2CppCodeGenWriteBarrier(&___whitespaceChars_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
