﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_S1438243711.h"

// System.ServiceModel.Channels.AsymmetricSecurityBindingElement
struct AsymmetricSecurityBindingElement_t2673164393;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.AsymmetricSecurityCapabilities
struct  AsymmetricSecurityCapabilities_t888703423  : public SecurityCapabilities_t1438243711
{
public:
	// System.ServiceModel.Channels.AsymmetricSecurityBindingElement System.ServiceModel.Channels.AsymmetricSecurityCapabilities::element
	AsymmetricSecurityBindingElement_t2673164393 * ___element_0;

public:
	inline static int32_t get_offset_of_element_0() { return static_cast<int32_t>(offsetof(AsymmetricSecurityCapabilities_t888703423, ___element_0)); }
	inline AsymmetricSecurityBindingElement_t2673164393 * get_element_0() const { return ___element_0; }
	inline AsymmetricSecurityBindingElement_t2673164393 ** get_address_of_element_0() { return &___element_0; }
	inline void set_element_0(AsymmetricSecurityBindingElement_t2673164393 * value)
	{
		___element_0 = value;
		Il2CppCodeGenWriteBarrier(&___element_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
