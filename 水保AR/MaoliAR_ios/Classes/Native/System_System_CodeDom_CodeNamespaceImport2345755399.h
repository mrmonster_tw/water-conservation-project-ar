﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeObject3927604602.h"

// System.CodeDom.CodeLinePragma
struct CodeLinePragma_t3085002198;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeNamespaceImport
struct  CodeNamespaceImport_t2345755399  : public CodeObject_t3927604602
{
public:
	// System.CodeDom.CodeLinePragma System.CodeDom.CodeNamespaceImport::linePragma
	CodeLinePragma_t3085002198 * ___linePragma_1;
	// System.String System.CodeDom.CodeNamespaceImport::nameSpace
	String_t* ___nameSpace_2;

public:
	inline static int32_t get_offset_of_linePragma_1() { return static_cast<int32_t>(offsetof(CodeNamespaceImport_t2345755399, ___linePragma_1)); }
	inline CodeLinePragma_t3085002198 * get_linePragma_1() const { return ___linePragma_1; }
	inline CodeLinePragma_t3085002198 ** get_address_of_linePragma_1() { return &___linePragma_1; }
	inline void set_linePragma_1(CodeLinePragma_t3085002198 * value)
	{
		___linePragma_1 = value;
		Il2CppCodeGenWriteBarrier(&___linePragma_1, value);
	}

	inline static int32_t get_offset_of_nameSpace_2() { return static_cast<int32_t>(offsetof(CodeNamespaceImport_t2345755399, ___nameSpace_2)); }
	inline String_t* get_nameSpace_2() const { return ___nameSpace_2; }
	inline String_t** get_address_of_nameSpace_2() { return &___nameSpace_2; }
	inline void set_nameSpace_2(String_t* value)
	{
		___nameSpace_2 = value;
		Il2CppCodeGenWriteBarrier(&___nameSpace_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
