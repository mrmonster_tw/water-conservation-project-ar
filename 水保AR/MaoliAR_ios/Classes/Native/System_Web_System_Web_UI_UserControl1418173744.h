﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_TemplateControl1922940480.h"

// System.Web.UI.AttributeCollection
struct AttributeCollection_t3488369622;
// System.Web.UI.StateBag
struct StateBag_t282928164;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.UserControl
struct  UserControl_t1418173744  : public TemplateControl_t1922940480
{
public:
	// System.Boolean System.Web.UI.UserControl::initialized
	bool ___initialized_36;
	// System.Web.UI.AttributeCollection System.Web.UI.UserControl::attributes
	AttributeCollection_t3488369622 * ___attributes_37;
	// System.Web.UI.StateBag System.Web.UI.UserControl::attrBag
	StateBag_t282928164 * ___attrBag_38;

public:
	inline static int32_t get_offset_of_initialized_36() { return static_cast<int32_t>(offsetof(UserControl_t1418173744, ___initialized_36)); }
	inline bool get_initialized_36() const { return ___initialized_36; }
	inline bool* get_address_of_initialized_36() { return &___initialized_36; }
	inline void set_initialized_36(bool value)
	{
		___initialized_36 = value;
	}

	inline static int32_t get_offset_of_attributes_37() { return static_cast<int32_t>(offsetof(UserControl_t1418173744, ___attributes_37)); }
	inline AttributeCollection_t3488369622 * get_attributes_37() const { return ___attributes_37; }
	inline AttributeCollection_t3488369622 ** get_address_of_attributes_37() { return &___attributes_37; }
	inline void set_attributes_37(AttributeCollection_t3488369622 * value)
	{
		___attributes_37 = value;
		Il2CppCodeGenWriteBarrier(&___attributes_37, value);
	}

	inline static int32_t get_offset_of_attrBag_38() { return static_cast<int32_t>(offsetof(UserControl_t1418173744, ___attrBag_38)); }
	inline StateBag_t282928164 * get_attrBag_38() const { return ___attrBag_38; }
	inline StateBag_t282928164 ** get_address_of_attrBag_38() { return &___attrBag_38; }
	inline void set_attrBag_38(StateBag_t282928164 * value)
	{
		___attrBag_38 = value;
		Il2CppCodeGenWriteBarrier(&___attrBag_38, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
