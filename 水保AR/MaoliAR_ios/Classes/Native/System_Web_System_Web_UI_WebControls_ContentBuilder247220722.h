﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_TemplateBuilder3864847030.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.WebControls.ContentBuilderInternal
struct  ContentBuilderInternal_t247220722  : public TemplateBuilder_t3864847030
{
public:
	// System.String System.Web.UI.WebControls.ContentBuilderInternal::placeHolderID
	String_t* ___placeHolderID_32;

public:
	inline static int32_t get_offset_of_placeHolderID_32() { return static_cast<int32_t>(offsetof(ContentBuilderInternal_t247220722, ___placeHolderID_32)); }
	inline String_t* get_placeHolderID_32() const { return ___placeHolderID_32; }
	inline String_t** get_address_of_placeHolderID_32() { return &___placeHolderID_32; }
	inline void set_placeHolderID_32(String_t* value)
	{
		___placeHolderID_32 = value;
		Il2CppCodeGenWriteBarrier(&___placeHolderID_32, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
