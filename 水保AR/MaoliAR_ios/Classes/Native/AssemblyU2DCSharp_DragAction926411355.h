﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Vector33722313464.h"
#include "UnityEngine_UnityEngine_Vector22156229523.h"

// UnityEngine.RectTransform
struct RectTransform_t3704657025;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DragAction
struct  DragAction_t926411355  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform DragAction::item
	RectTransform_t3704657025 * ___item_2;
	// UnityEngine.Vector3 DragAction::StartPos
	Vector3_t3722313464  ___StartPos_3;
	// UnityEngine.Vector2 DragAction::clampRange
	Vector2_t2156229523  ___clampRange_4;
	// UnityEngine.Vector3 DragAction::nowPos
	Vector3_t3722313464  ___nowPos_5;

public:
	inline static int32_t get_offset_of_item_2() { return static_cast<int32_t>(offsetof(DragAction_t926411355, ___item_2)); }
	inline RectTransform_t3704657025 * get_item_2() const { return ___item_2; }
	inline RectTransform_t3704657025 ** get_address_of_item_2() { return &___item_2; }
	inline void set_item_2(RectTransform_t3704657025 * value)
	{
		___item_2 = value;
		Il2CppCodeGenWriteBarrier(&___item_2, value);
	}

	inline static int32_t get_offset_of_StartPos_3() { return static_cast<int32_t>(offsetof(DragAction_t926411355, ___StartPos_3)); }
	inline Vector3_t3722313464  get_StartPos_3() const { return ___StartPos_3; }
	inline Vector3_t3722313464 * get_address_of_StartPos_3() { return &___StartPos_3; }
	inline void set_StartPos_3(Vector3_t3722313464  value)
	{
		___StartPos_3 = value;
	}

	inline static int32_t get_offset_of_clampRange_4() { return static_cast<int32_t>(offsetof(DragAction_t926411355, ___clampRange_4)); }
	inline Vector2_t2156229523  get_clampRange_4() const { return ___clampRange_4; }
	inline Vector2_t2156229523 * get_address_of_clampRange_4() { return &___clampRange_4; }
	inline void set_clampRange_4(Vector2_t2156229523  value)
	{
		___clampRange_4 = value;
	}

	inline static int32_t get_offset_of_nowPos_5() { return static_cast<int32_t>(offsetof(DragAction_t926411355, ___nowPos_5)); }
	inline Vector3_t3722313464  get_nowPos_5() const { return ___nowPos_5; }
	inline Vector3_t3722313464 * get_address_of_nowPos_5() { return &___nowPos_5; }
	inline void set_nowPos_5(Vector3_t3722313464  value)
	{
		___nowPos_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
