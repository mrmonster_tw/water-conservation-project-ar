﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_Services_System_Web_Services_Descriptio2466402210.h"

// System.Web.Services.Description.Binding
struct Binding_t3952932040;
// System.Web.Services.Description.ServiceDescriptionFormatExtensionCollection
struct ServiceDescriptionFormatExtensionCollection_t2630153888;
// System.Web.Services.Description.FaultBindingCollection
struct FaultBindingCollection_t2785831862;
// System.Web.Services.Description.InputBinding
struct InputBinding_t3136796011;
// System.Web.Services.Description.OutputBinding
struct OutputBinding_t1318715061;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.OperationBinding
struct  OperationBinding_t314325322  : public NamedItem_t2466402210
{
public:
	// System.Web.Services.Description.Binding System.Web.Services.Description.OperationBinding::binding
	Binding_t3952932040 * ___binding_4;
	// System.Web.Services.Description.ServiceDescriptionFormatExtensionCollection System.Web.Services.Description.OperationBinding::extensions
	ServiceDescriptionFormatExtensionCollection_t2630153888 * ___extensions_5;
	// System.Web.Services.Description.FaultBindingCollection System.Web.Services.Description.OperationBinding::faults
	FaultBindingCollection_t2785831862 * ___faults_6;
	// System.Web.Services.Description.InputBinding System.Web.Services.Description.OperationBinding::input
	InputBinding_t3136796011 * ___input_7;
	// System.Web.Services.Description.OutputBinding System.Web.Services.Description.OperationBinding::output
	OutputBinding_t1318715061 * ___output_8;

public:
	inline static int32_t get_offset_of_binding_4() { return static_cast<int32_t>(offsetof(OperationBinding_t314325322, ___binding_4)); }
	inline Binding_t3952932040 * get_binding_4() const { return ___binding_4; }
	inline Binding_t3952932040 ** get_address_of_binding_4() { return &___binding_4; }
	inline void set_binding_4(Binding_t3952932040 * value)
	{
		___binding_4 = value;
		Il2CppCodeGenWriteBarrier(&___binding_4, value);
	}

	inline static int32_t get_offset_of_extensions_5() { return static_cast<int32_t>(offsetof(OperationBinding_t314325322, ___extensions_5)); }
	inline ServiceDescriptionFormatExtensionCollection_t2630153888 * get_extensions_5() const { return ___extensions_5; }
	inline ServiceDescriptionFormatExtensionCollection_t2630153888 ** get_address_of_extensions_5() { return &___extensions_5; }
	inline void set_extensions_5(ServiceDescriptionFormatExtensionCollection_t2630153888 * value)
	{
		___extensions_5 = value;
		Il2CppCodeGenWriteBarrier(&___extensions_5, value);
	}

	inline static int32_t get_offset_of_faults_6() { return static_cast<int32_t>(offsetof(OperationBinding_t314325322, ___faults_6)); }
	inline FaultBindingCollection_t2785831862 * get_faults_6() const { return ___faults_6; }
	inline FaultBindingCollection_t2785831862 ** get_address_of_faults_6() { return &___faults_6; }
	inline void set_faults_6(FaultBindingCollection_t2785831862 * value)
	{
		___faults_6 = value;
		Il2CppCodeGenWriteBarrier(&___faults_6, value);
	}

	inline static int32_t get_offset_of_input_7() { return static_cast<int32_t>(offsetof(OperationBinding_t314325322, ___input_7)); }
	inline InputBinding_t3136796011 * get_input_7() const { return ___input_7; }
	inline InputBinding_t3136796011 ** get_address_of_input_7() { return &___input_7; }
	inline void set_input_7(InputBinding_t3136796011 * value)
	{
		___input_7 = value;
		Il2CppCodeGenWriteBarrier(&___input_7, value);
	}

	inline static int32_t get_offset_of_output_8() { return static_cast<int32_t>(offsetof(OperationBinding_t314325322, ___output_8)); }
	inline OutputBinding_t1318715061 * get_output_8() const { return ___output_8; }
	inline OutputBinding_t1318715061 ** get_address_of_output_8() { return &___output_8; }
	inline void set_output_8(OutputBinding_t1318715061 * value)
	{
		___output_8 = value;
		Il2CppCodeGenWriteBarrier(&___output_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
