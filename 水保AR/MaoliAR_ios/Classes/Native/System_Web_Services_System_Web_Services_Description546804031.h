﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Xml.XmlElement
struct XmlElement_t561603118;
// System.Xml.XmlAttribute[]
struct XmlAttributeU5BU5D_t1490365106;
// System.Xml.Serialization.XmlSerializerNamespaces
struct XmlSerializerNamespaces_t2702737953;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.DocumentableItem
struct  DocumentableItem_t546804031  : public Il2CppObject
{
public:
	// System.Xml.XmlElement System.Web.Services.Description.DocumentableItem::docElement
	XmlElement_t561603118 * ___docElement_0;
	// System.Xml.XmlAttribute[] System.Web.Services.Description.DocumentableItem::extAttributes
	XmlAttributeU5BU5D_t1490365106* ___extAttributes_1;
	// System.Xml.Serialization.XmlSerializerNamespaces System.Web.Services.Description.DocumentableItem::namespaces
	XmlSerializerNamespaces_t2702737953 * ___namespaces_2;

public:
	inline static int32_t get_offset_of_docElement_0() { return static_cast<int32_t>(offsetof(DocumentableItem_t546804031, ___docElement_0)); }
	inline XmlElement_t561603118 * get_docElement_0() const { return ___docElement_0; }
	inline XmlElement_t561603118 ** get_address_of_docElement_0() { return &___docElement_0; }
	inline void set_docElement_0(XmlElement_t561603118 * value)
	{
		___docElement_0 = value;
		Il2CppCodeGenWriteBarrier(&___docElement_0, value);
	}

	inline static int32_t get_offset_of_extAttributes_1() { return static_cast<int32_t>(offsetof(DocumentableItem_t546804031, ___extAttributes_1)); }
	inline XmlAttributeU5BU5D_t1490365106* get_extAttributes_1() const { return ___extAttributes_1; }
	inline XmlAttributeU5BU5D_t1490365106** get_address_of_extAttributes_1() { return &___extAttributes_1; }
	inline void set_extAttributes_1(XmlAttributeU5BU5D_t1490365106* value)
	{
		___extAttributes_1 = value;
		Il2CppCodeGenWriteBarrier(&___extAttributes_1, value);
	}

	inline static int32_t get_offset_of_namespaces_2() { return static_cast<int32_t>(offsetof(DocumentableItem_t546804031, ___namespaces_2)); }
	inline XmlSerializerNamespaces_t2702737953 * get_namespaces_2() const { return ___namespaces_2; }
	inline XmlSerializerNamespaces_t2702737953 ** get_address_of_namespaces_2() { return &___namespaces_2; }
	inline void set_namespaces_2(XmlSerializerNamespaces_t2702737953 * value)
	{
		___namespaces_2 = value;
		Il2CppCodeGenWriteBarrier(&___namespaces_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
