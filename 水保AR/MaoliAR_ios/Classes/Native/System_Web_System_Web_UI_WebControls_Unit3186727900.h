﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"
#include "System_Web_System_Web_UI_WebControls_UnitType3617768156.h"
#include "System_Web_System_Web_UI_WebControls_Unit3186727900.h"

// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.WebControls.Unit
struct  Unit_t3186727900 
{
public:
	// System.Web.UI.WebControls.UnitType System.Web.UI.WebControls.Unit::type
	int32_t ___type_0;
	// System.Double System.Web.UI.WebControls.Unit::value
	double ___value_1;
	// System.Boolean System.Web.UI.WebControls.Unit::valueSet
	bool ___valueSet_2;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(Unit_t3186727900, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(Unit_t3186727900, ___value_1)); }
	inline double get_value_1() const { return ___value_1; }
	inline double* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(double value)
	{
		___value_1 = value;
	}

	inline static int32_t get_offset_of_valueSet_2() { return static_cast<int32_t>(offsetof(Unit_t3186727900, ___valueSet_2)); }
	inline bool get_valueSet_2() const { return ___valueSet_2; }
	inline bool* get_address_of_valueSet_2() { return &___valueSet_2; }
	inline void set_valueSet_2(bool value)
	{
		___valueSet_2 = value;
	}
};

struct Unit_t3186727900_StaticFields
{
public:
	// System.Web.UI.WebControls.Unit System.Web.UI.WebControls.Unit::Empty
	Unit_t3186727900  ___Empty_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Web.UI.WebControls.Unit::<>f__switch$map1B
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map1B_4;

public:
	inline static int32_t get_offset_of_Empty_3() { return static_cast<int32_t>(offsetof(Unit_t3186727900_StaticFields, ___Empty_3)); }
	inline Unit_t3186727900  get_Empty_3() const { return ___Empty_3; }
	inline Unit_t3186727900 * get_address_of_Empty_3() { return &___Empty_3; }
	inline void set_Empty_3(Unit_t3186727900  value)
	{
		___Empty_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1B_4() { return static_cast<int32_t>(offsetof(Unit_t3186727900_StaticFields, ___U3CU3Ef__switchU24map1B_4)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map1B_4() const { return ___U3CU3Ef__switchU24map1B_4; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map1B_4() { return &___U3CU3Ef__switchU24map1B_4; }
	inline void set_U3CU3Ef__switchU24map1B_4(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map1B_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1B_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Web.UI.WebControls.Unit
struct Unit_t3186727900_marshaled_pinvoke
{
	int32_t ___type_0;
	double ___value_1;
	int32_t ___valueSet_2;
};
// Native definition for COM marshalling of System.Web.UI.WebControls.Unit
struct Unit_t3186727900_marshaled_com
{
	int32_t ___type_0;
	double ___value_1;
	int32_t ___valueSet_2;
};
