﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Object
struct Il2CppObject;
// System.Web.UI.Page
struct Page_t770747966;
// System.Web.UI.IStateFormatter
struct IStateFormatter_t3869134886;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.PageStatePersister
struct  PageStatePersister_t2741789537  : public Il2CppObject
{
public:
	// System.Object System.Web.UI.PageStatePersister::control_state
	Il2CppObject * ___control_state_0;
	// System.Object System.Web.UI.PageStatePersister::view_state
	Il2CppObject * ___view_state_1;
	// System.Web.UI.Page System.Web.UI.PageStatePersister::page
	Page_t770747966 * ___page_2;
	// System.Web.UI.IStateFormatter System.Web.UI.PageStatePersister::state_formatter
	Il2CppObject * ___state_formatter_3;

public:
	inline static int32_t get_offset_of_control_state_0() { return static_cast<int32_t>(offsetof(PageStatePersister_t2741789537, ___control_state_0)); }
	inline Il2CppObject * get_control_state_0() const { return ___control_state_0; }
	inline Il2CppObject ** get_address_of_control_state_0() { return &___control_state_0; }
	inline void set_control_state_0(Il2CppObject * value)
	{
		___control_state_0 = value;
		Il2CppCodeGenWriteBarrier(&___control_state_0, value);
	}

	inline static int32_t get_offset_of_view_state_1() { return static_cast<int32_t>(offsetof(PageStatePersister_t2741789537, ___view_state_1)); }
	inline Il2CppObject * get_view_state_1() const { return ___view_state_1; }
	inline Il2CppObject ** get_address_of_view_state_1() { return &___view_state_1; }
	inline void set_view_state_1(Il2CppObject * value)
	{
		___view_state_1 = value;
		Il2CppCodeGenWriteBarrier(&___view_state_1, value);
	}

	inline static int32_t get_offset_of_page_2() { return static_cast<int32_t>(offsetof(PageStatePersister_t2741789537, ___page_2)); }
	inline Page_t770747966 * get_page_2() const { return ___page_2; }
	inline Page_t770747966 ** get_address_of_page_2() { return &___page_2; }
	inline void set_page_2(Page_t770747966 * value)
	{
		___page_2 = value;
		Il2CppCodeGenWriteBarrier(&___page_2, value);
	}

	inline static int32_t get_offset_of_state_formatter_3() { return static_cast<int32_t>(offsetof(PageStatePersister_t2741789537, ___state_formatter_3)); }
	inline Il2CppObject * get_state_formatter_3() const { return ___state_formatter_3; }
	inline Il2CppObject ** get_address_of_state_formatter_3() { return &___state_formatter_3; }
	inline void set_state_formatter_3(Il2CppObject * value)
	{
		___state_formatter_3 = value;
		Il2CppCodeGenWriteBarrier(&___state_formatter_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
