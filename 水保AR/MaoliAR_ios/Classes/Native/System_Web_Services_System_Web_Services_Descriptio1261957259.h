﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_Services_System_Web_Services_Descriptio3177955060.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Description.HttpAddressBinding
struct  HttpAddressBinding_t1261957259  : public ServiceDescriptionFormatExtension_t3177955060
{
public:
	// System.String System.Web.Services.Description.HttpAddressBinding::location
	String_t* ___location_1;

public:
	inline static int32_t get_offset_of_location_1() { return static_cast<int32_t>(offsetof(HttpAddressBinding_t1261957259, ___location_1)); }
	inline String_t* get_location_1() const { return ___location_1; }
	inline String_t** get_address_of_location_1() { return &___location_1; }
	inline void set_location_1(String_t* value)
	{
		___location_1 = value;
		Il2CppCodeGenWriteBarrier(&___location_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
