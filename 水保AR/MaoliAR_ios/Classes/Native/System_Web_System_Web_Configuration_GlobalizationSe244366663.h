﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3156163955.h"

// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Globalization.CultureInfo
struct CultureInfo_t4157843068;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.GlobalizationSection
struct  GlobalizationSection_t244366663  : public ConfigurationSection_t3156163955
{
public:
	// System.String System.Web.Configuration.GlobalizationSection::cached_fileencoding
	String_t* ___cached_fileencoding_27;
	// System.String System.Web.Configuration.GlobalizationSection::cached_requestencoding
	String_t* ___cached_requestencoding_28;
	// System.String System.Web.Configuration.GlobalizationSection::cached_responseencoding
	String_t* ___cached_responseencoding_29;
	// System.Collections.Hashtable System.Web.Configuration.GlobalizationSection::encodingHash
	Hashtable_t1853889766 * ___encodingHash_30;
	// System.String System.Web.Configuration.GlobalizationSection::cached_culture
	String_t* ___cached_culture_31;
	// System.Globalization.CultureInfo System.Web.Configuration.GlobalizationSection::cached_cultureinfo
	CultureInfo_t4157843068 * ___cached_cultureinfo_32;
	// System.String System.Web.Configuration.GlobalizationSection::cached_uiculture
	String_t* ___cached_uiculture_33;
	// System.Globalization.CultureInfo System.Web.Configuration.GlobalizationSection::cached_uicultureinfo
	CultureInfo_t4157843068 * ___cached_uicultureinfo_34;
	// System.Boolean System.Web.Configuration.GlobalizationSection::autoCulture
	bool ___autoCulture_37;
	// System.Boolean System.Web.Configuration.GlobalizationSection::autoUICulture
	bool ___autoUICulture_38;

public:
	inline static int32_t get_offset_of_cached_fileencoding_27() { return static_cast<int32_t>(offsetof(GlobalizationSection_t244366663, ___cached_fileencoding_27)); }
	inline String_t* get_cached_fileencoding_27() const { return ___cached_fileencoding_27; }
	inline String_t** get_address_of_cached_fileencoding_27() { return &___cached_fileencoding_27; }
	inline void set_cached_fileencoding_27(String_t* value)
	{
		___cached_fileencoding_27 = value;
		Il2CppCodeGenWriteBarrier(&___cached_fileencoding_27, value);
	}

	inline static int32_t get_offset_of_cached_requestencoding_28() { return static_cast<int32_t>(offsetof(GlobalizationSection_t244366663, ___cached_requestencoding_28)); }
	inline String_t* get_cached_requestencoding_28() const { return ___cached_requestencoding_28; }
	inline String_t** get_address_of_cached_requestencoding_28() { return &___cached_requestencoding_28; }
	inline void set_cached_requestencoding_28(String_t* value)
	{
		___cached_requestencoding_28 = value;
		Il2CppCodeGenWriteBarrier(&___cached_requestencoding_28, value);
	}

	inline static int32_t get_offset_of_cached_responseencoding_29() { return static_cast<int32_t>(offsetof(GlobalizationSection_t244366663, ___cached_responseencoding_29)); }
	inline String_t* get_cached_responseencoding_29() const { return ___cached_responseencoding_29; }
	inline String_t** get_address_of_cached_responseencoding_29() { return &___cached_responseencoding_29; }
	inline void set_cached_responseencoding_29(String_t* value)
	{
		___cached_responseencoding_29 = value;
		Il2CppCodeGenWriteBarrier(&___cached_responseencoding_29, value);
	}

	inline static int32_t get_offset_of_encodingHash_30() { return static_cast<int32_t>(offsetof(GlobalizationSection_t244366663, ___encodingHash_30)); }
	inline Hashtable_t1853889766 * get_encodingHash_30() const { return ___encodingHash_30; }
	inline Hashtable_t1853889766 ** get_address_of_encodingHash_30() { return &___encodingHash_30; }
	inline void set_encodingHash_30(Hashtable_t1853889766 * value)
	{
		___encodingHash_30 = value;
		Il2CppCodeGenWriteBarrier(&___encodingHash_30, value);
	}

	inline static int32_t get_offset_of_cached_culture_31() { return static_cast<int32_t>(offsetof(GlobalizationSection_t244366663, ___cached_culture_31)); }
	inline String_t* get_cached_culture_31() const { return ___cached_culture_31; }
	inline String_t** get_address_of_cached_culture_31() { return &___cached_culture_31; }
	inline void set_cached_culture_31(String_t* value)
	{
		___cached_culture_31 = value;
		Il2CppCodeGenWriteBarrier(&___cached_culture_31, value);
	}

	inline static int32_t get_offset_of_cached_cultureinfo_32() { return static_cast<int32_t>(offsetof(GlobalizationSection_t244366663, ___cached_cultureinfo_32)); }
	inline CultureInfo_t4157843068 * get_cached_cultureinfo_32() const { return ___cached_cultureinfo_32; }
	inline CultureInfo_t4157843068 ** get_address_of_cached_cultureinfo_32() { return &___cached_cultureinfo_32; }
	inline void set_cached_cultureinfo_32(CultureInfo_t4157843068 * value)
	{
		___cached_cultureinfo_32 = value;
		Il2CppCodeGenWriteBarrier(&___cached_cultureinfo_32, value);
	}

	inline static int32_t get_offset_of_cached_uiculture_33() { return static_cast<int32_t>(offsetof(GlobalizationSection_t244366663, ___cached_uiculture_33)); }
	inline String_t* get_cached_uiculture_33() const { return ___cached_uiculture_33; }
	inline String_t** get_address_of_cached_uiculture_33() { return &___cached_uiculture_33; }
	inline void set_cached_uiculture_33(String_t* value)
	{
		___cached_uiculture_33 = value;
		Il2CppCodeGenWriteBarrier(&___cached_uiculture_33, value);
	}

	inline static int32_t get_offset_of_cached_uicultureinfo_34() { return static_cast<int32_t>(offsetof(GlobalizationSection_t244366663, ___cached_uicultureinfo_34)); }
	inline CultureInfo_t4157843068 * get_cached_uicultureinfo_34() const { return ___cached_uicultureinfo_34; }
	inline CultureInfo_t4157843068 ** get_address_of_cached_uicultureinfo_34() { return &___cached_uicultureinfo_34; }
	inline void set_cached_uicultureinfo_34(CultureInfo_t4157843068 * value)
	{
		___cached_uicultureinfo_34 = value;
		Il2CppCodeGenWriteBarrier(&___cached_uicultureinfo_34, value);
	}

	inline static int32_t get_offset_of_autoCulture_37() { return static_cast<int32_t>(offsetof(GlobalizationSection_t244366663, ___autoCulture_37)); }
	inline bool get_autoCulture_37() const { return ___autoCulture_37; }
	inline bool* get_address_of_autoCulture_37() { return &___autoCulture_37; }
	inline void set_autoCulture_37(bool value)
	{
		___autoCulture_37 = value;
	}

	inline static int32_t get_offset_of_autoUICulture_38() { return static_cast<int32_t>(offsetof(GlobalizationSection_t244366663, ___autoUICulture_38)); }
	inline bool get_autoUICulture_38() const { return ___autoUICulture_38; }
	inline bool* get_address_of_autoUICulture_38() { return &___autoUICulture_38; }
	inline void set_autoUICulture_38(bool value)
	{
		___autoUICulture_38 = value;
	}
};

struct GlobalizationSection_t244366663_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Web.Configuration.GlobalizationSection::cultureProp
	ConfigurationProperty_t3590861854 * ___cultureProp_17;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.GlobalizationSection::enableBestFitResponseEncodingProp
	ConfigurationProperty_t3590861854 * ___enableBestFitResponseEncodingProp_18;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.GlobalizationSection::enableClientBasedCultureProp
	ConfigurationProperty_t3590861854 * ___enableClientBasedCultureProp_19;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.GlobalizationSection::fileEncodingProp
	ConfigurationProperty_t3590861854 * ___fileEncodingProp_20;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.GlobalizationSection::requestEncodingProp
	ConfigurationProperty_t3590861854 * ___requestEncodingProp_21;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.GlobalizationSection::resourceProviderFactoryTypeProp
	ConfigurationProperty_t3590861854 * ___resourceProviderFactoryTypeProp_22;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.GlobalizationSection::responseEncodingProp
	ConfigurationProperty_t3590861854 * ___responseEncodingProp_23;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.GlobalizationSection::responseHeaderEncodingProp
	ConfigurationProperty_t3590861854 * ___responseHeaderEncodingProp_24;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.GlobalizationSection::uiCultureProp
	ConfigurationProperty_t3590861854 * ___uiCultureProp_25;
	// System.Configuration.ConfigurationPropertyCollection System.Web.Configuration.GlobalizationSection::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_26;
	// System.Boolean System.Web.Configuration.GlobalizationSection::encoding_warning
	bool ___encoding_warning_35;
	// System.Boolean System.Web.Configuration.GlobalizationSection::culture_warning
	bool ___culture_warning_36;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Web.Configuration.GlobalizationSection::<>f__switch$mapA
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24mapA_39;

public:
	inline static int32_t get_offset_of_cultureProp_17() { return static_cast<int32_t>(offsetof(GlobalizationSection_t244366663_StaticFields, ___cultureProp_17)); }
	inline ConfigurationProperty_t3590861854 * get_cultureProp_17() const { return ___cultureProp_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_cultureProp_17() { return &___cultureProp_17; }
	inline void set_cultureProp_17(ConfigurationProperty_t3590861854 * value)
	{
		___cultureProp_17 = value;
		Il2CppCodeGenWriteBarrier(&___cultureProp_17, value);
	}

	inline static int32_t get_offset_of_enableBestFitResponseEncodingProp_18() { return static_cast<int32_t>(offsetof(GlobalizationSection_t244366663_StaticFields, ___enableBestFitResponseEncodingProp_18)); }
	inline ConfigurationProperty_t3590861854 * get_enableBestFitResponseEncodingProp_18() const { return ___enableBestFitResponseEncodingProp_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_enableBestFitResponseEncodingProp_18() { return &___enableBestFitResponseEncodingProp_18; }
	inline void set_enableBestFitResponseEncodingProp_18(ConfigurationProperty_t3590861854 * value)
	{
		___enableBestFitResponseEncodingProp_18 = value;
		Il2CppCodeGenWriteBarrier(&___enableBestFitResponseEncodingProp_18, value);
	}

	inline static int32_t get_offset_of_enableClientBasedCultureProp_19() { return static_cast<int32_t>(offsetof(GlobalizationSection_t244366663_StaticFields, ___enableClientBasedCultureProp_19)); }
	inline ConfigurationProperty_t3590861854 * get_enableClientBasedCultureProp_19() const { return ___enableClientBasedCultureProp_19; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_enableClientBasedCultureProp_19() { return &___enableClientBasedCultureProp_19; }
	inline void set_enableClientBasedCultureProp_19(ConfigurationProperty_t3590861854 * value)
	{
		___enableClientBasedCultureProp_19 = value;
		Il2CppCodeGenWriteBarrier(&___enableClientBasedCultureProp_19, value);
	}

	inline static int32_t get_offset_of_fileEncodingProp_20() { return static_cast<int32_t>(offsetof(GlobalizationSection_t244366663_StaticFields, ___fileEncodingProp_20)); }
	inline ConfigurationProperty_t3590861854 * get_fileEncodingProp_20() const { return ___fileEncodingProp_20; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_fileEncodingProp_20() { return &___fileEncodingProp_20; }
	inline void set_fileEncodingProp_20(ConfigurationProperty_t3590861854 * value)
	{
		___fileEncodingProp_20 = value;
		Il2CppCodeGenWriteBarrier(&___fileEncodingProp_20, value);
	}

	inline static int32_t get_offset_of_requestEncodingProp_21() { return static_cast<int32_t>(offsetof(GlobalizationSection_t244366663_StaticFields, ___requestEncodingProp_21)); }
	inline ConfigurationProperty_t3590861854 * get_requestEncodingProp_21() const { return ___requestEncodingProp_21; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_requestEncodingProp_21() { return &___requestEncodingProp_21; }
	inline void set_requestEncodingProp_21(ConfigurationProperty_t3590861854 * value)
	{
		___requestEncodingProp_21 = value;
		Il2CppCodeGenWriteBarrier(&___requestEncodingProp_21, value);
	}

	inline static int32_t get_offset_of_resourceProviderFactoryTypeProp_22() { return static_cast<int32_t>(offsetof(GlobalizationSection_t244366663_StaticFields, ___resourceProviderFactoryTypeProp_22)); }
	inline ConfigurationProperty_t3590861854 * get_resourceProviderFactoryTypeProp_22() const { return ___resourceProviderFactoryTypeProp_22; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_resourceProviderFactoryTypeProp_22() { return &___resourceProviderFactoryTypeProp_22; }
	inline void set_resourceProviderFactoryTypeProp_22(ConfigurationProperty_t3590861854 * value)
	{
		___resourceProviderFactoryTypeProp_22 = value;
		Il2CppCodeGenWriteBarrier(&___resourceProviderFactoryTypeProp_22, value);
	}

	inline static int32_t get_offset_of_responseEncodingProp_23() { return static_cast<int32_t>(offsetof(GlobalizationSection_t244366663_StaticFields, ___responseEncodingProp_23)); }
	inline ConfigurationProperty_t3590861854 * get_responseEncodingProp_23() const { return ___responseEncodingProp_23; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_responseEncodingProp_23() { return &___responseEncodingProp_23; }
	inline void set_responseEncodingProp_23(ConfigurationProperty_t3590861854 * value)
	{
		___responseEncodingProp_23 = value;
		Il2CppCodeGenWriteBarrier(&___responseEncodingProp_23, value);
	}

	inline static int32_t get_offset_of_responseHeaderEncodingProp_24() { return static_cast<int32_t>(offsetof(GlobalizationSection_t244366663_StaticFields, ___responseHeaderEncodingProp_24)); }
	inline ConfigurationProperty_t3590861854 * get_responseHeaderEncodingProp_24() const { return ___responseHeaderEncodingProp_24; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_responseHeaderEncodingProp_24() { return &___responseHeaderEncodingProp_24; }
	inline void set_responseHeaderEncodingProp_24(ConfigurationProperty_t3590861854 * value)
	{
		___responseHeaderEncodingProp_24 = value;
		Il2CppCodeGenWriteBarrier(&___responseHeaderEncodingProp_24, value);
	}

	inline static int32_t get_offset_of_uiCultureProp_25() { return static_cast<int32_t>(offsetof(GlobalizationSection_t244366663_StaticFields, ___uiCultureProp_25)); }
	inline ConfigurationProperty_t3590861854 * get_uiCultureProp_25() const { return ___uiCultureProp_25; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_uiCultureProp_25() { return &___uiCultureProp_25; }
	inline void set_uiCultureProp_25(ConfigurationProperty_t3590861854 * value)
	{
		___uiCultureProp_25 = value;
		Il2CppCodeGenWriteBarrier(&___uiCultureProp_25, value);
	}

	inline static int32_t get_offset_of_properties_26() { return static_cast<int32_t>(offsetof(GlobalizationSection_t244366663_StaticFields, ___properties_26)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_26() const { return ___properties_26; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_26() { return &___properties_26; }
	inline void set_properties_26(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_26 = value;
		Il2CppCodeGenWriteBarrier(&___properties_26, value);
	}

	inline static int32_t get_offset_of_encoding_warning_35() { return static_cast<int32_t>(offsetof(GlobalizationSection_t244366663_StaticFields, ___encoding_warning_35)); }
	inline bool get_encoding_warning_35() const { return ___encoding_warning_35; }
	inline bool* get_address_of_encoding_warning_35() { return &___encoding_warning_35; }
	inline void set_encoding_warning_35(bool value)
	{
		___encoding_warning_35 = value;
	}

	inline static int32_t get_offset_of_culture_warning_36() { return static_cast<int32_t>(offsetof(GlobalizationSection_t244366663_StaticFields, ___culture_warning_36)); }
	inline bool get_culture_warning_36() const { return ___culture_warning_36; }
	inline bool* get_address_of_culture_warning_36() { return &___culture_warning_36; }
	inline void set_culture_warning_36(bool value)
	{
		___culture_warning_36 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapA_39() { return static_cast<int32_t>(offsetof(GlobalizationSection_t244366663_StaticFields, ___U3CU3Ef__switchU24mapA_39)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24mapA_39() const { return ___U3CU3Ef__switchU24mapA_39; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24mapA_39() { return &___U3CU3Ef__switchU24mapA_39; }
	inline void set_U3CU3Ef__switchU24mapA_39(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24mapA_39 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapA_39, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
