﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Generic.List`1<System.ServiceModel.FaultReasonText>
struct List_1_t724458495;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.SynchronizedReadOnlyCollection`1<System.ServiceModel.FaultReasonText>
struct  SynchronizedReadOnlyCollection_1_t1625918761  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.SynchronizedReadOnlyCollection`1::l
	List_1_t724458495 * ___l_0;
	// System.Object System.Collections.Generic.SynchronizedReadOnlyCollection`1::sync_root
	Il2CppObject * ___sync_root_1;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(SynchronizedReadOnlyCollection_1_t1625918761, ___l_0)); }
	inline List_1_t724458495 * get_l_0() const { return ___l_0; }
	inline List_1_t724458495 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t724458495 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier(&___l_0, value);
	}

	inline static int32_t get_offset_of_sync_root_1() { return static_cast<int32_t>(offsetof(SynchronizedReadOnlyCollection_1_t1625918761, ___sync_root_1)); }
	inline Il2CppObject * get_sync_root_1() const { return ___sync_root_1; }
	inline Il2CppObject ** get_address_of_sync_root_1() { return &___sync_root_1; }
	inline void set_sync_root_1(Il2CppObject * value)
	{
		___sync_root_1 = value;
		Il2CppCodeGenWriteBarrier(&___sync_root_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
