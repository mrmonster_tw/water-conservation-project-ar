﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Selector3905425829.h"

// System.IdentityModel.Selectors.SecurityTokenResolver[]
struct SecurityTokenResolverU5BU5D_t91456040;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.UnionSecurityTokenResolver
struct  UnionSecurityTokenResolver_t4128541908  : public SecurityTokenResolver_t3905425829
{
public:
	// System.IdentityModel.Selectors.SecurityTokenResolver[] System.ServiceModel.Channels.UnionSecurityTokenResolver::resolvers
	SecurityTokenResolverU5BU5D_t91456040* ___resolvers_0;

public:
	inline static int32_t get_offset_of_resolvers_0() { return static_cast<int32_t>(offsetof(UnionSecurityTokenResolver_t4128541908, ___resolvers_0)); }
	inline SecurityTokenResolverU5BU5D_t91456040* get_resolvers_0() const { return ___resolvers_0; }
	inline SecurityTokenResolverU5BU5D_t91456040** get_address_of_resolvers_0() { return &___resolvers_0; }
	inline void set_resolvers_0(SecurityTokenResolverU5BU5D_t91456040* value)
	{
		___resolvers_0 = value;
		Il2CppCodeGenWriteBarrier(&___resolvers_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
