﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Runtime_Serialization_System_Runtime_Serial4088073813.h"

// System.Type
struct Type_t;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2760654312;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.CollectionDataContractAttribute
struct CollectionDataContractAttribute_t3671020647;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.DictionaryTypeMap
struct  DictionaryTypeMap_t3897582736  : public SerializationMap_t4088073813
{
public:
	// System.Type System.Runtime.Serialization.DictionaryTypeMap::key_type
	Type_t * ___key_type_6;
	// System.Type System.Runtime.Serialization.DictionaryTypeMap::value_type
	Type_t * ___value_type_7;
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.DictionaryTypeMap::item_qname
	XmlQualifiedName_t2760654312 * ___item_qname_8;
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.DictionaryTypeMap::key_qname
	XmlQualifiedName_t2760654312 * ___key_qname_9;
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.DictionaryTypeMap::value_qname
	XmlQualifiedName_t2760654312 * ___value_qname_10;
	// System.Reflection.MethodInfo System.Runtime.Serialization.DictionaryTypeMap::add_method
	MethodInfo_t * ___add_method_11;
	// System.Runtime.Serialization.CollectionDataContractAttribute System.Runtime.Serialization.DictionaryTypeMap::a
	CollectionDataContractAttribute_t3671020647 * ___a_12;
	// System.Type System.Runtime.Serialization.DictionaryTypeMap::pair_type
	Type_t * ___pair_type_15;
	// System.Reflection.PropertyInfo System.Runtime.Serialization.DictionaryTypeMap::pair_key_property
	PropertyInfo_t * ___pair_key_property_16;
	// System.Reflection.PropertyInfo System.Runtime.Serialization.DictionaryTypeMap::pair_value_property
	PropertyInfo_t * ___pair_value_property_17;

public:
	inline static int32_t get_offset_of_key_type_6() { return static_cast<int32_t>(offsetof(DictionaryTypeMap_t3897582736, ___key_type_6)); }
	inline Type_t * get_key_type_6() const { return ___key_type_6; }
	inline Type_t ** get_address_of_key_type_6() { return &___key_type_6; }
	inline void set_key_type_6(Type_t * value)
	{
		___key_type_6 = value;
		Il2CppCodeGenWriteBarrier(&___key_type_6, value);
	}

	inline static int32_t get_offset_of_value_type_7() { return static_cast<int32_t>(offsetof(DictionaryTypeMap_t3897582736, ___value_type_7)); }
	inline Type_t * get_value_type_7() const { return ___value_type_7; }
	inline Type_t ** get_address_of_value_type_7() { return &___value_type_7; }
	inline void set_value_type_7(Type_t * value)
	{
		___value_type_7 = value;
		Il2CppCodeGenWriteBarrier(&___value_type_7, value);
	}

	inline static int32_t get_offset_of_item_qname_8() { return static_cast<int32_t>(offsetof(DictionaryTypeMap_t3897582736, ___item_qname_8)); }
	inline XmlQualifiedName_t2760654312 * get_item_qname_8() const { return ___item_qname_8; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_item_qname_8() { return &___item_qname_8; }
	inline void set_item_qname_8(XmlQualifiedName_t2760654312 * value)
	{
		___item_qname_8 = value;
		Il2CppCodeGenWriteBarrier(&___item_qname_8, value);
	}

	inline static int32_t get_offset_of_key_qname_9() { return static_cast<int32_t>(offsetof(DictionaryTypeMap_t3897582736, ___key_qname_9)); }
	inline XmlQualifiedName_t2760654312 * get_key_qname_9() const { return ___key_qname_9; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_key_qname_9() { return &___key_qname_9; }
	inline void set_key_qname_9(XmlQualifiedName_t2760654312 * value)
	{
		___key_qname_9 = value;
		Il2CppCodeGenWriteBarrier(&___key_qname_9, value);
	}

	inline static int32_t get_offset_of_value_qname_10() { return static_cast<int32_t>(offsetof(DictionaryTypeMap_t3897582736, ___value_qname_10)); }
	inline XmlQualifiedName_t2760654312 * get_value_qname_10() const { return ___value_qname_10; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_value_qname_10() { return &___value_qname_10; }
	inline void set_value_qname_10(XmlQualifiedName_t2760654312 * value)
	{
		___value_qname_10 = value;
		Il2CppCodeGenWriteBarrier(&___value_qname_10, value);
	}

	inline static int32_t get_offset_of_add_method_11() { return static_cast<int32_t>(offsetof(DictionaryTypeMap_t3897582736, ___add_method_11)); }
	inline MethodInfo_t * get_add_method_11() const { return ___add_method_11; }
	inline MethodInfo_t ** get_address_of_add_method_11() { return &___add_method_11; }
	inline void set_add_method_11(MethodInfo_t * value)
	{
		___add_method_11 = value;
		Il2CppCodeGenWriteBarrier(&___add_method_11, value);
	}

	inline static int32_t get_offset_of_a_12() { return static_cast<int32_t>(offsetof(DictionaryTypeMap_t3897582736, ___a_12)); }
	inline CollectionDataContractAttribute_t3671020647 * get_a_12() const { return ___a_12; }
	inline CollectionDataContractAttribute_t3671020647 ** get_address_of_a_12() { return &___a_12; }
	inline void set_a_12(CollectionDataContractAttribute_t3671020647 * value)
	{
		___a_12 = value;
		Il2CppCodeGenWriteBarrier(&___a_12, value);
	}

	inline static int32_t get_offset_of_pair_type_15() { return static_cast<int32_t>(offsetof(DictionaryTypeMap_t3897582736, ___pair_type_15)); }
	inline Type_t * get_pair_type_15() const { return ___pair_type_15; }
	inline Type_t ** get_address_of_pair_type_15() { return &___pair_type_15; }
	inline void set_pair_type_15(Type_t * value)
	{
		___pair_type_15 = value;
		Il2CppCodeGenWriteBarrier(&___pair_type_15, value);
	}

	inline static int32_t get_offset_of_pair_key_property_16() { return static_cast<int32_t>(offsetof(DictionaryTypeMap_t3897582736, ___pair_key_property_16)); }
	inline PropertyInfo_t * get_pair_key_property_16() const { return ___pair_key_property_16; }
	inline PropertyInfo_t ** get_address_of_pair_key_property_16() { return &___pair_key_property_16; }
	inline void set_pair_key_property_16(PropertyInfo_t * value)
	{
		___pair_key_property_16 = value;
		Il2CppCodeGenWriteBarrier(&___pair_key_property_16, value);
	}

	inline static int32_t get_offset_of_pair_value_property_17() { return static_cast<int32_t>(offsetof(DictionaryTypeMap_t3897582736, ___pair_value_property_17)); }
	inline PropertyInfo_t * get_pair_value_property_17() const { return ___pair_value_property_17; }
	inline PropertyInfo_t ** get_address_of_pair_value_property_17() { return &___pair_value_property_17; }
	inline void set_pair_value_property_17(PropertyInfo_t * value)
	{
		___pair_value_property_17 = value;
		Il2CppCodeGenWriteBarrier(&___pair_value_property_17, value);
	}
};

struct DictionaryTypeMap_t3897582736_StaticFields
{
public:
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.DictionaryTypeMap::kvpair_key_qname
	XmlQualifiedName_t2760654312 * ___kvpair_key_qname_13;
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.DictionaryTypeMap::kvpair_value_qname
	XmlQualifiedName_t2760654312 * ___kvpair_value_qname_14;

public:
	inline static int32_t get_offset_of_kvpair_key_qname_13() { return static_cast<int32_t>(offsetof(DictionaryTypeMap_t3897582736_StaticFields, ___kvpair_key_qname_13)); }
	inline XmlQualifiedName_t2760654312 * get_kvpair_key_qname_13() const { return ___kvpair_key_qname_13; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_kvpair_key_qname_13() { return &___kvpair_key_qname_13; }
	inline void set_kvpair_key_qname_13(XmlQualifiedName_t2760654312 * value)
	{
		___kvpair_key_qname_13 = value;
		Il2CppCodeGenWriteBarrier(&___kvpair_key_qname_13, value);
	}

	inline static int32_t get_offset_of_kvpair_value_qname_14() { return static_cast<int32_t>(offsetof(DictionaryTypeMap_t3897582736_StaticFields, ___kvpair_value_qname_14)); }
	inline XmlQualifiedName_t2760654312 * get_kvpair_value_qname_14() const { return ___kvpair_value_qname_14; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_kvpair_value_qname_14() { return &___kvpair_value_qname_14; }
	inline void set_kvpair_value_qname_14(XmlQualifiedName_t2760654312 * value)
	{
		___kvpair_value_qname_14 = value;
		Il2CppCodeGenWriteBarrier(&___kvpair_value_qname_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
