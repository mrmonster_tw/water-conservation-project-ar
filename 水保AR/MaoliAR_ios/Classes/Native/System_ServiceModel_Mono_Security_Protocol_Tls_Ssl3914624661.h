﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_Mono_Security_Protocol_Tls_Ssl1667413407.h"

// Mono.Security.Protocol.Tls.CertificateValidationCallback
struct CertificateValidationCallback_t4091668219;
// Mono.Security.Protocol.Tls.CertificateSelectionCallback
struct CertificateSelectionCallback_t3743405225;
// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback
struct PrivateKeySelectionCallback_t3240194218;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.SslClientStream
struct  SslClientStream_t3914624662  : public SslStreamBase_t1667413408
{
public:
	// Mono.Security.Protocol.Tls.CertificateValidationCallback Mono.Security.Protocol.Tls.SslClientStream::ServerCertValidation
	CertificateValidationCallback_t4091668219 * ___ServerCertValidation_15;
	// Mono.Security.Protocol.Tls.CertificateSelectionCallback Mono.Security.Protocol.Tls.SslClientStream::ClientCertSelection
	CertificateSelectionCallback_t3743405225 * ___ClientCertSelection_16;
	// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback Mono.Security.Protocol.Tls.SslClientStream::PrivateKeySelection
	PrivateKeySelectionCallback_t3240194218 * ___PrivateKeySelection_17;

public:
	inline static int32_t get_offset_of_ServerCertValidation_15() { return static_cast<int32_t>(offsetof(SslClientStream_t3914624662, ___ServerCertValidation_15)); }
	inline CertificateValidationCallback_t4091668219 * get_ServerCertValidation_15() const { return ___ServerCertValidation_15; }
	inline CertificateValidationCallback_t4091668219 ** get_address_of_ServerCertValidation_15() { return &___ServerCertValidation_15; }
	inline void set_ServerCertValidation_15(CertificateValidationCallback_t4091668219 * value)
	{
		___ServerCertValidation_15 = value;
		Il2CppCodeGenWriteBarrier(&___ServerCertValidation_15, value);
	}

	inline static int32_t get_offset_of_ClientCertSelection_16() { return static_cast<int32_t>(offsetof(SslClientStream_t3914624662, ___ClientCertSelection_16)); }
	inline CertificateSelectionCallback_t3743405225 * get_ClientCertSelection_16() const { return ___ClientCertSelection_16; }
	inline CertificateSelectionCallback_t3743405225 ** get_address_of_ClientCertSelection_16() { return &___ClientCertSelection_16; }
	inline void set_ClientCertSelection_16(CertificateSelectionCallback_t3743405225 * value)
	{
		___ClientCertSelection_16 = value;
		Il2CppCodeGenWriteBarrier(&___ClientCertSelection_16, value);
	}

	inline static int32_t get_offset_of_PrivateKeySelection_17() { return static_cast<int32_t>(offsetof(SslClientStream_t3914624662, ___PrivateKeySelection_17)); }
	inline PrivateKeySelectionCallback_t3240194218 * get_PrivateKeySelection_17() const { return ___PrivateKeySelection_17; }
	inline PrivateKeySelectionCallback_t3240194218 ** get_address_of_PrivateKeySelection_17() { return &___PrivateKeySelection_17; }
	inline void set_PrivateKeySelection_17(PrivateKeySelectionCallback_t3240194218 * value)
	{
		___PrivateKeySelection_17 = value;
		Il2CppCodeGenWriteBarrier(&___PrivateKeySelection_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
