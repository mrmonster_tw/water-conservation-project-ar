﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Selector3905425829.h"

// System.Collections.Generic.Dictionary`2<System.Xml.UniqueId,System.Collections.Generic.Dictionary`2<System.Xml.UniqueId,System.ServiceModel.Security.Tokens.SecurityContextSecurityToken>>
struct Dictionary_2_t2586176522;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.SecurityContextSecurityTokenResolver
struct  SecurityContextSecurityTokenResolver_t1523964452  : public SecurityTokenResolver_t3905425829
{
public:
	// System.Int32 System.ServiceModel.Security.Tokens.SecurityContextSecurityTokenResolver::capacity
	int32_t ___capacity_0;
	// System.Boolean System.ServiceModel.Security.Tokens.SecurityContextSecurityTokenResolver::allow_removal
	bool ___allow_removal_1;
	// System.Collections.Generic.Dictionary`2<System.Xml.UniqueId,System.Collections.Generic.Dictionary`2<System.Xml.UniqueId,System.ServiceModel.Security.Tokens.SecurityContextSecurityToken>> System.ServiceModel.Security.Tokens.SecurityContextSecurityTokenResolver::cache
	Dictionary_2_t2586176522 * ___cache_2;

public:
	inline static int32_t get_offset_of_capacity_0() { return static_cast<int32_t>(offsetof(SecurityContextSecurityTokenResolver_t1523964452, ___capacity_0)); }
	inline int32_t get_capacity_0() const { return ___capacity_0; }
	inline int32_t* get_address_of_capacity_0() { return &___capacity_0; }
	inline void set_capacity_0(int32_t value)
	{
		___capacity_0 = value;
	}

	inline static int32_t get_offset_of_allow_removal_1() { return static_cast<int32_t>(offsetof(SecurityContextSecurityTokenResolver_t1523964452, ___allow_removal_1)); }
	inline bool get_allow_removal_1() const { return ___allow_removal_1; }
	inline bool* get_address_of_allow_removal_1() { return &___allow_removal_1; }
	inline void set_allow_removal_1(bool value)
	{
		___allow_removal_1 = value;
	}

	inline static int32_t get_offset_of_cache_2() { return static_cast<int32_t>(offsetof(SecurityContextSecurityTokenResolver_t1523964452, ___cache_2)); }
	inline Dictionary_2_t2586176522 * get_cache_2() const { return ___cache_2; }
	inline Dictionary_2_t2586176522 ** get_address_of_cache_2() { return &___cache_2; }
	inline void set_cache_2(Dictionary_2_t2586176522 * value)
	{
		___cache_2 = value;
		Il2CppCodeGenWriteBarrier(&___cache_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
