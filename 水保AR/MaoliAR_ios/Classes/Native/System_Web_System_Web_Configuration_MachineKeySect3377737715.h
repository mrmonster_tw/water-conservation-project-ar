﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3156163955.h"

// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.MachineKeySection
struct  MachineKeySection_t3377737715  : public ConfigurationSection_t3156163955
{
public:

public:
};

struct MachineKeySection_t3377737715_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Web.Configuration.MachineKeySection::decryptionProp
	ConfigurationProperty_t3590861854 * ___decryptionProp_17;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.MachineKeySection::decryptionKeyProp
	ConfigurationProperty_t3590861854 * ___decryptionKeyProp_18;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.MachineKeySection::validationProp
	ConfigurationProperty_t3590861854 * ___validationProp_19;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.MachineKeySection::validationKeyProp
	ConfigurationProperty_t3590861854 * ___validationKeyProp_20;
	// System.Configuration.ConfigurationPropertyCollection System.Web.Configuration.MachineKeySection::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_21;

public:
	inline static int32_t get_offset_of_decryptionProp_17() { return static_cast<int32_t>(offsetof(MachineKeySection_t3377737715_StaticFields, ___decryptionProp_17)); }
	inline ConfigurationProperty_t3590861854 * get_decryptionProp_17() const { return ___decryptionProp_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_decryptionProp_17() { return &___decryptionProp_17; }
	inline void set_decryptionProp_17(ConfigurationProperty_t3590861854 * value)
	{
		___decryptionProp_17 = value;
		Il2CppCodeGenWriteBarrier(&___decryptionProp_17, value);
	}

	inline static int32_t get_offset_of_decryptionKeyProp_18() { return static_cast<int32_t>(offsetof(MachineKeySection_t3377737715_StaticFields, ___decryptionKeyProp_18)); }
	inline ConfigurationProperty_t3590861854 * get_decryptionKeyProp_18() const { return ___decryptionKeyProp_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_decryptionKeyProp_18() { return &___decryptionKeyProp_18; }
	inline void set_decryptionKeyProp_18(ConfigurationProperty_t3590861854 * value)
	{
		___decryptionKeyProp_18 = value;
		Il2CppCodeGenWriteBarrier(&___decryptionKeyProp_18, value);
	}

	inline static int32_t get_offset_of_validationProp_19() { return static_cast<int32_t>(offsetof(MachineKeySection_t3377737715_StaticFields, ___validationProp_19)); }
	inline ConfigurationProperty_t3590861854 * get_validationProp_19() const { return ___validationProp_19; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_validationProp_19() { return &___validationProp_19; }
	inline void set_validationProp_19(ConfigurationProperty_t3590861854 * value)
	{
		___validationProp_19 = value;
		Il2CppCodeGenWriteBarrier(&___validationProp_19, value);
	}

	inline static int32_t get_offset_of_validationKeyProp_20() { return static_cast<int32_t>(offsetof(MachineKeySection_t3377737715_StaticFields, ___validationKeyProp_20)); }
	inline ConfigurationProperty_t3590861854 * get_validationKeyProp_20() const { return ___validationKeyProp_20; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_validationKeyProp_20() { return &___validationKeyProp_20; }
	inline void set_validationKeyProp_20(ConfigurationProperty_t3590861854 * value)
	{
		___validationKeyProp_20 = value;
		Il2CppCodeGenWriteBarrier(&___validationKeyProp_20, value);
	}

	inline static int32_t get_offset_of_properties_21() { return static_cast<int32_t>(offsetof(MachineKeySection_t3377737715_StaticFields, ___properties_21)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_21() const { return ___properties_21; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_21() { return &___properties_21; }
	inline void set_properties_21(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_21 = value;
		Il2CppCodeGenWriteBarrier(&___properties_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
