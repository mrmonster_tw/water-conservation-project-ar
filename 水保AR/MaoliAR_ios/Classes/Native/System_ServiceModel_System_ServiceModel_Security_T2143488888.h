﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Security_T3240244108.h"

// System.ServiceModel.Security.Tokens.SpnegoSecurityTokenAuthenticator
struct SpnegoSecurityTokenAuthenticator_t485325505;
// System.ServiceModel.Description.WSTrustSecurityTokenServiceProxy
struct WSTrustSecurityTokenServiceProxy_t2160987012;
// System.Collections.Generic.Dictionary`2<System.String,System.ServiceModel.Security.SspiServerSession>
struct Dictionary_2_t1862209182;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.SpnegoAuthenticatorCommunicationObject
struct  SpnegoAuthenticatorCommunicationObject_t2143488888  : public AuthenticatorCommunicationObject_t3240244108
{
public:
	// System.ServiceModel.Security.Tokens.SpnegoSecurityTokenAuthenticator System.ServiceModel.Security.Tokens.SpnegoAuthenticatorCommunicationObject::owner
	SpnegoSecurityTokenAuthenticator_t485325505 * ___owner_16;
	// System.ServiceModel.Description.WSTrustSecurityTokenServiceProxy System.ServiceModel.Security.Tokens.SpnegoAuthenticatorCommunicationObject::proxy
	WSTrustSecurityTokenServiceProxy_t2160987012 * ___proxy_17;
	// System.Collections.Generic.Dictionary`2<System.String,System.ServiceModel.Security.SspiServerSession> System.ServiceModel.Security.Tokens.SpnegoAuthenticatorCommunicationObject::sessions
	Dictionary_2_t1862209182 * ___sessions_18;

public:
	inline static int32_t get_offset_of_owner_16() { return static_cast<int32_t>(offsetof(SpnegoAuthenticatorCommunicationObject_t2143488888, ___owner_16)); }
	inline SpnegoSecurityTokenAuthenticator_t485325505 * get_owner_16() const { return ___owner_16; }
	inline SpnegoSecurityTokenAuthenticator_t485325505 ** get_address_of_owner_16() { return &___owner_16; }
	inline void set_owner_16(SpnegoSecurityTokenAuthenticator_t485325505 * value)
	{
		___owner_16 = value;
		Il2CppCodeGenWriteBarrier(&___owner_16, value);
	}

	inline static int32_t get_offset_of_proxy_17() { return static_cast<int32_t>(offsetof(SpnegoAuthenticatorCommunicationObject_t2143488888, ___proxy_17)); }
	inline WSTrustSecurityTokenServiceProxy_t2160987012 * get_proxy_17() const { return ___proxy_17; }
	inline WSTrustSecurityTokenServiceProxy_t2160987012 ** get_address_of_proxy_17() { return &___proxy_17; }
	inline void set_proxy_17(WSTrustSecurityTokenServiceProxy_t2160987012 * value)
	{
		___proxy_17 = value;
		Il2CppCodeGenWriteBarrier(&___proxy_17, value);
	}

	inline static int32_t get_offset_of_sessions_18() { return static_cast<int32_t>(offsetof(SpnegoAuthenticatorCommunicationObject_t2143488888, ___sessions_18)); }
	inline Dictionary_2_t1862209182 * get_sessions_18() const { return ___sessions_18; }
	inline Dictionary_2_t1862209182 ** get_address_of_sessions_18() { return &___sessions_18; }
	inline void set_sessions_18(Dictionary_2_t1862209182 * value)
	{
		___sessions_18 = value;
		Il2CppCodeGenWriteBarrier(&___sessions_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
