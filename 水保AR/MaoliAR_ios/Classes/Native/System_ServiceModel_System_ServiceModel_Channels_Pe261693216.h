﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_T2144904149.h"

// System.ServiceModel.PeerSecuritySettings
struct PeerSecuritySettings_t2125602112;
// System.Net.IPAddress
struct IPAddress_t241777590;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.PeerTransportBindingElement
struct  PeerTransportBindingElement_t261693216  : public TransportBindingElement_t2144904149
{
public:
	// System.Int64 System.ServiceModel.Channels.PeerTransportBindingElement::max_recv_message_size
	int64_t ___max_recv_message_size_3;
	// System.Int32 System.ServiceModel.Channels.PeerTransportBindingElement::port
	int32_t ___port_4;
	// System.ServiceModel.PeerSecuritySettings System.ServiceModel.Channels.PeerTransportBindingElement::security
	PeerSecuritySettings_t2125602112 * ___security_5;
	// System.Net.IPAddress System.ServiceModel.Channels.PeerTransportBindingElement::<ListenIPAddress>k__BackingField
	IPAddress_t241777590 * ___U3CListenIPAddressU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_max_recv_message_size_3() { return static_cast<int32_t>(offsetof(PeerTransportBindingElement_t261693216, ___max_recv_message_size_3)); }
	inline int64_t get_max_recv_message_size_3() const { return ___max_recv_message_size_3; }
	inline int64_t* get_address_of_max_recv_message_size_3() { return &___max_recv_message_size_3; }
	inline void set_max_recv_message_size_3(int64_t value)
	{
		___max_recv_message_size_3 = value;
	}

	inline static int32_t get_offset_of_port_4() { return static_cast<int32_t>(offsetof(PeerTransportBindingElement_t261693216, ___port_4)); }
	inline int32_t get_port_4() const { return ___port_4; }
	inline int32_t* get_address_of_port_4() { return &___port_4; }
	inline void set_port_4(int32_t value)
	{
		___port_4 = value;
	}

	inline static int32_t get_offset_of_security_5() { return static_cast<int32_t>(offsetof(PeerTransportBindingElement_t261693216, ___security_5)); }
	inline PeerSecuritySettings_t2125602112 * get_security_5() const { return ___security_5; }
	inline PeerSecuritySettings_t2125602112 ** get_address_of_security_5() { return &___security_5; }
	inline void set_security_5(PeerSecuritySettings_t2125602112 * value)
	{
		___security_5 = value;
		Il2CppCodeGenWriteBarrier(&___security_5, value);
	}

	inline static int32_t get_offset_of_U3CListenIPAddressU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PeerTransportBindingElement_t261693216, ___U3CListenIPAddressU3Ek__BackingField_6)); }
	inline IPAddress_t241777590 * get_U3CListenIPAddressU3Ek__BackingField_6() const { return ___U3CListenIPAddressU3Ek__BackingField_6; }
	inline IPAddress_t241777590 ** get_address_of_U3CListenIPAddressU3Ek__BackingField_6() { return &___U3CListenIPAddressU3Ek__BackingField_6; }
	inline void set_U3CListenIPAddressU3Ek__BackingField_6(IPAddress_t241777590 * value)
	{
		___U3CListenIPAddressU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CListenIPAddressU3Ek__BackingField_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
