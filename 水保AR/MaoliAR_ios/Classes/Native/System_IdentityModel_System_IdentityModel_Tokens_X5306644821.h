﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Tokens_A3970863348.h"

// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t714049126;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Tokens.X509AsymmetricSecurityKey
struct  X509AsymmetricSecurityKey_t306644821  : public AsymmetricSecurityKey_t3970863348
{
public:
	// System.Security.Cryptography.X509Certificates.X509Certificate2 System.IdentityModel.Tokens.X509AsymmetricSecurityKey::cert
	X509Certificate2_t714049126 * ___cert_0;

public:
	inline static int32_t get_offset_of_cert_0() { return static_cast<int32_t>(offsetof(X509AsymmetricSecurityKey_t306644821, ___cert_0)); }
	inline X509Certificate2_t714049126 * get_cert_0() const { return ___cert_0; }
	inline X509Certificate2_t714049126 ** get_address_of_cert_0() { return &___cert_0; }
	inline void set_cert_0(X509Certificate2_t714049126 * value)
	{
		___cert_0 = value;
		Il2CppCodeGenWriteBarrier(&___cert_0, value);
	}
};

struct X509AsymmetricSecurityKey_t306644821_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.IdentityModel.Tokens.X509AsymmetricSecurityKey::<>f__switch$map6
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map6_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.IdentityModel.Tokens.X509AsymmetricSecurityKey::<>f__switch$map8
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map8_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.IdentityModel.Tokens.X509AsymmetricSecurityKey::<>f__switch$map9
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map9_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.IdentityModel.Tokens.X509AsymmetricSecurityKey::<>f__switch$mapA
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24mapA_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map6_1() { return static_cast<int32_t>(offsetof(X509AsymmetricSecurityKey_t306644821_StaticFields, ___U3CU3Ef__switchU24map6_1)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map6_1() const { return ___U3CU3Ef__switchU24map6_1; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map6_1() { return &___U3CU3Ef__switchU24map6_1; }
	inline void set_U3CU3Ef__switchU24map6_1(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map6_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map6_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map8_2() { return static_cast<int32_t>(offsetof(X509AsymmetricSecurityKey_t306644821_StaticFields, ___U3CU3Ef__switchU24map8_2)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map8_2() const { return ___U3CU3Ef__switchU24map8_2; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map8_2() { return &___U3CU3Ef__switchU24map8_2; }
	inline void set_U3CU3Ef__switchU24map8_2(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map8_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map8_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map9_3() { return static_cast<int32_t>(offsetof(X509AsymmetricSecurityKey_t306644821_StaticFields, ___U3CU3Ef__switchU24map9_3)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map9_3() const { return ___U3CU3Ef__switchU24map9_3; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map9_3() { return &___U3CU3Ef__switchU24map9_3; }
	inline void set_U3CU3Ef__switchU24map9_3(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map9_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map9_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapA_4() { return static_cast<int32_t>(offsetof(X509AsymmetricSecurityKey_t306644821_StaticFields, ___U3CU3Ef__switchU24mapA_4)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24mapA_4() const { return ___U3CU3Ef__switchU24mapA_4; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24mapA_4() { return &___U3CU3Ef__switchU24mapA_4; }
	inline void set_U3CU3Ef__switchU24mapA_4(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24mapA_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapA_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
