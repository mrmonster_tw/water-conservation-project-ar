﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.Util.SearchPattern/Op
struct Op_t1428817098;
// System.Char[]
struct CharU5BU5D_t3528271667;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Util.SearchPattern
struct  SearchPattern_t3593873567  : public Il2CppObject
{
public:
	// System.Web.Util.SearchPattern/Op System.Web.Util.SearchPattern::ops
	Op_t1428817098 * ___ops_0;
	// System.Boolean System.Web.Util.SearchPattern::ignore
	bool ___ignore_1;

public:
	inline static int32_t get_offset_of_ops_0() { return static_cast<int32_t>(offsetof(SearchPattern_t3593873567, ___ops_0)); }
	inline Op_t1428817098 * get_ops_0() const { return ___ops_0; }
	inline Op_t1428817098 ** get_address_of_ops_0() { return &___ops_0; }
	inline void set_ops_0(Op_t1428817098 * value)
	{
		___ops_0 = value;
		Il2CppCodeGenWriteBarrier(&___ops_0, value);
	}

	inline static int32_t get_offset_of_ignore_1() { return static_cast<int32_t>(offsetof(SearchPattern_t3593873567, ___ignore_1)); }
	inline bool get_ignore_1() const { return ___ignore_1; }
	inline bool* get_address_of_ignore_1() { return &___ignore_1; }
	inline void set_ignore_1(bool value)
	{
		___ignore_1 = value;
	}
};

struct SearchPattern_t3593873567_StaticFields
{
public:
	// System.Char[] System.Web.Util.SearchPattern::WildcardChars
	CharU5BU5D_t3528271667* ___WildcardChars_2;

public:
	inline static int32_t get_offset_of_WildcardChars_2() { return static_cast<int32_t>(offsetof(SearchPattern_t3593873567_StaticFields, ___WildcardChars_2)); }
	inline CharU5BU5D_t3528271667* get_WildcardChars_2() const { return ___WildcardChars_2; }
	inline CharU5BU5D_t3528271667** get_address_of_WildcardChars_2() { return &___WildcardChars_2; }
	inline void set_WildcardChars_2(CharU5BU5D_t3528271667* value)
	{
		___WildcardChars_2 = value;
		Il2CppCodeGenWriteBarrier(&___WildcardChars_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
