﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_PeerResolv1567980581.h"
#include "mscorlib_System_Guid3193532887.h"

// System.ServiceModel.Channels.ICustomPeerResolverClient
struct ICustomPeerResolverClient_t543357795;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.PeerCustomResolver
struct  PeerCustomResolver_t3922737138  : public PeerResolver_t1567980581
{
public:
	// System.Guid System.ServiceModel.Channels.PeerCustomResolver::client_id
	Guid_t  ___client_id_0;
	// System.ServiceModel.Channels.ICustomPeerResolverClient System.ServiceModel.Channels.PeerCustomResolver::client
	Il2CppObject * ___client_1;
	// System.String System.ServiceModel.Channels.PeerCustomResolver::preserved_mesh_id
	String_t* ___preserved_mesh_id_2;

public:
	inline static int32_t get_offset_of_client_id_0() { return static_cast<int32_t>(offsetof(PeerCustomResolver_t3922737138, ___client_id_0)); }
	inline Guid_t  get_client_id_0() const { return ___client_id_0; }
	inline Guid_t * get_address_of_client_id_0() { return &___client_id_0; }
	inline void set_client_id_0(Guid_t  value)
	{
		___client_id_0 = value;
	}

	inline static int32_t get_offset_of_client_1() { return static_cast<int32_t>(offsetof(PeerCustomResolver_t3922737138, ___client_1)); }
	inline Il2CppObject * get_client_1() const { return ___client_1; }
	inline Il2CppObject ** get_address_of_client_1() { return &___client_1; }
	inline void set_client_1(Il2CppObject * value)
	{
		___client_1 = value;
		Il2CppCodeGenWriteBarrier(&___client_1, value);
	}

	inline static int32_t get_offset_of_preserved_mesh_id_2() { return static_cast<int32_t>(offsetof(PeerCustomResolver_t3922737138, ___preserved_mesh_id_2)); }
	inline String_t* get_preserved_mesh_id_2() const { return ___preserved_mesh_id_2; }
	inline String_t** get_address_of_preserved_mesh_id_2() { return &___preserved_mesh_id_2; }
	inline void set_preserved_mesh_id_2(String_t* value)
	{
		___preserved_mesh_id_2 = value;
		Il2CppCodeGenWriteBarrier(&___preserved_mesh_id_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
