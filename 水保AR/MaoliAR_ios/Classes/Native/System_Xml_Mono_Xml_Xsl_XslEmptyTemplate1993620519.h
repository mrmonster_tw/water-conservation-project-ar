﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_XslTemplate152263049.h"

// Mono.Xml.Xsl.XslEmptyTemplate
struct XslEmptyTemplate_t1993620519;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.XslEmptyTemplate
struct  XslEmptyTemplate_t1993620519  : public XslTemplate_t152263049
{
public:

public:
};

struct XslEmptyTemplate_t1993620519_StaticFields
{
public:
	// Mono.Xml.Xsl.XslEmptyTemplate Mono.Xml.Xsl.XslEmptyTemplate::instance
	XslEmptyTemplate_t1993620519 * ___instance_10;

public:
	inline static int32_t get_offset_of_instance_10() { return static_cast<int32_t>(offsetof(XslEmptyTemplate_t1993620519_StaticFields, ___instance_10)); }
	inline XslEmptyTemplate_t1993620519 * get_instance_10() const { return ___instance_10; }
	inline XslEmptyTemplate_t1993620519 ** get_address_of_instance_10() { return &___instance_10; }
	inline void set_instance_10(XslEmptyTemplate_t1993620519 * value)
	{
		___instance_10 = value;
		Il2CppCodeGenWriteBarrier(&___instance_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
