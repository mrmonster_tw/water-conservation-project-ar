﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_Microsoft_Win32_SafeHandles_SafeHandleZer1182193648.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.Win32.SafeHandles.SafePipeHandle
struct  SafePipeHandle_t1989113880  : public SafeHandleZeroOrMinusOneIsInvalid_t1182193648
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
