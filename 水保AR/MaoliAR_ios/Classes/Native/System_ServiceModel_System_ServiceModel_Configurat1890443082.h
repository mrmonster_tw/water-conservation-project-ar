﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.IssuedTokenClientBehaviorsElement
struct  IssuedTokenClientBehaviorsElement_t1890443082  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct IssuedTokenClientBehaviorsElement_t1890443082_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.IssuedTokenClientBehaviorsElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.IssuedTokenClientBehaviorsElement::behavior_configuration
	ConfigurationProperty_t3590861854 * ___behavior_configuration_14;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.IssuedTokenClientBehaviorsElement::issuer_address
	ConfigurationProperty_t3590861854 * ___issuer_address_15;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(IssuedTokenClientBehaviorsElement_t1890443082_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier(&___properties_13, value);
	}

	inline static int32_t get_offset_of_behavior_configuration_14() { return static_cast<int32_t>(offsetof(IssuedTokenClientBehaviorsElement_t1890443082_StaticFields, ___behavior_configuration_14)); }
	inline ConfigurationProperty_t3590861854 * get_behavior_configuration_14() const { return ___behavior_configuration_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_behavior_configuration_14() { return &___behavior_configuration_14; }
	inline void set_behavior_configuration_14(ConfigurationProperty_t3590861854 * value)
	{
		___behavior_configuration_14 = value;
		Il2CppCodeGenWriteBarrier(&___behavior_configuration_14, value);
	}

	inline static int32_t get_offset_of_issuer_address_15() { return static_cast<int32_t>(offsetof(IssuedTokenClientBehaviorsElement_t1890443082_StaticFields, ___issuer_address_15)); }
	inline ConfigurationProperty_t3590861854 * get_issuer_address_15() const { return ___issuer_address_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_issuer_address_15() { return &___issuer_address_15; }
	inline void set_issuer_address_15(ConfigurationProperty_t3590861854 * value)
	{
		___issuer_address_15 = value;
		Il2CppCodeGenWriteBarrier(&___issuer_address_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
