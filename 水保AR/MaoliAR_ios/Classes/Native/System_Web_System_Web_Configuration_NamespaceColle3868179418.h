﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configura446763386.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.NamespaceCollection
struct  NamespaceCollection_t3868179418  : public ConfigurationElementCollection_t446763386
{
public:

public:
};

struct NamespaceCollection_t3868179418_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Web.Configuration.NamespaceCollection::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_23;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.NamespaceCollection::autoImportVBNamespaceProp
	ConfigurationProperty_t3590861854 * ___autoImportVBNamespaceProp_24;

public:
	inline static int32_t get_offset_of_properties_23() { return static_cast<int32_t>(offsetof(NamespaceCollection_t3868179418_StaticFields, ___properties_23)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_23() const { return ___properties_23; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_23() { return &___properties_23; }
	inline void set_properties_23(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_23 = value;
		Il2CppCodeGenWriteBarrier(&___properties_23, value);
	}

	inline static int32_t get_offset_of_autoImportVBNamespaceProp_24() { return static_cast<int32_t>(offsetof(NamespaceCollection_t3868179418_StaticFields, ___autoImportVBNamespaceProp_24)); }
	inline ConfigurationProperty_t3590861854 * get_autoImportVBNamespaceProp_24() const { return ___autoImportVBNamespaceProp_24; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_autoImportVBNamespaceProp_24() { return &___autoImportVBNamespaceProp_24; }
	inline void set_autoImportVBNamespaceProp_24(ConfigurationProperty_t3590861854 * value)
	{
		___autoImportVBNamespaceProp_24 = value;
		Il2CppCodeGenWriteBarrier(&___autoImportVBNamespaceProp_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
