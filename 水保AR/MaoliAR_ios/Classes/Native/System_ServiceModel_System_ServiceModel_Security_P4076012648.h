﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Security.X509PeerCertificateAuthentication
struct X509PeerCertificateAuthentication_t406335588;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.PeerCredential
struct  PeerCredential_t4076012648  : public Il2CppObject
{
public:
	// System.ServiceModel.Security.X509PeerCertificateAuthentication System.ServiceModel.Security.PeerCredential::cert_auth
	X509PeerCertificateAuthentication_t406335588 * ___cert_auth_0;
	// System.ServiceModel.Security.X509PeerCertificateAuthentication System.ServiceModel.Security.PeerCredential::peer_auth
	X509PeerCertificateAuthentication_t406335588 * ___peer_auth_1;

public:
	inline static int32_t get_offset_of_cert_auth_0() { return static_cast<int32_t>(offsetof(PeerCredential_t4076012648, ___cert_auth_0)); }
	inline X509PeerCertificateAuthentication_t406335588 * get_cert_auth_0() const { return ___cert_auth_0; }
	inline X509PeerCertificateAuthentication_t406335588 ** get_address_of_cert_auth_0() { return &___cert_auth_0; }
	inline void set_cert_auth_0(X509PeerCertificateAuthentication_t406335588 * value)
	{
		___cert_auth_0 = value;
		Il2CppCodeGenWriteBarrier(&___cert_auth_0, value);
	}

	inline static int32_t get_offset_of_peer_auth_1() { return static_cast<int32_t>(offsetof(PeerCredential_t4076012648, ___peer_auth_1)); }
	inline X509PeerCertificateAuthentication_t406335588 * get_peer_auth_1() const { return ___peer_auth_1; }
	inline X509PeerCertificateAuthentication_t406335588 ** get_address_of_peer_auth_1() { return &___peer_auth_1; }
	inline void set_peer_auth_1(X509PeerCertificateAuthentication_t406335588 * value)
	{
		___peer_auth_1 = value;
		Il2CppCodeGenWriteBarrier(&___peer_auth_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
