﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3156163955.h"

// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.TraceSection
struct  TraceSection_t3447155379  : public ConfigurationSection_t3156163955
{
public:

public:
};

struct TraceSection_t3447155379_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Web.Configuration.TraceSection::enabledProp
	ConfigurationProperty_t3590861854 * ___enabledProp_17;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.TraceSection::localOnlyProp
	ConfigurationProperty_t3590861854 * ___localOnlyProp_18;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.TraceSection::mostRecentProp
	ConfigurationProperty_t3590861854 * ___mostRecentProp_19;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.TraceSection::pageOutputProp
	ConfigurationProperty_t3590861854 * ___pageOutputProp_20;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.TraceSection::requestLimitProp
	ConfigurationProperty_t3590861854 * ___requestLimitProp_21;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.TraceSection::traceModeProp
	ConfigurationProperty_t3590861854 * ___traceModeProp_22;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.TraceSection::writeToDiagnosticsTraceProp
	ConfigurationProperty_t3590861854 * ___writeToDiagnosticsTraceProp_23;
	// System.Configuration.ConfigurationPropertyCollection System.Web.Configuration.TraceSection::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_24;

public:
	inline static int32_t get_offset_of_enabledProp_17() { return static_cast<int32_t>(offsetof(TraceSection_t3447155379_StaticFields, ___enabledProp_17)); }
	inline ConfigurationProperty_t3590861854 * get_enabledProp_17() const { return ___enabledProp_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_enabledProp_17() { return &___enabledProp_17; }
	inline void set_enabledProp_17(ConfigurationProperty_t3590861854 * value)
	{
		___enabledProp_17 = value;
		Il2CppCodeGenWriteBarrier(&___enabledProp_17, value);
	}

	inline static int32_t get_offset_of_localOnlyProp_18() { return static_cast<int32_t>(offsetof(TraceSection_t3447155379_StaticFields, ___localOnlyProp_18)); }
	inline ConfigurationProperty_t3590861854 * get_localOnlyProp_18() const { return ___localOnlyProp_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_localOnlyProp_18() { return &___localOnlyProp_18; }
	inline void set_localOnlyProp_18(ConfigurationProperty_t3590861854 * value)
	{
		___localOnlyProp_18 = value;
		Il2CppCodeGenWriteBarrier(&___localOnlyProp_18, value);
	}

	inline static int32_t get_offset_of_mostRecentProp_19() { return static_cast<int32_t>(offsetof(TraceSection_t3447155379_StaticFields, ___mostRecentProp_19)); }
	inline ConfigurationProperty_t3590861854 * get_mostRecentProp_19() const { return ___mostRecentProp_19; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_mostRecentProp_19() { return &___mostRecentProp_19; }
	inline void set_mostRecentProp_19(ConfigurationProperty_t3590861854 * value)
	{
		___mostRecentProp_19 = value;
		Il2CppCodeGenWriteBarrier(&___mostRecentProp_19, value);
	}

	inline static int32_t get_offset_of_pageOutputProp_20() { return static_cast<int32_t>(offsetof(TraceSection_t3447155379_StaticFields, ___pageOutputProp_20)); }
	inline ConfigurationProperty_t3590861854 * get_pageOutputProp_20() const { return ___pageOutputProp_20; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_pageOutputProp_20() { return &___pageOutputProp_20; }
	inline void set_pageOutputProp_20(ConfigurationProperty_t3590861854 * value)
	{
		___pageOutputProp_20 = value;
		Il2CppCodeGenWriteBarrier(&___pageOutputProp_20, value);
	}

	inline static int32_t get_offset_of_requestLimitProp_21() { return static_cast<int32_t>(offsetof(TraceSection_t3447155379_StaticFields, ___requestLimitProp_21)); }
	inline ConfigurationProperty_t3590861854 * get_requestLimitProp_21() const { return ___requestLimitProp_21; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_requestLimitProp_21() { return &___requestLimitProp_21; }
	inline void set_requestLimitProp_21(ConfigurationProperty_t3590861854 * value)
	{
		___requestLimitProp_21 = value;
		Il2CppCodeGenWriteBarrier(&___requestLimitProp_21, value);
	}

	inline static int32_t get_offset_of_traceModeProp_22() { return static_cast<int32_t>(offsetof(TraceSection_t3447155379_StaticFields, ___traceModeProp_22)); }
	inline ConfigurationProperty_t3590861854 * get_traceModeProp_22() const { return ___traceModeProp_22; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_traceModeProp_22() { return &___traceModeProp_22; }
	inline void set_traceModeProp_22(ConfigurationProperty_t3590861854 * value)
	{
		___traceModeProp_22 = value;
		Il2CppCodeGenWriteBarrier(&___traceModeProp_22, value);
	}

	inline static int32_t get_offset_of_writeToDiagnosticsTraceProp_23() { return static_cast<int32_t>(offsetof(TraceSection_t3447155379_StaticFields, ___writeToDiagnosticsTraceProp_23)); }
	inline ConfigurationProperty_t3590861854 * get_writeToDiagnosticsTraceProp_23() const { return ___writeToDiagnosticsTraceProp_23; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_writeToDiagnosticsTraceProp_23() { return &___writeToDiagnosticsTraceProp_23; }
	inline void set_writeToDiagnosticsTraceProp_23(ConfigurationProperty_t3590861854 * value)
	{
		___writeToDiagnosticsTraceProp_23 = value;
		Il2CppCodeGenWriteBarrier(&___writeToDiagnosticsTraceProp_23, value);
	}

	inline static int32_t get_offset_of_properties_24() { return static_cast<int32_t>(offsetof(TraceSection_t3447155379_StaticFields, ___properties_24)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_24() const { return ___properties_24; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_24() { return &___properties_24; }
	inline void set_properties_24(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_24 = value;
		Il2CppCodeGenWriteBarrier(&___properties_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
