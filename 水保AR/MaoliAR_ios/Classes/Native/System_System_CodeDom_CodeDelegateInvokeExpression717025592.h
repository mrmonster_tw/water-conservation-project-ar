﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeExpression2166265795.h"

// System.CodeDom.CodeExpressionCollection
struct CodeExpressionCollection_t2370433003;
// System.CodeDom.CodeExpression
struct CodeExpression_t2166265795;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeDelegateInvokeExpression
struct  CodeDelegateInvokeExpression_t717025592  : public CodeExpression_t2166265795
{
public:
	// System.CodeDom.CodeExpressionCollection System.CodeDom.CodeDelegateInvokeExpression::parameters
	CodeExpressionCollection_t2370433003 * ___parameters_1;
	// System.CodeDom.CodeExpression System.CodeDom.CodeDelegateInvokeExpression::targetObject
	CodeExpression_t2166265795 * ___targetObject_2;

public:
	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(CodeDelegateInvokeExpression_t717025592, ___parameters_1)); }
	inline CodeExpressionCollection_t2370433003 * get_parameters_1() const { return ___parameters_1; }
	inline CodeExpressionCollection_t2370433003 ** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(CodeExpressionCollection_t2370433003 * value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_1, value);
	}

	inline static int32_t get_offset_of_targetObject_2() { return static_cast<int32_t>(offsetof(CodeDelegateInvokeExpression_t717025592, ___targetObject_2)); }
	inline CodeExpression_t2166265795 * get_targetObject_2() const { return ___targetObject_2; }
	inline CodeExpression_t2166265795 ** get_address_of_targetObject_2() { return &___targetObject_2; }
	inline void set_targetObject_2(CodeExpression_t2166265795 * value)
	{
		___targetObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___targetObject_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
