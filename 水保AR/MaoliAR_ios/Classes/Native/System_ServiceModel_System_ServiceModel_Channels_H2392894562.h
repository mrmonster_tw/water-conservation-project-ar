﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_T2144904149.h"
#include "System_ServiceModel_System_ServiceModel_HostNameCo1351856580.h"
#include "System_ServiceModel_System_ServiceModel_TransferMod436880017.h"
#include "System_System_Net_AuthenticationSchemes3459406435.h"

// System.Uri
struct Uri_t100236324;
// System.String
struct String_t;
// System.ServiceModel.IDefaultCommunicationTimeouts
struct IDefaultCommunicationTimeouts_t2848643016;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.HttpTransportBindingElement
struct  HttpTransportBindingElement_t2392894562  : public TransportBindingElement_t2144904149
{
public:
	// System.Boolean System.ServiceModel.Channels.HttpTransportBindingElement::allow_cookies
	bool ___allow_cookies_3;
	// System.Boolean System.ServiceModel.Channels.HttpTransportBindingElement::bypass_proxy_on_local
	bool ___bypass_proxy_on_local_4;
	// System.Boolean System.ServiceModel.Channels.HttpTransportBindingElement::unsafe_ntlm_auth
	bool ___unsafe_ntlm_auth_5;
	// System.Boolean System.ServiceModel.Channels.HttpTransportBindingElement::use_default_proxy
	bool ___use_default_proxy_6;
	// System.Boolean System.ServiceModel.Channels.HttpTransportBindingElement::keep_alive_enabled
	bool ___keep_alive_enabled_7;
	// System.Int32 System.ServiceModel.Channels.HttpTransportBindingElement::max_buffer_size
	int32_t ___max_buffer_size_8;
	// System.ServiceModel.HostNameComparisonMode System.ServiceModel.Channels.HttpTransportBindingElement::host_cmp_mode
	int32_t ___host_cmp_mode_9;
	// System.Uri System.ServiceModel.Channels.HttpTransportBindingElement::proxy_address
	Uri_t100236324 * ___proxy_address_10;
	// System.String System.ServiceModel.Channels.HttpTransportBindingElement::realm
	String_t* ___realm_11;
	// System.ServiceModel.TransferMode System.ServiceModel.Channels.HttpTransportBindingElement::transfer_mode
	int32_t ___transfer_mode_12;
	// System.ServiceModel.IDefaultCommunicationTimeouts System.ServiceModel.Channels.HttpTransportBindingElement::timeouts
	Il2CppObject * ___timeouts_13;
	// System.Net.AuthenticationSchemes System.ServiceModel.Channels.HttpTransportBindingElement::auth_scheme
	int32_t ___auth_scheme_14;
	// System.Net.AuthenticationSchemes System.ServiceModel.Channels.HttpTransportBindingElement::proxy_auth_scheme
	int32_t ___proxy_auth_scheme_15;

public:
	inline static int32_t get_offset_of_allow_cookies_3() { return static_cast<int32_t>(offsetof(HttpTransportBindingElement_t2392894562, ___allow_cookies_3)); }
	inline bool get_allow_cookies_3() const { return ___allow_cookies_3; }
	inline bool* get_address_of_allow_cookies_3() { return &___allow_cookies_3; }
	inline void set_allow_cookies_3(bool value)
	{
		___allow_cookies_3 = value;
	}

	inline static int32_t get_offset_of_bypass_proxy_on_local_4() { return static_cast<int32_t>(offsetof(HttpTransportBindingElement_t2392894562, ___bypass_proxy_on_local_4)); }
	inline bool get_bypass_proxy_on_local_4() const { return ___bypass_proxy_on_local_4; }
	inline bool* get_address_of_bypass_proxy_on_local_4() { return &___bypass_proxy_on_local_4; }
	inline void set_bypass_proxy_on_local_4(bool value)
	{
		___bypass_proxy_on_local_4 = value;
	}

	inline static int32_t get_offset_of_unsafe_ntlm_auth_5() { return static_cast<int32_t>(offsetof(HttpTransportBindingElement_t2392894562, ___unsafe_ntlm_auth_5)); }
	inline bool get_unsafe_ntlm_auth_5() const { return ___unsafe_ntlm_auth_5; }
	inline bool* get_address_of_unsafe_ntlm_auth_5() { return &___unsafe_ntlm_auth_5; }
	inline void set_unsafe_ntlm_auth_5(bool value)
	{
		___unsafe_ntlm_auth_5 = value;
	}

	inline static int32_t get_offset_of_use_default_proxy_6() { return static_cast<int32_t>(offsetof(HttpTransportBindingElement_t2392894562, ___use_default_proxy_6)); }
	inline bool get_use_default_proxy_6() const { return ___use_default_proxy_6; }
	inline bool* get_address_of_use_default_proxy_6() { return &___use_default_proxy_6; }
	inline void set_use_default_proxy_6(bool value)
	{
		___use_default_proxy_6 = value;
	}

	inline static int32_t get_offset_of_keep_alive_enabled_7() { return static_cast<int32_t>(offsetof(HttpTransportBindingElement_t2392894562, ___keep_alive_enabled_7)); }
	inline bool get_keep_alive_enabled_7() const { return ___keep_alive_enabled_7; }
	inline bool* get_address_of_keep_alive_enabled_7() { return &___keep_alive_enabled_7; }
	inline void set_keep_alive_enabled_7(bool value)
	{
		___keep_alive_enabled_7 = value;
	}

	inline static int32_t get_offset_of_max_buffer_size_8() { return static_cast<int32_t>(offsetof(HttpTransportBindingElement_t2392894562, ___max_buffer_size_8)); }
	inline int32_t get_max_buffer_size_8() const { return ___max_buffer_size_8; }
	inline int32_t* get_address_of_max_buffer_size_8() { return &___max_buffer_size_8; }
	inline void set_max_buffer_size_8(int32_t value)
	{
		___max_buffer_size_8 = value;
	}

	inline static int32_t get_offset_of_host_cmp_mode_9() { return static_cast<int32_t>(offsetof(HttpTransportBindingElement_t2392894562, ___host_cmp_mode_9)); }
	inline int32_t get_host_cmp_mode_9() const { return ___host_cmp_mode_9; }
	inline int32_t* get_address_of_host_cmp_mode_9() { return &___host_cmp_mode_9; }
	inline void set_host_cmp_mode_9(int32_t value)
	{
		___host_cmp_mode_9 = value;
	}

	inline static int32_t get_offset_of_proxy_address_10() { return static_cast<int32_t>(offsetof(HttpTransportBindingElement_t2392894562, ___proxy_address_10)); }
	inline Uri_t100236324 * get_proxy_address_10() const { return ___proxy_address_10; }
	inline Uri_t100236324 ** get_address_of_proxy_address_10() { return &___proxy_address_10; }
	inline void set_proxy_address_10(Uri_t100236324 * value)
	{
		___proxy_address_10 = value;
		Il2CppCodeGenWriteBarrier(&___proxy_address_10, value);
	}

	inline static int32_t get_offset_of_realm_11() { return static_cast<int32_t>(offsetof(HttpTransportBindingElement_t2392894562, ___realm_11)); }
	inline String_t* get_realm_11() const { return ___realm_11; }
	inline String_t** get_address_of_realm_11() { return &___realm_11; }
	inline void set_realm_11(String_t* value)
	{
		___realm_11 = value;
		Il2CppCodeGenWriteBarrier(&___realm_11, value);
	}

	inline static int32_t get_offset_of_transfer_mode_12() { return static_cast<int32_t>(offsetof(HttpTransportBindingElement_t2392894562, ___transfer_mode_12)); }
	inline int32_t get_transfer_mode_12() const { return ___transfer_mode_12; }
	inline int32_t* get_address_of_transfer_mode_12() { return &___transfer_mode_12; }
	inline void set_transfer_mode_12(int32_t value)
	{
		___transfer_mode_12 = value;
	}

	inline static int32_t get_offset_of_timeouts_13() { return static_cast<int32_t>(offsetof(HttpTransportBindingElement_t2392894562, ___timeouts_13)); }
	inline Il2CppObject * get_timeouts_13() const { return ___timeouts_13; }
	inline Il2CppObject ** get_address_of_timeouts_13() { return &___timeouts_13; }
	inline void set_timeouts_13(Il2CppObject * value)
	{
		___timeouts_13 = value;
		Il2CppCodeGenWriteBarrier(&___timeouts_13, value);
	}

	inline static int32_t get_offset_of_auth_scheme_14() { return static_cast<int32_t>(offsetof(HttpTransportBindingElement_t2392894562, ___auth_scheme_14)); }
	inline int32_t get_auth_scheme_14() const { return ___auth_scheme_14; }
	inline int32_t* get_address_of_auth_scheme_14() { return &___auth_scheme_14; }
	inline void set_auth_scheme_14(int32_t value)
	{
		___auth_scheme_14 = value;
	}

	inline static int32_t get_offset_of_proxy_auth_scheme_15() { return static_cast<int32_t>(offsetof(HttpTransportBindingElement_t2392894562, ___proxy_auth_scheme_15)); }
	inline int32_t get_proxy_auth_scheme_15() const { return ___proxy_auth_scheme_15; }
	inline int32_t* get_address_of_proxy_auth_scheme_15() { return &___proxy_auth_scheme_15; }
	inline void set_proxy_auth_scheme_15(int32_t value)
	{
		___proxy_auth_scheme_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
