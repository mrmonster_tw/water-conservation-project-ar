﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Reflection.Emit.ModuleBuilder
struct ModuleBuilder_t731887691;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.CodeGeneration.CodeModule
struct  CodeModule_t2318530162  : public Il2CppObject
{
public:
	// System.Reflection.Emit.ModuleBuilder Mono.CodeGeneration.CodeModule::module
	ModuleBuilder_t731887691 * ___module_0;

public:
	inline static int32_t get_offset_of_module_0() { return static_cast<int32_t>(offsetof(CodeModule_t2318530162, ___module_0)); }
	inline ModuleBuilder_t731887691 * get_module_0() const { return ___module_0; }
	inline ModuleBuilder_t731887691 ** get_address_of_module_0() { return &___module_0; }
	inline void set_module_0(ModuleBuilder_t731887691 * value)
	{
		___module_0 = value;
		Il2CppCodeGenWriteBarrier(&___module_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
