﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Security_T2868958784.h"

// System.ServiceModel.Security.ChannelProtectionRequirements
struct ChannelProtectionRequirements_t2141883287;
// System.ServiceModel.Channels.BindingContext
struct BindingContext_t2842489830;
// System.ServiceModel.Channels.SecurityBindingElement
struct SecurityBindingElement_t1737011943;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.SecureConversationSecurityTokenParameters
struct  SecureConversationSecurityTokenParameters_t2961949979  : public SecurityTokenParameters_t2868958784
{
public:
	// System.ServiceModel.Channels.SecurityBindingElement System.ServiceModel.Security.Tokens.SecureConversationSecurityTokenParameters::element
	SecurityBindingElement_t1737011943 * ___element_6;
	// System.ServiceModel.Security.ChannelProtectionRequirements System.ServiceModel.Security.Tokens.SecureConversationSecurityTokenParameters::requirements
	ChannelProtectionRequirements_t2141883287 * ___requirements_7;
	// System.Boolean System.ServiceModel.Security.Tokens.SecureConversationSecurityTokenParameters::cancellable
	bool ___cancellable_8;

public:
	inline static int32_t get_offset_of_element_6() { return static_cast<int32_t>(offsetof(SecureConversationSecurityTokenParameters_t2961949979, ___element_6)); }
	inline SecurityBindingElement_t1737011943 * get_element_6() const { return ___element_6; }
	inline SecurityBindingElement_t1737011943 ** get_address_of_element_6() { return &___element_6; }
	inline void set_element_6(SecurityBindingElement_t1737011943 * value)
	{
		___element_6 = value;
		Il2CppCodeGenWriteBarrier(&___element_6, value);
	}

	inline static int32_t get_offset_of_requirements_7() { return static_cast<int32_t>(offsetof(SecureConversationSecurityTokenParameters_t2961949979, ___requirements_7)); }
	inline ChannelProtectionRequirements_t2141883287 * get_requirements_7() const { return ___requirements_7; }
	inline ChannelProtectionRequirements_t2141883287 ** get_address_of_requirements_7() { return &___requirements_7; }
	inline void set_requirements_7(ChannelProtectionRequirements_t2141883287 * value)
	{
		___requirements_7 = value;
		Il2CppCodeGenWriteBarrier(&___requirements_7, value);
	}

	inline static int32_t get_offset_of_cancellable_8() { return static_cast<int32_t>(offsetof(SecureConversationSecurityTokenParameters_t2961949979, ___cancellable_8)); }
	inline bool get_cancellable_8() const { return ___cancellable_8; }
	inline bool* get_address_of_cancellable_8() { return &___cancellable_8; }
	inline void set_cancellable_8(bool value)
	{
		___cancellable_8 = value;
	}
};

struct SecureConversationSecurityTokenParameters_t2961949979_StaticFields
{
public:
	// System.ServiceModel.Security.ChannelProtectionRequirements System.ServiceModel.Security.Tokens.SecureConversationSecurityTokenParameters::default_channel_protection_requirements
	ChannelProtectionRequirements_t2141883287 * ___default_channel_protection_requirements_4;
	// System.ServiceModel.Channels.BindingContext System.ServiceModel.Security.Tokens.SecureConversationSecurityTokenParameters::dummy_context
	BindingContext_t2842489830 * ___dummy_context_5;

public:
	inline static int32_t get_offset_of_default_channel_protection_requirements_4() { return static_cast<int32_t>(offsetof(SecureConversationSecurityTokenParameters_t2961949979_StaticFields, ___default_channel_protection_requirements_4)); }
	inline ChannelProtectionRequirements_t2141883287 * get_default_channel_protection_requirements_4() const { return ___default_channel_protection_requirements_4; }
	inline ChannelProtectionRequirements_t2141883287 ** get_address_of_default_channel_protection_requirements_4() { return &___default_channel_protection_requirements_4; }
	inline void set_default_channel_protection_requirements_4(ChannelProtectionRequirements_t2141883287 * value)
	{
		___default_channel_protection_requirements_4 = value;
		Il2CppCodeGenWriteBarrier(&___default_channel_protection_requirements_4, value);
	}

	inline static int32_t get_offset_of_dummy_context_5() { return static_cast<int32_t>(offsetof(SecureConversationSecurityTokenParameters_t2961949979_StaticFields, ___dummy_context_5)); }
	inline BindingContext_t2842489830 * get_dummy_context_5() const { return ___dummy_context_5; }
	inline BindingContext_t2842489830 ** get_address_of_dummy_context_5() { return &___dummy_context_5; }
	inline void set_dummy_context_5(BindingContext_t2842489830 * value)
	{
		___dummy_context_5 = value;
		Il2CppCodeGenWriteBarrier(&___dummy_context_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
