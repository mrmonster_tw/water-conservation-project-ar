﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Description.ServiceMetadataExtension
struct ServiceMetadataExtension_t3543702878;
// System.Collections.Generic.Dictionary`2<System.String,System.Web.Services.Description.ServiceDescription>
struct Dictionary_2_t3478960662;
// System.Collections.Generic.Dictionary`2<System.String,System.Xml.Schema.XmlSchema>
struct Dictionary_2_t3527814196;
// System.Uri
struct Uri_t100236324;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.HttpGetWsdl
struct  HttpGetWsdl_t3793260870  : public Il2CppObject
{
public:
	// System.ServiceModel.Description.ServiceMetadataExtension System.ServiceModel.Description.HttpGetWsdl::ext
	ServiceMetadataExtension_t3543702878 * ___ext_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Web.Services.Description.ServiceDescription> System.ServiceModel.Description.HttpGetWsdl::wsdl_documents
	Dictionary_2_t3478960662 * ___wsdl_documents_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Xml.Schema.XmlSchema> System.ServiceModel.Description.HttpGetWsdl::schemas
	Dictionary_2_t3527814196 * ___schemas_2;
	// System.Uri System.ServiceModel.Description.HttpGetWsdl::<HelpUrl>k__BackingField
	Uri_t100236324 * ___U3CHelpUrlU3Ek__BackingField_3;
	// System.Uri System.ServiceModel.Description.HttpGetWsdl::<WsdlUrl>k__BackingField
	Uri_t100236324 * ___U3CWsdlUrlU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_ext_0() { return static_cast<int32_t>(offsetof(HttpGetWsdl_t3793260870, ___ext_0)); }
	inline ServiceMetadataExtension_t3543702878 * get_ext_0() const { return ___ext_0; }
	inline ServiceMetadataExtension_t3543702878 ** get_address_of_ext_0() { return &___ext_0; }
	inline void set_ext_0(ServiceMetadataExtension_t3543702878 * value)
	{
		___ext_0 = value;
		Il2CppCodeGenWriteBarrier(&___ext_0, value);
	}

	inline static int32_t get_offset_of_wsdl_documents_1() { return static_cast<int32_t>(offsetof(HttpGetWsdl_t3793260870, ___wsdl_documents_1)); }
	inline Dictionary_2_t3478960662 * get_wsdl_documents_1() const { return ___wsdl_documents_1; }
	inline Dictionary_2_t3478960662 ** get_address_of_wsdl_documents_1() { return &___wsdl_documents_1; }
	inline void set_wsdl_documents_1(Dictionary_2_t3478960662 * value)
	{
		___wsdl_documents_1 = value;
		Il2CppCodeGenWriteBarrier(&___wsdl_documents_1, value);
	}

	inline static int32_t get_offset_of_schemas_2() { return static_cast<int32_t>(offsetof(HttpGetWsdl_t3793260870, ___schemas_2)); }
	inline Dictionary_2_t3527814196 * get_schemas_2() const { return ___schemas_2; }
	inline Dictionary_2_t3527814196 ** get_address_of_schemas_2() { return &___schemas_2; }
	inline void set_schemas_2(Dictionary_2_t3527814196 * value)
	{
		___schemas_2 = value;
		Il2CppCodeGenWriteBarrier(&___schemas_2, value);
	}

	inline static int32_t get_offset_of_U3CHelpUrlU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HttpGetWsdl_t3793260870, ___U3CHelpUrlU3Ek__BackingField_3)); }
	inline Uri_t100236324 * get_U3CHelpUrlU3Ek__BackingField_3() const { return ___U3CHelpUrlU3Ek__BackingField_3; }
	inline Uri_t100236324 ** get_address_of_U3CHelpUrlU3Ek__BackingField_3() { return &___U3CHelpUrlU3Ek__BackingField_3; }
	inline void set_U3CHelpUrlU3Ek__BackingField_3(Uri_t100236324 * value)
	{
		___U3CHelpUrlU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHelpUrlU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CWsdlUrlU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(HttpGetWsdl_t3793260870, ___U3CWsdlUrlU3Ek__BackingField_4)); }
	inline Uri_t100236324 * get_U3CWsdlUrlU3Ek__BackingField_4() const { return ___U3CWsdlUrlU3Ek__BackingField_4; }
	inline Uri_t100236324 ** get_address_of_U3CWsdlUrlU3Ek__BackingField_4() { return &___U3CWsdlUrlU3Ek__BackingField_4; }
	inline void set_U3CWsdlUrlU3Ek__BackingField_4(Uri_t100236324 * value)
	{
		___U3CWsdlUrlU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CWsdlUrlU3Ek__BackingField_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
