﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.FaultReasonText
struct  FaultReasonText_t3547351049  : public Il2CppObject
{
public:
	// System.String System.ServiceModel.FaultReasonText::text
	String_t* ___text_0;
	// System.String System.ServiceModel.FaultReasonText::xmllang
	String_t* ___xmllang_1;

public:
	inline static int32_t get_offset_of_text_0() { return static_cast<int32_t>(offsetof(FaultReasonText_t3547351049, ___text_0)); }
	inline String_t* get_text_0() const { return ___text_0; }
	inline String_t** get_address_of_text_0() { return &___text_0; }
	inline void set_text_0(String_t* value)
	{
		___text_0 = value;
		Il2CppCodeGenWriteBarrier(&___text_0, value);
	}

	inline static int32_t get_offset_of_xmllang_1() { return static_cast<int32_t>(offsetof(FaultReasonText_t3547351049, ___xmllang_1)); }
	inline String_t* get_xmllang_1() const { return ___xmllang_1; }
	inline String_t** get_address_of_xmllang_1() { return &___xmllang_1; }
	inline void set_xmllang_1(String_t* value)
	{
		___xmllang_1 = value;
		Il2CppCodeGenWriteBarrier(&___xmllang_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
