﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Channels.IChannel
struct IChannel_t937207545;
// System.ServiceModel.Dispatcher.ListenerLoopManager
struct ListenerLoopManager_t4138186672;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.ListenerLoopManager/<ChannelAccepted>c__AnonStorey1D
struct  U3CChannelAcceptedU3Ec__AnonStorey1D_t3926660875  : public Il2CppObject
{
public:
	// System.ServiceModel.Channels.IChannel System.ServiceModel.Dispatcher.ListenerLoopManager/<ChannelAccepted>c__AnonStorey1D::ch
	Il2CppObject * ___ch_0;
	// System.ServiceModel.Dispatcher.ListenerLoopManager System.ServiceModel.Dispatcher.ListenerLoopManager/<ChannelAccepted>c__AnonStorey1D::<>f__this
	ListenerLoopManager_t4138186672 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_ch_0() { return static_cast<int32_t>(offsetof(U3CChannelAcceptedU3Ec__AnonStorey1D_t3926660875, ___ch_0)); }
	inline Il2CppObject * get_ch_0() const { return ___ch_0; }
	inline Il2CppObject ** get_address_of_ch_0() { return &___ch_0; }
	inline void set_ch_0(Il2CppObject * value)
	{
		___ch_0 = value;
		Il2CppCodeGenWriteBarrier(&___ch_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CChannelAcceptedU3Ec__AnonStorey1D_t3926660875, ___U3CU3Ef__this_1)); }
	inline ListenerLoopManager_t4138186672 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline ListenerLoopManager_t4138186672 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(ListenerLoopManager_t4138186672 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
