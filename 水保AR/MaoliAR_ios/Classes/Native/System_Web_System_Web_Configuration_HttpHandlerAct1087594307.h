﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.HttpHandlerAction
struct  HttpHandlerAction_t1087594307  : public ConfigurationElement_t3318566633
{
public:
	// System.Object System.Web.Configuration.HttpHandlerAction::instance
	Il2CppObject * ___instance_18;
	// System.Type System.Web.Configuration.HttpHandlerAction::type
	Type_t * ___type_19;
	// System.String System.Web.Configuration.HttpHandlerAction::cached_verb
	String_t* ___cached_verb_20;
	// System.String[] System.Web.Configuration.HttpHandlerAction::cached_verbs
	StringU5BU5D_t1281789340* ___cached_verbs_21;

public:
	inline static int32_t get_offset_of_instance_18() { return static_cast<int32_t>(offsetof(HttpHandlerAction_t1087594307, ___instance_18)); }
	inline Il2CppObject * get_instance_18() const { return ___instance_18; }
	inline Il2CppObject ** get_address_of_instance_18() { return &___instance_18; }
	inline void set_instance_18(Il2CppObject * value)
	{
		___instance_18 = value;
		Il2CppCodeGenWriteBarrier(&___instance_18, value);
	}

	inline static int32_t get_offset_of_type_19() { return static_cast<int32_t>(offsetof(HttpHandlerAction_t1087594307, ___type_19)); }
	inline Type_t * get_type_19() const { return ___type_19; }
	inline Type_t ** get_address_of_type_19() { return &___type_19; }
	inline void set_type_19(Type_t * value)
	{
		___type_19 = value;
		Il2CppCodeGenWriteBarrier(&___type_19, value);
	}

	inline static int32_t get_offset_of_cached_verb_20() { return static_cast<int32_t>(offsetof(HttpHandlerAction_t1087594307, ___cached_verb_20)); }
	inline String_t* get_cached_verb_20() const { return ___cached_verb_20; }
	inline String_t** get_address_of_cached_verb_20() { return &___cached_verb_20; }
	inline void set_cached_verb_20(String_t* value)
	{
		___cached_verb_20 = value;
		Il2CppCodeGenWriteBarrier(&___cached_verb_20, value);
	}

	inline static int32_t get_offset_of_cached_verbs_21() { return static_cast<int32_t>(offsetof(HttpHandlerAction_t1087594307, ___cached_verbs_21)); }
	inline StringU5BU5D_t1281789340* get_cached_verbs_21() const { return ___cached_verbs_21; }
	inline StringU5BU5D_t1281789340** get_address_of_cached_verbs_21() { return &___cached_verbs_21; }
	inline void set_cached_verbs_21(StringU5BU5D_t1281789340* value)
	{
		___cached_verbs_21 = value;
		Il2CppCodeGenWriteBarrier(&___cached_verbs_21, value);
	}
};

struct HttpHandlerAction_t1087594307_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Web.Configuration.HttpHandlerAction::_properties
	ConfigurationPropertyCollection_t2852175726 * ____properties_13;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.HttpHandlerAction::pathProp
	ConfigurationProperty_t3590861854 * ___pathProp_14;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.HttpHandlerAction::typeProp
	ConfigurationProperty_t3590861854 * ___typeProp_15;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.HttpHandlerAction::validateProp
	ConfigurationProperty_t3590861854 * ___validateProp_16;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.HttpHandlerAction::verbProp
	ConfigurationProperty_t3590861854 * ___verbProp_17;

public:
	inline static int32_t get_offset_of__properties_13() { return static_cast<int32_t>(offsetof(HttpHandlerAction_t1087594307_StaticFields, ____properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get__properties_13() const { return ____properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of__properties_13() { return &____properties_13; }
	inline void set__properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		____properties_13 = value;
		Il2CppCodeGenWriteBarrier(&____properties_13, value);
	}

	inline static int32_t get_offset_of_pathProp_14() { return static_cast<int32_t>(offsetof(HttpHandlerAction_t1087594307_StaticFields, ___pathProp_14)); }
	inline ConfigurationProperty_t3590861854 * get_pathProp_14() const { return ___pathProp_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_pathProp_14() { return &___pathProp_14; }
	inline void set_pathProp_14(ConfigurationProperty_t3590861854 * value)
	{
		___pathProp_14 = value;
		Il2CppCodeGenWriteBarrier(&___pathProp_14, value);
	}

	inline static int32_t get_offset_of_typeProp_15() { return static_cast<int32_t>(offsetof(HttpHandlerAction_t1087594307_StaticFields, ___typeProp_15)); }
	inline ConfigurationProperty_t3590861854 * get_typeProp_15() const { return ___typeProp_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_typeProp_15() { return &___typeProp_15; }
	inline void set_typeProp_15(ConfigurationProperty_t3590861854 * value)
	{
		___typeProp_15 = value;
		Il2CppCodeGenWriteBarrier(&___typeProp_15, value);
	}

	inline static int32_t get_offset_of_validateProp_16() { return static_cast<int32_t>(offsetof(HttpHandlerAction_t1087594307_StaticFields, ___validateProp_16)); }
	inline ConfigurationProperty_t3590861854 * get_validateProp_16() const { return ___validateProp_16; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_validateProp_16() { return &___validateProp_16; }
	inline void set_validateProp_16(ConfigurationProperty_t3590861854 * value)
	{
		___validateProp_16 = value;
		Il2CppCodeGenWriteBarrier(&___validateProp_16, value);
	}

	inline static int32_t get_offset_of_verbProp_17() { return static_cast<int32_t>(offsetof(HttpHandlerAction_t1087594307_StaticFields, ___verbProp_17)); }
	inline ConfigurationProperty_t3590861854 * get_verbProp_17() const { return ___verbProp_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_verbProp_17() { return &___verbProp_17; }
	inline void set_verbProp_17(ConfigurationProperty_t3590861854 * value)
	{
		___verbProp_17 = value;
		Il2CppCodeGenWriteBarrier(&___verbProp_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
