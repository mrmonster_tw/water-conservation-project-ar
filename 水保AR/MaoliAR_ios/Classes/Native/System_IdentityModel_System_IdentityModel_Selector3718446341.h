﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Selector1375267704.h"

// System.Security.Cryptography.X509Certificates.X509ChainPolicy
struct X509ChainPolicy_t2426922870;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t194917408;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Selectors.X509CertificateValidator/X509CertificateValidatorImpl
struct  X509CertificateValidatorImpl_t3718446341  : public X509CertificateValidator_t1375267704
{
public:
	// System.Boolean System.IdentityModel.Selectors.X509CertificateValidator/X509CertificateValidatorImpl::check_peer
	bool ___check_peer_4;
	// System.Boolean System.IdentityModel.Selectors.X509CertificateValidator/X509CertificateValidatorImpl::check_chain
	bool ___check_chain_5;
	// System.Boolean System.IdentityModel.Selectors.X509CertificateValidator/X509CertificateValidatorImpl::use_machine_ctx
	bool ___use_machine_ctx_6;
	// System.Security.Cryptography.X509Certificates.X509ChainPolicy System.IdentityModel.Selectors.X509CertificateValidator/X509CertificateValidatorImpl::policy
	X509ChainPolicy_t2426922870 * ___policy_7;
	// System.Security.Cryptography.X509Certificates.X509Chain System.IdentityModel.Selectors.X509CertificateValidator/X509CertificateValidatorImpl::chain
	X509Chain_t194917408 * ___chain_8;

public:
	inline static int32_t get_offset_of_check_peer_4() { return static_cast<int32_t>(offsetof(X509CertificateValidatorImpl_t3718446341, ___check_peer_4)); }
	inline bool get_check_peer_4() const { return ___check_peer_4; }
	inline bool* get_address_of_check_peer_4() { return &___check_peer_4; }
	inline void set_check_peer_4(bool value)
	{
		___check_peer_4 = value;
	}

	inline static int32_t get_offset_of_check_chain_5() { return static_cast<int32_t>(offsetof(X509CertificateValidatorImpl_t3718446341, ___check_chain_5)); }
	inline bool get_check_chain_5() const { return ___check_chain_5; }
	inline bool* get_address_of_check_chain_5() { return &___check_chain_5; }
	inline void set_check_chain_5(bool value)
	{
		___check_chain_5 = value;
	}

	inline static int32_t get_offset_of_use_machine_ctx_6() { return static_cast<int32_t>(offsetof(X509CertificateValidatorImpl_t3718446341, ___use_machine_ctx_6)); }
	inline bool get_use_machine_ctx_6() const { return ___use_machine_ctx_6; }
	inline bool* get_address_of_use_machine_ctx_6() { return &___use_machine_ctx_6; }
	inline void set_use_machine_ctx_6(bool value)
	{
		___use_machine_ctx_6 = value;
	}

	inline static int32_t get_offset_of_policy_7() { return static_cast<int32_t>(offsetof(X509CertificateValidatorImpl_t3718446341, ___policy_7)); }
	inline X509ChainPolicy_t2426922870 * get_policy_7() const { return ___policy_7; }
	inline X509ChainPolicy_t2426922870 ** get_address_of_policy_7() { return &___policy_7; }
	inline void set_policy_7(X509ChainPolicy_t2426922870 * value)
	{
		___policy_7 = value;
		Il2CppCodeGenWriteBarrier(&___policy_7, value);
	}

	inline static int32_t get_offset_of_chain_8() { return static_cast<int32_t>(offsetof(X509CertificateValidatorImpl_t3718446341, ___chain_8)); }
	inline X509Chain_t194917408 * get_chain_8() const { return ___chain_8; }
	inline X509Chain_t194917408 ** get_address_of_chain_8() { return &___chain_8; }
	inline void set_chain_8(X509Chain_t194917408 * value)
	{
		___chain_8 = value;
		Il2CppCodeGenWriteBarrier(&___chain_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
