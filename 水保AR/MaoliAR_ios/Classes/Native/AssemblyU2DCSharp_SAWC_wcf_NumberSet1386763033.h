﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Runtime.Serialization.ExtensionDataObject
struct ExtensionDataObject_t285311256;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SAWC.wcf.NumberSet
struct  NumberSet_t1386763033  : public Il2CppObject
{
public:
	// System.Runtime.Serialization.ExtensionDataObject SAWC.wcf.NumberSet::extensionDataField
	ExtensionDataObject_t285311256 * ___extensionDataField_0;
	// System.Boolean SAWC.wcf.NumberSet::ExpiredField
	bool ___ExpiredField_1;
	// System.Int32 SAWC.wcf.NumberSet::NumberField
	int32_t ___NumberField_2;
	// System.String SAWC.wcf.NumberSet::TypeField
	String_t* ___TypeField_3;

public:
	inline static int32_t get_offset_of_extensionDataField_0() { return static_cast<int32_t>(offsetof(NumberSet_t1386763033, ___extensionDataField_0)); }
	inline ExtensionDataObject_t285311256 * get_extensionDataField_0() const { return ___extensionDataField_0; }
	inline ExtensionDataObject_t285311256 ** get_address_of_extensionDataField_0() { return &___extensionDataField_0; }
	inline void set_extensionDataField_0(ExtensionDataObject_t285311256 * value)
	{
		___extensionDataField_0 = value;
		Il2CppCodeGenWriteBarrier(&___extensionDataField_0, value);
	}

	inline static int32_t get_offset_of_ExpiredField_1() { return static_cast<int32_t>(offsetof(NumberSet_t1386763033, ___ExpiredField_1)); }
	inline bool get_ExpiredField_1() const { return ___ExpiredField_1; }
	inline bool* get_address_of_ExpiredField_1() { return &___ExpiredField_1; }
	inline void set_ExpiredField_1(bool value)
	{
		___ExpiredField_1 = value;
	}

	inline static int32_t get_offset_of_NumberField_2() { return static_cast<int32_t>(offsetof(NumberSet_t1386763033, ___NumberField_2)); }
	inline int32_t get_NumberField_2() const { return ___NumberField_2; }
	inline int32_t* get_address_of_NumberField_2() { return &___NumberField_2; }
	inline void set_NumberField_2(int32_t value)
	{
		___NumberField_2 = value;
	}

	inline static int32_t get_offset_of_TypeField_3() { return static_cast<int32_t>(offsetof(NumberSet_t1386763033, ___TypeField_3)); }
	inline String_t* get_TypeField_3() const { return ___TypeField_3; }
	inline String_t** get_address_of_TypeField_3() { return &___TypeField_3; }
	inline void set_TypeField_3(String_t* value)
	{
		___TypeField_3 = value;
		Il2CppCodeGenWriteBarrier(&___TypeField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
